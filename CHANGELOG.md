# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.63.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.61.0...v2.63.0) (2023-11-23)


### Features

* [Email] change attach files to file label (UCRM-2249) ([0dc3c8e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0dc3c8ed22802d5ceef855ecf714464aaac6a6d8))
* [Report] add row summary report type (UCRM-2180) ([d9e9cbd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d9e9cbd4090d85eaacae5d88c05570b05c3aa5ab))
* improvement custom report (UCRM-2186) ([c38a437](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c38a437c3785887d42cf13694efdc628d76fafcb))
* improvement UI knowledge base type blog (UCRM-1667) ([5f79054](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5f79054a4920c82e97fa652ea0edb1e4b0591234))
* ucrm-2263 filter condition in/exclude ([12df619](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/12df619a82e89bcb82f41795d110e60b57ce21f8))


### Bug Fixes

* [Report]  duplicate field ID chart (BUCRM-2145) ([6cf92c6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6cf92c6e489ee40d1db7d816e8caf29a7bd9a454))
* [Report] add some cal method Summary report (UCRM-2180) ([3f7da60](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3f7da60867a34ffc5b801f8a254d166b6c24f922))
* [Report] add validate modal report ([20cf7f6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/20cf7f6ea996300037177a6fdbfd685d2a03e9cf))
* [Report] load data when edit Summary report ([569c03b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/569c03b9f438d1a65c8d26b0dea632847eafcc28))
* [Report] navigate custom report ([b1e22b4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b1e22b4a28822c1b60b0b1a44abb7771a2a3483e))
* [Report] remove disabled field ([b9f18bf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b9f18bf582f72e9d932a0e790f1e330a7bc27429))
* [Report] show fields summary report ([5dbf60e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5dbf60e3653969fc6df494c1ceb07b5966ead20c))
* [Report] show fields Summary report ([1072e53](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1072e53b990415cc39bd8a42b34558d84b9629e7))
* [Report] summary report ([cb74397](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cb74397917a1d20178b76f6c166ebed31ebd3f60))
* [Voicebot] add text out ([02d1608](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/02d1608ff31d0adab590287a82e94f6b35dbeae7))


### Others

* **release:** 2.62.0 ([51814cc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/51814ccf12738f246af7b81fd9340954e61d5e21))
* **release:** 2.62.0 ([7d42f83](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7d42f837a12c42611233553c75247667be5a9002))

## [2.62.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.58.0...v2.62.0) (2023-11-16)


### Features

* [Email] change attach files to file label (UCRM-2249) ([0dc3c8e](https://10.189.120.23/crm/ucrm-fe/commit/0dc3c8ed22802d5ceef855ecf714464aaac6a6d8))
* [LDAP] allow to login with Alias (UCRM-2144) ([94875d0](https://10.189.120.23/crm/ucrm-fe/commit/94875d0ed84effea493bdb13f14471273571f15c))
* [Report] add row summary report type (UCRM-2180) ([d9e9cbd](https://10.189.120.23/crm/ucrm-fe/commit/d9e9cbd4090d85eaacae5d88c05570b05c3aa5ab))
* allow setting current time in setting min max value ([4725bd6](https://10.189.120.23/crm/ucrm-fe/commit/4725bd6574c615f978ceeb786976765baa4c3513))
* allow to setup soft required in field setting (UCRM-2131) ([511b1b1](https://10.189.120.23/crm/ucrm-fe/commit/511b1b17fd719436c9311fa4e93388453c48a000))
* bổ sung min/max cho trường ngày tháng và trường number ở form Internet (UCRM-2237) ([10a4839](https://10.189.120.23/crm/ucrm-fe/commit/10a4839caa90ec6db4a1a3519f6d6d14449eb4c9))
* export agent status (UCRM-2162) ([b050da5](https://10.189.120.23/crm/ucrm-fe/commit/b050da5cc1cb11d508a0d1607f33f8cd17296759))
* on/off cho phép search ở field select ở form internet (UCRM-2243) ([0db521f](https://10.189.120.23/crm/ucrm-fe/commit/0db521fea8f2a8677b9fd65da8b837926b50e96e))


### Bug Fixes

* [Noti] change navigate view logs ([83708b4](https://10.189.120.23/crm/ucrm-fe/commit/83708b42734f7176fa64ad979375937bfb2ff3de))
* [Report] add some cal method Summary report (UCRM-2180) ([3f7da60](https://10.189.120.23/crm/ucrm-fe/commit/3f7da60867a34ffc5b801f8a254d166b6c24f922))
* [Report] add validate modal report ([20cf7f6](https://10.189.120.23/crm/ucrm-fe/commit/20cf7f6ea996300037177a6fdbfd685d2a03e9cf))
* [Report] bug quick filter; [Tenant] add API change pass root ([2336171](https://10.189.120.23/crm/ucrm-fe/commit/2336171bef035a9cfc7a4b79aa0dd2d5976f279e))
* [Report] load data when edit Summary report ([569c03b](https://10.189.120.23/crm/ucrm-fe/commit/569c03b9f438d1a65c8d26b0dea632847eafcc28))
* [Report] navigate custom report ([b1e22b4](https://10.189.120.23/crm/ucrm-fe/commit/b1e22b4a28822c1b60b0b1a44abb7771a2a3483e))
* [Report] remove disabled field ([b9f18bf](https://10.189.120.23/crm/ucrm-fe/commit/b9f18bf582f72e9d932a0e790f1e330a7bc27429))
* [Report] show fields summary report ([5dbf60e](https://10.189.120.23/crm/ucrm-fe/commit/5dbf60e3653969fc6df494c1ceb07b5966ead20c))
* [Report] show fields Summary report ([1072e53](https://10.189.120.23/crm/ucrm-fe/commit/1072e53b990415cc39bd8a42b34558d84b9629e7))
* [Report] summary report ([cb74397](https://10.189.120.23/crm/ucrm-fe/commit/cb74397917a1d20178b76f6c166ebed31ebd3f60))
* [Voicebot] add text out ([02d1608](https://10.189.120.23/crm/ucrm-fe/commit/02d1608ff31d0adab590287a82e94f6b35dbeae7))
* allow field datetime local in internet form ([459a28a](https://10.189.120.23/crm/ucrm-fe/commit/459a28ad01957c41f6cb4e41b9e6a8766625f7a3))
* default value for time range export agent status (UCRM-2075) ([cf61aa3](https://10.189.120.23/crm/ucrm-fe/commit/cf61aa3dbadbba19033efb08744059de2c6da25c))
* fix datetime local in internet form ([823fbfe](https://10.189.120.23/crm/ucrm-fe/commit/823fbfe5888e09238f4bdf14af1473661ec02207))
* fix datetime local in internet form (UCRM-2074) ([8ac0031](https://10.189.120.23/crm/ucrm-fe/commit/8ac0031ee0e79140ca03bb5a3313f3306297fdae))


### Others

* **release:** 2.58.0 ([680107d](https://10.189.120.23/crm/ucrm-fe/commit/680107d57b90aba64e8cd4d7661d1a3d03ef8cac))
* **release:** 2.59.0 ([38340ee](https://10.189.120.23/crm/ucrm-fe/commit/38340ee2a593d8308344fe50861335d3c04644e0))
* **release:** 2.60.0 ([e0443be](https://10.189.120.23/crm/ucrm-fe/commit/e0443be3bbcf757a4a0f2589a745d4fcb29d9ad7))
* **release:** 2.61.0 ([c3a7024](https://10.189.120.23/crm/ucrm-fe/commit/c3a70243425b47e33b677f031adf9d315bf4960e))

## [2.61.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.60.0...v2.61.0) (2023-11-13)


### Features

* allow setting current time in setting min max value ([4725bd6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4725bd6574c615f978ceeb786976765baa4c3513))

## [2.60.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.59.0...v2.60.0) (2023-11-10)


### Features

* bổ sung min/max cho trường ngày tháng và trường number ở form Internet (UCRM-2237) ([10a4839](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/10a4839caa90ec6db4a1a3519f6d6d14449eb4c9))
* on/off cho phép search ở field select ở form internet (UCRM-2243) ([0db521f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0db521fea8f2a8677b9fd65da8b837926b50e96e))

## [2.59.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.52.0...v2.59.0) (2023-11-03)


### Features

* [AssignmentRule] add working time (UCRM-2075) ([6ab4237](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6ab42372db24365a88c6fa014ca970b0675e46d1))
* [AssignmentRule] allow to set the maximum number (UCRM-2081) ([5d55c1e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d55c1edee62de8bd4fab2172e3db7fcb0fd6517))
* [Comments] add sort comments (UCRM-1763) ([78d8e68](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/78d8e684c95e545b7c5d84fcf893bc4d052f714c))
* [DateTimeSetting] allow tenant to setup working hours and holidays (UCRM-2063) ([695b678](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/695b6789f025d11e69141cb5f689dd92caa5e31a))
* [Dynamic button] add Triggered User in action Create/Update/Call API (UCRM-1745) ([13214da](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/13214dac15bb4ea3d49453423f1391c94407b9ae))
* [LDAP] allow to login with Alias (UCRM-2144) ([94875d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/94875d0ed84effea493bdb13f14471273571f15c))
* **[object standard] rewrite api for new object standard contact:** uCRM-1888 ([049d434](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/049d43466ed1050ad744e6e8c7a282b1c3e89d3a))
* [Report] allow user save quick filter (UCRM-2006) ([d9ee4e1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d9ee4e1ab4877633ceecdf4465910d4a2aba450e))
* [URLPopup] allow add multiple param in URL popup (UCRM-1906) ([96b3830](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96b3830f480b4689d8fb90e2fc438a8eaf5439cf))
* [Workflow] allow to use field ID and Assign To in Create record and Update Related Source (UCRM-2138) ([a08a749](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a08a74943f0a997d2d0d8014028613195412177f))
* [Workflow] allow to use selected value of triggered record (UCRM-2095) ([17dab37](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/17dab37180784edb1772ade7e4dd1a706e7d4ad7))
* add knowledge in left-right component (UCRM-1631) ([ee368c6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ee368c67d8ad35b3673ea94717293d61328cf709))
* add new Consolidated view setting (UCRM-1974) ([30e9da0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30e9da09c190e3fc0f7a60700e48476cdde5b95a))
* allow add knowledge base in right component (UCRM-1631) ([dbbed0a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dbbed0a7b756f5f8b66763e13e4bf8bb35484d61))
* allow condition pagination in internet form (UCRM-1863) ([85fcbf4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/85fcbf43e84e8288253fd94a27db610ad3d9f39d))
* allow delete tenants (UCRM-1962) ([bb4894a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bb4894ac5e9570114268fefe8df55aac32f217b5))
* allow display default value in form description (UCRM-1869) ([6689240](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6689240a9a434b78b5262328225c1d107ed318d2))
* allow generate link QR code in internet form ([a7dcee4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a7dcee4958dd30ee0b4a2325b7c38c4d618ae921))
* allow pagination in internet form (UCRM-496) ([762bee5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/762bee5696ec7aaa595325331bc61c07a951dfe1))
* allow setting default value for internet form - progress (UCRM-1938) ([48c84ff](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/48c84ffb63bba287b0edc141fc4a4def0889d569))
* allow sort form in internet form (UCRM-1980) ([6884938](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/68849382ffac3fd8cb1feacfb177614edf89460b))
* allow to increase/decrease field number when Update Record (UCRM-389) ([0c0c74f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0c0c74f8213bf2dd952b358b2b4ac9a28c67d322))
* allow to increase/decrease field number when Update Source Record (UCRM-390) ([2f00da2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f00da27e29ba7e982bf2b11c42a256e293f4b9d))
* allow to setup soft required in field setting (UCRM-2131) ([511b1b1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/511b1b17fd719436c9311fa4e93388453c48a000))
* conditional pagination in internetform (UCRM-1863) ([3430fb3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3430fb39f0c2d24fe5241852304f464cf21d9aab))
* export agent status (UCRM-2162) ([b050da5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b050da5cc1cb11d508a0d1607f33f8cd17296759))
* format value number in internet form (UCRM-2024) ([4df55e3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4df55e300219383faf7d92b2e544ce753f4e058d))
* mask value input in internet form (UCRM-2018) ([5501171](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5501171632ca67105681641f773a91f9f44fbd6f))
* or conditions in SLA (UCRM-2000) ([943fb24](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/943fb24dffffbfb04791482e4d217051ca7e1b9a))
* save IP adress of customer submit form (UCRM-1944) ([373fc3d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/373fc3d42ab62bfae1bcdd625934ba28fbddb64e))
* ucrm-2069 [Workflow] Condition to check if the created date or modified time ([559d019](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/559d0196a3a8304f9ad60cad4ca6606b759d2ca0))
* uCRM-2087 Tính năng merge Contact theo mô hình standard Object ([e13a8cb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e13a8cb7bc906cac2244025d6ba651cab8749387))


### Bug Fixes

* [Automation Selenium] add id settings page ([80eb111](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/80eb111f3bbd9431f6f817dc96af5181c0766a90))
* [Automation Selenium] add id settings page ([7fa7e84](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7fa7e84b43af55cea04731bb42ebde9f62624687))
* [Automation Selenium] add id settings page ([854f8bb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/854f8bb595c2ed09def1034b1a938634e3fe6b85))
* [Dynamic button] add allow clear select type ([7e4e4b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e4e4b0fbe8e0426cd136cfb404b57693ef1be40))
* [Dynamic Button] add format date when update fields (BUCRM-1846) ([95fc6ce](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/95fc6ce34358cf5dd514ee9be16b70cc7ddfe8a6))
* [Dynamic Button] add required fields and validate date (UCRM-1739) ([c9d2e03](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c9d2e03ce201780c5574649408f3fee4a43baae9))
* [Dynamic Button] change record data when update in listview (BUCRM-1834) ([d17c404](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d17c404e6b800089901c47e525ec01c546f4502c))
* [Dynamic button] display data in Details, multi details (BUCRM-1834) ([6c1fbbb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6c1fbbb783d9d702464eb70ccfa5a0da9a7858aa))
* [Dynamic button] remove create date and modify time in Update fields (BUCRM-1850) ([cc2bfcd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc2bfcd6b4007645fd707aeab23a0a10e5ac2272))
* [DynamicButton] Add select, date, datetime in modal udpate fields (UCRM-1739) ([e300d7e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e300d7ecb0815245e56c89e3e3216a74cf72e478))
* [Email Outgoing] remove password empty (BUCRM-1553) ([cfd2c96](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cfd2c96051346f31ab65e41d33feb285965fa269))
* [Email] remove style css when parse in Editor (BUCRM-1744) ([f58106a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f58106a335cd6b16edc690cdd6c1e922e7e9feac))
* [IC Integration] reset value after change object (BUCRM-1739) ([fbf6f26](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fbf6f269f7ad9e701e6e2a27fc5cf2ddc45754b2))
* [Interaction] add data ws to update interaction (UCRM-1809) ([1db2701](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1db2701d398e3e6b9988e75c262283e7a1d9c2b9))
* [KB] add API public, prevent right click details ([0d43575](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d435752d3debd2f0ef676daf7378bac7e792827))
* [KB] upload base64 image; handle link media ([64a46f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64a46f0d11529aa1d6d9446964905cc407249012))
* [Modal interaction] overflow UI body email (BUCRM-1734) ([534bf64](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/534bf6427a37a9c0894a67a8f358e1551caae3bb))
* [Modal record] error when upload file in call and table (BUCRM-1788) ([9c95dd4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9c95dd45abae869d5b5346760af40c45ed680d34))
* [Modal Record] remove empty tag editor field when update record (BUCRM-1755) ([fd94571](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fd94571b79d848154c9212a2d1d4df87ec9ff729))
* [Modal report] add id report type ([8663a57](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8663a57bd0ec7a88a2025c349a1cb3affcc78b21))
* [Noti] change navigate view logs ([83708b4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/83708b42734f7176fa64ad979375937bfb2ff3de))
* [Report] bug quick filter; [Tenant] add API change pass root ([2336171](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2336171bef035a9cfc7a4b79aa0dd2d5976f279e))
* [SLA] hide pending condition when deadline is active ([a99c33d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a99c33d44a0d4d43b648258b3871f1e445a1b95e))
* [URL Popup] add decodeURI (UCRM-1906) ([f5ce969](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f5ce969be894dfd30ef843a552c697756d623f70))
* [VoiceBiometric] add agent name ([eab36f7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eab36f71ad416e303029c53c246d787cfd9f45c6))
* [VoiceBiometric] call voice authen one time ([0854376](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0854376f3866bea5a99f747a2737796f7cc5cb42))
* add authen file (BUCRM-1777) ([25c369d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25c369d56ae024896a1273198a840bee02c6b67d))
* add button scroll quickFilter and Style details info IC (BUCRM 1767) ([cedfb3a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cedfb3ace4cc00ad2acb960f0b02bd13fd5f9d1b))
* add Icomoon (UCRM-1968) ([da38f19](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/da38f19f0caf6b548ea2293971a4695ffffff4a0))
* add id for quanbmn ([955155a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/955155a7298d620724767b45ab4d694e66ddae66))
* add id for quanbnm ([417229a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/417229ac6ce537451202dce0bf940a63aa2aa3f1))
* add id sign out for quanbmn ([d20173c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d20173cc4a8b29e4688120eeeed5c4ea0315fb04))
* add load user in comment (UCRM-1839) ([2c1b170](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2c1b170d07093ccd28556bb3ef8d085fcdf166d1))
* add sort comments in QM (UCRM-1763) ([44c0ed2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/44c0ed2f7816a25a97597ca7df9799bf494409ff))
* advance config (UCRM-1803) ([97dde50](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/97dde50a50bf560bd9a47b99c9438d9b722b0bc3))
* advance config (UCRM-1803) ([4fe0a37](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4fe0a3794332eadf2c00462770d6d72e1039e1c1))
* advanced config (UCRM-1803) ([5884c32](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5884c32ab2de0730f428ded98c9ae2bb54ade91c))
* allow field datetime local in internet form ([459a28a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/459a28ad01957c41f6cb4e41b9e6a8766625f7a3))
* allow pagination in internet form - process setting (UCRM-496) ([ff2d12e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ff2d12ed63b40594620fff8d37540f34b1a043f5))
* allow pagination in internet form (UCRM-496) ([c73b5e2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c73b5e2955f073b84aec51c318a4df03ed70c272))
* allow pagination in internet form (UCRM-496) ([a3e432e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a3e432ea8091393c954d6d7681aef1834074193a))
* allow pagination in internet form (UCRM-496) ([521a2f7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/521a2f724a45887ab08bda219f272263bd6b1115))
* allow pagination in internet form (UCRM-496) ([7bc98d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7bc98d06c08a5a22c61ac9396fb6be6bee84b341))
* allow pagination in internet form (UCRM-496) ([8a7712a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8a7712a285f93cffbd082da05f164e054cfc6af7))
* allow pagination in internet form (UCRM-496) ([54d9ce7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/54d9ce7744469fd604a46703e0bdbf6d3b6c8e9a))
* allow pagination in internet form (UCRM-496) ([6faea57](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6faea57f3e5439404c3d21e63ce1836236879837))
* allow pagination in internet form (UCRM-496) ([997de5f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/997de5f216c1024bb103920b787a415ddbb5320b))
* allow set default value internet form - inprogress (UCRM-1938) ([b19847a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b19847a9d2d554981f4be35b9384df5eafa65131))
* authen data type file ([f50e0e3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f50e0e32f3608ef38510edd3bfbab535ed7fa8e4))
* base64 only in KB ([52901ce](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/52901cef952f3ac6becafd69351f21f0da5936db))
* bucrm 2056 formula field in condition ([9a7549e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9a7549e5d701bbe37ba3d10f30b613cf83d50b41))
* **bucrm-1800:** fix when reload auto disable action object ([d247e23](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d247e23f6ab29d964463f1a0e33e3b1399aee756))
* **bucrm-1840 500 when edit record:** bucrm-1840 ([fda793e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fda793e22fd15f1acdf2eae86294169ce4a867ea))
* bucrm-1964 security issue about payload of API ([7111ed1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7111ed150f57fa9fcb672aca334699da5a616b7c))
* bucrm-1970 fix payload api ([6569bfe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6569bfe05c55f261547d6a1e702f317e566c71cc))
* bug update undefinded pass (UCRM 1851) ([9304c66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9304c665067c0327fb4912ba406d954646792b03))
* burcm-1973 fix payload is not define ([ad3f935](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ad3f935ca73ddd46b374c350fe0d02c2aad05335))
* **cai thien temaplte record ucrm-1733:** ucrm-1733 ([2a56469](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2a56469bc7d21ab5b1d61910d4bb4f8deb45ed29))
* **cai thien template edit ucrm1733:** ucrm1733 ([5d9b1c7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d9b1c74278088e3ee07a2d96f992b968d020c8a))
* **can not submit form:** bUCRM-1909 ([9f6695e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9f6695e0f4d16885c18dcac19580ac229c4c9175))
* checkbox use fixed deadline ([14b8783](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/14b8783c23b6aa8f1ed9a13039c627252c5f3b92))
* **date to parts allow get weekday:** uCRM1900 ([64cf425](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64cf4250dbb0c00f17f33d7bdfb7f11dceceefae))
* default value for time range export agent status (UCRM-2075) ([cf61aa3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cf61aa3dbadbba19033efb08744059de2c6da25c))
* disable unit datetoParts type (UCRM 1900) ([807818d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/807818de94992e37d8e8ec758718cc38aa9d84f9))
* display increase/decrease field number in workflow (UCRM-389) ([c5db277](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c5db27780477f92f8e80e5244b31d143a8cb1c26))
* dynamic pagination in Internet form (UCRM-1863) ([6056930](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/605693008c5e4b22565b4290e70b7aba571b3b70))
* encode password type owa ([2522037](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25220377226f72b81ec5f075848dcc61a61fa868))
* fix 500 when double click ([e8e0d8b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8e0d8bddf531e9c574b03c5ac9152308e19db2d))
* fix 500 when mass edit field editor (UCRM-1897) ([2a2a0dc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2a2a0dcb967654124eae9f5aef828d4a15448ee8))
* fix bimat component bucrm-1815 ([dea3a25](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dea3a255c1ffce2ed4cf98efa63fb58bcc8feddc))
* fix bug email (UCRM-1920) ([48f6579](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/48f657937516072ea6824d2d1d0cc98d3b24a1ff))
* fix bug pagination (UCRM-496) ([9d748ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9d748aeb55bf7f9c86075e9c02e64546769c2065))
* fix bug pagination in internet form (UCRM-496) ([b233fb7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b233fb7593be915fa8326a1bde9b2d90b1d2b1e5))
* fix custom report (UCRM-1068) ([476326c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/476326c9726b7be177f4ddc6f06d8d47d8c8f773))
* fix custom view QM (BUCRM 1765) ([9c2302a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9c2302ad40c9b364cee476590f1364512b472e71))
* fix datetime local in internet form ([823fbfe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/823fbfe5888e09238f4bdf14af1473661ec02207))
* fix datetime local in internet form (UCRM-2074) ([8ac0031](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8ac0031ee0e79140ca03bb5a3313f3306297fdae))
* fix defalut value in form (UCRM-1938) ([d40382d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d40382db8698cec4a104ccaf6163c14e81df55c9))
* fix delay text in modal record (BUCRM-1789) ([d3054b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3054b03cd6cabaf48464d64e99d83c84d2a5664))
* fix delete tenant (UCRM-1962) ([f8d2819](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8d281972e9ec0be2aca5d13dc19d714e7f50af0))
* fix field textarea edtior (UCRM-1870) ([338fa4f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/338fa4fceecb44f0e8de202ac55bd6656d483521))
* fix internet form (UCRM-1697) ([6723cc5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6723cc5d23111fba1d90d4af8bfaffc5155a49b7))
* fix knowledge base in consolidate view(UCRM-1631) ([5b49109](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5b49109bc169b36f954068e1b2d856e3e79e984a))
* fix not clear old data in modal share (BUCRM-1857) ([1965d88](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1965d8863ad0d4e7bc41198a753e892176a59c63))
* fix paginaiton in internet form (UCRM-496) ([18ba621](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/18ba621481b2c40936cba07ee23b7003240de0c5))
* fix pagination in internet form (BUCRM-1854) ([2f654c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f654c14906cb7a3bc149f1128d5c6ba4b339efe))
* fix pagination in internet form (BUCRM-1854) ([023810a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/023810a3462df4c1e0105c4d192d945d10857952))
* fix pagination in internet form (BUCRM-1857) ([3ac6ff9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ac6ff980d5e40b69d28ddc72df8655ed0fd70bf))
* fix permission QM (BUCRM 1762) ([57e8542](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/57e85421af86253c763262c2943bbace97f20ac4))
* fix permission quickfilter QM (BUCRM 1760) ([d09fb36](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d09fb36191aa60e12e2e3ff731cd0b33aaf5daf9))
* fix save ip customer submit form (UCRM-1944) ([282b584](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/282b584fadda05b55573ae988acd20a28f1098e8))
* image in comment ([cc984f6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc984f6e64ff2ee001a476283e216c9cc12ca7ab))
* layout Comment QM View ([a19aa6f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a19aa6f0e2cedae4bf3c07ec18124f9e2fcf6db1))
* link media object related ([425526b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/425526b022c3ee521616b71e85a4884fb49af2c4))
* multiDetail crash page ([749faaa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/749faaa5b003d1bf5b242c9fa9d5aac8b37de84c))
* pagination form in QM (UCRM-496) ([9fa306b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9fa306bf6ed061bdbfd14789ee530305fffa0b35))
* pagination in form setting (UCRM-1925) ([e4c3c35](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e4c3c359a0f8e2afa25c2d02a1f64a70a02bbc58))
* pagination in internet form (UCRM-496) ([ee567ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ee567ae8ca33cd379743646e07e779c2460fa8ad))
* pagination in internet form (UCRM-496) ([ba004fe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ba004fe06e3ba529e878afd8e71b439e2c382024))
* pagination in internet form (UCRM-496) ([0d1f04c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d1f04cb0b00d9cba6af8cb7f038f6146290d636))
* permission QM ([ef6526f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ef6526f2a31c535b6f0463de5c4ddc5c47279a0b))
* remove redundant APIs get user (UCRM-1839) ([9852914](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/98529145e0c72489043fbe7bf3b2b9e4b6591b15))
* search field formula ([66f5a55](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/66f5a5561d5a4191045852b8883a33a05c55acf9))
* show form type ThirdParty (QM) ([e214ae7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e214ae772c02fc6363f03730c90d99ba6320c261))
* update search in knowledge base (UCRM-1787) ([829fd04](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/829fd04848be68d73e9d9b63b96b4c3a71e54461))


### Others

* **release:** 2.53.0 ([c85b966](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c85b96655603e396b4262128147b3b95cf834fa7))
* **release:** 2.54.0 ([d19ccf6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d19ccf62427296125c3ca0199a22c4ff9e9b08e1))
* **release:** 2.55.0 ([936d877](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/936d877aceea689cbda1e1408f9fbbcafe74a418))
* **release:** 2.56.0 ([0c3093d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0c3093d3f739cc85a7406a64e66f90f9702429c7))
* **release:** 2.57.0 ([785bfa6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/785bfa661081137d8eeca7a8bace0c6e9f1435c6))
* **release:** 2.57.1 ([c02a0b5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c02a0b597bbffb4751abd618dc2a0b35b5793bf2))
* **release:** 2.57.2 ([562de50](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/562de50288aa53a2feb04d17ce0243ff068680ea))
* **release:** 2.57.2 ([7e72d42](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e72d42340ee96be9019ca3f7f59f00094885515))
* **release:** 2.57.3 ([db48452](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/db48452ba945a88bb26b51ee9080194592ec7d9f))
* **release:** 2.58.0 ([680107d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/680107d57b90aba64e8cd4d7661d1a3d03ef8cac))
* **release:** 2.58.0 ([3723ee6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3723ee62b2b3f71aae83a49da7bf6815f9f8f62b))

## [2.58.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.55.0...v2.58.0) (2023-10-26)


### Features

* [AssignmentRule] add working time (UCRM-2075) ([6ab4237](https://10.189.120.23/crm/ucrm-fe/commit/6ab42372db24365a88c6fa014ca970b0675e46d1))
* [AssignmentRule] allow to set the maximum number (UCRM-2081) ([5d55c1e](https://10.189.120.23/crm/ucrm-fe/commit/5d55c1edee62de8bd4fab2172e3db7fcb0fd6517))
* [DateTimeSetting] allow tenant to setup working hours and holidays (UCRM-2063) ([695b678](https://10.189.120.23/crm/ucrm-fe/commit/695b6789f025d11e69141cb5f689dd92caa5e31a))
* **[object standard] rewrite api for new object standard contact:** uCRM-1888 ([049d434](https://10.189.120.23/crm/ucrm-fe/commit/049d43466ed1050ad744e6e8c7a282b1c3e89d3a))
* [Report] allow user save quick filter (UCRM-2006) ([d9ee4e1](https://10.189.120.23/crm/ucrm-fe/commit/d9ee4e1ab4877633ceecdf4465910d4a2aba450e))
* [Workflow] allow to use field ID and Assign To in Create record and Update Related Source (UCRM-2138) ([a08a749](https://10.189.120.23/crm/ucrm-fe/commit/a08a74943f0a997d2d0d8014028613195412177f))
* [Workflow] allow to use selected value of triggered record (UCRM-2095) ([17dab37](https://10.189.120.23/crm/ucrm-fe/commit/17dab37180784edb1772ade7e4dd1a706e7d4ad7))
* add new Consolidated view setting (UCRM-1974) ([30e9da0](https://10.189.120.23/crm/ucrm-fe/commit/30e9da09c190e3fc0f7a60700e48476cdde5b95a))
* allow condition pagination in internet form (UCRM-1863) ([85fcbf4](https://10.189.120.23/crm/ucrm-fe/commit/85fcbf43e84e8288253fd94a27db610ad3d9f39d))
* allow delete tenants (UCRM-1962) ([bb4894a](https://10.189.120.23/crm/ucrm-fe/commit/bb4894ac5e9570114268fefe8df55aac32f217b5))
* allow pagination in internet form (UCRM-496) ([762bee5](https://10.189.120.23/crm/ucrm-fe/commit/762bee5696ec7aaa595325331bc61c07a951dfe1))
* allow setting default value for internet form - progress (UCRM-1938) ([48c84ff](https://10.189.120.23/crm/ucrm-fe/commit/48c84ffb63bba287b0edc141fc4a4def0889d569))
* allow sort form in internet form (UCRM-1980) ([6884938](https://10.189.120.23/crm/ucrm-fe/commit/68849382ffac3fd8cb1feacfb177614edf89460b))
* conditional pagination in internetform (UCRM-1863) ([3430fb3](https://10.189.120.23/crm/ucrm-fe/commit/3430fb39f0c2d24fe5241852304f464cf21d9aab))
* format value number in internet form (UCRM-2024) ([4df55e3](https://10.189.120.23/crm/ucrm-fe/commit/4df55e300219383faf7d92b2e544ce753f4e058d))
* mask value input in internet form (UCRM-2018) ([5501171](https://10.189.120.23/crm/ucrm-fe/commit/5501171632ca67105681641f773a91f9f44fbd6f))
* or conditions in SLA (UCRM-2000) ([943fb24](https://10.189.120.23/crm/ucrm-fe/commit/943fb24dffffbfb04791482e4d217051ca7e1b9a))
* save IP adress of customer submit form (UCRM-1944) ([373fc3d](https://10.189.120.23/crm/ucrm-fe/commit/373fc3d42ab62bfae1bcdd625934ba28fbddb64e))
* ucrm-2069 [Workflow] Condition to check if the created date or modified time ([559d019](https://10.189.120.23/crm/ucrm-fe/commit/559d0196a3a8304f9ad60cad4ca6606b759d2ca0))
* uCRM-2087 Tính năng merge Contact theo mô hình standard Object ([e13a8cb](https://10.189.120.23/crm/ucrm-fe/commit/e13a8cb7bc906cac2244025d6ba651cab8749387))


### Bug Fixes

* [Dynamic button] remove create date and modify time in Update fields (BUCRM-1850) ([cc2bfcd](https://10.189.120.23/crm/ucrm-fe/commit/cc2bfcd6b4007645fd707aeab23a0a10e5ac2272))
* [Email Outgoing] remove password empty (BUCRM-1553) ([cfd2c96](https://10.189.120.23/crm/ucrm-fe/commit/cfd2c96051346f31ab65e41d33feb285965fa269))
* [Interaction] add data ws to update interaction (UCRM-1809) ([1db2701](https://10.189.120.23/crm/ucrm-fe/commit/1db2701d398e3e6b9988e75c262283e7a1d9c2b9))
* [Modal report] add id report type ([8663a57](https://10.189.120.23/crm/ucrm-fe/commit/8663a57bd0ec7a88a2025c349a1cb3affcc78b21))
* [VoiceBiometric] add agent name ([eab36f7](https://10.189.120.23/crm/ucrm-fe/commit/eab36f71ad416e303029c53c246d787cfd9f45c6))
* [VoiceBiometric] call voice authen one time ([0854376](https://10.189.120.23/crm/ucrm-fe/commit/0854376f3866bea5a99f747a2737796f7cc5cb42))
* add Icomoon (UCRM-1968) ([da38f19](https://10.189.120.23/crm/ucrm-fe/commit/da38f19f0caf6b548ea2293971a4695ffffff4a0))
* add id for quanbmn ([955155a](https://10.189.120.23/crm/ucrm-fe/commit/955155a7298d620724767b45ab4d694e66ddae66))
* add id for quanbnm ([417229a](https://10.189.120.23/crm/ucrm-fe/commit/417229ac6ce537451202dce0bf940a63aa2aa3f1))
* add id sign out for quanbmn ([d20173c](https://10.189.120.23/crm/ucrm-fe/commit/d20173cc4a8b29e4688120eeeed5c4ea0315fb04))
* allow pagination in internet form - process setting (UCRM-496) ([ff2d12e](https://10.189.120.23/crm/ucrm-fe/commit/ff2d12ed63b40594620fff8d37540f34b1a043f5))
* allow pagination in internet form (UCRM-496) ([c73b5e2](https://10.189.120.23/crm/ucrm-fe/commit/c73b5e2955f073b84aec51c318a4df03ed70c272))
* allow pagination in internet form (UCRM-496) ([a3e432e](https://10.189.120.23/crm/ucrm-fe/commit/a3e432ea8091393c954d6d7681aef1834074193a))
* allow pagination in internet form (UCRM-496) ([521a2f7](https://10.189.120.23/crm/ucrm-fe/commit/521a2f724a45887ab08bda219f272263bd6b1115))
* allow pagination in internet form (UCRM-496) ([7bc98d0](https://10.189.120.23/crm/ucrm-fe/commit/7bc98d06c08a5a22c61ac9396fb6be6bee84b341))
* allow pagination in internet form (UCRM-496) ([8a7712a](https://10.189.120.23/crm/ucrm-fe/commit/8a7712a285f93cffbd082da05f164e054cfc6af7))
* allow pagination in internet form (UCRM-496) ([54d9ce7](https://10.189.120.23/crm/ucrm-fe/commit/54d9ce7744469fd604a46703e0bdbf6d3b6c8e9a))
* allow pagination in internet form (UCRM-496) ([6faea57](https://10.189.120.23/crm/ucrm-fe/commit/6faea57f3e5439404c3d21e63ce1836236879837))
* allow pagination in internet form (UCRM-496) ([997de5f](https://10.189.120.23/crm/ucrm-fe/commit/997de5f216c1024bb103920b787a415ddbb5320b))
* allow set default value internet form - inprogress (UCRM-1938) ([b19847a](https://10.189.120.23/crm/ucrm-fe/commit/b19847a9d2d554981f4be35b9384df5eafa65131))
* bucrm 2056 formula field in condition ([9a7549e](https://10.189.120.23/crm/ucrm-fe/commit/9a7549e5d701bbe37ba3d10f30b613cf83d50b41))
* bucrm-1964 security issue about payload of API ([7111ed1](https://10.189.120.23/crm/ucrm-fe/commit/7111ed150f57fa9fcb672aca334699da5a616b7c))
* bucrm-1970 fix payload api ([6569bfe](https://10.189.120.23/crm/ucrm-fe/commit/6569bfe05c55f261547d6a1e702f317e566c71cc))
* burcm-1973 fix payload is not define ([ad3f935](https://10.189.120.23/crm/ucrm-fe/commit/ad3f935ca73ddd46b374c350fe0d02c2aad05335))
* **can not submit form:** bUCRM-1909 ([9f6695e](https://10.189.120.23/crm/ucrm-fe/commit/9f6695e0f4d16885c18dcac19580ac229c4c9175))
* dynamic pagination in Internet form (UCRM-1863) ([6056930](https://10.189.120.23/crm/ucrm-fe/commit/605693008c5e4b22565b4290e70b7aba571b3b70))
* fix 500 when mass edit field editor (UCRM-1897) ([2a2a0dc](https://10.189.120.23/crm/ucrm-fe/commit/2a2a0dcb967654124eae9f5aef828d4a15448ee8))
* fix bug email (UCRM-1920) ([48f6579](https://10.189.120.23/crm/ucrm-fe/commit/48f657937516072ea6824d2d1d0cc98d3b24a1ff))
* fix bug pagination (UCRM-496) ([9d748ae](https://10.189.120.23/crm/ucrm-fe/commit/9d748aeb55bf7f9c86075e9c02e64546769c2065))
* fix bug pagination in internet form (UCRM-496) ([b233fb7](https://10.189.120.23/crm/ucrm-fe/commit/b233fb7593be915fa8326a1bde9b2d90b1d2b1e5))
* fix custom report (UCRM-1068) ([476326c](https://10.189.120.23/crm/ucrm-fe/commit/476326c9726b7be177f4ddc6f06d8d47d8c8f773))
* fix defalut value in form (UCRM-1938) ([d40382d](https://10.189.120.23/crm/ucrm-fe/commit/d40382db8698cec4a104ccaf6163c14e81df55c9))
* fix delete tenant (UCRM-1962) ([f8d2819](https://10.189.120.23/crm/ucrm-fe/commit/f8d281972e9ec0be2aca5d13dc19d714e7f50af0))
* fix field textarea edtior (UCRM-1870) ([338fa4f](https://10.189.120.23/crm/ucrm-fe/commit/338fa4fceecb44f0e8de202ac55bd6656d483521))
* fix not clear old data in modal share (BUCRM-1857) ([1965d88](https://10.189.120.23/crm/ucrm-fe/commit/1965d8863ad0d4e7bc41198a753e892176a59c63))
* fix paginaiton in internet form (UCRM-496) ([18ba621](https://10.189.120.23/crm/ucrm-fe/commit/18ba621481b2c40936cba07ee23b7003240de0c5))
* fix pagination in internet form (BUCRM-1854) ([2f654c1](https://10.189.120.23/crm/ucrm-fe/commit/2f654c14906cb7a3bc149f1128d5c6ba4b339efe))
* fix pagination in internet form (BUCRM-1854) ([023810a](https://10.189.120.23/crm/ucrm-fe/commit/023810a3462df4c1e0105c4d192d945d10857952))
* fix pagination in internet form (BUCRM-1857) ([3ac6ff9](https://10.189.120.23/crm/ucrm-fe/commit/3ac6ff980d5e40b69d28ddc72df8655ed0fd70bf))
* fix save ip customer submit form (UCRM-1944) ([282b584](https://10.189.120.23/crm/ucrm-fe/commit/282b584fadda05b55573ae988acd20a28f1098e8))
* pagination form in QM (UCRM-496) ([9fa306b](https://10.189.120.23/crm/ucrm-fe/commit/9fa306bf6ed061bdbfd14789ee530305fffa0b35))
* pagination in form setting (UCRM-1925) ([e4c3c35](https://10.189.120.23/crm/ucrm-fe/commit/e4c3c359a0f8e2afa25c2d02a1f64a70a02bbc58))
* pagination in internet form (UCRM-496) ([ee567ae](https://10.189.120.23/crm/ucrm-fe/commit/ee567ae8ca33cd379743646e07e779c2460fa8ad))
* pagination in internet form (UCRM-496) ([ba004fe](https://10.189.120.23/crm/ucrm-fe/commit/ba004fe06e3ba529e878afd8e71b439e2c382024))
* pagination in internet form (UCRM-496) ([0d1f04c](https://10.189.120.23/crm/ucrm-fe/commit/0d1f04cb0b00d9cba6af8cb7f038f6146290d636))


### Others

* **release:** 2.56.0 ([0c3093d](https://10.189.120.23/crm/ucrm-fe/commit/0c3093d3f739cc85a7406a64e66f90f9702429c7))
* **release:** 2.57.0 ([785bfa6](https://10.189.120.23/crm/ucrm-fe/commit/785bfa661081137d8eeca7a8bace0c6e9f1435c6))
* **release:** 2.57.1 ([c02a0b5](https://10.189.120.23/crm/ucrm-fe/commit/c02a0b597bbffb4751abd618dc2a0b35b5793bf2))
* **release:** 2.57.2 ([562de50](https://10.189.120.23/crm/ucrm-fe/commit/562de50288aa53a2feb04d17ce0243ff068680ea))
* **release:** 2.57.2 ([7e72d42](https://10.189.120.23/crm/ucrm-fe/commit/7e72d42340ee96be9019ca3f7f59f00094885515))
* **release:** 2.57.3 ([db48452](https://10.189.120.23/crm/ucrm-fe/commit/db48452ba945a88bb26b51ee9080194592ec7d9f))

### [2.57.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.57.2...v2.57.3) (2023-10-12)


### Bug Fixes

* [VoiceBiometric] add agent name ([eab36f7](https://10.189.120.23/crm/ucrm-fe/commit/eab36f71ad416e303029c53c246d787cfd9f45c6))
* [VoiceBiometric] call voice authen one time ([0854376](https://10.189.120.23/crm/ucrm-fe/commit/0854376f3866bea5a99f747a2737796f7cc5cb42))


### Others

* **release:** 2.57.2 ([562de50](https://10.189.120.23/crm/ucrm-fe/commit/562de50288aa53a2feb04d17ce0243ff068680ea))

### [2.57.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.57.0...v2.57.1) (2023-09-21)


### Bug Fixes

* **can not submit form:** bUCRM-1909 ([9f6695e](https://10.189.120.23/crm/ucrm-fe/commit/9f6695e0f4d16885c18dcac19580ac229c4c9175))
* fix bug email (UCRM-1920) ([48f6579](https://10.189.120.23/crm/ucrm-fe/commit/48f657937516072ea6824d2d1d0cc98d3b24a1ff))
* pagination in form setting (UCRM-1925) ([e4c3c35](https://10.189.120.23/crm/ucrm-fe/commit/e4c3c359a0f8e2afa25c2d02a1f64a70a02bbc58))

## [2.57.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.54.0...v2.57.0) (2023-09-14)


### Features

* [Comments] add sort comments (UCRM-1763) ([78d8e68](https://10.189.120.23/crm/ucrm-fe/commit/78d8e684c95e545b7c5d84fcf893bc4d052f714c))
* [Dynamic button] add Triggered User in action Create/Update/Call API (UCRM-1745) ([13214da](https://10.189.120.23/crm/ucrm-fe/commit/13214dac15bb4ea3d49453423f1391c94407b9ae))
* **[object standard] rewrite api for new object standard contact:** uCRM-1888 ([049d434](https://10.189.120.23/crm/ucrm-fe/commit/049d43466ed1050ad744e6e8c7a282b1c3e89d3a))
* [URLPopup] allow add multiple param in URL popup (UCRM-1906) ([96b3830](https://10.189.120.23/crm/ucrm-fe/commit/96b3830f480b4689d8fb90e2fc438a8eaf5439cf))
* add new Consolidated view setting (UCRM-1974) ([30e9da0](https://10.189.120.23/crm/ucrm-fe/commit/30e9da09c190e3fc0f7a60700e48476cdde5b95a))
* allow condition pagination in internet form (UCRM-1863) ([85fcbf4](https://10.189.120.23/crm/ucrm-fe/commit/85fcbf43e84e8288253fd94a27db610ad3d9f39d))
* allow delete tenants (UCRM-1962) ([bb4894a](https://10.189.120.23/crm/ucrm-fe/commit/bb4894ac5e9570114268fefe8df55aac32f217b5))
* allow pagination in internet form (UCRM-496) ([762bee5](https://10.189.120.23/crm/ucrm-fe/commit/762bee5696ec7aaa595325331bc61c07a951dfe1))
* allow setting default value for internet form - progress (UCRM-1938) ([48c84ff](https://10.189.120.23/crm/ucrm-fe/commit/48c84ffb63bba287b0edc141fc4a4def0889d569))
* conditional pagination in internetform (UCRM-1863) ([3430fb3](https://10.189.120.23/crm/ucrm-fe/commit/3430fb39f0c2d24fe5241852304f464cf21d9aab))
* or conditions in SLA (UCRM-2000) ([943fb24](https://10.189.120.23/crm/ucrm-fe/commit/943fb24dffffbfb04791482e4d217051ca7e1b9a))
* save IP adress of customer submit form (UCRM-1944) ([373fc3d](https://10.189.120.23/crm/ucrm-fe/commit/373fc3d42ab62bfae1bcdd625934ba28fbddb64e))


### Bug Fixes

* [Automation Selenium] add id settings page ([80eb111](https://10.189.120.23/crm/ucrm-fe/commit/80eb111f3bbd9431f6f817dc96af5181c0766a90))
* [Automation Selenium] add id settings page ([7fa7e84](https://10.189.120.23/crm/ucrm-fe/commit/7fa7e84b43af55cea04731bb42ebde9f62624687))
* [Automation Selenium] add id settings page ([854f8bb](https://10.189.120.23/crm/ucrm-fe/commit/854f8bb595c2ed09def1034b1a938634e3fe6b85))
* [Dynamic button] add allow clear select type ([7e4e4b0](https://10.189.120.23/crm/ucrm-fe/commit/7e4e4b0fbe8e0426cd136cfb404b57693ef1be40))
* [Dynamic Button] add format date when update fields (BUCRM-1846) ([95fc6ce](https://10.189.120.23/crm/ucrm-fe/commit/95fc6ce34358cf5dd514ee9be16b70cc7ddfe8a6))
* [Dynamic Button] add required fields and validate date (UCRM-1739) ([c9d2e03](https://10.189.120.23/crm/ucrm-fe/commit/c9d2e03ce201780c5574649408f3fee4a43baae9))
* [Dynamic Button] change record data when update in listview (BUCRM-1834) ([d17c404](https://10.189.120.23/crm/ucrm-fe/commit/d17c404e6b800089901c47e525ec01c546f4502c))
* [Dynamic button] display data in Details, multi details (BUCRM-1834) ([6c1fbbb](https://10.189.120.23/crm/ucrm-fe/commit/6c1fbbb783d9d702464eb70ccfa5a0da9a7858aa))
* [Dynamic button] remove create date and modify time in Update fields (BUCRM-1850) ([cc2bfcd](https://10.189.120.23/crm/ucrm-fe/commit/cc2bfcd6b4007645fd707aeab23a0a10e5ac2272))
* [DynamicButton] Add select, date, datetime in modal udpate fields (UCRM-1739) ([e300d7e](https://10.189.120.23/crm/ucrm-fe/commit/e300d7ecb0815245e56c89e3e3216a74cf72e478))
* [Email Outgoing] remove password empty (BUCRM-1553) ([cfd2c96](https://10.189.120.23/crm/ucrm-fe/commit/cfd2c96051346f31ab65e41d33feb285965fa269))
* [Email] remove style css when parse in Editor (BUCRM-1744) ([f58106a](https://10.189.120.23/crm/ucrm-fe/commit/f58106a335cd6b16edc690cdd6c1e922e7e9feac))
* [Modal report] add id report type ([8663a57](https://10.189.120.23/crm/ucrm-fe/commit/8663a57bd0ec7a88a2025c349a1cb3affcc78b21))
* [SLA] hide pending condition when deadline is active ([a99c33d](https://10.189.120.23/crm/ucrm-fe/commit/a99c33d44a0d4d43b648258b3871f1e445a1b95e))
* [URL Popup] add decodeURI (UCRM-1906) ([f5ce969](https://10.189.120.23/crm/ucrm-fe/commit/f5ce969be894dfd30ef843a552c697756d623f70))
* add Icomoon (UCRM-1968) ([da38f19](https://10.189.120.23/crm/ucrm-fe/commit/da38f19f0caf6b548ea2293971a4695ffffff4a0))
* add id for quanbmn ([955155a](https://10.189.120.23/crm/ucrm-fe/commit/955155a7298d620724767b45ab4d694e66ddae66))
* add id for quanbnm ([417229a](https://10.189.120.23/crm/ucrm-fe/commit/417229ac6ce537451202dce0bf940a63aa2aa3f1))
* add sort comments in QM (UCRM-1763) ([44c0ed2](https://10.189.120.23/crm/ucrm-fe/commit/44c0ed2f7816a25a97597ca7df9799bf494409ff))
* allow pagination in internet form - process setting (UCRM-496) ([ff2d12e](https://10.189.120.23/crm/ucrm-fe/commit/ff2d12ed63b40594620fff8d37540f34b1a043f5))
* allow pagination in internet form (UCRM-496) ([c73b5e2](https://10.189.120.23/crm/ucrm-fe/commit/c73b5e2955f073b84aec51c318a4df03ed70c272))
* allow pagination in internet form (UCRM-496) ([a3e432e](https://10.189.120.23/crm/ucrm-fe/commit/a3e432ea8091393c954d6d7681aef1834074193a))
* allow pagination in internet form (UCRM-496) ([521a2f7](https://10.189.120.23/crm/ucrm-fe/commit/521a2f724a45887ab08bda219f272263bd6b1115))
* allow pagination in internet form (UCRM-496) ([7bc98d0](https://10.189.120.23/crm/ucrm-fe/commit/7bc98d06c08a5a22c61ac9396fb6be6bee84b341))
* allow pagination in internet form (UCRM-496) ([8a7712a](https://10.189.120.23/crm/ucrm-fe/commit/8a7712a285f93cffbd082da05f164e054cfc6af7))
* allow pagination in internet form (UCRM-496) ([54d9ce7](https://10.189.120.23/crm/ucrm-fe/commit/54d9ce7744469fd604a46703e0bdbf6d3b6c8e9a))
* allow pagination in internet form (UCRM-496) ([6faea57](https://10.189.120.23/crm/ucrm-fe/commit/6faea57f3e5439404c3d21e63ce1836236879837))
* allow pagination in internet form (UCRM-496) ([997de5f](https://10.189.120.23/crm/ucrm-fe/commit/997de5f216c1024bb103920b787a415ddbb5320b))
* allow set default value internet form - inprogress (UCRM-1938) ([b19847a](https://10.189.120.23/crm/ucrm-fe/commit/b19847a9d2d554981f4be35b9384df5eafa65131))
* **bucrm-1840 500 when edit record:** bucrm-1840 ([fda793e](https://10.189.120.23/crm/ucrm-fe/commit/fda793e22fd15f1acdf2eae86294169ce4a867ea))
* bug update undefinded pass (UCRM 1851) ([9304c66](https://10.189.120.23/crm/ucrm-fe/commit/9304c665067c0327fb4912ba406d954646792b03))
* **cai thien temaplte record ucrm-1733:** ucrm-1733 ([2a56469](https://10.189.120.23/crm/ucrm-fe/commit/2a56469bc7d21ab5b1d61910d4bb4f8deb45ed29))
* **cai thien template edit ucrm1733:** ucrm1733 ([5d9b1c7](https://10.189.120.23/crm/ucrm-fe/commit/5d9b1c74278088e3ee07a2d96f992b968d020c8a))
* **date to parts allow get weekday:** uCRM1900 ([64cf425](https://10.189.120.23/crm/ucrm-fe/commit/64cf4250dbb0c00f17f33d7bdfb7f11dceceefae))
* disable unit datetoParts type (UCRM 1900) ([807818d](https://10.189.120.23/crm/ucrm-fe/commit/807818de94992e37d8e8ec758718cc38aa9d84f9))
* dynamic pagination in Internet form (UCRM-1863) ([6056930](https://10.189.120.23/crm/ucrm-fe/commit/605693008c5e4b22565b4290e70b7aba571b3b70))
* encode password type owa ([2522037](https://10.189.120.23/crm/ucrm-fe/commit/25220377226f72b81ec5f075848dcc61a61fa868))
* fix 500 when double click ([e8e0d8b](https://10.189.120.23/crm/ucrm-fe/commit/e8e0d8bddf531e9c574b03c5ac9152308e19db2d))
* fix 500 when mass edit field editor (UCRM-1897) ([2a2a0dc](https://10.189.120.23/crm/ucrm-fe/commit/2a2a0dcb967654124eae9f5aef828d4a15448ee8))
* fix bug pagination (UCRM-496) ([9d748ae](https://10.189.120.23/crm/ucrm-fe/commit/9d748aeb55bf7f9c86075e9c02e64546769c2065))
* fix bug pagination in internet form (UCRM-496) ([b233fb7](https://10.189.120.23/crm/ucrm-fe/commit/b233fb7593be915fa8326a1bde9b2d90b1d2b1e5))
* fix custom report (UCRM-1068) ([476326c](https://10.189.120.23/crm/ucrm-fe/commit/476326c9726b7be177f4ddc6f06d8d47d8c8f773))
* fix defalut value in form (UCRM-1938) ([d40382d](https://10.189.120.23/crm/ucrm-fe/commit/d40382db8698cec4a104ccaf6163c14e81df55c9))
* fix delete tenant (UCRM-1962) ([f8d2819](https://10.189.120.23/crm/ucrm-fe/commit/f8d281972e9ec0be2aca5d13dc19d714e7f50af0))
* fix field textarea edtior (UCRM-1870) ([338fa4f](https://10.189.120.23/crm/ucrm-fe/commit/338fa4fceecb44f0e8de202ac55bd6656d483521))
* fix not clear old data in modal share (BUCRM-1857) ([1965d88](https://10.189.120.23/crm/ucrm-fe/commit/1965d8863ad0d4e7bc41198a753e892176a59c63))
* fix paginaiton in internet form (UCRM-496) ([18ba621](https://10.189.120.23/crm/ucrm-fe/commit/18ba621481b2c40936cba07ee23b7003240de0c5))
* fix pagination in internet form (BUCRM-1854) ([2f654c1](https://10.189.120.23/crm/ucrm-fe/commit/2f654c14906cb7a3bc149f1128d5c6ba4b339efe))
* fix pagination in internet form (BUCRM-1854) ([023810a](https://10.189.120.23/crm/ucrm-fe/commit/023810a3462df4c1e0105c4d192d945d10857952))
* fix pagination in internet form (BUCRM-1857) ([3ac6ff9](https://10.189.120.23/crm/ucrm-fe/commit/3ac6ff980d5e40b69d28ddc72df8655ed0fd70bf))
* fix save ip customer submit form (UCRM-1944) ([282b584](https://10.189.120.23/crm/ucrm-fe/commit/282b584fadda05b55573ae988acd20a28f1098e8))
* layout Comment QM View ([a19aa6f](https://10.189.120.23/crm/ucrm-fe/commit/a19aa6f0e2cedae4bf3c07ec18124f9e2fcf6db1))
* multiDetail crash page ([749faaa](https://10.189.120.23/crm/ucrm-fe/commit/749faaa5b003d1bf5b242c9fa9d5aac8b37de84c))
* pagination form in QM (UCRM-496) ([9fa306b](https://10.189.120.23/crm/ucrm-fe/commit/9fa306bf6ed061bdbfd14789ee530305fffa0b35))
* pagination in internet form (UCRM-496) ([ee567ae](https://10.189.120.23/crm/ucrm-fe/commit/ee567ae8ca33cd379743646e07e779c2460fa8ad))
* pagination in internet form (UCRM-496) ([ba004fe](https://10.189.120.23/crm/ucrm-fe/commit/ba004fe06e3ba529e878afd8e71b439e2c382024))
* pagination in internet form (UCRM-496) ([0d1f04c](https://10.189.120.23/crm/ucrm-fe/commit/0d1f04cb0b00d9cba6af8cb7f038f6146290d636))


### Others

* **release:** 2.55.0 ([936d877](https://10.189.120.23/crm/ucrm-fe/commit/936d877aceea689cbda1e1408f9fbbcafe74a418))
* **release:** 2.56.0 ([0c3093d](https://10.189.120.23/crm/ucrm-fe/commit/0c3093d3f739cc85a7406a64e66f90f9702429c7))

## [2.56.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.53.0...v2.56.0) (2023-08-31)


### Features

* [Comments] add sort comments (UCRM-1763) ([78d8e68](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/78d8e684c95e545b7c5d84fcf893bc4d052f714c))
* [Dynamic button] add Triggered User in action Create/Update/Call API (UCRM-1745) ([13214da](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/13214dac15bb4ea3d49453423f1391c94407b9ae))
* [URLPopup] allow add multiple param in URL popup (UCRM-1906) ([96b3830](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96b3830f480b4689d8fb90e2fc438a8eaf5439cf))
* allow condition pagination in internet form (UCRM-1863) ([85fcbf4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/85fcbf43e84e8288253fd94a27db610ad3d9f39d))
* allow display default value in form description (UCRM-1869) ([6689240](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6689240a9a434b78b5262328225c1d107ed318d2))
* allow generate link QR code in internet form ([a7dcee4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a7dcee4958dd30ee0b4a2325b7c38c4d618ae921))
* allow pagination in internet form (UCRM-496) ([762bee5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/762bee5696ec7aaa595325331bc61c07a951dfe1))
* conditional pagination in internetform (UCRM-1863) ([3430fb3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3430fb39f0c2d24fe5241852304f464cf21d9aab))


### Bug Fixes

* [Automation Selenium] add id settings page ([80eb111](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/80eb111f3bbd9431f6f817dc96af5181c0766a90))
* [Automation Selenium] add id settings page ([7fa7e84](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7fa7e84b43af55cea04731bb42ebde9f62624687))
* [Automation Selenium] add id settings page ([854f8bb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/854f8bb595c2ed09def1034b1a938634e3fe6b85))
* [Dynamic button] add allow clear select type ([7e4e4b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e4e4b0fbe8e0426cd136cfb404b57693ef1be40))
* [Dynamic Button] add format date when update fields (BUCRM-1846) ([95fc6ce](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/95fc6ce34358cf5dd514ee9be16b70cc7ddfe8a6))
* [Dynamic Button] add required fields and validate date (UCRM-1739) ([c9d2e03](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c9d2e03ce201780c5574649408f3fee4a43baae9))
* [Dynamic Button] change record data when update in listview (BUCRM-1834) ([d17c404](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d17c404e6b800089901c47e525ec01c546f4502c))
* [Dynamic button] display data in Details, multi details (BUCRM-1834) ([6c1fbbb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6c1fbbb783d9d702464eb70ccfa5a0da9a7858aa))
* [Dynamic button] remove create date and modify time in Update fields (BUCRM-1850) ([cc2bfcd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc2bfcd6b4007645fd707aeab23a0a10e5ac2272))
* [DynamicButton] Add select, date, datetime in modal udpate fields (UCRM-1739) ([e300d7e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e300d7ecb0815245e56c89e3e3216a74cf72e478))
* [Email Outgoing] remove password empty (BUCRM-1553) ([cfd2c96](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cfd2c96051346f31ab65e41d33feb285965fa269))
* [Email] remove style css when parse in Editor (BUCRM-1744) ([f58106a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f58106a335cd6b16edc690cdd6c1e922e7e9feac))
* [KB] add API public, prevent right click details ([0d43575](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d435752d3debd2f0ef676daf7378bac7e792827))
* [KB] upload base64 image; handle link media ([64a46f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64a46f0d11529aa1d6d9446964905cc407249012))
* [SLA] hide pending condition when deadline is active ([a99c33d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a99c33d44a0d4d43b648258b3871f1e445a1b95e))
* [URL Popup] add decodeURI (UCRM-1906) ([f5ce969](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f5ce969be894dfd30ef843a552c697756d623f70))
* add load user in comment (UCRM-1839) ([2c1b170](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2c1b170d07093ccd28556bb3ef8d085fcdf166d1))
* add sort comments in QM (UCRM-1763) ([44c0ed2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/44c0ed2f7816a25a97597ca7df9799bf494409ff))
* allow pagination in internet form - process setting (UCRM-496) ([ff2d12e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ff2d12ed63b40594620fff8d37540f34b1a043f5))
* allow pagination in internet form (UCRM-496) ([c73b5e2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c73b5e2955f073b84aec51c318a4df03ed70c272))
* allow pagination in internet form (UCRM-496) ([a3e432e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a3e432ea8091393c954d6d7681aef1834074193a))
* allow pagination in internet form (UCRM-496) ([521a2f7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/521a2f724a45887ab08bda219f272263bd6b1115))
* allow pagination in internet form (UCRM-496) ([7bc98d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7bc98d06c08a5a22c61ac9396fb6be6bee84b341))
* allow pagination in internet form (UCRM-496) ([8a7712a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8a7712a285f93cffbd082da05f164e054cfc6af7))
* allow pagination in internet form (UCRM-496) ([54d9ce7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/54d9ce7744469fd604a46703e0bdbf6d3b6c8e9a))
* allow pagination in internet form (UCRM-496) ([6faea57](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6faea57f3e5439404c3d21e63ce1836236879837))
* allow pagination in internet form (UCRM-496) ([997de5f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/997de5f216c1024bb103920b787a415ddbb5320b))
* base64 only in KB ([52901ce](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/52901cef952f3ac6becafd69351f21f0da5936db))
* **bucrm-1800:** fix when reload auto disable action object ([d247e23](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d247e23f6ab29d964463f1a0e33e3b1399aee756))
* **bucrm-1840 500 when edit record:** bucrm-1840 ([fda793e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fda793e22fd15f1acdf2eae86294169ce4a867ea))
* bug update undefinded pass (UCRM 1851) ([9304c66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9304c665067c0327fb4912ba406d954646792b03))
* **cai thien temaplte record ucrm-1733:** ucrm-1733 ([2a56469](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2a56469bc7d21ab5b1d61910d4bb4f8deb45ed29))
* **cai thien template edit ucrm1733:** ucrm1733 ([5d9b1c7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d9b1c74278088e3ee07a2d96f992b968d020c8a))
* **date to parts allow get weekday:** uCRM1900 ([64cf425](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64cf4250dbb0c00f17f33d7bdfb7f11dceceefae))
* disable unit datetoParts type (UCRM 1900) ([807818d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/807818de94992e37d8e8ec758718cc38aa9d84f9))
* display increase/decrease field number in workflow (UCRM-389) ([c5db277](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c5db27780477f92f8e80e5244b31d143a8cb1c26))
* dynamic pagination in Internet form (UCRM-1863) ([6056930](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/605693008c5e4b22565b4290e70b7aba571b3b70))
* encode password type owa ([2522037](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25220377226f72b81ec5f075848dcc61a61fa868))
* fix 500 when double click ([e8e0d8b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8e0d8bddf531e9c574b03c5ac9152308e19db2d))
* fix bimat component bucrm-1815 ([dea3a25](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dea3a255c1ffce2ed4cf98efa63fb58bcc8feddc))
* fix bug pagination (UCRM-496) ([9d748ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9d748aeb55bf7f9c86075e9c02e64546769c2065))
* fix bug pagination in internet form (UCRM-496) ([b233fb7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b233fb7593be915fa8326a1bde9b2d90b1d2b1e5))
* fix custom report (UCRM-1068) ([476326c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/476326c9726b7be177f4ddc6f06d8d47d8c8f773))
* fix internet form (UCRM-1697) ([6723cc5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6723cc5d23111fba1d90d4af8bfaffc5155a49b7))
* fix not clear old data in modal share (BUCRM-1857) ([1965d88](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1965d8863ad0d4e7bc41198a753e892176a59c63))
* fix paginaiton in internet form (UCRM-496) ([18ba621](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/18ba621481b2c40936cba07ee23b7003240de0c5))
* fix pagination in internet form (BUCRM-1854) ([2f654c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f654c14906cb7a3bc149f1128d5c6ba4b339efe))
* fix pagination in internet form (BUCRM-1854) ([023810a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/023810a3462df4c1e0105c4d192d945d10857952))
* fix pagination in internet form (BUCRM-1857) ([3ac6ff9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ac6ff980d5e40b69d28ddc72df8655ed0fd70bf))
* image in comment ([cc984f6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc984f6e64ff2ee001a476283e216c9cc12ca7ab))
* layout Comment QM View ([a19aa6f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a19aa6f0e2cedae4bf3c07ec18124f9e2fcf6db1))
* link media object related ([425526b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/425526b022c3ee521616b71e85a4884fb49af2c4))
* multiDetail crash page ([749faaa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/749faaa5b003d1bf5b242c9fa9d5aac8b37de84c))
* pagination form in QM (UCRM-496) ([9fa306b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9fa306bf6ed061bdbfd14789ee530305fffa0b35))
* pagination in internet form (UCRM-496) ([ee567ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ee567ae8ca33cd379743646e07e779c2460fa8ad))
* pagination in internet form (UCRM-496) ([ba004fe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ba004fe06e3ba529e878afd8e71b439e2c382024))
* pagination in internet form (UCRM-496) ([0d1f04c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d1f04cb0b00d9cba6af8cb7f038f6146290d636))
* remove redundant APIs get user (UCRM-1839) ([9852914](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/98529145e0c72489043fbe7bf3b2b9e4b6591b15))
* search field formula ([66f5a55](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/66f5a5561d5a4191045852b8883a33a05c55acf9))
* show form type ThirdParty (QM) ([e214ae7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e214ae772c02fc6363f03730c90d99ba6320c261))


### Others

* **release:** 2.54.0 ([d19ccf6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d19ccf62427296125c3ca0199a22c4ff9e9b08e1))
* **release:** 2.55.0 ([936d877](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/936d877aceea689cbda1e1408f9fbbcafe74a418))

## [2.48.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.47.0...v2.48.0) (2023-06-27)

## [2.55.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.53.0...v2.55.0) (2023-08-24)


### Features

* [Comments] add sort comments (UCRM-1763) ([78d8e68](https://10.189.120.23/crm/ucrm-fe/commit/78d8e684c95e545b7c5d84fcf893bc4d052f714c))
* [Dynamic button] add Triggered User in action Create/Update/Call API (UCRM-1745) ([13214da](https://10.189.120.23/crm/ucrm-fe/commit/13214dac15bb4ea3d49453423f1391c94407b9ae))
* [URLPopup] allow add multiple param in URL popup (UCRM-1906) ([96b3830](https://10.189.120.23/crm/ucrm-fe/commit/96b3830f480b4689d8fb90e2fc438a8eaf5439cf))
* allow display default value in form description (UCRM-1869) ([6689240](https://10.189.120.23/crm/ucrm-fe/commit/6689240a9a434b78b5262328225c1d107ed318d2))
* allow generate link QR code in internet form ([a7dcee4](https://10.189.120.23/crm/ucrm-fe/commit/a7dcee4958dd30ee0b4a2325b7c38c4d618ae921))


### Bug Fixes

* [Automation Selenium] add id settings page ([80eb111](https://10.189.120.23/crm/ucrm-fe/commit/80eb111f3bbd9431f6f817dc96af5181c0766a90))
* [Automation Selenium] add id settings page ([7fa7e84](https://10.189.120.23/crm/ucrm-fe/commit/7fa7e84b43af55cea04731bb42ebde9f62624687))
* [Automation Selenium] add id settings page ([854f8bb](https://10.189.120.23/crm/ucrm-fe/commit/854f8bb595c2ed09def1034b1a938634e3fe6b85))
* [Dynamic button] add allow clear select type ([7e4e4b0](https://10.189.120.23/crm/ucrm-fe/commit/7e4e4b0fbe8e0426cd136cfb404b57693ef1be40))
* [Dynamic Button] add format date when update fields (BUCRM-1846) ([95fc6ce](https://10.189.120.23/crm/ucrm-fe/commit/95fc6ce34358cf5dd514ee9be16b70cc7ddfe8a6))
* [Dynamic Button] add required fields and validate date (UCRM-1739) ([c9d2e03](https://10.189.120.23/crm/ucrm-fe/commit/c9d2e03ce201780c5574649408f3fee4a43baae9))
* [Dynamic Button] change record data when update in listview (BUCRM-1834) ([d17c404](https://10.189.120.23/crm/ucrm-fe/commit/d17c404e6b800089901c47e525ec01c546f4502c))
* [Dynamic button] display data in Details, multi details (BUCRM-1834) ([6c1fbbb](https://10.189.120.23/crm/ucrm-fe/commit/6c1fbbb783d9d702464eb70ccfa5a0da9a7858aa))
* [DynamicButton] Add select, date, datetime in modal udpate fields (UCRM-1739) ([e300d7e](https://10.189.120.23/crm/ucrm-fe/commit/e300d7ecb0815245e56c89e3e3216a74cf72e478))
* [Email] remove style css when parse in Editor (BUCRM-1744) ([f58106a](https://10.189.120.23/crm/ucrm-fe/commit/f58106a335cd6b16edc690cdd6c1e922e7e9feac))
* [KB] add API public, prevent right click details ([0d43575](https://10.189.120.23/crm/ucrm-fe/commit/0d435752d3debd2f0ef676daf7378bac7e792827))
* [KB] upload base64 image; handle link media ([64a46f0](https://10.189.120.23/crm/ucrm-fe/commit/64a46f0d11529aa1d6d9446964905cc407249012))
* [SLA] hide pending condition when deadline is active ([a99c33d](https://10.189.120.23/crm/ucrm-fe/commit/a99c33d44a0d4d43b648258b3871f1e445a1b95e))
* [URL Popup] add decodeURI (UCRM-1906) ([f5ce969](https://10.189.120.23/crm/ucrm-fe/commit/f5ce969be894dfd30ef843a552c697756d623f70))
* add load user in comment (UCRM-1839) ([2c1b170](https://10.189.120.23/crm/ucrm-fe/commit/2c1b170d07093ccd28556bb3ef8d085fcdf166d1))
* add sort comments in QM (UCRM-1763) ([44c0ed2](https://10.189.120.23/crm/ucrm-fe/commit/44c0ed2f7816a25a97597ca7df9799bf494409ff))
* base64 only in KB ([52901ce](https://10.189.120.23/crm/ucrm-fe/commit/52901cef952f3ac6becafd69351f21f0da5936db))
* **bucrm-1840 500 when edit record:** bucrm-1840 ([fda793e](https://10.189.120.23/crm/ucrm-fe/commit/fda793e22fd15f1acdf2eae86294169ce4a867ea))
* bug update undefinded pass (UCRM 1851) ([9304c66](https://10.189.120.23/crm/ucrm-fe/commit/9304c665067c0327fb4912ba406d954646792b03))
* **cai thien temaplte record ucrm-1733:** ucrm-1733 ([2a56469](https://10.189.120.23/crm/ucrm-fe/commit/2a56469bc7d21ab5b1d61910d4bb4f8deb45ed29))
* **cai thien template edit ucrm1733:** ucrm1733 ([5d9b1c7](https://10.189.120.23/crm/ucrm-fe/commit/5d9b1c74278088e3ee07a2d96f992b968d020c8a))
* **date to parts allow get weekday:** uCRM1900 ([64cf425](https://10.189.120.23/crm/ucrm-fe/commit/64cf4250dbb0c00f17f33d7bdfb7f11dceceefae))
* disable unit datetoParts type (UCRM 1900) ([807818d](https://10.189.120.23/crm/ucrm-fe/commit/807818de94992e37d8e8ec758718cc38aa9d84f9))
* display increase/decrease field number in workflow (UCRM-389) ([c5db277](https://10.189.120.23/crm/ucrm-fe/commit/c5db27780477f92f8e80e5244b31d143a8cb1c26))
* encode password type owa ([2522037](https://10.189.120.23/crm/ucrm-fe/commit/25220377226f72b81ec5f075848dcc61a61fa868))
* fix 500 when double click ([e8e0d8b](https://10.189.120.23/crm/ucrm-fe/commit/e8e0d8bddf531e9c574b03c5ac9152308e19db2d))
* fix bimat component bucrm-1815 ([dea3a25](https://10.189.120.23/crm/ucrm-fe/commit/dea3a255c1ffce2ed4cf98efa63fb58bcc8feddc))
* fix internet form (UCRM-1697) ([6723cc5](https://10.189.120.23/crm/ucrm-fe/commit/6723cc5d23111fba1d90d4af8bfaffc5155a49b7))
* image in comment ([cc984f6](https://10.189.120.23/crm/ucrm-fe/commit/cc984f6e64ff2ee001a476283e216c9cc12ca7ab))
* layout Comment QM View ([a19aa6f](https://10.189.120.23/crm/ucrm-fe/commit/a19aa6f0e2cedae4bf3c07ec18124f9e2fcf6db1))
* link media object related ([425526b](https://10.189.120.23/crm/ucrm-fe/commit/425526b022c3ee521616b71e85a4884fb49af2c4))
* multiDetail crash page ([749faaa](https://10.189.120.23/crm/ucrm-fe/commit/749faaa5b003d1bf5b242c9fa9d5aac8b37de84c))
* remove redundant APIs get user (UCRM-1839) ([9852914](https://10.189.120.23/crm/ucrm-fe/commit/98529145e0c72489043fbe7bf3b2b9e4b6591b15))
* search field formula ([66f5a55](https://10.189.120.23/crm/ucrm-fe/commit/66f5a5561d5a4191045852b8883a33a05c55acf9))
* show form type ThirdParty (QM) ([e214ae7](https://10.189.120.23/crm/ucrm-fe/commit/e214ae772c02fc6363f03730c90d99ba6320c261))


### Others

* **release:** 2.54.0 ([d19ccf6](https://10.189.120.23/crm/ucrm-fe/commit/d19ccf62427296125c3ca0199a22c4ff9e9b08e1))

## [2.54.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.50.0...v2.54.0) (2023-08-17)


### Features

* [Email] allow config on off overwrite subject (UCRM-1727) ([e548fce](https://10.189.120.23/crm/ucrm-fe/commit/e548fceb0a53c1297d86c78ecf9925dea473541f))
* add knowledge in left-right component (UCRM-1631) ([ee368c6](https://10.189.120.23/crm/ucrm-fe/commit/ee368c67d8ad35b3673ea94717293d61328cf709))
* allow add knowledge base in right component (UCRM-1631) ([dbbed0a](https://10.189.120.23/crm/ucrm-fe/commit/dbbed0a7b756f5f8b66763e13e4bf8bb35484d61))
* allow display default value in form description (UCRM-1869) ([6689240](https://10.189.120.23/crm/ucrm-fe/commit/6689240a9a434b78b5262328225c1d107ed318d2))
* allow generate link QR code in internet form ([a7dcee4](https://10.189.120.23/crm/ucrm-fe/commit/a7dcee4958dd30ee0b4a2325b7c38c4d618ae921))
* allow to increase/decrease field number when Update Record (UCRM-389) ([0c0c74f](https://10.189.120.23/crm/ucrm-fe/commit/0c0c74f8213bf2dd952b358b2b4ac9a28c67d322))
* allow to increase/decrease field number when Update Source Record (UCRM-390) ([2f00da2](https://10.189.120.23/crm/ucrm-fe/commit/2f00da27e29ba7e982bf2b11c42a256e293f4b9d))
* config allow clone tenant (UCRM-1775) ([084178e](https://10.189.120.23/crm/ucrm-fe/commit/084178e8a2a908f829577f5aea3427473de7dabc))
* custom report (UCRM-1068) ([ba1b29d](https://10.189.120.23/crm/ucrm-fe/commit/ba1b29da15a06a615f2e0cab8290b84133979e3b))
* description in internet form (UCRM-1697) ([306d103](https://10.189.120.23/crm/ucrm-fe/commit/306d103c844f8ec21341bc19782962dca0179efc))
* search knowledge base with ID (UCMR-1643) ([4f58fc0](https://10.189.120.23/crm/ucrm-fe/commit/4f58fc00082bbbd410797782902a4327a7a7ca6d))


### Bug Fixes

* [Email Outgoing] add default alias config (BUCRM-1702) ([2daf5f3](https://10.189.120.23/crm/ucrm-fe/commit/2daf5f3d2cb8cf2b6a811213532ca4968e838e87))
* [IC Integration] reset value after change object (BUCRM-1739) ([fbf6f26](https://10.189.120.23/crm/ucrm-fe/commit/fbf6f269f7ad9e701e6e2a27fc5cf2ddc45754b2))
* [KB] add API public, prevent right click details ([0d43575](https://10.189.120.23/crm/ucrm-fe/commit/0d435752d3debd2f0ef676daf7378bac7e792827))
* [KB] upload base64 image; handle link media ([64a46f0](https://10.189.120.23/crm/ucrm-fe/commit/64a46f0d11529aa1d6d9446964905cc407249012))
* [Modal interaction] overflow UI body email (BUCRM-1734) ([534bf64](https://10.189.120.23/crm/ucrm-fe/commit/534bf6427a37a9c0894a67a8f358e1551caae3bb))
* [Modal record] error when upload file in call and table (BUCRM-1788) ([9c95dd4](https://10.189.120.23/crm/ucrm-fe/commit/9c95dd45abae869d5b5346760af40c45ed680d34))
* [Modal Record] remove empty tag editor field when update record (BUCRM-1755) ([fd94571](https://10.189.120.23/crm/ucrm-fe/commit/fd94571b79d848154c9212a2d1d4df87ec9ff729))
* [Report details] change report folder (BUCRM-1658) ([08915ae](https://10.189.120.23/crm/ucrm-fe/commit/08915aea1f9d5147d2ef21e15a02a5687256ebdf))
* [Report] allow to change report folder (BUCRM-1658) ([5830cf9](https://10.189.120.23/crm/ucrm-fe/commit/5830cf99d848b175517f6fdc087945e7c5c54b56))
* [Report] set color chart (UCRM-1715) ([f24c504](https://10.189.120.23/crm/ucrm-fe/commit/f24c504c5edc90a1598718738bbb1e55c752391d))
* add authen file (BUCRM-1777) ([25c369d](https://10.189.120.23/crm/ucrm-fe/commit/25c369d56ae024896a1273198a840bee02c6b67d))
* add button scroll quickFilter and Style details info IC (BUCRM 1767) ([cedfb3a](https://10.189.120.23/crm/ucrm-fe/commit/cedfb3ace4cc00ad2acb960f0b02bd13fd5f9d1b))
* add check condition when call API load form create (BUCRM-1771) ([4145a64](https://10.189.120.23/crm/ucrm-fe/commit/4145a647cc2a43e967d42c5c8c548c7fe119f4a5))
* add load user in comment (UCRM-1839) ([2c1b170](https://10.189.120.23/crm/ucrm-fe/commit/2c1b170d07093ccd28556bb3ef8d085fcdf166d1))
* add new test editor ([3031062](https://10.189.120.23/crm/ucrm-fe/commit/30310626487f9c7432063eb2f69d253fbf02e511))
* add type message chat IC QM (UCRM 1594) ([db09712](https://10.189.120.23/crm/ucrm-fe/commit/db09712490428f4434496a12530460f9f1a32649))
* advance config (UCRM-1803) ([97dde50](https://10.189.120.23/crm/ucrm-fe/commit/97dde50a50bf560bd9a47b99c9438d9b722b0bc3))
* advance config (UCRM-1803) ([4fe0a37](https://10.189.120.23/crm/ucrm-fe/commit/4fe0a3794332eadf2c00462770d6d72e1039e1c1))
* advanced config (UCRM-1803) ([5884c32](https://10.189.120.23/crm/ucrm-fe/commit/5884c32ab2de0730f428ded98c9ae2bb54ade91c))
* allow export pdf in custom report (UCRM-1068) ([1a2b6b8](https://10.189.120.23/crm/ucrm-fe/commit/1a2b6b8ddd2409e5bb2482a133d80a178b0bd40c))
* authen data type file ([f50e0e3](https://10.189.120.23/crm/ucrm-fe/commit/f50e0e32f3608ef38510edd3bfbab535ed7fa8e4))
* base64 only in KB ([52901ce](https://10.189.120.23/crm/ucrm-fe/commit/52901cef952f3ac6becafd69351f21f0da5936db))
* **bucrm-1800:** fix when reload auto disable action object ([d247e23](https://10.189.120.23/crm/ucrm-fe/commit/d247e23f6ab29d964463f1a0e33e3b1399aee756))
* change file scss quickfilter ([171c1b8](https://10.189.120.23/crm/ucrm-fe/commit/171c1b83206cff0c34fe9360aee57bb8f580336f))
* chat ic QM (UCRM 1594) ([d331c8d](https://10.189.120.23/crm/ucrm-fe/commit/d331c8d83df636d7f5433c1de78d8e31d07ea05e))
* chat ic QM (UCRM 1594) ([196ac4e](https://10.189.120.23/crm/ucrm-fe/commit/196ac4edbc669e39e3ab5766d203eaf3ff2e187b))
* checkbox use fixed deadline ([14b8783](https://10.189.120.23/crm/ucrm-fe/commit/14b8783c23b6aa8f1ed9a13039c627252c5f3b92))
* component select users ([351a768](https://10.189.120.23/crm/ucrm-fe/commit/351a768fcc47666c1ca5aac2bcbef33d3f5de71f))
* conflict ([b5cc99f](https://10.189.120.23/crm/ucrm-fe/commit/b5cc99f4af8b226de2dbf736d1d9c3f0bda23da7))
* custom report (UCRM-1068) ([f8cd1e8](https://10.189.120.23/crm/ucrm-fe/commit/f8cd1e8e8a2a569dae021931ed6d40496423e6a3))
* custom report (UCRM-1068) ([677c749](https://10.189.120.23/crm/ucrm-fe/commit/677c749d05dc4ed15752e9d1734d62059c2042e6))
* custom report (UCRM-1068) ([da05723](https://10.189.120.23/crm/ucrm-fe/commit/da05723e1d45c5d5e3c0bb263a396a013db92b47))
* custom report (UCRM-1068) ([e8eb5e6](https://10.189.120.23/crm/ucrm-fe/commit/e8eb5e61d0a09818416bf7b4910e2a7c5bab97d7))
* custom report (UCRM-1068) ([0d54138](https://10.189.120.23/crm/ucrm-fe/commit/0d54138eff1bebc03728a0b62f4c4c58e7cf94b9))
* custom report (UCRM-1068) ([79dce86](https://10.189.120.23/crm/ucrm-fe/commit/79dce8606c18ea21aa17a8696ba4680076ad6360))
* custom report (UCRM-1068) ([a12bcc2](https://10.189.120.23/crm/ucrm-fe/commit/a12bcc20c725898b0317014d94e82f84cc602d2c))
* custom report (UCRM-1068) ([c07b48c](https://10.189.120.23/crm/ucrm-fe/commit/c07b48c5af1f76754b7da64c12ce5a7567202bde))
* custom report (UCRM-1068) ([7ce561f](https://10.189.120.23/crm/ucrm-fe/commit/7ce561f220ff870399f55aa844ddcd4f27b70fab))
* custom report (UCRM-1068) ([7575983](https://10.189.120.23/crm/ucrm-fe/commit/75759833fe85914606c0b322f0570f25f8d6663d))
* custom report (UCRM-1068) ([3fefc8b](https://10.189.120.23/crm/ucrm-fe/commit/3fefc8bf04971f9c928e1394c75becc0db83d2a2))
* custom report (UCRM-1608) ([6d6b51b](https://10.189.120.23/crm/ucrm-fe/commit/6d6b51b0feb958e73a0e7129fa393bb92a553d24))
* custom report (UCRM-1608) ([7ef5c7b](https://10.189.120.23/crm/ucrm-fe/commit/7ef5c7bb4578537065f9f220aeed5d8fc04ae6dc))
* custom report (UCRM-1608) ([4480ffb](https://10.189.120.23/crm/ucrm-fe/commit/4480ffbd153279728c229893ae74685fa04720df))
* custom report [UCRM-1068] ([28b4619](https://10.189.120.23/crm/ucrm-fe/commit/28b4619cea72d0371a60990cd7184633dc570274))
* display increase/decrease field number in workflow (UCRM-389) ([c5db277](https://10.189.120.23/crm/ucrm-fe/commit/c5db27780477f92f8e80e5244b31d143a8cb1c26))
* fix bimat component bucrm-1815 ([dea3a25](https://10.189.120.23/crm/ucrm-fe/commit/dea3a255c1ffce2ed4cf98efa63fb58bcc8feddc))
* fix custom view QM (BUCRM 1765) ([9c2302a](https://10.189.120.23/crm/ucrm-fe/commit/9c2302ad40c9b364cee476590f1364512b472e71))
* fix delay text in modal record (BUCRM-1789) ([d3054b0](https://10.189.120.23/crm/ucrm-fe/commit/d3054b03cd6cabaf48464d64e99d83c84d2a5664))
* fix internet form (UCRM-1697) ([6723cc5](https://10.189.120.23/crm/ucrm-fe/commit/6723cc5d23111fba1d90d4af8bfaffc5155a49b7))
* fix knowledge base in consolidate view(UCRM-1631) ([5b49109](https://10.189.120.23/crm/ucrm-fe/commit/5b49109bc169b36f954068e1b2d856e3e79e984a))
* fix permission QM (BUCRM 1762) ([57e8542](https://10.189.120.23/crm/ucrm-fe/commit/57e85421af86253c763262c2943bbace97f20ac4))
* fix permission quickfilter QM (BUCRM 1760) ([d09fb36](https://10.189.120.23/crm/ucrm-fe/commit/d09fb36191aa60e12e2e3ff731cd0b33aaf5daf9))
* hidden pdf, template file, workflow create file, update, delete permission ([71dfb8e](https://10.189.120.23/crm/ucrm-fe/commit/71dfb8e75507f8d6af421ca7695a9919dc561475))
* image in comment ([cc984f6](https://10.189.120.23/crm/ucrm-fe/commit/cc984f6e64ff2ee001a476283e216c9cc12ca7ab))
* knowledge base (BUCRM-1750) ([08a3d0d](https://10.189.120.23/crm/ucrm-fe/commit/08a3d0d2c05b341061ea242b33dde62cc19b6b0b))
* knowledge base (UCRM-1727) ([9d74282](https://10.189.120.23/crm/ucrm-fe/commit/9d742820d7c5eb84127000c8763ef86d73d96a14))
* knowledge base (UCRM-1727) ([7ecfc77](https://10.189.120.23/crm/ucrm-fe/commit/7ecfc77f775cb717f5a07aa4434275f8ec683f65))
* knowledge base (UCRM-1745) ([d99785b](https://10.189.120.23/crm/ucrm-fe/commit/d99785b7b281de2ec6a76b1f1a77254481143775))
* layout chat ic QM (UCRM 1594) ([88f5c3c](https://10.189.120.23/crm/ucrm-fe/commit/88f5c3c5dc247a77739f88c1c256139cdbd51797))
* link media object related ([425526b](https://10.189.120.23/crm/ucrm-fe/commit/425526b022c3ee521616b71e85a4884fb49af2c4))
* permission QM ([ef6526f](https://10.189.120.23/crm/ucrm-fe/commit/ef6526f2a31c535b6f0463de5c4ddc5c47279a0b))
* QM view (UCRM 1582) ([adb88ca](https://10.189.120.23/crm/ucrm-fe/commit/adb88cacba219e26be26506fc0acca5e42c8018b))
* remove redundant APIs get user (UCRM-1839) ([9852914](https://10.189.120.23/crm/ucrm-fe/commit/98529145e0c72489043fbe7bf3b2b9e4b6591b15))
* search field formula ([66f5a55](https://10.189.120.23/crm/ucrm-fe/commit/66f5a5561d5a4191045852b8883a33a05c55acf9))
* show form type ThirdParty (QM) ([e214ae7](https://10.189.120.23/crm/ucrm-fe/commit/e214ae772c02fc6363f03730c90d99ba6320c261))
* **ucrm-1721:** cai thien condition ([4cf2995](https://10.189.120.23/crm/ucrm-fe/commit/4cf2995849e621091a74c79e1f379d439be5aef5))
* **ucrm-1752:** fix edit 500 ([a932031](https://10.189.120.23/crm/ucrm-fe/commit/a932031a882d70089b2622dfd7c1ff35e417691f))
* update img form QM ([a6bb1fb](https://10.189.120.23/crm/ucrm-fe/commit/a6bb1fbf738b8f8ea63d06f533678c54df5dceb1))
* update quickfilter ([5e7c30b](https://10.189.120.23/crm/ucrm-fe/commit/5e7c30b1bd056d61f79fd5d4584553b5591ccd48))
* update search in knowledge base (UCRM-1787) ([829fd04](https://10.189.120.23/crm/ucrm-fe/commit/829fd04848be68d73e9d9b63b96b4c3a71e54461))


### Others

* **release:** 2.51.0 ([d37213c](https://10.189.120.23/crm/ucrm-fe/commit/d37213c84dba094baad629575839c68c21640d18))
* **release:** 2.52.0 ([26b93f1](https://10.189.120.23/crm/ucrm-fe/commit/26b93f10676960f71cbceab357c3bb1c2623243d))
* **release:** 2.53.0 ([c85b966](https://10.189.120.23/crm/ucrm-fe/commit/c85b96655603e396b4262128147b3b95cf834fa7))

### [2.49.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.49.0...v2.49.1) (2023-07-13)

## [2.49.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.48.0...v2.49.0) (2023-07-07)

## [2.48.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.47.0...v2.48.0) (2023-07-04)

## [2.47.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.46.1...v2.47.0) (2023-06-23)

## [2.53.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.52.0...v2.53.0) (2023-08-10)


### Features

* add knowledge in left-right component (UCRM-1631) ([ee368c6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ee368c67d8ad35b3673ea94717293d61328cf709))
* allow add knowledge base in right component (UCRM-1631) ([dbbed0a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dbbed0a7b756f5f8b66763e13e4bf8bb35484d61))
* allow to increase/decrease field number when Update Record (UCRM-389) ([0c0c74f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0c0c74f8213bf2dd952b358b2b4ac9a28c67d322))
* allow to increase/decrease field number when Update Source Record (UCRM-390) ([2f00da2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f00da27e29ba7e982bf2b11c42a256e293f4b9d))


### Bug Fixes

* [IC Integration] reset value after change object (BUCRM-1739) ([fbf6f26](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fbf6f269f7ad9e701e6e2a27fc5cf2ddc45754b2))
* [Modal interaction] overflow UI body email (BUCRM-1734) ([534bf64](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/534bf6427a37a9c0894a67a8f358e1551caae3bb))
* [Modal record] error when upload file in call and table (BUCRM-1788) ([9c95dd4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9c95dd45abae869d5b5346760af40c45ed680d34))
* [Modal Record] remove empty tag editor field when update record (BUCRM-1755) ([fd94571](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fd94571b79d848154c9212a2d1d4df87ec9ff729))
* add authen file (BUCRM-1777) ([25c369d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25c369d56ae024896a1273198a840bee02c6b67d))
* add button scroll quickFilter and Style details info IC (BUCRM 1767) ([cedfb3a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cedfb3ace4cc00ad2acb960f0b02bd13fd5f9d1b))
* advance config (UCRM-1803) ([97dde50](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/97dde50a50bf560bd9a47b99c9438d9b722b0bc3))
* advance config (UCRM-1803) ([4fe0a37](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4fe0a3794332eadf2c00462770d6d72e1039e1c1))
* advanced config (UCRM-1803) ([5884c32](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5884c32ab2de0730f428ded98c9ae2bb54ade91c))
* authen data type file ([f50e0e3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f50e0e32f3608ef38510edd3bfbab535ed7fa8e4))
* checkbox use fixed deadline ([14b8783](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/14b8783c23b6aa8f1ed9a13039c627252c5f3b92))
* fix custom view QM (BUCRM 1765) ([9c2302a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9c2302ad40c9b364cee476590f1364512b472e71))
* fix delay text in modal record (BUCRM-1789) ([d3054b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3054b03cd6cabaf48464d64e99d83c84d2a5664))
* fix knowledge base in consolidate view(UCRM-1631) ([5b49109](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5b49109bc169b36f954068e1b2d856e3e79e984a))
* fix permission QM (BUCRM 1762) ([57e8542](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/57e85421af86253c763262c2943bbace97f20ac4))
* fix permission quickfilter QM (BUCRM 1760) ([d09fb36](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d09fb36191aa60e12e2e3ff731cd0b33aaf5daf9))
* permission QM ([ef6526f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ef6526f2a31c535b6f0463de5c4ddc5c47279a0b))
* update search in knowledge base (UCRM-1787) ([829fd04](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/829fd04848be68d73e9d9b63b96b4c3a71e54461))

## [2.52.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.51.0...v2.52.0) (2023-08-03)


### Features

* [Email] allow config on off overwrite subject (UCRM-1727) ([e548fce](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e548fceb0a53c1297d86c78ecf9925dea473541f))
* config allow clone tenant (UCRM-1775) ([084178e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/084178e8a2a908f829577f5aea3427473de7dabc))
* description in internet form (UCRM-1697) ([306d103](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/306d103c844f8ec21341bc19782962dca0179efc))
* search knowledge base with ID (UCMR-1643) ([4f58fc0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4f58fc00082bbbd410797782902a4327a7a7ca6d))


### Bug Fixes

* [Report] set color chart (UCRM-1715) ([f24c504](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f24c504c5edc90a1598718738bbb1e55c752391d))
* add check condition when call API load form create (BUCRM-1771) ([4145a64](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4145a647cc2a43e967d42c5c8c548c7fe119f4a5))
* add new test editor ([3031062](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30310626487f9c7432063eb2f69d253fbf02e511))
* add type message chat IC QM (UCRM 1594) ([db09712](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/db09712490428f4434496a12530460f9f1a32649))
* allow export pdf in custom report (UCRM-1068) ([1a2b6b8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1a2b6b8ddd2409e5bb2482a133d80a178b0bd40c))
* chat ic QM (UCRM 1594) ([d331c8d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d331c8d83df636d7f5433c1de78d8e31d07ea05e))
* chat ic QM (UCRM 1594) ([196ac4e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/196ac4edbc669e39e3ab5766d203eaf3ff2e187b))
* conflict ([b5cc99f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b5cc99f4af8b226de2dbf736d1d9c3f0bda23da7))
* custom report (UCRM-1068) ([f8cd1e8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8cd1e8e8a2a569dae021931ed6d40496423e6a3))
* layout chat ic QM (UCRM 1594) ([88f5c3c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/88f5c3c5dc247a77739f88c1c256139cdbd51797))
* **ucrm-1721:** cai thien condition ([4cf2995](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4cf2995849e621091a74c79e1f379d439be5aef5))

## [2.51.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.49.1...v2.51.0) (2023-07-27)


### Features

* custom report (UCRM-1068) ([ba1b29d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ba1b29da15a06a615f2e0cab8290b84133979e3b))


### Bug Fixes

* [Email Outgoing] add default alias config (BUCRM-1702) ([2daf5f3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2daf5f3d2cb8cf2b6a811213532ca4968e838e87))
* [Report details] change report folder (BUCRM-1658) ([08915ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/08915aea1f9d5147d2ef21e15a02a5687256ebdf))
* [Report] allow to change report folder (BUCRM-1658) ([5830cf9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5830cf99d848b175517f6fdc087945e7c5c54b56))
* add code upload log scoring ([61a9111](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/61a91113d01725c40c2f2c451277daa9ca5b3a52))
* add component chat QM View (UCRM 981) ([cadf1b6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cadf1b650669cd5c243927e6fe70e670e7b6a7ba))
* add default search type call Finesse ([be8a9e8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/be8a9e864e949ea9b4afda3ac41595ac5eb1ccdc))
* add img submit form qm ([c0c6842](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c0c6842e5c5d73fce57fc237c993b0e969cf004f))
* bg qm form ([bd17cf7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bd17cf7b6b823dcd7f20e124908b1fbfa3f5fbd9))
* BUCRM-1666 fix add user when select anther record ([4ea967d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4ea967de120c30dd8a00bc4a004747601160e6e9))
* change file scss quickfilter ([171c1b8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/171c1b83206cff0c34fe9360aee57bb8f580336f))
* comment qm ([3a64723](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3a647230f087a711ce1960866f795f3b4bfd1e4a))
* component select users ([351a768](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/351a768fcc47666c1ca5aac2bcbef33d3f5de71f))
* conflict code ([edd566e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/edd566e5afa578347e9e98561afaeae6b5bf2ad1))
* custom report (UCRM-1068) ([677c749](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/677c749d05dc4ed15752e9d1734d62059c2042e6))
* custom report (UCRM-1068) ([da05723](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/da05723e1d45c5d5e3c0bb263a396a013db92b47))
* custom report (UCRM-1068) ([e8eb5e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8eb5e61d0a09818416bf7b4910e2a7c5bab97d7))
* custom report (UCRM-1068) ([0d54138](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d54138eff1bebc03728a0b62f4c4c58e7cf94b9))
* custom report (UCRM-1068) ([79dce86](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/79dce8606c18ea21aa17a8696ba4680076ad6360))
* custom report (UCRM-1068) ([a12bcc2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a12bcc20c725898b0317014d94e82f84cc602d2c))
* custom report (UCRM-1068) ([c07b48c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c07b48c5af1f76754b7da64c12ce5a7567202bde))
* custom report (UCRM-1068) ([7ce561f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7ce561f220ff870399f55aa844ddcd4f27b70fab))
* custom report (UCRM-1068) ([7575983](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/75759833fe85914606c0b322f0570f25f8d6663d))
* custom report (UCRM-1068) ([3fefc8b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3fefc8bf04971f9c928e1394c75becc0db83d2a2))
* custom report (UCRM-1608) ([6d6b51b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6d6b51b0feb958e73a0e7129fa393bb92a553d24))
* custom report (UCRM-1608) ([7ef5c7b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7ef5c7bb4578537065f9f220aeed5d8fc04ae6dc))
* custom report (UCRM-1608) ([4480ffb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4480ffbd153279728c229893ae74685fa04720df))
* custom report [UCRM-1068] ([28b4619](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/28b4619cea72d0371a60990cd7184633dc570274))
* err bg form qm ([4f94bc3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4f94bc3932f2988f16d1119601045f41bffd8eda))
* err bg form qm ([fe7a788](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fe7a7886d4c0bd3e264f9669c6fec8986ad333d0))
* hidden 3 options ([b3eca2e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b3eca2e6706f34b78bd752b21f33e061ddadae91))
* hidden pdf, template file, workflow create file, update, delete permission ([71dfb8e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/71dfb8e75507f8d6af421ca7695a9919dc561475))
* hidden qm view and check custom admin interaction ([597d7e1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/597d7e14ca2966bd45f3a8c211a559295231a0b5))
* knowledge base (BUCRM-1750) ([08a3d0d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/08a3d0d2c05b341061ea242b33dde62cc19b6b0b))
* knowledge base (UCRM-1727) ([9d74282](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9d742820d7c5eb84127000c8763ef86d73d96a14))
* knowledge base (UCRM-1727) ([7ecfc77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7ecfc77f775cb717f5a07aa4434275f8ec683f65))
* knowledge base (UCRM-1745) ([d99785b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d99785b7b281de2ec6a76b1f1a77254481143775))
* QM view (UCRM 1582) ([adb88ca](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/adb88cacba219e26be26506fc0acca5e42c8018b))
* save log form qm (UCRM 1576) ([f3d626d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f3d626d8e13714fb7695bc2339a17a3bc2cc9b03))
* **ucrm-1752:** fix edit 500 ([a932031](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a932031a882d70089b2622dfd7c1ff35e417691f))
* update attach file log QM (UCRM 1576) ([153906f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/153906fb3d5bbc56a91c963b92a885a4a6f5a5dd))
* update img form QM ([a6bb1fb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a6bb1fbf738b8f8ea63d06f533678c54df5dceb1))
* update modal Assignment Rule ([e50d197](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e50d197baa34e66aaee01ff99cc499da063221e7))
* update modal Assignment Rule ([8aa1ab3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8aa1ab344f61ecf936d3c9f9ca7b7e5cede0c448))
* update quickfilter ([5e7c30b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5e7c30b1bd056d61f79fd5d4584553b5591ccd48))
* update save log form QM (UCRM 1576) ([0fb3aa1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0fb3aa1b88fd09be08708a40f4bdf51e3201aa43))
* update translate QM ([05cd034](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/05cd03439f9c48374e85eac0a76a7452e86b3cb2))
* update version 2.49.1 ([e0cdb3a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e0cdb3ab05bde1ff83c77959cd304971500ae61a))


### Others

* **release:** 2.50.0 ([f5aef2f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f5aef2fbf6355d202b6d128265a2f1176b269846))

### [2.49.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.49.0...v2.49.1) (2023-07-13)


### Bug Fixes

* [Numpad] show disconnect btn when dynamic ext (#BUCRM-1677); [Email] white space (#BUCRM-1665) ([28ba089](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/28ba0893bf654379f356110b118b24cd7a1bdb12)), closes [#BUCRM-1677](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/BUCRM-1677) [#BUCRM-1665](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/BUCRM-1665)
* add field interaction detail (UCRM 1307) ([b71b617](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b71b617f21e457fef64724606a13bccdb0375671))
* add permission (UCRM 1307) ([ac0ec9b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ac0ec9b61e73999dd578924979a0bee7fe8c5d48))
* add permission QM View (1307) ([7b1acb7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7b1acb77bb92f4607afc677734c4d04416bb7685))
* BUCRM-1698 Fix create template record ([cabd43a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cabd43a7b9dc89bde96ba4c1ffd600ae024a95e4))
* BUCRM-1700 fix bug create record template ([854c3b2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/854c3b26d3cbea25fc9372c2df50e20a4ac9f8b4))
* BUCRM-1700 fix type user ([cb8621b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cb8621b25889c3c19304924f97d06789fedff33b))
* change BG Score form QM (UCRM 1307) ([34576d8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/34576d8e34c3d5fa8f0e46188112d73ce288e415))
* change BG Score form QM (UCRM 1307) ([fde87a5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fde87a590145e234e177f65d35818153ff6c252a))
* change BG Score form QM (UCRM 1307) ([a56491f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a56491f8025a088eaa844a7c42eaa0394f8f4ec4))
* change payload submit form (UCRM 1307) ([2742459](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2742459b8d33a89d2299325c44bdb8969c233b69))
* change payload submit form (UCRM 1307) ([565da8f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/565da8faf3ea67caff5ac89d33c9a630a998d8ee))
* hidden QM feature ([7969c28](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7969c28bf94567d92edde4f6f6f109a2bd33c3fa))
* icon Qm View (1307) ([7edb81f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7edb81f19975372fb50de156c3547e4017bf2db6))
* ObjectCategory null ([13e210d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/13e210dc32ebe04814c3cdd1523d13f485a90b04))
* parse HTML in component detail, multi-detail, tags (BUCRM-1703) ([3496095](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/349609573132bf5cccac0080ff2da35dd8ab795b))
* parse HTML in consolidated view (BUCRM-1703) ([8801477](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/88014772d0c6165f1739213f32f5f54d9d5a03f3))
* qm view ([e855135](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8551356e1a54865f392fb7d1b312ff2d85a6110))
* qm view ([be80a56](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/be80a563b2d5693df2281bc6a2618c903affada6))
* remove text (1307) ([dc8f61a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dc8f61af553ad1dac396c14b5c66f3bfe930d2f0))
* style avatar select user (UCRM 1307) ([8481103](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/84811030cab9bea04f2196932bebbf5f06b10bdc))
* UCRM-408 improvement list view ([7447ebc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7447ebc5bed283da5930b396c42b6bd662a17403))
* UI QM (UCRM 1307) ([54582af](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/54582af4e7d113d18d8a56d756f807a40fba33fd))
* UI QM (UCRM 1307) ([25159bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25159bcb482444ca22604fb5f97f8568bc7a9268))
* update pyload (UCRM 1037) ([2f7e6f1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f7e6f19be62e346e5216e02dd8198aba8a5af51))
* update UI QM View (UCRM 1307) ([89ba3a0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/89ba3a0ef075861115aab8f0e5f6ad35251ac2ea))

## [2.49.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.47.0...v2.49.0) (2023-07-06)


### Features

* [Finesse] Allow users to share extension [#UCRM-1360] ([798ccb8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/798ccb84e70d1239a2dc1eb63be7e620c63c6044)), closes [#UCRM-1360](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1360)
* add code (UCRM 1307) ([c413079](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c4130795ed9dd4fba128077f4bb641213af526f5))
* add code (UCRM 1307) ([0e11fc5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0e11fc52abbaa5dd9382b4fac2f11417d8f07872))
* add custom view (UCRM 1307) ([c82e3ba](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c82e3ba005a1c7393bc1853f93e28dbd3518d408))
* add email (UCRM 1307) ([7a09668](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7a096687914527baa3ac2405023591df1a8aa0cb))
* add form qm view (UCRM 1307) ([aeb322d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aeb322dbd4aa417b4671da5effde0fcd253889b1))
* add form qm view (UCRM 1307) ([a0acfe6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a0acfe612c7f03e87230576a4e0e0f59322d4219))
* Add loading QM view (UCRM 1307) ([0dafe25](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0dafe252769472e179eade7f40c7db043ea10160))
* add record call (UCRM 1307) ([3a2f912](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3a2f91253ca3c017f03c9433e0f6904fcfccc37d))
* add UI QM (UCRM 1307 ([8cead82](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8cead828e539743e2e8a323ebd3fc16a309a95cb))
* change Icon IC (UCRM 1307) ([cc84964](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc8496415179c974e7c8e8e50b1bc572a26cdd31))
* change payload api form qm (UCRM 1307) ([0dc76ff](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0dc76ff4d0a80500c522755834856679f3a3ec6e))
* change UI QM settings (UCRM 1307) ([c82b2c5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c82b2c54b18e94a80dc2658e25a6f495619e4675))
* check rule QM settings (UCRM 1307) ([b8551fe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b8551fed41c35700a1d34ad3d328f2ffb79c0ff4))
* filter by owner (UCRM 1307) ([a5da029](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a5da0297d1766a480bf70c4ed05b0ff1465d1afe))
* load data interaction detail (UCRM 1307) ([d3b6d4b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3b6d4bbf2a7d11fee8ea2d0313e27160bb108c7))
* load data sidebar (UCRM 1307) ([8313d18](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8313d18ef652103f59f5b3cf5a27a3c7ce70a12b))
* load detail interaction (UCRM 1307) ([d3d74ec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3d74ecd16356000f9f8375eb047f3dfed5b3172))
* QM (UCRM 1307) ([a2a6d32](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a2a6d32d76dceb6514788323781e045fe5919155))
* qm setting (UCRM 1307) ([adb7a1a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/adb7a1a903703a238babc1a75cee7e6b79d3b777))
* qm setting (UCRM 1307) ([09d01d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/09d01d930956680c10a54b72862d7908f1019af6))
* qm setting (UCRM 1307) ([5e27b8d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5e27b8dfdc8b41d826618a3ce823873a9ab0a8c2))
* qm setting (UCRM 1307) ([2801df0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2801df0c1bd4ddd3441d61073462b69185341f81))
* QM setting (UCRM 1307) ([33bf1c7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/33bf1c7472f2cad20dd1b15715a6254945e4e538))
* required form_name QM (UCRM 1307) ([6af4166](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6af41661412614a33d9b9bef99c644c1db7b53c4))
* UCRM-1408 features monitoring resource ([f09331c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f09331c237b445182cec5ea7ea654e912fe08e2a))
* UCRM-1459 feature edit record can using template ([1a7a4f4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1a7a4f4e4d3ba6bb7dbaf75933763cfb2e93c485))
* UCRM-1493 add reCaptcha for submit internet form ([2f2539a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f2539a1728267dbf3eeaee95467615bb3b06f01))
* UCRM-408 improvement search in listview ([952a0bb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/952a0bb0cbf6c46dd6a8e0718a68c8e6edea9bd7))
* UCRM-969 Allow User to create Form for QM feature ([93e0c30](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/93e0c3003bc9ec6edfe9c4ed22008d6eb92a6ada))
* Ui QM (UCRM 1307) ([a64e0fe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a64e0fe89b8668443db1112bd870b4f8206efae3))


### Bug Fixes

* [Finesse] add modal noti extension is being used (#UCRM-1360) ([843ef0a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/843ef0a56e2cb7192a3ecf0fe5c6825b0bed9933)), closes [#UCRM-1360](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1360)
* [Interaction Settings] allow user edit interaction settings [#UCRM-898] ([2aea714](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2aea7148729f03eddbac2e1e23184b90c205b9ac)), closes [#UCRM-898](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-898)
* [Interaction] add QM fields settings (#UCRM-898) ([bffa94b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bffa94b207cf31bad29f39c3080d9d8a9e626003)), closes [#UCRM-898](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-898)
* add clone config email [Root]; Allow update Interaction setting ([71c7b77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/71c7b7738052b0abb7cf8fd7a9ed939c7a196052))
* add tenant id when clone ([64970aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64970aa0c2fabd1e81b9328b26976a291df48865))
* change payload update interaction (UCRM 898) ([6f3edc7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6f3edc7caa5c19e01da706f713bd935aa2405b75))
* fix conflict code develop (UCRM 1307) ([d08730b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d08730b8060fb60cac12d82ed4f9dd597ee62a19))
* hidden QM (UCRM 1307) ([5b5889e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5b5889e8935a82280cd98b359d62a01b89e940cb))
* hot fix version v2.46.0 ([051ada3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/051ada3b3d0579d4f27a40120119cac5815b7487))
* optimize code (UCRM 1307) ([2b62eda](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2b62eda38feaca71fef72f1aabd15d09c205d68a))
* optimize code (UCRM 1307) ([54d57eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/54d57ebcdd0d30512bcb79345dda7fde0e038f12))
* optimize code (UCRM 1307) ([980e4eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/980e4ebfb2899a05e21ee11cf4c1e109f8b45699))
* optimize code (UCRM 1307) ([6e0f637](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6e0f637c7a3de5d15cc1cb132c370cb0d73a3ba4))
* set value form qm ([3c3d642](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3c3d642798e6e92fe25b878e2d5c11a123cc4b95))
* set value form qm ([e0813f5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e0813f58b05eaa4a957083b0a04b8c9b745f1910))
* UCRM-1000 tronglv fix internet form ([6c4a476](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6c4a47663b9dfaa2d271cc165987dcf83ebbf1ae))
* UCRM-1408 fix UI for monitoring ([e003060](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e00306005a217f5b341a2bf057a0da60be63ec0b))
* UCRM-1408 fix UI monitoring ([f986f93](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f986f936011d6406ce10d57e6ff4c16ff604bdd4))
* UCRM-1459 fix bug for template when using in edit record ([1acf045](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1acf045cf13bf36042f40806aa47462b2113a430))
* UCRM-1459 fix bug template create record when edit ([c3eea15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3eea15fd406b59899e83b7a7478fb7a53b2886d))
* UCRM-1459 Fix bug template edit record ([2cd136c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2cd136c3f952706dfa1f3b869baefe25491e1242))
* UCRM-1459 Fix template record when edit ([8c302c0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8c302c0c4cbe5c89e4b65fad4bbe9b21a54c6a49))
* UCRM-1492 default 0 field number in internet form ([6a95e38](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6a95e383a474c13dba79dde490ff3332bc5dc945))
* UCRM-1493 capcha in form ([1619e57](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1619e576c328b058304c1015b2cc6d4b51f16afd))
* UCRM-1493 fix ([3087e5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3087e5de784cf32ec7d1b7a6229bfd1691d0ce9f))
* UCRM-1494 config size QR ([b8cde4d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b8cde4d92ebefa7de022b8e2e93c66953bed1f24))
* UCRM-1494 fix qr code ([b48fd7e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b48fd7ecb6636f11e9f5343d473d559305e39697))
* UCRM-1496 change QR name ([ba2ea7a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ba2ea7ada3dcb2a743ff7b07c448bdbef05d178e))
* UCRM-1502 fix response BE error ([3216f96](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3216f96a0c88f124cc3f40751c32f7d89af696c1))
* UCRM-1502 format only number in field text ([2e6d0db](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2e6d0db79bb3a684d6c8c6f0d3b0b3d8b75c933c))
* UCRM-1504 allow setting uppercase and lowercase in internetform ([78790ba](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/78790baf4c8b836d403311ff5c3065370f54b864))
* UCRM-1597 Fix Authentication type ([c09340b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c09340b08bc850739c2755748aa4f11b260b603b))
* UCRM-408 tronglv-fix fix response  data BE error ([f00ca77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f00ca7730aa5cb318d0e812705cd638c0b7b7dc7))
* UCRM-469 tronglv QM ([ce56c51](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ce56c51bd9a9049f9aeae8ec32fb6e0206262258))
* UI QM (UCRM 1307) ([5fea90b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5fea90bf9e7f80d0c82983cc5e8bfa9a02574df0))


### Others

* **release:** 2.48.0 ([38cb6fc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/38cb6fc72bea6b3612cec7175ff6a830e2300cf4))
* **release:** 2.48.0 ([0f3011e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0f3011ef63351d17b9fa22654d327781a19f8827))

### [2.46.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.46.0...v2.46.1) (2023-06-16)


### Bug Fixes

* hot fix FE v2.46.0 ([b38597e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b38597ede251d4998c542afd75bca5083528c20b))


### CI

* test ([068ea0e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/068ea0e48f6917f519426b6f476b71cbdb9df780))


### Others

* **release:** 2.46.1 ([48cdac3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/48cdac3b3252f090ef838077532cfc013b30f5ec))

### [2.45.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.45.0...v2.45.1) (2023-05-25)

## [2.45.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.44.1...v2.45.0) (2023-05-23)

### [2.42.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.41.0...v2.42.1) (2023-05-05)

### [2.40.2](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.40.1...v2.40.2) (2023-04-05)

### [2.40.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.40.0...v2.40.1) (2023-03-28)

## [2.40.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.38.2...v2.40.0) (2023-03-21)

### [2.38.2](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.38.1...v2.38.2) (2023-03-09)

### [2.38.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.38.0...v2.38.1) (2023-03-07)

## [2.48.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.46.1...v2.48.0) (2023-06-27)


### Features

* [UCRM-1000] [Form] Allow to attach file. Done ([75298c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/75298c11ba3df6f75c70c1d4fb9b71e4b381e02f))
* object standard (#UCRM-436) ([aec6486](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aec648666c3390924408f48fd2431175cf6c49a5)), closes [#UCRM-436](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-436)
* UCRM-408 improvement search in listview ([952a0bb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/952a0bb0cbf6c46dd6a8e0718a68c8e6edea9bd7))


### Bug Fixes

* (CTI) add generate Token C247 (#UCRM-1378) ([adbc73b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/adbc73bb8cccd88ddd40ed773fa7f8786ea0a380)), closes [#UCRM-1378](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1378)
* [BUCRM-1631] [tronglv-fix] ([5402356](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/540235623bbeb609beea9f881ec6bb5be15ec295))
* [BUCRM-1639] tronglv fix bug and display type file ([148ddfe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/148ddfe80254305cf70e3171a09c59be6c72cfbf))
* [Object Standard] Allow clear ([b6b6918](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b6b6918efe08a8dcf95db5b1192bd90be43e5f35))
* [tronglv-feature] allow file in internet form ([e91c534](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e91c53429def7e90e5218a7309446a5911fc3145))
* [tronglv-fix] add noti max capacity ([fb8347a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fb8347a755b627df98f340d75dbd5b2043712960))
* [tronglv-fix] fix API URL in file user ([77c49dc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/77c49dc87c5d0a27fb51bff0bd3e00f2de67bb81))
* [tronglv-fix] high quality QR code ([c77ab4b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c77ab4be5e8d3f9c4c35e634bfb5c86e9dd02c2d))
* add clone config email [Root]; Allow update Interaction setting ([71c7b77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/71c7b7738052b0abb7cf8fd7a9ed939c7a196052))
* add Email config in Root (#UCRM-1322) ([a79ad74](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a79ad748375ef97dfb612f236a8efd29e016cb02)), closes [#UCRM-1322](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1322)
* add tenant id when clone ([64970aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64970aa0c2fabd1e81b9328b26976a291df48865))
* BUCRM-1640 tronglv-fix ([303ea78](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/303ea787a1fbc8c37273a61700800cb784b94a18))
* BUCRM-1655 Tronglv fix issues search tag in knowledge base ([fe97a85](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fe97a85351d1e5fe8951cd8c7743faad507e81e7))
* build ([84a3198](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/84a3198f0295acb57924971b381d515314bea4ed))
* change API get sender in component Email (#UCRM-1322) ([31105d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31105d0a78e79b0c0efbc4ec55d4638b5595c3d9)), closes [#UCRM-1322](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1322)
* hide preview Editor ([22e1723](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/22e172374f3d3bf9e5f3fa7a0266d728cb50e9c3))
* UCRM-1282 tronglv-fix ([883ff41](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/883ff416614e42841a51fb901ed96fdf8b80f813))
* UCRM-1322 unrequired email config ([8ced9e4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8ced9e45ef0eedaf60232c3d07b82d6a9aa1bd12))
* UCRM-1621 tronglv-fix filter thời gian trong knowledge base ([ce1c30b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ce1c30b371d631fa47f4f6bff8a9964f71dd5776))
* UCRM-1646 tronglv-fix required field file ([2ee094d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2ee094d38fca8cebea18632f7263dd88ebca09a6))


### Others

* **release:** 2.47.0 ([9966750](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9966750fc28028805e19a24ca0c771af80171ac6))

## [2.47.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.46.0...v2.47.0) (2023-06-22)

### Features

- [UCRM-1000] [Form] Allow to attach file. Done ([75298c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/75298c11ba3df6f75c70c1d4fb9b71e4b381e02f))
- object standard (#UCRM-436) ([aec6486](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aec648666c3390924408f48fd2431175cf6c49a5)), closes [#UCRM-436](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-436)

### Bug Fixes

- (CTI) add generate Token C247 (#UCRM-1378) ([adbc73b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/adbc73bb8cccd88ddd40ed773fa7f8786ea0a380)), closes [#UCRM-1378](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1378)
- [BUCRM-1631] [tronglv-fix] ([5402356](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/540235623bbeb609beea9f881ec6bb5be15ec295))
- [BUCRM-1639] tronglv fix bug and display type file ([148ddfe](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/148ddfe80254305cf70e3171a09c59be6c72cfbf))
- [Object Standard] Allow clear ([b6b6918](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b6b6918efe08a8dcf95db5b1192bd90be43e5f35))
- [tronglv-feature] allow file in internet form ([e91c534](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e91c53429def7e90e5218a7309446a5911fc3145))
- [tronglv-fix] add noti max capacity ([fb8347a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fb8347a755b627df98f340d75dbd5b2043712960))
- [tronglv-fix] fix API URL in file user ([77c49dc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/77c49dc87c5d0a27fb51bff0bd3e00f2de67bb81))
- [tronglv-fix] high quality QR code ([c77ab4b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c77ab4be5e8d3f9c4c35e634bfb5c86e9dd02c2d))
- add Email config in Root (#UCRM-1322) ([a79ad74](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a79ad748375ef97dfb612f236a8efd29e016cb02)), closes [#UCRM-1322](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1322)
- BUCRM-1640 tronglv-fix ([303ea78](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/303ea787a1fbc8c37273a61700800cb784b94a18))
- BUCRM-1655 Tronglv fix issues search tag in knowledge base ([fe97a85](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fe97a85351d1e5fe8951cd8c7743faad507e81e7))
- build ([84a3198](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/84a3198f0295acb57924971b381d515314bea4ed))
- change API get sender in component Email (#UCRM-1322) ([31105d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31105d0a78e79b0c0efbc4ec55d4638b5595c3d9)), closes [#UCRM-1322](https://gitlab.smbbasebs.com/crm/ucrm-fe/issues/UCRM-1322)
- hide preview Editor ([22e1723](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/22e172374f3d3bf9e5f3fa7a0266d728cb50e9c3))
- UCRM-1282 tronglv-fix ([883ff41](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/883ff416614e42841a51fb901ed96fdf8b80f813))
- UCRM-1322 unrequired email config ([8ced9e4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8ced9e45ef0eedaf60232c3d07b82d6a9aa1bd12))
- UCRM-1621 tronglv-fix filter thời gian trong knowledge base ([ce1c30b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ce1c30b371d631fa47f4f6bff8a9964f71dd5776))
- UCRM-1646 tronglv-fix required field file ([2ee094d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2ee094d38fca8cebea18632f7263dd88ebca09a6))
- update version ([67ec6db](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/67ec6dbed503cde775941d5b67de5a8a0e0bc8a9))

### [2.46.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.46.0...v2.46.1) (2023-06-16)

### Bug Fixes

- hot fix FE v2.46.0 ([b38597e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b38597ede251d4998c542afd75bca5083528c20b))

## [2.46.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.45.3...v2.46.0) (2023-06-13)

### Features

- [SLA] Allow to set Action for each Alert Milestone and Overdue SLA ([d7fcff1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d7fcff178f3e251f4fc51cab2fe3bfe448840f35))
- [tronglv-feature] Allow to set required field in Form ([0aceb24](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0aceb24ae7a95180389b390613165ded75c4fc28))
- [tronglv-feature] form apply grading form setting - done ([bac9269](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bac9269692b17c19f2d89ba43a5824d6f2729905))
- [Workflow] add trigger when popup [UCRM-1226] ([34148c3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/34148c3b271ce637ff8af5800c29859acff5b406))

### Bug Fixes

- [DynamicButton] hide encrypted fields ([facf39e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/facf39e8b5481e0d93505e98ebebe0a965378b14))
- [Email] ref body ([dad9c70](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dad9c70d813dc57f683704b5ccfcb9a1ad53c6ce))
- [PopupURL] add API check workflow ([f7369a0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f7369a0a2ba84413323e80f87e2cf3da838c0413))
- [SLA] Add overdue SLA ([f0d4598](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f0d4598d31504117cb40167e584f6b8174b6ea40))
- [tronglv-feature] apply grading in internet form done ([91cbb5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/91cbb5d97d4eb1427f00ba87838992a3251c71bf))
- [tronglv-fix] apply grading in form ([2e8b033](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2e8b0331cb03cac716abd2518cd0449083a94857))
- [tronglv-fix] fix BUCRM-1538 ([552dd27](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/552dd2728d5164ddf81fb48ab44451fc9ee1ccbe))
- [tronglv-fix] fix grading in form ([548878c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/548878c28e3f4c370bc61cdb95936761c84865bc))
- [tronglv-fix] Fix issue Cross-Site Scripting in all text-editor ([e8f1754](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8f17549804263c7b74644d39c9c49c19016c223))
- [tronglv-fix] Fix issue Cross-Site Scripting in all text-editor ([568cd20](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/568cd20f5ab9eec29e41fc22b58cf8307e5b21b6))
- [tronglv-update] update login page ([1a8a7bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1a8a7bc1af3ee41783eb815d589772037954e554))
- [Workflow] add label trigger in table ([21ab83c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/21ab83c9da507a2afa1090d917643e8223b4a868))
- add API run index in root ([aa87e78](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aa87e78ec201a6f9e40749504c7f03879e6faf22))
- add ask before paste html Editor ([3999eec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3999eec113492e0eb9e447e5235213a840b225df))
- add lineHeight editor ([6176cd5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6176cd5db23f1549058125c241fc011789f52e64))
- API IC ([cff040d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cff040df0d996002c82c2fba5b7351ed184562ab))
- fix ̀500 Internal Server cause by showFields loading after access into it ([787c191](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/787c1917b08f2fed4150c004191b168cfce9842a))
- fix Form ([2c15775](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2c15775bd48fc7207c35953ddb7d31e2462d758b))
- hide encrypted fields action Send email ([64ee3b2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64ee3b281c922c8347247e2c8bb8c405c7b72e4a))
- hide encrypted fields in some features ([7dc0e0c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7dc0e0cd5eb962c985ea8aee05f087de820e4064))
- limit user's information in API ([e76daa5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e76daa5595cba976ea3f5b3f1476f9acfb2d63ff))
- link IC ([509fd0f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/509fd0f778e0d87110e12071ba509218c3970d40))
- tronglv-fix ([608d834](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/608d834893d6dd7e2fffbb342f5b7e24e015c268))
- unhide encrypted fields in action Send email ([36fddd9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36fddd9ba0201a5c7c3ffee3c04c791430155dee))
- update version 2.45.3 ([a8fb12d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a8fb12d8fb69d94ccb99cc924cda85a89fe88abb))

### [2.45.3](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.45.2...v2.45.3) (2023-06-01)

### Bug Fixes

- [tronglv-fix] allow label textarea ([b6d7af3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b6d7af33bb53408a300b316d46b941312ba5519f))
- [tronglv-fix] fix color in png ([ec99e1a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ec99e1a2f28faee77991b65d6924178d92e6589a))
- [tronglv-fix] fix perfomance modal share ([a60f9ad](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a60f9ad2bf9e21fa99b88df964a9b68556099dd4))
- [tronglv-fix] update color in qr ([cdee4db](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cdee4dbd3926b7ee8a8454ffc5c9344b35e16b7e))
- [tronglv-update] change display field type file ([ac7ffae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ac7ffae188656ebd0fa07587ef9ef0060f7664f8))
- [tronglv-update] delete showtime in article ([090f914](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/090f914730fe7bff95417835f82fd420196cb884))
- [tronglv-update] update display multiple select ([22fc459](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/22fc4595d52e810d0546cd69929076c8ccc7ef4e))
- [tronlv-fix] block delete component ([6a69668](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6a69668ce2df7ae0b9fb4ae4383a48f8db4f8f33))
- [tronlv-fix] fix UI noti ([2e12c58](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2e12c58a652b1bdb3c24454c518cf06f6273d231))
- [tronvlv-update] allow update display_status_options in knowledge base setting ([6ed0f84](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6ed0f842601096b8f2b369278888b5aef2ac12f0))
- update version 2.45.2 ([5b0da82](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5b0da82ce74e5d4abdfaac40e37e3be74c0f83cf))

### [2.45.2](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.44.1...v2.45.2) (2023-05-29)

### Bug Fixes

- "tronglv-color qr" ([4acd603](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4acd603dedc8455bc3d36c865374a2b3bbc61195))
- [DynamicButton-UpdateField] Load old data; required; text editor ([1d6baf8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1d6baf81183c94f81de98dad8a2e244576cf2ef9))
- [DynamicButton] change update ([2614a3b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2614a3b2139df7ddd1c2623c57771568bbc23c56))
- [DynamicButton] Clear dynamic fields when change object ([ae15761](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ae15761b51663e7046001ec37cb711868d75c32a))
- [DynamicButton] Multi details and Details ([2289a4f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2289a4f04c139b640e29d5300d842a3f4c4aa0f2))
- [Email] Allow change subject when reply, forward email ([ae0dd32](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ae0dd32070d6ec22da271b9d42a5e2c46fbcc089))
- [Email] Allow copy table from excel ([9165202](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9165202f0506cec12fce3f1cf6f8e7414da66e76))
- [Report] Edit value fields chart ([e448493](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e4484938036feba40d692c68d54418a713214277))
- [tronglv-feature] Editor with field textarea ([1ef6b2a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1ef6b2ae1789ebb6da2b2008b0f16899f8e7d7a4))
- [tronglv-fix] fix border in table component ([0f29323](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0f293237ae710d38a61c59d75ee53f7b443a8948))
- [tronglv-fix] fix time range in modal component ([07f18e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/07f18e07882035e7f728f9afd2ac7e0609a09a41))
- [tronglv-update] clear required and massedit when change field type ([142a932](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/142a93235085fde09fdd1cac46cdb63b5d9cdd8f))
- [tronglv-update] update sms outgoing ([9624a92](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9624a92a0a299e4e7e4a515b6e48741a46917e1e))
- [VoiceBiometric] add outgoing call ([a4af8d4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a4af8d4f5f557c41efbc0ba6535f1e52ea3b8270))
- crash comment ([57b3ebf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/57b3ebfe5e1f9ffe4ff986c8431a53c9a3c0bfc2))
- fix border table ([3f30a5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3f30a5d8726521b2d02d9e2703ccf592c03e4b04))
- IC chat widget SSO ([fed103f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fed103fff5a85d00019d52f117b4104e2b1ca218))
- modal version ([58a3934](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/58a3934e22ee03d69aec6f8f6a0dea62026870fe))
- Protect from cross-site scripting attacks ([65fc22d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/65fc22d802c13865b51cc9e957084833d6fa504f))
- table view ([7249371](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/724937191d27ddbeb83da023d87bb33a37c736a6))
- update version 2.44.1 ([5883b37](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5883b37a68f5390046191c1dadad5712b683205d))

### Others

- **release:** 2.45.0 ([930e71b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/930e71ba2cdf120bbe0a60a5b22f59d4d54f31dd))
- **release:** 2.45.1 ([eb1375e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eb1375e682f1ad170ea041d995be07162c1730f2))

### [2.45.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.45.0...v2.45.1) (2023-05-25)

### Bug Fixes

- [DynamicButton-UpdateField] Load old data; required; text editor ([1d6baf8](https://10.189.120.23/crm/ucrm-fe/commit/1d6baf81183c94f81de98dad8a2e244576cf2ef9))
- [DynamicButton] change update ([2614a3b](https://10.189.120.23/crm/ucrm-fe/commit/2614a3b2139df7ddd1c2623c57771568bbc23c56))
- [DynamicButton] Clear dynamic fields when change object ([ae15761](https://10.189.120.23/crm/ucrm-fe/commit/ae15761b51663e7046001ec37cb711868d75c32a))
- [DynamicButton] Multi details and Details ([2289a4f](https://10.189.120.23/crm/ucrm-fe/commit/2289a4f04c139b640e29d5300d842a3f4c4aa0f2))
- [tronglv-feature] Editor with field textarea ([1ef6b2a](https://10.189.120.23/crm/ucrm-fe/commit/1ef6b2ae1789ebb6da2b2008b0f16899f8e7d7a4))
- [tronglv-fix] fix border in table component ([0f29323](https://10.189.120.23/crm/ucrm-fe/commit/0f293237ae710d38a61c59d75ee53f7b443a8948))
- [tronglv-update] clear required and massedit when change field type ([142a932](https://10.189.120.23/crm/ucrm-fe/commit/142a93235085fde09fdd1cac46cdb63b5d9cdd8f))
- fix border table ([3f30a5d](https://10.189.120.23/crm/ucrm-fe/commit/3f30a5d8726521b2d02d9e2703ccf592c03e4b04))

## [2.45.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.40.2...v2.45.0) (2023-05-23)

### Features

- [tronglv-feature] Bổ sung Logo trong QR Code ([b533a4d](https://10.189.120.23/crm/ucrm-fe/commit/b533a4d7d72684db67a9add20a8b925473545097))
- [tronglv-feature] copy body email ([2e4c964](https://10.189.120.23/crm/ucrm-fe/commit/2e4c9643febcb1525dedf7a38e338c0efd218598))
- [tronglv-feature] dynamic field for internet form ([140aa0b](https://10.189.120.23/crm/ucrm-fe/commit/140aa0b53cc117dbd242dc129e03804b4386bea3))
- [tronglv-feature] improvement internet form ([92852d1](https://10.189.120.23/crm/ucrm-fe/commit/92852d133ca4d9b9178a6ebd63940f15b527b123))
- cRUD Package ([e9b19aa](https://10.189.120.23/crm/ucrm-fe/commit/e9b19aa55c1cd5f04a712ed17f56d7a1c7968799))
- template file ([f371bfc](https://10.189.120.23/crm/ucrm-fe/commit/f371bfcfa8da63e5cdeb0808628a34c2ac42e152))

### Bug Fixes

- [CustomAdminTenant] Change key ([3f0f75e](https://10.189.120.23/crm/ucrm-fe/commit/3f0f75ef86b79827d377279afa14b6d912a00907))
- [DynamicButton] Allow update fields when run dynamic button ([f41da61](https://10.189.120.23/crm/ucrm-fe/commit/f41da6148fdd7ef04ad31fa78bc7fca670f8772c))
- [Email] Allow change subject when reply, forward email ([ae0dd32](https://10.189.120.23/crm/ucrm-fe/commit/ae0dd32070d6ec22da271b9d42a5e2c46fbcc089))
- [Finesse] Add identify phone prefix ([fd4cd3b](https://10.189.120.23/crm/ucrm-fe/commit/fd4cd3bab4ee131e7081c1a636d7f62b1ccfc9eb))
- [Finesse] add test connection, update scoring, allow direct signal ([297a2df](https://10.189.120.23/crm/ucrm-fe/commit/297a2df0fa4df3d938f7bd09e2849563e8e42284))
- [InteractionCall] Add meta data QM ([432e7eb](https://10.189.120.23/crm/ucrm-fe/commit/432e7eb9ea73623e2875a351a64377a3486d66b6))
- [Report-Timerange] Add select field ([c289310](https://10.189.120.23/crm/ucrm-fe/commit/c289310ab71ec03555eeea68df1cbff3a3710ca6))
- [Report] Add funnel chart ([9f97591](https://10.189.120.23/crm/ucrm-fe/commit/9f97591018c87383378477ce4b3b9bb2bc64b32d))
- [Report] Add horizontal bar and details pie chart; [Password] Email,forgot, changePass,createUser ([cabe1da](https://10.189.120.23/crm/ucrm-fe/commit/cabe1da52f7a7054f88fa50cf7f12b765309b874))
- [Report] Add time range ([a63c3a1](https://10.189.120.23/crm/ucrm-fe/commit/a63c3a15161c618dc651e406a0c28e11243d43bd))
- [Report] Edit value fields chart ([e448493](https://10.189.120.23/crm/ucrm-fe/commit/e4484938036feba40d692c68d54418a713214277))
- [troglv-fix] fix VI ([14e012e](https://10.189.120.23/crm/ucrm-fe/commit/14e012ee07af0a044cc7f53d4898c2536601ab69))
- [tronglv-feature] add external qr code ([35c83aa](https://10.189.120.23/crm/ucrm-fe/commit/35c83aadd80db3f77991d73ef80fc81c028d5f84))
- [tronglv-feature] clone customview ([208bf5d](https://10.189.120.23/crm/ucrm-fe/commit/208bf5d55f2fcd6f9b1356d39698e0a37b9225e8))
- [tronglv-feature] custom view ([64a5b72](https://10.189.120.23/crm/ucrm-fe/commit/64a5b72916dd26d0e6bcf733eee057803a444e1d))
- [tronglv-feature] time range filter progress ([cf4c0c2](https://10.189.120.23/crm/ucrm-fe/commit/cf4c0c214081f81a3a470e8dcace1395c4e9a3fa))
- [tronglv-feature] time range in modal component ([c2a7278](https://10.189.120.23/crm/ucrm-fe/commit/c2a72784a0034c769b97a7ac4e768ca0142ddec2))
- [tronglv-feature] timerange in modal component ([63cd4c1](https://10.189.120.23/crm/ucrm-fe/commit/63cd4c17c4d143ece49a4c4b820e8aa12f987209))
- [tronglv-feature] ucrm 496 progress ([7c18ca3](https://10.189.120.23/crm/ucrm-fe/commit/7c18ca34f2bfa71686d70867312e59d22b6f19ce))
- [tronglv-feature] validate field phone number in internet form ([c3a8106](https://10.189.120.23/crm/ucrm-fe/commit/c3a8106c6463021a2c5f76220f049f135f705dff))
- [tronglv-fix] ([7673fc1](https://10.189.120.23/crm/ucrm-fe/commit/7673fc10e9b8978da8546708d41cb37ea18c06cf))
- [tronglv-fix] ([19e810b](https://10.189.120.23/crm/ucrm-fe/commit/19e810b4e100e5fcd76a171ea0d60bef568a741c))
- [tronglv-fix] BUCRM 1494 ([3a6d428](https://10.189.120.23/crm/ucrm-fe/commit/3a6d42849f1654ecf8f31a38ab8ba2d39a8c87a3))
- [tronglv-fix] BUGCRM 1509 1510 ([92208b1](https://10.189.120.23/crm/ucrm-fe/commit/92208b1280289569d9ef01b14cec43a3160a8e5e))
- [tronglv-fix] BUGCRM 1511 ([ef7ea4a](https://10.189.120.23/crm/ucrm-fe/commit/ef7ea4aac9aedc5e07d24fb3653623166eb3d49e))
- [tronglv-fix] delete time range in mutiple detail ([f2d644e](https://10.189.120.23/crm/ucrm-fe/commit/f2d644ef1502bb8ac96bc6d875e2c3108daa548f))
- [tronglv-fix] fix bug 1056-1057-1058 ([5f0af29](https://10.189.120.23/crm/ucrm-fe/commit/5f0af29f130e93ece77fffbbffc53cce364a9356))
- [tronglv-fix] fix bug form ([cd82199](https://10.189.120.23/crm/ucrm-fe/commit/cd821991a0df9e79b1e5464504a97bca62e2583a))
- [tronglv-fix] fix expose api ([fe8a849](https://10.189.120.23/crm/ucrm-fe/commit/fe8a8491b4ac005e847494ad29db8dcc1222c333))
- [tronglv-fix] fix file index layout ([1ded3b6](https://10.189.120.23/crm/ucrm-fe/commit/1ded3b69c653d9e6db51457ab91a00f701275df6))
- [tronglv-fix] fix form ([f45a56f](https://10.189.120.23/crm/ucrm-fe/commit/f45a56f2484d09466140b22bd5f2d43fba213eb3))
- [tronglv-fix] fix form.js ([03755ff](https://10.189.120.23/crm/ucrm-fe/commit/03755fff8f0f659dccd1e171012e59191d71a11a))
- [tronglv-fix] fix height third party, save in with detail ([65044bf](https://10.189.120.23/crm/ucrm-fe/commit/65044bfc9c8c29cd7b710f2842707b9aed08ae29))
- [tronglv-fix] fix in tenant root ([be445e0](https://10.189.120.23/crm/ucrm-fe/commit/be445e09540f14fd43412d4692876232830b074b))
- [tronglv-fix] fix modal call api ([9a4ef87](https://10.189.120.23/crm/ucrm-fe/commit/9a4ef87ef8cd96158e576b870dfa8dec867ad251))
- [tronglv-fix] fix size start ([0f57bc9](https://10.189.120.23/crm/ucrm-fe/commit/0f57bc9be64c278d6f1ed0078c55b69cafd591f7))
- [tronglv-fix] fix template file ([c77b489](https://10.189.120.23/crm/ucrm-fe/commit/c77b48931500e9c06f3c6b7680d4aa9b44e18915))
- [tronglv-fix] fix time range in modal component ([07f18e0](https://10.189.120.23/crm/ucrm-fe/commit/07f18e07882035e7f728f9afd2ac7e0609a09a41))
- [tronglv-fix] fix url in qr code ([1d2930e](https://10.189.120.23/crm/ucrm-fe/commit/1d2930e2b596fabb6ed0309e0f136bf1f9604aae))
- [tronglv-fix] min height for body ([49aacd9](https://10.189.120.23/crm/ucrm-fe/commit/49aacd9eb51372f235d4caaadbe5a72a82460c6e))
- [tronglv-fix] update for mobile and encryption ([c5802f1](https://10.189.120.23/crm/ucrm-fe/commit/c5802f19c40606508fb6aab22984fe37eaa66c5a))
- [tronglv-fix] update message ([5dfbe14](https://10.189.120.23/crm/ucrm-fe/commit/5dfbe14fdf93295d9390f642af1a56ef84935860))
- [tronglv-fix] update total in data report ([b2298ca](https://10.189.120.23/crm/ucrm-fe/commit/b2298ca4f67f5861a41cdf8265dbe3ed2b58072a))
- [tronglv-fix] url QR code ([ea9161f](https://10.189.120.23/crm/ucrm-fe/commit/ea9161f0f50d5449e56701af237c81b6eca71e5f))
- [tronglv-improvement] improve internet form - config success ([c23cb3a](https://10.189.120.23/crm/ucrm-fe/commit/c23cb3afdcb8ed55563683ea1c979c016465fb5d))
- [tronglv-update] custom admin internet form ([e3a5ab9](https://10.189.120.23/crm/ucrm-fe/commit/e3a5ab961eaeb827288fd6e82d7cf27a5b497d17))
- [tronglv-update] custom view - set default custom view ([446e9aa](https://10.189.120.23/crm/ucrm-fe/commit/446e9aac7dc570a7586b18569e71d2970f09b5d9))
- [tronglv-update] display internet form ([c41232b](https://10.189.120.23/crm/ucrm-fe/commit/c41232b7a5cee18f57fa5ded4b7e32764ced6d65))
- [tronglv-update] expose api and tenant root ([8f860d0](https://10.189.120.23/crm/ucrm-fe/commit/8f860d05defc2c58dbd5a2bdd7712b4034218df8))
- [tronglv-update] hidden form ([84db87b](https://10.189.120.23/crm/ucrm-fe/commit/84db87b6b311e0bd0a2f1a9757aebaab077f9b8f))
- [tronglv-update] improvement internet form and un comment file template ([64841d9](https://10.189.120.23/crm/ucrm-fe/commit/64841d98a400173b56428ae44b3cd397e994f515))
- [tronglv-update] page policy ([d150b14](https://10.189.120.23/crm/ucrm-fe/commit/d150b14ef763e28cccd21c3eecc47297828f831c))
- [tronglv-update] update config in expose api ([65c7288](https://10.189.120.23/crm/ucrm-fe/commit/65c728816b72444f79ea8ca67c982d164bddeae6))
- [tronglv-update] update internet form ([58ceda4](https://10.189.120.23/crm/ucrm-fe/commit/58ceda414c1ea467574bf8f72faa8c0c180693b2))
- [tronglv-update] update regex phone number ([8ff450e](https://10.189.120.23/crm/ucrm-fe/commit/8ff450e132f4ec84e424587456ad64121cb91fbf))
- [tronglv-update] update sms outgoing ([9624a92](https://10.189.120.23/crm/ucrm-fe/commit/9624a92a0a299e4e7e4a515b6e48741a46917e1e))
- [tronglv-update] update template file ([30df534](https://10.189.120.23/crm/ucrm-fe/commit/30df534873c59974939ac9fa5f2675ebb499b8c4))
- [tronglv-update] update template file ([7d03fb7](https://10.189.120.23/crm/ucrm-fe/commit/7d03fb76186975e4a83bcd8f2e4fa2a0243acdb3))
- [tronglv-update] update UI knowledge base ([95bf0b2](https://10.189.120.23/crm/ucrm-fe/commit/95bf0b2d13e1e8a85ef9fc49e966c8b5bcdd53d8))
- [tronglv] hidden template file ([a15a997](https://10.189.120.23/crm/ucrm-fe/commit/a15a997c8aeb2fb70fbdf1346cf234b6818c9e7d))
- [trongv-fix] accept internet form in mobile ([77df107](https://10.189.120.23/crm/ucrm-fe/commit/77df10765eed93f3bd83e6897b96c73fadc81b0f))
- [URL-Popup] Only add param text, textarea, email and not read only ([5917872](https://10.189.120.23/crm/ucrm-fe/commit/5917872b558d51d32dac8489e92d4f5803672e5b))
- [VoiceBiometric] add outgoing call ([a4af8d4](https://10.189.120.23/crm/ucrm-fe/commit/a4af8d4f5f557c41efbc0ba6535f1e52ea3b8270))
- [Workflows] Add link assign to interaction to user ([057a84c](https://10.189.120.23/crm/ucrm-fe/commit/057a84cb7105035af459e9df178f8167be72651f))
- add CN247 Cloud ([751c371](https://10.189.120.23/crm/ucrm-fe/commit/751c371c6b343ab5eab41361a245710d695ed2f9))
- add Custom admin Setting in root ([e151947](https://10.189.120.23/crm/ucrm-fe/commit/e151947559eac8182aac1ab643b5056a07c75ef4))
- add new field into interaction setting ([4d6717e](https://10.189.120.23/crm/ucrm-fe/commit/4d6717e4b568934cd6f2c9af4339bdd3cca3961d))
- add template file and actions ([175fb82](https://10.189.120.23/crm/ucrm-fe/commit/175fb82415079afa5a8044ee3e70fa1c0e04dab2))
- add timeline into listview with details ([c37563e](https://10.189.120.23/crm/ucrm-fe/commit/c37563e4c92c772a0a6e51147c92e414040c5f4a))
- bugs UCRM-466 ([27de66b](https://10.189.120.23/crm/ucrm-fe/commit/27de66b1f7be2e17a3d205cba98e28054c533a0a))
- change flow call API get token IC ([0089db3](https://10.189.120.23/crm/ucrm-fe/commit/0089db382baa99735122c8f837412ada4827f6a7))
- change label ([bea1aed](https://10.189.120.23/crm/ucrm-fe/commit/bea1aed7087714a886d54e001f071c17827e3a1e))
- custom admin root ([5da5e3c](https://10.189.120.23/crm/ucrm-fe/commit/5da5e3cced7dba9d6f339613d91b8f8c89281d5b))
- env ([9bb9eec](https://10.189.120.23/crm/ucrm-fe/commit/9bb9eeca9844c70ef7000ae5276544c2e54f6452))
- fix call API get token IC ([ec8cef6](https://10.189.120.23/crm/ucrm-fe/commit/ec8cef63adb37867ac44ba9fbab7d6043495d86f))
- fix reload to homepage of sso ([815cfe5](https://10.189.120.23/crm/ucrm-fe/commit/815cfe560ffa1af909325e2db6c20ea674828dc1))
- fix route ([f96e2de](https://10.189.120.23/crm/ucrm-fe/commit/f96e2de7aca0c9df86935430bf9229fce9d1d15e))
- fix sso reload ([140f86b](https://10.189.120.23/crm/ucrm-fe/commit/140f86b25c802fdaea9da8ceeb615e113eae95ce))
- IC chat widget SSO ([fed103f](https://10.189.120.23/crm/ucrm-fe/commit/fed103fff5a85d00019d52f117b4104e2b1ca218))
- Protect from cross-site scripting attacks ([65fc22d](https://10.189.120.23/crm/ucrm-fe/commit/65fc22d802c13865b51cc9e957084833d6fa504f))
- remove disable and add related fields ([705c5b0](https://10.189.120.23/crm/ucrm-fe/commit/705c5b0e36c81d26418af3fb088a1ffb028c1a94))
- reset fields after submit ([16f58c9](https://10.189.120.23/crm/ucrm-fe/commit/16f58c9eaf38702eeee7dcb1fcff899cda380f69))
- table view ([7249371](https://10.189.120.23/crm/ucrm-fe/commit/724937191d27ddbeb83da023d87bb33a37c736a6))
- unhide Template file ([4767b19](https://10.189.120.23/crm/ucrm-fe/commit/4767b19a3ee177e1a61ccd95c756676d2c903418))
- update version 2.43.0 ([ede39d0](https://10.189.120.23/crm/ucrm-fe/commit/ede39d0a806c982432544eda5658666ee38c575c))
- update version 2.44.0 ([fa513e0](https://10.189.120.23/crm/ucrm-fe/commit/fa513e03aa332013b3fc27053c14d80a73bb1915))
- update version 2.44.1 ([5883b37](https://10.189.120.23/crm/ucrm-fe/commit/5883b37a68f5390046191c1dadad5712b683205d))
- url popup; add: param to modal create ([5020226](https://10.189.120.23/crm/ucrm-fe/commit/502022678057e5f2550e0c254f34844b3dddbf11))

### Others

- **release:** 2.41.0 ([51f7ef2](https://10.189.120.23/crm/ucrm-fe/commit/51f7ef2f146b26a853074b0123a01c636b567399))
- **release:** 2.42.0 ([30d47d7](https://10.189.120.23/crm/ucrm-fe/commit/30d47d78ed90287dc6516c09358675319d042621))
- **release:** 2.42.1 ([71ed5eb](https://10.189.120.23/crm/ucrm-fe/commit/71ed5eb60b24b379cef476a434f1caca1d5cc8bd))
- **release:** 2.43.0 ([4458b8c](https://10.189.120.23/crm/ucrm-fe/commit/4458b8c3cc7be2b0e308a0bb4076a85072db452e))
- **release:** 2.44.0 ([40c1e2f](https://10.189.120.23/crm/ucrm-fe/commit/40c1e2fec1e716b6ce02c22fa2814bfa6a459740))
- **release:** 2.44.1 ([d5317a9](https://10.189.120.23/crm/ucrm-fe/commit/d5317a96cd1708318add5d305a656223d3cd0daa))

### [2.44.1](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.44.0...v2.44.1) (2023-05-19)

### Bug Fixes

- [Report] Add funnel chart ([9f97591](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9f97591018c87383378477ce4b3b9bb2bc64b32d))
- [Report] Add horizontal bar and details pie chart; [Password] Email,forgot, changePass,createUser ([cabe1da](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cabe1da52f7a7054f88fa50cf7f12b765309b874))
- [tronglv-feature] clone customview ([208bf5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/208bf5d55f2fcd6f9b1356d39698e0a37b9225e8))
- [tronglv-feature] time range filter progress ([cf4c0c2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cf4c0c214081f81a3a470e8dcace1395c4e9a3fa))
- [tronglv-feature] time range in modal component ([c2a7278](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c2a72784a0034c769b97a7ac4e768ca0142ddec2))
- [tronglv-feature] timerange in modal component ([63cd4c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/63cd4c17c4d143ece49a4c4b820e8aa12f987209))
- [tronglv-feature] ucrm 496 progress ([7c18ca3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7c18ca34f2bfa71686d70867312e59d22b6f19ce))
- [tronglv-fix] delete time range in mutiple detail ([f2d644e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f2d644ef1502bb8ac96bc6d875e2c3108daa548f))
- [tronglv-update] improvement internet form and un comment file template ([64841d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64841d98a400173b56428ae44b3cd397e994f515))
- change flow call API get token IC ([0089db3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0089db382baa99735122c8f837412ada4827f6a7))
- fix call API get token IC ([ec8cef6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ec8cef63adb37867ac44ba9fbab7d6043495d86f))
- unhide Template file ([4767b19](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4767b19a3ee177e1a61ccd95c756676d2c903418))
- update version 2.44.0 ([fa513e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fa513e03aa332013b3fc27053c14d80a73bb1915))

## [2.44.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.43.0...v2.44.0) (2023-05-15)

### Features

- [tronglv-feature] Bổ sung Logo trong QR Code ([b533a4d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b533a4d7d72684db67a9add20a8b925473545097))
- [tronglv-feature] dynamic field for internet form ([140aa0b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/140aa0b53cc117dbd242dc129e03804b4386bea3))

### Bug Fixes

- [tronglv-feature] validate field phone number in internet form ([c3a8106](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3a8106c6463021a2c5f76220f049f135f705dff))
- [tronglv-fix] fix form.js ([03755ff](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/03755fff8f0f659dccd1e171012e59191d71a11a))
- [tronglv-update] update regex phone number ([8ff450e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8ff450e132f4ec84e424587456ad64121cb91fbf))
- [URL-Popup] Only add param text, textarea, email and not read only ([5917872](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5917872b558d51d32dac8489e92d4f5803672e5b))
- update version 2.43.0 ([ede39d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ede39d0a806c982432544eda5658666ee38c575c))
- url popup; add: param to modal create ([5020226](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/502022678057e5f2550e0c254f34844b3dddbf11))

## [2.43.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.41.0...v2.43.0) (2023-05-09)

### Features

- [tronglv-feature] improvement internet form ([92852d1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/92852d133ca4d9b9178a6ebd63940f15b527b123))
- cRUD Package ([e9b19aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e9b19aa55c1cd5f04a712ed17f56d7a1c7968799))
- template file ([f371bfc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f371bfcfa8da63e5cdeb0808628a34c2ac42e152))

### Bug Fixes

- [CustomAdminTenant] Change key ([3f0f75e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3f0f75ef86b79827d377279afa14b6d912a00907))
- [DynamicButton] Allow update fields when run dynamic button ([f41da61](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f41da6148fdd7ef04ad31fa78bc7fca670f8772c))
- [Finesse] Add identify phone prefix ([fd4cd3b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fd4cd3bab4ee131e7081c1a636d7f62b1ccfc9eb))
- [InteractionCall] Add meta data QM ([432e7eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/432e7eb9ea73623e2875a351a64377a3486d66b6))
- [troglv-fix] fix VI ([14e012e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/14e012ee07af0a044cc7f53d4898c2536601ab69))
- [tronglv-feature] add external qr code ([35c83aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/35c83aadd80db3f77991d73ef80fc81c028d5f84))
- [tronglv-fix] BUCRM 1494 ([3a6d428](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3a6d42849f1654ecf8f31a38ab8ba2d39a8c87a3))
- [tronglv-fix] BUGCRM 1509 1510 ([92208b1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/92208b1280289569d9ef01b14cec43a3160a8e5e))
- [tronglv-fix] BUGCRM 1511 ([ef7ea4a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ef7ea4aac9aedc5e07d24fb3653623166eb3d49e))
- [tronglv-fix] fix bug 1056-1057-1058 ([5f0af29](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5f0af29f130e93ece77fffbbffc53cce364a9356))
- [tronglv-fix] fix bug form ([cd82199](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cd821991a0df9e79b1e5464504a97bca62e2583a))
- [tronglv-fix] fix expose api ([fe8a849](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fe8a8491b4ac005e847494ad29db8dcc1222c333))
- [tronglv-fix] fix file index layout ([1ded3b6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1ded3b69c653d9e6db51457ab91a00f701275df6))
- [tronglv-fix] fix in tenant root ([be445e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/be445e09540f14fd43412d4692876232830b074b))
- [tronglv-fix] fix modal call api ([9a4ef87](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9a4ef87ef8cd96158e576b870dfa8dec867ad251))
- [tronglv-fix] fix template file ([c77b489](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c77b48931500e9c06f3c6b7680d4aa9b44e18915))
- [tronglv-fix] fix url in qr code ([1d2930e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1d2930e2b596fabb6ed0309e0f136bf1f9604aae))
- [tronglv-fix] update message ([5dfbe14](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5dfbe14fdf93295d9390f642af1a56ef84935860))
- [tronglv-fix] url QR code ([ea9161f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ea9161f0f50d5449e56701af237c81b6eca71e5f))
- [tronglv-improvement] improve internet form - config success ([c23cb3a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c23cb3afdcb8ed55563683ea1c979c016465fb5d))
- [tronglv-update] expose api and tenant root ([8f860d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8f860d05defc2c58dbd5a2bdd7712b4034218df8))
- [tronglv-update] page policy ([d150b14](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d150b14ef763e28cccd21c3eecc47297828f831c))
- [tronglv-update] update config in expose api ([65c7288](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/65c728816b72444f79ea8ca67c982d164bddeae6))
- [tronglv-update] update internet form ([58ceda4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/58ceda414c1ea467574bf8f72faa8c0c180693b2))
- [tronglv-update] update template file ([30df534](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30df534873c59974939ac9fa5f2675ebb499b8c4))
- [tronglv-update] update template file ([7d03fb7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7d03fb76186975e4a83bcd8f2e4fa2a0243acdb3))
- [tronglv-update] update UI knowledge base ([95bf0b2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/95bf0b2d13e1e8a85ef9fc49e966c8b5bcdd53d8))
- [tronglv] hidden template file ([a15a997](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a15a997c8aeb2fb70fbdf1346cf234b6818c9e7d))
- [trongv-fix] accept internet form in mobile ([77df107](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/77df10765eed93f3bd83e6897b96c73fadc81b0f))
- [Workflows] Add link assign to interaction to user ([057a84c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/057a84cb7105035af459e9df178f8167be72651f))
- add CN247 Cloud ([751c371](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/751c371c6b343ab5eab41361a245710d695ed2f9))
- add Custom admin Setting in root ([e151947](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e151947559eac8182aac1ab643b5056a07c75ef4))
- add new field into interaction setting ([4d6717e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4d6717e4b568934cd6f2c9af4339bdd3cca3961d))
- add template file and actions ([175fb82](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/175fb82415079afa5a8044ee3e70fa1c0e04dab2))
- bugs UCRM-466 ([27de66b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/27de66b1f7be2e17a3d205cba98e28054c533a0a))
- change label ([bea1aed](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bea1aed7087714a886d54e001f071c17827e3a1e))
- custom admin root ([5da5e3c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5da5e3cced7dba9d6f339613d91b8f8c89281d5b))
- env ([9bb9eec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9bb9eeca9844c70ef7000ae5276544c2e54f6452))
- fix route ([f96e2de](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f96e2de7aca0c9df86935430bf9229fce9d1d15e))
- fix sso reload ([140f86b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/140f86b25c802fdaea9da8ceeb615e113eae95ce))
- remove disable and add related fields ([705c5b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/705c5b0e36c81d26418af3fb088a1ffb028c1a94))
- reset fields after submit ([16f58c9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/16f58c9eaf38702eeee7dcb1fcff899cda380f69))

### Others

- **release:** 2.42.0 ([30d47d7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30d47d78ed90287dc6516c09358675319d042621))
- **release:** 2.42.1 ([71ed5eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/71ed5eb60b24b379cef476a434f1caca1d5cc8bd))

### [2.42.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.42.0...v2.42.1) (2023-04-18)

### Bug Fixes

- [Finesse] Add identify phone prefix ([fd4cd3b](https://10.189.120.23/crm/ucrm-fe/commit/fd4cd3bab4ee131e7081c1a636d7f62b1ccfc9eb))
- add new field into interaction setting ([4d6717e](https://10.189.120.23/crm/ucrm-fe/commit/4d6717e4b568934cd6f2c9af4339bdd3cca3961d))

## [2.42.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.40.1...v2.42.0) (2023-04-13)

### Features

- [tronglv-feature] copy body email ([2e4c964](https://10.189.120.23/crm/ucrm-fe/commit/2e4c9643febcb1525dedf7a38e338c0efd218598))

### Bug Fixes

- [CallCenter] keep data list records ([4294965](https://10.189.120.23/crm/ucrm-fe/commit/4294965aa4c852175c3c5e3ddc2c6e1a2e9d8ae5))
- [Finesse] add test connection, update scoring, allow direct signal ([297a2df](https://10.189.120.23/crm/ucrm-fe/commit/297a2df0fa4df3d938f7bd09e2849563e8e42284))
- [Report-Timerange] Add select field ([c289310](https://10.189.120.23/crm/ucrm-fe/commit/c289310ab71ec03555eeea68df1cbff3a3710ca6))
- [Report] Add time range ([a63c3a1](https://10.189.120.23/crm/ucrm-fe/commit/a63c3a15161c618dc651e406a0c28e11243d43bd))
- [tronglv-feature] custom view ([64a5b72](https://10.189.120.23/crm/ucrm-fe/commit/64a5b72916dd26d0e6bcf733eee057803a444e1d))
- [tronglv-fix] ([7673fc1](https://10.189.120.23/crm/ucrm-fe/commit/7673fc10e9b8978da8546708d41cb37ea18c06cf))
- [tronglv-fix] ([19e810b](https://10.189.120.23/crm/ucrm-fe/commit/19e810b4e100e5fcd76a171ea0d60bef568a741c))
- [tronglv-fix] fix form ([f45a56f](https://10.189.120.23/crm/ucrm-fe/commit/f45a56f2484d09466140b22bd5f2d43fba213eb3))
- [tronglv-fix] fix height third party, save in with detail ([65044bf](https://10.189.120.23/crm/ucrm-fe/commit/65044bfc9c8c29cd7b710f2842707b9aed08ae29))
- [tronglv-fix] fix size start ([0f57bc9](https://10.189.120.23/crm/ucrm-fe/commit/0f57bc9be64c278d6f1ed0078c55b69cafd591f7))
- [tronglv-fix] min height for body ([49aacd9](https://10.189.120.23/crm/ucrm-fe/commit/49aacd9eb51372f235d4caaadbe5a72a82460c6e))
- [tronglv-fix] update for mobile and encryption ([c5802f1](https://10.189.120.23/crm/ucrm-fe/commit/c5802f19c40606508fb6aab22984fe37eaa66c5a))
- [tronglv-fix] update total in data report ([b2298ca](https://10.189.120.23/crm/ucrm-fe/commit/b2298ca4f67f5861a41cdf8265dbe3ed2b58072a))
- [tronglv-update] custom admin internet form ([e3a5ab9](https://10.189.120.23/crm/ucrm-fe/commit/e3a5ab961eaeb827288fd6e82d7cf27a5b497d17))
- [tronglv-update] custom view - set default custom view ([446e9aa](https://10.189.120.23/crm/ucrm-fe/commit/446e9aac7dc570a7586b18569e71d2970f09b5d9))
- [tronglv-update] dispaly internet form ([dfba66b](https://10.189.120.23/crm/ucrm-fe/commit/dfba66bb78ec6f5f61787b8c22dc7047d13da8bf))
- [tronglv-update] display internet form ([c41232b](https://10.189.120.23/crm/ucrm-fe/commit/c41232b7a5cee18f57fa5ded4b7e32764ced6d65))
- [tronglv-update] hidden form ([84db87b](https://10.189.120.23/crm/ucrm-fe/commit/84db87b6b311e0bd0a2f1a9757aebaab077f9b8f))
- [tronglv-update] update feature in third party done ([a5abc15](https://10.189.120.23/crm/ucrm-fe/commit/a5abc15370f14b24a244d037805f447ff9368fc2))
- add timeline into listview with details ([c37563e](https://10.189.120.23/crm/ucrm-fe/commit/c37563e4c92c772a0a6e51147c92e414040c5f4a))
- fix reload to homepage of sso ([815cfe5](https://10.189.120.23/crm/ucrm-fe/commit/815cfe560ffa1af909325e2db6c20ea674828dc1))
- remove form ([622dfb3](https://10.189.120.23/crm/ucrm-fe/commit/622dfb3c2024beedbf87697d3a6eb489fe9c21eb))

### Others

- **release:** 2.40.2 ([8d96233](https://10.189.120.23/crm/ucrm-fe/commit/8d96233229a486a8e151f616940a5e70f85f04dc))
- **release:** 2.41.0 ([51f7ef2](https://10.189.120.23/crm/ucrm-fe/commit/51f7ef2f146b26a853074b0123a01c636b567399))

## [2.41.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.38.0...v2.41.0) (2023-04-06)

### Features

- [tronglv-feature] copy body email ([2e4c964](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2e4c9643febcb1525dedf7a38e338c0efd218598))
- [tronglv-feature] tronglv feature share custom view ([3e433f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3e433f02799a61e8dc3d679723c5ee22d3bb21bf))

### Bug Fixes

- [CallCenter] Add loading when search core ([f7fbe9d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f7fbe9d69073c69f0e63cd3f02e702303fce70e4))
- [CallCenter] Add search core ([36bade3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36bade399c385f24813f433fdb1148f5b5d01b73))
- [CallCenter] Change button switch ([5a074ba](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5a074bac4f147c00295fb484f3213d5906382b42))
- [CallCenter] keep data list records ([4294965](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4294965aa4c852175c3c5e3ddc2c6e1a2e9d8ae5))
- [CallCenter] Search core ([409f998](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/409f9981e1c2f6f3b30e30a45033e7e038b6c738))
- [Finesse] add test connection, update scoring, allow direct signal ([297a2df](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/297a2df0fa4df3d938f7bd09e2849563e8e42284))
- [IC Chat Widget] Add loading when upload file; Send comment, file viber ([459dacc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/459daccfdc064b7dba96757e9fbc894ce0dd9115))
- [IC Chat Widget] Add send voice ([e2e896c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e2e896c4625b1e5b65c8ca607b2582efa85888c3))
- [IC Chat Widget] Add show file type when upload ([96504e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96504e6ddc615e1feebae434e9479815931dddcd))
- [IC Chat Widget] Add type comment ([337d12b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/337d12bd570c043bda02163fbc1ce10b8b93252e))
- [IC Chat Widget] Add type email and comment ([9776bf7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9776bf743e239322f8652df8f0eb164a28d637a8))
- [Interaction Email] Add body ([99a7ee9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/99a7ee94474c4d04066274e77ac0a68f988c8f09))
- [Interaction Setting] Add call status ([7262ff0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7262ff07700d0ac4673052b465567ccf359ce987))
- [Interaction Setting] Add phone number ([d3ccc9f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3ccc9ffd09ebce62d97fbe64709f40b84b56daa))
- [Report-Timerange] Add select field ([c289310](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c289310ab71ec03555eeea68df1cbff3a3710ca6))
- [Report] Add time range ([a63c3a1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a63c3a15161c618dc651e406a0c28e11243d43bd))
- [TabComponent] Add drag ([4bf9ddc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4bf9ddcaf3307a8c7807726fba3b648bb390a516))
- [tronglv-feature] custom view ([64a5b72](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64a5b72916dd26d0e6bcf733eee057803a444e1d))
- [tronglv-feature] form setting ([f6f5b50](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f6f5b503c471b5da2ad8698a59061af846ea281f))
- [tronglv-feature] form setting - done 80% ([0d87b14](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0d87b14c2b2d806d0026f84e7e1c9b0a67dc58af))
- [tronglv-feature] form setting progress ([1e99a79](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1e99a798d9d18b4b55a41a026532f5e2dad56ff1))
- [tronglv-feature] form setting progress ([064c61f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/064c61f5e0b466672c3fe0caa60e7d09342294ab))
- [tronglv-feature] form view build test ([959b5d2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/959b5d2124457bcca69c93fc0d3ddbca71113f53))
- [tronglv-feature] internet form progress ([30796cd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30796cdcf30e8ed3009d5676b93fcce25079f4b1))
- [tronglv-feature] share custom view ([11d8348](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/11d83489b9a15b29378f8bb1eb1bc9664ef0e232))
- [tronglv-feature] share custom view progress ([2a7e116](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2a7e11663998ca7d4d74861f666990365033e0d0))
- [tronglv-fix] ([7673fc1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7673fc10e9b8978da8546708d41cb37ea18c06cf))
- [tronglv-fix] ([19e810b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/19e810b4e100e5fcd76a171ea0d60bef568a741c))
- [tronglv-fix] ad form link ([ba5ae36](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ba5ae3626f76757f6e4d2a79f25b61204a378831))
- [tronglv-fix] display internet form to test ([2dcfa80](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2dcfa8080f56ca0ad13eaf3be505c984642e60a1))
- [tronglv-fix] fix bug 1318 1321 ([43fdae2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/43fdae2b673e74f4e4db03ae233bf5237a0815df))
- [tronglv-fix] fix confilict ([52c32df](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/52c32df245ecefb15e708fa5e9ec0323f3c4cfb9))
- [tronglv-fix] fix detail third party and with detail ([9969f82](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9969f820c4be154ca6827b7d2693990e8ad6367d))
- [tronglv-fix] fix form ([f45a56f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f45a56f2484d09466140b22bd5f2d43fba213eb3))
- [tronglv-fix] fix height third party, save in with detail ([65044bf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/65044bfc9c8c29cd7b710f2842707b9aed08ae29))
- [tronglv-fix] fix size start ([0f57bc9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0f57bc9be64c278d6f1ed0078c55b69cafd591f7))
- [tronglv-fix] fix sonaquebe ([c73e4cf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c73e4cf70ab65694c181ad100329c93b76db3f6b))
- [tronglv-fix] fix third party ([7124fcf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7124fcfbe575aef6647e257ff43f74e3c1999430))
- [tronglv-fix] fix third party ([556a179](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/556a1799ed34da4aac704aa04fe78158c66b1101))
- [tronglv-fix] fix total article, internet form, height third party ([260729a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/260729a639a7212e3def799540c503e3d76f17e9))
- [tronglv-fix] fix truncate knowledge base ([75c1c30](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/75c1c308740d47bcaafcde0045a99fd692d338f0))
- [tronglv-fix] hidden form ([0affba2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0affba2077faec7805f8dc6b2c39b77444833849))
- [tronglv-fix] hidden internet form ([f5ac617](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f5ac617cbf874d431f9dada0dd6ff19c0b8827b5))
- [tronglv-fix] min height for body ([49aacd9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/49aacd9eb51372f235d4caaadbe5a72a82460c6e))
- [tronglv-fix] update for mobile and encryption ([c5802f1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c5802f19c40606508fb6aab22984fe37eaa66c5a))
- [tronglv-fix] update total in data report ([b2298ca](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b2298ca4f67f5861a41cdf8265dbe3ed2b58072a))
- [tronglv-fix] update with detail and detail third party ([9b04d3e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9b04d3ee91324f6f0daea05b0c0724ec422520f0))
- [tronglv-test] test donem ([31d1d6a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31d1d6a4bf8961ad233b46534459cb96e77ee860))
- [tronglv-test] test form ([1a9afaa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1a9afaacacd6acc5074d3cca56fbb4b3a29d0984))
- [tronglv-test] test internet form ([18030cc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/18030cc9cef0611957fa35d56005c7a4f5b630a6))
- [tronglv-test] van trong test form ([5957ddb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5957ddb5baae2c3fe8ab5795e0a0b4bd957d1c8d))
- [tronglv-test] van trong test form setting ([482d4dd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/482d4dde79e24ca54851949a29f9c7537fc9eb80))
- [tronglv-update] custom admin internet form ([e3a5ab9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e3a5ab961eaeb827288fd6e82d7cf27a5b497d17))
- [tronglv-update] custom view - set default custom view ([446e9aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/446e9aac7dc570a7586b18569e71d2970f09b5d9))
- [tronglv-update] dispaly internet form ([dfba66b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/dfba66bb78ec6f5f61787b8c22dc7047d13da8bf))
- [tronglv-update] display internet form ([c41232b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c41232b7a5cee18f57fa5ded4b7e32764ced6d65))
- [tronglv-update] hidden form ([84db87b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/84db87b6b311e0bd0a2f1a9757aebaab077f9b8f))
- [tronglv-update] third party progress ([83a9955](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/83a995544918d1b98383ab6a68e07c50bbfdbabb))
- [tronglv-update] update feature in third party done ([a5abc15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a5abc15370f14b24a244d037805f447ff9368fc2))
- [tronglv-update] update third party ([c2e92c3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c2e92c37be0c5d89b25b65e8966b5059d1d29392))
- [tronglv-update] update third party in consolidated view ([147d716](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/147d716be51aa50dbe91b9ec8e259a9c7897ea96))
- add timeline into listview with details ([c37563e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c37563e4c92c772a0a6e51147c92e414040c5f4a))
- fix duplicate rule ([32fb53d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/32fb53de82070dfa422b67f7b770cc2adfe49ced))
- fix dynamic field for multi column detail ([e6049bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e6049bc4d0b84f7cfd5d7a698412ebc0a15f389a))
- fix reload to homepage of sso ([815cfe5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/815cfe560ffa1af909325e2db6c20ea674828dc1))
- **list view with detail component:** list view with detail component ([b9bf13d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b9bf13d16d37808ffe68df5da575954064cbbaed))
- remove form ([622dfb3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/622dfb3c2024beedbf87697d3a6eb489fe9c21eb))

### Others

- **release:** 2.38.1 ([c97fc6e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c97fc6e56cc1e009a58f8a70b4acc24c45c1c2a2))
- **release:** 2.38.2 ([db909e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/db909e63b90c9a308c79cafd58c439d26a1477ac))
- **release:** 2.39.0 ([deca6c8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/deca6c8f1d3a0c740901c970a8b17e5a94f32526))
- **release:** 2.40.0 ([b3751c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b3751c1eb96eebe89fa4e28d1aee4c3131867599))
- **release:** 2.40.1 ([48e9b06](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/48e9b060e38acc949f72a137bf0cebf7417c8e8d))
- **release:** 2.40.2 ([8d96233](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8d96233229a486a8e151f616940a5e70f85f04dc))

### [2.40.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.40.0...v2.40.2) (2023-03-29)

### Bug Fixes

- [CallCenter] Change button switch ([5a074ba](https://10.189.120.23/crm/ucrm-fe/commit/5a074bac4f147c00295fb484f3213d5906382b42))
- [CallCenter] keep data list records ([4294965](https://10.189.120.23/crm/ucrm-fe/commit/4294965aa4c852175c3c5e3ddc2c6e1a2e9d8ae5))
- [Interaction Setting] Add call status ([7262ff0](https://10.189.120.23/crm/ucrm-fe/commit/7262ff07700d0ac4673052b465567ccf359ce987))
- [tronglv-fix] display internet form to test ([2dcfa80](https://10.189.120.23/crm/ucrm-fe/commit/2dcfa8080f56ca0ad13eaf3be505c984642e60a1))
- [tronglv-fix] fix confilict ([52c32df](https://10.189.120.23/crm/ucrm-fe/commit/52c32df245ecefb15e708fa5e9ec0323f3c4cfb9))
- [tronglv-fix] fix third party ([7124fcf](https://10.189.120.23/crm/ucrm-fe/commit/7124fcfbe575aef6647e257ff43f74e3c1999430))
- [tronglv-fix] fix third party ([556a179](https://10.189.120.23/crm/ucrm-fe/commit/556a1799ed34da4aac704aa04fe78158c66b1101))
- [tronglv-fix] fix total article, internet form, height third party ([260729a](https://10.189.120.23/crm/ucrm-fe/commit/260729a639a7212e3def799540c503e3d76f17e9))
- [tronglv-fix] hidden form ([0affba2](https://10.189.120.23/crm/ucrm-fe/commit/0affba2077faec7805f8dc6b2c39b77444833849))
- [tronglv-fix] hidden internet form ([f5ac617](https://10.189.120.23/crm/ucrm-fe/commit/f5ac617cbf874d431f9dada0dd6ff19c0b8827b5))
- [tronglv-update] dispaly internet form ([dfba66b](https://10.189.120.23/crm/ucrm-fe/commit/dfba66bb78ec6f5f61787b8c22dc7047d13da8bf))
- [tronglv-update] third party progress ([83a9955](https://10.189.120.23/crm/ucrm-fe/commit/83a995544918d1b98383ab6a68e07c50bbfdbabb))
- [tronglv-update] update feature in third party done ([a5abc15](https://10.189.120.23/crm/ucrm-fe/commit/a5abc15370f14b24a244d037805f447ff9368fc2))
- [tronglv-update] update third party ([c2e92c3](https://10.189.120.23/crm/ucrm-fe/commit/c2e92c37be0c5d89b25b65e8966b5059d1d29392))
- [tronglv-update] update third party in consolidated view ([147d716](https://10.189.120.23/crm/ucrm-fe/commit/147d716be51aa50dbe91b9ec8e259a9c7897ea96))
- remove form ([622dfb3](https://10.189.120.23/crm/ucrm-fe/commit/622dfb3c2024beedbf87697d3a6eb489fe9c21eb))

### Others

- **release:** 2.40.1 ([48e9b06](https://10.189.120.23/crm/ucrm-fe/commit/48e9b060e38acc949f72a137bf0cebf7417c8e8d))

### [2.40.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.39.0...v2.40.1) (2023-03-28)

### Bug Fixes

- [CallCenter] Add loading when search core ([f7fbe9d](https://10.189.120.23/crm/ucrm-fe/commit/f7fbe9d69073c69f0e63cd3f02e702303fce70e4))
- [CallCenter] Add search core ([36bade3](https://10.189.120.23/crm/ucrm-fe/commit/36bade399c385f24813f433fdb1148f5b5d01b73))
- [CallCenter] Change button switch ([5a074ba](https://10.189.120.23/crm/ucrm-fe/commit/5a074bac4f147c00295fb484f3213d5906382b42))
- [CallCenter] Search core ([409f998](https://10.189.120.23/crm/ucrm-fe/commit/409f9981e1c2f6f3b30e30a45033e7e038b6c738))
- [Interaction Setting] Add call status ([7262ff0](https://10.189.120.23/crm/ucrm-fe/commit/7262ff07700d0ac4673052b465567ccf359ce987))
- [Interaction Setting] Add phone number ([d3ccc9f](https://10.189.120.23/crm/ucrm-fe/commit/d3ccc9ffd09ebce62d97fbe64709f40b84b56daa))
- [tronglv-fix] display internet form to test ([2dcfa80](https://10.189.120.23/crm/ucrm-fe/commit/2dcfa8080f56ca0ad13eaf3be505c984642e60a1))
- [tronglv-fix] fix confilict ([52c32df](https://10.189.120.23/crm/ucrm-fe/commit/52c32df245ecefb15e708fa5e9ec0323f3c4cfb9))
- [tronglv-fix] fix third party ([7124fcf](https://10.189.120.23/crm/ucrm-fe/commit/7124fcfbe575aef6647e257ff43f74e3c1999430))
- [tronglv-fix] fix third party ([556a179](https://10.189.120.23/crm/ucrm-fe/commit/556a1799ed34da4aac704aa04fe78158c66b1101))
- [tronglv-fix] fix total article, internet form, height third party ([260729a](https://10.189.120.23/crm/ucrm-fe/commit/260729a639a7212e3def799540c503e3d76f17e9))
- [tronglv-fix] hidden form ([0affba2](https://10.189.120.23/crm/ucrm-fe/commit/0affba2077faec7805f8dc6b2c39b77444833849))
- [tronglv-fix] hidden internet form ([f5ac617](https://10.189.120.23/crm/ucrm-fe/commit/f5ac617cbf874d431f9dada0dd6ff19c0b8827b5))
- [tronglv-update] third party progress ([83a9955](https://10.189.120.23/crm/ucrm-fe/commit/83a995544918d1b98383ab6a68e07c50bbfdbabb))
- [tronglv-update] update third party ([c2e92c3](https://10.189.120.23/crm/ucrm-fe/commit/c2e92c37be0c5d89b25b65e8966b5059d1d29392))
- [tronglv-update] update third party in consolidated view ([147d716](https://10.189.120.23/crm/ucrm-fe/commit/147d716be51aa50dbe91b9ec8e259a9c7897ea96))

### Others

- **release:** 2.40.0 ([b3751c1](https://10.189.120.23/crm/ucrm-fe/commit/b3751c1eb96eebe89fa4e28d1aee4c3131867599))

## [2.40.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.38.0...v2.40.0) (2023-03-21)

### Features

- [tronglv-feature] tronglv feature share custom view ([3e433f0](https://10.189.120.23/crm/ucrm-fe/commit/3e433f02799a61e8dc3d679723c5ee22d3bb21bf))

### Bug Fixes

- [CallCenter] Add loading when search core ([f7fbe9d](https://10.189.120.23/crm/ucrm-fe/commit/f7fbe9d69073c69f0e63cd3f02e702303fce70e4))
- [CallCenter] Add search core ([36bade3](https://10.189.120.23/crm/ucrm-fe/commit/36bade399c385f24813f433fdb1148f5b5d01b73))
- [CallCenter] Search core ([409f998](https://10.189.120.23/crm/ucrm-fe/commit/409f9981e1c2f6f3b30e30a45033e7e038b6c738))
- [IC Chat Widget] Add loading when upload file; Send comment, file viber ([459dacc](https://10.189.120.23/crm/ucrm-fe/commit/459daccfdc064b7dba96757e9fbc894ce0dd9115))
- [IC Chat Widget] Add send voice ([e2e896c](https://10.189.120.23/crm/ucrm-fe/commit/e2e896c4625b1e5b65c8ca607b2582efa85888c3))
- [IC Chat Widget] Add show file type when upload ([96504e6](https://10.189.120.23/crm/ucrm-fe/commit/96504e6ddc615e1feebae434e9479815931dddcd))
- [IC Chat Widget] Add type comment ([337d12b](https://10.189.120.23/crm/ucrm-fe/commit/337d12bd570c043bda02163fbc1ce10b8b93252e))
- [IC Chat Widget] Add type email and comment ([9776bf7](https://10.189.120.23/crm/ucrm-fe/commit/9776bf743e239322f8652df8f0eb164a28d637a8))
- [Interaction Email] Add body ([99a7ee9](https://10.189.120.23/crm/ucrm-fe/commit/99a7ee94474c4d04066274e77ac0a68f988c8f09))
- [Interaction Setting] Add phone number ([d3ccc9f](https://10.189.120.23/crm/ucrm-fe/commit/d3ccc9ffd09ebce62d97fbe64709f40b84b56daa))
- [TabComponent] Add drag ([4bf9ddc](https://10.189.120.23/crm/ucrm-fe/commit/4bf9ddcaf3307a8c7807726fba3b648bb390a516))
- [tronglv-feature] form setting ([f6f5b50](https://10.189.120.23/crm/ucrm-fe/commit/f6f5b503c471b5da2ad8698a59061af846ea281f))
- [tronglv-feature] form setting - done 80% ([0d87b14](https://10.189.120.23/crm/ucrm-fe/commit/0d87b14c2b2d806d0026f84e7e1c9b0a67dc58af))
- [tronglv-feature] form setting progress ([1e99a79](https://10.189.120.23/crm/ucrm-fe/commit/1e99a798d9d18b4b55a41a026532f5e2dad56ff1))
- [tronglv-feature] form setting progress ([064c61f](https://10.189.120.23/crm/ucrm-fe/commit/064c61f5e0b466672c3fe0caa60e7d09342294ab))
- [tronglv-feature] form view build test ([959b5d2](https://10.189.120.23/crm/ucrm-fe/commit/959b5d2124457bcca69c93fc0d3ddbca71113f53))
- [tronglv-feature] internet form progress ([30796cd](https://10.189.120.23/crm/ucrm-fe/commit/30796cdcf30e8ed3009d5676b93fcce25079f4b1))
- [tronglv-feature] share custom view ([11d8348](https://10.189.120.23/crm/ucrm-fe/commit/11d83489b9a15b29378f8bb1eb1bc9664ef0e232))
- [tronglv-feature] share custom view progress ([2a7e116](https://10.189.120.23/crm/ucrm-fe/commit/2a7e11663998ca7d4d74861f666990365033e0d0))
- [tronglv-fix] ad form link ([ba5ae36](https://10.189.120.23/crm/ucrm-fe/commit/ba5ae3626f76757f6e4d2a79f25b61204a378831))
- [tronglv-fix] fix bug 1318 1321 ([43fdae2](https://10.189.120.23/crm/ucrm-fe/commit/43fdae2b673e74f4e4db03ae233bf5237a0815df))
- [tronglv-fix] fix detail third party and with detail ([9969f82](https://10.189.120.23/crm/ucrm-fe/commit/9969f820c4be154ca6827b7d2693990e8ad6367d))
- [tronglv-fix] fix sonaquebe ([c73e4cf](https://10.189.120.23/crm/ucrm-fe/commit/c73e4cf70ab65694c181ad100329c93b76db3f6b))
- [tronglv-fix] fix truncate knowledge base ([75c1c30](https://10.189.120.23/crm/ucrm-fe/commit/75c1c308740d47bcaafcde0045a99fd692d338f0))
- [tronglv-fix] update with detail and detail third party ([9b04d3e](https://10.189.120.23/crm/ucrm-fe/commit/9b04d3ee91324f6f0daea05b0c0724ec422520f0))
- [tronglv-test] test donem ([31d1d6a](https://10.189.120.23/crm/ucrm-fe/commit/31d1d6a4bf8961ad233b46534459cb96e77ee860))
- [tronglv-test] test form ([1a9afaa](https://10.189.120.23/crm/ucrm-fe/commit/1a9afaacacd6acc5074d3cca56fbb4b3a29d0984))
- [tronglv-test] test internet form ([18030cc](https://10.189.120.23/crm/ucrm-fe/commit/18030cc9cef0611957fa35d56005c7a4f5b630a6))
- [tronglv-test] van trong test form ([5957ddb](https://10.189.120.23/crm/ucrm-fe/commit/5957ddb5baae2c3fe8ab5795e0a0b4bd957d1c8d))
- [tronglv-test] van trong test form setting ([482d4dd](https://10.189.120.23/crm/ucrm-fe/commit/482d4dde79e24ca54851949a29f9c7537fc9eb80))
- fix duplicate rule ([32fb53d](https://10.189.120.23/crm/ucrm-fe/commit/32fb53de82070dfa422b67f7b770cc2adfe49ced))
- fix dynamic field for multi column detail ([e6049bc](https://10.189.120.23/crm/ucrm-fe/commit/e6049bc4d0b84f7cfd5d7a698412ebc0a15f389a))
- **list view with detail component:** list view with detail component ([b9bf13d](https://10.189.120.23/crm/ucrm-fe/commit/b9bf13d16d37808ffe68df5da575954064cbbaed))

### Others

- **release:** 2.38.1 ([c97fc6e](https://10.189.120.23/crm/ucrm-fe/commit/c97fc6e56cc1e009a58f8a70b4acc24c45c1c2a2))
- **release:** 2.38.2 ([db909e6](https://10.189.120.23/crm/ucrm-fe/commit/db909e63b90c9a308c79cafd58c439d26a1477ac))
- **release:** 2.39.0 ([deca6c8](https://10.189.120.23/crm/ucrm-fe/commit/deca6c8f1d3a0c740901c970a8b17e5a94f32526))

## [2.39.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.38.2...v2.39.0) (2023-03-16)

### Features

- [tronglv-feature] tronglv feature share custom view ([3e433f0](https://10.189.120.23/crm/ucrm-fe/commit/3e433f02799a61e8dc3d679723c5ee22d3bb21bf))

### Bug Fixes

- [IC Chat Widget] Add show file type when upload ([96504e6](https://10.189.120.23/crm/ucrm-fe/commit/96504e6ddc615e1feebae434e9479815931dddcd))
- [tronglv-feature] form setting - done 80% ([0d87b14](https://10.189.120.23/crm/ucrm-fe/commit/0d87b14c2b2d806d0026f84e7e1c9b0a67dc58af))
- [tronglv-feature] form view build test ([959b5d2](https://10.189.120.23/crm/ucrm-fe/commit/959b5d2124457bcca69c93fc0d3ddbca71113f53))
- [tronglv-feature] internet form progress ([30796cd](https://10.189.120.23/crm/ucrm-fe/commit/30796cdcf30e8ed3009d5676b93fcce25079f4b1))
- [tronglv-feature] share custom view ([11d8348](https://10.189.120.23/crm/ucrm-fe/commit/11d83489b9a15b29378f8bb1eb1bc9664ef0e232))
- [tronglv-feature] share custom view progress ([2a7e116](https://10.189.120.23/crm/ucrm-fe/commit/2a7e11663998ca7d4d74861f666990365033e0d0))
- [tronglv-fix] ad form link ([ba5ae36](https://10.189.120.23/crm/ucrm-fe/commit/ba5ae3626f76757f6e4d2a79f25b61204a378831))
- [tronglv-fix] fix bug 1318 1321 ([43fdae2](https://10.189.120.23/crm/ucrm-fe/commit/43fdae2b673e74f4e4db03ae233bf5237a0815df))
- [tronglv-fix] fix sonaquebe ([c73e4cf](https://10.189.120.23/crm/ucrm-fe/commit/c73e4cf70ab65694c181ad100329c93b76db3f6b))
- [tronglv-test] test donem ([31d1d6a](https://10.189.120.23/crm/ucrm-fe/commit/31d1d6a4bf8961ad233b46534459cb96e77ee860))
- [tronglv-test] test form ([1a9afaa](https://10.189.120.23/crm/ucrm-fe/commit/1a9afaacacd6acc5074d3cca56fbb4b3a29d0984))
- [tronglv-test] test internet form ([18030cc](https://10.189.120.23/crm/ucrm-fe/commit/18030cc9cef0611957fa35d56005c7a4f5b630a6))
- [tronglv-test] van trong test form ([5957ddb](https://10.189.120.23/crm/ucrm-fe/commit/5957ddb5baae2c3fe8ab5795e0a0b4bd957d1c8d))
- [tronglv-test] van trong test form setting ([482d4dd](https://10.189.120.23/crm/ucrm-fe/commit/482d4dde79e24ca54851949a29f9c7537fc9eb80))

### [2.38.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.38.1...v2.38.2) (2023-03-09)

### Bug Fixes

- [Interaction Email] Add body ([99a7ee9](https://10.189.120.23/crm/ucrm-fe/commit/99a7ee94474c4d04066274e77ac0a68f988c8f09))
- [TabComponent] Add drag ([4bf9ddc](https://10.189.120.23/crm/ucrm-fe/commit/4bf9ddcaf3307a8c7807726fba3b648bb390a516))
- [tronglv-fix] fix detail third party and with detail ([9969f82](https://10.189.120.23/crm/ucrm-fe/commit/9969f820c4be154ca6827b7d2693990e8ad6367d))
- [tronglv-fix] update with detail and detail third party ([9b04d3e](https://10.189.120.23/crm/ucrm-fe/commit/9b04d3ee91324f6f0daea05b0c0724ec422520f0))
- fix duplicate rule ([32fb53d](https://10.189.120.23/crm/ucrm-fe/commit/32fb53de82070dfa422b67f7b770cc2adfe49ced))

### [2.29.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.29.0...v2.29.1) (2022-12-13)

## [2.29.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.28.0...v2.29.0) (2022-12-13)

### [2.27.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.27.0...v2.27.1) (2022-11-29)

### [2.26.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.25.0...v2.26.1) (2022-11-22)

## [2.25.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.24.1...v2.25.0) (2022-11-15)

### [2.38.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.38.0...v2.38.1) (2023-03-06)

### Bug Fixes

- [IC Chat Widget] Add loading when upload file; Send comment, file viber ([459dacc](https://10.189.120.23/crm/ucrm-fe/commit/459daccfdc064b7dba96757e9fbc894ce0dd9115))
- [IC Chat Widget] Add send voice ([e2e896c](https://10.189.120.23/crm/ucrm-fe/commit/e2e896c4625b1e5b65c8ca607b2582efa85888c3))
- [IC Chat Widget] Add type comment ([337d12b](https://10.189.120.23/crm/ucrm-fe/commit/337d12bd570c043bda02163fbc1ce10b8b93252e))
- [IC Chat Widget] Add type email and comment ([9776bf7](https://10.189.120.23/crm/ucrm-fe/commit/9776bf743e239322f8652df8f0eb164a28d637a8))
- [tronglv-feature] form setting ([f6f5b50](https://10.189.120.23/crm/ucrm-fe/commit/f6f5b503c471b5da2ad8698a59061af846ea281f))
- [tronglv-feature] form setting progress ([1e99a79](https://10.189.120.23/crm/ucrm-fe/commit/1e99a798d9d18b4b55a41a026532f5e2dad56ff1))
- [tronglv-feature] form setting progress ([064c61f](https://10.189.120.23/crm/ucrm-fe/commit/064c61f5e0b466672c3fe0caa60e7d09342294ab))
- [tronglv-fix] fix truncate knowledge base ([75c1c30](https://10.189.120.23/crm/ucrm-fe/commit/75c1c308740d47bcaafcde0045a99fd692d338f0))
- fix dynamic field for multi column detail ([e6049bc](https://10.189.120.23/crm/ucrm-fe/commit/e6049bc4d0b84f7cfd5d7a698412ebc0a15f389a))
- **list view with detail component:** list view with detail component ([b9bf13d](https://10.189.120.23/crm/ucrm-fe/commit/b9bf13d16d37808ffe68df5da575954064cbbaed))

## [2.38.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.34.0...v2.38.0) (2023-02-23)

### Features

- [AgentMonitor] Done; [MenuSettings] Add new menu ([5900f94](https://10.189.120.23/crm/ucrm-fe/commit/5900f9495ef3aeeaaf8aa0b6bc29c4167e310a6a))
- [ApprovalProcesses] Done ([bc0bde7](https://10.189.120.23/crm/ucrm-fe/commit/bc0bde7dc05a3a12a099c519eaaa913041052ee6))
- [ApprovalProcesses] Inprogress ([8ba865b](https://10.189.120.23/crm/ucrm-fe/commit/8ba865ba9f0ed74b416108b1187283ec633c3610))
- [Expose API Upload/Delete/View] In progress ([43cdb30](https://10.189.120.23/crm/ucrm-fe/commit/43cdb300fef05a5b562805d4b87f4f4af309c637))
- [IC Chat Widget] Done phase 1 ([7fce9f7](https://10.189.120.23/crm/ucrm-fe/commit/7fce9f73aeac4800e22716d34bda63dd12ccaa74))
- [IC Chat] Inprogress ([acea04d](https://10.189.120.23/crm/ucrm-fe/commit/acea04d04877ffb1783aad214fca92d1eba4dbbd))
- [IC Chat] Inprogress ([fc353ae](https://10.189.120.23/crm/ucrm-fe/commit/fc353ae3d6bebb32066f599a4e56ac5c4dd78d2f))
- [tronglv-feature] setting page title in consolidated view ([cd8374a](https://10.189.120.23/crm/ucrm-fe/commit/cd8374a3920c1ef2cab87f276f51466f857fd6f6))
- dynamic in detail component vs new comp vs fixed component ([c75105a](https://10.189.120.23/crm/ucrm-fe/commit/c75105a575ec943ad12574c8adef509fabeb80b6))

### Bug Fixes

- [Action workflow] Add today +- n ([4cffdb2](https://10.189.120.23/crm/ucrm-fe/commit/4cffdb2f3a68a6f5d3f4974e398d257e881e989a))
- [Agent Monitor] Get init status CRM ([5afbc2a](https://10.189.120.23/crm/ucrm-fe/commit/5afbc2ac88e4471a35c24ca38b01a60a988deab4))
- [Agent Monitor] Sync status and remove when logout ([e8b58e1](https://10.189.120.23/crm/ucrm-fe/commit/e8b58e119eea6876b0ac17db753715ea6bd9f7d8))
- [Approval Processes] Depend field text ([0b64d05](https://10.189.120.23/crm/ucrm-fe/commit/0b64d0576314bdda6282dd82e4f6f8c9377b5133))
- [Callcenter] Add search global, add object click to call; [VBEE] Add outgoing type ([0b39a61](https://10.189.120.23/crm/ucrm-fe/commit/0b39a61fcf16fa6b5f37106ceacaad8dc2675cc3))
- [Criteria Sharing; Report] Add decimal separator ([9c0c465](https://10.189.120.23/crm/ucrm-fe/commit/9c0c465798409438cd762ce1640c5e8953af8d9f))
- [Email] Get email after send email draft ([9876073](https://10.189.120.23/crm/ucrm-fe/commit/9876073bc4abe4a59e4a8f29d8a919e706595fe9))
- [Email] remove attach file ([02c9d2d](https://10.189.120.23/crm/ucrm-fe/commit/02c9d2d9b6caeb8e49fffc9445d590f2ebd4cb98))
- [ExposeAPI] 500 ([7c67f76](https://10.189.120.23/crm/ucrm-fe/commit/7c67f76d1f8ca2448a15bcbf8e44a0ae60bfa03f))
- [External app integration] disabled autocomplete pass ([e77282f](https://10.189.120.23/crm/ucrm-fe/commit/e77282fc1b45bee14c8536a59875db1b1b14f998))
- [External app] Add new content type ([8149036](https://10.189.120.23/crm/ucrm-fe/commit/814903614d43fc08eb238333f7d26fff1d821b68))
- [Finesse] add default hotline ([fa7e358](https://10.189.120.23/crm/ucrm-fe/commit/fa7e35834531eb918be4af11bdb79282839ace28))
- [form-setting] config form ([dc50b39](https://10.189.120.23/crm/ucrm-fe/commit/dc50b39a00dcaa3609a7f6810fc8d22f83f43a70))
- [IC Chat Widget] remove localstorage IC when logout; push message warning ([5cf471a](https://10.189.120.23/crm/ucrm-fe/commit/5cf471ad40b746bce5485ffd863d177eaaab6e88))
- [IC Chat Widget] Tag, responsive, admin; [Email] Remove file ([6bb12e4](https://10.189.120.23/crm/ucrm-fe/commit/6bb12e441fbfd7356ad930bf7766d065073c08b9))
- [IC] Conflict; Avatar header ([5bf6b9f](https://10.189.120.23/crm/ucrm-fe/commit/5bf6b9f50b4977c1d409610e972e32b4ea9a89af))
- [Interaction setting] add data to form ([511c987](https://10.189.120.23/crm/ucrm-fe/commit/511c98748e9858cda5ccf01afe45ec9ae92a2601))
- [Password3rd] OTP Finesse, CTI, Voicebiometric, Internal app integration ([71ef247](https://10.189.120.23/crm/ucrm-fe/commit/71ef247708b9bee49805320e4fd3287e49315776))
- [Report] Add number with decimal separator ([9a098fb](https://10.189.120.23/crm/ucrm-fe/commit/9a098fb56807872a52938481335fade5e1df5990))
- [Report] Format number; [Object fields] Validate decimal separator ([d29223d](https://10.189.120.23/crm/ucrm-fe/commit/d29223dd884bc2071534f0bf23b5ed0847ae811d))
- [SLA] Allow to set Success Milestone to a datetime field of a record which is matched SLA rule ([47b4c14](https://10.189.120.23/crm/ucrm-fe/commit/47b4c140195f592ed6d2e380c16bf36a7c93db07))
- [trong-feature] commit code form setting ([93ce067](https://10.189.120.23/crm/ucrm-fe/commit/93ce06794c3a7509cc024473244019680e22d439))
- [tronglv-feature] form setting progress ([1a1f691](https://10.189.120.23/crm/ucrm-fe/commit/1a1f69135c244dae2d0d869e1b741ca883f1cd64))
- [tronglv-feature] form setting push code test ([53fc98c](https://10.189.120.23/crm/ucrm-fe/commit/53fc98c10a2ea699ef7fa82cf8ba5c6ad7e20ad0))
- [tronglv-feature] form setting UI finish ([e1b7f53](https://10.189.120.23/crm/ucrm-fe/commit/e1b7f53d428fcdd92f706be58202ecfe98a133ea))
- [tronglv-feature] form-setting progress - setting data ([f1f1de2](https://10.189.120.23/crm/ucrm-fe/commit/f1f1de2d7a500aa416f83e29cf21c1543195d508))
- [tronglv-feature] global-search setting done ([54ab50e](https://10.189.120.23/crm/ucrm-fe/commit/54ab50e0b0f91830fcc9d81742ec931b72e018a6))
- [tronglv-feature] global-search setting done ([ba8b290](https://10.189.120.23/crm/ucrm-fe/commit/ba8b290febfec6e1ca4f399f73bd8ad079e6e84a))
- [tronglv-feature] search global in list view ([1c138f7](https://10.189.120.23/crm/ucrm-fe/commit/1c138f7746055650120cc82ea4bba8ba47f27a8a))
- [tronglv-feature] search global in list view ([7d67750](https://10.189.120.23/crm/ucrm-fe/commit/7d67750816a06644362529fa87183ba7ca2a3d8c))
- [tronglv-fix] ([3c5a83c](https://10.189.120.23/crm/ucrm-fe/commit/3c5a83cccb1febd4e0abafa93973d58f0200c2c1))
- [tronglv-fix] approval sms done ([644b154](https://10.189.120.23/crm/ucrm-fe/commit/644b15499fd9e4c015302e8607e70a752fd8c864))
- [tronglv-fix] display knowledge base ([c29f136](https://10.189.120.23/crm/ucrm-fe/commit/c29f13689646d73c4968da7a9d46b19bb2d1f11d))
- [tronglv-fix] fix ([2e76e79](https://10.189.120.23/crm/ucrm-fe/commit/2e76e7987aeb632b99e3628dc1412610064bf4e2))
- [tronglv-fix] fix 1220 ([1b31311](https://10.189.120.23/crm/ucrm-fe/commit/1b31311c472e2fc1f35638ee0d22120c34f4ce46))
- [tronglv-fix] fix 1220 ([664366e](https://10.189.120.23/crm/ucrm-fe/commit/664366e2b3c75b01eef7f3d1d68a141d2415ed08))
- [tronglv-fix] fix bug pagination ([f9b0cf9](https://10.189.120.23/crm/ucrm-fe/commit/f9b0cf9a8e9cb98ea7726a08f9166f31adf4d874))
- [tronglv-fix] fix build code ([3638899](https://10.189.120.23/crm/ucrm-fe/commit/36388999f1b69a8d4f9adea8ab8dc2552545d579))
- [tronglv-fix] fix component sms ([dc67392](https://10.189.120.23/crm/ucrm-fe/commit/dc673928101018b78209a4f60f91610fe6fa23a4))
- [tronglv-fix] fix conflict ([480f6b3](https://10.189.120.23/crm/ucrm-fe/commit/480f6b32091a4e78e88cde296a2ff259d7ce5844))
- [tronglv-fix] fix conflict ([bcfb195](https://10.189.120.23/crm/ucrm-fe/commit/bcfb1958d03d553a8d2a2dfd9e9e987d1e2cdf0b))
- [tronglv-fix] fix conflict ([4c8bbbb](https://10.189.120.23/crm/ucrm-fe/commit/4c8bbbbc7e9abc36f2e93fb0f201ef5a360f6c2a))
- [tronglv-fix] fix disable field id ([a11ae54](https://10.189.120.23/crm/ucrm-fe/commit/a11ae5407c0d31144ef863a6aa2ef7fdb434620f))
- [tronglv-fix] fix dislike in knowledge base, format number in kanban view ([054fbb4](https://10.189.120.23/crm/ucrm-fe/commit/054fbb473d1773d504b474305ebb63add76f8a2f))
- [tronglv-fix] fix global search ([7d24b65](https://10.189.120.23/crm/ucrm-fe/commit/7d24b65fbe500bf3ed8ff3b85b912276b402f0f6))
- [tronglv-fix] fix global search - done ([c891bc7](https://10.189.120.23/crm/ucrm-fe/commit/c891bc78d76dff887ffd920c57aea94125b71b47))
- [tronglv-fix] fix global search in lookup and linking ([aa18cde](https://10.189.120.23/crm/ucrm-fe/commit/aa18cde2da4f631816eb3d9540fada950b053f95))
- [tronglv-fix] fix icon ([1a2e064](https://10.189.120.23/crm/ucrm-fe/commit/1a2e06466986677f0cfbee76f2f72ffe934297a2))
- [tronglv-fix] fix index modal record ([a8b5f79](https://10.189.120.23/crm/ucrm-fe/commit/a8b5f794cc8ff8cedcc08e5c277e72794c208110))
- [tronglv-fix] fix knowledge base ([bc32e72](https://10.189.120.23/crm/ucrm-fe/commit/bc32e7290ccf5965f0db6060643c7fbf74fa8b56))
- [tronglv-fix] fix knowledge base ([095ceff](https://10.189.120.23/crm/ucrm-fe/commit/095ceffdec816d2278190bb17ce728cfcee4c887))
- [tronglv-fix] fix number attach ment ([de85d75](https://10.189.120.23/crm/ucrm-fe/commit/de85d759b9e0aa5f7902f67ab1f1173aa2826fa0))
- [tronglv-fix] fix page title ([eb86bb6](https://10.189.120.23/crm/ucrm-fe/commit/eb86bb631e3100e2b73bae0bdd4ac3a323455332))
- [tronglv-fix] fix page title in consolidated view ([8cc40b8](https://10.189.120.23/crm/ucrm-fe/commit/8cc40b830a93a63ffe2f5455f77d33391f8577c9))
- [tronglv-fix] fix related object ([bbba184](https://10.189.120.23/crm/ucrm-fe/commit/bbba1846884f10a173ba6c900fc2a78f576a1f6e))
- [tronglv-fix] fix sms ([125dd3f](https://10.189.120.23/crm/ucrm-fe/commit/125dd3f289bef52fef2d8b7ef4e87bc12509e23a))
- [tronglv-fix] fix space in consolidated view ([f507f8e](https://10.189.120.23/crm/ucrm-fe/commit/f507f8e53a572445bb3ca1a4ba93d8e7661e0173))
- [tronglv-fix] hidden page title ([32236ab](https://10.189.120.23/crm/ucrm-fe/commit/32236ab93220696936f519dbbdce7ab3ba5129bb))
- [tronglv-fix] pull code ([d387026](https://10.189.120.23/crm/ucrm-fe/commit/d387026b747b75a7a097b7551f4c6fbf1cd3aaa9))
- [tronglv-fix] update assignment rule ([abbcafd](https://10.189.120.23/crm/ucrm-fe/commit/abbcafd351c1909f945304671dd7690553989b08))
- [tronglv-improvement] improvement jira 1150, 1151, 1155 ([5328220](https://10.189.120.23/crm/ucrm-fe/commit/532822015177f0b69f7de46a1dc63fc51e1f53b6))
- [tronglv-improvement] improvement jira 1152, 1153 ([c1f3f2e](https://10.189.120.23/crm/ucrm-fe/commit/c1f3f2eebd65229b5956c5f3bfd306be2009d510))
- [tronglv-search] done ([adba236](https://10.189.120.23/crm/ucrm-fe/commit/adba236486f5ffb0c25314028fd2aa875051dec3))
- [tronglv-search] progress ([edca84b](https://10.189.120.23/crm/ucrm-fe/commit/edca84b88c49e2b880788ff8283cc9ad2f4f8040))
- [tronglv-search] progress ([38a5cba](https://10.189.120.23/crm/ucrm-fe/commit/38a5cbac19be359a260559fadae28e405fc453e8))
- [tronglv-searchManagement] push code BE test ([584111c](https://10.189.120.23/crm/ucrm-fe/commit/584111cfd03beed4473cbe5b7c95bb72288c8f13))
- [tronglv-searchManagement] push code BE test ([fd6483b](https://10.189.120.23/crm/ucrm-fe/commit/fd6483bff57961baeb954622f94abcc8ff638626))
- [tronglv-test] test build ([ec19a8f](https://10.189.120.23/crm/ucrm-fe/commit/ec19a8f94ad87310a226f7ee904bc3cf6319a1e4))
- [tronglv-test] test build ([a84107b](https://10.189.120.23/crm/ucrm-fe/commit/a84107bdd2b9b4ca121ec9b013e5ce81da3eabee))
- [tronglv-update] display prefix in consolidated view ([b1c03a4](https://10.189.120.23/crm/ucrm-fe/commit/b1c03a4b32474d4c80bbf0f603854e22602b055a))
- [tronglv-update] hidden input search in list view, lookup, linking ([5c8a333](https://10.189.120.23/crm/ucrm-fe/commit/5c8a33382f67b3d849024a073dd57e7d02a70b3f))
- [tronglv-update] pre index and compare in knowledge base ([4ae969a](https://10.189.120.23/crm/ucrm-fe/commit/4ae969a906320d8928ee55272967dfa7f89fccb5))
- [tronlv-fix] fix conflict ([ee77463](https://10.189.120.23/crm/ucrm-fe/commit/ee7746305e1f8a7dee58a49277826a1af1d5325a))
- add key interaction settings ([1d071be](https://10.189.120.23/crm/ucrm-fe/commit/1d071be381452939543615d2f287334cf0735b3c))
- add lib react-player ([3d285da](https://10.189.120.23/crm/ucrm-fe/commit/3d285da10c418919cff189689fc9939cb8532499))
- add new timeline ([2654ea0](https://10.189.120.23/crm/ucrm-fe/commit/2654ea017ff7f52939233fe64d62f5f8c41aa696))
- add special character ([1346165](https://10.189.120.23/crm/ucrm-fe/commit/13461650c89d81807e423e5122079ccc44aa16ab))
- add token external ([4e28e42](https://10.189.120.23/crm/ucrm-fe/commit/4e28e42c13394a68da666e1e9ac00bd831a4632c))
- change env ([33c1647](https://10.189.120.23/crm/ucrm-fe/commit/33c16479c73f12d1063b9e9855b2d770a6e178d8))
- conflicts ([0f0aa29](https://10.189.120.23/crm/ucrm-fe/commit/0f0aa29e917d14b2017a05a688e629ffff75759c))
- constants file ([5505d53](https://10.189.120.23/crm/ucrm-fe/commit/5505d53fc45c9b90fbfb354067365907ed686254))
- fix assign to in detail comp, add new feature in modal tenant ([cc5090c](https://10.189.120.23/crm/ucrm-fe/commit/cc5090c69d3b1997ddf30cf03b1ef170438245b4))
- fix comp detail profile for table and tag ([5393479](https://10.189.120.23/crm/ucrm-fe/commit/5393479e746624d2b452f2c846fccfb08203f36f))
- fix hightlight ([43acedc](https://10.189.120.23/crm/ucrm-fe/commit/43acedcbcc10c160b55c9e2db038f58c0f1b45f9))
- fix search in select, fix linking in detail ([1eadff8](https://10.189.120.23/crm/ucrm-fe/commit/1eadff8d242aaf289cdf56e6b8e02ba2aabbbd03))
- fixed component and new detail ([b840f91](https://10.189.120.23/crm/ucrm-fe/commit/b840f919a33bb5ac0d66249117fb1d19f13b8b26))
- hide agent monitor ([de0be26](https://10.189.120.23/crm/ucrm-fe/commit/de0be268a3d1a706af8115b850e93761c78f7c66))
- Hide reopen SLA ([0d86657](https://10.189.120.23/crm/ucrm-fe/commit/0d866579b51f9180bb19d60a5cfaf0bf87623ce8))
- hot fix search ([c342474](https://10.189.120.23/crm/ucrm-fe/commit/c34247428a815b573a29ccf7ff034b9bf3ceec39))
- interaction setting ([436f855](https://10.189.120.23/crm/ucrm-fe/commit/436f85567286ad2f00d0b45220b17840494c5091))
- logout ([1b078a2](https://10.189.120.23/crm/ucrm-fe/commit/1b078a23f4be9dff2e91197195257267064ea105))
- optimmize search in linking list ([dcfeda8](https://10.189.120.23/crm/ucrm-fe/commit/dcfeda8e5bc0bf40cbcef351a287d0570b760155))
- option number ([5c18be0](https://10.189.120.23/crm/ucrm-fe/commit/5c18be0dbc4fc961482e12c597abc61f58c5a311))
- prevent click edit thirdparty comp ([51f0504](https://10.189.120.23/crm/ucrm-fe/commit/51f0504c7104db56e550a55a8887be0968d612a4))
- refresh token ([b23b922](https://10.189.120.23/crm/ucrm-fe/commit/b23b9224d49361a6d73848ac4336476fd1cfb5fe))
- remove key interaction ([8f38862](https://10.189.120.23/crm/ucrm-fe/commit/8f3886272104baddd65f2cfffb4632825a1c6b16))
- tronglv-fix ([3dbb303](https://10.189.120.23/crm/ucrm-fe/commit/3dbb303feecd21618ab488eed626242fac1da69a))
- tronglv-fix fix code push ([0eda4e2](https://10.189.120.23/crm/ucrm-fe/commit/0eda4e2680257487cc07d24ab17d04fc8d843137))
- unhide status user ([0d6cf03](https://10.189.120.23/crm/ucrm-fe/commit/0d6cf03d5171ccf0fa4a4119af73e69e53e59004))

### Others

- **release:** 2.35.0 ([cfe888c](https://10.189.120.23/crm/ucrm-fe/commit/cfe888c4d15b17db517207a0578acac70db08391))
- **release:** 2.36.0 ([a5314c2](https://10.189.120.23/crm/ucrm-fe/commit/a5314c2da9a8d823c94a96b08c08d0f36eee2416))
- **release:** 2.37.0 ([d02b69f](https://10.189.120.23/crm/ucrm-fe/commit/d02b69f4d3405c2fc4eff91f3d05fa3c839cdbf2))

## [2.37.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.36.0...v2.37.0) (2023-02-17)

### Features

- [tronglv-feature] setting page title in consolidated view ([cd8374a](https://10.189.120.23/crm/ucrm-fe/commit/cd8374a3920c1ef2cab87f276f51466f857fd6f6))

### Bug Fixes

- [IC] Conflict; Avatar header ([5bf6b9f](https://10.189.120.23/crm/ucrm-fe/commit/5bf6b9f50b4977c1d409610e972e32b4ea9a89af))
- [Report] Format number; [Object fields] Validate decimal separator ([d29223d](https://10.189.120.23/crm/ucrm-fe/commit/d29223dd884bc2071534f0bf23b5ed0847ae811d))
- [tronglv-fix] fix disable field id ([a11ae54](https://10.189.120.23/crm/ucrm-fe/commit/a11ae5407c0d31144ef863a6aa2ef7fdb434620f))
- [tronglv-fix] fix page title ([eb86bb6](https://10.189.120.23/crm/ucrm-fe/commit/eb86bb631e3100e2b73bae0bdd4ac3a323455332))
- [tronglv-fix] fix page title in consolidated view ([8cc40b8](https://10.189.120.23/crm/ucrm-fe/commit/8cc40b830a93a63ffe2f5455f77d33391f8577c9))
- [tronglv-fix] fix space in consolidated view ([f507f8e](https://10.189.120.23/crm/ucrm-fe/commit/f507f8e53a572445bb3ca1a4ba93d8e7661e0173))
- [tronglv-fix] hidden page title ([32236ab](https://10.189.120.23/crm/ucrm-fe/commit/32236ab93220696936f519dbbdce7ab3ba5129bb))
- [tronglv-update] hidden input search in list view, lookup, linking ([5c8a333](https://10.189.120.23/crm/ucrm-fe/commit/5c8a33382f67b3d849024a073dd57e7d02a70b3f))
- unhide status user ([0d6cf03](https://10.189.120.23/crm/ucrm-fe/commit/0d6cf03d5171ccf0fa4a4119af73e69e53e59004))

## [2.36.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.35.0...v2.36.0) (2023-02-15)

### Features

- [ApprovalProcesses] Done ([bc0bde7](https://10.189.120.23/crm/ucrm-fe/commit/bc0bde7dc05a3a12a099c519eaaa913041052ee6))
- [ApprovalProcesses] Inprogress ([8ba865b](https://10.189.120.23/crm/ucrm-fe/commit/8ba865ba9f0ed74b416108b1187283ec633c3610))
- [IC Chat Widget] Done phase 1 ([7fce9f7](https://10.189.120.23/crm/ucrm-fe/commit/7fce9f73aeac4800e22716d34bda63dd12ccaa74))
- [IC Chat] Inprogress ([acea04d](https://10.189.120.23/crm/ucrm-fe/commit/acea04d04877ffb1783aad214fca92d1eba4dbbd))
- [IC Chat] Inprogress ([fc353ae](https://10.189.120.23/crm/ucrm-fe/commit/fc353ae3d6bebb32066f599a4e56ac5c4dd78d2f))

### Bug Fixes

- [Action workflow] Add today +- n ([4cffdb2](https://10.189.120.23/crm/ucrm-fe/commit/4cffdb2f3a68a6f5d3f4974e398d257e881e989a))
- [Approval Processes] Depend field text ([0b64d05](https://10.189.120.23/crm/ucrm-fe/commit/0b64d0576314bdda6282dd82e4f6f8c9377b5133))
- [Criteria Sharing; Report] Add decimal separator ([9c0c465](https://10.189.120.23/crm/ucrm-fe/commit/9c0c465798409438cd762ce1640c5e8953af8d9f))
- [Email] Get email after send email draft ([9876073](https://10.189.120.23/crm/ucrm-fe/commit/9876073bc4abe4a59e4a8f29d8a919e706595fe9))
- [Email] remove attach file ([02c9d2d](https://10.189.120.23/crm/ucrm-fe/commit/02c9d2d9b6caeb8e49fffc9445d590f2ebd4cb98))
- [External app] Add new content type ([8149036](https://10.189.120.23/crm/ucrm-fe/commit/814903614d43fc08eb238333f7d26fff1d821b68))
- [Finesse] add default hotline ([fa7e358](https://10.189.120.23/crm/ucrm-fe/commit/fa7e35834531eb918be4af11bdb79282839ace28))
- [form-setting] config form ([dc50b39](https://10.189.120.23/crm/ucrm-fe/commit/dc50b39a00dcaa3609a7f6810fc8d22f83f43a70))
- [IC Chat Widget] remove localstorage IC when logout; push message warning ([5cf471a](https://10.189.120.23/crm/ucrm-fe/commit/5cf471ad40b746bce5485ffd863d177eaaab6e88))
- [IC Chat Widget] Tag, responsive, admin; [Email] Remove file ([6bb12e4](https://10.189.120.23/crm/ucrm-fe/commit/6bb12e441fbfd7356ad930bf7766d065073c08b9))
- [Interaction setting] add data to form ([511c987](https://10.189.120.23/crm/ucrm-fe/commit/511c98748e9858cda5ccf01afe45ec9ae92a2601))
- [Report] Add number with decimal separator ([9a098fb](https://10.189.120.23/crm/ucrm-fe/commit/9a098fb56807872a52938481335fade5e1df5990))
- [trong-feature] commit code form setting ([93ce067](https://10.189.120.23/crm/ucrm-fe/commit/93ce06794c3a7509cc024473244019680e22d439))
- [tronglv-feature] form setting progress ([1a1f691](https://10.189.120.23/crm/ucrm-fe/commit/1a1f69135c244dae2d0d869e1b741ca883f1cd64))
- [tronglv-feature] form setting push code test ([53fc98c](https://10.189.120.23/crm/ucrm-fe/commit/53fc98c10a2ea699ef7fa82cf8ba5c6ad7e20ad0))
- [tronglv-feature] form setting UI finish ([e1b7f53](https://10.189.120.23/crm/ucrm-fe/commit/e1b7f53d428fcdd92f706be58202ecfe98a133ea))
- [tronglv-feature] form-setting progress - setting data ([f1f1de2](https://10.189.120.23/crm/ucrm-fe/commit/f1f1de2d7a500aa416f83e29cf21c1543195d508))
- [tronglv-feature] global-search setting done ([54ab50e](https://10.189.120.23/crm/ucrm-fe/commit/54ab50e0b0f91830fcc9d81742ec931b72e018a6))
- [tronglv-feature] global-search setting done ([ba8b290](https://10.189.120.23/crm/ucrm-fe/commit/ba8b290febfec6e1ca4f399f73bd8ad079e6e84a))
- [tronglv-feature] search global in list view ([1c138f7](https://10.189.120.23/crm/ucrm-fe/commit/1c138f7746055650120cc82ea4bba8ba47f27a8a))
- [tronglv-feature] search global in list view ([7d67750](https://10.189.120.23/crm/ucrm-fe/commit/7d67750816a06644362529fa87183ba7ca2a3d8c))
- [tronglv-fix] ([3c5a83c](https://10.189.120.23/crm/ucrm-fe/commit/3c5a83cccb1febd4e0abafa93973d58f0200c2c1))
- [tronglv-fix] approval sms done ([644b154](https://10.189.120.23/crm/ucrm-fe/commit/644b15499fd9e4c015302e8607e70a752fd8c864))
- [tronglv-fix] fix ([2e76e79](https://10.189.120.23/crm/ucrm-fe/commit/2e76e7987aeb632b99e3628dc1412610064bf4e2))
- [tronglv-fix] fix 1220 ([1b31311](https://10.189.120.23/crm/ucrm-fe/commit/1b31311c472e2fc1f35638ee0d22120c34f4ce46))
- [tronglv-fix] fix 1220 ([664366e](https://10.189.120.23/crm/ucrm-fe/commit/664366e2b3c75b01eef7f3d1d68a141d2415ed08))
- [tronglv-fix] fix bug pagination ([f9b0cf9](https://10.189.120.23/crm/ucrm-fe/commit/f9b0cf9a8e9cb98ea7726a08f9166f31adf4d874))
- [tronglv-fix] fix build code ([3638899](https://10.189.120.23/crm/ucrm-fe/commit/36388999f1b69a8d4f9adea8ab8dc2552545d579))
- [tronglv-fix] fix component sms ([dc67392](https://10.189.120.23/crm/ucrm-fe/commit/dc673928101018b78209a4f60f91610fe6fa23a4))
- [tronglv-fix] fix conflict ([480f6b3](https://10.189.120.23/crm/ucrm-fe/commit/480f6b32091a4e78e88cde296a2ff259d7ce5844))
- [tronglv-fix] fix conflict ([bcfb195](https://10.189.120.23/crm/ucrm-fe/commit/bcfb1958d03d553a8d2a2dfd9e9e987d1e2cdf0b))
- [tronglv-fix] fix conflict ([4c8bbbb](https://10.189.120.23/crm/ucrm-fe/commit/4c8bbbbc7e9abc36f2e93fb0f201ef5a360f6c2a))
- [tronglv-fix] fix dislike in knowledge base, format number in kanban view ([054fbb4](https://10.189.120.23/crm/ucrm-fe/commit/054fbb473d1773d504b474305ebb63add76f8a2f))
- [tronglv-fix] fix global search ([7d24b65](https://10.189.120.23/crm/ucrm-fe/commit/7d24b65fbe500bf3ed8ff3b85b912276b402f0f6))
- [tronglv-fix] fix global search - done ([c891bc7](https://10.189.120.23/crm/ucrm-fe/commit/c891bc78d76dff887ffd920c57aea94125b71b47))
- [tronglv-fix] fix global search in lookup and linking ([aa18cde](https://10.189.120.23/crm/ucrm-fe/commit/aa18cde2da4f631816eb3d9540fada950b053f95))
- [tronglv-fix] fix index modal record ([a8b5f79](https://10.189.120.23/crm/ucrm-fe/commit/a8b5f794cc8ff8cedcc08e5c277e72794c208110))
- [tronglv-fix] fix knowledge base ([bc32e72](https://10.189.120.23/crm/ucrm-fe/commit/bc32e7290ccf5965f0db6060643c7fbf74fa8b56))
- [tronglv-fix] fix knowledge base ([095ceff](https://10.189.120.23/crm/ucrm-fe/commit/095ceffdec816d2278190bb17ce728cfcee4c887))
- [tronglv-fix] fix number attach ment ([de85d75](https://10.189.120.23/crm/ucrm-fe/commit/de85d759b9e0aa5f7902f67ab1f1173aa2826fa0))
- [tronglv-fix] fix related object ([bbba184](https://10.189.120.23/crm/ucrm-fe/commit/bbba1846884f10a173ba6c900fc2a78f576a1f6e))
- [tronglv-fix] fix sms ([125dd3f](https://10.189.120.23/crm/ucrm-fe/commit/125dd3f289bef52fef2d8b7ef4e87bc12509e23a))
- [tronglv-fix] pull code ([d387026](https://10.189.120.23/crm/ucrm-fe/commit/d387026b747b75a7a097b7551f4c6fbf1cd3aaa9))
- [tronglv-improvement] improvement jira 1150, 1151, 1155 ([5328220](https://10.189.120.23/crm/ucrm-fe/commit/532822015177f0b69f7de46a1dc63fc51e1f53b6))
- [tronglv-improvement] improvement jira 1152, 1153 ([c1f3f2e](https://10.189.120.23/crm/ucrm-fe/commit/c1f3f2eebd65229b5956c5f3bfd306be2009d510))
- [tronglv-search] done ([adba236](https://10.189.120.23/crm/ucrm-fe/commit/adba236486f5ffb0c25314028fd2aa875051dec3))
- [tronglv-search] progress ([edca84b](https://10.189.120.23/crm/ucrm-fe/commit/edca84b88c49e2b880788ff8283cc9ad2f4f8040))
- [tronglv-search] progress ([38a5cba](https://10.189.120.23/crm/ucrm-fe/commit/38a5cbac19be359a260559fadae28e405fc453e8))
- [tronglv-searchManagement] push code BE test ([584111c](https://10.189.120.23/crm/ucrm-fe/commit/584111cfd03beed4473cbe5b7c95bb72288c8f13))
- [tronglv-searchManagement] push code BE test ([fd6483b](https://10.189.120.23/crm/ucrm-fe/commit/fd6483bff57961baeb954622f94abcc8ff638626))
- [tronglv-test] test build ([ec19a8f](https://10.189.120.23/crm/ucrm-fe/commit/ec19a8f94ad87310a226f7ee904bc3cf6319a1e4))
- [tronglv-test] test build ([a84107b](https://10.189.120.23/crm/ucrm-fe/commit/a84107bdd2b9b4ca121ec9b013e5ce81da3eabee))
- [tronlv-fix] fix conflict ([ee77463](https://10.189.120.23/crm/ucrm-fe/commit/ee7746305e1f8a7dee58a49277826a1af1d5325a))
- add key interaction settings ([1d071be](https://10.189.120.23/crm/ucrm-fe/commit/1d071be381452939543615d2f287334cf0735b3c))
- add lib react-player ([3d285da](https://10.189.120.23/crm/ucrm-fe/commit/3d285da10c418919cff189689fc9939cb8532499))
- add new timeline ([2654ea0](https://10.189.120.23/crm/ucrm-fe/commit/2654ea017ff7f52939233fe64d62f5f8c41aa696))
- add token external ([4e28e42](https://10.189.120.23/crm/ucrm-fe/commit/4e28e42c13394a68da666e1e9ac00bd831a4632c))
- conflicts ([0f0aa29](https://10.189.120.23/crm/ucrm-fe/commit/0f0aa29e917d14b2017a05a688e629ffff75759c))
- constants file ([5505d53](https://10.189.120.23/crm/ucrm-fe/commit/5505d53fc45c9b90fbfb354067365907ed686254))
- fix assign to in detail comp, add new feature in modal tenant ([cc5090c](https://10.189.120.23/crm/ucrm-fe/commit/cc5090c69d3b1997ddf30cf03b1ef170438245b4))
- fix search in select, fix linking in detail ([1eadff8](https://10.189.120.23/crm/ucrm-fe/commit/1eadff8d242aaf289cdf56e6b8e02ba2aabbbd03))
- interaction setting ([436f855](https://10.189.120.23/crm/ucrm-fe/commit/436f85567286ad2f00d0b45220b17840494c5091))
- logout ([1b078a2](https://10.189.120.23/crm/ucrm-fe/commit/1b078a23f4be9dff2e91197195257267064ea105))
- prevent click edit thirdparty comp ([51f0504](https://10.189.120.23/crm/ucrm-fe/commit/51f0504c7104db56e550a55a8887be0968d612a4))
- refresh token ([b23b922](https://10.189.120.23/crm/ucrm-fe/commit/b23b9224d49361a6d73848ac4336476fd1cfb5fe))
- remove key interaction ([8f38862](https://10.189.120.23/crm/ucrm-fe/commit/8f3886272104baddd65f2cfffb4632825a1c6b16))
- tronglv-fix ([3dbb303](https://10.189.120.23/crm/ucrm-fe/commit/3dbb303feecd21618ab488eed626242fac1da69a))
- tronglv-fix fix code push ([0eda4e2](https://10.189.120.23/crm/ucrm-fe/commit/0eda4e2680257487cc07d24ab17d04fc8d843137))

## [2.35.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.31.1...v2.35.0) (2023-01-17)

### Features

- [AgentMonitor] Done; [MenuSettings] Add new menu ([5900f94](https://10.189.120.23/crm/ucrm-fe/commit/5900f9495ef3aeeaaf8aa0b6bc29c4167e310a6a))
- [Expose API Upload/Delete/View] In progress ([43cdb30](https://10.189.120.23/crm/ucrm-fe/commit/43cdb300fef05a5b562805d4b87f4f4af309c637))

### Bug Fixes

- [Agent Monitor] Sync status and remove when logout ([e8b58e1](https://10.189.120.23/crm/ucrm-fe/commit/e8b58e119eea6876b0ac17db753715ea6bd9f7d8))
- [Duplicated rule] call API in listview ([ee94290](https://10.189.120.23/crm/ucrm-fe/commit/ee9429041726f093ce6edd9bd011510e7fb6e3b6))
- [ExposeAPI] 500 ([7c67f76](https://10.189.120.23/crm/ucrm-fe/commit/7c67f76d1f8ca2448a15bcbf8e44a0ae60bfa03f))
- [External app integration] disabled autocomplete pass ([e77282f](https://10.189.120.23/crm/ucrm-fe/commit/e77282fc1b45bee14c8536a59875db1b1b14f998))
- [Knowledge Base] Hiển thị bài viết liên quan không đúng trong Knowledge Base ([28e225c](https://10.189.120.23/crm/ucrm-fe/commit/28e225c67888a9b8c780fa94d42e5fb9410bb90c))
- [knowledge-base] add component comment in detail article ([6e5320f](https://10.189.120.23/crm/ucrm-fe/commit/6e5320f142f8af56da3c1fd413b9d65ac50fe889))
- [knowledge-base] add language knowledge base ([5491584](https://10.189.120.23/crm/ucrm-fe/commit/5491584b0e9944cf1621387538104da288d0ae6c))
- [knowledge-base] build test ([3508471](https://10.189.120.23/crm/ucrm-fe/commit/3508471184fde93e255f68a354cedb6331ef13b7))
- [Password3rd] OTP Finesse, CTI, Voicebiometric, Internal app integration ([71ef247](https://10.189.120.23/crm/ucrm-fe/commit/71ef247708b9bee49805320e4fd3287e49315776))
- [PopupContact] Add duplicate rule; Fix two new call popup ([7c60c18](https://10.189.120.23/crm/ucrm-fe/commit/7c60c18bf74f5c0f3d9b801e026f1d29b1ab6ef1))
- [PopupContact] duplicate with 3rd ([7999e5c](https://10.189.120.23/crm/ucrm-fe/commit/7999e5c06979e059404a45d194b78dff60fb8fde))
- [SLA] Allow to set Success Milestone to a datetime field of a record which is matched SLA rule ([47b4c14](https://10.189.120.23/crm/ucrm-fe/commit/47b4c140195f592ed6d2e380c16bf36a7c93db07))
- [trong-fix] hidden knowledge base enhancement ([f21c153](https://10.189.120.23/crm/ucrm-fe/commit/f21c1531cf575f803457c99376a31ff4a92abfcb))
- [tronglv-commit] commit code test code list view ([3ecdf52](https://10.189.120.23/crm/ucrm-fe/commit/3ecdf525abaadab324ceccb645e4caedb504dd28))
- [tronglv-feature] finish page edit and add article ([d49fcd9](https://10.189.120.23/crm/ucrm-fe/commit/d49fcd9f5165c9282ce429fef6dc2f0d932de96d))
- [tronglv-feature] knowledge consolidated ([3531b19](https://10.189.120.23/crm/ucrm-fe/commit/3531b1999a71945b6293057f913e7bf696faf659))
- [tronglv-feature] knowledge in consolidated done - begin test ([b18f7db](https://10.189.120.23/crm/ucrm-fe/commit/b18f7db852b9e638c0bff01cf1f79e7530239af3))
- [tronglv-feature] push code anh Vinh test ([6195d3b](https://10.189.120.23/crm/ucrm-fe/commit/6195d3b625492f7cb9c1d23f3d4d7e88226c5a8c))
- [tronglv-feature] update feature knowlege base and display manual sharing ([eeae037](https://10.189.120.23/crm/ucrm-fe/commit/eeae037739377509a7810b5693fd62a87a242ee0))
- [tronglv-fix] add api sync and test connect in knowledge base ([72dd8da](https://10.189.120.23/crm/ucrm-fe/commit/72dd8da4e3707f50b44d81af4bb53de8c421c323))
- [tronglv-fix] change date ([d047acb](https://10.189.120.23/crm/ucrm-fe/commit/d047acb69199c740814456101cbab0de33577563))
- [tronglv-fix] commit code ([e414413](https://10.189.120.23/crm/ucrm-fe/commit/e4144139a64f34dfbe6705aa73eb1c60e55ffa6a))
- [tronglv-fix] custom admin ([84afa3e](https://10.189.120.23/crm/ucrm-fe/commit/84afa3e8ce1fdbc35ff3274994552b252b7905f1))
- [tronglv-fix] custom admin knowledge base ([de406ba](https://10.189.120.23/crm/ucrm-fe/commit/de406ba08389e6c72f29ec7b349addf432d06e69))
- [tronglv-fix] custom admin knowledge base done ([49ef81d](https://10.189.120.23/crm/ucrm-fe/commit/49ef81deecf0f910a59614d363408b0bbb35d304))
- [tronglv-fix] display knowledge base ([c29f136](https://10.189.120.23/crm/ucrm-fe/commit/c29f13689646d73c4968da7a9d46b19bb2d1f11d))
- [tronglv-fix] fix bug 1066 - permission knowledge base ([2537f21](https://10.189.120.23/crm/ucrm-fe/commit/2537f21362defae525523a575d4257d7115a6268))
- [tronglv-fix] fix bug 1071 knowledge base ([d24aead](https://10.189.120.23/crm/ucrm-fe/commit/d24aead8364aa540c3b42bf924baacb98f9dc930))
- [tronglv-fix] fix bug knowledge ([56b7b50](https://10.189.120.23/crm/ucrm-fe/commit/56b7b50be104dfba20d03a8e943145828e61c32f))
- [tronglv-fix] fix bug knowledge base ([25eaa47](https://10.189.120.23/crm/ucrm-fe/commit/25eaa47dc4393d6971b411dd4f8c7cec86b47d3b))
- [tronglv-fix] fix bug parse html in knowledge base ([9a1d7c8](https://10.189.120.23/crm/ucrm-fe/commit/9a1d7c85915a0168cb6139dbe220eee59162750b))
- [tronglv-fix] fix bug search in knowledge base ([91614a1](https://10.189.120.23/crm/ucrm-fe/commit/91614a11a33db077491319f52fdbce7138edfb37))
- [tronglv-fix] fix conflict ([e5ca212](https://10.189.120.23/crm/ucrm-fe/commit/e5ca2121324b598eb85bf5bbf4fbd57466aee3c9))
- [tronglv-fix] fix icon ([1a2e064](https://10.189.120.23/crm/ucrm-fe/commit/1a2e06466986677f0cfbee76f2f72ffe934297a2))
- [tronglv-fix] fix plaintext search in knowledge base ([13069be](https://10.189.120.23/crm/ucrm-fe/commit/13069be1c65a1835db7686edf607c9618571eafe))
- [tronglv-fix] fix repalce in email, trim title in knowledge base ([7b852b3](https://10.189.120.23/crm/ucrm-fe/commit/7b852b3e9ef33771f7c1878d9970720691097b2a))
- [tronglv-fix] fix search with category id ([f6f2b2e](https://10.189.120.23/crm/ucrm-fe/commit/f6f2b2e389f6610114c80dcc015e31e74c6258ba))
- [tronglv-fix] hidden component knowledge base in consolidated view setting ([5b311ea](https://10.189.120.23/crm/ucrm-fe/commit/5b311ea7ca9003bee0c6fa395a616c9ad0c4c28a))
- [tronglv-fix] hidden knowledge base ([7e6f97d](https://10.189.120.23/crm/ucrm-fe/commit/7e6f97df89f73ae8dd8f4f12dd5bfad075d2b206))
- [tronglv-fix] hidden manual sharing ([ff113d4](https://10.189.120.23/crm/ucrm-fe/commit/ff113d430003b098dfd02771643dc1853a2e4002))
- [tronglv-fix] improment feature knowledge base ([b22afe6](https://10.189.120.23/crm/ucrm-fe/commit/b22afe6d20a06ef1cc146ae970209abf4d3d9a44))
- [tronglv-fix] update assignment rule ([abbcafd](https://10.189.120.23/crm/ucrm-fe/commit/abbcafd351c1909f945304671dd7690553989b08))
- [tronglv-fix] update language for knowledge base ([cab5d89](https://10.189.120.23/crm/ucrm-fe/commit/cab5d89bffc57e5088e476539a7068dc46e449e4))
- [tronglv-knowledge] feature search ([79429cd](https://10.189.120.23/crm/ucrm-fe/commit/79429cd2f14774031d0e21a834aa4be309b21634))
- [tronglv-setting] setting knowledge base in consolidated ([1b4c80e](https://10.189.120.23/crm/ucrm-fe/commit/1b4c80ee2688de89d9e5e342f258439be41e9a21))
- [tronglv-up conflict] push code conflict ([732262b](https://10.189.120.23/crm/ucrm-fe/commit/732262b191c349b7fcaa2ee263d4ce52131bd96b))
- [tronglv-update] add article ([eeb596b](https://10.189.120.23/crm/ucrm-fe/commit/eeb596b873a5f932bb46efaa8211c73a21a472c2))
- [tronglv-update] knowledge base demo ([628afc5](https://10.189.120.23/crm/ucrm-fe/commit/628afc5676b6060edd239612431ac1d19fe3089d))
- [tronglv-update] update modal setting knowledge base ([3badcdb](https://10.189.120.23/crm/ucrm-fe/commit/3badcdba83431d7a950d79768b964a5e818b895e))
- add C247 Standard CTI ([467acfb](https://10.189.120.23/crm/ucrm-fe/commit/467acfba6be5da5ff4ff49faa781d02ed7d19ec1))
- add special character ([1346165](https://10.189.120.23/crm/ucrm-fe/commit/13461650c89d81807e423e5122079ccc44aa16ab))
- change env ([33c1647](https://10.189.120.23/crm/ucrm-fe/commit/33c16479c73f12d1063b9e9855b2d770a6e178d8))
- create record call duplicate rule ([6ee8f7c](https://10.189.120.23/crm/ucrm-fe/commit/6ee8f7c1818afa55cb7543e7a774fa42f0e568d7))
- double click edit detail, UI SLA ([518affe](https://10.189.120.23/crm/ucrm-fe/commit/518affe89b9c3324728348a65ae31420865bb9d0))
- fix comp detail profile for table and tag ([5393479](https://10.189.120.23/crm/ucrm-fe/commit/5393479e746624d2b452f2c846fccfb08203f36f))
- fix hightlight ([43acedc](https://10.189.120.23/crm/ucrm-fe/commit/43acedcbcc10c160b55c9e2db038f58c0f1b45f9))
- fix role profile for consolidated view tag comp ([f2d9971](https://10.189.120.23/crm/ucrm-fe/commit/f2d9971e195c6e461c67c762d698902a67dd96ec))
- hide agent monitor ([de0be26](https://10.189.120.23/crm/ucrm-fe/commit/de0be268a3d1a706af8115b850e93761c78f7c66))
- Hide reopen SLA ([0d86657](https://10.189.120.23/crm/ucrm-fe/commit/0d866579b51f9180bb19d60a5cfaf0bf87623ce8))
- hot fix search ([c342474](https://10.189.120.23/crm/ucrm-fe/commit/c34247428a815b573a29ccf7ff034b9bf3ceec39))
- optimize search in listview and hot fix edit detail comp ([0b66790](https://10.189.120.23/crm/ucrm-fe/commit/0b667900090f8a498000f5a7cf145696dca4529b))
- optimmize search in linking list ([dcfeda8](https://10.189.120.23/crm/ucrm-fe/commit/dcfeda8e5bc0bf40cbcef351a287d0570b760155))
- update version 2.33.0 ([f9b815e](https://10.189.120.23/crm/ucrm-fe/commit/f9b815ea192dd75262d32c91e15ac321a99c37ac))

### Others

- **release:** 2.32.0 ([99ed093](https://10.189.120.23/crm/ucrm-fe/commit/99ed09365396e32549e7dfb423acbc1ab25d297b))
- **release:** 2.33.0 ([ab50bab](https://10.189.120.23/crm/ucrm-fe/commit/ab50bab443d2c009488c9e919eb28fad1a805528))
- **release:** 2.34.0 ([71916c4](https://10.189.120.23/crm/ucrm-fe/commit/71916c4fde1a066cd59545ace4e0e68f9bed0cf1))

## [2.34.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.30.1...v2.34.0) (2023-01-10)

### Features

- create template record ([73f0536](https://10.189.120.23/crm/ucrm-fe/commit/73f053621708bb1536f4df4aa66dfab1dfa1f567))

### Bug Fixes

- [Action workflow] Fix type linkingobject ([d513120](https://10.189.120.23/crm/ucrm-fe/commit/d513120fdfe4d24067d07c7c0226e6b4ca23ecf1))
- [Call] Run time; [Timeline,Call] Save linking; [URL,Call] Create record duplicate ([03d2ade](https://10.189.120.23/crm/ucrm-fe/commit/03d2ade7b37d5b10cbb12903e4101d53fa95a9fa))
- [Create record call] Hide modal duplicate ([31805f2](https://10.189.120.23/crm/ucrm-fe/commit/31805f24816285badaf43b347c02ded13b89bee4))
- [Create record] Add duplicated ([856d784](https://10.189.120.23/crm/ucrm-fe/commit/856d78492c9e6563f56beeb111ebb628e131895a))
- [Create record] Add modal duplicate ([092d9d8](https://10.189.120.23/crm/ucrm-fe/commit/092d9d87b01ec4c2b009a98174456248d8bcfc4b))
- [Duplicated rule] call API in listview ([ee94290](https://10.189.120.23/crm/ucrm-fe/commit/ee9429041726f093ce6edd9bd011510e7fb6e3b6))
- [Email Signature] Allow show signature before quote; [Action update source target] Hide related ([20ab39a](https://10.189.120.23/crm/ucrm-fe/commit/20ab39a071925aef68c8c2cff350aadd5e154f87))
- [Finesse] Hide password ([9d96903](https://10.189.120.23/crm/ucrm-fe/commit/9d96903445c05fef9ccd7350d88e5bbcdd8eb817))
- [Knowledge Base] Hiển thị bài viết liên quan không đúng trong Knowledge Base ([28e225c](https://10.189.120.23/crm/ucrm-fe/commit/28e225c67888a9b8c780fa94d42e5fb9410bb90c))
- [knowledge-base] add component comment in detail article ([6e5320f](https://10.189.120.23/crm/ucrm-fe/commit/6e5320f142f8af56da3c1fd413b9d65ac50fe889))
- [knowledge-base] add language knowledge base ([5491584](https://10.189.120.23/crm/ucrm-fe/commit/5491584b0e9944cf1621387538104da288d0ae6c))
- [knowledge-base] build test ([3508471](https://10.189.120.23/crm/ucrm-fe/commit/3508471184fde93e255f68a354cedb6331ef13b7))
- [PopupContact] Add duplicate rule; Fix two new call popup ([7c60c18](https://10.189.120.23/crm/ucrm-fe/commit/7c60c18bf74f5c0f3d9b801e026f1d29b1ab6ef1))
- [PopupContact] duplicate with 3rd ([7999e5c](https://10.189.120.23/crm/ucrm-fe/commit/7999e5c06979e059404a45d194b78dff60fb8fde))
- [trong-fix] hidden knowledge base enhancement ([f21c153](https://10.189.120.23/crm/ucrm-fe/commit/f21c1531cf575f803457c99376a31ff4a92abfcb))
- [tronglv-commit] commit code test code list view ([3ecdf52](https://10.189.120.23/crm/ucrm-fe/commit/3ecdf525abaadab324ceccb645e4caedb504dd28))
- [tronglv-feature] finish page edit and add article ([d49fcd9](https://10.189.120.23/crm/ucrm-fe/commit/d49fcd9f5165c9282ce429fef6dc2f0d932de96d))
- [tronglv-feature] knowledge consolidated ([3531b19](https://10.189.120.23/crm/ucrm-fe/commit/3531b1999a71945b6293057f913e7bf696faf659))
- [tronglv-feature] knowledge in consolidated done - begin test ([b18f7db](https://10.189.120.23/crm/ucrm-fe/commit/b18f7db852b9e638c0bff01cf1f79e7530239af3))
- [tronglv-feature] push code anh Vinh test ([6195d3b](https://10.189.120.23/crm/ucrm-fe/commit/6195d3b625492f7cb9c1d23f3d4d7e88226c5a8c))
- [tronglv-feature] update feature knowlege base and display manual sharing ([eeae037](https://10.189.120.23/crm/ucrm-fe/commit/eeae037739377509a7810b5693fd62a87a242ee0))
- [tronglv-fix] add api sync and test connect in knowledge base ([72dd8da](https://10.189.120.23/crm/ucrm-fe/commit/72dd8da4e3707f50b44d81af4bb53de8c421c323))
- [tronglv-fix] change date ([d047acb](https://10.189.120.23/crm/ucrm-fe/commit/d047acb69199c740814456101cbab0de33577563))
- [tronglv-fix] commit code ([e414413](https://10.189.120.23/crm/ucrm-fe/commit/e4144139a64f34dfbe6705aa73eb1c60e55ffa6a))
- [tronglv-fix] custom admin ([84afa3e](https://10.189.120.23/crm/ucrm-fe/commit/84afa3e8ce1fdbc35ff3274994552b252b7905f1))
- [tronglv-fix] custom admin knowledge base ([de406ba](https://10.189.120.23/crm/ucrm-fe/commit/de406ba08389e6c72f29ec7b349addf432d06e69))
- [tronglv-fix] custom admin knowledge base done ([49ef81d](https://10.189.120.23/crm/ucrm-fe/commit/49ef81deecf0f910a59614d363408b0bbb35d304))
- [tronglv-fix] fix bug 1066 - permission knowledge base ([2537f21](https://10.189.120.23/crm/ucrm-fe/commit/2537f21362defae525523a575d4257d7115a6268))
- [tronglv-fix] fix bug 1071 knowledge base ([d24aead](https://10.189.120.23/crm/ucrm-fe/commit/d24aead8364aa540c3b42bf924baacb98f9dc930))
- [tronglv-fix] fix bug knowledge ([56b7b50](https://10.189.120.23/crm/ucrm-fe/commit/56b7b50be104dfba20d03a8e943145828e61c32f))
- [tronglv-fix] fix bug knowledge base ([25eaa47](https://10.189.120.23/crm/ucrm-fe/commit/25eaa47dc4393d6971b411dd4f8c7cec86b47d3b))
- [tronglv-fix] fix bug parse html in knowledge base ([9a1d7c8](https://10.189.120.23/crm/ucrm-fe/commit/9a1d7c85915a0168cb6139dbe220eee59162750b))
- [tronglv-fix] fix bug search in knowledge base ([91614a1](https://10.189.120.23/crm/ucrm-fe/commit/91614a11a33db077491319f52fdbce7138edfb37))
- [tronglv-fix] fix conflict ([e5ca212](https://10.189.120.23/crm/ucrm-fe/commit/e5ca2121324b598eb85bf5bbf4fbd57466aee3c9))
- [tronglv-fix] fix plaintext search in knowledge base ([13069be](https://10.189.120.23/crm/ucrm-fe/commit/13069be1c65a1835db7686edf607c9618571eafe))
- [tronglv-fix] fix repalce in email, trim title in knowledge base ([7b852b3](https://10.189.120.23/crm/ucrm-fe/commit/7b852b3e9ef33771f7c1878d9970720691097b2a))
- [tronglv-fix] fix search with category id ([f6f2b2e](https://10.189.120.23/crm/ucrm-fe/commit/f6f2b2e389f6610114c80dcc015e31e74c6258ba))
- [tronglv-fix] hidden component knowledge base in consolidated view setting ([5b311ea](https://10.189.120.23/crm/ucrm-fe/commit/5b311ea7ca9003bee0c6fa395a616c9ad0c4c28a))
- [tronglv-fix] hidden knowledge base ([7e6f97d](https://10.189.120.23/crm/ucrm-fe/commit/7e6f97df89f73ae8dd8f4f12dd5bfad075d2b206))
- [tronglv-fix] hidden manual sharing ([ff113d4](https://10.189.120.23/crm/ucrm-fe/commit/ff113d430003b098dfd02771643dc1853a2e4002))
- [tronglv-fix] improment feature knowledge base ([b22afe6](https://10.189.120.23/crm/ucrm-fe/commit/b22afe6d20a06ef1cc146ae970209abf4d3d9a44))
- [tronglv-fix] update language for knowledge base ([cab5d89](https://10.189.120.23/crm/ucrm-fe/commit/cab5d89bffc57e5088e476539a7068dc46e449e4))
- [tronglv-knowledge] feature search ([79429cd](https://10.189.120.23/crm/ucrm-fe/commit/79429cd2f14774031d0e21a834aa4be309b21634))
- [tronglv-setting] setting knowledge base in consolidated ([1b4c80e](https://10.189.120.23/crm/ucrm-fe/commit/1b4c80ee2688de89d9e5e342f258439be41e9a21))
- [tronglv-up conflict] push code conflict ([732262b](https://10.189.120.23/crm/ucrm-fe/commit/732262b191c349b7fcaa2ee263d4ce52131bd96b))
- [tronglv-update] add article ([eeb596b](https://10.189.120.23/crm/ucrm-fe/commit/eeb596b873a5f932bb46efaa8211c73a21a472c2))
- [tronglv-update] knowledge base demo ([628afc5](https://10.189.120.23/crm/ucrm-fe/commit/628afc5676b6060edd239612431ac1d19fe3089d))
- [tronglv-update] update modal setting knowledge base ([3badcdb](https://10.189.120.23/crm/ucrm-fe/commit/3badcdba83431d7a950d79768b964a5e818b895e))
- add C247 Standard CTI ([467acfb](https://10.189.120.23/crm/ucrm-fe/commit/467acfba6be5da5ff4ff49faa781d02ed7d19ec1))
- create record call duplicate rule ([6ee8f7c](https://10.189.120.23/crm/ucrm-fe/commit/6ee8f7c1818afa55cb7543e7a774fa42f0e568d7))
- double click edit detail, UI SLA ([518affe](https://10.189.120.23/crm/ucrm-fe/commit/518affe89b9c3324728348a65ae31420865bb9d0))
- fix create template record ([e9f8964](https://10.189.120.23/crm/ucrm-fe/commit/e9f89643a2e04c2d782c4242d7a48a3313350ec6))
- fix role profile for consolidated view tag comp ([f2d9971](https://10.189.120.23/crm/ucrm-fe/commit/f2d9971e195c6e461c67c762d698902a67dd96ec))
- optimize search in listview and hot fix edit detail comp ([0b66790](https://10.189.120.23/crm/ucrm-fe/commit/0b667900090f8a498000f5a7cf145696dca4529b))
- update version 2.33.0 ([f9b815e](https://10.189.120.23/crm/ucrm-fe/commit/f9b815ea192dd75262d32c91e15ac321a99c37ac))

### Others

- **release:** 2.31.0 ([c099f9e](https://10.189.120.23/crm/ucrm-fe/commit/c099f9e3d413ebc4a63443bb013569c1a553c52d))
- **release:** 2.31.1 ([1bc45b7](https://10.189.120.23/crm/ucrm-fe/commit/1bc45b72b8915a3b7bbdf36184ede63c2e6ff188))
- **release:** 2.32.0 ([99ed093](https://10.189.120.23/crm/ucrm-fe/commit/99ed09365396e32549e7dfb423acbc1ab25d297b))
- **release:** 2.33.0 ([ab50bab](https://10.189.120.23/crm/ucrm-fe/commit/ab50bab443d2c009488c9e919eb28fad1a805528))

## [2.33.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.25.0...v2.33.0) (2022-12-29)

### Features

- [Comment] Allow user copy paste image ([bdd64c4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bdd64c4093f44d2bfe03362da26eeef83b34e65d))
- [Draft email] Done ([9342230](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9342230e4b120d76acdae69bebe9e8f270436544))
- [Duplicate rule] Add third party ([980df66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/980df6603e5f3fd2f5f6f438e473cc3d80647e3c))
- [Email Incoming] Add button scan mail ([b4595c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b4595c1fa182aa8749f4ab308a7f41c0c1c5d26b))
- [Email Signature] Done ([5d0e7eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d0e7eb5b9b155ab6609e3b87208e9c16d40004d))
- [Embed Iframe] Done ([93090f5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/93090f5ec4f167a6568dce94ceb0b59622351275))
- [Embed Iframe] Inprogress ([60bb10a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/60bb10a57efd36c684e907c89c2dbe540afb420a))
- [Embed Iframe] Inprogress ([7a3b846](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7a3b846790b46893684329c3580f493b11abd0ea))
- [Profile] Add report permission ([c47d6f6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c47d6f609944f6af125ec58914f58a75d2a6d9d6))
- [Profiles] Add permission for Export, Import, Report ([f7e6439](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f7e6439e1858278e3a85b382833becf02e6a3886))
- [Workflow] Add trigger workflow with interaction ([30400e1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30400e10d45cad06bc66ec930b6f2b46ad1822e2))
- create template record ([73f0536](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73f053621708bb1536f4df4aa66dfab1dfa1f567))
- new duplicated rule ([1fccb77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1fccb77752a82c71562274725ff9fc35d85666d0))

### Bug Fixes

- [Action workflow] Fix type linkingobject ([d513120](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d513120fdfe4d24067d07c7c0226e6b4ca23ecf1))
- [Brand and color] Update permission ([5ac6598](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5ac6598d594be0f4332759ae2f7650824fd6a41e))
- [Calendar] Fix empty description ([a47fd07](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a47fd071bbc81d614fd43217ea4210db4367e81d))
- [Call center] Create record ([511dc4e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/511dc4e44de4a7c17def1d8e67b2aa0b24ba18b2))
- [Call] Run time; [Timeline,Call] Save linking; [URL,Call] Create record duplicate ([03d2ade](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/03d2ade7b37d5b10cbb12903e4101d53fa95a9fa))
- [Comment] Add download image ([c3604d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3604d0d1cc23bc074df31cff00fbd521240e8f1))
- [Comments] Dup API; [Email signature] Auto delete last signature ([375ef06](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/375ef06388706ea03ce00db9ae33d30c79baed8e))
- [Comments] Upload one image when copy ([36373de](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36373de02e54499ee1ba9649e2ba899b8ad3887e))
- [Create record call] Hide modal duplicate ([31805f2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31805f24816285badaf43b347c02ded13b89bee4))
- [Create record] Add duplicated ([856d784](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/856d78492c9e6563f56beeb111ebb628e131895a))
- [Create record] Add modal duplicate ([092d9d8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/092d9d87b01ec4c2b009a98174456248d8bcfc4b))
- [Duplicate rule] payload ([90d36ff](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/90d36ff300ebd89215bdb070b1004df6bd263a7e))
- [Email component] Add click noti show email error ([d867129](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d867129914c7590d376d108cd3524fe4a7270ac6))
- [Email component] Keep content of reply email ([e092638](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e092638883a5586decaeee82cb24ed42e5ce8e0b))
- [Email Signature] Allow show signature before quote; [Action update source target] Hide related ([20ab39a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/20ab39a071925aef68c8c2cff350aadd5e154f87))
- [Email Signature] On blur editor submit ([d90567b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d90567b8f4e6de3ce35eb4454f82a2cb86b48d75))
- [Email] 500 ([0f8f961](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0f8f96121dc6fedbdb5db97f8f29fcf7bc6a2959))
- [Email] Delete draft email when have component Comments ([1b3798a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1b3798a8735108e567088a6f7de9f25ce135197d))
- [Email] Select template do not replace subject ([f11c999](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f11c9995db1d43d5b47f23a400b21a894d88b607))
- [Export report] Add quick filter when export ([605cb15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/605cb15b9f61a90707c1274b1c36d655daaa2b7f))
- [External app integration] Add authen token ([9cdbee5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9cdbee5c68ab613b792b42fa70be2b4e28dab33b))
- [Finesse] Hide password ([9d96903](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9d96903445c05fef9ccd7350d88e5bbcdd8eb817))
- [knowledge-base] add component comment in detail article ([6e5320f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6e5320f142f8af56da3c1fd413b9d65ac50fe889))
- [knowledge-base] add language knowledge base ([5491584](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5491584b0e9944cf1621387538104da288d0ae6c))
- [knowledge-base] build test ([3508471](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3508471184fde93e255f68a354cedb6331ef13b7))
- [Modal Email Incoming] Update UI; Update logic Action on read ([9b6f7d4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9b6f7d425dba2944e0ea3bf431039f5fce9d0736))
- [Modal Version] Add version BE; [Report] Fix display report details; [Email] Add ctrl v email ([201575f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/201575f4d7261a84f60668e7cd331d6c8995e87e))
- [Numpad] Fix UI status; [Modal Email Incoming] Load data user ([c6de994](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c6de994e95130de5ed365328b5546ac5317788ea))
- [PopupContact] Add duplicate rule; Fix two new call popup ([7c60c18](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7c60c18bf74f5c0f3d9b801e026f1d29b1ab6ef1))
- [PopupContact] duplicate with 3rd ([7999e5c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7999e5c06979e059404a45d194b78dff60fb8fde))
- [Profile permission] Report ([5edddd3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5edddd3052996f4841958687cd85560dcd50fe9f))
- [Profile Setting] Hide email signature ([f16644c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f16644cb6da46a9575f882ea41e4d0616c9b54b2))
- [Report Details] Add responsive chart ([feba3e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/feba3e63033608a913bbc42e2b88fa62f3588f05))
- [Report] Field type formula datediff in words ([41f7775](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/41f7775b86ab233741b6b93319ba5594f11a3325))
- [Rule Incoming] Add on fail ([31e199f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31e199fd0f01645b8bc9a562b22e967c3131b0d2))
- [Rule incoming] Add select picklist ([61d4902](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/61d490273ad59ae7b8c53125f3ed62ff0b698a58))
- [Rule Incoming] Change linking type ([fa48cab](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fa48cabee8a8b2b062622070b4ab63f99c7b9c0d))
- [Rule incoming] New UI new logic ([cdaaac9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cdaaac9bf8f8ec478a7af99523f8db20420024a4))
- [Rule Incoming] Type select ([c2078ed](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c2078ed2519dacc0dee5e002cc13cd80425f25e0))
- [tronglv-commit] commit code test code list view ([3ecdf52](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ecdf525abaadab324ceccb645e4caedb504dd28))
- [tronglv-feature] finish page edit and add article ([d49fcd9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d49fcd9f5165c9282ce429fef6dc2f0d932de96d))
- [tronglv-feature] knowledge consolidated ([3531b19](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3531b1999a71945b6293057f913e7bf696faf659))
- [tronglv-feature] knowledge in consolidated done - begin test ([b18f7db](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b18f7db852b9e638c0bff01cf1f79e7530239af3))
- [tronglv-feature] push code anh Vinh test ([6195d3b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6195d3b625492f7cb9c1d23f3d4d7e88226c5a8c))
- [tronglv-feature] update feature knowlege base and display manual sharing ([eeae037](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eeae037739377509a7810b5693fd62a87a242ee0))
- [tronglv-fix] commit code ([e414413](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e4144139a64f34dfbe6705aa73eb1c60e55ffa6a))
- [tronglv-fix] fix bug knowledge ([56b7b50](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/56b7b50be104dfba20d03a8e943145828e61c32f))
- [tronglv-fix] fix bug knowledge base ([25eaa47](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/25eaa47dc4393d6971b411dd4f8c7cec86b47d3b))
- [tronglv-fix] fix bug option dashboard ([9b37ee8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9b37ee89a2c8a4667f03180cbc69b3144c0b3651))
- [tronglv-fix] fix conflict ([e5ca212](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e5ca2121324b598eb85bf5bbf4fbd57466aee3c9))
- [tronglv-fix] fix search with category id ([f6f2b2e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f6f2b2e389f6610114c80dcc015e31e74c6258ba))
- [tronglv-fix] fix UI detail component ([c3bbdf1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3bbdf18d4ad12c95fd2156594ba0c6fd92a792f))
- [tronglv-fix] hidden manual sharing ([ff113d4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ff113d430003b098dfd02771643dc1853a2e4002))
- [tronglv-fix] improvement modal list view, edit, UI modal record, consolidated view ([38a4e33](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/38a4e33366e7c4cdc873fefaad280cb310d947ac))
- [tronglv-fix] number in kanban view, detail consolidated view, trim list view ([4031eec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4031eec870bfbd59359c21909d550de9749edf24))
- [tronglv-fix] update language for knowledge base ([cab5d89](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cab5d89bffc57e5088e476539a7068dc46e449e4))
- [tronglv-fixbug] fix bug search number in list view ([422e816](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/422e816d2be4800c29cb050a07d886c46ce6bf8d))
- [tronglv-knowledge] feature search ([79429cd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/79429cd2f14774031d0e21a834aa4be309b21634))
- [tronglv-setting] setting knowledge base in consolidated ([1b4c80e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1b4c80ee2688de89d9e5e342f258439be41e9a21))
- [tronglv-up conflict] push code conflict ([732262b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/732262b191c349b7fcaa2ee263d4ce52131bd96b))
- [tronglv-update] add article ([eeb596b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eeb596b873a5f932bb46efaa8211c73a21a472c2))
- [tronglv-update] knowledge base demo ([628afc5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/628afc5676b6060edd239612431ac1d19fe3089d))
- [tronglv-update] update display listview ([fd5649d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fd5649d73809fc64098b7379edc2c57b07fd0f2e))
- [tronglv-update] update modal setting knowledge base ([3badcdb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3badcdba83431d7a950d79768b964a5e818b895e))
- [tronglv-update] update option view detail consolidated view ([893188d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/893188d4fceb7eef81ae6709ab352cb54433d2e0))
- [URL Popup] auto open modal create record ([5be1c04](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5be1c04350864ce31e5e5b7c161efa405ea1fe01))
- [Voice Biometric] Call event when use ([537cba1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/537cba150809e3ffdde51dae744059da31784b0f))
- [Workflow-SourceTarget] UI conditions ([fb1737c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fb1737c530251266bd5eb4c3f8d03a0ff4f1c0fb))
- add C247 Standard CTI ([467acfb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/467acfba6be5da5ff4ff49faa781d02ed7d19ec1))
- add modal zoom image in comments component ([c37a279](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c37a2795b9f14e63913f71e86775efbed130ed20))
- add new editor ([c1da2c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c1da2c14923fc2b08f11002372f38829e1fbf74b))
- bumb version ([b4d5006](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b4d50068fe8cb788175cbe7ced3a673df8566521))
- create record call duplicate rule ([6ee8f7c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6ee8f7c1818afa55cb7543e7a774fa42f0e568d7))
- default view, linking required, custom admin ([38b2e5c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/38b2e5cc36edda81da322772fe94c7129a6fda25))
- fix create template record ([e9f8964](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e9f89643a2e04c2d782c4242d7a48a3313350ec6))
- fix duplicated rule ([1c5f1b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1c5f1b72ad6eb0424514ddf1847ea5fad07cc6c3))
- fix edit detail ([3175a24](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3175a24f02a8854e252e39b5b47e28ec85fe5346))
- fix edit detail loading ([9f65784](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9f65784c8e832f807f13c2b8fda4710dbd3bc930))
- fix edit file upload in consolidated view ([5bd97b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5bd97b7014ff054fbb16a692326d19979a149042))
- fix linking filter and double click to edit detail in consolidated view ([7e9ed47](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e9ed47af393766fab4cdf77ff7d69c5b3b0a546))
- fix third party ([4ad5f45](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4ad5f45f50a3684f459654f3c656ca460ef347cd))
- fix third party and duplicate rule ([a768119](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a7681196336f80b309070c13b048b08a756f9cff))
- parse email ([d91e135](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d91e135a0f95a3a7cee616061d44108859c400a7))
- unlock action update related target record ([4d68ded](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4d68ded91fea83a1a6407978d0a64db01c628dc4))
- upsize text and textarea length from 10000 to 100000 ([d4914cc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d4914cc89ae18906b0ececdc67819fc73516a22c))

### Others

- **release:** 2.26.0 ([d2087f4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d2087f41be6a07cd85a34314957a09b10d77d07b))
- **release:** 2.26.1 ([154c6e5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/154c6e5b699d79c92c0dfcdeabbf6fcc815e2bec))
- **release:** 2.27.0 ([505cc72](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/505cc72c935c8e3098f4668f49b9ab89c68e6675))
- **release:** 2.27.1 ([96298f8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96298f868915e24bc9c9c99fd6b28fb2ca1cb3ad))
- **release:** 2.28.0 ([f859544](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8595449a57d0f1b249414e5bed44b5ccec5da79))
- **release:** 2.29.0 ([2f5aa41](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f5aa41ee702b4b76627ee41116451550f925b91))
- **release:** 2.29.1 ([cc3fa2d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc3fa2ddf12707539b2f4efced256648392608e2))
- **release:** 2.29.2 ([ff88c66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ff88c6663a91f9c2db248e6a29be94dd0f035a79))
- **release:** 2.30.0 ([69fb3b8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/69fb3b8c8f1885c4113bfcf283e5918909f2aaf0))
- **release:** 2.30.1 ([7e5b810](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e5b810314234ef0269f7d5b6adb1160e6ee676c))
- **release:** 2.31.0 ([c099f9e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c099f9e3d413ebc4a63443bb013569c1a553c52d))
- **release:** 2.31.1 ([1bc45b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1bc45b72b8915a3b7bbdf36184ede63c2e6ff188))
- **release:** 2.32.0 ([99ed093](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/99ed09365396e32549e7dfb423acbc1ab25d297b))

## [2.32.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.25.0...v2.32.0) (2022-12-27)

### Features

- [Comment] Allow user copy paste image ([bdd64c4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bdd64c4093f44d2bfe03362da26eeef83b34e65d))
- [Draft email] Done ([9342230](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9342230e4b120d76acdae69bebe9e8f270436544))
- [Duplicate rule] Add third party ([980df66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/980df6603e5f3fd2f5f6f438e473cc3d80647e3c))
- [Email Incoming] Add button scan mail ([b4595c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b4595c1fa182aa8749f4ab308a7f41c0c1c5d26b))
- [Email Signature] Done ([5d0e7eb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d0e7eb5b9b155ab6609e3b87208e9c16d40004d))
- [Embed Iframe] Done ([93090f5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/93090f5ec4f167a6568dce94ceb0b59622351275))
- [Embed Iframe] Inprogress ([60bb10a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/60bb10a57efd36c684e907c89c2dbe540afb420a))
- [Embed Iframe] Inprogress ([7a3b846](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7a3b846790b46893684329c3580f493b11abd0ea))
- [Profile] Add report permission ([c47d6f6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c47d6f609944f6af125ec58914f58a75d2a6d9d6))
- [Profiles] Add permission for Export, Import, Report ([f7e6439](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f7e6439e1858278e3a85b382833becf02e6a3886))
- [Workflow] Add trigger workflow with interaction ([30400e1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/30400e10d45cad06bc66ec930b6f2b46ad1822e2))
- create template record ([73f0536](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73f053621708bb1536f4df4aa66dfab1dfa1f567))
- new duplicated rule ([1fccb77](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1fccb77752a82c71562274725ff9fc35d85666d0))

### Bug Fixes

- [Action workflow] Fix type linkingobject ([d513120](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d513120fdfe4d24067d07c7c0226e6b4ca23ecf1))
- [Brand and color] Update permission ([5ac6598](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5ac6598d594be0f4332759ae2f7650824fd6a41e))
- [Calendar] Fix empty description ([a47fd07](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a47fd071bbc81d614fd43217ea4210db4367e81d))
- [Call center] Create record ([511dc4e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/511dc4e44de4a7c17def1d8e67b2aa0b24ba18b2))
- [Call] Run time; [Timeline,Call] Save linking; [URL,Call] Create record duplicate ([03d2ade](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/03d2ade7b37d5b10cbb12903e4101d53fa95a9fa))
- [Comment] Add download image ([c3604d0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3604d0d1cc23bc074df31cff00fbd521240e8f1))
- [Comments] Dup API; [Email signature] Auto delete last signature ([375ef06](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/375ef06388706ea03ce00db9ae33d30c79baed8e))
- [Comments] Upload one image when copy ([36373de](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36373de02e54499ee1ba9649e2ba899b8ad3887e))
- [Create record call] Hide modal duplicate ([31805f2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31805f24816285badaf43b347c02ded13b89bee4))
- [Create record] Add duplicated ([856d784](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/856d78492c9e6563f56beeb111ebb628e131895a))
- [Create record] Add modal duplicate ([092d9d8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/092d9d87b01ec4c2b009a98174456248d8bcfc4b))
- [Duplicate rule] payload ([90d36ff](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/90d36ff300ebd89215bdb070b1004df6bd263a7e))
- [Email component] Add click noti show email error ([d867129](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d867129914c7590d376d108cd3524fe4a7270ac6))
- [Email component] Keep content of reply email ([e092638](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e092638883a5586decaeee82cb24ed42e5ce8e0b))
- [Email Signature] Allow show signature before quote; [Action update source target] Hide related ([20ab39a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/20ab39a071925aef68c8c2cff350aadd5e154f87))
- [Email Signature] On blur editor submit ([d90567b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d90567b8f4e6de3ce35eb4454f82a2cb86b48d75))
- [Email] 500 ([0f8f961](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0f8f96121dc6fedbdb5db97f8f29fcf7bc6a2959))
- [Email] Delete draft email when have component Comments ([1b3798a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1b3798a8735108e567088a6f7de9f25ce135197d))
- [Email] Select template do not replace subject ([f11c999](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f11c9995db1d43d5b47f23a400b21a894d88b607))
- [Export report] Add quick filter when export ([605cb15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/605cb15b9f61a90707c1274b1c36d655daaa2b7f))
- [External app integration] Add authen token ([9cdbee5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9cdbee5c68ab613b792b42fa70be2b4e28dab33b))
- [Finesse] Hide password ([9d96903](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9d96903445c05fef9ccd7350d88e5bbcdd8eb817))
- [Modal Email Incoming] Update UI; Update logic Action on read ([9b6f7d4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9b6f7d425dba2944e0ea3bf431039f5fce9d0736))
- [Modal Version] Add version BE; [Report] Fix display report details; [Email] Add ctrl v email ([201575f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/201575f4d7261a84f60668e7cd331d6c8995e87e))
- [Numpad] Fix UI status; [Modal Email Incoming] Load data user ([c6de994](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c6de994e95130de5ed365328b5546ac5317788ea))
- [Profile permission] Report ([5edddd3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5edddd3052996f4841958687cd85560dcd50fe9f))
- [Profile Setting] Hide email signature ([f16644c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f16644cb6da46a9575f882ea41e4d0616c9b54b2))
- [Report Details] Add responsive chart ([feba3e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/feba3e63033608a913bbc42e2b88fa62f3588f05))
- [Report] Field type formula datediff in words ([41f7775](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/41f7775b86ab233741b6b93319ba5594f11a3325))
- [Rule Incoming] Add on fail ([31e199f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/31e199fd0f01645b8bc9a562b22e967c3131b0d2))
- [Rule incoming] Add select picklist ([61d4902](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/61d490273ad59ae7b8c53125f3ed62ff0b698a58))
- [Rule Incoming] Change linking type ([fa48cab](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fa48cabee8a8b2b062622070b4ab63f99c7b9c0d))
- [Rule incoming] New UI new logic ([cdaaac9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cdaaac9bf8f8ec478a7af99523f8db20420024a4))
- [Rule Incoming] Type select ([c2078ed](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c2078ed2519dacc0dee5e002cc13cd80425f25e0))
- [tronglv-fix] fix bug option dashboard ([9b37ee8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9b37ee89a2c8a4667f03180cbc69b3144c0b3651))
- [tronglv-fix] fix UI detail component ([c3bbdf1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c3bbdf18d4ad12c95fd2156594ba0c6fd92a792f))
- [tronglv-fix] improvement modal list view, edit, UI modal record, consolidated view ([38a4e33](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/38a4e33366e7c4cdc873fefaad280cb310d947ac))
- [tronglv-fix] number in kanban view, detail consolidated view, trim list view ([4031eec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4031eec870bfbd59359c21909d550de9749edf24))
- [tronglv-fixbug] fix bug search number in list view ([422e816](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/422e816d2be4800c29cb050a07d886c46ce6bf8d))
- [tronglv-update] update display listview ([fd5649d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fd5649d73809fc64098b7379edc2c57b07fd0f2e))
- [tronglv-update] update option view detail consolidated view ([893188d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/893188d4fceb7eef81ae6709ab352cb54433d2e0))
- [URL Popup] auto open modal create record ([5be1c04](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5be1c04350864ce31e5e5b7c161efa405ea1fe01))
- [Voice Biometric] Call event when use ([537cba1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/537cba150809e3ffdde51dae744059da31784b0f))
- [Workflow-SourceTarget] UI conditions ([fb1737c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fb1737c530251266bd5eb4c3f8d03a0ff4f1c0fb))
- add C247 Standard CTI ([467acfb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/467acfba6be5da5ff4ff49faa781d02ed7d19ec1))
- add modal zoom image in comments component ([c37a279](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c37a2795b9f14e63913f71e86775efbed130ed20))
- add new editor ([c1da2c1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c1da2c14923fc2b08f11002372f38829e1fbf74b))
- bumb version ([b4d5006](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b4d50068fe8cb788175cbe7ced3a673df8566521))
- default view, linking required, custom admin ([38b2e5c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/38b2e5cc36edda81da322772fe94c7129a6fda25))
- fix create template record ([e9f8964](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e9f89643a2e04c2d782c4242d7a48a3313350ec6))
- fix duplicated rule ([1c5f1b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1c5f1b72ad6eb0424514ddf1847ea5fad07cc6c3))
- fix edit detail ([3175a24](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3175a24f02a8854e252e39b5b47e28ec85fe5346))
- fix edit detail loading ([9f65784](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9f65784c8e832f807f13c2b8fda4710dbd3bc930))
- fix edit file upload in consolidated view ([5bd97b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5bd97b7014ff054fbb16a692326d19979a149042))
- fix linking filter and double click to edit detail in consolidated view ([7e9ed47](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e9ed47af393766fab4cdf77ff7d69c5b3b0a546))
- fix third party ([4ad5f45](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4ad5f45f50a3684f459654f3c656ca460ef347cd))
- fix third party and duplicate rule ([a768119](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a7681196336f80b309070c13b048b08a756f9cff))
- parse email ([d91e135](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d91e135a0f95a3a7cee616061d44108859c400a7))
- unlock action update related target record ([4d68ded](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4d68ded91fea83a1a6407978d0a64db01c628dc4))
- upsize text and textarea length from 10000 to 100000 ([d4914cc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d4914cc89ae18906b0ececdc67819fc73516a22c))

### Others

- **release:** 2.26.0 ([d2087f4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d2087f41be6a07cd85a34314957a09b10d77d07b))
- **release:** 2.26.1 ([154c6e5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/154c6e5b699d79c92c0dfcdeabbf6fcc815e2bec))
- **release:** 2.27.0 ([505cc72](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/505cc72c935c8e3098f4668f49b9ab89c68e6675))
- **release:** 2.27.1 ([96298f8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96298f868915e24bc9c9c99fd6b28fb2ca1cb3ad))
- **release:** 2.28.0 ([f859544](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8595449a57d0f1b249414e5bed44b5ccec5da79))
- **release:** 2.29.0 ([2f5aa41](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f5aa41ee702b4b76627ee41116451550f925b91))
- **release:** 2.29.1 ([cc3fa2d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cc3fa2ddf12707539b2f4efced256648392608e2))
- **release:** 2.29.2 ([ff88c66](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ff88c6663a91f9c2db248e6a29be94dd0f035a79))
- **release:** 2.30.0 ([69fb3b8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/69fb3b8c8f1885c4113bfcf283e5918909f2aaf0))
- **release:** 2.30.1 ([7e5b810](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e5b810314234ef0269f7d5b6adb1160e6ee676c))
- **release:** 2.31.0 ([c099f9e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c099f9e3d413ebc4a63443bb013569c1a553c52d))
- **release:** 2.31.1 ([1bc45b7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1bc45b72b8915a3b7bbdf36184ede63c2e6ff188))

### [2.31.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.31.0...v2.31.1) (2022-12-27)

### Bug Fixes

- [Create record call] Hide modal duplicate ([31805f2](https://10.189.120.23/crm/ucrm-fe/commit/31805f24816285badaf43b347c02ded13b89bee4))

## [2.31.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.29.1...v2.31.0) (2022-12-27)

### Features

- [Comment] Allow user copy paste image ([bdd64c4](https://10.189.120.23/crm/ucrm-fe/commit/bdd64c4093f44d2bfe03362da26eeef83b34e65d))
- create template record ([73f0536](https://10.189.120.23/crm/ucrm-fe/commit/73f053621708bb1536f4df4aa66dfab1dfa1f567))

### Bug Fixes

- [Action workflow] Fix type linkingobject ([d513120](https://10.189.120.23/crm/ucrm-fe/commit/d513120fdfe4d24067d07c7c0226e6b4ca23ecf1))
- [Call center] Create record ([511dc4e](https://10.189.120.23/crm/ucrm-fe/commit/511dc4e44de4a7c17def1d8e67b2aa0b24ba18b2))
- [Call] Run time; [Timeline,Call] Save linking; [URL,Call] Create record duplicate ([03d2ade](https://10.189.120.23/crm/ucrm-fe/commit/03d2ade7b37d5b10cbb12903e4101d53fa95a9fa))
- [Comments] Upload one image when copy ([36373de](https://10.189.120.23/crm/ucrm-fe/commit/36373de02e54499ee1ba9649e2ba899b8ad3887e))
- [Create record] Add duplicated ([856d784](https://10.189.120.23/crm/ucrm-fe/commit/856d78492c9e6563f56beeb111ebb628e131895a))
- [Create record] Add modal duplicate ([092d9d8](https://10.189.120.23/crm/ucrm-fe/commit/092d9d87b01ec4c2b009a98174456248d8bcfc4b))
- [Email Signature] Allow show signature before quote; [Action update source target] Hide related ([20ab39a](https://10.189.120.23/crm/ucrm-fe/commit/20ab39a071925aef68c8c2cff350aadd5e154f87))
- [Email] Delete draft email when have component Comments ([1b3798a](https://10.189.120.23/crm/ucrm-fe/commit/1b3798a8735108e567088a6f7de9f25ce135197d))
- [Finesse] Hide password ([9d96903](https://10.189.120.23/crm/ucrm-fe/commit/9d96903445c05fef9ccd7350d88e5bbcdd8eb817))
- [Modal Version] Add version BE; [Report] Fix display report details; [Email] Add ctrl v email ([201575f](https://10.189.120.23/crm/ucrm-fe/commit/201575f4d7261a84f60668e7cd331d6c8995e87e))
- [Report] Field type formula datediff in words ([41f7775](https://10.189.120.23/crm/ucrm-fe/commit/41f7775b86ab233741b6b93319ba5594f11a3325))
- [tronglv-fixbug] fix bug search number in list view ([422e816](https://10.189.120.23/crm/ucrm-fe/commit/422e816d2be4800c29cb050a07d886c46ce6bf8d))
- default view, linking required, custom admin ([38b2e5c](https://10.189.120.23/crm/ucrm-fe/commit/38b2e5cc36edda81da322772fe94c7129a6fda25))
- fix create template record ([e9f8964](https://10.189.120.23/crm/ucrm-fe/commit/e9f89643a2e04c2d782c4242d7a48a3313350ec6))
- fix linking filter and double click to edit detail in consolidated view ([7e9ed47](https://10.189.120.23/crm/ucrm-fe/commit/7e9ed47af393766fab4cdf77ff7d69c5b3b0a546))
- fix third party ([4ad5f45](https://10.189.120.23/crm/ucrm-fe/commit/4ad5f45f50a3684f459654f3c656ca460ef347cd))
- upsize text and textarea length from 10000 to 100000 ([d4914cc](https://10.189.120.23/crm/ucrm-fe/commit/d4914cc89ae18906b0ececdc67819fc73516a22c))

### Others

- **release:** 2.29.2 ([ff88c66](https://10.189.120.23/crm/ucrm-fe/commit/ff88c6663a91f9c2db248e6a29be94dd0f035a79))
- **release:** 2.30.0 ([69fb3b8](https://10.189.120.23/crm/ucrm-fe/commit/69fb3b8c8f1885c4113bfcf283e5918909f2aaf0))
- **release:** 2.30.1 ([7e5b810](https://10.189.120.23/crm/ucrm-fe/commit/7e5b810314234ef0269f7d5b6adb1160e6ee676c))

### [2.30.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.30.0...v2.30.1) (2022-12-22)

### Bug Fixes

- [Email] Delete draft email when have component Comments ([1b3798a](https://10.189.120.23/crm/ucrm-fe/commit/1b3798a8735108e567088a6f7de9f25ce135197d))
- [Report] Field type formula datediff in words ([41f7775](https://10.189.120.23/crm/ucrm-fe/commit/41f7775b86ab233741b6b93319ba5594f11a3325))

## [2.30.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.28.0...v2.30.0) (2022-12-19)

### Features

- [Comment] Allow user copy paste image ([bdd64c4](https://10.189.120.23/crm/ucrm-fe/commit/bdd64c4093f44d2bfe03362da26eeef83b34e65d))
- [Draft email] Done ([9342230](https://10.189.120.23/crm/ucrm-fe/commit/9342230e4b120d76acdae69bebe9e8f270436544))
- [Embed Iframe] Done ([93090f5](https://10.189.120.23/crm/ucrm-fe/commit/93090f5ec4f167a6568dce94ceb0b59622351275))
- [Embed Iframe] Inprogress ([60bb10a](https://10.189.120.23/crm/ucrm-fe/commit/60bb10a57efd36c684e907c89c2dbe540afb420a))
- [Embed Iframe] Inprogress ([7a3b846](https://10.189.120.23/crm/ucrm-fe/commit/7a3b846790b46893684329c3580f493b11abd0ea))
- [Workflow] Add trigger workflow with interaction ([30400e1](https://10.189.120.23/crm/ucrm-fe/commit/30400e10d45cad06bc66ec930b6f2b46ad1822e2))

### Bug Fixes

- [Call center] Create record ([511dc4e](https://10.189.120.23/crm/ucrm-fe/commit/511dc4e44de4a7c17def1d8e67b2aa0b24ba18b2))
- [Comments] Dup API; [Email signature] Auto delete last signature ([375ef06](https://10.189.120.23/crm/ucrm-fe/commit/375ef06388706ea03ce00db9ae33d30c79baed8e))
- [Comments] Upload one image when copy ([36373de](https://10.189.120.23/crm/ucrm-fe/commit/36373de02e54499ee1ba9649e2ba899b8ad3887e))
- [Email Signature] On blur editor submit ([d90567b](https://10.189.120.23/crm/ucrm-fe/commit/d90567b8f4e6de3ce35eb4454f82a2cb86b48d75))
- [Export report] Add quick filter when export ([605cb15](https://10.189.120.23/crm/ucrm-fe/commit/605cb15b9f61a90707c1274b1c36d655daaa2b7f))
- [External app integration] Add authen token ([9cdbee5](https://10.189.120.23/crm/ucrm-fe/commit/9cdbee5c68ab613b792b42fa70be2b4e28dab33b))
- [Modal Version] Add version BE; [Report] Fix display report details; [Email] Add ctrl v email ([201575f](https://10.189.120.23/crm/ucrm-fe/commit/201575f4d7261a84f60668e7cd331d6c8995e87e))
- [tronglv-fix] fix bug option dashboard ([9b37ee8](https://10.189.120.23/crm/ucrm-fe/commit/9b37ee89a2c8a4667f03180cbc69b3144c0b3651))
- [tronglv-fixbug] fix bug search number in list view ([422e816](https://10.189.120.23/crm/ucrm-fe/commit/422e816d2be4800c29cb050a07d886c46ce6bf8d))
- [Voice Biometric] Call event when use ([537cba1](https://10.189.120.23/crm/ucrm-fe/commit/537cba150809e3ffdde51dae744059da31784b0f))
- default view, linking required, custom admin ([38b2e5c](https://10.189.120.23/crm/ucrm-fe/commit/38b2e5cc36edda81da322772fe94c7129a6fda25))
- fix linking filter and double click to edit detail in consolidated view ([7e9ed47](https://10.189.120.23/crm/ucrm-fe/commit/7e9ed47af393766fab4cdf77ff7d69c5b3b0a546))
- fix third party ([4ad5f45](https://10.189.120.23/crm/ucrm-fe/commit/4ad5f45f50a3684f459654f3c656ca460ef347cd))
- upsize text and textarea length from 10000 to 100000 ([d4914cc](https://10.189.120.23/crm/ucrm-fe/commit/d4914cc89ae18906b0ececdc67819fc73516a22c))

### Others

- **release:** 2.29.0 ([2f5aa41](https://10.189.120.23/crm/ucrm-fe/commit/2f5aa41ee702b4b76627ee41116451550f925b91))
- **release:** 2.29.1 ([cc3fa2d](https://10.189.120.23/crm/ucrm-fe/commit/cc3fa2ddf12707539b2f4efced256648392608e2))
- **release:** 2.29.2 ([ff88c66](https://10.189.120.23/crm/ucrm-fe/commit/ff88c6663a91f9c2db248e6a29be94dd0f035a79))

### [2.29.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.29.1...v2.29.2) (2022-12-15)

### Bug Fixes

- [Modal Version] Add version BE; [Report] Fix display report details; [Email] Add ctrl v email ([201575f](https://10.189.120.23/crm/ucrm-fe/commit/201575f4d7261a84f60668e7cd331d6c8995e87e))
- [tronglv-fixbug] fix bug search number in list view ([422e816](https://10.189.120.23/crm/ucrm-fe/commit/422e816d2be4800c29cb050a07d886c46ce6bf8d))
- fix linking filter and double click to edit detail in consolidated view ([7e9ed47](https://10.189.120.23/crm/ucrm-fe/commit/7e9ed47af393766fab4cdf77ff7d69c5b3b0a546))
- fix third party ([4ad5f45](https://10.189.120.23/crm/ucrm-fe/commit/4ad5f45f50a3684f459654f3c656ca460ef347cd))

### [2.29.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.29.0...v2.29.1) (2022-12-13)

### Bug Fixes

- [External app integration] Add authen token ([9cdbee5](https://10.189.120.23/crm/ucrm-fe/commit/9cdbee5c68ab613b792b42fa70be2b4e28dab33b))

## [2.29.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.27.1...v2.29.0) (2022-12-13)

### Features

- [Draft email] Done ([9342230](https://10.189.120.23/crm/ucrm-fe/commit/9342230e4b120d76acdae69bebe9e8f270436544))
- [Duplicate rule] Add third party ([980df66](https://10.189.120.23/crm/ucrm-fe/commit/980df6603e5f3fd2f5f6f438e473cc3d80647e3c))
- [Email Signature] Done ([5d0e7eb](https://10.189.120.23/crm/ucrm-fe/commit/5d0e7eb5b9b155ab6609e3b87208e9c16d40004d))
- [Embed Iframe] Done ([93090f5](https://10.189.120.23/crm/ucrm-fe/commit/93090f5ec4f167a6568dce94ceb0b59622351275))
- [Embed Iframe] Inprogress ([60bb10a](https://10.189.120.23/crm/ucrm-fe/commit/60bb10a57efd36c684e907c89c2dbe540afb420a))
- [Embed Iframe] Inprogress ([7a3b846](https://10.189.120.23/crm/ucrm-fe/commit/7a3b846790b46893684329c3580f493b11abd0ea))
- [Profile] Add report permission ([c47d6f6](https://10.189.120.23/crm/ucrm-fe/commit/c47d6f609944f6af125ec58914f58a75d2a6d9d6))
- [Profiles] Add permission for Export, Import, Report ([f7e6439](https://10.189.120.23/crm/ucrm-fe/commit/f7e6439e1858278e3a85b382833becf02e6a3886))
- [Workflow] Add trigger workflow with interaction ([30400e1](https://10.189.120.23/crm/ucrm-fe/commit/30400e10d45cad06bc66ec930b6f2b46ad1822e2))
- new duplicated rule ([1fccb77](https://10.189.120.23/crm/ucrm-fe/commit/1fccb77752a82c71562274725ff9fc35d85666d0))

### Bug Fixes

- [Comment] Add download image ([c3604d0](https://10.189.120.23/crm/ucrm-fe/commit/c3604d0d1cc23bc074df31cff00fbd521240e8f1))
- [Comments] Dup API; [Email signature] Auto delete last signature ([375ef06](https://10.189.120.23/crm/ucrm-fe/commit/375ef06388706ea03ce00db9ae33d30c79baed8e))
- [Duplicate rule] payload ([90d36ff](https://10.189.120.23/crm/ucrm-fe/commit/90d36ff300ebd89215bdb070b1004df6bd263a7e))
- [Email Signature] On blur editor submit ([d90567b](https://10.189.120.23/crm/ucrm-fe/commit/d90567b8f4e6de3ce35eb4454f82a2cb86b48d75))
- [Email] 500 ([0f8f961](https://10.189.120.23/crm/ucrm-fe/commit/0f8f96121dc6fedbdb5db97f8f29fcf7bc6a2959))
- [Export report] Add quick filter when export ([605cb15](https://10.189.120.23/crm/ucrm-fe/commit/605cb15b9f61a90707c1274b1c36d655daaa2b7f))
- [Profile permission] Report ([5edddd3](https://10.189.120.23/crm/ucrm-fe/commit/5edddd3052996f4841958687cd85560dcd50fe9f))
- [Profile Setting] Hide email signature ([f16644c](https://10.189.120.23/crm/ucrm-fe/commit/f16644cb6da46a9575f882ea41e4d0616c9b54b2))
- [tronglv-fix] fix bug option dashboard ([9b37ee8](https://10.189.120.23/crm/ucrm-fe/commit/9b37ee89a2c8a4667f03180cbc69b3144c0b3651))
- [tronglv-fix] improvement modal list view, edit, UI modal record, consolidated view ([38a4e33](https://10.189.120.23/crm/ucrm-fe/commit/38a4e33366e7c4cdc873fefaad280cb310d947ac))
- [tronglv-update] update display listview ([fd5649d](https://10.189.120.23/crm/ucrm-fe/commit/fd5649d73809fc64098b7379edc2c57b07fd0f2e))
- [tronglv-update] update option view detail consolidated view ([893188d](https://10.189.120.23/crm/ucrm-fe/commit/893188d4fceb7eef81ae6709ab352cb54433d2e0))
- [URL Popup] auto open modal create record ([5be1c04](https://10.189.120.23/crm/ucrm-fe/commit/5be1c04350864ce31e5e5b7c161efa405ea1fe01))
- [Voice Biometric] Call event when use ([537cba1](https://10.189.120.23/crm/ucrm-fe/commit/537cba150809e3ffdde51dae744059da31784b0f))
- fix duplicated rule ([1c5f1b7](https://10.189.120.23/crm/ucrm-fe/commit/1c5f1b72ad6eb0424514ddf1847ea5fad07cc6c3))
- fix edit detail loading ([9f65784](https://10.189.120.23/crm/ucrm-fe/commit/9f65784c8e832f807f13c2b8fda4710dbd3bc930))
- fix third party and duplicate rule ([a768119](https://10.189.120.23/crm/ucrm-fe/commit/a7681196336f80b309070c13b048b08a756f9cff))

### Others

- **release:** 2.28.0 ([f859544](https://10.189.120.23/crm/ucrm-fe/commit/f8595449a57d0f1b249414e5bed44b5ccec5da79))

## [2.28.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.27.0...v2.28.0) (2022-12-08)

### Features

- [Duplicate rule] Add third party ([980df66](https://10.189.120.23/crm/ucrm-fe/commit/980df6603e5f3fd2f5f6f438e473cc3d80647e3c))
- [Email Signature] Done ([5d0e7eb](https://10.189.120.23/crm/ucrm-fe/commit/5d0e7eb5b9b155ab6609e3b87208e9c16d40004d))
- [Profile] Add report permission ([c47d6f6](https://10.189.120.23/crm/ucrm-fe/commit/c47d6f609944f6af125ec58914f58a75d2a6d9d6))
- [Profiles] Add permission for Export, Import, Report ([f7e6439](https://10.189.120.23/crm/ucrm-fe/commit/f7e6439e1858278e3a85b382833becf02e6a3886))
- new duplicated rule ([1fccb77](https://10.189.120.23/crm/ucrm-fe/commit/1fccb77752a82c71562274725ff9fc35d85666d0))

### Bug Fixes

- [Brand and color] Update permission ([5ac6598](https://10.189.120.23/crm/ucrm-fe/commit/5ac6598d594be0f4332759ae2f7650824fd6a41e))
- [Calendar] Fix empty description ([a47fd07](https://10.189.120.23/crm/ucrm-fe/commit/a47fd071bbc81d614fd43217ea4210db4367e81d))
- [Comment] Add download image ([c3604d0](https://10.189.120.23/crm/ucrm-fe/commit/c3604d0d1cc23bc074df31cff00fbd521240e8f1))
- [Duplicate rule] payload ([90d36ff](https://10.189.120.23/crm/ucrm-fe/commit/90d36ff300ebd89215bdb070b1004df6bd263a7e))
- [Email component] Add click noti show email error ([d867129](https://10.189.120.23/crm/ucrm-fe/commit/d867129914c7590d376d108cd3524fe4a7270ac6))
- [Email component] Keep content of reply email ([e092638](https://10.189.120.23/crm/ucrm-fe/commit/e092638883a5586decaeee82cb24ed42e5ce8e0b))
- [Email] 500 ([0f8f961](https://10.189.120.23/crm/ucrm-fe/commit/0f8f96121dc6fedbdb5db97f8f29fcf7bc6a2959))
- [Profile permission] Report ([5edddd3](https://10.189.120.23/crm/ucrm-fe/commit/5edddd3052996f4841958687cd85560dcd50fe9f))
- [Profile Setting] Hide email signature ([f16644c](https://10.189.120.23/crm/ucrm-fe/commit/f16644cb6da46a9575f882ea41e4d0616c9b54b2))
- [Report Details] Add responsive chart ([feba3e6](https://10.189.120.23/crm/ucrm-fe/commit/feba3e63033608a913bbc42e2b88fa62f3588f05))
- [tronglv-fix] improvement modal list view, edit, UI modal record, consolidated view ([38a4e33](https://10.189.120.23/crm/ucrm-fe/commit/38a4e33366e7c4cdc873fefaad280cb310d947ac))
- [tronglv-update] update display listview ([fd5649d](https://10.189.120.23/crm/ucrm-fe/commit/fd5649d73809fc64098b7379edc2c57b07fd0f2e))
- [tronglv-update] update option view detail consolidated view ([893188d](https://10.189.120.23/crm/ucrm-fe/commit/893188d4fceb7eef81ae6709ab352cb54433d2e0))
- [URL Popup] auto open modal create record ([5be1c04](https://10.189.120.23/crm/ucrm-fe/commit/5be1c04350864ce31e5e5b7c161efa405ea1fe01))
- [Workflow-SourceTarget] UI conditions ([fb1737c](https://10.189.120.23/crm/ucrm-fe/commit/fb1737c530251266bd5eb4c3f8d03a0ff4f1c0fb))
- fix duplicated rule ([1c5f1b7](https://10.189.120.23/crm/ucrm-fe/commit/1c5f1b72ad6eb0424514ddf1847ea5fad07cc6c3))
- fix edit detail loading ([9f65784](https://10.189.120.23/crm/ucrm-fe/commit/9f65784c8e832f807f13c2b8fda4710dbd3bc930))
- fix third party and duplicate rule ([a768119](https://10.189.120.23/crm/ucrm-fe/commit/a7681196336f80b309070c13b048b08a756f9cff))

### Others

- **release:** 2.27.1 ([96298f8](https://10.189.120.23/crm/ucrm-fe/commit/96298f868915e24bc9c9c99fd6b28fb2ca1cb3ad))

### [2.27.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.26.1...v2.27.1) (2022-11-29)

### Bug Fixes

- [Brand and color] Update permission ([5ac6598](https://10.189.120.23/crm/ucrm-fe/commit/5ac6598d594be0f4332759ae2f7650824fd6a41e))
- [Calendar] Fix empty description ([a47fd07](https://10.189.120.23/crm/ucrm-fe/commit/a47fd071bbc81d614fd43217ea4210db4367e81d))
- [Email component] Add click noti show email error ([d867129](https://10.189.120.23/crm/ucrm-fe/commit/d867129914c7590d376d108cd3524fe4a7270ac6))
- [Email component] Keep content of reply email ([e092638](https://10.189.120.23/crm/ucrm-fe/commit/e092638883a5586decaeee82cb24ed42e5ce8e0b))
- [Modal Email Incoming] Update UI; Update logic Action on read ([9b6f7d4](https://10.189.120.23/crm/ucrm-fe/commit/9b6f7d425dba2944e0ea3bf431039f5fce9d0736))
- [Numpad] Fix UI status; [Modal Email Incoming] Load data user ([c6de994](https://10.189.120.23/crm/ucrm-fe/commit/c6de994e95130de5ed365328b5546ac5317788ea))
- [Report Details] Add responsive chart ([feba3e6](https://10.189.120.23/crm/ucrm-fe/commit/feba3e63033608a913bbc42e2b88fa62f3588f05))
- [tronglv-fix] fix UI detail component ([c3bbdf1](https://10.189.120.23/crm/ucrm-fe/commit/c3bbdf18d4ad12c95fd2156594ba0c6fd92a792f))
- [tronglv-fix] number in kanban view, detail consolidated view, trim list view ([4031eec](https://10.189.120.23/crm/ucrm-fe/commit/4031eec870bfbd59359c21909d550de9749edf24))
- [Workflow-SourceTarget] UI conditions ([fb1737c](https://10.189.120.23/crm/ucrm-fe/commit/fb1737c530251266bd5eb4c3f8d03a0ff4f1c0fb))
- add modal zoom image in comments component ([c37a279](https://10.189.120.23/crm/ucrm-fe/commit/c37a2795b9f14e63913f71e86775efbed130ed20))
- add new editor ([c1da2c1](https://10.189.120.23/crm/ucrm-fe/commit/c1da2c14923fc2b08f11002372f38829e1fbf74b))
- fix edit detail ([3175a24](https://10.189.120.23/crm/ucrm-fe/commit/3175a24f02a8854e252e39b5b47e28ec85fe5346))
- fix edit file upload in consolidated view ([5bd97b7](https://10.189.120.23/crm/ucrm-fe/commit/5bd97b7014ff054fbb16a692326d19979a149042))
- parse email ([d91e135](https://10.189.120.23/crm/ucrm-fe/commit/d91e135a0f95a3a7cee616061d44108859c400a7))
- unlock action update related target record ([4d68ded](https://10.189.120.23/crm/ucrm-fe/commit/4d68ded91fea83a1a6407978d0a64db01c628dc4))

### Others

- **release:** 2.27.0 ([505cc72](https://10.189.120.23/crm/ucrm-fe/commit/505cc72c935c8e3098f4668f49b9ab89c68e6675))

## [2.27.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.24.1...v2.27.0) (2022-11-24)

### Features

- [Email Incoming] Add button scan mail ([b4595c1](https://10.189.120.23/crm/ucrm-fe/commit/b4595c1fa182aa8749f4ab308a7f41c0c1c5d26b))

### Bug Fixes

- [Email] Select template do not replace subject ([f11c999](https://10.189.120.23/crm/ucrm-fe/commit/f11c9995db1d43d5b47f23a400b21a894d88b607))
- [Modal Email Incoming] Update UI; Update logic Action on read ([9b6f7d4](https://10.189.120.23/crm/ucrm-fe/commit/9b6f7d425dba2944e0ea3bf431039f5fce9d0736))
- [Numpad] Fix UI status; [Modal Email Incoming] Load data user ([c6de994](https://10.189.120.23/crm/ucrm-fe/commit/c6de994e95130de5ed365328b5546ac5317788ea))
- [Rule Incoming] Add on fail ([31e199f](https://10.189.120.23/crm/ucrm-fe/commit/31e199fd0f01645b8bc9a562b22e967c3131b0d2))
- [Rule incoming] Add select picklist ([61d4902](https://10.189.120.23/crm/ucrm-fe/commit/61d490273ad59ae7b8c53125f3ed62ff0b698a58))
- [Rule Incoming] Change linking type ([fa48cab](https://10.189.120.23/crm/ucrm-fe/commit/fa48cabee8a8b2b062622070b4ab63f99c7b9c0d))
- [Rule incoming] New UI new logic ([cdaaac9](https://10.189.120.23/crm/ucrm-fe/commit/cdaaac9bf8f8ec478a7af99523f8db20420024a4))
- [Rule Incoming] Type select ([c2078ed](https://10.189.120.23/crm/ucrm-fe/commit/c2078ed2519dacc0dee5e002cc13cd80425f25e0))
- [tronglv-fix] fix bug translate ([b079757](https://10.189.120.23/crm/ucrm-fe/commit/b0797575a1bc7f475d27f465b5fc2450e477d105))
- [tronglv-fix] fix UI detail component ([c3bbdf1](https://10.189.120.23/crm/ucrm-fe/commit/c3bbdf18d4ad12c95fd2156594ba0c6fd92a792f))
- [tronglv-fix] number in kanban view, detail consolidated view, trim list view ([4031eec](https://10.189.120.23/crm/ucrm-fe/commit/4031eec870bfbd59359c21909d550de9749edf24))
- [tronglv-fix] update: page title, icon edit OBJ & layout,bg record hover ([44d0c37](https://10.189.120.23/crm/ucrm-fe/commit/44d0c37150755a14040cf2cef237914e86a93c86))
- [tronglv-update] update collapse in object layout fields and hover in header ([3ca3515](https://10.189.120.23/crm/ucrm-fe/commit/3ca3515a873d64c7fc58a7542396c21c63831a55))
- [tronglv-update] update modal version ([3e4456f](https://10.189.120.23/crm/ucrm-fe/commit/3e4456f740a77d7f73d3d29ce234704365b2c0d0))
- [tronglv] fix conflict ([2d1c638](https://10.189.120.23/crm/ucrm-fe/commit/2d1c638b4a47d432e3c39bbb16ddf6f5cbc1ec46))
- add modal zoom image in comments component ([c37a279](https://10.189.120.23/crm/ucrm-fe/commit/c37a2795b9f14e63913f71e86775efbed130ed20))
- add new editor ([c1da2c1](https://10.189.120.23/crm/ucrm-fe/commit/c1da2c14923fc2b08f11002372f38829e1fbf74b))
- bumb version ([b4d5006](https://10.189.120.23/crm/ucrm-fe/commit/b4d50068fe8cb788175cbe7ced3a673df8566521))
- fix edit detail ([3175a24](https://10.189.120.23/crm/ucrm-fe/commit/3175a24f02a8854e252e39b5b47e28ec85fe5346))
- fix edit file upload in consolidated view ([5bd97b7](https://10.189.120.23/crm/ucrm-fe/commit/5bd97b7014ff054fbb16a692326d19979a149042))
- parse email ([d91e135](https://10.189.120.23/crm/ucrm-fe/commit/d91e135a0f95a3a7cee616061d44108859c400a7))
- unlock action update related target record ([4d68ded](https://10.189.120.23/crm/ucrm-fe/commit/4d68ded91fea83a1a6407978d0a64db01c628dc4))

### Others

- **release:** 2.25.0 ([d18e49c](https://10.189.120.23/crm/ucrm-fe/commit/d18e49c6b8e21645fe1d69f7e697b5a77659d736))
- **release:** 2.26.0 ([d2087f4](https://10.189.120.23/crm/ucrm-fe/commit/d2087f41be6a07cd85a34314957a09b10d77d07b))
- **release:** 2.26.1 ([154c6e5](https://10.189.120.23/crm/ucrm-fe/commit/154c6e5b699d79c92c0dfcdeabbf6fcc815e2bec))

### [2.26.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.26.0...v2.26.1) (2022-11-17)

### Bug Fixes

- [Rule Incoming] Add on fail ([31e199f](https://10.189.120.23/crm/ucrm-fe/commit/31e199fd0f01645b8bc9a562b22e967c3131b0d2))

## [2.26.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.6...v2.26.0) (2022-11-17)

### Features

- [Email Incoming] Add button scan mail ([b4595c1](https://10.189.120.23/crm/ucrm-fe/commit/b4595c1fa182aa8749f4ab308a7f41c0c1c5d26b))
- [Workflow/Dynamic Button] Add action update related record source target ([69f0fdb](https://10.189.120.23/crm/ucrm-fe/commit/69f0fdbbb016ae9d3b26ee156276b47029d26f0b))
- add manual tasks in SMS campaign (Campaign) ([fbbc4bf](https://10.189.120.23/crm/ucrm-fe/commit/fbbc4bf15f7318fe71a4c281a2d8267e5a5fd111))

### Bug Fixes

- [Action-UpdateSourceTarget] New conditions ([3b153d3](https://10.189.120.23/crm/ucrm-fe/commit/3b153d30e914793226dcfd6010a26bea8e0cc661))
- [Action-UpdateSourceTarget] Options field type linkingobject ([e66864b](https://10.189.120.23/crm/ucrm-fe/commit/e66864bbed26028c01d6d60a07db0a1df353d05f))
- [Campaign] Change click add/remove account ([e679ba1](https://10.189.120.23/crm/ucrm-fe/commit/e679ba14d1c7f9460cab9c6dd805e1857ea4555b))
- [Comment] Add click link, open modal when click image ([a3c6b54](https://10.189.120.23/crm/ucrm-fe/commit/a3c6b548c63176ffcc075060858731269a3b2525))
- [Dynamic Button] Required fields ([5690bcb](https://10.189.120.23/crm/ucrm-fe/commit/5690bcb54071ea13007371496f1b521c2b2e4adb))
- [Email] Select template do not replace subject ([f11c999](https://10.189.120.23/crm/ucrm-fe/commit/f11c9995db1d43d5b47f23a400b21a894d88b607))
- [Email/SMS setting] Remove dup API ([73f27d5](https://10.189.120.23/crm/ucrm-fe/commit/73f27d5056c5d85f5c00fc7eda1a43caca5e95c9))
- [Rule Incoming Email] remove field type linkingobject with !key ([4e0c7ca](https://10.189.120.23/crm/ucrm-fe/commit/4e0c7ca8c54c7432d122c564a49a9d33ae656e10))
- [Rule incoming] Add select picklist ([61d4902](https://10.189.120.23/crm/ucrm-fe/commit/61d490273ad59ae7b8c53125f3ed62ff0b698a58))
- [Rule Incoming] Change linking type ([fa48cab](https://10.189.120.23/crm/ucrm-fe/commit/fa48cabee8a8b2b062622070b4ab63f99c7b9c0d))
- [Rule incoming] New UI new logic ([cdaaac9](https://10.189.120.23/crm/ucrm-fe/commit/cdaaac9bf8f8ec478a7af99523f8db20420024a4))
- [Rule Incoming] Type select ([c2078ed](https://10.189.120.23/crm/ucrm-fe/commit/c2078ed2519dacc0dee5e002cc13cd80425f25e0))
- [Timeline] add more icons interaction IC ([f9867b9](https://10.189.120.23/crm/ucrm-fe/commit/f9867b9db18e8aa36a0b626b9de200228f3ff683))
- [tronglv-fix] fix btn group list view, kanban, lvwdt, update nodata in kanban ([9446827](https://10.189.120.23/crm/ucrm-fe/commit/9446827d724afd5d42bd63b911140f5d4bbb3aa0))
- [tronglv-fix] fix bug 500 tenant root ([e887f35](https://10.189.120.23/crm/ucrm-fe/commit/e887f35ded0d26a26201a3bd78d66c34cef1f1f1))
- [tronglv-fix] fix bug translate ([b079757](https://10.189.120.23/crm/ucrm-fe/commit/b0797575a1bc7f475d27f465b5fc2450e477d105))
- [tronglv-fix] fix bug workflow schedule ([7b0ec6c](https://10.189.120.23/crm/ucrm-fe/commit/7b0ec6c53fe2c176c4197ac95913f4a0db6eeb89))
- [tronglv-fix] fix calendar, update customview, fix type light in kanban ([d97caa0](https://10.189.120.23/crm/ucrm-fe/commit/d97caa01828006d37b298c77231a1fdefbc54752))
- [tronglv-fix] fix call api in calendar ([e8ddbb4](https://10.189.120.23/crm/ucrm-fe/commit/e8ddbb45c4a57d94223557447f4ff0c21a0fb421))
- [tronglv-fix] fix language in list view, improvement report ([469ebed](https://10.189.120.23/crm/ucrm-fe/commit/469ebed041d9bc363d4322b8608cea2515126292))
- [tronglv-fix] hidden manmual sharing ([cb17e4c](https://10.189.120.23/crm/ucrm-fe/commit/cb17e4cb70fa0db1548842d45160437a14445610))
- [tronglv-fix] update language full project ([3550d65](https://10.189.120.23/crm/ucrm-fe/commit/3550d65c6748dd8f29d9fc004e79151e5ecddd1b))
- [tronglv-fix] update list view, manage you account ([45ece56](https://10.189.120.23/crm/ucrm-fe/commit/45ece560d3560d8707497381afff69355696bd57))
- [tronglv-fix] update pagination Kanban ([69d7e6b](https://10.189.120.23/crm/ucrm-fe/commit/69d7e6baaf2c3e79b47001009eaef664d909a991))
- [tronglv-fix] update pagination report ([f06f1ae](https://10.189.120.23/crm/ucrm-fe/commit/f06f1ae7ffa5ef344d29c2f18797c4b9b3b3bd1a))
- [tronglv-fix] update: page title, icon edit OBJ & layout,bg record hover ([44d0c37](https://10.189.120.23/crm/ucrm-fe/commit/44d0c37150755a14040cf2cef237914e86a93c86))
- [tronglv-language] Language calendar ([07664fd](https://10.189.120.23/crm/ucrm-fe/commit/07664fd67e60beeb0254d230498450aceedf50dd))
- [tronglv-update] delete related object ([2eefa25](https://10.189.120.23/crm/ucrm-fe/commit/2eefa255e78c160b33e1a6fe8f03a0be817e58f0))
- [tronglv-update] update another in list view, consolidated view, component SMS ([819ee49](https://10.189.120.23/crm/ucrm-fe/commit/819ee49a86cd199e516708a1a1f4ea4dbbdf489a))
- [tronglv-update] update collapse in object layout fields and hover in header ([3ca3515](https://10.189.120.23/crm/ucrm-fe/commit/3ca3515a873d64c7fc58a7542396c21c63831a55))
- [tronglv-update] update header object view ([ec52f2f](https://10.189.120.23/crm/ucrm-fe/commit/ec52f2fb7ae31f302fc1d45337fee470ba972045))
- [tronglv-update] update header object view and modal in dashboard ([da376a6](https://10.189.120.23/crm/ucrm-fe/commit/da376a6d134b86860ed2119ef234843b7c4dd5dc))
- [tronglv-update] update modal version ([3e4456f](https://10.189.120.23/crm/ucrm-fe/commit/3e4456f740a77d7f73d3d29ce234704365b2c0d0))
- [tronglv-update] update page title ([5cd024f](https://10.189.120.23/crm/ucrm-fe/commit/5cd024feefbea4fe21cba660a860920aa715bd36))
- [tronglv] fix conflict ([2d1c638](https://10.189.120.23/crm/ucrm-fe/commit/2d1c638b4a47d432e3c39bbb16ddf6f5cbc1ec46))
- [tronglv] fix conflict ([829ee7b](https://10.189.120.23/crm/ucrm-fe/commit/829ee7bd8719d31a02a1725a4b18e2e119a1a9f5))
- [WIP] new pagi report ([1019dda](https://10.189.120.23/crm/ucrm-fe/commit/1019dda81c3d5acfd58fa0eee17f2d89895b2260))
- add auto reload listview, component Details when trigger Dynamic button (action update/create) ([e7c13b8](https://10.189.120.23/crm/ucrm-fe/commit/e7c13b8e714759fd65ea9598c2aca1a30595874e))
- add check use cti before call get fields cti ([a2823cf](https://10.189.120.23/crm/ucrm-fe/commit/a2823cf82c56410776e85c6661bed385140cca29))
- add from name [Rule Email Incoming] ([110d5f7](https://10.189.120.23/crm/ucrm-fe/commit/110d5f72f48e017f674baf4f9929f0af84f83a7b))
- add metrics fields type details [Report setting] ([d984f63](https://10.189.120.23/crm/ucrm-fe/commit/d984f63165e2bf38f1307b931aff6e25fe3b7726))
- add new scroll in object, fix delete system field of campaign ([b6a0878](https://10.189.120.23/crm/ucrm-fe/commit/b6a087814ce7a45cd4f1804c1ad23db7c383e233))
- add reset all [IC / CTI] ([e5b3d12](https://10.189.120.23/crm/ucrm-fe/commit/e5b3d123672b6277240553f1b14b80719abb02df))
- add swicth source sharing ([4839c7b](https://10.189.120.23/crm/ucrm-fe/commit/4839c7b182179a08bacba06d3527a5e7434ea8c8))
- bumb version ([b4d5006](https://10.189.120.23/crm/ucrm-fe/commit/b4d50068fe8cb788175cbe7ced3a673df8566521))
- delete console log ([0233c2f](https://10.189.120.23/crm/ucrm-fe/commit/0233c2f00df1b798080d24e2ff860309e601f8f2))
- fix delete field linking value ([2404cc7](https://10.189.120.23/crm/ucrm-fe/commit/2404cc7a235088ac8501aa4c5d302cf84508840e))
- fix detail no data in consolidated view ([3c57e01](https://10.189.120.23/crm/ucrm-fe/commit/3c57e01d803810d6e0cbeb7de71c8117413b15d6))
- fix email template has been overwrite ([b9c21a5](https://10.189.120.23/crm/ucrm-fe/commit/b9c21a5d9b279b8abb61e67c03c27cd175d28c2c))
- fix linking ([6b4abc1](https://10.189.120.23/crm/ucrm-fe/commit/6b4abc163a9d5a9bc0e72d710872ac5f0b23e6fb))
- fix nodata in detail component ([072f01c](https://10.189.120.23/crm/ucrm-fe/commit/072f01c7a89136362445735304628939c5808a2e))
- fix search owner, pagination listview ([1585d89](https://10.189.120.23/crm/ucrm-fe/commit/1585d89ff38691f870098742c8ecee3d2efccc34))
- fix search, fix value compare in widget ([1f4f359](https://10.189.120.23/crm/ucrm-fe/commit/1f4f359cc72ae732fb6784094c13f90a46b2f834))
- fix type number + create report (measures) ([efea33b](https://10.189.120.23/crm/ucrm-fe/commit/efea33bffdd0974c10a66bd65f9fec2499d265d5))
- fix upload 50mb ([2cc9ef4](https://10.189.120.23/crm/ucrm-fe/commit/2cc9ef4ab576f49b17e6376037b740525a1253a6))
- hidden user filter ([c8f737f](https://10.189.120.23/crm/ucrm-fe/commit/c8f737f5920de5cd7413d6f7c956e3bca3506ee5))
- UI (SLA / Users) ([0fbe298](https://10.189.120.23/crm/ucrm-fe/commit/0fbe298ba94be92b6995a11328fce8e8eeceda6d))

### Others

- **release:** 2.23.0 ([31ece3a](https://10.189.120.23/crm/ucrm-fe/commit/31ece3a95beafd8dc2090a9ff82dc3b68ab6ca44))
- **release:** 2.23.1 ([64a2fde](https://10.189.120.23/crm/ucrm-fe/commit/64a2fde5ff736a5a284e302d020e40df7bc0fd2b))
- **release:** 2.23.2 ([f9c3b6e](https://10.189.120.23/crm/ucrm-fe/commit/f9c3b6e910870bee48e787c2b878bf5ebf71bc0e))
- **release:** 2.23.3 ([5e76ece](https://10.189.120.23/crm/ucrm-fe/commit/5e76ece842a9a900f441fc7395965207848ceac6))
- **release:** 2.23.4 ([11e1f73](https://10.189.120.23/crm/ucrm-fe/commit/11e1f736b8662d6e3d68fc8e5a205265f131fb91))
- **release:** 2.24.0 ([5d8f920](https://10.189.120.23/crm/ucrm-fe/commit/5d8f92057eaa9ec13c9db7ffb0036ad12c8e8963))
- **release:** 2.24.1 ([9a856f8](https://10.189.120.23/crm/ucrm-fe/commit/9a856f823e3fd9b5c668590298e54bce2dd56018))
- **release:** 2.25.0 ([d18e49c](https://10.189.120.23/crm/ucrm-fe/commit/d18e49c6b8e21645fe1d69f7e697b5a77659d736))

## [2.25.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.23.0...v2.25.0) (2022-11-14)

### Features

- [Workflow/Dynamic Button] Add action update related record source target ([69f0fdb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/69f0fdbbb016ae9d3b26ee156276b47029d26f0b))
- add manual tasks in SMS campaign (Campaign) ([fbbc4bf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fbbc4bf15f7318fe71a4c281a2d8267e5a5fd111))

### Bug Fixes

- [Action-UpdateSourceTarget] New conditions ([3b153d3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3b153d30e914793226dcfd6010a26bea8e0cc661))
- [Action-UpdateSourceTarget] Options field type linkingobject ([e66864b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e66864bbed26028c01d6d60a07db0a1df353d05f))
- [Campaign] Change click add/remove account ([e679ba1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e679ba14d1c7f9460cab9c6dd805e1857ea4555b))
- [Comment] Add click link, open modal when click image ([a3c6b54](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a3c6b548c63176ffcc075060858731269a3b2525))
- [Dynamic Button] Required fields ([5690bcb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5690bcb54071ea13007371496f1b521c2b2e4adb))
- [Email/SMS setting] Remove dup API ([73f27d5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73f27d5056c5d85f5c00fc7eda1a43caca5e95c9))
- [Rule Incoming Email] remove field type linkingobject with !key ([4e0c7ca](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4e0c7ca8c54c7432d122c564a49a9d33ae656e10))
- [Timeline] add more icons interaction IC ([f9867b9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f9867b9db18e8aa36a0b626b9de200228f3ff683))
- [tronglv-fix] fix btn group list view, kanban, lvwdt, update nodata in kanban ([9446827](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9446827d724afd5d42bd63b911140f5d4bbb3aa0))
- [tronglv-fix] fix bug 500 tenant root ([e887f35](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e887f35ded0d26a26201a3bd78d66c34cef1f1f1))
- [tronglv-fix] fix bug translate ([b079757](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b0797575a1bc7f475d27f465b5fc2450e477d105))
- [tronglv-fix] fix bug workflow schedule ([7b0ec6c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7b0ec6c53fe2c176c4197ac95913f4a0db6eeb89))
- [tronglv-fix] fix calendar, update customview, fix type light in kanban ([d97caa0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d97caa01828006d37b298c77231a1fdefbc54752))
- [tronglv-fix] fix call api in calendar ([e8ddbb4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e8ddbb45c4a57d94223557447f4ff0c21a0fb421))
- [tronglv-fix] fix language in list view, improvement report ([469ebed](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/469ebed041d9bc363d4322b8608cea2515126292))
- [tronglv-fix] hidden manmual sharing ([cb17e4c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cb17e4cb70fa0db1548842d45160437a14445610))
- [tronglv-fix] update language full project ([3550d65](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3550d65c6748dd8f29d9fc004e79151e5ecddd1b))
- [tronglv-fix] update list view, manage you account ([45ece56](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/45ece560d3560d8707497381afff69355696bd57))
- [tronglv-fix] update pagination Kanban ([69d7e6b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/69d7e6baaf2c3e79b47001009eaef664d909a991))
- [tronglv-fix] update pagination report ([f06f1ae](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f06f1ae7ffa5ef344d29c2f18797c4b9b3b3bd1a))
- [tronglv-fix] update: page title, icon edit OBJ & layout,bg record hover ([44d0c37](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/44d0c37150755a14040cf2cef237914e86a93c86))
- [tronglv-language] Language calendar ([07664fd](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/07664fd67e60beeb0254d230498450aceedf50dd))
- [tronglv-update] delete related object ([2eefa25](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2eefa255e78c160b33e1a6fe8f03a0be817e58f0))
- [tronglv-update] update another in list view, consolidated view, component SMS ([819ee49](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/819ee49a86cd199e516708a1a1f4ea4dbbdf489a))
- [tronglv-update] update collapse in object layout fields and hover in header ([3ca3515](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ca3515a873d64c7fc58a7542396c21c63831a55))
- [tronglv-update] update header object view ([ec52f2f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/ec52f2fb7ae31f302fc1d45337fee470ba972045))
- [tronglv-update] update header object view and modal in dashboard ([da376a6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/da376a6d134b86860ed2119ef234843b7c4dd5dc))
- [tronglv-update] update modal version ([3e4456f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3e4456f740a77d7f73d3d29ce234704365b2c0d0))
- [tronglv-update] update page title ([5cd024f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5cd024feefbea4fe21cba660a860920aa715bd36))
- [tronglv] fix conflict ([2d1c638](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2d1c638b4a47d432e3c39bbb16ddf6f5cbc1ec46))
- [tronglv] fix conflict ([829ee7b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/829ee7bd8719d31a02a1725a4b18e2e119a1a9f5))
- [WIP] new pagi report ([1019dda](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1019dda81c3d5acfd58fa0eee17f2d89895b2260))
- add auto reload listview, component Details when trigger Dynamic button (action update/create) ([e7c13b8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e7c13b8e714759fd65ea9598c2aca1a30595874e))
- add check use cti before call get fields cti ([a2823cf](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a2823cf82c56410776e85c6661bed385140cca29))
- add from name [Rule Email Incoming] ([110d5f7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/110d5f72f48e017f674baf4f9929f0af84f83a7b))
- add metrics fields type details [Report setting] ([d984f63](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d984f63165e2bf38f1307b931aff6e25fe3b7726))
- add new scroll in object, fix delete system field of campaign ([b6a0878](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b6a087814ce7a45cd4f1804c1ad23db7c383e233))
- add reset all [IC / CTI] ([e5b3d12](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e5b3d123672b6277240553f1b14b80719abb02df))
- add swicth source sharing ([4839c7b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4839c7b182179a08bacba06d3527a5e7434ea8c8))
- delete console log ([0233c2f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0233c2f00df1b798080d24e2ff860309e601f8f2))
- fix delete field linking value ([2404cc7](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2404cc7a235088ac8501aa4c5d302cf84508840e))
- fix detail no data in consolidated view ([3c57e01](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3c57e01d803810d6e0cbeb7de71c8117413b15d6))
- fix email template has been overwrite ([b9c21a5](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b9c21a5d9b279b8abb61e67c03c27cd175d28c2c))
- fix linking ([6b4abc1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6b4abc163a9d5a9bc0e72d710872ac5f0b23e6fb))
- fix nodata in detail component ([072f01c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/072f01c7a89136362445735304628939c5808a2e))
- fix search owner, pagination listview ([1585d89](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1585d89ff38691f870098742c8ecee3d2efccc34))
- fix search, fix value compare in widget ([1f4f359](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1f4f359cc72ae732fb6784094c13f90a46b2f834))
- fix type number + create report (measures) ([efea33b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/efea33bffdd0974c10a66bd65f9fec2499d265d5))
- fix upload 50mb ([2cc9ef4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2cc9ef4ab576f49b17e6376037b740525a1253a6))
- hidden user filter ([c8f737f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c8f737f5920de5cd7413d6f7c956e3bca3506ee5))
- UI (SLA / Users) ([0fbe298](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0fbe298ba94be92b6995a11328fce8e8eeceda6d))

### Others

- **release:** 2.23.1 ([64a2fde](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/64a2fde5ff736a5a284e302d020e40df7bc0fd2b))
- **release:** 2.23.2 ([f9c3b6e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f9c3b6e910870bee48e787c2b878bf5ebf71bc0e))
- **release:** 2.23.3 ([5e76ece](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5e76ece842a9a900f441fc7395965207848ceac6))
- **release:** 2.23.4 ([11e1f73](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/11e1f736b8662d6e3d68fc8e5a205265f131fb91))
- **release:** 2.24.0 ([5d8f920](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5d8f92057eaa9ec13c9db7ffb0036ad12c8e8963))
- **release:** 2.24.1 ([9a856f8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9a856f823e3fd9b5c668590298e54bce2dd56018))

### [2.24.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.24.0...v2.24.1) (2022-11-10)

### Bug Fixes

- [Action-UpdateSourceTarget] New conditions ([3b153d3](https://10.189.120.23/crm/ucrm-fe/commit/3b153d30e914793226dcfd6010a26bea8e0cc661))
- [Action-UpdateSourceTarget] Options field type linkingobject ([e66864b](https://10.189.120.23/crm/ucrm-fe/commit/e66864bbed26028c01d6d60a07db0a1df353d05f))
- [Campaign] Change click add/remove account ([e679ba1](https://10.189.120.23/crm/ucrm-fe/commit/e679ba14d1c7f9460cab9c6dd805e1857ea4555b))
- [Dynamic Button] Required fields ([5690bcb](https://10.189.120.23/crm/ucrm-fe/commit/5690bcb54071ea13007371496f1b521c2b2e4adb))
- [Timeline] add more icons interaction IC ([f9867b9](https://10.189.120.23/crm/ucrm-fe/commit/f9867b9db18e8aa36a0b626b9de200228f3ff683))
- [tronglv-fix] fix btn group list view, kanban, lvwdt, update nodata in kanban ([9446827](https://10.189.120.23/crm/ucrm-fe/commit/9446827d724afd5d42bd63b911140f5d4bbb3aa0))
- [tronglv-fix] fix bug 500 tenant root ([e887f35](https://10.189.120.23/crm/ucrm-fe/commit/e887f35ded0d26a26201a3bd78d66c34cef1f1f1))
- [tronglv-update] delete related object ([2eefa25](https://10.189.120.23/crm/ucrm-fe/commit/2eefa255e78c160b33e1a6fe8f03a0be817e58f0))
- [tronglv] fix conflict ([829ee7b](https://10.189.120.23/crm/ucrm-fe/commit/829ee7bd8719d31a02a1725a4b18e2e119a1a9f5))
- fix delete field linking value ([2404cc7](https://10.189.120.23/crm/ucrm-fe/commit/2404cc7a235088ac8501aa4c5d302cf84508840e))
- fix linking ([6b4abc1](https://10.189.120.23/crm/ucrm-fe/commit/6b4abc163a9d5a9bc0e72d710872ac5f0b23e6fb))

## [2.24.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.4...v2.24.0) (2022-11-08)

### Features

- [Workflow/Dynamic Button] Add action update related record source target ([69f0fdb](https://10.189.120.23/crm/ucrm-fe/commit/69f0fdbbb016ae9d3b26ee156276b47029d26f0b))
- add manual tasks in SMS campaign (Campaign) ([fbbc4bf](https://10.189.120.23/crm/ucrm-fe/commit/fbbc4bf15f7318fe71a4c281a2d8267e5a5fd111))

### Bug Fixes

- [Comment] Add click link, open modal when click image ([a3c6b54](https://10.189.120.23/crm/ucrm-fe/commit/a3c6b548c63176ffcc075060858731269a3b2525))
- [Email/SMS setting] Remove dup API ([73f27d5](https://10.189.120.23/crm/ucrm-fe/commit/73f27d5056c5d85f5c00fc7eda1a43caca5e95c9))
- [Rule Incoming Email] remove field type linkingobject with !key ([4e0c7ca](https://10.189.120.23/crm/ucrm-fe/commit/4e0c7ca8c54c7432d122c564a49a9d33ae656e10))
- [tronglv-fix] fix language in list view, improvement report ([469ebed](https://10.189.120.23/crm/ucrm-fe/commit/469ebed041d9bc363d4322b8608cea2515126292))
- [tronglv-update] update header object view ([ec52f2f](https://10.189.120.23/crm/ucrm-fe/commit/ec52f2fb7ae31f302fc1d45337fee470ba972045))
- [tronglv-update] update header object view and modal in dashboard ([da376a6](https://10.189.120.23/crm/ucrm-fe/commit/da376a6d134b86860ed2119ef234843b7c4dd5dc))
- [tronglv-update] update page title ([5cd024f](https://10.189.120.23/crm/ucrm-fe/commit/5cd024feefbea4fe21cba660a860920aa715bd36))
- add swicth source sharing ([4839c7b](https://10.189.120.23/crm/ucrm-fe/commit/4839c7b182179a08bacba06d3527a5e7434ea8c8))
- fix upload 50mb ([2cc9ef4](https://10.189.120.23/crm/ucrm-fe/commit/2cc9ef4ab576f49b17e6376037b740525a1253a6))

### [2.23.4](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.3...v2.23.4) (2022-11-03)

### Bug Fixes

- fix detail no data in consolidated view ([3c57e01](https://10.189.120.23/crm/ucrm-fe/commit/3c57e01d803810d6e0cbeb7de71c8117413b15d6))
- fix search, fix value compare in widget ([1f4f359](https://10.189.120.23/crm/ucrm-fe/commit/1f4f359cc72ae732fb6784094c13f90a46b2f834))

### [2.23.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.2...v2.23.3) (2022-11-01)

### Bug Fixes

- [tronglv-fix] hidden manmual sharing ([cb17e4c](https://10.189.120.23/crm/ucrm-fe/commit/cb17e4cb70fa0db1548842d45160437a14445610))
- [tronglv-fix] update language full project ([3550d65](https://10.189.120.23/crm/ucrm-fe/commit/3550d65c6748dd8f29d9fc004e79151e5ecddd1b))
- [tronglv-fix] update pagination Kanban ([69d7e6b](https://10.189.120.23/crm/ucrm-fe/commit/69d7e6baaf2c3e79b47001009eaef664d909a991))
- [tronglv-fix] update pagination report ([f06f1ae](https://10.189.120.23/crm/ucrm-fe/commit/f06f1ae7ffa5ef344d29c2f18797c4b9b3b3bd1a))
- [tronglv-update] update another in list view, consolidated view, component SMS ([819ee49](https://10.189.120.23/crm/ucrm-fe/commit/819ee49a86cd199e516708a1a1f4ea4dbbdf489a))
- [WIP] new pagi report ([1019dda](https://10.189.120.23/crm/ucrm-fe/commit/1019dda81c3d5acfd58fa0eee17f2d89895b2260))
- add new scroll in object, fix delete system field of campaign ([b6a0878](https://10.189.120.23/crm/ucrm-fe/commit/b6a087814ce7a45cd4f1804c1ad23db7c383e233))
- fix search owner, pagination listview ([1585d89](https://10.189.120.23/crm/ucrm-fe/commit/1585d89ff38691f870098742c8ecee3d2efccc34))
- hidden user filter ([c8f737f](https://10.189.120.23/crm/ucrm-fe/commit/c8f737f5920de5cd7413d6f7c956e3bca3506ee5))

### [2.23.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.1...v2.23.2) (2022-10-26)

### Bug Fixes

- [tronglv-fix] fix bug workflow schedule ([7b0ec6c](https://10.189.120.23/crm/ucrm-fe/commit/7b0ec6c53fe2c176c4197ac95913f4a0db6eeb89))
- [tronglv-fix] fix call api in calendar ([e8ddbb4](https://10.189.120.23/crm/ucrm-fe/commit/e8ddbb45c4a57d94223557447f4ff0c21a0fb421))
- [tronglv-fix] update list view, manage you account ([45ece56](https://10.189.120.23/crm/ucrm-fe/commit/45ece560d3560d8707497381afff69355696bd57))
- [tronglv-language] Language calendar ([07664fd](https://10.189.120.23/crm/ucrm-fe/commit/07664fd67e60beeb0254d230498450aceedf50dd))
- add auto reload listview, component Details when trigger Dynamic button (action update/create) ([e7c13b8](https://10.189.120.23/crm/ucrm-fe/commit/e7c13b8e714759fd65ea9598c2aca1a30595874e))
- add from name [Rule Email Incoming] ([110d5f7](https://10.189.120.23/crm/ucrm-fe/commit/110d5f72f48e017f674baf4f9929f0af84f83a7b))
- add metrics fields type details [Report setting] ([d984f63](https://10.189.120.23/crm/ucrm-fe/commit/d984f63165e2bf38f1307b931aff6e25fe3b7726))
- delete console log ([0233c2f](https://10.189.120.23/crm/ucrm-fe/commit/0233c2f00df1b798080d24e2ff860309e601f8f2))
- fix email template has been overwrite ([b9c21a5](https://10.189.120.23/crm/ucrm-fe/commit/b9c21a5d9b279b8abb61e67c03c27cd175d28c2c))
- fix type number + create report (measures) ([efea33b](https://10.189.120.23/crm/ucrm-fe/commit/efea33bffdd0974c10a66bd65f9fec2499d265d5))

### [2.23.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.7...v2.23.1) (2022-10-20)

### Bug Fixes

- [tronglv-fix] fix calendar, update customview, fix type light in kanban ([d97caa0](https://10.189.120.23/crm/ucrm-fe/commit/d97caa01828006d37b298c77231a1fdefbc54752))
- add check use cti before call get fields cti ([a2823cf](https://10.189.120.23/crm/ucrm-fe/commit/a2823cf82c56410776e85c6661bed385140cca29))
- add reset all [IC / CTI] ([e5b3d12](https://10.189.120.23/crm/ucrm-fe/commit/e5b3d123672b6277240553f1b14b80719abb02df))
- fix nodata in detail component ([072f01c](https://10.189.120.23/crm/ucrm-fe/commit/072f01c7a89136362445735304628939c5808a2e))
- UI (SLA / Users) ([0fbe298](https://10.189.120.23/crm/ucrm-fe/commit/0fbe298ba94be92b6995a11328fce8e8eeceda6d))

### Others

- **release:** 2.23.0 ([31ece3a](https://10.189.120.23/crm/ucrm-fe/commit/31ece3a95beafd8dc2090a9ff82dc3b68ab6ca44))

## [2.23.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.20.0...v2.23.0) (2022-10-19)

### Features

- [Emails component] Add auto detect email ([a3f20bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a3f20bcb09a88f7e1686cf83cf71fac66df3a00d))
- [tronglv-feature] component SMS ([51525d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/51525d94bae4c03931a8f1f80971232fee9a1a92))

### Bug Fixes

- [Action workflow/dynamic] Hide select record lookup fields ([cd43400](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cd434000f61aafbb4504c9536ed5afcb3159511d))
- [Campaign] Add check required fields ([73fdb2b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73fdb2b867a685c60a188c4080d03eb850ad49c3))
- [Campaign] Add nodata mailchimp campaign; Fix reload template SMS ([179f820](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/179f82094211898552a76206a440105cf1d9f6ad))
- [Campaign] Text ([3377d15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3377d157a0f50495488337910370dc46a2b09bd9))
- [Comments] Lower case search ([3ce41b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ce41b09a5382315fd5a7e5df064a3fdf46a0ea1))
- [Email Incoming] Add select folder scan ([91a3a94](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/91a3a94aa58c02bdaf5b172d42e04d162f6c19a0))
- [Notifications] Limit noti ([bd00689](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bd006896b3056a5e368b151d0e933ae45d481c8c))
- [RoleBaseGuard] Rule 403 ([c8b82d1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c8b82d1e5078ed9bc3b1f096c7b0d54b9f956090))
- [Roles] Add expand all roles by default ([a86f37b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a86f37b82e51edf6e48cbe1304b6c0c46db154cb))
- [Sider] Fix collapse group menu ([bc46f63](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bc46f631c3317bcf515fa41d53894d8f46ccca8b))
- [SMS template] Duplicate fields meta data ([6c2ba17](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6c2ba17a521f6ffc90b4a69b278a434df74dff74))
- [Timeline] Add email IC; [Email] 500 when select template, nodata when filter ([37aea26](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/37aea2666d5607c56dc6bddc98ccd1942561b7a1))
- [tronglv-fix] edit component SMS ([0ce9380](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0ce93806c63bc2917449f235731e557ba53d05fd))
- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- [tronglv-fix] update duplicate rule ([80683f1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/80683f1a6069444047e7c1714352b23fab46bbd2))
- [tronglv-fix] update kanban view, list view, list view with detail, manual sharing ([754771f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/754771fddc7ddf3bfdd93c8fa75889d4ea197ce7))
- [tronglv-fixbug] fix bug kanban ([c97580a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c97580abfa18d163500362b3a35bb8672a5eb8ca))
- [URL Popup setting] Add display input params ([f556c98](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f556c9871aff9bb01632b74d87a75d30cfbfcf3f))
- [Users] Add quick filter ([4e0b4d2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4e0b4d214e8c62f1746ff9e7800306b24df33f08))
- [Widget] Add responsive widget in Dashboard ([4450c60](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4450c60193be29ee99a8bbc955a48765ce9c6cfe))
- add modal check scan ([a4b5b85](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a4b5b8534c6d35a60487c083b471b9eadfb2b776))
- add new version page ([4955e18](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4955e189eb077099f7bf40f26aea363ac71cc30c))
- change API mass delete campaign ([fbe5b81](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change celery check ([9860e75](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9860e75fa1e1bd553643c7975b9434f0a151445f))
- change flow find auto detect email ([283c87f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- change modal folder scan [Email Incoming] ([07b9665](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/07b966597974640918eaf50e76053b9efaad4ddf))
- current page search users ([c43aeec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c43aeecad9549ff353eb25799644cc875c7cc2b5))
- display JSON modal Check scan ([1ef0dde](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1ef0ddedcd2c54678036cf244e24c9fe1a0e1f85))
- fix file in table of consolidated view ([36132f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36132f0d4f9fd2e04f51b95e9616295ee83a6aa0))
- fix linking ([d3b46e8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3b46e8fd78e41b1aef405445d4b032a08577368))
- fix linking ([2ec564c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2ec564ccb9272e7e5b8a7183b66e1f2e84563421))
- fix table in consolidated view ([8314c1c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8314c1c43a9d365495363ba52179495222ab04e4))
- fix UI Comment ([f9c2ece](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f9c2ece9bd37f5062b00d8abcb812327c9780de6))
- fix user in detail consolidated view ([8948adb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8948adbb18275a7599f7a7b13fde0b2191466d79))
- improve performance Pagination for listview ([5851681](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5851681c1e371e73142c545b398f6a6f8cb492d0))
- pagination new in consolidated view ([73c593b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73c593b4cc2546789d44fa302ca61a66d75f0839))
- revert style widget ([a398401](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### Code Refactoring

- [Email] Change action email from dropdown to icons ([9bdd392](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9bdd3924c323c5cee78414f9991306da77b154f9))
- [Report] Change style of widgets and charts ([5ad68aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5ad68aa9c3832a9ea4d872f18971b3ca8e573ea2))

### Others

- **release:** 2.21.0 ([2f8764b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f8764b6740dafeba5c25a5467600b06765d4be4))
- **release:** 2.22.0 ([1b0df3e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1b0df3ec806313d455cce3aa5cbdfe8b1eb4fcf8))
- **release:** 2.22.1 ([172726a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/172726a2150bd2151782703bfe362865de53351c))
- **release:** 2.22.2 ([c982d8a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c982d8a1ee89caac7431eb31820d9a5496fffe2c))
- **release:** 2.22.3 ([5465250](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5465250fc3a157ca3b3b53a11029edc7a8990db6))
- **release:** 2.22.4 ([bc1475c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bc1475cfbdc27247983f1ef058b7fcf547540f61))
- **release:** 2.22.5 ([bd873e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bd873e61d49c879c8e211d0677fc1555f77a1610))
- **release:** 2.22.6 ([36341a0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36341a08c43fbb9c10ebac86e4438a1d30a7c6e5))
- **release:** 2.22.7 ([4a4074e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4a4074ef7dbba3d4386a2f8930e90c4eaf76a741))

### [2.22.7](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.6...v2.22.7) (2022-10-18)

### Bug Fixes

- change modal folder scan [Email Incoming] ([07b9665](https://10.189.120.23/crm/ucrm-fe/commit/07b966597974640918eaf50e76053b9efaad4ddf))
- fix user in detail consolidated view ([8948adb](https://10.189.120.23/crm/ucrm-fe/commit/8948adbb18275a7599f7a7b13fde0b2191466d79))

### [2.22.6](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.5...v2.22.6) (2022-10-18)

### [2.22.5](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.4...v2.22.5) (2022-10-17)

### Bug Fixes

- [Email Incoming] Add select folder scan ([91a3a94](https://10.189.120.23/crm/ucrm-fe/commit/91a3a94aa58c02bdaf5b172d42e04d162f6c19a0))
- add modal check scan ([a4b5b85](https://10.189.120.23/crm/ucrm-fe/commit/a4b5b8534c6d35a60487c083b471b9eadfb2b776))
- change celery check ([9860e75](https://10.189.120.23/crm/ucrm-fe/commit/9860e75fa1e1bd553643c7975b9434f0a151445f))
- current page search users ([c43aeec](https://10.189.120.23/crm/ucrm-fe/commit/c43aeecad9549ff353eb25799644cc875c7cc2b5))
- display JSON modal Check scan ([1ef0dde](https://10.189.120.23/crm/ucrm-fe/commit/1ef0ddedcd2c54678036cf244e24c9fe1a0e1f85))
- fix linking ([d3b46e8](https://10.189.120.23/crm/ucrm-fe/commit/d3b46e8fd78e41b1aef405445d4b032a08577368))

### [2.22.4](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.3...v2.22.4) (2022-10-13)

### Bug Fixes

- [Timeline] Add email IC; [Email] 500 when select template, nodata when filter ([37aea26](https://10.189.120.23/crm/ucrm-fe/commit/37aea2666d5607c56dc6bddc98ccd1942561b7a1))
- [tronglv-fix] update kanban view, list view, list view with detail, manual sharing ([754771f](https://10.189.120.23/crm/ucrm-fe/commit/754771fddc7ddf3bfdd93c8fa75889d4ea197ce7))
- [tronglv-fixbug] fix bug kanban ([c97580a](https://10.189.120.23/crm/ucrm-fe/commit/c97580abfa18d163500362b3a35bb8672a5eb8ca))
- fix file in table of consolidated view ([36132f0](https://10.189.120.23/crm/ucrm-fe/commit/36132f0d4f9fd2e04f51b95e9616295ee83a6aa0))
- fix UI Comment ([f9c2ece](https://10.189.120.23/crm/ucrm-fe/commit/f9c2ece9bd37f5062b00d8abcb812327c9780de6))

### [2.22.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.1...v2.22.3) (2022-10-12)

### Bug Fixes

- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://10.189.120.23/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- [tronglv-fix] update duplicate rule ([80683f1](https://10.189.120.23/crm/ucrm-fe/commit/80683f1a6069444047e7c1714352b23fab46bbd2))
- add new version page ([4955e18](https://10.189.120.23/crm/ucrm-fe/commit/4955e189eb077099f7bf40f26aea363ac71cc30c))
- change API mass delete campaign ([fbe5b81](https://10.189.120.23/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change flow find auto detect email ([283c87f](https://10.189.120.23/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- fix table in consolidated view ([8314c1c](https://10.189.120.23/crm/ucrm-fe/commit/8314c1c43a9d365495363ba52179495222ab04e4))
- revert style widget ([a398401](https://10.189.120.23/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://10.189.120.23/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### Others

- **release:** 2.22.2 ([c982d8a](https://10.189.120.23/crm/ucrm-fe/commit/c982d8a1ee89caac7431eb31820d9a5496fffe2c))

### [2.22.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.1...v2.22.2) (2022-10-11)

### Bug Fixes

- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://10.189.120.23/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- change API mass delete campaign ([fbe5b81](https://10.189.120.23/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change flow find auto detect email ([283c87f](https://10.189.120.23/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- revert style widget ([a398401](https://10.189.120.23/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://10.189.120.23/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### [2.22.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.0...v2.22.1) (2022-10-07)

### Bug Fixes

- [Action workflow/dynamic] Hide select record lookup fields ([cd43400](https://10.189.120.23/crm/ucrm-fe/commit/cd434000f61aafbb4504c9536ed5afcb3159511d))
- [Campaign] Add check required fields ([73fdb2b](https://10.189.120.23/crm/ucrm-fe/commit/73fdb2b867a685c60a188c4080d03eb850ad49c3))
- [tronglv-fix] edit component SMS ([0ce9380](https://10.189.120.23/crm/ucrm-fe/commit/0ce93806c63bc2917449f235731e557ba53d05fd))
- [URL Popup setting] Add display input params ([f556c98](https://10.189.120.23/crm/ucrm-fe/commit/f556c9871aff9bb01632b74d87a75d30cfbfcf3f))
- [Widget] Add responsive widget in Dashboard ([4450c60](https://10.189.120.23/crm/ucrm-fe/commit/4450c60193be29ee99a8bbc955a48765ce9c6cfe))
- fix linking ([2ec564c](https://10.189.120.23/crm/ucrm-fe/commit/2ec564ccb9272e7e5b8a7183b66e1f2e84563421))

## [2.22.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.21.0...v2.22.0) (2022-10-06)

### Features

- [Emails component] Add auto detect email ([a3f20bc](https://10.189.120.23/crm/ucrm-fe/commit/a3f20bcb09a88f7e1686cf83cf71fac66df3a00d))
- [tronglv-feature] component SMS ([51525d9](https://10.189.120.23/crm/ucrm-fe/commit/51525d94bae4c03931a8f1f80971232fee9a1a92))

### Bug Fixes

- [Campaign] Add nodata mailchimp campaign; Fix reload template SMS ([179f820](https://10.189.120.23/crm/ucrm-fe/commit/179f82094211898552a76206a440105cf1d9f6ad))
- [Comments] Lower case search ([3ce41b0](https://10.189.120.23/crm/ucrm-fe/commit/3ce41b09a5382315fd5a7e5df064a3fdf46a0ea1))
- [Notifications] Limit noti ([bd00689](https://10.189.120.23/crm/ucrm-fe/commit/bd006896b3056a5e368b151d0e933ae45d481c8c))
- [RoleBaseGuard] Rule 403 ([c8b82d1](https://10.189.120.23/crm/ucrm-fe/commit/c8b82d1e5078ed9bc3b1f096c7b0d54b9f956090))
- [Roles] Add expand all roles by default ([a86f37b](https://10.189.120.23/crm/ucrm-fe/commit/a86f37b82e51edf6e48cbe1304b6c0c46db154cb))
- [Sider] Fix collapse group menu ([bc46f63](https://10.189.120.23/crm/ucrm-fe/commit/bc46f631c3317bcf515fa41d53894d8f46ccca8b))
- [SMS template] Duplicate fields meta data ([6c2ba17](https://10.189.120.23/crm/ucrm-fe/commit/6c2ba17a521f6ffc90b4a69b278a434df74dff74))
- [Users] Add quick filter ([4e0b4d2](https://10.189.120.23/crm/ucrm-fe/commit/4e0b4d214e8c62f1746ff9e7800306b24df33f08))
- pagination new in consolidated view ([73c593b](https://10.189.120.23/crm/ucrm-fe/commit/73c593b4cc2546789d44fa302ca61a66d75f0839))

## [2.21.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.20.0...v2.21.0) (2022-10-05)

### Features

- [tronglv-feature] Manual sharing ([e41cb3a](https://10.189.120.23/crm/ucrm-fe/commit/e41cb3aeedbf7805e309474c37cd87bb9ca664fc))
- [tronglv-feature] Manual sharing ([216fc8d](https://10.189.120.23/crm/ucrm-fe/commit/216fc8d9bbf1c5908a3e4fc7004e84454f0ba2d2))
- campaign with mailchimp ([9c87a87](https://10.189.120.23/crm/ucrm-fe/commit/9c87a874ac2dcb94c95728b6ebc920b93ea80157))

### Bug Fixes

- (Action update related record) Select value linking ([647ab4e](https://10.189.120.23/crm/ucrm-fe/commit/647ab4ed85599833206db9b132b4c2cc01bb45d9))
- (Charts) Add no data Pie/Line chart ([bc7c7cc](https://10.189.120.23/crm/ucrm-fe/commit/bc7c7ccd7f598ed6e3f055b607e38d4a92c539dc))
- (Expose API create/update) Add copy payload ([0a8f380](https://10.189.120.23/crm/ucrm-fe/commit/0a8f380cf43f5cae1f1f68da430dd235a8576d4f))
- (SMS Template) display object ID when edit template ([72dcbf2](https://10.189.120.23/crm/ucrm-fe/commit/72dcbf2ddc4f85903e20a334e1da3f9a20bc1497))
- [Campaign] Text ([3377d15](https://10.189.120.23/crm/ucrm-fe/commit/3377d157a0f50495488337910370dc46a2b09bd9))
- add check object null ([fb4a4a6](https://10.189.120.23/crm/ucrm-fe/commit/fb4a4a63e061b90f5a19e471c4c939843e7b49fb))
- call API load pagi paralel ([acfea69](https://10.189.120.23/crm/ucrm-fe/commit/acfea690ef334a1d7b180737f55e5ffb58d92739))
- file env ([4e1e635](https://10.189.120.23/crm/ucrm-fe/commit/4e1e635e8a0c37ebfda9c467a9d279220c2364e2))
- improve performance Pagination for listview ([5851681](https://10.189.120.23/crm/ucrm-fe/commit/5851681c1e371e73142c545b398f6a6f8cb492d0))
- load active campaign ([3969eea](https://10.189.120.23/crm/ucrm-fe/commit/3969eea7eb04c321e3759eadbf771a34df0f0844))
- redirect noti ([e6953db](https://10.189.120.23/crm/ucrm-fe/commit/e6953dbe92e441802febb89822001a1d300ffacd))
- required conditions SLA ([f1cf533](https://10.189.120.23/crm/ucrm-fe/commit/f1cf533ecee447d45fa8d6f78f22e1c591341c6e))
- required field has change workflows ([ec20e2d](https://10.189.120.23/crm/ucrm-fe/commit/ec20e2d3b6feadc3ea49efb3f4d144d5a333e3d7))

### Code Refactoring

- [Email] Change action email from dropdown to icons ([9bdd392](https://10.189.120.23/crm/ucrm-fe/commit/9bdd3924c323c5cee78414f9991306da77b154f9))
- [Report] Change style of widgets and charts ([5ad68aa](https://10.189.120.23/crm/ucrm-fe/commit/5ad68aa9c3832a9ea4d872f18971b3ca8e573ea2))
- change style of component Email ([69eaee8](https://10.189.120.23/crm/ucrm-fe/commit/69eaee8ab148eecbf6b8830aae795e21cb10f7c0))

## [2.20.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.19.0...v2.20.0) (2022-09-28)

### Features

- [tronglv-feature] Workflow - Action thêm event vào calendar ([470ab1c](https://10.189.120.23/crm/ucrm-fe/commit/470ab1c3dba2e27b1a2a538c9ec011e72777ddd0))

### Bug Fixes

- add required from config email ([b6cf223](https://10.189.120.23/crm/ucrm-fe/commit/b6cf223253174605f4b9d49bdb85277d1c3f871b))
- add type file in mapping IC and Expose API create ([959c2d6](https://10.189.120.23/crm/ucrm-fe/commit/959c2d66a090e9dcf869d23a4144acb14daf415d))
- clear related object (Action workflow) ([d22a53b](https://10.189.120.23/crm/ucrm-fe/commit/d22a53b9c889662c99289671d7c3757097356fad))
- edit action update related record 500 ([efb59e2](https://10.189.120.23/crm/ucrm-fe/commit/efb59e21da4d94218b02367daf0d77f968aebb98))
- query user in comment ([0cb2c2a](https://10.189.120.23/crm/ucrm-fe/commit/0cb2c2a93362a34d864fdd26611414bdd42087d8))

## [2.19.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.18.1...v2.19.0) (2022-09-23)

### Features

- new campaign ([cc7992b](https://10.189.120.23/crm/ucrm-fe/commit/cc7992bb8b547c43beb4ef49ae1870c2225db50b))
- open component campaign ([709d624](https://10.189.120.23/crm/ucrm-fe/commit/709d6249b656dbc44647531248e7e7f479df62de))

### Bug Fixes

- change BASE URL API ([8f59087](https://10.189.120.23/crm/ucrm-fe/commit/8f59087358715cc45d8eff1e3103dab0e5d218e7))
- hot fix page 500 in lisview because highlighter ([2f12ff1](https://10.189.120.23/crm/ucrm-fe/commit/2f12ff1a44fc2a539d300cbd6efaa1de9267e0fb))

### [2.18.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.18.0...v2.18.1) (2022-09-22)

### Bug Fixes

- fix 403 forbiden ([7252c82](https://10.189.120.23/crm/ucrm-fe/commit/7252c828d8896aa0c6534a5730eb7b3e852f7428))
- fix bug can not copy content in third party table ([6c7d3c2](https://10.189.120.23/crm/ucrm-fe/commit/6c7d3c20bbeba7463df867c52b688b46ee229baa))

## [2.18.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.17.1...v2.18.0) (2022-09-22)

### Features

- action update related record ([31753c0](https://10.189.120.23/crm/ucrm-fe/commit/31753c01dda6340b862c1fc7190465c86ddc82d0))

### Bug Fixes

- [tronglv-fix bug] fix bug kanban view ([a12fe71](https://10.189.120.23/crm/ucrm-fe/commit/a12fe719074fbcfbf2d3dbdb61e0b95dd74363d0))
- add fix config sso ([739a1bd](https://10.189.120.23/crm/ucrm-fe/commit/739a1bd9b4d741f2f4b06c43d7518f48d593e35b))
- add highlight words after search in listview ([62b3293](https://10.189.120.23/crm/ucrm-fe/commit/62b329327eeecad5df64f7e677ce3fb8f42ca4db))
- hot fix file type upload ([4ffdfe0](https://10.189.120.23/crm/ucrm-fe/commit/4ffdfe0dca26ec95d5ae0d83295fa00911075322))

### [2.17.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.17.0...v2.17.1) (2022-09-21)

### Bug Fixes

- hot fix sso config ([40e77d7](https://10.189.120.23/crm/ucrm-fe/commit/40e77d73108e93bb95bc174afbab567864475fb5))

## [2.17.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.14.0...v2.17.0) (2022-09-20)

### Features

- add duplicate rule SLA ([d23cfc0](https://10.189.120.23/crm/ucrm-fe/commit/d23cfc0516a6e6b7c8f96cff9de6d06d33ea9b80))
- add on/off linking ([82c64f0](https://10.189.120.23/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- click to call in listview ([0bc8031](https://10.189.120.23/crm/ucrm-fe/commit/0bc8031df3ba67c4ca27cc8339d6021e440143e9))

### Bug Fixes

- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://10.189.120.23/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://10.189.120.23/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://10.189.120.23/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://10.189.120.23/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-fix bug] Bị nhảy về trang listview Default sau khi chọn Search data có Custom View ([2b04778](https://10.189.120.23/crm/ucrm-fe/commit/2b04778ae9896bdce1dd52b3384376e08462be56))
- [tronglv-fix bug] Bug UI ([62d1c16](https://10.189.120.23/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix bug] fix bug consolidated view and update search meta list view ([13de2fb](https://10.189.120.23/crm/ucrm-fe/commit/13de2fb488ef587e4f1d1e95337ac181fb725546))
- [tronglv-fix/ui]update consolidated view setting ([b45018c](https://10.189.120.23/crm/ucrm-fe/commit/b45018c46abc9d9898a020e85c03dd57d81fd326))
- [tronglv-language] update language SLA setting ([2b5d1ea](https://10.189.120.23/crm/ucrm-fe/commit/2b5d1ea5eb675029408695146383d84a3c9e3983))
- [tronglv-language] update language Workflow ([90d3e0e](https://10.189.120.23/crm/ucrm-fe/commit/90d3e0ee32c9aa4327f77fb08b6a6ba48bde9446))
- add type when pin chart; remove formula date diff with in work ([6d63b71](https://10.189.120.23/crm/ucrm-fe/commit/6d63b71feec00cef26fd6d3594f89df53d93924f))
- arimetic one line; Number with commas; Height (Report) ([a264632](https://10.189.120.23/crm/ucrm-fe/commit/a26463298135f6ae4eefc458c48fff2b310429da))
- clear value when change field in widget ([7aa2e7c](https://10.189.120.23/crm/ucrm-fe/commit/7aa2e7c89b50655f85f5b4d17115c8d8647efdba))
- dynamic button display wrong object (Consolidated View); clear value chart ([8d27a5d](https://10.189.120.23/crm/ucrm-fe/commit/8d27a5d2feca7cd6d5516f4ec077e5f9bf11d508))
- fix bug linking ([fed6c87](https://10.189.120.23/crm/ucrm-fe/commit/fed6c8739e65b9e3ebe72fab81fd2fa64cfafd96))
- fix bug linking final ([4a1e415](https://10.189.120.23/crm/ucrm-fe/commit/4a1e415ce46e84985e7758e06e29b3a82c548164))
- fix condition ([3aab316](https://10.189.120.23/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://10.189.120.23/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- fix format date ([7646f93](https://10.189.120.23/crm/ucrm-fe/commit/7646f93fec4ecc1f07327872aeac16f652135b71))
- fix layout dashboard and sonarque ([b473445](https://10.189.120.23/crm/ucrm-fe/commit/b47344568709a51b1ceb45c604b97b80517e6cec))
- fix linking 2 ([fa8084f](https://10.189.120.23/crm/ucrm-fe/commit/fa8084fb3643f1d0d813f350d2ff3144e2eaf91a))
- fix new UI for account setting ([778bdc7](https://10.189.120.23/crm/ucrm-fe/commit/778bdc7c188d0f52fd1ead884a24c79434f4fee9))
- fix phan quyen consolidated view ([7c7a7a8](https://10.189.120.23/crm/ucrm-fe/commit/7c7a7a89b773ec5b0eb5a1462d45aea2fecea112))
- formular Datediff in words in combo chart ([431b186](https://10.189.120.23/crm/ucrm-fe/commit/431b1866fd94d78ce69611b46ffe20580df6b502))
- save mapping expose api IC integration ([f2edf37](https://10.189.120.23/crm/ucrm-fe/commit/f2edf372d6aad8b56cd31026e089156855cbbe6a))
- sonar bug ([3262fe0](https://10.189.120.23/crm/ucrm-fe/commit/3262fe0c1ad3c093f1c9cdfed9e64277f4087c52))
- UI Escalate rule SLA ([621f8de](https://10.189.120.23/crm/ucrm-fe/commit/621f8de37e4db79239394c372ecca3ea32ec097f))

### Others

- **release:** 2.15.0 ([a2e623b](https://10.189.120.23/crm/ucrm-fe/commit/a2e623be2ef6d0d22b1882dd96373a3fa822ef56))
- **release:** 2.16.0 ([e06d20c](https://10.189.120.23/crm/ucrm-fe/commit/e06d20c85c2f9a08c8cd45da1323b91cb3080ee3))

### Code Refactoring

- email ([d070c39](https://10.189.120.23/crm/ucrm-fe/commit/d070c3985df1dba7048d419de46788f64c62d05d))
- routes ([ad4cbd8](https://10.189.120.23/crm/ucrm-fe/commit/ad4cbd8661257a6322647cfefeb6f3c5f7b4192f))

## [2.16.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.12.1...v2.16.0) (2022-09-13)

### Features

- [tronglv-assginment rule] ([cac1411](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))
- add custom admin in profile ([1596855](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add duplicate rule SLA ([d23cfc0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d23cfc0516a6e6b7c8f96cff9de6d06d33ea9b80))
- add export roles ([c70d517](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- add on/off linking ([82c64f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))
- click to call in listview ([0bc8031](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0bc8031df3ba67c4ca27cc8339d6021e440143e9))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-assignment rule] update assignment rule ([4e7b010](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-feature] assignment rule ([cae8da9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- [tronglv-fix bug] Bug UI ([62d1c16](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-fix/ui]update consolidated view setting ([b45018c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b45018c46abc9d9898a020e85c03dd57d81fd326))
- [tronglv-language] update language SLA setting ([2b5d1ea](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2b5d1ea5eb675029408695146383d84a3c9e3983))
- [tronglv-language] update language Workflow ([90d3e0e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/90d3e0ee32c9aa4327f77fb08b6a6ba48bde9446))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- add search user report to; add repeat escalate rule ([b99299c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- add type when pin chart; remove formula date diff with in work ([6d63b71](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6d63b71feec00cef26fd6d3594f89df53d93924f))
- fix cannot delete record ([97b7ce2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))
- fix condition ([3aab316](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- linking object & condition ([52eb49d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))
- type download export roles ([b63d203](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))
- UI Escalate rule SLA ([621f8de](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/621f8de37e4db79239394c372ecca3ea32ec097f))

### Others

- **release:** 2.12.2 ([96dc0d1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96dc0d15c7a98923a4450fd8741c4a1b2a3f058d))
- **release:** 2.13.0 ([6f19491](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6f1949144f14b9b300c915bd6516bb14b8be3c10))
- **release:** 2.14.0 ([5e98e45](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5e98e452f341a631b925dbf3c29d8856ac33bacd))
- **release:** 2.15.0 ([a2e623b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a2e623be2ef6d0d22b1882dd96373a3fa822ef56))

### Code Refactoring

- email ([d070c39](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d070c3985df1dba7048d419de46788f64c62d05d))

## [2.15.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.13.0...v2.15.0) (2022-09-08)

### Features

- add custom admin in profile ([1596855](https://10.189.120.23/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add export roles ([c70d517](https://10.189.120.23/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- add on/off linking ([82c64f0](https://10.189.120.23/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://10.189.120.23/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://10.189.120.23/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://10.189.120.23/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://10.189.120.23/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://10.189.120.23/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://10.189.120.23/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://10.189.120.23/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://10.189.120.23/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://10.189.120.23/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://10.189.120.23/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-fix bug] Bug UI ([62d1c16](https://10.189.120.23/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://10.189.120.23/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://10.189.120.23/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://10.189.120.23/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://10.189.120.23/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://10.189.120.23/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://10.189.120.23/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://10.189.120.23/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- fix condition ([3aab316](https://10.189.120.23/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://10.189.120.23/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- linking object & condition ([52eb49d](https://10.189.120.23/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- type download export roles ([b63d203](https://10.189.120.23/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))

### Others

- **release:** 2.14.0 ([5e98e45](https://10.189.120.23/crm/ucrm-fe/commit/5e98e452f341a631b925dbf3c29d8856ac33bacd))

## [2.14.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.1...v2.14.0) (2022-09-06)

### Features

- [tronglv-assginment rule] ([cac1411](https://10.189.120.23/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))
- add custom admin in profile ([1596855](https://10.189.120.23/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add export roles ([c70d517](https://10.189.120.23/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://10.189.120.23/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://10.189.120.23/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://10.189.120.23/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://10.189.120.23/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://10.189.120.23/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://10.189.120.23/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://10.189.120.23/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://10.189.120.23/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://10.189.120.23/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://10.189.120.23/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://10.189.120.23/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-assignment rule] update assignment rule ([4e7b010](https://10.189.120.23/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-feature] assignment rule ([cae8da9](https://10.189.120.23/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://10.189.120.23/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://10.189.120.23/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://10.189.120.23/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://10.189.120.23/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://10.189.120.23/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://10.189.120.23/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://10.189.120.23/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://10.189.120.23/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- add search user report to; add repeat escalate rule ([b99299c](https://10.189.120.23/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- fix cannot delete record ([97b7ce2](https://10.189.120.23/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://10.189.120.23/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- linking object & condition ([52eb49d](https://10.189.120.23/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://10.189.120.23/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))
- type download export roles ([b63d203](https://10.189.120.23/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))

### Others

- **release:** 2.12.2 ([96dc0d1](https://10.189.120.23/crm/ucrm-fe/commit/96dc0d15c7a98923a4450fd8741c4a1b2a3f058d))
- **release:** 2.13.0 ([6f19491](https://10.189.120.23/crm/ucrm-fe/commit/6f1949144f14b9b300c915bd6516bb14b8be3c10))

## [2.13.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.2...v2.13.0) (2022-08-31)

### Features

- [tronglv-assginment rule] ([cac1411](https://10.189.120.23/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))

### Bug Fixes

- [tronglv-assignment rule] update assignment rule ([4e7b010](https://10.189.120.23/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-feature] assignment rule ([cae8da9](https://10.189.120.23/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- fix cannot delete record ([97b7ce2](https://10.189.120.23/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))

### [2.12.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.1...v2.12.2) (2022-08-25)

### Bug Fixes

- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://10.189.120.23/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://10.189.120.23/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://10.189.120.23/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://10.189.120.23/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://10.189.120.23/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://10.189.120.23/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- add search user report to; add repeat escalate rule ([b99299c](https://10.189.120.23/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://10.189.120.23/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://10.189.120.23/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))

### [2.12.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.0...v2.12.1) (2022-08-19)

### Bug Fixes

- [tronglv-fix bug] fix list view and toggle object ([19344e3](https://10.189.120.23/crm/ucrm-fe/commit/19344e398163dd7231a168800ad387877e94279c))
- option chọn Type View đang dư chức năng chọn ListView ([eaf52ac](https://10.189.120.23/crm/ucrm-fe/commit/eaf52ac4bf5ac619e66e0c360b45fb60c1cf8c50))
- search in condition ([8774a22](https://10.189.120.23/crm/ucrm-fe/commit/8774a22abe316b18a709bd767fb47e10979843f3))

## [2.12.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.11.1...v2.12.0) (2022-08-18)

### Features

- [Email Incoming Rule] - Config email. Các trường dạng dynamic-field ko hiện droplist ([39608f8](https://10.189.120.23/crm/ucrm-fe/commit/39608f8d2f47945f97796085cc612e0cb8f2ed3a))
- add status call in interaction ([176cc23](https://10.189.120.23/crm/ucrm-fe/commit/176cc23c6724f295434116a9daf65dcd75a0a2d1))

### Bug Fixes

- "tronglv-fix bug] list view" ([2df70db](https://10.189.120.23/crm/ucrm-fe/commit/2df70db4ae0a4907fbd04b66b0a8b122ed532113))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([511a8cc](https://10.189.120.23/crm/ucrm-fe/commit/511a8cc794a06859a1062f13f996965b346df337))
- [ListviewDetails] Reload comments; [Campaign] Undef record id ([56356ee](https://10.189.120.23/crm/ucrm-fe/commit/56356ee94a9ea1d66ee7612b485ff0a94d04f2dd))
- [tronglv-fix bug] list view and update calendar ([5fc0b84](https://10.189.120.23/crm/ucrm-fe/commit/5fc0b841db82c4bebe3eb12f47cd66f4a7e9de5c))
- [tronglv-fix bug] listview and other bug ([e0ac945](https://10.189.120.23/crm/ucrm-fe/commit/e0ac945f13cdfaa8d0d3b7ca010cc4ebef1aec32))
- create record in interaction (field linking) ([2a7f18a](https://10.189.120.23/crm/ucrm-fe/commit/2a7f18ab01b1427e654cf8c59f62f9672110f747))
- fix some bugs ([f19a7d0](https://10.189.120.23/crm/ucrm-fe/commit/f19a7d01aafb4aabc0c954b244e073c5165c3115))

### [2.11.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.11.0...v2.11.1) (2022-08-12)

### Bug Fixes

- [trongvl-fixui] Header view ([41e0e43](https://10.189.120.23/crm/ucrm-fe/commit/41e0e4363af8b5dad9a5cbff23bc971751e626c4))

## [2.11.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.10.0...v2.11.0) (2022-08-12)

### Features

- (FE) Tạo record trong interaction ([f79a8ff](https://10.189.120.23/crm/ucrm-fe/commit/f79a8ff8841bfc9bb71638a624429749387b8332))

### Bug Fixes

- [Knowledge base] Khi phân quyền View + Edit cho user thì lưu lại bị lỗi ([b74b254](https://10.189.120.23/crm/ucrm-fe/commit/b74b254812e01e9a73ef0ad03188c49bec6e8232))
- [Listview] - UI/UX - Chưa Disable Merge record khi chưa Tick chọn record ([549e117](https://10.189.120.23/crm/ucrm-fe/commit/549e11730851dcfef9fea692596558374081c144))
- [SLA Alert] Khi xóa Alert của SLA thì dữ liệu không clean ([715790c](https://10.189.120.23/crm/ucrm-fe/commit/715790c733876ee579c75ba669369c5ce30dcd95))
- [Ucrm Lab] Report: Step 3 chon Equal field select, -> step 2 -> step 3 bi loi field Equal ([c1b76ae](https://10.189.120.23/crm/ucrm-fe/commit/c1b76ae519cdf6cf7488c43b26bb96528ea8e692))
- component Consolidate view thì hiện thông báo thành công nhưng component vẫn còn không bị xóa ([85a2f80](https://10.189.120.23/crm/ucrm-fe/commit/85a2f8009c068ba874a93cde117797d1cc6060f2))
- edit record thì những Field không hiển thị trên View sẽ không Load được dữ liệu lên form Edit ([5943e6f](https://10.189.120.23/crm/ucrm-fe/commit/5943e6f1fa80ec3db1662be2189f20fa7e91e43f))
- fix change object id in listview with detail ([1829c04](https://10.189.120.23/crm/ucrm-fe/commit/1829c0452fd2109111216b1b154f0a8ed33206d9))
- fix type of field duplicate, type email, text, number ([8642b00](https://10.189.120.23/crm/ucrm-fe/commit/8642b00a1ca19f4118b85a95e9d6308fcb439bef))

### Code Refactoring

- [UI/UX] Change tab CTI setting, IC integration ([7da77ba](https://10.189.120.23/crm/ucrm-fe/commit/7da77ba88f0924343e36bd71e2e7f4a66c38c730))

## [2.10.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.3...v2.10.0) (2022-08-11)

### Features

- [Look up] - Chưa gắn thông tin Field Look up trong Action on read all của Email Incoming ([50a045c](https://10.189.120.23/crm/ucrm-fe/commit/50a045ce73f40d6bb4fd5cdc359d608c10d32f3d))
- [Look up] - Thêm tính năng cho chọn Trigger kế thừa data từ Object Parent ([3e05622](https://10.189.120.23/crm/ucrm-fe/commit/3e05622d07a711bd50ff145f3169d86fa7989552))
- [tronglv-newtable] custom view table ([af357f2](https://10.189.120.23/crm/ucrm-fe/commit/af357f2333bc9f3d4c6bbd1422c65c0dc2058a0e))
- create record in interaction ([1b3002c](https://10.189.120.23/crm/ucrm-fe/commit/1b3002c572feeebb57da4e6238c38b82288df124))
- tronglv-custom table ([f212f70](https://10.189.120.23/crm/ucrm-fe/commit/f212f701e82162f25f55d13a7b439280550a6e3f))

### Bug Fixes

- - Hiển thị câu thông báo yêu cầu chọn Object có field Require sau khi chọn Object bên Kanban ([a237e28](https://10.189.120.23/crm/ucrm-fe/commit/a237e282ea2a589c8d0e4d64e544413c92348009))
- [Consolidateb view] - UI/UX - Bị dư dấu ":" sau khi Field hiển thị trong Component Detail bị xó ([fe832f3](https://10.189.120.23/crm/ucrm-fe/commit/fe832f3d159b8cf3a85b7ce8af72629e03ca7000))
- [Dashboard] - Không Collapse lại được group "My dahsboard" ([ecd46b9](https://10.189.120.23/crm/ucrm-fe/commit/ecd46b919c2faf118927f65a49224540e577faa3))
- [Listview] - UI/UX - Thông tin bị lệch nhau trong Merge record ([034956f](https://10.189.120.23/crm/ucrm-fe/commit/034956fa6c26fb9990a4b93f727178ae697e939e))
- [Look up] - Field vẫn cho edit bình thường khi set quyền Field là Read only ([a3dce80](https://10.189.120.23/crm/ucrm-fe/commit/a3dce809164cdac2a277e6bd2f611d74e6003d0b))
- [Object Management] - UI/UX - Chữ "Add Object" chạy lên xuống theo scroll ([52f67f5](https://10.189.120.23/crm/ucrm-fe/commit/52f67f59aaf5b26b0550444a3bbc7929a68be134))
- [Object Management] Khi Object bị ẩn nhưng edit save tại thì Object bị bỏ ẩn ([52caee7](https://10.189.120.23/crm/ucrm-fe/commit/52caee7e6a79b93a6b4b74a830ae6eea5e24c7fc))
- [Report] - UI/UX - List danh sách với detail log scroll cùng lúc ([f7de9bc](https://10.189.120.23/crm/ucrm-fe/commit/f7de9bcf4f5be96ca09a897379c5a9cc80b6b477))
- [System log] Khi chuyển Action thì số Item/ Page ở bên dưới chưa reload lại ([333c40d](https://10.189.120.23/crm/ucrm-fe/commit/333c40d79208c7e76c2113fe031eae86451de440))
- [tronglv-fix bug] bug sider ([4a3f73b](https://10.189.120.23/crm/ucrm-fe/commit/4a3f73b44eda89340230cf7c94d00f0d8c50b6b3))
- [UI/UX - Consolidate view] CoreS: Nút thùng rác có kích thước quá lớn so với các Setting khác ([658dc2f](https://10.189.120.23/crm/ucrm-fe/commit/658dc2fe29194ea07d60cdf4b9fef769b5c5854c))
- fix new list view in consolidated view ([1c4d490](https://10.189.120.23/crm/ucrm-fe/commit/1c4d490cecb7d4f068f493f90f29f709c37d001d))
- fix: [Language] init language ([3fd6d29](https://10.189.120.23/crm/ucrm-fe/commit/3fd6d29d1880c5a3a2794b42880c8b0eb0ce6a74))

### [2.9.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.2...v2.9.3) (2022-08-09)

### Bug Fixes

- [fix-bug - UI/UX General List view] ([fd9a26b](https://10.189.120.23/crm/ucrm-fe/commit/fd9a26bac3b0fc68f0abbc48e8d083f09fc3efc4))
- [UI/UX - Picklist] Giao diện Add new/ Edit chưa có in đậm tiêu đề, thiếu dấu : sau Target field ([8c37928](https://10.189.120.23/crm/ucrm-fe/commit/8c379283eeaaf08ff36d07375bc66de2c35a1c93))
- [UI/UX General List view] Giao diện list view hiển thị quá ít còn dư quá nhiều khoảng trống ([e78c8ea](https://10.189.120.23/crm/ucrm-fe/commit/e78c8ea1242e31ab1789e13c029f941063f1ec11))
- [UI/UX-Consolidatedview] Finesse integration ([5de9810](https://10.189.120.23/crm/ucrm-fe/commit/5de9810d94a48409e0e35cf32951dda6ec9c74ab))
- bi loi chon Field là Attached file ([b95d102](https://10.189.120.23/crm/ucrm-fe/commit/b95d102d09115dad93a00cab7d921fccc477124e))
- delete file custom Table ([1f577ba](https://10.189.120.23/crm/ucrm-fe/commit/1f577ba16999cabb1ee6507ad89ad521f1e6f858))
- fix Invalid date after import Field Date ([b14b129](https://10.189.120.23/crm/ucrm-fe/commit/b14b1293db15fdaf6b3421b52f0a74665a2f8f91))
- import lookup field ([82b2855](https://10.189.120.23/crm/ucrm-fe/commit/82b28556a5f03e18fc72709f14f84faef6b46899))
- khi setting quyen view cho User thi Section chua duoc thiet lap dung phan quyen ([8bf5ff0](https://10.189.120.23/crm/ucrm-fe/commit/8bf5ff01845b58031a414babf66be00ad99dc4df))

### [2.9.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.1...v2.9.2) (2022-08-08)

### Bug Fixes

- test commit ([4b84b45](https://10.189.120.23/crm/ucrm-fe/commit/4b84b4529f5e57a5c5be8a879ca45d66da5193b8))

### [2.9.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.0...v2.9.1) (2022-08-08)

### Bug Fixes

- fix conflict ([e300043](https://10.189.120.23/crm/ucrm-fe/commit/e3000433684d2c5cb033f0ddb593bd36c7082d60))
- fix import trigger SLA and Workflow ([b1b0a42](https://10.189.120.23/crm/ucrm-fe/commit/b1b0a42bfc2b131cda65af93ceec5ef5e79248e1))

## [2.9.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.8.9...v2.9.0) (2022-08-08)

### Features

- # test ([b7c7c1d](https://10.189.120.23/crm/ucrm-fe/commit/b7c7c1d0ba91d206b1a1b294b62095f3c44688cc))
  ---------------------8/8/2022---------------------
  Bug fixes:

* [UI/UX General List view] Khi thêm View không nhập các field require, lỗi không hiển thị đầy đủ nội dung cảnh báo
* [Duplicated Rule] Hiển thị cảnh báo yêu cầu nhập Field mặc dù là đang xóa Rule
* [UI/UX General List view] Giao diện list view hiển thị quá ít còn dư quá nhiều khoảng trống
* [UI/UX General List view] - Lỗi UI/UX ở trong Header Listview
* [UI/UX General List view] - Bị dư "New listview" trong option view hiển thị
* [General List view] - Hiển thị ra button Delete sau khi check vào ô record

- [SMS Out going] Khi Addnew trùng Object hệ thống trả về lỗi 500
- [Custom admin - Workflow] User được set quyền View + Create + Delete, không có quyền edit, nhưng khi Copy trang Edit thì vẫn có quyền truy cập được trang
- [Custom admin ] Disable các tính năng không dùng trong setting Custom admin, cần Dissable lại
- [Custom admin] Khi user thường bị bỏ phân quyền Setting, reset form lại vẫn ở trang setting
- [Custom Admin] Giao diện phân quyền khi chưa tick quyền view nên cho tô xám các chức năng Create, Edit, Delete như bên phân quyền của Profile
- [Workflow] Khi Update Type Number từ Workflow vào Record thì dữ liệu Update là kiểu String không phải Type Number

* Features: Edit new avatar header

---------------------5/8/2022---------------------

- Bug fixes:
  - [Custom admin - Consolidated view] User được setting quyền View, nhưng vẫn hiện các nút thêm sửa xóa Layout setting
  - [Custom admin - Object&Layout] Khi set quyền view nhưng vẫn Edit được Name của field&layout
  - [Audit log] - System - UI/UX - Phân chia lại cột hiển thị Header
- Features: New listview
  ---------------------4/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [ General Object ] - Search không ra dữ liệu ở từng cột ở ngoài list view
  - [Consolidated View] - Không hiển thị thông báo sau khi record được chọn xem Consolidated đã bị xóa
  - [Custom admin - Knowledge base] User được phân quyền View + Create, thì hệ thống cho nhập liệu nhưng không có hiện nút Save để lưu
  - [Custom admin - Object&Layout] Khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền
- Features:
  ---------------------3/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Audit log] - Không hiển thị đúng cột thông tin Type of action, Action được chọn
  - [Mass Edit] - Bị mất thông tin của Field Look up 1 khi chọn Field Look up 2
  - [Audit log] - System - UI/UX - Hiển thị icon load sau mỗi lần nhập thông tin tìm kiếm
- Features:
  ---------------------2/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Object & Field Layout] - Hiển thị ra ID sau khi chọn xem Edit Field Look up
  - [UI/UX] Khi xóa Role, Profile, Object&layout field, sửa lại nút Save thành Delete cho thống nhất
  - [Mass Edit] - Không hiển thị thông tin các trường chỉnh sửa của Field Look up
- Features: Lookup field
  ---------------------1/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Object & Field Layout] - Bị mất checkbox Ghi log
  - [Object Ticket - Add new Record] Assign to/ Search key word không thực hiện được trên hệ điều hành Win 7
  - [Consolidated View] - Không tạo được record trong Tags, Table component
  - [Trash] - Ra trang 404 sau khi chọn Breakcrum Recyclibin
  - [Trash] - Button Delete Forever không Disable đi sau khi chọn xóa record thành công
  - [Consolidated View] - Bị mất Component sau khi chọn ở Left Screen, Right Screen trong Layout setting đối với Object mới tạo lần đầu
  - [Field & Layout Field] - Formula Field - Không xóa được Field đã chọn trong Type Formula (Date)
- Features:
  ---------------------31/7/2022---------------------
- Bug fixes:
- Features: Duplicate rule
  ---------------------30/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------29/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------28/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------27/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------26/7/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Field & Layout Field] - Formula Field - Không xóa Field sau khi chọn qua type Formula field khác
  - [ General Object ] - Hiển thị sai tên field trên màn hình Hình list view
  - [UI/UX - Picklist dependency] Giao diện Add new/ Edit chưa có in đậm tiêu đề, thiếu dấu : sau Target field
  - [ListView with detail] - Create By, Assign to, Modified by hiển thi ra mã ID trong component hệ thống Details
  - [ Object & Layout field ] - Không có dữ liệu trong Record giống như Thông tin đã setting trong Object & Layout field
  - [Setting Object] Sau khi Delete Object không thành công mà nhấn Double Edit Object thì dữ liệu đã lưu không load lên form Edit
- Features:
  - List view with details
    ---------------------25/7/2022---------------------
- Bug fixes:
  - [Listview with detail] - Không hiển thị thông tin của field meta data sau khi setting có chọn field metadata
  - [Related Object] - Xung đột 2 field có type là linking object trong cùng 1 Object
  - [SLA] - Phải chọn lại User khi thay đổi điều kiện Equals và Not Equals thì mới Lưu được
  - [Component -Detail] Không Edit được thông tin Field linking trên Component Detail
  - [Import Excel] - Bị lỗi khi nhập thông tin của Field Email bên cột "Default Value"
- Features:
  > > > > > > > 03f1e6a61d03383cafd4206afcbb6dfeeb48ce19
