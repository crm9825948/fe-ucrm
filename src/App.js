// routes
import Router from "./routes";
import useAuth from "./hooks/useAuth";
import ThemePrimaryColor from "./components/ThemePrimaryColor";
import { SettingsProvider } from "./contexts/SettingsContext";
import LoadingScreen from "./components/LoadingScreen";
import DownloadApp from "components/downloadAppScreen";
import { ErrorBoundary } from "react-error-boundary";
import { FE_URL } from "constants/constants";
import { isMobile } from "react-device-detect";

import "react-multi-carousel/lib/styles.css";

function ErrorFallback({ error, resetErrorBoundary }) {
  return <></>;
}

function App() {
  const { isInitialized } = useAuth();
  // const [width, setWidth] = useState(0)
  // const { width } = useWindowDimensions();
  return (
    <SettingsProvider>
      <ThemePrimaryColor>
        {isInitialized ? (
          isMobile && !window.location.pathname.includes("internet-form") ? (
            <DownloadApp />
          ) : (
            <ErrorBoundary
              fallbackRender={ErrorFallback}
              onError={(error) => {
                localStorage.setItem("errorCrash", error);
                window.open(`${FE_URL}/500`, "_self");
              }}
            >
              <Router />
            </ErrorBoundary>
          )
        ) : (
          <LoadingScreen />
        )}
        <LoadingScreen />
      </ThemePrimaryColor>
    </SettingsProvider>
  );
}
export default App;
