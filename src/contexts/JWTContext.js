import PropTypes from "prop-types";
import { useEffect, useReducer, useState } from "react";
import { useSelector } from "react-redux";
import { checkValidToken } from "../redux/api/authenticatedAPIs";
import authenticatedReducer, {
  AuthContext,
  initialize,
} from "../redux/slices/authenticated";
import ModalChangePass from "pages/Users/ModalChangePass";
import { logInICAPI, getUserICAPI } from "redux/api/icChatWidgetAPI";
import _ from "lodash";
import { Notification } from "components/Notification/Noti";

// const jwt = require("jsonwebtoken");

function AuthProvider({ children }) {
  const initialState = useSelector((state) => state.authenticatedReducer);
  const { userDetail } = useSelector((state) => state.userReducer);
  const [state, dispatch] = useReducer(authenticatedReducer, initialState);
  const [token, setToken] = useState(undefined);
  const [responve, setResponve] = useState(undefined);
  const [showModalChangePass, setShowModalChangePass] = useState(false);

  // function isTokenExpired(token) {
  //   try {
  //     const decoded = jwt.verify(token, "secret");
  //     const exp = decoded.exp;
  //     const currentTime = Math.floor(Date.now() / 1000);
  //     return exp < currentTime;
  //   } catch (err) {
  //     return true;
  //   }
  // }

  useEffect(() => {
    if (initialState.isLoading === false && initialState.loginStatus === null) {
      const init = () => {
        const setting_accessToken = window.localStorage.getItem(
          "setting_accessToken"
        );

        if (!token) {
          setToken(setting_accessToken);
        }
        let result = undefined;

        if (setting_accessToken !== token && setting_accessToken) {
          result = checkValidToken();
          setResponve(result);
        } else {
          result = responve;
        }

        if (setting_accessToken) {
          result
            .then((res) => {
              if (res.data.mess === "Acceptable" && res.data.status === 202) {
                if (res.data.need_pwd_change === true) {
                  setShowModalChangePass(true);
                }
                dispatch(
                  initialize({
                    isAuthenticated: true,
                    userInfo: null,
                    isInitialized: true,
                    required: setting_accessToken,
                  })
                );
                if (!localStorage.getItem("icIntegration_accessToken")) {
                  //IC
                  localStorage.setItem(
                    "icIntegration_useIC",
                    _.get(res, "data.use_ic", false)
                  );
                  localStorage.setItem(
                    "icIntegration_link_api",
                    _.get(res, "data.link_api_ic", "")
                  );

                  if (_.get(res, "data.use_ic", false)) {
                    if (_.get(res, "data.authen_type", "") === "sso") {
                      const userIC = getUserICAPI();

                      userIC
                        .then((dataIC) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            localStorage.getItem("setting_accessToken")
                          );
                          localStorage.setItem(
                            "icIntegration_refreshToken",
                            localStorage.getItem("setting_refreshToken")
                          );
                          localStorage.setItem(
                            "icIntegration_tenantID",
                            _.get(dataIC, "data.data.tenant_id", "")
                          );

                          localStorage.setItem(
                            "icIntegration_is_administrator",
                            _.get(dataIC, "data.data.is_administrator", false)
                          );
                        })
                        .catch((error) => {
                          Notification(
                            "error",
                            _.get(error, "response.data.error", "Error!")
                          );
                        });
                    } else {
                      const responseIC = logInICAPI({ agent_admin: false });

                      responseIC
                        .then((dataIC) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            _.get(dataIC, "data.data.token_ic", "1")
                          );
                          localStorage.setItem(
                            "icIntegration_refreshToken",
                            _.get(dataIC, "data.data.refresh_token_ic", "")
                          );
                          localStorage.setItem(
                            "icIntegration_tenantID",
                            _.get(dataIC, "data.data.tenant_id_ic", "")
                          );

                          localStorage.setItem(
                            "icIntegration_is_administrator",
                            _.get(
                              dataIC,
                              "data.data.is_administrator_ic",
                              false
                            )
                          );
                        })
                        .catch((error) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            "1"
                          );
                        });
                    }
                  } else {
                    if (_.get(res, "data.authen_type", "") === "sso") {
                      const userIC = getUserICAPI();

                      userIC
                        .then((dataIC) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            localStorage.getItem("setting_accessToken")
                          );
                          localStorage.setItem(
                            "icIntegration_refreshToken",
                            localStorage.getItem("setting_refreshToken")
                          );
                          localStorage.setItem(
                            "icIntegration_tenantID",
                            _.get(dataIC, "data.data.tenant_id", "")
                          );

                          localStorage.setItem(
                            "icIntegration_is_administrator",
                            _.get(dataIC, "data.data.is_administrator", false)
                          );
                        })
                        .catch((error) => {
                          Notification(
                            "error",
                            _.get(error, "response.data.error", "Error!")
                          );
                        });
                    } else {
                      const responseIC = logInICAPI({ agent_admin: true });

                      responseIC
                        .then((dataIC) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            _.get(dataIC, "data.data.token_ic", "1")
                          );
                          localStorage.setItem(
                            "icIntegration_refreshToken",
                            _.get(dataIC, "data.data.refresh_token_ic", "")
                          );
                          localStorage.setItem(
                            "icIntegration_tenantID",
                            _.get(dataIC, "data.data.tenant_id_ic", "")
                          );
                          localStorage.setItem(
                            "icIntegration_is_administrator",
                            _.get(dataIC, "data.data.is_administrator_ic", true)
                          );
                        })
                        .catch((error) => {
                          localStorage.setItem(
                            "icIntegration_accessToken",
                            "1"
                          );
                        });
                    }
                  }
                  //EndIC
                }
              } else {
                dispatch(
                  initialize({
                    isAuthenticated: false,
                    userInfo: null,
                    isInitialized: true,
                  })
                );
              }
            })
            .catch((err) => {
              dispatch(
                initialize({
                  isAuthenticated: false,
                  userInfo: null,
                  isInitialized: true,
                })
              );
            });
        } else {
          dispatch(
            initialize({
              isAuthenticated: false,
              userInfo: null,
              isInitialized: true,
            })
          );
        }
      };

      init();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [initialState]);

  return (
    <AuthContext.Provider
      value={{
        ...state,
        method: "jwt",
      }}
    >
      {children}
      <ModalChangePass
        showModalChangePass={showModalChangePass}
        onHideModalChangePass={() => setShowModalChangePass(false)}
        getListUser={() => {}}
        user={userDetail}
        isAdmin={false}
        firstPass={true}
      />
    </AuthContext.Provider>
  );
}

AuthProvider.propTypes = {
  children: PropTypes.node,
};

export { AuthContext, AuthProvider };
