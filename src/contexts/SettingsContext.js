import PropTypes from "prop-types";
import { createContext, useEffect } from "react";
import { useSelector } from "react-redux";
import { PRIMARY_COLOR, THEME_COLOR } from "../configs/color";
// hooks
import useLocalStorage from "../hooks/useLocalStorage";

function SetColor(themeColor) {
  let color;
  const DEFAULT = PRIMARY_COLOR[0];
  const WHITE = PRIMARY_COLOR[1];
  const cFDA92D = THEME_COLOR[0];
  const cC0A145 = THEME_COLOR[1];
  const cEB0606 = THEME_COLOR[2];
  const cF53163 = THEME_COLOR[3];
  const cEB00FF = THEME_COLOR[4];
  const c5e2e86 = THEME_COLOR[5];
  const c1CCAFF = THEME_COLOR[6];
  const c00A3FF = THEME_COLOR[7];
  const c00AB55 = THEME_COLOR[8];
  const c20A2A2 = THEME_COLOR[9];
  const c2065D1 = THEME_COLOR[10];
  const c145388 = THEME_COLOR[11];

  switch (themeColor) {
    case "white":
      color = WHITE;
      break;
    case "#FDA92D":
      color = cFDA92D;
      break;
    case "#C0A145":
      color = cC0A145;
      break;
    case "#EB0606":
      color = cEB0606;
      break;
    case "#F53163":
      color = cF53163;
      break;
    case "#EB00FF":
      color = cEB00FF;
      break;
    case "#5e2e86":
      color = c5e2e86;
      break;
    case "#1CCAFF":
      color = c1CCAFF;
      break;
    case "#00A3FF":
      color = c00A3FF;
      break;
    case "#00AB55":
      color = c00AB55;
      break;
    case "#20A2A2":
      color = c20A2A2;
      break;
    case "#2065D1":
      color = c2065D1;
      break;
    case "#145388":
      color = c145388;
      break;
    default:
      color = DEFAULT;
  }

  return color;
}

const initialState = {
  themeMode: "light",
  themeColor: "default",
  onChangeColor: () => {},
  setColor: PRIMARY_COLOR[0],
};

const SettingsContext = createContext(initialState);

SettingsProvider.propTypes = {
  children: PropTypes.node,
};

function SettingsProvider({ children }) {
  // const dispatch = useDispatch();
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const [settings, setSettings] = useLocalStorage("settings", {
    themeMode: "light",
    themeColor: defaultBrandName && defaultBrandName.theme_color,
  });

  useEffect(() => {
    setSettings({
      themeMode: "light",
      themeColor: defaultBrandName && defaultBrandName.theme_color,
    });
    localStorage.setItem(
      "settings",
      JSON.stringify({
        themeMode: "light",
        themeColor: defaultBrandName && defaultBrandName.theme_color,
      })
    );
    // window.location.reload();
    /* eslint-disable-next-line */
  }, [defaultBrandName]);

  const onChangeMode = (event) => {
    setSettings({
      ...settings,
      themeMode: event.target.value,
    });
  };

  const onChangeColor = (value) => {
    setSettings({
      ...settings,
      themeColor: value,
    });
  };

  return (
    <SettingsContext.Provider
      value={{
        ...settings,
        //Mode
        onChangeMode,
        //Color
        onChangeColor,
        setColor: SetColor(settings.themeColor),
      }}
    >
      {children}
    </SettingsContext.Provider>
  );
}

export { SettingsContext, SettingsProvider };
