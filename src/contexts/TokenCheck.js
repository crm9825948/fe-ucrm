import { refreshToken } from "redux/api/authenticatedAPIs";
import { Notification } from "components/Notification/Noti";
import AuthService from "services/AuthService";

const jwt = require("jsonwebtoken");

export const checkTokenExpiration = async () => {
  const decoded = jwt.decode(localStorage.getItem("setting_accessToken"));
  const exp = decoded.exp;
  const methodAlg = jwt.decode(localStorage.getItem("setting_accessToken"), {
    complete: true,
  }).header.alg;
  const currentTime = Math.floor(Date.now() / 1000);
  if (exp > currentTime) {
    return localStorage.getItem("setting_accessToken");
  } else {
    try {
      if (methodAlg !== "HS256") {
        AuthService.login();
      } else {
        const res = await refreshToken();
        window.localStorage.setItem("setting_accessToken", res.data.data);
        return res.data.data;
      }
    } catch (err) {
      Notification("error", err.response.data.error);
    }
  }
};
