export const BASE_URL_API = process.env.REACT_APP_BASE_URL_API;
// export const BASE_URL_API = "http://127.0.0.1:8000/api/";
export const BE_URL = process.env.REACT_APP_BE_URL;
export const FE_URL = process.env.REACT_APP_FE_URL;
export const WS_URL = process.env.REACT_APP_WS_URL;
export const INTERVAL_URL = process.env.REACT_APP_INTERVAL;
export const BASENAME = process.env.REACT_APP_BASENAME;
export const USE_SSO = process.env.REACT_APP_USE_SSO;
export const RANDOM_VARIABLE = process.env.REACT_APP_RANDOM_VARIABLE;
export const REACT_APP_URL_TRACKING_LOGIN =
  process.env.REACT_APP_URL_TRACKING_LOGIN;
export const SITE_KEY = process.env.REACT_APP_SITE_KEY;

// SSO
const stsAuthority = process.env.REACT_APP_STS_AUTH;
const clientId = process.env.REACT_APP_CLIENT_ID;
const clientRoot = process.env.REACT_APP_CLIENT_ROOT;
const clientScope = process.env.REACT_APP_CLIENT_SCOPE;
// eslint-disable-next-line
const apiRoot = process.env.REACT_APP_API_ROOT;
const clientLogoutURL = process.env.REACT_APP_CLIENT_LOGOUT_URL;

export const META_DATA = [
  {
    label: "Created date",
    name: "Created date",
    value: "created_date",
    ID: "created_date",
    type: "datetime-local",
  },
  {
    label: "Modified by",
    name: "Modified by",
    value: "modify_by",
    ID: "modify_by",
    type: "user",
  },
  {
    label: "Modified date",
    name: "Modify time",
    value: "modify_time",
    ID: "modify_time",
    type: "datetime-local",
  },
  {
    label: "Assign to",
    name: "Assign to",
    value: "owner",
    ID: "owner",
    type: "user",
  },
  {
    label: "Created by",
    name: "Created by",
    value: "created_by",
    ID: "created_by",
    type: "user",
  },
];

export const oidcClient = {
  stsAuthority: stsAuthority,
  clientId: clientId,
  clientRoot: clientRoot,
  clientScope: clientScope,
  clientLogoutURL: clientLogoutURL,
};
