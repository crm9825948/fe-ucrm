import _ from "lodash";
import Icon from "components/Icon/Icon";

import AllIcon from "assets/icons/qualityManagement/icon-all.png";
import EmailInIcon from "assets/icons/qualityManagement/Emailin-icon.png";
import EmailOutIcon from "assets/icons/qualityManagement/Emailout-icon.png";
import MessInIcon from "assets/icons/qualityManagement/Messin-icon.png";
import MessOutIcon from "assets/icons/qualityManagement/Messout-icon.png";
import CallInIcon from "assets/icons/qualityManagement/Callin-icon.png";
import CallOutIcon from "assets/icons/qualityManagement/Callout-icon.png";
import VBBIIcon from "assets/icons/qualityManagement/VBBI.png";
import Viber from "assets/icons/qualityManagement/viber.svg";
import Webchat from "assets/icons/qualityManagement/chat.svg";
import Zalo from "assets/icons/qualityManagement/zalo.svg";
import EmailIC from "assets/icons/qualityManagement/mail.svg";
// import Facebook from "assets/icons/qualityManagement/Facebook.svg";
import FacebookMessage from "assets/icons/qualityManagement/Mess.svg";
import FacebookComment from "assets/icons/timeline/facebook_comment.svg";
// import Instagram from "assets/icons/qualityManagement/Intagram.svg";
import InstagramMessage from "assets/icons/qualityManagement/Intagram_mess.svg";
import InstagramComment from "assets/icons/timeline/instagram_comment.svg";
import Telegram from "assets/icons/qualityManagement/Telegram.svg";
import ThirdPartyChannel from "assets/icons/qualityManagement/3rd.svg";
import Whatsapp from "assets/icons/qualityManagement/Whatsapp.svg";

//Component layout
import KnowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";
import LogsImg from "assets/images/consolidatedView/Log.png";
import SLAImg from "assets/images/consolidatedView/SLA.png";
import TimelineVerticalImg from "assets/images/consolidatedView/timeline-vertical.png";
import TimelineHorizontalImg from "assets/images/consolidatedView/timeline-horizontal.png";
import ICChatWidget from "assets/images/consolidatedView/ICChatWidget.svg";
import Comment from "assets/images/consolidatedView/comment.png";
import General from "assets/images/consolidatedView/General configuration.png";
import Template from "assets/images/consolidatedView/template configuration.png";
import SMS from "assets/images/consolidatedView/SMS component.svg";
import Email from "assets/images/consolidatedView/Email.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import InteractionFormImg from "assets/images/consolidatedView/FormInteraction.png";

//Call View Templates
import BaseTemplate from "assets/images/callView/BaseTemplateImg.png";
import DiversityTemplateImg from "assets/images/callView/DiversityTemplateImg.png";
import BaseFrameReview from "assets/images/callView/baseFrame.png";
import DiversityFrameReview from "assets/images/callView/diversityFrame.png";

//
import tagsImg from "assets/images/consolidatedView/tags.png";
import tableImg from "assets/images/consolidatedView/table.png";
import detailsImg from "assets/images/consolidatedView/detail.png";

export const metaData = [
  {
    label: "Created Date",
    value: "created_date",
    type: "date",
  },
  {
    label: "Created By",
    value: "created_by",
    type: "text",
  },
  {
    label: "Modify Time",
    value: "modify_time",
    type: "date",
  },
  {
    label: "Modified by",
    value: "modify_by",
    type: "text",
  },
  {
    label: "Assign To",
    value: "owner",
    type: "text",
  },
];

export const generalInfo = [
  {
    label: "Date",
    value: "date",
  },
  {
    label: "Datetime",
    value: "datetime",
  },
  {
    label: "Created date",
    value: "created_date",
  },
  {
    label: "Modified by",
    value: "modify_by",
  },
  {
    label: "Modify time",
    value: "modify_time",
  },
  {
    label: "Assign to",
    value: "owner",
  },
  {
    label: "Created by",
    value: "created_by",
  },
];

export const colorChart = [
  "#4A85F2",
  "#E84F6E",
  "#FBBB3C",
  "#9765CD",
  "#00CCB1",
  "#F9539B",
  "#59CEE8",
  "#5C7093",
  "#E3C178",
  "#EE7959",
  "#A0E552",
  "#AA783C",
  "#566ECD",
  "#E576C2",
  "#C2D3AB",
  "#37B363",
  "#B8C13E",
  "#4BB7E2",
  "#F1E1B1",
  "#7CC7CB",
  "#3583B1",
  "#D09A9C",
  "#00998F",
  "#FFD36B",
  "#FA7070",
  "#6886B6",
  "#7BE5E3",
  "#9E76C1",
  "#ADBCD7",
  "#466CAA",
  "#D05B4E",
  "#D9D9D9",
  "#26A0FC",
  "#26E7A6",
  "#FE6769",
  "#BAE490",
  "#56A8FB",
  "#D2ADCF",
  "#F5669D",
  "#FC9264",
  "#5EC0C5",
  "#BFBFBF",
  "#7EC15C",
  "#D0AA95",
  "#8696BB",
  "#C79A33",
  "#377EA4",
];

export const triggerWorkflow = [
  {
    label: "workflow.onlyFirstSave",
    value: 1,
  },
  {
    label: "workflow.untilCondition",
    value: 2,
  },
  {
    label: "workflow.everyTime",
    value: 3,
  },
  {
    label: "workflow.hasChange",
    value: 5,
  },
  {
    label: "workflow.schedule",
    value: 4,
  },
  {
    label: "workflow.triggerInteraction",
    value: 6,
  },
  {
    label: "workflow.triggerWhenPopup",
    value: 7,
  },
];

export const typeInteraction = [
  {
    label: "Call",
    value: "Call",
  },
];

export const optionsCreateRecordCall = [
  {
    label: "Interaction - Extension",
    value: "extension",
  },
  {
    label: "Interaction - Bill duration",
    value: "bill_duration",
  },
  {
    label: "Interaction - Call status",
    value: "call_status",
  },
  {
    label: "Interaction - Link record",
    value: "link_record",
  },
  {
    label: "Interaction - Phone",
    value: "phone",
  },
  {
    label: "Interaction - Reason code",
    value: "reason_code",
  },
  {
    label: "Interaction - Reason name",
    value: "reason_name",
  },
  {
    label: "Interaction - Total duration",
    value: "total_duration",
  },
  {
    label: "Interaction - Username",
    value: "username",
  },
];

export const optionsCreateRecordCallValues = [
  "extension",
  "bill_duration",
  "call_status",
  "link_record",
  "phone",
  "reason_code",
  "reason_name",
  "total_duration",
  "username",
];

export const frequencyWorkflow = [
  {
    label: "workflow.Hourly",
    value: 1,
  },
  {
    label: "workflow.Daily",
    value: 2,
  },
  {
    label: "workflow.Weekly",
    value: 3,
  },
  {
    label: "workflow.Monthly",
    value: 5,
  },
  {
    label: "workflow.Yearly",
    value: 6,
  },
  {
    label: "workflow.SpecificDate",
    value: 4,
  },
];

export const week = [
  {
    label: "common.monday",
    value: 0,
  },
  {
    label: "common.tuesday",
    value: 1,
  },
  {
    label: "common.wednesday",
    value: 2,
  },
  {
    label: "common.thurday",
    value: 3,
  },
  {
    label: "common.friday",
    value: 4,
  },
  {
    label: "common.saturday",
    value: 5,
  },
  {
    label: "common.sunday",
    value: 6,
  },
];

export const days = [
  {
    label: "1",
    value: 1,
  },
  {
    label: "2",
    value: 2,
  },
  {
    label: "3",
    value: 3,
  },
  {
    label: "4",
    value: 4,
  },
  {
    label: "5",
    value: 5,
  },
  {
    label: "6",
    value: 6,
  },
  {
    label: "7",
    value: 7,
  },
  {
    label: "8",
    value: 8,
  },
  {
    label: "9",
    value: 9,
  },
  {
    label: "10",
    value: 10,
  },
  {
    label: "11",
    value: 11,
  },
  {
    label: "12",
    value: 12,
  },
  {
    label: "13",
    value: 13,
  },
  {
    label: "14",
    value: 14,
  },
  {
    label: "15",
    value: 15,
  },
  {
    label: "16",
    value: 16,
  },
  {
    label: "17",
    value: 17,
  },
  {
    label: "18",
    value: 18,
  },
  {
    label: "19",
    value: 19,
  },
  {
    label: "20",
    value: 20,
  },
  {
    label: "21",
    value: 21,
  },
  {
    label: "22",
    value: 22,
  },
  {
    label: "23",
    value: 23,
  },
  {
    label: "24",
    value: 24,
  },
  {
    label: "25",
    value: 25,
  },
  {
    label: "26",
    value: 26,
  },
  {
    label: "27",
    value: 27,
  },
  {
    label: "28",
    value: 28,
  },
  {
    label: "29",
    value: 29,
  },
  {
    label: "30",
    value: 30,
  },
  {
    label: "31",
    value: 31,
  },
];

export const metaFields = [
  {
    label: "Assign to",
    value: "owner",
  },
  {
    label: "Assign to.Report to",
    value: "owner.Report_To",
  },
  {
    label: "Created by",
    value: "created_by",
  },
  {
    label: "Created by.Report to",
    value: "created_by.Report_To",
  },
  {
    label: "Modified by",
    value: "modify_by",
  },
  {
    label: "Modified by.Report to",
    value: "modify_by.Report_To",
  },
];

export const sendToUserSLA = [
  {
    label: "Assigned to",
    value: "assigned_to_user",
  },
  {
    label: "Report to",
    value: "assigned_to_user_report_to",
  },
];

export const allowedTags = [
  "address",
  "article",
  "aside",
  "footer",
  "header",
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "hgroup",
  "main",
  "nav",
  "section",
  "blockquote",
  "dd",
  "div",
  "dl",
  "dt",
  "figcaption",
  "figure",
  "hr",
  "li",
  "main",
  "ol",
  "p",
  "pre",
  "ul",
  "a",
  "abbr",
  "b",
  "bdi",
  "bdo",
  "br",
  "cite",
  "code",
  "data",
  "dfn",
  "em",
  "i",
  "kbd",
  "mark",
  "q",
  "rb",
  "rp",
  "rt",
  "rtc",
  "ruby",
  "s",
  "samp",
  "small",
  "span",
  "strong",
  "sub",
  "sup",
  "time",
  "u",
  "var",
  "wbr",
  "caption",
  "col",
  "colgroup",
  "table",
  "tbody",
  "td",
  "tfoot",
  "th",
  "thead",
  "tr",
  "img",
  "!doctype",
  "html",
  "head",
  "body",
  "font",
  "center",
  "picture",
  "meta",
  "style",
  "o:p",
  "figure",
  "link",
  "google-sheets-html-origin",
  "string",
];

export const optionsParse = {
  replace: (domNode) => {
    if (_.get(domNode, "name", "") === "style") {
      return <style></style>;
    }

    if (
      _.get(domNode, "name", "") !== "" &&
      !allowedTags.includes(_.get(domNode, "name", ""))
    ) {
      return (
        <>
          {"< "}
          {_.get(domNode, "name", "")}
          {" >"}
          {_.map(_.get(domNode, "children", []), (item) =>
            _.get(item, "data", "")
          )}
        </>
      );
    }
  },
};

export const OptionSortArticleDefault = [
  { label: "related", value: "related" },
  {
    label: "newest",
    value: "newest",
  },
  {
    label: "oledest",
    value: "oldest",
  },
];

export const OptionSortArticleView = [
  {
    label: "viewasc",
    value: "viewascending",
  },
  {
    label: "viewdec",
    value: "viewdecrease",
  },
];

export const OptionSortArticleVote = [
  {
    label: "upvoteAsc",
    value: "upvoteascending",
  },
  {
    label: "upvoteDec",
    value: "upvotedecrease",
  },
];

export const filterStatus = [
  {
    label: "Online",
    value: "online",
  },
  {
    label: "Busy",
    value: "busy",
  },
  {
    label: "Offline",
    value: "offline",
  },
];

export const listStatusCRM = [
  {
    label: "Online",
    value: "online",
  },
  {
    label: "Busy",
    value: "busy",
  },
];

export const listStatusIC = [
  {
    label: "Available",
    value: "available",
  },
  {
    label: "Not Ready",
    value: "not_ready",
  },
];

export const channelTypeIC = [
  { label: "Facebook message", value: "facebook_message" },
  { label: "Facebook comment", value: "facebook_comment" },
  { label: "Instagram message", value: "instagram_message" },
  { label: "Instagram comment", value: "instagram_comment" },
  { label: "Zalo", value: "zalo" },
  { label: "Viber", value: "viber" },
  { label: "Email", value: "email" },
  { label: "Telegram", value: "telegram" },
  { label: "Whatsapp", value: "whatsapp" },
  { label: "Webchat", value: "webchat" },
  { label: "3rd party", value: "third_party_channel" },
];

export const optionsTimeRangeReport = [
  { label: "All time", value: "all-time" },
  { label: "Custom", value: "custom" },
  {
    label: "Day",
    options: [
      { label: "Today", value: "today" },
      { label: "Yesterday", value: "yesterday" },
      { label: "Last n days", value: "last-n-days" },
    ],
  },
  {
    label: "Week",
    options: [
      { label: "This week", value: "this-week" },
      { label: "Last week", value: "last-week" },
      { label: "Last n weeks", value: "last-n-weeks" },
    ],
  },
  {
    label: "Month",
    options: [
      { label: "This month", value: "this-month" },
      { label: "Last month", value: "last-month" },
      { label: "Last n months", value: "last-n-months" },
    ],
  },
  {
    label: "Quarter",
    options: [
      { label: "This quarter", value: "this-quarter" },
      { label: "Last quarter", value: "last-quarter" },
    ],
  },
  {
    label: "Year",
    options: [
      { label: "This year", value: "this-year" },
      { label: "Last year", value: "last-year" },
      { label: "Last n years", value: "last-n-years" },
    ],
  },
];

export const optionsTimeRangeReport2 = [
  { label: "All time", value: "all-time" },
  { label: "Custom", value: "custom" },
  { label: "Today", value: "today" },
  { label: "Yesterday", value: "yesterday" },
  { label: "Last n days", value: "last-n-days" },
  { label: "This week", value: "this-week" },
  { label: "Last week", value: "last-week" },
  { label: "Last n weeks", value: "last-n-weeks" },
  { label: "This month", value: "this-month" },
  { label: "Last month", value: "last-month" },
  { label: "Last n months", value: "last-n-months" },
  { label: "This quarter", value: "this-quarter" },
  { label: "Last quarter", value: "last-quarter" },
  { label: "This year", value: "this-year" },
  { label: "Last year", value: "last-year" },
  { label: "Last n years", value: "last-n-years" },
];

export const COLOR_INTERACTION_TYPE = {
  ALL: "#C7F5F5",
  CALLIN: "#D3E0FF",
  CALLOUT: "#DAFFEF",
  EMAILIN: "#FCD4D4",
  EMAILOUT: "#DAE0FB",
  MESSIN: "#CAE3FF",
  MESSOUT: "#EEDDF6",
  VBEE: "#FFEBDD",
  VBEEIN: "#FFEBDD",
  VBEEOUT: "#DDEBFF",
  IC_VIDBER: "#F4E3FF",
  IC_WEBCHAT: "#E0FFFF",
  IC_ZALO: "#CCE2FF",
  IC_EMAIL: "#FFDDDD",
  IC_FACEBOOK: "#DEECFF",
  IC_FACEBOOKMESSAGE: "#DEECFF",
  IC_FACEBOOKCOMMENT: "#DEECFF",
  IC_INSTAGRAM: "#FEE6F9",
  IC_INSTAGRAMMESSAGE: "#FEE6F9",
  IC_INSTAGRAMCOMMENT: "#FEE6F9",
  IC_TELEGRAM: "#DFF4FF",
  IC_THIRDPARTYCHANNEL: "#E2E8FF",
  IC_WHATSAPP: "#E4FFE8",
  IC: "#CDF8F8",
};

export const COLOR_LIST_FILTER = {
  EMAILIN: "#FFEBEB",
  EMAILOUT: "#DAE0FB",
  MESSIN: "#CAE3FF",
  MESSOUT: "#EEDDF6",
  CALLIN: "#D3E0FF",
  CALLOUT: "#DAFFEF",
  VBBI: "#FFEBDD",
  MAILIC: "#CDF8F8",
};

export const listOptionQMSettings = [
  {
    icon: CallInIcon,
    text: "Call in",
    bgColor: COLOR_INTERACTION_TYPE.CALLIN,
    value: "call_in",
  },
  {
    icon: CallOutIcon,
    text: "Call out",
    bgColor: COLOR_INTERACTION_TYPE.CALLOUT,
    value: "call_out",
  },
  {
    icon: EmailInIcon,
    text: "Email in",
    bgColor: COLOR_INTERACTION_TYPE.EMAILIN,
    value: "email_in",
  },
  {
    icon: EmailOutIcon,
    text: "Email out",
    bgColor: COLOR_INTERACTION_TYPE.EMAILOUT,
    value: "email_out",
  },
  // {
  //   icon: MessInIcon,
  //   text: "SMS in",
  //   bgColor: COLOR_INTERACTION_TYPE.MESSIN,
  //   value: "sms_in",
  // },
  {
    icon: MessOutIcon,
    text: "SMS out",
    bgColor: COLOR_INTERACTION_TYPE.MESSOUT,
    value: "sms_out",
  },
  // {
  //   icon: VBBIIcon,
  //   text: "VBEE in",
  //   bgColor: COLOR_INTERACTION_TYPE.VBEE,
  //   value: "vbee_in",
  // },
  // {
  //   icon: VBBIIcon,
  //   text: "VBEE out",
  //   bgColor: COLOR_INTERACTION_TYPE.IC_VIDBER,
  //   value: "vbee_out",
  // },
  {
    icon: Viber,
    text: "IC Viber",
    bgColor: COLOR_INTERACTION_TYPE.IC_VIDBER,
    value: "ic_viber",
  },
  {
    icon: Webchat,
    text: "IC Webchat",
    bgColor: COLOR_INTERACTION_TYPE.IC_WEBCHAT,
    value: "ic_webchat",
  },
  {
    icon: Zalo,
    text: "IC Zalo",
    bgColor: COLOR_INTERACTION_TYPE.IC_ZALO,
    value: "ic_zalo",
  },
  {
    icon: EmailIC,
    text: "IC Email",
    bgColor: COLOR_INTERACTION_TYPE.IC_EMAIL,
    value: "ic_email",
  },
  // {
  //   icon: Facebook,
  //   text: "IC Facebook",
  //   bgColor: COLOR_INTERACTION_TYPE.IC_FACEBOOK,
  //   value: "ic_facebook",
  // },
  {
    icon: FacebookMessage,
    text: "IC Facebook Message",
    bgColor: COLOR_INTERACTION_TYPE.IC_FACEBOOKMESSAGE,
    value: "ic_facebook_message",
  },
  {
    icon: FacebookComment,
    text: "IC Facebook Comment",
    bgColor: COLOR_INTERACTION_TYPE.IC_FACEBOOKCOMMENT,
    value: "ic_facebook_comment",
  },
  // {
  //   icon: Instagram,
  //   text: "IC Instagram",
  //   bgColor: COLOR_INTERACTION_TYPE.IC_INSTAGRAM,
  //   value: "ic_instagram",
  // },
  {
    icon: InstagramMessage,
    text: "IC Instagram Message",
    bgColor: COLOR_INTERACTION_TYPE.IC_INSTAGRAMMESSAGE,
    value: "ic_instagram_message",
  },
  {
    icon: InstagramComment,
    text: "IC Instagram Comment",
    bgColor: COLOR_INTERACTION_TYPE.IC_INSTAGRAMCOMMENT,
    value: "ic_instagram_comment",
  },
  {
    icon: Telegram,
    text: "IC Telegram",
    bgColor: COLOR_INTERACTION_TYPE.IC_TELEGRAM,
    value: "ic_telegram",
  },
  {
    icon: ThirdPartyChannel,
    text: "IC Third Party Channel",
    bgColor: COLOR_INTERACTION_TYPE.IC_THIRDPARTYCHANNEL,
    value: "ic_third_party_channel",
  },
  {
    icon: Whatsapp,
    text: "IC Whatsapp",
    bgColor: COLOR_INTERACTION_TYPE.IC_WHATSAPP,
    value: "ic_whatsapp",
  },
];

export const listOptionQM = [
  {
    icon: AllIcon,
    text: "All",
    bgColor: COLOR_INTERACTION_TYPE.ALL,
    value: "all",
  },
  {
    icon: CallInIcon,
    text: "Call In",
    bgColor: COLOR_INTERACTION_TYPE.CALLIN,
    value: "call_in",
  },
  {
    icon: CallOutIcon,
    text: "Call Out",
    bgColor: COLOR_INTERACTION_TYPE.CALLOUT,
    value: "call_out",
  },
  {
    icon: EmailInIcon,
    text: "Email In",
    bgColor: COLOR_INTERACTION_TYPE.EMAILIN,
    value: "email_in",
  },
  {
    icon: EmailOutIcon,
    text: "Email Out",
    bgColor: COLOR_INTERACTION_TYPE.EMAILOUT,
    value: "email_out",
  },
  {
    icon: MessInIcon,
    text: "SMS In",
    bgColor: COLOR_INTERACTION_TYPE.MESSIN,
    value: "sms_in",
  },
  {
    icon: MessOutIcon,
    text: "SMS Out",
    bgColor: COLOR_INTERACTION_TYPE.MESSOUT,
    value: "sms_out",
  },
  {
    icon: VBBIIcon,
    text: "VBEE In",
    bgColor: COLOR_INTERACTION_TYPE.VBEE,
    value: "vbee_in",
  },
  {
    icon: VBBIIcon,
    text: "VBEE Out",
    bgColor: COLOR_INTERACTION_TYPE.MAILIC,
    value: "vbee_out",
  },
];

export const optionsDate = [
  {
    label: "YYYY-MM-DD",
    value: "%Y-%m-%d",
  },
  {
    label: "YYYY/MM/DD",
    value: "%Y/%m/%d",
  },
  {
    label: "YYYY MM DD",
    value: "%Y %m %d",
  },
  {
    label: "MM-DD-YYYY",
    value: "%m-%d-%Y",
  },
  {
    label: "MM/DD/YYYY",
    value: "%m/%d/%Y",
  },
  {
    label: "MM DD YYYY",
    value: "%m %d %Y",
  },

  {
    label: "DD-MM-YYYY",
    value: "%d-%m-%Y",
  },
  {
    label: "DD/MM/YYYY",
    value: "%d/%m/%Y",
  },
  {
    label: "DD MM YYYY",
    value: "%d %m %Y",
  },
];

export const optionsTime = [
  {
    label: "HH:mm:ss",
    value: "%H:%M:%S",
  },
  {
    label: "HH:mm",
    value: "%H:%M",
  },
];

export const getFormatDate = (allConfig) => {
  const formatTemp = optionsDate.find(
    (ele) => ele.value === _.get(allConfig, "tenant_date_format", "YYYY-MM-DD")
  );
  return _.get(formatTemp, "label", "YYYY-MM-DD");
};

export const getFormatDateTime = (allConfig) => {
  const date = _.get(
    allConfig,
    "tenant_datetime_format",
    "%Y-%m-%d %H:%M:%S"
  ).split(" ")[0];
  const time = _.get(
    allConfig,
    "tenant_datetime_format",
    "%Y-%m-%d %H:%M:%S"
  ).split(" ")[1];
  const finalDate = optionsDate.find((ele) => ele.value === date).label;
  const finalTime = optionsTime.find((ele) => ele.value === time).label;
  return finalDate + " " + finalTime;
};

export const componentsLayoutSettings = [
  {
    title: "Tabs",
    des: "Tabs component",
    icon: tabsImg,
  },
  {
    title: "SLA",
    des: "Service-level agreement",
    icon: SLAImg,
  },
  {
    title: "Vertical Timeline",
    des: "Vertical Timeline component",
    icon: TimelineVerticalImg,
  },
  {
    title: "Horizontal Timeline",
    des: "Horizontal Timeline component",
    icon: TimelineHorizontalImg,
  },
  {
    title: "Vertical Timeline New",
    des: "Vertical Timeline component new",
    icon: TimelineVerticalImg,
  },
  {
    title: "Horizontal Timeline New",
    des: "Horizontal Timeline component new",
    icon: TimelineHorizontalImg,
  },
  {
    title: "IC Chat Widget",
    des: "IC Chat Widget component",
    icon: ICChatWidget,
  },
  {
    title: "Logs",
    des: "Logs component",
    icon: LogsImg,
  },
  {
    title: "Knowledge Base",
    des: "Knowledge Base component",
    icon: KnowledgeBaseImg,
  },
  {
    title: "Comment",
    des: "Comment component",
    icon: Comment,
  },
  {
    title: "Email",
    des: "Email component",
    icon: Email,
  },
];

export const components = [
  {
    title: "SLA",
    des: "Service-level agreement",
    icon: SLAImg,
  },
  {
    title: "Vertical Timeline",
    des: "Vertical Timeline component",
    icon: TimelineVerticalImg,
  },
  {
    title: "Horizontal Timeline",
    des: "Horizontal Timeline component",
    icon: TimelineHorizontalImg,
  },
  {
    title: "Vertical Timeline New",
    des: "Vertical Timeline component new",
    icon: TimelineVerticalImg,
  },
  {
    title: "Horizontal Timeline New",
    des: "Horizontal Timeline component new",
    icon: TimelineHorizontalImg,
  },
  {
    title: "IC Chat Widget",
    des: "IC Chat Widget component",
    icon: ICChatWidget,
  },
  {
    title: "Logs",
    des: "Logs component",
    icon: LogsImg,
  },
  {
    title: "Knowledge Base",
    des: "Knowledge Base component",
    icon: KnowledgeBaseImg,
  },
  {
    title: "Comment",
    des: "Comment component",
    icon: Comment,
  },
  {
    title: "Email",
    des: "Email component",
    icon: Email,
  },
  {
    title: "General Configuration",
    des: "General configuration component",
    icon: General,
  },
  {
    title: "Template Configuration",
    des: "Template configuration component",
    icon: Template,
  },
  {
    title: "SMS component",
    des: "SMS component",
    icon: SMS,
  },
  {
    title: "Tabs",
    des: "Tabs component",
    icon: tabsImg,
  },
];

export const iconTabComp = [
  {
    icon: <Icon icon="tab-comp-1" size={20} />,
    key: "tab-comp-1",
  },
  {
    icon: <Icon icon="tab-comp-2" size={20} />,
    key: "tab-comp-2",
  },
  {
    icon: <Icon icon="tab-comp-3" size={20} />,
    key: "tab-comp-3",
  },
  {
    icon: <Icon icon="tab-comp-4" size={20} />,
    key: "tab-comp-4",
  },
  {
    icon: <Icon icon="tab-comp-5" size={20} />,
    key: "tab-comp-5",
  },
  {
    icon: <Icon icon="tab-comp-6" size={20} />,
    key: "tab-comp-6",
  },
  {
    icon: <Icon icon="tab-comp-7" size={20} />,
    key: "tab-comp-7",
  },
  {
    icon: <Icon icon="tab-comp-8" size={20} />,
    key: "tab-comp-8",
  },
  {
    icon: <Icon icon="tab-comp-9" size={20} />,
    key: "tab-comp-9",
  },
  {
    icon: <Icon icon="tab-comp-10" size={20} />,
    key: "tab-comp-10",
  },
  {
    icon: <Icon icon="tab-comp-11" size={20} />,
    key: "tab-comp-11",
  },
  {
    icon: <Icon icon="tab-comp-12" size={20} />,
    key: "tab-comp-12",
  },
  {
    icon: <Icon icon="tab-comp-13" size={20} />,
    key: "tab-comp-13",
  },
  {
    icon: <Icon icon="tab-comp-14" size={20} />,
    key: "tab-comp-14",
  },
  {
    icon: <Icon icon="tab-comp-15" size={20} />,
    key: "tab-comp-15",
  },
  {
    icon: <Icon icon="tab-comp-16" size={20} />,
    key: "tab-comp-16",
  },
];

export const ContactFields = [
  [
    {
      label: "Name",
      value: "fld_contact_name_00000001",
      type: "formula",
    },
    {
      label: "Phone",
      value: "fld_contact_phone_00000001",
      type: "text",
    },
    {
      label: "CIF",
      value: "fld_contact_cif_00000001",
      type: "text",
    },
    {
      label: "ID card number",
      value: "fld_contact_id_card_number_00000001",
      type: "text",
    },
    {
      label: "Ngày cấp",
      value: "fld_ngaycap_57630107",
      type: "text",
    },
    {
      label: "Nơi cấp",
      value: "fld_noicap_92698894",
      type: "text",
    },
    {
      label: "Email ",
      value: "fld_contact_email_00000001",
      type: "email",
    },
    {
      label: "Street",
      value: "fld_contact_street_00000001",
      type: "text",
    },
    {
      label: "Birthdate ",
      value: "fld_contact_birthdate_00000001",
      type: "date",
    },
    {
      label: "Gender",
      value: "fld_contact_gender_00000001",
      type: "linkingobject",
    },
    {
      label: "Phân Loại KH",
      value: "fld_phanloaikh_30934903",
      type: "text",
    },
    {
      label: "Chi nhánh mở CIF",
      value: "fld_chinhanhmocif_84641276",
      type: "text",
    },
    {
      label: "Dịch vụ SMS banking",
      value: "fld_dichvusmsbanking_16829106",
      type: "text",
    },
    {
      label: "Key",
      value: "fld_key_73570797",
      type: "text",
    },
    {
      label: "Last choice",
      value: "fld_lastchoice_18231288",
      type: "text",
    },
    {
      label: "Source",
      value: "fld_source_39490358",
      type: "text",
    },
    {
      label: "Description ",
      value: "fld_contact_description_00000001",
      type: "textarea",
    },
    {
      label: "Phone ",
      value: "fld_contact_phone_00000001",
      type: "text",
    },
    {
      label: "Other phone",
      value: "fld_contact_other_phone_00000001",
      type: "text",
    },
    {
      label: "Other phone 2",
      value: "fld_contact_other_phone2_00000001",
      type: "text",
    },
    {
      label: "PHONE 3",
      value: "fld_phone3_32962410",
      type: "text",
    },
    {
      label: "Ic id",
      value: "fld_contact_ic_id_00000001",
      type: "text",
    },
    {
      label: "Account Name",
      value: "fld_contact_account_name_00000001",
      type: "linkingobject",
    },
    {
      label: "Salutation ",
      value: "fld_contact_salutation_00000001",
      type: "linkingobject",
    },
    {
      label: "First Name",
      value: "fld_contact_first_name_00000001",
      type: "text",
    },
    {
      label: "Middle Name",
      value: "fld_contact_middle_name_00000001",
      type: "text",
    },
    {
      label: "Last Name",
      value: "fld_contact_last_name_00000001",
      type: "text",
    },
    {
      label: "Name for search",
      value: "fld_contact_name_for_search_00000001",
      type: "text",
    },
    {
      label: "Title ",
      value: "fld_contact_title_00000001",
      type: "text",
    },
    {
      label: "Department ",
      value: "fld_contact_department_00000001",
      type: "text",
    },
    {
      label: "Avatar",
      value: "fld_contact_avatar_00000001",
      type: "file",
    },
    {
      label: "Zalo id",
      value: "fld_contact_zalo_id_00000001",
      type: "text",
    },
    {
      label: "Facebook id",
      value: "fld_contact_facebook_id_00000001",
      type: "text",
    },
    {
      label: "Viber id",
      value: "fld_contact_viber_id_00000001",
      type: "text",
    },
    {
      label: "Webchat id",
      value: "fld_contact_webchat_id_00000001",
      type: "text",
    },
    {
      label: "Email id",
      value: "fld_contact_email_id_00000001",
      type: "text",
    },
    {
      label: "Telegram id",
      value: "fld_contact_telegram_id_00000001",
      type: "text",
    },
    {
      label: "Whatapp id",
      value: "fld_contact_whatapp_id_00000001",
      type: "text",
    },
    {
      label: "Fpt id",
      value: "fld_contact_fpt_id_00000001",
      type: "text",
    },
    {
      label: "Lead Source",
      value: "fld_contact_lead_source_00000001",
      type: "linkingobject",
    },
  ],
];

export const TicketFields = [
  {
    label: "ID",
    value: "fld_ticket_id_00000001",
    type: "text",
  },
  {
    label: "Parent Ticket",
    value: "fld_ticket_parent_ticket_00000001",
    type: "linkingobject",
  },
  {
    label: "Contact",
    value: "fld_ticket_contact_00000001",
    type: "linkingobject",
  },
  {
    label: "Topic LV1",
    value: "fld_topiclv1_94799529",
    type: "select",
  },
  {
    label: "Topic LV2",
    value: "fld_topiclv2_67017392",
    type: "select",
  },
  {
    label: "Các câu hỏi hỗ trợ",
    value: "fld_caccauhoihotro_47855395",
    type: "select",
  },
  {
    label: "Description",
    value: "fld_ticket_description_00000001",
    type: "textarea",
  },
  {
    label: "Solution",
    value: "fld_ticket_solution_00000001",
    type: "textarea",
  },
  {
    label: "Email",
    value: "fld_ticket_email_00000001",
    type: "email",
  },
  {
    label: "Email-CC",
    value: "fld_emailcc_32533454",
    type: "text",
  },
  {
    label: "Flag",
    value: "fld_flag_62575417",
    type: "select",
  },
  {
    label: "Status",
    value: "fld_ticket_status_00000001",
    type: "select",
  },
  {
    label: "Trạng thái xử lý",
    value: "fld_trangthaixuly_78053428",
    type: "select",
  },
  {
    label: "Channel",
    value: "fld_ticket_channel_00000001",
    type: "select",
  },
  {
    label: "SLA Violation",
    value: "fld_ticket_sla_violation_00000001",
    type: "select",
  },
  {
    label: "Phone",
    value: "fld_ticket_phone_00000001",
    type: "text",
  },
  {
    label: "SĐT đăng ký",
    value: "fld_sdtdangky_9647434",
    type: "linkingobject",
  },
  {
    label: "Chi nhánh mở CIF",
    value: "fld_chinhanhmocif_74863728",
    type: "linkingobject",
  },
  {
    label: "CIF",
    value: "fld_cif_79817383",
    type: "linkingobject",
  },
  {
    label: "Assign to Avaya",
    value: "fld_assigntoavaya_32045032",
    type: "text",
  },
  {
    label: "Subject ",
    value: "fld_ticket_subject_00000001",
    type: "text",
  },
  {
    label: "Key contact",
    value: "fld_keycontact_32974758",
    type: "linkingobject",
  },
  {
    label: "Call Type",
    value: "fld_calltype_72369814",
    type: "select",
  },
  {
    label: "Ngày kết thúc",
    value: "fld_ngayketthuc_73322949",
    type: "datetime-local",
  },
  {
    label: "Time Created",
    value: "fld_timecreated_62665533",
    type: "formula",
  },
  {
    label: "Extension",
    value: "fld_extension_56173423",
    type: "text",
  },
  {
    label: "Subject Email",
    value: "fld_subjectemail_44913587",
    type: "text",
  },
  {
    label: "Interaction ID IC",
    value: "fld_interactionidic_51646333",
    type: "text",
  },
  {
    label: "Ticket ID IC",
    value: "fld_ticketidic_11002350",
    type: "text",
  },
  {
    label: "Contact ID IC",
    value: "fld_contactidic_66577754",
    type: "text",
  },
  {
    label: "Contactlinking",
    value: "fld_contactlinking_8963463",
    type: "linkingobject",
  },
  {
    label: "Contact name",
    value: "fld_contactname_69010162",
    type: "linkingobject",
  },
  {
    label: "From",
    value: "fld_from_65841457",
    type: "text",
  },
  {
    label: "Thời gian phản hồi KH",
    value: "fld_thoigianphanhoikh_45335848",
    type: "datetime-local",
  },
  {
    label: "Tự động đóng Email",
    value: "fld_tudongdongemail_36753863",
    type: "select",
  },
  {
    label: "Email phản hồi",
    value: "fld_emailphanhoi_12088713",
    type: "text",
  },
  {
    label: "TG KH gửi email gần nhất",
    value: "fld_tgkhguiemailgannhat_94461841",
    type: "datetime-local",
  },
  {
    label: "Account",
    value: "fld_ticket_account_00000001",
    type: "linkingobject",
  },
  {
    label: "Read mail",
    value: "fld_ticket_read_mail_00000001",
    type: "select",
  },
  {
    label: "Parent Ticket",
    value: "fld_ticket_parent_ticket_00000001",
    type: "linkingobject",
  },
  {
    label: "Time Closed",
    value: "fld_ticket_time_closed_00000001",
    type: "date",
  },
  {
    label: "Processing time",
    value: "fld_ticket_processing_time_00000001",
    type: "formula",
  },
  {
    label: "Processing time2",
    value: "fld_ticket_processing_time2_00000001",
    type: "formula",
  },
  {
    label: "Attach file",
    value: "fld_ticket_attach_file_00000001",
    type: "file",
  },
  {
    label: "Priority",
    value: "fld_ticket_priority_00000001",
    type: "select",
  },
];

export const InteractionFields = [
  {
    label: "ID",
    value: "fld_interaction_interaction_id_00000001",
    type: "ID",
  },
  {
    label: "Type",
    value: "fld_interaction_type_00000001",
    type: "select",
  },
  {
    label: "Channel",
    value: "fld_interaction_channel_00000001",
    type: "select",
  },
  {
    label: "Contact ID",
    value: "fld_contactid_14271203",
    type: "linkingobject",
  },
  {
    label: "Source Lead ID",
    value: "fld_interaction_source_lead_id_00000001",
    type: "linkingobject",
  },
  {
    label: "Source Ticket ID",
    value: "fld_interaction_source_ticket_id_00000001",
    type: "linkingobject",
  },
  {
    label: "Note",
    value: "fld_interaction_note_00000001",
    type: "textarea",
  },
];

export const CallInteractionFields = [
  {
    label: "ID",
    value: "fld_call_detail_id_00000001",
    type: "text",
  },
  {
    label: "Source Interaction ID",
    value: "flld_call_detail_interaction_id_00000001",
    type: "linkingobject",
  },
  {
    label: "Start Time",
    value: "fld_call_detail_start_time_00000001",
    type: "datetime-local",
  },
  {
    label: "End Time",
    value: "fld_call_detail_end_time_00000001",
    type: "datetime-local",
  },
  {
    label: "Third Party ID",
    value: "fld_call_detail_third_party_id_00000001",
    type: "text",
  },
  {
    label: "Extension",
    value: "fld_call_detail_extension_00000001",
    type: "text",
  },
  {
    label: "Extension Transfer",
    value: "fld_call_detail_extension_transfer_00000001",
    type: "text",
  },
  {
    label: "Bill Duration",
    value: "fld_call_detail_bill_duration_00000001",
    type: "text",
  },
  {
    label: "Call Status",
    value: "fld_call_detail_call_status_00000001",
    type: "select",
  },
  {
    label: "Connect Time",
    value: "fld_call_detail_connect_time_00000001",
    type: "text",
  },
  {
    label: "Hold Start Time",
    value: "fld_call_detail_hold_start_time_00000001",
    type: "text",
  },
  {
    label: "Hold End Time",
    value: "fld_call_detail_hold_end_time_00000001",
    type: "text",
  },
  {
    label: "Hotline",
    value: "fld_call_detail_hotline_00000001",
    type: "text",
  },
  {
    label: "Queue ID",
    value: "fld_call_detail_queue_id_00000001",
    type: "text",
  },
  {
    label: "Total Duration",
    value: "fld_call_detail_total_duration_00000001",
    type: "text",
  },
  {
    label: "Talkover Duration",
    value: "fld_call_detail_talkovertotalduration_00000001",
    type: "number",
  },
  {
    label: "Talkover Max Duration",
    value: "fld_call_detail_talkovermaxduration_00000001",
    type: "number",
  },
  {
    label: "Talkover Min Duration",
    value: "fld_call_detail_talkoverminduration_00000001",
    type: "number",
  },
  {
    label: "Talkover Average Duration",
    value: "fld_call_detail_talkoveravgduration_00000001",
    type: "number",
  },
  {
    label: "Talkover Total Events",
    value: "fld_call_detail_talkovertotalevents_00000001",
    type: "number",
  },
  {
    label: "Talkover Percent of Call",
    value: "fld_call_detail_talkoverpercentofcall_00000001",
    type: "number",
  },
  {
    label: "Silence Duration",
    value: "fld_call_detail_silencetotalduration_00000001",
    type: "number",
  },
  {
    label: "Silence Max Duration",
    value: "fld_call_detail_silencemaxduration_00000001",
    type: "number",
  },
  {
    label: "Silence Min Duration",
    value: "fld_call_detail_silenceminduration_00000001",
    type: "number",
  },
  {
    label: "Silence Average Duration",
    value: "fld_call_detail_silenceavgduration_00000001",
    type: "number",
  },
  {
    label: "Silence Total Events",
    value: "fld_call_detail_silencetotalevents_00000001",
    type: "number",
  },
  {
    label: "Silence Percent of Call",
    value: "fld_call_detail_silencepercentofcall_00000001",
    type: "number",
  },
  {
    label: "Pause Duration",
    value: "fld_call_detail_pausetotalduration_00000001",
    type: "number",
  },
  {
    label: "Pause Max Duration",
    value: "fld_call_detail_pausemaxduration_00000001",
    type: "number",
  },
  {
    label: "Pause Min Duration",
    value: "fld_call_detail_pauseminduration_00000001",
    type: "number",
  },
  {
    label: "Pause Average Duration",
    value: "fld_call_detail_pauseavgduration_00000001",
    type: "number",
  },
  {
    label: "Pause Total Events",
    value: "fld_call_detail_pausetotalevents_00000001",
    type: "number",
  },
  {
    label: "Pause Percent of Call",
    value: "fld_call_detail_pausepercentofcall_00000001",
    type: "number",
  },
  {
    label: "Hold Duration",
    value: "fld_call_detail_holdtotalduration_00000001",
    type: "number",
  },
  {
    label: "Hold Max Duration",
    value: "fld_call_detail_holdmaxduration_00000001",
    type: "number",
  },
  {
    label: "Hold Min Duration",
    value: "fld_call_detail_holdminduration_00000001",
    type: "number",
  },
  {
    label: "Hold Average Duration",
    value: "fld_call_detail_holdavgduration_00000001",
    type: "number",
  },
  {
    label: "Hold Total Events",
    value: "fld_call_detail_holdtotalevents_00000001",
    type: "number",
  },
  {
    label: "Hold Percent of Call",
    value: "fld_call_detail_holdpercentofcall_00000001",
    type: "number",
  },
];

export const EmailInteractionFields = [
  {
    label: "ID",
    value: "fld_email_detail_id_00000001",
    type: "ID",
  },
  {
    label: "Source Interaction ID",
    value: "fld_email_detail_interaction_id_00000001",
    type: "linkingobject",
  },
  {
    label: "Subject",
    value: "fld_email_detail_subject_00000001",
    type: "text",
  },
  {
    label: "From",
    value: "fld_email_detail_from_00000001",
    type: "text",
  },
  {
    label: "To",
    value: "fld_email_detail_to_00000001",
    type: "text",
  },
  {
    label: "CC",
    value: "fld_email_detail_cc_00000001",
    type: "text",
  },
  {
    label: "BCC",
    value: "fld_email_detail_bcc_00000001",
    type: "text",
  },
  {
    label: "Body",
    value: "fld_email_body_00000001",
    type: "text",
  },
];

export const ICInteractionFields = [
  {
    label: "ID",
    value: "fld_ic_detail_id_00000001",
    type: "ID",
  },
  {
    label: "Source Interaction ID",
    value: "fld_ic_detail_interaction_id_00000001",
    type: "linkingobject",
  },
  {
    label: "Channel Type",
    value: "fld_ic_detail_channel_type_00000001",
    type: "text",
  },
  {
    label: "Customer Name",
    value: "fld_ic_detail_customer_name_00000001",
    type: "text",
  },
  {
    label: "Sender Type",
    value: "fld_ic_detail_sender_type_00000001",
    type: "text",
  },
  {
    label: "Avatar",
    value: "fld_ic_detail_avatar_00000001",
    type: "text",
  },
  {
    label: "Content Text",
    value: "fld_ic_detail_content_text_00000001",
    type: "text",
  },
  {
    label: "File Url",
    value: "fld_ic_detail_content_file_url_00000001",
    type: "text",
  },
  {
    label: "File Type",
    value: "fld_ic_detail_content_file_type_00000001",
    type: "text",
  },
  {
    label: "Topic",
    value: "fld_ic_detail_topic_00000001",
    type: "text",
  },
  {
    label: "Content File Name",
    value: "fld_ic_detail_content_file_name_00000001",
    type: "text",
  },
  {
    label: "Interaction Type",
    value: "fld_ic_detail_interaction_type_00000001",
    type: "text",
  },
  {
    label: "Content download URL",
    value: "fld_ic_detail_content_download_url_00000001",
    type: "text",
  },
  {
    label: "Completed By",
    value: "fld_ic_detail_complete_by_00000001",
    type: "text",
  },
  {
    label: "Angry Count",
    value: "fld_ic_detail_angry_count_00000001",
    type: "number",
  },
  {
    label: "Care Count",
    value: "fld_ic_detail_care_count_00000001",
    type: "number",
  },
  {
    label: "Haha Count",
    value: "fld_ic_detail_haha_count_00000001",
    type: "number",
  },
  {
    label: "Is Like",
    value: "fld_ic_detail_is_like_00000001",
    type: "number",
  },
  {
    label: "Like Count",
    value: "fld_ic_detail_like_count_00000001",
    type: "number",
  },
  {
    label: "Love Count",
    value: "fld_ic_detail_love_count_00000001",
    type: "number",
  },
  {
    label: "Sad Count",
    value: "fld_ic_detail_sad_count_00000001",
    type: "number",
  },
  {
    label: "Wow Count",
    value: "fld_ic_detail_wow_count_00000001",
    type: "number",
  },
  {
    label: "Sent Time",
    value: "fld_ic_detail_sent_time_00000001",
    type: "text",
  },
  {
    label: "Create Time",
    value: "fld_ic_detail_create_time_00000001",
    type: "text",
  },
  {
    label: "Location Latitude",
    value: "fld_ic_detail_location_lat_00000001",
    type: "text",
  },
  {
    label: "Location Longtitude",
    value: "fld_ic_detail_location_long_0000001",
    type: "text",
  },
  {
    label: "Direction",
    value: "fld_ic_detail_direction_0000001",
    type: "text",
  },
  {
    label: "Subject",
    value: "fld_ic_detail_subject_0000001",
    type: "text",
  },
  {
    label: "From Email",
    value: "fld_ic_detail_from_00000001",
    type: "text",
  },
  {
    label: "To Email",
    value: "fld_ic_detail_to_0000001",
    type: "text",
  },
  {
    label: "CC Email",
    value: "fld_ic_detail_list_cc_00000001",
    type: "text",
  },
  {
    label: "BCC Email",
    value: "fld_ic_detail_list_bcc_00000001",
    type: "text",
  },
  {
    label: "Reply To Email",
    value: "fld_ic_detail_reply_to_00000001",
    type: "text",
  },
  {
    label: "Email Content",
    value: "fld_ic_detail_content_00000001",
    type: "text",
  },
  {
    label: "Email State",
    value: "fld_ic_detail_state_00000001",
    type: "text",
  },
  {
    label: "Email Server Type",
    value: "fld_ic_detail_server_email_type_00000001",
    type: "text",
  },
];
export const CALL_VIEW_TEMPLATES = [
  {
    title: "CallViewTemplate.titleTemplate1",
    desc: "CallViewTemplate.descTemplate1",
    BG: BaseTemplate,
    BG_Review: BaseFrameReview,
    value: "1",
    components: [
      {},
      {},
      {},
      {
        title: "Tabs",
        des: "Tabs component",
        icon: tabsImg,
      },
    ],
  },
  {
    title: "CallViewTemplate.titleTemplate2",
    desc: "CallViewTemplate.descTemplate2",
    BG: DiversityTemplateImg,
    BG_Review: DiversityFrameReview,
    value: "2",
    components: [
      {
        title: "SLA",
        des: "Service-level agreement",
        icon: SLAImg,
      },
      {
        title: "Tabs",
        des: "Tabs component",
        icon: tabsImg,
      },
      {
        title: "Comment",
        des: "Comment component",
        icon: Comment,
      },
      {
        title: "Logs",
        des: "Logs component",
        icon: LogsImg,
      },
    ],
  },
];

export const handlerIconComp = (type) => {
  switch (type) {
    case "details":
    case "DETAILS":
    case "multi_detail":
      return detailsImg;
    case "table":
    case "TABLE":
      return tableImg;
    case "tags":
      return tagsImg;
    case "knowledge":
      return KnowledgeBaseImg;
    case "form":
      return InteractionFormImg;
    default:
      break;
  }
};
