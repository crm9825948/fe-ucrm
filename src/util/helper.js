import moment from "moment";

export const checkConditions = (allCondition, operatorValue, value) => {
  let flag = false;
  if (operatorValue.length === allCondition.length) {
    operatorValue.forEach((item) => {
      if (item === undefined) {
        flag = true;
      }
    });
  } else {
    flag = true;
  }
  value.forEach((item, idx) => {
    if (
      item === "" &&
      operatorValue[idx] !== "empty" &&
      operatorValue[idx] !== "not-empty" &&
      operatorValue[idx] !== "mine" &&
      operatorValue[idx] !== "not-mine" &&
      operatorValue[idx] !== "today" &&
      operatorValue[idx] !== "yesterday" &&
      operatorValue[idx] !== "this-week" &&
      operatorValue[idx] !== "last-week" &&
      operatorValue[idx] !== "this-month" &&
      operatorValue[idx] !== "last-month" &&
      operatorValue[idx] !== "this-year" &&
      operatorValue[idx] !== "$inWorkingTimeUCRM" &&
      operatorValue[idx] !== "$ninWorkingTimeUCRM" &&
      operatorValue[idx] !== "$inWorkingDayUCRM" &&
      operatorValue[idx] !== "$ninWorkingDayUCRM"
    ) {
      flag = true;
    }
    if (item === undefined) {
      flag = true;
    }
  });
  return flag;
};

export const setOperatorCondition = (conditions, operatorVal, valueArray) => {
  conditions.forEach((item, idx) => {
    if (Object.keys(item).length > 0)
      if (Object.keys(item && item.value).length === 1) {
        let keys = Object.keys(item.value);

        if (keys[0] !== "$eq" && keys[0] !== "$ne") {
          operatorVal[idx] = keys[0];
        } else if (keys[0] === "$eq" || keys[0] === "$ne") {
          if (item.value["$eq"] === null) {
            operatorVal[idx] = "empty";
          } else if (item.value["$ne"] === null) {
            operatorVal[idx] = "not-empty";
          } else if (item.value["$eq"] === "mine") {
            operatorVal[idx] = "mine";
          } else if (item.value["$ne"] === "mine") {
            operatorVal[idx] = "not-mine";
          } else if (item.value["$ne"] === "today") {
            operatorVal[idx] = "not-today";
          } else {
            if (
              item.value[keys[0]] === "today" ||
              item.value[keys[0]] === "yesterday" ||
              item.value[keys[0]] === "this-week" ||
              item.value[keys[0]] === "last-week" ||
              item.value[keys[0]] === "this-month" ||
              item.value[keys[0]] === "last-month" ||
              item.value[keys[0]] === "this-year"
            ) {
              operatorVal[idx] = item.value[keys[0]];
            } else {
              operatorVal[idx] = keys[0] === undefined ? "" : keys[0];
            }
          }
        }
        if (item.type === "datetime" || item.type === "datetime-local") {
          if (
            item.value[keys[0]] === "today" ||
            item.value[keys[0]] === "yesterday" ||
            item.value[keys[0]] === "this-week" ||
            item.value[keys[0]] === "last-week" ||
            item.value[keys[0]] === "this-month" ||
            item.value[keys[0]] === "last-month" ||
            item.value[keys[0]] === "this-year"
          ) {
            valueArray[idx] = null;
          } else if (item.value[keys[0]]) {
            valueArray[idx] = moment(item.value[keys[0]]);
          } else {
            valueArray[idx] = null;
          }
        }
        if (item.type === "user") {
          if (keys[0] === "$not") {
            valueArray[idx] = item.value[keys[0]]["$regex"];
          } else {
            valueArray[idx] = item.value[keys[0]];
          }
        } else {
          if (keys[0] === "$not") {
            valueArray[idx] = item.value[keys[0]]["$regex"];
          } else {
            if (
              operatorVal[idx] === "mine" ||
              operatorVal[idx] === "not-mine"
            ) {
              valueArray[idx] = null;
            } else {
              valueArray[idx] = item.value[keys[0]];
            }
          }
        }
      } else if (Object.keys(item && item.value).length > 1) {
        let keys = Object.keys(item.value);
        if (keys[0] === "$gte" && keys[1] === "$lte") {
          operatorVal[idx] = "$between";
        } else {
          operatorVal[idx] = keys[0];
        }
        if (keys[0] === "$gte" && keys[1] === "$lte") {
          valueArray[idx] = [
            moment(item.value[keys[0]]),
            moment(item.value[keys[1]]),
          ];
        } else {
          valueArray[idx] = item.value[keys[0]];
        }
      } else {
      }
  });
  return {
    valueArray: valueArray,
    operatorVal: operatorVal,
  };
};
