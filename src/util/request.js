import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import queryString from "query-string";

const request = axios.create({
  baseURL: BASE_URL_API,
  headers: {
    "content-type": "application/json",
  },
  paramsSerializer: (params) => queryString.stringify(params),
});

// Add a request interceptor
request.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("setting_accessToken");
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

//Add a response interceptor
request.interceptors.response.use(
  (response) => {
    if (response && response.data) {
      return response.data;
    }
    return response;
  },
  (error) => {
    // eslint-disable-next-line
    const originalRequest = error.config;
    if (error.response.status === 401) {
      localStorage.removeItem("setting_accessToken");
      localStorage.removeItem("setting_refreshToken");
      localStorage.removeItem("themeColor");
      localStorage.removeItem("i18nextLng");
      localStorage.removeItem("redux-root");
      localStorage.removeItem("settings");

      //IC
      localStorage.removeItem("icIntegration_accessToken");
      localStorage.removeItem("icIntegration_tenantID");
      localStorage.removeItem("icIntegration_username");
      localStorage.removeItem("icIntegration_is_administrator");
      localStorage.removeItem("icIntegration_link_api");
      localStorage.removeItem("icIntegration_refreshToken");
      localStorage.removeItem("icIntegration_current_connectionID");
      localStorage.removeItem("icIntegration_useIC");

      delete axios.defaults.headers.common["Authorization"];
      window.location = "/login";
      return Promise.reject(error);
    }

    // Now not implement back end for  refresh token
    // if (error.response.status === 401 && !originalRequest._retry) {
    //   originalRequest._retry = true
    //   const refreshToken = localStorageService.getRefreshToken()
    //   return axios
    //     .post('/auth/token', {
    //       refresh_token: refreshToken
    //     })
    //     .then((res) => {
    //       if (res.status === 201) {
    //         localStorageService.setToken(res.data)
    //         axios.defaults.headers.common['Authorization'] =
    //           'Bearer ' + localStorageService.getAccessToken()
    //         return axios(originalRequest)
    //       }
    //     })
    // }
    return Promise.reject(error);
  }
);
export default request;
