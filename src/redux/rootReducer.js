import { combineReducers } from "redux";
import storage from "redux-persist/lib/storage";
import consolidatedViewSettingsReducer from "redux/slices/consolidatedViewSettings";
import criteriaSharingReducer from "redux/slices/criteriaSharing";
import dashboardReducer from "redux/slices/dashboard";
import dynamicButtonReducer from "redux/slices/dynamicButton";
import exportHistoryReducer from "redux/slices/exportHistory";
import groupReducer from "redux/slices/group";
import kanbanViewReducer from "redux/slices/kanbanView";
import knowledgeBaseSettingReducer from "redux/slices/knowledgeBaseSetting";
import notificationReducer from "redux/slices/notification";
import objectsReducer from "redux/slices/objects";
import picklistReducer from "redux/slices/picklist";
import profileReducer from "redux/slices/profile";
import reportReducer from "redux/slices/report";
import reportDetailsReducer from "redux/slices/reportDetails";
import rolesReducer from "redux/slices/roles";
import sharingReducer from "redux/slices/sharing";
import smsTemplateReducer from "redux/slices/smsTemplate";
import userReducer from "redux/slices/user";
import workflowsReducer from "redux/slices/workflows";
import authenticatedReducer from "../redux/slices/authenticated";
import brandNameReducer from "../redux/slices/brandName";
import componentEmail from "../redux/slices/email";
import emailIncomingReducer from "../redux/slices/emailIncoming";
import emailOutgoingReducer from "../redux/slices/emailOutgoing";
import emailTemplateReducer from "../redux/slices/emailTemplate";
import fieldsManagementReducer from "../redux/slices/fieldsManagement";
import globalReducer from "../redux/slices/global";
import kanbanViewSettingReducer from "../redux/slices/kanbanViewSetting";
import objectsManagementReducer from "../redux/slices/objectsManagement";
import relatedObjectReducer from "../redux/slices/relatedObject";
import ruleIncomingReducer from "../redux/slices/ruleIncoming";
import smsOutgoingReducer from "../redux/slices/smsOutgoing";
import componentKnowledgeBaseReducer from "../redux/slices/componentKnowledgeBase";
import datetimeSettingReducer from "redux/slices/datetimeSetting";
import knowledgeBaseViewReducer from "../redux/slices/knowledgeBaseView";
import articlesReducer from "../redux/slices/articles";
import slaSettingReducer from "redux/slices/slaSetting";
import googleIntegrationReducer from "redux/slices/googleIntegration";
import tenantsReducer from "redux/slices/tenants";
import campaignReducer from "redux/slices/campaign";
import callCenterReducer from "redux/slices/callCenter";
import profileSettingReducer from "redux/slices/profileSetting";
import highlightSettingReducer from "redux/slices/highlightSetting";
import ldapReducer from "redux/slices/ldap";
import voiceBiometricReducer from "redux/slices/voiceBiometric";
import commentReducer from "redux/slices/comment";
import calendarReducer from "redux/slices/calendar";
import outlookReducer from "redux/slices/outlook";
import duplicateRulesReducer from "redux/slices/duplicateRules";
import systemLogReducer from "redux/slices/systemLog";
import assignmentRuleReducer from "redux/slices/assignmentRule";
import componentSMSReducer from "redux/slices/componentSMS";
import agentMonitorReducer from "redux/slices/agentMonitor";
import knowledgeEnhancementReducer from "redux/slices/knowledgeEnhancement";
import enhancementViewReducer from "./slices/enhancementView";
import enhancementComponentReducer from "./slices/enhancementComponent";
import formSettingReducer from "./slices/formSetting";
import approvalProcessesReducer from "./slices/approvalProcesses";
import icChatWidgetReducer from "./slices/icChatWidget";
import searchManagementReducer from "./slices/searchManagement";
import templateFileReducer from "./slices/templateFile";
import qualityManagementReducer from "./slices/QualityManagement";
import objectStandardReducer from "./slices/objectStandard";
import monitorReducer from "./slices/monitoring";
import customReportReducer from "./slices/customReport";
import callViewReducer from "./slices/callView";

// ----------------------------------------------------------------------

const rootPersistConfig = {
  key: "root",
  storage,
  keyPrefix: "redux-",
  whitelist: [],
};

const rootReducer = combineReducers({
  authenticatedReducer: authenticatedReducer,
  objectsManagementReducer: objectsManagementReducer,
  fieldsManagementReducer: fieldsManagementReducer,
  rolesReducer: rolesReducer,
  profileReducer: profileReducer,
  relatedObjectReducer: relatedObjectReducer,
  userReducer: userReducer,
  objectsReducer: objectsReducer,
  groupReducer: groupReducer,
  reportReducer: reportReducer,
  componentEmail: componentEmail,
  reportDetailsReducer: reportDetailsReducer,
  sharingReducer: sharingReducer,
  criteriaSharingReducer: criteriaSharingReducer,
  picklistReducer: picklistReducer,
  workflowsReducer: workflowsReducer,
  emailIncomingReducer: emailIncomingReducer,
  globalReducer: globalReducer,
  emailOutgoingReducer: emailOutgoingReducer,
  ruleIncomingReducer: ruleIncomingReducer,
  consolidatedViewSettingsReducer: consolidatedViewSettingsReducer,
  emailTemplateReducer: emailTemplateReducer,
  brandNameReducer: brandNameReducer,
  smsOutgoingReducer: smsOutgoingReducer,
  kanbanViewSettingReducer: kanbanViewSettingReducer,
  kanbanViewReducer: kanbanViewReducer,
  dynamicButtonReducer: dynamicButtonReducer,
  dashboardReducer: dashboardReducer,
  exportHistoryReducer: exportHistoryReducer,
  smsTemplateReducer: smsTemplateReducer,
  notificationReducer: notificationReducer,
  knowledgeBaseSettingReducer: knowledgeBaseSettingReducer,
  componentKnowledgeBaseReducer: componentKnowledgeBaseReducer,
  datetimeSettingReducer: datetimeSettingReducer,
  knowledgeBaseViewReducer: knowledgeBaseViewReducer,
  articlesReducer: articlesReducer,
  slaSettingReducer: slaSettingReducer,
  googleIntegrationReducer: googleIntegrationReducer,
  tenantsReducer: tenantsReducer,
  campaignReducer: campaignReducer,
  callCenterReducer: callCenterReducer,
  profileSettingReducer: profileSettingReducer,
  highlightSettingReducer: highlightSettingReducer,
  ldapReducer: ldapReducer,
  voiceBiometricReducer: voiceBiometricReducer,
  commentReducer: commentReducer,
  calendarReducer: calendarReducer,
  outlookReducer: outlookReducer,
  duplicateRulesReducer: duplicateRulesReducer,
  systemLogReducer: systemLogReducer,
  assignmentRuleReducer: assignmentRuleReducer,
  componentSMSReducer: componentSMSReducer,
  agentMonitorReducer: agentMonitorReducer,
  knowledgeEnhancementReducer: knowledgeEnhancementReducer,
  enhancementViewReducer: enhancementViewReducer,
  enhancementComponentReducer: enhancementComponentReducer,
  formSettingReducer: formSettingReducer,
  approvalProcessesReducer: approvalProcessesReducer,
  icChatWidgetReducer: icChatWidgetReducer,
  searchManagementReducer: searchManagementReducer,
  templateFileReducer: templateFileReducer,
  qualityManagementReducer: qualityManagementReducer,
  objectStandardReducer: objectStandardReducer,
  monitorReducer: monitorReducer,
  customReportReducer: customReportReducer,
  callViewReducer: callViewReducer,
});

export { rootPersistConfig, rootReducer };
