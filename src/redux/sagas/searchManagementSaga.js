import { put, takeLatest } from "@redux-saga/core/effects";
import {
  loadDataSettingAPI,
  loadListObjectAPI,
  toggleGlobalSearchAPI,
  updateSearchManagementAPI,
} from "redux/api/searchManagementAPI";
import {
  loadDataSetting,
  loadDataSettingSuccess,
  loadListObject,
  loadListObjectSuccess,
  toggleGlobalSearch,
  updateSearchManagement,
} from "redux/slices/searchManagement";
import { Notification } from "components/Notification/Noti";
import { setShowModalConfirm } from "redux/slices/global";

export default function* searchManagementSaga() {
  yield takeLatest(loadDataSetting.type, loadDataSettingSaga);
  yield takeLatest(loadListObject.type, loadListObjectSaga);
  yield takeLatest(toggleGlobalSearch.type, toggleGlobalSearchSaga);
  yield takeLatest(updateSearchManagement.type, updateSearchManagementSaga);
}

function* loadDataSettingSaga(action) {
  try {
    const { data } = yield loadDataSettingAPI(action.payload);
    yield put(loadDataSettingSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* loadListObjectSaga(action) {
  try {
    const { data } = yield loadListObjectAPI(action.payload);
    yield put(loadListObjectSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* toggleGlobalSearchSaga(action) {
  try {
    yield toggleGlobalSearchAPI(action.payload.data);
    yield put(loadDataSetting(action.payload.reload));
    Notification("success", "Update success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateSearchManagementSaga(action) {
  try {
    yield updateSearchManagementAPI(action.payload.update);
    Notification("success", "Update success");
  } catch (error) {
    // Notification("error", error.response.data.error);
    if (
      error.response.data.error.includes(
        "Are you sure to change the search indexing structure"
      )
    ) {
      yield put(setShowModalConfirm(true));
    }
  }
}
