import { put, takeLatest } from "redux-saga/effects";
import { Notification } from "components/Notification/Noti";
import {
  UpdateListOptionsQuickFilter,
  changeActiveSettings,
  getActiveQMView,
  getActiveQMViewSuccess,
  getAllFormQM,
  getAllFormQMSuccess,
  getChat,
  getChatSuccess,
  getDetailSettings,
  getDetailSettingsSuccess,
  getEmails,
  getEmailsSuccess,
  getInteractionDetail,
  getInteractionDetailSuccess,
  getListAllOptionsQuickFilter,
  getListAllOptionsQuickFilterSuccess,
  getListChatIC,
  getListChatICSuccess,
  getListDataFilter,
  getListDataFilterSuccess,
  getListDetailState,
  getListDetailStateSuccess,
  getListOptionsQuickFilter,
  getListOptionsQuickFilterSuccess,
  getListScoring,
  getListScoringSuccess,
  getListUser,
  getListUserSuccess,
  getMoreListDataFilter,
  getMoreListDataFilterSuccess,
  getTotalRecord,
  getTotalRecordSuccess,
  getURLRecord,
  getURLRecordSuccess,
  loadListObjectFieldQM,
  loadListObjectFieldQMSuccess,
  saveDetailSettings,
  setLoadingMore,
} from "redux/slices/QualityManagement";
import { setShowLoadingScreen } from "redux/slices/global";
import {
  UpdateListOptionsQuickFilterAPI,
  changeActiveSettingsAPI,
  getActiveQMViewAPI,
  getAllFormQMSagaAPI,
  getChatAPI,
  getDetailSettingsAPI,
  getEmailsAPI,
  getInteractionDetailAPI,
  getListAllOptionsQuickFilterAPI,
  getListChatICAPI,
  getListDataFilterAPI,
  getListDetailStateAPI,
  getListOptionsQuickFilterAPI,
  getListScoringAPI,
  getListUserAPI,
  getTotalRecordAPI,
  getURLRecordAPI,
  loadFieldsAPI,
  saveDetailSettingsAPI,
} from "redux/api/QualityManagementAPI";

export default function* qualityManagement() {
  yield takeLatest(changeActiveSettings.type, changeActiveSettingsSaga);
  yield takeLatest(getAllFormQM.type, getAllFormQMSaga);
  yield takeLatest(getDetailSettings.type, getDetailSettingsSaga);
  yield takeLatest(saveDetailSettings.type, saveDetailSettingsSaga);
  yield takeLatest(
    getListOptionsQuickFilter.type,
    getListOptionsQuickFilterSaga
  );
  yield takeLatest(getListDataFilter.type, getListDataFilterSaga);
  yield takeLatest(getMoreListDataFilter.type, getMoreListDataFilterSaga);
  yield takeLatest(getListUser.type, getListUserSaga);
  yield takeLatest(getListScoring.type, getListScoringSaga);
  yield takeLatest(getListDetailState.type, getListDetailStateSaga);
  yield takeLatest(getEmails.type, getEmailsSaga);
  yield takeLatest(getInteractionDetail.type, getInteractionDetailSaga);
  yield takeLatest(getURLRecord.type, getURLRecordSaga);
  yield takeLatest(getTotalRecord.type, getTotalRecordSaga);
  yield takeLatest(getActiveQMView.type, getActiveQMViewSaga);
  yield takeLatest(loadListObjectFieldQM.type, loadListObjectFieldSaga);
  yield takeLatest(getChat.type, getChatSaga);
  yield takeLatest(
    getListAllOptionsQuickFilter.type,
    getListAllOptionsQuickFilterSaga
  );
  yield takeLatest(
    UpdateListOptionsQuickFilter.type,
    UpdateListOptionsQuickFilterSaga
  );
  yield takeLatest(getListChatIC.type, getListChatICSaga);
}

function* changeActiveSettingsSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield changeActiveSettingsAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(getActiveQMView());

    if (action.payload.active) {
      yield put(getDetailSettings());
      Notification("success", "Active successfully!");
    } else {
      yield put(getDetailSettings());
      Notification("success", "Deactive successfully!");
    }
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getAllFormQMSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getAllFormQMSagaAPI(action.payload);
    yield put(getAllFormQMSuccess(response?.data?.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getDetailSettingsSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getDetailSettingsAPI(action.payload);
    yield put(getDetailSettingsSuccess(response?.data?.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* saveDetailSettingsSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield saveDetailSettingsAPI(action.payload);
    Notification("success", "Save successfully!");
    yield put(setShowLoadingScreen(false));
    yield put(getDetailSettings());
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getListOptionsQuickFilterSaga() {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getListOptionsQuickFilterAPI();
    yield put(getListOptionsQuickFilterSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getListAllOptionsQuickFilterSaga() {
  try {
    const response = yield getListAllOptionsQuickFilterAPI();
    yield put(getListAllOptionsQuickFilterSuccess(response.data.data));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getListDataFilterSaga(data) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getListDataFilterAPI(data.payload);
    yield put(getListDataFilterSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getMoreListDataFilterSaga(data) {
  try {
    const response = yield getListDataFilterAPI(data.payload);
    yield put(setLoadingMore(false));
    yield put(getMoreListDataFilterSuccess(response.data.data));
    yield put(setLoadingMore(true));
  } catch (error) {
    yield put(setLoadingMore(true));
    Notification("error", error.response.data.error);
  }
}

function* getListUserSaga(data) {
  try {
    const response = yield getListUserAPI(data.payload);
    yield put(getListUserSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListScoringSaga(data) {
  try {
    const response = yield getListScoringAPI(data.payload);
    yield put(getListScoringSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListDetailStateSaga(data) {
  try {
    const response = yield getListDetailStateAPI(data.payload);
    yield put(getListDetailStateSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getEmailsSaga(data) {
  try {
    const response = yield getEmailsAPI(data.payload);
    yield put(getEmailsSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getInteractionDetailSaga(data) {
  try {
    const response = yield getInteractionDetailAPI(data.payload);
    yield put(getInteractionDetailSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getURLRecordSaga(data) {
  try {
    const response = yield getURLRecordAPI(data.payload);
    yield put(getURLRecordSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getTotalRecordSaga(data) {
  try {
    const response = yield getTotalRecordAPI(data.payload);

    yield put(getTotalRecordSuccess(response?.data?.data?.total_record));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getActiveQMViewSaga() {
  try {
    const response = yield getActiveQMViewAPI();
    yield put(getActiveQMViewSuccess(response?.data?.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* loadListObjectFieldSaga(action) {
  try {
    if (!window.location.pathname.includes("/dashboard")) {
      yield put(setShowLoadingScreen(true));
    }

    const { data } = yield loadFieldsAPI(action.payload);
    yield put(loadListObjectFieldQMSuccess(data?.data));

    if (!window.location.pathname.includes("/dashboard")) {
      yield put(setShowLoadingScreen(false));
    }
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* getChatSaga(action) {
  try {
    const { data } = yield getChatAPI(action.payload);
    yield put(getChatSuccess(data?.data));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateListOptionsQuickFilterSaga(action) {
  try {
    const response = yield UpdateListOptionsQuickFilterAPI(action.payload);
    yield put(getListOptionsQuickFilterSuccess(response.data.data));
    Notification("success", "Update successfully!");
  } catch (error) {}
}

function* getListChatICSaga(action) {
  try {
    const response = yield getListChatICAPI(action.payload);
    yield put(getListChatICSuccess(response.data.data.messages));
  } catch (error) {}
}
