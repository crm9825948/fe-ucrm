import { put, takeEvery } from "@redux-saga/core/effects";

import { setShowLoadingScreen } from "redux/slices/global";
import { loadAllGroups } from "redux/slices/group";
import { loadObject } from "redux/slices/objectsManagement";
import { Notification } from "components/Notification/Noti";
import { loadDetailsReportAPI } from "redux/api/reportAPI";

import {
  loadSpecificReportAPI,
  loadReportArithmeticAPI,
  loadTotalRecordsAPI,
  exportReportAPI,
  unpinReportAPI,
  pinToDashBoardAPI,
  loadAllWidgetsAPI,
  loadAllChartsAPI,
  getDataWidgetAPI,
  getDataChartAPI,
  deleteWidgetAPI,
  addWidgetAPI,
  loadDetailsWidgetAPI,
  updateWidgetAPI,
  deleteChartAPI,
  loadCloumnValueAPI,
  addChartAPI,
  loadDetailsChartAPI,
  updateChartAPI,
  refreshChartAPI,
  refreshWidgetAPI,
  refreshAllReportAPI,
  refreshAllDashboardAPI,
  quickFilterFeatAPI,
} from "redux/api/reportDetailsAPI";
import {
  loadDataNecessary,
  loadSpecificReport,
  loadSpecificReportSuccess,
  loadSpecificReportFail,
  loadTotalRecords,
  setLoadDataWithOutPagi,
  loadReportArithmetic,
  loadReportArithmeticSuccess,
  loadReportArithmeticFail,
  loadTotalRecordsSuccess,
  loadTotalRecordsFail,
  loadInfoReport,
  loadInfoReportSuccess,
  loadInfoReportFail,
  exportReport,
  exportReportSuccess,
  exportReportFail,
  unpinReport,
  unpinReportSuccess,
  unpinReportFail,
  pinToDashBoard,
  pinToDashBoardSuccess,
  pinToDashBoardFail,
  loadAllWidgets,
  loadAllWidgetsSuccess,
  loadAllWidgetsFail,
  loadAllCharts,
  loadAllChartsSuccess,
  loadAllChartsFail,
  getDataWidget,
  getDataWidgetSuccess,
  getDataWidgetFail,
  getDataChart,
  getDataChartSuccess,
  getDataChartFail,
  deleteWidget,
  deleteWidgetSuccess,
  deleteWidgetFail,
  addWidget,
  addWidgetSuccess,
  addWidgetFail,
  loadDetailsWidget,
  loadDetailsWidgetSuccess,
  loadDetailsWidgetFail,
  updateWidget,
  updateWidgetSuccess,
  updateWidgetFail,
  deleteChart,
  deleteChartSuccess,
  deleteChartFail,
  loadCloumnValue,
  loadCloumnValueSuccess,
  loadCloumnValueFail,
  addChart,
  addChartSuccess,
  addChartFail,
  loadDetailsChart,
  loadDetailsChartSuccess,
  loadDetailsChartFail,
  updateChart,
  updateChartSuccess,
  updateChartFail,
  filterReport,
  filterReportSuccess,
  refreshChart,
  refreshWidget,
  refreshAllReport,
  refreshAllDashboard,
  quickFilterFeat,
  quickFilterFeatResult,
} from "redux/slices/reportDetails";

export default function* report() {
  yield takeEvery(loadDataNecessary.type, LoadDataNecessary);
  yield takeEvery(loadSpecificReport.type, LoadSpecificReport);
  yield takeEvery(loadReportArithmetic.type, LoadReportArithmetic);
  yield takeEvery(loadTotalRecords.type, LoadTotalRecords);
  yield takeEvery(loadInfoReport.type, LoadInfoReport);
  yield takeEvery(exportReport.type, ExportReport);
  yield takeEvery(unpinReport.type, UnpinReport);
  yield takeEvery(pinToDashBoard.type, PinToDashBoard);
  yield takeEvery(loadAllWidgets.type, LoadAllWidgets);
  yield takeEvery(loadAllCharts.type, LoadAllCharts);
  yield takeEvery(getDataWidget.type, GetDataWidget);
  yield takeEvery(deleteWidget.type, DeleteWidget);
  yield takeEvery(addWidget.type, AddWidget);
  yield takeEvery(loadDetailsWidget.type, LoadDetailsWidget);
  yield takeEvery(updateWidget.type, UpdateWidget);
  yield takeEvery(getDataChart.type, GetDataChart);
  yield takeEvery(deleteChart.type, DeleteChart);
  yield takeEvery(loadCloumnValue.type, LoadCloumnValue);
  yield takeEvery(addChart.type, AddChart);
  yield takeEvery(loadDetailsChart.type, LoadDetailsChart);
  yield takeEvery(updateChart.type, UpdateChart);
  yield takeEvery(filterReport.type, FilterReport);
  yield takeEvery(refreshChart.type, RefreshChart);
  yield takeEvery(refreshWidget.type, RefreshWidget);
  yield takeEvery(refreshAllReport.type, RefreshAllReport);
  yield takeEvery(refreshAllDashboard.type, RefreshAllDashboard);
  yield takeEvery(quickFilterFeat.type, QuickFilterFeat);
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );

    yield put(loadObject());

    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadSpecificReport(action) {
  try {
    yield put(setLoadDataWithOutPagi(true));
    const response = yield loadSpecificReportAPI(action.payload);
    yield put(loadSpecificReportSuccess(response.data.data));
    yield put(setLoadDataWithOutPagi(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(loadSpecificReportFail(action.payload));
    yield put(setLoadDataWithOutPagi(false));
  }
}

function* LoadReportArithmetic(action) {
  try {
    const response = yield loadReportArithmeticAPI(action.payload);
    yield put(loadReportArithmeticSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(loadReportArithmeticFail());
  }
}

function* LoadTotalRecords(action) {
  try {
    const response = yield loadTotalRecordsAPI(action.payload);
    if (response.data.data) {
      yield put(
        loadTotalRecordsSuccess(response.data.data.pagination.total_record)
      );
    }
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(loadTotalRecordsFail());
  }
}

function* LoadInfoReport(action) {
  try {
    const response = yield loadDetailsReportAPI(action.payload);
    yield put(loadInfoReportSuccess(response.data.data));
  } catch (error) {
    yield put(loadInfoReportFail());
  }
}

function* ExportReport(action) {
  try {
    const response = yield exportReportAPI(action.payload);
    yield put(exportReportSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(exportReportFail(error.response.data.error));
  }
}

function* UnpinReport(action) {
  try {
    const response = yield unpinReportAPI(action.payload);
    yield put(unpinReportSuccess(response.data.data));
  } catch (error) {
    yield put(unpinReportFail(error.response.data.error));
  }
}

function* PinToDashBoard(action) {
  try {
    const response = yield pinToDashBoardAPI(action.payload);
    yield put(pinToDashBoardSuccess(response.data.data));
  } catch (error) {
    yield put(pinToDashBoardFail(error.response.data.error));
  }
}

function* LoadAllWidgets(action) {
  try {
    const response = yield loadAllWidgetsAPI(action.payload);
    yield put(loadAllWidgetsSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllWidgetsFail(error.response.data.error));
  }
}

function* LoadAllCharts(action) {
  try {
    const response = yield loadAllChartsAPI(action.payload);
    yield put(loadAllChartsSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllChartsFail(error.response.data.error));
  }
}

function* GetDataWidget(action) {
  try {
    const response = yield getDataWidgetAPI(action.payload);
    yield put(getDataWidgetSuccess(response.data.data));
  } catch (error) {
    yield put(getDataWidgetFail(action.payload));
  }
}

function* DeleteWidget(action) {
  try {
    const response = yield deleteWidgetAPI(action.payload);
    yield put(deleteWidgetSuccess(response.data.data));
  } catch (error) {
    yield put(deleteWidgetFail(error.response.data.error));
  }
}

function* AddWidget(action) {
  try {
    const response = yield addWidgetAPI(action.payload);
    yield put(addWidgetSuccess(response.data.data));
  } catch (error) {
    yield put(addWidgetFail(error.response.data.error));
  }
}

function* LoadDetailsWidget(action) {
  try {
    const response = yield loadDetailsWidgetAPI(action.payload);
    yield put(loadDetailsWidgetSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsWidgetFail(error.response.data.error));
  }
}

function* UpdateWidget(action) {
  try {
    const response = yield updateWidgetAPI(action.payload);
    yield put(updateWidgetSuccess(response.data.data));
  } catch (error) {
    yield put(updateWidgetFail(error.response.data.error));
  }
}

function* GetDataChart(action) {
  try {
    const response = yield getDataChartAPI(action.payload);
    yield put(getDataChartSuccess(response.data.data));
  } catch (error) {
    yield put(getDataChartFail(action.payload));
  }
}

function* DeleteChart(action) {
  try {
    const response = yield deleteChartAPI(action.payload);
    yield put(deleteChartSuccess(response.data.data));
  } catch (error) {
    yield put(deleteChartFail(error.response.data.error));
  }
}

function* LoadCloumnValue(action) {
  try {
    const response = yield loadCloumnValueAPI(action.payload);
    yield put(loadCloumnValueSuccess(response.data.data));
  } catch (error) {
    yield put(loadCloumnValueFail(error.response.data.error));
  }
}

function* AddChart(action) {
  try {
    const response = yield addChartAPI(action.payload);
    yield put(addChartSuccess(response.data.data));
  } catch (error) {
    yield put(addChartFail(error.response.data.error));
  }
}

function* LoadDetailsChart(action) {
  try {
    const response = yield loadDetailsChartAPI(action.payload);
    yield put(loadDetailsChartSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsChartFail(error.response.data.error));
  }
}

function* UpdateChart(action) {
  try {
    const response = yield updateChartAPI(action.payload);
    yield put(updateChartSuccess(response.data.data));
  } catch (error) {
    yield put(updateChartFail(error.response.data.error));
  }
}

function* FilterReport(action) {
  try {
    yield put(setShowLoadingScreen(true));
    // yield put(loadTotalRecords(action.payload));
    yield put(loadSpecificReport(action.payload));
    yield put(filterReportSuccess());
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* RefreshChart(action) {
  try {
    yield refreshChartAPI(action.payload);
    Notification("success", "Refresh successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* RefreshWidget(action) {
  try {
    yield refreshWidgetAPI(action.payload);
    Notification("success", "Refresh successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* RefreshAllReport(action) {
  try {
    yield refreshAllReportAPI(action.payload);
    Notification("success", "Refresh successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* RefreshAllDashboard(action) {
  try {
    yield refreshAllDashboardAPI(action.payload);
    Notification("success", "Refresh successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* QuickFilterFeat(action) {
  try {
    const response = yield quickFilterFeatAPI(action.payload);
    yield put(quickFilterFeatResult(response.data.data));
    if (action.payload.report_type !== "load") {
      Notification("success", "Successfully!");
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
