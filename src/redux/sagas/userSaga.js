import {
  put,
  takeEvery,
  takeLatest,
  select,
  delay,
} from "@redux-saga/core/effects";
import _ from "lodash";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import fileDownload from "js-file-download";
import { getStatusIC } from "redux/slices/icChatWidget";

import {
  loadUserAPI,
  loadRolesAPI,
  loadUserReportAPI,
  createUserAPI,
  updateUserAPI,
  loadAllUserAPI,
  deleteUserAPI,
  changePasswordAPI,
  changePasswordUserRootAPI,
  changePasswordUserAPI,
  loadUserDetailAPI,
  LoadUsernameApi,
  updateUserSettingAPI,
  searchUserAPI,
  activeUserAPI,
  importUsersAPI,
  generateSampleDataAPI,
  getLogsUserAPI,
  exportUsersAPI,
  getDomainAPI,
  getUserRuleAPI,
  getReportPermissionAPI,
  setUserRuleAPI,
  setUserRuleAllAPI,
  getAllUserAPI,
} from "redux/api/userAPI";
import {
  loadUser,
  loadUserSuccess,
  getUser,
  getUserResult,
  loadAllRole,
  loadAllRoleSuccess,
  loadAllRoleFail,
  loadUserReport,
  loadUserReportSuccess,
  loadUserReportFail,
  createUser,
  createUserSuccess,
  createUserFail,
  updateUser,
  updateUserSuccess,
  updateUserFail,
  loadAllUser,
  loadAllUserSuccess,
  getAllUser,
  getAllUserResult,
  deleteUser,
  deleteUserSuccess,
  deleteUserFail,
  changePass,
  changePassSuccess,
  changePassFail,
  loadUserDetail,
  loadUserDetailFail,
  loadUserDetailSuccess,
  getUserName,
  getUserNameSuccess,
  updateUserSetting,
  searchUser,
  searchUserFail,
  searchUserSuccess,
  activeUser,
  activeUserSuccess,
  importUsers,
  generateSampleData,
  exportUsers,
  getLogsUser,
  getLogsUserResult,
  getDomain,
  getDomainResult,
  getUserRule,
  getUserRuleResult,
  getUserRuleGlobal,
  getUserRuleGlobalResult,
  setUserRule,
  setUserRuleAll,
  getReportPermission,
  getReportPermissionResult,
} from "redux/slices/user";
import { Notification } from "components/Notification/Noti";
import { loadProfileDetailSuccess } from "redux/slices/profileSetting";

const userReducer = (state) => state.userReducer;

export default function* user() {
  yield takeEvery(loadUser.type, LoadListUser);
  yield takeEvery(getUser.type, GetUser);
  yield takeEvery(loadAllRole.type, LoadListRole);
  yield takeEvery(loadUserReport.type, LoadUserReport);
  yield takeEvery(createUser.type, CreateUser);
  yield takeEvery(updateUser.type, UpdateUser);
  yield takeEvery(loadAllUser.type, LoadAllUser);
  yield takeEvery(getAllUser.type, GetAllUser);
  yield takeEvery(deleteUser.type, DeleteUser);
  yield takeEvery(changePass.type, ChangePassUser);
  yield takeEvery(loadUserDetail.type, LoadUserDetail);
  yield takeLatest(getUserName.type, LoadUsername);
  yield takeLatest(updateUserSetting.type, updateUserSettingSaga);
  yield takeLatest(searchUser.type, SearchUser);
  yield takeLatest(activeUser.type, ActiveUser);
  yield takeLatest(importUsers.type, ImportUsers);
  yield takeLatest(generateSampleData.type, GenerateSampleData);
  yield takeLatest(getLogsUser.type, GetLogsUser);
  yield takeLatest(exportUsers.type, ExportUsers);
  yield takeLatest(getDomain.type, GetDomain);
  yield takeLatest(getUserRule.type, GetUserRule);
  yield takeLatest(getUserRuleGlobal.type, GetUserRuleGlobal);
  yield takeLatest(setUserRule.type, SetUserRule);
  yield takeLatest(setUserRuleAll.type, SetUserRuleAll);
  yield takeLatest(getReportPermission.type, GetReportPermission);
}

function* SearchUser(action) {
  try {
    yield put(setShowLoadingScreen(true));

    const response = yield searchUserAPI(action.payload);
    yield put(loadUserSuccess(response.data.data));
    yield put(searchUserSuccess());
    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(searchUserFail());
  }
}

function* LoadListUser(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadUserAPI(action.payload);
    yield put(loadUserSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* GetUser(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getAllUserAPI(action.payload);
    yield put(getUserResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadListRole(action) {
  try {
    const response = yield loadRolesAPI();
    yield put(loadAllRoleSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllRoleFail());
  }
}

function* LoadUserReport(action) {
  try {
    const response = yield loadUserReportAPI(action.payload);
    yield put(loadUserReportSuccess(response.data.data));
  } catch (error) {
    yield put(loadUserReportFail());
  }
}

function* CreateUser(action) {
  try {
    const response = yield createUserAPI(action.payload);
    yield put(createUserSuccess(response.data.data));
  } catch (error) {
    yield put(createUserFail(error.response.data.error));
  }
}

function* UpdateUser(action) {
  try {
    const userReducers = yield select(userReducer);
    const response = yield updateUserAPI(action.payload);
    yield put(updateUserSuccess(response.data.data));
    if (userReducers.userDetail._id === action.payload.ID) {
      yield put(
        loadUserDetail({
          _id: action.payload.ID,
        })
      );
    }
  } catch (error) {
    yield put(updateUserFail(error.response.data.error));
  }
}

function* LoadAllUser(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadAllUserAPI(action.payload);
    yield put(loadAllUserSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* GetAllUser(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getAllUserAPI(action.payload);
    yield put(getAllUserResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* DeleteUser(action) {
  try {
    yield deleteUserAPI(action.payload);
    yield put(deleteUserSuccess());
  } catch (error) {
    yield put(deleteUserFail(error.response.data.error));
  }
}

function* ChangePassUser(action) {
  try {
    if (action.payload.isAdmin) {
      yield changePasswordAPI(action.payload.data);
    } else if (action.payload?.user_root) {
      yield changePasswordUserRootAPI(action.payload.data);
    } else {
      yield changePasswordUserAPI(action.payload.data);
    }
    if (action.payload?.firstPass) {
      window.location.reload();
    }
    yield put(changePassSuccess());
    Notification("success", "Change password successfully!");
  } catch (error) {
    yield put(changePassFail(error.response.data.error));
    Notification("error", error.response.data.error);
  }
}

function* LoadUserDetail(action) {
  try {
    if (action.payload._id) {
      const { data } = yield loadUserDetailAPI(action.payload);

      if (!data.data.user_root) {
        yield put(getUserRuleGlobal());
        yield put(getReportPermission());
      }

      if (_.get(data, "data.use_ic", false)) {
        yield put(
          getStatusIC({
            username: _.get(data, "data.username_ic", ""),
          })
        );

        if (!localStorage.getItem("icIntegration_username")) {
          localStorage.setItem(
            "icIntegration_username",
            _.get(data, "data.username_ic", "")
          );
        }
      }

      if (!localStorage.getItem("icIntegration_link_api")) {
        localStorage.setItem(
          "icIntegration_link_api",
          _.get(data, "data.link_api_ic", "")
        );
      }

      let payload = { ...data.data };
      if (!payload.avatar_config) {
        payload.avatar_config = {
          scale: 1,
          position: {
            x: 0,
            y: 0,
          },
          url: "",
        };
      }
      yield put(loadUserDetailSuccess(payload));
      yield put(loadProfileDetailSuccess(payload));
    } else {
      return;
    }
  } catch (error) {
    yield put(loadUserDetailFail(error.response.data.error));
  }
}

function* LoadUsername() {
  try {
    const { data } = yield LoadUsernameApi();
    yield put(getUserNameSuccess(data));
  } catch (error) {}
}

function* updateUserSettingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateUserSettingAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(
      loadUserDetail({
        _id: action.payload._id,
      })
    );
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ActiveUser(action) {
  try {
    yield activeUserAPI(action.payload);
    yield put(activeUserSuccess(action.payload.id));
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ImportUsers(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield importUsersAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", response.data.data, 2);
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* GenerateSampleData(action) {
  try {
    const response = yield generateSampleDataAPI(action.payload);
    fileDownload(response.data, "sample.xlsx");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetLogsUser(action) {
  try {
    const res = yield getLogsUserAPI(action.payload);
    yield put(getLogsUserResult(res.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ExportUsers() {
  try {
    const response = yield exportUsersAPI();
    fileDownload(response.data, "sample.xlsx");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetDomain(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const res = yield getDomainAPI(action.payload);
    yield put(getDomainResult(res.data.data));
    delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(getDomainResult([]));
    Notification("error", error.response.data.error);
  }
}

function* GetUserRule(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const res = yield getUserRuleAPI(action.payload);
    yield put(getUserRuleResult(res.data.data));
    delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(getUserRuleResult([]));
    Notification("error", error.response.data.error);
  }
}

function* GetUserRuleGlobal(action) {
  try {
    const res = yield getUserRuleAPI(action.payload);
    yield put(getUserRuleGlobalResult(res.data.data));
  } catch (error) {
    yield put(getUserRuleGlobalResult([]));
  }
}

function* SetUserRule(action) {
  try {
    const res = yield setUserRuleAPI(action.payload);
    yield put(getUserRuleResult(res.data.data));
    Notification("success", "Change rule successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* SetUserRuleAll(action) {
  try {
    const res = yield setUserRuleAllAPI(action.payload);
    yield put(getUserRuleResult(res.data.data));
    Notification("success", "Change rule successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetReportPermission(action) {
  try {
    const res = yield getReportPermissionAPI(action.payload);
    yield put(getReportPermissionResult(res.data.data));
  } catch (error) {
    yield put(getReportPermissionResult({}));
  }
}
