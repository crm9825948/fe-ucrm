import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import _ from "lodash";

import {
  newFileAPI,
  copyFileAPI,
  uploadTemplateAPI,
  getListTemplateAPI,
  editTemplateAPI,
  deleteTemplateAPI,
} from "redux/api/templateFileAPI";
import {
  newFile,
  newFileResult,
  copyFile,
  copyFileResult,
  uploadTemplate,
  modalConfig,
  getListTemplate,
  getListTemplateResult,
  editTemplate,
  deleteTemplate,
} from "redux/slices/templateFile";
import { Notification } from "components/Notification/Noti";

export default function* user() {
  yield takeLatest(newFile.type, NewFile);
  yield takeLatest(copyFile.type, CopyFile);
  yield takeLatest(uploadTemplate.type, UploadTemplate);
  yield takeLatest(getListTemplate.type, GetListTemplate);
  yield takeLatest(editTemplate.type, EditTemplate);
  yield takeLatest(deleteTemplate.type, DeleteTemplate);
}

function* NewFile(action) {
  try {
    const response = yield newFileAPI(action.payload);
    yield put(newFileResult(_.get(response, "data.data.final_URL", "")));
  } catch (error) {
    yield put(newFileResult(""));
    Notification("error", error.response.data.error);
  }
}
function* CopyFile(action) {
  try {
    const response = yield copyFileAPI(action.payload);
    yield put(copyFileResult(_.get(response, "data.data.final_URL", "")));
  } catch (error) {
    yield put(newFileResult(""));
    Notification("error", error.response.data.error);
  }
}
function* UploadTemplate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield uploadTemplateAPI(action.payload);
    yield put(modalConfig(false));
    yield put(getListTemplate());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}
function* GetListTemplate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getListTemplateAPI(action.payload);
    yield put(getListTemplateResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}
function* EditTemplate(action) {
  try {
    yield editTemplateAPI(action.payload);

    Notification("success", "Edit template successfully!");
    yield put(modalConfig(false));
    yield put(getListTemplate());
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* DeleteTemplate(action) {
  try {
    yield deleteTemplateAPI(action.payload);
    Notification("success", "Delete template successfully!");
    yield put(getListTemplate());
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
