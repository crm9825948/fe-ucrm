import { put, takeLatest } from "@redux-saga/core/effects";
import { deleteArticle, getListArticle, getListArticleSuccess, loadMoreArticle, loadMoreArticleSuccess, updateTotalPageAndRecord } from '../slices/componentKnowledgeBase'
import { loadDataAPI, loadPaginationAPI } from '../api/objectsAPIs'
import { deleteArticleAPI } from "redux/api/componentKnowledgeBaseAPI";
import { setShowLoadingScreen, setShowModalConfirmDelete } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

export default function* componentKnowledgeBaseSaga() {
    yield takeLatest(getListArticle.type, getListArticleSaga)
    yield takeLatest(loadMoreArticle.type, loadMoreArticleSaga)
    yield takeLatest(deleteArticle.type, deleteArticleSaga)
}

function* getListArticleSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield loadDataAPI(action.payload.data)
        const res = yield loadPaginationAPI(action.payload.pagination)
        yield put(updateTotalPageAndRecord({
            totalPage: res.data.data.total_page,
            totalRecord:res.data.data.total_record
        }))
        yield put(getListArticleSuccess(data.data))
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        
    }
}

function* deleteArticleSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield deleteArticleAPI(action.payload)
        yield put(setShowModalConfirmDelete(false))
        yield put(getListArticle({
            current_page: 1,
            object_id: action.payload.object_id,
            record_per_page: 10,
            search_with: {meta: [], data: []}
        }))
        Notification('success', 'Xoá thành công')
    } catch (error) {
        yield put(setShowLoadingScreen(false))
        Notification('error', 'Something went wrong')
    }
}

function* loadMoreArticleSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield loadDataAPI(action.payload.data)
        const res = yield loadPaginationAPI(action.payload.pagination)
        yield put(updateTotalPageAndRecord({
            totalPage: res.data.data.total_page,
            totalRecord:res.data.data.total_record
        }))
        yield put(loadMoreArticleSuccess(data.data))
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
    }
}