import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import {
  exportAgentMonitorAPI,
  getUserStatusAPI,
  getUserStatusLogAPI,
  setStatusAPI,
} from "redux/api/agentMonitorAPI";
import {
  getUserStatus,
  getUserStatusResult,
  getUserStatusLog,
  getUserStatusLogResult,
  changeStatusCRM,
  changeStatusCRMResult,
  exportAgentMonitor,
} from "redux/slices/agentMonitor";
import fileDownload from "js-file-download";
import moment from "moment";

export default function* campaign() {
  yield takeLatest(getUserStatus.type, GetUserStatus);
  yield takeLatest(getUserStatusLog.type, GetUserStatusLog);
  yield takeLatest(changeStatusCRM.type, ChangeStatusCRM);
  yield takeLatest(exportAgentMonitor.type, ExportAgentMonitor);
}

function* GetUserStatus(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getUserStatusAPI(action.payload);
    yield put(getUserStatusResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* GetUserStatusLog(action) {
  try {
    const response = yield getUserStatusLogAPI(action.payload);
    yield put(
      getUserStatusLogResult({
        type: "add",
        data: response.data.data,
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ChangeStatusCRM(action) {
  try {
    yield setStatusAPI(action.payload);
    Notification("success", "Change status successfully!");
    yield put(changeStatusCRMResult(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ExportAgentMonitor(action) {
  try {
    const response = yield exportAgentMonitorAPI(action.payload);
    Notification("success", "Export successfully!");
    fileDownload(
      response.data,
      `Agent_Monitor_${moment().format("YYYY-MM-DD")}.${
        action.payload.format_type
      }`
    );
  } catch (error) {
    error.response.data.text().then((err) => {
      Notification("error", JSON.parse(err).error);
    });
  }
}
