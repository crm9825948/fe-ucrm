import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { getListSMSTemplate } from "redux/slices/smsTemplate";
import { getListSMSTemplateAPI } from "redux/api/smsTemplateAPIs";
import {
  loadCategory,
  loadObjectCategory,
} from "redux/slices/objectsManagement";

import {
  deactivateCampaignAPI,
  activateCampaignAPI,
  checkAvailableCampaignAPI,
  loadCampaignAPI,
  loadObjectsAPI,
  validateObjectsAPI,
  createCampaignAPI,
  loadDetailsCampaignAPI,
  updateCampaignAPI,
  loadInfoComponentAPI,
  loadTemplateComponentAPI,
  deleteCampaignAPI,
  getInfoMailChimpAPI,
  connectMailchimpAPI,
  disconnectMailchimpAPI,
  getListCampaignMailChimpAPI,
  getContentCampaignMailchimpAPI,
} from "redux/api/campaignAPI";
import {
  loadFieldsAPI,
  loadRecordDataAPI,
  updateRecordAPI,
  loadDataAPI,
  loadPaginationAPI,
} from "redux/api/objectsAPIs";
import { loadDataSuccess, loadPaginationSuccess } from "redux/slices/objects";

// import { loadRecordData } from "redux/slices/objects";
import {
  checkAvailable,
  //   loadFieldsCampaign,
  //   loadFieldsCampaignSuccess,
  loadFieldsCampaignMember,
  loadFieldsCampaignMemberResult,
  loadFieldsCampaignTask,
  loadFieldsCampaignTaskSuccess,
  createCampaign,
  updateCampaign,
  //   loadSetingsCampaign,
  //   loadRulesSuccess,
  //   loadTaskCampaign,
  //   loadTaskCampaignSuccess,
  //   generateTask,
  //   updateStatusGenerateTask,
  //   runCampaign,
  //   stopCampaign,
  activateCampaign,
  deactivateCampaign,
  updateLoadingActive,
  updateActivateCampaign,
  loadCampaign,
  loadCampaignResult,
  loadObjects,
  loadObjectsResult,
  validateObjects,
  validateObjectsResult,
  loadDetailsCampaign,
  loadDetailsCampaignResult,
  loadInfoComponent,
  loadInfoComponentResult,
  loadTemplateComponent,
  loadTemplateComponentResult,
  updateCampaignConfig,
  deleteCampaign,
  getInfoMailChimp,
  getInfoMailChimpResult,
  connectMailchimp,
  disconnectMailchimp,
  getListCampaignMailChimp,
  getListCampaignMailChimpResult,
  getContentCampaignMailchimp,
  getContentCampaignMailchimpResult,
  getTemplateCampaignTasks,
  getTemplateCampaignTasksResult,
} from "redux/slices/campaign";

export default function* campaign() {
  yield takeLatest(checkAvailable.type, CheckAvailable);
  yield takeLatest(loadFieldsCampaignMember.type, LoadFieldsCampaignMember);
  yield takeLatest(loadCampaign.type, LoadCampaign);
  yield takeLatest(loadFieldsCampaignTask.type, LoadFieldsCampaignTask);
  yield takeLatest(createCampaign.type, CreateCampaign);
  yield takeLatest(updateCampaign.type, UpdateCampaign);
  //   yield takeLatest(loadSetingsCampaign.type, LoadSetingsCampaign);
  //   yield takeLatest(loadTaskCampaign.type, LoadTaskCampaign);
  //   yield takeLatest(generateTask.type, GenerateTask);
  //   yield takeLatest(runCampaign.type, RunCampaign);
  //   yield takeLatest(stopCampaign.type, StopCampaign);
  yield takeLatest(activateCampaign.type, ActivateCampaign);
  yield takeLatest(deactivateCampaign.type, DeactivateCampaign);
  yield takeLatest(loadObjects.type, LoadObjects);
  yield takeLatest(validateObjects.type, ValidateObjects);
  yield takeLatest(loadDetailsCampaign.type, LoadDetailsCampaign);
  yield takeLatest(loadInfoComponent.type, LoadInfoComponent);
  yield takeLatest(loadTemplateComponent.type, LoadTemplateComponent);
  yield takeLatest(updateCampaignConfig.type, UpdateCampaignConfig);
  yield takeLatest(deleteCampaign.type, DeleteCampaign);
  yield takeLatest(getInfoMailChimp.type, GetInfoMailChimp);
  yield takeLatest(connectMailchimp.type, ConnectMailchimp);
  yield takeLatest(disconnectMailchimp.type, DisconnectMailchimp);
  yield takeLatest(getListCampaignMailChimp.type, GetListCampaignMailChimp);
  yield takeLatest(
    getContentCampaignMailchimp.type,
    GetContentCampaignMailchimp
  );
  yield takeLatest(getTemplateCampaignTasks.type, GetTemplateCampaignTasks);
}

function* CheckAvailable(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield checkAvailableCampaignAPI(action.payload);
    yield put(updateActivateCampaign(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* ActivateCampaign(action) {
  try {
    yield activateCampaignAPI();
    yield put(updateActivateCampaign(true));
    yield put(loadCategory());
    yield put(loadObjectCategory());
    yield put(updateLoadingActive(false));
    Notification("success", "Activate campaign successfully!");
  } catch (error) {
    yield put(updateLoadingActive(false));
    Notification("error", error.response.data.error);
  }
}

function* DeactivateCampaign(action) {
  try {
    yield deactivateCampaignAPI();
    yield put(updateActivateCampaign(false));
    yield put(loadCategory());
    yield put(loadObjectCategory());
    yield put(updateLoadingActive(false));
    Notification("success", "Deactivate campaign successfully!");
  } catch (error) {
    yield put(updateLoadingActive(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadCampaign(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadCampaignAPI(action.payload);
    yield put(loadCampaignResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadObjects(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadObjectsAPI(action.payload);
    yield put(loadObjectsResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadObjectsResult([]));
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ValidateObjects(action) {
  try {
    yield validateObjectsAPI(action.payload);
    yield put(validateObjectsResult(true));
  } catch (error) {
    yield put(validateObjectsResult(false));
    Notification("error", error.response.data.error);
  }
}

// function* LoadFieldsCampaign(action) {
//   try {
//     yield put(setShowLoadingScreen(true));
//     const response = yield loadFieldsAPI(action.payload);
//     let tempOptionsFields = [];
//     response.data.data.forEach((item) => {
//       if (Object.values(item)[0].readable || Object.values(item)[0].writeable) {
//         if (Object.keys(item)[0] === "main_object") {
//           Object.values(item)[0].sections.forEach((ele) => {
//             ele.fields.forEach((field) => {
//               if (
//                 field.hidden === false &&
//                 field.permission_hidden === false &&
//                 field.type !== "id"
//               ) {
//                 tempOptionsFields.push({
//                   label: field.related_name,
//                   value: field.full_field_id,
//                   type: field.type,
//                   required: field.required,
//                   option: field.option || [],
//                 });
//               }
//             });
//           });
//         }
//       }
//     });

//     yield put(loadFieldsCampaignSuccess(tempOptionsFields));
//     yield put(setShowLoadingScreen(false));
//   } catch (error) {
//     yield put(setShowLoadingScreen(false));
//   }
// }

function* LoadFieldsCampaignTask(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadFieldsAPI(action.payload);
    let tempOptionsFields = [];
    response.data.data.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  required: field.required,
                  option: field.option || [],
                });
              }
            });
          });
        }
      }
    });

    yield put(loadFieldsCampaignTaskSuccess(tempOptionsFields));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateCampaign(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createCampaignAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    window.open(
      `${FE_URL}/consolidated-view/obj_crm_campaign_00001/${response.data.data}`,
      "_self"
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsCampaign(action) {
  try {
    yield put(setShowLoadingScreen(true));

    const result = {
      campaignInfo: {},
      campaignConfig: {},
    };

    const response2 = yield loadDetailsCampaignAPI({
      campaign_id: action.payload.id,
    });

    yield put(setShowLoadingScreen(true));

    const response = yield loadRecordDataAPI(action.payload);
    result.campaignInfo = response.data.data;

    result.campaignConfig = response2.data.data;

    result.campaignConfig.schedule_configs.forEach((item, idx) => {
      let tempDays = [];
      item.applicable_weekdays.forEach((day, index) => {
        if (day) {
          tempDays.push(index);
        }
      });

      result.campaignConfig.schedule_configs[idx] = {
        time: [
          String(
            result.campaignConfig.schedule_configs[idx].from_time.hour
          ).padStart(2, "0") +
            ":" +
            String(
              result.campaignConfig.schedule_configs[idx].from_time.minute
            ).padStart(2, "0"),
          String(
            result.campaignConfig.schedule_configs[idx].to_time.hour
          ).padStart(2, "0") +
            ":" +
            String(
              result.campaignConfig.schedule_configs[idx].to_time.minute
            ).padStart(2, "0"),
        ],
        day: [...tempDays],
      };
    });

    yield put(
      loadObjects({
        campaign_type: result.campaignInfo?.fld_campaign_type_01?.value || "",
      })
    );

    if (result.campaignInfo?.fld_campaign_type_01?.value === "Email") {
      yield put(getListEmailTemplate());
      yield put(
        getListCampaignMailChimp({
          current_page: 1,
          record_per_page: 1000,
        })
      );
    }

    if (result.campaignInfo?.fld_campaign_type_01?.value === "SMS") {
      yield put(getListSMSTemplate());
    }
    yield put(loadDetailsCampaignResult(result));

    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateCampaign(action) {
  try {
    yield put(setShowLoadingScreen(true));

    yield updateRecordAPI({ ...action.payload.campaign_info });

    yield updateCampaignAPI({
      campaign_config: { ...action.payload.campaign_config },
    });

    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield delay(500);
    window.open(
      `${FE_URL}/objects/obj_crm_campaign_00001/default-view`,
      "_self"
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateCampaignConfig(action) {
  try {
    yield updateCampaignAPI(action.payload.newData);

    let newResult = {
      ...action.payload.oldData,
      is_one_time: action.payload.newData.campaign_config.is_one_time,
      checking_workflow_sla:
        action.payload.newData.campaign_config.checking_workflow_sla,
      fld_campaign_running_status_01:
        action.payload.newData.fld_campaign_running_status_01,
    };

    yield put(loadInfoComponentResult(newResult));

    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsCampaignMember(action) {
  try {
    const response = yield loadFieldsAPI(action.payload);

    let tempOptionsFields = [];
    response.data.data.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !field.full_field_id.includes("_01")
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  required: field.required,
                  option: field.option || [],
                });
              }
            });
          });
        }
      }
    });

    yield put(loadFieldsCampaignMemberResult(tempOptionsFields));
  } catch (error) {}
}

// function* LoadSetingsCampaign(action) {
//   try {
//     yield put(setShowLoadingScreen(true));

//     const rules = yield loadTaskRulesAPI(action.payload.task);

//     rules.data.data.schedule_configs.forEach((item, idx) => {
//       let tempDays = [];
//       item.applicable_weekdays.forEach((day, index) => {
//         if (day) {
//           tempDays.push(index);
//         }
//       });

//       rules.data.data.schedule_configs[idx] = {
//         time: [
//           String(rules.data.data.schedule_configs[idx].from_time.hour).padStart(
//             2,
//             "0"
//           ) +
//             ":" +
//             String(
//               rules.data.data.schedule_configs[idx].from_time.minute
//             ).padStart(2, "0"),
//           String(rules.data.data.schedule_configs[idx].to_time.hour).padStart(
//             2,
//             "0"
//           ) +
//             ":" +
//             String(
//               rules.data.data.schedule_configs[idx].to_time.minute
//             ).padStart(2, "0"),
//         ],
//         day: [...tempDays],
//       };
//     });

//     yield put(loadRulesSuccess(rules.data.data));

//     yield put(loadRecordData(action.payload.data_record));

//     yield put(
//       loadTaskCampaign({
//         campaign_id: rules.data.data.campaign_id,
//         current_page: 1,
//         record_per_page: 10,
//       })
//     );

//     yield put(setShowLoadingScreen(false));
//   } catch (error) {
//     yield put(setShowLoadingScreen(false));
//     Notification("error", error.response.data.error);
//   }
// }

function* LoadInfoComponent(action) {
  try {
    const response = yield loadInfoComponentAPI(action.payload);

    const result = response.data.data;

    result.schedule_configs.forEach((item, idx) => {
      let tempDays = [];
      item.applicable_weekdays.forEach((day, index) => {
        if (day) {
          tempDays.push(index);
        }
      });

      result.schedule_configs[idx] = {
        time: [
          String(result.schedule_configs[idx].from_time.hour).padStart(2, "0") +
            ":" +
            String(result.schedule_configs[idx].from_time.minute).padStart(
              2,
              "0"
            ),
          String(result.schedule_configs[idx].to_time.hour).padStart(2, "0") +
            ":" +
            String(result.schedule_configs[idx].to_time.minute).padStart(
              2,
              "0"
            ),
        ],
        day: [...tempDays],
      };
    });

    yield put(loadInfoComponentResult(result));
  } catch (error) {
    yield put(loadInfoComponentResult({}));
    Notification("error", error.response.data.error);
  }
}

function* LoadTemplateComponent(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadTemplateComponentAPI(action.payload);
    yield put(loadTemplateComponentResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadTemplateComponentResult({}));
    Notification("error", error.response.data.error);
  }
}

function* DeleteCampaign(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteCampaignAPI({
      campaign_id: action.payload.data.id,
    });
    const responseData = yield loadDataAPI(action.payload.load);
    const responsePagi = yield loadPaginationAPI(action.payload.load);
    if (action.payload && action.payload.type === "reload") {
    } else {
      yield put(loadDataSuccess(responseData.data.data));
      yield put(loadPaginationSuccess(responsePagi.data.data));
    }
    Notification("success", "Delete successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* GetInfoMailChimp(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getInfoMailChimpAPI(action.payload);
    yield put(getInfoMailChimpResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(getInfoMailChimpResult({}));
    Notification("error", error.response.data.error);
  }
}

function* ConnectMailchimp(action) {
  try {
    const response = yield connectMailchimpAPI();
    yield window.open(response.data.data, "", "width=800,height=1000");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DisconnectMailchimp(action) {
  try {
    yield disconnectMailchimpAPI();
    yield put(getInfoMailChimp());
    Notification("success", "Disconnect successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetListCampaignMailChimp(action) {
  try {
    const response = yield getListCampaignMailChimpAPI(action.payload);
    if (response.data.data.total_record !== 0) {
      yield put(getListCampaignMailChimpResult(response.data.data.campaigns));
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetContentCampaignMailchimp(action) {
  try {
    const response = yield getContentCampaignMailchimpAPI(action.payload);
    yield put(getContentCampaignMailchimpResult(response.data.data));
  } catch (error) {
    yield put(getContentCampaignMailchimpResult({ archive_html: "" }));
    Notification("error", error.response.data.error);
  }
}

function* GetTemplateCampaignTasks() {
  try {
    const response = yield getListSMSTemplateAPI();
    let result = [];
    response.data.data.forEach((item) => {
      if (item.object_id === "obj_crm_campaign_task_00001") {
        result.push(item);
      }
    });

    yield put(getTemplateCampaignTasksResult(result));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(getTemplateCampaignTasksResult([]));
    Notification("error", error.response.data.error);
  }
}
