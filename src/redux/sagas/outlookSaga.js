import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import {
  addAccountAPI,
  deleteAccountAPI,
  loadOutlookAccountAPI,
} from "redux/api/outlookAPI";

import {
  loadOutlookAccount,
  loadOutlookAccountResult,
  addAccount,
  deleteAccount,
} from "redux/slices/outlook";

export default function* ldap() {
  yield takeLatest(loadOutlookAccount.type, LoadOutlookAccount);
  yield takeLatest(addAccount.type, AddAccount);
  yield takeLatest(deleteAccount.type, DeleteAccount);
}

function* LoadOutlookAccount(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadOutlookAccountAPI(action.payload);
    yield put(loadOutlookAccountResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* AddAccount(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield addAccountAPI();
    yield put(setShowLoadingScreen(false));
    yield window.open(response.data.data[0], "", "width=800,height=800");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteAccount(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteAccountAPI(action.payload);
    yield put(loadOutlookAccount());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
