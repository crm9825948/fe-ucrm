import { put, takeLatest } from "@redux-saga/core/effects";

import {
  loadRules,
  loadRulesFail,
  loadRulesSuccess,
  updateRules,
  updateRulesFail,
  updateRulesSuccess,
  updatePosition,
  actionDuplicate,
  deleteDuplicate,
  createRules,
  createDuplicate3rd,
  updateDuplicate3rd,
  notify,
} from "redux/slices/duplicateRules";

import { Notification } from "components/Notification/Noti";
import {
  loadRulesAPI,
  updateRulesAPI,
  updatePositionAPI,
  actionDuplicateAPI,
  deleteDuplicateAPI,
  createRulesAPI,
  createDuplicate3rdAPI,
  updateDuplicate3rdAPI,
  deleteThirdPartyAPI,
  notifyAPI,
} from "redux/api/duplicateRulesAPI";
import { setShowLoadingScreen } from "redux/slices/global";

export default function* duplicateRules() {
  yield takeLatest(loadRules.type, LoadRules);
  yield takeLatest(updateRules.type, UpdateRules);
  yield takeLatest(updatePosition.type, UpdatePosition);
  yield takeLatest(actionDuplicate.type, ActionDuplicate);
  yield takeLatest(deleteDuplicate.type, DeleteDuplicate);
  yield takeLatest(createRules.type, CreateRules);
  yield takeLatest(createDuplicate3rd.type, CreateDuplicate3rd);
  yield takeLatest(updateDuplicate3rd.type, UpdateDuplicate3rd);
  yield takeLatest(notify.type, Notify);
}

function* LoadRules(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadRulesAPI(action.payload);
    yield put(loadRulesSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadRulesFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateRules(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateRulesAPI(action.payload);
    yield put(updateRulesSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(updateRulesFail());
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdatePosition(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updatePositionAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ActionDuplicate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield actionDuplicateAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteDuplicate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    if (action.payload.third_party) {
      yield deleteThirdPartyAPI(action.payload);
    } else {
      yield deleteDuplicateAPI(action.payload);
    }
    const response = yield loadRulesAPI(action.payload);
    yield put(loadRulesSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateRules(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createRulesAPI(action.payload);
    const response = yield loadRulesAPI(action.payload);
    yield put(loadRulesSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateDuplicate3rd(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createDuplicate3rdAPI(action.payload);

    const response = yield loadRulesAPI(action.payload);
    yield put(loadRulesSuccess(response.data.data));

    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateDuplicate3rd(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateDuplicate3rdAPI(action.payload);

    const response = yield loadRulesAPI(action.payload);
    yield put(loadRulesSuccess(response.data.data));

    yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* Notify(action) {
  try {
    // yield put(setShowLoadingScreen(true));
    yield notifyAPI(action.payload);
    // yield put(setShowLoadingScreen(false));
    Notification("success", "Successfully!");
  } catch (error) {
    // yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
