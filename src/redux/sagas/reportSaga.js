import { put, takeEvery, delay } from "@redux-saga/core/effects";
import { loadObject } from "redux/slices/objectsManagement";
import { loadAllGroups } from "redux/slices/group";
import { setShowLoadingScreen } from "redux/slices/global";

import {
  loadFoldersAPI,
  createFoldersAPI,
  updateFoldersAPI,
  deleteFoldersAPI,
  loadReportsAPI,
  loadFieldsAPI,
  loadSharedReportsAPI,
  deleteReportAPI,
  loadDetailsReportAPI,
  updateReportAPI,
  createReportAPI,
  cloneReportAPI,
} from "redux/api/reportAPI";
import {
  loadDataNecessary,
  loadFolders,
  loadFoldersSuccess,
  loadFoldersFail,
  createFolder,
  createFolderSuccess,
  createFolderFail,
  updateFolder,
  updateFolderSuccess,
  updateFolderFail,
  deleteFolder,
  deleteFolderSuccess,
  deleteFolderFail,
  loadReports,
  loadReportsSuccess,
  loadReportsFail,
  loadFields,
  loadFieldsSuccess,
  loadFieldsFail,
  loadSharedReport,
  loadSharedReportSuccess,
  loadSharedReportFail,
  deleteReport,
  deleteReportSuccess,
  deleteReportFail,
  loadDetailsReport,
  loadDetailsReportSuccess,
  loadDetailsReportFail,
  updateReport,
  updateReportSuccess,
  updateReportFail,
  createReport,
  createReportSuccess,
  createReportFail,
  cloneReport,
  cloneReportSuccess,
  cloneReportFail,
} from "redux/slices/report";

export default function* report() {
  yield takeEvery(loadDataNecessary.type, LoadDataNecessary);
  yield takeEvery(loadFolders.type, LoadFolders);
  yield takeEvery(createFolder.type, CreateFolder);
  yield takeEvery(updateFolder.type, UpdateFolder);
  yield takeEvery(deleteFolder.type, DeleteFolder);
  yield takeEvery(loadReports.type, LoadReports);
  yield takeEvery(loadFields.type, LoadFields);
  yield takeEvery(loadSharedReport.type, LoadSharedReports);
  yield takeEvery(deleteReport.type, DeleteReport);
  yield takeEvery(loadDetailsReport.type, LoadDetailsReport);
  yield takeEvery(updateReport.type, UpdateReport);
  yield takeEvery(createReport.type, CreateReport);
  yield takeEvery(cloneReport.type, CloneReport);
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());
    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
  } catch (error) {}
}

function* LoadFolders(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadFoldersAPI();
    yield put(loadFoldersSuccess(response.data.data));
    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadFoldersFail());
  }
}

function* CreateFolder(action) {
  try {
    const response = yield createFoldersAPI(action.payload);
    yield put(createFolderSuccess(response.data.data));
  } catch (error) {
    yield put(createFolderFail(error.response.data.error));
  }
}

function* UpdateFolder(action) {
  try {
    const response = yield updateFoldersAPI(action.payload);
    yield put(updateFolderSuccess(response.data.data));
  } catch (error) {
    yield put(updateFolderFail(error.response.data.error));
  }
}

function* DeleteFolder(action) {
  try {
    const response = yield deleteFoldersAPI(action.payload);
    yield put(deleteFolderSuccess(response.data.data));
  } catch (error) {
    yield put(deleteFolderFail(error.response.data.error));
  }
}

function* LoadReports(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadReportsAPI(action.payload);
    yield put(loadReportsSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadReportsFail(error.response.data.error));
  }
}

function* LoadFields(action) {
  try {
    const response = yield loadFieldsAPI(action.payload);
    yield put(loadFieldsSuccess(response.data.data));
  } catch (error) {
    yield put(loadFieldsFail(error.response.data.error));
  }
}

function* LoadSharedReports(action) {
  try {
    const response = yield loadSharedReportsAPI(action.payload);
    yield put(loadSharedReportSuccess(response.data.data));
  } catch (error) {
    yield put(loadSharedReportFail(error.response.data.error));
  }
}

function* DeleteReport(action) {
  try {
    const response = yield deleteReportAPI(action.payload);
    yield put(deleteReportSuccess(response.data.data));
  } catch (error) {
    yield put(deleteReportFail(error.response.data.error));
  }
}

function* LoadDetailsReport(action) {
  try {
    const response = yield loadDetailsReportAPI(action.payload);
    if (response.data.data) {
      if (response.data.data.report_info.report_type !== "custom_report") {
        let tempData = response.data.data.data;
        let tempReportInfo = response.data.data.report_info;

        tempReportInfo = {
          ...tempReportInfo,
          name: response.data.data.report_info.report_name,
        };

        delete tempReportInfo.created_by;
        delete tempReportInfo.folder_description;
        delete tempReportInfo.folder_name;
        delete tempReportInfo.is_pinned_to_dashboard;
        delete tempReportInfo.linking_obj_condition;
        delete tempReportInfo.main_object_name;
        delete tempReportInfo.meta_filter;
        delete tempReportInfo.readonly;
        delete tempReportInfo.report_name;

        let result = {
          data: tempData,
          report_info: tempReportInfo,
        };

        yield put(loadDetailsReportSuccess(result));
      } else {
        yield put(loadDetailsReportSuccess(response.data.data));
      }
    }
  } catch (error) {
    yield put(loadDetailsReportFail(error.response.data.error));
  }
}

function* UpdateReport(action) {
  try {
    const response = yield updateReportAPI(action.payload);
    yield put(updateReportSuccess(response.data.data));
  } catch (error) {
    yield put(updateReportFail(error.response.data.error));
  }
}

function* CreateReport(action) {
  try {
    const response = yield createReportAPI(action.payload);
    yield put(createReportSuccess(response.data.data));
  } catch (error) {
    yield put(createReportFail(error.response.data.error));
  }
}

function* CloneReport(action) {
  try {
    const response = yield cloneReportAPI(action.payload);
    yield put(cloneReportSuccess(response.data.data));
  } catch (error) {
    yield put(cloneReportFail(error.response.data.error));
  }
}
