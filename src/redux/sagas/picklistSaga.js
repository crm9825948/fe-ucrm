import { put, takeEvery } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  createPicklist,
  createPicklistFail,
  createPicklistSuccess,
  loadPicklist,
  loadPicklistFail,
  loadPicklistSuccess,
  loadDetail,
  loadDetailFail,
  loadDetailSuccess,
  updateDetail,
  updateDetailFail,
  updateDetailSuccess,
  loadValue,
  loadValueFail,
  loadValueSuccess,
  deletePicklist,
  deletePicklistSuccess,
  deletePicklistFail,
} from "redux/slices/picklist";
import {
  createPicklistAPI,
  loadPicklistAPI,
  loadDetailAPI,
  updateDetailAPI,
  loadValueAPI,
  deleteValueAPI,
} from "redux/api/picklistAPI";

export default function* picklist() {
  yield takeEvery(createPicklist, CreatePicklist);
  yield takeEvery(loadPicklist, LoadPickList);
  yield takeEvery(loadDetail, LoadDetail);
  yield takeEvery(updateDetail, UpdateDetail);
  yield takeEvery(loadValue, LoadValue);
  yield takeEvery(deletePicklist, DeletePicklist);
}

function* CreatePicklist(action) {
  try {
    yield createPicklistAPI(action.payload);
    yield put(createPicklistSuccess());
    Notification("success", "Create successfully!");

    const responseData = yield loadPicklistAPI(action.payload);
    yield put(loadPicklistSuccess(responseData.data.data));
  } catch (error) {
    yield put(createPicklistFail());
    Notification("error", error.response.data.mess);
  }
}

function* LoadPickList(action) {
  try {
    const response = yield loadPicklistAPI(action.payload);
    yield put(loadPicklistSuccess(response.data.data));
  } catch (error) {
    yield put(loadPicklistFail());
  }
}

function* LoadDetail(action) {
  try {
    const response = yield loadDetailAPI(action.payload);
    yield put(loadDetailSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailFail());
  }
}

function* UpdateDetail(action) {
  try {
    yield updateDetailAPI(action.payload);
    yield put(updateDetailSuccess());
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(updateDetailFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadValue(action) {
  try {
    const response = yield loadValueAPI(action.payload.data);
    let newObj = { ...action.payload.listValue };
    let temp = {};
    /* eslint-disable-next-line */
    Object.entries(response.data.data).forEach(([key, value], index) => {
      if (value) {
        temp[key] = value;
      }
    });
    newObj[action.payload.ID] = { ...temp };
    yield put(loadValueSuccess(newObj));
  } catch (error) {
    yield put(loadValueFail());
  }
}

function* DeletePicklist(action) {
  try {
    yield deleteValueAPI(action.payload.data);
    const response = yield loadPicklistAPI(action.payload.load);
    yield put(loadPicklistSuccess(response.data.data));
    Notification("success", "Delete successfully!");
    yield put(deletePicklistSuccess());
  } catch (error) {
    yield put(updateDetailFail());
    Notification("error", error.response.data.error);
    yield put(deletePicklistFail());
  }
}
