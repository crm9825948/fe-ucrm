import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";

import { loadLogsAPI } from "redux/api/systemLogAPI";
import {
  loadLogs,
  loadLogsFail,
  loadLogsSuccess,
} from "redux/slices/systemLog";

export default function* systemLog() {
  yield takeLatest(loadLogs.type, LoadLogs);
}

function* LoadLogs(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadLogsAPI(action.payload);
    yield put(loadLogsSuccess(response.data.data));

    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(loadLogsFail());
  }
}
