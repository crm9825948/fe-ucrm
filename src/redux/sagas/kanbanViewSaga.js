import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  getSettingKanbanViewAPI,
  getDataKanbanViewAPI,
  deleteRecordAPI,
  loadPaginationKanbanAPI,
} from "redux/api/kanbanViewAPIs";
import { loadRecordDataAPI, updateRecordAPI } from "redux/api/objectsAPIs";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import {
  getDataKanbanView,
  getDataKanbanViewSuccess,
  getSettingKanban,
  getSettingKanbanSuccess,
  updateCurrentPage,
  updateRecordKanbanOnDrag,
  updateRecordKanban,
  getDataKanbanViewById,
  deleteRecord,
  getPaginationKanban,
  getPaginationKanbanSuccess,
  setCurrentPage,
} from "redux/slices/kanbanView";
import { loadRecordDataSuccess, setShowModal } from "redux/slices/objects";

export default function* kanbanViewSaga() {
  yield takeLatest(getSettingKanban.type, getSettingKanbanViewSaga);
  yield takeLatest(getDataKanbanView.type, getDataKanbanViewSaga);
  yield takeLatest(updateRecordKanbanOnDrag.type, updateRecordKanbanOnDragSaga);
  yield takeLatest(updateCurrentPage.type, getDataKanbanViewSaga);
  yield takeLatest(updateRecordKanban.type, updateRecordKanbanSaga);
  yield takeLatest(getDataKanbanViewById.type, getDataKanbanViewByIdSaga);
  yield takeLatest(deleteRecord.type, deleteRecordSaga);
  yield takeLatest(getPaginationKanban.type, getPaginationKanbanSaga);
}

function* getSettingKanbanViewSaga(action) {
  try {
    const { data } = yield getSettingKanbanViewAPI(action.payload);
    yield put(getSettingKanbanSuccess(data.data));
  } catch (error) {}
}

function* getDataKanbanViewSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getDataKanbanViewAPI(action.payload);
    // data.data.current_page = action.payload.current_page;
    yield put(getDataKanbanViewSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(
      getDataKanbanViewSuccess({
        current_page: 1,
        object_id: "",
        record_per_page: 10,
        search_with: { meta: [], data: [] },
      })
    );
    yield put(setShowLoadingScreen(false));
  }
}

function* updateRecordKanbanOnDragSaga(action) {
  try {
    const { data } = yield loadRecordDataAPI(action.payload.record);
    let payload = {
      data: [],
      id: action.payload.record.id,
      object_id: action.payload.record.object_id,
    };
    Object.keys(data.data).forEach((key) => {
      if (
        typeof data.data[key] == "object" &&
        Array.isArray(data.data[key]) === false
      ) {
        if (key === action.payload.column_field) {
          let item = {
            ...data.data[key],
            id_field: key,
            value: action.payload.value_field,
          };
          delete item.ID;
          payload.data.push(item);
        } else {
          let item = {
            ...data.data[key],
            id_field: key,
          };
          delete item.ID;
          payload.data.push(item);
        }
      }
    });
    yield put(setShowLoadingScreen(true));
    yield updateRecordAPI(payload);
    yield put(
      getDataKanbanView({
        object_id: action.payload.record.object_id,
        search_with: { meta: [], data: [] },
        // record_per_page: 10,
        // current_page: action.payload.current_page,
        first_record_id: action.payload.first_record_id,
        last_record_id: action.payload.last_record_id,
      })
    );
    yield put(setShowLoadingScreen(false));
    Notification("success", "Cập nhật thành công");
  } catch ({ response }) {
    yield put(
      getDataKanbanView({
        object_id: action.payload.record.object_id,
        search_with: { meta: [], data: [] },
        // record_per_page: 10,
        // current_page: action.payload.current_page,
        first_record_id: action.payload.first_record_id,
        last_record_id: action.payload.last_record_id,
      })
    );
    Notification("error", response.data.error);
  }
}

function* updateRecordKanbanSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateRecordAPI(action.payload);
    yield put(
      getDataKanbanView({
        object_id: action.payload.object_id,
        search_with: { meta: [], data: [] },
        // record_per_page: 10,
        // current_page: action.payload.current_page,
        first_record_id: action.payload.first_record_id,
        last_record_id: action.payload.last_record_id,
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(setShowModal(false));
    Notification("success", "Cập nhật thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* getDataKanbanViewByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadRecordDataAPI(action.payload);
    yield put(loadRecordDataSuccess(response.data.data));
    yield put(setShowModal(true));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", "");
    yield put(setShowLoadingScreen(false));
  }
}

function* deleteRecordSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteRecordAPI(action.payload);
    yield put(
      getDataKanbanView({
        object_id: action.payload.object_id,
        search_with: { meta: [], data: [] },
        first_record_id: null,
        last_record_id: null,
      })
    );
    yield put(setShowModalConfirmDelete(false));
    yield put(setCurrentPage(1));
    Notification("success", "Xoá thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", "Something went wrong");
  }
}

function* getPaginationKanbanSaga(action) {
  try {
    const { data } = yield loadPaginationKanbanAPI(action.payload);
    yield put(getPaginationKanbanSuccess(data.data));
  } catch (error) {
    Notification("error", "Something went wrong");
  }
}
