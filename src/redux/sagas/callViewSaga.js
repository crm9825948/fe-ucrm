import _ from "lodash";
import { put, takeLatest } from "redux-saga/effects";
import {
  getTemplateDefaultCallViewAPI,
  updateTemplateCallViewAPI,
} from "redux/api/callViewAPI";
import {
  getTemplateDefaultCallView,
  getTemplateDefaultCallViewSuccess,
  updateTemplateCallView,
} from "redux/slices/callView";
import { Notification } from "components/Notification/Noti";

export default function* callViewSaga() {
  yield takeLatest(
    getTemplateDefaultCallView.type,
    getTemplateDefaultCallViewSaga
  );
  yield takeLatest(updateTemplateCallView.type, updateTemplateCallViewSaga);
}

function* getTemplateDefaultCallViewSaga(action) {
  try {
    const response = yield getTemplateDefaultCallViewAPI(action.payload);
    yield put(getTemplateDefaultCallViewSuccess(_.get(response, "data.data")));
  } catch (error) {
    console.log(error);
  }
}

function* updateTemplateCallViewSaga(action) {
  try {
    yield updateTemplateCallViewAPI(action.payload);
    yield put(
      getTemplateDefaultCallView({
        object_id: "obj_contact_00000001",
      })
    );
    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error);
  }
}
