import { put, takeEvery, delay } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadObject } from "redux/slices/objectsManagement";
import { loadAllGroups } from "redux/slices/group";

import {
  createRuleAPI,
  loadRuleObjectAPI,
  deleteRuleAPI,
  updateRuleAPI,
} from "redux/api/sharingAPI";
import {
  loadDataNecessary,
  createRule,
  createRuleSuccess,
  createRuleFail,
  loadRuleObject,
  loadRuleObjectSuccess,
  loadRuleObjectFail,
  deleteRule,
  deleteRuleSuccess,
  deleteRuleFail,
  updateRule,
  updateRuleSuccess,
  updateRuleFail,
} from "redux/slices/sharing";

export default function* sharing() {
  yield takeEvery(loadDataNecessary.type, LoadDataNecessary);
  yield takeEvery(createRule.type, CreateRule);
  yield takeEvery(loadRuleObject.type, LoadRuleObject);
  yield takeEvery(deleteRule.type, DeleteRule);
  yield takeEvery(updateRule.type, UpdateRule);
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());
    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
    yield delay(800);

    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateRule(action) {
  try {
    const response = yield createRuleAPI(action.payload);
    yield put(createRuleSuccess(response.data.data));
  } catch (error) {
    yield put(createRuleFail(error.response.data.error));
  }
}

function* LoadRuleObject(action) {
  try {
    const response = yield loadRuleObjectAPI(action.payload);
    yield put(loadRuleObjectSuccess(response.data.data));
  } catch (error) {
    yield put(loadRuleObjectFail(error.response.data.error));
  }
}

function* DeleteRule(action) {
  try {
    const response = yield deleteRuleAPI(action.payload);
    yield put(deleteRuleSuccess(response.data.data));
  } catch (error) {
    yield put(deleteRuleFail(error.response.data.error));
  }
}

function* UpdateRule(action) {
  try {
    const response = yield updateRuleAPI(action.payload);
    yield put(updateRuleSuccess(response.data.data));
  } catch (error) {
    yield put(updateRuleFail(error.response.data.error));
  }
}
