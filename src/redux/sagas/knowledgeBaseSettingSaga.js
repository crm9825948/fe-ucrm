import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { deleteKnowledgeBaseSettingAPI, getKnowledgeBaseSettingAPI, updateKnowledgeBaseSettingAPI } from "redux/api/knowledgeBaseSettingAPI";
import { setShowLoadingScreen, setShowModalConfirmDelete } from "redux/slices/global";
import { deleteKnowledgeBaseSetting, getKnowledgeBaseSetting, getKnowledgeBaseSettingSuccess, updateKnowledgeBaseSetting } from "redux/slices/knowledgeBaseSetting";

export default function* knowledgeBaseSettingSaga() {
    yield takeLatest(getKnowledgeBaseSetting.type, getKnowledgeBaseSettingSaga)
    yield takeLatest(updateKnowledgeBaseSetting.type, updateKnowledgeBaseSettingSaga)
    yield takeLatest(deleteKnowledgeBaseSetting.type, deleteKnowledgeBaseSettingSaga)
}

function* getKnowledgeBaseSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield getKnowledgeBaseSettingAPI(action.payload)
        yield put(getKnowledgeBaseSettingSuccess(data.data.length > 0 ? data.data[0] : {
            object_id: '',
            section_name: '',
            category_name: '',
            title: '',
            body: ''
        }))
        yield put(setShowLoadingScreen(false))

    } catch (error) {
        
    }
}

function* updateKnowledgeBaseSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield updateKnowledgeBaseSettingAPI(action.payload)
        yield put(setShowLoadingScreen(false))
        yield put(getKnowledgeBaseSetting())
        Notification('success', 'Thành công')
    } catch (error) {
        yield put(setShowLoadingScreen(false))
        Notification('error', 'Something went wrong')
        
    }
}

function* deleteKnowledgeBaseSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield deleteKnowledgeBaseSettingAPI(action.payload)
        yield put(setShowLoadingScreen(false))
        yield put(setShowModalConfirmDelete(false))
        yield put(getKnowledgeBaseSetting())
        Notification('success', 'Xoá thành công')
    } catch (error) {
        yield put(setShowLoadingScreen(false))
        Notification('error', 'Something went wrong')
        
    }
}