import { put, takeLatest, delay } from "@redux-saga/core/effects";

import { loadObject } from "redux/slices/objectsManagement";
import { loadAllGroups } from "redux/slices/group";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadListObjectField } from "redux/slices/objects";

import {
  createRuleAPI,
  loadRuleObjectAPI,
  deleteRuleAPI,
  updateRuleAPI,
} from "redux/api/criteriaSharingAPI";
import {
  loadDataNecessary,
  createRule,
  createRuleSuccess,
  createRuleFail,
  loadRuleObject,
  loadRuleObjectSuccess,
  loadRuleObjectFail,
  deleteRule,
  deleteRuleSuccess,
  deleteRuleFail,
  updateRule,
  updateRuleSuccess,
  updateRuleFail,
} from "redux/slices/criteriaSharing";

export default function* criteriaSharing() {
  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  yield takeLatest(createRule.type, CreateRule);
  yield takeLatest(loadRuleObject.type, LoadRuleObject);
  yield takeLatest(deleteRule.type, DeleteRule);
  yield takeLatest(updateRule.type, UpdateRule);
}

function* LoadDataNecessary() {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());
    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
    yield delay(800);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateRule(action) {
  try {
    const response = yield createRuleAPI(action.payload);
    yield put(createRuleSuccess(response.data.data));
  } catch (error) {
    yield put(createRuleFail(error.response.data.error));
  }
}

function* LoadRuleObject(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadListObjectField({
        api_version: "2",
        object_id: action.payload.object_id,
        show_meta_fields: true,
      })
    );
    const response = yield loadRuleObjectAPI(action.payload);
    yield put(loadRuleObjectSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadRuleObjectFail(error.response.data.error));
    yield put(setShowLoadingScreen(false));
  }
}

function* DeleteRule(action) {
  try {
    const response = yield deleteRuleAPI(action.payload);
    yield put(deleteRuleSuccess(response.data.data));
  } catch (error) {
    yield put(deleteRuleFail(error.response.data.error));
  }
}

function* UpdateRule(action) {
  try {
    const response = yield updateRuleAPI(action.payload);
    yield put(updateRuleSuccess(response.data.data));
  } catch (error) {
    yield put(updateRuleFail(error.response.data.error));
  }
}
