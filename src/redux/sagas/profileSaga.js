import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";

import { loadObjectCategory } from "redux/slices/objectsManagement";

import {
  loadProfilesAPI,
  deleteProfileAPI,
  createProfileAPI,
  loadDetailProfileAPI,
  updateProfileAPI,
  loadProfileFieldsAPI,
  updateProfileFieldsAPI,
} from "redux/api/profileAPIs";
import {
  loadProfiles,
  loadProfilesSuccess,
  deleteProfile,
  deleteProfileSuccess,
  deleteProfileFail,
  createProfile,
  createProfileSuccess,
  createProfileFail,
  loadAllProfiles,
  loadAllProfilesSuccess,
  loadAllProfilesFail,
  loadDetailProfile,
  loadDetailProfileSuccess,
  updateProfile,
  updateProfileSuccess,
  updateProfileFail,
  loadProfileFields,
  loadProfileFieldsSuccess,
  updateProfileFields,
  updateProfileFieldsSuccess,
  updateProfileFieldsFail,
} from "redux/slices/profile";

export default function* profile() {
  yield takeLatest(loadProfiles.type, LoadProfiles);
  yield takeLatest(loadAllProfiles.type, LoadAllProfiles);
  yield takeLatest(deleteProfile.type, DeleteProfile);
  yield takeLatest(createProfile.type, CreateProfile);
  yield takeLatest(loadDetailProfile.type, LoadDetailProfile);
  yield takeLatest(updateProfile.type, UpdateProfile);
  yield takeLatest(loadProfileFields.type, LoadProfileFields);
  yield takeLatest(updateProfileFields.type, UpdateProfileFields);
}

function* LoadProfiles(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadProfilesAPI(action.payload);
    yield put(loadProfilesSuccess(response.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadAllProfiles(action) {
  try {
    const response = yield loadProfilesAPI(action.payload);
    yield put(loadAllProfilesSuccess(response.data));
  } catch (error) {
    yield put(loadAllProfilesFail());
  }
}

function* DeleteProfile(action) {
  try {
    yield deleteProfileAPI(action.payload);
    yield put(deleteProfileSuccess());
  } catch (error) {
    yield put(deleteProfileFail(error.response.data.error));
  }
}

function* CreateProfile(action) {
  try {
    yield createProfileAPI(action.payload);
    yield put(createProfileSuccess());
  } catch (error) {
    yield put(createProfileFail(error.response.data.error));
  }
}

function* LoadDetailProfile(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailProfileAPI(action.payload);
    if (Object.keys(response.data.data).length > 0) {
      yield put(loadDetailProfileSuccess(response.data.data));
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateProfile(action) {
  try {
    yield updateProfileAPI(action.payload);
    yield put(updateProfileSuccess());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(updateProfileFail(error.response.data.error));
  }
}

function* LoadProfileFields(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadProfileFieldsAPI(action.payload);
    yield put(loadProfileFieldsSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateProfileFields(action) {
  try {
    yield updateProfileFieldsAPI(action.payload);
    yield put(updateProfileFieldsSuccess());
  } catch (error) {
    yield put(updateProfileFieldsFail(error.response.data.error));
  }
}
