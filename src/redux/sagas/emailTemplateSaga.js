import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  createEmailTemplateAPI,
  deleteEmailTemplateAPI,
  getEmailTemplateByIdAPI,
  getListEmailTemplateAPI,
  updateEmailTemplateAPI,
} from "redux/api/emailTemplateAPI";
import {
  deleteEmailTemplate,
  getEmailTempateById,
  getEmailTempateByIdSuccess,
  getListEmailTemplate,
  getListEmailTemplateSuccess,
  saveEmailTemplate,
  setShowDrawer,
  updateEmailTemplate,
} from "redux/slices/emailTemplate";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";

export default function* emailTemplateSaga() {
  yield takeLatest(getListEmailTemplate.type, getListEmailTemplateSaga);
  yield takeLatest(getEmailTempateById.type, getEmailTemplateByIdSaga);
  yield takeLatest(deleteEmailTemplate.type, deleteEmailTemplateSaga);
  yield takeLatest(saveEmailTemplate.type, saveEmailTemplateSaga);
  yield takeLatest(updateEmailTemplate.type, updateEmailTemplateSaga);
}

function* getListEmailTemplateSaga() {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListEmailTemplateAPI();
    yield put(getListEmailTemplateSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.mess);
  }
}
function* getEmailTemplateByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getEmailTemplateByIdAPI(action.payload);
    yield put(getEmailTempateByIdSuccess(data.data[0]));
    yield put(setShowDrawer(true));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.mess);
  }
}
function* deleteEmailTemplateSaga(action) {
  try {
    yield put(setShowModalConfirmDelete(true));
    yield deleteEmailTemplateAPI(action.payload);
    yield put(getListEmailTemplate());
    Notification("success", "Xoá thành công");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    Notification("error", error.response.data.mess);
  }
}
function* saveEmailTemplateSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createEmailTemplateAPI(action.payload);
    yield put(getListEmailTemplate());
    yield put(setShowDrawer(false));
    Notification("success", "Tạo thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}
function* updateEmailTemplateSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateEmailTemplateAPI(action.payload);
    yield put(getListEmailTemplate());
    yield put(setShowDrawer(false));
    Notification("success", "Cập nhật thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
