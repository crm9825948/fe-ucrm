import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { getDefaultBrandName } from "redux/slices/brandName";
import {
  loginAPI,
  forgotPasswordAPI,
  resetPasswordAPI,
  getDataVersionBEAPI,
  getMediaAPI,
} from "../api/authenticatedAPIs";
import {
  login,
  loginError,
  loginSuccessfully,
  forgotPassword,
  forgotPasswordSuccess,
  forgotPasswordFail,
  resetPassword,
  resetPasswordResult,
  getDataVersionBE,
  getDataVersionBEResult,
  getMedia,
  getMediaResult,
} from "../slices/authenticated";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import { setShowLoadingScreen } from "redux/slices/global";

export default function* authenSaga() {
  yield takeLatest(login.type, Login);
  yield takeLatest(forgotPassword.type, ForgotPassword);
  yield takeLatest(resetPassword.type, ResetPassword);
  yield takeLatest(getDataVersionBE.type, GetDataVersionBE);
  yield takeLatest(getMedia.type, GetMedia);
}

function* Login(action) {
  try {
    const response = yield loginAPI(action.payload);
    localStorage.setItem("setting_accessToken", response.data.Token);
    localStorage.setItem("setting_refreshToken", response.data.Refresh_Token);
    localStorage.setItem("themeColor", response.data.theme_color);
    localStorage.setItem("i18nextLng", "en");
    yield put(loginSuccessfully(response.data));
    if (response && response.data && response.data.Tenant === "root") {
      window.open(FE_URL + "/tenants", "_self");
    }
    yield delay(2000);
    Notification("success", "Login successfully!");
    yield put(getDefaultBrandName());
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(loginError(error.response.data.error));
  }
}

function* ForgotPassword(action) {
  try {
    yield forgotPasswordAPI(action.payload);
    yield put(forgotPasswordSuccess());
    Notification("success", "Successfully!");
  } catch (err) {
    yield put(forgotPasswordFail());
    Notification("error", err.response.data.error);
  }
}

function* ResetPassword(action) {
  try {
    yield resetPasswordAPI(action.payload);
    yield put(resetPasswordResult());
    Notification("success", "Reset password successfully!");
    yield delay(2000);
    window.open(FE_URL + "/", "_self");
  } catch (err) {
    yield put(resetPasswordResult());
    Notification("error", err.response.data.error);
  }
}

function* GetDataVersionBE(action) {
  try {
    const response = yield getDataVersionBEAPI(action.payload);
    yield put(getDataVersionBEResult(response.data));
  } catch (err) {
    yield put(getDataVersionBEResult(""));
  }
}

function* GetMedia(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getMediaAPI(action.payload.link);
    if (response?.data) {
      if (response?.data?.type?.split("/")[0] === "image") {
        yield put(getMediaResult(URL.createObjectURL(response.data)));
      } else {
        yield put(getMediaResult("otherType"));
        const link = document.createElement("a");
        link.setAttribute("href", URL.createObjectURL(response.data));
        link.setAttribute("download", action.payload.fileName);
        link.style.display = "none";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    yield put(getMediaResult(""));
    Notification("error", err.response.data.error);
  }
}
