import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { addAccountMappingAPI, createGoogleIntegrationSettingAPI, deleteAccountMappingAPI, deleteGoogleIntegrationSettingAPI, getListAccountMappingAPI, getListGoogleIntegrationSettingAPI,
    updateGoogleIntegrationSettingAPI } from "redux/api/googleIntegrationAPIs";
import { setShowLoadingScreen, setShowModalConfirmDelete } from "redux/slices/global";
import { addAccountMapping, getListAccountMapping, getListAccountMappingSuccess, deleteGoogleIntegrationSetting,
    getListGoogleIntegrationSetting, getListGoogleIntegrationSettingSuccess, createGoogleIntegrationSetting,
    updateGoogleIntegrationSetting, 
    deleteAccountMapping} from "redux/slices/googleIntegration";

export default function* googleIntegrationSaga() {
    yield takeLatest(getListGoogleIntegrationSetting.type, getListGoogleIntegrationSettingSaga)
    yield takeLatest(getListAccountMapping.type, getListAccountMappingSaga)
    yield takeLatest(addAccountMapping.type, addAccountMappingSaga)
    yield takeLatest(createGoogleIntegrationSetting.type, createGoogleIntegrationSettingSaga)
    yield takeLatest(updateGoogleIntegrationSetting.type,updateGoogleIntegrationSettingSaga)
    yield takeLatest(deleteGoogleIntegrationSetting.type, deleteGoogleIntegrationSettingSaga)
    yield takeLatest(deleteAccountMapping.type, deleteAccountMappingSaga)
}

function* getListGoogleIntegrationSettingSaga() {
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield getListGoogleIntegrationSettingAPI()
        yield put(getListGoogleIntegrationSettingSuccess(data.data))
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
    }
}

function* getListAccountMappingSaga(action) {
    try {
        const { data } = yield getListAccountMappingAPI(action.payload)
        yield put(getListAccountMappingSuccess(data.data))
    } catch (error) {
        
    }
}

function* addAccountMappingSaga(action) {
    try {
        const { data } = yield addAccountMappingAPI(action.payload.payload)
        yield action.payload.window.open(data.data, '', "width=800,height=1000")
    } catch (error) {
        
    }
}

function* createGoogleIntegrationSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield createGoogleIntegrationSettingAPI(action.payload)
        yield put(getListGoogleIntegrationSetting())
        Notification('success', 'Tạo thành công')
    } catch (error) {
        Notification('error', 'Something went wrong!')
        yield put(setShowLoadingScreen(false))
    }
}

function* updateGoogleIntegrationSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield updateGoogleIntegrationSettingAPI(action.payload)
        yield Notification('success', 'Cập nhật thành công')
        yield put(getListGoogleIntegrationSetting())
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
    }
}

function* deleteGoogleIntegrationSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield deleteGoogleIntegrationSettingAPI(action.payload)
        yield Notification('success', 'Xoá thành công')
        yield put(setShowModalConfirmDelete(false))
        yield put(getListGoogleIntegrationSetting())
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
        yield Notification('error', 'Something went wrong')
    }
}

function* deleteAccountMappingSaga(action) {
    try {
        try {
            yield put(setShowLoadingScreen(true))
            yield deleteAccountMappingAPI(action.payload.payload)
            yield getListAccountMapping(action.payload.credential_id)
            yield put(setShowLoadingScreen(false))
            yield put(setShowModalConfirmDelete(false))
            Notification('success', 'Xoá thành công')
        } catch (error) {
            Notification('eroor', 'Something went wrong')
            yield put(setShowLoadingScreen(false))
        }
    } catch (error) {
        
    }
}