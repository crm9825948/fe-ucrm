import { put, takeLatest } from "@redux-saga/core/effects";
import {
  createEventAPI,
  getListEventAPI,
  getListEventFilterAPI,
  updateEventAPI,
  deleteEventApi,
  updateStatusAPI,
} from "redux/api/calendarAPI";
import {
  getListEvent,
  getListEventSuccess,
  getListEventFilter,
  getListEventFilterSuccess,
  createEvent,
  createEventSuccess,
  createEventFail,
  updateEvent,
  updateEventSuccess,
  updateEventFail,
  deleteEvent,
  updateStatus,
} from "redux/slices/calendar";

import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";

export default function* CalendarSaga() {
  yield takeLatest(getListEvent.type, getListEventSaga);
  yield takeLatest(getListEventFilter.type, GetListEventFilterSaga);
  yield takeLatest(createEvent.type, CreateEvent);
  yield takeLatest(updateEvent.type, UpdateEvent);
  yield takeLatest(deleteEvent.type, DeleteEvent);
  yield takeLatest(updateStatus.type, UpdateStatus);
}

function* getListEventSaga(action) {
  try {
    const { data } = yield getListEventAPI(action.payload);
    yield put(getListEventSuccess(data));
  } catch (err) {}
}

function* GetListEventFilterSaga(action) {
  try {
    const { data } = yield getListEventFilterAPI(action.payload);
    yield put(getListEventFilterSuccess(data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* CreateEvent(action) {
  try {
    yield createEventAPI(action.payload);
    yield put(createEventSuccess());
    Notification("success", "Create event success");

    if (action.payload.isCall) {
      yield put(getListEvent(action.payload));
      yield put(getListEventFilter(action.payload));
    }
  } catch (error) {
    put(createEventFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateEvent(action) {
  try {
    yield updateEventAPI(action.payload);
    yield put(updateEventSuccess());
    Notification("success", "Update event success");
    if (action.payload.isCall) {
      yield put(getListEvent(action.payload));
      yield put(getListEventFilter(action.payload));
    }
  } catch (error) {
    put(updateEventFail());
    Notification("error", error.response.data.error);
  }
}

function* DeleteEvent(action) {
  try {
    yield deleteEventApi(action.payload);
    Notification("success", "Delete event success");
    if (action.payload.isCall) {
      yield put(getListEvent(action.payload));
      yield put(getListEventFilter(action.payload));
    }
    if (action.payload.isBack) {
      setTimeout(() => {
        window.open(`${FE_URL}/calendar`, "_self");
      }, 1000);
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* UpdateStatus(action) {
  try {
    yield updateStatusAPI(action.payload);
    Notification("success", "Update status success");

    if (action.payload.isCall) {
      yield put(getListEvent(action.payload));
      yield put(getListEventFilter(action.payload));
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
