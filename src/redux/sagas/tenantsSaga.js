import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
import {
  cloneTenantAPI,
  createTenantsAPI,
  disableTenantsAPI,
  getLogIntervalAPI,
  getLogCeleryAPI,
  loadTenantsAPI,
  updateTenantsAPI,
  getAllRootPermissionAPI,
  updateRootAPI,
  createPermissionAPI,
  deletePermissionAPI,
  loadLogAPI,
  resetPasswordAPI,
  healthCheckInteractionAPI,
  createPackageAPI,
  loadPackageAPI,
  loadPackageDetailAPI,
  updatePackageAPI,
  deletePackageAPI,
  getTenantRuleAPI,
  setTenantRuleAPI,
  setTenantRuleAllAPI,
  runIndexAPI,
  allowCloneTenantAPI,
  loadAllObjectAPI,
  updateObjectMediaAPI,
  updateStatusTenantAPI,
  deleteTenantsAPI,
} from "redux/api/tenantsAPI";
import {
  deletePackage,
  updatePackage,
  cloneTenant,
  cloneTenantFail,
  cloneTenantSuccess,
  createTenants,
  createTenantsFail,
  createTenantsSuccess,
  disableTenant,
  disableTenantFail,
  disableTenantSuccess,
  getLogInterval,
  getLogIntervalSuccess,
  getLogCelery,
  getLogCelerySuccess,
  setShowModalCelery,
  loadData,
  loadDataFail,
  loadDataSuccess,
  loadDataToExport,
  loadDataToExportSuccess,
  setShowModalScan,
  loadRootPermission,
  loadRootPermissionFail,
  loadRootPermissionSuccess,
  updateRoot,
  updateRootSuccess,
  updateRootFail,
  createPermission,
  createPermissionFail,
  createPermissionSuccess,
  deletePermission,
  deletePermissionFail,
  deletePermissionSuccess,
  loadLogSuccess,
  loadLogFail,
  loadLog,
  changePass,
  healthCheckInteraction,
  createPackage,
  loadPackage,
  loadPackageSuccess,
  loadPackageDetail,
  loadPackageDetailSuccess,
  getTenantRule,
  getTenantRuleResult,
  setTenantRule,
  setTenantRuleAll,
  runIndex,
  allowCloneTenant,
  loadAllObjectSuccess,
  loadAllObject,
  updateObjectMedia,
  updateStatusTenant,
  deleteTenant,
} from "redux/slices/tenants";
import { FE_URL } from "constants/constants";

export default function* tenants() {
  yield takeLatest(createTenants.type, CreateTenants);
  yield takeLatest(loadData.type, LoadData);
  yield takeLatest(disableTenant.type, DisableTenant);
  yield takeLatest(getLogInterval.type, GetLogInterval);
  yield takeLatest(getLogCelery.type, GetLogCelery);
  yield takeLatest(cloneTenant.type, CloneTenant);
  yield takeLatest(loadDataToExport.type, LoadDataToExport);
  yield takeLatest(loadRootPermission.type, LoadRootPermission);
  yield takeLatest(updateRoot.type, UpdateRoot);
  yield takeLatest(createPermission.type, CreatePermission);
  yield takeLatest(deletePermission.type, DeletePermission);
  yield takeLatest(loadLog.type, LoadLog);
  yield takeLatest(changePass.type, ChangePass);
  yield takeLatest(healthCheckInteraction.type, HealthCheckInteraction);
  yield takeLatest(createPackage.type, CreatePackage);
  yield takeLatest(loadPackage.type, LoadPackageList);
  yield takeLatest(loadPackageDetail.type, LoadPackageDetail);
  yield takeLatest(updatePackage.type, UpdatePackage);
  yield takeLatest(deletePackage.type, DeletePackage);
  yield takeLatest(getTenantRule.type, GetTenantRule);
  yield takeLatest(setTenantRule.type, SetTenantRule);
  yield takeLatest(setTenantRuleAll.type, SetTenantRuleAll);
  yield takeLatest(runIndex.type, RunIndex);
  yield takeLatest(allowCloneTenant.type, AllowCloneTenant);
  yield takeLatest(loadAllObject.type, LoadAllObject);
  yield takeLatest(updateObjectMedia.type, UpdateObjectMedia);
  yield takeLatest(updateStatusTenant.type, UpdateStatusTenant);
  yield takeLatest(deleteTenant.type, DeleteTenant);
}

function* CreateTenants(action) {
  try {
    yield createTenantsAPI(action.payload);
    yield put(createTenantsSuccess());
    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(createTenantsFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadData(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadTenantsAPI(action.payload);
    yield put(loadDataSuccess(response.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadDataFail());
    yield put(setShowLoadingScreen(false));
    Notification("err", error.response.data.error);
  }
}

function* LoadDataToExport(action) {
  try {
    const response = yield loadTenantsAPI({
      ...action.payload,
      Record_per_page: 99999,
    });
    let dataToExport = [];
    Object.entries(response.data.data).forEach(([key, value], index) => {
      // dataToExport.push(value);
      dataToExport.push(value);
    });
    yield put(loadDataToExportSuccess(dataToExport));
  } catch (error) {
    yield put(loadDataFail());
    Notification("err", error.response.data.error);
  }
}

function* DisableTenant(action) {
  if (action.payload.type === "edit") {
    try {
      yield updateTenantsAPI(action.payload.data);
      yield put(loadData(action.payload.load));
      yield put(disableTenantSuccess());
      Notification("success", "Successfully!");
    } catch (error) {
      Notification("error", error.response.data.error);
      yield put(disableTenantFail());
    }
  } else {
    try {
      yield disableTenantsAPI(action.payload.data);
      yield put(loadData(action.payload.load));
      yield put(disableTenantSuccess());
      Notification("success", "Successfully!");
    } catch (error) {
      Notification("error", error.response.data.error);
      yield put(disableTenantFail());
    }
  }
}

function* GetLogInterval() {
  try {
    const response = yield getLogIntervalAPI();
    yield put(getLogIntervalSuccess(response.data.data));
    yield put(setShowModalScan(true));
  } catch (error) {
    Notification(
      "error",
      error?.response?.data?.error || "Do not use Interval!"
    );
  }
}

function* GetLogCelery() {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getLogCeleryAPI();
    yield put(getLogCelerySuccess(response.data));
    yield put(setShowLoadingScreen(false));
    yield put(setShowModalCelery(true));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error?.response?.data?.error || "Do not use Celery!");
  }
}

function* CloneTenant(action) {
  try {
    yield createTenantsAPI(action.payload.dataCreate);
    yield put(createTenantsSuccess());
    yield cloneTenantAPI(action.payload.data);
    yield put(cloneTenantSuccess());
    Notification("success", "Duplicate Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(cloneTenantFail());
  }
}

function* LoadRootPermission(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getAllRootPermissionAPI(action.payload);
    yield put(loadRootPermissionSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadRootPermissionFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateRoot(action) {
  try {
    yield updateRootAPI(action.payload);
    yield put(updateRootSuccess());

    const response = yield getAllRootPermissionAPI(action.payload);
    yield put(loadRootPermissionSuccess(response.data.data));
    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(updateRootFail());
  }
}

function* CreatePermission(action) {
  try {
    yield createPermissionAPI(action.payload);
    yield put(createPermissionSuccess());

    const response = yield getAllRootPermissionAPI(action.payload);
    yield put(loadRootPermissionSuccess(response.data.data));
    Notification("success", "Create successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(createPermissionFail());
  }
}

function* DeletePermission(action) {
  try {
    yield deletePermissionAPI(action.payload);
    yield put(deletePermissionSuccess());

    const response = yield getAllRootPermissionAPI(action.payload);
    yield put(loadRootPermissionSuccess(response.data.data));
    Notification("success", "Delete successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(deletePermissionFail());
  }
}

function* LoadLog(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadLogAPI(action.payload);
    yield put(loadLogSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(loadLogFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* ChangePass(action) {
  try {
    yield resetPasswordAPI(action.payload);
    Notification("success", "Reset successfully!");
  } catch (err) {
    // yield put(loadLogFail());
  }
}

function* HealthCheckInteraction(action) {
  try {
    yield healthCheckInteractionAPI(action.payload);
    Notification("success", "Successfully!");
  } catch (err) {
    Notification("error", err.response.data.error);
  }
}

function* CreatePackage(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createPackageAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    const response = yield loadPackageAPI(action.payload);
    yield put(loadPackageSuccess(response.data.data));

    Notification("success", "Successfully!");
    window.location.replace(FE_URL + "/package");
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    Notification("error", err.response.data.error);
  }
}

function* LoadPackageList(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadPackageAPI(action.payload);
    yield put(loadPackageSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(loadLogFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadPackageDetail(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadPackageDetailAPI(action.payload);
    yield put(loadPackageDetailSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (err) {
    yield put(loadLogFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdatePackage(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updatePackageAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    const response = yield loadPackageAPI(action.payload);
    yield put(loadPackageSuccess(response.data.data));

    Notification("success", "Successfully!");
    window.location.replace(FE_URL + "/package");
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    Notification("error", err.response.data.error);
  }
}

function* DeletePackage(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deletePackageAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    const response = yield loadPackageAPI(action.payload);
    yield put(loadPackageSuccess(response.data.data));

    Notification("success", "Successfully!");
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    Notification("error", err.response.data.error);
  }
}

function* GetTenantRule(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const res = yield getTenantRuleAPI(action.payload);
    yield put(getTenantRuleResult(res.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(getTenantRuleResult([]));
    Notification("error", error.response.data.error);
  }
}

function* SetTenantRule(action) {
  try {
    const res = yield setTenantRuleAPI(action.payload);
    yield put(getTenantRuleResult(res.data.data));
    Notification("success", "Change rule successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* SetTenantRuleAll(action) {
  try {
    const res = yield setTenantRuleAllAPI(action.payload);
    yield put(getTenantRuleResult(res.data.data));
    Notification("success", "Change rule successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* RunIndex(action) {
  try {
    yield runIndexAPI(action.payload);
    Notification("success", "Run index successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* AllowCloneTenant(action) {
  try {
    yield allowCloneTenantAPI(action.payload.data);
    yield put(loadData(action.payload.load));
    Notification("success", "Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadAllObject(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadAllObjectAPI(action.payload);
    yield put(loadAllObjectSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateObjectMedia(action) {
  try {
    yield updateObjectMediaAPI(action.payload);
    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* UpdateStatusTenant(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateStatusTenantAPI(action.payload.data);
    yield put(loadData(action.payload.load));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteTenant(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteTenantsAPI(action.payload.data);
    yield put(loadData(action.payload.load));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(loadData(action.payload.load));
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
