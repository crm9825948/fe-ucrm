import { put, takeEvery, takeLatest } from "@redux-saga/core/effects";
import {
  createGroupAPI,
  deleteGroupAPI,
  loadGroupAPI,
  loadObjectAPI,
  updateGroupAPI,
  createObjectAPI,
  updateObjectAPI,
  deleteObjectAPI,
  loadObjectsAPI,
} from "../api/objectsManagementAPI";
import {
  createGroup,
  createGroupFail,
  createGroupSuccess,
  deleteGroup,
  deleteGroupFail,
  deleteGroupSuccess,
  loadCategory,
  loadCategorySuccess,
  loadCategoryError,
  loadObject,
  loadObjectSuccessfully,
  loadObjectError,
  updateGroup,
  updateGroupFail,
  updateGroupSuccess,
  createObject,
  createObjectFail,
  createObjectSuccess,
  updateObject,
  updateObjectFail,
  updateObjectSuccess,
  deleteObject,
  deleteObjectFail,
  deleteObjectSuccess,
  loadObjectCategory,
  loadObjectCategorySuccess,
  loadObjectCategoryError,
  getListObject,
  getListObjectSuccess,
} from "../slices/objectsManagement";

export default function* objectsManagement() {
  yield takeEvery(loadObject.type, LoadObject);
  yield takeEvery(loadCategory.type, LoadCategory);
  yield takeEvery(loadObjectCategory.type, LoadObjectCategory);
  yield takeEvery(createGroup.type, CreateGroup);
  yield takeEvery(updateGroup.type, UpdateGroup);
  yield takeEvery(deleteGroup.type, DeleteGroup);
  yield takeEvery(createObject.type, CreateObject);
  yield takeEvery(updateObject.type, UpdateObject);
  yield takeEvery(deleteObject.type, DeleteObject);
  yield takeLatest(getListObject.type, GetListObjectSaga);
}

function* LoadObject(action) {
  try {
    const response = yield loadObjectAPI();
    yield put(loadObjectSuccessfully(response.data.data));
  } catch (error) {
    yield put(loadObjectError());
  }
}

function* LoadCategory(action) {
  try {
    const response = yield loadGroupAPI();

    let order = [];
    let data = {};
    let ID = {};
    let visibleList = {};

    // eslint-disable-next-line
    response.data.data.map((item, idx) => {
      order.push(item.name);
      data[`${item.name}`] = item.object_detail;
      ID[`${item.name}`] = item._id;
      visibleList[`${item.name}`] = item.visible;
    });
    yield put(loadCategorySuccess({ data, order, ID, visibleList }));
  } catch (error) {
    yield put(loadCategoryError());
  }
}

function* LoadObjectCategory(action) {
  try {
    const response = yield loadObjectsAPI();

    let order = [];
    let data = {};
    let ID = {};
    let visibleList = {};
    // eslint-disable-next-line
    response.data.data.map((item, idx) => {
      order.push(item.name);
      data[`${item.name}`] = item.object_detail;
      ID[`${item.name}`] = item._id;
      visibleList[`${item.name}`] = item.visible;
    });

    let arr = [];
    response.data.data.forEach((element) => {
      arr = arr.concat(element.object_detail);
    });
    yield put(getListObjectSuccess(arr));

    yield put(loadObjectCategorySuccess({ data, order, ID, visibleList }));
  } catch (error) {
    yield put(loadObjectCategoryError());
  }
}

//Create group
function* CreateGroup(action) {
  try {
    const response = yield createGroupAPI(action.payload);
    yield put(createGroupSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(createGroupFail(error.response.data.error));
  }
}

//Update group
function* UpdateGroup(action) {
  try {
    const response = yield updateGroupAPI(action.payload);
    yield put(updateGroupSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(updateGroupFail(error.response.data.error));
  }
}

//Delete group
function* DeleteGroup(action) {
  try {
    const response = yield deleteGroupAPI(action.payload);
    yield put(deleteGroupSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(deleteGroupFail(error.response.data.error));
  }
}

//Create object
function* CreateObject(action) {
  try {
    const response = yield createObjectAPI(action.payload);
    yield put(createObjectSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(createObjectFail(error.response.data.error));
  }
}

//Update object
function* UpdateObject(action) {
  try {
    const response = yield updateObjectAPI(action.payload);
    yield put(updateObjectSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(loadCategory());
    yield put(updateObjectFail(error.response.data.error));
  }
}

//Delete object
function* DeleteObject(action) {
  try {
    const response = yield deleteObjectAPI(action.payload);
    yield put(deleteObjectSuccess(response.data));
    yield put(loadCategory());
    yield put(loadObjectCategory());
  } catch (error) {
    yield put(deleteObjectFail(error.response.data.error));
  }
}

function* GetListObjectSaga(action) {
  try {
    const { data } = yield loadObjectsAPI();
    let arr = [];
    data.data.forEach((element) => {
      arr = arr.concat(element.object_detail);
    });
    yield put(getListObjectSuccess(arr));
  } catch (error) {}
}
