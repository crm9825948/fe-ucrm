import { put, takeLatest, takeEvery, delay } from "@redux-saga/core/effects";
import _ from "lodash";
import { FE_URL } from "constants/constants.js";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
// import { loadObject } from "redux/slices/objectsManagement";
import { loadObjectFields } from "redux/slices/fieldsManagement";
import { loadFields } from "redux/slices/report";
import { loadAllUser } from "redux/slices/user";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  createRecordAPI,
  loadRecordDataAPI,
  updateRecordAPI as updateRecordListViewAPI,
} from "redux/api/objectsAPIs";

import { loadObjectFieldsAPI } from "redux/api/fieldsManagementAPI";
import { loadFieldsAPI } from "redux/api/objectsAPIs";
import {
  createRecordFail,
  setIsOpenDuplicate,
  setListDuplicate,
  setShowModal,
} from "redux/slices/objects";

import {
  loadAllExposeAPI,
  updateExposeAPI,
  loadAllPopupSettingAPI,
  updatePopupSettingAPI,
  loadPopupAPI,
  popupCheckWorkflowAPI,
  //
  createComponentAPI,
  loadComponentsAPI,
  deleteComponentsAPI,
  updateComponentAPI,
  loadCoreSettingAPI,
  createCoreSettingAPI,
  updateCoreSettingAPI,
  deleteCoreSettingAPI,
  loadDetailsCoreSettingAPI,
  loadExposeCreateAPI,
  createExposeCreateAPI,
  loadDetailsExposeCreateAPI,
  updateExposeCreateAPI,
  deleteExposeCreateAPI,
  //ICIntegration
  loadUsersMappingICAPI,
  createUserMappingICAPI,
  deleteUserMappingICAPI,
  loadDetailsUserMappingICAPI,
  updateUserMappingICAPI,
  getTokenAPI,
  getConfigICAPI,
  createConfigICAPI,
  updateConfigICAPI,
  loadMappingExposeAPI,
  createMappingExposeAPI,
  updateMappingExposeAPI,
  loadLayoutAPI,
  updateLayoutAPI,
  loadDetailsAPI,
  getLogsAPI,
  getAllSlaAPI,
  resetAllICAPI,
  //VoiceBotSetting
  getDetailsVoiceBotAPI,
  createVoiceBotSettingAPI,
  updateVoiceBotSettingAPI,
  //Timeline
  getInteractionsAPI,
  loadObjectsTargetAPI,
  updateInteractionDetailsAPI,
  loadRecordInteractionAPI,
  //CTISetting
  getCTIConfigAPI,
  loadUsersMappingCTIAPI,
  createUserMappingCTIAPI,
  deleteUserMappingCTIAPI,
  loadDetailsUserMappingCTIAPI,
  updateUserMappingCTIAPI,
  updateConfigCTIAPI,
  resetAllCTIAPI,
  generateTokenC247API,
  //
  updateRecordAPI,
  //Callcenter
  getCallCenterConfigAPI,
  loadUsersMappingCallCenterAPI,
  createUserMappingCallCenterAPI,
  updateUserMappingCallCenterAPI,
  deleteUserMappingCallCenterAPI,
  updateConfigCallCenterAPI,
  resetAllConfigCallCenterAPI,
  testConnectionCalabrioAPI,
  updateCallScoringAPI,
  //VoiceBiometric
  getConfigVoiceBiometricAPI,
  updateConfigVoiceBiometricAPI,
  deleteConfigVoiceBiometricAPI,
  createInteractionConfigAPI,
  updateInteractionConfigAPI,
  //
  loadDetailInteractionConfigAPI,
  deleteInteractionConfigAPI,
  loadPageTitleAPI,
  loadConfigComponentsAPI,
} from "redux/api/consolidatedViewSettingsAPI";
import {
  loadDataNecessary,
  //ExposeAPI
  loadAllExpose,
  loadAllExposeSuccess,
  updateExpose,
  updateExposeSuccess,
  //PopupSetting
  loadAllPopupSetting,
  loadAllPopupSettingSuccess,
  updatePopup,
  updatePopupSuccess,
  loadPopup,
  loadPopupSuccess,
  createRecordPopup,
  createRecordPopupSuccess,
  updateRecordPopup,
  updateRecordPopupResult,
  popupCheckWorkflow,
  updateRecordManyParam,
  updateRecordManyParamResult,
  //
  createComponent,
  createComponentSuccesss,
  createComponentFail,
  loadComponents,
  loadComponentsSuccess,
  loadComponentsFail,
  deleteComponent,
  deleteComponentFail,
  deleteComponentSuccess,
  updateComponent,
  updateComponentFail,
  updateComponentSuccess,
  //CoreSetting
  loadDataCoreSetting,
  loadCoreSetting,
  loadCoreSettingSuccess,
  loadFieldsCreate,
  loadFieldsCreateSuccess,
  createCoreSetting,
  createCoreSettingSuccess,
  updateCoreSetting,
  updateCoreSettingSuccess,
  deleteCoreSetting,
  deleteCoreSettingSuccess,
  deleteCoreSettingFail,
  changeStateCoreSetting,
  loadDetailsCoreSetting,
  loadDetailsCoreSettingSuccess,
  loadDetailsCoreSettingFail,
  //ExposeAPICreate
  loadDataExposeCreate,
  loadExposeCreate,
  loadExposeCreateSuccess,
  createExposeCreate,
  createExposeCreateSuccess,
  loadDetailsExposeCreate,
  loadDetailsExposeCreateSuccess,
  loadDetailsExposeCreateFail,
  changeStateExposeCreate,
  updateExposeCreate,
  updateExposeCreateSuccess,
  deleteExposeCreate,
  deleteExposeCreateSuccess,
  deleteExposeCreateFail,
  //ICIntegration
  loadDataICIntegration,
  loadUsersMappingIC,
  loadUsersMappingICSuccess,
  createUserMappingIC,
  createUserMappingICSuccess,
  deleteUserMappingIC,
  deleteUserMappingICResult,
  changeStateICIntegration,
  loadDetailsUserMappingIC,
  loadDetailsUserMappingICSuccess,
  loadDetailsUserMappingICFail,
  updateUserMappingIC,
  updateUserMappingICSuccess,
  loadFieldsConfig,
  loadFieldsConfigSuccess,
  getToken,
  getTokenSuccess,
  loadObjectFieldsConfig,
  loadObjectFieldsConfigSuccess,
  loadConfigICSuccess,
  createConfigIC,
  updateConfigIC,
  loadMappingExpose,
  loadMappingExposeSuccess,
  createMappingExpose,
  updateMappingExpose,
  resetAllIC,
  //
  loadLayouts,
  loadLayoutsFail,
  loadLayoutsSuccess,
  updateLayout,
  updateLayoutFail,
  updateLayoutSuccess,
  loadDetailsComponent,
  loadDetailsComponentFail,
  loadDetailsComponentSuccess,
  //
  updateTab,
  getLogs,
  getLogsFail,
  getLogsSuccess,
  //
  getAllSla,
  getAllSlaSuccess,
  getAllSlaFail,
  // updateTabFail,
  // updateTabSuccess,
  //VoiceBotSetting
  getDetailsVoiceBot,
  getDetailsVoiceBotSuccess,
  getDetailsVoiceBotFail,
  createVoiceBotSetting,
  updateVoiceBotSetting,
  //Timeline
  getInteraction,
  getInteractionSuccess,
  getInteractionFail,
  getInteractionVertical,
  getInteractionVerticalSuccess,
  getInteractionVerticalFail,
  loadObjectsTarget,
  loadObjectsTargetSuccess,
  updateInteractionDetails,
  updateInteractionDetailsSuccess,
  loadRecordInteraction,
  loadRecordInteractionResult,
  //CTISetting
  loadDataCTISetting,
  loadConfigCTISuccess,
  loadUsersMappingCTI,
  loadUsersMappingCTISuccess,
  createUserMappingCTI,
  createUserMappingCTISuccess,
  deleteUserMappingCTI,
  deleteUserMappingCTIResult,
  changeStateCTISetting,
  loadDetailsUserMappingCTI,
  loadDetailsUserMappingCTISuccess,
  loadDetailsUserMappingCTIFail,
  updateUserMappingCTI,
  updateUserMappingCTISuccess,
  updateConfigCTI,
  loadFieldsObjectToCall,
  loadFieldsObjectToCallResult,
  resetAllCTI,
  generateTokenC247,
  generateTokenC247Result,
  //
  updateRecord,
  updateRecordSuccess,
  updateRecordFail,
  //Callcenter
  loadCallCenterSetting,
  loadConfigCallCenterSuccess,
  loadUsersMappingCallCenter,
  loadUsersMappingCallCenterSuccess,
  createUserMappingCallCenter,
  createUserMappingCallCenterSuccess,
  deleteUserMappingCallCenter,
  deleteUserMappingCallCenterResult,
  changeStateCallCenterSetting,
  updateUserMappingCallCenter,
  updateUserMappingCallCenterSuccess,
  updateConfigCallCenter,
  testConnectionCalabrio,
  updateCallScoring,
  resetAllConfigCallCenter,
  //Voice Biometric
  getConfigVoiceBiometric,
  getConfigVoiceBiometricResult,
  updateConfigVoiceBiometric,
  deleteConfigVoiceBiometric,
  createInteractionConfig,
  createInteractionConfigFail,
  createInteractionConfigSuccess,
  updateInteractionConfig,
  updateInteractionConfigResult,
  interactionConfig,
  interactionConfigSuccess,
  interactionConfigFail,
  //
  deleteInteractionConfig,
  loadPageTitle,
  loadPageTitleSuccess,
  loadConfigComponents,
  loadConfigComponentsSuccess,
} from "redux/slices/consolidatedViewSettings";

export default function* consolidatedViewSettings() {
  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  //Expose API
  yield takeLatest(loadAllExpose.type, LoadAllExpose);
  yield takeLatest(updateExpose.type, UpdateExpose);
  //PopupSetting
  yield takeLatest(loadAllPopupSetting.type, LoadAllPopup);
  yield takeLatest(updatePopup.type, UpdatePopup);
  yield takeLatest(loadPopup.type, LoadPopup);
  yield takeLatest(createRecordPopup.type, CreateRecordPopup);
  yield takeLatest(updateRecordPopup.type, UpdateRecordPopup);
  yield takeLatest(popupCheckWorkflow.type, PopupCheckWorkflow);
  yield takeLatest(updateRecordManyParam.type, UpdateRecordManyParam);
  //CoreSetting
  yield takeLatest(loadDataCoreSetting.type, LoadDataCoreSetting);
  yield takeLatest(loadCoreSetting.type, LoadCoreSetting);
  yield takeLatest(loadFieldsCreate.type, LoadFieldsCreate);
  yield takeLatest(createCoreSetting.type, CreateCoreSetting);
  yield takeLatest(updateCoreSetting.type, UpdateCoreSetting);
  yield takeLatest(deleteCoreSetting.type, DeleteCoreSetting);
  yield takeLatest(loadDetailsCoreSetting.type, LoadDetailsCoreSetting);
  //ExposeAPICreate
  yield takeLatest(loadDataExposeCreate.type, LoadDataExposeCreate);
  yield takeLatest(loadExposeCreate.type, LoadExposeCreate);
  yield takeLatest(createExposeCreate.type, CreateExposeCreate);
  yield takeLatest(loadDetailsExposeCreate.type, LoadDetailsExposeCreate);
  yield takeLatest(updateExposeCreate.type, UpdateExposeCreate);
  yield takeLatest(deleteExposeCreate.type, DeleteExposeCreate);
  //ICIntegration
  yield takeLatest(loadDataICIntegration.type, LoadDataICIntegration);
  yield takeLatest(loadUsersMappingIC.type, LoadUsersMappingIC);
  yield takeLatest(createUserMappingIC.type, CreateUserMappingIC);
  yield takeLatest(deleteUserMappingIC.type, DeleteUserMappingIC);
  yield takeLatest(loadDetailsUserMappingIC.type, LoadDetailsUserMappingIC);
  yield takeLatest(updateUserMappingIC.type, UpdateUserMappingIC);
  yield takeLatest(loadFieldsConfig.type, LoadFieldsConfig);
  yield takeLatest(getToken.type, GetToken);
  yield takeLatest(loadObjectFieldsConfig.type, LoadObjectFieldsConfig);
  yield takeLatest(createConfigIC.type, CreateConfigIC);
  yield takeLatest(updateConfigIC.type, UpdateConfigIC);
  yield takeLatest(loadMappingExpose.type, LoadMappingExpose);
  yield takeLatest(createMappingExpose.type, CreateMappingExpose);
  yield takeLatest(updateMappingExpose.type, UpdateMappingExpose);
  yield takeLatest(resetAllIC.type, ResetAllIC);

  //
  yield takeLatest(createComponent.type, CreateComponent);
  yield takeLatest(loadComponents.type, LoadComponents);
  yield takeLatest(deleteComponent.type, DeleteComponent);
  yield takeLatest(updateComponent.type, UpdateComponent);
  yield takeLatest(updateLayout.type, UpdateLayout);
  yield takeLatest(loadLayouts.type, LoadLayout);
  yield takeLatest(loadDetailsComponent.type, LoadDetailsComponent);
  yield takeLatest(updateTab.type, UpdateComponentNoNoti);
  yield takeLatest(getLogs.type, GetLogs);
  yield takeLatest(getAllSla.type, GetAllSLA);
  //VoiceBotSetting
  yield takeLatest(getDetailsVoiceBot.type, GetDetailsVoiceBot);
  yield takeLatest(createVoiceBotSetting.type, CreateVoiceBotSetting);
  yield takeLatest(updateVoiceBotSetting.type, UpdateVoiceBotSetting);
  //Timeline
  yield takeLatest(getInteraction.type, GetInteraction);
  yield takeLatest(getInteractionVertical.type, GetInteractionVertical);
  yield takeLatest(loadObjectsTarget.type, LoadObjectsTarget);
  yield takeLatest(updateInteractionDetails.type, UpdateInteractionDetails);
  yield takeLatest(loadRecordInteraction.type, LoadRecordInteraction);
  //CTISetting
  yield takeLatest(loadDataCTISetting.type, LoadDataCTISetting);
  yield takeLatest(loadUsersMappingCTI.type, LoadUsersMappingCTI);
  yield takeLatest(createUserMappingCTI.type, CreateUserMappingCTI);
  yield takeLatest(deleteUserMappingCTI.type, DeleteUserMappingCTI);
  yield takeLatest(loadDetailsUserMappingCTI.type, LoadDetailsUserMappingCTI);
  yield takeLatest(updateUserMappingCTI.type, UpdateUserMappingCTI);
  yield takeLatest(updateConfigCTI.type, UpdateConfigCTI);
  yield takeEvery(loadFieldsObjectToCall.type, LoadFieldsObjectToCall);
  yield takeLatest(resetAllCTI.type, ResetAllCTI);
  yield takeLatest(generateTokenC247.type, GenerateTokenC247);
  //
  yield takeLatest(updateRecord.type, UpdateRecord);
  //Callcenter
  yield takeLatest(loadCallCenterSetting.type, LoadCallCenterSetting);
  yield takeLatest(loadUsersMappingCallCenter.type, LoadUsersMappingCallCenter);
  yield takeLatest(
    createUserMappingCallCenter.type,
    CreateUserMappingCallCenter
  );
  yield takeLatest(
    deleteUserMappingCallCenter.type,
    DeleteUserMappingCallCenter
  );
  yield takeLatest(
    updateUserMappingCallCenter.type,
    UpdateUserMappingCallCenter
  );
  yield takeLatest(updateConfigCallCenter.type, UpdateConfigCallCenter);
  yield takeLatest(testConnectionCalabrio.type, TestConnectionCalabrio);
  yield takeLatest(updateCallScoring.type, UpdateCallScoring);
  yield takeLatest(resetAllConfigCallCenter.type, ResetAllConfigCallCenter);

  //Voice Biometric
  yield takeLatest(getConfigVoiceBiometric.type, GetConfigVoiceBiometric);
  yield takeLatest(updateConfigVoiceBiometric.type, UpdateVoiceBiometric);
  yield takeLatest(deleteConfigVoiceBiometric.type, DeleteConfigVoiceBiometric);
  yield takeLatest(createInteractionConfig.type, CreateInteractionConfig);
  yield takeLatest(updateInteractionConfig.type, UpdateInteractionConfig);
  yield takeLatest(interactionConfig.type, LoadDetailsInteractionConfig);
  yield takeLatest(deleteInteractionConfig.type, DeleteConfigInteraction);
  yield takeLatest(loadPageTitle.type, LoadPageTitleSaga);

  //v3
  yield takeLatest(loadConfigComponents.type, LoadConfigComponents);
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    // yield put(loadObject());
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

//Expose API
function* LoadAllExpose(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObjectFields(action.payload));
    const response = yield loadAllExposeAPI(action.payload);
    if (response.data.data.fields !== undefined) {
      yield put(loadAllExposeSuccess(response.data.data.fields));
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateExpose(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateExposeAPI(action.payload);
    const response = yield loadAllExposeAPI({
      object_id: action.payload.object_id,
    });
    if (response.data.data.fields !== undefined) {
      yield put(loadAllExposeSuccess(response.data.data.fields));
    }

    Notification("success", "Update successfully!");
    yield put(updateExposeSuccess());
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

//PopupSetting

function* LoadAllPopup(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObjectFields(action.payload));
    const response = yield loadAllPopupSettingAPI(action.payload);
    yield put(loadAllPopupSettingSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdatePopup(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updatePopupSettingAPI(action.payload);
    Notification("success", "Update successfully!");
    yield put(updatePopupSuccess());
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadPopup(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const pagination = yield loadPopupAPI(action.payload.pagination);
    yield put(
      loadPopupSuccess({
        type: "pagination",
        data: pagination.data.data,
      })
    );

    if (pagination.data.data.total_record !== 0) {
      const data = yield loadPopupAPI(action.payload.data);
      yield put(
        loadPopupSuccess({
          type: "data",
          data: data.data.data,
        })
      );
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    window.open(`${FE_URL}/404`, "_self");
    yield put(setShowLoadingScreen(false));
  }
}

function* PopupCheckWorkflow(action) {
  try {
    yield popupCheckWorkflowAPI(action.payload);
  } catch (error) {}
}

function* CreateRecordPopup(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createRecordAPI(action.payload.data);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    yield put(
      createRecordPopupSuccess({
        status: "Success",
        record_id: response.data.data._id,
        object_id: action.payload.data.object_id,
      })
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));

    if (error.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(error.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
      yield put(setShowModal(false));
    } else {
      Notification("error", error.response.data.error);
      yield put(createRecordFail());
    }
  }
}

function* UpdateRecordPopup(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateRecordListViewAPI(action.payload.data);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield delay(500);
    yield put(
      updateRecordPopupResult({
        status: "Success",
        record_id: action.payload.data.id,
        object_id: action.payload.data.object_id,
      })
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateRecordManyParam(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadRecordDataAPI(action.payload.payload);

    const owner = _.get(response, "data.data.owner");
    delete _.get(response, "data.data").created_by;
    delete _.get(response, "data.data").created_date;
    delete _.get(response, "data.data").modify_by;
    delete _.get(response, "data.data").modify_time;
    delete _.get(response, "data.data").owner;
    delete _.get(response, "data.data")._id;
    const data = [];
    Object.entries(_.get(response, "data.data")).forEach(([key, val]) => {
      data.push({
        ...val,
        id_field: key,
      });
    });

    _.map(action.payload.pathname?.split("&&&"), (field, idx) => {
      if (idx > 0) {
        const indexData = data.findIndex(
          (item) => item.id_field === field.split("===")[0]
        );
        if (indexData > -1) {
          data[indexData] = {
            ...data[indexData],
            value: decodeURI(field.split("===")[1]),
          };
        }
      }
    });

    yield updateRecordListViewAPI({
      id: action.payload.payload.id,
      object_id: action.payload.payload.object_id,
      owner_id: owner,
      data: data,
    });

    yield put(updateRecordManyParamResult(action.payload.payload.id));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
//CoreSetting

function* LoadDataCoreSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObjectFields(action.payload));
    yield put(loadFields(action.payload));
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    const response = yield loadCoreSettingAPI(action.payload);
    yield put(loadCoreSettingSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadCoreSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadCoreSettingAPI(action.payload);
    yield put(loadCoreSettingSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadFieldsCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadObjectFieldsAPI(action.payload);
    yield put(loadFieldsCreateSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateCoreSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createCoreSettingAPI(action.payload);
    yield put(createCoreSettingSuccess());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadComponents(action) {
  try {
    const response = yield loadComponentsAPI(action.payload);
    yield put(loadComponentsSuccess(response.data.data));
  } catch (error) {
    yield put(loadComponentsFail());
  }
}

function* DeleteComponent(action) {
  try {
    yield deleteComponentsAPI(action.payload);
    yield put(deleteComponentSuccess());

    const response = yield loadComponentsAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadComponentsSuccess(response.data.data));

    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(deleteComponentFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateComponent(action) {
  try {
    yield updateComponentAPI(action.payload);
    yield put(updateComponentSuccess());

    const response = yield loadComponentsAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadComponentsSuccess(response.data.data));

    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(updateComponentFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateComponentNoNoti(action) {
  try {
    yield updateComponentAPI(action.payload);
    yield put(updateComponentSuccess());

    const response = yield loadComponentsAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadComponentsSuccess(response.data.data));

    // Notification("success", "Update successfully!");
  } catch (error) {
    yield put(updateComponentFail());
    // Notification("error", error.response.data.error);
  }
}

function* UpdateCoreSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateCoreSettingAPI(action.payload);
    yield put(updateCoreSettingSuccess());
    yield put(
      changeStateCoreSetting({
        api_id: action.payload.api_id,
        type: "update",
      })
    );
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteCoreSetting(action) {
  try {
    yield deleteCoreSettingAPI(action.payload);
    yield put(
      changeStateCoreSetting({
        api_id: action.payload.api_id,
        type: "delete",
      })
    );
    yield put(deleteCoreSettingSuccess());
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(deleteCoreSettingFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsCoreSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailsCoreSettingAPI(action.payload);
    yield put(loadDetailsCoreSettingSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadDetailsCoreSettingFail());
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

//ExposeAPICreate

function* LoadDataExposeCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadObjectFields({
        object_id: action.payload.object_id,
      })
    );
    yield put(
      loadFields({
        api_version: "2",
        object_id: action.payload.object_id,
      })
    );
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    const response = yield loadExposeCreateAPI(action.payload);
    yield put(loadExposeCreateSuccess(response.data.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadExposeCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadExposeCreateAPI(action.payload);
    yield put(loadExposeCreateSuccess(response.data.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateExposeCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createExposeCreateAPI(action.payload);
    yield put(createExposeCreateSuccess());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateComponent(action) {
  try {
    yield createComponentAPI(action.payload);
    yield put(createComponentSuccesss());

    const response = yield loadComponentsAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadComponentsSuccess(response.data.data));

    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(createComponentFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateExposeCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateExposeCreateAPI(action.payload);
    yield put(updateExposeCreateSuccess());
    yield put(
      changeStateExposeCreate({
        _id: action.payload._id,
        type: "update",
      })
    );
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteExposeCreate(action) {
  try {
    yield deleteExposeCreateAPI(action.payload);
    yield put(
      changeStateExposeCreate({
        _id: action.payload._id,
        type: "delete",
      })
    );
    yield put(deleteExposeCreateSuccess());
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(deleteExposeCreateFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsExposeCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailsExposeCreateAPI(action.payload);
    yield put(loadDetailsExposeCreateSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadDetailsExposeCreateFail());
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

//ICIntegration
function* LoadDataICIntegration(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    const res = yield getConfigICAPI(action.payload);
    yield put(loadConfigICSuccess(res.data.data));
    yield put(loadObjectFieldsConfig(action.payload));
    const response = yield loadUsersMappingICAPI(action.payload);
    yield put(loadUsersMappingICSuccess(response.data.data));
    const mapping = yield loadMappingExposeAPI({
      object_id: action.payload.object_id,
      type: "IC",
    });
    if (mapping.data.data !== "No record found.") {
      yield put(loadMappingExposeSuccess(mapping.data.data));
    } else {
      yield put(loadMappingExposeSuccess(null));
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadUsersMappingIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadUsersMappingICAPI(action.payload);
    yield put(loadUsersMappingICSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateUserMappingIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createUserMappingICAPI(action.payload);
    yield put(createUserMappingICSuccess());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadLayout(action) {
  try {
    const response = yield loadLayoutAPI(action.payload);
    yield put(loadLayoutsSuccess(response.data.data));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteUserMappingIC(action) {
  try {
    yield deleteUserMappingICAPI(action.payload);
    yield put(
      changeStateICIntegration({
        id: action.payload.id,
        type: "delete",
      })
    );
    yield put(deleteUserMappingICResult());
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(deleteUserMappingICResult());
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsUserMappingIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailsUserMappingICAPI(action.payload);
    yield put(loadDetailsUserMappingICSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadDetailsUserMappingICFail());
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateUserMappingIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateUserMappingICAPI(action.payload);
    yield put(updateUserMappingICSuccess());
    yield put(
      changeStateICIntegration({
        id: action.payload.id,
        type: "update",
      })
    );
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadObjectFieldsAPI(action.payload);
    yield put(loadFieldsConfigSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* GetToken(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getTokenAPI(action.payload);
    yield put(getTokenSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadObjectFieldsConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadObjectFieldsAPI(action.payload);
    yield put(loadObjectFieldsConfigSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateConfigIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createConfigICAPI(action.payload);
    const res = yield getConfigICAPI({
      object_id: action.payload.object_id,
    });
    if (Object.keys(res.data.data).length > 0) {
      yield put(loadConfigICSuccess(res.data.data));
    }
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateConfigIC(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateConfigICAPI(action.payload);
    const res = yield getConfigICAPI({ object_id: action.payload.object_id });
    if (Object.keys(res.data.data).length > 0) {
      yield put(loadConfigICSuccess(res.data.data));
    }
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadMappingExpose(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const mapping = yield loadMappingExposeAPI({
      object_id: action.payload.object_id,
      type: "IC",
    });
    if (mapping.data.data !== "No record found.") {
      yield put(loadMappingExposeSuccess(mapping.data.data));
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateMappingExpose(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createMappingExposeAPI(action.payload);
    const mapping = yield loadMappingExposeAPI({
      object_id: action.payload.object_id,
      type: "IC",
    });
    if (mapping.data.data !== "No record found.") {
      yield put(loadMappingExposeSuccess(mapping.data.data));
    }
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateMappingExpose(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateMappingExposeAPI(action.payload);
    const mapping = yield loadMappingExposeAPI({
      object_id: action.payload.object_id,
      type: "IC",
    });
    if (mapping.data.data !== "No record found.") {
      yield put(loadMappingExposeSuccess(mapping.data.data));
    }
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* ResetAllIC() {
  try {
    yield put(setShowLoadingScreen(true));
    yield resetAllICAPI();
    yield put(loadConfigICSuccess(null));
    yield put(loadUsersMappingICSuccess(null));
    yield put(loadMappingExposeSuccess(null));
    Notification("success", "Reset successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateLayout(action) {
  try {
    yield updateLayoutAPI(action.payload);
    Notification("success", "Update successfully!", 2);
    yield put(updateLayoutSuccess());
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(updateLayoutFail());
    yield put(loadLayoutsFail());
  }
}

function* LoadDetailsComponent(action) {
  try {
    const response = yield loadDetailsAPI(action.payload);
    yield put(loadDetailsComponentSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsComponentFail());
  }
}

function* GetLogs(action) {
  try {
    const response = yield getLogsAPI(action.payload);
    yield put(getLogsSuccess(response.data.data));
  } catch (error) {
    yield put(getLogsFail());
  }
}

function* GetAllSLA(action) {
  try {
    const response = yield getAllSlaAPI(action.payload);
    yield put(getAllSlaSuccess(response.data.data));
  } catch (error) {
    yield put(getAllSlaFail());
  }
}
//     yield updateComponentAPI(action.payload.updateComponent);
//     yield put(updateComponentSuccess());

//     const responseData = yield loadComponentsAPI({
//       object_id: action.payload.object_id,
//     });

//     yield put(loadComponentsSuccess(responseData.data.data));
//     yield put(updateTabSuccess());
//     Notification("success", "Update successfully!");
//   } catch (error) {
//     Notification("error", error.response.data.error);
//     yield put(updateLayoutFail());
//     yield put(updateComponentFail());
//     yield put(updateTabFail());
//   }
// }

//VoiceBotSetting
function* GetDetailsVoiceBot(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getDetailsVoiceBotAPI(action.payload);
    yield put(getDetailsVoiceBotSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(getDetailsVoiceBotFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateVoiceBotSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createVoiceBotSettingAPI(action.payload);
    yield getDetailsVoiceBot({
      object_id: action.payload.object_id,
    });
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateVoiceBotSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateVoiceBotSettingAPI(action.payload);
    yield getDetailsVoiceBot({
      object_id: action.payload.object_id,
    });
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

//Timeline
function* GetInteraction(action) {
  try {
    const response = yield getInteractionsAPI(action.payload);
    yield put(getInteractionSuccess(response.data.data));
  } catch (error) {
    yield put(getInteractionFail());
  }
}

function* GetInteractionVertical(action) {
  try {
    const response = yield getInteractionsAPI(action.payload);
    yield put(getInteractionVerticalSuccess(response.data.data));
  } catch (error) {
    yield put(getInteractionVerticalFail());
  }
}

function* LoadObjectsTarget(action) {
  try {
    const response = yield loadObjectsTargetAPI(action.payload);
    yield put(loadObjectsTargetSuccess(response.data.data));
  } catch (error) {
    yield put(loadObjectsTargetSuccess({}));
  }
}

function* LoadRecordInteraction(action) {
  try {
    const response = yield loadRecordInteractionAPI(action.payload);
    yield put(loadRecordInteractionResult(response.data.data));
  } catch (error) {
    yield put(loadRecordInteractionResult([]));
  }
}

function* UpdateInteractionDetails(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateInteractionDetailsAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield put(updateInteractionDetailsSuccess("success"));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

//CTI Setting
function* LoadDataCTISetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    const res = yield getCTIConfigAPI(action.payload);
    yield put(loadConfigCTISuccess(res.data.data));

    yield put(loadObjectFieldsConfig(action.payload));
    const response = yield loadUsersMappingCTIAPI(action.payload);
    yield put(loadUsersMappingCTISuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadUsersMappingCTI(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadUsersMappingCTIAPI(action.payload);
    yield put(loadUsersMappingCTISuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateUserMappingCTI(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createUserMappingCTIAPI(action.payload);
    yield put(createUserMappingCTISuccess());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteUserMappingCTI(action) {
  try {
    yield deleteUserMappingCTIAPI(action.payload);
    yield put(
      changeStateCTISetting({
        id: action.payload._id,
        type: "delete",
      })
    );
    yield put(deleteUserMappingCTIResult());
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(deleteUserMappingCTIResult());
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsUserMappingCTI(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailsUserMappingCTIAPI(action.payload);
    yield put(loadDetailsUserMappingCTISuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadDetailsUserMappingCTIFail());
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateUserMappingCTI(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateUserMappingCTIAPI(action.payload);
    yield put(updateUserMappingCTISuccess());
    yield put(
      changeStateCTISetting({
        id: action.payload.user_id,
        type: "update",
      })
    );
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateConfigCTI(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateConfigCTIAPI(action.payload);
    const res = yield getCTIConfigAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadConfigCTISuccess(res.data.data));
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadFieldsObjectToCall(action) {
  try {
    const response = yield loadFieldsAPI(action.payload);

    let tempOptionsFields = [];
    response.data.data?.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                });
              }
            });
          });
        }
      }
    });

    yield put(
      loadFieldsObjectToCallResult({
        object_id: action.payload.object_id,
        fields: tempOptionsFields,
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ResetAllCTI() {
  try {
    yield put(setShowLoadingScreen(true));
    yield resetAllCTIAPI();
    yield put(loadUsersMappingCTISuccess([]));
    yield put(loadConfigCTISuccess([]));
    Notification("success", "Reset successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* GenerateTokenC247() {
  try {
    yield put(setShowLoadingScreen(true));
    const res = yield generateTokenC247API();
    yield put(generateTokenC247Result(res.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

//

function* UpdateRecord(action) {
  try {
    yield updateRecordAPI(action.payload);
    yield put(updateRecordSuccess());
    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(updateRecordFail());
  }
}

//Callcenter

function* LoadCallCenterSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    const res = yield getCallCenterConfigAPI();
    yield put(loadConfigCallCenterSuccess(res.data.data));

    yield put(loadObjectFieldsConfig(action.payload));

    const response = yield loadUsersMappingCallCenterAPI();
    yield put(loadUsersMappingCallCenterSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadUsersMappingCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadUsersMappingCallCenterAPI();
    yield put(loadUsersMappingCallCenterSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateUserMappingCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createUserMappingCallCenterAPI(action.payload);
    yield put(createUserMappingCallCenterSuccess());
    Notification("success", "Create successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteUserMappingCallCenter(action) {
  try {
    yield deleteUserMappingCallCenterAPI(action.payload);
    yield put(
      changeStateCallCenterSetting({
        id: action.payload._id,
        type: "delete",
      })
    );
    yield put(deleteUserMappingCallCenterResult());
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(deleteUserMappingCallCenterResult());
    Notification("error", error.response.data.error);
  }
}

function* UpdateUserMappingCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateUserMappingCallCenterAPI(action.payload);
    yield put(updateUserMappingCallCenterSuccess());
    yield put(
      changeStateCTISetting({
        id: action.payload.user_id,
        type: "update",
      })
    );
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateConfigCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateConfigCallCenterAPI(action.payload);
    const res = yield getCallCenterConfigAPI(action.payload);
    yield put(loadConfigCallCenterSuccess(res.data.data));
    Notification("success", "Update successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* TestConnectionCalabrio(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield testConnectionCalabrioAPI(action.payload);
    Notification("success", "Successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateCallScoring(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateCallScoringAPI(action.payload);
    Notification("success", "Update call scoring successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* ResetAllConfigCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield resetAllConfigCallCenterAPI(action.payload);

    yield put(loadConfigCallCenterSuccess({}));
    yield put(loadUsersMappingCallCenterSuccess([]));

    Notification("success", "Reset all config successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

//Voice Biometric
function* GetConfigVoiceBiometric(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getConfigVoiceBiometricAPI(action.payload);
    yield put(getConfigVoiceBiometricResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(getConfigVoiceBiometricResult({}));
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateVoiceBiometric(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateConfigVoiceBiometricAPI(action.payload);
    yield put(
      getConfigVoiceBiometric({
        object_id: action.payload.object_id,
      })
    );
    Notification("success", "Update config successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* DeleteConfigVoiceBiometric(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteConfigVoiceBiometricAPI(action.payload);
    yield put(
      getConfigVoiceBiometric({
        object_id: action.payload.object_id,
      })
    );
    Notification("success", "Delete config successfully!");
    yield put(setShowLoadingScreen(false));
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateInteractionConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createInteractionConfigAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(createInteractionConfigSuccess());

    const response = yield loadDetailInteractionConfigAPI(action.payload);
    yield put(interactionConfigSuccess(response.data.data));
    Notification("success", "Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
    yield put(createInteractionConfigFail());
  }
}

function* UpdateInteractionConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateInteractionConfigAPI(action.payload);
    yield put(setShowLoadingScreen(false));

    const response = yield loadDetailInteractionConfigAPI(action.payload);
    yield put(interactionConfigSuccess(response.data.data));
    Notification("success", "Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
    yield put(updateInteractionConfigResult());
  }
}

function* LoadDetailsInteractionConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailInteractionConfigAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(interactionConfigSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
    yield put(interactionConfigFail());
  }
}

function* DeleteConfigInteraction(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteInteractionConfigAPI(action.payload);
    yield put(setShowLoadingScreen(false));

    const response = yield loadDetailInteractionConfigAPI(action.payload);
    yield put(interactionConfigSuccess(response.data.data));
    Notification("success", "Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadPageTitleSaga(action) {
  try {
    const { data } = yield loadPageTitleAPI(action.payload);
    yield put(loadPageTitleSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadConfigComponents(action) {
  try {
    const { data } = yield loadConfigComponentsAPI(action.payload);

    if (action.payload.is_update) {
      yield put(loadConfigComponentsSuccess(action.payload.data));
    } else if (data.data.data) {
      yield put(loadConfigComponentsSuccess(data.data.data));
    } else {
      let temData = {
        layouts_center: {
          center: [],
          left: [],
          right: [],
          top: [],
          tabs: [],
        },
        layouts_left: {
          center: [],
          left: [],
          right: [],
          top: [],
          tabs: [],
        },
        layouts_right: {
          center: [],
          left: [],
          right: [],
          top: [],
          tabs: [],
        },
      };
      yield put(loadConfigComponentsSuccess(temData));
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
