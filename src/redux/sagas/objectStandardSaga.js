import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";
import _ from "lodash";

import {
  objectStandardCreateAPI,
  objectStandardDeleteAPI,
  objectStandardGetListAPI,
  objectStandardGetFieldsAPI,
  objectStandardGetObjectsAPI,
  objectStandardSyncDataAPI,
  objectStandardUpdateAPI,
} from "redux/api/objectStandardAPI";
import {
  objectStandardCreate,
  objectStandardDelete,
  objectStandardUpdate,
  objectStandardSyncData,
  objectStandardGetObjects,
  objectStandardGetObjectsResult,
  objectStandardGetFields,
  objectStandardGetFieldsResult,
  objectStandardGetList,
  objectStandardGetListResult,
} from "redux/slices/objectStandard";

export default function* campaign() {
  yield takeLatest(objectStandardCreate.type, ObjectStandardCreate);
  yield takeLatest(objectStandardUpdate.type, ObjectStandardUpdate);
  yield takeLatest(objectStandardDelete.type, ObjectStandardDelete);
  yield takeLatest(objectStandardSyncData.type, ObjectStandardSyncData);
  yield takeLatest(objectStandardGetObjects.type, ObjectStandardGetObjects);
  yield takeLatest(objectStandardGetFields.type, ObjectStandardGetFields);
  yield takeLatest(objectStandardGetList.type, ObjectStandardGetList);
}

function* ObjectStandardCreate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield objectStandardCreateAPI(action.payload);
    yield put(objectStandardGetList());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardUpdate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield objectStandardUpdateAPI(action.payload);
    yield put(objectStandardGetList());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardDelete(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield objectStandardDeleteAPI(action.payload);
    yield put(objectStandardGetList());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardSyncData(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield objectStandardSyncDataAPI(action.payload);
    yield put(objectStandardGetList());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Sync data successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardGetObjects(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield objectStandardGetObjectsAPI(action.payload);
    yield put(objectStandardGetObjectsResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardGetFields(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield objectStandardGetFieldsAPI(action.payload);
    yield put(
      objectStandardGetFieldsResult(_.get(response, "data.data[0]", {}))
    );
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* ObjectStandardGetList(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield objectStandardGetListAPI(action.payload);
    yield put(objectStandardGetListResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
