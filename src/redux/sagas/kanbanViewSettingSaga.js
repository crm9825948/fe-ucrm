import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen, setShowModalConfirmDelete } from "redux/slices/global";
import { getListKanbanView, getListKanbanViewSuccess, saveKanbanViewSetting,
    deleteKanbanViewSetting, getKanbanViewSettingById,
    setShowModal, updateKanbanViewSetting,
    getKanViewSettingByIdSuccess, 
    setStep} from "redux/slices/kanbanViewSetting";
import { loadListObjectFieldSuccess } from 'redux/slices/objects';
import { getListKanbanViewAPI, saveKanbanViewSettingAPI, deleteKanbanViewSettingAPI, getKanbanViewSettingByIdAPI,
    updateKanbanViewSettingAPI } from '../api/kanbanViewSettingAPI'
import { loadFieldsAPI,
} from "redux/api/objectsAPIs";
export default function* kanbanViewSettingSaga() {
    yield takeLatest(getListKanbanView.type, getListKanbanViewSaga)
    yield takeLatest(saveKanbanViewSetting.type, saveKanbanViewSettingSaga)
    yield takeLatest(deleteKanbanViewSetting.type, deleteKanbanViewSettingSaga)
    yield takeLatest(getKanbanViewSettingById.type, getKanbanViewSettingByIdSaga)
    yield takeLatest(updateKanbanViewSetting.type, updateKanbanViewSaga)
}

function* getListKanbanViewSaga() {
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield getListKanbanViewAPI()
        yield put(getListKanbanViewSuccess(data.data))
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        
    }
}

function* saveKanbanViewSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        let payload = {...action.payload}
        if(!payload.show_uncategorized) {
            delete payload.uncategorized_column_config
        }
        yield saveKanbanViewSettingAPI(payload)
        yield put(getListKanbanView())
        Notification('success', "Tạo thành công")
        yield put(setShowModal(false))
        yield put(setStep(0))
        yield put(setShowLoadingScreen(false))
    } catch ({response}) {
        Notification('error', response.data.error)
        yield put(setShowLoadingScreen(false))
    }
}

function* deleteKanbanViewSettingSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        yield deleteKanbanViewSettingAPI(action.payload)
        yield put(setShowModalConfirmDelete(false))
        yield put(setShowLoadingScreen(false))
        yield put(getListKanbanView())
        Notification('success', "Xoá thành công")
    } catch (error) {
        Notification('error', error.response.data.mess)
    }
}

function* getKanbanViewSettingByIdSaga(action){
    try {
        yield put(setShowLoadingScreen(true))
        const { data } = yield getKanbanViewSettingByIdAPI(action.payload)
        yield put(getKanViewSettingByIdSuccess(data.data))
        const field = yield loadFieldsAPI({
                api_version: "2",
                object_id: data.data.object_id
        });
        yield put(loadListObjectFieldSuccess(field.data.data));
        yield put(setShowModal(true))
        yield put(setShowLoadingScreen(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
    }
}

function* updateKanbanViewSaga(action) {
    try {
        yield put(setShowLoadingScreen(true))
        let payload = {...action.payload}
        if(!payload.show_uncategorized) {
            delete payload.uncategorized_column_config
        }
        yield updateKanbanViewSettingAPI(payload)
        yield put(getListKanbanView())
        Notification('success', "Cập nhật thành công")
        yield put(setStep(0))
        yield put(setShowModal(false))
    } catch (error) {
        yield put(setShowLoadingScreen(false))
    }
}