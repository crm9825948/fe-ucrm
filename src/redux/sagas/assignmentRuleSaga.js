import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";

import {
  createAssignmentRuleAPI,
  deleteAssignmentRuleAPI,
  getAssignmentRuleDetailAPI,
  getListAssignmentRuleAPI,
  resetUsersCapacityAPI,
  runManualAssignAPI,
  updateAssignmentRuleAPI,
  updatePriorityRuleAPI,
} from "redux/api/assignmentRuleAPI";
import {
  getListAssignmentRuleSuccess,
  getAssignmentRuleDetailFail,
  getAssignmentRuleDetailSuccess,
  getListAssignmentRule,
  createAssignmentRule,
  deleteAssignmentRule,
  getAssignmentRuleDetail,
  updateAssignmentRule,
  runManualAssign,
  resetUserCapacity,
  updatePriorityRule,
} from "redux/slices/assignmentRule";

export default function* AssignmentRuleSaga() {
  yield takeLatest(getListAssignmentRule.type, GetListAssignmentRule);
  yield takeLatest(createAssignmentRule.type, CreateAssignmentRule);
  yield takeLatest(deleteAssignmentRule.type, DeleteAssignmentRule);
  yield takeLatest(getAssignmentRuleDetail.type, GetAssignmentRuleDetail);
  yield takeLatest(updateAssignmentRule.type, UpdateAssignmentRule);
  yield takeLatest(runManualAssign.type, RunManualAssign);
  yield takeLatest(resetUserCapacity.type, ResetUsersCapacity);
  yield takeLatest(updatePriorityRule.type, UpdatePriorityRule);
}

function* GetListAssignmentRule(action) {
  try {
    const { data } = yield getListAssignmentRuleAPI(action.payload);
    yield put(getListAssignmentRuleSuccess(data.data));
  } catch (error) {}
}

function* CreateAssignmentRule(action) {
  try {
    yield createAssignmentRuleAPI(action.payload);
    Notification("success", "Create assignment rule success");
    yield put(getListAssignmentRule(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DeleteAssignmentRule(action) {
  try {
    yield deleteAssignmentRuleAPI(action.payload);
    Notification("success", "Delete assignment rule success");
    yield put(getListAssignmentRule(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetAssignmentRuleDetail(action) {
  try {
    const response = yield getAssignmentRuleDetailAPI(action.payload);
    if (response.data.data?.shift_work) {
      response.data.data.shift_work.forEach((item, idx) => {
        let tempDays = [];
        item.applicable_weekdays.forEach((day, index) => {
          if (day) {
            tempDays.push(index);
          }
        });

        response.data.data.shift_work[idx] = {
          time: [
            String(response.data.data.shift_work[idx].from_time.hour).padStart(
              2,
              "0"
            ) +
              ":" +
              String(
                response.data.data.shift_work[idx].from_time.minute
              ).padStart(2, "0"),
            String(response.data.data.shift_work[idx].to_time.hour).padStart(
              2,
              "0"
            ) +
              ":" +
              String(
                response.data.data.shift_work[idx].to_time.minute
              ).padStart(2, "0"),
          ],
          day: [...tempDays],
        };
      });
    }

    yield put(getAssignmentRuleDetailSuccess(response.data.data));
  } catch (error) {
    yield put(getAssignmentRuleDetailFail());
  }
}

function* UpdateAssignmentRule(action) {
  try {
    yield updateAssignmentRuleAPI(action.payload);
    Notification("success", "Update assignment rule success");
    yield put(getListAssignmentRule(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* RunManualAssign(action) {
  try {
    yield runManualAssignAPI(action.payload);
    Notification("success", "Run manual assgin success");
  } catch (error) {}
}

function* ResetUsersCapacity(action) {
  try {
    yield resetUsersCapacityAPI(action.payload);
    Notification("success", "Reset users capacity success");
  } catch (error) {}
}

function* UpdatePriorityRule(action) {
  try {
    yield updatePriorityRuleAPI(action.payload);
    Notification("success", "Update priority rule success");
    yield put(getListAssignmentRule(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
