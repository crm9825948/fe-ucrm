import { put, takeLatest } from "@redux-saga/core/effects";
import {
  loadEmails,
  loadEmailsSuccessfully,
  loadEmailTemplate,
  loadEmailTemplateSuccessfull,
  sendNewEmail,
  replyEmail,
  forwardEmail,
  loadRecordData,
  loadRecordDataSuccess,
  setShowModal,
  setShowEditor,
  setStatusDrawer,
  setIsSending,
  getEmailTemplateById,
  updateFieldEmail,
  markReadEmail,
  updateShowEdit,
  saveDraft,
  editDraft,
  deleteDraft,
} from "redux/slices/email";
import {
  LoadEmailApi,
  LoadTemplateEmailApi,
  SendNewEmailApi,
  ReplyEmailApi,
  ForwardEmailApi,
  LoadRecordDataApi,
  GetEmailTemplateByIdApi,
  MarkReadEmailApi,
  saveDraftAPI,
  editDraftAPI,
  deleteDraftAPI,
} from "redux/api/emailComponentAPIs";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
import _ from "lodash";

export const componentEmail = (state) => state.componentEmail;

export default function* emailComponentSaga() {
  yield takeLatest(loadEmails.type, LoadEmails);
  yield takeLatest(loadEmailTemplate.type, LoadTemplateEmail);
  yield takeLatest(sendNewEmail.type, SendNewEmail);
  yield takeLatest(replyEmail.type, ReplyEmail);
  yield takeLatest(forwardEmail.type, ForwardEmail);
  yield takeLatest(loadRecordData.type, LoadRecordData);
  yield takeLatest(getEmailTemplateById.type, GetEmailTemplateById);
  yield takeLatest(markReadEmail.type, MarkReadEmail);
  yield takeLatest(saveDraft.type, SaveDraft);
  yield takeLatest(editDraft.type, EditDraft);
  yield takeLatest(deleteDraft.type, DeleteDraft);
}

function* LoadEmails(action) {
  try {
    // yield put(setShowLoadingScreen(true));
    const { data } = yield LoadEmailApi(action.payload);
    yield put(loadEmailsSuccessfully(data.data));
    // yield put(setShowLoadingScreen(false));
  } catch (error) {
    // yield put(setShowLoadingScreen(false));
  }
}

function* LoadTemplateEmail() {
  try {
    const { data } = yield LoadTemplateEmailApi();
    yield put(loadEmailTemplateSuccessfull(data.data));
  } catch (error) {}
}

function* SendNewEmail(action) {
  try {
    yield SendNewEmailApi(action.payload);
    yield put(
      loadEmails(action.payload?.record_id_main || action.payload?.record_id)
    );
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    Notification("success", "Send email successfull");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ReplyEmail(action) {
  try {
    yield ReplyEmailApi(action.payload);
    yield put(loadEmails(action.payload.record_id_main));
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    Notification("success", "Send email successfull");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ForwardEmail(action) {
  try {
    yield ForwardEmailApi(action.payload);
    yield put(loadEmails(action.payload.record_id_main));
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    Notification("success", "Send email successfull");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadRecordData(action) {
  try {
    const { data } = yield LoadRecordDataApi(action.payload);
    yield put(loadRecordDataSuccess(data.data));
  } catch (error) {}
}

function* GetEmailTemplateById(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield GetEmailTemplateByIdApi({
      record_id: action.payload.record_id,
      template_id: action.payload.template_id,
    });

    yield put(
      updateFieldEmail({
        key: "message",
        value: data.data.body.concat(action.payload.message),
      })
    );

    if (!_.get(data, "data.keep_original_subject", false)) {
      yield put(
        updateFieldEmail({
          key: "subject",
          value: data.data.subject,
        })
      );
    }

    yield put(updateShowEdit(true));
    yield put(setShowLoadingScreen(false));
  } catch (error) {}
}

function* MarkReadEmail({ payload }) {
  try {
    yield MarkReadEmailApi(payload.payload);
    yield put(loadEmails(payload.record_id));
  } catch (error) {
    Notification("error", "Mark read email fail");
  }
}

function* SaveDraft(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield saveDraftAPI(action.payload);
    yield put(
      loadEmails(action.payload?.record_id_main || action.payload?.record_id)
    );
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Save draft successfully");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* EditDraft(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield editDraftAPI(action.payload);
    yield put(
      loadEmails(action.payload?.record_id_main || action.payload?.record_id)
    );
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Save draft successfully");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteDraft(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteDraftAPI({ _id: action.payload._id });
    yield put(setIsSending(false));
    yield put(setShowModal(false));
    yield put(setShowEditor(false));
    yield put(setStatusDrawer("close"));
    yield put(setShowLoadingScreen(false));

    if (!action.payload.isSend) {
      yield put(loadEmails(action.payload?.record_id));
      Notification("success", "Delete successfully!");
    }
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
