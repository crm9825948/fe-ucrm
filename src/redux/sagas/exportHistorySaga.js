import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";

import { setShowLoadingScreen } from "redux/slices/global";
import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadAllExportHistoryAPI,
  loadExportHistoryAPI,
  deleteExportHistoryAPI,
} from "redux/api/exportHistoryAPI";
import {
  loadAllExportHistory,
  loadAllExportHistorySuccess,
  loadExportHistory,
  loadExportHistorySuccess,
  loadExportHistoryFail,
  deleteExportHistory,
  deleteExportHistorySuccess,
  setActive,
} from "redux/slices/exportHistory";

export default function* group() {
  yield takeLatest(loadAllExportHistory.type, LoadAllExportHistory);
  yield takeLatest(loadExportHistory.type, LoadExportHistory);
  yield takeLatest(deleteExportHistory.type, DeleteExportHistory);
}

function* LoadAllExportHistory() {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadAllExportHistoryAPI();
    yield put(loadAllExportHistorySuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadExportHistory(action) {
  try {
    const response = yield loadExportHistoryAPI(action.payload);
    yield put(loadExportHistorySuccess(response.data.data));
  } catch (error) {
    yield put(loadExportHistoryFail());
    Notification("error", error.response.data.error);
  }
}

function* DeleteExportHistory(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteExportHistoryAPI(action.payload);
    yield put(setActive("0"));
    yield put(deleteExportHistorySuccess(action.payload));
    yield put(setShowLoadingScreen(false));
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
