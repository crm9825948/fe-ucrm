import { put, takeEvery } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadProfiles } from "redux/slices/profile";
import fileDownload from "js-file-download";
import { Notification } from "components/Notification/Noti";

import {
  loadListRolesAPI,
  createRoleAPI,
  deleteRoleAPI,
  loadDetailsRoleAPI,
  updateRoleAPI,
  getRoleToSwitchAPI,
  exportRolesAPI,
} from "redux/api/rolesAPI";
import {
  loadDataNecessary,
  loadListRoles,
  loadListRolesSuccessfully,
  createRole,
  createRoleSuccess,
  createRoleFail,
  deleteRole,
  deleteRoleSuccess,
  deleteRoleFail,
  loadDetailsRole,
  loadDetailsRoleSuccess,
  loadDetailsRoleFail,
  updateRole,
  updateRoleSuccess,
  updateRoleFail,
  getRoleToSwitch,
  getRoleToSwitchSuccess,
  getRoleToSwitchFail,
  exportRoles,
} from "redux/slices/roles";

export default function* roles() {
  yield takeEvery(loadDataNecessary.type, LoadDataNecessary);
  yield takeEvery(loadListRoles.type, LoadListRoles);
  yield takeEvery(createRole.type, CreateRole);
  yield takeEvery(deleteRole.type, DeleteRole);
  yield takeEvery(loadDetailsRole.type, LoadDetailsRole);
  yield takeEvery(updateRole.type, UpdateRole);
  yield takeEvery(getRoleToSwitch.type, GetRoleToSwitch);
  yield takeEvery(exportRoles.type, ExportRoles);
}

function* LoadDataNecessary() {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadProfiles({
        current_page: 1,
        record_per_page: 1000,
      })
    );

    const response = yield loadListRolesAPI();
    let tempData = [];
    tempData.push(response.data.data);
    yield put(loadListRolesSuccessfully(tempData));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadListRoles() {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadListRolesAPI();
    let tempData = [];
    tempData.push(response.data.data);
    yield put(loadListRolesSuccessfully(tempData));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateRole(action) {
  try {
    yield createRoleAPI(action.payload);
    yield put(createRoleSuccess());
  } catch (error) {
    yield put(createRoleFail(error.response.data.error));
  }
}

function* UpdateRole(action) {
  try {
    yield updateRoleAPI(action.payload);
    yield put(updateRoleSuccess());
  } catch (error) {
    yield put(updateRoleFail(error.response.data.error));
  }
}

function* DeleteRole(action) {
  try {
    yield deleteRoleAPI(action.payload);
    yield put(deleteRoleSuccess());
  } catch (error) {
    yield put(deleteRoleFail(error.response.data.error));
  }
}

function* LoadDetailsRole(action) {
  try {
    const response = yield loadDetailsRoleAPI(action.payload);
    yield put(loadDetailsRoleSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsRoleFail());
  }
}

function* GetRoleToSwitch(action) {
  try {
    const response = yield getRoleToSwitchAPI(action.payload);
    let tempRoles = [];
    Object.entries(response.data).forEach(([key, val]) => {
      tempRoles.push({
        label: val,
        value: key,
      });
    });
    yield put(getRoleToSwitchSuccess(tempRoles));
  } catch (error) {
    yield put(getRoleToSwitchFail());
  }
}

function* ExportRoles() {
  try {
    const response = yield exportRolesAPI();
    fileDownload(response.data, "roles.xlsx");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
