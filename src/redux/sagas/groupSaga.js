import { put, takeLatest, delay } from "@redux-saga/core/effects";

import { setShowLoadingScreen } from "redux/slices/global";
import { loadAllRole, loadAllUser } from "redux/slices/user";

import {
  loadGroupsAPI,
  createGroupsAPI,
  loadDetailsGroupsAPI,
  updateGroupAPI,
  deleteGroupAPI,
} from "redux/api/groupAPI";
import {
  loadDataNecessary,
  loadGroups,
  loadGroupsSuccess,
  loadAllGroups,
  loadAllGroupsSuccess,
  loadAllGroupsFail,
  createGroup,
  createGroupSuccess,
  createGroupFail,
  loadDetailsGroup,
  loadDetailsGroupSuccess,
  loadDetailsGroupFail,
  updateGroup,
  updateGroupSuccess,
  updateGroupFail,
  deleteGroup,
  deleteGroupSuccess,
  deleteGroupFail,
} from "redux/slices/group";

export default function* group() {
  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  yield takeLatest(loadGroups.type, LoadGroups);
  yield takeLatest(loadAllGroups.type, LoadAllGroups);
  yield takeLatest(createGroup.type, CreateGroup);
  yield takeLatest(loadDetailsGroup.type, LoadDetailsGroup);
  yield takeLatest(updateGroup.type, UpdateGroup);
  yield takeLatest(deleteGroup.type, DeleteGroup);
}

function* LoadDataNecessary() {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
    yield put(loadAllRole());
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 100000,
      })
    );
    yield delay(800);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadGroups(action) {
  try {
    if (!window.location.pathname.includes("consolidated-view")) {
      yield put(setShowLoadingScreen(true));
    }
    const response = yield loadGroupsAPI(action.payload);
    yield put(loadGroupsSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadAllGroups(action) {
  try {
    const response = yield loadGroupsAPI(action.payload);
    yield put(loadAllGroupsSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllGroupsFail());
  }
}

function* CreateGroup(action) {
  try {
    const response = yield createGroupsAPI(action.payload);
    yield put(createGroupSuccess(response.data.data));
  } catch (error) {
    yield put(createGroupFail(error.response.data.error));
  }
}

function* LoadDetailsGroup(action) {
  try {
    const response = yield loadDetailsGroupsAPI(action.payload);
    yield put(loadDetailsGroupSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsGroupFail(error.response.data.error));
  }
}

function* UpdateGroup(action) {
  try {
    const response = yield updateGroupAPI(action.payload);
    yield put(updateGroupSuccess(response.data.data));
  } catch (error) {
    yield put(updateGroupFail(error.response.data.error));
  }
}

function* DeleteGroup(action) {
  try {
    const response = yield deleteGroupAPI(action.payload);
    yield put(deleteGroupSuccess(response.data.data));
  } catch (error) {
    yield put(deleteGroupFail(error.response.data.error));
  }
}
