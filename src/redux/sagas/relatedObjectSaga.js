import { put, takeLatest } from "@redux-saga/core/effects";
import {
  loadObjectAPI,
  createRelatedObjectAPI,
  deleteRelatedAPI,
} from "../api/relatedObjectAPI";
import {
  loadRelatedObject,
  loadRelatedObjectFail,
  loadRelatedObjectSuccess,
  createRelatedObject,
  createRelatedObjectFail,
  createRelatedObjectSuccess,
  deleteRelated,
  deleteRelatedFail,
  deleteRelatedSuccess,
} from "../slices/relatedObject";
import { Notification } from "components/Notification/Noti";

export default function* relatedObject() {
  yield takeLatest(loadRelatedObject.type, LoadRelatedObject);
  yield takeLatest(createRelatedObject.type, CreateRelatedObject);
  yield takeLatest(deleteRelated.type, DeleteRelatedObject);
}

function* LoadRelatedObject(action) {
  try {
    const response = yield loadObjectAPI();
    yield put(loadRelatedObjectSuccess(response.data.data));
  } catch (error) {
    yield loadRelatedObjectFail();
  }
}

function* CreateRelatedObject(action) {
  try {
    yield createRelatedObjectAPI({
      Source_Object: action.payload.Source_Object,
      Target_Object: action.payload.Target_Object,
    });
    yield put(createRelatedObjectSuccess());
    if (action.payload.type !== "diagram") {
      yield put(loadRelatedObject());
    }
  } catch (error) {
    Notification("error", error.response.data.error);
    if (
      error.response.data.error ===
      "The reversed relationship has already exists."
    ) {
      yield put(loadRelatedObject());
    }
    yield put(createRelatedObjectFail());
  }
}

function* DeleteRelatedObject(action) {
  try {
    yield deleteRelatedAPI(action.payload);
    yield put(deleteRelatedSuccess());
    yield put(loadRelatedObject());
    Notification("success", "Delete successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(deleteRelatedFail());
  }
}
