import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import { createRecordAPI, updateRecordAPI } from "redux/api/objectsAPIs";
import { loadUserDetail } from "redux/slices/user";

import {
  createRecordFail,
  setIsOpenDuplicate,
  setListDuplicate,
  setShowModal,
} from "redux/slices/objects";

import {
  loadListStatusAPI,
  loadListReasonCodeAPI,
  changeAgentStatusAPI,
  makeCallAPI,
  endCallAPI,
  holdCallAPI,
  unHoldCallAPI,
  loadAgentsOnlineAPI,
  transferCallAPI,
  conferenceCallAPI,
  consultCallAPI,
  answerCallAPI,
  updateInteractionAPI,
  createInteractionAPI,
  getFieldCallCenterAPI,
  getObjectsToCallCenterAPI,
  searchCoreCallCenterAPI,
  createRecordCoreCallCenterAPI,
  connectExtAPI,
  disconnectExtAPI,
} from "redux/api/callCenterAPI";
import {
  updateStatusCall,
  updateScreen,
  updateLoading,
  updateHold,
  loadlistStatus,
  loadlistStatusSuccess,
  loadListReasonCode,
  loadListReasonCodeSuccess,
  changeAgentStatus,
  makeCall,
  endCall,
  holdCall,
  unHoldCall,
  loadAgentsOnline,
  loadAgentsOnlineSuccess,
  transferCall,
  conferenceCall,
  consultCall,
  answerCall,
  updateInteraction,
  createRecordCallCenter,
  createRecordCallCenterSuccess,
  updateRecordCallCenter,
  updateRecordCallCenterResult,
  getFieldsMappingCallCenter,
  getFieldsMappingCallCenterSuccess,
  getObjectsToCall,
  getObjectsToCallResult,
  searchCoreCallCenter,
  searchCoreCallCenterResult,
  createRecordCoreCallCenter,
  createRecordCoreCallCenterResult,
  connectExt,
  setAlreadyUseExt,
  disconnectExt,
} from "redux/slices/callCenter";

export default function* campaign() {
  yield takeLatest(loadlistStatus.type, LoadlistStatus);
  yield takeLatest(loadListReasonCode.type, LoadListReasonCode);
  yield takeLatest(changeAgentStatus.type, ChangeAgentStatus);
  yield takeLatest(makeCall.type, MakeCall);
  yield takeLatest(endCall.type, EndCall);
  yield takeLatest(holdCall.type, HoldCall);
  yield takeLatest(unHoldCall.type, UnHoldCall);
  yield takeLatest(loadAgentsOnline.type, LoadAgentsOnline);
  yield takeLatest(transferCall.type, TransferCall);
  yield takeLatest(conferenceCall.type, ConferenceCall);
  yield takeLatest(consultCall.type, ConsultCall);
  yield takeLatest(answerCall.type, AnswerCall);
  yield takeLatest(updateInteraction.type, UpdateInteraction);
  yield takeLatest(createRecordCallCenter.type, CreateRecordCallCenter);
  yield takeLatest(updateRecordCallCenter.type, UpdateRecordCallCenter);
  yield takeLatest(getFieldsMappingCallCenter.type, GetFieldsMappingCallCenter);
  yield takeLatest(getObjectsToCall.type, GetObjectsToCall);
  yield takeLatest(searchCoreCallCenter.type, SearchCoreCallCenter);
  yield takeLatest(createRecordCoreCallCenter.type, CreateRecordCoreCallCenter);
  yield takeLatest(connectExt.type, ConnectExt);
  yield takeLatest(disconnectExt.type, DisConnectExt);
}

function* LoadlistStatus(action) {
  try {
    const response = yield loadListStatusAPI(action.payload);
    yield put(loadlistStatusSuccess(response.data.data.data));
  } catch (error) {
    // Notification("error", error.response.data.error);
  }
}

function* LoadListReasonCode(action) {
  try {
    const response = yield loadListReasonCodeAPI(action.payload);
    yield put(loadListReasonCodeSuccess(response.data.data.reasonName));
  } catch (error) {
    // Notification("error", error.response.data.error);
  }
}

function* ChangeAgentStatus(action) {
  try {
    yield changeAgentStatusAPI(action.payload);
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* MakeCall(action) {
  try {
    yield makeCallAPI(action.payload);
    yield put(
      updateLoading({
        type: "loadingMakeCall",
        loading: false,
      })
    );
    yield put(updateStatusCall("Connecting..."));
    yield put(updateScreen("inCall"));
  } catch (error) {
    yield put(
      updateLoading({
        type: "loadingMakeCall",
        loading: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* EndCall(action) {
  try {
    yield endCallAPI(action.payload);
    yield put(
      updateLoading({
        type: "loadingEndCall",
        loading: false,
      })
    );
  } catch (error) {
    yield put(
      updateLoading({
        type: "loadingEndCall",
        loading: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* HoldCall(action) {
  try {
    yield holdCallAPI(action.payload);
    yield put(updateHold(true));
    Notification("success", "Hold call successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* UnHoldCall(action) {
  try {
    yield unHoldCallAPI(action.payload);
    yield put(updateHold(false));
    Notification("success", "Un hold call successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadAgentsOnline(action) {
  try {
    const response = yield loadAgentsOnlineAPI(action.payload);
    if (response.data.data.data.length > 0) {
      let tempList = [];
      response.data.data.data.map((item, idx) => {
        return tempList.push({
          key: idx,
          name: item.userName,
          extension: item.extentionID,
          action: "",
        });
      });
      yield put(loadAgentsOnlineSuccess(tempList));
    }
  } catch (error) {
    yield put(loadAgentsOnlineSuccess([]));
    Notification("error", error.response.data.error);
  }
}

function* TransferCall(action) {
  try {
    yield transferCallAPI(action.payload);
    Notification("success", "Transfer call successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ConferenceCall(action) {
  try {
    yield conferenceCallAPI(action.payload);
    Notification("success", "Conference call successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ConsultCall(action) {
  try {
    yield consultCallAPI(action.payload);
    Notification("success", "Consult call successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* AnswerCall(action) {
  try {
    yield answerCallAPI(action.payload);
    yield put(
      updateLoading({
        type: "loadingAnswerCall",
        loading: false,
      })
    );
    yield put(updateStatusCall("Connecting..."));
  } catch (error) {
    yield put(
      updateLoading({
        type: "loadingAnswerCall",
        loading: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* UpdateInteraction(action) {
  try {
    yield updateInteractionAPI(action.payload);
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* CreateRecordCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createRecordAPI(action.payload.data);
    yield createInteractionAPI({
      ...action.payload.dataCall,
      record_id: response.data.data._id,
    });
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    yield put(
      createRecordCallCenterSuccess({
        status: "Success",
        record_id: response.data.data._id,
        object_id: action.payload.data.object_id,
      })
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));

    if (error.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(error.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
      yield put(setShowModal(false));
    } else {
      Notification("error", error.response.data.error);
      yield put(createRecordFail());
    }
  }
}

function* UpdateRecordCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateRecordAPI(action.payload.data);

    yield put(updateInteraction(action.payload.dataCall));

    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield delay(500);
    yield put(
      updateRecordCallCenterResult({
        status: "Success",
        record_id: action.payload.dataCall.record_id || "",
        object_id: action.payload.dataCall.object_id || "",
      })
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* GetFieldsMappingCallCenter(action) {
  try {
    const response = yield getFieldCallCenterAPI(action.payload);
    yield put(getFieldsMappingCallCenterSuccess(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetObjectsToCall(action) {
  try {
    const response = yield getObjectsToCallCenterAPI(action.payload);
    yield put(getObjectsToCallResult(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* SearchCoreCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield searchCoreCallCenterAPI(action.payload);
    yield put(searchCoreCallCenterResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateRecordCoreCallCenter(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createRecordCoreCallCenterAPI(action.payload.data);

    yield createInteractionAPI({
      ...action.payload.dataCall,
      record_id: response.data.data._id,
    });

    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    yield put(
      createRecordCoreCallCenterResult({
        status: "Success",
        record_id: response.data.data._id,
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ConnectExt(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield connectExtAPI(action.payload);
    if (response.data.status_code === 226) {
      yield put(setAlreadyUseExt(true));
    } else {
      yield put(
        loadUserDetail({
          _id: action.payload.userID,
        })
      );
      Notification("success", "Connect successfully!");
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DisConnectExt(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield disconnectExtAPI(action.payload);
    yield put(
      loadUserDetail({
        _id: action.payload.userID,
      })
    );
    Notification("success", "Disconnect successfully!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
