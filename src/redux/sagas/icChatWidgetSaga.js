import { put, takeLatest, select, takeEvery } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";

import { loadRecordDataAPI } from "redux/api/objectsAPIs";
import { loadMappingExposeAPI } from "redux/api/consolidatedViewSettingsAPI";

import {
  getListMessagesICAPI,
  getAllUserICAPI,
  getChatBotICAPI,
  getMessageDetailsICAPI,
  updateStatusICCRMAPI,
  updateStatusICAPI,
  getStatusICAPI,
  addTagICAPI,
  deleteTagICAPI,
  getTagsICAPI,
  createTagICAPI,
  disableBotICAPI,
  sendMessageICAPI,
  completeConversationICAPI,
  resgisterSignalrICAPI,
  registerConnectionIntervalICAPI,
  getQuickMessagesICAPI,
  getUserStateICAPI,
  getInteractionMemberICAPI,
  addAgentICAPI,
  transferAgentICAPI,
  getIndexMessageICAPI,
  getFacebookPostICAPI,
  getListSocialRegisterICAPI,
  getEmailDetailsICAPI,
} from "redux/api/icChatWidgetAPI";
import {
  getConfigIC,
  getConfigICResult,
  getAllUserIC,
  getAllUserICResult,
  getChatBotIC,
  getChatBotICResult,
  getListMessagesAssignedIC,
  getListMessagesAssignedICResult,
  getListMessagesCompletedIC,
  getListMessagesCompletedICResult,
  getMessageDetailsIC,
  getMessageDetailsICResult,
  $selectedMessage,
  $currentPageIC,
  $isActive,
  updateStatusIC,
  getStatusIC,
  getStatusICResult,
  addTagIC,
  addTagICResult,
  deleteTagIC,
  deleteTagICResult,
  getTagsIC,
  getTagsICResult,
  createTagIC,
  $addTag,
  disableBotIC,
  sendMessageIC,
  sendMessageWithFileIC,
  completeConversationIC,
  resgisterSignalrIC,
  registerConnectionIntervalIC,
  addNewMessage,
  getQuickMessagesIC,
  getQuickMessagesICResult,
  $newMessage,
  getUserToAddIC,
  getUserToAddICResult,
  addAgentIC,
  $addAgent,
  transferAgentIC,
  $transferAgent,
  searchMessagesIC,
  searchMessagesICResult,
  getIndexMessageIC,
  getIndexMessageICResult,
  getFacebookPostIC,
  getFacebookPostICResult,
  getListSocialRegisterIC,
  getListSocialRegisterICResult,
} from "redux/slices/icChatWidget";
import _ from "lodash";
export const icChatWidgetReducer = (state) => state.icChatWidgetReducer;

export default function* icIntegration() {
  yield takeLatest(getConfigIC.type, GetConfigIC);
  yield takeLatest(getAllUserIC.type, GetAllUserIC);
  yield takeLatest(getChatBotIC.type, GetChatBotIC);
  yield takeLatest(getListMessagesAssignedIC.type, GetListMessagesAssignedIC);
  yield takeLatest(getListMessagesCompletedIC.type, GetListMessagesCompletedIC);
  yield takeLatest(getMessageDetailsIC.type, GetMessageDetailsIC);
  yield takeLatest(updateStatusIC.type, UpdateStatusIC);
  yield takeLatest(getStatusIC.type, GetStatusIC);
  yield takeLatest(addTagIC.type, AddTagIC);
  yield takeLatest(deleteTagIC.type, DeleteTagIC);
  yield takeLatest(getTagsIC.type, GetTagsIC);
  yield takeLatest(createTagIC.type, CreateTagIC);
  yield takeLatest(disableBotIC.type, DisableBotIC);
  yield takeEvery(sendMessageIC.type, SendMessageIC);
  yield takeEvery(sendMessageWithFileIC.type, SendMessageWithFileIC);
  yield takeLatest(completeConversationIC.type, CompleteConversationIC);
  yield takeLatest(resgisterSignalrIC.type, ResgisterSignalrIC);
  yield takeLatest(
    registerConnectionIntervalIC.type,
    RegisterConnectionIntervalIC
  );
  yield takeLatest(getQuickMessagesIC.type, GetQuickMessagesIC);
  yield takeLatest(getUserToAddIC.type, GetUserToAddIC);
  yield takeLatest(addAgentIC.type, AddAgentIC);
  yield takeLatest(transferAgentIC.type, TransferAgentIC);
  yield takeLatest(searchMessagesIC.type, SearchMessagesIC);
  yield takeLatest(getIndexMessageIC.type, GetIndexMessageIC);
  yield takeLatest(getFacebookPostIC.type, GetFacebookPostIC);
  yield takeLatest(getListSocialRegisterIC.type, GetListSocialRegisterIC);
}

function* GetConfigIC(action) {
  try {
    const res = yield loadMappingExposeAPI({
      object_id: action.payload.object_id,
      type: "IC",
    });
    const response = yield loadRecordDataAPI(action.payload);

    if (res.data.data !== "No record found.") {
      const fieldConfig = {
        fieldID: _.get(res, "data.data.key_field_update.value", ""),
        value:
          response.data.data?.[
            _.get(res, "data.data.key_field_update.value", "")
          ]?.value,
      };
      yield put(getConfigICResult(fieldConfig));
    }
  } catch (error) {
    // Notification("error", error.response.data.error);
  }
}

function* GetChatBotIC(action) {
  try {
    const response = yield getChatBotICAPI(action.payload);
    yield put(getChatBotICResult(_.get(response, "data.data.items", [])));
  } catch (error) {
    // Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetAllUserIC(action) {
  try {
    const response = yield getAllUserICAPI(action.payload);
    yield put(getAllUserICResult(_.get(response, "data.data.items", [])));
  } catch (error) {
    // Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetListMessagesAssignedIC(action) {
  try {
    yield put($currentPageIC(1));
    yield put($isActive(0));
    const response = yield getListMessagesICAPI(action.payload);
    yield put(
      getListMessagesAssignedICResult(_.get(response, "data.data.items", []))
    );

    if (_.get(response, "data.data.items", [])?.length > 0) {
      yield put($selectedMessage(_.get(response, "data.data.items[0]", {})));
    }
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetListMessagesCompletedIC(action) {
  try {
    yield put($currentPageIC(1));

    yield put($isActive(0));
    const response = yield getListMessagesICAPI(action.payload);
    yield put(
      getListMessagesCompletedICResult(_.get(response, "data.data.items", []))
    );
    if (_.get(response, "data.data.items", [])?.length > 0) {
      yield put($selectedMessage(_.get(response, "data.data.items[0]", {})));
    }
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetMessageDetailsIC(action) {
  try {
    const response = yield getMessageDetailsICAPI({
      ...action.payload,
      post_id: undefined,
    });

    if (_.get(action, "payload.channel_type", "") === "email") {
      let emailIds = [];
      let logsIndex = [];

      _.map(_.get(response, "data.data.items", [])?.reverse(), (item, idx) => {
        if (_.get(item, "message_id", "")) {
          emailIds.push({
            id: _.get(item, "message_id", ""),
          });
        } else {
          logsIndex.push(idx);
        }
      });

      const responseEmail = yield getEmailDetailsICAPI(emailIds);
      let result = [..._.get(responseEmail, "data.data.items", [])];

      _.map(logsIndex, (index) => {
        result.splice(index, 0, response?.data?.data?.items[index] || {});
      });

      yield put(
        getMessageDetailsICResult({
          data: result,
          page: _.get(action, "payload.page", 1),
          channel_type: _.get(action, "payload.channel_type", ""),
        })
      );
      yield put(getIndexMessageICResult(null));
    } else {
      yield put(
        getMessageDetailsICResult({
          data: _.get(response, "data.data.items", []),
          page: _.get(action, "payload.page", 1),
          channel_type: _.get(action, "payload.channel_type", ""),
        })
      );
      yield put(getIndexMessageICResult(null));

      if (
        _.get(action, "payload.channel_type", "") === "facebook_comment" ||
        _.get(action, "payload.channel_type", "") === "instagram_comment"
      ) {
        yield put(
          getFacebookPostIC({
            page_social_id: _.get(action, "payload.page_social_id", ""),
            post_id: _.get(action, "payload.post_id", ""),
          })
        );
      } else {
        yield put(getFacebookPostICResult({}));
      }
    }
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* UpdateStatusIC(action) {
  try {
    yield updateStatusICCRMAPI({
      ic_status: _.get(action, "payload.ic_status", ""),
    });
    yield updateStatusICAPI({
      state_detail: _.get(action, "payload.ic_status", ""),
      username: _.get(action, "payload.username", ""),
    });
    yield put(
      getStatusIC({
        username: _.get(action, "payload.username", ""),
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetStatusIC(action) {
  try {
    const response = yield getStatusICAPI(action.payload);
    yield put(
      getStatusICResult(_.get(response, "data.data.state_detail", "available"))
    );
  } catch (error) {
    // Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* AddTagIC(action) {
  try {
    const response = yield addTagICAPI(action.payload);
    yield put(
      addTagICResult({
        ..._.get(response, "data.data", {}),
        interaction_id: _.get(action, "payload.interaction_id", ""),
      })
    );
    yield put(
      $addTag({
        visible: false,
        interaction_id: "",
      })
    );
    Notification("success", "Add tag successfully!");
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* DeleteTagIC(action) {
  try {
    yield deleteTagICAPI(action.payload);
    yield put(deleteTagICResult(action.payload));
    Notification("success", "Delete tag successfully!");
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* CreateTagIC(action) {
  try {
    yield createTagICAPI(action.payload);
    yield put(getTagsIC({}));
    Notification("success", "Create tag successfully!");
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetTagsIC(action) {
  try {
    const response = yield getTagsICAPI(action.payload);

    let result = [];
    _.map(_.get(response, "data.data.items", []), (item) => {
      result.push({
        label: _.get(item, "name", ""),
        value: _.get(item, "id", ""),
      });
    });
    yield put(getTagsICResult(result));
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* DisableBotIC(action) {
  try {
    const response = yield disableBotICAPI(action.payload);

    // const { assignedMessages } = yield select(icChatWidgetReducer);
    // let temp = [...assignedMessages];

    // const findIndex = temp.findIndex(
    //   (item) =>
    //     _.get(item, "interaction_id", "") === action.payload.interaction_id
    // );
    // if (findIndex !== -1) {
    //   temp[findIndex] = {
    //     ...temp[findIndex],
    //     is_botprogress: false,
    //   };
    // }

    // yield put(getListMessagesAssignedICResult(temp));

    yield put(
      $newMessage({
        ..._.get(response, "data.data", {}),
        is_botprogress: false,
      })
    );
    Notification("success", "Disable bot successfully!");
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* SendMessageIC(action) {
  try {
    const response = yield sendMessageICAPI(action.payload);

    if (_.get(action, "payload.channel_type") === "email") {
      let newMessage = {
        server_email_type: _.get(action, "payload.data_mail.server_email_type"),
        attachments: _.get(action, "payload.data_mail.attachments"),
        direction: "send",
        subject: _.get(action, "payload.data_mail.subject"),
        from: _.get(action, "payload.data_mail.from_mail"),
        to: _.get(action, "payload.to", [])?.toString(),
        list_cc: _.get(action, "payload.cc", [])?.toString(),
        list_bcc: null,
        reply_to: _.get(action, "payload.data_mail.message_id", ""),
        reply_message_id: null,
        emaildata_warning: null,
        modify_by: null,
        content: _.get(action, "payload.data_mail.content", ""),
        state: "sent",
        create_by: _.get(response, "data.data.create_by", ""),
        create_time: _.get(response, "data.data.create_time", ""),
        modify_time: _.get(response, "data.data.modify_time", ""),
        //unused?
        email_box_id: "",
        thread_id: "",
        message_id: "",
        id: _.get(response, "data.data.id", ""),
        tenant_id: _.get(response, "data.data.tenant_id", ""),
      };

      yield put(addNewMessage(newMessage));
    } else {
      yield put(addNewMessage(_.get(response, "data.data"), {}));
    }
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* SendMessageWithFileIC(action) {
  try {
    for (const i in action.payload) {
      if (action.payload.hasOwnProperty(i)) {
        const response = yield sendMessageICAPI(action.payload[i]);
        yield put(addNewMessage(_.get(response, "data.data"), {}));
      }
    }
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* ResgisterSignalrIC(action) {
  try {
    yield resgisterSignalrICAPI(action.payload);
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* RegisterConnectionIntervalIC(action) {
  try {
    yield registerConnectionIntervalICAPI(action.payload);
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* CompleteConversationIC(action) {
  try {
    const { configIC } = yield select(icChatWidgetReducer);
    yield completeConversationICAPI(action.payload);
    yield put(
      getListMessagesAssignedIC({
        extention: "assigned",
        limit: 1000,
        page: 1,
        view_type: "",
        // sorts: [
        //   {
        //     name_field: "state",
        //     type_sort: "ASC",
        //   },
        //   {
        //     name_field: "create_time",
        //     type_sort: "desc",
        //   },
        // ],
        search_list: [
          {
            name_field: "customer_id",
            value_search: _.get(configIC, "value", ""),
          },
        ],
      })
    );
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetQuickMessagesIC(action) {
  try {
    const response = yield getQuickMessagesICAPI(action.payload);
    yield put(getQuickMessagesICResult(_.get(response, "data.data.items", [])));
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetUserToAddIC(action) {
  try {
    const response1 = yield getUserStateICAPI({
      search_list: [
        {
          name_field: "state",
          value_search: "online",
        },
      ],
    });
    const response2 = yield getInteractionMemberICAPI({
      interaction_id: _.get(action, "payload.interaction_id", ""),
      tenant_id: localStorage.getItem("icIntegration_tenantID"),
    });

    let temp = [];

    if (_.get(action, "payload.type") === "transfer") {
      temp = _.get(response1, "data.data.items", [])?.filter((el) => {
        return !_.get(response2, "data.data.items", []).find((element) => {
          return element?.username === el?.username && element?.is_owner;
        });
      });
    } else {
      temp = _.get(response1, "data.data.items", [])?.filter((el) => {
        return !_.get(response2, "data.data.items", []).find((element) => {
          return element?.username === el?.username;
        });
      });
    }

    let result = [];
    _.map(temp, (item, idx) => {
      result.push({
        label:
          _.get(item, "fullname", "") + " - " + _.get(item, "username", ""),
        value: _.get(item, "username", idx),
      });
    });

    yield put(getUserToAddICResult(result));
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* AddAgentIC(action) {
  try {
    yield put(
      $addAgent({
        isLoading: true,
        visible: true,
        ...action.payload,
      })
    );
    const response = yield addAgentICAPI(action.payload);
    yield put(addNewMessage(_.get(response, "data.data"), {}));
    yield put(
      $addAgent({
        isLoading: false,
        visible: false,
        interaction_id: "",
        note_content: "",
        username_dest: "",
        username_source: "",
      })
    );
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
    yield put(
      $addAgent({
        isLoading: false,
        visible: false,
        interaction_id: "",
        note_content: "",
        username_dest: "",
        username_source: "",
      })
    );
  }
}

function* TransferAgentIC(action) {
  try {
    yield put(
      $transferAgent({
        isLoading: true,
        visible: true,
        ...action.payload,
      })
    );
    yield transferAgentICAPI(action.payload);

    const { configIC } = yield select(icChatWidgetReducer);

    yield put(
      getListMessagesAssignedIC({
        extention: "assigned",
        limit: 1000,
        page: 1,
        view_type: "",
        // sorts: [
        //   {
        //     name_field: "state",
        //     type_sort: "ASC",
        //   },
        //   {
        //     name_field: "create_time",
        //     type_sort: "desc",
        //   },
        // ],
        search_list: [
          {
            name_field: "customer_id",
            value_search: _.get(configIC, "value", ""),
          },
        ],
      })
    );

    yield put(
      $transferAgent({
        isLoading: false,
        visible: false,
        interaction_id: "",
        note_content: "",
        username_dest: "",
        username_source: "",
      })
    );
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
    yield put(
      $transferAgent({
        isLoading: false,
        visible: false,
        interaction_id: "",
        note_content: "",
        username_dest: "",
        username_source: "",
      })
    );
  }
}

function* SearchMessagesIC(action) {
  try {
    const response = yield getMessageDetailsICAPI(action.payload);
    yield put(searchMessagesICResult(_.get(response, "data.data.items", [])));
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetIndexMessageIC(action) {
  try {
    const response = yield getIndexMessageICAPI(action.payload);
    yield put(
      getIndexMessageICResult(_.get(response, "data.data.index", null))
    );
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetFacebookPostIC(action) {
  try {
    const response = yield getFacebookPostICAPI(action.payload);
    yield put(getFacebookPostICResult(_.get(response, "data.data", {})));
  } catch (error) {
    yield put(getFacebookPostICResult({}));
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}

function* GetListSocialRegisterIC(action) {
  try {
    const response = yield getListSocialRegisterICAPI(action.payload);
    yield put(
      getListSocialRegisterICResult(_.get(response, "data.data.items", []))
    );
  } catch (error) {
    Notification("error", _.get(error, "response.data.message", "Error!"));
  }
}
