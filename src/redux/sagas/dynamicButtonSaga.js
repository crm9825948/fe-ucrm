import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadObject } from "redux/slices/objectsManagement";
import { loadObjectFields } from "redux/slices/fieldsManagement";
import { loadAllGroups } from "redux/slices/group";
import { loadAllUser } from "redux/slices/user";
import { loadListObjectField } from "redux/slices/objects";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { getListSMSTemplate } from "redux/slices/smsTemplate";
import { FE_URL } from "constants/constants";
import { loadFieldsAPI } from "redux/api/objectsAPIs";

import {
  loadUserFieldsAPI,
  getLinkingFieldEmailAPI,
} from "redux/api/workflowsAPI";

import {
  loadUserFields,
  loadUserFieldsSuccess,
  loadLinkingFieldEmailSuccess,
  loadFieldsMainObjectSuccess,
} from "redux/slices/workflows";

import {
  loadAllDynamicButtonAPI,
  createDynamicButtonAPI,
  activeDynamicButtonAPI,
  deleteDynamicButtonAPI,
  loadDetailsDynamicButtonAPI,
  updateDynamicButtonAPI,
  loadAllActionsAPI,
  createActionDynamicButtonAPI,
  activeActionDynamicButtonAPI,
  deleteActionDynamicButtonAPI,
  loadDetailsActionDynamicButtonAPI,
  updateActionDynamicButtonAPI,
  loadUserDynamicButtonAPI,
  runDynamicButtonAPI,
} from "redux/api/dynamicButtonAPI";
import {
  loadDataNecessary,
  allDynamicButton,
  createDynamicButton,
  setShowModalAdd,
  activeDynamicButton,
  changeState,
  deleteDynamicButton,
  deleteDynamicButtonResult,
  loadDataNecessaryEdit,
  loadDetailsDynamicButtonSuccess,
  updateDynamicButton,
  loadListActions,
  loadListActionsSuccess,
  createActionDynamicButton,
  setStatus,
  activeActionDynamicButton,
  updateStateAction,
  deleteActionDynamicButton,
  loadDetailsActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  loadDetailsActionDynamicButtonFail,
  updateActionDynamicButton,
  loadUserDynamicButton,
  loadUserDynamicButtonSuccess,
  runDynamicButton,
  runDynamicButtonResult,
} from "redux/slices/dynamicButton";

export default function* sharing() {
  yield takeLatest(loadUserFields.type, LoadUserFields);

  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  yield takeLatest(createDynamicButton.type, CreateDynamicButton);
  yield takeLatest(activeDynamicButton.type, ActiveDynamicButton);
  yield takeLatest(deleteDynamicButton.type, DeleteDynamicButton);
  yield takeLatest(loadDataNecessaryEdit.type, LoadDataNecessaryEdit);
  yield takeLatest(updateDynamicButton.type, UpdateDynamicButton);
  yield takeLatest(loadListActions.type, LoadListActions);
  yield takeLatest(createActionDynamicButton.type, CreateActionDynamicButton);
  yield takeLatest(activeActionDynamicButton.type, ActiveActionDynamicButton);
  yield takeLatest(deleteActionDynamicButton.type, DeleteActionDynamicButton);
  yield takeLatest(
    loadDetailsActionDynamicButton.type,
    LoadDetailsActionDynamicButton
  );
  yield takeLatest(updateActionDynamicButton.type, UpdateActionDynamicButton);
  yield takeLatest(loadUserDynamicButton.type, LoadUserDynamicButton);
  yield takeLatest(runDynamicButton.type, RunDynamicButton);
}

function* LoadUserFields(action) {
  try {
    const response = yield loadUserFieldsAPI(action.payload);
    yield put(loadUserFieldsSuccess(response.data.data));
  } catch (error) {}
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());

    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );

    const response = yield loadAllDynamicButtonAPI(action.payload);
    yield put(allDynamicButton(response.data.data));
    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateDynamicButton(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createDynamicButtonAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(setShowModalAdd(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    window.open(`${FE_URL}/edit-dynamic-button/${response.data.data}`, "_self");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ActiveDynamicButton(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield activeDynamicButtonAPI(action.payload);
    yield put(
      changeState({
        _id: action.payload._id,
        type: "change-status",
      })
    );
    yield put(setShowLoadingScreen(false));
    Notification("success", "Change status successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteDynamicButton(action) {
  try {
    yield deleteDynamicButtonAPI(action.payload);
    yield put(
      changeState({
        _id: action.payload._id,
        type: "delete",
      })
    );
    yield put(deleteDynamicButtonResult());
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(deleteDynamicButtonResult());
    Notification("error", error.response.data.error);
  }
}

function* LoadDataNecessaryEdit(action) {
  try {
    yield put(setShowLoadingScreen(true));

    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );

    yield put(loadObject());

    const details = yield loadDetailsDynamicButtonAPI(action.payload);
    yield put(loadDetailsDynamicButtonSuccess(details.data.data));

    yield put(loadObjectFields({ object_id: details.data.data.object_id }));

    yield put(loadUserFields({ object_id: details.data.data.object_id }));

    yield put(getListEmailTemplate());

    yield put(getListSMSTemplate());

    yield put(
      loadListObjectField({
        object_id: details.data.data.object_id,
        api_version: "2",
        show_meta_fields: true,
      })
    );

    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );

    const email = yield getLinkingFieldEmailAPI({
      object_id: details.data.data.object_id,
    });
    yield put(loadLinkingFieldEmailSuccess(email.data.data));

    const actions = yield loadAllActionsAPI(action.payload);
    yield put(loadListActionsSuccess(actions.data.data));

    const fieldMain = yield loadFieldsAPI({
      api_version: "2",
      object_id: details.data.data.object_id,
    });

    yield put(loadFieldsMainObjectSuccess(fieldMain.data.data));

    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadListActions(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const actions = yield loadAllActionsAPI(action.payload);
    yield put(loadListActionsSuccess(actions.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateDynamicButton(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateDynamicButtonAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield delay(500);
    window.open(`${FE_URL}/dynamic-button`, "_self");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateActionDynamicButton(action) {
  try {
    yield put(setStatus("Loading"));
    yield createActionDynamicButtonAPI(action.payload);
    yield put(setStatus("Create action successfully!"));
    Notification("success", "Create successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* UpdateActionDynamicButton(action) {
  try {
    yield put(setStatus("Loading"));
    yield updateActionDynamicButtonAPI(action.payload);
    yield put(
      updateStateAction({
        type: "update",
        id: action.payload._id,
        name: action.payload.action_name || action.payload.data.action_name,
      })
    );

    yield put(setStatus("Update action successfully!"));
    Notification("success", "Update successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ActiveActionDynamicButton(action) {
  try {
    yield activeActionDynamicButtonAPI(action.payload);
    yield put(
      updateStateAction({
        type: "change-status",
        id: action.payload._id,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DeleteActionDynamicButton(action) {
  try {
    yield deleteActionDynamicButtonAPI(action.payload);
    yield put(
      updateStateAction({
        type: "delete",
        id: action.payload._id,
      })
    );
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Delete action successfully!");
  } catch (error) {
    yield put(setShowModalConfirmDelete(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsActionDynamicButton(action) {
  try {
    yield put(setStatus("LoadingAction"));
    const response = yield loadDetailsActionDynamicButtonAPI(action.payload);
    yield put(loadDetailsActionDynamicButtonSuccess(response.data.data));
    yield put(setStatus(null));
  } catch (error) {
    yield put(setStatus(null));
    Notification("error", error.response.data.error);
    yield put(loadDetailsActionDynamicButtonFail(error.response.data.error));
  }
}

function* LoadUserDynamicButton(action) {
  try {
    if (!window.location.pathname.includes("consolidated-view")) {
      yield put(setShowLoadingScreen(true));
    }
    const response = yield loadUserDynamicButtonAPI(action.payload);
    yield put(loadUserDynamicButtonSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadUserDynamicButtonSuccess([]));
    Notification("error", error.response.data.error);
  }
}

function* RunDynamicButton(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield runDynamicButtonAPI({
      ...action.payload.data,
      dynamic_field: { ...action.payload.dynamic_field },
    });
    yield put(setShowLoadingScreen(false));
    yield put(
      runDynamicButtonResult({
        isRunSuccess: true,
        isReload: action.payload.reloadable || false,
      })
    );
    Notification("success", "Run dynamic button successfully!");
    if (
      window.location.pathname.includes(
        "knowledge-base-enhancement/edit-article"
      )
    ) {
      yield delay(500);
      window.open(
        `${FE_URL}/knowledge-base-enhancement/${action.payload.data.object_id}`,
        "_self"
      );
    }
  } catch (error) {
    yield put(
      runDynamicButtonResult({
        isRunSuccess: false,
        isReload: action.payload.reloadable || false,
      })
    );
    Notification("error", error.response.data.error);
  }
}
