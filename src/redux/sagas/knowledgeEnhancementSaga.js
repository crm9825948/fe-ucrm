import { put, takeEvery, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  createKnowledgeSettingAPI,
  deleteEnhancementSettingAPI,
  getAllSettingKnowledgeBaseAPI,
  loadFieldsAPI,
  updateEnhancementSettingAPI,
} from "redux/api/knowledgeEnhancementAPI";
import {
  createKnowledgeSetting,
  getAllSettingKnowledgeBaseSuccess,
  getFieldsArticleSuccess,
  getFieldsObject,
  getFieldsObjectFail,
  getFieldsObjectSuccess,
  getFieldsTagSuccess,
  getAllSettingKnowledgeBase,
  updateSettingEnhancement,
  deleteSettingEnhancement,
} from "redux/slices/knowledgeEnhancement";

export default function* knowledgeEnhancementSage() {
  yield takeEvery(getFieldsObject.type, getFieldsObjectSaga);
  yield takeLatest(createKnowledgeSetting.type, createKnowledgeSettingSaga);
  yield takeLatest(
    getAllSettingKnowledgeBase.type,
    getAllSettingKnowledgeBaseSaga
  );
  yield takeLatest(updateSettingEnhancement.type, updateSettingEnhancementSaga);
  yield takeLatest(deleteSettingEnhancement.type, deleteSettingEnhancementSaga);
}

function* getFieldsObjectSaga(action) {
  try {
    const { data } = yield loadFieldsAPI(action.payload);
    if (action.payload.key === "tag") {
      yield put(getFieldsTagSuccess(data.data));
    } else if (action.payload.key === "article") {
      yield put(getFieldsArticleSuccess(data.data));
    } else {
      yield put(getFieldsObjectSuccess(data.data));
    }
  } catch (error) {
    yield put(getFieldsObjectFail());
    Notification("error", error.response.data.error);
  }
}

function* createKnowledgeSettingSaga(action) {
  try {
    yield createKnowledgeSettingAPI(action.payload);
    Notification("success", "Create success");
    yield put(getAllSettingKnowledgeBase());
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getAllSettingKnowledgeBaseSaga() {
  try {
    const { data } = yield getAllSettingKnowledgeBaseAPI();
    yield put(getAllSettingKnowledgeBaseSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateSettingEnhancementSaga(action) {
  try {
    yield updateEnhancementSettingAPI(action.payload);
    yield put(getAllSettingKnowledgeBase());
    Notification("success", "Update success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* deleteSettingEnhancementSaga(action) {
  try {
    yield deleteEnhancementSettingAPI(action.payload);
    Notification("success", "Delete setting success");
    yield put(getAllSettingKnowledgeBase());
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
