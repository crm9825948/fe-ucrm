import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  deleteSMSOutgoingAPI,
  getListSMSOutgoingAPI,
  saveSMSOutgoingAPI,
  getSMSOutgoingAPI,
  updateSMSOutgoingAPI,
  testSMSOutgoingAPI,
} from "redux/api/smsOutgoingAPI";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import {
  deleteSMSOutgoing,
  getListSMSOutgoing,
  getListSMSOutgoingSuccess,
  getSMSOutgoingById,
  saveSMSOutgoing,
  setEditSMSOutgoing,
  setShowDrawer,
  updateSMSOutgoing,
  testSMSOutgoing,
  setShowModalTest,
} from "redux/slices/smsOutgoing";
// eslint-disable-next-line
const jwt = require("jsonwebtoken");
export default function* smsOutgoingSaga() {
  yield takeLatest(getListSMSOutgoing.type, getListSMSOutgoingSaga);
  yield takeLatest(saveSMSOutgoing.type, saveSMSOutgoingSaga);
  yield takeLatest(updateSMSOutgoing.type, updateSMSOutgoingSaga);
  yield takeLatest(getSMSOutgoingById.type, getSMSOutgoingByIdSaga);
  yield takeLatest(deleteSMSOutgoing.type, deleteSMSOutgoingSaga);
  yield takeLatest(testSMSOutgoing.type, TestSMSOutgoing);
}

function* getListSMSOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListSMSOutgoingAPI();
    yield put(getListSMSOutgoingSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {}
}

function* saveSMSOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    let payload = {
      ...action.payload,
    };
    // payload.password = jwt.sign({ password: action.payload.password }, "BASEBS", { algorithm: "HS256" })
    yield saveSMSOutgoingAPI(payload);
    yield put(getListSMSOutgoing());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Tạo mới thành công");
    yield put(setShowDrawer(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* getSMSOutgoingByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getSMSOutgoingAPI(action.payload);
    let temp = { ...data.data[0] };
    if (!temp.method) {
      temp = {
        ...temp,
        method: "",
      };
    }
    if (!temp.body_type) {
      temp = {
        ...temp,
        body_type: "",
      };
    }

    if (!temp.params) {
      temp = {
        ...temp,
        params: [],
      };
    }

    if (!temp.body) {
      temp = {
        ...temp,
        body: [],
      };
    }

    yield put(setEditSMSOutgoing(temp));
    yield put(setShowLoadingScreen(false));
    yield put(setShowDrawer(true));
  } catch (error) {}
}

function* deleteSMSOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteSMSOutgoingAPI(action.payload);
    yield put(getListSMSOutgoing());
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Xoá thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateSMSOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateSMSOutgoingAPI(action.payload);
    yield put(getListSMSOutgoing());
    yield put(setShowDrawer(false));
    Notification("success", "Cập nhật thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* TestSMSOutgoing(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield testSMSOutgoingAPI(action.payload);
    yield put(setShowModalTest(false));
    Notification("success", "Success!");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
