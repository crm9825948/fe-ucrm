import { put, takeLatest, select } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadCommentAPI,
  postCommentAPI,
  editCommentAPI,
  deleteCommentAPI,
  likeCommentAPI,
  replyCommentAPI,
  loadReplyCommentAPI,
} from "redux/api/commentAPI";
import {
  loadComment,
  updateLoading,
  loadCommentResult,
  postComment,
  updateListComment,
  editComment,
  deleteComment,
  likeComment,
  replyComment,
  loadReplyComment,
  loadReplyCommentResult,
  updateSort,
} from "redux/slices/comment";

export const userReducer = (state) => state.userReducer;
export const commentReducer = (state) => state.commentReducer;

export default function* campaign() {
  yield takeLatest(loadComment.type, LoadComment);
  yield takeLatest(postComment.type, PostComment);
  yield takeLatest(editComment.type, EditComment);
  yield takeLatest(deleteComment.type, DeleteComment);
  yield takeLatest(likeComment.type, LikeComment);
  yield takeLatest(replyComment.type, ReplyComment);
  yield takeLatest(loadReplyComment.type, LoadReplyComment);
}

function* LoadComment(action) {
  try {
    const { isSort } = yield select(commentReducer);
    if (action.payload.current_page === 1) {
      yield put(updateLoading(true));
    }

    if (isSort) {
      yield put(updateSort(false));
    }

    const response = yield loadCommentAPI(action.payload);
    yield put(
      loadCommentResult({
        data: response.data.data,
        recordID: action.payload.record_id,
      })
    );
  } catch (error) {
    // Notification("error", error.response.data.error);
    yield put(
      loadCommentResult({
        data: { comment: [], total: 0 },
        recordID: action.payload.record_id,
      })
    );
  }
}

function* LoadReplyComment(action) {
  try {
    const response = yield loadReplyCommentAPI(action.payload);
    yield put(loadReplyCommentResult(response.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* PostComment(action) {
  try {
    const { userDetail } = yield select(userReducer);

    const response = yield postCommentAPI(action.payload);
    yield put(
      updateListComment({
        type: "post",
        data: {
          ...response.data.data,
          reply_count: 0,
          likes: 0,
          interacted: 0,
          userDetails: {
            ...userDetail,
          },
        },
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ReplyComment(action) {
  try {
    const { userDetail } = yield select(userReducer);

    const response = yield replyCommentAPI(action.payload);
    yield put(
      updateListComment({
        type: "reply",
        data: {
          ...response.data.data,
          reply_count: 0,
          likes: 0,
          interacted: 0,
          userDetails: {
            ...userDetail,
          },
        },
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* EditComment(action) {
  try {
    yield editCommentAPI(action.payload);
    yield put(
      updateListComment({
        type: "update",
        data: {
          ...action.payload,
        },
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DeleteComment(action) {
  try {
    yield deleteCommentAPI(action.payload);
    yield put(
      updateListComment({
        type: "delete",
        data: {
          ...action.payload,
        },
      })
    );
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LikeComment(action) {
  try {
    const response = yield likeCommentAPI(action.payload);
    yield put(
      updateListComment({
        type: "like",
        data: {
          ...response.data.data,
        },
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
