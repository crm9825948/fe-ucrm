import { put, takeLatest, takeEvery, delay } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadObject } from "redux/slices/objectsManagement";
import { loadObjectFields } from "redux/slices/fieldsManagement";
import { loadAllGroups } from "redux/slices/group";
import { loadAllUser } from "redux/slices/user";
import { loadListObjectField } from "redux/slices/objects";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { getListSMSTemplate } from "redux/slices/smsTemplate";
import { FE_URL } from "constants/constants";

import { loadFieldsAPI } from "redux/api/objectsAPIs";
import { loadValueAPI } from "redux/api/picklistAPI";
import { loadTargetAPI } from "redux/api/objectsAPIs";
import { loadRelatedObjectAPI } from "redux/api/fieldsManagementAPI";

import {
  loadAllWorkflowsAPI,
  activeWorkflowAPI,
  deleteWorkflowAPI,
  createWorkflowAPI,
  detailsWorkflowAPI,
  updateWorkflowAPI,
  loadAllActionsAPI,
  loadUserFieldsAPI,
  createActionAPI,
  loadDetailsActionAPI,
  updateActionAPI,
  activeActionAPI,
  deleteActionWorkflowAPI,
  getLinkingFieldEmailAPI,
  getTargetObjectsAPI,
  getListPopupAPI,
} from "redux/api/workflowsAPI";
import {
  loadDataNecessary,
  loadDataNecessaryEdit,
  setLoading,
  setStatus,
  // loadAllWorkflows,
  loadAllWorkflowsSuccess,
  activeWorkflow,
  activeWorkflowSuccess,
  activeWorkflowFail,
  deleteWorkflow,
  deleteWorkflowSuccess,
  deleteWorkflowFail,
  createWorkflow,
  createWorkflowSuccess,
  createWorkflowFail,
  detailsWorkflow,
  detailsWorkflowSuccess,
  detailsWorkflowFail,
  updateWorkflow,
  loadAllActions,
  loadAllActionsSuccess,
  loadAllActionsFail,
  loadUserFields,
  loadUserFieldsSuccess,
  createAction,
  loadDetailsAction,
  loadDetailsActionSuccess,
  loadDetailsActionFail,
  updateAction,
  activeAction,
  updateStateAction,
  loadFieldsCondition,
  deleteActionWorkflow,
  deleteActionWorkflowSuccess,
  deleteActionWorkflowFail,
  loadLinkingFieldEmailSuccess,
  loadFieldsObject,
  loadFieldsObjectSuccess,
  loadFieldsObjectFail,
  loadFieldsObjectRelated,
  loadFieldsObjectRelatedSuccess,
  loadFieldsObjectRelatedFail,
  loadObjectRelated,
  loadObjectRelatedResult,
  loadFieldsMainObjectSuccess,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
  loadTargetObjects,
  loadTargetObjectsResult,
  getListPopup,
  getListPopupResult,
} from "redux/slices/workflows";

export default function* sharing() {
  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  yield takeLatest(loadDataNecessaryEdit.type, LoadDataNecessaryEdit);
  // yield takeLatest(loadAllWorkflows.type, LoadAllWorkflows);
  yield takeLatest(activeWorkflow.type, ActiveWorkflow);
  yield takeLatest(deleteWorkflow.type, DeleteWorkflow);
  yield takeLatest(createWorkflow.type, CreateWorkflow);
  yield takeLatest(detailsWorkflow.type, DetailsWorkflow);
  yield takeLatest(updateWorkflow.type, UpdateWorkflow);
  yield takeLatest(loadAllActions.type, LoadAllActions);
  yield takeLatest(loadUserFields.type, LoadUserFields);
  yield takeLatest(createAction.type, CreateAction);
  yield takeLatest(loadDetailsAction.type, LoadDetailsAction);
  yield takeLatest(updateAction.type, UpdateAction);
  yield takeLatest(activeAction.type, ActiveAction);
  yield takeLatest(loadFieldsCondition.type, LoadFieldsCondition);
  yield takeLatest(deleteActionWorkflow.type, DeleteActionWorkflow);
  yield takeLatest(loadFieldsObject.type, LoadFieldsObject);
  yield takeLatest(loadObjectRelated.type, LoadObjectRelated);
  yield takeLatest(loadFieldsObjectRelated.type, LoadFieldsObjectRelated);
  yield takeEvery(loadValuePicklist.type, LoadValuePicklist);
  yield takeLatest(loadTargetPicklist.type, LoadTargetPicklist);
  yield takeLatest(loadTargetObjects.type, GetTargetObjects);
  yield takeLatest(getListPopup.type, GetListPopup);
}

function* LoadDataNecessary(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());
    const response = yield loadAllWorkflowsAPI(action.payload);
    yield put(loadAllWorkflowsSuccess(response.data.data));
    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadDataNecessaryEdit(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObject());

    yield put(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );

    yield put(loadObjectFields(action.payload));

    yield put(loadUserFields(action.payload));

    yield put(getListEmailTemplate());

    yield put(getListSMSTemplate());

    yield put(
      loadListObjectField({
        object_id: action.payload.object_id,
        api_version: "2",
        show_meta_fields: true,
      })
    );

    yield put(getListPopup({ object_id: action.payload.object_id }));

    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );

    const email = yield getLinkingFieldEmailAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadLinkingFieldEmailSuccess(email.data.data));

    const fieldMain = yield loadFieldsAPI({
      api_version: "2",
      object_id: action.payload.object_id,
    });

    yield put(loadFieldsMainObjectSuccess(fieldMain.data.data));

    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

// function* LoadAllWorkflows(action) {
//   try {
//     const response = yield loadAllWorkflowsAPI(action.payload);
//     yield put(loadAllWorkflowsSuccess(response.data.data));
//   } catch (error) {
//     yield put(loadAllWorkflowsFail(error.response.data.error));
//   }
// }

function* ActiveWorkflow(action) {
  try {
    const response = yield activeWorkflowAPI(action.payload);
    yield put(activeWorkflowSuccess(response.data.data));
  } catch (error) {
    yield put(activeWorkflowFail(error.response.data.error));
  }
}

function* DeleteWorkflow(action) {
  try {
    const response = yield deleteWorkflowAPI(action.payload);
    yield put(deleteWorkflowSuccess(response.data.data));
  } catch (error) {
    yield put(deleteWorkflowFail(error.response.data.error));
  }
}

function* CreateWorkflow(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield createWorkflowAPI(action.payload);
    yield put(createWorkflowSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(createWorkflowFail(error.response.data.error));
  }
}

function* DetailsWorkflow(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield detailsWorkflowAPI(action.payload);
    yield put(detailsWorkflowSuccess(response.data.data.mess));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(detailsWorkflowFail(error.response.data.error));
  }
}

function* UpdateWorkflow(action) {
  try {
    yield put(
      setLoading({
        type: "update",
        status: true,
      })
    );
    yield updateWorkflowAPI(action.payload);
    yield put(
      setLoading({
        type: "update",
        status: false,
      })
    );
    Notification("success", "Update successfully!");
    yield delay(500);
    window.open(`${FE_URL}/workflows`, "_self");
  } catch (error) {
    yield put(
      setLoading({
        type: "update",
        status: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* LoadAllActions(action) {
  try {
    const response = yield loadAllActionsAPI(action.payload);
    yield put(loadAllActionsSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllActionsFail(error.response.data.error));
  }
}

function* LoadUserFields(action) {
  try {
    const response = yield loadUserFieldsAPI(action.payload);
    yield put(loadUserFieldsSuccess(response.data.data));
  } catch (error) {}
}

function* CreateAction(action) {
  try {
    yield put(
      setLoading({
        type: "modal",
        status: true,
      })
    );
    yield createActionAPI(action.payload);
    yield put(
      setLoading({
        type: "modal",
        status: false,
      })
    );
    Notification("success", "Create successfully!");
    yield put(setStatus("Create action successfully!"));
  } catch (error) {
    yield put(
      setLoading({
        type: "modal",
        status: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsAction(action) {
  try {
    const response = yield loadDetailsActionAPI(action.payload);
    yield put(loadDetailsActionSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsActionFail(error.response.data.error));
  }
}

function* UpdateAction(action) {
  try {
    yield put(
      setLoading({
        type: "modal",
        status: true,
      })
    );
    yield updateActionAPI(action.payload);

    yield put(
      updateStateAction({
        type: "update",
        id: action.payload._id,
        name: action.payload.action_name || action.payload.data.action_name,
      })
    );

    yield put(
      setLoading({
        type: "modal",
        status: false,
      })
    );
    Notification("success", "Update successfully!");
    yield put(setStatus("Update action successfully!"));
  } catch (error) {
    yield put(
      setLoading({
        type: "modal",
        status: false,
      })
    );
    Notification("error", error.response.data.error);
  }
}

function* ActiveAction(action) {
  try {
    yield activeActionAPI(action.payload);
    yield put(
      updateStateAction({
        type: "change-status",
        id: action.payload._id,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsCondition(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadListObjectField(action.payload));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* DeleteActionWorkflow(action) {
  try {
    yield deleteActionWorkflowAPI(action.payload);
    yield put(deleteActionWorkflowSuccess());
    yield put(
      updateStateAction({
        type: "delete",
        id: action.payload._id,
      })
    );
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Delete action successfully!");
  } catch (error) {
    yield put(deleteActionWorkflowFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsObject(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadFieldsAPI(action.payload);
    yield put(loadFieldsObjectSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadFieldsObjectFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadObjectRelated(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadRelatedObjectAPI(action.payload);
    yield put(loadObjectRelatedResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadObjectRelatedResult([]));
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsObjectRelated(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadFieldsAPI(action.payload);
    yield put(loadFieldsObjectRelatedSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadFieldsObjectRelatedFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadValuePicklist(action) {
  try {
    const response = yield loadValueAPI(action.payload.data);
    let newObj = { ...action.payload.listValue };
    let temp = {};
    Object.entries(response.data.data).forEach(([key, value], index) => {
      // if (value) {
      temp[key] = value;
      // }
    });
    newObj[action.payload.ID] = { ...temp };
    yield put(loadValuePicklistSuccess(newObj));
  } catch (error) {
    yield put(loadValuePicklistFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadTargetPicklist(action) {
  try {
    const response = yield loadTargetAPI(action.payload);
    yield put(loadTargetPicklistSuccess(response.data.data));
  } catch (error) {
    yield put(loadTargetPicklistFail());
    Notification("error", error.response.data.error);
  }
}

function* GetTargetObjects(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getTargetObjectsAPI(action.payload);
    yield put(loadTargetObjectsResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadTargetObjectsResult([]));
    Notification("error", error.response.data.error);
  }
}

function* GetListPopup(action) {
  try {
    const response = yield getListPopupAPI(action.payload);
    yield put(getListPopupResult(response.data.data));
  } catch (error) {
    yield put(getListPopupResult([]));
    Notification("error", error.response.data.error);
  }
}
