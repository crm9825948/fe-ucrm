import {
  put,
  // takeEvery,
  takeLatest,
  delay,
  all,
} from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
// import { all } from "redux-saga/effects";
import { loadRecordInteraction } from "redux/slices/consolidatedViewSettings";
import { massDeleteCampaignAPI } from "redux/api/campaignAPI";
import {
  updateStatusTemplateAPI,
  createCustomViewAPI,
  createRecordAPI,
  deleteCustomViewAPI,
  deleteRecordAPI,
  downloadFileAPI,
  getRecordsAPI,
  loadCustomViewAPI,
  loadDataAPI,
  loadDetailCustomViewAPI,
  loadFavouriteObjectsAPI,
  loadFieldsAPI,
  loadFormCreateAPI,
  loadHeaderAPI,
  loadLinkingFieldValueAPI,
  loadPaginationAPI,
  loadRecordDataAPI,
  loadSourceObjectsAPI,
  loadTargetAPI,
  loadTargetObjectsAPI,
  loadUserAPI,
  loadUserAssignToAPI,
  massDeleteRecordAPI,
  massEditAPI,
  mergeRecordAPI,
  toggleFavouriteObjectAPI,
  updateCustomViewAPI,
  updateRecordAPI,
  uploadFileAndMappingAPI,
  viewLogsAPI,
  loadListViewWithDetailsAPI,
  updateListViewDetailAPI,
  recycleBinLoadListViewAPI,
  restoreRecordAPI,
  deleteMultipleRecord,
  emptyTrashAPI,
  manualSharingAPI,
  createTemplateAPI,
  loadTemplateAPI,
  updateTemplateAPI,
  makeDefaultTemplateAPI,
  deleteTemplateAPI,
  viewObjectSearchAPI,
  globalSearchAPI,
  shareViewAPI,
  loadPermissionAPI,
  updateDefaultViewAPI,
} from "redux/api/objectsAPIs";
import { setShowLoadingScreen } from "redux/slices/global";
import { getDataKanbanView } from "redux/slices/kanbanView";
import {
  makeDefaultTemplate,
  updateStatusTemplate,
  restoreRecord,
  restoreRecordSuccess,
  restoreRecordFail,
  createCustomView,
  createCustomViewFail,
  createCustomViewSuccess,
  createRecord,
  createRecordFail,
  createRecordSuccess,
  deleteCustomView,
  deleteCustomViewFail,
  deleteCustomViewSuccess,
  deleteRecord,
  deleteRecordFail,
  deleteRecordSuccess,
  exportTemplate,
  getRecords,
  getRecordsFail,
  getRecordsSuccess,
  loadAllData,
  loadAllDataFail,
  loadAllDataSuccess,
  loadCustomView,
  loadCustomViewFail,
  loadCustomViewSuccess,
  loadData,
  loadDataFail,
  loadDataFailLinking,
  loadDataLinking,
  loadDataSuccess,
  loadDataSuccessLinking,
  loadDetailsCustomView,
  loadDetailsCustomViewFail,
  loadDetailsCustomViewSuccess,
  loadFavouriteObjects,
  loadFavouriteObjectsFail,
  loadFavouriteObjectsSuccess,
  loadFormCreate,
  loadFormCreateFail,
  loadFormCreateSuccess,
  loadHeader,
  loadHeaderFail,
  loadHeaderFailLinking,
  loadHeaderLinking,
  loadHeaderSuccess,
  loadHeaderSuccessLinking,
  loadLinkingFieldValue,
  loadLinkingFieldValueFail,
  loadLinkingFieldValueSuccess,
  loadListObjectField,
  loadListObjectFieldSuccess,
  loadPagination,
  loadPaginationFail,
  loadPaginationFailLinking,
  loadPaginationLinking,
  loadPaginationSuccess,
  loadPaginationSuccessLinking,
  loadRecordData,
  loadRecordDataFail,
  loadRecordDataSuccess,
  loadSourceObjects,
  loadSourceObjectsFail,
  loadSourceObjectsSuccess,
  loadTarget,
  loadTargetFail,
  loadTargetObjects,
  loadTargetObjectsFail,
  loadTargetObjectsSuccess,
  loadTargetSucces,
  loadUser,
  loadUserAssignTo,
  loadUserAssignToFail,
  loadUserAssignToSuccess,
  loadUserFail,
  loadUserMore,
  loadUserMoreSuccess,
  loadUserSuccess,
  mappingFieldAndUpdate,
  mappingFieldAndUpdateFail,
  mappingFieldAndUpdateSuccess,
  massDelete,
  massDeleteFail,
  massDeleteSuccess,
  massEdit,
  massEditFail,
  massEditSuccess,
  mergeRecord,
  mergeRecordFail,
  mergeRecordSuccess,
  toggleFavouriteObject,
  toggleFavouriteObjectFail,
  toggleFavouriteObjectSuccess,
  updateCustomView,
  updateCustomViewFail,
  updateCustomViewSuccess,
  updateRecord,
  updateRecordFail,
  updateRecordSuccess,
  viewLogs,
  viewLogsFail,
  viewLogsSuccess,
  loadListViewWithDetails,
  loadListViewWithDetailsSuccess,
  loadListViewWithDetailsFail,
  updateListViewDetails,
  updateListViewDetailsFail,
  updateListViewDetailsSuccess,
  loadDataListViewRecycleBin,
  loadDataListViewRecycleBinFail,
  loadDataListViewRecycleBinSuccess,
  deleteMultiple,
  deleteMultipleFail,
  deleteMultipleSuccess,
  emptyTrash,
  setIsOpenDuplicate,
  setListDuplicate,
  loadDataWithOutPagi,
  loadDataWithOutPagiFail,
  loadDataWithOutPagiSuccess,
  getManualSharingSuccess,
  getManualSharing,
  createTemplate,
  createTemplateFail,
  createTemplateSuccess,
  loadTemplateSuccess,
  loadTemplateFail,
  loadTemplate,
  updateTemplate,
  updateTemplateSuccess,
  updateTemplateUpdateFail,
  deleteTemplate,
  deleteTemplateSuccess,
  deleteTemplateFail,
  viewObjectSearch,
  viewObjectSearchSuccess,
  globalSearch,
  viewObjectLinkingSearchSuccess,
  viewObjectLinkingSearch,
  linkingGlobalSearch,
  updateLoadingLinking,
  updateLoadingData,
  updateIsSearchGlobal,
  shareView,
  loadPermissionSucces,
  loadPermission,
  updateDefaultView,
} from "redux/slices/objects";
import fileDownload from "js-file-download";
// import { FE_URL } from "constants/constants";

export default function* objects() {
  yield takeLatest(loadFavouriteObjects.type, LoadFavouriteObjects);
  yield takeLatest(toggleFavouriteObject.type, ToggleFavouriteObject);
  yield takeLatest(loadFormCreate.type, LoadFormCreate);
  yield takeLatest(loadUserAssignTo.type, LoadUserAssignTo);
  yield takeLatest(createRecord.type, CreateRecord);
  yield takeLatest(loadHeader.type, LoadHeader);
  yield takeLatest(loadData.type, LoadData);
  yield takeLatest(loadPagination.type, LoadPagination);
  yield takeLatest(loadUser.type, LoadUser);
  yield takeLatest(loadLinkingFieldValue.type, LoadLinkingFieldValue);
  yield takeLatest(loadDataLinking.type, LoadDataLinking);
  yield takeLatest(loadHeaderLinking.type, LoadHeaderLinking);
  yield takeLatest(loadPaginationLinking.type, LoadPaginationLinking);
  yield takeLatest(loadRecordData.type, LoadRecordData);
  yield takeLatest(updateRecord.type, UpdateRecord);
  yield takeLatest(deleteRecord.type, DeleteRecord);
  yield takeLatest(massDelete.type, MassDeleteRecord);
  yield takeLatest(loadTarget.type, LoadTargetAPI);
  yield takeLatest(massEdit.type, MassEdit);
  yield takeLatest(getRecords.type, GetRecords);
  yield takeLatest(mergeRecord.type, MergeRecord);
  yield takeLatest(loadListObjectField.type, loadListObjectFieldSaga);
  yield takeLatest(createCustomView.type, CreateCustomView);
  yield takeLatest(loadCustomView.type, LoadCustomView);
  yield takeLatest(loadDetailsCustomView.type, LoadCustomViewDetails);
  yield takeLatest(updateCustomView.type, UpdateCustomView);
  yield takeLatest(deleteCustomView.type, DeleteCustomView);
  yield takeLatest(loadAllData.type, LoadAllData);
  yield takeLatest(loadDataWithOutPagi.type, LoadAllDataWithOutPagi);
  yield takeLatest(loadSourceObjects.type, LoadSourceObjects);
  yield takeLatest(loadTargetObjects.type, LoadTargetObjects);
  yield takeLatest(loadUserMore.type, LoadUserMore);
  yield takeLatest(mappingFieldAndUpdate.type, UploadFileAndMapping);
  yield takeLatest(exportTemplate.type, DownloadFile);
  yield takeLatest(viewLogs.type, ViewLogs);
  yield takeLatest(loadListViewWithDetails.type, LoadListViewWithDetails);
  yield takeLatest(updateListViewDetails.type, UpdateListViewWithDetails);
  yield takeLatest(loadDataListViewRecycleBin.type, LoadDataListViewRecycleBin);
  yield takeLatest(restoreRecord.type, RestoreRecord);
  yield takeLatest(deleteMultiple.type, DeleteMultiple);
  yield takeLatest(emptyTrash.type, EmptyTrash);
  yield takeLatest(getManualSharing.type, GetManualSharing);
  yield takeLatest(createTemplate.type, CreateTemplateRecord);
  yield takeLatest(updateTemplate.type, UpdateTemplateRecord);
  yield takeLatest(loadTemplate.type, LoadTemplate);
  yield takeLatest(updateStatusTemplate.type, UpdateTemplateStatus);
  yield takeLatest(makeDefaultTemplate.type, MakeDefaultTemplate);
  yield takeLatest(deleteTemplate.type, DeleteTemplate);
  yield takeLatest(viewObjectSearch.type, ViewObjectSearchSaga);
  yield takeLatest(globalSearch.type, GlobalSearchSaga);
  yield takeLatest(viewObjectLinkingSearch.type, ViewObjectLinkingSearchSaga);
  yield takeLatest(linkingGlobalSearch.type, LinkingGlobalSearchSaga);
  yield takeLatest(shareView.type, shareViewSaga);
  yield takeLatest(loadPermission.type, loadPermissionSaga);
  yield takeLatest(updateDefaultView.type, updateDefaultViewSaga);
}

function* LoadFavouriteObjects(action) {
  try {
    const response = yield loadFavouriteObjectsAPI({});
    yield put(loadFavouriteObjectsSuccess(response.data.data));
  } catch (error) {
    yield put(loadFavouriteObjectsFail());
  }
}

function* ToggleFavouriteObject(action) {
  try {
    /*eslint-disable-next-line*/
    yield toggleFavouriteObjectAPI(action.payload);
    Notification("success", "Successfully!");
    yield put(toggleFavouriteObjectSuccess());
    yield put(loadFavouriteObjects());
  } catch (error) {
    yield put(toggleFavouriteObjectFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadFormCreate(action) {
  try {
    const response = yield loadFormCreateAPI(action.payload);

    let hiddenArray = [];
    /*eslint-disable-next-line*/
    response.data.data.forEach((sections, idx) => {
      /*eslint-disable-next-line*/
      sections.fields.forEach((item, idx) => {
        if (item.type === "dynamic-field") {
          /*eslint-disable-next-line*/
          if (item.list_items)
            /*eslint-disable-next-line*/
            Object.entries(item.list_items).forEach(([key, value], idx) => {
              hiddenArray = [...hiddenArray, ...value];
            });
        }
      });
    });

    let tempField = [];
    let tempFieldHidden = [];
    /*eslint-disable-next-line*/
    response.data.data.forEach((sections, idx) => {
      // tempField.push(sections);
      tempField[idx] = { ...sections };
      tempField[idx].fields = [];

      tempFieldHidden[idx] = { ...sections };
      tempFieldHidden[idx].fields = [];
      /*eslint-disable-next-line*/
      sections.fields.forEach((item, index) => {
        if (hiddenArray.findIndex((ele) => ele === item._id) < 0) {
          tempField[idx].fields.push(item);
        }
        tempFieldHidden[idx].fields.push(item);
      });
    });

    yield put(
      loadFormCreateSuccess({
        data: tempFieldHidden,
        hiddenFields: tempField,
        hiddenDynamic: hiddenArray,
      })
    );
  } catch (error) {
    yield put(loadFormCreateFail());
  }
}

function* LoadUserAssignTo(action) {
  try {
    const response = yield loadUserAssignToAPI(action.payload);
    yield put(loadUserAssignToSuccess(response.data.data));
  } catch (error) {
    yield put(loadUserAssignToFail());
  }
}

function* CreateRecord(action) {
  try {
    yield createRecordAPI(action.payload.data);
    Notification("success", "Create successfully!");
    yield put(createRecordSuccess());

    if (action.payload.data.interaction_data) {
      yield put(loadRecordInteraction(action.payload.loadInteraction));
    }

    if (action.payload.load) {
      const responseData = yield loadDataAPI(action.payload.load);
      yield put(updateIsSearchGlobal(false));
      // const responsePagi = yield loadPaginationAPI(action.payload.load);
      if (action.payload && action.payload.type === "reload") {
      } else {
        yield put(loadDataSuccess(responseData.data.data));
        // yield put(loadPaginationSuccess(responsePagi.data.data));
      }
    }
    if (action.payload.kanban) {
      yield put(getDataKanbanView(action.payload.kanban));
    }
    if (action.payload.navigate) {
      yield action.payload.navigate(
        "/knowledge-base-view?object_id=" + action.payload.data.object_id
      );
    }
    // }
  } catch (error) {
    if (error.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(error.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
    } else {
      Notification("error", error.response.data.error);
      yield put(createRecordFail());
    }
  }
}

function* LoadHeader(action) {
  try {
    const response = yield loadHeaderAPI(action.payload);
    yield put(loadHeaderSuccess(response.data.data));
  } catch (error) {
    yield put(loadHeaderFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadHeaderLinking(action) {
  try {
    const response = yield loadHeaderAPI(action.payload);
    yield put(loadHeaderSuccessLinking(response.data.data));
  } catch (error) {
    yield put(loadHeaderFailLinking());
    Notification("error", error.response.data.error);
  }
}

function* LoadData(action) {
  try {
    const response = yield loadDataAPI(action.payload);
    yield put(updateIsSearchGlobal(false));
    yield put(loadDataSuccess(response.data.data));
  } catch (error) {
    yield put(loadDataFail());
  }
}

function* LoadDataLinking(action) {
  try {
    const response = yield loadDataAPI(action.payload);
    yield put(loadDataSuccessLinking(response.data.data));
  } catch (error) {
    yield put(loadDataFailLinking());
  }
}

function* LoadPagination(action) {
  try {
    const response = yield loadPaginationAPI(action.payload);
    yield put(loadPaginationSuccess(response.data.data));
  } catch (error) {
    yield put(loadPaginationFail());
  }
}

function* LoadPaginationLinking(action) {
  try {
    const response = yield loadPaginationAPI(action.payload);
    yield put(loadPaginationSuccessLinking(response.data.data));
  } catch (error) {
    yield put(loadPaginationFailLinking());
  }
}

function* LoadUser(action) {
  try {
    const response = yield loadUserAPI(action.payload);
    yield put(loadUserSuccess(response.data.data));
  } catch (error) {
    yield put(loadUserFail());
  }
}

function* LoadLinkingFieldValue(action) {
  try {
    const response = yield loadLinkingFieldValueAPI(action.payload);
    yield put(loadLinkingFieldValueSuccess(response.data.data));
  } catch (error) {
    yield put(loadLinkingFieldValueFail());
  }
}

function* LoadRecordData(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadRecordDataAPI(action.payload);
    yield put(loadRecordDataSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadRecordDataFail());
  }
}

function* UpdateRecord(action) {
  try {
    yield updateRecordAPI(action.payload.data);
    yield put(updateRecordSuccess());

    if (
      (action.payload && action.payload.type === "reload") ||
      (action.payload && action.payload.type === "no-reload")
    ) {
    } else {
      const responseData = yield loadDataAPI(action.payload.load);
      yield put(updateIsSearchGlobal(false));
      // const responsePagi = yield loadPaginationAPI(action.payload.load);
      yield put(loadDataSuccess(responseData.data.data));
      // yield put(loadPaginationSuccess(responsePagi.data.data));
    }

    if (action.payload.navigate) {
      yield action.payload.navigate(
        "/knowledge-base-view?object_id=" + action.payload.data.object_id
      );
    }
    Notification("success", "Update successfully!");
    // }
  } catch (error) {
    if (error.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(error.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
    } else {
      yield put(updateRecordFail());
      Notification("error", error.response.data.error);
    }
  }
}

function* DeleteRecord(action) {
  try {
    yield deleteRecordAPI(action.payload.data);
    yield put(deleteRecordSuccess());
    const responseData = yield loadDataAPI(action.payload.load);
    yield put(updateIsSearchGlobal(false));
    // const responsePagi = yield loadPaginationAPI(action.payload.load);
    if (action.payload && action.payload.type === "reload") {
    } else {
      yield put(loadDataSuccess(responseData.data.data));
      // yield put(loadPaginationSuccess(responsePagi.data.data));
    }

    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(deleteRecordFail());
    Notification("error", error.response.data.error);
  }
}

function* MassDeleteRecord(action) {
  try {
    if (action.payload.type === "campaign") {
      yield massDeleteCampaignAPI({
        campaign_ids: [...action.payload.data.ids],
      });
    } else {
      yield massDeleteRecordAPI(action.payload.data);
    }
    yield put(massDeleteSuccess());
    const responseData = yield loadDataAPI(action.payload.load);
    yield put(updateIsSearchGlobal(false));
    // const responsePagi = yield loadPaginationAPI(action.payload.load);
    yield put(loadDataSuccess(responseData.data.data));
    // yield put(loadPaginationSuccess(responsePagi.data.data));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(massDeleteFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadTargetAPI(action) {
  try {
    const response = yield loadTargetAPI(action.payload);
    yield put(loadTargetSucces(response.data.data));
  } catch (error) {
    yield put(loadTargetFail());
  }
}

function* MassEdit(action) {
  try {
    yield massEditAPI(action.payload.data);
    yield put(massEditSuccess());
    const responseData = yield loadDataAPI(action.payload.methodData.load);
    yield put(updateIsSearchGlobal(false));
    // const responsePagi = yield loadPaginationAPI(
    //   action.payload.methodData.load
    // );
    yield put(loadDataSuccess(responseData.data.data));
    // yield put(loadPaginationSuccess(responsePagi.data.data));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(massEditFail());
    Notification("error", error.response.data.error);
  }
}

function* GetRecords(action) {
  try {
    const response = yield getRecordsAPI(action.payload);
    yield put(getRecordsSuccess(response.data.data));
  } catch (error) {
    yield put(getRecordsFail());
  }
}

function* MergeRecord(action) {
  try {
    yield mergeRecordAPI(action.payload.data);
    yield put(mergeRecordSuccess());
    const responseData = yield loadDataAPI(action.payload.methodData.load);
    yield put(updateIsSearchGlobal(false));
    // const responsePagi = yield loadPaginationAPI(
    //   action.payload.methodData.load
    // );
    yield put(loadDataSuccess(responseData.data.data));
    // yield put(loadPaginationSuccess(responsePagi.data.data));
    Notification("success", "Update successfully!");
  } catch (error) {
    if (error.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(error.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
    } else {
      yield put(mergeRecordFail());
      Notification("error", error.response.data.error);
    }
  }
}

function* loadListObjectFieldSaga(action) {
  try {
    if (!window.location.pathname.includes("/dashboard")) {
      yield put(setShowLoadingScreen(true));
    }

    const { data } = yield loadFieldsAPI(action.payload);
    yield put(loadListObjectFieldSuccess(data.data));

    if (!window.location.pathname.includes("/dashboard")) {
      yield put(setShowLoadingScreen(false));
    }
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* CreateCustomView(action) {
  try {
    yield createCustomViewAPI(action.payload.data);
    yield put(createCustomViewSuccess());
    Notification(
      "success",
      action.payload.clone ? "Clone successfully!" : "Create successfully!"
    );

    yield put(loadCustomView(action.payload.load));
  } catch (error) {
    yield put(createCustomViewFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadCustomView(action) {
  try {
    const response = yield loadCustomViewAPI(action.payload);
    yield put(loadCustomViewSuccess(response.data.data));
  } catch (error) {
    yield put(loadCustomViewFail());
    // Notification("warning", error.response.data.error);
  }
}

function* LoadCustomViewDetails(action) {
  try {
    const response = yield loadDetailCustomViewAPI(action.payload);
    yield put(loadDetailsCustomViewSuccess(response.data.data));
  } catch (error) {
    yield put(loadDetailsCustomViewFail());
  }
}

function* UpdateCustomView(action) {
  try {
    yield updateCustomViewAPI(action.payload.data);
    yield put(updateCustomViewSuccess());
    Notification("success", "Update successfully!");

    yield put(loadCustomView(action.payload.load));
  } catch (error) {
    yield put(updateCustomViewFail());
    Notification("error", error.response.data.error);
  }
}

function* DeleteCustomView(action) {
  try {
    yield deleteCustomViewAPI(action.payload.data);
    yield put(deleteCustomViewSuccess());

    Notification("success", "Delete successfully!");
    yield put(loadCustomView(action.payload.load));
  } catch (error) {
    yield put(deleteCustomViewFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadAllData(action) {
  yield put(setShowLoadingScreen(true));
  try {
    const [response, responseData, responsePagi] = yield all([
      loadHeaderAPI(action.payload.object_id),
      loadDataAPI(action.payload.data),
      loadPaginationAPI(action.payload.data),
    ]);

    // const response = yield loadHeaderAPI(action.payload.object_id);
    yield put(loadHeaderSuccess(response.data.data));
    // const responseData = yield loadDataAPI(action.payload.data);

    yield put(loadDataSuccess(responseData.data.data));
    yield put(setShowLoadingScreen(false));
    // const responsePagi = yield loadPaginationAPI(action.payload.data);
    yield put(loadPaginationSuccess(responsePagi.data.data));

    // try {
    //   const response = yield loadHeaderAPI(action.payload.object_id);
    //   yield put(loadHeaderSuccess(response.data.data));
    // } catch (error) {
    //   yield put(loadHeaderFail());
    //   Notification("error", error.response.data.error);
    // }

    // try {
    //   const response = yield loadDataAPI(action.payload.data);
    //   yield put(loadDataSuccess(response.data.data));
    // } catch (error) {
    //   yield put(loadDataFail());
    // }

    // try {
    //   const response = yield loadPaginationAPI(action.payload.data);
    //   yield put(loadPaginationSuccess(response.data.data));
    // } catch (error) {
    //   yield put(loadPaginationFail());
    // }

    // yield put(loadDataSuccess(data.data.data));
    // yield put(loadPaginationSuccess(pagi.data.data));
    yield put(loadAllDataSuccess());
  } catch (error) {
    // window.open(FE_URL + "/500", "_self");
    yield put(loadAllDataFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadAllDataWithOutPagi(action) {
  yield put(setShowLoadingScreen(true));
  try {
    const [response, responseData] = yield all([
      loadHeaderAPI(action.payload.object_id),
      loadDataAPI(action.payload.data),
    ]);
    yield put(updateIsSearchGlobal(false));
    yield put(setShowLoadingScreen(false));
    // const response = yield loadHeaderAPI(action.payload.object_id);
    yield put(loadHeaderSuccess(response.data.data));
    // const responseData = yield loadDataAPI(action.payload.data);
    if (Array.isArray(responseData.data.data)) {
      yield put(loadDataSuccess(responseData.data.data));
    } else {
      yield put(loadDataSuccess([]));
    }

    // const responsePagi = yield loadPaginationAPI(action.payload.data);
    yield put(loadDataWithOutPagiSuccess());
  } catch (error) {
    // window.open(FE_URL + "/500", "_self");
    yield put(loadDataWithOutPagiFail());
    yield put(setShowLoadingScreen(false));
  }
}

function* LoadSourceObjects(action) {
  try {
    const response = yield loadSourceObjectsAPI(action.payload);
    yield put(loadSourceObjectsSuccess(response.data.data));
  } catch (error) {
    yield put(loadSourceObjectsFail());
  }
}

function* LoadTargetObjects(action) {
  try {
    const response = yield loadTargetObjectsAPI(action.payload);
    yield put(loadTargetObjectsSuccess(response.data.data));
  } catch (error) {
    yield put(loadTargetObjectsFail());
  }
}

function* LoadUserMore(action) {
  try {
    const { data } = yield loadUserAPI(action.payload);
    yield put(loadUserMoreSuccess(data.data.records));
  } catch (error) {}
}

function* UploadFileAndMapping(action) {
  try {
    yield delay(2000);
    yield uploadFileAndMappingAPI(action.payload);
    yield put(mappingFieldAndUpdateSuccess());
    Notification(
      "warning",
      "This file is being processed. We will notify you when it completed!!"
    );
  } catch (error) {
    Notification(
      "error",
      error.response.data.error[Object.keys(error.response.data.error)[0]]
    );
    yield put(mappingFieldAndUpdateFail());
  }
}

function* DownloadFile(action) {
  try {
    const response = yield downloadFileAPI(action.payload.data);
    Notification("success", "Download successfully!");
    fileDownload(response.data, `${action.payload.name}.xlsx`);
  } catch (error) {
    error.response.data.text().then((err) => {
      Notification("error", JSON.parse(err).error);
    });
  }
}

function* ViewLogs(action) {
  try {
    const response = yield viewLogsAPI(action.payload);
    yield put(viewLogsSuccess(response.data.data));
  } catch (error) {
    yield put(viewLogsFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadListViewWithDetails(action) {
  try {
    const response = yield loadListViewWithDetailsAPI(action.payload);
    yield put(loadListViewWithDetailsSuccess(response.data.data));
  } catch (error) {
    yield put(loadListViewWithDetailsFail());
  }
}

function* UpdateListViewWithDetails(action) {
  try {
    yield updateListViewDetailAPI(action.payload);
    yield put(updateListViewDetailsSuccess());
    Notification("success", "Succcessfully!");

    const response = yield loadListViewWithDetailsAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadListViewWithDetailsSuccess(response.data.data));
  } catch (error) {
    yield put(updateListViewDetailsFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadDataListViewRecycleBin(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield recycleBinLoadListViewAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    yield put(loadDataListViewRecycleBinSuccess(response.data.data));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadDataListViewRecycleBinFail());
  }
}

function* RestoreRecord(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield restoreRecordAPI(action.payload.data);
    yield put(restoreRecordSuccess());
    const response = yield recycleBinLoadListViewAPI(action.payload.load);
    yield put(loadDataListViewRecycleBinSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
    // }
  } catch (err) {
    if (err.response.data.error.includes("Record already exists in")) {
      yield put(setListDuplicate(err.response.data.data));
      yield put(setIsOpenDuplicate(true));
      yield put(createRecordFail());
      yield put(setShowLoadingScreen(false));
    } else {
      yield put(setShowLoadingScreen(false));
      yield put(restoreRecordFail());
      yield put(loadDataListViewRecycleBinFail());
      Notification("error", err.response.data.error);
    }
  }
}

function* DeleteMultiple(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteMultipleRecord(action.payload.data);
    yield put(deleteMultipleSuccess());
    const response = yield recycleBinLoadListViewAPI(action.payload.load);
    yield put(loadDataListViewRecycleBinSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    yield put(restoreRecordFail());
    yield put(deleteMultipleFail());
    Notification("error", err.response.data.error);
  }
}

function* EmptyTrash(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield emptyTrashAPI(action.payload.data);

    const response = yield recycleBinLoadListViewAPI(action.payload.load);
    yield put(loadDataListViewRecycleBinSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (err) {
    yield put(setShowLoadingScreen(false));
    Notification("error", err.response.data.error);
  }
}

function* GetManualSharing(action) {
  try {
    const { data } = yield manualSharingAPI(action.payload);
    yield put(getManualSharingSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* CreateTemplateRecord(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createTemplateAPI(action.payload);

    const response = yield loadTemplateAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadTemplateSuccess(response.data.data));

    yield put(createTemplateSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(createTemplateFail());
    Notification("error", error.response.data.error);
  }
}
function* UpdateTemplateRecord(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateTemplateAPI(action.payload);

    const response = yield loadTemplateAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadTemplateSuccess(response.data.data));

    yield put(updateTemplateSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(updateTemplateUpdateFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateTemplateStatus(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateStatusTemplateAPI(action.payload);

    const response = yield loadTemplateAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadTemplateSuccess(response.data.data));

    // yield put(updateTemplateSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(updateTemplateUpdateFail());
    Notification("error", error.response.data.error);
  }
}

function* DeleteTemplate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteTemplateAPI(action.payload);

    const response = yield loadTemplateAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadTemplateSuccess(response.data.data));

    yield put(deleteTemplateSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (error) {
    yield put(deleteTemplateFail());
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* MakeDefaultTemplate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield makeDefaultTemplateAPI(action.payload);

    const response = yield loadTemplateAPI({
      object_id: action.payload.object_id,
    });
    yield put(loadTemplateSuccess(response.data.data));

    // yield put(updateTemplateSuccess());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Succcessfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadTemplate(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadTemplateAPI(action.payload);
    yield put(loadTemplateSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadTemplateFail());
    Notification("error", error.response.data.error);
  }
}

function* ViewObjectSearchSaga(action) {
  try {
    const { data } = yield viewObjectSearchAPI(action.payload);
    yield put(viewObjectSearchSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* ViewObjectLinkingSearchSaga(action) {
  try {
    const { data } = yield viewObjectSearchAPI(action.payload);
    yield put(viewObjectLinkingSearchSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* GlobalSearchSaga(action) {
  try {
    yield put(updateLoadingData(true));
    const { data } = yield globalSearchAPI(action.payload);
    yield put(loadDataSuccess(data.data));
    yield put(updateLoadingData(false));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* LinkingGlobalSearchSaga(action) {
  try {
    yield put(updateLoadingLinking());
    const { data } = yield globalSearchAPI(action.payload);
    yield put(loadDataSuccessLinking(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* shareViewSaga(action) {
  const { dataShare, dataLoad } = action.payload;
  try {
    yield shareViewAPI(dataShare);
    Notification("success", "Share success");
    yield put(loadCustomView(dataLoad));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* loadPermissionSaga(action) {
  try {
    const { data } = yield loadPermissionAPI(action.payload);
    yield put(loadPermissionSucces(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateDefaultViewSaga(action) {
  const { dataUpdate, dataLoad } = action.payload;
  try {
    yield updateDefaultViewAPI(dataUpdate);
    Notification("success", "Update success");
    yield put(loadCustomView(dataLoad));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
