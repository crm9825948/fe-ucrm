import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadObject } from "redux/slices/objectsManagement";
import { Notification } from "components/Notification/Noti";

import {
  createSettingAPI,
  loadAllSettingAPI,
  changeStatusAPI,
  deleteSettingAPI,
  updateSettingAPI,
} from "redux/api/hightlightSettingAPI";

import {
  loadDataNecessary,
  showHighlightSetting,
  loadAllSetting,
  loadAllSettingResult,
  createSetting,
  changeStatus,
  changeState,
  deleteSetting,
  updateSetting,
} from "redux/slices/highlightSetting";

export default function* group() {
  yield takeLatest(loadDataNecessary.type, LoadDataNecessary);
  yield takeLatest(loadAllSetting.type, LoadAllSetting);
  yield takeLatest(createSetting.type, CreateSetting);
  yield takeLatest(changeStatus.type, ChangeStatus);
  yield takeLatest(deleteSetting.type, DeleteSetting);
  yield takeLatest(updateSetting.type, UpdateSetting);
}

function* LoadDataNecessary(action) {
  try {
    yield put(loadObject());
  } catch (error) {}
}

function* LoadAllSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadAllSettingAPI(action.payload);
    yield put(loadAllSettingResult(response.data.data));
    yield delay(800);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createSettingAPI(action.payload);
    yield put(
      loadAllSetting({
        current_page: 1,
        record_per_page: 10,
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(showHighlightSetting(false));
    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateSettingAPI(action.payload);
    yield put(
      loadAllSetting({
        current_page: 1,
        record_per_page: 10,
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(showHighlightSetting(false));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ChangeStatus(action) {
  try {
    yield changeStatusAPI(action.payload);
    yield put(
      changeState({
        type: "change-status",
        id: action.payload.id,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DeleteSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteSettingAPI(action.payload);
    yield put(
      changeState({
        type: "delete",
        id: action.payload.id,
      })
    );
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
