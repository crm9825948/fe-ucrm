import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import { loadValueAPI } from "redux/api/picklistAPI";
import { loadTargetAPI } from "redux/api/objectsAPIs";

import {
  deleteRuleById,
  deleteRuleByIdSuccess,
  getListRule,
  getListRuleSuccess,
  getRuleById,
  saveRule,
  setEditing,
  setEditRule,
  updateEditRuleIncoming,
  // updateNormalField,
  updatePositionRule,
  // updateRequiredField,
  updateRule,
  updateStatusRule,
  setIsCreate,
  // updateReplyFields,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
  loadValuePicklistReply,
  loadValuePicklistReplySuccess,
  loadValuePicklistReplyFail,
  loadTargetPicklistReply,
  loadTargetPicklistReplySuccess,
  loadTargetPicklistReplyFail,
} from "redux/slices/ruleIncoming";
import {
  deleteRuleByIdAPI,
  getListRuleIncomingAPI,
  getRuleByIdAPI,
  saveRuleAPI,
  updatePositionRuleAPI,
  updateRuleAPI,
  updateStatusRuleAPI,
} from "redux/api/ruleIncomingAPI";
export default function* ruleIncomingSaga() {
  yield takeLatest(getListRule.type, getListRuleSaga);
  yield takeLatest(saveRule.type, saveRuleSaga);
  yield takeLatest(updateRule.type, updateRuleSaga);
  yield takeLatest(getRuleById.type, getRuleByIdSaga);
  yield takeLatest(deleteRuleById.type, deleteRuleByIdSaga);
  yield takeLatest(updatePositionRule.type, updatePositionRuleSaga);
  yield takeLatest(updateStatusRule.type, updateStatusRuleSaga);
  yield takeLatest(loadValuePicklist.type, LoadValuePicklist);
  yield takeLatest(loadTargetPicklist.type, LoadTargetPicklist);
  yield takeLatest(loadValuePicklistReply.type, LoadValuePicklistReply);
  yield takeLatest(loadTargetPicklistReply.type, LoadTargetPicklistReply);
}
export const ruleIncomingReducer = (state) => state.ruleIncomingReducer;
export const objectsReducer = (state) => state.objectsReducer;

function* getListRuleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListRuleIncomingAPI(action.payload);
    data.data.sort(function (a, b) {
      return a.position - b.position;
    });
    yield put(getListRuleSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* saveRuleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield saveRuleAPI(action.payload);
    yield put(updateEditRuleIncoming({}));
    yield put(
      getListRule({
        id_mailbox: action.payload.id_mailbox,
      })
    );

    const { data } = yield getListRuleIncomingAPI(action.payload);
    data.data.sort(function (a, b) {
      return a.position - b.position;
    });

    yield put(
      getRuleById({
        _id: data.data[data.data.length - 1]._id,
      })
    );

    Notification("success", "Create successfully!");
    yield put(setEditing(false));
    yield put(setIsCreate(false));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* getRuleByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getRuleByIdAPI(action.payload);
    // const { listRequiredField } = yield select(ruleIncomingReducer);
    // const { listObjectField } = yield select(objectsReducer);

    // let arrReq = [];
    // let arrNor = [];
    // let arrReply = [];
    // // eslint-disable-next-line
    // listRequiredField.map((item) => {
    //   if (
    //     data.data.mapping.filter((x) => x.key_object === item.key_object)
    //       .length > 0
    //   ) {
    //     let t = {
    //       ...data.data.mapping.filter(
    //         (x) => x.key_object === item.key_object
    //       )[0],
    //     };
    //     t.related_object_id = item.related_object_id;
    //     t.field_linking = item.field_linking;
    //     arrReq.push(t);
    //   } else {
    //     arrReq.push({
    //       key_object: item.key_object,
    //       value: "",
    //       type: item.type,
    //       field_linking: item.field_linking,
    //       related_object_id: item.related_object_id
    //         ? item.related_object_id
    //         : "",
    //     });
    //   }
    // });

    // // eslint-disable-next-line
    // listObjectField.map((item) => {
    //   if (item.main_object) {
    //     // eslint-disable-next-line
    //     item.main_object.sections.map((section) => {
    //       // eslint-disable-next-line
    //       section.fields.map((field) => {
    //         if (
    //           data.data.mapping_reply.filter(
    //             (x) => x.key_object === field.field_id
    //           ).length > 0
    //         ) {
    //           let t = {
    //             ...data.data.mapping_reply.filter(
    //               (x) => x.key_object === field.field_id
    //             )[0],
    //           };
    //           t.related_object_id = field.objectname || "";
    //           t.field_linking = field.field;
    //           arrReply.push(t);
    //         }
    //       });
    //     });
    //   }
    // });

    // data.data.mapping.forEach((item) => {
    //   if (
    //     listRequiredField.filter((x) => x.key_object === item.key_object)
    //       .length === 0
    //   ) {
    //     arrNor.push(item);
    //   }
    // });
    // data.data.assign_to = data.data.assign_To
    // delete data.data.assign_To
    // yield put(updateRequiredField(arrReq));
    // yield put(updateNormalField(arrNor));
    // yield put(updateReplyFields(arrReply));
    yield put(updateEditRuleIncoming(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* deleteRuleByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteRuleByIdAPI({
      _id: action.payload._id,
    });
    yield put(
      getListRule({
        id_mailbox: action.payload.id_mailbox,
      })
    );
    Notification("success", "Delete rule successfully!");
    yield put(updateEditRuleIncoming({}));
    yield put(deleteRuleByIdSuccess(true));
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* updateRuleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateRuleAPI(action.payload);
    Notification("success", "Update successfully!");
    yield put(
      getListRule({
        id_mailbox: action.payload.id_mailbox,
      })
    );

    yield put(setEditing(false));
    yield put(setEditRule());
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* updatePositionRuleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updatePositionRuleAPI(action.payload);
    Notification("success", "Update successfully!");
    // yield put(
    //   getListRule({
    //     id_mailbox: action.payload.id_mailbox,
    //   })
    // );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* updateStatusRuleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateStatusRuleAPI(action.payload.status);
    yield put(
      getListRule({
        id_mailbox: action.payload.id_mailbox,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadValuePicklist(action) {
  try {
    const response = yield loadValueAPI(action.payload.data);
    let newObj = { ...action.payload.listValue };
    let temp = {};
    Object.entries(response.data.data).forEach(([key, value], index) => {
      // if (value) {
      temp[key] = value;
      // }
    });
    newObj[action.payload.ID] = { ...temp };
    yield put(loadValuePicklistSuccess(newObj));
  } catch (error) {
    yield put(loadValuePicklistFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadTargetPicklist(action) {
  try {
    const response = yield loadTargetAPI(action.payload);
    yield put(loadTargetPicklistSuccess(response.data.data));
  } catch (error) {
    yield put(loadTargetPicklistFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadValuePicklistReply(action) {
  try {
    const response = yield loadValueAPI(action.payload.data);
    let newObj = { ...action.payload.listValue };
    let temp = {};
    Object.entries(response.data.data).forEach(([key, value], index) => {
      // if (value) {
      temp[key] = value;
      // }
    });
    newObj[action.payload.ID] = { ...temp };
    yield put(loadValuePicklistReplySuccess(newObj));
  } catch (error) {
    yield put(loadValuePicklistReplyFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadTargetPicklistReply(action) {
  try {
    const response = yield loadTargetAPI(action.payload);
    yield put(loadTargetPicklistReplySuccess(response.data.data));
  } catch (error) {
    yield put(loadTargetPicklistReplyFail());
    Notification("error", error.response.data.error);
  }
}
