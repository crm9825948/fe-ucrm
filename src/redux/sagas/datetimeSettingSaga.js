import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";

import { loadConfigAPI, editConfigAPI } from "redux/api/datetimeSettingAPI";
import {
  loadConfig,
  loadConfigSuccess,
  loadConfigFail,
  editConfig,
} from "redux/slices/datetimeSetting";

export default function* datetimeSetting() {
  yield takeLatest(loadConfig.type, LoadConfig);
  yield takeLatest(editConfig.type, EditConfig);
}

function* LoadConfig() {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadConfigAPI();

    if (response.data.data?.shift_work) {
      response.data.data.shift_work.forEach((item, idx) => {
        let tempDays = [];
        item.applicable_weekdays.forEach((day, index) => {
          if (day) {
            tempDays.push(index);
          }
        });

        response.data.data.shift_work[idx] = {
          time: [
            String(response.data.data.shift_work[idx].from_time.hour).padStart(
              2,
              "0"
            ) +
              ":" +
              String(
                response.data.data.shift_work[idx].from_time.minute
              ).padStart(2, "0"),
            String(response.data.data.shift_work[idx].to_time.hour).padStart(
              2,
              "0"
            ) +
              ":" +
              String(
                response.data.data.shift_work[idx].to_time.minute
              ).padStart(2, "0"),
          ],
          day: [...tempDays],
        };
      });
    }

    yield put(loadConfigSuccess(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(loadConfigFail());
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* EditConfig(action) {
  try {
    yield editConfigAPI(action.payload);
    yield put(loadConfig());
    Notification("success", "Successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
