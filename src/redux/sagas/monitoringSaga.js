import { takeLatest, put } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";

const { loadDataMonitorAPI } = require("redux/api/monitoringAPI");
const { setShowLoadingScreen } = require("redux/slices/global");
const {
  loadDataMonitorSuccess,
  loadDataMonitor,
} = require("redux/slices/monitoring");
export default function* MonitoringSaga() {
  yield takeLatest(loadDataMonitor.type, loadDataMonitorSaga);
}

function* loadDataMonitorSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadDataMonitorAPI();
    yield put(loadDataMonitorSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
