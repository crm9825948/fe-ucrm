import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import {
  createServiceAPI,
  deleteServiceAPI,
  getArticleDetailAPI,
  getListArticleSearchAPI,
  getNewestAPI,
  getTopViewAPI,
  loadArticleDashboardAPI,
  loadListCategoryAPI,
  reIndexAPI,
  syncDataAPI,
  testConnectionAPI,
  updateServiceAPI,
} from "redux/api/enhancementViewAPI";

import { loadFieldsAPI } from "redux/api/knowledgeEnhancementAPI";
import {
  getListArticleDashboardSuccess,
  getListCategory,
  getListCategorySuccess,
  getListFieldObject,
  getListArticleDashboard,
  getListFieldObjectSuccess,
  getTopviewSuccess,
  getNewestSuccess,
  getTopview,
  getNewest,
  getListArticleSearchSuccess,
  getListArticleSearch,
  getArticleDetailSuccess,
  getArticleDetail,
  getListArticleFilterSuccess,
  getListArticleFilter,
  createService,
  createCategory,
  updateArticle,
  updateCategory,
  deleteCategory,
  deleteArticle,
  syncData,
  testConnection,
  reIndex,
} from "redux/slices/enhancementView";
import { setShowLoadingScreen } from "redux/slices/global";

export default function* enhancementViewSaga() {
  yield takeLatest(getListFieldObject.type, getListFieldObjectSaga);
  yield takeLatest(getListCategory.type, getListCategorySaga);
  yield takeLatest(getListArticleDashboard.type, getListArticleDashboardSaga);
  yield takeLatest(getTopview.type, getTopViewSaga);
  yield takeLatest(getNewest.type, getNewstSaga);
  yield takeLatest(getListArticleSearch.type, getListArticleSearchSaga);
  yield takeLatest(getArticleDetail.type, getArticleDetailSaga);
  yield takeLatest(getListArticleFilter.type, getListArticleFilterSaga);
  yield takeLatest(createService.type, createServiceSaga);
  yield takeLatest(createCategory.type, createCategorySaga);
  yield takeLatest(updateArticle.type, updateServiceSaga);
  yield takeLatest(updateCategory.type, updateCateggorySaga);
  yield takeLatest(deleteCategory.type, deleteCateggorySaga);
  yield takeLatest(deleteArticle.type, deleteArticleSaga);
  yield takeLatest(syncData.type, syncDataSaga);
  yield takeLatest(testConnection.type, testConnectionSaga);
  yield takeLatest(reIndex.type, reIndexSaga);
}

function* getListFieldObjectSaga(action) {
  try {
    const { data } = yield loadFieldsAPI(action.payload);
    yield put(getListFieldObjectSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListCategorySaga(action) {
  try {
    const { data } = yield loadListCategoryAPI(action.payload);
    yield put(getListCategorySuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListArticleDashboardSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadArticleDashboardAPI(action.payload);
    yield put(getListArticleDashboardSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getTopViewSaga(action) {
  try {
    const { data } = yield getTopViewAPI(action.payload);
    yield put(getTopviewSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getNewstSaga(action) {
  try {
    const { data } = yield getNewestAPI(action.payload);
    yield put(getNewestSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListArticleSearchSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListArticleSearchAPI(action.payload);
    yield put(getListArticleSearchSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getArticleDetailSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getArticleDetailAPI(action.payload);
    yield put(getArticleDetailSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* getListArticleFilterSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListArticleSearchAPI(action.payload);
    yield put(getListArticleFilterSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* createServiceSaga(action) {
  try {
    yield createServiceAPI(action.payload);
    Notification("success", "Add article success");
    setTimeout(() => {
      window.open(
        `${FE_URL}/knowledge-base-enhancement/${action.payload.object_id}`,
        "_self"
      );
    }, 1000);
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateServiceSaga(action) {
  try {
    yield updateServiceAPI(action.payload);
    Notification("success", "Update article success");
    setTimeout(() => {
      window.open(
        `${FE_URL}/knowledge-base-enhancement/${action.payload.object_id}`,
        "_self"
      );
    }, 1000);
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* createCategorySaga(action) {
  try {
    yield createServiceAPI(action.payload);
    Notification("success", "Create category success");
    yield put(getListCategory(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateCateggorySaga(action) {
  try {
    yield updateServiceAPI(action.payload);
    Notification("success", "Update category success");
    yield put(getListCategory(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* deleteCateggorySaga(action) {
  try {
    yield deleteServiceAPI(action.payload);
    Notification("success", "Delete category success");
    yield put(getListCategory(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* deleteArticleSaga(action) {
  try {
    yield deleteServiceAPI(action.payload);
    Notification("success", "Delete article success");
    setTimeout(() => {
      window.open(
        `${FE_URL}/knowledge-base-enhancement/${action.payload.object_id}`,
        "_self"
      );
    }, 1000);
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* syncDataSaga(action) {
  try {
    yield syncDataAPI(action.payload);
    Notification("success", "Sync data success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* testConnectionSaga(action) {
  try {
    yield testConnectionAPI(action.payload);
    Notification("success", "Success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* reIndexSaga(action) {
  try {
    yield reIndexAPI(action.payload);
    Notification("success", "Re-index success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
