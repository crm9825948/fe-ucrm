import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";

import { callEventAPI, updateCustomerAPI } from "redux/api/voiceBiometricAPI";
import { callEvent, updateCustomer } from "redux/slices/voiceBiometric";

export default function* voicebiometric() {
  yield takeLatest(callEvent.type, CallEvent);
  yield takeLatest(updateCustomer.type, UpdateCustomer);
}

function* UpdateCustomer(action) {
  try {
    yield updateCustomerAPI(action.payload);
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* CallEvent(action) {
  try {
    yield callEventAPI(action.payload);

    if (action.payload.voice_biometric_event === "decline_voice_registration") {
      yield put(
        updateCustomer({
          record_id: action.payload.record_id,
          object_id: action.payload.object_id,
          value: "Từ chối đăng ký",
        })
      );
    }

    if (action.payload.voice_biometric_event === "unregister_voice") {
      yield put(
        updateCustomer({
          record_id: action.payload.record_id,
          object_id: action.payload.object_id,
          value: "Huỷ đăng ký",
        })
      );
    }

    if (action.payload.voice_biometric_event === "register_voice") {
      yield put(
        updateCustomer({
          record_id: action.payload.record_id,
          object_id: action.payload.object_id,
          value: "Đồng ý đăng ký",
        })
      );
    }
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
