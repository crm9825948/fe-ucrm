import { put, takeLatest, select } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { loadListObjectField } from "redux/slices/objects";
import { loadFieldsAPI } from "redux/api/objectsAPIs";
import { loadValueAPI } from "redux/api/picklistAPI";
import { loadTargetAPI } from "redux/api/objectsAPIs";

import {
  getListEmailIncoming,
  getListEmailIncomingSuccess,
  changeStatus,
  changeStatusSuccess,
  submitMailBox,
  deleteMailIncoming,
  checkConnection,
  updateEditEmail,
  updateEditEmailSuccess,
  updateEditEmailFolder,
  updateEditEmailFolderSuccess,
  updateMailBoxIncoming,
  setShowDrawer,
  updateMappingFields,
  setDefaultEmailIncoming,
  getEmailFolders,
  getEmailFoldersResult,
  scanEmail,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
} from "redux/slices/emailIncoming";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import {
  getListEmailIncomingAPI,
  changeStatusAPI,
  submitMaiBoxIncomingAPI,
  deleteMailBoxIncomingAPI,
  checkConnectionAPI,
  getEmailIncomingByIdAPI,
  updateMailBoxIncomingAPI,
  setDefaultIncomingConfigAPI,
  getEmailFoldersAPI,
  scanEmailAPI,
} from "../api/emailIncomingAPIs";
export const emailIncomingReducer = (state) => state.emailIncomingReducer;
export const jwt = require("jsonwebtoken");

export default function* emailIncomingSaga() {
  yield takeLatest(getListEmailIncoming.type, GetListEmailIncoming);
  yield takeLatest(changeStatus.type, ChangeStatusItem);
  yield takeLatest(submitMailBox.type, SubmitMailBoxIncoming);
  yield takeLatest(deleteMailIncoming.type, DeleteMailBoxIncoming);
  yield takeLatest(checkConnection.type, CheckConnection);
  yield takeLatest(updateEditEmail.type, UpdateEditEmailIncoming);
  yield takeLatest(updateEditEmailFolder.type, UpdateEditEmailFolder);
  yield takeLatest(updateMailBoxIncoming.type, UpdateMailBoxSaga);
  yield takeLatest(setDefaultEmailIncoming.type, SetDefaultEmailIncoming);
  yield takeLatest(getEmailFolders.type, GetEmailFolders);
  yield takeLatest(scanEmail.type, ScanEmail);
  yield takeLatest(loadValuePicklist.type, LoadValuePicklist);
  yield takeLatest(loadTargetPicklist.type, LoadTargetPicklist);
}

function* GetListEmailIncoming() {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListEmailIncomingAPI();
    yield put(getListEmailIncomingSuccess(data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* ChangeStatusItem(action) {
  try {
    yield changeStatusAPI(action.payload.payload);
    const emailIncomingState = yield select(emailIncomingReducer);
    let payload = [];
    emailIncomingState.listEmailIncoming.forEach((item) => {
      if (item._id === action.payload.item._id) {
        let tempObj = {
          ...item,
          status: !action.payload.item.status,
        };
        payload.push(tempObj);
      } else {
        payload.push(item);
      }
    });
    yield put(changeStatusSuccess(payload));
    Notification("success", "Cập nhật trạng thái thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* SubmitMailBoxIncoming(action) {
  try {
    yield put(setShowLoadingScreen(true));
    let payload = {
      ...action.payload,
      ssl_type:
        action.payload.server_type === "google"
          ? null
          : action.payload.ssl_type,
      pass_word:
        action.payload.server_type === "custom" ||
        action.payload.server_type === "owa"
          ? jwt.sign(
              { password: action.payload.pass_word },
              "RANDOM_VARIABLE",
              {
                algorithm: "HS256",
              }
            )
          : "",
    };
    if (payload.port === "custom") {
      payload.port = payload.port_custom;
    }
    if (payload.ssl_type === "none") {
      delete payload.ssl_type;
    }
    delete payload.port_custom;
    yield submitMaiBoxIncomingAPI(payload);
    Notification("success", "Thêm thành công");
    yield put(updateMappingFields([]));
    yield put(setShowDrawer(false));
    yield put(getListEmailIncoming());
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateEditEmailIncoming(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getEmailIncomingByIdAPI(action.payload);

    const fields = yield loadFieldsAPI({
      api_version: "2",
      object_id: data.data.object_id,
    });

    let payload = {
      ...data.data,
      // pass_word:
      //   data.data.server_type === "custom"
      //     ? jwt.verify(data.data.pass_word, "basebsmail").password
      //     : "",
      server_type:
        !data.data.server_type || data.data.server_type.length === 0
          ? "custom"
          : data.data.server_type,
    };
    // eslint-disable-next-line
    if (payload.port != 993 && payload.port != 153) {
      payload.port_custom = payload.port;
      payload.port = "custom";
    }
    if (!payload.ssl_type) {
      payload.ssl_type = "none";
    }

    yield put(
      loadListObjectField({
        api_version: "2",
        object_id: data.data.object_id,
      })
    );

    let tempMappingFields = [];

    // eslint-disable-next-line
    fields.data.data.map((item) => {
      if (item.main_object) {
        // eslint-disable-next-line
        item.main_object.sections.map((section) => {
          // eslint-disable-next-line
          section.fields.map((field) => {
            if (
              data.data.mapping_fields_update &&
              data.data.mapping_fields_update.filter(
                (x) => x.key_object === field.field_id
              ).length > 0
            ) {
              let t = {
                ...data.data.mapping_fields_update.filter(
                  (x) => x.key_object === field.field_id
                )[0],
              };
              t.related_object_id = field.objectname || "";
              t.field_linking = field.field;
              tempMappingFields.push(t);
            }
          });
        });
      }
    });

    yield put(updateMappingFields(tempMappingFields));
    yield put(updateEditEmailSuccess(payload));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateEditEmailFolder(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getEmailIncomingByIdAPI(action.payload);

    const fields = yield loadFieldsAPI({
      api_version: "2",
      object_id: data.data.object_id,
    });

    let payload = {
      ...data.data,
      // pass_word:
      //   data.data.server_type === "custom"
      //     ? jwt.verify(data.data.pass_word, "basebsmail").password
      //     : "",
      server_type:
        !data.data.server_type || data.data.server_type.length === 0
          ? "custom"
          : data.data.server_type,
    };
    // eslint-disable-next-line
    if (payload.port != 993 && payload.port != 153) {
      payload.port_custom = payload.port;
      payload.port = "custom";
    }
    if (!payload.ssl_type) {
      payload.ssl_type = "none";
    }

    yield put(
      loadListObjectField({
        api_version: "2",
        object_id: data.data.object_id,
      })
    );

    let tempMappingFields = [];

    // eslint-disable-next-line
    fields.data.data.map((item) => {
      if (item.main_object) {
        // eslint-disable-next-line
        item.main_object.sections.map((section) => {
          // eslint-disable-next-line
          section.fields.map((field) => {
            if (
              data.data.mapping_fields_update &&
              data.data.mapping_fields_update.filter(
                (x) => x.key_object === field.field_id
              ).length > 0
            ) {
              let t = {
                ...data.data.mapping_fields_update.filter(
                  (x) => x.key_object === field.field_id
                )[0],
              };
              t.related_object_id = field.objectname || "";
              t.field_linking = field.field;
              tempMappingFields.push(t);
            }
          });
        });
      }
    });

    yield put(updateMappingFields(tempMappingFields));
    yield put(updateEditEmailFolderSuccess(payload));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* DeleteMailBoxIncoming(action) {
  try {
    yield deleteMailBoxIncomingAPI(action.payload);
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Xoá thành công");
    yield put(getListEmailIncoming());
  } catch (error) {
    Notification("error", error.response.data.mess);
  }
}

function* CheckConnection(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield checkConnectionAPI(action.payload);
    yield put(setShowLoadingScreen(false));
    Notification("success", "Thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* UpdateMailBoxSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    let payload = {
      _id: action.payload._id,
      data: {
        ...action.payload,
        ssl_type:
          action.payload.server_type === "google"
            ? null
            : action.payload.ssl_type,
        pass_word:
          action.payload.server_type === "custom" ||
          action.payload.server_type === "owa"
            ? jwt.sign(
                { password: action.payload.pass_word },
                "RANDOM_VARIABLE",
                {
                  algorithm: "HS256",
                }
              )
            : "",
      },
    };
    if (payload.data.port === "custom") {
      payload.data.port = payload.data.port_custom;
    }
    delete payload.data._id;
    delete payload.data.port_custom;
    delete payload.data.create_date;
    delete payload.data.create_by;
    delete payload.data.modify_by;
    delete payload.data.modify_time;
    delete payload.data.active;
    !action.payload.pass_word && delete payload.data.pass_word;
    yield updateMailBoxIncomingAPI(payload);
    yield put(updateMappingFields([]));
    Notification("success", "Cập nhật thành công");
    yield put(getListEmailIncoming());
    yield put(setShowDrawer(false));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* SetDefaultEmailIncoming(action) {
  try {
    yield setDefaultIncomingConfigAPI(action.payload);
    yield put(getListEmailIncoming());
    Notification("success", "Make default successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetEmailFolders(action) {
  try {
    const response = yield getEmailFoldersAPI(action.payload);
    yield put(getEmailFoldersResult(response.data.data));
  } catch (error) {
    yield put(getEmailFoldersResult(["INBOX"]));
    Notification("error", error.response.data.error);
  }
}

function* ScanEmail(action) {
  try {
    yield scanEmailAPI(action.payload);
    Notification("success", "Thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadValuePicklist(action) {
  try {
    const response = yield loadValueAPI(action.payload.data);
    let newObj = { ...action.payload.listValue };
    let temp = {};
    Object.entries(response.data.data).forEach(([key, value], index) => {
      // if (value) {
      temp[key] = value;
      // }
    });
    newObj[action.payload.ID] = { ...temp };
    yield put(loadValuePicklistSuccess(newObj));
  } catch (error) {
    yield put(loadValuePicklistFail());
    Notification("error", error.response.data.error);
  }
}

function* LoadTargetPicklist(action) {
  try {
    const response = yield loadTargetAPI(action.payload);
    yield put(loadTargetPicklistSuccess(response.data.data));
  } catch (error) {
    yield put(loadTargetPicklistFail());
    Notification("error", error.response.data.error);
  }
}
