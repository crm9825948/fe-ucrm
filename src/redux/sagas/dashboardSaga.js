import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  createDashboardAPI,
  deleteDashboardAPI,
  getAvailableDashboardAPI,
  getDefaultDashboardAPI,
  makeDefaultAPI,
  updateDashboardAPI,
} from "redux/api/dashboardAPI";
import {
  createDashboard,
  createDashboardFail,
  createDashboardSuccess,
  deleteDashboard,
  deleteDashboardFail,
  deleteDashboardSuccess,
  loadAllDashboard,
  loadAllDashboardFail,
  loadAllDashboardSuccess,
  loadDefaultDashboard,
  loadDefaultDashboardFail,
  loadDefaultDashboardSuccess,
  loadListDashboard,
  loadListDashboardFail,
  loadListDashboardSuccess,
  makeDefault,
  makeDefaultFail,
  makeDefaultSuccess,
  updateDashboard,
  updateNameDashboard,
} from "redux/slices/dashboard";

export default function* dashboard() {
  yield takeLatest(loadAllDashboard.type, LoadAllDashboard);
  yield takeLatest(loadDefaultDashboard.type, LoadDefaultDashboard);
  yield takeLatest(updateDashboard.type, UpdateDashboard);
  yield takeLatest(createDashboard.type, CreateDashboard);
  yield takeLatest(deleteDashboard.type, DeleteDashboard);
  yield takeLatest(updateNameDashboard.type, UpdateNameDashboard);
  yield takeLatest(loadListDashboard.type, LoadListDashboard);
  yield takeLatest(makeDefault.type, MakeDefault);
}

function* LoadAllDashboard(action) {
  try {
    const response = yield getAvailableDashboardAPI(action.payload);
    yield put(loadAllDashboardSuccess(response.data.data));
    yield put(loadListDashboardSuccess(response.data.data));
  } catch (error) {
    yield put(loadAllDashboardFail());
  }
}

function* LoadListDashboard(action) {
  try {
    const response = yield getAvailableDashboardAPI(action.payload);
    yield put(loadListDashboardSuccess(response.data.data));
  } catch (error) {
    yield put(loadListDashboardFail());
  }
}

function* LoadDefaultDashboard(action) {
  try {
    const response = yield getDefaultDashboardAPI(action.payload);
    yield put(loadDefaultDashboardSuccess(response.data.data));
  } catch (error) {
    yield put(loadDefaultDashboardFail());
  }
}

function* UpdateDashboard(action) {
  try {
    yield updateDashboardAPI(action.payload);
    // yield put(updateDashboardSuccess());
    Notification("success", "Update successfully!", 1);
  } catch (error) {
    // yield put(updateDashboardFail());
    Notification("error", error.response.data.error);
  }
}

function* UpdateNameDashboard(action) {
  try {
    yield updateDashboardAPI(action.payload);
    // yield put(updateDashboardSuccess());
    Notification("success", "Update successfully!");
    const response = yield getAvailableDashboardAPI(action.payload);
    yield put(loadListDashboardSuccess(response.data.data));
  } catch (error) {
    // yield put(updateDashboardFail());
    Notification("error", error.response.data.error);
  }
}

function* CreateDashboard(action) {
  try {
    yield createDashboardAPI(action.payload);
    yield put(createDashboardSuccess());
    Notification("success", "Create successfully!");

    const responseNext = yield getAvailableDashboardAPI({
      view_components: false,
    });
    yield put(loadListDashboardSuccess(responseNext.data.data));
  } catch (error) {
    yield put(loadListDashboardFail());
    yield put(createDashboardFail());
    Notification("error", error.response.data.error);
  }
}

function* DeleteDashboard(action) {
  try {
    yield deleteDashboardAPI(action.payload);
    yield put(deleteDashboardSuccess());

    const responseNext = yield getAvailableDashboardAPI({
      view_components: false,
    });
    yield put(loadListDashboardSuccess(responseNext.data.data));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(loadListDashboardFail());
    yield put(deleteDashboardFail());
    Notification("error", error.response.data.error);
  }
}

function* MakeDefault(action) {
  try {
    yield makeDefaultAPI(action.payload);
    yield put(makeDefaultSuccess());

    // const responseNext = yield getAvailableDashboardAPI({
    //   view_components: false,
    // });
    // yield put(loadListDashboardSuccess(responseNext.data.data));

    const response = yield getDefaultDashboardAPI(action.payload);
    yield put(loadDefaultDashboardSuccess(response.data.data));
    Notification("success", "Mark default successfully!");
  } catch (error) {
    yield put(loadListDashboardFail());
    yield put(makeDefaultFail());
    Notification("error", error.response.data.error);
  }
}
