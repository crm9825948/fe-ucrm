import { all } from "redux-saga/effects";
import authenticatedSaga from "./authenticatedSaga";
import brandNameSaga from "./brandNameSaga";
import consolidatedViewSettingsSaga from "./consolidatedViewSettingsSaga";
import criteriaSharingSaga from "./criteriaSharingSaga";
import dashboardSaga from "./dashboardSaga";
import dynamicButtonSaga from "./dynamicButtonSaga";
import emailComponentSaga from "./emailComponentSaga";
import emailIncomingSaga from "./emailIncomingSaga";
import emailOutgoingSaga from "./emailOutgoingSaga";
import emailTemplate from "./emailTemplateSaga";
import exportHistorySaga from "./exportHistorySaga";
import fieldsManagementSaga from "./fieldsManagementSaga";
import groupSaga from "./groupSaga";
import kanbanViewSaga from "./kanbanViewSaga";
import kanbanViewSettingSaga from "./kanbanViewSettingSaga";
import knowledgeBaseSettingSaga from "./knowledgeBaseSettingSaga";
import notificationSaga from "./notificationSaga";
import objectsManagementSaga from "./objectsManagementSaga";
import objectsSaga from "./objectsSaga";
import picklistSaga from "./picklistSaga";
import profileSaga from "./profileSaga";
import relatedObject from "./relatedObjectSaga";
import reportDetailsSaga from "./reportDetailsSaga";
import reportSaga from "./reportSaga";
import rolesSaga from "./rolesSaga";
import ruleIncomingSaga from "./ruleIncomingSaga";
import sharingSaga from "./sharingSaga";
import smsOutgoingSaga from "./smsOutgoingSaga";
import smsTemplateSaga from "./smsTemplateSaga";
import userSaga from "./userSaga";
import workflowsSaga from "./workflowsSaga";
import componentKnowledgeBaseSaga from "./componentKnowledgeBaseSaga";
import datetimeSettingSaga from "./datetimeSettingSaga";
import knowledgeBaseViewSaga from "./knowledgeBaseViewSaga";
import articleSaga from "./articlesSaga";
import slaSettingSaga from "./slaSettingSaga";
import googleIntegrationSaga from "./googleIntegrationSaga";
import tenantsSaga from "./tenantsSaga";
import campaignSaga from "./campaignSaga";
import callCenterSaga from "./callCenterSaga";
import highlightSettingSaga from "./highlightSettingSaga";
import ldapSettingSaga from "./ldapSaga";
import voiceBiometricSaga from "./voiceBiometricSaga";
import commentSaga from "./commentSaga";
import calendarSaga from "./calendarSaga";
import outlookSaga from "./outlookSaga";
import duplicateRulesSaga from "./duplicateRulesSaga";
import systemLogSaga from "./systemLogSaga";
import assignmentRuleSaga from "./assignmentRuleSaga";
import componentSMSSaga from "./componentSMSSaga";
import knowledgeEnhancementSaga from "./knowledgeEnhancementSaga";
import enhancementViewSaga from "./enhancementViewSaga";
import enhancementComponentSaga from "./enhancementComponentSaga";
import formSettingSaga from "./formSettingSaga";
import agentMonitorSaga from "./agentMonitorSaga";
import approvalProcessesSaga from "./approvalProcessesSaga";
import icChatWidgetSaga from "./icChatWidgetSaga";
import searchManagementSaga from "./searchManagementSaga";
import templateFileSaga from "./templateFileSaga";
import qualityManagement from "./QualityManagementSaga";
import objectStandardSaga from "./objectStandardSaga";
import monitoringSaga from "./monitoringSaga";
import customReportSaga from "./customReportSaga";
import callViewSaga from "./callViewSaga";

export default function* rootSaga() {
  yield all([
    ...authenticatedSaga(),
    ...objectsManagementSaga(),
    ...fieldsManagementSaga(),
    ...rolesSaga(),
    ...profileSaga(),
    ...relatedObject(),
    ...userSaga(),
    ...objectsSaga(),
    ...groupSaga(),
    ...reportSaga(),
    ...emailComponentSaga(),
    ...reportDetailsSaga(),
    ...sharingSaga(),
    ...picklistSaga(),
    ...criteriaSharingSaga(),
    ...workflowsSaga(),
    ...emailIncomingSaga(),
    ...emailOutgoingSaga(),
    ...ruleIncomingSaga(),
    ...consolidatedViewSettingsSaga(),
    ...emailTemplate(),
    ...brandNameSaga(),
    ...smsOutgoingSaga(),
    ...kanbanViewSettingSaga(),
    ...kanbanViewSaga(),
    ...dynamicButtonSaga(),
    ...dashboardSaga(),
    ...exportHistorySaga(),
    ...smsTemplateSaga(),
    ...notificationSaga(),
    ...knowledgeBaseSettingSaga(),
    ...componentKnowledgeBaseSaga(),
    ...datetimeSettingSaga(),
    ...knowledgeBaseViewSaga(),
    ...articleSaga(),
    ...slaSettingSaga(),
    ...googleIntegrationSaga(),
    ...tenantsSaga(),
    ...campaignSaga(),
    ...callCenterSaga(),
    ...highlightSettingSaga(),
    ...ldapSettingSaga(),
    ...voiceBiometricSaga(),
    ...commentSaga(),
    ...calendarSaga(),
    ...outlookSaga(),
    ...duplicateRulesSaga(),
    ...systemLogSaga(),
    ...assignmentRuleSaga(),
    ...componentSMSSaga(),
    ...knowledgeEnhancementSaga(),
    ...enhancementViewSaga(),
    ...enhancementComponentSaga(),
    ...formSettingSaga(),
    ...agentMonitorSaga(),
    ...approvalProcessesSaga(),
    ...icChatWidgetSaga(),
    ...searchManagementSaga(),
    ...templateFileSaga(),
    ...qualityManagement(),
    ...objectStandardSaga(),
    ...monitoringSaga(),
    ...customReportSaga(),
    ...callViewSaga(),
  ]);
}
