import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  deleteLogoByIdAPI,
  getDefaultBrandAPI,
  getListLogoAPI,
  uploadLogoAPI,
} from "redux/api/brandNameAPI";
import {
  deleteLogoById,
  getDefaultBrandName,
  getDefaultBrandNameSuccess,
  getListLogo,
  getListLogoSuccess,
  setDetailLogo,
  uploadLogo,
} from "redux/slices/brandName";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import defaultBrandName from "../../assets/images/defaultBrand.png";

export default function* brandNameSaga() {
  yield takeLatest(uploadLogo.type, uploadLogoSaga);
  yield takeLatest(getListLogo.type, getListLogoSaga);
  yield takeLatest(getDefaultBrandName.type, getDefaultBrandSaga);
  yield takeLatest(deleteLogoById.type, deleteLogoByIdSaga);
}

function* uploadLogoSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield uploadLogoAPI(action.payload);
    localStorage.setItem("themeColor", action.payload.theme_color);
    yield put(getListLogo());
    Notification("success", "Tải lên thành công");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}
function* getListLogoSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListLogoAPI(action.payload);
    yield put(getListLogoSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {}
}

function* getDefaultBrandSaga() {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getDefaultBrandAPI();
    if (data.data) {
      if (data.data.length === 0) {
        yield put(
          getDefaultBrandNameSuccess({
            media_url: defaultBrandName,
            media_config: {
              position: {
                x: 0.5,
                y: 0.5,
              },
              border: 0,
              borderRadius: "0%",
              scale: 1,
              width: 150,
              height: 150,
            },
            theme_color: "#20a2a2",
          })
        );
      } else {
        yield put(getDefaultBrandNameSuccess(data.data));
      }
    } else {
      yield put(
        getDefaultBrandNameSuccess({
          media_url: defaultBrandName,
          media_config: {
            position: {
              x: 0.5,
              y: 0.5,
            },
            border: 0,
            borderRadius: "0%",
            scale: 1,
            width: 150,
            height: 150,
          },
          theme_color: "#20a2a2",
        })
      );
    }
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* deleteLogoByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteLogoByIdAPI(action.payload);
    yield put(getListLogo());
    yield put(setDetailLogo({}));
    Notification("success", "Xoá thành công");
    yield put(getDefaultBrandName());
    yield put(setShowModalConfirmDelete(false));
  } catch (error) {
    Notification("error", error.response.data.mess);
    yield put(setShowLoadingScreen(false));
  }
}
