import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { deleteArticleAPI } from "redux/api/componentKnowledgeBaseAPI";
import { loadPaginationAPI } from "redux/api/objectsAPIs";
import {} from "redux/slices/articles";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import {
  getListArticle,
  getListArticleSuccess,
  getTreeViewKnowledgeBase,
  getTreeViewKnowledgeBaseSuccess,
  deleteArticleById,
  loadMoreArticle,
  loadMoreArticleSuccess,
  updateFilterArticle,
  updateTotalPageAndRecord,
} from "redux/slices/knowledgeBaseView";
import {
  getListArticleAPI,
  getTreeKnowledgeBaseAPI,
} from "../api/knowledgeBaseViewAPIs";

export default function* KnowledgeBaseViewSaga() {
  yield takeLatest(getListArticle.type, getListArticleSaga);
  yield takeLatest(getTreeViewKnowledgeBase.type, getTreeViewKnowledgeBaseSaga);
  yield takeLatest(deleteArticleById.type, deleteArticleByIdSaga);
  yield takeLatest(loadMoreArticle, loadMoreArticleSaga);
}

function* getListArticleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListArticleAPI(action.payload.data);
    const res = yield loadPaginationAPI(action.payload.pagination);
    if (action.payload.data.section_name) {
      yield put(
        updateFilterArticle({
          key: "section_name",
          value: action.payload.data.section_name,
        })
      );
    }
    if (action.payload.data.category_name) {
      yield put(
        updateFilterArticle({
          key: "category_name",
          value: action.payload.data.category_name,
        })
      );
    }
    yield put(
      updateTotalPageAndRecord({
        totalPage: res.data.data.total_page ? res.data.data.total_page : 0,
        totalRecord: res.data.data.total_record
          ? res.data.data.total_record
          : 0,
      })
    );
    yield put(getListArticleSuccess(data.data[0]));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* getTreeViewKnowledgeBaseSaga(action) {
  try {
    const { data } = yield getTreeKnowledgeBaseAPI(action.payload);
    yield put(getTreeViewKnowledgeBaseSuccess(data.data[0]));
  } catch (error) {}
}

function* deleteArticleByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteArticleAPI(action.payload);
    yield put(setShowModalConfirmDelete(false));
    yield put(setShowLoadingScreen(false));
    yield put(
      getListArticle({
        data: {
          object_id: action.payload.object_id,
          page: 0,
          limit: 16,
          search_with_title: "",
        },
        pagination: {
          object_id: action.payload.object_id,
          current_page: 1,
          record_per_page: 16,
          search_with: {
            meta: [],
            data: [],
          },
        },
      })
    );
    Notification("success", "Xoá bài viết thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}

function* loadMoreArticleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListArticleAPI(action.payload.data);
    const res = yield loadPaginationAPI(action.payload.pagination);
    yield put(
      loadMoreArticleSuccess({
        data: [...data.data[0]],
        pagination: {
          limit: action.payload.data.limit,
          page: action.payload.data.page,
        },
      })
    );
    yield put(
      updateTotalPageAndRecord({
        totalPage: res.data.data.total_page,
        totalRecord: res.data.data.total_record,
      })
    );
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
  }
}
