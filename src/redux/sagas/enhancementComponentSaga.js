import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { getListArticleSearchAPI } from "redux/api/enhancementViewAPI";
import {
  getListArticle,
  getListArticleSuccess,
} from "redux/slices/enhancementComponent";
import { setShowLoadingScreen } from "redux/slices/global";

export default function* enhancementViewSaga() {
  yield takeLatest(getListArticle.type, getListArticleSaga);
}

function* getListArticleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListArticleSearchAPI(action.payload);
    yield put(getListArticleSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
