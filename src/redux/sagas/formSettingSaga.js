import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { loadRelatedObjectAPI } from "redux/api/fieldsManagementAPI";
import {
  createFormSettingAPI,
  deleteFormSettingAPI,
  getFormSettingAPI,
  getListFormSettingAPI,
  submitFormAPI,
  toggleFormSettingAPI,
  updateFormSettingAPI,
} from "redux/api/formSettingAPI";
import { loadFieldsAPI } from "redux/api/knowledgeEnhancementAPI";
import {
  createFormsetting,
  deleteFormSetting,
  getFieldObject,
  getFieldObjectSuccess,
  getFormSettingSuccess,
  getListFormSetting,
  getListFormSettingSuccess,
  toggleFormSetting,
  updateFormSetting,
  getFormSetting,
  submitForm,
  getListRelatedObjectSuccess,
  getFieldObjectRelatedSuccess,
  getListRelatedObject,
  getFieldObjectRelated,
  updateDataReturn,
  submitFormSuccess,
  setLoadingSubmit,
  updateTotalRecord,
} from "redux/slices/formSetting";
import { setShowLoadingScreen } from "redux/slices/global";

export default function* formSettingSaga() {
  yield takeLatest(getFieldObject.type, getFieldObjectSaga);
  yield takeLatest(createFormsetting.type, createFormSettingSaga);
  yield takeLatest(deleteFormSetting.type, deleteFormSettingSaga);
  yield takeLatest(updateFormSetting.type, updateFormSettingSaga);
  yield takeLatest(getListFormSetting.type, getListFormSettingSaga);
  yield takeLatest(toggleFormSetting.type, toggleFormSettingSaga);
  yield takeLatest(getFormSetting.type, getFormSettingSaga);
  yield takeLatest(submitForm.type, submitFormSaga);
  yield takeLatest(getListRelatedObject.type, getListRelatedObjectSaga);
  yield takeLatest(getFieldObjectRelated.type, getFieldObjectRelatedSaga);
}

function* getFieldObjectSaga(action) {
  try {
    const { data } = yield loadFieldsAPI(action.payload);
    yield put(getFieldObjectSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* createFormSettingSaga(action) {
  try {
    yield createFormSettingAPI(action.payload);
    Notification("success", "Create setting success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* deleteFormSettingSaga(action) {
  try {
    yield deleteFormSettingAPI(action.payload.data);
    Notification("success", "Delete setting success");
    yield put(getListFormSetting(action.payload.dataReload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateFormSettingSaga(action) {
  try {
    yield updateFormSettingAPI(action.payload);
    Notification("success", "Update setting success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getListFormSettingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListFormSettingAPI(action.payload);
    yield put(getListFormSettingSuccess(data.data.data));
    yield put(updateTotalRecord(data.data.total_records));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));

    Notification("error", error.response.data.error);
  }
}

function* toggleFormSettingSaga(action) {
  try {
    yield toggleFormSettingAPI(action.payload.data);
    Notification("success", "Update status success");
    yield put(getListFormSetting(action.payload.dataReload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* getFormSettingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getFormSettingAPI(action.payload);
    yield put(getFormSettingSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* submitFormSaga(action) {
  try {
    const { data } = yield submitFormAPI(action.payload);
    yield put(updateDataReturn(data));
    yield put(submitFormSuccess());
    Notification("success", "Submit form success");
  } catch (error) {
    yield put(updateDataReturn({ key: false }));

    Notification("error", error.response.error);
    yield put(setLoadingSubmit(false));
  }
}

function* getListRelatedObjectSaga(action) {
  try {
    const { data } = yield loadRelatedObjectAPI(action.payload);
    yield put(getListRelatedObjectSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.error);
  }
}

function* getFieldObjectRelatedSaga(action) {
  try {
    const { data } = yield loadFieldsAPI(action.payload);
    yield put(getFieldObjectRelatedSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
