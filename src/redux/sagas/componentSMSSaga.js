import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  deleteSMSAPI,
  getAllMessageAPI,
  getListSMSTemplateAPI,
  getSMSTemplateAPI,
  sendSMSAPI,
} from "redux/api/componentSMSAPI";
import {
  actionSendSMS,
  // changeListSMS,
  changeTotalRecord,
  deleteSMS,
  getListMessage,
  getListMessageSuccess,
  getListSMSTemplate,
  getListSMSTemplateSuccess,
  getSmsTemplateDetail,
  getSmsTemplateDetailSuccess,
} from "redux/slices/componentSMS";

export default function* ComponentSMSSaga() {
  yield takeLatest(getListMessage.type, GetListMessage);
  yield takeLatest(getListSMSTemplate.type, GetListSMSTemplate);
  yield takeLatest(getSmsTemplateDetail.type, GetSmsTemplateDetail);
  yield takeLatest(actionSendSMS.type, ActionSendSMS);
  yield takeLatest(deleteSMS.type, DeleteSMS);
}

function* GetListMessage(action) {
  try {
    const { data } = yield getAllMessageAPI(action.payload);

    yield put(changeTotalRecord(data.data.total_records));
    // yield delay(500);
    yield put(getListMessageSuccess(data.data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetListSMSTemplate(action) {
  try {
    const { data } = yield getListSMSTemplateAPI();
    yield put(getListSMSTemplateSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* GetSmsTemplateDetail(action) {
  try {
    const { data } = yield getSMSTemplateAPI(action.payload);
    yield put(getSmsTemplateDetailSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ActionSendSMS(action) {
  try {
    yield sendSMSAPI(action.payload);
    Notification("success", "Send SMS success");
    // yield put(changeListSMS([]));
    yield put(getListMessage(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DeleteSMS(action) {
  try {
    yield deleteSMSAPI(action.payload);
    Notification("success", "Delete SMS success");

    // yield put(getListMessage(action.payload));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
