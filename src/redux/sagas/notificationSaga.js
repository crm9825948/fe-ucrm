import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import {
  getListNotiAPI,
  seenOneNotiAPI,
  seenAllNotiAPI,
} from "redux/api/notificationAPI";
import {
  getListNoti,
  getListNotiSuccess,
  seenOneNoti,
  seenAllNoti,
} from "redux/slices/notification";

export default function* sharing() {
  yield takeLatest(getListNoti.type, GetListNoti);
  yield takeLatest(seenOneNoti.type, SeenOneNoti);
  yield takeLatest(seenAllNoti.type, SeenAllNoti);
}

function* GetListNoti(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getListNotiAPI(action.payload);
    yield put(getListNotiSuccess(response.data.data[0]));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* SeenOneNoti(action) {
  try {
    yield seenOneNotiAPI(action.payload);
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* SeenAllNoti(action) {
  try {
    yield seenAllNotiAPI(action.payload);
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}
