import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import {
  createSettingAPI,
  loadAllSettingAPI,
  deleteSettingAPI,
  updateSettingAPI,
  syncDataAPI,
} from "redux/api/ldapAPI";

import {
  showLdapSetting,
  loadAllSetting,
  loadAllSettingResult,
  createSetting,
  changeState,
  deleteSetting,
  updateSetting,
  syncData,
} from "redux/slices/ldap";

export default function* ldap() {
  yield takeLatest(loadAllSetting.type, LoadAllSetting);
  yield takeLatest(createSetting.type, CreateSetting);
  yield takeLatest(deleteSetting.type, DeleteSetting);
  yield takeLatest(updateSetting.type, UpdateSetting);
  yield takeLatest(syncData.type, SyncData);
}

function* LoadAllSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadAllSettingAPI(action.payload);
    yield put(loadAllSettingResult(response.data.data));
    yield delay(800);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createSettingAPI(action.payload);
    yield put(
      loadAllSetting({
        current_page: 1,
        record_per_page: 10,
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(showLdapSetting(false));
    Notification("success", "Create successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateSettingAPI(action.payload);
    yield put(
      loadAllSetting({
        current_page: 1,
        record_per_page: 10,
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(showLdapSetting(false));
    Notification("success", "Update successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteSetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteSettingAPI(action.payload);
    yield put(
      changeState({
        type: "delete",
        id: action.payload.id,
      })
    );
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* SyncData() {
  try {
    yield put(setShowLoadingScreen(true));
    yield syncDataAPI();
    yield put(setShowLoadingScreen(false));
    Notification("success", "Sync Data successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
