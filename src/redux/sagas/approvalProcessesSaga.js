import { put, takeLatest } from "@redux-saga/core/effects";
import { setShowLoadingScreen } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";
import _ from "lodash";

import { loadFieldsAPI } from "redux/api/objectsAPIs";

import {
  getListConfigAPI,
  createConfigAPI,
  updateConfigAPI,
  deleteConfigAPI,
} from "redux/api/approvalProcessesAPI";
import {
  getListConfig,
  getListConfigResult,
  createConfig,
  updateConfig,
  deleteConfig,
  getFieldsObject,
  getFieldsObjectResult,
  setShowModalConfig,
} from "redux/slices/approvalProcesses";

export default function* campaign() {
  yield takeLatest(getListConfig.type, GetListConfig);
  yield takeLatest(getFieldsObject.type, GetFieldsObject);
  yield takeLatest(createConfig.type, CreateConfig);
  yield takeLatest(deleteConfig.type, DeleteConfig);
  yield takeLatest(updateConfig.type, UpdateConfig);
}

function* GetListConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield getListConfigAPI(action.payload);
    yield put(getListConfigResult(response.data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* GetFieldsObject(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadFieldsAPI(action.payload);

    let tempOptionsFields = [];
    data.data.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  option: field?.option || [],
                });
              }
            });
          });
        }
      }
    });

    yield put(getFieldsObjectResult(tempOptionsFields));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createConfigAPI(action.payload);
    yield put(getListConfig());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield put(setShowModalConfig(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteConfigAPI(action.payload);
    yield put(getListConfig());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Delete successfully!");
    yield put(setShowModalConfig(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* UpdateConfig(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateConfigAPI(action.payload);
    yield put(getListConfig());
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield put(setShowModalConfig(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
