import { put, takeEvery, takeLatest } from "@redux-saga/core/effects";
import {
  createFieldAPI,
  createSectionAPI,
  deleteFieldAPI,
  deleteSectionAPI,
  loadFieldsAPI,
  updateFieldAPI,
  updateFieldPositionAPI,
  updatePositionSectionsAPI,
  loadRelatedObjectAPI,
  loadObjectFieldsAPI,
  loadListOfFieldKeyAPI,
} from "../api/fieldsManagementAPI";
import {
  createField,
  createFieldFail,
  createFieldSuccess,
  createSection,
  createSectionFail,
  createSectionSuccess,
  deleteField,
  deleteFieldFail,
  deleteFieldSuccess,
  deleteSection,
  deleteSectionFail,
  deleteSectionSuccess,
  loadFields,
  loadFieldsFail,
  loadFieldsSuccess,
  updateField,
  updateFieldFail,
  updateFieldSuccess,
  updatePosition,
  updatePositionFail,
  updatePositionField,
  updatePositionFieldFail,
  updatePositionFieldSuccess,
  updatePositionSuccess,
  loadRelatedObject,
  loadRelatedObjectFail,
  loadRelatedObjectSuccess,
  loadObjectFields,
  loadObjectFieldsFail,
  loadObjectFieldsSuccess,
  loadListOfFieldKey,
  loadListOfFieldKeyFail,
  loadListOfFieldKeySuccess,
} from "../slices/fieldsManagement";
import { Notification } from "components/Notification/Noti";

export default function* fieldsManagement() {
  yield takeEvery(createSection.type, CreateSection);
  yield takeEvery(loadFields.type, LoadFields);
  yield takeEvery(updatePosition.type, UpdatePositionSections);
  yield takeEvery(deleteSection.type, DeleteSection);
  yield takeEvery(createField.type, CreateField);
  yield takeEvery(updatePositionField.type, UpdateFieldPosition);
  yield takeLatest(updateField.type, UpdateField);
  yield takeEvery(deleteField.type, DeleteField);
  yield takeEvery(loadRelatedObject.type, LoadRelatedObject);
  yield takeEvery(loadObjectFields.type, LoadObjectFields);
  yield takeEvery(loadListOfFieldKey.type, LoadListOfFieldKey);
}

function* CreateSection(action) {
  try {
    const response = yield createSectionAPI(action.payload.data);
    yield put(createSectionSuccess(response.data));
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    yield put(createSectionFail(error.response.data.error));
  }
}

function* LoadFields(action) {
  let order = [];
  let sections = {};
  let listIDofSection = {};
  try {
    const response = yield loadFieldsAPI(action.payload);
    // eslint-disable-next-line
    response.data.data.map((item, idx) => {
      sections[`${item.name}`] = item.fields;
      listIDofSection[`${item.name}`] = item._id;
      order.push(item.name);
    });
    yield put(loadFieldsSuccess({ order, sections, listIDofSection }));
  } catch (error) {
    yield put(loadFieldsFail());
  }
}

function* UpdatePositionSections(action) {
  try {
    const response = yield updatePositionSectionsAPI({
      sections: action.payload.sections,
    });
    yield put(updatePositionSuccess(response.data));
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(updatePositionFail(error.response.data.error));
  }
}

function* DeleteSection(action) {
  try {
    const response = yield deleteSectionAPI(action.payload.data);
    yield put(deleteSectionSuccess(response.data));
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    yield put(deleteSectionFail(error.response.data.error));
  }
}

function* CreateField(action) {
  try {
    const response = yield createFieldAPI(action.payload.data);
    yield put(createFieldSuccess(response.data));
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    yield put(createFieldFail(error.response.data.error));
  }
}

function* UpdateFieldPosition(action) {
  try {
    const response = yield updateFieldPositionAPI(action.payload.data);
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
    yield put(updatePositionFieldSuccess(response.data));
  } catch (error) {
    yield put(updatePositionFieldFail(error.response.data.error));
  }
}

function* UpdateField(action) {
  let order = [];
  let sections = {};
  let listIDofSection = {};

  try {
    const response = yield updateFieldAPI(action.payload.data);
    yield put(updateFieldSuccess(response.data));
    const responseData = yield loadFieldsAPI(action.payload);
    // eslint-disable-next-line
    responseData.data.data.map((item, idx) => {
      sections[`${item.name}`] = item.fields;
      listIDofSection[`${item.name}`] = item._id;
      order.push(item.name);
    });
    yield put(loadFieldsSuccess({ order, sections, listIDofSection }));
  } catch (error) {
    // Notification("error", error.response.data.error);
    yield put(updateFieldFail(error.response.data.error));
  }
}

function* DeleteField(action) {
  try {
    const response = yield deleteFieldAPI(action.payload.data);
    yield put(deleteFieldSuccess(response.data));
    yield put(
      loadFields({
        object_id: action.payload.object_id,
      })
    );
  } catch (error) {
    yield put(deleteFieldFail(error.response.data.error));
  }
}

function* LoadRelatedObject(action) {
  try {
    const response = yield loadRelatedObjectAPI(action.payload);
    yield put(loadRelatedObjectSuccess(response.data.data));
  } catch (error) {
    yield put(loadRelatedObjectFail());
  }
}

function* LoadObjectFields(action) {
  try {
    const response = yield loadObjectFieldsAPI(action.payload);
    yield put(loadObjectFieldsSuccess(response.data.data));
  } catch (error) {
    yield put(loadObjectFieldsFail());
  }
}

function* LoadListOfFieldKey(action) {
  try {
    const response = yield loadListOfFieldKeyAPI(action.payload);
    yield put(loadListOfFieldKeySuccess(response.data.data));
  } catch (error) {
    yield put(loadListOfFieldKeyFail());
  }
}
