import { put, takeEvery } from "@redux-saga/core/effects";
import {
  createReportAPI,
  deleteReportAPI,
  loadDetailsReportAPI,
  runReportAPI,
  updateReportAPI,
  loadDataReportAPI,
  exportCustomReportAPI,
  cancelReportAPI,
} from "redux/api/reportAPI";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import {
  cancelReport,
  createCustomReport,
  deleteCustomReport,
  exportCustomReport,
  loadDataReport,
  loadDataReportSuccess,
  loadDetailCustomReport,
  loadDetailCustomReportSuccess,
  runReport,
  runReportSuccess,
  updateCustomReport,
  updateIsRun,
} from "redux/slices/customReport";
import { setShowLoadingScreen } from "redux/slices/global";
import fileDownload from "js-file-download";

export default function* customReportSaga() {
  yield takeEvery(createCustomReport.type, CreateCustomReport);
  yield takeEvery(updateCustomReport.type, UpdateCustomReport);
  yield takeEvery(deleteCustomReport.type, DeleteCustomReport);
  yield takeEvery(loadDetailCustomReport.type, LoadDetailCustomReport);
  yield takeEvery(runReport.type, RunReport);
  yield takeEvery(loadDataReport.type, LoadDataReport);
  yield takeEvery(exportCustomReport.type, ExportCustomReport);
  yield takeEvery(cancelReport.type, CancelReport);
}

function* CreateCustomReport(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield createReportAPI(action.payload);
    Notification("success", "Create report success");
    window.open(`${FE_URL}/custom-report/${data.data.report_id}`, "_self");
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* UpdateCustomReport(action) {
  try {
    yield put(setShowLoadingScreen(true));

    yield updateReportAPI(action.payload);
    Notification("success", "Update report success");
    yield put(
      loadDataReport({
        report_id: action.payload.report_id,
      })
    );
    yield put(
      loadDetailCustomReport({
        report_id: action.payload.report_id,
      })
    );
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
function* DeleteCustomReport(action) {
  try {
    yield deleteReportAPI(action.payload);
    Notification("success", "Delete report success");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
function* LoadDetailCustomReport(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadDetailsReportAPI(action.payload);
    yield put(loadDetailCustomReportSuccess(data.data.report_info));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));

    Notification("error", error.response.data.error);
  }
}
function* RunReport(action) {
  try {
    const { data } = yield runReportAPI(action.payload);
    yield put(updateIsRun(true));
    yield put(runReportSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* LoadDataReport(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield loadDataReportAPI(action.payload);
    yield put(loadDataReportSuccess(data.data));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Load data successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ExportCustomReport(action) {
  try {
    const response = yield exportCustomReportAPI(action.payload.data);
    Notification("success", "Download successfully!");
    if (action.payload.data.report_type !== "pdf") {
      fileDownload(
        response.data,
        `${action.payload.name}.${action.payload.data.report_type}`
      );
    } else {
      const blob = new Blob([response.data], { type: "application/pdf" });
      const url = URL.createObjectURL(blob);
      const link = document.createElement("a");
      link.href = url;
      link.download = `${action.payload.name}.pdf`;
      document.body.appendChild(link);
      link.click();
      URL.revokeObjectURL(url);
      document.body.removeChild(link);
    }
  } catch (error) {
    error.response.data.text().then((err) => {
      Notification("error", JSON.parse(err).error);
    });
  }
}

function* CancelReport(action) {
  try {
    yield cancelReportAPI(action.payload);
    yield put(updateIsRun(false));
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
