import { takeLatest, put, select } from "@redux-saga/core/effects";
import {
  deleteArticle,
  getArticleById,
  getArticleByIdSuccess,
  getOptionCategory,
  getOptionCategorySuccess,
  unmountArticle,
} from "redux/slices/articles";
import { loadValueAPI } from "redux/api/picklistAPI";
import { LoadRecordDataApi } from "redux/api/emailComponentAPIs";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import { deleteRecordAPI } from "redux/api/objectsAPIs";
import { Notification } from "components/Notification/Noti";
import { createRecordSuccess } from "redux/slices/objects";
const knowledgeBaseSettingReducer = (state) =>
  state.knowledgeBaseSettingReducer;
export default function* ArticleSaga() {
  yield takeLatest(getOptionCategory.type, getOptionCategorySaga);
  yield takeLatest(getArticleById.type, getArticleByIdSaga);
  yield takeLatest(deleteArticle.type, deleteArticleSaga);
  yield takeLatest(createRecordSuccess.type, createRecordSuccessSaga);
}

function* getOptionCategorySaga(action) {
  try {
    const { data } = yield loadValueAPI(action.payload);
    let arr = [];
    // eslint-disable-next-line
    Object.keys(data.data).forEach((key) => {
      if (data.data[key]) {
        arr.push({
          label: key,
          value: key,
        });
      }
    });
    yield put(getOptionCategorySuccess(arr));
  } catch (error) {}
}

function* getArticleByIdSaga(action) {
  try {
    const { data } = yield LoadRecordDataApi(action.payload.data);
    const { editKnowledgeBaseSetting } = yield select(
      knowledgeBaseSettingReducer
    );
    let payload = {};
    // eslint-disable-next-line
    Object.keys(editKnowledgeBaseSetting).forEach((key) => {
      if (data.data[editKnowledgeBaseSetting[key]]) {
        payload[key] = data.data[editKnowledgeBaseSetting[key]].value;
      }
    });
    payload.created_by = data.data.created_by;
    payload.created_date = data.data.created_date;
    payload.modify_by = data.data.modify_by;
    payload.modify_time = data.data.modify_time;
    yield put(
      getArticleByIdSuccess({
        ...payload,
        _id: data.data._id,
      })
    );
  } catch (error) {
    if (error.response.data.error === "Record data is empty") {
      yield action.payload.navigate(
        "/knowledge-base-view?object_id=" + action.payload.data.object_id
      );
    }
  }
}

function* deleteArticleSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteRecordAPI(action.payload.data);
    yield put(setShowModalConfirmDelete(false));
    yield put(setShowLoadingScreen(false));
    yield action.payload.navigate(
      "/knowledge-base-view?object_id=" + action.payload.data.object_id
    );
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", "Something went wrong");
  }
}

function* createRecordSuccessSaga(action) {
  try {
    yield put(unmountArticle());
  } catch (error) {}
}
