import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  createSMSTemplateAPI,
  deleteSMSTemplateAPI,
  getListSMSTemplateAPI,
  getSMSTemplateByIdAPI,
  updateSMSTemplateAPI,
} from "redux/api/smsTemplateAPIs";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import { loadListObjectFieldSuccess } from "redux/slices/objects";
import { loadObject } from "redux/slices/objectsManagement";
import {
  createSMSTemplate,
  deleteSMSTemplate,
  getListSMSTemplate,
  getListSMSTemplateSuccess,
  getSMSTemplateByIdSuccess,
  setShowDrawer,
  getSMSTemplateById,
  updateSMSTemplate,
} from "../slices/smsTemplate";
export default function* smsTemplateSaga() {
  yield takeLatest(getListSMSTemplate.type, getListSMSTemplateSaga);
  yield takeLatest(createSMSTemplate.type, createSMSTemplateSaga);
  yield takeLatest(deleteSMSTemplate.type, deleteSMSTemplateSaga);
  yield takeLatest(getSMSTemplateById.type, getSMSTemplateByIdSaga);
  yield takeLatest(updateSMSTemplate.type, updateSMSTemplateSaga);
}

function* getListSMSTemplateSaga() {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListSMSTemplateAPI();
    yield put(getListSMSTemplateSuccess(data.data));
    yield put(setShowLoadingScreen(false));
  } catch (error) {}
}

function* createSMSTemplateSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createSMSTemplateAPI(action.payload);
    yield put(setShowDrawer(false));
    yield put(setShowLoadingScreen(false));
    yield put(getListSMSTemplate());
    yield put(loadListObjectFieldSuccess([]));
    Notification("success", "Tạo thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* deleteSMSTemplateSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteSMSTemplateAPI(action.payload);
    yield put(setShowModalConfirmDelete(false));
    yield put(setShowLoadingScreen(false));
    yield put(getListSMSTemplate());
    Notification("success", "Xoá thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", "Something went wrong");
  }
}

function* getSMSTemplateByIdSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getSMSTemplateByIdAPI(action.payload);
    yield put(loadObject());
    yield put(getSMSTemplateByIdSuccess(data.data[0]));
    yield put(setShowLoadingScreen(false));
    yield put(setShowDrawer(true));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", "Something went wrong");
  }
}

function* updateSMSTemplateSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateSMSTemplateAPI(action.payload);
    yield put(getListSMSTemplate());
    yield put(setShowDrawer(false));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Cập nhật thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
