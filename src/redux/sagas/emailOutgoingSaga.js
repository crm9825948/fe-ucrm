import { put, takeLatest } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import {
  getListEmailOutgoingAPI,
  addNewEmailOutgoingAPI,
  deleteEmailOutgoingAPI,
  getEmailOutgoingByIdAPI,
  updateEditEmailOutgoingAPI,
  setDefaultOutgoingConfigAPI,
  getSenderAPI,
} from "redux/api/emailOutgoingAPIs";
import {
  addNewEmailOutgoing,
  getListEmailOutgoing,
  getListEmailOutgoingSuccess,
  updateEditEmail,
  deleteEmailOutgoing,
  updateMailBoxOutgoing,
  setShowDrawer,
  updateEditEmailSuccess,
  setDefaultEmailOutgoing,
  getSender,
  getSenderResult,
} from "redux/slices/emailOutgoing";
import {
  setShowModalConfirmDelete,
  setShowLoadingScreen,
} from "redux/slices/global";
export const jwt = require("jsonwebtoken");

export default function* emailOutgoingSaga() {
  yield takeLatest(getListEmailOutgoing.type, getListEmailOutgoingSaga);
  yield takeLatest(addNewEmailOutgoing.type, addNewEmailOutgoingSaga);
  yield takeLatest(updateEditEmail.type, updateEmailOutgoingSaga);
  yield takeLatest(deleteEmailOutgoing.type, deleteEmailOutgoingSaga);
  yield takeLatest(updateMailBoxOutgoing.type, updateMailBoxOutgoingSaga);
  yield takeLatest(setDefaultEmailOutgoing.type, setDefaultEmailOutgoingSaga);
  yield takeLatest(getSender.type, GetSender);
}

function* getListEmailOutgoingSaga() {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getListEmailOutgoingAPI();
    yield put(setShowLoadingScreen(false));
    yield put(getListEmailOutgoingSuccess(data.data));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* GetSender(action) {
  try {
    const { data } = yield getSenderAPI(action.payload);
    yield put(getSenderResult(data.data));
  } catch (error) {
    yield put(getSenderResult([]));
    Notification("error", error.response.data.error);
  }
}

function* addNewEmailOutgoingSaga(action) {
  try {
    let payload = {
      ...action.payload,
      ssl_type:
        action.payload.server_type === "google"
          ? null
          : action.payload.ssl_type,
      pass_word:
        action.payload.server_type === "owa" ||
        action.payload.server_type === "custom"
          ? jwt.sign(
              { password: action.payload.pass_word },
              "RANDOM_VARIABLE",
              {
                algorithm: "HS256",
              }
            )
          : "",
    };
    if (payload.port === "custom") {
      payload.port = payload.port_custom;
    }
    delete payload.port_custom;
    yield addNewEmailOutgoingAPI(payload);
    yield put(getListEmailOutgoing());
    yield put(setShowDrawer(false));
    Notification("success", "Thêm thành công");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* updateEmailOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const { data } = yield getEmailOutgoingByIdAPI(action.payload);
    let payload = {
      ...data.data[0],
      // pass_word:
      //   data.data[0].server_type === "custom"
      //     ? jwt.verify(data.data[0].pass_word, "basebsmail").password
      //     : "",
      server_type:
        !data.data[0].server_type || data.data[0].server_type.length === 0
          ? "custom"
          : data.data[0].server_type,
    };
    if (payload.port !== 23 && payload.port !== 465 && payload.port !== 587) {
      payload.port_custom = payload.port;
      payload.port = "custom";
    }
    yield put(updateEditEmailSuccess(payload));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    Notification("error", error.response.data.error);
    yield put(setShowLoadingScreen(false));
  }
}

function* deleteEmailOutgoingSaga(action) {
  try {
    yield deleteEmailOutgoingAPI(action.payload);
    yield put(setShowModalConfirmDelete(false));
    yield put(getListEmailOutgoing());
    Notification("success", "Xoá thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* updateMailBoxOutgoingSaga(action) {
  try {
    yield put(setShowLoadingScreen(true));
    let payload = {
      _id: action.payload._id,
      data: {
        ...action.payload,
        ssl_type:
          action.payload.server_type === "google"
            ? null
            : action.payload.ssl_type,
        pass_word:
          action.payload.server_type === "owa" ||
          action.payload.server_type === "custom"
            ? jwt.sign(
                { password: action.payload.pass_word },
                "RANDOM_VARIABLE",
                {
                  algorithm: "HS256",
                }
              )
            : "",
      },
    };
    if (payload.data.port === "custom") {
      payload.data.port = payload.data.port_custom;
    }
    delete payload.data._id;
    delete payload.data.port_custom;
    !action.payload.pass_word && delete payload.data.pass_word;
    yield updateEditEmailOutgoingAPI(payload);
    yield put(setShowDrawer(false));
    yield put(getListEmailOutgoing());
    Notification("success", "Cập nhật thành công");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* setDefaultEmailOutgoingSaga(action) {
  try {
    yield setDefaultOutgoingConfigAPI(action.payload);
    yield put(getListEmailOutgoing());
    Notification("success", "Make default successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}
