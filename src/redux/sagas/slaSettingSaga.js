import { put, takeLatest, delay } from "@redux-saga/core/effects";
import { Notification } from "components/Notification/Noti";
import { setShowLoadingScreen } from "redux/slices/global";
import { setShowModalConfirm } from "redux/slices/global";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { loadObjectFields } from "redux/slices/fieldsManagement";
import { loadListObjectField } from "redux/slices/objects";
import { loadAllUser } from "redux/slices/user";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { FE_URL } from "constants/constants";

import {
  loadListSLAAPI,
  activeSLAAPI,
  duplicateSLAAPI,
  deleteSLAAPI,
  createSLAAPI,
  loadDetailsSLAAPI,
  updateSLAAPI,
  notificationSLAAPI,
} from "redux/api/slaSettingAPI";
import {
  loadListSLA,
  loadListSLASuccess,
  activeSLA,
  changeState,
  duplicateSLA,
  deleteSLA,
  loadFieldsObject,
  loadDataNecessaryEdit,
  createSLASetting,
  loadDetailsSLA,
  loadDetailsSLASuccess,
  editSLASetting,
  changeStateEdit,
  toggleNotification,
} from "redux/slices/slaSetting";

export default function* slaSetting() {
  yield takeLatest(loadListSLA.type, LoadListSLA);
  yield takeLatest(activeSLA.type, ActiveSLA);
  yield takeLatest(duplicateSLA.type, DuplicateSLA);
  yield takeLatest(deleteSLA.type, DeleteSLA);
  yield takeLatest(loadFieldsObject.type, LoadFieldsObject);
  yield takeLatest(loadDataNecessaryEdit.type, LoadDataNecessaryEdit);
  yield takeLatest(createSLASetting.type, CreateSLASetting);
  yield takeLatest(loadDetailsSLA.type, LoadDetailsSLA);
  yield takeLatest(editSLASetting.type, EditSLASetting);
  yield takeLatest(toggleNotification.type, ToggleNotification);
}

function* LoadListSLA(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadListSLAAPI(action.payload);
    yield put(loadListSLASuccess(response.data.data));
    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadDataNecessaryEdit(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
    yield put(getListEmailTemplate());
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* ActiveSLA(action) {
  try {
    yield activeSLAAPI(action.payload);
    yield put(
      changeState({
        type: "change-status",
        id: action.payload._id,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* ToggleNotification(action) {
  try {
    yield notificationSLAAPI(action.payload);
    yield put(
      changeState({
        type: "change-notification",
        id: action.payload._id,
      })
    );
    Notification("success", "Change status successfully!");
  } catch (error) {
    Notification("error", error.response.data.error);
  }
}

function* DuplicateSLA(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield duplicateSLAAPI(action.payload);
    yield put(
      loadListSLA({
        current_page: 1,
        record_per_page: 10,
        api_version: "2",
      })
    );
    yield put(setShowModalConfirm(false));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Duplicate SLA successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* DeleteSLA(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield deleteSLAAPI(action.payload);
    yield put(
      loadListSLA({
        current_page: 1,
        record_per_page: 10,
        api_version: "2",
      })
    );
    yield put(setShowLoadingScreen(false));
    yield put(setShowModalConfirmDelete(false));
    Notification("success", "Delete successfully!");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadFieldsObject(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield put(loadObjectFields(action.payload));
    yield put(
      loadListObjectField({
        ...action.payload,
        show_meta_fields: true,
      })
    );
    yield put(changeStateEdit(true));
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* CreateSLASetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield createSLAAPI(action.payload);
    yield put(changeStateEdit(false));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Create successfully!");
    yield delay(500);
    window.open(`${FE_URL}/sla-setting`, "_self");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}

function* LoadDetailsSLA(action) {
  try {
    yield put(setShowLoadingScreen(true));
    const response = yield loadDetailsSLAAPI(action.payload);

    response.data.data.sla_rules.forEach((item, idx) => {
      response.data.data.sla_rules[idx] = {
        ...response.data.data.sla_rules[idx],
        time_to_completion: {
          days: Math.floor(item.time_to_completion / (60 * 24)),
          hours: Math.floor((item.time_to_completion % (60 * 24)) / 60),
          minutes: (item.time_to_completion % (60 * 24)) % 60,
        },
      };

      item.alert_configs.forEach((ele, index) => {
        response.data.data.sla_rules[idx].alert_configs[index] = {
          ...response.data.data.sla_rules[idx].alert_configs[index],
          time_before_violation: {
            days: Math.floor(ele.time_before_violation / (60 * 24)),
            hours: Math.floor((ele.time_before_violation % (60 * 24)) / 60),
            minutes: (ele.time_before_violation % (60 * 24)) % 60,
          },
        };
      });
    });

    response.data.data.escalate_rules.forEach((item, idx) => {
      response.data.data.escalate_rules[idx] = {
        ...response.data.data.escalate_rules[idx],
        time_after_violation: {
          days: Math.floor(item.time_after_violation / (60 * 24)),
          hours: Math.floor((item.time_after_violation % (60 * 24)) / 60),
          minutes: (item.time_after_violation % (60 * 24)) % 60,
        },
      };
    });

    // // eslint-disable-next-line array-callback-return
    // response.data.data.shift_work.map((item, idx) => {
    //   response.data.data.shift_work[idx] = {
    //     time: [
    //       String(response.data.data.shift_work[idx].from_time.hour).padStart(
    //         2,
    //         "0"
    //       ) +
    //         ":" +
    //         String(
    //           response.data.data.shift_work[idx].from_time.minute
    //         ).padStart(2, "0"),
    //       String(response.data.data.shift_work[idx].to_time.hour).padStart(
    //         2,
    //         "0"
    //       ) +
    //         ":" +
    //         String(response.data.data.shift_work[idx].to_time.minute).padStart(
    //           2,
    //           "0"
    //         ),
    //     ],
    //   };
    // });

    response.data.data.shift_work.forEach((item, idx) => {
      let tempDays = [];
      item.applicable_weekdays.forEach((day, index) => {
        if (day) {
          tempDays.push(index);
        }
      });

      response.data.data.shift_work[idx] = {
        time: [
          String(response.data.data.shift_work[idx].from_time.hour).padStart(
            2,
            "0"
          ) +
            ":" +
            String(
              response.data.data.shift_work[idx].from_time.minute
            ).padStart(2, "0"),
          String(response.data.data.shift_work[idx].to_time.hour).padStart(
            2,
            "0"
          ) +
            ":" +
            String(response.data.data.shift_work[idx].to_time.minute).padStart(
              2,
              "0"
            ),
        ],
        day: [...tempDays],
      };
    });

    yield put(loadDetailsSLASuccess(response.data.data));

    yield delay(500);
    yield put(setShowLoadingScreen(false));
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    yield put(loadDetailsSLASuccess({}));
    Notification("error", error.response.data.error);
  }
}

function* EditSLASetting(action) {
  try {
    yield put(setShowLoadingScreen(true));
    yield updateSLAAPI(action.payload);
    yield put(changeStateEdit(false));
    yield put(setShowLoadingScreen(false));
    Notification("success", "Update successfully!");
    yield delay(500);
    yield put(loadDetailsSLASuccess({}));
    window.open(`${FE_URL}/sla-setting`, "_self");
  } catch (error) {
    yield put(setShowLoadingScreen(false));
    Notification("error", error.response.data.error);
  }
}
