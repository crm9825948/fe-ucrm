import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  objectSearch: {},
  isLoadMore: false,
  dataSearch: {
    page: 0,
    size: 10,
    text_fields: ["title", "body", "tag", "article_id"],
    text_search: "",
    created_date: "all",
  },
};
const slice = createSlice({
  name: "enhancementComponent",
  initialState,
  reducers: {
    updateDataSearch(state, action) {
      Object.entries(action.payload).forEach(([key, value]) => {
        state.dataSearch[key] = value;
      });
    },
    getListArticle() {},
    getListArticleSuccess(state, action) {
      state.objectSearch = action.payload;
    },
    updateLoadMore(state, action) {
      state.isLoadMore = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  updateDataSearch,
  getListArticle,
  getListArticleSuccess,
  updateLoadMore,
} = slice.actions;
