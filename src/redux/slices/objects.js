import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: true,
  error: false,
  objectStatus: null,
  statusCode: null,
  objectsFavourite: [],
  //
  fields: [],
  originalFields: [],
  rootFields: [],
  userAssignTo: [],
  header: [],
  data: [],
  searchList: {},
  selectedRowKeys: [],
  totalRecord: null,
  totalPage: 0,
  //
  linkingFieldValue: {},
  users: {},
  //data
  dataLinking: [],
  headerLinking: [],
  totalRecordLinking: 0,
  totalPageLinking: 0,
  //
  recordData: {},
  //
  target: [],
  //
  recordToMerge: {},
  listObjectField: [],
  //
  hiddenDynamic: [],
  hiddenArray: [],
  //custom view
  customView: [],
  customViewDetail: {},
  loadingDetails: false,
  loadingUpdateCustomview: false,

  isLoadingData: false,
  isLoadingDelete: false,

  //source
  sourceObjects: [],
  //target
  targetObjects: [],
  showModal: false,
  //loading mapping
  isLoadingMapping: false,

  //loading logs
  isLoadingLogs: false,
  logs: [],
  isLoadLinking: false,
  isLoadingUpdateRecord: null,
  isLoadingCreateRecord: null,
  isLoadingDeleteRecord: null,
  listViewWithDetail: [],
  loadListViewDetails: false,
  updateListViewDetail: false,
  isLoadingRecycleBin: false,
  dataRecycleBinListView: [],
  isRestore: false,
  isMultipleDelete: false,
  isOpenDuplicate: false,
  listDuplicate: [],
  isLoadingDataWithOutPagi: false,
  isLoadingDataLinking: false,
  isLoadingPagi: false,
  total_record: null,
  manualSharing: {},
  isLoadingModal: false,
  isLoadingCreateTemplate: false,
  isLoadingUpdateTemplate: false,
  isLoadingLoadTemplate: false,
  isLoadingDeleteTemplate: false,
  template: [],
  objectGlobalSearch: {},
  objectLinkingGlobalSearch: {},
  isSeacrhGlobal: false,
  customViewPermission: {},
};

const slice = createSlice({
  name: "objects",
  initialState,
  reducers: {
    //
    deleteTemplate(state, action) {
      state.isLoadingDeleteTemplate = true;
    },
    deleteTemplateSuccess(state, action) {
      state.isLoadingDeleteTemplate = false;
    },
    deleteTemplateFail(state, action) {
      state.isLoadingDeleteTemplate = false;
    },
    makeDefaultTemplate(state, action) {},
    updateStatusTemplate(state, action) {},

    updateTemplateUpdateFail(state, action) {
      state.isLoadingUpdateTemplate = false;
    },
    updateTemplateSuccess(state, action) {
      state.isLoadingUpdateTemplate = false;
    },
    updateTemplate(state, action) {
      state.isLoadingUpdateTemplate = true;
    },
    loadTemplate(state, action) {
      state.isLoadingLoadTemplate = true;
    },
    loadTemplateSuccess(state, action) {
      state.isLoadingLoadTemplate = false;
      state.template = action.payload;
    },
    loadTemplateFail(state, action) {
      state.isLoadingLoadTemplate = false;
    },
    setListDuplicate(state, action) {
      state.listDuplicate = action.payload;
    },
    setIsOpenDuplicate(state, action) {
      state.isOpenDuplicate = action.payload;
    },
    //Load favourite object
    loadFavouriteObjects(state, action) {
      state.isLoading = true;
    },
    loadFavouriteObjectsSuccess(state, action) {
      state.isLoading = false;
      state.objectsFavourite = action.payload;
    },
    loadFavouriteObjectsFail(state, action) {
      state.isLoading = false;
    },
    //toggle favourite object
    toggleFavouriteObject(state, action) {
      state.isLoading = true;
    },
    toggleFavouriteObjectSuccess(state, action) {
      state.isLoading = false;
    },
    toggleFavouriteObjectFail(state, action) {
      state.isLoading = false;
    },
    //load-form-create
    loadFormCreate(state, action) {
      state.isLoading = true;
    },
    loadFormCreateSuccess(state, action) {
      state.isLoading = false;
      state.fields = action.payload.data;
      state.originalFields = action.payload.hiddenFields;
      state.rootFields = action.payload.data;
      state.hiddenDynamic = action.payload.hiddenDynamic;
      state.hiddenArray = action.payload.hiddenDynamic;
    },
    loadFormCreateFail(state, action) {
      state.isLoading = false;
    },
    //set field
    setFields(state, action) {
      state.fields = action.payload;
    },
    //load user assign to
    loadUserAssignTo(state, action) {
      state.isLoading = true;
    },
    loadUserAssignToSuccess(state, action) {
      state.isLoading = false;
      state.userAssignTo = action.payload;
    },
    loadUserAssignToFail(state, action) {
      state.isLoading = false;
    },

    //Create record
    createRecord(state, action) {
      state.isLoading = true;
      state.isLoadingCreateRecord = true;
    },
    createRecordSuccess(state, action) {
      state.isLoading = false;
      state.isLoadingCreateRecord = false;
    },
    createRecordFail(state, action) {
      state.isLoading = false;
      state.isLoadingCreateRecord = false;
    },

    //load headers
    loadHeader(state, action) {
      // state.isLoading = true;
    },
    loadHeaderSuccess(state, action) {
      // state.isLoading = false;
      state.header = action.payload;
    },
    loadHeaderFail(state, action) {
      // state.isLoading = false;
    },

    //load headers linking
    loadHeaderLinking(state, action) {
      // state.isLoading = false;
    },
    loadHeaderSuccessLinking(state, action) {
      // state.isLoading = false;
      state.headerLinking = action.payload;
    },
    loadHeaderFailLinking(state, action) {
      // state.isLoading = false;
    },

    //load-data
    loadData(state, action) {
      // state.isLoading = true;
    },
    loadDataSuccess(state, action) {
      // state.isLoading = false;
      state.data = action.payload;
    },
    loadDataFail(state, action) {
      // state.isLoading = false;
    },

    //load-data-linking
    loadDataLinking(state, action) {
      state.isLoadingDataLinking = true;
    },
    loadDataSuccessLinking(state, action) {
      state.isLoadingDataLinking = false;
      state.dataLinking = action.payload;
    },
    loadDataFailLinking(state, action) {
      state.isLoadingDataLinking = false;
    },

    //search list
    setSearchList(state, action) {
      state.searchList = action.payload;
    },

    //selectedRowKeys
    setSelectedRowKeys(state, action) {
      state.selectedRowKeys = action.payload;
    },

    //load totalRecord
    loadPagination(state, action) {
      state.isLoadingPagi = true;
    },
    loadPaginationSuccess(state, action) {
      state.isLoadingPagi = false;
      state.totalPage = action.payload.total_page;
      state.totalRecord = action.payload.total_record;
    },
    loadPaginationFail(state, action) {
      state.isLoadingPagi = false;
    },

    //load totalRecord linking
    loadPaginationLinking(state, action) {
      // state.isLoading = false;
    },
    loadPaginationSuccessLinking(state, action) {
      // state.isLoading = false;
      state.totalPageLinking = action.payload.total_page;
      state.totalRecordLinking = action.payload.total_record;
    },
    loadPaginationFailLinking(state, action) {
      // state.isLoading = false;
    },

    //load user
    loadUser(state, action) {
      // state.isLoading = false;
    },
    loadUserMore() {},
    loadUserMoreSuccess(state, action) {
      state.users.records = state.users.records.concat(action.payload);
    },
    searchUser() {},
    loadUserSuccess(state, action) {
      // state.isLoading = false;
      state.users = action.payload;
    },
    loadUserFail(state, action) {
      // state.isLoading = false;
    },

    //loadLinkingFieldValue
    loadLinkingFieldValue(state, action) {
      // state.isLoading = false;
      state.isLoadLinking = true;
    },
    loadLinkingFieldValueSuccess(state, action) {
      // state.isLoading = false;
      state.linkingFieldValue = action.payload;
      state.isLoadLinking = false;
    },
    loadLinkingFieldValueFail(state, action) {
      // state.isLoading = false;
      state.isLoadLinking = false;
    },

    //setLinkingFieldValue
    setLinkingFieldValue(state, action) {
      state.linkingFieldValue = action.payload;
    },

    //record data
    loadRecordData(state, action) {
      state.isLoading = false;
    },
    loadRecordDataSuccess(state, action) {
      state.isLoading = false;
      state.recordData = action.payload;
    },
    loadRecordDataFail(state, action) {
      state.isLoading = false;
    },

    //update record
    updateRecord(state, action) {
      state.isLoading = true;
      state.isLoadingUpdateRecord = true;
    },
    updateRecordSuccess(state, action) {
      state.isLoading = false;
      state.isLoadingUpdateRecord = false;
    },
    updateRecordFail(state, action) {
      state.isLoading = false;
      state.isLoadingUpdateRecord = false;
    },

    //delete record
    deleteRecord(state, action) {
      state.isLoading = true;
      state.isLoadingDeleteRecord = true;
    },
    deleteRecordSuccess(state, action) {
      state.isLoading = false;
      state.isLoadingDeleteRecord = false;
    },
    deleteRecordFail(state, action) {
      state.isLoading = false;
      state.isLoadingDeleteRecord = false;
    },

    //mass delete
    massDelete(state, action) {
      state.isLoading = true;
    },
    massDeleteSuccess(state, action) {
      state.isLoading = false;
    },
    massDeleteFail(state, action) {
      state.isLoading = false;
    },

    //load target
    loadTarget(state, action) {
      state.isLoading = false;
    },
    loadTargetSucces(state, action) {
      state.isLoading = false;
      state.target = action.payload;
    },
    loadTargetFail(state, action) {
      state.isLoading = false;
    },

    //mass edit
    massEdit(state, action) {
      state.isLoading = true;
    },
    massEditSuccess(state, action) {
      state.isLoading = false;
    },
    massEditFail(state, action) {
      state.isLoading = false;
    },

    //get record
    getRecords(state, action) {
      state.isLoading = false;
    },
    getRecordsSuccess(state, action) {
      state.isLoading = false;
      state.recordToMerge = action.payload;
    },
    getRecordsFail(state, action) {
      state.isLoading = false;
    },

    //merge record
    mergeRecord(state, action) {
      state.isLoading = true;
    },
    mergeRecordSuccess(state, action) {
      state.isLoading = false;
    },
    mergeRecordFail(state, action) {
      state.isLoading = false;
    },
    loadListObjectField(state, action) {},
    loadListObjectFieldSuccess(state, action) {
      state.listObjectField = action.payload;
    },
    setHiddenArray(state, action) {
      state.hiddenArray = action.payload;
    },

    //customview
    createCustomView(state, action) {
      state.isLoading = true;
    },
    createCustomViewSuccess(state, action) {
      state.isLoading = false;
    },
    createCustomViewFail(state, action) {
      state.isLoading = false;
    },

    //load custom view
    loadCustomView(state, action) {
      // state.isLoading = true;
    },
    loadCustomViewSuccess(state, action) {
      // state.isLoading = false;
      state.customView = action.payload;
    },
    loadCustomViewFail(state, action) {
      // state.isLoading = false;
      state.customView = [];
    },

    //load details custom view
    loadDetailsCustomView(state, action) {
      state.loadingDetails = true;
    },
    loadDetailsCustomViewSuccess(state, action) {
      state.loadingDetails = false;
      state.customViewDetail = action.payload;
    },
    loadDetailsCustomViewFail(state, action) {
      state.loadingDetails = false;
    },

    //
    updateCustomView(state, action) {
      state.loadingUpdateCustomview = true;
    },
    updateCustomViewSuccess(state, action) {
      state.loadingUpdateCustomview = false;
    },
    updateCustomViewFail(state, action) {
      state.loadingUpdateCustomview = false;
    },

    //delete custom view
    deleteCustomView(state, action) {
      state.isLoadingDelete = true;
    },
    deleteCustomViewSuccess(state, action) {
      state.isLoadingDelete = false;
    },
    deleteCustomViewFail(state, action) {
      state.isLoadingDelete = false;
    },

    loadAllData(state, action) {
      state.isLoadingData = true;
    },
    loadAllDataSuccess(state, action) {
      state.isLoadingData = false;
    },
    loadAllDataFail(state, action) {
      state.isLoadingData = false;
    },

    loadDataWithOutPagi(state, action) {
      state.isLoadingDataWithOutPagi = true;
    },
    loadDataWithOutPagiSuccess(state, action) {
      state.isLoadingDataWithOutPagi = false;
    },
    loadDataWithOutPagiFail(state, action) {
      state.isLoadingDataWithOutPagi = false;
    },

    //load source
    loadSourceObjects(state, action) {},
    loadSourceObjectsSuccess(state, action) {
      state.sourceObjects = action.payload;
    },
    loadSourceObjectsFail(state, action) {},
    //load target
    loadTargetObjects(state, action) {},
    loadTargetObjectsSuccess(state, action) {
      state.targetObjects = action.payload;
    },
    loadTargetObjectsFail(state, action) {},
    setShowModal(state, action) {
      state.showModal = action.payload;
    },
    mappingFieldAndUpdate(state, action) {
      state.isLoadingMapping = true;
    },
    mappingFieldAndUpdateSuccess(state, action) {
      state.isLoadingMapping = false;
    },
    mappingFieldAndUpdateFail(state, action) {
      state.isLoadingMapping = false;
    },
    exportTemplate(state, action) {},

    viewLogs(state, action) {
      state.isLoadingLogs = true;
    },
    viewLogsSuccess(state, action) {
      state.isLoadingLogs = false;
      state.logs = action.payload;
    },
    viewLogsFail(state, action) {
      state.isLoadingLogs = false;
    },
    unmountObjects: () => initialState,
    loadListViewWithDetails(state, action) {
      state.loadListViewDetails = true;
    },
    loadListViewWithDetailsSuccess(state, action) {
      state.loadListViewDetails = false;
      state.listViewWithDetail = action.payload;
    },
    loadListViewWithDetailsFail(state, action) {
      state.loadListViewDetails = false;
    },
    updateListViewDetails(state, action) {
      state.updateListViewDetail = true;
    },
    updateListViewDetailsSuccess(state, action) {
      state.updateListViewDetail = false;
    },
    updateListViewDetailsFail(state, action) {
      state.updateListViewDetail = false;
    },
    loadDataListViewRecycleBin(state, action) {
      state.isLoadingRecycleBin = true;
    },
    loadDataListViewRecycleBinSuccess(state, action) {
      state.isLoadingRecycleBin = false;
      state.dataRecycleBinListView = action.payload;
    },
    loadDataListViewRecycleBinFail(state, action) {
      state.isLoadingRecycleBin = false;
    },
    restoreRecord(state, action) {
      state.isRestore = true;
    },
    restoreRecordSuccess(state, action) {
      state.isRestore = false;
    },
    restoreRecordFail(state, action) {
      state.isRestore = false;
    },
    deleteMultiple(state, action) {
      state.isMultipleDelete = true;
    },
    deleteMultipleSuccess(state, action) {
      state.isMultipleDelete = false;
    },
    deleteMultipleFail(state, action) {
      state.isMultipleDelete = false;
    },
    emptyTrash(state, action) {},
    getManualSharing() {},
    getManualSharingSuccess(state, action) {
      state.manualSharing = action.payload;
    },
    setLoadingModal(state, action) {
      state.isLoadingModal = action.payload;
    },
    createTemplate(state, action) {
      state.isLoadingCreateTemplate = true;
    },
    createTemplateSuccess(state, action) {
      state.isLoadingCreateTemplate = false;
    },
    createTemplateFail(state, action) {
      state.isLoadingCreateTemplate = false;
    },
    viewObjectSearch() {},
    viewObjectSearchSuccess(state, action) {
      state.objectGlobalSearch = action.payload;
    },
    viewObjectLinkingSearch() {},
    viewObjectLinkingSearchSuccess(state, action) {
      state.objectLinkingGlobalSearch = action.payload;
    },
    globalSearch() {},
    linkingGlobalSearch() {},
    lookupGlobalSearch() {},
    updateLoadingLinking(state) {
      state.isLoadingDataLinking = true;
    },
    updateLoadingData(state, action) {
      state.isLoadingModal = action.payload;
    },
    updateIsSearchGlobal(state, action) {
      state.isSeacrhGlobal = action.payload;
    },
    shareView() {},
    loadPermission() {},
    loadPermissionSucces(state, action) {
      state.customViewPermission = action.payload;
    },
    updateDefaultView() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  createTemplate,
  createTemplateSuccess,
  createTemplateFail,
  loadDataWithOutPagi,
  loadDataWithOutPagiSuccess,
  loadDataWithOutPagiFail,
  loadAllData,
  loadAllDataSuccess,
  loadAllDataFail,
  loadFavouriteObjects,
  loadFavouriteObjectsFail,
  loadFavouriteObjectsSuccess,
  toggleFavouriteObject,
  toggleFavouriteObjectSuccess,
  toggleFavouriteObjectFail,
  loadFormCreate,
  loadFormCreateFail,
  loadFormCreateSuccess,
  setFields,
  loadUserAssignTo,
  loadUserAssignToSuccess,
  loadUserAssignToFail,
  createRecord,
  createRecordSuccess,
  createRecordFail,
  loadHeader,
  loadHeaderSuccess,
  loadHeaderFail,
  loadData,
  loadDataSuccess,
  loadDataFail,
  setSearchList,
  setSelectedRowKeys,
  loadPagination,
  loadPaginationSuccess,
  loadPaginationFail,
  loadUser,
  loadUserSuccess,
  loadUserFail,
  loadLinkingFieldValue,
  loadLinkingFieldValueSuccess,
  loadLinkingFieldValueFail,
  loadDataLinking,
  loadDataSuccessLinking,
  loadDataFailLinking,
  loadHeaderLinking,
  loadHeaderFailLinking,
  loadHeaderSuccessLinking,
  loadPaginationLinking,
  loadPaginationFailLinking,
  loadPaginationSuccessLinking,
  setLinkingFieldValue,
  loadRecordData,
  loadRecordDataSuccess,
  loadRecordDataFail,
  updateRecord,
  updateRecordSuccess,
  updateRecordFail,
  deleteRecord,
  deleteRecordFail,
  deleteRecordSuccess,
  massDelete,
  massDeleteSuccess,
  massDeleteFail,
  loadTarget,
  loadTargetSucces,
  loadTargetFail,
  massEdit,
  massEditFail,
  massEditSuccess,
  getRecords,
  getRecordsSuccess,
  getRecordsFail,
  mergeRecord,
  mergeRecordSuccess,
  mergeRecordFail,
  loadListObjectField,
  loadListObjectFieldSuccess,
  setHiddenArray,
  createCustomView,
  createCustomViewSuccess,
  createCustomViewFail,
  loadCustomView,
  loadCustomViewFail,
  loadCustomViewSuccess,
  loadDetailsCustomView,
  loadDetailsCustomViewSuccess,
  loadDetailsCustomViewFail,
  updateCustomView,
  updateCustomViewSuccess,
  updateCustomViewFail,
  deleteCustomView,
  deleteCustomViewFail,
  deleteCustomViewSuccess,
  loadSourceObjects,
  loadSourceObjectsSuccess,
  loadSourceObjectsFail,
  loadTargetObjects,
  loadTargetObjectsFail,
  loadTargetObjectsSuccess,
  setShowModal,
  loadUserMore,
  loadUserMoreSuccess,
  searchUser,
  mappingFieldAndUpdate,
  mappingFieldAndUpdateSuccess,
  mappingFieldAndUpdateFail,
  exportTemplate,
  viewLogs,
  viewLogsSuccess,
  viewLogsFail,
  unmountObjects,
  loadListViewWithDetails,
  loadListViewWithDetailsFail,
  loadListViewWithDetailsSuccess,
  updateListViewDetails,
  updateListViewDetailsFail,
  updateListViewDetailsSuccess,
  loadDataListViewRecycleBin,
  loadDataListViewRecycleBinFail,
  loadDataListViewRecycleBinSuccess,
  restoreRecord,
  restoreRecordSuccess,
  restoreRecordFail,
  deleteMultiple,
  deleteMultipleFail,
  deleteMultipleSuccess,
  emptyTrash,
  setIsOpenDuplicate,
  setListDuplicate,
  getManualSharing,
  getManualSharingSuccess,
  setLoadingModal,
  loadTemplate,
  loadTemplateSuccess,
  loadTemplateFail,
  updateTemplate,
  updateTemplateUpdateFail,
  updateTemplateSuccess,
  updateStatusTemplate,
  makeDefaultTemplate,
  deleteTemplate,
  deleteTemplateSuccess,
  deleteTemplateFail,
  viewObjectSearch,
  viewObjectSearchSuccess,
  globalSearch,
  viewObjectLinkingSearch,
  viewObjectLinkingSearchSuccess,
  linkingGlobalSearch,
  lookupGlobalSearch,
  updateLoadingLinking,
  updateLoadingData,
  updateIsSearchGlobal,
  shareView,
  loadPermission,
  loadPermissionSucces,
  updateDefaultView,
} = slice.actions;
