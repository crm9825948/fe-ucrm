import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showSetting: false,
  allSetting: [],
  totalSetting: 0,
};

const slice = createSlice({
  name: "ldap",
  initialState,
  reducers: {
    showLdapSetting(state, action) {
      state.showSetting = action.payload;
    },
    createSetting() {},
    loadAllSetting() {},
    loadAllSettingResult(state, action) {
      state.allSetting = action.payload.data;
      state.totalSetting = action.payload.total;
    },
    deleteSetting() {},
    changeState(state, action) {
      let tempList = [...state.allSetting];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.allSetting[idx] = {
              ...state.allSetting[idx],
              status: !state.allSetting[idx].status,
            };
          }
        });
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.allSetting.splice(idx, 1);
            state.totalSetting = state.totalSetting - 1;
          }
        });
      }
    },
    updateSetting() {},
    syncData() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  createSetting,
  showLdapSetting,
  loadAllSetting,
  loadAllSettingResult,
  deleteSetting,
  changeState,
  updateSetting,
  syncData,
} = slice.actions;
