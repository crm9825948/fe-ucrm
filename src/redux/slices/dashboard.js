import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  isLoadingDefaultDashboard: false,
  isLoadingUpdate: false,
  dashboards: [],
  defaultDashboard: {},
  isLoadingCreate: false,
  isLoadingDelete: false,
  listDashboard: [],
  isLoadListDashboard: false,
  isMakeDefault: false,
};

const slice = createSlice({
  name: "authenticated",
  initialState,
  reducers: {
    loadAllDashboard(state, action) {
      state.isLoadingDefaultDashboard = true;
    },
    loadAllDashboardSuccess(state, action) {
      state.isLoadingDefaultDashboard = false;
      state.dashboards = action.payload;
    },
    loadAllDashboardFail(state, action) {
      state.isLoadingDefaultDashboard = false;
    },
    //
    loadDefaultDashboard(state, action) {
      state.isLoadingDefaultDashboard = true;
    },
    loadDefaultDashboardSuccess(state, action) {
      state.isLoadingDefaultDashboard = false;
      state.defaultDashboard = action.payload;
    },
    loadDefaultDashboardFail(state, action) {
      state.isLoadingDefaultDashboard = false;
    },
    updateDashboard(state, action) {
      state.isLoadingUpdate = true;
    },
    updateDashboardSuccess(state, action) {
      state.isLoadingUpdate = false;
    },
    updateDashboardFail(state, action) {
      state.isLoadingUpdate = false;
    },
    //create dashboard
    createDashboard(state, action) {
      state.isLoadingCreate = true;
    },
    createDashboardSuccess(state, action) {
      state.isLoadingCreate = false;
    },
    createDashboardFail(state, action) {
      state.isLoadingCreate = false;
    },
    //delete dashboard
    deleteDashboard(state, action) {
      state.isLoadingDelete = true;
    },
    deleteDashboardSuccess(state, action) {
      state.isLoadingDelete = false;
    },
    deleteDashboardFail(state, action) {
      state.isLoadingDelete = false;
    },
    //update name
    updateNameDashboard(state, action) {},
    loadListDashboard(state, action) {
      state.isLoadListDashboard = true;
    },
    loadListDashboardSuccess(state, action) {
      state.isLoadListDashboard = false;
      state.listDashboard = action.payload;
    },
    loadListDashboardFail(state, action) {
      state.isLoadListDashboard = false;
    },
    //mark default
    makeDefault(state, action) {
      state.isMakeDefault = true;
    },
    makeDefaultSuccess(state, action) {
      state.isMakeDefault = false;
    },
    makeDefaultFail(state, action) {
      state.isMakeDefault = false;
    },
    unMountDashboard: () => initialState
  },
});

export default slice.reducer;

//Actions
export const {
  loadAllDashboard,
  loadAllDashboardSuccess,
  loadAllDashboardFail,
  loadDefaultDashboard,
  loadDefaultDashboardSuccess,
  loadDefaultDashboardFail,
  updateDashboard,
  updateDashboardSuccess,
  updateDashboardFail,
  createDashboard,
  createDashboardSuccess,
  createDashboardFail,
  deleteDashboard,
  deleteDashboardSuccess,
  deleteDashboardFail,
  updateNameDashboard,
  loadListDashboard,
  loadListDashboardSuccess,
  loadListDashboardFail,
  makeDefault,
  makeDefaultFail,
  makeDefaultSuccess,
  unMountDashboard,
} = slice.actions;
