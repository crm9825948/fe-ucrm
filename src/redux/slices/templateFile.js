import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showModalConfig: false,
  isLoading: {
    newFile: false,
    copyFile: false,
  },
  targetURL: "",
  listTemplate: [],
};

const slice = createSlice({
  name: "templateFile",
  initialState,
  reducers: {
    modalConfig(state, action) {
      state.showModalConfig = action.payload;
    },
    newFile(state, action) {
      state.isLoading.newFile = true;
    },
    newFileResult(state, action) {
      state.isLoading.newFile = false;
      state.targetURL = action.payload;
    },
    copyFile(state, action) {
      state.isLoading.copyFile = true;
    },
    copyFileResult(state, action) {
      state.isLoading.copyFile = false;
      state.targetURL = action.payload;
    },
    uploadTemplate() {},
    getListTemplate() {},
    getListTemplateResult(state, action) {
      state.listTemplate = action.payload;
    },
    editTemplate() {},
    deleteTemplate() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  modalConfig,
  newFile,
  newFileResult,
  copyFile,
  copyFileResult,
  uploadTemplate,
  getListTemplate,
  getListTemplateResult,
  editTemplate,
  deleteTemplate,
} = slice.actions;
