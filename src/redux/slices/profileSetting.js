const { createSlice } = require("@reduxjs/toolkit");

const initialState  = {
   userDetail: {}
}

const slice = createSlice({
    name: 'profileSetting',
    initialState,
    reducers: {
        loadProfileDetailSuccess(state, action) {
            state.userDetail = action.payload
        },
        updateFieldUserDetail(state, action) {
            state.userDetail[action.payload.key] = action.payload.value;
        }
    }
})

export const {
    updateFieldUserDetail,
    loadProfileDetailSuccess
} = slice.actions

export default slice.reducer