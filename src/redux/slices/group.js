import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  listGroups: [],
  totalRecords: 0,
  listAllGroups: [],
  groupInfo: null,
  details: {},
  isLoadingData: false,
  statusDelete: null,
};

const slice = createSlice({
  name: "group",
  initialState,
  reducers: {
    loadDataNecessary() {},
    loadGroups(state, action) {},
    loadGroupsSuccess(state, action) {
      state.listGroups = action.payload.data;
      state.totalRecords = action.payload.total_record;
    },
    loadAllGroups(state, action) {
      state.loadingScreen = true;
    },
    loadAllGroupsSuccess(state, action) {
      state.loadingScreen = false;
      state.listAllGroups = action.payload.data;
    },
    loadAllGroupsFail(state, action) {
      state.loadingScreen = false;
    },
    createGroup(state, action) {
      state.isLoading = true;
    },
    createGroupSuccess(state, action) {
      state.isLoading = false;
      state.groupInfo = "success";
    },
    createGroupFail(state, action) {
      state.isLoading = false;
      state.groupInfo = action.payload;
    },
    resetGroupInfo(state, action) {
      state.groupInfo = null;
      state.statusDelete = null;
    },
    loadDetailsGroup(state, action) {
      state.isLoadingData = true;
    },
    loadDetailsGroupSuccess(state, action) {
      state.isLoadingData = false;
      state.details = action.payload;
    },
    resetGroupDetails(state, action) {
      state.details = {};
    },
    loadDetailsGroupFail(state, action) {
      state.isLoadingData = false;
      state.groupInfo = action.payload;
    },
    updateGroup(state, action) {
      state.isLoading = true;
    },
    updateGroupSuccess(state, action) {
      state.isLoading = false;
      state.groupInfo = "success";
    },
    updateGroupFail(state, action) {
      state.isLoading = false;
      state.groupInfo = action.payload;
    },
    deleteGroup(state, action) {
      state.isLoading = true;
    },
    deleteGroupSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteGroupFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  loadGroups,
  loadGroupsSuccess,
  loadAllGroups,
  loadAllGroupsSuccess,
  loadAllGroupsFail,
  createGroup,
  createGroupSuccess,
  createGroupFail,
  resetGroupInfo,
  loadDetailsGroup,
  loadDetailsGroupSuccess,
  loadDetailsGroupFail,
  updateGroup,
  updateGroupSuccess,
  updateGroupFail,
  deleteGroup,
  deleteGroupSuccess,
  deleteGroupFail,
} = slice.actions;
