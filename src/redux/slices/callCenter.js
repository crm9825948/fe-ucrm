import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: {
    loadingMakeCall: false,
    loadingEndCall: false,
    loadingAgentsOnline: false,
    loadingAnswerCall: false,
  },
  listStatus: [],
  initialStatus: "",
  statusCall: null,
  screen: null,
  isHold: false,
  listAgentsOnline: [],
  statusCreateRecord: null,
  fieldsMappingCallCenter: {},
  objectsToCall: {},
  dataSearchCore: [],
  alreadyUseExt: false,
};

const slice = createSlice({
  name: "callCenter",
  initialState,
  reducers: {
    updateStatusCall(state, action) {
      state.statusCall = action.payload;
    },
    updateScreen(state, action) {
      state.screen = action.payload;
    },
    updateLoading(state, action) {
      switch (action.payload.type) {
        case "loadingMakeCall":
          state.isLoading.loadingMakeCall = action.payload.loading;
          break;
        case "loadingEndCall":
          state.isLoading.loadingEndCall = action.payload.loading;
          break;
        case "loadingAnswerCall":
          state.isLoading.loadingAnswerCall = action.payload.loading;
          break;
        default:
          break;
      }
    },
    updateHold(state, action) {
      state.isHold = action.payload;
    },
    loadlistStatus() {},
    loadlistStatusSuccess(state, action) {
      state.listStatus = action.payload;
    },
    loadListReasonCode() {},
    loadListReasonCodeSuccess(state, action) {
      state.initialStatus = action.payload;
    },
    changeAgentStatus() {},
    makeCall(state) {
      state.isLoading.loadingMakeCall = true;
    },
    endCall(state) {
      state.isLoading.loadingEndCall = true;
    },
    holdCall() {},
    unHoldCall() {},
    loadAgentsOnline(state) {
      state.isLoading.loadingAgentsOnline = true;
    },
    loadAgentsOnlineSuccess(state, action) {
      state.isLoading.loadingAgentsOnline = false;
      state.listAgentsOnline = action.payload;
    },
    transferCall() {},
    conferenceCall() {},
    consultCall() {},
    answerCall(state) {
      state.isLoading.loadingAnswerCall = true;
    },
    updateInteraction() {},
    createRecordCallCenter() {},
    createRecordCallCenterSuccess(state, action) {
      state.statusCreateRecord = action.payload;
    },
    updateRecordCallCenter() {},
    updateRecordCallCenterResult(state, action) {
      state.statusCreateRecord = action.payload;
    },
    getFieldsMappingCallCenter() {},
    getFieldsMappingCallCenterSuccess(state, action) {
      state.fieldsMappingCallCenter = action.payload;
    },
    getObjectsToCall() {},
    getObjectsToCallResult(state, action) {
      state.objectsToCall = action.payload;
    },
    searchCoreCallCenter() {},
    searchCoreCallCenterResult(state, action) {
      state.dataSearchCore = action.payload;
    },
    createRecordCoreCallCenter() {},
    createRecordCoreCallCenterResult(state, action) {
      state.statusCreateRecord = action.payload;
    },
    connectExt() {},
    setAlreadyUseExt(state, action) {
      state.alreadyUseExt = action.payload;
    },
    disconnectExt() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  updateStatusCall,
  updateScreen,
  updateLoading,
  updateHold,
  loadlistStatus,
  loadlistStatusSuccess,
  loadListReasonCode,
  loadListReasonCodeSuccess,
  changeAgentStatus,
  makeCall,
  endCall,
  holdCall,
  unHoldCall,
  loadAgentsOnline,
  loadAgentsOnlineSuccess,
  transferCall,
  conferenceCall,
  consultCall,
  answerCall,
  updateInteraction,
  createRecordCallCenter,
  createRecordCallCenterSuccess,
  updateRecordCallCenter,
  updateRecordCallCenterResult,
  getFieldsMappingCallCenter,
  getFieldsMappingCallCenterSuccess,
  getObjectsToCall,
  getObjectsToCallResult,
  searchCoreCallCenter,
  searchCoreCallCenterResult,
  createRecordCoreCallCenter,
  createRecordCoreCallCenterResult,
  connectExt,
  setAlreadyUseExt,
  disconnectExt,
} = slice.actions;
