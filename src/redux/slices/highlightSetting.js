import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  showSetting: false,
  allSetting: [],
  totalSetting: 0,
};

const slice = createSlice({
  name: "highlightSetting",
  initialState,
  reducers: {
    loadDataNecessary() {},
    showHighlightSetting(state, action) {
      state.showSetting = action.payload;
    },
    createSetting() {},
    loadAllSetting() {},
    loadAllSettingResult(state, action) {
      state.allSetting = action.payload.result;
      state.totalSetting = action.payload.total;
    },
    changeStatus() {},
    deleteSetting() {},
    changeState(state, action) {
      let tempList = [...state.allSetting];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.allSetting[idx] = {
              ...state.allSetting[idx],
              status: !state.allSetting[idx].status,
            };
          }
        });
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.allSetting.splice(idx, 1);
            state.totalSetting = state.totalSetting - 1;
          }
        });
      }
    },
    updateSetting() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  createSetting,
  showHighlightSetting,
  loadAllSetting,
  loadAllSettingResult,
  changeStatus,
  deleteSetting,
  changeState,
  updateSetting,
} = slice.actions;
