const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  showModalConfirmDelete: false,
  showLoadingScreen: false,
  showModalConfirm: false,
  listObjectField: [],
};

const slice = createSlice({
  name: "global",
  initialState,
  reducers: {
    setShowModalConfirmDelete(state, action) {
      state.showModalConfirmDelete = action.payload;
    },
    setShowModalConfirm(state, action) {
      state.showModalConfirm = action.payload;
    },
    setShowLoadingScreen(state, action) {
      state.showLoadingScreen = action.payload;
    },
    getListObjectField(state, action) {},
    getListObjectFieldSuccess(state, action) {
      state.listObjectField = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  setShowModalConfirmDelete,
  setShowLoadingScreen,
  getListObjectField,
  getListObjectFieldSuccess,
  setShowModalConfirm,
} = slice.actions;
