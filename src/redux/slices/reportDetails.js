import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  isLoadingPin: false,
  loadingScreen: true,
  dataReport: [],
  reportArithmetic: [],
  infoReport: {},
  totalRecords: null,
  statusExport: null,
  statusPin: null,
  listWidgets: [],
  tempListWidgets: [],
  listCharts: [],
  tempListCharts: [],
  deleteInfo: null,
  widgetInfo: null,
  chartInfo: null,
  isLoadingWidget: false,
  isLoadingChart: false,
  detailsWidget: {},
  detailsChart: {},
  isLoadingDataWidget: false,
  isLoadingDataChart: false,
  isLoadingColumnValue: false,
  columnValue: [],
  loadingFilter: null,
  isloadingNumberRecords: false,
  isLoadingDataWithOutPagi: false,
  notFoundChart: {
    status: false,
    id: "",
  },
  notFoundWidget: {
    status: false,
    id: "",
  },
  quickFilters: [],
};

const slice = createSlice({
  name: "reportDetails",
  initialState,
  reducers: {
    loadDataNecessary() {},
    loadSpecificReport(state, action) {
      state.loadingScreen = true;
    },
    loadSpecificReportSuccess(state, action) {
      state.loadingScreen = false;
      state.dataReport = action.payload;
    },
    loadSpecificReportFail(state, action) {
      state.loadingScreen = false;
    },
    loadReportArithmetic(state, action) {
      state.loadingScreen = true;
    },
    loadReportArithmeticSuccess(state, action) {
      state.loadingScreen = false;
      state.reportArithmetic = action.payload;
    },
    loadReportArithmeticFail(state, action) {
      state.loadingScreen = false;
    },
    loadTotalRecords(state, action) {
      state.isloadingNumberRecords = true;
    },
    loadTotalRecordsSuccess(state, action) {
      state.isloadingNumberRecords = false;
      state.totalRecords = action.payload;
    },
    loadTotalRecordsFail(state, action) {
      state.isloadingNumberRecords = false;
    },
    setLoadDataWithOutPagi(state, action) {
      state.isLoadingDataWithOutPagi = action.payload;
    },
    loadInfoReport(state, action) {
      state.loadingScreen = true;
    },
    loadInfoReportSuccess(state, action) {
      state.loadingScreen = false;
      state.infoReport = action.payload;
    },
    loadInfoReportFail(state, action) {
      state.loadingScreen = false;
    },
    exportReport(state, action) {
      state.isLoading = true;
    },
    exportReportSuccess(state, action) {
      state.isLoading = false;
      state.statusExport = "success";
    },
    exportReportFail(state, action) {
      state.isLoading = false;
    },
    resetStatus(state, action) {
      state.statusExport = null;
      state.statusPin = null;
      state.deleteInfo = null;
      state.widgetInfo = null;
      state.chartInfo = null;
      state.loadingFilter = null;
      state.detailsWidget = {};
    },
    unpinReport(state, action) {
      state.isLoadingPin = true;
    },
    unpinReportSuccess(state, action) {
      state.isLoadingPin = false;
      state.statusPin = {
        ...action.payload,
        status: "unpin success",
      };
    },
    unpinReportFail(state, action) {
      state.isLoadingPin = false;
      state.statusPin = {
        status: action.payload,
      };
    },
    pinToDashBoard(state, action) {
      state.isLoadingPin = true;
    },
    pinToDashBoardSuccess(state, action) {
      state.isLoadingPin = false;
      state.statusPin = {
        ...action.payload,
        status: "pin success",
      };
    },
    pinToDashBoardFail(state, action) {
      state.isLoadingPin = false;
      state.statusPin = {
        ...action.payload,
        status: action.payload,
      };
    },
    loadAllWidgets(state, action) {
      state.loadingScreen = true;
    },
    loadAllWidgetsSuccess(state, action) {
      state.loadingScreen = false;
      state.tempListWidgets = action.payload;
    },
    loadAllWidgetsFail(state, action) {
      state.loadingScreen = false;
    },
    getDataWidget(state, action) {
      state.isLoadingDataWidget = true;
      let tempList = [...state.listWidgets];
      tempList.forEach((item, idx) => {
        if (item._id === action.payload._id) {
          tempList.splice(idx, 1);
        }
      });
      state.listWidgets = tempList;
    },
    getDataWidgetSuccess(state, action) {
      let tempList = [...state.listWidgets];
      tempList.push(action.payload);
      state.listWidgets = tempList;
      state.isLoadingDataWidget = false;
    },
    getDataWidgetFail(state, action) {
      state.isLoadingDataWidget = false;
      state.notFoundWidget = {
        status: true,
        id: action.payload._id,
      };
    },
    deleteWidget(state, action) {
      state.isLoading = true;
    },
    deleteWidgetSuccess(state, action) {
      state.isLoading = false;
      state.deleteInfo = "success widget";
    },
    deleteWidgetFail(state, action) {
      state.isLoading = false;
      state.deleteInfo = action.payload;
    },
    addWidget(state, action) {
      state.isLoading = true;
    },
    addWidgetSuccess(state, action) {
      state.isLoading = false;
      state.widgetInfo = "success";
    },
    addWidgetFail(state, action) {
      state.isLoading = false;
      state.widgetInfo = action.payload;
    },
    loadDetailsWidget(state, action) {
      state.isLoadingWidget = true;
    },
    loadDetailsWidgetSuccess(state, action) {
      state.isLoadingWidget = false;
      state.detailsWidget = action.payload;
    },
    loadDetailsWidgetFail(state, action) {
      state.isLoadingWidget = false;
      state.detailsWidget = {};
    },
    updateWidget(state, action) {
      state.isLoading = true;
    },
    updateWidgetSuccess(state, action) {
      state.isLoading = false;
      state.widgetInfo = "success";
    },
    updateWidgetFail(state, action) {
      state.isLoading = false;
      state.widgetInfo = action.payload;
    },
    loadAllCharts(state, action) {
      state.loadingScreen = true;
    },
    loadAllChartsSuccess(state, action) {
      state.loadingScreen = false;
      state.tempListCharts = action.payload;
    },
    loadAllChartsFail(state, action) {
      state.loadingScreen = false;
    },
    getDataChart(state, action) {
      state.isLoadingDataChart = true;
      let tempList = [...state.listCharts];
      tempList.forEach((item, idx) => {
        if (item._id === action.payload._id) {
          tempList.splice(idx, 1);
        }
      });
      state.listCharts = tempList;
    },
    getDataChartSuccess(state, action) {
      let tempList = [...state.listCharts];
      tempList.push(action.payload);
      state.listCharts = tempList;
      state.isLoadingDataChart = false;
    },
    getDataChartFail(state, action) {
      state.isLoadingDataChart = false;
      state.notFoundChart = {
        status: true,
        id: action.payload._id,
      };
    },
    deleteChart(state, action) {
      state.isLoading = true;
    },
    deleteChartSuccess(state, action) {
      state.isLoading = false;
      state.deleteInfo = "success chart";
    },
    deleteChartFail(state, action) {
      state.isLoading = false;
      state.deleteInfo = action.payload;
    },
    loadCloumnValue(state, action) {
      state.isLoadingColumnValue = true;
    },
    loadCloumnValueSuccess(state, action) {
      state.isLoadingColumnValue = false;
      state.columnValue = action.payload;
    },
    loadCloumnValueFail(state, action) {
      state.isLoading = false;
    },
    addChart(state, action) {
      state.isLoading = true;
    },
    addChartSuccess(state, action) {
      state.isLoading = false;
      state.chartInfo = "success";
    },
    addChartFail(state, action) {
      state.isLoading = false;
      state.chartInfo = action.payload;
    },
    loadDetailsChart(state, action) {
      state.isLoadingChart = true;
    },
    loadDetailsChartSuccess(state, action) {
      state.isLoadingChart = false;
      state.detailsChart = action.payload;
    },
    loadDetailsChartFail(state, action) {
      state.isLoadingChart = false;
      state.detailsChart = {};
    },
    updateChart(state, action) {
      state.isLoading = true;
    },
    updateChartSuccess(state, action) {
      state.isLoading = false;
      state.chartInfo = "success";
    },
    updateChartFail(state, action) {
      state.isLoading = false;
      state.chartInfo = action.payload;
    },
    changeState(state, action) {
      if (action.payload.type === "pin") {
        state.infoReport = {
          ...state.infoReport,
          report_info: {
            ...state.infoReport.report_info,
            is_pinned_to_dashboard:
              !state.infoReport.report_info.is_pinned_to_dashboard,
          },
        };
      }
    },
    filterReport(state, action) {},
    filterReportSuccess(state, action) {
      state.loadingFilter = "success";
    },
    refreshChart() {},
    refreshWidget() {},
    refreshAllReport() {},
    refreshAllDashboard() {},
    quickFilterFeat() {},
    quickFilterFeatResult(state, action) {
      state.quickFilters = action.payload;
    },

    unMountReportDetails(state) {
      state.dataReport = [];
      state.reportArithmetic = [];
      state.infoReport = {};
      state.totalRecords = null;
      state.listWidgets = [];
      state.tempListWidgets = [];
      state.listCharts = [];
      state.tempListCharts = [];
      state.detailsWidget = {};
      state.detailsChart = {};
      state.columnValue = [];
      state.notFoundChart = {
        status: false,
        id: "",
      };
      state.notFoundWidget = {
        status: false,
        id: "",
      };
      state.quickFilters = [];
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  loadSpecificReport,
  loadSpecificReportSuccess,
  loadSpecificReportFail,
  loadReportArithmetic,
  loadReportArithmeticSuccess,
  loadReportArithmeticFail,
  loadTotalRecords,
  loadTotalRecordsSuccess,
  loadTotalRecordsFail,
  setLoadDataWithOutPagi,
  loadInfoReport,
  loadInfoReportSuccess,
  loadInfoReportFail,
  exportReport,
  exportReportSuccess,
  exportReportFail,
  resetStatus,
  unpinReport,
  unpinReportSuccess,
  unpinReportFail,
  pinToDashBoard,
  pinToDashBoardSuccess,
  pinToDashBoardFail,
  loadAllWidgets,
  loadAllWidgetsSuccess,
  loadAllWidgetsFail,
  loadAllCharts,
  loadAllChartsSuccess,
  loadAllChartsFail,
  getDataWidget,
  getDataWidgetSuccess,
  getDataWidgetFail,
  getDataChart,
  getDataChartSuccess,
  getDataChartFail,
  deleteWidget,
  deleteWidgetSuccess,
  deleteWidgetFail,
  addWidget,
  addWidgetSuccess,
  addWidgetFail,
  loadDetailsWidget,
  loadDetailsWidgetSuccess,
  loadDetailsWidgetFail,
  updateWidget,
  updateWidgetSuccess,
  updateWidgetFail,
  deleteChart,
  deleteChartSuccess,
  deleteChartFail,
  loadCloumnValue,
  loadCloumnValueSuccess,
  loadCloumnValueFail,
  addChart,
  addChartSuccess,
  addChartFail,
  loadDetailsChart,
  loadDetailsChartSuccess,
  loadDetailsChartFail,
  updateChart,
  updateChartSuccess,
  updateChartFail,
  changeState,
  filterReport,
  filterReportSuccess,
  refreshChart,
  refreshWidget,
  refreshAllReport,
  refreshAllDashboard,
  unMountReportDetails,
  quickFilterFeat,
  quickFilterFeatResult,
} = slice.actions;
