import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: true,
  error: false,
  objects: null,
  statusCode: null,
  visible: false,
  category: {},
  objectCategory: {},
  categoryVisible: {},
  order: [],
  listID: {},
  visibleList: {},
  objectStatus: null,
  listObject: []
};

const slice = createSlice({
  name: "objectsManagement",
  initialState,
  reducers: {
    //Load object
    loadObject(state, action) {
      state.isLoading = true;
    },
    loadObjectSuccessfully(state, action) {
      state.isLoading = false;
      state.objects = action.payload;
      state.error = false;
    },
    loadObjectError(state, action) {
      state.isLoading = false;
      state.error = true;
      state.objects = null;
    },
    //Create object
    createObject(state, action) {
      state.isLoading = true;
    },
    createObjectSuccess(state, action) {
      state.isLoading = false;
      state.objectStatus = "success";
      state.statusCode = "create-object";
    },
    createObjectFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    //Update object
    updateObject(state, action) {
      state.isLoading = true;
    },
    updateObjectSuccess(state, action) {
      state.isLoading = false;
      state.objectStatus = "success";
      state.statusCode = "update-object";
    },
    updateObjectFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    //Delete object
    deleteObject(state, action) {
      state.isLoading = true;
    },
    deleteObjectSuccess(state, action) {
      state.isLoading = false;
      state.objectStatus = "success";
      state.statusCode = "delete-object";
    },
    deleteObjectFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    //Category
    toggleVisibleCategory(state, action) {
      state.isLoading = true;
    },
    toggleVisibleSuccess(state, action) {
      state.isLoading = false;
      state.visible = !state.visible;
    },
    toggleVisileFail(state, action) {
      state.isLoading = false;
      // state.visible = state.visible;
    },
    loadCategory(state, action) {
      state.isLoading = true;
    },
    loadCategorySuccess(state, action) {
      state.isLoading = false;
      state.category = action.payload.data;
      state.order = action.payload.order;
      state.error = false;
      state.listID = action.payload.ID;
      state.visibleList = action.payload.visibleList;
      state.categoryVisible = action.payload.categoryVisible;
    },
    loadCategoryError(state, action) {
      state.isLoading = false;
      state.category = null;
      state.error = true;
    },

    loadObjectCategory(state, action) {
      state.isLoading = true;
    },
    loadObjectCategorySuccess(state, action) {
      state.isLoading = false;
      state.objectCategory = action.payload.data;
      state.error = false;
    },
    loadObjectCategoryError(state, action) {
      state.isLoading = false;
      state.objectCategory = null;
      state.error = true;
    },

    createGroup(state, action) {
      state.isLoading = true;
    },
    createGroupSuccess(state, action) {
      state.isLoading = false;
      state.objectStatus = "success";
      state.statusCode = "create-group";
    },
    createGroupFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    reorderGroup(state, action) {
      state.isLoading = false;
      state.order = action.payload;
    },
    updateGroup(state, action) {
      state.isLoading = true;
    },
    updateGroupSuccess(state, action) {
      state.isLoading = false;
      state.objectStatus = "success";
      state.statusCode = "update-group";
    },
    updateGroupFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    deleteGroup(state, action) {
      state.isLoading = true;
    },
    deleteGroupSuccess(state, action) {
      state.isLoading = false;
      state.statusCode = "delete-group";
    },
    deleteGroupFail(state, action) {
      state.isLoading = false;
      state.objectStatus = action.payload;
      state.statusCode = "fail";
    },
    getListObject() {
    },
    getListObjectSuccess(state, action) {
      state.listObject = action.payload
    },
    //set status code
    setStatusCode(state, action) {
      state.statusCode = null;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadObject,
  loadObjectError,
  loadObjectSuccessfully,
  toggleVisibleCategory,
  toggleVisibleSuccess,
  toggleVisileFail,
  loadCategory,
  loadCategoryError,
  loadCategorySuccess,
  createGroup,
  createGroupSuccess,
  createGroupFail,
  reorderGroup,
  updateGroup,
  updateGroupFail,
  updateGroupSuccess,
  deleteGroup,
  deleteGroupFail,
  deleteGroupSuccess,
  createObject,
  createObjectFail,
  createObjectSuccess,
  updateObject,
  updateObjectFail,
  updateObjectSuccess,
  deleteObject,
  deleteObjectFail,
  deleteObjectSuccess,
  setStatusCode,
  loadObjectCategory,
  loadObjectCategorySuccess,
  loadObjectCategoryError,
  getListObject,
  getListObjectSuccess
} = slice.actions;
