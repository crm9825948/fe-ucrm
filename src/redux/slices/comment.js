import { createSlice, current } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  listComment: {
    comment: [],
    total: 0,
  },
  showReplyComment: [],
  currentPageReply: {},
  showMore: {},
  recordID: "",
  isSort: false,
};

const slice = createSlice({
  name: "comment",
  initialState,
  reducers: {
    loadComment(state) {},
    updateLoading(state, action) {
      state.isLoading = action.payload;
    },
    updateSort(state, action) {
      state.isSort = action.payload;
      state.listComment.comment = [];
      state.listComment.total = 0;
    },
    loadCommentResult(state, action) {
      if (state.recordID !== action.payload.recordID) {
        let comments = action.payload.data.comment.filter(
          (value, index, self) =>
            index === self.findIndex((t) => t._id === value._id)
        );
        // if (state.listComment.comment.length === 0) {
        //   comments = comments.reverse();
        // }

        state.listComment.comment = comments;
        state.listComment.total = action.payload.data.total;
        state.isLoading = false;

        let temp = [];
        comments.forEach(() => {
          temp.push(false);
        });
        state.showReplyComment = temp;
      } else {
        let comments = state.listComment.comment
          .concat(action.payload.data.comment)
          .filter(
            (value, index, self) =>
              index === self.findIndex((t) => t._id === value._id)
          );
        // if (state.listComment.comment.length === 0) {
        //   comments = comments.reverse();
        // }

        state.listComment.comment = comments;
        state.listComment.total = action.payload.data.total;
        state.isLoading = false;

        let temp = [];
        comments.forEach(() => {
          temp.push(false);
        });
        state.showReplyComment = temp;
      }
      state.recordID = action.payload.recordID;
    },
    updateShowReplyComment(state, action) {
      state.showReplyComment = action.payload;
    },
    updateCurrentPageReply(state, action) {
      state.currentPageReply = action.payload;
    },
    updateShowMore(state, action) {
      state.showMore = action.payload;
    },
    updateListComment(state, action) {
      if (action.payload.type === "post") {
        let comments = [...state.listComment.comment];
        comments.push(action.payload.data);
        const temp = {
          comment: comments,
          total: current(state.listComment).total + 1,
        };
        state.listComment = temp;
      } else if (action.payload.type === "update") {
        let comments = [...state.listComment.comment];
        let commentUpdate = -1;
        commentUpdate = comments.findIndex(
          (item) => item._id === action.payload.data.id
        );
        if (commentUpdate !== -1) {
          comments[commentUpdate] = {
            ...comments[commentUpdate],
            content: action.payload.data.content,
            modify_time: Date.now(),
            link: action.payload.data.link,
          };
          state.listComment.comment = comments;
        } else {
          let indexParent = -1;
          comments.forEach((item, index) => {
            if (!!item.reply) {
              const tempCommentUpdate = item.reply.findIndex(
                (ele) => ele._id === action.payload.data.id
              );
              if (tempCommentUpdate !== -1) {
                commentUpdate = tempCommentUpdate;
                indexParent = index;
              }
            }
          });

          let commentParent = [...comments];
          let newReply = [...commentParent[indexParent].reply];

          newReply[commentUpdate] = {
            ...newReply[commentUpdate],
            content: action.payload.data.content,
            modify_time: Date.now(),
            link: action.payload.data.link,
          };

          commentParent[indexParent] = {
            ...commentParent[indexParent],
            reply: newReply,
          };
          state.listComment.comment = commentParent;
        }
      } else if (action.payload.type === "delete") {
        let comments = [...state.listComment.comment];
        let commentUpdate = -1;
        commentUpdate = comments.findIndex(
          (item) => item._id === action.payload.data.id
        );
        if (commentUpdate !== -1) {
          comments.splice(commentUpdate, 1);
          state.listComment.comment = comments;
          state.listComment.total = state.listComment.total - 1;
        } else {
          let indexParent = -1;
          comments.forEach((item, index) => {
            if (!!item.reply) {
              const tempCommentUpdate = item.reply.findIndex(
                (ele) => ele._id === action.payload.data.id
              );
              if (tempCommentUpdate !== -1) {
                commentUpdate = tempCommentUpdate;
                indexParent = index;
              }
            }
          });

          let commentParent = [...comments];
          let newReply = [...commentParent[indexParent].reply];

          newReply.splice(commentUpdate, 1);
          commentParent[indexParent] = {
            ...commentParent[indexParent],
            reply: newReply,
            reply_count: commentParent[indexParent].reply_count - 1,
          };
          state.listComment.comment = commentParent;
        }
      } else if (action.payload.type === "reply") {
        let comments = [...current(state.listComment.comment)];
        let indexReplyComment = comments.findIndex(
          (item) => item._id === action.payload.data.comment_id
        );

        if (comments[indexReplyComment]?.reply) {
          let commentChild = [...comments[indexReplyComment].reply];
          commentChild.push(action.payload.data);
          comments[indexReplyComment] = {
            ...comments[indexReplyComment],
            reply: commentChild,
            reply_count: comments[indexReplyComment].reply_count + 1,
          };
        } else {
          comments[indexReplyComment] = {
            ...comments[indexReplyComment],
            reply: [action.payload.data],
            reply_count: comments[indexReplyComment].reply_count + 1,
          };
        }
        state.listComment.comment = comments;
      } else if (action.payload.type === "like") {
        let comments = [...current(state.listComment.comment)];
        let commentUpdate = -1;

        commentUpdate = comments.findIndex(
          (item) => item._id === action.payload.data.comment_id
        );
        if (commentUpdate !== -1) {
          comments[commentUpdate] = {
            ...comments[commentUpdate],
            interacted: comments[commentUpdate].interacted === 0 ? 1 : 0,
            likes:
              comments[commentUpdate].interacted === 0
                ? comments[commentUpdate].likes + 1
                : comments[commentUpdate].likes - 1,
          };

          state.listComment.comment = comments;
        } else {
          let indexParent = -1;
          comments.forEach((item, index) => {
            if (!!item.reply) {
              const tempCommentUpdate = item.reply.findIndex(
                (ele) => ele._id === action.payload.data.comment_id
              );
              if (tempCommentUpdate !== -1) {
                commentUpdate = tempCommentUpdate;
                indexParent = index;
              }
            }
          });

          let commentParent = [...comments];
          let newReply = [...commentParent[indexParent].reply];

          newReply[commentUpdate] = {
            ...newReply[commentUpdate],
            interacted: newReply[commentUpdate].interacted === 0 ? 1 : 0,
            likes:
              newReply[commentUpdate].interacted === 0
                ? newReply[commentUpdate].likes + 1
                : newReply[commentUpdate].likes - 1,
          };

          commentParent[indexParent] = {
            ...commentParent[indexParent],
            reply: newReply,
          };
          state.listComment.comment = commentParent;
        }
      }
    },
    postComment() {},
    editComment() {},
    deleteComment() {},
    likeComment() {},
    replyComment() {},
    loadReplyComment() {},
    loadReplyCommentResult(state, action) {
      let comments = [...state.listComment.comment];
      let indexReplyComment = comments.findIndex(
        (item) => item._id === action.payload[0].comment_id
      );

      if (comments[indexReplyComment]?.reply) {
        comments[indexReplyComment] = {
          ...comments[indexReplyComment],
          reply: [
            ...comments[indexReplyComment]?.reply,
            ...action.payload,
          ].filter(
            (value, index, self) =>
              index === self.findIndex((t) => t._id === value._id)
          ),
        };
      } else {
        comments[indexReplyComment] = {
          ...comments[indexReplyComment],
          reply: [...action.payload].filter(
            (value, index, self) =>
              index === self.findIndex((t) => t._id === value._id)
          ),
        };
      }
      state.listComment.comment = comments;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadComment,
  updateLoading,
  updateSort,
  loadCommentResult,
  postComment,
  updateListComment,
  updateCurrentPageReply,
  updateShowMore,
  editComment,
  deleteComment,
  likeComment,
  replyComment,
  loadReplyComment,
  loadReplyCommentResult,
  updateShowReplyComment,
} = slice.actions;
