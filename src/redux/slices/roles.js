import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  listRoles: [],
  roleInfo: null,
  details: {},
  statusDelete: null,
  listTransfer: [],
};

const slice = createSlice({
  name: "roles",
  initialState,
  reducers: {
    //Load list roles
    loadDataNecessary() {},
    loadListRoles(state, action) {},
    loadListRolesSuccessfully(state, action) {
      state.listRoles = action.payload;
    },
    createRole(state, action) {
      state.isLoading = true;
    },
    createRoleSuccess(state, action) {
      state.isLoading = false;
      state.roleInfo = "success";
    },
    resetRoleInfo(state, action) {
      state.roleInfo = null;
    },
    createRoleFail(state, action) {
      state.isLoading = false;
      state.roleInfo = action.payload;
    },
    deleteRole(state, action) {
      state.isLoading = true;
    },
    deleteRoleSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteRoleFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
    resetStatusDelete(state, action) {
      state.statusDelete = null;
    },
    loadDetailsRole(state, action) {
      state.isLoading = true;
    },
    loadDetailsRoleSuccess(state, action) {
      state.isLoading = false;
      state.details = action.payload;
    },
    resetRoleDetails(state, action) {
      state.details = {};
    },
    loadDetailsRoleFail(state, action) {
      state.isLoading = false;
    },
    updateRole(state, action) {
      state.isLoading = true;
    },
    updateRoleSuccess(state, action) {
      state.isLoading = false;
      state.roleInfo = "success";
    },
    updateRoleFail(state, action) {
      state.isLoading = false;
      state.roleInfo = action.payload;
    },
    getRoleToSwitch(state, action) {
      state.isLoading = true;
    },
    getRoleToSwitchSuccess(state, action) {
      state.isLoading = false;
      state.listTransfer = action.payload;
    },
    getRoleToSwitchFail(state, action) {
      state.isLoading = false;
    },
    exportRoles() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  loadListRoles,
  loadListRolesSuccessfully,
  createRole,
  createRoleSuccess,
  resetRoleInfo,
  createRoleFail,
  deleteRole,
  deleteRoleFail,
  deleteRoleSuccess,
  resetStatusDelete,
  loadDetailsRole,
  loadDetailsRoleSuccess,
  loadDetailsRoleFail,
  resetRoleDetails,
  updateRole,
  updateRoleSuccess,
  updateRoleFail,
  getRoleToSwitch,
  getRoleToSwitchSuccess,
  getRoleToSwitchFail,
  exportRoles,
} = slice.actions;
