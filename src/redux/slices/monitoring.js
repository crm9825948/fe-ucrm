const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  dataMonitor: {},
};
const slice = createSlice({
  name: "monitoring",
  initialState,
  reducers: {
    loadDataMonitor() {},
    loadDataMonitorSuccess(state, action) {
      state.dataMonitor = action.payload;
    },
  },
});
export default slice.reducer;
export const { loadDataMonitor, loadDataMonitorSuccess } = slice.actions;
