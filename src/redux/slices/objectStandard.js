import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listOS: [],
  fieldsOS: [],
  listSettingsOS: [],
};

const slice = createSlice({
  name: "objectStandard",
  initialState,
  reducers: {
    objectStandardCreate() {},
    objectStandardDelete() {},
    objectStandardUpdate() {},
    objectStandardSyncData() {},
    objectStandardGetObjects() {},
    objectStandardGetObjectsResult(state, action) {
      state.listOS = action.payload;
    },
    objectStandardGetFields() {},
    objectStandardGetFieldsResult(state, action) {
      state.fieldsOS = action.payload;
    },
    objectStandardGetList() {},
    objectStandardGetListResult(state, action) {
      state.listSettingsOS = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  objectStandardCreate,
  objectStandardDelete,
  objectStandardUpdate,
  objectStandardSyncData,
  objectStandardGetObjects,
  objectStandardGetObjectsResult,
  objectStandardGetFields,
  objectStandardGetFieldsResult,
  objectStandardGetList,
  objectStandardGetListResult,
} = slice.actions;
