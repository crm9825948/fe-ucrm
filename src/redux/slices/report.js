import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  loadingScreen: true,
  listFolders: [],
  totalRecords: 0,
  //   listAllGroups: [],
  reportInfo: null,
  updateInfo: null,
  createInfo: null,
  cloneInfo: null,
  details: {},
  statusDelete: null,
  isLoadingReport: false,
  listReports: [],
  reportDetails: {},
  listFieldsObject: [],
  //   isLoadingData: false,
  currentPage: 1,
};

const slice = createSlice({
  name: "report",
  initialState,
  reducers: {
    loadDataNecessary() {},
    loadFolders(state, action) {},
    loadFoldersSuccess(state, action) {
      state.listFolders = action.payload;
    },
    loadFoldersFail(state, action) {},
    createFolder(state, action) {
      state.isLoading = true;
    },
    createFolderSuccess(state, action) {
      state.isLoading = false;
      state.reportInfo = "success";
    },
    createFolderFail(state, action) {
      state.isLoading = false;
      state.reportInfo = action.payload;
    },
    resetReportInfo(state, action) {
      state.reportInfo = null;
      state.updateInfo = null;
      state.createInfo = null;
      state.cloneInfo = null;
    },
    updateFolder(state, action) {
      state.isLoading = true;
    },
    updateFolderSuccess(state, action) {
      state.isLoading = false;
      state.reportInfo = "success";
    },
    updateFolderFail(state, action) {
      state.isLoading = false;
      state.reportInfo = action.payload;
    },
    deleteFolder(state, action) {
      state.isLoading = true;
    },
    deleteFolderSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteFolderFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
    resetStatusDelete(state, action) {
      state.statusDelete = null;
    },
    loadReports(state, action) {
      state.isLoadingReport = true;
    },
    loadReportsSuccess(state, action) {
      state.isLoadingReport = false;
      state.listReports = action.payload[0].data;
      state.totalRecords = action.payload[0].total_record;
    },
    loadReportsFail(state, action) {
      state.isLoadingReport = false;
      state.listReports = [];
    },
    loadFields(state, action) {},
    loadFieldsSuccess(state, action) {
      state.listFieldsObject = action.payload;
    },
    loadFieldsFail(state, action) {},
    loadSharedReport(state, action) {
      state.isLoadingReport = true;
    },
    loadSharedReportSuccess(state, action) {
      state.isLoadingReport = false;
      state.listReports = action.payload.reports;
      state.totalRecords = action.payload.total_record;
    },
    loadSharedReportFail(state, action) {
      state.isLoadingReport = false;
      state.listReports = [];
    },
    deleteReport(state, action) {
      state.isLoading = true;
    },
    deleteReportSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteReportFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
    loadDetailsReport(state, action) {
      state.isLoadingReport = true;
    },
    loadDetailsReportSuccess(state, action) {
      state.isLoadingReport = false;
      state.reportDetails = action.payload;
    },
    loadDetailsReportFail(state, action) {
      state.isLoadingReport = false;
    },
    updateReport(state, action) {
      state.isLoading = true;
    },
    updateReportSuccess(state, action) {
      state.isLoading = false;
      state.updateInfo = "success";
    },
    updateReportFail(state, action) {
      state.isLoading = false;
      state.updateInfo = action.payload;
    },
    createReport(state, action) {
      state.isLoading = true;
    },
    createReportSuccess(state, action) {
      state.isLoading = false;
      state.createInfo = "success";
    },
    createReportFail(state, action) {
      state.isLoading = false;
      state.createInfo = action.payload;
    },
    cloneReport(state, action) {
      state.isLoading = true;
    },
    cloneReportSuccess(state, action) {
      state.isLoading = false;
      state.cloneInfo = "success";
    },
    cloneReportFail(state, action) {
      state.isLoading = false;
      state.cloneInfo = action.payload;
    },
    changeState(state, action) {
      if (action.payload.type === "share") {
        state.reportDetails = {
          ...state.reportDetails,
          report_info: {
            ...state.reportDetails.report_info,
            share_to_groups: action.payload.shared,
          },
        };
      }
    },
    setCurrentPage(state, action) {
      state.currentPage = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  loadFolders,
  loadFoldersSuccess,
  loadFoldersFail,
  createFolder,
  createFolderSuccess,
  createFolderFail,
  resetReportInfo,
  updateFolder,
  updateFolderSuccess,
  updateFolderFail,
  deleteFolder,
  deleteFolderSuccess,
  deleteFolderFail,
  resetStatusDelete,
  loadReports,
  loadReportsSuccess,
  loadReportsFail,
  resetListReports,
  loadFields,
  loadFieldsSuccess,
  loadFieldsFail,
  loadSharedReport,
  loadSharedReportSuccess,
  loadSharedReportFail,
  deleteReport,
  deleteReportSuccess,
  deleteReportFail,
  loadDetailsReport,
  loadDetailsReportSuccess,
  loadDetailsReportFail,
  updateReport,
  updateReportSuccess,
  updateReportFail,
  createReport,
  createReportSuccess,
  createReportFail,
  cloneReport,
  cloneReportSuccess,
  cloneReportFail,
  changeState,
  setCurrentPage,
} = slice.actions;
