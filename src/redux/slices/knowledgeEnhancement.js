const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  fieldsCategory: [],
  fieldsTag: [],
  fieldsArticle: [],
  isLoading: false,
  valueModal: {},
  listKnowledgeSetting: [],
};

const slice = createSlice({
  name: "knowledgeEnhancement",
  initialState,
  reducers: {
    getFieldsObject(state) {
      state.isLoading = true;
    },
    getFieldsObjectSuccess(state, action) {
      state.isLoading = false;
      state.fieldsCategory = action.payload;
    },
    getFieldsTagSuccess(state, action) {
      state.isLoading = false;
      state.fieldsTag = action.payload;
    },
    getFieldsArticleSuccess(state, action) {
      state.isLoading = false;
      state.fieldsArticle = action.payload;
    },
    getFieldsObjectFail(state) {
      state.isLoading = true;
    },
    updateValueModal(state, action) {
      state.valueModal[action.payload.key] = action.payload.value;
    },
    resetValueModal(state, action) {
      state.valueModal = action.payload;
    },
    createKnowledgeSetting() {},
    getAllSettingKnowledgeBase() {},
    getAllSettingKnowledgeBaseSuccess(state, action) {
      state.listKnowledgeSetting = action.payload;
    },
    unmountModalSetting(state) {
      state.fieldsCategory = [];
      state.fieldsTag = [];
      state.fieldsArticle = [];
      state.isLoading = false;
      state.valueModal = {};
    },
    updateSettingEnhancement() {},
    deleteSettingEnhancement() {},
  },
});

export default slice.reducer;

export const {
  getFieldsObject,
  getFieldsObjectSuccess,
  getFieldsObjectFail,
  updateValueModal,
  getFieldsTagSuccess,
  getFieldsArticleSuccess,
  unmountModalSetting,
  createKnowledgeSetting,
  resetValueModal,
  getAllSettingKnowledgeBase,
  getAllSettingKnowledgeBaseSuccess,
  updateSettingEnhancement,
  deleteSettingEnhancement,
} = slice.actions;
