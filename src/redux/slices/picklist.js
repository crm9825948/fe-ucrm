import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  data: [],
  isLoadingPicklist: false,
  details: [],
  listValue: {},
};

const slice = createSlice({
  name: "picklistDependency",
  initialState,
  reducers: {
    createPicklist(state, action) {
      state.isLoading = true;
    },
    createPicklistSuccess(state, action) {
      state.isLoading = false;
    },
    createPicklistFail(state, action) {
      state.isLoading = false;
    },

    //load picklist
    loadPicklist(state, action) {
      state.isLoadingPicklist = true;
    },
    loadPicklistSuccess(state, action) {
      state.isLoadingPicklist = false;
      state.data = action.payload;
    },
    loadPicklistFail(state, action) {
      state.isLoadingPicklist = false;
    },

    //load details
    loadDetail(state, action) {
      state.isLoading = true;
    },
    loadDetailSuccess(state, action) {
      state.isLoading = false;
      state.details = action.payload;
    },
    loadDetailFail(state, action) {
      state.isLoading = false;
    },
    resetDetail(state, action) {
      state.details = [];
    },

    //update detail
    updateDetail(state, action) {
      state.isLoading = true;
    },
    updateDetailSuccess(state, action) {
      state.isLoading = false;
    },
    updateDetailFail(state, action) {
      state.isLoading = false;
    },

    //load value
    loadValue(state, action) {
      state.isLoading = true;
    },
    loadValueSuccess(state, action) {
      state.isLoading = false;
      state.listValue = action.payload;
    },
    loadValueFail(state, action) {
      state.isLoading = false;
    },
    //delete picklist
    deletePicklist(state, action) {
      state.isLoading = true;
    },
    deletePicklistSuccess(state, action) {
      state.isLoading = false;
    },
    deletePicklistFail(state, action) {
      state.isLoading = false;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  createPicklist,
  createPicklistFail,
  createPicklistSuccess,
  loadPicklist,
  loadPicklistSuccess,
  loadPicklistFail,
  loadDetail,
  loadDetailSuccess,
  loadDetailFail,
  updateDetail,
  updateDetailSuccess,
  updateDetailFail,
  loadValue,
  loadValueSuccess,
  loadValueFail,
  resetDetail,
  deletePicklist,
  deletePicklistSuccess,
  deletePicklistFail,
} = slice.actions;
