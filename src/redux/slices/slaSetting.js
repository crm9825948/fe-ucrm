import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listSLA: [],
  totalRecordSLA: 0,
  listFieldsObject: [],
  detailsSLA: {},
  isEdit: false,
};

const slice = createSlice({
  name: "slaSetting",
  initialState,
  reducers: {
    loadListSLA() {},
    loadListSLASuccess(state, action) {
      state.listSLA = action.payload.data;
      state.totalRecordSLA = action.payload.total_record;
    },
    activeSLA() {},
    toggleNotification() {},
    changeState(state, action) {
      let tempList = [...state.listSLA];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listSLA[idx] = {
              ...state.listSLA[idx],
              status: !state.listSLA[idx].status,
            };
          }
        });
      }

      if (action.payload.type === "change-notification") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listSLA[idx] = {
              ...state.listSLA[idx],
              turn_off_notification: !state.listSLA[idx].turn_off_notification,
            };
          }
        });
      }
      //   else {
      //     // eslint-disable-next-line array-callback-return
      //     tempList.map((item, idx) => {
      //       if (item._id === action.payload.id) {
      //         state.listSLA.splice(idx, 1);
      //       }
      //     });
      //   }
    },
    duplicateSLA() {},
    deleteSLA() {},

    //slaSetting
    loadFieldsObject() {},
    loadDataNecessaryEdit() {},
    createSLASetting() {},
    loadDetailsSLA() {},
    loadDetailsSLASuccess(state, action) {
      state.detailsSLA = action.payload;
    },
    editSLASetting() {},
    changeStateEdit(state, action) {
      state.isEdit = action.payload;
    },

    unMountSLA(state) {
      state.listSLA = [];
      state.totalRecordSLA = 0;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadListSLA,
  loadListSLASuccess,
  activeSLA,
  toggleNotification,
  changeState,
  duplicateSLA,
  deleteSLA,
  loadFieldsObject,
  loadDataNecessaryEdit,
  createSLASetting,
  loadDetailsSLA,
  loadDetailsSLASuccess,
  editSLASetting,
  changeStateEdit,
  unMountSLA,
} = slice.actions;
