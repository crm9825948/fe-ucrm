import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  listMessage: [],
  listSMS: [],
  listSMSTemplate: [],
  smsTemplateDetail: {},
  totalRecord: 0,
  isLoadMore: false,
};

const slice = createSlice({
  name: "componentSMS",
  initialState,
  reducers: {
    getListMessage() {},
    getListMessageSuccess(state, action) {
      state.listMessage = action.payload;
      state.isLoading = false;
    },
    sendSMS() {},
    getListSMSTemplate() {},
    getListSMSTemplateSuccess(state, action) {
      state.listSMSTemplate = action.payload;
    },
    getSmsTemplateDetail() {},
    getSmsTemplateDetailSuccess(state, action) {
      state.smsTemplateDetail = action.payload;
    },
    actionSendSMS() {},
    deleteSMS() {},
    changeListSMS(state, action) {
      state.listSMS = action.payload;
    },
    changeTotalRecord(state, action) {
      state.totalRecord = action.payload;
    },
    changeLoadMore(state, action) {
      state.isLoadMore = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  getListMessage,
  getListMessageSuccess,
  sendSMS,
  getListSMSTemplate,
  getListSMSTemplateSuccess,
  getSmsTemplateDetail,
  getSmsTemplateDetailSuccess,
  actionSendSMS,
  deleteSMS,
  changeListSMS,
  changeTotalRecord,
  changeLoadMore,
} = slice.actions;
