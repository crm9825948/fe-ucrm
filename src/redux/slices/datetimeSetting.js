import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allConfig: {},
};

const slice = createSlice({
  name: "datetimeSetting",
  initialState,
  reducers: {
    loadConfig() {},
    loadConfigSuccess(state, action) {
      state.allConfig = action.payload;
    },
    loadConfigFail(state) {
      state.allConfig = {};
    },
    editConfig() {},
    editConfigSuccess(state, action) {
      state.allConfig = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadConfig,
  loadConfigSuccess,
  loadConfigFail,
  editConfig,
  editConfigSuccess,
} = slice.actions;
