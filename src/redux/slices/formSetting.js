const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listField: [],
  listFieldMapping: [],
  infoField: {},
  listFormSetting: [],
  config_design: {},
  config_data: {},
  dataSetting: {},
  listObjectRelated: [],
  fieldsObjectRelated: [],
  listFieldConfig: [],
  dataReturn: {},
  isSubmit: false,
  isLoadingSubmit: false,
  listOrdered: ["Default"],
  columns: {
    Default: [],
  },
  sectionName: "",
  modalSection: false,
  openDrawer: "",
  listFieldObject: [],
  dataDelete: {},
  totalRecord: 0,
};
const handleColumns = (ordereds, listField) => {
  let tem = {};
  ordereds.forEach((item) => {
    let list = [];
    listField.forEach((field) => {
      if (field.section === item) {
        list.push(field);
      }
    });
    tem[item] = list;
  });
  return tem;
};
const slice = createSlice({
  name: "formSetting",
  initialState,
  reducers: {
    getFieldObject() {},
    getFieldObjectSuccess(state, action) {
      state.listField = action.payload;
    },
    updateFieldMapping(state, action) {
      state.listFieldMapping = action.payload;
    },
    deleteFieldMapping(state, action) {
      state.listFieldMapping = state.listFieldMapping.filter(
        (item) => item.id !== action.payload.id
      );
      let tem = [];
      state.listFieldMapping.forEach((item, index) => {
        tem.push({
          ...item,
          position: index,
        });
      });
      state.listFieldMapping = tem;
      state.columns = handleColumns(state.listOrdered, tem);
    },
    updateInfoField(state, action) {
      state.infoField = action.payload;
    },
    updateConfigDesign(state, action) {
      state.config_design = action.payload;
    },
    updateConfigData(state, action) {
      state.config_data = action.payload;
    },
    createFormsetting() {},
    updateFormSetting() {},
    deleteFormSetting() {},
    getListFormSetting() {},
    getListFormSettingSuccess(state, action) {
      state.listFormSetting = action.payload;
    },
    toggleFormSetting() {},
    getFormSetting() {},
    getFormSettingSuccess(state, action) {
      state.dataSetting = action.payload;
    },
    setLoadingSubmitForm(state, action) {
      state.isLoadingSubmit = action.payload;
    },
    submitForm() {},
    submitFormSuccess(state) {
      state.isLoadingSubmit = false;
    },
    getListRelatedObject() {},
    getListRelatedObjectSuccess(state, action) {
      state.listObjectRelated = action.payload;
    },
    getFieldObjectRelated() {},
    getFieldObjectRelatedSuccess(state, action) {
      state.fieldsObjectRelated = action.payload;
    },
    getListFieldConfigSuccess(state, action) {
      state.listFieldConfig = action.payload;
    },
    updateDataReturn(state, action) {
      state.dataReturn = action.payload;
      state.isSubmit = !state.isSubmit;
    },
    unMountFormSetting: () => initialState,
    setLoadingSubmit(state, action) {
      state.isLoadingSubmit = action.payload;
    },
    updateListOrdered(state, action) {
      state.listOrdered = action.payload;
    },
    updateColumns(state, action) {
      state.columns = action.payload;
    },
    updateSectionName(state, action) {
      state.sectionName = action.payload;
    },
    updateModalSection(state, action) {
      state.modalSection = action.payload;
    },
    handleDeleteSection(state, action) {
      state.listOrdered = state.listOrdered.filter(
        (item) => item !== action.payload.name
      );
      let index = 0;
      let tempMapping = [];
      state.listFieldMapping.forEach((item) => {
        if (item.section !== action.payload.name) {
          tempMapping.push({ ...item, position: index });
          index++;
        }
      });
      state.listFieldMapping = tempMapping;
      state.columns = handleColumns(state.listOrdered, tempMapping);
    },
    updateOpenDrawer(state, action) {
      state.openDrawer = action.payload;
    },
    updateListFieldOject(state, action) {
      state.listFieldObject = action.payload;
    },
    updateDataDelete(state, action) {
      state.dataDelete = action.payload;
    },
    updateTotalRecord(state, action) {
      state.totalRecord = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  getFieldObject,
  getFieldObjectSuccess,
  unMountFormSetting,
  updateFieldMapping,
  updateInfoField,
  deleteFieldMapping,
  createFormsetting,
  updateFormSetting,
  deleteFormSetting,
  getListFormSetting,
  getListFormSettingSuccess,
  updateConfigDesign,
  updateConfigData,
  toggleFormSetting,
  getFormSetting,
  getFormSettingSuccess,
  setLoadingSubmitForm,
  submitForm,
  submitFormSuccess,
  getListRelatedObject,
  getListRelatedObjectSuccess,
  getFieldObjectRelated,
  getFieldObjectRelatedSuccess,
  getListFieldConfigSuccess,
  updateDataReturn,
  updateIsSubmit,
  setLoadingSubmit,
  updateListOrdered,
  updateColumns,
  updateSectionName,
  updateModalSection,
  handleDeleteSection,
  updateOpenDrawer,
  updateListFieldOject,
  updateDataDelete,
  updateTotalRecord,
} = slice.actions;
