const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    listArticle: [],
    pagination: {
        page: 0,
        limit: 12
    },
    totalPage: 0,
    totalRecord: 0,
    searchArticle: '',
    popupView: false,
    articleDetail: {}
}

const slice = createSlice({
    name: "componentKnowledgeBase",
    initialState,
    reducers: {
        getListArticle() {
        },
        getListArticleSuccess(state, action) {
            state.listArticle = action.payload
            state.pagination = {
                page: 1,
                limit: 12
            }
        },
        loadMoreArticle() {
        },
        loadMoreArticleSuccess(state, action) {
            state.listArticle = state.listArticle.concat(action.payload)
            state.pagination = {
                page: state.pagination.page + 1,
                limit: 12
            }
        },
        updatePagination(state, action) {
            state.pagination = action.payload
        },
        updateTotalPageAndRecord(state, action) {
            state.totalPage = action.payload.totalPage
            state.totalRecord = action.payload.totalRecord
        },
        updateSearchArticle(state, action) {
            state.searchArticle = action.payload
        },
        updatePopupView(state, action) {
            state.popupView = action.payload
        },
        selectArticleDetail(state, action) {
            state.articleDetail = action.payload
        },
        deleteArticle() {
        }
    }
})

export default slice.reducer

export const {
    getListArticle,
    getListArticleSuccess,
    loadMoreArticle,
    loadMoreArticleSuccess,
    updatePagination,
    updateTotalPageAndRecord,
    updateSearchArticle,
    deleteArticle,
    selectArticleDetail,
    updatePopupView
} = slice.actions