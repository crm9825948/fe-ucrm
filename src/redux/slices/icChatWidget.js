import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  configIC: {
    fieldID: "",
    value: "",
  },
  allChatBot: [],
  allUserIC: [],
  completedMessages: [],
  assignedMessages: [],
  isLoadingMessages: true,
  messageDetails: [],
  selectedMessage: {},
  statusIC: "available",
  tags: [],
  addTag: {
    visible: false,
    interaction_id: "",
  },
  currentPageIC: 1,
  recordPerPageIC: null,
  hasMoreIC: true,
  isActive: 0,
  newMessage: {},
  quickMessages: [],
  addAgent: {
    isLoading: false,
    visible: false,
    interaction_id: "",
    note_content: "",
    username_dest: "",
    username_source: "",
  },
  transferAgent: {
    isLoading: false,
    visible: false,
    interaction_id: "",
    note_content: "",
    username_dest: "",
    username_source: "",
  },
  userToAdd: [],
  searchMessages: [],
  indexMessage: null,
  facebookPost: {},
  listSocialRegister: [],
};

const slice = createSlice({
  name: "icChatWidget",
  initialState,
  reducers: {
    getConfigIC() {},
    getConfigICResult(state, action) {
      state.configIC = action.payload;
    },
    getAllUserIC() {},
    getAllUserICResult(state, action) {
      state.allUserIC = action.payload;
    },
    getChatBotIC() {},
    getChatBotICResult(state, action) {
      state.allChatBot = action.payload;
    },
    getListMessagesAssignedIC(state) {
      state.isLoadingMessages = true;
    },
    getListMessagesAssignedICResult(state, action) {
      state.assignedMessages = action.payload;
      state.isLoadingMessages = false;
    },
    getListMessagesCompletedIC(state) {
      state.isLoadingMessages = true;
    },
    getListMessagesCompletedICResult(state, action) {
      state.completedMessages = action.payload;
      state.isLoadingMessages = false;
    },
    $currentPageIC(state, action) {
      state.currentPageIC = action.payload;
    },
    $recordPerPageIC(state, action) {
      state.recordPerPageIC = action.payload;
    },
    $isActive(state, action) {
      state.isActive = action.payload;
    },
    getMessageDetailsIC() {},
    getMessageDetailsICResult(state, action) {
      if (action.payload.page === 1 || action.payload.page === 0) {
        if (action.payload.channel_type === "email") {
          state.messageDetails = action.payload.data;
        } else {
          state.messageDetails = action.payload.data?.reverse();
        }
      } else {
        if (action.payload.channel_type === "email") {
          state.messageDetails = state.messageDetails.concat(
            action.payload.data
          );
        } else {
          state.messageDetails = state.messageDetails.concat(
            action.payload.data?.reverse()
          );
        }
      }

      if (
        action.payload.channel_type === "facebook_comment" ||
        action.payload.channel_type === "instagram_comment" ||
        action.payload.data.length < 20
      ) {
        state.hasMoreIC = false;
      } else {
        state.hasMoreIC = true;
      }
    },
    $selectedMessage(state, action) {
      state.selectedMessage = action.payload;
    },
    getStatusIC() {},
    getStatusICResult(state, action) {
      state.statusIC = action.payload;
    },
    updateStatusIC() {},
    getTagsIC() {},
    getTagsICResult(state, action) {
      state.tags = action.payload;
    },
    addTagIC() {},
    addTagICResult(state, action) {
      let tempAssigned = [...state.assignedMessages];
      let tempCompleted = [...state.completedMessages];
      const interactionIDAssigned = tempAssigned.findIndex(
        (item) => item?.interaction_id === action.payload?.interaction_id
      );
      const interactionIDCompleted = tempCompleted.findIndex(
        (item) => item?.interaction_id === action.payload?.interaction_id
      );
      if (interactionIDAssigned !== -1) {
        tempAssigned[interactionIDAssigned]?.interactionTags?.push(
          action.payload
        );
        state.assignedMessages = tempAssigned;
      }
      if (interactionIDCompleted !== -1) {
        tempCompleted[interactionIDCompleted]?.interactionTags?.push(
          action.payload
        );
        state.completedMessages = tempCompleted;
      }
    },
    deleteTagIC() {},
    deleteTagICResult(state, action) {
      let tempAssigned = [...state.assignedMessages];
      let tempCompleted = [...state.completedMessages];
      const interactionIDAssigned = tempAssigned.findIndex(
        (item) => item?.interaction_id === action.payload?.interaction_id
      );
      const interactionIDCompleted = tempCompleted.findIndex(
        (item) => item?.interaction_id === action.payload?.interaction_id
      );
      if (interactionIDAssigned !== -1) {
        const indexInteraction = tempAssigned[
          interactionIDAssigned
        ]?.interactionTags.findIndex(
          (item) => item?.id === action.payload?.interaction_tag_id
        );
        if (indexInteraction !== -1) {
          tempAssigned[interactionIDAssigned]?.interactionTags?.splice(
            indexInteraction,
            1
          );
          state.assignedMessages = tempAssigned;
        }
      }
      if (interactionIDCompleted !== -1) {
        const indexInteraction = tempCompleted[
          interactionIDCompleted
        ]?.interactionTags.findIndex(
          (item) => item?.id === action.payload?.interaction_tag_id
        );
        if (indexInteraction !== -1) {
          tempCompleted[interactionIDCompleted]?.interactionTags?.splice(
            indexInteraction,
            1
          );
          state.completedMessages = tempCompleted;
        }
      }
    },
    createTagIC() {},
    $addTag(state, action) {
      state.addTag = action.payload;
    },
    disableBotIC() {},
    sendMessageIC() {},
    sendMessageWithFileIC() {},
    completeConversationIC() {},
    $newMessage(state, action) {
      state.newMessage = action.payload;
    },
    addNewMessage(state, action) {
      if (
        !state.messageDetails?.find(
          (item) =>
            item?.id === action.payload?.id ||
            item?.id === action.payload?.interaction_detail_id
        )
      ) {
        state.messageDetails = [action.payload, ...state.messageDetails];
      } else {
        const indexMessage = state.messageDetails?.findIndex(
          (item) =>
            item?.id === action.payload?.id ||
            item?.id === action.payload?.interaction_detail_id
        );
        if (indexMessage > -1) {
          let tempMessageDetails = [...state.messageDetails];

          tempMessageDetails[indexMessage] = {
            ...tempMessageDetails[indexMessage],
            ...action.payload,
          };
          state.messageDetails = tempMessageDetails;
        }
      }

      let temp = [...state.assignedMessages];
      const findIndex = temp.findIndex(
        (item) => item?.interaction_id === action.payload?.interaction_id
      );
      if (findIndex !== -1) {
        temp[findIndex] = {
          ...temp[findIndex],
          ...action.payload,
        };
      }
      state.assignedMessages = temp;
    },
    resgisterSignalrIC() {},
    registerConnectionIntervalIC() {},
    getQuickMessagesIC() {},
    getQuickMessagesICResult(state, action) {
      state.quickMessages = action.payload;
    },
    getUserToAddIC() {},
    getUserToAddICResult(state, action) {
      state.userToAdd = action.payload;
    },
    $addAgent(state, action) {
      state.addAgent = action.payload;
    },
    addAgentIC() {},
    transferAgentIC() {},
    $transferAgent(state, action) {
      state.transferAgent = action.payload;
    },
    searchMessagesIC() {},
    searchMessagesICResult(state, action) {
      state.searchMessages = action.payload;
    },
    getIndexMessageIC() {},
    getIndexMessageICResult(state, action) {
      state.indexMessage = action.payload;
    },
    getFacebookPostIC() {},
    getFacebookPostICResult(state, action) {
      state.facebookPost = action.payload;
    },
    getListSocialRegisterIC() {},
    getListSocialRegisterICResult(state, action) {
      state.listSocialRegister = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  getConfigIC,
  getConfigICResult,
  getAllUserIC,
  getAllUserICResult,
  getChatBotIC,
  getChatBotICResult,
  getListMessagesAssignedIC,
  getListMessagesAssignedICResult,
  getListMessagesCompletedIC,
  getListMessagesCompletedICResult,
  $currentPageIC,
  $recordPerPageIC,
  $isActive,
  getMessageDetailsIC,
  getMessageDetailsICResult,
  $selectedMessage,
  getStatusIC,
  getStatusICResult,
  updateStatusIC,
  getTagsIC,
  getTagsICResult,
  addTagIC,
  addTagICResult,
  deleteTagIC,
  deleteTagICResult,
  createTagIC,
  $addTag,
  disableBotIC,
  sendMessageIC,
  sendMessageWithFileIC,
  completeConversationIC,
  $newMessage,
  addNewMessage,
  resgisterSignalrIC,
  registerConnectionIntervalIC,
  getQuickMessagesIC,
  getQuickMessagesICResult,
  $addAgent,
  getUserToAddIC,
  getUserToAddICResult,
  addAgentIC,
  transferAgentIC,
  $transferAgent,
  searchMessagesIC,
  searchMessagesICResult,
  getIndexMessageIC,
  getIndexMessageICResult,
  getFacebookPostIC,
  getFacebookPostICResult,
  getListSocialRegisterIC,
  getListSocialRegisterICResult,
} = slice.actions;
