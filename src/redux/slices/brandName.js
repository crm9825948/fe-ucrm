const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listLogo: [],
  defaultBrandName: {
    media_url: "",
    media_config: {
      position: {},
      scale: 0,
      border: 0,
      borderRadius: "0%",
      width: 150,
      height: 150,
    },
    theme_color: localStorage.getItem("themeColor") || "#20a2a2",
  },
  detailLogo: {},
};

const slice = createSlice({
  name: "brandName",
  initialState,
  reducers: {
    getListLogo() {},
    getListLogoSuccess(state, action) {
      state.listLogo = action.payload;
    },
    deleteLogoById() {},
    getDefaultBrandName() {},
    getDefaultBrandNameSuccess(state, action) {
      state.defaultBrandName = action.payload;
    },
    uploadLogo() {},
    changeThemeColor(state, action) {
      state.defaultBrandName.theme_color = action.payload;
    },
    setDetailLogo(state, action) {
      state.detailLogo = action.payload;
    },
    submitConfigBrandName() {},
  },
});

export default slice.reducer;

export const {
  getListLogo,
  getListLogoSuccess,
  deleteLogoById,
  uploadLogo,
  getDefaultBrandName,
  getDefaultBrandNameSuccess,
  changeThemeColor,
  setDetailLogo,
  submitConfigBrandName,
} = slice.actions;
