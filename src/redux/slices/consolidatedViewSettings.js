import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: {
    coreSetting: false,
    exposeCreate: false,
    userMappingIC: false,
    userMappingCTI: false,
    userMappingCallCenter: false,
  },
  //ExposeAPI
  allExpose: null,
  statusUpdateExpose: null,
  //PopupSetting
  allPopupSetting: null,
  statusUpdatePopup: null,
  isLoadingCreate: false,
  detailsPopup: {
    data: {},
    pagination: {
      total_page: null,
      total_record: null,
    },
  },
  statusCreateRecordPopup: null,
  statusManyParam: false,
  //
  components: [],
  loadingComponents: false,
  //
  isLoadingDelete: false,
  isLoadingUpdate: false,
  //CoreSetting
  listCoreSetting: null,
  fieldsObject: null,
  statusCoreSetting: null,
  detailsCoreSetting: null,
  //ExposeAPICreate
  listExposeAPICreate: null,
  // fieldsObject: null,
  statusExposeAPICreate: null,
  detailsExposeAPICreate: null,
  //ICIntegration
  usersMapping: null,
  statusUserMapping: null,
  detailsUserMappingIC: null,
  fieldsObjectIC: null,
  tokenIC: null,
  objectFieldsConfig: null,
  configIC: null,
  detailsMappingExposeAPI: null,
  layouts: {},
  isLoadingLoadLayout: false,
  //
  detailsComponent: {},
  isloading: false,

  //
  isLoadingUpdateTab: false,
  logs: [],
  //
  sla: [],
  //isloadingLogs
  isloadingLogs: false,
  //VoiceBotSetting
  detailsVoiceBot: null,
  //timeline
  isLoadingTimeline: false,
  isLoadingTimelineVertical: false,
  timelines: [],
  timelinesVertical: [],
  totalTimelines: 0,
  totalTimelinesVertical: 0,
  hasMore: true,
  objectsTarget: [],
  statusUpdateTimeline: null,
  recordInteraction: [],
  //CTI Setting
  configCTI: [],
  userMappingCTI: [],
  statusUserMappingCTI: null,
  detailsUserMappingCTI: null,
  fieldsObjectToCall: [],
  tokenC247: null,
  //
  isLoadingUpdateRecord: false,
  //Callcenter
  configCallCenter: {},
  userMappingCallCenter: [],
  detailsUserMappingCallCenter: null,
  statusUserMappingCallCenter: null,
  //Voice Biometric
  configVoiceBiometric: {},
  loadingCreateInteractionConfig: false,
  pageTitle: [],
  interactionConfig: {},
  layoutsCenter: {
    center: [],
    left: [],
    right: [],
    top: [],
    tabs: [],
  },
  layoutsLeft: {
    center: [],
    left: [],
    right: [],
    top: [],
    tabs: [],
  },
  layoutsRight: {
    center: [],
    left: [],
    right: [],
    top: [],
    tabs: [],
  },
};

const slice = createSlice({
  name: "consolidatedViewSettings",
  initialState,
  reducers: {
    updateTab(state, action) {
      state.isLoadingUpdateTab = true;
    },
    updateTabSuccess(state, action) {
      state.isLoadingUpdateTab = false;
    },
    updateTabFail(state, action) {
      state.isLoadingUpdateTab = false;
    },
    loadDataNecessary() {},
    resetStatus(state, action) {
      state.statusUpdateExpose = null;
      state.statusUpdatePopup = null;
      state.statusCoreSetting = null;
      state.statusExposeAPICreate = null;
      state.statusUserMapping = null;
      state.statusUserMappingCTI = null;
      state.statusUpdateTimeline = null;
      state.statusUserMappingCallCenter = null;
    },

    //ExposeAPI
    loadAllExpose(state, action) {},
    loadAllExposeSuccess(state, action) {
      state.allExpose = action.payload;
    },
    updateExpose(state, action) {},
    updateExposeSuccess(state, action) {
      state.statusUpdateExpose = "success";
    },

    //PopupSetting
    loadAllPopupSetting(state, action) {},
    loadAllPopupSettingSuccess(state, action) {
      state.allPopupSetting = action.payload;
    },
    updatePopup(state, action) {},
    updatePopupSuccess(state, action) {
      state.statusUpdatePopup = "success";
    },
    loadPopup() {},
    loadPopupSuccess(state, action) {
      if (action.payload.type === "pagination") {
        state.detailsPopup = {
          ...state.detailsPopup,
          pagination: {
            total_page: action.payload.data.total_page,
            total_record: action.payload.data.total_record,
          },
        };
      } else {
        state.detailsPopup = {
          ...state.detailsPopup,
          data: { ...action.payload.data },
        };
      }
    },
    popupCheckWorkflow() {},
    createRecordPopup() {},
    createRecordPopupSuccess(state, action) {
      state.statusCreateRecordPopup = action.payload;
    },
    updateRecordPopup() {},
    updateRecordPopupResult(state, action) {
      state.statusCreateRecordPopup = action.payload;
    },
    updateRecordManyParam() {},
    updateRecordManyParamResult(state, action) {
      state.statusManyParam = action.payload;
    },
    //Create component
    createComponent(state, action) {
      state.isLoadingCreate = true;
    },
    createComponentSuccesss(state, action) {
      state.isLoadingCreate = false;
    },
    createComponentFail(state, action) {
      state.isLoadingCreate = false;
    },
    //load component
    loadComponents(state, action) {
      state.loadingComponents = true;
    },
    loadComponentsSuccess(state, action) {
      state.components = action.payload;
      state.loadingComponents = false;
    },
    loadComponentsFail(state, action) {
      state.loadingComponents = false;
    },
    //delete
    deleteComponent(state, action) {
      state.isLoadingDelete = true;
    },
    deleteComponentSuccess(state, action) {
      state.isLoadingDelete = false;
    },
    deleteComponentFail(state, action) {
      state.isLoadingDelete = false;
    },
    //Update
    updateComponent(state, action) {
      state.isLoadingUpdate = true;
    },
    updateComponentSuccess(state, action) {
      state.isLoadingUpdate = false;
    },
    updateComponentFail(state, action) {
      state.isLoadingUpdate = false;
    },
    //CoreSetting
    loadDataCoreSetting(state, action) {},
    loadCoreSetting(state, action) {},
    loadCoreSettingSuccess(state, action) {
      state.listCoreSetting = action.payload;
    },
    loadFieldsCreate(state, action) {},
    loadFieldsCreateSuccess(state, action) {
      state.fieldsObject = action.payload;
    },
    createCoreSetting(state, action) {},
    createCoreSettingSuccess(state, action) {
      state.statusCoreSetting = "success";
    },
    updateCoreSetting(state, action) {},
    updateCoreSettingSuccess(state, action) {
      state.statusCoreSetting = "success";
    },
    deleteCoreSetting(state, action) {
      state.loading.coreSetting = true;
    },
    deleteCoreSettingSuccess(state, action) {
      state.loading.coreSetting = false;
    },
    deleteCoreSettingFail(state, action) {
      state.loading.coreSetting = false;
    },
    changeStateCoreSetting(state, action) {
      if (action.payload.type === "delete") {
        let tempList = [...state.listCoreSetting];

        tempList.forEach((item, idx) => {
          if (item._id === action.payload.api_id) {
            state.listCoreSetting.splice(idx, 1);
          }
        });
      } else {
        state.detailsCoreSetting = null;
      }
    },
    loadDetailsCoreSetting(state, action) {},
    loadDetailsCoreSettingSuccess(state, action) {
      state.detailsCoreSetting = action.payload;
    },
    loadDetailsCoreSettingFail(state, action) {
      state.detailsCoreSetting = null;
    },
    //ExposeAPICreate
    loadDataExposeCreate(state, action) {},
    loadExposeCreate(state, action) {},
    loadExposeCreateSuccess(state, action) {
      state.listExposeAPICreate = action.payload;
    },
    createExposeCreate(state, action) {},
    createExposeCreateSuccess(state, action) {
      state.statusExposeAPICreate = "success";
    },
    updateExposeCreate(state, action) {},
    updateExposeCreateSuccess(state, action) {
      state.statusExposeAPICreate = "success";
    },
    deleteExposeCreate(state, action) {
      state.loading.exposeCreate = true;
    },
    deleteExposeCreateSuccess(state, action) {
      state.loading.exposeCreate = false;
      state.statusExposeAPICreate = "delete success";
    },
    deleteExposeCreateFail(state, action) {
      state.loading.exposeCreate = false;
    },
    changeStateExposeCreate(state, action) {
      if (action.payload.type === "delete") {
        let tempList = [...state.listExposeAPICreate];

        tempList.forEach((item, idx) => {
          if (item._id === action.payload._id) {
            state.listExposeAPICreate.splice(idx, 1);
          }
        });
      } else {
        state.detailsExposeAPICreate = null;
      }
    },
    loadDetailsExposeCreate(state, action) {},
    loadDetailsExposeCreateSuccess(state, action) {
      state.detailsExposeAPICreate = action.payload;
    },
    loadDetailsExposeCreateFail(state, action) {
      state.detailsExposeAPICreate = null;
    },

    //ICIntegration
    loadDataICIntegration(state, action) {},
    loadUsersMappingIC(state, action) {},
    loadUsersMappingICSuccess(state, action) {
      state.usersMapping = action.payload;
    },
    createUserMappingIC(state, action) {},
    createUserMappingICSuccess(state, action) {
      state.statusUserMapping = "success";
    },
    deleteUserMappingIC(state, action) {
      state.loading.userMappingIC = true;
    },
    deleteUserMappingICResult(state, action) {
      state.loading.userMappingIC = false;
    },
    changeStateICIntegration(state, action) {
      if (action.payload.type === "delete") {
        let tempList = [...state.usersMapping];
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.usersMapping.splice(idx, 1);
          }
        });
      } else {
        state.detailsUserMappingIC = null;
      }
    },
    loadDetailsUserMappingIC(state, action) {},
    loadDetailsUserMappingICSuccess(state, action) {
      state.detailsUserMappingIC = action.payload;
    },
    loadDetailsUserMappingICFail(state, action) {
      state.detailsUserMappingIC = null;
    },
    updateUserMappingIC(state, action) {},
    updateUserMappingICSuccess(state, action) {
      state.statusUserMapping = "success";
    },
    loadFieldsConfig(state, action) {},
    loadFieldsConfigSuccess(state, action) {
      state.fieldsObjectIC = action.payload;
    },
    getToken(state, action) {},
    getTokenSuccess(state, action) {
      state.tokenIC = action.payload;
    },
    loadObjectFieldsConfig(state, action) {},
    loadObjectFieldsConfigSuccess(state, action) {
      state.objectFieldsConfig = action.payload;
    },
    loadConfigICSuccess(state, action) {
      state.configIC = action.payload;
    },
    createConfigIC(state, action) {},
    updateConfigIC(state, action) {},
    loadMappingExpose(state, action) {},
    loadMappingExposeSuccess(state, action) {
      state.detailsMappingExposeAPI = action.payload;
    },
    createMappingExpose(state, action) {},
    updateMappingExpose(state, action) {},
    resetAllIC() {},
    //
    loadLayouts(state, action) {
      state.isLoadingLoadLayout = true;
    },
    loadLayoutsSuccess(state, action) {
      state.isLoadingLoadLayout = false;
      state.layouts = action.payload;
    },
    loadLayoutsFail(state, action) {
      state.isLoadingLoadLayout = false;
    },
    updateLayout(state, action) {
      state.isLoadingUpdate = true;
    },
    updateLayoutSuccess(state, action) {
      state.isLoadingUpdate = false;
    },
    updateLayoutFail(state, action) {
      state.isLoadingUpdate = false;
    },
    loadDetailsComponent(state, action) {
      state.isLoading = false;
    },
    loadDetailsComponentSuccess(state, action) {
      state.isLoading = false;
      state.detailsComponent = {};
    },
    loadDetailsComponentFail(state, action) {
      state.isLoading = false;
    },
    updateRecord(state, action) {
      state.isLoadingUpdateRecord = true;
    },
    updateRecordSuccess(state, action) {
      state.isLoadingUpdateRecord = false;
    },
    updateRecordFail(state, action) {
      state.isLoadingUpdateRecord = false;
    },

    //
    getLogs(state, action) {
      state.isloadingLogs = true;
    },
    getLogsSuccess(state, action) {
      state.isloadingLogs = false;
      state.logs = action.payload;
    },
    getLogsFail(state, action) {
      state.isloadingLogs = false;
    },

    //
    getAllSla(state, action) {
      state.isLoading = true;
    },
    getAllSlaSuccess(state, action) {
      state.isLoading = false;
      state.sla = action.payload;
    },
    getAllSlaFail(state, action) {
      state.isLoading = false;
    },
    //VoiceBotSetting
    getDetailsVoiceBot() {},
    getDetailsVoiceBotSuccess(state, action) {
      state.detailsVoiceBot = action.payload;
    },
    getDetailsVoiceBotFail(state, action) {
      state.detailsVoiceBot = null;
    },
    createVoiceBotSetting() {},
    updateVoiceBotSetting() {},
    //Timeline
    getInteraction(state, action) {
      state.isLoadingTimeline = true;
    },
    getInteractionSuccess(state, action) {
      state.isLoadingTimeline = false;
      if (state.timelines.length > 0) {
        let temp = [...state.timelines];
        action.payload.data.map((item) => {
          return temp.push(item);
        });

        state.timelines = temp;
      } else {
        state.timelines = action.payload.data;
      }
      state.totalTimelines = action.payload.total_record;
    },
    getInteractionFail(state, action) {
      state.isLoadingTimeline = false;
      state.timelines = [];
      state.totalTimelines = 0;
    },
    getInteractionVertical(state, action) {
      state.isLoadingTimelineVertical = true;
    },
    getInteractionVerticalSuccess(state, action) {
      state.isLoadingTimelineVertical = false;
      if (state.timelinesVertical.length > 0) {
        let temp = [...state.timelinesVertical];
        action.payload.data.map((item) => {
          return temp.push(item);
        });

        state.timelinesVertical = temp;
        if (temp.length === action.payload.total_record) {
          state.hasMore = false;
        }
      } else {
        state.timelinesVertical = action.payload.data;
        if (action.payload.data.length === action.payload.total_record) {
          state.hasMore = false;
        }
      }
      state.totalTimelinesVertical = action.payload.total_record;
    },
    getInteractionVerticalFail(state, action) {
      state.isLoadingTimelineVertical = false;
      state.timelinesVertical = [];
      state.totalTimelinesVertical = 0;
      state.hasMore = false;
    },
    loadObjectsTarget() {},
    loadObjectsTargetSuccess(state, action) {
      state.objectsTarget = action.payload;
    },
    updateInteractionDetails() {},
    updateInteractionDetailsSuccess(state, action) {
      state.statusUpdateTimeline = action.payload;
    },
    loadRecordInteraction() {},
    loadRecordInteractionResult(state, action) {
      state.recordInteraction = action.payload;
    },
    //CTI Setting
    loadDataCTISetting() {},
    loadConfigCTISuccess(state, action) {
      state.configCTI = action.payload;
    },
    loadUsersMappingCTI() {},
    loadUsersMappingCTISuccess(state, action) {
      state.userMappingCTI = action.payload;
    },
    createUserMappingCTI() {},
    createUserMappingCTISuccess(state) {
      state.statusUserMappingCTI = "success";
    },
    deleteUserMappingCTI(state) {
      state.loading.userMappingCTI = true;
    },
    deleteUserMappingCTIResult(state) {
      state.loading.userMappingCTI = false;
    },
    changeStateCTISetting(state, action) {
      if (action.payload.type === "delete") {
        let tempList = [...state.userMappingCTI];
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.userMappingCTI.splice(idx, 1);
          }
        });
      } else {
        state.detailsUserMappingCTI = null;
      }
    },
    loadDetailsUserMappingCTI(state, action) {},
    loadDetailsUserMappingCTISuccess(state, action) {
      state.detailsUserMappingCTI = action.payload;
    },
    loadDetailsUserMappingCTIFail(state, action) {
      state.detailsUserMappingCTI = null;
    },
    updateUserMappingCTI(state, action) {},
    updateUserMappingCTISuccess(state, action) {
      state.statusUserMappingCTI = "success";
    },
    updateConfigCTI() {},
    loadFieldsObjectToCall() {},
    loadFieldsObjectToCallResult(state, action) {
      state.fieldsObjectToCall.push(action.payload);
    },
    generateTokenC247() {},
    generateTokenC247Result(state, action) {
      state.tokenC247 = action.payload;
    },
    resetAllCTI() {},

    //CallCenter
    loadCallCenterSetting() {},
    loadConfigCallCenterSuccess(state, action) {
      state.configCallCenter = action.payload;
    },
    loadUsersMappingCallCenter() {},
    loadUsersMappingCallCenterSuccess(state, action) {
      state.userMappingCallCenter = action.payload;
    },
    createUserMappingCallCenter() {},
    createUserMappingCallCenterSuccess(state) {
      state.statusUserMappingCallCenter = "success";
    },
    deleteUserMappingCallCenter(state) {
      state.loading.userMappingCallCenter = true;
    },
    deleteUserMappingCallCenterResult(state) {
      state.loading.userMappingCallCenter = false;
    },
    changeStateCallCenterSetting(state, action) {
      if (action.payload.type === "delete") {
        let tempList = [...state.userMappingCallCenter];
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.userMappingCallCenter.splice(idx, 1);
          } else {
            state.detailsUserMappingCallCenter = null;
          }
        });
      }
    },
    setDetailsUserMappingCallCenter(state, action) {
      state.detailsUserMappingCallCenter = action.payload;
    },
    updateUserMappingCallCenter() {},
    updateUserMappingCallCenterSuccess(state, action) {
      state.statusUserMappingCallCenter = "success";
    },
    updateConfigCallCenter() {},
    testConnectionCalabrio() {},
    updateCallScoring() {},
    resetAllConfigCallCenter() {},

    //Voice Biometric
    getConfigVoiceBiometric() {},
    getConfigVoiceBiometricResult(state, action) {
      state.configVoiceBiometric = action.payload;
    },
    updateConfigVoiceBiometric() {},
    deleteConfigVoiceBiometric() {},
    createInteractionConfig(state, action) {
      state.loadingCreateInteractionConfig = true;
    },
    createInteractionConfigSuccess(state, action) {
      state.loadingCreateInteractionConfig = false;
    },
    createInteractionConfigFail(state, action) {
      state.loadingCreateInteractionConfig = false;
    },
    updateInteractionConfig(state, action) {
      state.loadingCreateInteractionConfig = true;
    },
    updateInteractionConfigResult(state, action) {
      state.loadingCreateInteractionConfig = false;
    },
    interactionConfig(state, action) {},
    interactionConfigSuccess(state, action) {
      state.interactionConfig = action.payload;
    },
    interactionConfigFail(state, action) {},
    deleteInteractionConfig(state, action) {},
    loadPageTitle() {},
    loadPageTitleSuccess(state, action) {
      state.pageTitle = action.payload;
    },
    //v3
    loadConfigComponents() {},
    loadConfigComponentsSuccess(state, action) {
      state.layoutsCenter = action.payload.layouts_center;
      state.layoutsLeft = action.payload.layouts_left;
      state.layoutsRight = action.payload.layouts_right;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  resetStatus,
  //ExposeAPI
  loadAllExpose,
  loadAllExposeSuccess,
  updateExpose,
  updateExposeSuccess,
  //PopupSetting
  loadAllPopupSetting,
  loadAllPopupSettingSuccess,
  updatePopup,
  updatePopupSuccess,
  loadPopup,
  loadPopupSuccess,
  popupCheckWorkflow,
  createRecordPopup,
  createRecordPopupSuccess,
  updateRecordPopup,
  updateRecordPopupResult,
  updateRecordManyParam,
  updateRecordManyParamResult,
  //CoreSetting
  loadDataCoreSetting,
  loadCoreSetting,
  loadCoreSettingSuccess,
  loadFieldsCreate,
  loadFieldsCreateSuccess,
  createCoreSetting,
  createCoreSettingSuccess,
  updateCoreSetting,
  updateCoreSettingSuccess,
  deleteCoreSetting,
  deleteCoreSettingSuccess,
  deleteCoreSettingFail,
  changeStateCoreSetting,
  loadDetailsCoreSetting,
  loadDetailsCoreSettingSuccess,
  loadDetailsCoreSettingFail,
  //ExposeAPICreate
  loadDataExposeCreate,
  loadExposeCreate,
  loadExposeCreateSuccess,
  createExposeCreate,
  createExposeCreateSuccess,
  loadDetailsExposeCreate,
  loadDetailsExposeCreateSuccess,
  loadDetailsExposeCreateFail,
  updateExposeCreate,
  updateExposeCreateSuccess,
  changeStateExposeCreate,
  deleteExposeCreate,
  deleteExposeCreateSuccess,
  deleteExposeCreateFail,
  //ICIntegration
  loadDataICIntegration,
  loadUsersMappingIC,
  loadUsersMappingICSuccess,
  createUserMappingIC,
  createUserMappingICSuccess,
  deleteUserMappingIC,
  deleteUserMappingICResult,
  changeStateICIntegration,
  loadDetailsUserMappingIC,
  loadDetailsUserMappingICSuccess,
  loadDetailsUserMappingICFail,
  updateUserMappingIC,
  updateUserMappingICSuccess,
  loadFieldsConfig,
  loadFieldsConfigSuccess,
  getToken,
  getTokenSuccess,
  loadObjectFieldsConfig,
  loadObjectFieldsConfigSuccess,
  loadConfigICSuccess,
  createConfigIC,
  updateConfigIC,
  loadMappingExpose,
  loadMappingExposeSuccess,
  createMappingExpose,
  updateMappingExpose,
  resetAllIC,
  //
  createComponent,
  createComponentSuccesss,
  createComponentFail,
  loadComponents,
  loadComponentsSuccess,
  loadComponentsFail,
  deleteComponent,
  deleteComponentSuccess,
  deleteComponentFail,
  updateComponent,
  updateComponentSuccess,
  updateComponentFail,
  //layout
  loadLayouts,
  loadLayoutsSuccess,
  loadLayoutsFail,
  updateLayout,
  updateLayoutSuccess,
  updateLayoutFail,
  loadDetailsComponent,
  loadDetailsComponentFail,
  loadDetailsComponentSuccess,
  updateTab,
  updateTabSuccess,
  updateTabFail,
  getLogs,
  getLogsFail,
  getLogsSuccess,
  //sla
  getAllSla,
  getAllSlaSuccess,
  getAllSlaFail,
  //VoiceBotSetting
  getDetailsVoiceBot,
  getDetailsVoiceBotSuccess,
  getDetailsVoiceBotFail,
  createVoiceBotSetting,
  updateVoiceBotSetting,
  //Timeline
  getInteraction,
  getInteractionSuccess,
  getInteractionFail,
  getInteractionVertical,
  getInteractionVerticalSuccess,
  getInteractionVerticalFail,
  loadObjectsTarget,
  loadObjectsTargetSuccess,
  updateInteractionDetails,
  updateInteractionDetailsSuccess,
  loadRecordInteraction,
  loadRecordInteractionResult,
  //CTI Setting
  loadDataCTISetting,
  loadConfigCTISuccess,
  loadUsersMappingCTI,
  loadUsersMappingCTISuccess,
  createUserMappingCTI,
  createUserMappingCTISuccess,
  deleteUserMappingCTI,
  deleteUserMappingCTIResult,
  changeStateCTISetting,
  loadDetailsUserMappingCTI,
  loadDetailsUserMappingCTISuccess,
  loadDetailsUserMappingCTIFail,
  updateUserMappingCTI,
  updateUserMappingCTISuccess,
  updateConfigCTI,
  loadFieldsObjectToCall,
  loadFieldsObjectToCallResult,
  resetAllCTI,
  generateTokenC247,
  generateTokenC247Result,
  //
  updateRecord,
  updateRecordSuccess,
  updateRecordFail,
  //CallCenter
  loadCallCenterSetting,
  loadConfigCallCenterSuccess,
  loadUsersMappingCallCenter,
  loadUsersMappingCallCenterSuccess,
  createUserMappingCallCenter,
  createUserMappingCallCenterSuccess,
  deleteUserMappingCallCenter,
  deleteUserMappingCallCenterResult,
  changeStateCallCenterSetting,
  updateUserMappingCallCenter,
  updateUserMappingCallCenterSuccess,
  setDetailsUserMappingCallCenter,
  updateConfigCallCenter,
  testConnectionCalabrio,
  updateCallScoring,
  resetAllConfigCallCenter,

  //Voice Biometric
  getConfigVoiceBiometric,
  getConfigVoiceBiometricResult,
  updateConfigVoiceBiometric,
  deleteConfigVoiceBiometric,

  //
  createInteractionConfig,
  createInteractionConfigFail,
  createInteractionConfigSuccess,
  //
  updateInteractionConfig,
  updateInteractionConfigResult,
  //
  interactionConfig,
  interactionConfigSuccess,
  interactionConfigFail,
  //
  deleteInteractionConfig,
  loadPageTitle,
  loadPageTitleSuccess,
  //v3
  loadConfigComponents,
  loadConfigComponentsSuccess,
} = slice.actions;
