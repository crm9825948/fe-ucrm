import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  logs: {},
};

const slice = createSlice({
  name: "systemLog",
  initialState,
  reducers: {
    loadLogs(state, action) {
      state.isLoading = true;
    },
    loadLogsSuccess(state, action) {
      state.isLoading = false;
      state.logs = action.payload;
    },
    loadLogsFail(state, action) {
      state.isLoading = false;
    },
  },
});

export default slice.reducer;

export const { loadLogs, loadLogsFail, loadLogsSuccess } = slice.actions;
