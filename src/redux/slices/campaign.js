import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listFieldsCampaignTask: [],
  listFieldsCampaignMember: [],
  activateStatus: false,
  listCampaign: [],
  listObjects: [],
  validateObject: false,
  detailsCampaign: {
    campaignInfo: {},
    campaignConfig: {},
  },
  infoComponent: {},
  templateComponent: {},
  isLoading: {
    infoComponent: false,
    templateComponent: false,
    active: false,
    contentCampaignMailchimp: false,
  },
  infoMailChimp: {},
  listCampaignMailChimp: [],
  contentCampaignMailchimp: {
    archive_html: "",
  },
  templateSMSCampaignTasks: [],
};

const slice = createSlice({
  name: "campaignNew",
  initialState,
  reducers: {
    checkAvailable() {},
    activateCampaign(state) {
      state.isLoading.active = true;
    },
    deactivateCampaign(state) {
      state.isLoading.active = true;
    },
    updateLoadingActive(state, action) {
      state.isLoading.active = action.payload;
    },
    updateActivateCampaign(state, action) {
      state.activateStatus = action.payload;
    },

    loadFieldsCampaignMember() {},
    loadFieldsCampaignMemberResult(state, action) {
      state.listFieldsCampaignMember = action.payload;
    },
    loadCampaign() {},
    loadCampaignResult(state, action) {
      state.listCampaign = action.payload;
    },
    loadObjects() {},
    loadObjectsResult(state, action) {
      state.listObjects = action.payload;
    },
    validateObjects() {},
    validateObjectsResult(state, action) {
      state.validateObject = action.payload;
    },
    loadFieldsCampaignTask() {},
    loadFieldsCampaignTaskSuccess(state, action) {
      state.listFieldsCampaignTask = action.payload;
    },
    createCampaign() {},
    loadDetailsCampaign() {},
    loadDetailsCampaignResult(state, action) {
      state.detailsCampaign = action.payload;
    },
    updateCampaign() {},

    loadInfoComponent(state) {
      state.isLoading.infoComponent = true;
    },
    loadInfoComponentResult(state, action) {
      state.infoComponent = action.payload;
      state.isLoading.infoComponent = false;
    },
    loadTemplateComponent(state) {
      state.isLoading.templateComponent = true;
    },
    loadTemplateComponentResult(state, action) {
      state.templateComponent = action.payload;
      state.isLoading.templateComponent = false;
    },
    updateCampaignConfig() {},
    deleteCampaign() {},
    getInfoMailChimp() {},
    getInfoMailChimpResult(state, action) {
      state.infoMailChimp = action.payload;
    },
    connectMailchimp() {},
    disconnectMailchimp() {},
    getListCampaignMailChimp() {},
    getListCampaignMailChimpResult(state, action) {
      state.listCampaignMailChimp = action.payload;
    },
    getContentCampaignMailchimp(state) {
      state.isLoading.contentCampaignMailchimp = true;
    },
    getContentCampaignMailchimpResult(state, action) {
      state.contentCampaignMailchimp = action.payload;
      state.isLoading.contentCampaignMailchimp = false;
    },
    getTemplateCampaignTasks() {},
    getTemplateCampaignTasksResult(state, action) {
      state.templateSMSCampaignTasks = action.payload;
    },

    // loadSetingsCampaign() {},
    // loadRulesSuccess(state, action) {
    //   state.taskRules = action.payload;
    // },
    // loadTaskCampaign() {},
    // loadTaskCampaignSuccess(state, action) {
    //   state.taskCampaigns = action.payload;
    // },
    // generateTask() {},
    // updateStatusGenerateTask(state, action) {
    //   state.statusGenerateTask = action.payload;
    // },
    // runCampaign() {},
    // stopCampaign() {},

    unMountCampaignSetting(state) {
      state.listFieldsCampaignTask = [];
      state.listObjects = [];
      state.validateObject = false;
      state.detailsCampaign = {
        campaignInfo: {},
        campaignConfig: {},
      };
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  checkAvailable,
  activateCampaign,
  deactivateCampaign,
  updateLoadingActive,
  updateActivateCampaign,
  loadFieldsCampaignMember,
  loadFieldsCampaignMemberResult,
  loadCampaign,
  loadCampaignResult,
  loadObjects,
  loadObjectsResult,
  validateObjects,
  validateObjectsResult,
  loadFieldsCampaignTask,
  loadFieldsCampaignTaskSuccess,
  createCampaign,
  loadDetailsCampaign,
  loadDetailsCampaignResult,
  updateCampaign,
  loadInfoComponent,
  loadInfoComponentResult,
  loadTemplateComponent,
  loadTemplateComponentResult,
  updateCampaignConfig,
  deleteCampaign,
  unMountCampaignSetting,
  getInfoMailChimp,
  getInfoMailChimpResult,
  connectMailchimp,
  disconnectMailchimp,
  getListCampaignMailChimp,
  getListCampaignMailChimpResult,
  getContentCampaignMailchimp,
  getContentCampaignMailchimpResult,
  getTemplateCampaignTasks,
  getTemplateCampaignTasksResult,
  //   loadFieldsCampaign,
  //   loadFieldsCampaignSuccess,
  //   loadSetingsCampaign,
  //   loadRulesSuccess,
  //   loadTaskCampaign,
  //   loadTaskCampaignSuccess,
  //   generateTask,
  //   updateStatusGenerateTask,
  //   runCampaign,
  //   stopCampaign,
} = slice.actions;
