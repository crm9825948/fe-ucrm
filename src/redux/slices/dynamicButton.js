import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: {
    deleteDynamicButton: false,
  },
  listDynamicButton: [],
  showModalAdd: false,
  detailsDynamicButton: {},
  listActions: [],
  status: null,
  detailsActionDynamicButton: {},
  userDynamicButton: [],
  isRunSuccess: false,
  isReload: false,
};

const slice = createSlice({
  name: "dynamicButton",
  initialState,
  reducers: {
    loadDataNecessary() {},
    setStatus(state, action) {
      state.status = action.payload;
    },
    allDynamicButton(state, action) {
      state.listDynamicButton = action.payload;
    },
    setShowModalAdd(state, action) {
      state.showModalAdd = action.payload;
    },
    createDynamicButton() {},
    activeDynamicButton() {},
    changeState(state, action) {
      let tempList = [...state.listDynamicButton];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload._id) {
            state.listDynamicButton[idx] = {
              ...state.listDynamicButton[idx],
              status: !state.listDynamicButton[idx].status,
            };
          }
        });
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload._id) {
            state.listDynamicButton.splice(idx, 1);
          }
        });
      }
    },
    deleteDynamicButton(state) {
      state.loading.deleteDynamicButton = true;
    },
    deleteDynamicButtonResult(state) {
      state.loading.deleteDynamicButton = false;
    },
    loadDataNecessaryEdit() {},
    loadDetailsDynamicButtonSuccess(state, action) {
      state.detailsDynamicButton = action.payload;
    },
    loadListActions() {},
    loadListActionsSuccess(state, action) {
      state.listActions = action.payload;
    },
    updateDynamicButton() {},
    createActionDynamicButton() {},
    activeActionDynamicButton() {},
    deleteActionDynamicButton() {},
    loadDetailsActionDynamicButton() {},
    loadDetailsActionDynamicButtonSuccess(state, action) {
      state.detailsActionDynamicButton = action.payload;
    },
    loadDetailsActionDynamicButtonFail(state) {
      state.detailsActionDynamicButton = {};
    },
    updateActionDynamicButton() {},
    updateStateAction(state, action) {
      let tempList = [...state.listActions];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions[idx] = {
              ...state.listActions[idx],
              status: !state.listActions[idx].status,
            };
          }
        });
      } else if (action.payload.type === "update") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions[idx] = {
              ...state.listActions[idx],
              action_name: action.payload.name,
            };
          }
        });
        state.detailsAction = {};
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions.splice(idx, 1);
          }
        });
      }
    },
    loadUserDynamicButton() {},
    loadUserDynamicButtonSuccess(state, action) {
      state.userDynamicButton = action.payload;
    },
    runDynamicButton() {},
    runDynamicButtonResult(state, action) {
      state.isRunSuccess = action.payload.isRunSuccess;
      state.isReload = action.payload.isReload;
    },
    unMountEditDynamicButton(state) {
      state.detailsDynamicButton = {};
      state.listActions = [];
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  allDynamicButton,
  setShowModalAdd,
  createDynamicButton,
  activeDynamicButton,
  changeState,
  deleteDynamicButton,
  deleteDynamicButtonResult,
  loadDataNecessaryEdit,
  loadDetailsDynamicButtonSuccess,
  loadListActions,
  loadListActionsSuccess,
  updateDynamicButton,
  createActionDynamicButton,
  setStatus,
  activeActionDynamicButton,
  updateStateAction,
  deleteActionDynamicButton,
  loadDetailsActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  loadDetailsActionDynamicButtonFail,
  updateActionDynamicButton,
  loadUserDynamicButton,
  loadUserDynamicButtonSuccess,
  runDynamicButton,
  runDynamicButtonResult,
  unMountEditDynamicButton,
} = slice.actions;
