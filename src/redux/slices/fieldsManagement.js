import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  error: false,
  object_id: null,
  sections: [],
  order: [],
  listIDofSection: {},
  field: {},
  option: [],
  objects_related: [],
  fields_related: [],
  list_of_field_key: [],
  statusCode: null,
  fieldsStatus: null,
  isChange: false,
};

const slice = createSlice({
  name: "fieldsManagement",
  initialState,
  reducers: {
    //Load section and field
    loadFields(state, action) {
      state.isLoading = true;
    },
    loadFieldsSuccess(state, action) {
      state.isLoading = false;
      state.sections = action.payload.sections;
      state.order = action.payload.order;
      state.listIDofSection = action.payload.listIDofSection;
    },
    setOrder(state, action) {
      state.order = action.payload;
    },
    loadFieldsFail(state, action) {
      state.isLoading = false;
    },
    //Set object ID
    setObjectID(state, action) {
      state.object_id = action.payload;
    },
    //Create section
    createSection(state, action) {
      state.isLoading = true;
    },
    createSectionSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "create-section";
    },
    createSectionFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
      state.fieldsStatus = action.payload;
    },
    //Update postion section
    updatePosition(state, action) {
      state.isLoading = true;
    },
    updatePositionSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "update-section";
    },
    updatePositionFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
    },
    //Delete section
    deleteSection(state, action) {
      state.isLoading = true;
    },
    deleteSectionSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "delete-section";
    },
    deleteSectionFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
    },
    //Create field
    createField(state, action) {
      state.isLoading = true;
    },
    createFieldSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "create-field";
    },
    createFieldFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
      state.fieldsStatus = action.payload;
    },
    //Update position field
    updatePositionField(state, action) {
      state.isLoading = true;
    },
    updatePositionFieldSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "update-position-field";
    },
    updatePositionFieldFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
      state.fieldsStatus = action.payload;
    },
    //Update field
    updateField(state, action) {
      state.isLoading = true;
    },
    updateFieldSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "update-field";
    },
    updateFieldFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
      state.fieldsStatus = action.payload;
    },
    //Set field
    setField(state, action) {
      state.field = action.payload;
    },
    //Delete field
    deleteField(state, action) {
      state.isLoading = true;
    },
    deleteFieldSuccess(state, action) {
      state.isLoading = false;
      state.fieldsStatus = "success";
      state.statusCode = "delete-position-field";
    },
    deleteFieldFail(state, action) {
      state.isLoading = false;
      state.statusCode = "fail";
      state.fieldsStatus = action.payload;
    },
    //Set option
    setOption(state, action) {
      state.option = action.payload;
    },
    //Related object
    loadRelatedObject(state, action) {
      state.isLoading = true;
    },
    loadRelatedObjectSuccess(state, action) {
      state.isLoading = false;
      state.objects_related = action.payload;
    },
    loadRelatedObjectFail(state, action) {
      state.isLoading = false;
    },

    //Object field related
    loadObjectFields(state, action) {
      state.isLoading = false;
    },
    loadObjectFieldsSuccess(state, action) {
      state.isLoading = false;
      state.fields_related = action.payload;
    },
    loadObjectFieldsFail(state, action) {
      state.isLoading = false;
    },
    //set list og field key
    loadListOfFieldKey(state, action) {
      state.isLoading = false;
    },
    loadListOfFieldKeySuccess(state, action) {
      state.list_of_field_key = action.payload;
      state.isLoading = false;
    },
    loadListOfFieldKeyFail(state, action) {
      state.isLoading = false;
    },
    //set status code
    setStatusCode(state, action) {
      state.statusCode = null;
    },
    updateIsChange(state, action) {
      state.isChange = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadFields,
  loadFieldsFail,
  loadFieldsSuccess,
  setObjectID,
  createSection,
  createSectionFail,
  createSectionSuccess,
  updatePosition,
  updatePositionFail,
  updatePositionSuccess,
  deleteSection,
  deleteSectionSuccess,
  deleteSectionFail,
  createField,
  createFieldFail,
  createFieldSuccess,
  updatePositionField,
  updatePositionFieldFail,
  updatePositionFieldSuccess,
  updateField,
  updateFieldFail,
  updateFieldSuccess,
  setField,
  deleteField,
  deleteFieldFail,
  deleteFieldSuccess,
  setOption,
  loadRelatedObject,
  loadRelatedObjectSuccess,
  loadRelatedObjectFail,
  loadObjectFields,
  loadObjectFieldsFail,
  loadObjectFieldsSuccess,
  loadListOfFieldKey,
  loadListOfFieldKeySuccess,
  loadListOfFieldKeyFail,
  setStatusCode,
  setOrder,
  updateIsChange,
} = slice.actions;
