import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allAccount: [],
};

const slice = createSlice({
  name: "outlook",
  initialState,
  reducers: {
    loadOutlookAccount() {},
    loadOutlookAccountResult(state, action) {
      state.allAccount = action.payload;
    },
    addAccount() {},
    deleteAccount() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadOutlookAccount,
  loadOutlookAccountResult,
  addAccount,
  deleteAccount,
} = slice.actions;
