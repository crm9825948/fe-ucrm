const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  settingKanban: {},
  dataKanbanView: {
    current_page: 1,
    object_id: "",
    record_per_page: 10,
    search_with: { meta: [], data: [] },
  },
  firstRecordId: null,
  lastRecordId: null,
  isLoadingPagi: false,
  totalRecord: null,
  isLoadingData: false,
  page_records: 0,
  argument: {
    first: null,
    last: null,
  },
  currentPage: 1,
};

const slice = createSlice({
  name: "kanbanView",
  initialState,
  reducers: {
    getSettingKanban() {},
    getSettingKanbanSuccess(state, action) {
      state.settingKanban = action.payload;
    },
    getDataKanbanView(state) {
      state.isLoadingData = true;
    },
    getDataKanbanViewSuccess(state, action) {
      state.dataKanbanView = action.payload;
      state.firstRecordId = action.payload.first_record_id;
      state.lastRecordId = action.payload.last_record_id;
      state.isLoadingData = false;
      state.page_records = action.payload.page_records;
    },
    getDataKanbanViewById() {},
    updateColumn(state, action) {
      const { index, data } = action.payload;
      state.dataKanbanView.columns[index].data = data;
    },
    updateRecordKanbanOnDrag(state, action) {},
    updateRecordKanban(state, action) {},
    updateCurrentPage(state, action) {
      state.dataKanbanView.current_page = action.payload;
    },
    deleteRecord(state, action) {},
    getPaginationKanban(state, acion) {
      state.isLoadingPagi = true;
    },
    getPaginationKanbanSuccess(state, action) {
      state.totalRecord = action.payload.total_record;
      state.isLoadingPagi = false;
    },
    setTotalRecord(state, action) {
      state.totalRecord = action.payload;
    },
    updateArgument(state, action) {
      state.argument = action.payload;
    },
    setCurrentPage(state, action) {
      state.currentPage = action.payload;
    },
    unmountKanbanView: () => initialState,
  },
});

export default slice.reducer;

export const {
  getSettingKanban,
  getSettingKanbanSuccess,
  getDataKanbanView,
  getDataKanbanViewSuccess,
  updateColumn,
  updateCurrentPage,
  updateRecordKanbanOnDrag,
  updateRecordKanban,
  getDataKanbanViewById,
  deleteRecord,
  unmountKanbanView,
  getPaginationKanban,
  getPaginationKanbanSuccess,
  setTotalRecord,
  updateArgument,
  setCurrentPage,
} = slice.actions;
