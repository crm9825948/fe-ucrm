const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listEmailTemplate: [],
  editEmailTemplate: {
    body: "",
    description: "",
    email_template_name: "",
    object: "",
    selected_fields: [],
    selected_general_fields: [],
    subject: [],
    keep_original_subject: false,
  },
  showDrawer: false,
};
const slice = createSlice({
  name: "emailTemplate",
  initialState,
  reducers: {
    getListEmailTemplate(state, action) {},
    getListEmailTemplateSuccess(state, action) {
      state.listEmailTemplate = action.payload;
    },
    getEmailTempateById(state, action) {},
    getEmailTempateByIdSuccess(state, action) {
      state.editEmailTemplate = action.payload;
    },
    updateFieldEmailTemplate(state, action) {
      state.editEmailTemplate[action.payload.key] = action.payload.value;
    },
    deleteEmailTemplate() {},
    setShowDrawer(state, action) {
      state.showDrawer = action.payload;
    },
    saveEmailTemplate() {},
    updateEmailTemplate() {},
    unmountEmailTemplate: () => initialState,
  },
});

export default slice.reducer;

export const {
  getListEmailTemplate,
  getListEmailTemplateSuccess,
  getEmailTempateById,
  getEmailTempateByIdSuccess,
  setShowDrawer,
  deleteEmailTemplate,
  updateFieldEmailTemplate,
  saveEmailTemplate,
  updateEmailTemplate,
  unmountEmailTemplate,
} = slice.actions;
