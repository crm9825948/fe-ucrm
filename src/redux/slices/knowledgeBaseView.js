const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listArticle: [],
  treeView: [],
  pagination: {
    page: 0,
    limit: 16,
  },
  filterArticle: {
    section_name: "",
    category_name: "",
  },
  searchArticle: "",
  totalPage: 0,
  totalRecord: 0,
};

const slice = createSlice({
  name: "knowledgeBaseView",
  initialState,
  reducers: {
    getListArticle() {},
    getListArticleSuccess(state, action) {
      state.listArticle = action.payload;
      state.pagination = {
        page: 0,
        limit: 16,
      };
    },
    getTreeViewKnowledgeBase() {},
    getTreeViewKnowledgeBaseSuccess(state, action) {
      state.treeView = action.payload;
    },
    deleteArticleById() {},
    updateFilterArticle(state, action) {
      state.filterArticle[action.payload.key] = action.payload.value;
    },
    updateSearchArticle(state, action) {
      state.searchArticle = action.payload;
    },
    loadMoreArticle() {},
    updatePagination(state, action) {
      state.pagination = action.payload;
    },
    loadMoreArticleSuccess(state, action) {
      state.listArticle = state.listArticle.concat(action.payload.data);
      state.pagination = action.payload.pagination;
    },
    updateTotalPageAndRecord(state, action) {
      state.totalPage = action.payload.totalPage;
      state.totalRecord = action.payload.totalRecord;
    },
    unmountKnowledgeBaseView: (state) => ({
      ...initialState,
      searchArticle: state.searchArticle,
    }),
  },
});

export default slice.reducer;

export const {
  getListArticle,
  getListArticleSuccess,
  getTreeViewKnowledgeBase,
  getTreeViewKnowledgeBaseSuccess,
  deleteArticleById,
  updateFilterArticle,
  updateSearchArticle,
  loadMoreArticle,
  updatePagination,
  loadMoreArticleSuccess,
  updateTotalPageAndRecord,
  unmountKnowledgeBaseView,
} = slice.actions;
