import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allExportHistory: [],
  loading: false,
  defaultActive: "0",
};

const slice = createSlice({
  name: "exportHistory",
  initialState,
  reducers: {
    loadAllExportHistory(state, action) {},
    loadAllExportHistorySuccess(state, action) {
      state.allExportHistory = action.payload;
    },
    loadExportHistory(state, action) {
      state.loading = true;
    },
    loadExportHistorySuccess(state, action) {
      state.loading = false;
      let tempList = [...state.allExportHistory];
      const indexNewData = tempList.findIndex(
        (item) => item._id === action.payload._id
      );
      tempList[indexNewData] = action.payload;
      state.allExportHistory = tempList;
    },
    loadExportHistoryFail(state, action) {
      state.loading = false;
    },
    deleteExportHistory() {},
    deleteExportHistorySuccess(state, action) {
      let tempList = [...state.allExportHistory];
      tempList.forEach((item, idx) => {
        if (item._id === action.payload.export_history_id) {
          state.allExportHistory.splice(idx, 1);
        }
      });
    },
    setActive(state, action) {
      state.defaultActive = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  loadAllExportHistory,
  loadAllExportHistorySuccess,
  loadExportHistory,
  loadExportHistorySuccess,
  loadExportHistoryFail,
  deleteExportHistory,
  deleteExportHistorySuccess,
  setActive,
} = slice.actions;
