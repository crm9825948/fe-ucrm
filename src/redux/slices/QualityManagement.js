import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  active: false,
  listFormQMSettings: [],
  detailSettings: [],
  listOptionsQuickFilter: [],
  listAllOptionsQuickFilter: [],
  listDataFilter: [],
  payloadFilter: {},
  loadingMore: true,
  loadingListOptions: false,
  loadedAllData: false,
  scrollToTop: false,
  listUser: [],
  listLogScoring: {},
  loadingLogScoring: false,
  listDetailState: [],
  Emails: {},
  chat: {},
  interactionDetail: {},
  loadingInteractionDetail: false,
  typeSelected: "",
  urlRecord: "",
  totalRecord: null,
  loadingTotalRecord: false,
  activeQMView: {},
  listObjectFieldQM: [],
  listChatIC: [],
  loadedAllChatIC: false,
  fieldInteractionID: "",
  fieldsourceInteractionID: "",
  fieldsourceObjectID: "",
};

const slice = createSlice({
  name: "qualityManagement",
  initialState,
  reducers: {
    changeActiveSettings() {},
    getListDataFilter(state) {
      state.scrollToTop = false;
    },
    getListDataFilterSuccess(state, action) {
      if (action.payload.length < 1) {
        state.loadingMore = false;
      } else {
        state.loadingMore = true;
      }
      state.loadedAllData = false;
      state.listDataFilter = action.payload;
      state.scrollToTop = true;
    },
    getMoreListDataFilter() {},
    getMoreListDataFilterSuccess(state, action) {
      if (action.payload.length < 50) {
        state.loadedAllData = true;
      }
      state.listDataFilter = [...state.listDataFilter, ...action.payload];
    },
    UpdateListOptionsQuickFilter() {},
    getListOptionsQuickFilter(state) {
      state.loadingListOptions = true;
    },
    getListOptionsQuickFilterSuccess(state, action) {
      state.listOptionsQuickFilter = action.payload;
      state.loadingListOptions = false;
    },
    getListAllOptionsQuickFilter() {},
    getListAllOptionsQuickFilterSuccess(state, action) {
      state.listAllOptionsQuickFilter = action.payload;
    },
    getAllFormQM() {},
    getAllFormQMSuccess(state, action) {
      state.listFormQMSettings = action.payload;
    },
    getDetailSettings() {},
    getDetailSettingsSuccess(state, action) {
      state.detailSettings = action.payload;
    },
    saveDetailSettings() {},
    setPayloadFilter(state, action) {
      state.payloadFilter = action.payload;
    },
    setLoadingMore(state, action) {
      state.loadingMore = action.payload;
    },
    getListUser() {},
    getListUserSuccess(state, action) {
      state.listUser = action.payload;
    },
    getListScoring(state) {
      state.loadingLogScoring = true;
    },
    getListScoringSuccess(state, action) {
      state.listLogScoring = action.payload;
      state.loadingLogScoring = false;
    },
    getListDetailState() {},
    getListDetailStateSuccess(state, action) {
      state.listDetailState = action.payload;
    },
    getEmails() {},
    getEmailsSuccess(state, action) {
      state.Emails = action.payload;
    },
    getChat() {},
    getChatSuccess(state, action) {
      state.chat = action.payload;
    },
    getInteractionDetail(state) {
      state.loadingInteractionDetail = true;
    },
    getInteractionDetailSuccess(state, action) {
      state.interactionDetail = action.payload;
      state.loadingInteractionDetail = false;
    },
    getURLRecord() {},
    getURLRecordSuccess(state, action) {
      state.urlRecord = action.payload;
    },
    setTypeSelected(state, action) {
      state.typeSelected = action.payload;
    },
    getTotalRecord(state) {
      state.loadingTotalRecord = true;
    },
    getTotalRecordSuccess(state, action) {
      state.totalRecord = action.payload;
      state.loadingTotalRecord = false;
    },
    resetTotalRecord(state) {
      state.totalRecord = null;
    },
    getActiveQMView() {},
    getActiveQMViewSuccess(state, action) {
      state.activeQMView = action.payload;
    },
    loadListObjectFieldQM() {},
    loadListObjectFieldQMSuccess(state, action) {
      state.listObjectFieldQM = action.payload;
    },
    resetListDataFilter(state) {
      state.listDataFilter = [];
    },
    getListChatIC() {},
    getListChatICSuccess(state, action) {
      if (action.payload.length < 20) {
        state.loadedAllChatIC = true;
      }
      state.listChatIC = [...state.listChatIC, ...action.payload];
    },
    setListChatIC(state, action) {
      state.listChatIC = action.payload;
    },
    setLoadedAllChatIC(state, action) {
      state.loadedAllChatIC = action.payload;
    },
    setFieldInteractionID(state, action) {
      state.fieldInteractionID = action.payload;
    },
    setFieldsourceInteractionID(state, action) {
      state.fieldsourceInteractionID = action.payload;
    },
    setFieldsourceObjectID(state, action) {
      state.fieldsourceObjectID = action.payload;
    },
  },
});

export default slice.reducer;

//Action
export const {
  getListChatIC,
  getListChatICSuccess,
  setListChatIC,
  setLoadedAllChatIC,
  UpdateListOptionsQuickFilter,
  UpdateListOptionsQuickFilterSuccess,
  getListScoring,
  getListScoringSuccess,
  getListUser,
  getListUserSuccess,
  setLoadingMore,
  changeActiveSettings,
  getMoreListDataFilter,
  getMoreListDataFilterSuccess,
  getListDataFilter,
  getListDataFilterSuccess,
  getListOptionsQuickFilter,
  getListOptionsQuickFilterSuccess,
  getListAllOptionsQuickFilter,
  getListAllOptionsQuickFilterSuccess,
  getAllFormQM,
  getAllFormQMSuccess,
  getDetailSettings,
  getDetailSettingsSuccess,
  saveDetailSettings,
  setPayloadFilter,
  getListDetailState,
  getListDetailStateSuccess,
  getEmails,
  getEmailsSuccess,
  getInteractionDetail,
  getInteractionDetailSuccess,
  setTypeSelected,
  getURLRecord,
  getURLRecordSuccess,
  getTotalRecord,
  getTotalRecordSuccess,
  resetTotalRecord,
  getActiveQMView,
  getActiveQMViewSuccess,
  loadListObjectFieldQM,
  loadListObjectFieldQMSuccess,
  resetListDataFilter,
  getChat,
  getChatSuccess,
  setFieldInteractionID,
  setFieldsourceInteractionID,
  setFieldsourceObjectID,
} = slice.actions;
