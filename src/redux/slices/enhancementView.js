import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listFieldObject: [],
  listCategory: [],
  isKnowledgeView: false,
  listArticleDashboard: [],
  listArticleTopview: [],
  listArticleNewest: [],
  listAriticleSearch: [],
  objectSearch: {},
  articleDetail: {},
  dataSearch: {
    page: 0,
    size: 10,
    text_fields: ["title", "body", "tag", "article_id"],
    text_search: "",
    category: "all",
    created_date: "all",
    sort: "related",
  },
  isLoadMore: false,
  objectFilter: {},
};
const slice = createSlice({
  name: "enhancementView",
  initialState,
  reducers: {
    getListFieldObject() {},
    getListFieldObjectSuccess(state, action) {
      state.listFieldObject = action.payload;
    },
    getListCategory() {},
    getListCategorySuccess(state, action) {
      state.listCategory = action.payload;
    },
    updateView(state, action) {
      state.isKnowledgeView = action.payload;
    },
    getListArticleDashboard() {},
    getListArticleDashboardSuccess(state, action) {
      state.listArticleDashboard = action.payload;
    },
    getTopview() {},
    getTopviewSuccess(state, action) {
      state.listArticleTopview = action.payload;
    },
    getNewest() {},
    getNewestSuccess(state, action) {
      state.listArticleNewest = action.payload;
    },
    getListArticleSearch() {},
    getListArticleSearchSuccess(state, action) {
      state.listAriticleSearch = action.payload.records;
      state.objectSearch = action.payload;
    },
    updateUrlSearch(state, action) {
      state.urlSearch[action.payload.key] = action.payload.value;
    },
    getArticleDetail() {},
    getArticleDetailSuccess(state, action) {
      state.articleDetail = action.payload;
    },
    updateDataSearch(state, action) {
      Object.entries(action.payload).forEach(([key, value]) => {
        state.dataSearch[key] = value;
      });
    },
    updateLoadMore(state, action) {
      state.isLoadMore = action.payload;
    },
    getListArticleFilter() {},
    getListArticleFilterSuccess(state, action) {
      state.objectFilter = action.payload;
    },
    createService() {},
    updateArticle() {},
    createCategory() {},
    updateCategory() {},
    deleteCategory() {},
    deleteArticle() {},
    syncData() {},
    testConnection() {},
    reIndex() {},
  },
});

export default slice.reducer;

export const {
  getListFieldObject,
  getListFieldObjectSuccess,
  getListCategory,
  getListCategorySuccess,
  updateView,
  getListArticleDashboard,
  getListArticleDashboardSuccess,
  getTopview,
  getTopviewSuccess,
  getNewest,
  getNewestSuccess,
  getListArticleSearch,
  getListArticleSearchSuccess,
  updateUrlSearch,
  getArticleDetail,
  getArticleDetailSuccess,
  updateDataSearch,
  updateLoadMore,
  getListArticleFilter,
  getListArticleFilterSuccess,
  createService,
  createCategory,
  updateArticle,
  updateCategory,
  deleteCategory,
  deleteArticle,
  syncData,
  testConnection,
  reIndex,
} = slice.actions;
