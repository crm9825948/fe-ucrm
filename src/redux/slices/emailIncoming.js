const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listEmailIncoming: [],
  editEmailIncoming: {},
  showDrawer: false,
  listMappingFieldsUpdate: [],
  listEmailFolders: [],
  valuePicklist: [],
  targetPicklist: [],
};
const slice = createSlice({
  name: "emailIncoming",
  initialState,
  reducers: {
    getListEmailIncoming(state, action) {},
    getListEmailIncomingSuccess(state, action) {
      state.listEmailIncoming = action.payload;
    },
    changeStatus(state, action) {},
    changeStatusSuccess(state, action) {
      state.listEmailIncoming = action.payload;
    },
    updateEditEmail(state, action) {},
    updateEditEmailSuccess(state, action) {
      state.editEmailIncoming = action.payload;
      state.showDrawer = true;
    },
    updateEditEmailFolder() {},
    updateEditEmailFolderSuccess(state, action) {
      state.editEmailIncoming = action.payload;
    },
    changeField(state, action) {
      state.editEmailIncoming[action.payload.key] = action.payload.value;
    },
    submitMailBox(state, action) {},
    updateMailBoxIncoming(state, action) {},
    deleteMailIncoming(state, action) {},
    checkConnection(state, action) {},
    setShowDrawer(state, action) {
      state.showDrawer = action.payload;
    },
    updateMappingFields(state, action) {
      state.listMappingFieldsUpdate = action.payload;
    },
    addMappingFields(state, action) {
      state.listMappingFieldsUpdate.push(action.payload);
    },
    editMappingFields(state, action) {
      state.listMappingFieldsUpdate[action.payload.index][action.payload.key] =
        action.payload.value;
    },
    setDefaultEmailIncoming() {},
    getEmailFolders() {},
    getEmailFoldersResult(state, action) {
      state.listEmailFolders = action.payload;
    },
    scanEmail() {},
    loadValuePicklist() {},
    loadValuePicklistSuccess(state, action) {
      state.valuePicklist = action.payload;
    },
    loadValuePicklistFail(state) {
      state.valuePicklist = [];
    },
    loadTargetPicklist() {},
    loadTargetPicklistSuccess(state, action) {
      state.targetPicklist = action.payload;
    },
    loadTargetPicklistFail(state) {
      state.targetPicklist = [];
    },
    unmountEmailIncoming: () => initialState,
  },
});

export default slice.reducer;

export const {
  getListEmailIncoming,
  getListEmailIncomingSuccess,
  changeStatus,
  changeStatusSuccess,
  updateEditEmail,
  updateEditEmailSuccess,
  updateEditEmailFolder,
  updateEditEmailFolderSuccess,
  changeField,
  submitMailBox,
  deleteMailIncoming,
  checkConnection,
  setShowDrawer,
  updateMailBoxIncoming,
  updateMappingFields,
  addMappingFields,
  editMappingFields,
  setDefaultEmailIncoming,
  getEmailFolders,
  getEmailFoldersResult,
  scanEmail,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
  unmountEmailIncoming,
} = slice.actions;
