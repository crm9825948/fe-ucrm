import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  listProfile: [],
  totalRecords: 0,
  statusDelete: null,
  profileInfo: null,
  allProfile: null,
  detailProfile: null,
  profileFields: null,
  isLoadingUpdate: false,
  fieldsInfo: null,
};

const slice = createSlice({
  name: "profile",
  initialState,
  reducers: {
    loadProfiles(state, action) {},
    loadProfilesSuccess(state, action) {
      state.listProfile = action.payload.data.profiles;
      state.totalRecords = action.payload.data.total_records;
    },
    loadAllProfiles(state, action) {
      state.isLoading = true;
    },
    loadAllProfilesSuccess(state, action) {
      state.isLoading = false;
      state.allProfile = action.payload.data.profiles;
    },
    loadAllProfilesFail(state, action) {
      state.isLoading = false;
    },
    deleteProfile(state, action) {
      state.isLoading = true;
    },
    deleteProfileSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteProfileFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
    resetStatusDelete(state, action) {
      state.statusDelete = null;
    },
    createProfile(state, action) {
      state.isLoading = true;
    },
    createProfileSuccess(state, action) {
      state.isLoading = false;
      state.profileInfo = "success";
    },
    resetProfileInfo(state, action) {
      state.profileInfo = null;
    },
    createProfileFail(state, action) {
      state.isLoading = false;
      state.profileInfo = action.payload;
    },
    loadDetailProfile(state, action) {},
    loadDetailProfileSuccess(state, action) {
      state.detailProfile = action.payload;
    },
    updateProfile(state, action) {
      state.isLoading = true;
    },
    updateProfileSuccess(state, action) {
      state.isLoading = false;
      state.profileInfo = "success";
    },
    updateProfileFail(state, action) {
      state.isLoading = false;
      state.profileInfo = action.payload;
    },
    loadProfileFields(state, action) {},
    loadProfileFieldsSuccess(state, action) {
      state.profileFields = action.payload;
    },
    resetProfileFields(state, action) {
      state.profileFields = null;
    },
    updateProfileFields(state, action) {
      state.isLoadingUpdate = true;
    },
    updateProfileFieldsSuccess(state, action) {
      state.isLoadingUpdate = false;
      state.fieldsInfo = "success";
    },
    updateProfileFieldsFail(state, action) {
      state.isLoadingUpdate = false;
      state.fieldsInfo = action.payload;
    },
    resetFieldsInfo(state, action) {
      state.fieldsInfo = null;
    },
    unMountEditProfile(state) {
      state.profileInfo = null;
      state.detailProfile = null;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadProfiles,
  loadProfilesSuccess,
  deleteProfile,
  deleteProfileSuccess,
  deleteProfileFail,
  resetStatusDelete,
  createProfile,
  createProfileSuccess,
  resetProfileInfo,
  createProfileFail,
  loadAllProfiles,
  loadAllProfilesSuccess,
  loadAllProfilesFail,
  loadDetailProfile,
  loadDetailProfileSuccess,
  updateProfile,
  updateProfileSuccess,
  updateProfileFail,
  loadProfileFields,
  loadProfileFieldsSuccess,
  resetProfileFields,
  updateProfileFields,
  updateProfileFieldsSuccess,
  updateProfileFieldsFail,
  resetFieldsInfo,
  unMountEditProfile,
} = slice.actions;
