const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listSMSOutgoing: [],
  showDrawer: false,
  showModalTest: false,
  editSMSOutgoing: {},
};

const slice = createSlice({
  name: "smsOutgoing",
  initialState,
  reducers: {
    getListSMSOutgoing() {},
    getListSMSOutgoingSuccess(state, action) {
      state.listSMSOutgoing = action.payload;
    },
    getSMSOutgoingById(state, action) {},
    setEditSMSOutgoing(state, action) {
      state.editSMSOutgoing = action.payload;
    },
    updateFieldSMSOutgoing(state, action) {
      state.editSMSOutgoing[action.payload.key] = action.payload.value;
    },
    saveSMSOutgoing(state, action) {},
    updateSMSOutgoing(state, action) {},
    deleteSMSOutgoing(state, action) {},
    setShowDrawer(state, action) {
      state.showDrawer = action.payload;
    },
    setShowModalTest(state, action) {
      state.showModalTest = action.payload;
    },
    testSMSOutgoing() {},
    unmountSMSOutgoing: () => initialState,
  },
});

export default slice.reducer;

export const {
  getListSMSOutgoing,
  getListSMSOutgoingSuccess,
  getSMSOutgoingById,
  setEditSMSOutgoing,
  saveSMSOutgoing,
  updateSMSOutgoing,
  deleteSMSOutgoing,
  updateFieldSMSOutgoing,
  setShowDrawer,
  setShowModalTest,
  testSMSOutgoing,
  unmountSMSOutgoing,
} = slice.actions;
