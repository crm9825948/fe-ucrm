import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoadingCreate: false,
  isLoading: false,
  tenants: {},
  total_page: 0,
  total_record: 0,
  isLoadingDisable: false,
  logInterval: {},
  logCelery: {},
  showModalScan: false,
  showModalCelery: false,
  duplicate: false,
  isLoadingClone: false,
  dataExport: [],
  rootPermission: [],
  isLoadingLoadPermission: false,
  isLoadingUpdateRoot: false,
  isCreatePermission: false,
  isDeletePermission: false,
  isLoadingLog: false,
  auditLog: [],
  packageList: [],
  packageDetail: {},
  tenantRule: [],
  allObject: [],
};

const slice = createSlice({
  name: "tenants",
  initialState,
  reducers: {
    //create tenants
    createTenants(state, action) {
      state.isLoadingCreate = true;
    },
    createTenantsSuccess(state, action) {
      state.isLoadingCreate = false;
    },

    createTenantsFail(state, action) {
      state.isLoadingCreate = false;
    },
    //load data
    loadData(state, action) {
      state.isLoading = true;
    },
    loadDataSuccess(state, action) {
      state.isLoading = false;
      state.tenants = action.payload.data;
      state.total_page = action.payload.total_page;
      state.total_record = action.payload.total_record;
    },
    loadDataFail(state, action) {
      state.isLoading = false;
    },
    disableTenant(state, action) {
      state.isLoadingDisable = true;
    },
    disableTenantSuccess(state, action) {
      state.isLoadingDisable = false;
    },
    disableTenantFail(state, action) {
      state.isLoadingDisable = false;
    },
    getLogInterval() {},
    getLogIntervalSuccess(state, action) {
      state.logInterval = action.payload;
    },
    setShowModalScan(state, action) {
      state.showModalScan = action.payload;
    },
    getLogCelery() {},
    getLogCelerySuccess(state, action) {
      state.logCelery = action.payload;
    },
    setShowModalCelery(state, action) {
      state.showModalCelery = action.payload;
    },
    cloneTenant(state, action) {
      state.isLoadingClone = true;
      state.isLoadingCreate = true;
    },
    cloneTenantSuccess(state, action) {
      state.isLoadingClone = false;
      state.isLoadingCreate = false;
    },
    cloneTenantFail(state, action) {
      state.isLoadingClone = false;
      state.isLoadingCreate = false;
    },
    loadDataToExport(state, action) {},
    loadDataToExportSuccess(state, action) {
      state.dataExport = action.payload;
    },
    loadRootPermission(state, action) {
      state.isLoadingLoadPermission = true;
    },
    loadRootPermissionSuccess(state, action) {
      state.isLoadingLoadPermission = false;
      state.rootPermission = action.payload;
    },
    loadRootPermissionFail(state, action) {
      state.isLoadingLoadPermission = false;
    },
    updateRoot(state, action) {
      state.isLoadingUpdateRoot = true;
    },
    updateRootSuccess(state, action) {
      state.isLoadingUpdateRoot = false;
    },
    updateRootFail(state, action) {
      state.isLoadingUpdateRoot = false;
    },
    createPermission(state, action) {
      state.isCreatePermission = true;
    },
    createPermissionSuccess(state, action) {
      state.isCreatePermission = false;
    },
    createPermissionFail(state, action) {
      state.isCreatePermission = false;
    },
    deletePermission(state, action) {
      state.isDeletePermission = true;
    },
    deletePermissionSuccess(state, action) {
      state.isDeletePermission = false;
    },
    deletePermissionFail(state, action) {
      state.isDeletePermission = false;
    },
    loadLog(state, action) {
      state.isLoadingLog = true;
    },
    loadLogSuccess(state, action) {
      state.isLoadingLog = false;
      state.auditLog = action.payload;
    },
    loadLogFail(state, action) {
      state.isLoadingLog = false;
    },
    changePass(state, action) {},
    healthCheckInteraction(state, action) {},
    createPackage(state, action) {},
    updatePackage(state, action) {},
    loadPackage(state, action) {},
    loadPackageSuccess(state, action) {
      state.packageList = action.payload;
    },
    loadPackageDetail(state, action) {},
    loadPackageDetailSuccess(state, action) {
      state.packageDetail = action.payload;
    },
    deletePackage(state, action) {},
    getTenantRule() {},
    getTenantRuleResult(state, action) {
      state.tenantRule = action.payload;
    },
    setTenantRule() {},
    setTenantRuleAll() {},
    runIndex() {},
    allowCloneTenant() {},
    loadAllObject() {},
    loadAllObjectSuccess(state, action) {
      state.allObject = action.payload;
    },
    updateObjectMedia() {},
    updateStatusTenant() {},
    deleteTenant() {},
  },
});

// Reducer
export default slice.reducer;

//Actions
export const {
  deletePackage,
  updatePackage,
  loadPackageDetail,
  loadPackageDetailSuccess,
  loadPackageSuccess,
  loadPackage,
  createTenants,
  createTenantsFail,
  createTenantsSuccess,
  loadData,
  loadDataFail,
  loadDataSuccess,
  disableTenant,
  disableTenantSuccess,
  disableTenantFail,
  getLogInterval,
  getLogIntervalSuccess,
  setShowModalScan,
  getLogCelery,
  getLogCelerySuccess,
  setShowModalCelery,
  cloneTenant,
  cloneTenantSuccess,
  cloneTenantFail,
  loadDataToExport,
  loadDataToExportSuccess,
  loadRootPermission,
  loadRootPermissionSuccess,
  loadRootPermissionFail,
  updateRoot,
  updateRootSuccess,
  updateRootFail,
  createPermission,
  createPermissionFail,
  createPermissionSuccess,
  deletePermission,
  deletePermissionFail,
  deletePermissionSuccess,
  loadLog,
  loadLogFail,
  loadLogSuccess,
  changePass,
  healthCheckInteraction,
  createPackage,
  getTenantRule,
  getTenantRuleResult,
  setTenantRule,
  setTenantRuleAll,
  runIndex,
  allowCloneTenant,
  loadAllObject,
  loadAllObjectSuccess,
  updateObjectMedia,
  updateStatusTenant,
  deleteTenant,
} = slice.actions;
