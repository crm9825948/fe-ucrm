import { createSlice } from "@reduxjs/toolkit";
import _ from "lodash";

const initialState = {
  templateDefault: null,
};

const slice = createSlice({
  name: "callView",
  initialState,
  reducers: {
    getTemplateDefaultCallView() {},
    getTemplateDefaultCallViewSuccess(state, action) {
      if (_.get(action.payload, "_id")) {
        state.templateDefault = action.payload;
      } else {
        state.templateDefault = null;
      }
    },
    updateTemplateCallView() {},
  },
});

export default slice.reducer;

//Action
export const {
  getTemplateDefaultCallView,
  getTemplateDefaultCallViewSuccess,
  updateTemplateCallView,
  updateTemplateCallViewSuccess,
} = slice.actions;
