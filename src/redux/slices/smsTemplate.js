const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    listSMSTemplate: [],
    editSMSTemplate: {}, 
    showDrawer: false
}

const slice = createSlice({
    name: "smsTemplate",
    initialState,
    reducers: {
        getListSMSTemplate(state, action) {
        },
        getListSMSTemplateSuccess(state, action) {
            state.listSMSTemplate = action.payload
        },
        setShowDrawer(state, action) {
            state.showDrawer = action.payload
        },
        updateFieldSMSTemplate(state, action) {
            state.editSMSTemplate[action.payload.key] = action.payload.value
        },
        getSMSTemplateById() {
        },
        getSMSTemplateByIdSuccess(state, action) {
            state.editSMSTemplate = action.payload
        },
        createSMSTemplate() {
        },
        updateSMSTemplate() {
        },
        deleteSMSTemplate() {
        },
        unmountSMSTemplate: () => initialState
    }
})

export default slice.reducer

export const {
    getListSMSTemplate,
    getListSMSTemplateSuccess,
    setShowDrawer,
    updateFieldSMSTemplate,
    createSMSTemplate,
    getSMSTemplateById,
    getSMSTemplateByIdSuccess,
    updateSMSTemplate,
    deleteSMSTemplate,
    unmountSMSTemplate
} = slice.actions