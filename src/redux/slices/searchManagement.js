import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  allowGlobalSearch: false,
  listField: [],
  listObject: [],
  loadMoreObject: false,
  loadMoreField: false,
};

const slice = createSlice({
  name: "searchManagement",
  initialState,
  reducers: {
    loadDataSetting() {},
    loadDataSettingSuccess(state, action) {
      state.listField = action.payload.searchable_fields;
      state.allowGlobalSearch = action.payload.allow_global_search;
    },
    updateListField(state, action) {
      state.listField = action.payload;
    },
    loadListObject() {},
    loadListObjectSuccess(state, action) {
      state.listObject = action.payload;
    },
    updateLoadMoreObject(state, action) {
      state.loadMoreObject = action.payload;
    },
    updateLoadMoreField(state, action) {
      state.loadMoreObject = action.payload;
    },
    toggleGlobalSearch() {},
    updateSearchManagement() {},
    unMountListField(state, action) {
      state.listField = [];
    },
  },
});
export default slice.reducer;

export const {
  loadDataSetting,
  loadDataSettingSuccess,
  loadListObject,
  loadListObjectSuccess,
  updateLoadMoreObject,
  updateLoadMoreField,
  unMountListField,
  toggleGlobalSearch,
  updateListField,
  updateSearchManagement,
} = slice.actions;
