import { createSlice } from "@reduxjs/toolkit";
import { createContext } from "react";

const initialState = {
  isLoading: false,
  error: false,
  userInfo: {},
  isAuthenticated: false,
  isInitialized: false,
  loginStatus: null,
  title: "Unify CRM",
  dataVersionBE: "",
  checkPermissionQMView: false,
  media: "",
};

export const AuthContext = createContext({
  ...initialState,
  method: "jwt",
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
});

const slice = createSlice({
  name: "authenticated",
  initialState,
  reducers: {
    //login
    login(state, action) {
      state.isLoading = true;
    },
    loginSuccessfully(state, action) {
      state.isLoading = false;
      state.userInfo = action.payload;
      state.isAuthenticated = true;
      state.isInitialized = true;
      state.loginStatus = "success";
    },
    loginError(state, action) {
      state.isLoading = false;
      state.userInfo = {};
      state.loginStatus = action.payload;
    },
    resetLoginStatus(state, action) {
      state.loginStatus = null;
    },
    logout(state, action) {
      state.isLoading = false;
      state.userInfo = {};
      state.isAuthenticated = false;
      state.isInitialized = true;
    },
    initialize(state, action) {
      state.isAuthenticated = action.payload.isAuthenticated;
      state.isInitialized = true;
      state.userInfo = action.payload.userInfo;
    },
    forgotPassword(state, action) {
      state.isLoading = true;
    },
    forgotPasswordSuccess(state, action) {
      state.isLoading = false;
    },
    forgotPasswordFail(state, action) {
      state.isLoading = false;
    },
    resetPassword(state, action) {
      state.isLoading = true;
    },
    resetPasswordResult(state, action) {
      state.isLoading = false;
    },
    changeTitlePage(state, action) {
      state.title = action.payload;
    },
    getDataVersionBE() {},
    getDataVersionBEResult(state, action) {
      state.dataVersionBE = action.payload;
    },
    setCheckPermissionQMView(state, action) {
      state.checkPermissionQMView = action.payload;
    },
    getMedia() {},
    getMediaResult(state, action) {
      state.media = action.payload;
    },
  },
});

// Reducer
export default slice.reducer;

//Actions
export const {
  login,
  loginSuccessfully,
  loginError,
  logout,
  initialize,
  resetLoginStatus,
  forgotPassword,
  forgotPasswordSuccess,
  forgotPasswordFail,
  resetPassword,
  resetPasswordResult,
  changeTitlePage,
  getDataVersionBE,
  getDataVersionBEResult,
  setCheckPermissionQMView,
  getMedia,
  getMediaResult,
} = slice.actions;
