import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  ruleList: [],
  isLoadingUpdate: false,
};

const slice = createSlice({
  name: "duplicateRules",
  initialState,
  reducers: {
    loadRules(state, action) {
      state.isLoading = true;
    },
    loadRulesSuccess(state, action) {
      state.isLoading = false;
      state.ruleList = action.payload;
    },
    loadRulesFail(state, action) {
      state.isLoading = false;
    },
    updateRules(state, action) {
      state.isLoadingUpdate = true;
    },
    updateRulesSuccess(state, action) {
      state.isLoadingUpdate = false;
    },
    updateRulesFail(state, action) {
      state.isLoadingUpdate = false;
    },
    updatePosition(state, action) {},
    actionDuplicate(state, action) {},
    deleteDuplicate(state, action) {},
    createRules(state, action) {},
    createDuplicate3rd() {},
    updateDuplicate3rd() {},
    notify() {},
  },
});

export default slice.reducer;

export const {
  loadRules,
  loadRulesFail,
  loadRulesSuccess,
  updateRules,
  updateRulesSuccess,
  updateRulesFail,
  updatePosition,
  actionDuplicate,
  deleteDuplicate,
  createRules,
  createDuplicate3rd,
  updateDuplicate3rd,
  notify,
} = slice.actions;
