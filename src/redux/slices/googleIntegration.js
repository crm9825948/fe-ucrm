const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    listGoogleIntegrationSetting: [],
    editGoogleIntegrationSetting: {},
    listAccountMapping: []
}

const slice = createSlice({
    name: 'googleIntegration',
    initialState,
    reducers: {
        getListGoogleIntegrationSetting() {
        },
        getListGoogleIntegrationSettingSuccess(state, action) {
            state.listGoogleIntegrationSetting = action.payload
        },
        updateGoogleIntegration(state, action) {
            state.editGoogleIntegrationSetting = action.payload
        },
        updateFieldGoogleIntegration(state, action) {
            state.editGoogleIntegrationSetting[action.payload.key] = action.payload.value
        },
        addAccountMapping() {
        },
        getListAccountMapping() {
        },
        getListAccountMappingSuccess(state, action) {
            state.listAccountMapping = action.payload
        },
        createGoogleIntegrationSetting() {
        },
        updateGoogleIntegrationSetting() {
        },
        deleteGoogleIntegrationSetting() {
        },
        deleteAccountMapping() {

        }
    }
})

export default slice.reducer

export const {
    getListGoogleIntegrationSetting,
    getListGoogleIntegrationSettingSuccess,
    updateGoogleIntegration,
    updateFieldGoogleIntegration,
    getListAccountMapping,
    getListAccountMappingSuccess,
    addAccountMapping,
    createGoogleIntegrationSetting,
    updateGoogleIntegrationSetting,
    deleteGoogleIntegrationSetting,
    deleteAccountMapping
} = slice.actions