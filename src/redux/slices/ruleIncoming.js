const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listRule: [],
  editing: false,
  editRule: "",
  editRuleIncoming: {},
  listNormalField: [],
  listRequiredField: [],
  listReplyFields: [],
  isDelete: false,
  isCreate: false,

  valuePicklist: [],
  valuePicklistReply: [],
  targetPicklist: [],
  targetPicklistReply: [],
};

const slice = createSlice({
  name: "ruleIncoming",
  initialState,
  reducers: {
    getListRule(state, action) {},
    getListRuleSuccess(state, action) {
      state.listRule = action.payload;
    },
    getRuleById() {},
    updateEditRuleIncoming(state, action) {
      state.editRuleIncoming = action.payload;
    },
    updateFieldRuleIncoming(state, action) {
      state.editRuleIncoming[action.payload.key] = action.payload.value;
      if (action.payload.index) {
        state.listRule[action.payload.index][action.payload.key] =
          action.payload.value;
      }
    },
    updateRequiredField(state, action) {
      state.listRequiredField = action.payload;
    },
    editRequiredField(state, action) {
      state.listRequiredField[action.payload.index][action.payload.key] =
        action.payload.value;
    },
    updateNormalField(state, action) {
      state.listNormalField = action.payload;
    },
    addNormalField(state, action) {
      state.listNormalField.push(action.payload);
    },
    editNormalField(state, action) {
      state.listNormalField[action.payload.index][action.payload.key] =
        action.payload.value;
    },
    removeNormalField(state, action) {
      state.listNormalField.splice(action.payload, 1);
    },
    updateReplyFields(state, action) {
      state.listReplyFields = action.payload;
    },
    addReplyFields(state, action) {
      state.listReplyFields.push(action.payload);
    },
    editReplyFields(state, action) {
      state.listReplyFields[action.payload.index][action.payload.key] =
        action.payload.value;
    },
    saveRule(state, action) {},
    updateRule(state, action) {},
    updatePositionRule() {},
    deleteRuleById() {},
    deleteRuleByIdSuccess(state, action) {
      state.isDelete = action.payload;
    },
    setEditing(state, action) {
      state.editing = action.payload;
    },
    setEditRule(state, action) {
      state.editRule = action.payload;
    },
    updateStatusRule(state, aciton) {},
    setIsCreate(state, action) {
      state.isCreate = action.payload;
    },
    loadValuePicklist() {},
    loadValuePicklistSuccess(state, action) {
      state.valuePicklist = action.payload;
    },
    loadValuePicklistFail(state) {
      state.valuePicklist = [];
    },

    loadValuePicklistReply() {},
    loadValuePicklistReplySuccess(state, action) {
      state.valuePicklistReply = action.payload;
    },
    loadValuePicklistReplyFail(state) {
      state.valuePicklistReply = [];
    },

    loadTargetPicklist() {},
    loadTargetPicklistSuccess(state, action) {
      state.targetPicklist = action.payload;
    },
    loadTargetPicklistFail(state) {
      state.targetPicklist = [];
    },
    loadTargetPicklistReply() {},
    loadTargetPicklistReplySuccess(state, action) {
      state.targetPicklistReply = action.payload;
    },
    loadTargetPicklistReplyFail(state) {
      state.targetPicklistReply = [];
    },

    unmountRuleIncoming: () => initialState,
  },
});

export default slice.reducer;

export const {
  getListRule,
  getListRuleSuccess,
  updateEditRuleIncoming,
  getRuleById,
  updateFieldRuleIncoming,
  updateNormalField,
  addNormalField,
  editNormalField,
  removeNormalField,
  updateReplyFields,
  addReplyFields,
  editReplyFields,
  updateRequiredField,
  editRequiredField,
  saveRule,
  updatePositionRule,
  deleteRuleById,
  deleteRuleByIdSuccess,
  updateRule,
  setEditRule,
  setEditing,
  updateStatusRule,
  setIsCreate,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
  loadValuePicklistReply,
  loadValuePicklistReplySuccess,
  loadValuePicklistReplyFail,
  loadTargetPicklistReply,
  loadTargetPicklistReplySuccess,
  loadTargetPicklistReplyFail,
  unmountRuleIncoming,
} = slice.actions;
