import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  reportDetail: {},
  listActionKey: [],
  result: {},
  isRun: false,
  isChange: false,
};
const slice = createSlice({
  name: "customReport",
  initialState,
  reducers: {
    updateListActionKey(state, action) {
      state.listActionKey = action.payload;
    },
    deleteActionKey(state, action) {
      state.isChange = true;
      state.listActionKey = state.listActionKey.filter(
        (item) => item.key_id !== action.payload.key_id
      );
    },
    createCustomReport() {},
    updateCustomReport() {},
    deleteCustomReport() {},
    loadDetailCustomReport() {},
    loadDetailCustomReportSuccess(state, action) {
      state.reportDetail = action.payload;
    },
    runReport() {},
    runReportSuccess(state, action) {
      //   state.result = action.payload;
    },
    loadDataReport() {},
    loadDataReportSuccess(state, action) {
      state.result = action.payload;
    },
    exportCustomReport() {},
    updateIsRun(state, action) {
      state.isRun = action.payload;
    },
    updateIsChange(state, action) {
      state.isChange = action.payload;
    },
    cancelReport() {},
  },
});
export default slice.reducer;

export const {
  updateListActionKey,
  deleteActionKey,
  createCustomReport,
  updateCustomReport,
  deleteCustomReport,
  loadDetailCustomReport,
  loadDetailCustomReportSuccess,
  runReport,
  runReportSuccess,
  loadDataReport,
  loadDataReportSuccess,
  exportCustomReport,
  updateIsRun,
  updateIsChange,
  cancelReport,
} = slice.actions;
