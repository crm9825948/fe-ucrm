import { createSlice } from "@reduxjs/toolkit";

const initialState = {};

const slice = createSlice({
  name: "voiceBiometric",
  initialState,
  reducers: {
    callEvent() {},
    updateCustomer() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const { callEvent, updateCustomer } = slice.actions;
