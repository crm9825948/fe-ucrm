const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  listEmailOutgoing: [],
  editEmailOutgoing: {},
  showDrawer: false,
  listSender: [],
};
const slice = createSlice({
  name: "emailOutgoing",
  initialState,
  reducers: {
    getListEmailOutgoing(state, action) {},
    getListEmailOutgoingSuccess(state, action) {
      state.listEmailOutgoing = action.payload;
    },
    getSender() {},
    getSenderResult(state, action) {
      state.listSender = action.payload;
    },
    updateField(state, action) {
      state.editEmailOutgoing[action.payload.key] = action.payload.value;
    },
    updateEditEmail() {},
    updateMailBoxOutgoing() {},
    updateEditEmailSuccess(state, action) {
      state.editEmailOutgoing = action.payload;
      state.showDrawer = true;
    },
    addNewEmailOutgoing() {},
    deleteEmailOutgoing() {},
    setShowDrawer(state, action) {
      state.showDrawer = action.payload;
    },
    setDefaultEmailOutgoing() {},
    unmountEmailOutgoing: () => initialState,
  },
});

export default slice.reducer;

export const {
  getListEmailOutgoing,
  getListEmailOutgoingSuccess,
  getSender,
  getSenderResult,
  updateField,
  updateEditEmail,
  updateEditEmailSuccess,
  deleteEmailOutgoing,
  addNewEmailOutgoing,
  updateMailBoxOutgoing,
  setShowDrawer,
  setDefaultEmailOutgoing,
  unmountEmailOutgoing,
} = slice.actions;
