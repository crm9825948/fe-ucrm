const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    editKnowledgeBaseSetting: {
        object_id: '',
        section_name: '',
        category_name: '',
        title: '',
        body: '',
        start_date:'',
        end_date:''
    },
    listCategory: [],
    isFirst: false
}

const slice = createSlice({
    name: 'knowledgeBaseSetting',
    initialState,
    reducers: {
        getKnowledgeBaseSetting() {
        },
        getKnowledgeBaseSettingSuccess(state, action) {
            state.editKnowledgeBaseSetting = action.payload
        },
        updateFieldKnowledgeBaseSetting(state, action) {
            state.editKnowledgeBaseSetting[action.payload.key] = action.payload.value
            if(action.payload.key === 'object_id') {
                state.editKnowledgeBaseSetting = {
                    ...state.editKnowledgeBaseSetting,
                    section_name: '',
                    category_name: '',
                    title: '',
                    body: '',
                    start_date:'',
                    end_date:''
                }
            }
        },
        updateKnowledgeBaseSetting() {
        },
        deleteKnowledgeBaseSetting() {
        },
        updateIsFirst(state, action) {
            state.isFirst = action.payload
        },
        unmountKnowledgeBaseSetting: () => initialState
    }
})

export default slice.reducer

export const {
    getKnowledgeBaseSetting,
    getKnowledgeBaseSettingSuccess,
    updateFieldKnowledgeBaseSetting,
    updateKnowledgeBaseSetting,
    deleteKnowledgeBaseSetting,
    updateIsFirst,
    unmountKnowledgeBaseSetting
} = slice.actions