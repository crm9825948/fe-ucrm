const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    editArticle: {
        title: '',
        body: '',
        section_name:'',
        category_name: '',
        start_date: '',
        end_date: ''
    },
    optionCategory: []
}

const slice = createSlice({
    name: 'article',
    initialState,
    reducers: {
        getArticleById() {
        },
        getArticleByIdSuccess(state, action) {
            state.editArticle = action.payload
        },
        updateFieldArticle(state, action) {
            state.editArticle[action.payload.key] = action.payload.value
        },
        getOptionCategory() {
        },
        getOptionCategorySuccess(state, action) {
            state.optionCategory = action.payload
        },
        deleteArticle(){},
        unmountArticle: () => initialState
    }
})

export default slice.reducer

export const {
    getArticleById,
    getArticleByIdSuccess,
    updateFieldArticle,
    getOptionCategory,
    getOptionCategorySuccess,
    deleteArticle,
    unmountArticle
} = slice.actions