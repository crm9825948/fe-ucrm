import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listAssignmentRule: [],
  assignmentRuleDetail: {},
};

const slice = createSlice({
  name: "assignmentRule",
  initialState,
  reducers: {
    getListAssignmentRule() {},
    getListAssignmentRuleSuccess(state, action) {
      state.listAssignmentRule = action.payload;
    },
    createAssignmentRule() {},
    deleteAssignmentRule() {},
    getAssignmentRuleDetail() {},
    runManualAssign() {},
    resetUserCapacity() {},
    updateAssignmentRule() {},
    updatePriorityRule() {},
    getAssignmentRuleDetailSuccess(state, action) {
      state.assignmentRuleDetail = action.payload;
    },
    getAssignmentRuleDetailFail(state, action) {
      state.assignmentRuleDetail = {};
    },
  },
});

export default slice.reducer;

export const {
  getListAssignmentRule,
  getListAssignmentRuleSuccess,
  createAssignmentRule,
  deleteAssignmentRule,
  getAssignmentRuleDetail,
  getAssignmentRuleDetailSuccess,
  getAssignmentRuleDetailFail,
  updateAssignmentRule,
  runManualAssign,
  resetUserCapacity,
  updatePriorityRule,
} = slice.actions;
