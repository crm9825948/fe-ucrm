import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  userStatus: [],
  userStatusLog: [],
  hasMoreLog: true,
  currentPageLog: 0,
  statusCRM: "online",
};

const slice = createSlice({
  name: "agentMonitor",
  initialState,
  reducers: {
    getUserStatus() {},
    getUserStatusResult(state, action) {
      state.userStatus = action.payload;
    },
    getUserStatusLog() {},
    getUserStatusLogResult(state, action) {
      if (action.payload.type === "remove") {
        state.userStatusLog = [];
      } else {
        state.userStatusLog = state.userStatusLog.concat(action.payload.data);

        if (action.payload.data.length < 20) {
          state.hasMoreLog = false;
        }
      }
    },
    setHasMore(state, action) {
      state.hasMoreLog = action.payload;
    },
    setCurrentPageLog(state, action) {
      state.currentPageLog = action.payload;
    },
    changeStatusCRM() {},
    changeStatusCRMResult(state, action) {
      state.statusCRM = action.payload.crm_status;
    },
    unMountLogout: () => initialState,
    exportAgentMonitor() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  getUserStatus,
  getUserStatusResult,
  getUserStatusLog,
  getUserStatusLogResult,
  setHasMore,
  setCurrentPageLog,
  changeStatusCRM,
  changeStatusCRMResult,
  unMountLogout,
  exportAgentMonitor,
} = slice.actions;
