const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
    listKanbanView: [],
    editKanbanView: {},
    showModal: false,
    step: 0
}
const slice = createSlice({
    name: 'kanbanViewSetting',
    initialState,
    reducers: {
        getListKanbanView(state, action) {
        },
        getListKanbanViewSuccess(state, action) {
            state.listKanbanView = action.payload
        },
        getKanbanViewSettingById(state, action) {
        },
        getKanViewSettingByIdSuccess(state, action) {
            state.editKanbanView = action.payload
        },
        updateFieldKanbanView(state, action) {
            state.editKanbanView[action.payload.key] = action.payload.value
        },
        updateColorColumnConfig(state, action) {
            state.editKanbanView.column_configs[action.payload.index].style = action.payload.value 
        },
        addDisplayField(state, action){
            state.editKanbanView.column_configs[action.payload.index].display_fields[action.payload.value] = {
                color: "",
                font_weight: "400"
            }
            state.editKanbanView.column_configs[action.payload.index].fields_order.push(action.payload.value)
        },
        deleteDisplayField(state, action) {
            delete state.editKanbanView.column_configs[action.payload.index].display_fields[action.payload.value]
            const index = state.editKanbanView.column_configs[action.payload.index].fields_order.indexOf(action.payload.value);
            if (index > -1) {
                state.editKanbanView.column_configs[action.payload.index].fields_order.splice(index, 1);
            }
        },
        updateDisplayFields(state, action) {
            state.editKanbanView.column_configs[action.payload.index].display_fields[action.payload.key][action.payload.i] = action.payload.value
        },
        addDisplayFieldUncategorized(state, action) {
            state.editKanbanView.uncategorized_column_config.display_fields[action.payload] = {
                color: "",
                font_weight: "400"
            }
            state.editKanbanView.uncategorized_column_config.fields_order.push(action.payload)
        },
        deleteDisplayFieldUncategorized(state, action) {
            delete state.editKanbanView.uncategorized_column_config.display_fields[action.payload]
            const index = state.editKanbanView.uncategorized_column_config.fields_order.indexOf(action.payload);
            if (index > -1) {
                state.editKanbanView.uncategorized_column_config.fields_order.splice(index, 1);
            }
        },
        updateDisplayFieldUncategorized(state, action) {
            state.editKanbanView.uncategorized_column_config.display_fields[action.payload.key][action.payload.i] = action.payload.value
        },
        updateColorStatus(state, action) {
            state.editKanbanView.status_style[action.payload.index].style.color = action.payload.value
        },
        updateColorUncategorized(state, action) {
            state.editKanbanView.uncategorized_column_config.style.color = action.payload
        },
        updatePositionField(state, action) {
            state.editKanbanView.column_configs[action.payload.index].fields_order = action.payload.value
        },
        updatePositionFieldUncategorized(state, action) {
            state.editKanbanView.uncategorized_column_config.fields_order = action.payload
        },
        saveKanbanViewSetting(state, action) {
        },
        updateKanbanViewSetting() {
        },
        deleteKanbanViewSetting() {
        },
        setShowModal(state, action) {
            state.showModal = action.payload
        },
        setStep(state, action){
            state.step = action.payload
        },
        changePositionColumnsConfig(state, action) {
            state.editKanbanView.column_configs = action.payload
        },
        unmountKanbanViewSetting: () => initialState
    }
})

export default slice.reducer

export const {
    getListKanbanView,
    getListKanbanViewSuccess,
    updateFieldKanbanView,
    updateColorColumnConfig,
    addDisplayField,
    updateDisplayFields,
    updateColorStatus,
    addDisplayFieldUncategorized,
    getKanbanViewSettingById,
    getKanViewSettingByIdSuccess,
    updateColorUncategorized,
    saveKanbanViewSetting,
    updatePositionFieldUncategorized,
    updatePositionField,
    deleteKanbanViewSetting,
    updateKanbanViewSetting,
    setShowModal,
    setStep,
    deleteDisplayField,
    deleteDisplayFieldUncategorized,
    updateDisplayFieldUncategorized,
    unmountKanbanViewSetting,
    changePositionColumnsConfig
} = slice.actions