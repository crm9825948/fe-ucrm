import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: {
    modalRule: false,
    modalDelete: false,
    loadRule: false,
  },
  status: {
    createRule: null,
    updateRule: null,
    deleteRule: null,
  },
  allRules: [],
};

const slice = createSlice({
  name: "sharing",
  initialState,
  reducers: {
    loadDataNecessary() {},
    createRule(state, action) {
      state.loading.modalRule = true;
    },
    createRuleSuccess(state, action) {
      state.loading.modalRule = false;
      state.status.createRule = "success";
    },
    createRuleFail(state, action) {
      state.loading.modalRule = false;
      state.status.createRule = action.payload;
    },
    resetStatus(state, action) {
      state.status.createRule = null;
      state.status.updateRule = null;
      state.status.deleteRule = null;
    },
    loadRuleObject(state, action) {
      state.loading.loadRule = true;
    },
    loadRuleObjectSuccess(state, action) {
      state.loading.loadRule = false;
      state.allRules = action.payload;
    },
    loadRuleObjectFail(state, action) {
      state.loading.loadRule = false;
    },
    deleteRule(state, action) {
      state.loading.modalDelete = true;
    },
    deleteRuleSuccess(state, action) {
      state.loading.modalDelete = false;
      state.status.deleteRule = "success";
    },
    deleteRuleFail(state, action) {
      state.loading.modalDelete = false;
      state.status.deleteRule = action.payload;
    },
    updateRule(state, action) {
      state.loading.modalRule = true;
    },
    updateRuleSuccess(state, action) {
      state.loading.modalRule = false;
      state.status.updateRule = "success";
    },
    updateRuleFail(state, action) {
      state.loading.modalRule = false;
      state.status.updateRule = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  createRule,
  createRuleSuccess,
  createRuleFail,
  resetStatus,
  loadRuleObject,
  loadRuleObjectSuccess,
  loadRuleObjectFail,
  deleteRule,
  deleteRuleSuccess,
  deleteRuleFail,
  updateRule,
  updateRuleSuccess,
  updateRuleFail,
} = slice.actions;
