import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: false,
  loadingScreen: true,
  listUser: [],
  totalRecords: 0,
  listRole: [],
  isLoadingUser: false,
  listUserReport: [],
  userInfo: null,
  listAllUser: [],
  statusDelete: null,
  statusChangePass: null,
  userDetail: {},
  listUserName: [],
  showModalEdit: false,
  isSearching: false,
  logsUser: {
    results: [],
    total: 0,
  },
  domains: [],
  userRule: [],
  userRuleGlobal: [],
  isLoadingUserRuleGlobal: false,
  reportPermission: {},
};

const slice = createSlice({
  name: "user",
  initialState,
  reducers: {
    loadUser(state, action) {},
    loadUserSuccess(state, action) {
      state.listUser = action.payload.records;
      state.totalRecords = action.payload.total_record;
    },
    getUser() {},
    getUserResult(state, action) {
      state.listUser = action.payload.records;
      state.totalRecords = action.payload.total_record;
    },
    searchUser(state, action) {
      state.isSearching = true;
    },
    searchUserSuccess(state, action) {
      state.isSearching = false;
    },
    searchUserFail(state, action) {
      state.isSearching = false;
    },
    loadAllRole(state, action) {},
    loadAllRoleSuccess(state, action) {
      state.listRole = action.payload;
    },
    loadAllRoleFail(state, action) {},
    loadUserReport(state, action) {
      state.isLoadingUser = true;
    },
    loadUserReportSuccess(state, action) {
      state.isLoadingUser = false;
      state.listUserReport = action.payload;
    },
    loadUserReportFail(state, action) {
      state.isLoadingUser = false;
    },
    createUser(state, action) {
      state.isLoading = true;
    },
    createUserSuccess(state, action) {
      state.isLoading = false;
      state.userInfo = "success";
    },
    createUserFail(state, action) {
      state.isLoading = false;
      state.userInfo = action.payload;
    },
    resetUserInfo(state, action) {
      state.userInfo = null;
    },
    updateUser(state, action) {
      state.isLoading = true;
    },
    updateUserSuccess(state, action) {
      state.isLoading = false;
      state.userInfo = "success";
    },
    updateUserFail(state, action) {
      state.isLoading = false;
      state.userInfo = action.payload;
    },
    loadAllUser(state, action) {},
    loadAllUserSuccess(state, action) {
      state.listAllUser = action.payload.records;
    },
    getAllUser(state, action) {},
    getAllUserResult(state, action) {
      state.listAllUser = action.payload.records;
    },
    deleteUser(state, action) {
      state.isLoading = true;
    },
    deleteUserSuccess(state, action) {
      state.isLoading = false;
      state.statusDelete = "success";
    },
    deleteUserFail(state, action) {
      state.isLoading = false;
      state.statusDelete = action.payload;
    },
    resetStatusDelete(state, action) {
      state.statusDelete = null;
    },
    changePass(state, action) {
      state.isLoading = true;
    },
    changePassSuccess(state, action) {
      state.isLoading = false;
      state.statusChangePass = "success";
    },
    changePassFail(state, action) {
      state.isLoading = false;
      state.statusChangePass = action.payload;
    },
    resetStatusChangePass(state, action) {
      state.statusChangePass = null;
    },
    loadUserDetail(state, action) {
      state.isLoading = true;
    },
    loadUserDetailSuccess(state, action) {
      state.isLoading = false;
      state.userDetail = action.payload;
    },
    loadUserDetailFail(state, action) {
      state.isLoading = false;
    },
    getUserName(state, action) {},
    getUserNameSuccess(state, action) {
      state.listUserName = action.payload;
    },
    setShowModalEdit(state, action) {
      state.showModalEdit = action.payload;
    },
    updatePositionAvatar(state, action) {
      state.userDetail.avatar_config[action.payload.key] = action.payload.value;
    },
    updateUserSetting(state, action) {},
    activeUser() {},
    activeUserSuccess(state, action) {
      let tempList = [...state.listUser];
      tempList.forEach((item, idx) => {
        if (item._id === action.payload) {
          state.listUser[idx] = {
            ...state.listUser[idx],
            Active: !state.listUser[idx].Active,
          };
        }
      });
    },
    unmountProfileSetting(state, action) {
      state.userDetail = {};
      state.showModalEdit = false;
    },
    importUsers() {},
    generateSampleData() {},
    exportUsers() {},
    getLogsUser() {},
    getLogsUserResult(state, action) {
      state.logsUser = {
        results: action.payload.results,
        total: action.payload.total_records,
      };
    },
    getDomain() {},
    getDomainResult(state, action) {
      state.domains = action.payload;
    },
    getUserRule() {},
    getUserRuleResult(state, action) {
      state.userRule = action.payload;
    },
    getUserRuleGlobal(state) {
      state.isLoadingUserRuleGlobal = true;
    },
    getUserRuleGlobalResult(state, action) {
      state.userRuleGlobal = action.payload;
      state.isLoadingUserRuleGlobal = false;
    },
    setUserRule() {},
    setUserRuleAll() {},
    getReportPermission() {},
    getReportPermissionResult(state, action) {
      state.reportPermission = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadUser,
  loadUserSuccess,
  getUser,
  getUserResult,
  loadAllRole,
  loadAllRoleSuccess,
  loadAllRoleFail,
  loadUserReport,
  loadUserReportSuccess,
  loadUserReportFail,
  createUser,
  createUserSuccess,
  createUserFail,
  resetUserInfo,
  updateUser,
  updateUserSuccess,
  updateUserFail,
  loadAllUser,
  loadAllUserSuccess,
  getAllUser,
  getAllUserResult,
  deleteUser,
  deleteUserSuccess,
  deleteUserFail,
  resetStatusDelete,
  changePass,
  changePassSuccess,
  changePassFail,
  resetStatusChangePass,
  loadUserDetail,
  loadUserDetailSuccess,
  loadUserDetailFail,
  getUserName,
  getUserNameSuccess,
  setShowModalEdit,
  updatePositionAvatar,
  updateUserSetting,
  unmountProfileSetting,
  searchUser,
  searchUserSuccess,
  searchUserFail,
  activeUser,
  activeUserSuccess,
  importUsers,
  generateSampleData,
  exportUsers,
  getLogsUser,
  getLogsUserResult,
  getDomain,
  getDomainResult,
  getUserRule,
  getUserRuleResult,
  getUserRuleGlobal,
  getUserRuleGlobalResult,
  setUserRule,
  setUserRuleAll,
  getReportPermission,
  getReportPermissionResult,
} = slice.actions;
