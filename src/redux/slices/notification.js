import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  dataNoti: [],
};

const slice = createSlice({
  name: "notification",
  initialState,
  reducers: {
    getListNoti() {},
    getListNotiSuccess(state, action) {
      state.dataNoti = action.payload;
    },
    seenOneNoti() {},
    seenAllNoti() {},
  },
});

//Reducer
export default slice.reducer;

//Actions
export const { getListNoti, getListNotiSuccess, seenOneNoti, seenAllNoti } =
  slice.actions;
