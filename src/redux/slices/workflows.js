import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  loading: {
    allWorkflows: false,
    modalDelete: false,
    modalCreate: false,
    details: false,
    update: false,
    allActions: false,
    action: false,
    modalSendNoti: false,
    modal: false,
  },
  status: null,
  IDNewWorkflow: null,
  listWorkflows: [],
  details: {},
  listActions: [],
  detailsAction: {},
  userFields: [],
  linkingFieldEmail: [],

  //action Create record
  listFields: [],
  listFieldsMain: [],
  requiredFieldsCreate: [],
  optionalFieldsCreate: [],
  requiredFieldsUpdate: [],
  optionalFieldsUpdate: [],

  optionalFieldsUpdateRelated: [],
  listFieldsRelated: [],
  listObjectsRelated: [],

  valuePicklist: [],
  targetPicklist: [],

  //action update related record source target
  targetObjects: [],
  optionalFieldsUpdateSourceTarget: [],
  listPopup: [],
};

const slice = createSlice({
  name: "workflows",
  initialState,
  reducers: {
    loadDataNecessary() {},
    loadDataNecessaryEdit() {},
    setLoading(state, action) {
      state.loading[action.payload.type] = action.payload.status;
    },
    setStatus(state, action) {
      state.status = action.payload;
    },
    resetStatus(state, action) {
      state.status = null;
    },
    loadAllWorkflows(state, action) {},
    loadAllWorkflowsSuccess(state, action) {
      state.listWorkflows = action.payload;
    },
    activeWorkflow(state, action) {},
    activeWorkflowSuccess(state, action) {
      state.status = "change status success";
    },
    activeWorkflowFail(state, action) {
      // state.status.activeWorkflow = action.payload;
      state.status = action.payload;
    },
    changeState(state, action) {
      let tempList = [...state.listWorkflows];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.workflowChange) {
            state.listWorkflows[idx] = {
              ...state.listWorkflows[idx],
              status: !state.listWorkflows[idx].status,
            };
          }
        });
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.workflowChange) {
            state.listWorkflows.splice(idx, 1);
          }
        });
      }
    },
    deleteWorkflow(state, action) {
      state.loading.modalDelete = true;
    },
    deleteWorkflowSuccess(state, action) {
      state.status = "delete workflow success";
      state.loading.modalDelete = false;
    },
    deleteWorkflowFail(state, action) {
      state.status = action.payload;
      state.loading.modalDelete = false;
    },
    createWorkflow(state, action) {
      state.loading.modalCreate = true;
    },
    createWorkflowSuccess(state, action) {
      state.status = "create workflow success";
      state.loading.modalCreate = false;
      state.IDNewWorkflow = action.payload._id;
    },
    createWorkflowFail(state, action) {
      state.status = action.payload;
      state.loading.modalCreate = false;
      state.IDNewWorkflow = null;
    },
    detailsWorkflow(state, action) {},
    detailsWorkflowSuccess(state, action) {
      state.details = action.payload;
    },
    detailsWorkflowFail(state, action) {
      state.details = {};
    },
    updateWorkflow(state, action) {},
    loadAllActions(state, action) {
      state.loading.allActions = true;
    },
    loadAllActionsSuccess(state, action) {
      state.listActions = action.payload;
      state.loading.allActions = false;
    },
    loadAllActionsFail(state, action) {
      state.loading.allActions = false;
    },
    loadUserFields(state, action) {},
    loadUserFieldsSuccess(state, action) {
      state.userFields = action.payload;
    },
    createAction(state, action) {},
    loadDetailsAction(state, action) {
      state.loading.action = true;
    },
    loadDetailsActionSuccess(state, action) {
      state.loading.action = false;
      state.detailsAction = action.payload;
    },
    loadDetailsActionFail(state, action) {
      state.loading.action = false;
      state.detailsAction = {};
    },
    updateAction(state, action) {},
    activeAction(state, action) {
      state.loading.userFields = true;
    },
    updateStateAction(state, action) {
      state.loading.userFields = false;

      let tempList = [...state.listActions];
      if (action.payload.type === "change-status") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions[idx] = {
              ...state.listActions[idx],
              status: !state.listActions[idx].status,
            };
          }
        });
      } else if (action.payload.type === "update") {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions[idx] = {
              ...state.listActions[idx],
              action_name: action.payload.name,
            };
          }
        });
        state.detailsAction = {};
      } else {
        tempList.forEach((item, idx) => {
          if (item._id === action.payload.id) {
            state.listActions.splice(idx, 1);
          }
        });
      }
    },
    loadFieldsCondition() {},
    deleteActionWorkflow(state, action) {
      state.loading.modalDelete = true;
    },
    deleteActionWorkflowSuccess(state, action) {
      state.loading.modalDelete = false;
    },
    deleteActionWorkflowFail(state, action) {
      state.loading.modalDelete = false;
    },
    loadLinkingFieldEmailSuccess(state, action) {
      state.linkingFieldEmail = action.payload;
    },
    loadFieldsObject() {},
    loadFieldsObjectSuccess(state, action) {
      state.listFields = action.payload;
    },
    loadFieldsObjectFail(state, action) {
      state.listFields = [];
    },
    loadFieldsObjectRelated() {},
    loadFieldsObjectRelatedSuccess(state, action) {
      state.listFieldsRelated = action.payload;
    },
    loadFieldsObjectRelatedFail(state, action) {
      state.listFieldsRelated = [];
    },
    loadObjectRelated() {},
    loadObjectRelatedResult(state, action) {
      state.listObjectsRelated = action.payload;
    },
    loadFieldsMainObjectSuccess(state, action) {
      state.listFieldsMain = action.payload;
    },
    updateRequiredFieldsCreate(state, action) {
      state.requiredFieldsCreate = action.payload;
    },
    updateOptionalFieldsCreate(state, action) {
      state.optionalFieldsCreate = action.payload;
    },
    updateRequiredFieldsUpdate(state, action) {
      state.requiredFieldsUpdate = action.payload;
    },
    updateOptionalFieldsUpdate(state, action) {
      state.optionalFieldsUpdate = action.payload;
    },
    updateOptionalFieldsUpdateRelated(state, action) {
      state.optionalFieldsUpdateRelated = action.payload;
    },
    updateOptionalFieldsUpdateSourceTarget(state, action) {
      state.optionalFieldsUpdateSourceTarget = action.payload;
    },
    loadValuePicklist() {},
    loadValuePicklistSuccess(state, action) {
      state.valuePicklist = action.payload;
    },
    loadValuePicklistFail(state) {
      state.valuePicklist = [];
    },
    loadTargetPicklist() {},
    loadTargetPicklistSuccess(state, action) {
      state.targetPicklist = action.payload;
    },
    loadTargetPicklistFail(state) {
      state.targetPicklist = [];
    },

    loadTargetObjects() {},
    loadTargetObjectsResult(state, action) {
      state.targetObjects = action.payload;
    },
    getListPopup() {},
    getListPopupResult(state, action) {
      state.listPopup = action.payload;
    },
    unMountWorkflows(state) {
      state.listWorkflows = [];
      state.listPopup = [];
    },
    unMountDetailsWorkflows(state) {
      state.details = {};
      state.listActions = [];
      state.listPopup = [];
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  loadDataNecessary,
  loadDataNecessaryEdit,
  setLoading,
  loadAllWorkflows,
  loadAllWorkflowsSuccess,
  resetStatus,
  activeWorkflow,
  activeWorkflowSuccess,
  activeWorkflowFail,
  changeState,
  deleteWorkflow,
  deleteWorkflowSuccess,
  deleteWorkflowFail,
  createWorkflow,
  createWorkflowSuccess,
  createWorkflowFail,
  detailsWorkflow,
  detailsWorkflowSuccess,
  detailsWorkflowFail,
  updateWorkflow,
  loadAllActions,
  loadAllActionsSuccess,
  loadAllActionsFail,
  loadUserFields,
  loadUserFieldsSuccess,
  createAction,
  setStatus,
  loadDetailsAction,
  loadDetailsActionSuccess,
  loadDetailsActionFail,
  updateAction,
  activeAction,
  updateStateAction,
  loadFieldsCondition,
  loadCondition,
  deleteActionWorkflow,
  deleteActionWorkflowSuccess,
  deleteActionWorkflowFail,
  loadLinkingFieldEmailSuccess,
  loadFieldsObject,
  loadFieldsObjectSuccess,
  loadFieldsObjectFail,
  loadFieldsObjectRelated,
  loadFieldsObjectRelatedSuccess,
  loadFieldsObjectRelatedFail,
  loadObjectRelated,
  loadObjectRelatedResult,
  updateRequiredFieldsCreate,
  updateOptionalFieldsCreate,
  updateRequiredFieldsUpdate,
  updateOptionalFieldsUpdate,
  updateOptionalFieldsUpdateRelated,
  updateOptionalFieldsUpdateSourceTarget,
  loadFieldsMainObjectSuccess,
  loadValuePicklist,
  loadValuePicklistSuccess,
  loadValuePicklistFail,
  loadTargetPicklist,
  loadTargetPicklistSuccess,
  loadTargetPicklistFail,
  loadTargetObjects,
  loadTargetObjectsResult,
  getListPopup,
  getListPopupResult,
  unMountWorkflows,
  unMountDetailsWorkflows,
} = slice.actions;
