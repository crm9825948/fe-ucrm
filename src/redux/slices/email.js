const { createSlice } = require("@reduxjs/toolkit");

const initialState = {
  isLoading: false,
  listEmail: [],
  emailSelected: {},
  emailEdit: {},
  emailTemplate: [],
  isSending: false,
  recordData: {},
  showModal: false,
  showEditor: false,
  statusDrawer: "close",
  typeEmail: "",
  showEdit: true,
};

const slice = createSlice({
  name: "componentEmail",
  initialState,
  reducers: {
    updateShowEdit(state, action) {
      state.showEdit = action.payload;
    },
    loadEmails(state) {
      state.isLoading = true;
    },
    loadEmailsSuccessfully(state, action) {
      state.listEmail = action.payload;
      state.isLoading = false;
    },
    selectEmail(state, action) {
      state.emailSelected = action.payload;
    },
    updateEmail(state, action) {
      state.emailEdit = action.payload;
    },
    updateFieldEmail(state, action) {
      state.emailEdit[action.payload.key] = action.payload.value;
    },
    loadEmailTemplate(state, action) {},
    loadEmailTemplateSuccessfull(state, action) {
      state.emailTemplate = action.payload;
    },
    sendNewEmail() {},
    replyEmail() {},
    forwardEmail() {},
    setIsSending(state, action) {
      state.isSending = action.payload;
    },
    uploadFile(state, action) {
      state.emailEdit.attach_files.push(action.payload);
    },
    removeFile(state, action) {
      state.emailEdit.attach_files = state.emailEdit.attach_files.filter(
        (x) => x !== action.payload
      );
    },
    loadRecordData() {},
    loadRecordDataSuccess(state, action) {
      state.recordData = action.payload;
    },
    getEmailTemplateById() {},
    markReadEmail() {},
    setShowModal(state, action) {
      state.showModal = action.payload;
    },
    setShowEditor(state, action) {
      state.showEditor = action.payload;
    },
    setStatusDrawer(state, action) {
      state.statusDrawer = action.payload;
    },
    setTypeEmail(state, action) {
      state.typeEmail = action.payload;
    },
    saveDraft() {},
    editDraft() {},
    deleteDraft() {},
  },
});

export default slice.reducer;

export const {
  loadEmails,
  loadEmailsSuccessfully,
  selectEmail,
  updateEmail,
  updateFieldEmail,
  loadEmailTemplate,
  sendNewEmail,
  replyEmail,
  loadEmailTemplateSuccessfull,
  forwardEmail,
  uploadFile,
  removeFile,
  setIsSending,
  loadRecordData,
  loadRecordDataSuccess,
  getEmailTemplateById,
  setShowModal,
  setStatusDrawer,
  setShowEditor,
  setTypeEmail,
  markReadEmail,
  updateShowEdit,
  saveDraft,
  editDraft,
  deleteDraft,
} = slice.actions;
