import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isLoading: true,
  error: null,
  relatedObject: [],
  isLoadingDelete: true,
};

const slice = createSlice({
  name: "relatedObject",
  initialState,
  reducers: {
    //Load related object
    loadRelatedObject(state, action) {
      state.isLoading = true;
    },
    loadRelatedObjectSuccess(state, action) {
      state.isLoading = false;
      state.relatedObject = action.payload;
    },
    loadRelatedObjectFail(state, action) {
      state.isLoading = false;
    },

    //Create related object
    createRelatedObject(state, action) {
      state.isLoading = true;
    },
    createRelatedObjectSuccess(state, action) {
      state.isLoading = false;
    },
    createRelatedObjectFail(state, action) {
      state.isLoading = false;
    },
    //
    deleteRelated(state, action) {
      state.isLoadingDelete = true;
    },
    deleteRelatedSuccess(state, action) {
      state.isLoadingDelete = false;
    },
    deleteRelatedFail(state, action) {
      state.isLoadingDelete = false;
    },
  },
});

export default slice.reducer;

//Action
export const {
  loadRelatedObject,
  loadRelatedObjectFail,
  loadRelatedObjectSuccess,
  createRelatedObject,
  createRelatedObjectFail,
  createRelatedObjectSuccess,
  deleteRelated,
  deleteRelatedSuccess,
  deleteRelatedFail,
} = slice.actions;
