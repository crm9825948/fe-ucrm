import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listConfig: [],
  fieldsObjectArroval: [],
  showModalConfig: false,
};

const slice = createSlice({
  name: "approvalProcesses",
  initialState,
  reducers: {
    setShowModalConfig(state, action) {
      state.showModalConfig = action.payload;
    },
    getListConfig() {},
    getListConfigResult(state, action) {
      state.listConfig = action.payload;
    },
    createConfig() {},
    updateConfig() {},
    deleteConfig() {},
    getFieldsObject() {},
    getFieldsObjectResult(state, action) {
      state.fieldsObjectArroval = action.payload;
    },
  },
});

//Reducer
export default slice.reducer;

//Actions
export const {
  setShowModalConfig,
  getListConfig,
  getListConfigResult,
  createConfig,
  updateConfig,
  deleteConfig,
  getFieldsObject,
  getFieldsObjectResult,
} = slice.actions;
