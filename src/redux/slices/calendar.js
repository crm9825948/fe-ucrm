import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listEvent: [],
  listEventFilter: [],
  eventDetail: {},
  eventID: "",
  isLoadingCreate: false,
  isLoadingUpdate: false,
};

const slice = createSlice({
  name: "calendar",
  initialState,
  reducers: {
    getListEvent() {},
    getListEventSuccess(state, action) {
      state.listEvent = action.payload;
    },
    getListEventFilter() {},
    getListEventFilterSuccess(state, action) {
      state.listEventFilter = action.payload;
    },
    createEvent(state, action) {
      state.isLoadingCreate = true;
    },
    createEventSuccess(state, action) {
      state.isLoadingCreate = false;
    },
    createEventFail(state, action) {
      state.isLoadingCreate = true;
    },
    updateEvent(state, action) {
      state.isLoadingUpdate = true;
    },
    updateEventSuccess(state, action) {
      state.isLoadingUpdate = false;
    },
    updateEventFail(state, action) {
      state.isLoadingUpdate = true;
    },
    updateEventDetail(state, action) {
      state.eventDetail = action.payload;
    },
    deleteEvent() {},
    updateStatus() {},
    setEventId(state, action) {
      state.eventID = action.payload;
    },
  },
});

export default slice.reducer;

export const {
  getListEvent,
  getListEventSuccess,
  getListEventFilter,
  getListEventFilterSuccess,
  createEvent,
  createEventSuccess,
  createEventFail,
  updateEvent,
  updateEventSuccess,
  updateEventFail,
  updateEventDetail,
  deleteEvent,
  updateStatus,
  setEventId,
} = slice.actions;
