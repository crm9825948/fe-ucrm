import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

//Create picklist
export async function createPicklistAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-create/picklist-local", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Load picklist
export async function loadPicklistAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object-load/picklist-local", {
    headers: {
      Authorization: token,
    },
  });
}

//Load details
export async function loadDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object-load-detail/picklist-local", {
    headers: {
      Authorization: token,
    },
    params: { ID: data.ID },
  });
}

//update details
export async function updateDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-update-detail/picklist-local",
    {
      Matrix: data.Matrix,
    },
    {
      headers: {
        Authorization: token,
        ID: data.ID,
      },
    }
  );
}

//load value
export async function loadValueAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-load-value/picklist-local", data, {
    headers: {
      Authorization: token,
    },
  });
}

//delete value
export async function deleteValueAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "picklist/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}
