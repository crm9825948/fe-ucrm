import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadDataMonitorAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "tenant-config/monitoring", {
    headers: {
      Authorization: token,
    },
  });
}
