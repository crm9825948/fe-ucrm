import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListEmailTemplateAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "email-template/view-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getEmailTemplateByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email-template/view-detail", payload, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteEmailTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email-template/delete", payload, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createEmailTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email-template/create", payload, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateEmailTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email-template/update", payload, {
    headers: {
      Authorization: token,
    },
  });
}
