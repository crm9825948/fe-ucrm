import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadSpecificReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-specific-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadReportArithmeticAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-report-arithmetic", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadTotalRecordsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-report-record-number", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function exportReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/export-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function unpinReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/unpin-from-dashboard", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function pinToDashBoardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/pin-to-dashboard", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllWidgetsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "widget/all-widgets-in-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getDataWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "widget/get-data", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "widget/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function addWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "widget/add", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "widget/view-widget", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "widget/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllChartsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "chart/all-charts-in-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getDataChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "chart/view-chart", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "chart/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function loadCloumnValueAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "chart/get-column-values", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function addChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "chart/add", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "chart/view-configs", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "chart/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function refreshChartAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "chart/run-chart", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function refreshWidgetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "widget/run-widget", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function refreshAllReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "report/run-all-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function refreshAllDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "dashboard/run-all-pinned-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function quickFilterFeatAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/load-quick-filter", data, {
    headers: {
      Authorization: token,
    },
  });
}
