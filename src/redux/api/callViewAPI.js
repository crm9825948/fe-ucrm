import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getTemplateDefaultCallViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/setting/get-template-default", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateTemplateCallViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/setting/update-template", data, {
    headers: {
      Authorization: token,
    },
  });
}
