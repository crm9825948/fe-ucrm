import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getSettingKanbanViewAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "kanban-setting/get-object-setting",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getDataKanbanViewAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "kanban-setting/load-data",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteRecordAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object/delete-record",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadPaginationKanbanAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/load-pagination", data, {
    headers: {
      Authorization: token,
    },
  });
}
