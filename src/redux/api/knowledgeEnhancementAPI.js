import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object/objects-fields-permission",
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createKnowledgeSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "knowledge-base-enhancement/setting/create",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getAllSettingKnowledgeBaseAPI() {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + "knowledge-base-enhancement/setting/get-all",
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateEnhancementSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "knowledge-base-enhancement/setting/update",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteEnhancementSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API +
      `knowledge-base-enhancement/setting/delete/${data.record_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
