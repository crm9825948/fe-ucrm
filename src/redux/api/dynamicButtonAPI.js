import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadAllDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/load-all-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/create-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/toggle-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/delete-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/load_button_details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/update-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllActionsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/load-all-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createActionDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/create-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeActionDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/toggle-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteActionDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/delete-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsActionDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/load-action-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateActionDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/update-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUserDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/load-user-button", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function runDynamicButtonAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dynamic-button/run-button", data, {
    headers: {
      Authorization: token,
    },
  });
}
