import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListRuleIncomingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/get-list-incoming-rule",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function saveRuleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/setting-incomming-rule",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getRuleByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email/get-detail-incoming-rule", payload, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteRuleByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/delete-setting-incoming-rule",
    payload,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateRuleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/update-setting-incoming-rule-v2",
    payload,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updatePositionRuleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/update-list-rules-position",
    payload,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateStatusRuleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "email/on-off-incoming-rule",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
