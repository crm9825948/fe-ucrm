import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadRulesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-duplicate-rules/load-rules", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateRulesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/update-duplicate-crm",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updatePositionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/location-update",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function actionDuplicateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/action-duplicate",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteDuplicateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/delete-duplicate-crm",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createRulesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/create-duplicate-crm",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createDuplicate3rdAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/create-duplicate-3rd",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function updateDuplicate3rdAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/update-duplicate-3rd",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function notifyAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-duplicate-rules/notify", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteThirdPartyAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-duplicate-rules/delete-duplicate-3rd",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
