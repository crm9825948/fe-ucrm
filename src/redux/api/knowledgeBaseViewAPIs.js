import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListArticleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "knowledge_base/get_list_articles",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getTreeKnowledgeBaseAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "knowledge-base/load-menu", {
    headers: {
      Authorization: token,
    },
  });
}
