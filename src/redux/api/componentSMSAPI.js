import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getAllMessageAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + `sms/get-all-message/${data.objectId}/${data.record_id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function sendSMSAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sms/send-sms", data.dataSms, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getSMSTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `sms/render/template-id/${data.templateId}/record-id/${data.recordId}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListSMSTemplateAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "sms/view_all_template", {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteSMSAPI(data) {
  const token = await checkTokenExpiration();
  return axios.patch(BASE_URL_API + "sms/delete-messages", data.dataDelete, {
    headers: {
      Authorization: token,
    },
  });
}
