import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadAllExportHistoryAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/latest-export-history", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadExportHistoryAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-history-by-id", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteExportHistoryAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "report/delete-history-by-id", {
    headers: {
      Authorization: token,
    },
    data,
  });
}
