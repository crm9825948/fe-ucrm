import axios from "axios";
import request from "util/request";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export function getListEmailIncomingAPI() {
  return request.get("email/get-list-incoming-v2");
  // return axios.get(BASE_URL_API + 'email/get-list-incoming-v2', {
  //     headers: {
  //         Authorization: token,
  //     }
  // })
}

export async function changeStatusAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/on-off-setting-in-coming-config",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function submitMaiBoxIncomingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/setting-incomming-config",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteMailBoxIncomingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/delete-incomming-config",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function checkConnectionAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/check-conection",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getEmailIncomingByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/get-detail-in-coming-config-v2",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateMailBoxIncomingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/update-setting-in-coming-config",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function setDefaultIncomingConfigAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/make-default-incoming-config",
    {
      _id: payload,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getEmailFoldersAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `scan-email/get-email-folders/${payload.config_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function scanEmailAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `scan-email/check-scan-specific-tenant?tenant_id=${payload.tenant_id}&object_id=${payload.object_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
