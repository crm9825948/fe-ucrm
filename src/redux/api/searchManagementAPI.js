import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadDataSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "search-management/view-search-management",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadListObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "search-management/all-objects", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function toggleGlobalSearchAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "search-management/toggle-global-search",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateSearchManagementAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "search-management/update-search-management",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
