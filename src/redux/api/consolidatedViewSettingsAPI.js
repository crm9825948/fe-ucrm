import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

//ExposeAPI
export async function loadAllExposeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/load-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateExposeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

//PopupSetting
export async function loadAllPopupSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/setting/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updatePopupSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/setting/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createComponentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/component/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadPopupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function popupCheckWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "popup/check-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

//CoreSetting
export async function loadCoreSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/load-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load component
export async function loadComponentsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/component/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

//delete component
export async function deleteComponentsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/component/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

//update component
export async function updateComponentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/component/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createCoreSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateCoreSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteCoreSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsCoreSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/load-setting-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

//ExposeAPICreate
export async function loadExposeCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/setting/get-list", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createExposeCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/setting/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateExposeCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "expose-api/setting/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteExposeCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "expose-api/setting/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function loadDetailsExposeCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/setting/get-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

//IC integration

export async function loadUsersMappingICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/user-mapping/get-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadLayoutAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/layout/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateLayoutAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "consolidated-view/layout/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createUserMappingICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/user-mapping/create", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function deleteUserMappingICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/user-mapping/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsUserMappingICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/user-mapping/get-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateUserMappingICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/user-mapping/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getTokenAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/generate-token", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getConfigICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/config/get-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createConfigICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/config/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateConfigICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ic/config/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadMappingExposeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/mapping/get-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createMappingExposeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "expose-api/mapping/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateMappingExposeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "expose-api/mapping/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function resetAllICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "ic/config/reset-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "consolidated-view/load-component-details",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getLogsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/get-logs", data, {
    headers: {
      Authorization: token,
    },
  });
}

//VoiceBotSetting
export async function getDetailsVoiceBotAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "vbee/get-setting-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getAllSlaAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sla-instance/view-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createVoiceBotSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "vbee/create-setting", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateVoiceBotSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "vbee/update-setting", data, {
    headers: {
      Authorization: token,
    },
  });
}

// Timeline
export async function getInteractionsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/view-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadObjectsTargetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/load-object-target", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateInteractionDetailsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "interactions/update-interaction-details",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadRecordInteractionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "list-view/load-record-by-interaction-id",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

// CTI Setting
export async function getCTIConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get_config", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUsersMappingCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get_list_users", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createUserMappingCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/register", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteUserMappingCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "cti/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function loadDetailsUserMappingCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get_user", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateUserMappingCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "cti/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateConfigCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "cti/update_config", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateRecordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "third-party/update-record", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function resetAllCTIAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "cti/reset-settings", {
    headers: {
      Authorization: token,
    },
  });
}

export async function generateTokenC247API(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/generate-token-c247", data, {
    headers: {
      Authorization: token,
    },
  });
}

// Callcenter
export async function getCallCenterConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "finesse-integration/load-advance-config", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUsersMappingCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "finesse-integration/get-list-user-mapping", {
    headers: {
      Authorization: token,
    },
  });
}

export async function createUserMappingCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "finesse-integration/create-user-mapping",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateUserMappingCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "finesse-integration/update-user-mapping",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteUserMappingCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API + "finesse-integration/delete-user-mapping",
    {
      headers: {
        Authorization: token,
      },
      data,
    }
  );
}

export async function updateConfigCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "finesse-integration/setting-advance-config",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function resetAllConfigCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "finesse-integration/reset-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function testConnectionCalabrioAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "calabrio-integration/test-connection", {
    headers: {
      Authorization: token,
    },
  });
}
export async function updateCallScoringAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + "calabrio-integration/trigger-update-call-scoring",
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

//Voice Biometric
export async function getConfigVoiceBiometricAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "voice-biometric-integration/get-configuration",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateConfigVoiceBiometricAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "voice-biometric-integration/setup-configuration",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteConfigVoiceBiometricAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "voice-biometric-integration/delete-configuration",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createInteractionConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/object-setting/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateInteractionConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/object-setting/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailInteractionConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "interactions/object-setting/load-details",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteInteractionConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/object-setting/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function loadPageTitleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-field/page-title", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function loadConfigComponentsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "consolidated-view/layout/update_component",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
