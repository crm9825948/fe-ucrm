import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/get-all-new-v2", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getAllUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/load-all-user", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadRolesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "role/get-roles-by-level-v2", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUserReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "role/get-all-user-report-to", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "user/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/get-all-new-v2", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "user/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function changePasswordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "admin/change-user-password", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function changePasswordUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/change-password", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function changePasswordUserRootAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "user/change-password-root", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUserDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/get-user-details", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function LoadUsernameApi() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "user/get-user-name", {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateUserSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "user-setting/update",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function searchUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/search", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/update-active", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function importUsersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "import-users/", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function generateSampleDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "import-users/generate-sample-data", {
    headers: {
      Authorization: token,
    },
    responseType: "arraybuffer",
  });
}

export async function getLogsUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "import-users/logs", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function exportUsersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "users/export", {
    headers: {
      Authorization: token,
    },
    responseType: "arraybuffer",
  });
}

export async function getDomainAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "admin/get-domain", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getUserRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "admin/get-user-rule", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getReportPermissionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report-permission", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function setUserRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "admin/set-rule", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function setUserRuleAllAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "admin/toggle-action", data, {
    headers: {
      Authorization: token,
    },
  });
}
