import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "approval/config/get-list", {
    headers: {
      Authorization: token,
    },
  });
}
export async function createConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "approval/config/create", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function updateConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + `approval/config/update/${data.id}`,
    data.payload,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function deleteConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + `approval/config/delete/${data.id}`, {
    headers: {
      Authorization: token,
    },
  });
}
