import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadListSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sla-setting/view-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "sla-setting/toggle-sla", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function notificationSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "sla-setting/toggle-notification", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function duplicateSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sla-setting/copy-sla", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "sla-setting/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function createSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sla-setting/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "sla-setting/view", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateSLAAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "sla-setting/edit", data, {
    headers: {
      Authorization: token,
    },
  });
}
