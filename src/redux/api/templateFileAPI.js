import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function newFileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "integration/google/generate_new_empty_file",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function copyFileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "integration/google/make_copy", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function uploadTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "integration/google/upload_template", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getListTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "integration/google/list_templates", {
    headers: {
      Authorization: token,
    },
  });
}

export async function editTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "integration/google/edit_template", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function deleteTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API + "integration/google/delete_template",

    {
      headers: {
        Authorization: token,
      },
      data: data,
    }
  );
}
