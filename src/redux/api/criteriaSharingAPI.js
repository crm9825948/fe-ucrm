import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function createRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "criteria-sharing-setting/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadRuleObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "get-criteria-sharing-setting", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "criteria-sharing-setting/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "criteria-sharing-setting/update", data, {
    headers: {
      Authorization: token,
    },
  });
}
