import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListNotiAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "notifications/get_list", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function seenOneNotiAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "notifications/seen_one", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function seenAllNotiAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "notifications/seen_all", data, {
    headers: {
      Authorization: token,
    },
  });
}
