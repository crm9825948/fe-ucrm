import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function uploadLogoAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "tenant-config/set-default-brand",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListLogoAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "tenant-config/get-all-brands", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getDefaultBrandAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "tenant-config/get-default-brand", {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteLogoByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "tenant-config/delete-brand", {
    headers: {
      Authorization: token,
    },
    data: {
      ...payload,
    },
  });
}
