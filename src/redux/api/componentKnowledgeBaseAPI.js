import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function deleteArticleAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object/mass-delete",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
