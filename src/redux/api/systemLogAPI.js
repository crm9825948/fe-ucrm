import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

//Create tenants api
export async function loadLogsAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "log-system/get", data, {
    headers: {
      Authorization: token,
    },
  });
}
