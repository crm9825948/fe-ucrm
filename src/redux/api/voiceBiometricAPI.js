import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function callEventAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "voice-biometric-integration/call-voice-biometric-event",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateCustomerAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "voice-biometric-integration/update-customer-last-choice",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
