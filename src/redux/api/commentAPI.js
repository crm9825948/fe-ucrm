import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function postCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/post", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function editCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function likeCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/interact", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function replyCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/reply", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadReplyCommentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "comment/load-reply", data, {
    headers: {
      Authorization: token,
    },
  });
}
