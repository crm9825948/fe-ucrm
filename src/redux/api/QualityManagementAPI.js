import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function changeActiveSettingsAPI(data) {
  const token = await checkTokenExpiration();
  if (data.active) {
    return axios.post(
      BASE_URL_API + "quality-management/active-quality-management",
      data,
      {
        headers: {
          Authorization: token,
        },
      }
    );
  } else {
    return axios.post(
      BASE_URL_API + "quality-management/toggle_deactive_quality",
      data,
      {
        headers: {
          Authorization: token,
        },
      }
    );
  }
}

export async function getAllFormQMSagaAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "quality-management/get-all-form-qm", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getDetailSettingsAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/view-all-setting-qm",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function saveDetailSettingsAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "quality-management/save-setting", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getListOptionsQuickFilterAPI() {
  const token = await checkTokenExpiration();

  return axios.get(BASE_URL_API + "quality-management/get-qm-fe-utils", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getListAllOptionsQuickFilterAPI() {
  const token = await checkTokenExpiration();

  return axios.get(
    BASE_URL_API + "quality-management/get-custom-quick-filter",
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListDataFilterAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "list-view/load-data", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getListUserAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/get-profile-picture",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListScoringAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/view-all-qm-interaction",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListDetailStateAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/get-event-calculations",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getEmailsAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/get-detail-email",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getInteractionDetailAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/get-detail-component",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getURLRecordAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "quality-management/get-url-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getTotalRecordAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "list-view/load-pagination", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getActiveQMViewAPI() {
  const token = await checkTokenExpiration();

  return axios.get(BASE_URL_API + "quality-management/get-infor-active", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadFieldsAPI(data) {
  const token = await checkTokenExpiration();
  if (data.object_id)
    return axios.post(BASE_URL_API + "object/objects-fields-permission", data, {
      headers: {
        Authorization: token,
      },
    });
}

export async function getChatAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "quality-management/get-detail-sms", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function UpdateListOptionsQuickFilterAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/update-custom-quick-filter",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListChatICAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(
    BASE_URL_API + "quality-management/get-detail-ic-chat",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
