import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function checkAvailableCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/check-available", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deactivateCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/deactivate", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activateCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/activate", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/all-campaigns", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/get-objects-for-campaign", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function validateObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/validate-campaign-objects", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/create-campaign", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/view-campaign-config", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/edit-campaign-config", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadInfoComponentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/campaign-info-component", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadTemplateComponentAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "campaign/campaign-template-component",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/delete-campaign", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getInfoMailChimpAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/mailchimp/root-info", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function connectMailchimpAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "campaign/mailchimp/auth", {
    headers: {
      Authorization: token,
    },
  });
}

export async function disconnectMailchimpAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "campaign/mailchimp/remove-mailchimp-account",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListCampaignMailChimpAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/mailchimp/campaigns-list", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getContentCampaignMailchimpAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "campaign/mailchimp/campaign-content",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function massDeleteCampaignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "campaign/mass-delete-campaign", data, {
    headers: {
      Authorization: token,
    },
  });
}
