import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

//load related objects
export async function loadObjectAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object-related/load", {
    headers: {
      Authorization: token,
    },
  });
}

//Create related field
export async function createRelatedObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-related/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Delete related object
export async function deleteRelatedAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-related/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}
