import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListAssignmentRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `assignment-rule/get-all/${data._id}`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createAssignmentRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "assignment-rule/create", data.data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteAssignmentRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + `assignment-rule/delete/${data.id}`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getAssignmentRuleDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `assignment-rule/get-details/${data.id}`, {
    headers: {
      Authorization: token,
    },
  });
}
export async function updateAssignmentRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + `assignment-rule/update/${data.id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function runManualAssignAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `assignment-rule/run-manual-assign/${data.id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function resetUsersCapacityAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `assignment-rule/reset-users-capacity/${data.id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updatePriorityRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + `assignment-rule/update-priority-rule/${data.id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
