import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadFoldersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "report/get-folder-list", {
    headers: {
      Authorization: token,
    },
  });
}

export async function createFoldersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/create-folder", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateFoldersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/update-folder", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteFoldersAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/delete-folder", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadReportsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-folder-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/objects-fields-permission", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadSharedReportsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-shared-reports", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/get-report-info", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/create-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function cloneReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/clone-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function runReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/run-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDataReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/load-data-report", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function exportCustomReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/export-report-custom", data, {
    headers: {
      Authorization: token,
    },
    responseType: "blob",
  });
}

export async function cancelReportAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "report/cancel-running", data, {
    headers: {
      Authorization: token,
    },
  });
}
