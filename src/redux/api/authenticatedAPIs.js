import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { RANDOM_VARIABLE } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
const jwt = require("jsonwebtoken");

export function loginAPI({ email, pass }) {
  let bodyFormData = new FormData();
  bodyFormData.append("email", email);
  bodyFormData.append(
    "pass",
    jwt.sign({ password: pass }, RANDOM_VARIABLE, { algorithm: "HS256" })
  );
  return axios.post(BASE_URL_API + "login/", bodyFormData);
}

export function refreshToken() {
  return axios.get(BASE_URL_API + "token/obtain", {
    headers: {
      Authorization: localStorage.getItem("setting_refreshToken"),
    },
  });
}

export async function checkValidToken() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "check-token", {
    headers: {
      Authorization: token,
    },
  });
}

export function logoutAPI() {
  return axios.get(BASE_URL_API + "logout/", {
    headers: {
      Authorization: localStorage.getItem("setting_accessToken"),
    },
  });
}

export function forgotPasswordAPI(payload) {
  return axios.post(BASE_URL_API + "forgot-password", { ...payload });
}

export function resetPasswordAPI(payload) {
  return axios.post(BASE_URL_API + "reset-password", { ...payload });
}

export function getDataVersionBEAPI(payload) {
  return axios.get(BASE_URL_API + "check-version?view_markdown=1", payload);
}
export async function getMediaAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + payload, {
    headers: {
      Authorization: token,
    },
    responseType: "blob",
  });
}
