import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListEventAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `calendar-event/get-all${data.url}`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getListEventFilterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `calendar-event/get-all${data.urlFilter}`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createEventAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "calendar-event/create", data.data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateEventAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + `calendar-event/update/${data.id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteEventApi(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + `calendar-event/delete/${data.id}`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API +
      `calendar-event/${data.status}/events/${data.event_id}/guests/${data.guest_id}`,
    {},
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
