import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
//Create section
export async function createSectionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-section/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Load fields
export async function loadFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-section/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Update position sections
export async function updatePositionSectionsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-section/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Delete section
export async function deleteSectionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-section/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Create field
export async function createFieldAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-field/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Update position field
export async function updateFieldPositionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-section/update-field", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Update field
export async function updateFieldAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-field/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Delete field
export async function deleteFieldAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-field/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Load related of object
export async function loadRelatedObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-related", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Load field of object
export async function loadObjectFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-field", data, {
    headers: {
      Authorization: token,
    },
  });
}

//object-related-field
export async function loadListOfFieldKeyAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-related-field", data, {
    headers: {
      Authorization: token,
    },
  });
}
