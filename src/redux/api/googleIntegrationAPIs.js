import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListGoogleIntegrationSettingAPI() {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + "integration/google/load-default-oauth2-credential",
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListAccountMappingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "integration/google/load-all-account-mapping",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function addAccountMappingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "integration/google/generate-oauth2-authorization-url",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createGoogleIntegrationSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "integration/google/store-credential",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateGoogleIntegrationSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "integration/google/update-credentials",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteGoogleIntegrationSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "integration/google/delete-credentials", {
    data: {
      ...payload,
    },
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteAccountMappingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API + "integration/google/delete-account-mapping",
    {
      data: {
        ...payload,
      },
      headers: {
        Authorization: token,
      },
    }
  );
}
