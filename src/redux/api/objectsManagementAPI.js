import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadObjectAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object/get-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function createGroupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-group/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadGroupAPI() {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-group/load",
    {},
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadObjectsAPI() {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object/load",
    {},
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateGroupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-group/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteGroupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-group/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}
