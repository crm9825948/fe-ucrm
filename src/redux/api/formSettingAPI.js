import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function createFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + `internet-form/config/create`, data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API + `internet-form/config/delete/${data.config_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + `internet-form/config/get-list/${data.id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + `internet-form/config/update/${data.config_id}`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function toggleFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + `internet-form/config/toggle`, data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getFormSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `internet-form/config/get/${data.config_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function submitFormAPI(data) {
  return axios.post(BASE_URL_API + `internet-form/submit`, data);
}
