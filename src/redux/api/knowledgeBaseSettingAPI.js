import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getKnowledgeBaseSettingAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "knowledge_base/view_default_setting", {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateKnowledgeBaseSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "knowledge_base/update_setting",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteKnowledgeBaseSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "knowledge_base/delete_setting", {
    data: {
      ...payload,
    },
    headers: {
      Authorization: token,
    },
  });
}
