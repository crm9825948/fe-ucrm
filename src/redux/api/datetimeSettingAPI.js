import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
export async function loadConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/all-configs", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function editConfigAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "tenant-config/edit-configs", data, {
    headers: {
      Authorization: token,
    },
  });
}
