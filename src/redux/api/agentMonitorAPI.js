import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getUserStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user-management/get-user-status", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getUserStatusLogAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + `user-management/get-user-status-log/${data.user_id}`,
    data.payload,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function setStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user-management/set-status", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function exportAgentMonitorAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "user-management/export-users-status-logs",
    data,
    {
      headers: {
        Authorization: token,
      },
      responseType: "blob",
    }
  );
}
