import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadListStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get-status-code", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadListReasonCodeAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get-reason-code", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function changeAgentStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/change-agent-status", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function makeCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/make-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function endCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/end-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function holdCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/hold-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function unHoldCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/unhold-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAgentsOnlineAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get-agents-online", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function transferCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/transfer-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function conferenceCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/mark-conference", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function consultCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/consult-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function answerCallAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/answer-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateInteractionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/update-interaction", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createInteractionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/manual_create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getFieldCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get-field", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getObjectsToCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/get-object-call", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function searchCoreCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/search-core", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createRecordCoreCallCenterAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "cti/create-record-from-core", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function connectExtAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `finesse-integration/connect-extension/${data.ext}?force_connect=${data.force_connect}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function disconnectExtAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `finesse-integration/disconnect-extension`, {
    headers: {
      Authorization: token,
    },
  });
}
