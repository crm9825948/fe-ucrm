import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function LoadEmailApi(id) {
  const token = await checkTokenExpiration();
  let payload = {
    record_id_main: id,
  };
  return axios.post(BASE_URL_API + "email/get-email", JSON.stringify(payload), {
    headers: {
      Authorization: token,
    },
  });
}

export async function LoadTemplateEmailApi() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "email-template/view-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function SendNewEmailApi(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/send-email",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function ReplyEmailApi(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/reply-email",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function ForwardEmailApi(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/forward-email",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function LoadRecordDataApi(payload) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      "load-record-data?id=" +
      payload.id +
      "&object_id=" +
      payload.object_id,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function GetEmailTemplateByIdApi(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/parse-email-template",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function MarkReadEmailApi(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/mark-read",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function saveDraftAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email/store-draft", payload, {
    headers: {
      Authorization: token,
    },
  });
}
export async function editDraftAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email/update-draft", payload, {
    headers: {
      Authorization: token,
    },
  });
}
export async function deleteDraftAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email/delete-draft", payload, {
    headers: {
      Authorization: token,
    },
  });
}
