import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListSMSOutgoingAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "sms/view_all_sms_outgoing_config", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getSMSOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "sms/view_details_sms_outgoing_config",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function saveSMSOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "sms/create_sms_outgoing_config",
    {
      ...payload,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteSMSOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "sms/delete_sms_outgoing_config", {
    headers: {
      Authorization: token,
    },
    data: {
      ...payload,
    },
  });
}

export async function updateSMSOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "sms/update_sms_outgoing_config",
    {
      ...payload,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function testSMSOutgoingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + `sms/test/${data.setting_id}`, data.data, {
    headers: {
      Authorization: token,
    },
  });
}
