import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function createSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "high-light/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "high-light/load-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function changeStatusAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "high-light/toggle", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "high-light/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "high-light/update", data, {
    headers: {
      Authorization: token,
    },
  });
}
