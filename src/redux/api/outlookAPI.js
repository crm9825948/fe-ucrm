import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function addAccountAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + "o365-integration/generate-authorization-url",
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadOutlookAccountAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "o365-integration/get-all-account", {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteAccountAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API + `o365-integration/delete-account/${data.record_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
