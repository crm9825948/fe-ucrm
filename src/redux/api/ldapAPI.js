import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function createSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ldap/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ldap/load-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ldap/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateSettingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ldap/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function syncDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "ldap/sync-data", data, {
    headers: {
      Authorization: token,
    },
  });
}
