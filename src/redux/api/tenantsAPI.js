import axios from "axios";
import { BASE_URL_API, INTERVAL_URL } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

//Create tenants api
export async function createTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "post_tenants/", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load tenants
export async function loadTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "search_tenants_field/", data, {
    headers: {
      Authorization: token,
    },
  });
}

//disable tenants
export async function disableTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "tenant/", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

//edit tenants

export async function editTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "tenant/", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getLogCeleryAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "interval/get-log", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getLogIntervalAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(INTERVAL_URL + "interval/get-log", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function cloneTenantAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/clone-settings", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "update-tenant", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getAllRootPermissionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "root-permission/get-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateRootAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "root-permission/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createPermissionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "root-permission/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deletePermissionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "root-permission/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadLogAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "audit-log/get", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function resetPasswordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user-root/reset-pwd", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function healthCheckInteractionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "tenant-config/health-check-interaction",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createPackageAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/create-edition", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadPackageAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "tenant-config/view-edition", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadPackageDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/get-details-edition", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updatePackageAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/update-edition", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deletePackageAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "tenant-config/delete-edition", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function getTenantRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/get-tenant-rule", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function setTenantRuleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/set-rule", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function setTenantRuleAllAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/toggle-action-tenant", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function runIndexAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/run-index", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function allowCloneTenantAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/allow-clone-tenant", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/load-all-object", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateObjectMediaAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant-config/update-object-media", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateStatusTenantAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "tenant/on-off-tenant", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteTenantsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "tenant/delete-tenant ", {
    headers: {
      Authorization: token,
    },
    data,
  });
}
