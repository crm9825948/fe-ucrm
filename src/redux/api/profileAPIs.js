import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadProfilesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "profile/get-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteProfileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "profile/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function createProfileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "profile/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailProfileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "profile/get-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateProfileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "profile/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadProfileFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-profile-field/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateProfileFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-profile-field/update", data, {
    headers: {
      Authorization: token,
    },
  });
}
