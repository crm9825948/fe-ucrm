import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListSMSTemplateAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "sms/view_all_template", {
    headers: {
      Authorization: token,
    },
  });
}

export async function createSMSTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "sms/create_template",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteSMSTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "sms/delete_template", {
    data: { ...payload },
    headers: {
      Authorization: token,
    },
  });
}

export async function getSMSTemplateByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "sms/view_details_template",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateSMSTemplateAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "sms/update_template",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
