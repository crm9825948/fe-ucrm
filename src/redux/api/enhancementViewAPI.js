import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadListCategoryAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/category/get-tree-view`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function loadArticleDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/article/dashboard`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getTopViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/article/top-viewed`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getNewestAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/article/newest`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListArticleSearchAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + `knowledge-base-enhancement/${data.object_id}/article/list`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getArticleDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/article/view/${data.record_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function createServiceAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/${data.service}/create`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateServiceAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/${data.service}/update`,
    data.data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteServiceAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/${data.service}/delete`,
    {
      headers: {
        Authorization: token,
      },
      data: data.data,
    }
  );
}

export async function syncDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `knowledge-base-enhancement/${data.object_id}/fetch-data`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
export async function testConnectionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API +
      `knowledge-base-enhancement/${data.object_id}/test-es-connection`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function reIndexAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `knowledge-base-enhancement/${data.object_id}/re-index`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
