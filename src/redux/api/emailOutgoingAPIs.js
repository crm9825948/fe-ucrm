import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListEmailOutgoingAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "email/get-out-going-config", {
    headers: {
      Authorization: token,
    },
  });
}

export async function getSenderAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + `email/${payload.object_id}/get-sender`, {
    headers: {
      Authorization: token,
    },
  });
}

export async function addNewEmailOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/setting-outgoing-config",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getEmailOutgoingByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/get-detail-out-going-config",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateEditEmailOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/update-setting-outgoing-config",
    JSON.stringify(payload),
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteEmailOutgoingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/delete-setting-outgoing-config",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function setDefaultOutgoingConfigAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "email/make-default-out-going-config",
    {
      _id: payload,
    },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
