import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadAllWorkflowsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "workflow/listview-workflow", {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/on-off-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/delete-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/create-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function detailsWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/detail-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/update-workflow", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadAllActionsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/action-listview", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadUserFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "action/get-user-field", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function activeActionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/action-on-off", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createActionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/create-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsActionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/load-details-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateActionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/update-action", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteActionWorkflowAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "workflow/action-delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getLinkingFieldEmailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "email/get_linking_field", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getTargetObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `action/${data.source_object}/get-all-target-object`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function getListPopupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + `popup/load-details`, data, {
    headers: {
      Authorization: token,
    },
  });
}
