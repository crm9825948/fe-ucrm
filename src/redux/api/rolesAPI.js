import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadListRolesAPI() {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "role/tree-view-get-all", {
    headers: {
      Authorization: token,
    },
  });
}

export async function createRoleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "role/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteRoleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "role/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function loadDetailsRoleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "role/v2/get-one", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateRoleAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "role/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getRoleToSwitchAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "role/get-role-to-switch", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function exportRolesAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "role/export-permission-role", data, {
    headers: {
      Authorization: token,
    },
    responseType: "arraybuffer",
  });
}
