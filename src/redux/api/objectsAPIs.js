import axios from "axios";
import { BASE_URL_API } from "../../constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
//Load favourite objects
export async function loadFavouriteObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "favorite-object/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

//Toggle favourite objects
export async function toggleFavouriteObjectAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/toggle", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load form create
export async function loadFormCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/load-form-create", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load user assign to
//load user assign to
export async function loadUserAssignToAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "user/get-user-assign-record-to", {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadFieldsAPI(data) {
  const token = await checkTokenExpiration();
  if (data.object_id)
    return axios.post(BASE_URL_API + "object/objects-fields-permission", data, {
      headers: {
        Authorization: token,
      },
    });
}

//create record
export async function createRecordAPI(data) {
  const token = await checkTokenExpiration();
  console.log(data);
  if (data?.object_id === "obj_contact_00000001") {
    return axios.post(
      BASE_URL_API + "object-standard/insert-record-obj-stand",
      data,
      {
        headers: {
          Authorization: token,
        },
      }
    );
  } else
    return axios.post(BASE_URL_API + "object/insert", data, {
      headers: {
        Authorization: token,
      },
    });
}

//load header
export async function loadHeaderAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/load-fields", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load data
export async function loadDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/load-data", data, {
    headers: {
      Authorization: token,
    },
  });
}

//loadPagination
export async function loadPaginationAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/load-pagination", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load user
export async function loadUserAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "user/get-all-new-v2", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load linking field value
export async function loadLinkingFieldValueAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/load-linking-field-value", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load record data
export async function loadRecordDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(
    BASE_URL_API + `load-record-data?id=${data.id}&object_id=${data.object_id}`,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

//update record
export async function updateRecordAPI(data) {
  const token = await checkTokenExpiration();
  if (data?.object_id === "obj_contact_00000001") {
    return axios.post(
      BASE_URL_API + "object-standard/update-record-obj-stand",
      data,
      {
        headers: {
          Authorization: token,
        },
      }
    );
  } else
    return axios.post(BASE_URL_API + "object/update-record", data, {
      headers: {
        Authorization: token,
      },
    });
}

//delete record
export async function deleteRecordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/delete-record", data, {
    headers: {
      Authorization: token,
    },
  });
}

//mass delete
export async function massDeleteRecordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/mass-delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

//loadd target
export async function loadTargetAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "picklist/load-target", data, {
    headers: {
      Authorization: token,
    },
  });
}

//mass edit
export async function massEditAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/mass-edit", data, {
    headers: {
      Authorization: token,
    },
  });
}

//get records
export async function getRecordsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/get-records", data, {
    headers: {
      Authorization: token,
    },
  });
}

//merge record
export async function mergeRecordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/merge-record", data, {
    headers: {
      Authorization: token,
    },
  });
}

//create custom view
export async function createCustomViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load custom view
export async function loadCustomViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-custom-view/load-all-custom-view",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

//load custom view detail
export async function loadDetailCustomViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-custom-view/load-details", data, {
    headers: {
      Authorization: token,
    },
  });
}

//update custom view detail
export async function updateCustomViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

//delete custom view
export async function deleteCustomViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/delete", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load pagination info
export async function loadPagiInfoAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-custom-view/load-pagination-info",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

//load specific custom view
export async function loadSpecificAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "object-custom-view/load-specific-custom-view",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

//load source objects
export async function loadSourceObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object/load-object-source", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load target object
export async function loadTargetObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "interactions/load-object-target", data, {
    headers: {
      Authorization: token,
    },
  });
}

//import data
export async function importDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "crm/get-file-headers", data, {
    headers: {
      Authorization: token,
    },
  });
}

//upload file and mapping
export async function uploadFileAndMappingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "crm/upload-file-and-mapping", data, {
    headers: {
      Authorization: token,
    },
  });
}

//download
export async function downloadFileAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "crm/export-template", data, {
    headers: {
      Authorization: token,
    },
    responseType: "blob",
  });
}

//view logs
export async function viewLogsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "crm/import-log", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load list view with details
export async function loadListViewWithDetailsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view-details/load", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load list view with details
export async function updateListViewDetailAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view-details/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load list view with details
export async function recycleBinLoadListViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "recycle-bin/load-list-view", data, {
    headers: {
      Authorization: token,
    },
  });
}

//load list view with details
export async function restoreRecordAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "recycle-bin/restore-record", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteMultipleRecord(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "recycle-bin/delete-multiple-records", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function emptyTrashAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "recycle-bin/empty-trash", {
    headers: {
      Authorization: token,
    },
    data,
  });
}

export async function manualSharingAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "manual-sharing", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-template/create", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-template/load-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-template/update", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateStatusTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-template/status", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function makeDefaultTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-template/default", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteTemplateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "object-template/delete", {
    headers: {
      Authorization: token,
    },
    data: {
      ...data,
    },
  });
}

export async function viewObjectSearchAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "search-management/view-object-search-management",
    data,
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function globalSearchAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "search-management/global-search", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function shareViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/share", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadPermissionAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/load-permission", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateDefaultViewAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "list-view/default", data, {
    headers: {
      Authorization: token,
    },
  });
}
