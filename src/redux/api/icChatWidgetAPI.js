import axios from "axios";
import { checkTokenExpirationIC } from "services/apiIC";
import { checkTokenExpiration } from "contexts/TokenCheck";
import { BASE_URL_API } from "constants/constants";

export async function logInICAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "login-get-token-ic", data, {
    headers: {
      Authorization: token,
    },
  });
}

export function refreshTokenICAPI(data) {
  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/auth/refresh-token",
    data,
    {
      headers: {
        Authorization:
          "Bearer " + localStorage.getItem("icIntegration_accessToken"),
      },
    }
  );
}

export async function getUserICAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/user/get-information",
    {},
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getChatBotICAPI() {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "settings/api/chat-bot-new/get-all",
    {},
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getAllUserICAPI() {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/user/get-all-user-state-not-report-to",
    {},
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getListMessagesICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interaction/get-list-interaction-by-view-type",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function updateStatusICCRMAPI(data) {
  const token = await checkTokenExpiration();

  return axios.post(BASE_URL_API + "user-management/set-status/ic", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateStatusICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/user/update-state",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getStatusICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/user/get-user-state",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getTagsICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interaction-tag/get-all",
    {},
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function addTagICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interaction-tag/add-tag-to-interaction",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function deleteTagICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interaction-tag/delete-in-interaction",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function createTagICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interaction-tag/create",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getMessageDetailsICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interactiondetail/get-list-interaction-detail",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function disableBotICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "conversation-session/api/agent-conversation/unlock-conversation",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function sendMessageICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "conversation-session/api/distribution/send-mess",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function completeConversationICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "conversation-session/api/agent-conversation/close-conversation",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function resgisterSignalrICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "notification/api/signalr/register",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function registerConnectionIntervalICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "notification/api/signalr/register-interval",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getQuickMessagesICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "settings/api/message-template/get-all-chat",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getUserStateICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "user/api/user/get-list-user-state",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getInteractionMemberICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interactionmember/get-list-interaction-member",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function addAgentICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "conversation-session/api/agent-conversation/add-agent",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function transferAgentICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "conversation-session/api/agent-conversation/transfer",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getIndexMessageICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "idstore/api/interactiondetail/get-index-message",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getFacebookPostICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "social-connection/api/social-custom/get-facebook-post",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getListSocialRegisterICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "settings/api/social-register/get-all",
    {},
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}

export async function getEmailDetailsICAPI(data) {
  const token = await checkTokenExpirationIC();

  return axios.post(
    localStorage.getItem("icIntegration_link_api") +
      "email/api/email-data/get-details-from-ids",
    data,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
}
