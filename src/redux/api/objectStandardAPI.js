import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function objectStandardCreateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-standard/create", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function objectStandardDeleteAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "object-standard/delete", {
    headers: {
      Authorization: token,
    },
    data: data,
  });
}
export async function objectStandardGetListAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object-standard/get-all", {
    headers: {
      Authorization: token,
    },
  });
}
export async function objectStandardGetFieldsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-standard/get-all-field", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function objectStandardGetObjectsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.get(BASE_URL_API + "object-standard/get-all-object-nstd", {
    headers: {
      Authorization: token,
    },
  });
}
export async function objectStandardSyncDataAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "object-standard/sync-data", data, {
    headers: {
      Authorization: token,
    },
  });
}
export async function objectStandardUpdateAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "object-standard/update", data, {
    headers: {
      Authorization: token,
    },
  });
}
