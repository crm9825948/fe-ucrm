import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getAvailableDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/get-available-dashboards", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function getDefaultDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/get-default-dashboard", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "dashboard/update-dashboard", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/create-dashboard", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function makeDefaultAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "dashboard/mark-as-default", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteDashboardAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "dashboard/delete-dashboard", {
    headers: {
      Authorization: token,
    },
    data: {
      ...data,
    },
  });
}
