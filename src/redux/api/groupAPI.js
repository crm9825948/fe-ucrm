import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function loadGroupsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "group/get-all", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function createGroupsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "group/add", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function loadDetailsGroupsAPI(data) {
  const token = await checkTokenExpiration();
  return axios.post(BASE_URL_API + "group/get-one", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function updateGroupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.put(BASE_URL_API + "group/edit", data, {
    headers: {
      Authorization: token,
    },
  });
}

export async function deleteGroupAPI(data) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "group/delete", {
    headers: {
      Authorization: token,
    },
    data,
  });
}
