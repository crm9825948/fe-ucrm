import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

export async function getListKanbanViewAPI() {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "kanban-setting/get-all-settings",
    {},
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function saveKanbanViewSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "kanban-setting/create",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function deleteKanbanViewSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.delete(BASE_URL_API + "kanban-setting/delete", {
    data: {
      ...payload,
    },
    headers: {
      Authorization: token,
    },
  });
}

export async function getKanbanViewSettingByIdAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.post(
    BASE_URL_API + "kanban-setting/view-by-id",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}

export async function updateKanbanViewSettingAPI(payload) {
  const token = await checkTokenExpiration();
  return axios.put(
    BASE_URL_API + "kanban-setting/update",
    { ...payload },
    {
      headers: {
        Authorization: token,
      },
    }
  );
}
