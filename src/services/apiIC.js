import { refreshTokenICAPI } from "redux/api/icChatWidgetAPI";
// import { Notification } from "components/Notification/Noti";
import _ from "lodash";
const jwt = require("jsonwebtoken");

export const checkTokenExpirationIC = async () => {
  const decoded = jwt.decode(localStorage.getItem("icIntegration_accessToken"));
  const exp = decoded.exp;
  const currentTime = Math.floor(Date.now() / 1000);
  if (exp > currentTime) {
    return localStorage.getItem("icIntegration_accessToken");
  } else {
    try {
      const res = await refreshTokenICAPI({
        refresh_token: localStorage.getItem("icIntegration_refreshToken"),
      });
      localStorage.setItem(
        "icIntegration_accessToken",
        _.get(res, "data.data.access_token")
      );
      localStorage.setItem(
        "icIntegration_refreshToken",
        _.get(res, "data.data.refresh_token")
      );
      return _.get(res, "data.data.access_token");
    } catch (err) {
      // Notification("error", "Error!");
    }
  }
};

// import axios from "axios";

// const instance = axios.create({
//   baseURL: localStorage.getItem("icIntegration_link_api"),
//   headers: {
//     "Content-Type": "application/json",
//   },
// });

// instance.interceptors.request.use(
//   (config) => {
//     const token = localStorage.getItem("icIntegration_accessToken");
//     if (token) {
//       config.headers["Authorization"] = "Bearer " + token; // for Spring Boot back-end
//       // config.headers["x-access-token"] = token; // for Node.js Express back-end
//     }
//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );

// instance.interceptors.response.use(
//   (res) => {
//     return res;
//   },
//   async (err) => {
//     const originalConfig = err.config;

//     if (originalConfig.url !== "login-get-token-ic" && err.response) {
//       // Access Token was expired
//       if (err.response.status === 401 && !originalConfig._retry) {
//         originalConfig._retry = true;

//         try {
//           const rs = await instance.post("user/api/auth/refresh-token/", {
//             refreshToken: localStorage.getItem("icIntegration_refreshToken"),
//           });

//           const { accessToken } = rs.data;
//           localStorage.setItem("icIntegration_accessToken", accessToken);

//           return instance(originalConfig);
//         } catch (_error) {
//           return Promise.reject(_error);
//         }
//       }
//     }

//     return Promise.reject(err);
//   }
// );

// export default instance;
