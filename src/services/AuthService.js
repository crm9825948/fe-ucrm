import { Log, UserManager } from "oidc-client";
import { oidcClient } from "../constants/constants";
import { USE_SSO } from "constants/constants";

const AuthService =
  USE_SSO === "true"
    ? (function () {
        const settings = {
          authority: oidcClient.stsAuthority,
          client_id: oidcClient.clientId,
          redirect_uri: `${oidcClient.clientRoot}signin-callback.html`,
          silent_redirect_uri: `${oidcClient.clientRoot}silent-renew.html`,
          post_logout_redirect_uri: `${oidcClient.clientLogoutURL}`,
          response_type: "code",
          scope: oidcClient.clientScope,
          loadUserInfo: true,
          automaticSilentRenew: true,
          filterProtocolClaims: true,
          monitorSession: true,
          accessTokenExpiringNotificationTime: 180,
        };
        var userManager = new UserManager(settings);
        Log.logger = console;
        Log.level = Log.INFO;

        function getUser() {
          var user = userManager.getUser();
          if (!user) {
            return userManager.signinRedirectCallback();
          }
          return user;
        }
        function login() {
          return userManager.signinRedirect();
        }
        function renewToken() {
          return userManager.signinSilent();
        }
        function logout() {
          let paramLogout = {
            id_token_hint: localStorage.getItem("setting_idToken"),
          };
          return userManager.signoutRedirect(paramLogout);
        }
        function checkSession() {
          return userManager.querySessionStatus();
        }
        function eventSignOut(event) {
          return userManager.events.addUserSignedOut(() => {
            event();
          });
        }
        function eventAddAccessTokenExpiring() {
          return userManager.events.addAccessTokenExpiring(() => {});
        }

        function eventAddAccessTokenExpired() {
          return userManager.events.addAccessTokenExpired(() => {
            console.log("AT expired event success");
            //v2.1 update
            setTimeout(() => {
              userManager.signinSilent();
            }, 1000);
          });
        }

        function eventAddSilentRenewError() {
          return userManager.events.addSilentRenewError(() => {
            //v2.1 update
            setTimeout(() => {
              userManager.signinSilent();
            }, 1000);
          });
        }

        function eventAddUserLoaded() {
          return userManager.events.addUserLoaded(() => {
            userManager.getUser().then((user) => {
              window.localStorage.setItem(
                "setting_accessToken",
                user.access_token
              );
              window.localStorage.setItem("setting_idToken", user.id_token);
              //v2.1 update
              window.localStorage.setItem("setting_oidc", JSON.stringify(user));
            });
          });
        }

        return {
          getUser: getUser,
          login: login,
          renewToken: renewToken,
          logout: logout,
          checkSession: checkSession,
          eventSignOut: eventSignOut,
          eventAddAccessTokenExpiring: eventAddAccessTokenExpiring,
          eventAddAccessTokenExpired: eventAddAccessTokenExpired,
          eventAddUserLoaded: eventAddUserLoaded,
          eventAddSilentRenewError: eventAddSilentRenewError,
        };
      })()
    : "";

export default AuthService;
