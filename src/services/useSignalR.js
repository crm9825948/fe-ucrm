import {
  HubConnectionBuilder,
  LogLevel,
  HttpTransportType,
} from "@microsoft/signalr";
import moment from "moment";
import { useDispatch } from "react-redux";
import _ from "lodash";

import {
  resgisterSignalrIC,
  registerConnectionIntervalIC,
  $newMessage,
} from "redux/slices/icChatWidget";

const initHubConnection = () => {
  return new HubConnectionBuilder()
    .withUrl(
      localStorage.getItem("icIntegration_link_api") +
        "notification/notificationHub",
      {
        transport: HttpTransportType.WebSockets,
        SkipNegotiation: true,
      }
    )
    .withAutomaticReconnect({
      nextRetryDelayInMilliseconds: (retryContext) => {
        if (retryContext.elapsedMilliseconds < 60000) {
          // If we've been reconnecting for less than 60 seconds so far,
          // wait between 0 and 10 seconds before the next reconnect attempt.
          return 8000;
        } else {
          // If we've been reconnecting for more than 60 seconds so far, stop reconnecting.
          return 10000;
        }
      },
    })
    .configureLogging(LogLevel.Information)
    .build();
};

let connect = initHubConnection();

export const stopSignalR = () => {
  if (connect) {
    console.log(
      `[${moment(new Date())}] Stop Connection:`,
      localStorage.getItem("baseic_current_connectionID"),
      `=> ${connect.state}`
    );
    connect.stop();
    connect = null;
  }
};

const useSignalR = () => {
  const dispatch = useDispatch();

  const startSignalR = async () => {
    try {
      if (
        !connect &&
        JSON.parse(localStorage.getItem("icIntegration_useIC")) === true
      ) {
        const initConnection = initHubConnection();
        connect = initConnection;
      }
      if (
        connect &&
        JSON.parse(localStorage.getItem("icIntegration_useIC")) === true
      ) {
        await connect
          .start()
          .then(() => {
            registerOnSever(connect);
            receiveMessage(connect);
            onReconnectedEvent(connect);
          })
          .catch((error) => console.log("Could not connect SignalR", error));
      } else {
        console.log("Can not initial signalR connection");
      }
    } catch (error) {
      console.log(error);
      setTimeout(startSignalR, 5000);
    }
  };

  const onReconnectedEvent = (connect) => {
    connect.onreconnected(async () => {
      console.log("[on-reconnected]");
      reconnectOnSever(connect);
    });
  };

  const reconnectSignalR = async () => {
    try {
      reconnectOnSever(connect);
    } catch (error) {
      console.log(error);
      setTimeout(reconnectSignalR, 5000);
    }
  };

  const registerConnection = (param) => {
    dispatch(resgisterSignalrIC(param));
  };

  const registerConnectionInterval = (param) => {
    dispatch(registerConnectionIntervalIC(param));
  };

  const registerOnSever = (connect) => {
    const username = localStorage.getItem("icIntegration_username");
    const tenant = localStorage.getItem("icIntegration_tenantID");
    if (username && tenant) {
      localStorage.setItem(
        "icIntegration_current_connectionID",
        connect?.connection?.connectionId
      );
      console.log(
        `[${moment(new Date())}] BaseIC-Connect Information:`,
        connect?.connection?.connectionId
      );
      const body = {
        username: username,
        connection_id: connect?.connection?.connectionId,
        tenant_id: tenant,
      };
      registerConnection(body);
    }
  };

  const reconnectOnSever = (connect) => {
    const username = localStorage.getItem("icIntegration_username");
    const tenant = localStorage.getItem("icIntegration_tenantID");
    if (username && tenant) {
      localStorage.setItem(
        "icIntegration_current_connectionID",
        connect?.connection?.connectionId
      );
      console.log(
        `[${moment(new Date())}] BaseIC-Reconnect Information:`,
        connect?.connection?.connectionId
      );
      const body = {
        username: username,
        connection_id: connect?.connection?.connectionId,
        tenant_id: tenant,
      };
      registerConnectionInterval(body);
    }
  };

  const receiveMessage = (connect) => {
    connect.on("PushNewMessage", (message) => {
      const parseJson = JSON.parse(message);
      dispatch($newMessage(_.get(parseJson, "data", {})));
      console.log("PushNewMessage", parseJson);
    });
    connect.on("PushUnlockConversation", (message) => {
      let parseJson = JSON.parse(message);
      const result = {
        ..._.get(parseJson, "data.log_activity", {}),
        is_botprogress: false,
      };
      dispatch($newMessage(result));
      console.log("PushUnlockConversation", parseJson);
    });
    connect.on("PushNotifyMessageWarning", (message) => {
      const parseJson = JSON.parse(message);
      dispatch($newMessage(_.get(parseJson, "data", {})));
      console.log("PushNotifyMessageWarning", parseJson);
    });
  };

  return { startSignalR, reconnectSignalR };
};

export default useSignalR;
