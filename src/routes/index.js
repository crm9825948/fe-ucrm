import { lazy, Suspense } from "react";
import { Navigate, useRoutes } from "react-router-dom";
import LoadingScreen from "../components/LoadingScreen";
import AuthGuard from "../guards/AuthGuard";
import GuestGuard from "../guards/GuestGuard";
import Layout from "../layout/layout";
// import RoleBaseGuard from "components/roleBaseGuard";
import TenantsGuard from "components/tenantsGuard";
import ComponentLoader from "util/componentLoader";
import PageTestForm from "pages/FormSetting/PageTestForm";
import Package from "pages/Package/package";
import PackageCreate from "pages/PackageCreate/packageCreate";
import PackageUpdate from "pages/PackageUpdate/packageUpdate";
import CheckRoleQMView from "components/CheckRoleQMView";
import { useSelector } from "react-redux";

const Loadable = (Component) => (props) => {
  return (
    <Suspense fallback={<LoadingScreen />}>
      <Component {...props} />
    </Suspense>
  );
};

export default function Router() {
  const { checkPermissionQMView } = useSelector(
    (state) => state.authenticatedReducer
  );

  return useRoutes([
    {
      path: "preview",
      element: <PreviewPage />,
    },
    {
      path: "test-form",
      element: <PageTestForm />,
    },
    {
      path: "internet-form/:tenantID/:configID",
      element: <FormLink />,
    },
    {
      path: "internet-form/:tenantID/:configID/:defaultParams",
      element: <FormLink />,
    },
    {
      path: "internet-form-crm",
      element: <FormEmbed />,
    },
    {
      path: "version",
      element: (
        // <GuestGuard>
        <Version />
        // </GuestGuard>
      ),
    },
    {
      path: "policies/privacy-policy",
      element: <Policy />,
    },
    {
      path: "login",
      element: (
        <GuestGuard>
          <Login />
        </GuestGuard>
      ),
    },
    {
      path: "sign-in",
      element: (
        <GuestGuard>
          <Login />
        </GuestGuard>
      ),
    },
    {
      path: "forgot-password",
      element: (
        <GuestGuard>
          <ForgotPassword />
        </GuestGuard>
      ),
    },
    {
      path: "reset-password",
      element: (
        <GuestGuard>
          <ForgotPassword />
        </GuestGuard>
      ),
    },
    {
      path: "404",
      element: (
        <HttpCode
          code={404}
          content={`The page doesn't exist!`}
          decs={`
      Sorry, the page you were looking for could not be found.`}
        />
      ),
    },
    {
      path: "500",
      element: (
        <HttpCode
          code={500}
          content={`The page isn't working
          `}
          decs={`
      
The page is currently unable to handle this request.`}
        />
      ),
    },
    // {
    //   path: "mfa",
    //   element: <MFA />,
    // },
    {
      path: "/",
      element: (
        <AuthGuard>
          <Layout />
        </AuthGuard>
      ),
      children: [
        {
          path: "call-view-settings",
          element: <CallView />,
        },
        {
          path: "quality-management/:customViewId",
          element: (
            <CheckRoleQMView
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              permission={checkPermissionQMView}
            >
              <QualityManagement />
            </CheckRoleQMView>
          ),
        },
        {
          path: "tenants",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <Tenants />
            </TenantsGuard>
          ),
        },
        {
          path: "permission",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <Permission />
            </TenantsGuard>
          ),
        },
        {
          path: "audit-log",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <AuditLog />
            </TenantsGuard>
          ),
        },
        {
          path: "package",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <Package />
            </TenantsGuard>
          ),
        },
        {
          path: "package/:packageId",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <PackageUpdate />
            </TenantsGuard>
          ),
        },
        {
          path: "add-new-package",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <PackageCreate />
            </TenantsGuard>
          ),
        },
        {
          path: ":tenantID/search-management",
          element: (
            <TenantsGuard
              isAdmin={true}
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
            >
              <SearchManagement />
            </TenantsGuard>
          ),
        },
        { path: "/", element: <Navigate to="/dashboard" replace /> },
        {
          path: "dashboard",
          element: <Dashboard />,
        },
        {
          path: "report",
          element: <Report />,
        },
        {
          path: "calendar",
          element: <Calendar />,
        },
        {
          path: "calendar-detail",
          element: <CalendarDetail />,
        },
        {
          path: "newObject/:objectId/:customViewId",
          element: <NewObject />,
        },
        {
          path: "objects/:objectId/:customViewId",
          element: <NewObject />,
        },
        {
          path: "knowledge-base-enhancement/:objectId",
          element: <EnhancementView />,
        },
        {
          path: "knowledge-base-enhancement/details/:objectId/:recordID",
          element: <EnhancementDetail />,
        },
        {
          path: "knowledge-base-enhancement/:objectId/category/:categoryId",
          element: <EnhancementDetail />,
        },
        {
          path: "knowledge-base-enhancement/:objectId/search",
          element: <EnhancementSearch />,
        },
        {
          path: "knowledge-base-enhancement/create-article/:objectId",
          element: <Article />,
        },
        {
          path: "knowledge-base-enhancement/edit-article/:objectId/:recordID",
          element: <Article />,
        },
        {
          path: "list-view-with-details/objects/:objectId/:customViewId",
          element: <ListViewWithDetail />,
        },
        {
          path: "custom-table",
          element: <CustomTable />,
        },
        {
          path: "test-grid",
          element: <ReactGrid />,
        },
        // {
        //   path: "objects/new-list-view/:objectId/:customViewId",
        //   element: <NewListView />,
        // },
        {
          path: "settings",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="setting"
            >
              <Setting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "system-log",
          element: <SystemLog />,
        },
        {
          path: "objects-management",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="object_management"
            >
              <ObjectsManagement />
            </RoleBaseGuard>
          ),
        },
        {
          path: "fields-management",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="object_layout"
            >
              <FieldsManagement />
            </RoleBaseGuard>
          ),
        },
        {
          path: "duplicate-rules",
          element: (
            // <RoleBaseGuard
            //   code={403}
            //   content={`Access to the page was denied`}
            //   decs={`You don’t have authorization to View this page.`}
            //   setting="object_layout"
            // >
            <DuplicateRules />
            // </RoleBaseGuard>
          ),
        },
        {
          path: "profiles",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="profile"
            >
              <Profiles />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "related-objects",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="related_object"
            >
              <RelatedObject />
            </RoleBaseGuard>
          ),
        },
        {
          path: "new-related-objects",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="related_object"
            >
              <NewRelatedObject />
            </RoleBaseGuard>
          ),
        },
        {
          path: "edit-profile/:recordID",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="profile"
            >
              <EditProfile />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "roles",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="role"
            >
              <Roles />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "users",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="user"
            >
              <Users />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "new-user",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="new-user"
            >
              <NewUser />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "groups",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="group"
            >
              <Groups />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "report-details/:objectID/:recordID",
          element: <ReportDetails />,
        },
        {
          path: "custom-report/:reportID",
          element: <CustomReport />,
        },
        {
          path: "sharing",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sharing"
            >
              <Sharing />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "criteria-sharing",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="criteria_sharing"
            >
              <CriteriaSharing />
            </RoleBaseGuard>
          ),
        },
        {
          path: "picklist-dependency",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="picklist_dependency"
            >
              <Picklist />
            </RoleBaseGuard>
          ),
        },
        {
          path: "workflows",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="workflow"
            >
              <Workflows />
            </RoleBaseGuard>
          ),
        },
        {
          path: "edit-workflow/:recordID",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="workflow"
              isEdit={true}
            >
              <EditWorkflows />
            </RoleBaseGuard>
          ),
        },
        {
          path: "email-outgoing",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="outbox"
            >
              <EmailOutgoing />
            </RoleBaseGuard>
          ),
        },
        {
          path: "email-incoming",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="inbox"
            >
              <EmailIncoming />
            </RoleBaseGuard>
          ),
        },
        {
          path: "edit-incoming-rule/:recordID/:objectID",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="inbox"
            >
              <RuleIncoming />
            </RoleBaseGuard>
          ),
        },
        {
          path: "sms-outgoing",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sms_outgoing"
            >
              <SMSOutgoing />
            </RoleBaseGuard>
          ),
        },
        {
          path: "kanban-view-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="kanban_view"
            >
              <KanbanViewSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "kanban-view/:object_id/:customViewId",
          element: <KanbanView />,
        },
        {
          path: "brand-name",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="brand_and_color"
            >
              <BrandName />
            </RoleBaseGuard>
          ),
        },
        {
          path: "assignment-rule",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="assignment_rules"
            >
              <AssignmentRule />
            </RoleBaseGuard>
          ),
        },
        {
          path: "view-logs",
          element: <ViewLogs />,
        },
        {
          path: "profile-setting",
          element: <ProfileSetting />,
        },
        {
          path: "consolidated-view-settings",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="consolidate_view"
            >
              <ConsolidatedViewSettings />
            </RoleBaseGuard>
          ),
        },
        {
          path: "v3-consolidated-view-settings",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don't have authorization to View this page.`}
              setting="consolidate_view"
            >
              <V3ConsolidatedViewSettings />
            </RoleBaseGuard>
          ),
        },
        {
          path: "email-template",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="template_email"
            >
              <EmailTemplate />
            </RoleBaseGuard>
          ),
        },
        {
          path: "consolidated-view/:objectId/:recordID",
          element: <ConsolidatedView />,
        },
        {
          path: "dynamic-button",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="dynamic_button"
            >
              <DynamicButton />
            </RoleBaseGuard>
          ),
        },
        {
          path: "edit-dynamic-button/:recordID",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="dynamic_button"
            >
              <EditDynamicButton />{" "}
            </RoleBaseGuard>
          ),
        },

        {
          path: "export-history",
          element: <ExportHistory />,
        },
        {
          path: "sms-template",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sms_template"
            >
              <SMSTemplate />
            </RoleBaseGuard>
          ),
        },
        {
          path: "knowledge-base-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="knowledge_view"
            >
              <KnowledgeBaseSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "knowledge-base-enhancement-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="knowledge_base_enhancement"
            >
              <EnhancementSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "knowledge-base-view",
          element: <KnowledgeBaseView />,
        },
        {
          path: "articles",
          element: <Articles />,
        },
        {
          path: "notifications",
          element: <Notifications />,
        },
        {
          path: "setting-datetime",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="date_time_setting"
            >
              <SettingDateTime />
            </RoleBaseGuard>
          ),
        },
        {
          path: "sla-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sla_setting"
            >
              <SLASetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "add-sla-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sla_setting"
            >
              <DetailsSLASetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "edit-sla-setting/:recordID",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="sla_setting"
            >
              <DetailsSLASetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "oauth-client-ids",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="oauth2"
            >
              <GoogleIntegration />
            </RoleBaseGuard>
          ),
        },
        {
          path: "active-campaign",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="campaign"
            >
              <ActiveCampaign />
            </RoleBaseGuard>
          ),
        },
        {
          path: "setting-campaign/:action/:type",
          element: <SettingCampaign />,
        },
        {
          path: "edit-campaign/:recordID",
          element: <SettingCampaign />,
        },
        {
          path: "call-center/:phoneNumber",
          element: <CreateRecord />,
        },
        // {
        //   path: "popup-list-consolidated-view",
        //   element: <ListRecord />,
        // },
        {
          path: "article-detail",
          element: <ArticleDetail />,
        },
        {
          path: "popup/:objectID/:fieldID/*",
          element: <URLPopup />,
        },
        {
          path: "forms",
          element: <Forms />,
        },
        {
          path: "highlight-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="highlight_setting"
            >
              <HighlightSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "ldap",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="ldap"
            >
              <Ldap />{" "}
            </RoleBaseGuard>
          ),
        },
        {
          path: "recycle-bin/:objectId",
          element: <RecycleBin />,
        },
        {
          path: "logs-user",
          element: <LogsUser />,
        },
        {
          path: "create-template-record",
          element: <CreateTemplateRecord />,
        },
        // {
        //   path: "new-objects/:objectId/:customViewId",
        //   element: <NewListView />,
        // },
        {
          path: "profile-permission/:profileID",
          element: <PermissionUser />,
        },
        {
          path: "close",
          element: <CloseTab />,
        },
        { path: "form-setting", element: <FormSetting /> },
        { path: "form-setting/:objectId/detail", element: <FormDetail /> },
        { path: "monitoring", element: <Monitoring /> },
        {
          path: "form-setting/:objectId/:configId/detail",
          element: <FormDetail />,
        },
        {
          path: "test-new-editor",
          element: <TestNewEditor />,
        },
        {
          path: "oauth-outlook",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="o365_integration"
            >
              <Outlook />
            </RoleBaseGuard>
          ),
        },
        {
          path: "agents-monitor",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="agent_monitor"
            >
              <AgentsMonitor />
            </RoleBaseGuard>
          ),
        },
        {
          path: "cti-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="cti_setting"
            >
              <CTISetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "finesse-integration",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="finesse_setting"
            >
              <FinesseIntegration />
            </RoleBaseGuard>
          ),
        },
        {
          path: "url-popup-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="url_popup_setting"
            >
              <URLPopupSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "voice-biometric",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="voice_biometric"
            >
              <VoiceBiometric />
            </RoleBaseGuard>
          ),
        },
        {
          path: "voice-bot-setting",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="voice_bot"
            >
              <VoicebotSetting />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-check",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_check"
            >
              <ExposeAPICheck />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-create",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_create"
            >
              <ExposeAPICreate />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-update",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_update"
            >
              <ExposeAPIUpdate />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-view",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_view_file"
            >
              <ExposeAPIView />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-delete",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_delete_file"
            >
              <ExposeAPIDelete />
            </RoleBaseGuard>
          ),
        },
        {
          path: "expose-api-upload",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="expose_api_upload_file"
            >
              <ExposeAPIUpload />
            </RoleBaseGuard>
          ),
        },
        {
          path: "external-app-integration",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="external_app_integration"
            >
              <ExternalAppIntegration />
            </RoleBaseGuard>
          ),
        },
        {
          path: "ic-integration",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="ic_integration"
            >
              <ICIntegration />
            </RoleBaseGuard>
          ),
        },
        {
          path: "approval-processes",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="approval"
            >
              <ApprovalProcesses />
            </RoleBaseGuard>
          ),
        },
        {
          path: "interaction",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="interaction_setting"
            >
              <Interaction />
            </RoleBaseGuard>
          ),
        },
        {
          path: "quality-management-settings",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don’t have authorization to View this page.`}
              setting="quality_management"
            >
              <QualityManagementSettings />
            </RoleBaseGuard>
          ),
        },
        {
          path: "template-file",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don't have authorization to View this page.`}
              setting="template_file"
            >
              <TemplateFile />
            </RoleBaseGuard>
          ),
        },
        {
          path: "object-standard",
          element: (
            <RoleBaseGuard
              code={403}
              content={`Access to the page was denied`}
              decs={`You don't have authorization to View this page.`}
              setting="object_standard"
            >
              <ObjectStandard />
            </RoleBaseGuard>
          ),
        },
        {
          path: "media/:tenantID/:objectID/*",
          element: <Media />,
        },
      ],
    },
    { path: "*", element: <Navigate to="/404" replace /> },
  ]);
}

//Import component here
const Login = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Login")))
);
const Policy = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Login/Policy")))
);

const Dashboard = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Dashboard/dashboard")))
);
const Report = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Report/report")))
);
const Setting = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Setting/setting")))
);
const ObjectsManagement = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ObjectsManagement/objectsManagement"))
  )
);
const FieldsManagement = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/FieldsManagement/fieldsManagement"))
  )
);
const RelatedObject = Loadable(
  lazy(() => ComponentLoader(() => import("pages/RelatedObject/relatedObject")))
);
const Profiles = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Profiles/profiles")))
);
const EditProfile = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Profiles/EditProfile")))
);
const Roles = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Roles/roles")))
);
const Users = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Users/users")))
);
const Groups = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Groups/groups")))
);
const Version = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Version/version")))
);
const ReportDetails = Loadable(
  lazy(() => ComponentLoader(() => import("pages/ReportDetails/reportDetails")))
);
const Sharing = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Sharing/sharing")))
);
const CriteriaSharing = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/CriteriaSharing/criteriaSharing"))
  )
);
const Picklist = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/PicklistDependency/picklistDependency"))
  )
);
const Workflows = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Workflows/workflows")))
);
const EditWorkflows = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Workflows/editWorkflow")))
);
const ConsolidatedViewSettings = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/consolidatedViewSettings")
    )
  )
);
const V3ConsolidatedViewSettings = Loadable(
  lazy(() => ComponentLoader(() => import("pages/V3ConsolidatedViewSettings")))
);
const ConsolidatedView = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ConsolidatedView/consolidatedView"))
  )
);
const DynamicButton = Loadable(
  lazy(() => ComponentLoader(() => import("pages/DynamicButton/dynamicButton")))
);
const EditDynamicButton = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/DynamicButton/editDynamicButton"))
  )
);
const ExportHistory = Loadable(
  lazy(() => ComponentLoader(() => import("pages/ExportHistory/exportHistory")))
);
const Notifications = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Notification/notification")))
);
const HttpCode = Loadable(
  lazy(() => ComponentLoader(() => import("components/httpCodePage")))
);
const SettingDateTime = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/SettingDateTime/settingDateTime"))
  )
);
const ViewLogs = Loadable(
  lazy(() => ComponentLoader(() => import("pages/ViewLogs/viewLogs")))
);
const SLASetting = Loadable(
  lazy(() => ComponentLoader(() => import("pages/SLASetting/slaSetting")))
);
const DetailsSLASetting = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/SLASetting/detailsSLASetting"))
  )
);
const ActiveCampaign = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Campaign/activeCampaign")))
);
const SettingCampaign = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Campaign/settingCampaign")))
);
const CreateRecord = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/PopupConsolidatedView/createRecord"))
  )
);
// const ListRecord = Loadable(
//   lazy(() =>
//     ComponentLoader(() => import("pages/PopupConsolidatedView/listRecord"))
//   )
// );
const URLPopup = Loadable(
  lazy(() => ComponentLoader(() => import("pages/URLPopup/urlPopup")))
);
const HighlightSetting = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/HighlightSetting/highlightSetting"))
  )
);
const Ldap = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Ldap/ldap")))
);
const LogsUser = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Users/LogsUser")))
);
const PermissionUser = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Users/Permission")))
);
const Outlook = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Outlook/outlook")))
);
const Calendar = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Calendar/Calendar")))
);
const CalendarDetail = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Calendar/calendarDetail")))
);
const AssignmentRule = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/AssignmentRule/assignmentRule"))
  )
);
const NewObject = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Objects/newObject")))
);
const CustomTable = Loadable(
  lazy(() =>
    ComponentLoader(() => import("components/CustomTable/CustomTable"))
  )
);
const NewUser = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Users/NewUser")))
);
const KanbanView = Loadable(
  lazy(() => ComponentLoader(() => import("pages/KanbanView/KanbanView")))
);
const KanbanViewSetting = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/KanbanViewSettting/KanbanViewSetting"))
  )
);
const NewRelatedObject = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/RelatedObject/newRelatedObject"))
  )
);
const BrandName = Loadable(
  lazy(() => ComponentLoader(() => import("pages/BrandName/BrandName")))
);
const EmailIncoming = Loadable(
  lazy(() => ComponentLoader(() => import("pages/EmailIncoming/EmailIncoming")))
);
const EmailOutgoing = Loadable(
  lazy(() => ComponentLoader(() => import("pages/EmailOutgoing/EmailOutgoing")))
);
const EmailTemplate = Loadable(
  lazy(() => ComponentLoader(() => import("pages/EmailTemplate/EmailTemplate")))
);
const KnowledgeBaseSetting = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/KnowledgeBaseSetting/KnowledgeBaseSetting")
    )
  )
);
const ProfileSetting = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ProfileSetting/ProfileSetting"))
  )
);
const RuleIncoming = Loadable(
  lazy(() => ComponentLoader(() => import("pages/RuleIncoming/ruleIncoming")))
);
const SMSOutgoing = Loadable(
  lazy(() => ComponentLoader(() => import("pages/SMSOutgoing/SMSOutgoing")))
);
const SMSTemplate = Loadable(
  lazy(() => ComponentLoader(() => import("pages/SMSTemplate/SMSTemplate")))
);
const KnowledgeBaseView = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/KnowledgeBaseView/KnowledgeBaseView"))
  )
);
const Articles = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Articles/Articles")))
);
const GoogleIntegration = Loadable(
  lazy(() => ComponentLoader(() => import("pages/GoogleIntegration")))
);
const Tenants = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Tenants/tenants")))
);
const ArticleDetail = Loadable(
  lazy(() => ComponentLoader(() => import("pages/ArticleDetail/ArticleDetail")))
);
const Forms = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Forms/forms")))
);
const ForgotPassword = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ForgotPassword/forgotPassword"))
  )
);
const Permission = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Permission/permission")))
);
const AuditLog = Loadable(
  lazy(() => ComponentLoader(() => import("pages/AuditLog/auditLog")))
);
const ListViewWithDetail = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ListViewWithDetail/listViewWithDetail"))
  )
);
const RecycleBin = Loadable(
  lazy(() => ComponentLoader(() => import("pages/RecycleBin/RecycleBin")))
);
const DuplicateRules = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/DuplicateRules/duplicateRules"))
  )
);
const SystemLog = Loadable(
  lazy(() => ComponentLoader(() => import("pages/LogSystem/logSystem")))
);
const ReactGrid = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Dashboard/reactGrid")))
);
const RoleBaseGuard = Loadable(
  lazy(() => ComponentLoader(() => import("components/roleBaseGuard")))
);
const CloseTab = Loadable(
  lazy(() => ComponentLoader(() => import("pages/CloseTab/closeTab")))
);
const EnhancementSetting = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/EnhancementSetting/EnhancementSetting"))
  )
);

const EnhancementView = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/EnhancementView/EnhancementView"))
  )
);
const EnhancementDetail = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/EnhancementView/EnhancementDetail"))
  )
);
const EnhancementSearch = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/EnhancementView/EnhancementSearch"))
  )
);
const Article = Loadable(
  lazy(() => ComponentLoader(() => import("pages/EnhancementView/Article")))
);
const CreateTemplateRecord = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/CreateTemplateRecord/createTemplateRecord")
    )
  )
);
const FormSetting = Loadable(
  lazy(() => ComponentLoader(() => import("pages/FormSetting/FormSetting")))
);
const FormDetail = Loadable(
  lazy(() => ComponentLoader(() => import("pages/FormSetting/FormDetail")))
);
const AgentsMonitor = Loadable(
  lazy(() => ComponentLoader(() => import("pages/AgentsMonitor/agentsMonitor")))
);
const CTISetting = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/CTISetting/CTISetting")
    )
  )
);
const FinesseIntegration = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/CallCenter/callCenter")
    )
  )
);
const URLPopupSetting = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/PopupSetting/popupSetting")
    )
  )
);
const VoiceBiometric = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/VoiceBiometric/voiceBiometric")
    )
  )
);
const VoicebotSetting = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/VoiceBotSetting/voiceBotSetting")
    )
  )
);
const ExposeAPICheck = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPI/exposeAPI")
    )
  )
);
const ExposeAPICreate = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPICreate/exposeAPICreate")
    )
  )
);
const ExposeAPIUpdate = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPIUpdate/exposeAPIUpdate")
    )
  )
);
const ExposeAPIView = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPIView/exposeAPIView")
    )
  )
);
const ExposeAPIDelete = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPIDelete/exposeAPIDelete")
    )
  )
);
const ExposeAPIUpload = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ExposeAPIUpload/exposeAPIUpload")
    )
  )
);
const ExternalAppIntegration = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/CoreSetting/coreSetting")
    )
  )
);
const ICIntegration = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/ConsolidatedViewSettings/ICIntegration/ICIntegration")
    )
  )
);
const ApprovalProcesses = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/ApprovalProcesses/approvalProcesses"))
  )
);
const SearchManagement = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/SeachManagement/searchManagement"))
  )
);

const Interaction = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Interaction/interaction")))
);
const PreviewPage = Loadable(
  lazy(() => ComponentLoader(() => import("pages/FormSetting/PreviewPage")))
);
const FormLink = Loadable(
  lazy(() => ComponentLoader(() => import("pages/FormSetting/FormLink")))
);
const FormEmbed = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/FormSetting/InternetForm/FormEmbed"))
  )
);
const TemplateFile = Loadable(
  lazy(() => ComponentLoader(() => import("pages/TemplateFile")))
);
const QualityManagement = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/QualityManagement/QualityManagement"))
  )
);

const QualityManagementSettings = Loadable(
  lazy(() =>
    ComponentLoader(() =>
      import("pages/QualityManagementSettings/QualityManagementSettings")
    )
  )
);

const ObjectStandard = Loadable(
  lazy(() => ComponentLoader(() => import("pages/ObjectStandard")))
);
const Monitoring = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Monitoring/Monitoring")))
);
const CustomReport = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/Report/CusomReport/CustomReport"))
  )
);
const TestNewEditor = Loadable(
  lazy(() => ComponentLoader(() => import("pages/TestNewEditor")))
);
const Media = Loadable(
  lazy(() => ComponentLoader(() => import("pages/Media")))
);

const CallView = Loadable(
  lazy(() =>
    ComponentLoader(() => import("pages/CallViewSettings/CallViewSettings"))
  )
);
