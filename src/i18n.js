import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import en_source from "translations/en.json";
import vn_source from "translations/vi.json";

const resources = {
  en: {
    translation: en_source,
  },
  vi: {
    translation: vn_source,
  },
};

i18n.use(initReactI18next).init({
  fallbackLng: "en",
  debug: false,
  resources,
  lng: localStorage.getItem("i18nextLng"),
  interpolation: {
    escapeValue: false,
  },
});

export default i18n;
