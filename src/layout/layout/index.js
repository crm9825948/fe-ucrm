import {
  Button,
  Col,
  Layout,
  Menu,
  Modal,
  Popover,
  Row,
  Select,
  Dropdown,
} from "antd";
import { DownOutlined } from "@ant-design/icons";
import _ from "lodash";
import en from "assets/icons/common/en.png";
import vi from "assets/icons/common/vi.png";
import { NotificationInfo } from "components/Notification/Noti";
import Numpad from "components/Numpad/numpad";
import NotificationSound from "assets/sound-noti.wav";
import { logoutAPI } from "redux/api/authenticatedAPIs";
import { THEME_COLOR } from "configs/color";

import {
  BE_URL,
  FE_URL,
  REACT_APP_URL_TRACKING_LOGIN,
  USE_SSO,
  // REACT_APP_URL_TRACKING_LOGIN,
  WS_URL,
} from "constants/constants";
import moment from "moment";
import PopupNoti from "pages/Notification/popup";
import ModalChangePass from "pages/Users/ModalChangePass";
import { useCallback, useEffect, useState, useRef } from "react";
import AvatarEditor from "react-avatar-editor";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Outlet, useParams } from "react-router";
import { useLocation, useNavigate } from "react-router-dom";
import useWebSocket from "react-use-websocket";
import { getDefaultBrandName } from "redux/slices/brandName";
import {
  loadCategory,
  loadObjectCategory,
} from "redux/slices/objectsManagement";
import { loadUserDetail, loadUserDetailSuccess } from "redux/slices/user";
import AuthService from "services/AuthService";
import styled, { useTheme } from "styled-components";
// import settingTheme from "../../assets/icons/layout/theme-setting.png";
import collapsedButton from "../../assets/icons/sidebar/colspase.png";
import signOutIMG from "../../assets/icons/header/signout.png";
import avatarImg from "../../assets/images/header/avatar.png";
// import HeartImg from "../../assets/icons/layout/dashicons_heart.png";
import {
  logout,
  initialize,
  setCheckPermissionQMView,
} from "../../redux/slices/authenticated";
import ICIntegrationIcon from "assets/icons/header/ICIntegration.svg";

import { getInfoMailChimp } from "redux/slices/campaign";
import {
  changeStatusCRM,
  getUserStatusResult,
  changeStatusCRMResult,
  unMountLogout,
} from "redux/slices/agentMonitor";
import HeaderObjectView from "components/Header/HeaderObjectView";
import ModalVersion from "components/Modal/ModalVersion";
import { listStatusCRM, listStatusIC } from "util/staticData";
import { getAllSettingKnowledgeBase } from "redux/slices/knowledgeEnhancement";
import { updateStatusIC } from "redux/slices/icChatWidget";
import useSignalR from "services/useSignalR";
import { oidcClient } from "../../constants/constants";
import { interactionConfig } from "redux/slices/consolidatedViewSettings";
import { getActiveQMView } from "redux/slices/QualityManagement";
import { loadDataReport, updateIsRun } from "redux/slices/customReport";
import { loadAllObject } from "redux/slices/tenants";

export const jwt = require("jsonwebtoken");

const { Header, Sider, Content } = Layout;
const { Option } = Select;
const { SubMenu } = Menu;

const LayoutTemplate = () => {
  const { t } = useTranslation();
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { i18n } = useTranslation();
  const { objectId, object_id, reportID } = useParams();
  const theme = useTheme();
  const { pathname } = useLocation();

  const [collapsed, setCollapsed] = useState(true);

  const [active, setActive] = useState(1);
  const [modalSignOut, setModalSignout] = useState(false);
  const [showModalDisconnect, $showModalDisconnect] = useState(false);
  // const [visible, setVisible] = useState(false);

  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { statusCRM, userStatus } = useSelector(
    (state) => state.agentMonitorReducer
  );
  const { statusIC } = useSelector((state) => state.icChatWidgetReducer);
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );
  // const { listEmail } = useSelector((state) => state.componentEmail);
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { avatar_config } = userDetail;

  // const { objectsFavourite } = useSelector((state) => state.objectsReducer);

  const { visibleList, objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );

  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const { activeQMView } = useSelector(
    (state) => state.qualityManagementReducer
  );

  const { title, checkPermissionQMView } = useSelector(
    (state) => state.authenticatedReducer
  );
  const [modalVersion, setModalVersion] = useState(false);

  useEffect(() => {
    document.title = title;
  }, [title]);

  const handleNameRoute = () => {
    if (objectId || object_id) {
      let name = "";
      // eslint-disable-next-line
      Object.entries(objectCategory).forEach(([key, value], idx) => {
        // eslint-disable-next-line
        value.map((object, index) => {
          if (object._id === objectId || object._id === object_id) {
            name = object.Name;
          }
        });
      });
      return (
        <>
          <span
            style={{ cursor: "pointer" }}
            onClick={() => {
              navigate(`/objects/${object_id || objectId}/default-view`);
            }}
          >
            {name}
          </span>
        </>
      );
    } else if (pathname.split("/")[1] === "setting-campaign") {
      if (pathname.split("/")[2] === "create") {
        return "Add new campaign";
      }
    } else if (pathname.split("/")[1] === "edit-campaign") {
      return "Edit campaign";
    } else if (pathname.split("/")[1] === "popup") {
      return "URL Popup";
    } else if (pathname.split("/")[1] === "edit-profile") {
      return "Edit profile";
    } else if (pathname.split("/")[1] === "report-details") {
      return t("reportDetails.reportDetails");
    } else if (pathname.split("/")[1] === "edit-workflow") {
      return t("workflow.editworkflow");
    } else if (pathname.split("/")[1] === "edit-dynamic-button") {
      return t("dynamicButton.editDynamic");
    } else if (pathname.split("/")[1] === "edit-sla-setting") {
      return t("slaSetting.editSLA");
    } else if (pathname.split("/")[1] === "kanban-view") {
      return "Kanban view";
    } else if (pathname.includes("package/")) {
      return "Update package";
    } else if (pathname.includes("quality-management/")) {
      return t("QualityManagement.QualityManagement");
    } else
      switch (pathname) {
        case "/dashboard":
          return "Dashboard";
        case "/report":
          return t("report.report");
        case "/settings":
          return t("settings.settings");
        case "/objects":
          return "Objects";
        case "/objects-management":
          return t("settings.objectManagement");
        case "/fields-management":
          return t("objectLayoutField.objectLayoutField");
        case "/profiles":
          return t("settings.profiles");
        case "/related-objects":
          return "Related object";
        case "/roles":
          return t("settings.role");
        case "/users":
          return t("settings.users");
        case "/groups":
          return t("settings.group");
        case "/sharing":
          return t("settings.sharing");
        case "/criteria-sharing":
          return t("settings.criteriaSharing");
        case "/picklist-dependency":
          return "Picklist dependency";
        case "/workflows":
          return t("settings.workflow");
        case "/email-outgoing":
          return "Email outgoing";
        case "/email-incoming":
          return "Email incoming";
        case "/edit-incoming-rule":
          return "Email incoming rule";
        case "/sms-outgoing":
          return "SMS Outgoing";
        case "/kanban-view-setting":
          return "Kanban view setting";

        case "/brand-name":
          return "Brand name";
        case "/view-logs":
          return "View logs";
        case "/profile-setting":
          return "Profile setting";
        case "/consolidated-view-settings":
          return "Consolidated view settings";
        case "/email-template":
          return "Email template";
        case "/dynamic-button":
          return "Dynamic button";
        case "/export-history":
          return "Export history";
        case "/sms-template":
          return "SMS template";
        case "/knowledge-base-setting":
          return "Knowledge base setting";
        case "/knowledge-base":
          return "Knowledge base";
        case "/notifications":
          return "Notifications";
        case "/tenants":
          return "Tenants";
        case "/setting-datetime":
          return t("settings.dateTimeSetting");
        case "/popup-list-consolidated-view":
          return "List record";
        case "/create-record":
          return "Create record";
        case "/active-campaign":
          return t("settings.campaignSetting");
        case "/add-new-campaign":
          return "Add new campaign";
        case "/sla-setting":
          return t("settings.slaSetting");
        case "/add-sla-setting":
          return t("slaSetting.addSLA");
        case "/highlight-setting":
          return "Highlight setting";
        case "/oauth-client-ids":
          return "Google Integration";
        case "/ldap":
          return "LDAP";
        case "/knowledge-base-view":
        case "/articles":
        case "/article-detail":
          let name = "";
          // eslint-disable-next-line
          Object.entries(objectCategory).forEach(([key, value], idx) => {
            // eslint-disable-next-line
            value.map((object, index) => {
              if (object._id === editKnowledgeBaseSetting.object_id) {
                name = object.Name;
              }
            });
          });
          return name;
        case "/audit-log":
          return "Audit log";
        case "/package":
          return "Package";
        case "/add-new-package":
          return "Add new package";
        case "/permission":
          return "Permission";
        case "/recycle-bin":
          return "Recycle bin";
        case "/calendar":
          return "My calendar";
        case "/calendar-detail":
          return "My calendar";
        case "/logs-user":
          return "Logs import";
        case "/duplicate-rules":
          return "Duplicate rules";
        case "/system-log":
          return "System log";
        case "/assignment-rule":
          return "Assignment rule";
        case "/oauth-outlook":
          return "Outlook Integration";
        case "/new-related-objects":
          return "Related object";
        case "/interaction":
          return "Interaction config";
        case "/quality-management-settings":
          return t("QualityManagement.QualityManagementSettings");

        case "/object-standard":
          return t("settings.objectStandard");
        default:
          break;
      }
  };

  useEffect(() => {
    if (
      objectCategory &&
      activeQMView &&
      typeof objectCategory === "object" &&
      !Array.isArray(objectCategory)
    ) {
      let temp = [];
      temp = _.flatMap(Object.values(objectCategory));

      let permissionQM = temp.find(
        (item) => _.get(item, "_id") === "obj_crm_qm_interaction_00001"
      );

      let permissionInteraction;
      if (interactionConfigState) {
        permissionInteraction = temp.find(
          (item) =>
            _.get(item, "_id") === _.get(interactionConfigState, "object_id")
        );
      }

      const checkPermission =
        permissionQM &&
        _.get(permissionQM, "Status", false) &&
        _.get(permissionQM, "visible", false) &&
        permissionInteraction &&
        _.get(permissionInteraction, "Status", false) &&
        _.get(permissionInteraction, "visible", false);

      dispatch(
        setCheckPermissionQMView(
          checkPermission &&
            _.get(activeQMView, "active", false) &&
            _.get(activeQMView, "active_root", false)
        )
      );
    }
  }, [objectCategory, interactionConfigState, activeQMView, dispatch]);

  const content = (
    <div style={{ width: "240px" }}>
      <CustomRow justify={"center"} style={{ width: "100%" }}>
        <Col span={12}>
          <AvatarEditor
            image={
              avatar_config && avatar_config.url && avatar_config.url.length > 0
                ? BE_URL + avatar_config.url
                : avatarImg
            }
            position={avatar_config && avatar_config.position}
            scale={avatar_config && avatar_config.scale}
            border={0}
          />
        </Col>
      </CustomRow>
      <Row
        justify={"center"}
        style={{
          width: "100%",
          marginTop: "16px",
          fontWeight: "700",
          color: "#2C2C2C",
          lineHeight: "24px",
          fontSize: "16px",
        }}
      >
        <Col span={24} style={{ textAlign: "center" }}>
          {userDetail.Last_Name} {userDetail.Middle_Name}{" "}
          {userDetail.First_Name}
        </Col>
      </Row>
      <Row
        justify={"center"}
        style={{
          width: "100%",
          fontWeight: "400",
          fontSize: "14px",
          lineHeight: "20px",
          color: "#6B6B6B",
        }}
      >
        <Col span={24} style={{ textAlign: "center" }}>
          <p> {userDetail.Email} </p>
        </Col>
      </Row>
      <Row justify={"center"} style={{ width: "100%" }}>
        <CustomCol
          span={20}
          onClick={() => {
            navigate("/profile-setting");
          }}
        >
          Manage your account
        </CustomCol>
      </Row>
      <Row justify={"center"} style={{ width: "100%" }}>
        <CustomCol
          span={20}
          style={{
            margin: "6px 0 12px 0",
          }}
          onClick={() => setShowModalChangePass(true)}
        >
          Change password
        </CustomCol>
      </Row>
      <Row justify={"center"} style={{ width: "100%" }}>
        <Col
          span={20}
          style={{ textAlign: "center", cursor: "pointer" }}
          onClick={() => {
            if (AuthService) {
              AuthService.checkSession()
                .then(() => {
                  AuthService.logout();
                  localStorage.removeItem("setting_idToken");
                })
                .catch(() => {
                  dispatch(logout());
                });
            }
            logoutAPI();
            dispatch(logout());
            dispatch(
              initialize({
                isAuthenticated: false,
                userInfo: null,
                isInitialized: true,
              })
            );

            localStorage.removeItem("setting_accessToken");
            localStorage.removeItem("setting_refreshToken");
            localStorage.removeItem("themeColor");
            localStorage.removeItem("i18nextLng");
            localStorage.removeItem("redux-root");
            localStorage.removeItem("settings");

            //IC
            localStorage.removeItem("icIntegration_accessToken");
            localStorage.removeItem("icIntegration_tenantID");
            localStorage.removeItem("icIntegration_username");
            localStorage.removeItem("icIntegration_is_administrator");
            localStorage.removeItem("icIntegration_link_api");
            localStorage.removeItem("icIntegration_refreshToken");
            localStorage.removeItem("icIntegration_current_connectionID");
            localStorage.removeItem("icIntegration_useIC");

            dispatch(loadUserDetailSuccess({}));
            dispatch(unMountLogout());
            navigate("/login");
          }}
        >
          <CustomSignOut>
            {" "}
            <img alt="Sign out" src={signOutIMG} />
            <span>Sign out</span>
          </CustomSignOut>
        </Col>
      </Row>
    </div>
  );

  const [showNoti, setShowNoti] = useState(false);
  const [showMenuNoti, setShowMenuNoti] = useState(false);

  const [dataWebSocket, setDataWebSocket] = useState({});
  const [listNoti, setListNoti] = useState([]);
  const [pageNoti, setPageNoti] = useState(0);
  const [unseenNoti, setUnseenNoti] = useState(0);
  const [hasMoreNoti, setHasMoreNoti] = useState(true);
  const [isLoadingNoti, setIsLoadingNoti] = useState(true);

  const [showStatus, $showStatus] = useState(false);
  const [showStatusIC, $showStatusIC] = useState(false);

  const [showModalChangePass, setShowModalChangePass] = useState(false);

  const audioPlayer = useRef(null);

  const _onChangeStatus = (value) => {
    $showStatus(false);
    dispatch(
      changeStatusCRM({
        crm_status: value,
      })
    );
  };

  const _onChangeStatusIC = (value) => {
    $showStatusIC(false);
    dispatch(
      updateStatusIC({
        ic_status: value,
        username: _.get(userDetail, "username_ic", ""),
      })
    );
  };

  const menuStatus = (listStatus, isIC) => (
    <Menu style={{ width: "135px", borderRadius: "2px" }}>
      {_.map(listStatus, (item) => (
        <div
          key={item.value}
          onClick={(e) => {
            e.stopPropagation();
            if (isIC) {
              _onChangeStatusIC(item.value);
            } else {
              _onChangeStatus(item.value);
            }
          }}
        >
          <CustomStatus eventKey={item.value} status={item.value}>
            {item.label}
          </CustomStatus>
        </div>
      ))}
    </Menu>
  );

  const handleShowNoti = () => {
    setShowNoti(!showNoti);
  };

  const handleShowMenuNoti = () => {
    setShowMenuNoti(!showMenuNoti);
  };

  const onMarkAllRead = () => {
    setIsLoadingNoti(true);
    setShowMenuNoti(false);
    setListNoti([]);

    sendJsonMessage({
      action: "update_notification_status",
      _id: "all",
    });

    sendJsonMessage({
      action: "get_list_notifications",
      record_per_page: 20,
      page: 0,
    });
  };

  const onOpenNotifications = () => {
    navigate("/notifications");
    setShowNoti(false);
    setShowMenuNoti(false);
  };

  const realTimeNoti = useCallback(() => {
    let tempNoti = [...listNoti];
    tempNoti.forEach((item, idx) => {
      let timeNew = moment(item.created_date).fromNow();
      tempNoti[idx].time = timeNew;
    });
    setListNoti(tempNoti);
  }, [listNoti]);

  const loadMoreNoti = () => {
    let tempPage = pageNoti + 1;
    setPageNoti(tempPage);

    setTimeout(() => {
      sendJsonMessage({
        action: "get_list_notifications",
        record_per_page: 20,
        page: tempPage,
      });
    }, 1000);
  };

  const onDisconected = () => {
    setListNoti([]);
    // localStorage.removeItem('popup-call')
    // localStorage.removeItem('outgoingCall')
  };

  const endpoint =
    WS_URL +
    "notifications/" +
    localStorage.getItem("setting_accessToken") +
    "/";

  const { sendJsonMessage } = useWebSocket(endpoint, {
    onMessage: (message) => handleMessage(message),
    onClose: () => onDisconected(),
  });

  const handleMessage = (message) => {
    const data = JSON.parse(message.data);
    setDataWebSocket(data);
  };

  const endpoint1 =
    WS_URL +
    "agent-monitor/" +
    localStorage.getItem("setting_accessToken") +
    "/";

  // eslint-disable-next-line no-unused-vars
  const { sendJsonMessage: sendJsonMessage2 } = useWebSocket(endpoint1, {
    onMessage: (message) => handleMessage2(message),
  });

  const handleMessage2 = (message) => {
    const data = JSON.parse(message.data);

    if (
      _.isEqual(_.get(data, "event", ""), "user_change_status") ||
      _.isEqual(_.get(data, "event", ""), "get_status")
    ) {
      if (_.get(userDetail, "_id", "") === _.get(data, "data.user_id")) {
        dispatch(
          changeStatusCRMResult({
            crm_status: _.get(data, "data.crm_status"),
          })
        );
      }

      ////agentMonitor
      const userIndex = _.get(userStatus, "data", []).findIndex(
        (item) =>
          _.get(item, "user_status.user_id") === _.get(data, "data.user_id")
      );

      if (userIndex !== -1) {
        let newUserStatus = [...userStatus.data];
        newUserStatus[userIndex] = {
          ...newUserStatus[userIndex],
          user_status: _.get(data, "data"),
        };

        dispatch(
          getUserStatusResult({
            ...userStatus,
            data: newUserStatus,
          })
        );
      }
    }
  };

  const onClickNoti = useCallback(
    (title, objectID, recordID, idNoti, item) => {
      setListNoti([]);

      sendJsonMessage({
        action: "update_notification_status",
        _id: idNoti,
      });

      sendJsonMessage({
        action: "get_list_notifications",
        record_per_page: 20,
        page: 0,
      });

      setShowNoti(false);

      if (title.includes("The import process of file")) {
        window.open(`${FE_URL}/view-logs`);
      } else if (
        title.includes("exported successfully") ||
        title.includes("is exporting. We will notify you when it is finished.")
      ) {
        window.open(`${FE_URL}/export-history`);
      } else if (
        title.includes(
          "Error occurs when exporting report, Please contact the Admin."
        )
      ) {
        //
      } else if (objectID === "User") {
        window.open(`${FE_URL}/logs-user`);
      } else if (objectID === "CalendarEvent") {
        if (item?.record_id) {
          localStorage.setItem("id_event", item.record_id);
          setTimeout(() => {
            window.open(`${FE_URL}/calendar-detail`);
          }, 500);
        } else {
          window.open(`${FE_URL}/404`);
        }
      } else {
        if (
          (item?.category === "email" || item?.category === "send_email") &&
          item?.notification_type === "error"
        ) {
          let regex = new RegExp(/style="(.*?)"/);
          let style = item.additional_info.match(regex);
          let body = {};
          if (style) {
            let temp = style[0].replaceAll('"', "'");
            body = JSON.parse(
              item.additional_info.replaceAll("'", '"').replace(style[0], temp)
            );
          } else {
            body = JSON.parse(item.additional_info.replaceAll("'", '"'));
          }
          let typeEmail = body.action_email ? body.action_email : "send_email";
          delete body.action_email;

          localStorage.setItem("emailError", JSON.stringify(body));
          localStorage.setItem("typeEmailError", typeEmail);
          // if (typeEmail === "send_email") {
          //   // dispatch(setShowModal(true));
          //   // dispatch(setShowEditor(false));
          //   // dispatch(selectEmail({}));
          // } else {
          //   // dispatch(setShowModal(false));
          //   // dispatch(setShowEditor(true));

          //   let selectItem = listEmail.filter(
          //     (x) => x.record_id_main === body.record_id_main
          //   )[0];
          //   if (selectItem) {
          //     localStorage.setItem(
          //       "selectedEmailError",
          //       JSON.stringify(selectItem)
          //     );

          //     // dispatch(selectEmail(selectItem));
          //   }
          // }
          if (typeEmail !== "send_email") {
            window.open(
              `${FE_URL}/consolidated-view/${objectID}/${body.record_id_main}`
            );
          } else {
            window.open(`${FE_URL}/consolidated-view/${objectID}/${recordID}`);
          }
        } else if (objectID && recordID) {
          window.open(`${FE_URL}/consolidated-view/${objectID}/${recordID}`);
        } else {
          window.open(`${FE_URL}/notifications`);
        }
      }
    },
    /*eslint-disable-next-line*/
    [navigate, sendJsonMessage, dispatch]
  );

  const renderListObject = Object.entries(
    objectCategory !== null && objectCategory
    /*eslint-disable-next-line*/
  ).map(([key, value], idx) => {
    if (visibleList[key])
      return (
        <SubmenuCustomItem collapsed={collapsed} key={key} title={key}>
          {/*eslint-disable-next-line*/}
          {value.map((item, index) => {
            if (item.visible === true && item.Status === true)
              return (
                <CustomMenuSubItem
                  className="custom-menu"
                  key={item._id}
                  style={{
                    whiteSpace: "normal",
                    wordBreak: "break-all",
                    fontSize: "16px",
                  }}
                  onClick={() => {
                    navigate(`/objects/${item._id}/default-view`);
                  }}
                >
                  {item.Name}
                </CustomMenuSubItem>
              );
          })}
        </SubmenuCustomItem>
      );
  });

  const propsSubmenu = {
    color: THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
      ? THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
          .main
      : "#20A2A2",
    hover: THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
      ? THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
          .darker
      : "#20A2A2",

    key: "/objects",
    icon: (
      <svg
        style={{
          marginTop: collapsed && "10px",
          marginLeft: "-8px",
        }}
        width="16"
        height="18"
        viewBox="0 0 24 18"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M22.8213 0.914062H8.357C7.8829 0.914062 7.49986 1.2971 7.49986 1.77121V8.09264H1.17843C0.704325 8.09264 0.321289 8.47567 0.321289 8.94978V16.2355C0.321289 16.7096 0.704325 17.0926 1.17843 17.0926H15.6427C16.1168 17.0926 16.4999 16.7096 16.4999 16.2355V9.91406H22.8213C23.2954 9.91406 23.6784 9.53103 23.6784 9.05692V1.77121C23.6784 1.2971 23.2954 0.914062 22.8213 0.914062ZM9.32129 2.73549H14.6784V8.09264H9.32129V2.73549ZM7.49986 15.2712H2.14272V9.91406H7.49986V15.2712ZM14.6784 15.2712H9.32129V9.91406H14.6784V15.2712ZM21.857 8.09264H16.4999V2.73549H21.857V8.09264Z"
          fill={
            active === "/objects"
              ? THEME_COLOR.filter(
                  (x) => x.name === defaultBrandName.theme_color
                )[0]
                ? THEME_COLOR.filter(
                    (x) => x.name === defaultBrandName.theme_color
                  )[0].main
                : theme.white
              : theme.white
          }
        />
      </svg>
    ),

    title: t("common.object"),
  };

  useEffect(() => {
    setActive(pathname);
    let userDetail = jwt.decode(localStorage.getItem("setting_accessToken"));
    if (userDetail) {
      dispatch(
        loadUserDetail({
          _id: userDetail.crm_claims
            ? JSON.parse(userDetail.crm_claims).ID
            : userDetail.ID,
        })
      );
    }

    // eslint-disable-next-line
  }, [pathname]);

  useEffect(() => {
    dispatch(loadCategory());
    dispatch(loadObjectCategory());
    dispatch(getDefaultBrandName());
    dispatch(getAllSettingKnowledgeBase());
  }, [dispatch]);

  useEffect(() => {
    if (_.get(userDetail, "tenant_id")) {
      dispatch(interactionConfig());
      dispatch(getActiveQMView());
      dispatch(
        loadAllObject({
          tenant_id: _.get(userDetail, "tenant_id"),
        })
      );
    }
  }, [dispatch, userDetail]);

  useEffect(() => {
    let realTime;
    if (listNoti.length > 0) {
      realTime = setInterval(realTimeNoti, 60000);
    }
    return () => clearInterval(realTime);
  }, [listNoti, realTimeNoti]);

  useEffect(() => {
    if (Object.keys(dataWebSocket).length > 0) {
      if (dataWebSocket.data?.list_notification) {
        let tempListNoti = [];
        if (dataWebSocket.status === "new_notifications") {
          let decs = dataWebSocket.data.list_notification[0].content;
          let objectID = dataWebSocket.data.list_notification[0].object_id;
          let recorID = dataWebSocket.data.list_notification[0].record_id;
          let ID = dataWebSocket.data.list_notification[0]._id;
          NotificationInfo(
            "New notification",
            decs,
            onClickNoti,
            objectID,
            recorID,
            ID,
            dataWebSocket.data.list_notification[0]
          );

          audioPlayer.current.play();
          if (
            reportID &&
            reportID === dataWebSocket.data.list_notification[0].record_id
          ) {
            dispatch(updateIsRun(false));
            dispatch(
              loadDataReport({
                report_id: reportID,
              })
            );
          }
        }

        dataWebSocket.data.list_notification.map((item) => {
          return tempListNoti.push(item);
        });

        tempListNoti.forEach((item, idx) => {
          tempListNoti[idx].time = moment(item.created_date).fromNow();
        });

        setListNoti((preState) => {
          let tempState = [];
          if (dataWebSocket.status === "new_notifications") {
            tempState = tempListNoti.concat(preState);
          } else {
            tempState = preState.concat(tempListNoti);
          }
          const resultListNoti = tempState.filter(
            (ele, idx, arr) =>
              arr.findIndex((noti) => noti._id === ele._id) === idx
          );

          if (
            dataWebSocket.data.list_notification.length < 20 ||
            resultListNoti.length === 100
          ) {
            setHasMoreNoti(false);
          }

          return resultListNoti;
        });

        setUnseenNoti(dataWebSocket.data.total_unseen);

        if (dataWebSocket.data.total_notifications >= 0) {
          setIsLoadingNoti(false);
        }
      }

      if (dataWebSocket.event === "MailChimp Connection Established") {
        dispatch(getInfoMailChimp());
      }

      if (_.get(dataWebSocket, "event") === "Disconnected") {
        $showModalDisconnect(true);
        dispatch(
          loadUserDetail({
            _id: _.get(userDetail, "_id"),
          })
        );
      }
    }
    // eslint-disable-next-line
  }, [dataWebSocket]);

  useEffect(() => {
    if (AuthService) {
      AuthService.eventSignOut(() => {
        setModalSignout(true);
        localStorage.removeItem("setting_accessToken");
        localStorage.removeItem("setting_refreshToken");
        localStorage.removeItem("themeColor");
        localStorage.removeItem("i18nextLng");
        localStorage.removeItem("redux-root");
        localStorage.removeItem("settings");
        dispatch(unMountLogout());
        // dispatch(logout())
        // navigate('/login')
      });
      AuthService.eventAddAccessTokenExpiring();
      AuthService.eventAddAccessTokenExpired();
      AuthService.eventAddUserLoaded();
      AuthService.eventAddSilentRenewError();

      //Cập nhật lại thông tin user vào sessionStorage
      if (window.addEventListener)
        window.addEventListener(
          "storage",
          () => {
            sessionStorage.setItem(
              `oidc.user:${oidcClient.stsAuthority}:${oidcClient.clientId}`,
              localStorage.getItem("setting_oidc")
            );
          },
          false
        );
    }
  }, [dispatch]);

  const { startSignalR } = useSignalR();

  useEffect(() => {
    async function runSignalr() {
      await startSignalR();
    }
    runSignalr();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <CustomLayout>
      {userDetail._id && USE_SSO === "true" ? (
        <iframe
          src={
            REACT_APP_URL_TRACKING_LOGIN +
            "?userName=" +
            userDetail.Email +
            "&userID=" +
            userDetail._id +
            "&baseAppName=CRM"
          }
          title={"tracking-user-login"}
          style={{ display: "none" }}
        ></iframe>
      ) : (
        ""
      )}
      <CustomSidebar
        color={
          THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
            ? THEME_COLOR.filter(
                (x) => x.name === defaultBrandName.theme_color
              )[0].main
            : "#20A2A2"
        }
        hover={
          THEME_COLOR.filter((x) => x.name === defaultBrandName.theme_color)[0]
            ? THEME_COLOR.filter(
                (x) => x.name === defaultBrandName.theme_color
              )[0].darker
            : "#20A2A2"
        }
        collapsed={collapsed}
        onCollapse={() => setCollapsed(!collapsed)}
        collapsible
      >
        {!userDetail.user_root ? (
          <>
            {" "}
            <img
              alt=""
              src={collapsedButton}
              className="button__collapse__left"
              onClick={() => setCollapsed(!collapsed)}
            />
            <img
              alt=""
              src={collapsedButton}
              className="button__collapse__right"
              onClick={() => setCollapsed(!collapsed)}
            />
            <div id="#test2" className="logo__base">
              <AvatarEditor
                style={{
                  borderRadius: defaultBrandName.media_config.borderRadius,
                }}
                position={defaultBrandName.media_config.position}
                scale={defaultBrandName.media_config.scale}
                border={defaultBrandName.media_config.border}
                width={
                  defaultBrandName.media_config.width < 70
                    ? defaultBrandName.media_config.width
                    : 70
                }
                height={
                  defaultBrandName.media_config.height < 70
                    ? defaultBrandName.media_config.height
                    : 70
                }
                image={defaultBrandName.media_url}
              />
            </div>
            <div className="menu__config">
              <div
                style={{
                  width: "85%",
                  borderTop: "1px solid #87B9BF",
                  margin: "auto",
                }}
              ></div>
              <CustomMenu
                color={
                  THEME_COLOR.filter(
                    (x) => x.name === defaultBrandName.theme_color
                  )[0]
                    ? THEME_COLOR.filter(
                        (x) => x.name === defaultBrandName.theme_color
                      )[0].main
                    : "#20A2A2"
                }
                hover={
                  THEME_COLOR.filter(
                    (x) => x.name === defaultBrandName.theme_color
                  )[0]
                    ? THEME_COLOR.filter(
                        (x) => x.name === defaultBrandName.theme_color
                      )[0].darker
                    : "#20A2A2"
                }
                defaultSelectedKeys={[`${active}`]}
                mode="inline"
                selectedKeys={active}
                triggerSubMenuAction="click"
                collapsed={collapsed}
              >
                <MenuItem
                  key="/dashboard"
                  onClick={() => {
                    navigate("/dashboard");
                  }}
                  icon={
                    <svg
                      width="20"
                      height="20"
                      viewBox="0 0 20 20"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{
                        marginLeft: "-8px",
                      }}
                    >
                      <path
                        d="M8.71401 0.144531H0.57115C0.335435 0.144531 0.142578 0.337388 0.142578 0.573103V8.71596C0.142578 8.95167 0.335435 9.14453 0.57115 9.14453H8.71401C8.94972 9.14453 9.14258 8.95167 9.14258 8.71596V0.573103C9.14258 0.337388 8.94972 0.144531 8.71401 0.144531ZM7.32115 7.3231H1.96401V1.96596H7.32115V7.3231ZM19.4283 0.144531H11.2854C11.0497 0.144531 10.8569 0.337388 10.8569 0.573103V8.71596C10.8569 8.95167 11.0497 9.14453 11.2854 9.14453H19.4283C19.664 9.14453 19.8569 8.95167 19.8569 8.71596V0.573103C19.8569 0.337388 19.664 0.144531 19.4283 0.144531ZM18.0354 7.3231H12.6783V1.96596H18.0354V7.3231ZM8.71401 10.8588H0.57115C0.335435 10.8588 0.142578 11.0517 0.142578 11.2874V19.4302C0.142578 19.666 0.335435 19.8588 0.57115 19.8588H8.71401C8.94972 19.8588 9.14258 19.666 9.14258 19.4302V11.2874C9.14258 11.0517 8.94972 10.8588 8.71401 10.8588ZM7.32115 18.0374H1.96401V12.6802H7.32115V18.0374ZM19.4283 10.8588H11.2854C11.0497 10.8588 10.8569 11.0517 10.8569 11.2874V19.4302C10.8569 19.666 11.0497 19.8588 11.2854 19.8588H19.4283C19.664 19.8588 19.8569 19.666 19.8569 19.4302V11.2874C19.8569 11.0517 19.664 10.8588 19.4283 10.8588ZM18.0354 18.0374H12.6783V12.6802H18.0354V18.0374Z"
                        fill={
                          active === "/dashboard"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                    </svg>
                  }
                >
                  {t("common.dashboard")}
                </MenuItem>

                {collapsed ? (
                  <SubmenuCustom {...propsSubmenu} collapsed={collapsed}>
                    {renderListObject}
                  </SubmenuCustom>
                ) : (
                  <SubmenuCustom2 {...propsSubmenu}>
                    {renderListObject}
                  </SubmenuCustom2>
                )}
                {checkPermissionQMView && (
                  <MenuItem
                    key="/quality-management/default-view"
                    onClick={() => {
                      navigate("/quality-management/default-view");
                    }}
                    icon={
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        style={{
                          marginLeft: "-8px",
                        }}
                      >
                        <path
                          d="M17.6201 9.62109H12.3701C11.9601 9.62109 11.6201 9.28109 11.6201 8.87109C11.6201 8.46109 11.9601 8.12109 12.3701 8.12109H17.6201C18.0301 8.12109 18.3701 8.46109 18.3701 8.87109C18.3701 9.28109 18.0401 9.62109 17.6201 9.62109Z"
                          fill={
                            active === "/quality-management/default-view"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                        <path
                          d="M7.11957 10.3803C6.92957 10.3803 6.73957 10.3103 6.58957 10.1603L5.83957 9.41031C5.54957 9.12031 5.54957 8.64031 5.83957 8.35031C6.12957 8.06031 6.60957 8.06031 6.89957 8.35031L7.11957 8.57031L8.83957 6.85031C9.12957 6.56031 9.60957 6.56031 9.89957 6.85031C10.1896 7.14031 10.1896 7.62031 9.89957 7.91031L7.64957 10.1603C7.50957 10.3003 7.31957 10.3803 7.11957 10.3803Z"
                          fill={
                            active === "/quality-management/default-view"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                        <path
                          d="M17.6201 16.6211H12.3701C11.9601 16.6211 11.6201 16.2811 11.6201 15.8711C11.6201 15.4611 11.9601 15.1211 12.3701 15.1211H17.6201C18.0301 15.1211 18.3701 15.4611 18.3701 15.8711C18.3701 16.2811 18.0401 16.6211 17.6201 16.6211Z"
                          fill={
                            active === "/quality-management/default-view"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                        <path
                          d="M7.11957 17.3803C6.92957 17.3803 6.73957 17.3103 6.58957 17.1603L5.83957 16.4103C5.54957 16.1203 5.54957 15.6403 5.83957 15.3503C6.12957 15.0603 6.60957 15.0603 6.89957 15.3503L7.11957 15.5703L8.83957 13.8503C9.12957 13.5603 9.60957 13.5603 9.89957 13.8503C10.1896 14.1403 10.1896 14.6203 9.89957 14.9103L7.64957 17.1603C7.50957 17.3003 7.31957 17.3803 7.11957 17.3803Z"
                          fill={
                            active === "/quality-management/default-view"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                        <path
                          d="M15 22.75H9C3.57 22.75 1.25 20.43 1.25 15V9C1.25 3.57 3.57 1.25 9 1.25H15C20.43 1.25 22.75 3.57 22.75 9V15C22.75 20.43 20.43 22.75 15 22.75ZM9 2.75C4.39 2.75 2.75 4.39 2.75 9V15C2.75 19.61 4.39 21.25 9 21.25H15C19.61 21.25 21.25 19.61 21.25 15V9C21.25 4.39 19.61 2.75 15 2.75H9Z"
                          fill={
                            active === "/quality-management/default-view"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                      </svg>
                    }
                  >
                    {t("common.qualityManagement")}
                  </MenuItem>
                )}

                <MenuItem
                  key="/report"
                  onClick={() => {
                    navigate("/report#my_folders");
                  }}
                  icon={
                    <svg
                      width="22"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{
                        marginLeft: "-8px",
                      }}
                    >
                      <path
                        d="M21.4282 12.1582H11.8389V2.56889C11.8389 2.45104 11.7425 2.35461 11.6246 2.35461H10.9282C9.52112 2.3523 8.12749 2.62824 6.82748 3.16654C5.52746 3.70484 4.34672 4.49489 3.35317 5.49122C2.37471 6.46665 1.59509 7.62289 1.05763 8.89568C0.498362 10.2158 0.211378 11.6352 0.213883 13.0689C0.211579 14.4759 0.487516 15.8696 1.02582 17.1696C1.56412 18.4696 2.35416 19.6503 3.35049 20.6439C4.33353 21.6269 5.47996 22.401 6.75496 22.9394C8.07506 23.4987 9.49449 23.7857 10.9282 23.7832C12.3352 23.7855 13.7288 23.5095 15.0289 22.9712C16.3289 22.4329 17.5096 21.6429 18.5032 20.6466C19.4862 19.6635 20.2603 18.5171 20.7987 17.2421C21.358 15.922 21.645 14.5026 21.6425 13.0689V12.3725C21.6425 12.2546 21.546 12.1582 21.4282 12.1582ZM17.188 19.385C16.3586 20.2079 15.3749 20.8591 14.2934 21.3013C13.2119 21.7435 12.0537 21.9679 10.8853 21.9618C8.52549 21.951 6.30763 21.0269 4.63888 19.3582C2.95942 17.6787 2.03531 15.4448 2.03531 13.0689C2.03531 10.693 2.95942 8.45907 4.63888 6.77961C6.10138 5.31711 7.98442 4.42514 10.0175 4.22157V13.9796H19.7755C19.5692 16.0234 18.6692 17.9171 17.188 19.385ZM23.7853 10.6689L23.7157 9.91354C23.488 7.44657 22.3925 5.11889 20.63 3.36175C18.8662 1.60138 16.5433 0.511926 14.0621 0.281394L13.3041 0.211751C13.1782 0.201037 13.071 0.297465 13.071 0.423358V10.7118C13.071 10.8296 13.1675 10.926 13.2853 10.926L23.571 10.8993C23.6969 10.8993 23.796 10.7921 23.7853 10.6689ZM14.8871 9.10997V2.23675C16.5744 2.58991 18.1229 3.42555 19.3442 4.64211C20.5683 5.86354 21.4067 7.41711 21.755 9.09121L14.8871 9.10997Z"
                        fill={
                          active === "/report"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                    </svg>
                  }
                >
                  {t("common.reports")}
                </MenuItem>
                <MenuItem
                  key="/calendar"
                  onClick={() => {
                    navigate("/calendar");
                  }}
                  icon={
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{
                        marginLeft: "-8px",
                      }}
                    >
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M8 1C8.55228 1 9 1.44772 9 2V5C9 5.55229 8.55228 6 8 6C7.44772 6 7 5.55229 7 5V2C7 1.44772 7.44772 1 8 1Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M16 1C16.5523 1 17 1.44772 17 2V5C17 5.55229 16.5523 6 16 6C15.4477 6 15 5.55229 15 5V2C15 1.44772 15.4477 1 16 1Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M2.5 9.08984C2.5 8.53756 2.94772 8.08984 3.5 8.08984H20.5C21.0523 8.08984 21.5 8.53756 21.5 9.08984C21.5 9.64213 21.0523 10.0898 20.5 10.0898H3.5C2.94772 10.0898 2.5 9.64213 2.5 9.08984Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M4.92029 5.55545C4.33682 6.18381 4 7.15329 4 8.5V17C4 18.3467 4.33682 19.3162 4.92029 19.9445C5.49131 20.5595 6.4406 21 8 21H16C17.5594 21 18.5087 20.5595 19.0797 19.9445C19.6632 19.3162 20 18.3467 20 17V8.5C20 7.15329 19.6632 6.18381 19.0797 5.55545C18.5087 4.94051 17.5594 4.5 16 4.5H8C6.4406 4.5 5.49131 4.94051 4.92029 5.55545ZM3.45471 4.19455C4.50869 3.05949 6.0594 2.5 8 2.5H16C17.9406 2.5 19.4913 3.05949 20.5453 4.19455C21.5868 5.31619 22 6.84671 22 8.5V17C22 18.6533 21.5868 20.1838 20.5453 21.3055C19.4913 22.4405 17.9406 23 16 23H8C6.0594 23 4.50869 22.4405 3.45471 21.3055C2.41318 20.1838 2 18.6533 2 17V8.5C2 6.84671 2.41318 5.31619 3.45471 4.19455Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M14.6943 13.6992C14.6943 13.1469 15.1421 12.6992 15.6943 12.6992H15.7033C16.2556 12.6992 16.7033 13.1469 16.7033 13.6992C16.7033 14.2515 16.2556 14.6992 15.7033 14.6992H15.6943C15.1421 14.6992 14.6943 14.2515 14.6943 13.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M14.6943 16.6992C14.6943 16.1469 15.1421 15.6992 15.6943 15.6992H15.7033C16.2556 15.6992 16.7033 16.1469 16.7033 16.6992C16.7033 17.2515 16.2556 17.6992 15.7033 17.6992H15.6943C15.1421 17.6992 14.6943 17.2515 14.6943 16.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M10.9951 13.6992C10.9951 13.1469 11.4428 12.6992 11.9951 12.6992H12.0041C12.5564 12.6992 13.0041 13.1469 13.0041 13.6992C13.0041 14.2515 12.5564 14.6992 12.0041 14.6992H11.9951C11.4428 14.6992 10.9951 14.2515 10.9951 13.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M10.9951 16.6992C10.9951 16.1469 11.4428 15.6992 11.9951 15.6992H12.0041C12.5564 15.6992 13.0041 16.1469 13.0041 16.6992C13.0041 17.2515 12.5564 17.6992 12.0041 17.6992H11.9951C11.4428 17.6992 10.9951 17.2515 10.9951 16.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M7.29395 13.6992C7.29395 13.1469 7.74166 12.6992 8.29395 12.6992H8.30293C8.85521 12.6992 9.30293 13.1469 9.30293 13.6992C9.30293 14.2515 8.85521 14.6992 8.30293 14.6992H8.29395C7.74166 14.6992 7.29395 14.2515 7.29395 13.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        fillRule="evenodd"
                        clipRule="evenodd"
                        d="M7.29395 16.6992C7.29395 16.1469 7.74166 15.6992 8.29395 15.6992H8.30293C8.85521 15.6992 9.30293 16.1469 9.30293 16.6992C9.30293 17.2515 8.85521 17.6992 8.30293 17.6992H8.29395C7.74166 17.6992 7.29395 17.2515 7.29395 16.6992Z"
                        fill={
                          active === "/calendar"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                    </svg>
                  }
                >
                  {t("common.calendar")}
                </MenuItem>

                {userDetail && userRuleGlobal.length > 0 && (
                  <MenuItem
                    key="/settings"
                    onClick={() => {
                      navigate("/settings");
                    }}
                    icon={
                      <svg
                        style={{
                          marginLeft: "-8px",
                        }}
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M23.0573 15.0496L21.3029 13.5496C21.3859 13.0406 21.4288 12.521 21.4288 12.0013C21.4288 11.4817 21.3859 10.9621 21.3029 10.4531L23.0573 8.95312C23.1897 8.83984 23.2844 8.68895 23.3289 8.52053C23.3734 8.35211 23.3656 8.17413 23.3065 8.01027L23.2823 7.94062C22.7993 6.5907 22.0761 5.33929 21.1475 4.24687L21.0993 4.19063C20.9867 4.05818 20.8365 3.96296 20.6687 3.91754C20.5009 3.87211 20.3232 3.87861 20.1591 3.93616L17.9815 4.71027C17.1779 4.05134 16.2806 3.5317 15.3109 3.16741L14.8904 0.890625C14.8587 0.719309 14.7755 0.561703 14.6521 0.438744C14.5287 0.315785 14.3707 0.233295 14.1993 0.202232L14.127 0.188839C12.7315 -0.0629464 11.2636 -0.0629464 9.86806 0.188839L9.79574 0.202232C9.6243 0.233295 9.46637 0.315785 9.34293 0.438744C9.2195 0.561703 9.13639 0.719309 9.10467 0.890625L8.68145 3.17813C7.71955 3.54249 6.82377 4.06185 6.02967 4.71562L3.83592 3.93616C3.6719 3.87815 3.4941 3.87142 3.32617 3.91687C3.15824 3.96233 3.00811 4.05781 2.89574 4.19063L2.84752 4.24687C1.92006 5.34006 1.19692 6.59126 0.712701 7.94062L0.688594 8.01027C0.568058 8.34509 0.667165 8.72009 0.937701 8.95312L2.71359 10.4692C2.63056 10.9728 2.59038 11.4871 2.59038 11.9987C2.59038 12.5129 2.63056 13.0272 2.71359 13.5281L0.937701 15.0442C0.805368 15.1575 0.710656 15.3084 0.666158 15.4768C0.62166 15.6452 0.629485 15.8232 0.688594 15.9871L0.712701 16.0567C1.19752 17.4067 1.91538 18.6522 2.84752 19.7504L2.89574 19.8067C3.00838 19.9391 3.15851 20.0344 3.32635 20.0798C3.49418 20.1252 3.67184 20.1187 3.83592 20.0612L6.02967 19.2817C6.82788 19.9379 7.71984 20.4576 8.68145 20.8192L9.10467 23.1067C9.13639 23.278 9.2195 23.4356 9.34293 23.5586C9.46637 23.6815 9.6243 23.764 9.79574 23.7951L9.86806 23.8085C11.2764 24.0616 12.7186 24.0616 14.127 23.8085L14.1993 23.7951C14.3707 23.764 14.5287 23.6815 14.6521 23.5586C14.7755 23.4356 14.8587 23.278 14.8904 23.1067L15.3109 20.8299C16.2802 20.4666 17.1826 19.9452 17.9815 19.2871L20.1591 20.0612C20.3232 20.1192 20.5009 20.1259 20.6689 20.0804C20.8368 20.035 20.9869 19.9395 21.0993 19.8067L21.1475 19.7504C22.0797 18.6496 22.7975 17.4067 23.2823 16.0567L23.3065 15.9871C23.427 15.6576 23.3279 15.2826 23.0573 15.0496ZM19.4011 10.7692C19.4681 11.1737 19.5029 11.5888 19.5029 12.004C19.5029 12.4192 19.4681 12.8344 19.4011 13.2388L19.2243 14.3129L21.2252 16.0246C20.9219 16.7234 20.539 17.3849 20.0841 17.996L17.5984 17.1147L16.7573 17.8058C16.1172 18.3308 15.4047 18.7433 14.6332 19.0326L13.6127 19.4156L13.1332 22.0138C12.3767 22.0996 11.613 22.0996 10.8565 22.0138L10.377 19.4103L9.36449 19.0219C8.60109 18.7326 7.89127 18.3201 7.25645 17.7978L6.41538 17.104L3.91359 17.9933C3.45824 17.3799 3.07788 16.7183 2.77252 16.0219L4.79484 14.2942L4.62074 13.2228C4.55645 12.8237 4.52163 12.4112 4.52163 12.004C4.52163 11.5942 4.55377 11.1844 4.62074 10.7853L4.79484 9.71384L2.77252 7.98616C3.0752 7.28705 3.45824 6.62812 3.91359 6.01473L6.41538 6.90402L7.25645 6.21027C7.89127 5.68795 8.60109 5.27545 9.36449 4.98616L10.3797 4.60312L10.8591 1.99955C11.6118 1.91384 12.3806 1.91384 13.1359 1.99955L13.6154 4.59777L14.6359 4.9808C15.4047 5.27009 16.1198 5.68259 16.76 6.20759L17.6011 6.89866L20.0868 6.01741C20.5422 6.6308 20.9225 7.29241 21.2279 7.98884L19.227 9.70045L19.4011 10.7692ZM12.0002 7.02187C9.39663 7.02187 7.28592 9.13259 7.28592 11.7362C7.28592 14.3397 9.39663 16.4504 12.0002 16.4504C14.6038 16.4504 16.7145 14.3397 16.7145 11.7362C16.7145 9.13259 14.6038 7.02187 12.0002 7.02187ZM14.1216 13.8576C13.8434 14.1366 13.5127 14.3579 13.1486 14.5087C12.7846 14.6595 12.3943 14.7368 12.0002 14.7362C11.1993 14.7362 10.4466 14.4228 9.87877 13.8576C9.59973 13.5793 9.37845 13.2487 9.22767 12.8846C9.07689 12.5205 8.99959 12.1302 9.0002 11.7362C9.0002 10.9353 9.31359 10.1826 9.87877 9.61473C10.4466 9.04688 11.1993 8.73616 12.0002 8.73616C12.8011 8.73616 13.5538 9.04688 14.1216 9.61473C14.4007 9.89298 14.622 10.2236 14.7727 10.5877C14.9235 10.9518 15.0008 11.3421 15.0002 11.7362C15.0002 12.5371 14.6868 13.2897 14.1216 13.8576Z"
                          fill={
                            active === "/settings"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                      </svg>
                    }
                  >
                    {t("common.setting")}
                  </MenuItem>
                )}
                <MenuItem
                  key="/monitoring"
                  onClick={() => {
                    navigate("/monitoring");
                  }}
                  icon={
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      style={{
                        marginLeft: "-8px",
                      }}
                    >
                      <path
                        d="M14.9702 22.75H8.97021C3.54022 22.75 1.22021 20.43 1.22021 15V9C1.22021 3.57 3.54022 1.25 8.97021 1.25H14.9702C20.4002 1.25 22.7202 3.57 22.7202 9V15C22.7202 20.43 20.4102 22.75 14.9702 22.75ZM8.97021 2.75C4.36021 2.75 2.72021 4.39 2.72021 9V15C2.72021 19.61 4.36021 21.25 8.97021 21.25H14.9702C19.5802 21.25 21.2202 19.61 21.2202 15V9C21.2202 4.39 19.5802 2.75 14.9702 2.75H8.97021Z"
                        fill={
                          active === "/monitoring"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                      <path
                        d="M11.4502 18.0599C11.2102 18.0599 10.6302 17.9699 10.2902 17.0999L9.15023 14.2199C8.99023 13.8099 8.42023 13.4299 7.99023 13.4299L1.99023 13.4499C1.58023 13.4499 1.24023 13.1199 1.24023 12.6999C1.24023 12.2899 1.57023 11.9499 1.99023 11.9499L7.99023 11.9299H8.00023C9.05023 11.9299 10.1702 12.6899 10.5602 13.6699L11.4702 15.9699L13.5302 10.7499C13.8402 9.96989 14.3902 9.85989 14.6202 9.83989C14.8502 9.82989 15.4102 9.87989 15.8002 10.6199L16.8402 12.5899C17.0202 12.9299 17.5602 13.2599 17.9502 13.2599H22.0102C22.4202 13.2599 22.7602 13.5999 22.7602 14.0099C22.7602 14.4199 22.4202 14.7599 22.0102 14.7599H17.9502C17.0002 14.7599 15.9602 14.1299 15.5202 13.2899L14.7402 11.8099L12.6402 17.0999C12.2702 17.9599 11.6802 18.0599 11.4502 18.0599Z"
                        fill={
                          active === "/monitoring"
                            ? THEME_COLOR.filter(
                                (x) => x.name === defaultBrandName.theme_color
                              )[0]
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0].main
                              : theme.white
                            : theme.white
                        }
                      />
                    </svg>
                  }
                >
                  Monitoring
                </MenuItem>
              </CustomMenu>
              {/* <div
                style={{
                  width: "90%",
                  borderTop: "1px solid #87B9BF",
                  margin: "auto",
                }}
              ></div> */}
              {!collapsed ? (
                <MoreInfo>
                  <CustomLanguage
                    defaultValue={
                      localStorage.getItem("i18nextLng")
                        ? localStorage.getItem("i18nextLng")
                        : "en"
                    }
                    onChange={(e) => {
                      i18n.changeLanguage(e);
                      localStorage.setItem("i18nextLng", e);
                    }}
                  >
                    <Option value="vi">
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <img
                          alt=""
                          src={vi}
                          style={{ width: "26px", marginRight: "8px" }}
                        />
                        Tiếng Việt
                      </div>
                    </Option>
                    <Option value="en">
                      <div style={{ display: "flex", alignItems: "center" }}>
                        <img
                          alt=""
                          src={en}
                          style={{ width: "26px", marginRight: "8px" }}
                        />
                        English
                      </div>
                    </Option>
                  </CustomLanguage>
                  <CustomVersion onClick={() => setModalVersion(true)}>
                    <p>{t("common.version")}</p>
                    {/* <p>
                      Make with <img src={HeartImg} alt="heart" /> by Basebs
                    </p> */}
                  </CustomVersion>
                </MoreInfo>
              ) : (
                ""
              )}
            </div>
          </>
        ) : (
          <>
            {" "}
            <img
              alt=""
              src={collapsedButton}
              className="button__collapse__left"
              onClick={() => setCollapsed(!collapsed)}
            />
            <img
              alt=""
              src={collapsedButton}
              className="button__collapse__right"
              onClick={() => setCollapsed(!collapsed)}
            />
            <div className="logo__base">
              <AvatarEditor
                style={{
                  borderRadius: defaultBrandName.media_config.borderRadius,
                }}
                position={defaultBrandName.media_config.position}
                scale={defaultBrandName.media_config.scale}
                border={defaultBrandName.media_config.border}
                width={
                  defaultBrandName.media_config.width < 70
                    ? defaultBrandName.media_config.width
                    : 70
                }
                height={
                  defaultBrandName.media_config.height < 70
                    ? defaultBrandName.media_config.height
                    : 70
                }
                image={defaultBrandName.media_url}
              />
            </div>
            <CustomMenu
              color={
                THEME_COLOR.filter(
                  (x) => x.name === defaultBrandName.theme_color
                )[0]
                  ? THEME_COLOR.filter(
                      (x) => x.name === defaultBrandName.theme_color
                    )[0].main
                  : "#20A2A2"
              }
              hover={
                THEME_COLOR.filter(
                  (x) => x.name === defaultBrandName.theme_color
                )[0]
                  ? THEME_COLOR.filter(
                      (x) => x.name === defaultBrandName.theme_color
                    )[0].darker
                  : "#20A2A2"
              }
              defaultSelectedKeys={[`${active}`]}
              mode="inline"
              selectedKeys={active}
              triggerSubMenuAction="click"
              collapsed={collapsed}
            >
              <MenuItem
                key="/tenants"
                onClick={() => {
                  navigate("/tenants");
                }}
                icon={
                  <svg
                    style={{
                      marginLeft: "-8px",
                    }}
                    width="20"
                    height="22"
                    viewBox="0 0 20 22"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M14.3221 4.90513C14.3221 4.7971 14.2337 4.70871 14.1257 4.70871H4.69713C4.58909 4.70871 4.5007 4.7971 4.5007 4.90513V6.08371C4.5007 6.19174 4.58909 6.28013 4.69713 6.28013H14.1257C14.2337 6.28013 14.3221 6.19174 14.3221 6.08371V4.90513ZM14.1257 8.24442H4.69713C4.58909 8.24442 4.5007 8.33281 4.5007 8.44085V9.61942C4.5007 9.72746 4.58909 9.81585 4.69713 9.81585H14.1257C14.2337 9.81585 14.3221 9.72746 14.3221 9.61942V8.44085C14.3221 8.33281 14.2337 8.24442 14.1257 8.24442ZM9.21498 11.7801H4.69713C4.58909 11.7801 4.5007 11.8685 4.5007 11.9766V13.1551C4.5007 13.2632 4.58909 13.3516 4.69713 13.3516H9.21498C9.32302 13.3516 9.41141 13.2632 9.41141 13.1551V11.9766C9.41141 11.8685 9.32302 11.7801 9.21498 11.7801ZM8.03641 19.3426H2.53641V2.05692H16.2864V10.5033C16.2864 10.6114 16.3748 10.6998 16.4828 10.6998H17.8578C17.9659 10.6998 18.0543 10.6114 18.0543 10.5033V1.07478C18.0543 0.640179 17.7032 0.289062 17.2686 0.289062H1.55427C1.11967 0.289062 0.768555 0.640179 0.768555 1.07478V20.3248C0.768555 20.7594 1.11967 21.1105 1.55427 21.1105H8.03641C8.14445 21.1105 8.23284 21.0221 8.23284 20.9141V19.5391C8.23284 19.431 8.14445 19.3426 8.03641 19.3426ZM16.7971 17.5158C17.5092 16.8701 17.9561 15.9371 17.9561 14.8984C17.9561 12.9464 16.3723 11.3627 14.4203 11.3627C12.4683 11.3627 10.8846 12.9464 10.8846 14.8984C10.8846 15.9371 11.3315 16.8701 12.0436 17.5158C10.644 18.3138 9.6815 19.792 9.60784 21.496C9.60293 21.6065 9.69378 21.6998 9.80427 21.6998H10.9853C11.0884 21.6998 11.1744 21.6188 11.1817 21.5132C11.2775 19.8141 12.6942 18.4587 14.4203 18.4587C16.1465 18.4587 17.5632 19.8141 17.659 21.5132C17.6639 21.6163 17.7498 21.6998 17.8554 21.6998H19.0364C19.1494 21.6998 19.2378 21.6065 19.2328 21.496C19.1616 19.7895 18.1967 18.3138 16.7971 17.5158ZM14.4203 12.9342C15.5056 12.9342 16.3846 13.8132 16.3846 14.8984C16.3846 15.9837 15.5056 16.8627 14.4203 16.8627C13.3351 16.8627 12.4561 15.9837 12.4561 14.8984C12.4561 13.8132 13.3351 12.9342 14.4203 12.9342Z"
                      fill="white"
                    />
                  </svg>
                }
              >
                List tenants
              </MenuItem>
              {userDetail && userDetail.permission.role === "super" ? (
                <>
                  <MenuItem
                    key="/permission"
                    onClick={() => {
                      navigate("/permission");
                    }}
                    icon={
                      <svg
                        style={{
                          marginLeft: "-8px",
                        }}
                        width="22"
                        height="22"
                        viewBox="0 0 22 22"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M10.9997 21.0846C5.90576 19.9526 1.83301 15.1465 1.83301 10.0846V4.58464L10.9997 0.917969L20.1663 4.58464V10.0846C20.1663 15.1483 16.0936 19.9526 10.9997 21.0846ZM3.66634 5.5013V10.0846C3.71873 12.2041 4.46417 14.248 5.78859 15.9035C7.113 17.559 8.94342 18.735 10.9997 19.2513C13.0559 18.735 14.8863 17.559 16.2108 15.9035C17.5352 14.248 18.2806 12.2041 18.333 10.0846V5.5013L10.9997 2.7513L3.66634 5.5013Z"
                          fill="white"
                        />
                        <path
                          d="M10.9997 10.0833C12.2653 10.0833 13.2913 9.05732 13.2913 7.79167C13.2913 6.52601 12.2653 5.5 10.9997 5.5C9.73402 5.5 8.70801 6.52601 8.70801 7.79167C8.70801 9.05732 9.73402 10.0833 10.9997 10.0833Z"
                          fill="white"
                        />
                        <path
                          d="M6.41699 13.75C6.86881 14.5734 7.53129 15.2619 8.33662 15.7451C9.14195 16.2283 10.0612 16.4888 11.0003 16.5C11.9394 16.4888 12.8587 16.2283 13.664 15.7451C14.4694 15.2619 15.1318 14.5734 15.5837 13.75C15.5607 12.012 12.5202 11 11.0003 11C9.47224 11 6.43991 12.012 6.41699 13.75Z"
                          fill="white"
                        />
                      </svg>
                    }
                  >
                    Root user
                  </MenuItem>
                  <MenuItem
                    key="/audit-log"
                    onClick={() => {
                      navigate("/audit-log");
                    }}
                    icon={
                      <svg
                        style={{
                          marginLeft: "-8px",
                        }}
                        width="22"
                        height="18"
                        viewBox="0 0 22 18"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M20.8217 1.14342H6.48242C6.37439 1.14342 6.28599 1.23181 6.28599 1.33984V2.71484C6.28599 2.82288 6.37439 2.91127 6.48242 2.91127H20.8217C20.9297 2.91127 21.0181 2.82288 21.0181 2.71484V1.33984C21.0181 1.23181 20.9297 1.14342 20.8217 1.14342ZM20.8217 8.11663H6.48242C6.37439 8.11663 6.28599 8.20502 6.28599 8.31306V9.68806C6.28599 9.79609 6.37439 9.88449 6.48242 9.88449H20.8217C20.9297 9.88449 21.0181 9.79609 21.0181 9.68806V8.31306C21.0181 8.20502 20.9297 8.11663 20.8217 8.11663ZM20.8217 15.0898H6.48242C6.37439 15.0898 6.28599 15.1782 6.28599 15.2863V16.6613C6.28599 16.7693 6.37439 16.8577 6.48242 16.8577H20.8217C20.9297 16.8577 21.0181 16.7693 21.0181 16.6613V15.2863C21.0181 15.1782 20.9297 15.0898 20.8217 15.0898ZM0.982422 2.02734C0.982422 2.20791 1.01799 2.38671 1.08709 2.55353C1.15619 2.72036 1.25747 2.87194 1.38515 2.99962C1.51283 3.1273 1.66441 3.22858 1.83123 3.29768C1.99805 3.36678 2.17685 3.40234 2.35742 3.40234C2.53799 3.40234 2.71679 3.36678 2.88361 3.29768C3.05043 3.22858 3.20201 3.1273 3.32969 2.99962C3.45737 2.87194 3.55866 2.72036 3.62776 2.55353C3.69686 2.38671 3.73242 2.20791 3.73242 2.02734C3.73242 1.84678 3.69686 1.66798 3.62776 1.50115C3.55866 1.33433 3.45737 1.18275 3.32969 1.05507C3.20201 0.927391 3.05043 0.826109 2.88361 0.757009C2.71679 0.687909 2.53799 0.652344 2.35742 0.652344C2.17685 0.652344 1.99805 0.687909 1.83123 0.757009C1.66441 0.826109 1.51283 0.927391 1.38515 1.05507C1.25747 1.18275 1.15619 1.33433 1.08709 1.50115C1.01799 1.66798 0.982422 1.84678 0.982422 2.02734ZM0.982422 9.00056C0.982422 9.18113 1.01799 9.35993 1.08709 9.52675C1.15619 9.69357 1.25747 9.84515 1.38515 9.97283C1.51283 10.1005 1.66441 10.2018 1.83123 10.2709C1.99805 10.34 2.17685 10.3756 2.35742 10.3756C2.53799 10.3756 2.71679 10.34 2.88361 10.2709C3.05043 10.2018 3.20201 10.1005 3.32969 9.97283C3.45737 9.84515 3.55866 9.69357 3.62776 9.52675C3.69686 9.35993 3.73242 9.18113 3.73242 9.00056C3.73242 8.81999 3.69686 8.64119 3.62776 8.47437C3.55866 8.30755 3.45737 8.15597 3.32969 8.02829C3.20201 7.90061 3.05043 7.79932 2.88361 7.73022C2.71679 7.66112 2.53799 7.62556 2.35742 7.62556C2.17685 7.62556 1.99805 7.66112 1.83123 7.73022C1.66441 7.79932 1.51283 7.90061 1.38515 8.02829C1.25747 8.15597 1.15619 8.30755 1.08709 8.47437C1.01799 8.64119 0.982422 8.81999 0.982422 9.00056ZM0.982422 15.9738C0.982422 16.1543 1.01799 16.3331 1.08709 16.5C1.15619 16.6668 1.25747 16.8184 1.38515 16.946C1.51283 17.0737 1.66441 17.175 1.83123 17.2441C1.99805 17.3132 2.17685 17.3488 2.35742 17.3488C2.53799 17.3488 2.71679 17.3132 2.88361 17.2441C3.05043 17.175 3.20201 17.0737 3.32969 16.946C3.45737 16.8184 3.55866 16.6668 3.62776 16.5C3.69686 16.3331 3.73242 16.1543 3.73242 15.9738C3.73242 15.7932 3.69686 15.6144 3.62776 15.4476C3.55866 15.2808 3.45737 15.1292 3.32969 15.0015C3.20201 14.8738 3.05043 14.7725 2.88361 14.7034C2.71679 14.6343 2.53799 14.5988 2.35742 14.5988C2.17685 14.5988 1.99805 14.6343 1.83123 14.7034C1.66441 14.7725 1.51283 14.8738 1.38515 15.0015C1.25747 15.1292 1.15619 15.2808 1.08709 15.4476C1.01799 15.6144 0.982422 15.7932 0.982422 15.9738Z"
                          fill="white"
                        />
                      </svg>
                    }
                  >
                    Audit log
                  </MenuItem>
                  <MenuItem
                    key="/package"
                    onClick={() => {
                      navigate("/package");
                    }}
                    icon={
                      <svg
                        style={{
                          marginLeft: "-8px",
                        }}
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M23.0573 15.0496L21.3029 13.5496C21.3859 13.0406 21.4288 12.521 21.4288 12.0013C21.4288 11.4817 21.3859 10.9621 21.3029 10.4531L23.0573 8.95312C23.1897 8.83984 23.2844 8.68895 23.3289 8.52053C23.3734 8.35211 23.3656 8.17413 23.3065 8.01027L23.2823 7.94062C22.7993 6.5907 22.0761 5.33929 21.1475 4.24687L21.0993 4.19063C20.9867 4.05818 20.8365 3.96296 20.6687 3.91754C20.5009 3.87211 20.3232 3.87861 20.1591 3.93616L17.9815 4.71027C17.1779 4.05134 16.2806 3.5317 15.3109 3.16741L14.8904 0.890625C14.8587 0.719309 14.7755 0.561703 14.6521 0.438744C14.5287 0.315785 14.3707 0.233295 14.1993 0.202232L14.127 0.188839C12.7315 -0.0629464 11.2636 -0.0629464 9.86806 0.188839L9.79574 0.202232C9.6243 0.233295 9.46637 0.315785 9.34293 0.438744C9.2195 0.561703 9.13639 0.719309 9.10467 0.890625L8.68145 3.17813C7.71955 3.54249 6.82377 4.06185 6.02967 4.71562L3.83592 3.93616C3.6719 3.87815 3.4941 3.87142 3.32617 3.91687C3.15824 3.96233 3.00811 4.05781 2.89574 4.19063L2.84752 4.24687C1.92006 5.34006 1.19692 6.59126 0.712701 7.94062L0.688594 8.01027C0.568058 8.34509 0.667165 8.72009 0.937701 8.95312L2.71359 10.4692C2.63056 10.9728 2.59038 11.4871 2.59038 11.9987C2.59038 12.5129 2.63056 13.0272 2.71359 13.5281L0.937701 15.0442C0.805368 15.1575 0.710656 15.3084 0.666158 15.4768C0.62166 15.6452 0.629485 15.8232 0.688594 15.9871L0.712701 16.0567C1.19752 17.4067 1.91538 18.6522 2.84752 19.7504L2.89574 19.8067C3.00838 19.9391 3.15851 20.0344 3.32635 20.0798C3.49418 20.1252 3.67184 20.1187 3.83592 20.0612L6.02967 19.2817C6.82788 19.9379 7.71984 20.4576 8.68145 20.8192L9.10467 23.1067C9.13639 23.278 9.2195 23.4356 9.34293 23.5586C9.46637 23.6815 9.6243 23.764 9.79574 23.7951L9.86806 23.8085C11.2764 24.0616 12.7186 24.0616 14.127 23.8085L14.1993 23.7951C14.3707 23.764 14.5287 23.6815 14.6521 23.5586C14.7755 23.4356 14.8587 23.278 14.8904 23.1067L15.3109 20.8299C16.2802 20.4666 17.1826 19.9452 17.9815 19.2871L20.1591 20.0612C20.3232 20.1192 20.5009 20.1259 20.6689 20.0804C20.8368 20.035 20.9869 19.9395 21.0993 19.8067L21.1475 19.7504C22.0797 18.6496 22.7975 17.4067 23.2823 16.0567L23.3065 15.9871C23.427 15.6576 23.3279 15.2826 23.0573 15.0496ZM19.4011 10.7692C19.4681 11.1737 19.5029 11.5888 19.5029 12.004C19.5029 12.4192 19.4681 12.8344 19.4011 13.2388L19.2243 14.3129L21.2252 16.0246C20.9219 16.7234 20.539 17.3849 20.0841 17.996L17.5984 17.1147L16.7573 17.8058C16.1172 18.3308 15.4047 18.7433 14.6332 19.0326L13.6127 19.4156L13.1332 22.0138C12.3767 22.0996 11.613 22.0996 10.8565 22.0138L10.377 19.4103L9.36449 19.0219C8.60109 18.7326 7.89127 18.3201 7.25645 17.7978L6.41538 17.104L3.91359 17.9933C3.45824 17.3799 3.07788 16.7183 2.77252 16.0219L4.79484 14.2942L4.62074 13.2228C4.55645 12.8237 4.52163 12.4112 4.52163 12.004C4.52163 11.5942 4.55377 11.1844 4.62074 10.7853L4.79484 9.71384L2.77252 7.98616C3.0752 7.28705 3.45824 6.62812 3.91359 6.01473L6.41538 6.90402L7.25645 6.21027C7.89127 5.68795 8.60109 5.27545 9.36449 4.98616L10.3797 4.60312L10.8591 1.99955C11.6118 1.91384 12.3806 1.91384 13.1359 1.99955L13.6154 4.59777L14.6359 4.9808C15.4047 5.27009 16.1198 5.68259 16.76 6.20759L17.6011 6.89866L20.0868 6.01741C20.5422 6.6308 20.9225 7.29241 21.2279 7.98884L19.227 9.70045L19.4011 10.7692ZM12.0002 7.02187C9.39663 7.02187 7.28592 9.13259 7.28592 11.7362C7.28592 14.3397 9.39663 16.4504 12.0002 16.4504C14.6038 16.4504 16.7145 14.3397 16.7145 11.7362C16.7145 9.13259 14.6038 7.02187 12.0002 7.02187ZM14.1216 13.8576C13.8434 14.1366 13.5127 14.3579 13.1486 14.5087C12.7846 14.6595 12.3943 14.7368 12.0002 14.7362C11.1993 14.7362 10.4466 14.4228 9.87877 13.8576C9.59973 13.5793 9.37845 13.2487 9.22767 12.8846C9.07689 12.5205 8.99959 12.1302 9.0002 11.7362C9.0002 10.9353 9.31359 10.1826 9.87877 9.61473C10.4466 9.04688 11.1993 8.73616 12.0002 8.73616C12.8011 8.73616 13.5538 9.04688 14.1216 9.61473C14.4007 9.89298 14.622 10.2236 14.7727 10.5877C14.9235 10.9518 15.0008 11.3421 15.0002 11.7362C15.0002 12.5371 14.6868 13.2897 14.1216 13.8576Z"
                          fill={
                            active === "/settings"
                              ? THEME_COLOR.filter(
                                  (x) => x.name === defaultBrandName.theme_color
                                )[0]
                                ? THEME_COLOR.filter(
                                    (x) =>
                                      x.name === defaultBrandName.theme_color
                                  )[0].main
                                : theme.white
                              : theme.white
                          }
                        />
                      </svg>
                    }
                  >
                    Package
                  </MenuItem>
                </>
              ) : (
                ""
              )}
            </CustomMenu>
          </>
        )}
      </CustomSidebar>
      <Modal
        visible={modalSignOut}
        footer={[
          <Button
            onClick={() => {
              dispatch(logout());
              navigate("/login");
            }}
          >
            Ok
          </Button>,
        ]}
      >
        <p> Vui lòng đăng nhập lại</p>
      </Modal>
      <ModalVersion
        modalVersion={modalVersion}
        setModalVersion={setModalVersion}
      />

      <Modal
        visible={showModalDisconnect}
        footer={[
          <Button
            onClick={() => {
              $showModalDisconnect(false);
            }}
          >
            Ok
          </Button>,
        ]}
      >
        <p> {t("common.notiExtension")} </p>
      </Modal>

      <Layout>
        <CustomHeader>
          {/* <img alt="" src={search} className="search__icon" />
          <Input size="large" placeholder="Search..." />
          <img alt="" src={vietnamese} className="language__config" /> */}
          <div id="#test1" className="title">
            {handleNameRoute()}
          </div>
          {userDetail.user_root !== true && <HeaderObjectView />}
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {/* {userDetail.use_voice_biometric && inCall && popupVoice && (
              <VoiceBiometric
                showVoice={showVoice}
                setShowVoice={setShowVoice}
                inCall={inCall}
                dataWebSocket={dataWebSocket}
              />
            )} */}

            {userDetail.use_cti && (
              <Numpad dataWebSocket={dataWebSocket} userDetail={userDetail} />
            )}

            {_.get(userDetail, "use_ic", false) && (
              <Dropdown
                visible={showStatusIC}
                onVisibleChange={() => $showStatusIC(!showStatusIC)}
                overlay={menuStatus(listStatusIC, true)}
                placement="bottom"
                trigger={["click"]}
              >
                <ICStatus status={statusIC}>
                  <img
                    style={{ marginRight: "20px" }}
                    src={ICIntegrationIcon}
                    alt=""
                  />
                  <span
                    style={{
                      textTransform: "capitalize",
                      color: statusIC === "available" ? "#0BB865" : "#CF1322",
                    }}
                  >
                    {statusIC === "not_ready" ? "Not Ready" : statusIC}
                  </span>
                  <DownOutlined
                    style={{ marginLeft: "5px", lineHeight: "unset" }}
                  />
                </ICStatus>
              </Dropdown>
            )}

            <audio ref={audioPlayer} src={NotificationSound} />
            <PopupNoti
              unseenNoti={unseenNoti}
              listNoti={listNoti}
              hasMoreNoti={hasMoreNoti}
              isLoading={isLoadingNoti}
              showNoti={showNoti}
              showMenuNoti={showMenuNoti}
              onClickNoti={onClickNoti}
              loadMoreNoti={loadMoreNoti}
              onMarkAllRead={onMarkAllRead}
              handleShowNoti={handleShowNoti}
              handleShowMenuNoti={handleShowMenuNoti}
              onOpenNotifications={onOpenNotifications}
            />
            <Popover content={content} placement="bottomRight" trigger="click">
              <div style={{ display: "flex", alignItems: "center" }}>
                {avatar_config && (
                  <InfoUser>
                    <AvatarEditor
                      className="avatar"
                      image={
                        avatar_config.url && avatar_config.url.length > 0
                          ? BE_URL + avatar_config.url
                          : avatarImg
                      }
                      position={avatar_config && avatar_config.position}
                      scale={avatar_config && avatar_config.scale}
                      border={0}
                    />
                    <InfoDetail>
                      <span> {userDetail.Full_Name}</span>

                      <Dropdown
                        visible={showStatus}
                        onVisibleChange={() => $showStatus(!showStatus)}
                        overlay={menuStatus(listStatusCRM, false)}
                        placement="bottomCenter"
                        trigger={["click"]}
                      >
                        <UserStatus
                          onClick={(e) => {
                            e.stopPropagation();
                            $showStatus(true);
                          }}
                          status={statusCRM}
                        >
                          <span
                            style={{
                              textTransform: "capitalize",
                              lineHeight: "10px",
                            }}
                          >
                            {statusCRM}
                          </span>
                          <DownOutlined
                            style={{ marginLeft: "5px", lineHeight: "10px" }}
                          />
                        </UserStatus>
                      </Dropdown>
                    </InfoDetail>
                  </InfoUser>
                )}
              </div>
            </Popover>

            <ModalChangePass
              showModalChangePass={showModalChangePass}
              onHideModalChangePass={() => setShowModalChangePass(false)}
              getListUser={() => {}}
              user={userDetail}
              isAdmin={false}
            />
          </div>
        </CustomHeader>
        <CustomContent
          className={pathname.includes("quality-management/") ? "QMView" : ""}
        >
          <Outlet />
        </CustomContent>
        {/* <CustomFooter>
          @ Copyright 2020 Basebs. All rights reserved.
        </CustomFooter> */}
      </Layout>
      {/* <MenuConfig visible={visible} setVisible={setVisible} /> */}
      {/* <img
        alt=""
        src={settingTheme}
        className="setting__button"
        onClick={() => setVisible(true)}
      /> */}
    </CustomLayout>
  );
};

const CustomLayout = styled(Layout)`
  .ant-layout-sider-has-trigger {
    max-width: 256px !important;
    width: 256px !important;
    flex: 0 0 256px !important;

    z-index: 10;

    .ant-menu-item {
      color: ${(props) => props.theme.white};
      :hover {
        background: ${(props) => {
          return props.theme.darker;
        }};
      }
    }
    .ant-menu-item-selected {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-layout-sider-collapsed {
    flex: 0 0 80px !important;
    max-width: 80px !important;
    min-width: 80px !important;
    width: 80px !important;
    padding: 0rem 0rem;
    z-index: 10;
    svg {
      transform: scale(1.5);
    }
    .logo__base {
      height: 80px;
      display: flex;
      justify-content: center;
      align-items: center;
      width: 100%;
      z-index: 1;
      img {
        width: 52px;
        position: absolute;
        top: 39px;
        left: 50%;
        transform: translate(-50%, 0);
      }
    }
    .ant-menu-item {
      /* padding: 20px !important; */
      :hover {
        background: ${(props) => {
          return props.theme.darker;
        }};
      }
    }
    .line-top {
      width: 275px;
      margin-top: 111px;
      display: none;
    }
    .line-bot {
      width: 275px;
      margin-top: 18px;
      visibility: hidden;
    }
    .button__collapse__left {
      display: none;
    }
    .button__collapse__right {
      display: block !important;
      width: 49px;
      position: absolute;
      right: -49px;
      top: 7px;
      transform: rotate(-180deg);
      &:hover {
        cursor: pointer;
      }
    }
    .ant-menu-submenu {
      :hover {
        background-color: ${(props) => props.color};
      }
    }
  }
  .ant-menu-submenu-inline {
    svg {
      scale: 1.6;
      margin-right: 4px;
    }
  }
  .setting__button {
    position: absolute;
    right: 0;
    top: 330px;
    z-index: 2;
    width: 50px;
    cursor: pointer;
  }
`;

const CustomSidebar = styled(Sider)`
  height: 100vh;
  background-color: ${(props) => props.color};

  .ant-menu-submenu-arrow {
    color: #fff;
  }

  position: relative;
  .ant-menu-title-content {
    font-size: 16px !important;
  }
  .ant-menu-inline {
    .ant-menu-item {
      :hover {
      }
    }
    .ant-menu-submenu-title {
      .ant-menu-title-content {
      }
    }
  }
  .button__collapse__left {
    width: 49px;
    position: absolute;
    right: 0;
    top: 14px;
    &:hover {
      cursor: pointer;
    }
  }
  .ant-layout-sider-trigger {
    display: none;
  }
  .button__collapse__right {
    display: none;
  }
  .logo__base {
    height: 80px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    z-index: 1;
    img {
      width: 52px;
      position: absolute;
      top: 39px;
      left: 136px;
    }
  }
  ul {
    background-color: ${(props) => props.color};
    border-right: none;
  }

  .ant-menu-item-selected {
    color: ${(props) => props.color};
    background-color: ${(props) => props.theme.white} !important;
    border-radius: 30px 0px 0px 30px;
    opacity: 1;
    path {
      fill: ${(props) => props.hover};
    }
    &:hover {
      color: ${(props) => props.hover}!important;
      svg {
        path {
          fill: ${(props) => props.hover};
        }
      }
    }
  }

  .ant-menu-submenu-selected {
    color: ${(props) => props.theme.main};
    background-color: ${(props) => props.theme.color} !important;
    border-radius: 30px 0px 0px 30px;
    opacity: 1;
    &:hover {
      color: ${(props) => props.theme.main}!important;
    }
    .ant-menu-submenu-title:hover {
      color: ${(props) => props.theme.main}!important;
    }
  }

  .ant-menu-submenu-title:hover {
    color: ${(props) => props.theme.white} !important;
    .ant-menu-submenu-arrow {
      color: ${(props) => props.theme.white};
    }
  }
  .ant-menu-submenu-title {
    padding-top: 0 !important;
    padding-bottom: 0 !important;
    color: ${(props) => props.theme.white};
  }

  .ant-menu-item-selected::after {
    display: none;
  }
  .line-top {
    width: 275px;
    margin-top: 111px;
    display: none;
  }
  .line-bot {
    width: 275px;
    margin-top: 18px;
  }

  .ant-menu-submenu-popup {
    .ant-menu-submenu-title {
      background-color: red !important;
    }
  }
`;
const MoreInfo = styled.div`
  position: absolute;
  bottom: 50px;
  left: 0;
  width: 100%;
`;
const CustomLanguage = styled(Select)`
  width: 180px;
  margin-bottom: 30px;
  margin-left: 38px;
`;
const CustomVersion = styled.div`
  width: 100%;
  font-size: 17px;
  line-height: 20px;
  color: #ececec;
  text-align: center;
  cursor: pointer;
  p {
    margin-bottom: 0px;
  }
`;

const CustomMenu = styled(Menu)`
  margin-top: 18px;
  padding-left: ${(props) => (props.collapsed ? "16px" : "30px")};
  ul {
    background-color: ${(props) => props.color};
  }
  .ant-menu-item {
    margin: 0 !important;
    padding: 20px 0;
  }
  li {
    overflow: hidden;
  }
`;

const MenuItem = styled(Menu.Item)`
  font-family: var(--roboto-400);
  display: flex;
  align-items: center;
  letter-spacing: 0.01em;
  color: ${(props) => props.theme.white};
  border-radius: 30px 0px 0px 30px;
  &:hover {
    background-color: ${(props) => {
      return props.hover;
    }};

    color: ${(props) => props.theme.white} !important;
    overflow: hidden;

    path {
      fill: ${(props) => props.theme.white};
    }
  }
  .ant-menu-title-content {
    font-size: 16px;
    line-height: 22px;
  }
`;

const CustomMenuSubItem = styled(Menu.Item)`
  border-radius: 30px 0px 0px 30px;
  overflow: hidden;
  .ant-menu-submenu-arrow {
    color: #fff;
  }
  color: ${(props) => props.theme.main} !important;
  &:hover {
    color: ${(props) => props.theme.main} !important;
  }
  .ant-menu-submenu-open {
    background: #fff;
  }
`;

const SubmenuCustomItem = styled(SubMenu)`
  ul {
    max-height: unset !important;
  }
  .ant-menu-submenu-arrow {
    color: ${(props) =>
      props.collapsed ? props.theme.main : "#fff"} !important;
  }
  &:hover {
    .ant-menu-submenu-arrow {
      color: #fff !important;
    }
  }
  .ant-menu-submenu-open {
    /* color: ${(props) => props.theme.main} !important; */
    color: #fff;
    font-size: 16px;
    line-height: 22px;
  }
  .ant-menu-submenu-title {
    color: ${(props) => (props.collapsed ? props.theme.main : "#fff")};
    font-size: 16px;
    line-height: 22px;
  }

  .custom-menu {
    .ant-menu-title-content {
      color: #fff;
      font-size: 16px;
      line-height: 22px;
    }
  }
  .ant-menu-submenu-title:hover {
    color: ${(props) => props.theme.white} !important;
    background-color: ${(props) => props.theme.darker} !important;
  }
`;

const SubmenuCustom = styled(SubMenu)`
  display: ${(props) => (!props.collapsed ? "none" : "unset")} !important;

  .ant-menu-item {
    padding-left: 48px !important;
  }
  .ant-menu-title-content {
    font-size: 16px;
    line-height: 22px;
  }
  ul {
    max-height: calc(100vh - 470px);
    overflow-y: auto;
    overflow-x: hidden;
    .ant-menu-submenu-title {
      ::before {
        position: absolute;
        top: 15px;
        left: 20px;
        content: "";
        width: 10px;
        height: 10px;
        border: 1px solid #fff;
        border-radius: 50%;
        z-index: 4;
      }
    }
  }

  .ant-menu-submenu-title {
    border-radius: 30px 0px 0px 30px;
    margin: 0px;

    &:hover {
      background-color: ${(props) => {
        return props.hover;
      }};

      color: ${(props) => props.theme.white} !important;
      overflow: hidden;

      path {
        fill: ${(props) => (props.theme.name === "white" ? "#fff" : "")};
      }
    }
  }
  letter-spacing: 0.01em;
  color: ${(props) => props.theme.color};

  .ant-menu-sub.ant-menu-inline {
    background: ${(props) => props.theme.main};
  }

  .ant-menu-submenu-open {
    .ant-menu-submenu-title {
      background-color: ${(props) =>
        props.collapsed ? props.theme.main : "#fff"} !important;
      color: ${(props) => props.theme.main} !important;
      ::before {
        border: 1px solid ${(props) => props.theme.main};
      }
    }

    .ant-menu-submenu-arrow {
      color: ${(props) => props.theme.main} !important;
    }
  }
`;
const SubmenuCustom2 = styled(SubMenu)`
  .ant-menu-item {
    padding-left: 48px !important;
  }
  .ant-menu-title-content {
    font-size: 16px;
    line-height: 22px;
  }
  ul {
    max-height: calc(100vh - 470px);
    overflow-y: auto;
    overflow-x: hidden;
    .ant-menu-submenu-title {
      ::before {
        position: absolute;
        top: 15px;
        left: 20px;
        content: "";
        width: 10px;
        height: 10px;
        border: 1px solid #fff;
        border-radius: 50%;
        z-index: 4;
      }
    }
  }

  .ant-menu-submenu-title {
    border-radius: 30px 0px 0px 30px;
    margin: 0px;

    &:hover {
      background-color: ${(props) => {
        return props.hover;
      }};

      color: ${(props) => props.theme.white} !important;
      overflow: hidden;

      path {
        fill: ${(props) => (props.theme.name === "white" ? "#fff" : "")};
      }
    }
  }
  letter-spacing: 0.01em;
  color: ${(props) => props.theme.color};

  .ant-menu-sub.ant-menu-inline {
    background: ${(props) => props.theme.main};
  }

  .ant-menu-submenu-open {
    .ant-menu-submenu-title {
      background-color: ${(props) =>
        props.collapsed ? props.theme.main : "#fff"} !important;
      color: ${(props) => props.theme.main} !important;
      ::before {
        border: 1px solid ${(props) => props.theme.main};
      }
    }

    .ant-menu-submenu-arrow {
      color: ${(props) => props.theme.main} !important;
    }
  }
`;

const CustomContent = styled(Content)`
  background-color: ${(props) =>
    window.location.pathname.includes("dashboard") ||
    window.location.pathname.includes("/objects/") ||
    window.location.pathname.includes("/kanban-view/") ||
    window.location.pathname.includes("/knowledge-base-enhancement/")
      ? "#F9FAFC"
      : window.location.pathname.includes("call-view")
      ? "#fff"
      : "#f2f8ff"};
  height: calc(100vh - 130px);
  z-index: 1;
  overflow-y: scroll;

  &.QMView {
    overflow: hidden;
  }
  /* padding-bottom: 50px; */
`;

const CustomHeader = styled(Header)`
  box-shadow: 0px 4px 10px rgb(0 0 0 / 5%);
  z-index: 2;
  background-color: ${(props) => props.theme.white};
  height: 80px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-right: 16px;
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* identical to box height, or 144% */

    /* Character/Body text */

    color: #2c2c2c;
  }
  .search__icon {
    width: 26px;
    margin-left: 56px;
  }
  .ant-input {
    border: none;
    &::placeholder {
      font-family: var(--roboto-400);
      font-size: 20px;
      line-height: 23px;
      display: flex;
      align-items: center;
      letter-spacing: 0.01em;
    }
  }
  .ant-input-focused {
    border: none;
    box-shadow: 0 0 0 2px rgb(24 144 255 / 0%);
  }
  .ant-input:focus {
    box-shadow: 0 0 0 2px rgb(24 144 255 / 0%);
  }
  .language__config {
    width: 49px;
    margin-right: 48px;
    cursor: pointer;
  }
  .notification {
    width: 36px;
    cursor: pointer;
    margin-right: 48px;
  }
  .avatar {
    width: 40px !important;
    height: 40px !important;
    border-radius: 50% !important;
    border: 2px solid #fff;
    margin-right: 12px;
    cursor: pointer;
    transition: all 0.5s;
    /* :hover {
      transform: scale(1.2);
    } */
  }
`;

// const CustomFooter = styled.div`
//   background-color: ${(props) => props.theme.white};
//   height: 50px;
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   position: fixed;
//   bottom: 0;
//   font-family: var(--roboto-400);
//   font-size: 14px;
//   line-height: 16px;
//   display: flex;
//   align-items: center;
//   letter-spacing: 0.01em;
//   width: 100%;
//   z-index: 1;
//   /* text xam */

//   color: #605f5f;
//   box-shadow: 0px -2px 16px rgba(0, 0, 0, 0.05);
// `;

const CustomSignOut = styled.p`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 6px;
  padding: 5px 16px;
  transition: all 0.5s;
  border: 1px solid transparent;
  &:hover {
    /* text-decoration: underline; */
    background: #ebebeb;
    border-radius: 5px;
    border-color: #ebebeb;
  }
  span {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
    margin-left: 6px;
  }
`;
const CustomRow = styled(Row)`
  canvas {
    border-radius: 50% !important;
    width: 100% !important;
    height: 100% !important;
  }
`;

const CustomCol = styled(Col)`
  border: 1px solid #d9d9d9;
  text-align: center;
  cursor: pointer;
  padding: 5px 16px;
  border-radius: 5px;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  transition: all 0.5s;
  &:hover {
    background: #ebebeb;
    border-color: #ebebeb;
  }
`;

const InfoUser = styled.div`
  display: flex;
  align-items: center;
  line-height: 24px;
  background: #e1e5f1;
  border-radius: 52px;
  transition: all 0.5s;
  padding: 4px 16px 4px 4px;
  min-width: 208px;
  max-width: 271px;

  cursor: pointer;
  :hover {
    background: #d4d8e5;
  }
`;

const InfoDetail = styled.div`
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  margin-bottom: 6px;
  span {
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 22px;
    color: #000000;
  }
`;

const ICStatus = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  border-right: 1px solid #e1e5f1;
  border-left: 1px solid #e1e5f1;
  padding: 0 16px;
  height: 28px;
  margin-left: 12px;

  span {
    font-family: var(--roboto-500);
    font-size: 14px;
    color: #6b6b6b;
    position: relative;

    ::before {
      width: 8px;
      height: 8px;
      background: ${({ status }) =>
        status === "available" ? "#0BB865" : "#CF1322"};
      border-radius: 50%;
      content: "";
      position: absolute;
      top: 27px;
      left: -12px;
    }
  }
`;

const UserStatus = styled.div`
  display: flex;
  align-items: center;
  margin-left: 12px;
  cursor: pointer;

  span {
    font-family: var(--roboto-500);
    font-size: 14px;
    color: #6b6b6b;
    position: relative;

    ::before {
      width: 8px;
      height: 8px;
      background: ${({ status }) =>
        status === "online"
          ? "#0BB865"
          : status === "busy"
          ? "#FAAD14"
          : "#CF1322"};
      border-radius: 50%;
      content: "";
      position: absolute;
      top: 0px;
      left: -12px;
    }
  }
`;

const CustomStatus = styled(Menu.Item)`
  position: relative;
  padding-left: 24px;
  font-family: var(--roboto-500);
  font-size: 16px;
  line-height: 21px;

  ::before {
    width: 8px;
    height: 8px;
    background: ${({ status }) =>
      status === "online" || status === "available"
        ? "#0BB865"
        : status === "busy"
        ? "#FAAD14"
        : "#CF1322"};
    color: ${({ status }) =>
      status === "online" || status === "available"
        ? "#0BB865"
        : status === "busy"
        ? "#FAAD14"
        : "#CF1322"};
    border-radius: 50%;
    content: "";
    position: absolute;
    top: 11px;
    left: 12px;
  }
`;

export default withTranslation()(LayoutTemplate);
