import { Drawer } from "antd";
import React, { useState } from "react";
import styled from "styled-components";
import sun from "../../assets/icons/layout/sun.png";
import moon from "../../assets/icons/layout/moon.png";
import defaultColor from "../../assets/icons/layout/default-color.png";
import fullscreenImg from "../../assets/icons/layout/fullscreen.png";
import useSettings from "../../hooks/useSettings";

const MenuConfig = (props) => {
  //   const [visible, setVisible] = useState(false);
  const { onChangeColor } = useSettings();
  const { visible, setVisible } = props;
  const onClose = () => {
    setVisible(false);
  };

  const [, setFullscreen] = useState(false);

  const toggleFullScreen = () => {
    if (!document.fullscreenElement) {
      document.documentElement.requestFullscreen();
      setFullscreen(true);
    } else if (document.exitFullscreen) {
      document.exitFullscreen();
      setFullscreen(false);
    }
  };

  return (
    <>
      <CustomDrawer
        title="Settings"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={260}
      >
        <div>
          <Mode>Mode</Mode>
          <ThemeMode>
            <Light>
              <img alt="" src={sun} />
            </Light>
            <Dark>
              <img alt="" src={moon} />
            </Dark>
          </ThemeMode>
          <ColorTitle>Color</ColorTitle>
          <ColorMode>
            <ColorItem
              onClick={() => {
                onChangeColor("default");
              }}
            >
              <img alt="" src={defaultColor} />
            </ColorItem>
            <ColorItem onClick={() => onChangeColor("white")}>
              {/* <img alt="" src={defaultColor} /> */}
            </ColorItem>
          </ColorMode>
          <FullScreen
            onClick={() => {
              toggleFullScreen();
            }}
          >
            <img alt="" src={fullscreenImg} />
            <span>Fullscreen</span>
          </FullScreen>
        </div>
      </CustomDrawer>
    </>
  );
};

const CustomDrawer = styled(Drawer)`
  .ant-drawer-content-wrapper {
    height: 442px;
    top: 50%;
    transform: translate(0, -50%);

    .ant-drawer-content {
      border-radius: 10px;
      box-shadow: 0px 4px 12px rgba(81, 137, 143, 0.16);
    }
  }
`;

const FullScreen = styled.div`
  width: 212px;
  height: 48px;
  border: 1px solid #ececec;
  box-sizing: border-box;
  border-radius: 5px;
  font-size: 14px;
  line-height: 16px;
  letter-spacing: -0.02em;
  cursor: pointer;
  color: #545454;
  img {
    width: 15px;
    margin-right: 18px;
  }
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 32px;
`;

const ColorMode = styled.div`
  display: flex;
`;

const ColorItem = styled.div`
  background: #ffffff;
  /* vien xam */

  border: 1px solid #ececec;
  box-sizing: border-box;
  /* sh */

  box-shadow: 0px 4px 12px rgba(81, 137, 143, 0.16);
  border-radius: 5px;
  width: 60px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;
  margin-right: 10px;
  cursor: pointer;
  img {
    width: 32px;
  }
`;

const Mode = styled.div`
  font-size: 16px;
  line-height: 19px;
  /* identical to box height */

  letter-spacing: -0.02em;

  /* text - main */

  color: #545454;
  margin-bottom: 16px;
`;

const ColorTitle = styled.div`
  font-size: 16px;
  line-height: 19px;
  /* identical to box height */

  letter-spacing: -0.02em;

  /* text - main */

  color: #545454;
  margin-top: 30px;
`;

const ThemeMode = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Light = styled.div`
  background: #ffffff;
  /* vien xam */

  border: 1px solid #ececec;
  box-sizing: border-box;
  /* sh */
  cursor: pointer;
  box-shadow: 0px 4px 12px rgba(81, 137, 143, 0.16);
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 98px;
  height: 89px;
  img {
    width: 16px;
  }
`;

const Dark = styled.div`
  background: #161c24;
  border: 1px solid #161c24;
  box-sizing: border-box;
  /* sh */
  cursor: pointer;
  box-shadow: 0px 4px 12px rgba(81, 137, 143, 0.16);
  border-radius: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 98px;
  height: 89px;
  img {
    width: 16px;
  }
`;

export default MenuConfig;
