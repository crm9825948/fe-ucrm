import { useEffect, useState, useRef } from "react";
import styled from "styled-components/macro";
import { useNavigate, useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Steps from "antd/lib/steps";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Breadcrumb from "antd/lib/breadcrumb";
import Tooltip from "antd/lib/tooltip";

import { Notification } from "components/Notification/Noti";
import Information from "./Setting/information";
import RuleSetting from "./Setting/ruleSetting";
import EscalateRule from "./Setting/escalateRule";

import Text from "pages/SLASetting/FieldType/text";
import SelectType from "pages/SLASetting/FieldType/select";
import Number from "pages/SLASetting/FieldType/number";
import Date from "pages/SLASetting/FieldType/date";

import { loadObject } from "redux/slices/objectsManagement";
import { loadDataNecessaryEdit } from "redux/slices/slaSetting";
import {
  createSLASetting,
  loadDetailsSLA,
  editSLASetting,
  loadDetailsSLASuccess,
} from "redux/slices/slaSetting";

function DetailsSLASetting(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();
  const { pathname } = useLocation();

  const [form] = Form.useForm();
  const { Step } = Steps;

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listAllUser } = useSelector((state) => state.userReducer);
  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );
  const { detailsSLA, isEdit } = useSelector(
    (state) => state.slaSettingReducer
  );

  useEffect(() => {
    if (pathname.split("/").includes("edit-sla-setting")) {
      dispatch(
        loadDetailsSLA({
          _id: recordID,
        })
      );
    }
  }, [dispatch, pathname, recordID]);

  const pageRef = useRef(null);

  const [currentStep, setCurrentStep] = useState(0);
  const [listUser, setListUser] = useState([]);
  const [listUserEmail, setListUserEmail] = useState([]);
  const [listTemplate, setListTemplate] = useState([]);
  const [listFields, setListFields] = useState([]);

  //information
  const [type, setType] = useState("calendar");
  const [dataBusinessHours, $dataBusinessHours] = useState({
    holidays: [],
    shifts: [],
    workingDays: [],
  });

  //ruleSetting
  const [rules, setRules] = useState([]);
  const [operatorValueAndCreated, setOperatorValueAndCreated] = useState([]);
  const [operatorValueAndSuccess, setOperatorValueAndSuccess] = useState([]);
  const [operatorValueAndPending, setOperatorValueAndPending] = useState([]);
  const [valueAndCreated, setValueAndCreated] = useState([]);
  const [valueAndSuccess, setValueAndSuccess] = useState([]);
  const [valueAndPending, setValueAndPending] = useState([]);
  const [emailValid, setEmailValid] = useState(true);
  const [operatorValueOrCreated, setOperatorValueOrCreated] = useState([]);
  const [operatorValueOrSuccess, setOperatorValueOrSuccess] = useState([]);
  const [operatorValueOrPending, setOperatorValueOrPending] = useState([]);
  const [valueOrCreated, setValueOrCreated] = useState([]);
  const [valueOrSuccess, setValueOrSuccess] = useState([]);
  const [valueOrPending, setValueOrPending] = useState([]);

  //escalateRule
  const [escalateRules, setEscalateRules] = useState([]);
  const [emailEscalateValid, setEmailEscalateValid] = useState(true);

  const _onSubmit = () => {
    let checkRequired = false;
    if (escalateRules.length > 0) {
      if (
        escalateRules.find(
          (item) =>
            (item.time_after_violation.days === 0 &&
              item.time_after_violation.hours === 0 &&
              item.time_after_violation.minutes === 0) ||
            item.title === "" ||
            item.title === undefined
        )
      ) {
        checkRequired = true;
      }
    }

    if (checkRequired) {
      Notification("warning", "Please full fields required!");
    } else if (!emailEscalateValid) {
      Notification("warning", "Please input valid email!");
    } else {
      let resultShifts = [];
      dataBusinessHours.shifts.forEach((shift) => {
        let applicable_weekdays = [
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
        ];

        shift.day.forEach((item) => {
          applicable_weekdays[item] = "True";
        });
        resultShifts.push({
          from_time: {
            hour: parseInt(shift.time[0].slice(0, 2)),
            minute: parseInt(shift.time[0].slice(3, 5)),
          },
          to_time: {
            hour: parseInt(shift.time[1].slice(0, 2)),
            minute: parseInt(shift.time[1].slice(3, 5)),
          },
          applicable_weekdays: [...applicable_weekdays],
        });
      });

      let resultRules = [];

      if (rules.length > 0) {
        rules.forEach((item, idx) => {
          if (_.get(item, "is_fixed_hour")) {
            resultRules.push({
              ...item,
              time_to_completion: 1,
            });
          } else {
            resultRules.push({
              ...item,
              time_to_completion:
                parseFloat(item.time_to_completion.days) * 60 * 24 +
                parseFloat(item.time_to_completion.hours) * 60 +
                parseFloat(item.time_to_completion.minutes),
            });
          }

          let resultAlert = [];
          if (item.alert_configs.length > 0) {
            item.alert_configs.forEach((alert) => {
              resultAlert.push({
                ...alert,
                time_before_violation:
                  parseFloat(alert.time_before_violation.days) * 60 * 24 +
                  parseFloat(alert.time_before_violation.hours) * 60 +
                  parseFloat(alert.time_before_violation.minutes),
              });
            });
          }

          resultRules[idx] = {
            ...resultRules[idx],
            alert_configs: resultAlert,
          };
        });
      }

      let resultEscalteRules = [];

      if (escalateRules.length > 0) {
        escalateRules.forEach((item) => {
          resultEscalteRules.push({
            ...item,
            time_after_violation:
              parseFloat(item.time_after_violation.days) * 60 * 24 +
              parseFloat(item.time_after_violation.hours) * 60 +
              parseFloat(item.time_after_violation.minutes),
          });
        });
      }

      let data = {
        title: form.getFieldValue("sla_name"),
        description: form.getFieldValue("description"),
        object_id: form.getFieldValue("object"),
        sla_type: type,
        working_days:
          type === "business_hours" ? form.getFieldValue("working_day") : [],
        holidays: type === "business_hours" ? dataBusinessHours.holidays : [],
        shift_work: type === "business_hours" ? resultShifts : [],
        sla_rules: resultRules,
        escalate_rules: resultEscalteRules,
      };

      if (Object.keys(detailsSLA).length > 0) {
        dispatch(
          editSLASetting({
            ...data,
            _id: detailsSLA._id,
          })
        );
      } else {
        dispatch(createSLASetting(data));
      }
    }
  };

  const _onNext = () => {
    if (currentStep === 0) {
      if (
        form.getFieldValue("object") !== undefined &&
        form.getFieldValue("sla_name") !== undefined &&
        form.getFieldValue("sla_name") !== ""
      ) {
        if (type === "business_hours") {
          if (
            dataBusinessHours.workingDays.length > 0 &&
            dataBusinessHours.shifts.length > 0 &&
            dataBusinessHours.shifts.every((shift) => shift.time.length !== 0)
          ) {
            setCurrentStep(currentStep + 1);
            pageRef.current?.scrollIntoView();
          } else {
            Notification("warning", "Please full fields required!");
          }
        } else {
          setCurrentStep(currentStep + 1);
          pageRef.current?.scrollIntoView();
        }
      } else {
        Notification("warning", "Please full fields required!");
      }
    }

    if (currentStep === 1) {
      //Check conditions
      let flag = false;

      rules.forEach((ele, index) => {
        //operator
        if (
          operatorValueAndCreated[index] &&
          operatorValueAndCreated[index].operator &&
          operatorValueAndCreated[index].operator.length ===
            ele.created_conditions.and_conditions.length
        ) {
          operatorValueAndCreated[index].operator.forEach((item) => {
            if (item === undefined) {
              flag = true;
            }
          });
        } else {
          flag = true;
        }
        if (
          operatorValueAndSuccess[index] &&
          operatorValueAndSuccess[index].operator &&
          operatorValueAndSuccess[index].operator.length ===
            ele.success_conditions.and_conditions.length
        ) {
          operatorValueAndSuccess[index].operator.forEach((item) => {
            if (item === undefined) {
              flag = true;
            }
          });
        } else {
          flag = true;
        }

        if (
          operatorValueAndPending[index] &&
          operatorValueAndPending[index].operator &&
          operatorValueAndPending[index].operator.length ===
            ele.pending_conditions.and_conditions.length
        ) {
          operatorValueAndPending[index].operator.forEach((item) => {
            if (item === undefined) {
              flag = true;
            }
          });
        } else {
          flag = true;
        }
        //value
        valueAndCreated[index] &&
          valueAndCreated[index].value &&
          valueAndCreated[index].value.forEach((item, idx) => {
            if (
              item === "" &&
              operatorValueAndCreated[index].operator[idx] !== "empty" &&
              operatorValueAndCreated[index].operator[idx] !== "not-empty" &&
              operatorValueAndCreated[index].operator[idx] !== "mine" &&
              operatorValueAndCreated[index].operator[idx] !== "not-mine" &&
              operatorValueAndCreated[index].operator[idx] !== "today" &&
              operatorValueAndCreated[index].operator[idx] !== "not-today" &&
              operatorValueAndCreated[index].operator[idx] !== "yesterday" &&
              operatorValueAndCreated[index].operator[idx] !== "this-week" &&
              operatorValueAndCreated[index].operator[idx] !== "last-week" &&
              operatorValueAndCreated[index].operator[idx] !== "this-month" &&
              operatorValueAndCreated[index].operator[idx] !== "last-month" &&
              operatorValueAndCreated[index].operator[idx] !== "this-year"
            ) {
              flag = true;
            }
            if (item === undefined) {
              flag = true;
            }
          });

        valueAndSuccess[index] &&
          valueAndSuccess[index].value &&
          valueAndSuccess[index].value.forEach((item, idx) => {
            if (
              item === "" &&
              operatorValueAndSuccess[index].operator[idx] !== "empty" &&
              operatorValueAndSuccess[index].operator[idx] !== "not-empty" &&
              operatorValueAndSuccess[index].operator[idx] !== "mine" &&
              operatorValueAndSuccess[index].operator[idx] !== "not-mine" &&
              operatorValueAndSuccess[index].operator[idx] !== "today" &&
              operatorValueAndSuccess[index].operator[idx] !== "not-today" &&
              operatorValueAndSuccess[index].operator[idx] !== "yesterday" &&
              operatorValueAndSuccess[index].operator[idx] !== "this-week" &&
              operatorValueAndSuccess[index].operator[idx] !== "last-week" &&
              operatorValueAndSuccess[index].operator[idx] !== "this-month" &&
              operatorValueAndSuccess[index].operator[idx] !== "last-month" &&
              operatorValueAndSuccess[index].operator[idx] !== "this-year"
            ) {
              flag = true;
            }
            if (item === undefined) {
              flag = true;
            }
          });

        valueAndPending[index] &&
          valueAndPending[index].value &&
          valueAndPending[index].value.forEach((item, idx) => {
            if (
              item === "" &&
              operatorValueAndPending[index].operator[idx] !== "empty" &&
              operatorValueAndPending[index].operator[idx] !== "not-empty" &&
              operatorValueAndPending[index].operator[idx] !== "mine" &&
              operatorValueAndPending[index].operator[idx] !== "not-mine" &&
              operatorValueAndPending[index].operator[idx] !== "today" &&
              operatorValueAndPending[index].operator[idx] !== "not-today" &&
              operatorValueAndPending[index].operator[idx] !== "yesterday" &&
              operatorValueAndPending[index].operator[idx] !== "this-week" &&
              operatorValueAndPending[index].operator[idx] !== "last-week" &&
              operatorValueAndPending[index].operator[idx] !== "this-month" &&
              operatorValueAndPending[index].operator[idx] !== "last-month" &&
              operatorValueAndPending[index].operator[idx] !== "this-year"
            ) {
              flag = true;
            }
            if (item === undefined) {
              flag = true;
            }
          });
      });

      const emptyInitAction = rules.find((ele) =>
        ele.init_action?.find((action) => action.field_id === "")
      );

      const emptyFailureAction = rules.find((ele) =>
        ele.failure_action.find((action) => action.field_id === "")
      );

      const emptySuccessAction = rules.find((ele) =>
        ele.success_action.find((action) => action.field_id === "")
      );

      //checkEmptyAlert
      let emptyAlert = false;
      rules.forEach((item) => {
        item.alert_configs.forEach((alert) => {
          if (
            (alert.time_before_violation.days === 0 &&
              alert.time_before_violation.hours === 0 &&
              alert.time_before_violation.minutes === 0) ||
            alert.send_email_to.length === 0 ||
            !alert.email_template_id
          ) {
            emptyAlert = true;
          }
        });
      });

      if (flag) {
        Notification("warning", "Please fullfill conditions!");
      } else if (
        rules.find(
          (item) =>
            item.time_to_completion.days === 0 &&
            item.time_to_completion.hours === 0 &&
            item.time_to_completion.minutes === 0 &&
            !_.get(item, "is_fixed_hour")
        )
      ) {
        Notification("warning", "Time to completion must be greater than 0!");
      } else if (
        rules.find(
          (item) =>
            _.get(item, "fixed_hour.hour") === 0 &&
            _.get(item, "fixed_hour.minute") === 0 &&
            _.get(item, "is_fixed_hour")
        )
      ) {
        Notification("warning", "Time to completion must be greater than 0!");
      } else if (!emailValid) {
        Notification("warning", "Please input valid email!");
      } else if (emptyFailureAction) {
        Notification(
          "warning",
          "Please fullfill or remove empty Failure actions!"
        );
      } else if (emptyInitAction) {
        Notification(
          "warning",
          "Please fullfill or remove empty Init actions!"
        );
      } else if (emptySuccessAction) {
        Notification(
          "warning",
          "Please fullfill or remove empty Success actions!"
        );
      } else if (emptyAlert) {
        Notification("warning", "Please fullfill alert!");
      } else {
        setCurrentStep(currentStep + 1);
        pageRef.current?.scrollIntoView();
      }
    }
  };

  const _onPrev = () => {
    setCurrentStep(currentStep - 1);
    pageRef.current?.scrollIntoView();
  };

  const handleChangeFieldAction = (
    value,
    indexRule,
    indexAction,
    field,
    type,
    indexAlert
  ) => {
    let tempRules = [...rules];
    switch (type) {
      case "init":
        let tempActionsInit = [...tempRules[indexRule].init_action];

        tempActionsInit[indexAction] = {
          ...tempActionsInit[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          init_action: tempActionsInit,
        };

        setRules(tempRules);

        break;
      case "failure":
        let tempActionsFailure = [...tempRules[indexRule].failure_action];

        tempActionsFailure[indexAction] = {
          ...tempActionsFailure[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          failure_action: tempActionsFailure,
        };

        setRules(tempRules);

        break;
      case "success":
        let tempActionsSuccess = [...tempRules[indexRule].success_action];
        tempActionsSuccess[indexAction] = {
          ...tempActionsSuccess[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          success_action: tempActionsSuccess,
        };

        setRules(tempRules);

        break;
      case "overdue":
        let tempActionsOverdue = [...tempRules[indexRule].overdue_action];
        tempActionsOverdue[indexAction] = {
          ...tempActionsOverdue[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          overdue_action: tempActionsOverdue,
        };

        setRules(tempRules);

        break;
      case "alertAction":
        let tempAlerts = [...tempRules[indexRule].alert_configs];
        let tempActionsAlert = [
          ..._.get(tempAlerts, `[${indexAlert}].alert_action`, []),
        ];

        tempActionsAlert[indexAction] = {
          ...tempActionsAlert[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };

        tempAlerts[indexAlert] = {
          ...tempAlerts[indexAlert],
          alert_action: tempActionsAlert,
        };

        tempRules[indexRule] = {
          ...tempRules[indexRule],
          alert_configs: tempAlerts,
        };

        setRules(tempRules);

        break;
      case "escalateAction":
        let tempEscalateRules = [...escalateRules];
        let tempActions = [...tempEscalateRules[indexRule].escalate_action];
        tempActions[indexAction] = {
          ...tempActions[indexAction],
          field_type: field.type,
          new_value: value,
          is_dynamic:
            field.type === "date" || field.type === "datetime-local"
              ? true
              : false,
        };
        tempEscalateRules[indexRule] = {
          ...tempEscalateRules[indexRule],
          escalate_action: tempActions,
        };

        setEscalateRules(tempEscalateRules);
        break;

      default:
        break;
    }
  };

  const handleFieldType = (field, indexRule, indexAction, type, indexAlert) => {
    if (field) {
      switch (field.type) {
        case "user":
        case "select":
        case "dynamic-field":
          return (
            <SelectType
              initAction={
                rules.length > 0 &&
                type === "init" &&
                rules[indexRule].init_action
              }
              failureAction={
                rules.length > 0 &&
                type === "failure" &&
                rules[indexRule].failure_action
              }
              successAction={
                rules.length > 0 &&
                type === "success" &&
                rules[indexRule].success_action
              }
              overdueAction={
                rules.length > 0 &&
                type === "overdue" &&
                rules[indexRule].overdue_action
              }
              alertAction={
                rules.length > 0 &&
                type === "alertAction" &&
                _.get(
                  rules,
                  `[${indexRule}].alert_configs[${indexAlert}].alert_action`,
                  []
                )
              }
              escalateAction={
                escalateRules.length > 0 &&
                type === "escalateAction" &&
                escalateRules[indexRule].escalate_action
              }
              field={field}
              type={type}
              indexRule={indexRule}
              indexAction={indexAction}
              indexAlert={indexAlert}
              listUser={listUser}
              handleChangeFieldAction={handleChangeFieldAction}
            />
          );
        case "number":
          return (
            <Number
              initAction={
                rules.length > 0 &&
                type === "init" &&
                rules[indexRule].init_action
              }
              failureAction={
                rules.length > 0 &&
                type === "failure" &&
                rules[indexRule].failure_action
              }
              successAction={
                rules.length > 0 &&
                type === "success" &&
                rules[indexRule].success_action
              }
              overdueAction={
                rules.length > 0 &&
                type === "overdue" &&
                rules[indexRule].overdue_action
              }
              alertAction={
                rules.length > 0 &&
                type === "alertAction" &&
                _.get(
                  rules,
                  `[${indexRule}].alert_configs[${indexAlert}].alert_action`,
                  []
                )
              }
              escalateAction={
                escalateRules.length > 0 &&
                type === "escalateAction" &&
                escalateRules[indexRule].escalate_action
              }
              field={field}
              type={type}
              indexRule={indexRule}
              indexAction={indexAction}
              indexAlert={indexAlert}
              handleChangeFieldAction={handleChangeFieldAction}
            />
          );

        case "date":
        case "datetime-local":
          return (
            <Date
              initAction={
                rules.length > 0 &&
                type === "init" &&
                rules[indexRule].init_action
              }
              failureAction={
                rules.length > 0 &&
                type === "failure" &&
                rules[indexRule].failure_action
              }
              successAction={
                rules.length > 0 &&
                type === "success" &&
                rules[indexRule].success_action
              }
              overdueAction={
                rules.length > 0 &&
                type === "overdue" &&
                rules[indexRule].overdue_action
              }
              alertAction={
                rules.length > 0 &&
                type === "alertAction" &&
                _.get(
                  rules,
                  `[${indexRule}].alert_configs[${indexAlert}].alert_action`,
                  []
                )
              }
              escalateAction={
                escalateRules.length > 0 &&
                type === "escalateAction" &&
                escalateRules[indexRule].escalate_action
              }
              field={field}
              type={type}
              indexRule={indexRule}
              indexAlert={indexAlert}
              indexAction={indexAction}
              handleChangeFieldAction={handleChangeFieldAction}
            />
          );
        default:
          return (
            <Text
              initAction={
                rules.length > 0 &&
                type === "init" &&
                rules[indexRule].init_action
              }
              failureAction={
                rules.length > 0 &&
                type === "failure" &&
                rules[indexRule].failure_action
              }
              successAction={
                rules.length > 0 &&
                type === "success" &&
                rules[indexRule].success_action
              }
              alertAction={
                rules.length > 0 &&
                type === "alertAction" &&
                _.get(
                  rules,
                  `[${indexRule}].alert_configs[${indexAlert}].alert_action`,
                  []
                )
              }
              overdueAction={
                rules.length > 0 &&
                type === "overdue" &&
                rules[indexRule].overdue_action
              }
              escalateAction={
                escalateRules.length > 0 &&
                type === "escalateAction" &&
                escalateRules[indexRule].escalate_action
              }
              field={field}
              type={type}
              indexRule={indexRule}
              indexAlert={indexAlert}
              indexAction={indexAction}
              handleChangeFieldAction={handleChangeFieldAction}
            />
          );
      }
    }
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);

    let tempUserEmail = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUserEmail.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item.Email,
        });
      }
    });
    setListUserEmail(tempUserEmail);
  }, [listAllUser]);

  useEffect(() => {
    let tempFields = [];
    fields_related.forEach((item) => {
      if (
        item.type !== "id" &&
        item.type !== "linkingobject" &&
        !_.get(item, "encrypted", false)
      ) {
        tempFields.push({
          label: item.name,
          value: item.ID,
        });
      }
    });

    setListFields(tempFields);
  }, [fields_related]);

  useEffect(() => {
    dispatch(loadObject());
    dispatch(loadDataNecessaryEdit());
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(loadDetailsSLASuccess({}));
    };
  }, [dispatch]);

  return (
    <Wrapper ref={pageRef}>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <Breadcrumb.Item
          onClick={() => {
            navigate("/sla-setting");
          }}
        >
          {t("settings.slaSetting")}
        </Breadcrumb.Item>
        <BreadcrumbItem>
          {Object.keys(detailsSLA).length > 0
            ? t("slaSetting.editSLA")
            : t("slaSetting.addSLA")}
        </BreadcrumbItem>
      </Breadcrumb>

      <WrapSetting>
        <Form
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          colon={false}
          labelAlign="left"
        >
          <Steps current={currentStep}>
            <Step
              title={t("slaSetting.information")}
              description={
                currentStep === 0
                  ? t("common.inprogress")
                  : t("common.finished")
              }
            />
            <Step
              title={t("slaSetting.ruleSetting")}
              description={
                currentStep === 0
                  ? t("common.waiting")
                  : currentStep === 1
                  ? t("common.inprogress")
                  : "Finished"
              }
            />
            <Step
              title={t("slaSetting.escalateRule")}
              description={
                currentStep === 2 ? t("common.inprogress") : t("common.waiting")
              }
            />
          </Steps>

          <Information
            currentStep={currentStep}
            form={form}
            objects={category}
            type={type}
            setType={setType}
            dataBusinessHours={dataBusinessHours}
            $dataBusinessHours={$dataBusinessHours}
            setListTemplate={setListTemplate}
            detailsSLA={detailsSLA}
          />

          <RuleSetting
            currentStep={currentStep}
            listUserEmail={listUserEmail}
            rules={rules}
            setRules={setRules}
            listTemplate={listTemplate}
            fields_related={fields_related}
            listFields={listFields}
            handleFieldType={handleFieldType}
            detailsSLA={detailsSLA}
            isEdit={isEdit}
            form={form}
            operatorValueAndCreated={operatorValueAndCreated}
            setOperatorValueAndCreated={setOperatorValueAndCreated}
            operatorValueAndSuccess={operatorValueAndSuccess}
            setOperatorValueAndSuccess={setOperatorValueAndSuccess}
            operatorValueAndPending={operatorValueAndPending}
            setOperatorValueAndPending={setOperatorValueAndPending}
            valueAndCreated={valueAndCreated}
            setValueAndCreated={setValueAndCreated}
            valueAndSuccess={valueAndSuccess}
            setValueAndSuccess={setValueAndSuccess}
            valueAndPending={valueAndPending}
            setValueAndPending={setValueAndPending}
            setEmailValid={setEmailValid}
            operatorValueOrCreated={operatorValueOrCreated}
            setOperatorValueOrCreated={setOperatorValueOrCreated}
            operatorValueOrSuccess={operatorValueOrSuccess}
            setOperatorValueOrSuccess={setOperatorValueOrSuccess}
            operatorValueOrPending={operatorValueOrPending}
            setOperatorValueOrPending={setOperatorValueOrPending}
            valueOrCreated={valueOrCreated}
            setValueOrCreated={setValueOrCreated}
            valueOrSuccess={valueOrSuccess}
            setValueOrSuccess={setValueOrSuccess}
            valueOrPending={valueOrPending}
            setValueOrPending={setValueOrPending}
          />

          <EscalateRule
            currentStep={currentStep}
            escalateRules={escalateRules}
            setEscalateRules={setEscalateRules}
            listUserEmail={listUserEmail}
            listTemplate={listTemplate}
            fields_related={fields_related}
            listFields={listFields}
            handleFieldType={handleFieldType}
            detailsSLA={detailsSLA}
            isEdit={isEdit}
            setEmailEscalateValid={setEmailEscalateValid}
          />

          {/* {currentStep === 0 ? (
            <Information
              form={form}
              objects={objects}
              type={type}
              setType={setType}
              workingDays={workingDays}
              setWorkingDays={setWorkingDays}
              holidays={holidays}
              setHolidays={setHolidays}
              shifts={shifts}
              setShifts={setShifts}
            />
          ) : currentStep === 1 ? (
            <RuleSetting
              listUser={listUser}
              rules={rules}
              setRules={setRules}
              allCreatedCondition={allCreatedCondition}
              setAllCreatedCondition={setAllCreatedCondition}
              allSuccessCondition={allSuccessCondition}
              setAllSuccessCondition={setAllSuccessCondition}
              allPendingCondition={allPendingCondition}
              setAllPendingCondition={setAllPendingCondition}
              operatorValuePage={operatorValuePage}
              setOperatorValuePage={setOperatorValuePage}
              valuePage={valuePage}
              setValuePage={setValuePage}
              typePage={typePage}
              setTypePage={setTypePage}
              fieldPage={fieldPage}
              setFieldPage={setFieldPage}
            />
          ) : (
            <EscalateRule />
          )} */}
          <WrapButton>
            {currentStep === 0 && (
              <Back onClick={() => navigate("/sla-setting")}>
                {t("common.cancel")}
              </Back>
            )}
            {currentStep > 0 && (
              <Back onClick={() => _onPrev()}>{t("common.back")}</Back>
            )}
            {currentStep === 2 ? (
              <>
                {_.get(detailsSLA, "status", false) ? (
                  <Tooltip title="Cannot edit an Active SLA Setting.">
                    <Button
                      disabled
                      type="primary"
                      htmlType="submit"
                      // loading={isLoading}
                    >
                      {t("common.save")}
                    </Button>
                  </Tooltip>
                ) : (
                  <Button
                    type="primary"
                    htmlType="submit"
                    // loading={isLoading}
                    onClick={() => _onSubmit()}
                  >
                    {t("common.save")}
                  </Button>
                )}
              </>
            ) : (
              <Button
                type="primary"
                onClick={() => _onNext()}
                disabled={
                  currentStep === 1 &&
                  (rules.length === 0 ||
                    rules.find(
                      (ele) =>
                        ele.success_conditions.and_conditions.length === 0
                    ))
                }
              >
                {t("common.next")}
              </Button>
            )}
          </WrapButton>
        </Form>
      </WrapSetting>
    </Wrapper>
  );
}

export default withTranslation()(DetailsSLASetting);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-steps {
    margin-bottom: 24px;
  }

  .ant-steps-item-process .ant-steps-item-icon {
    background: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-steps-item-title::after {
    background-color: ${(props) => props.theme.main}!important;
  }

  .ant-steps-item-finish .ant-steps-item-icon {
    border-color: ${(props) => props.theme.main};

    > .ant-steps-icon {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapSetting = styled.div`
  background: #fff;
  border-radius: 5px;
  margin-top: 20px;

  padding: 2.5rem 2rem;

  @media screen and (min-width: 1600px) {
    padding: 2.5rem 10rem;
  }
`;

const WrapButton = styled.div`
  margin-top: 30px;
  display: flex;
  justify-content: center;

  .ant-btn {
    font-size: 16px;
    height: unset;
    width: 8.125rem;

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    margin: 0 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    background: #f5f5f5;
    border: 1px solid #d9d9d9;
    color: rgba(0, 0, 0, 0.25);

    :hover {
      background: #f5f5f5 !important;
      border: 1px solid #d9d9d9 !important;
      color: rgba(0, 0, 0, 0.25) !important;
    }
  }
`;

const Back = styled(Button)`
  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;
