import { useState, useEffect } from "react";
import styled from "styled-components";
import { t } from "i18next";
import moment from "moment";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import DatePicker from "antd/lib/date-picker";
import TimePicker from "antd/lib/time-picker";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Checkbox from "antd/lib/checkbox";
import Modal from "antd/lib/modal";
import Collapse from "antd/lib/collapse";
import { week } from "util/staticData";

import Delete from "assets/icons/SLA/delete.svg";
import Icon from "components/Icon/Icon";
const { Panel } = Collapse;

const checkDuplicate = (arr) => {
  return new Set(arr).size !== arr.length;
};

const { Option } = Select;

function BusinessHours({
  form,
  dataBusinessHours,
  $dataBusinessHours,
  required = true,
}) {
  const [modalShift, setModalShift] = useState(false);
  const [dataShift, setDataShift] = useState({});

  const handleSelectWorkingDay = (value) => {
    $dataBusinessHours({
      ...dataBusinessHours,
      workingDays: value,
    });
  };

  const handleSelectHoliday = (value, values) => {
    if (values !== "") {
      if (dataBusinessHours.shifts.length === 8) {
        Notification("warning", "Max shifts, please delete one!");
      } else {
        let tempHolidays = [...dataBusinessHours.holidays, values];
        if (checkDuplicate(tempHolidays)) {
          Notification("warning", "Date exist!");
        } else {
          $dataBusinessHours({
            ...dataBusinessHours,
            holidays: [...dataBusinessHours.holidays, values],
          });
        }
      }
    }
  };

  const handleSelectShift = (value, idx, type) => {
    let temp = [...dataBusinessHours.shifts];

    if (type === "time") {
      temp[idx] = {
        ...temp[idx],
        time: value,
      };
    } else {
      temp[idx] = {
        ...temp[idx],
        day: value,
      };
    }
    $dataBusinessHours({
      ...dataBusinessHours,
      shifts: temp,
    });
  };

  const _onCloseModal = () => {
    setModalShift(false);
  };

  const _onDeleteHoliday = (idx) => {
    let tempHolidays = [...dataBusinessHours.holidays];
    tempHolidays.splice(idx, 1);
    $dataBusinessHours({
      ...dataBusinessHours,
      holidays: tempHolidays,
    });
  };

  const _onAddShift = () => {
    $dataBusinessHours({
      ...dataBusinessHours,
      shifts: [
        ...dataBusinessHours.shifts,
        { day: [...dataBusinessHours.workingDays], time: [] },
      ],
    });
  };

  const _onDeleteShift = (idx) => {
    let tempShifts = [...dataBusinessHours.shifts];
    tempShifts.splice(idx, 1);
    $dataBusinessHours({
      ...dataBusinessHours,
      shifts: tempShifts,
    });
  };

  const _onOpenModalShift = (idx) => {
    setDataShift(idx);
    setModalShift(true);
  };

  useEffect(() => {
    if (dataBusinessHours.shifts.length > 0) {
      dataBusinessHours.shifts.forEach((item, idx) => {
        if (item.time.length > 0) {
          form.setFieldsValue({
            ["shift" + idx]: [
              moment(item.time[0], "HH:mm"),
              moment(item.time[1], "HH:mm"),
            ],
          });
        } else {
          form.setFieldsValue({
            ["shift" + idx]: [undefined, undefined],
          });
        }
      });
    }
  }, [dataBusinessHours, form]);

  return (
    <>
      <SettingBussniess
        defaultActiveKey="working_time"
        expandIconPosition="end"
        size="large"
      >
        <Panel
          key="working_time"
          header={
            <div style={{ display: "flex", columnGap: "8px" }}>
              <Icon icon="work" size={24} />
              <span
                style={{ fontSize: "16px", fontFamily: "var(--roboto-700)" }}
              >
                Working time
              </span>
            </div>
          }
        >
          <Title required={required}>{t("slaSetting.workingDay")}</Title>
          <Form.Item
            label=""
            name="working_day"
            rules={[{ required: required, message: "Please select" }]}
            wrapperCol={{ span: 24 }}
          >
            <Select
              mode="multiple"
              placeholder={t("slaSetting.selectDay")}
              onChange={handleSelectWorkingDay}
              optionFilterProp="label"
              showSearch
            >
              {week.map((item, index) => {
                return (
                  <Option key={index} value={item.value}>
                    {t(item.label)}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>

          <Title>{t("slaSetting.holidays")}</Title>
          <WrapHolidays>
            <SelectDays>
              <Form.Item label="" name="holidays" wrapperCol={{ span: 24 }}>
                <DatePicker
                  placeholder={t("slaSetting.selectDay")}
                  onChange={handleSelectHoliday}
                />
              </Form.Item>
            </SelectDays>

            {dataBusinessHours.holidays.length > 0 && (
              <Holidays>
                {dataBusinessHours.holidays.map((item, idx) => {
                  return (
                    <Holiday key={idx}>
                      <span>{item}</span>
                      <DeleteDay onClick={() => _onDeleteHoliday(idx)}>
                        x
                      </DeleteDay>
                    </Holiday>
                  );
                })}
              </Holidays>
            )}
          </WrapHolidays>

          {dataBusinessHours.workingDays.length > 0 && (
            <>
              <Title required={required}>{t("slaSetting.shiftWork")}</Title>
              {dataBusinessHours.shifts.length > 0 && (
                <WrapShifts>
                  {dataBusinessHours.shifts.map((item, index) => {
                    return (
                      <Shift key={index}>
                        <Form.Item
                          colon={false}
                          label={
                            <Button onClick={() => _onOpenModalShift(index)}>
                              <span
                                style={{
                                  fontSize: "16px",
                                  fontFamily: "var(--roboto-500)",
                                }}
                              >
                                {t("slaSetting.shift")} {index + 1}
                              </span>
                            </Button>
                          }
                          name={"shift" + index}
                          labelCol={{ span: 4 }}
                          wrapperCol={{ span: 20 }}
                        >
                          <TimePicker.RangePicker
                            placeholder={[
                              t("slaSetting.startTime"),
                              t("slaSetting.endTime"),
                            ]}
                            onChange={(time, timeString) =>
                              handleSelectShift(timeString, index, "time")
                            }
                            allowClear={false}
                            suffixIcon=""
                            format="HH:mm"
                          />
                        </Form.Item>
                        <div className="action">
                          <Tooltip title="Delete">
                            <img
                              onClick={() => _onDeleteShift(index)}
                              src={Delete}
                              alt="delete"
                            />
                          </Tooltip>
                        </div>
                      </Shift>
                    );
                  })}
                </WrapShifts>
              )}

              {dataBusinessHours.holidays.length > 0 ? (
                <>
                  {dataBusinessHours.shifts.length < 7 && (
                    <AddNew onClick={() => _onAddShift()}>
                      <span>+ {t("slaSetting.addShift")}</span>
                    </AddNew>
                  )}
                </>
              ) : (
                <>
                  {dataBusinessHours.shifts.length < 8 && (
                    <AddNew onClick={() => _onAddShift()}>
                      <span>+ {t("slaSetting.addShift")}</span>
                    </AddNew>
                  )}
                </>
              )}
            </>
          )}
        </Panel>
      </SettingBussniess>

      <ModalCustom
        title="Shift work"
        visible={modalShift}
        width={400}
        footer={null}
        onCancel={_onCloseModal}
      >
        <Form.Item
          label="Giờ làm"
          name={"shift" + dataShift}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
        >
          <TimePicker.RangePicker
            onChange={(time, timeString) =>
              handleSelectShift(timeString, dataShift, "time")
            }
            allowClear={false}
            suffixIcon=""
            format="HH:mm"
          />
        </Form.Item>
        <Apply>
          <legend>Áp dụng</legend>
          <Checkbox.Group
            value={
              dataBusinessHours.shifts.length > 0 &&
              dataBusinessHours.shifts[dataShift]?.day
            }
            onChange={(e) => handleSelectShift(e, dataShift, "day")}
          >
            {week.map((item, index) => {
              return (
                <Checkbox key={index} value={item.value}>
                  {t(item.label)}
                </Checkbox>
              );
            })}
          </Checkbox.Group>
        </Apply>

        <WrapButton label=" " colon={false}>
          <Button type="primary" onClick={_onCloseModal}>
            Ok
          </Button>
        </WrapButton>
      </ModalCustom>
    </>
  );
}

export default BusinessHours;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-picker {
    width: 100%;
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }
`;

const SettingBussniess = styled(Collapse)`
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  border: 1px solid #ececec !important;
  border-bottom: 0 !important;
  border-radius: 8px !important;
  background-color: #fff !important;

  .ant-collapse-item {
    border-bottom: 1px solid #ececec !important;
    border-radius: 0 0 8px 8px !important;
  }

  .ant-collapse-content {
    border-top: 1px solid #ececec !important;
    border-radius: 0 0 8px 8px !important;
  }

  .ant-form-item {
    margin-bottom: 16px;
  }
`;

const Title = styled.p`
  color: #2c2c2c;
  font-family: var(--roboto-500);
  font-size: 16px;
  margin-bottom: 8px;

  :after {
    display: ${({ required }) => (required ? "inline-block" : "none")};
    margin-left: 4px;
    color: #ff4d4f;
    font-size: 14px;
    line-height: 1;
    content: "*";
  }
`;

const WrapHolidays = styled.div`
  display: flex;
  margin-bottom: 16px;

  .ant-picker-now-btn {
    color: ${(props) => props.theme.main};
  }

  .ant-picker-today-btn {
    color: ${(props) => props.theme.main};
  }
`;

const SelectDays = styled.div`
  padding: 16px 8px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  background: #fff;
  width: 35%;

  .ant-picker {
    width: 100%;
  }

  .ant-form-item {
    margin-bottom: 0;
  }
`;

const Holidays = styled.div`
  padding: 12px 4px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  background: #fff;
  width: 65%;
  margin-left: 4px;
  display: flex;
  flex-wrap: wrap;
`;

const Holiday = styled.div`
  display: flex;
  align-items: center;
  padding: 2px 4px 2px 2px;
  background: #f5f5f5;
  border: 1px solid #f0f0f0;
  border-radius: 2px;
  width: fit-content;
  margin: 2px 2px;
  height: 24px;
`;

const DeleteDay = styled.div`
  cursor: pointer;
  margin-left: 4px;
  color: rgba(0, 0, 0, 0.45);
`;

const AddNew = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const WrapShifts = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const Shift = styled.div`
  display: flex;
  position: relative;
  width: 48%;
  min-width: 400px;

  :nth-child(odd) {
    margin-right: 16px;
  }

  .action {
    position: absolute;
    top: 4px;
    right: 15px;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    img {
      cursor: pointer;
    }
  }

  :hover .action {
    visibility: visible;
    opacity: 1;

    img {
      :hover {
        background: #eeeeee;
      }
    }
  }

  .ant-picker {
    width: 100%;
  }

  .ant-form-item {
    width: 100%;
  }

  .ant-btn {
    border: 1px solid #e2eefe;
    background: #e2eefe;
    width: 100%;
    padding: 0 8px;
  }

  .ant-form-item-label > label.ant-form-item-no-colon::after {
    display: none;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Apply = styled.fieldset`
  background: #fff;
  border-radius: 10px;
  height: fit-content;
  border: 1px solid #ececec;
  padding: 0 16px 16px 16px;

  .ant-checkbox-group {
    display: flex;
    flex-direction: column;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox {
    :hover {
      .ant-checkbox-inner {
        border-color: ${(props) => props.theme.main}!important;
      }
    }
  }

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;
