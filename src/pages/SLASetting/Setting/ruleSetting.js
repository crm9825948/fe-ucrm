import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import isEmail from "validator/lib/isEmail";
import _ from "lodash";

import Button from "antd/lib/button";
import Collapse from "antd/lib/collapse";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";
import Select from "antd/lib/select";
import Form from "antd/lib/form";
import Switch from "antd/lib/switch";
import Checkbox from "antd/lib/checkbox";
import InputNumber from "antd/lib/input-number";
import Typography from "antd/lib/typography";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

import EmptySLA from "assets/icons/SLA/empty_SLA.png";
// import PlusGreen from "assets/icons/common/plus-green.svg";
import MinusSquare from "assets/icons/SLA/MinusSquare.svg";
import PlusSquare from "assets/icons/SLA/PlusSquare.svg";
import Delete from "assets/icons/common/delete.svg";
// import Edit from "assets/icons/common/edit.svg";
import Edit from "assets/icons/common/edit-icon.png";
import InforCircle from "assets/icons/SLA/InfoCircle.svg";
import DuplicateRule from "assets/icons/SLA/duplicate_rule.svg";

import Conditions from "components/Conditions/conditions";

import { sendToUserSLA } from "util/staticData";
import { t } from "i18next";

function RuleSetting({
  currentStep,
  listUserEmail,
  rules,
  setRules,
  listTemplate,
  fields_related,
  listFields,
  handleFieldType,
  detailsSLA,
  isEdit,
  form,
  operatorValueAndCreated,
  setOperatorValueAndCreated,
  operatorValueAndSuccess,
  setOperatorValueAndSuccess,
  operatorValueAndPending,
  setOperatorValueAndPending,
  valueAndCreated,
  setValueAndCreated,
  valueAndSuccess,
  setValueAndSuccess,
  valueAndPending,
  setValueAndPending,
  setEmailValid,
  operatorValueOrCreated,
  setOperatorValueOrCreated,
  operatorValueOrSuccess,
  setOperatorValueOrSuccess,
  operatorValueOrPending,
  setOperatorValueOrPending,
  valueOrCreated,
  setValueOrCreated,
  valueOrSuccess,
  setValueOrSuccess,
  valueOrPending,
  setValueOrPending,
}) {
  const { Panel } = Collapse;
  const { Text } = Typography;

  const [editRuleName, setEditRuleName] = useState({
    active: false,
    index: 0,
  });

  const [listValid, setListValid] = useState([]);

  useEffect(() => {
    let temp = [];
    sendToUserSLA.concat(listUserEmail).forEach((item) => {
      temp.push(item.value);
    });
    setListValid(temp);
  }, [listUserEmail]);

  const _onAddRule = () => {
    setRules([
      ...rules,
      {
        name: t("slaSetting.addNameSLA"),
        is_fixed_hour: false,
        fixed_hour: {
          hour: 0,
          minute: 0,
          day: 0,
        },
        time_to_completion: {
          days: 0,
          hours: 0,
          minutes: 0,
        },
        allow_parent_child_relationship: false,
        is_dependent_on_children: false,
        is_dependent_on_parent: false,
        // is_repeatable: false,
        init_action: [],
        failure_action: [],
        success_action: [],
        overdue_action: [],
        alert_configs: [],
        created_conditions: {
          and_conditions: [],
          or_conditions: [],
        },
        success_conditions: {
          and_conditions: [],
          or_conditions: [],
        },
        pending_conditions: {
          and_conditions: [],
          or_conditions: [],
        },
      },
    ]);
  };

  const _onDuplicateRule = (item, index) => {
    let tempRules = [...rules];
    tempRules.splice(index + 1, 0, {
      ...item,
      name: item.name + " (Copy)",
    });
    setRules(tempRules);
  };

  const _onDeleteRule = (index) => {
    let tempRules = [...rules];
    tempRules.splice(index, 1);
    setRules(tempRules);
  };

  const _onAddFailureAction = (index) => {
    let tempRules = [...rules];
    tempRules[index] = {
      ...tempRules[index],
      failure_action: [
        ...tempRules[index].failure_action,
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };
    setRules(tempRules);
  };

  const _onAddInitAction = (index) => {
    let tempRules = [...rules];
    tempRules[index] = {
      ...tempRules[index],
      init_action: [
        ..._.get(tempRules[index], "init_action", []),
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };
    setRules(tempRules);
  };

  const handleSelectFieldInit = (value, indexRule, indexInit) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].init_action];
    tempActions[indexInit] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      init_action: tempActions,
    };
    setRules(tempRules);
  };

  const handleSelectFieldFailure = (value, indexRule, indexFailure) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].failure_action];
    tempActions[indexFailure] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      failure_action: tempActions,
    };
    setRules(tempRules);
  };

  const _onDeleteInitActions = (indexRule, indexInit) => {
    let tempRules = [...rules];
    let tempActions = [...tempRules[indexRule].init_action];
    tempActions.splice(indexInit, 1);
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      init_action: tempActions,
    };

    setRules(tempRules);
  };

  const _onDeleteFailureActions = (indexRule, indexFailure) => {
    let tempRules = [...rules];
    let tempActions = [...tempRules[indexRule].failure_action];
    tempActions.splice(indexFailure, 1);
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      failure_action: tempActions,
    };

    setRules(tempRules);
  };

  const _onAddSuccessAction = (index) => {
    let tempRules = [...rules];
    tempRules[index] = {
      ...tempRules[index],
      success_action: [
        ...tempRules[index].success_action,
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };
    setRules(tempRules);
  };

  const handleSelectFieldSuccess = (value, indexRule, indexSuccess) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].success_action];
    tempActions[indexSuccess] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      success_action: tempActions,
    };
    setRules(tempRules);
  };

  const _onDeleteSuccessAction = (indexRule, indexSuccess) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].success_action];
    tempActions.splice(indexSuccess, 1);
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      success_action: tempActions,
    };

    setRules(tempRules);
  };

  const handleSelectFieldOverdue = (value, indexRule, indexOverdue) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].overdue_action];
    tempActions[indexOverdue] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      overdue_action: tempActions,
    };
    setRules(tempRules);
  };

  const _onDeleteOverdueAction = (indexRule, indexOverdue) => {
    let tempRules = [...rules];

    let tempActions = [...tempRules[indexRule].overdue_action];
    tempActions.splice(indexOverdue, 1);
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      overdue_action: tempActions,
    };

    setRules(tempRules);
  };

  const _onAddOverdueAction = (index) => {
    let tempRules = [...rules];
    tempRules[index] = {
      ...tempRules[index],
      overdue_action: [
        ..._.get(tempRules[index], "overdue_action", []),
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };
    setRules(tempRules);
  };

  const _onAddAlert = (index) => {
    let tempRules = [...rules];
    tempRules[index] = {
      ...tempRules[index],
      alert_configs: [
        ...tempRules[index].alert_configs,
        {
          email_template_id: "",
          send_email_to: [],
          time_before_violation: {
            days: 0,
            hours: 0,
            minutes: 0,
          },
          alert_action: [],
        },
      ],
    };

    form.setFieldsValue({
      ["email_template_rule" + index + rules[index].alert_configs.length]:
        undefined,
      ["send_email_to" + index + rules[index].alert_configs.length]: undefined,
    });

    setRules(tempRules);
  };

  const _onDeleteAlert = (indexRule, indexAlert) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];
    tempAlerts.splice(indexAlert, 1);
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };

    // form.setFieldsValue({
    //   ["email_template_rule" + indexRule + indexAlert]: undefined,
    //   ["send_email_to" + indexRule + indexAlert]: undefined,
    // });

    tempRules[indexRule].alert_configs.forEach((ele, idx) => {
      form.setFieldsValue({
        ["email_template_rule" + indexRule + idx]: ele.email_template_id,
        ["send_email_to" + indexRule + idx]: ele.send_email_to,
      });
    });

    setRules(tempRules);
  };

  const _onEditRuleName = (idx) => {
    setEditRuleName({
      active: true,
      index: idx,
    });
  };

  const handleChangeRuleName = (e) => {
    let tempRules = [...rules];
    tempRules[editRuleName.index] = {
      ...tempRules[editRuleName.index],
      name: e.target.value,
    };
    setRules(tempRules);
  };

  const handleKeyPress = (event) => {
    if (event.key === "Enter") {
      setEditRuleName({
        active: false,
        index: 0,
      });
      event.stopPropagation();
    }
  };

  const handleChangeTime = (e, index, type) => {
    let tempRules = [...rules];
    switch (type) {
      case "days":
        tempRules[index] = {
          ...tempRules[index],
          time_to_completion: {
            ...tempRules[index].time_to_completion,
            days: e || 0,
          },
        };
        setRules(tempRules);
        break;
      case "hours":
        tempRules[index] = {
          ...tempRules[index],
          time_to_completion: {
            ...tempRules[index].time_to_completion,
            hours: e || 0,
          },
        };
        setRules(tempRules);
        break;

      default:
        tempRules[index] = {
          ...tempRules[index],
          time_to_completion: {
            ...tempRules[index].time_to_completion,
            minutes: e || 0,
          },
        };
        setRules(tempRules);
        break;
    }
  };

  const handleSelectTime = (value, index, type) => {
    let tempRules = [...rules];

    switch (type) {
      case "days":
        tempRules[index] = {
          ...tempRules[index],
          fixed_hour: {
            ...tempRules[index].fixed_hour,
            day: value || 0,
          },
        };

        setRules(tempRules);
        break;

      case "hours":
        tempRules[index] = {
          ...tempRules[index],
          fixed_hour: {
            ...tempRules[index].fixed_hour,
            hour: value || 0,
          },
        };
        setRules(tempRules);
        break;

      default:
        tempRules[index] = {
          ...tempRules[index],
          fixed_hour: {
            ...tempRules[index].fixed_hour,
            minute: value || 0,
          },
        };
        setRules(tempRules);
        break;
    }

    setRules(tempRules);
  };

  const changeCheckboxDeadline = (e, idx) => {
    let tempRules = [...rules];

    tempRules[idx] = {
      ...tempRules[idx],
      is_fixed_hour: _.get(e, "target.checked") || false,
    };

    if (_.get(e, "target.checked")) {
      tempRules[idx] = {
        ...tempRules[idx],
        fixed_hour: {
          ...tempRules[idx].fixed_hour,
          day: 0,
          hour: 0,
          minute: 0,
        },
      };

      tempRules[idx] = {
        ...tempRules[idx],
        pending_conditions: {
          ...tempRules[idx].pending_conditions,
          and_conditions: [],
        },
      };
    } else {
      tempRules[idx] = {
        ...tempRules[idx],
        time_to_completion: {
          ...tempRules[idx].time_to_completion,
          days: 0,
          hours: 0,
          minutes: 0,
        },
      };
    }
    setRules(tempRules);
  };

  const handleChangeTimeAlert = (e, indexRule, indexAlert, type) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];

    switch (type) {
      case "days":
        tempAlerts[indexAlert] = {
          ...tempAlerts[indexAlert],
          time_before_violation: {
            ...tempAlerts[indexAlert].time_before_violation,
            days: e,
          },
        };

        tempRules[indexRule] = {
          ...tempRules[indexRule],
          alert_configs: tempAlerts,
        };

        setRules(tempRules);
        break;
      case "hours":
        tempAlerts[indexAlert] = {
          ...tempAlerts[indexAlert],
          time_before_violation: {
            ...tempAlerts[indexAlert].time_before_violation,
            hours: e,
          },
        };

        tempRules[indexRule] = {
          ...tempRules[indexRule],
          alert_configs: tempAlerts,
        };
        setRules(tempRules);
        break;

      default:
        tempAlerts[indexAlert] = {
          ...tempAlerts[indexAlert],
          time_before_violation: {
            ...tempAlerts[indexAlert].time_before_violation,
            minutes: e,
          },
        };

        tempRules[indexRule] = {
          ...tempRules[indexRule],
          alert_configs: tempAlerts,
        };
        setRules(tempRules);
        break;
    }
  };

  const handleChangeTemplateAlert = (e, indexRule, indexAlert) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];

    tempAlerts[indexAlert] = {
      ...tempAlerts[indexAlert],
      email_template_id: e,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };
    setRules(tempRules);
  };

  const handleChangeEmailToAlert = (e, indexRule, indexAlert) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];

    tempAlerts[indexAlert] = {
      ...tempAlerts[indexAlert],
      send_email_to: e,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };
    setRules(tempRules);
  };

  const handleChangeRelationship = (e, indexRule, type) => {
    let tempRules = [...rules];

    switch (type) {
      case "allow":
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          allow_parent_child_relationship: e,
        };
        setRules(tempRules);
        break;

      case "parent":
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          is_dependent_on_parent: e.target.checked,
        };
        setRules(tempRules);
        break;

      case "children":
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          is_dependent_on_children: e.target.checked,
        };
        setRules(tempRules);
        break;

      // case "is_repeatable":
      //   tempRules[indexRule] = {
      //     ...tempRules[indexRule],
      //     is_repeatable: e,
      //   };
      //   setRules(tempRules);
      //   break;

      default:
        break;
    }
  };

  const handleChangeConditions = (value, index, type, condition) => {
    let tempRules = [...rules];
    switch (type) {
      case "created":
        tempRules[index] = {
          ...tempRules[index],
          created_conditions: {
            ...tempRules[index].created_conditions,
            [condition === "and" ? "and_conditions" : "or_conditions"]: value,
          },
        };
        setRules(tempRules);
        break;
      case "success":
        tempRules[index] = {
          ...tempRules[index],
          success_conditions: {
            ...tempRules[index].success_conditions,
            [condition === "and" ? "and_conditions" : "or_conditions"]: value,
          },
        };
        setRules(tempRules);
        break;
      case "pending":
        tempRules[index] = {
          ...tempRules[index],
          pending_conditions: {
            ...tempRules[index].pending_conditions,
            [condition === "and" ? "and_conditions" : "or_conditions"]: value,
          },
        };
        setRules(tempRules);
        break;

      default:
        break;
    }
  };

  const handleChangeTempOperator = (value, idx, type, condition) => {
    let tempOperatorValueCreated =
      condition === "and"
        ? [...operatorValueAndCreated]
        : [...operatorValueOrCreated];
    let tempOperatorValueSuccess =
      condition === "and"
        ? [...operatorValueAndSuccess]
        : [...operatorValueOrSuccess];
    let tempOperatorValuePending =
      condition === "and"
        ? [...operatorValueAndPending]
        : [...operatorValueOrPending];

    switch (type) {
      case "created":
        tempOperatorValueCreated[idx] = {
          operator: [],
        };

        tempOperatorValueCreated[idx].operator = value;
        condition === "and"
          ? setOperatorValueAndCreated(tempOperatorValueCreated)
          : setOperatorValueOrCreated(tempOperatorValueCreated);
        break;
      case "success":
        tempOperatorValueSuccess[idx] = {
          operator: [],
        };

        tempOperatorValueSuccess[idx].operator = value;
        condition === "and"
          ? setOperatorValueAndSuccess(tempOperatorValueSuccess)
          : setOperatorValueOrSuccess(tempOperatorValueSuccess);
        break;
      case "pending":
        tempOperatorValuePending[idx] = {
          operator: [],
        };

        tempOperatorValuePending[idx].operator = value;
        condition === "and"
          ? setOperatorValueAndPending(tempOperatorValuePending)
          : setOperatorValueOrPending(tempOperatorValuePending);
        break;
      default:
        break;
    }
  };

  const handleChangeTempValue = (value, idx, type, condition) => {
    let tempValueCreated =
      condition === "and" ? [...valueAndCreated] : [...valueOrCreated];
    let tempValueSuccess =
      condition === "and" ? [...valueAndSuccess] : [...valueOrSuccess];
    let tempValuePending =
      condition === "and" ? [...valueAndPending] : [...valueOrPending];

    switch (type) {
      case "created":
        tempValueCreated[idx] = {
          value: "",
        };

        tempValueCreated[idx].value = value;
        condition === "and"
          ? setValueAndCreated(tempValueCreated)
          : setValueOrCreated(tempValueCreated);
        break;
      case "success":
        tempValueSuccess[idx] = {
          value: "",
        };

        tempValueSuccess[idx].value = value;
        condition === "and"
          ? setValueAndSuccess(tempValueSuccess)
          : setValueOrSuccess(tempValueSuccess);
        break;
      case "pending":
        tempValuePending[idx] = {
          value: "",
        };

        tempValuePending[idx].value = value;
        condition === "and"
          ? setValueAndPending(tempValuePending)
          : setValueOrPending(tempValuePending);
        break;
      default:
        break;
    }
  };

  const _onAddAlertAction = (indexRule, indexAlert) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];

    tempAlerts[indexAlert] = {
      ...tempAlerts[indexAlert],
      alert_action: [
        ..._.get(tempAlerts, `[${indexAlert}].alert_action`, []),
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };
    setRules(tempRules);
  };

  const handleSelectFieldAction = (
    value,
    indexRule,
    indexAlert,
    indexAction
  ) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];
    let tempActions = [
      ..._.get(tempAlerts, `[${indexAlert}].alert_action`, []),
    ];

    tempActions[indexAction] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempAlerts[indexAlert] = {
      ...tempAlerts[indexAlert],
      alert_action: tempActions,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };

    setRules(tempRules);
  };

  const _onDeleteAlertAction = (indexRule, indexAlert, indexAction) => {
    let tempRules = [...rules];
    let tempAlerts = [...tempRules[indexRule].alert_configs];
    let tempActions = [
      ..._.get(tempAlerts, `[${indexAlert}].alert_action`, []),
    ];

    tempActions.splice(indexAction, 1);

    tempAlerts[indexAlert] = {
      ...tempAlerts[indexAlert],
      alert_action: tempActions,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      alert_configs: tempAlerts,
    };

    setRules(tempRules);
  };

  useEffect(() => {
    if (
      Object.keys(detailsSLA).length > 0 &&
      isEdit &&
      fields_related.length > 0
    ) {
      setRules(detailsSLA.sla_rules);

      let tempOperatorValueAndCreated = [];
      let tempOperatorValueAndSuccess = [];
      let tempOperatorValueAndPending = [];
      let tempOperatorValueOrCreated = [];
      let tempOperatorValueOrSuccess = [];
      let tempOperatorValueOrPending = [];

      detailsSLA.sla_rules.forEach((rule, idx) => {
        tempOperatorValueAndCreated[idx] = {
          operator: [],
        };
        tempOperatorValueAndSuccess[idx] = {
          operator: [],
        };
        tempOperatorValueAndPending[idx] = {
          operator: [],
        };

        tempOperatorValueOrCreated[idx] = {
          operator: [],
        };
        tempOperatorValueOrSuccess[idx] = {
          operator: [],
        };
        tempOperatorValueOrPending[idx] = {
          operator: [],
        };

        rule.created_conditions.and_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueAndCreated[idx].operator = [
                ...tempOperatorValueAndCreated[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.success_conditions.and_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueAndSuccess[idx].operator = [
                ...tempOperatorValueAndSuccess[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.pending_conditions.and_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueAndPending[idx].operator = [
                ...tempOperatorValueAndPending[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.created_conditions.or_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueOrCreated[idx].operator = [
                ...tempOperatorValueOrCreated[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.success_conditions.or_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueOrSuccess[idx].operator = [
                ...tempOperatorValueOrSuccess[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.pending_conditions.or_conditions.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueOrPending[idx].operator = [
                ...tempOperatorValueOrPending[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });
      });

      setOperatorValueAndCreated(tempOperatorValueAndCreated);
      setOperatorValueAndSuccess(tempOperatorValueAndSuccess);
      setOperatorValueAndPending(tempOperatorValueAndPending);

      setOperatorValueOrCreated(tempOperatorValueOrCreated);
      setOperatorValueOrSuccess(tempOperatorValueOrSuccess);
      setOperatorValueOrPending(tempOperatorValueOrPending);

      detailsSLA.sla_rules.forEach((ele, indexRule) => {
        ele.alert_configs.forEach((item, idx) => {
          form.setFieldsValue({
            ["email_template_rule" + indexRule + idx]: item.email_template_id,
            ["send_email_to" + indexRule + idx]: item.send_email_to,
          });
        });
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [detailsSLA, isEdit, fields_related, form]);

  return (
    <Wrapper currentStep={currentStep}>
      {rules.length === 0 ? (
        <Empty>
          <img src={EmptySLA} alt="empty" />
          <p>{t("slaSetting.noRules")}</p>
          <AddButton onClick={() => _onAddRule()}>
            + {t("slaSetting.addSLARule")}
          </AddButton>
        </Empty>
      ) : (
        <>
          <Collapse
            expandIcon={({ isActive }) =>
              isActive ? (
                <img src={MinusSquare} alt="minus" />
              ) : (
                <img src={PlusSquare} alt="plus" />
              )
            }
          >
            {_.map(rules, (rule, idx) => {
              return (
                <Panel
                  header={
                    editRuleName.active && editRuleName.index === idx ? (
                      <Input
                        value={rule.name}
                        onChange={handleChangeRuleName}
                        onKeyPress={handleKeyPress}
                        onClick={(event) => event.stopPropagation()}
                      />
                    ) : (
                      <>
                        <RuleName
                          onClick={(event) => event.stopPropagation()}
                          onDoubleClick={() => _onEditRuleName(idx)}
                        >
                          <Text
                            ellipsis={{
                              tooltip: rule.name,
                            }}
                          >
                            {rule.name}
                          </Text>
                          <img
                            src={Edit}
                            alt="edit"
                            className="action-edit"
                            onClick={() => _onEditRuleName(idx)}
                          />
                        </RuleName>
                        <div className="action">
                          <Tooltip title="Duplicate rule">
                            <img
                              onClick={(event) => {
                                _onDuplicateRule(rule, idx);
                                event.stopPropagation();
                              }}
                              src={DuplicateRule}
                              alt="duplicate"
                              style={{ marginRight: "16px" }}
                            />
                          </Tooltip>
                          <Tooltip title="Delete">
                            <img
                              onClick={(event) => {
                                _onDeleteRule(idx);
                                event.stopPropagation();
                              }}
                              src={Delete}
                              alt="delete"
                            />
                          </Tooltip>
                        </div>
                      </>
                    )
                  }
                  key={idx}
                >
                  <Rule>
                    <Left>
                      <WrapConditions>
                        <BasicInfo>
                          <legend>{t("slaSetting.willRun")}</legend>
                          <Conditions
                            title={t("common.allCondition")}
                            decs={`(${t("common.descAllCondition")})`}
                            conditions={rule.created_conditions.and_conditions}
                            setConditions={(value) =>
                              handleChangeConditions(
                                value,
                                idx,
                                "created",
                                "and"
                              )
                            }
                            ID={
                              Object.keys(detailsSLA).length > 0 &&
                              detailsSLA._id
                                ? detailsSLA._id
                                : ""
                            }
                            dataDetails={
                              Object.keys(detailsSLA).length > 0 &&
                              rule.created_conditions.and_conditions.length > 0
                                ? rule.created_conditions.and_conditions
                                : {}
                            }
                            operatorValue={
                              operatorValueAndCreated[idx]?.operator
                            }
                            setOperatorValue={(value) =>
                              handleChangeTempOperator(
                                value,
                                idx,
                                "created",
                                "and"
                              )
                            }
                            value={valueAndCreated[idx]?.value}
                            setValue={(value) =>
                              handleChangeTempValue(
                                value,
                                idx,
                                "created",
                                "and"
                              )
                            }
                          />
                          <Conditions
                            title={t("common.anyCondition")}
                            decs={`(${t("common.descAnyCondition")})`}
                            conditions={rule.created_conditions.or_conditions}
                            setConditions={(value) =>
                              handleChangeConditions(
                                value,
                                idx,
                                "created",
                                "or"
                              )
                            }
                            ID={
                              Object.keys(detailsSLA).length > 0 &&
                              detailsSLA._id
                                ? detailsSLA._id
                                : ""
                            }
                            dataDetails={
                              Object.keys(detailsSLA).length > 0 &&
                              rule.created_conditions.or_conditions.length > 0
                                ? rule.created_conditions.or_conditions
                                : {}
                            }
                            operatorValue={
                              operatorValueOrCreated[idx]?.operator
                            }
                            setOperatorValue={(value) =>
                              handleChangeTempOperator(
                                value,
                                idx,
                                "created",
                                "or"
                              )
                            }
                            value={valueOrCreated[idx]?.value}
                            setValue={(value) =>
                              handleChangeTempValue(value, idx, "created", "or")
                            }
                          />
                        </BasicInfo>

                        {!_.get(rule, "is_fixed_hour") && (
                          <BasicInfo>
                            <legend>{t("slaSetting.willPause")}</legend>

                            <Conditions
                              title={t("common.allCondition")}
                              decs={`(${t("common.descAllCondition")})`}
                              conditions={
                                rule.pending_conditions.and_conditions
                              }
                              setConditions={(value) =>
                                handleChangeConditions(
                                  value,
                                  idx,
                                  "pending",
                                  "and"
                                )
                              }
                              ID={
                                Object.keys(detailsSLA).length > 0 &&
                                detailsSLA._id
                                  ? detailsSLA._id
                                  : ""
                              }
                              dataDetails={
                                Object.keys(detailsSLA).length > 0 &&
                                rule.pending_conditions.and_conditions.length >
                                  0
                                  ? rule.pending_conditions.and_conditions
                                  : {}
                              }
                              operatorValue={
                                operatorValueAndPending[idx]?.operator
                              }
                              setOperatorValue={(value) =>
                                handleChangeTempOperator(
                                  value,
                                  idx,
                                  "pending",
                                  "and"
                                )
                              }
                              value={valueAndPending[idx]?.value}
                              setValue={(value) =>
                                handleChangeTempValue(
                                  value,
                                  idx,
                                  "pending",
                                  "and"
                                )
                              }
                            />
                            <Conditions
                              title={t("common.anyCondition")}
                              decs={`(${t("common.descAnyCondition")})`}
                              conditions={rule.pending_conditions.or_conditions}
                              setConditions={(value) =>
                                handleChangeConditions(
                                  value,
                                  idx,
                                  "pending",
                                  "or"
                                )
                              }
                              ID={
                                Object.keys(detailsSLA).length > 0 &&
                                detailsSLA._id
                                  ? detailsSLA._id
                                  : ""
                              }
                              dataDetails={
                                Object.keys(detailsSLA).length > 0 &&
                                rule.pending_conditions.or_conditions.length > 0
                                  ? rule.pending_conditions.or_conditions
                                  : {}
                              }
                              operatorValue={
                                operatorValueOrPending[idx]?.operator
                              }
                              setOperatorValue={(value) =>
                                handleChangeTempOperator(
                                  value,
                                  idx,
                                  "pending",
                                  "or"
                                )
                              }
                              value={valueOrPending[idx]?.value}
                              setValue={(value) =>
                                handleChangeTempValue(
                                  value,
                                  idx,
                                  "pending",
                                  "or"
                                )
                              }
                            />
                          </BasicInfo>
                        )}

                        <SuccessCondition>
                          <BasicInfo>
                            <legend>{t("slaSetting.willClosed")}</legend>
                            <Conditions
                              title={t("common.allCondition")}
                              decs={`(${t("common.descAllCondition")})`}
                              conditions={
                                rule.success_conditions.and_conditions
                              }
                              setConditions={(value) =>
                                handleChangeConditions(
                                  value,
                                  idx,
                                  "success",
                                  "and"
                                )
                              }
                              ID={
                                Object.keys(detailsSLA).length > 0 &&
                                detailsSLA._id
                                  ? detailsSLA._id
                                  : ""
                              }
                              dataDetails={
                                Object.keys(detailsSLA).length > 0 &&
                                rule.success_conditions.and_conditions.length >
                                  0
                                  ? rule.success_conditions.and_conditions
                                  : {}
                              }
                              operatorValue={
                                operatorValueAndSuccess[idx]?.operator
                              }
                              setOperatorValue={(value) =>
                                handleChangeTempOperator(
                                  value,
                                  idx,
                                  "success",
                                  "and"
                                )
                              }
                              value={valueAndSuccess[idx]?.value}
                              setValue={(value) =>
                                handleChangeTempValue(
                                  value,
                                  idx,
                                  "success",
                                  "and"
                                )
                              }
                            />
                            <Conditions
                              title={t("common.anyCondition")}
                              decs={`(${t("common.descAnyCondition")})`}
                              conditions={rule.success_conditions.or_conditions}
                              setConditions={(value) =>
                                handleChangeConditions(
                                  value,
                                  idx,
                                  "success",
                                  "or"
                                )
                              }
                              ID={
                                Object.keys(detailsSLA).length > 0 &&
                                detailsSLA._id
                                  ? detailsSLA._id
                                  : ""
                              }
                              dataDetails={
                                Object.keys(detailsSLA).length > 0 &&
                                rule.success_conditions.or_conditions.length > 0
                                  ? rule.success_conditions.or_conditions
                                  : {}
                              }
                              operatorValue={
                                operatorValueOrSuccess[idx]?.operator
                              }
                              setOperatorValue={(value) =>
                                handleChangeTempOperator(
                                  value,
                                  idx,
                                  "success",
                                  "or"
                                )
                              }
                              value={valueOrSuccess[idx]?.value}
                              setValue={(value) =>
                                handleChangeTempValue(
                                  value,
                                  idx,
                                  "success",
                                  "or"
                                )
                              }
                            />
                          </BasicInfo>
                        </SuccessCondition>

                        <TimeToComplete
                          style={{
                            padding: "8px 24px 20px 24px",
                            gap: "16px",
                            display: "flex",
                            flexDirection: "column",
                          }}
                        >
                          <legend>{t("slaSetting.timeComplete")}</legend>
                          <div>
                            <CustomCheckbox
                              onChange={(e) => {
                                changeCheckboxDeadline(e, idx);
                              }}
                              checked={_.get(rule, "is_fixed_hour")}
                            >
                              {t("slaSetting.UsefixedDeadline")}
                            </CustomCheckbox>
                          </div>

                          {_.get(rule, "is_fixed_hour") ? (
                            <WrapperTime>
                              <InputNumber
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                                min={0}
                                value={_.get(rule, "fixed_hour.hour", 0)}
                                onChange={(e) => {
                                  if (e > 23) {
                                    handleSelectTime(23, idx, "hours");
                                  } else {
                                    handleSelectTime(e, idx, "hours");
                                  }
                                }}
                              />
                              <span>{t("slaSetting.hour")}</span>
                              <InputNumber
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                                min={0}
                                value={_.get(rule, "fixed_hour.minute", 0)}
                                onChange={(e) => {
                                  if (e > 59) {
                                    handleSelectTime(59, idx, "minutes");
                                  } else {
                                    handleSelectTime(e, idx, "minutes");
                                  }
                                }}
                              />
                              <span>{t("slaSetting.minute")}</span>
                              <Select
                                onChange={(value) => {
                                  if (value) {
                                    handleSelectTime(1, idx, "days");
                                  } else {
                                    handleSelectTime(0, idx, "days");
                                  }
                                }}
                                defaultValue={_.get(rule, "fixed_hour.day") > 0}
                                style={{ width: "165px" }}
                                options={[
                                  {
                                    value: false,
                                    label: "Same Working Day",
                                  },
                                  {
                                    value: true,
                                    label: "Next n Working day(s)",
                                  },
                                ]}
                              />
                              {_.get(rule, "fixed_hour.day") > 0 && (
                                <InputNumber
                                  onKeyPress={(event) => {
                                    if (!/[0-9]/.test(event.key)) {
                                      event.preventDefault();
                                    }
                                  }}
                                  min={1}
                                  value={_.get(rule, "fixed_hour.day", 0)}
                                  onChange={(e) => {
                                    if (e < 1) {
                                      handleSelectTime(1, idx, "days");
                                    } else {
                                      handleSelectTime(e, idx, "days");
                                    }
                                  }}
                                />
                              )}
                            </WrapperTime>
                          ) : (
                            <WrapInputTime>
                              <InputNumber
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                                min={0}
                                value={_.get(
                                  rule,
                                  "time_to_completion.days",
                                  0
                                )}
                                onChange={(e) =>
                                  handleChangeTime(e, idx, "days")
                                }
                              />
                              <span>{t("slaSetting.day")}</span>
                              <InputNumber
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                                min={0}
                                value={_.get(
                                  rule,
                                  "time_to_completion.hours",
                                  0
                                )}
                                onChange={(e) =>
                                  handleChangeTime(e, idx, "hours")
                                }
                              />
                              <span>{t("slaSetting.hour")}</span>
                              <InputNumber
                                onKeyPress={(event) => {
                                  if (!/[0-9]/.test(event.key)) {
                                    event.preventDefault();
                                  }
                                }}
                                min={0}
                                value={_.get(
                                  rule,
                                  "time_to_completion.minutes",
                                  0
                                )}
                                onChange={(e) =>
                                  handleChangeTime(e, idx, "minutes")
                                }
                              />
                              <span>{t("slaSetting.minute")}</span>
                            </WrapInputTime>
                          )}
                        </TimeToComplete>
                      </WrapConditions>

                      <WrapAlert>
                        <legend>{t("slaSetting.alert")}</legend>
                        {_.map(
                          _.get(rule, "alert_configs", []),
                          (alert, indexAlert) => {
                            return (
                              <Alert key={indexAlert}>
                                <NameAlert>
                                  {t("slaSetting.alert")} {indexAlert + 1}{" "}
                                  <Tooltip title="Delete">
                                    <img
                                      onClick={() =>
                                        _onDeleteAlert(idx, indexAlert)
                                      }
                                      src={Delete}
                                      alt="delete"
                                    />
                                  </Tooltip>
                                </NameAlert>
                                <BeforeViolation>
                                  <p className="alert_title">
                                    {t("slaSetting.beforeViolation")}
                                  </p>
                                  <InputNumber
                                    min={0}
                                    value={alert.time_before_violation.days}
                                    onChange={(e) =>
                                      handleChangeTimeAlert(
                                        e,
                                        idx,
                                        indexAlert,
                                        "days"
                                      )
                                    }
                                  />
                                  <p>{t("slaSetting.day")}</p>
                                  <InputNumber
                                    min={0}
                                    value={alert.time_before_violation.hours}
                                    onChange={(e) =>
                                      handleChangeTimeAlert(
                                        e,
                                        idx,
                                        indexAlert,
                                        "hours"
                                      )
                                    }
                                  />
                                  <p>{t("slaSetting.hour")}</p>
                                  <InputNumber
                                    min={0}
                                    value={alert.time_before_violation.minutes}
                                    onChange={(e) =>
                                      handleChangeTimeAlert(
                                        e,
                                        idx,
                                        indexAlert,
                                        "minutes"
                                      )
                                    }
                                  />
                                  <p>{t("slaSetting.minute")}</p>
                                </BeforeViolation>

                                <Form.Item
                                  label={t("slaSetting.emailTemplate")}
                                  name={
                                    "email_template_rule" + idx + indexAlert
                                  }
                                  className="form-custom"
                                  labelCol={{ span: 5 }}
                                  wrapperCol={{ span: 19 }}
                                >
                                  <Select
                                    optionFilterProp="label"
                                    showSearch
                                    options={listTemplate}
                                    placeholder={t(
                                      "slaSetting.selectEmailTemplate"
                                    )}
                                    onChange={(e) =>
                                      handleChangeTemplateAlert(
                                        e,
                                        idx,
                                        indexAlert
                                      )
                                    }
                                  />
                                </Form.Item>

                                <Form.Item
                                  label={t("slaSetting.sendEmail")}
                                  name={"send_email_to" + idx + indexAlert}
                                  className="form-custom"
                                  labelCol={{ span: 5 }}
                                  wrapperCol={{ span: 19 }}
                                  rules={[
                                    () => ({
                                      validator(_, value) {
                                        if (value && value.length > 0) {
                                          const found = value.filter(
                                            (item) => !listValid.includes(item)
                                          );
                                          if (
                                            found.some(
                                              (ele) => isEmail(ele) === false
                                            )
                                          ) {
                                            setEmailValid(false);
                                            return Promise.reject(
                                              new Error("Invalid email")
                                            );
                                          }
                                        }
                                        setEmailValid(true);
                                        return Promise.resolve();
                                      },
                                    }),
                                  ]}
                                >
                                  <Select
                                    options={sendToUserSLA.concat(
                                      listUserEmail
                                    )}
                                    placeholder={t(
                                      "slaSetting.selectEmailAddress"
                                    )}
                                    mode="tags"
                                    disabled={alert.email_template_id === ""}
                                    onChange={(e) =>
                                      handleChangeEmailToAlert(
                                        e,
                                        idx,
                                        indexAlert
                                      )
                                    }
                                  />
                                </Form.Item>

                                <Row>
                                  <Col span={5} style={{ fontSize: "16px" }}>
                                    {t("slaSetting.alertAction")}
                                  </Col>
                                  <Col>
                                    <Button
                                      onClick={() =>
                                        _onAddAlertAction(idx, indexAlert)
                                      }
                                    >
                                      + {t("slaSetting.add")}
                                    </Button>
                                  </Col>
                                </Row>

                                {_.get(alert, "alert_action", []).length >
                                  0 && (
                                  <RowCustom>
                                    {_.map(
                                      _.get(alert, "alert_action", []),
                                      (action, indexAction) => {
                                        return (
                                          <ActionAlert key={indexAction}>
                                            <p>{t("slaSetting.set")}</p>

                                            <Select
                                              placeholder="Please select"
                                              value={action.field_id}
                                              onChange={(value) =>
                                                handleSelectFieldAction(
                                                  value,
                                                  idx,
                                                  indexAlert,
                                                  indexAction
                                                )
                                              }
                                              showSearch
                                              optionFilterProp="children"
                                              filterOption={(
                                                inputValue,
                                                option
                                              ) => {
                                                if (option.children) {
                                                  return option.children
                                                    .toLowerCase()
                                                    .indexOf(
                                                      inputValue.toLowerCase()
                                                    ) >= 0
                                                    ? true
                                                    : false;
                                                } else if (option.label) {
                                                  return option.label
                                                    .toLowerCase()
                                                    .indexOf(
                                                      inputValue.toLowerCase()
                                                    ) >= 0
                                                    ? true
                                                    : false;
                                                }
                                              }}
                                            >
                                              {listFields.map((ele) => {
                                                return (
                                                  <Select.Option
                                                    disabled={
                                                      alert.alert_action?.find(
                                                        (field) =>
                                                          field.field_id ===
                                                          ele.value
                                                      )
                                                        ? true
                                                        : false
                                                    }
                                                    key={ele.value}
                                                  >
                                                    {ele.label}
                                                  </Select.Option>
                                                );
                                              })}
                                            </Select>
                                            <p>{t("slaSetting.to")}</p>
                                            {action.field_id !== "" ? (
                                              <>
                                                {handleFieldType(
                                                  fields_related.find(
                                                    (ele) =>
                                                      ele.ID === action.field_id
                                                  ),
                                                  idx,
                                                  indexAction,
                                                  "alertAction",
                                                  indexAlert
                                                )}
                                              </>
                                            ) : (
                                              <Input
                                                disabled
                                                placeholder={t(
                                                  "slaSetting.selectValue"
                                                )}
                                              />
                                            )}
                                            <Tooltip title={t("common.delete")}>
                                              <img
                                                onClick={() =>
                                                  _onDeleteAlertAction(
                                                    idx,
                                                    indexAlert,
                                                    indexAction
                                                  )
                                                }
                                                src={Delete}
                                                alt="delete"
                                              />
                                            </Tooltip>
                                          </ActionAlert>
                                        );
                                      }
                                    )}
                                  </RowCustom>
                                )}
                              </Alert>
                            );
                          }
                        )}

                        <AddNew onClick={() => _onAddAlert(idx)}>
                          {/* <img src={PlusGreen} alt="plus" /> */}
                          <span>+ {t("slaSetting.newAlert")}</span>
                        </AddNew>
                      </WrapAlert>
                    </Left>

                    <Right>
                      <RightTitle type="init_action">
                        <legend>{t("slaSetting.whenOpen")}</legend>
                        {_.map(
                          _.get(rule, "init_action", []),
                          (initAction, indexInit) => {
                            return (
                              <Action key={indexInit}>
                                <p>{t("slaSetting.set")}</p>

                                <Select
                                  placeholder="Please select"
                                  value={initAction.field_id}
                                  onChange={(value) =>
                                    handleSelectFieldInit(value, idx, indexInit)
                                  }
                                  showSearch
                                  optionFilterProp="children"
                                  filterOption={(inputValue, option) => {
                                    if (option.children) {
                                      return option.children
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    } else if (option.label) {
                                      return option.label
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    }
                                  }}
                                >
                                  {listFields.map((ele) => {
                                    return (
                                      <Select.Option
                                        disabled={
                                          rule.init_action.find(
                                            (field) =>
                                              field.field_id === ele.value
                                          )
                                            ? true
                                            : false
                                        }
                                        key={ele.value}
                                      >
                                        {ele.label}
                                      </Select.Option>
                                    );
                                  })}
                                </Select>
                                <p>{t("slaSetting.to")}</p>
                                {initAction.field_id !== "" ? (
                                  <>
                                    {handleFieldType(
                                      fields_related.find(
                                        (ele) => ele.ID === initAction.field_id
                                      ),
                                      idx,
                                      indexInit,
                                      "init"
                                    )}
                                  </>
                                ) : (
                                  <Input
                                    disabled
                                    placeholder={t("slaSetting.selectValue")}
                                  />
                                )}
                                <Tooltip title="Delete">
                                  <img
                                    onClick={() =>
                                      _onDeleteInitActions(idx, indexInit)
                                    }
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              </Action>
                            );
                          }
                        )}
                        <AddNewAction onClick={() => _onAddInitAction(idx)}>
                          {/* <img src={PlusGreen} alt="plus" /> */}
                          <span>+ {t("slaSetting.addAction")}</span>
                        </AddNewAction>
                      </RightTitle>

                      <RightTitle type="failure">
                        <legend>{t("slaSetting.whenExpired")}</legend>
                        {_.map(
                          _.get(rule, "failure_action", []),
                          (failureAction, indexFailure) => {
                            return (
                              <Action key={indexFailure}>
                                <p>{t("slaSetting.set")}</p>

                                <Select
                                  placeholder="Please select"
                                  value={failureAction.field_id}
                                  onChange={(value) =>
                                    handleSelectFieldFailure(
                                      value,
                                      idx,
                                      indexFailure
                                    )
                                  }
                                  showSearch
                                  optionFilterProp="children"
                                  filterOption={(inputValue, option) => {
                                    if (option.children) {
                                      return option.children
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    } else if (option.label) {
                                      return option.label
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    }
                                  }}
                                >
                                  {listFields.map((ele) => {
                                    return (
                                      <Select.Option
                                        disabled={
                                          rule.failure_action.find(
                                            (field) =>
                                              field.field_id === ele.value
                                          )
                                            ? true
                                            : false
                                        }
                                        key={ele.value}
                                      >
                                        {ele.label}
                                      </Select.Option>
                                    );
                                  })}
                                </Select>
                                <p>{t("slaSetting.to")}</p>
                                {failureAction.field_id !== "" ? (
                                  <>
                                    {handleFieldType(
                                      fields_related.find(
                                        (ele) =>
                                          ele.ID === failureAction.field_id
                                      ),
                                      idx,
                                      indexFailure,
                                      "failure"
                                    )}
                                  </>
                                ) : (
                                  <Input
                                    disabled
                                    placeholder={t("slaSetting.selectValue")}
                                  />
                                )}
                                <Tooltip title="Delete">
                                  <img
                                    onClick={() =>
                                      _onDeleteFailureActions(idx, indexFailure)
                                    }
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              </Action>
                            );
                          }
                        )}
                        <AddNewAction onClick={() => _onAddFailureAction(idx)}>
                          {/* <img src={PlusGreen} alt="plus" /> */}
                          <span>+ {t("slaSetting.addAction")}</span>
                        </AddNewAction>
                      </RightTitle>

                      <RightTitle type="success">
                        <legend>{t("slaSetting.whenclosedEarly")}</legend>
                        {_.map(
                          _.get(rule, "success_action", []),
                          (successAction, indexSuccess) => {
                            return (
                              <Action key={indexSuccess}>
                                <p>{t("slaSetting.set")}</p>

                                <Select
                                  placeholder="Please select"
                                  value={successAction.field_id}
                                  onChange={(value) =>
                                    handleSelectFieldSuccess(
                                      value,
                                      idx,
                                      indexSuccess
                                    )
                                  }
                                  showSearch
                                  optionFilterProp="children"
                                  filterOption={(inputValue, option) => {
                                    if (option.children) {
                                      return option.children
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    } else if (option.label) {
                                      return option.label
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    }
                                  }}
                                >
                                  {listFields.map((ele) => {
                                    return (
                                      <Select.Option
                                        disabled={
                                          rule.success_action.find(
                                            (field) =>
                                              field.field_id === ele.value
                                          )
                                            ? true
                                            : false
                                        }
                                        key={ele.value}
                                      >
                                        {ele.label}
                                      </Select.Option>
                                    );
                                  })}
                                </Select>
                                <p>{t("slaSetting.to")}</p>
                                {successAction.field_id !== "" ? (
                                  <>
                                    {handleFieldType(
                                      fields_related.find(
                                        (ele) =>
                                          ele.ID === successAction.field_id
                                      ),
                                      idx,
                                      indexSuccess,
                                      "success"
                                    )}
                                  </>
                                ) : (
                                  <Input
                                    disabled
                                    placeholder={t("slaSetting.selectValue")}
                                  />
                                )}
                                <Tooltip title="Delete">
                                  <img
                                    onClick={() =>
                                      _onDeleteSuccessAction(idx, indexSuccess)
                                    }
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              </Action>
                            );
                          }
                        )}
                        <AddNewAction onClick={() => _onAddSuccessAction(idx)}>
                          {/* <img src={PlusGreen} alt="plus" /> */}
                          <span>+ {t("slaSetting.addAction")}</span>
                        </AddNewAction>
                      </RightTitle>

                      <RightTitle type="overdue_action">
                        <legend>{t("slaSetting.whenclosedOverdue")}</legend>
                        {_.map(
                          _.get(rule, "overdue_action", []),
                          (overdueAction, indexOverdue) => {
                            return (
                              <Action key={indexOverdue}>
                                <p>{t("slaSetting.set")}</p>

                                <Select
                                  placeholder="Please select"
                                  value={overdueAction.field_id}
                                  onChange={(value) =>
                                    handleSelectFieldOverdue(
                                      value,
                                      idx,
                                      indexOverdue
                                    )
                                  }
                                  showSearch
                                  optionFilterProp="children"
                                  filterOption={(inputValue, option) => {
                                    if (option.children) {
                                      return option.children
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    } else if (option.label) {
                                      return option.label
                                        .toLowerCase()
                                        .indexOf(inputValue.toLowerCase()) >= 0
                                        ? true
                                        : false;
                                    }
                                  }}
                                >
                                  {listFields.map((ele) => {
                                    return (
                                      <Select.Option
                                        disabled={
                                          rule.overdue_action.find(
                                            (field) =>
                                              field.field_id === ele.value
                                          )
                                            ? true
                                            : false
                                        }
                                        key={ele.value}
                                      >
                                        {ele.label}
                                      </Select.Option>
                                    );
                                  })}
                                </Select>
                                <p>{t("slaSetting.to")}</p>
                                {overdueAction.field_id !== "" ? (
                                  <>
                                    {handleFieldType(
                                      fields_related.find(
                                        (ele) =>
                                          ele.ID === overdueAction.field_id
                                      ),
                                      idx,
                                      indexOverdue,
                                      "overdue"
                                    )}
                                  </>
                                ) : (
                                  <Input
                                    disabled
                                    placeholder={t("slaSetting.selectValue")}
                                  />
                                )}
                                <Tooltip title="Delete">
                                  <img
                                    onClick={() =>
                                      _onDeleteOverdueAction(idx, indexOverdue)
                                    }
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              </Action>
                            );
                          }
                        )}
                        <AddNewAction onClick={() => _onAddOverdueAction(idx)}>
                          {/* <img src={PlusGreen} alt="plus" /> */}
                          <span>+ {t("slaSetting.addAction")}</span>
                        </AddNewAction>
                      </RightTitle>

                      {/* <WrapRelationShip>
                        <Switch
                          checked={_.get(rule, "is_repeatable", false)}
                          onChange={(e) =>
                            handleChangeRelationship(e, idx, "is_repeatable")
                          }
                        />
                        <SubTitle>{t("slaSetting.reopenSLA")}</SubTitle>
                      </WrapRelationShip> */}

                      <WrapRelationShip>
                        <Switch
                          checked={rule.allow_parent_child_relationship}
                          onChange={(e) =>
                            handleChangeRelationship(e, idx, "allow")
                          }
                        />
                        <SubTitle>{t("slaSetting.parentChild")}</SubTitle>
                      </WrapRelationShip>

                      {rule.allow_parent_child_relationship && (
                        <WrapDependent>
                          <Checkbox
                            checked={rule.is_dependent_on_parent}
                            onChange={(e) =>
                              handleChangeRelationship(e, idx, "parent")
                            }
                          >
                            {t("slaSetting.dependParent")}
                            <Tooltip title="If the parent task succeeded, then the child tasks will also be successful.">
                              <img src={InforCircle} alt="InforCircle" />
                            </Tooltip>
                          </Checkbox>

                          <Checkbox
                            checked={rule.is_dependent_on_children}
                            onChange={(e) =>
                              handleChangeRelationship(e, idx, "children")
                            }
                          >
                            {t("slaSetting.dependChildren")}
                            <Tooltip title="If all child tasks succeeded, then the Parent task will also be successful.">
                              <img src={InforCircle} alt="InforCircle" />
                            </Tooltip>
                          </Checkbox>
                        </WrapDependent>
                      )}
                    </Right>
                  </Rule>
                </Panel>
              );
            })}
          </Collapse>
          <AddNew onClick={() => _onAddRule()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ {t("slaSetting.addSLARule")}</span>
          </AddNew>
        </>
      )}
    </Wrapper>
  );
}

export default RuleSetting;

const Wrapper = styled.div`
  opacity: ${({ currentStep }) => (currentStep === 1 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 1 ? "100%" : "0")};
  transition: opacity 0s ease-in;
  pointer-events: ${({ currentStep }) => (currentStep === 1 ? "auto" : "none")};

  .action {
    position: absolute;
    top: 1px;
    right: 5px;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    img {
      cursor: pointer;
    }
  }

  .action-edit {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  .relationship {
    .ant-form-item-control-input-content {
      display: flex;
      align-items: center;
    }
  }

  .form-custom {
    margin-bottom: 8px;
  }

  .ant-collapse-header {
    position: relative !important;
    background: #efeff4;
    padding: 0px 16px !important;
    border: 1px solid #ececec;
    align-items: center !important;

    border-radius: 2px !important;

    :hover {
      border: 1px solid ${(props) => props.theme.main};

      .action {
        visibility: visible;
        opacity: 1;

        img {
          :hover {
            background: #eeeeee;
          }
        }
      }

      .action-edit {
        visibility: visible;
        opacity: 1;
      }
    }
  }

  .ant-collapse-item {
    margin-bottom: 16px;
    border-bottom: none;
  }

  .ant-collapse {
    border: none;
    background: #fff;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-collapse-content > .ant-collapse-content-box {
    padding: 16px 0px;
  }

  .ant-collapse-header-text {
    width: 90%;
    flex: unset !important;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }

  :active {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff;
    }
  }

  :focus {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff;
    }
  }
`;

// const Wrap = styled.div``;

const RuleName = styled.div`
  display: flex;
  align-items: center;
  font-family: var(--roboto-500);
  font-size: 18px;
  color: #252424;

  img {
    width: 20px;
    margin-left: 13px;
  }
`;

const AddNew = styled.div`
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-top: 16px;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Rule = styled.div`
  display: flex;
`;

const Left = styled.div`
  flex: 1;
  margin-right: 8px;
`;

const WrapConditions = styled.div`
  padding: 16px;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
`;

const SuccessCondition = styled.div`
  .required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: -4px;
      left: 20px;
    }
  }
`;

const TimeToComplete = styled.fieldset`
  padding: 24px 24px 24px 24px;
  border: 1px solid #d9d9d9;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const WrapInputTime = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    color: #2c2c2c;
  }

  .ant-input {
    width: 7.8rem;
  }
`;

const WrapAlert = styled.fieldset`
  padding: 24px 24px 24px 24px;
  border: 1px solid #d9d9d9;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const Alert = styled.div`
  padding: 10px;
  background: #f5f5f5;
  border-radius: 10px;
  margin-bottom: 16px;

  img {
    cursor: pointer;

    :hover {
      background: #eeeeee;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const NameAlert = styled.div`
  color: ${(props) => props.theme.main};
  font-family: var(--roboto-700);
  display: flex;
  justify-content: space-between;
`;

const BeforeViolation = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;

  .alert_title {
    font-size: 16px;
    /* position: relative;

    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 5px;
      left: -8px;
    } */
  }

  p {
    margin-bottom: 0;
    color: #2c2c2c;
  }

  .ant-input {
    width: 82px;
  }
`;

const Right = styled.div`
  flex: 1;
  margin-left: 8px;
  padding: 16px;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  height: fit-content;
`;

const RightTitle = styled.fieldset`
  padding: 24px 24px 24px 24px;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 16px;

  :nth-child(2) {
    margin-bottom: 24px;
  }

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
    position: relative;

    ::before {
      content: "";
      position: absolute;
      width: 3px;
      height: 19px;
      background: ${({ type }) =>
        type === "success"
          ? "#00AB55"
          : type === "init_action"
          ? "#4884FA"
          : type === "overdue_action"
          ? "#db9303"
          : "#f5222d"};
      left: 1px;
      top: 2.5px;
    }
  }

  /* font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  margin-left: 4px;
  position: relative;
  margin-bottom: 8px; */
`;

const WrapRelationShip = styled.div`
  display: flex;
  margin-bottom: 8px;
`;

const WrapDependent = styled.div`
  display: flex;
  flex-direction: column;

  .ant-checkbox-wrapper {
    margin-bottom: 8px;

    :nth-child(2) {
      margin-bottom: 0;
    }
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const Action = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;

  img {
    cursor: pointer;

    :hover {
      background: #eeeeee;
    }
  }

  p {
    color: #2c2c2c;
    font-size: 16px;
    margin-bottom: 0;
  }

  .ant-select {
    width: 13rem;
  }

  .ant-input {
    width: 13rem;
  }

  .ant-picker {
    width: 11rem;
  }

  .ant-input-group {
    width: 13rem;
  }
`;

const AddNewAction = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-top: 8px;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const SubTitle = styled.p`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  margin-left: 8px;
  margin-bottom: 0;
`;

const RowCustom = styled(Row)`
  position: relative;
  margin-top: 16px;

  ::before {
    content: "";
    position: absolute;
    width: 1px;
    height: 100%;
    background: #d9d9d9;
    left: 0;
    top: 0;
  }
`;

const ActionAlert = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
  width: 100%;
  margin-left: 8px;

  :last-child {
    margin-bottom: 5px;
  }

  img {
    cursor: pointer;

    :hover {
      background: #eeeeee;
    }
  }

  p {
    color: #2c2c2c;
    font-size: 16px;
    margin-bottom: 0;
  }

  .ant-select {
    width: 200px;
  }

  .ant-input {
    width: 200px;
  }

  .ant-picker {
    width: 170px;
  }

  .ant-input-group {
    width: 200px;
  }
`;

const WrapperTime = styled.div`
  display: flex;
  gap: 8px;
  align-items: center;
`;

const CustomCheckbox = styled(Checkbox)`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const BasicInfo = styled.fieldset`
  padding: 16px 16px 0px 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 10px;
  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
    font-family: var(--roboto-500);
  }
`;
