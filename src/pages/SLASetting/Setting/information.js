import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";

import { loadFieldsObject } from "redux/slices/slaSetting";
import { t } from "i18next";
import BusinessHours from "pages/SLASetting/ServiceType/BusinessHours";

function Information({
  form,
  objects,
  type,
  setType,
  dataBusinessHours,
  $dataBusinessHours,
  currentStep,
  setListTemplate,
  detailsSLA,
}) {
  const dispatch = useDispatch();

  const { listEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );

  const [listObjects, setListObjects] = useState([]);

  const typeSLA = [
    {
      label: "24/7",
      value: "calendar",
    },
    {
      label: t("slaSetting.businessHours"),
      value: "business_hours",
    },
  ];

  const handleChangeType = (value) => {
    setType(value.target.value);
  };

  const handleSelectObject = useCallback(
    (value) => {
      dispatch(
        loadFieldsObject({
          object_id: value,
          api_version: "2",
        })
      );

      const tempList = listEmailTemplate.filter(
        (item) => item.object_id === value
      );

      let resultListTemplate = [];
      tempList.map((item) => {
        return resultListTemplate.push({
          label: item.email_template_name,
          value: item._id,
        });
      });

      setListTemplate(resultListTemplate);
    },
    [dispatch, listEmailTemplate, setListTemplate]
  );

  useEffect(() => {
    if (Object.keys(objects).length > 0) {
      let tempObjects = [];
      Object.entries(objects).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [objects]);

  useEffect(() => {
    if (Object.keys(detailsSLA).length > 0) {
      form.setFieldsValue({
        sla_name: detailsSLA.title,
        description: detailsSLA.description,
        object: detailsSLA.object_id,
        type: detailsSLA.sla_type,
        working_day: detailsSLA.working_days,
      });

      setType(detailsSLA.sla_type);
      $dataBusinessHours({
        holidays: _.get(detailsSLA, "holidays", []),
        shifts: _.get(detailsSLA, "shift_work", []),
        workingDays: _.get(detailsSLA, "working_days", []),
      });
      handleSelectObject(detailsSLA.object_id);
    }
  }, [detailsSLA, form, handleSelectObject, setType, $dataBusinessHours]);

  return (
    <Wrapper currentStep={currentStep}>
      <Form.Item
        label={t("slaSetting.slaName")}
        name="sla_name"
        rules={[{ required: true, message: "Please input" }]}
      >
        <Input placeholder={t("slaSetting.enterSlaName")} />
      </Form.Item>

      <Form.Item label={t("common.description")} name="description">
        <Input placeholder={t("slaSetting.enterDes")} />
      </Form.Item>
      <Form.Item
        label={t("common.selectObject")}
        name="object"
        rules={[{ required: true, message: "Please select" }]}
      >
        <Select
          disabled={Object.keys(detailsSLA).length > 0}
          options={listObjects}
          placeholder={t("common.selectObject")}
          onChange={handleSelectObject}
          optionFilterProp="label"
          showSearch
        />
      </Form.Item>
      <Form.Item label={t("slaSetting.chooseType")} name="type">
        <Radio.Group
          optionType="button"
          options={typeSLA}
          defaultValue="calendar"
          onChange={handleChangeType}
        />
      </Form.Item>

      {type === "business_hours" && (
        <BusinessHours
          form={form}
          dataBusinessHours={dataBusinessHours}
          $dataBusinessHours={$dataBusinessHours}
        />
      )}
    </Wrapper>
  );
}

export default Information;

const Wrapper = styled.div`
  width: 55rem;
  margin: 0 auto;
  opacity: ${({ currentStep }) => (currentStep === 0 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 0 ? "100%" : "0")};
  transition: opacity 0s ease-in;
  pointer-events: ${({ currentStep }) => (currentStep === 0 ? "auto" : "none")};

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper {
    width: 148px;
    text-align: center;

    :hover {
      color: ${(props) => props.theme.main};
    }
  }
`;
