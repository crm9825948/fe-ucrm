import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import isEmail from "validator/lib/isEmail";

import Collapse from "antd/lib/collapse";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Select from "antd/lib/select";
import InputNumber from "antd/lib/input-number";
import Checkbox from "antd/lib/checkbox";

// import PlusGreen from "assets/icons/common/plus-green.svg";
import EmptySLA from "assets/icons/SLA/empty_SLA.png";
import Delete from "assets/icons/common/delete.svg";
import DuplicateRule from "assets/icons/SLA/duplicate_rule.svg";
import MinusSquare from "assets/icons/SLA/MinusSquare.svg";
import PlusSquare from "assets/icons/SLA/PlusSquare.svg";
import { t } from "i18next";
import { sendToUserSLA } from "util/staticData";

function EscalateRule({
  currentStep,
  escalateRules,
  setEscalateRules,
  listUserEmail,
  listTemplate,
  fields_related,
  listFields,
  handleFieldType,
  detailsSLA,
  isEdit,
  setEmailEscalateValid,
}) {
  const { Panel } = Collapse;

  const [listValid, setListValid] = useState([]);

  useEffect(() => {
    let temp = [];
    sendToUserSLA.concat(listUserEmail).forEach((item) => {
      temp.push(item.value);
    });
    setListValid(temp);
  }, [listUserEmail]);

  const _onAddEscalateRule = () => {
    setEscalateRules([
      ...escalateRules,
      {
        email_template_id: undefined,
        time_after_violation: {
          days: 0,
          hours: 0,
          minutes: 0,
        },
        title: "",
        send_email_to: [],
        escalate_action: [],
        is_repeated: false,
      },
    ]);
  };

  const _onDuplicateRule = (item, index) => {
    let tempRules = [...escalateRules];
    tempRules.splice(index + 1, 0, {
      ...item,
      title: item.title + " (Copy)",
    });
    setEscalateRules(tempRules);
  };

  const _onDeleteEscalateRule = (index) => {
    let tempRules = [...escalateRules];
    tempRules.splice(index, 1);
    setEscalateRules(tempRules);
  };

  const handleChangeRuleName = (e, indexRule) => {
    let tempRules = [...escalateRules];
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      title: e,
    };
    setEscalateRules(tempRules);
  };

  const _onAddEscalateAction = (indexRule) => {
    let tempRules = [...escalateRules];
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      escalate_action: [
        ...tempRules[indexRule].escalate_action,
        {
          field_id: "",
          field_type: "",
          new_value: "",
          is_dynamic: false,
        },
      ],
    };
    setEscalateRules(tempRules);
  };

  const _onDeleteEscalateAction = (indexRule, indexAction) => {
    let tempRules = [...escalateRules];
    let tempActions = [...tempRules[indexRule].escalate_action];

    tempActions.splice(indexAction, 1);

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      escalate_action: tempActions,
    };

    // tempRules[indexRule].escalate_action.splice(indexAction, 1);
    setEscalateRules(tempRules);
  };

  const handleSelectFieldAction = (value, indexRule, indexAction) => {
    let tempRules = [...escalateRules];
    let tempActions = [...tempRules[indexRule].escalate_action];

    tempActions[indexAction] = {
      field_id: value,
      new_value: "",
      field_type: "",
      is_dynamic: false,
    };

    tempRules[indexRule] = {
      ...tempRules[indexRule],
      escalate_action: tempActions,
    };

    setEscalateRules(tempRules);
  };

  const handleSelectEmailTemplate = (value, indexRule) => {
    let tempRules = [...escalateRules];
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      email_template_id: value,
    };
    setEscalateRules(tempRules);
  };

  const handleSelectSendEmailTo = (value, indexRule) => {
    if (value && value.length > 0) {
      const found = value.filter((item) => !listValid.includes(item));
      if (found.some((ele) => isEmail(ele) === false)) {
        setEmailEscalateValid(false);
      } else {
        setEmailEscalateValid(true);
      }
    }

    let tempRules = [...escalateRules];
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      send_email_to: value,
    };

    setEscalateRules(tempRules);
  };

  const handleChangeTime = (e, indexRule, type) => {
    let tempRules = [...escalateRules];

    switch (type) {
      case "days":
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          time_after_violation: {
            ...tempRules[indexRule].time_after_violation,
            days: e,
          },
        };
        setEscalateRules(tempRules);
        break;
      case "hours":
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          time_after_violation: {
            ...tempRules[indexRule].time_after_violation,
            hours: e,
          },
        };
        setEscalateRules(tempRules);
        break;

      default:
        tempRules[indexRule] = {
          ...tempRules[indexRule],
          time_after_violation: {
            ...tempRules[indexRule].time_after_violation,
            minutes: e,
          },
        };
        setEscalateRules(tempRules);
        break;
    }
  };

  const handleCheckRepeat = (value, indexRule) => {
    let tempRules = [...escalateRules];
    tempRules[indexRule] = {
      ...tempRules[indexRule],
      is_repeated: value,
    };

    setEscalateRules(tempRules);
  };

  useEffect(() => {
    if (
      Object.keys(detailsSLA).length > 0 &&
      isEdit &&
      fields_related.length > 0
    ) {
      setEscalateRules(detailsSLA.escalate_rules);
    }
  }, [detailsSLA, setEscalateRules, isEdit, fields_related]);

  return (
    <Wrapper currentStep={currentStep}>
      {escalateRules.length === 0 ? (
        <Empty>
          <img src={EmptySLA} alt="empty" />
          <p>{t("slaSetting.noEscalation")}</p>
          <AddButton onClick={() => _onAddEscalateRule()}>
            {t("slaSetting.addEscalation")}
          </AddButton>
        </Empty>
      ) : (
        <>
          <Collapse
            accordion
            expandIcon={({ isActive }) =>
              isActive ? (
                <img src={MinusSquare} alt="minus" />
              ) : (
                <img src={PlusSquare} alt="plus" />
              )
            }
          >
            {escalateRules.map((escalateRule, idx) => {
              return (
                <Panel
                  header={
                    <>
                      <RuleName>{"Level " + (idx + 1)}</RuleName>
                      <div className="action">
                        <Tooltip title="Duplicate rule">
                          <img
                            onClick={(event) => {
                              _onDuplicateRule(escalateRule, idx);
                              event.stopPropagation();
                            }}
                            src={DuplicateRule}
                            alt="duplicate"
                            style={{ marginRight: "16px" }}
                          />
                        </Tooltip>
                        <Tooltip title="Delete">
                          <img
                            onClick={(event) => {
                              _onDeleteEscalateRule(idx);
                              event.stopPropagation();
                            }}
                            src={Delete}
                            alt="delete"
                          />
                        </Tooltip>
                      </div>
                    </>
                  }
                  key={idx}
                >
                  <Row align="middle">
                    <Col flex="175px" className="required">
                      {t("slaSetting.escalationName")}
                    </Col>
                    <Col flex="510px">
                      <Input
                        placeholder={t("slaSetting.enterEscalationName")}
                        value={escalateRule.title}
                        onChange={(e) =>
                          handleChangeRuleName(e.target.value, idx)
                        }
                      />
                    </Col>
                  </Row>
                  <Row align="middle">
                    <Col flex="175px">{t("slaSetting.escalationAction")}</Col>
                    <Col flex="80px">
                      <Button onClick={() => _onAddEscalateAction(idx)}>
                        + {t("slaSetting.add")}
                      </Button>
                    </Col>
                  </Row>
                  {escalateRule.escalate_action.length > 0 && (
                    <RowCustom>
                      {escalateRule.escalate_action.map(
                        (action, indexAction) => {
                          return (
                            <Action key={indexAction}>
                              <p>{t("slaSetting.set")}</p>

                              <Select
                                placeholder="Please select"
                                value={action.field_id}
                                onChange={(value) =>
                                  handleSelectFieldAction(
                                    value,
                                    idx,
                                    indexAction
                                  )
                                }
                                showSearch
                                optionFilterProp="children"
                                filterOption={(inputValue, option) => {
                                  if (option.children) {
                                    return option.children
                                      .toLowerCase()
                                      .indexOf(inputValue.toLowerCase()) >= 0
                                      ? true
                                      : false;
                                  } else if (option.label) {
                                    return option.label
                                      .toLowerCase()
                                      .indexOf(inputValue.toLowerCase()) >= 0
                                      ? true
                                      : false;
                                  }
                                }}
                              >
                                {listFields.map((ele) => {
                                  return (
                                    <Select.Option
                                      disabled={
                                        escalateRule.escalate_action.find(
                                          (field) =>
                                            field.field_id === ele.value
                                        )
                                          ? true
                                          : false
                                      }
                                      key={ele.value}
                                    >
                                      {ele.label}
                                    </Select.Option>
                                  );
                                })}
                              </Select>
                              <p>{t("slaSetting.to")}</p>
                              {action.field_id !== "" ? (
                                <>
                                  {handleFieldType(
                                    fields_related.find(
                                      (ele) => ele.ID === action.field_id
                                    ),
                                    idx,
                                    indexAction,
                                    "escalateAction"
                                  )}
                                </>
                              ) : (
                                <Input
                                  disabled
                                  placeholder={t("slaSetting.selectValue")}
                                />
                              )}
                              <Tooltip title={t("common.delete")}>
                                <img
                                  onClick={() =>
                                    _onDeleteEscalateAction(idx, indexAction)
                                  }
                                  src={Delete}
                                  alt="delete"
                                />
                              </Tooltip>
                            </Action>
                          );
                        }
                      )}
                    </RowCustom>
                  )}
                  <Row align="middle">
                    <Col flex="175px" className="required">
                      {t("slaSetting.afterViolation")}
                    </Col>
                    <Col flex="510px">
                      <BeforeViolation>
                        <InputNumber
                          min={0}
                          value={escalateRule.time_after_violation.days}
                          onChange={(e) => handleChangeTime(e, idx, "days")}
                        />
                        <p>{t("slaSetting.day")}</p>
                        <InputNumber
                          min={0}
                          value={escalateRule.time_after_violation.hours}
                          onChange={(e) => handleChangeTime(e, idx, "hours")}
                        />
                        <p>{t("slaSetting.hour")}</p>
                        <InputNumber
                          min={0}
                          value={escalateRule.time_after_violation.minutes}
                          onChange={(e) => handleChangeTime(e, idx, "minutes")}
                        />
                        <p>{t("slaSetting.minute")}</p>
                      </BeforeViolation>
                    </Col>
                  </Row>
                  <Row align="middle">
                    <Col flex="175px">{t("slaSetting.emailTemplate")}</Col>
                    <Col flex="510px">
                      <Select
                        value={escalateRule.email_template_id}
                        options={listTemplate}
                        placeholder={t("slaSetting.selectEmailTemplate")}
                        onChange={(value) =>
                          handleSelectEmailTemplate(value, idx)
                        }
                        optionFilterProp="label"
                        showSearch
                      />
                    </Col>
                  </Row>
                  <Row align="middle">
                    <Col flex="175px">{t("slaSetting.sendEmail")}</Col>
                    <Col flex="510px">
                      <Select
                        disabled={!escalateRule.email_template_id}
                        value={escalateRule.send_email_to}
                        options={sendToUserSLA.concat(listUserEmail)}
                        placeholder={t("slaSetting.selectEmail")}
                        mode="tags"
                        onChange={(value) =>
                          handleSelectSendEmailTo(value, idx)
                        }
                      />
                    </Col>
                  </Row>
                  <Row align="middle">
                    <Col flex="210px">{t("slaSetting.repeatEscalation")}</Col>
                    <Col flex="100px">
                      <Checkbox
                        checked={escalateRule.is_repeated}
                        onChange={(e) =>
                          handleCheckRepeat(e.target.checked, idx)
                        }
                      />
                    </Col>
                  </Row>
                </Panel>
              );
            })}
          </Collapse>
          <AddNew onClick={() => _onAddEscalateRule()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ {t("slaSetting.addEscalationRule")}</span>
          </AddNew>
        </>
      )}
    </Wrapper>
  );
}

export default EscalateRule;

const Wrapper = styled.div`
  opacity: ${({ currentStep }) => (currentStep === 2 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 2 ? "100%" : "0")};
  pointer-events: ${({ currentStep }) => (currentStep === 2 ? "auto" : "none")};
  transition: opacity 0s ease-in;

  .action {
    position: absolute;
    top: 1px;
    right: 5px;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    img {
      cursor: pointer;
    }
  }

  .required {
    ::after {
      display: inline-block;
      margin-left: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }

  .ant-collapse-header {
    background: #e3e3e3;
    padding: 0 16px !important;
    border: 1px solid #ececec;
    align-items: center !important;

    :hover {
      border: 1px solid ${(props) => props.theme.main};

      .action {
        visibility: visible;
        opacity: 1;

        img {
          :hover {
            background: #eeeeee;
          }
        }
      }
    }
  }

  .ant-collapse-item {
    margin-bottom: 16px;
    border-bottom: none;
  }

  .ant-collapse {
    border: none;
    background: #fff;
  }

  .ant-collapse-content {
    width: 700px;
    margin: 0 auto;
    border-top: none;
    margin-top: 24px;

    > .ant-collapse-content-box {
      padding: 0;
    }
  }

  .ant-select {
    width: 100%;
  }

  .ant-row {
    margin-bottom: 5px;
  }

  .ant-col {
    font-size: 16px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }

  :active {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff !important;
    }
  }

  :focus {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff !important;
    }
  }
`;

const AddNew = styled.div`
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-top: 16px;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const RuleName = styled.div`
  font-family: var(--roboto-500);
  font-size: 18px;
  color: #252424;
`;

const BeforeViolation = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  p {
    margin-bottom: 0;
    color: #2c2c2c;
  }

  .ant-input {
    width: 82px;
  }
`;

const RowCustom = styled(Row)`
  position: relative;

  ::before {
    content: "";
    position: absolute;
    width: 1px;
    height: 100%;
    background: #d9d9d9;
    left: 0;
    top: 0;
  }
`;

const Action = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
  width: 100%;
  margin-left: 8px;

  :last-child {
    margin-bottom: 5px;
  }

  img {
    cursor: pointer;

    :hover {
      background: #eeeeee;
    }
  }

  p {
    color: #2c2c2c;
    font-size: 16px;
    margin-bottom: 0;
  }

  .ant-select {
    width: 200px;
  }

  .ant-input {
    width: 200px;
  }

  .ant-picker {
    width: 170px;
  }

  .ant-input-group {
    width: 200px;
  }
`;
