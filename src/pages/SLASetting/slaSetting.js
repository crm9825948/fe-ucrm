import { useCallback, useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Switch from "antd/lib/switch";
import Pagination from "antd/lib/pagination";

import EmptySLA from "assets/icons/SLA/empty_SLA.png";
import Edit from "assets/icons/SLA/edit_sla.svg";
import Delete from "assets/icons/SLA/delete_sla.svg";
import Clone from "assets/icons/SLA/clone.svg";
import ImgConfirm from "assets/icons/common/confirm.png";

import ModalConfirm from "components/Modal/ModalConfirm";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import {
  loadListSLA,
  activeSLA,
  duplicateSLA,
  deleteSLA,
  unMountSLA,
  toggleNotification,
} from "redux/slices/slaSetting";
import { setShowModalConfirm } from "redux/slices/global";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { changeTitlePage } from "redux/slices/authenticated";

function SLASetting(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { Column } = Table;
  const { Text } = Typography;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { listSLA, totalRecordSLA } = useSelector(
    (state) => state.slaSettingReducer
  );

  const [dataTable, setDataTable] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [dataDuplicate, setDataDuplicate] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.slaSetting")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "sla_setting" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActive = (checked, id) => {
    dispatch(
      activeSLA({
        status: checked,
        _id: id,
      })
    );
  };

  const handleNoti = (id) => {
    dispatch(
      toggleNotification({
        _id: id,
      })
    );
  };

  const _onDuplicateSLA = (id) => {
    dispatch(setShowModalConfirm(true));
    setDataDuplicate({
      _id: id,
    });
  };

  const _onDeleteSLA = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  const getListSLA = useCallback(() => {
    dispatch(
      loadListSLA({
        current_page: currentPage,
        record_per_page: recordPerPage,
        api_version: "2",
      })
    );
  }, [dispatch, currentPage, recordPerPage]);

  const showTotal = () => {
    return `${t("common.total")} ${totalRecordSLA} ${t("common.items")}`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    getListSLA();
  }, [getListSLA]);

  useEffect(() => {
    let tempList = [];
    listSLA.map((item) => {
      return tempList.push({
        key: item._id,
        active: item.status,
        notification: _.get(item, "turn_off_notification", false),
        name: item.title,
        description: item.description,
        object: item.object_name,
        type: item.sla_type === "calendar" ? "Calendar" : "Business hours",
      });
    });

    setTimeout(() => {
      setDataTable(tempList);
    }, 50);
  }, [dispatch, listSLA]);

  useEffect(() => {
    return () => {
      dispatch(unMountSLA());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.slaSetting")}</BreadcrumbItem>
        </Breadcrumb>
        {listSLA.length > 0 && checkRule("create") && (
          <AddButton onClick={() => navigate("/add-sla-setting")}>
            + {t("slaSetting.addSLA")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listSLA.length === 0 ? (
        <Empty>
          <img src={EmptySLA} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("settings.slaSetting")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={() => navigate("/add-sla-setting")}>
              + {t("slaSetting.addSLA")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapSLA>
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title={t("common.status")}
              dataIndex="active"
              key="active"
              width="120px"
              render={(text, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checkedChildren={t("workflow.on")}
                  unCheckedChildren={t("workflow.off")}
                  checked={text}
                  onChange={(checked) => handleActive(checked, record.key)}
                />
              )}
            />
            <Column
              title={t("slaSetting.notification")}
              dataIndex="notification"
              key="notification"
              width="120px"
              render={(text, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checkedChildren={t("workflow.on")}
                  unCheckedChildren={t("workflow.off")}
                  checked={!text}
                  onChange={(checked) => handleNoti(record.key)}
                />
              )}
            />
            <Column
              title={t("slaSetting.slaName")}
              dataIndex="name"
              key="name"
              width="500px"
              sorter={(a, b) => a.name.localeCompare(b.name)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("common.description")}
              dataIndex="description"
              key="description"
              width="300px"
              sorter={(a, b) => a.description.localeCompare(b.description)}
              render={(text) => (
                <Text style={{ width: "300px" }} ellipsis={{ tooltip: text }}>
                  {text}
                </Text>
              )}
            />
            <Column
              title={t("object.object")}
              dataIndex="object"
              key="object"
              sorter={(a, b) => a.object.localeCompare(b.object)}
              width="200px"
            />
            <Column
              title={t("slaSetting.slaType")}
              dataIndex="type"
              key="type"
              sorter={(a, b) => a.type.localeCompare(b.type)}
              width="264px"
            />

            {(checkRule("create") ||
              checkRule("edit") ||
              checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                fixed="right"
                width="150px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() =>
                            navigate(`/edit-sla-setting/${record.key}`)
                          }
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDeleteSLA(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                    {checkRule("create") && (
                      <Tooltip title={t("common.duplicate")}>
                        <img
                          onClick={() => _onDuplicateSLA(record.key)}
                          src={Clone}
                          alt="duplicate"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={totalRecordSLA}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </WrapSLA>
      )}

      <ModalConfirm
        title={t("slaSetting.duplicateSLA")}
        decs={t("slaSetting.noteDuplicate")}
        method={duplicateSLA}
        data={dataDuplicate}
        img={ImgConfirm}
      />
      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteSLA}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default withTranslation()(SLASetting);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }

  :active {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff;
    }
  }

  :focus {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff;
    }
  }
`;

const WrapSLA = styled.div`
  padding: 24px;
  background: #fff;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
