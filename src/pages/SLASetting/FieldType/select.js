import Select from "antd/lib/select";
import _ from "lodash";

function SelectType({
  initAction,
  failureAction,
  successAction,
  overdueAction,
  escalateAction,
  alertAction,
  field,
  type,
  indexRule,
  indexAction,
  indexAlert,
  listUser,
  handleChangeFieldAction,
}) {
  return (
    <Select
      value={
        type === "failure"
          ? failureAction[indexAction].new_value !== ""
            ? failureAction[indexAction].new_value
            : undefined
          : type === "init"
          ? initAction[indexAction].new_value !== ""
            ? initAction[indexAction].new_value
            : undefined
          : type === "success"
          ? successAction[indexAction].new_value !== ""
            ? successAction[indexAction].new_value
            : undefined
          : type === "overdue"
          ? overdueAction[indexAction].new_value !== ""
            ? overdueAction[indexAction].new_value
            : undefined
          : type === "alertAction"
          ? _.get(alertAction, `[${indexAction}].new_value`, "") !== ""
            ? _.get(alertAction, `[${indexAction}].new_value`, "")
            : undefined
          : escalateAction[indexAction].new_value !== ""
          ? escalateAction[indexAction].new_value
          : undefined
      }
      onChange={(e) =>
        handleChangeFieldAction(
          e,
          indexRule,
          indexAction,
          field,
          type,
          indexAlert
        )
      }
      options={field.type === "user" ? listUser : field.option}
    />
  );
}

export default SelectType;
