import InputNumber from "antd/lib/input-number";
import _ from "lodash";

function Number({
  initAction,
  failureAction,
  successAction,
  overdueAction,
  escalateAction,
  alertAction,
  field,
  type,
  indexRule,
  indexAction,
  indexAlert,
  handleChangeFieldAction,
}) {
  return (
    <InputNumber
      style={{ width: "13rem" }}
      value={
        type === "failure"
          ? failureAction[indexAction].new_value
          : type === "init"
          ? initAction[indexAction].new_value
          : type === "success"
          ? successAction[indexAction].new_value
          : type === "overdue"
          ? overdueAction[indexAction].new_value
          : type === "alertAction"
          ? _.get(alertAction, `[${indexAction}].new_value`, "")
          : escalateAction[indexAction].new_value
      }
      onChange={(e) =>
        handleChangeFieldAction(
          e,
          indexRule,
          indexAction,
          field,
          type,
          indexAlert
        )
      }
    />
  );
}

export default Number;
