import { useState } from "react";
import styled from "styled-components/macro";
import moment from "moment";
import _ from "lodash";

import DatePicker from "antd/lib/date-picker";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import ClearOutlined from "@ant-design/icons/ClearOutlined";

function Date({
  initAction,
  failureAction,
  successAction,
  overdueAction,
  escalateAction,
  alertAction,
  field,
  type,
  indexRule,
  indexAction,
  indexAlert,
  handleChangeFieldAction,
}) {
  const [typeDate, setTypeDate] = useState("");

  const optionsDate = [
    {
      label: "Action time",
      value: "today",
    },
    {
      label: "Success Milestone",
      value: "deadline",
    },
    {
      label: "Select Day",
      value: "select",
    },
  ];

  const handleChange = (value) => {
    setTypeDate(value);
  };

  return (
    <Wrapper>
      {typeDate === "select" ? (
        <Input.Group
          compact
          addonBefore={
            <ClearOutlined
              onClick={() => {
                setTypeDate("");
              }}
            />
          }
        >
          <DatePicker
            showTime={field.type === "datetime-local"}
            value={
              type === "failure"
                ? failureAction[indexAction].new_value !== "" &&
                  failureAction[indexAction].new_value !== "today" &&
                  failureAction[indexAction].new_value !== "deadline"
                  ? moment(failureAction[indexAction].new_value)
                  : undefined
                : type === "init"
                ? initAction[indexAction].new_value !== "" &&
                  initAction[indexAction].new_value !== "today" &&
                  initAction[indexAction].new_value !== "deadline"
                  ? moment(initAction[indexAction].new_value)
                  : undefined
                : type === "success"
                ? successAction[indexAction].new_value !== "" &&
                  successAction[indexAction].new_value !== "today" &&
                  successAction[indexAction].new_value !== "deadline"
                  ? moment(successAction[indexAction].new_value)
                  : undefined
                : type === "overdue"
                ? overdueAction[indexAction].new_value !== "" &&
                  overdueAction[indexAction].new_value !== "today" &&
                  overdueAction[indexAction].new_value !== "deadline"
                  ? moment(overdueAction[indexAction].new_value)
                  : undefined
                : type === "alertAction"
                ? _.get(alertAction, `[${indexAction}].new_value`, "") !== "" &&
                  _.get(alertAction, `[${indexAction}].new_value`, "") !==
                    "today" &&
                  _.get(alertAction, `[${indexAction}].new_value`, "") !==
                    "deadline"
                  ? moment(_.get(alertAction, `[${indexAction}].new_value`, ""))
                  : undefined
                : escalateAction[indexAction].new_value !== "" &&
                  escalateAction[indexAction].new_value !== "today" &&
                  escalateAction[indexAction].new_value !== "deadline"
                ? moment(escalateAction[indexAction].new_value)
                : undefined
            }
            onChange={(date, dateString) =>
              handleChangeFieldAction(
                dateString,
                indexRule,
                indexAction,
                field,
                type,
                indexAlert
              )
            }
          />
        </Input.Group>
      ) : (
        <Select
          placeholder="Please select"
          options={optionsDate}
          value={
            type === "failure"
              ? failureAction[indexAction].new_value
              : type === "init"
              ? initAction[indexAction].new_value
              : type === "success"
              ? successAction[indexAction].new_value
              : type === "overdue"
              ? overdueAction[indexAction].new_value
              : type === "alertAction"
              ? _.get(alertAction, `[${indexAction}].new_value`, "")
              : type === "escalateAction"
              ? escalateAction[indexAction].new_value
              : undefined
          }
          onChange={(e) => {
            handleChange(e);

            if (e !== "select") {
              handleChangeFieldAction(
                e,
                indexRule,
                indexAction,
                field,
                type,
                indexAlert
              );
            }
          }}
        />
      )}

      {typeDate === "select" && (
        <Clear
          onClick={() => {
            setTypeDate("");
          }}
        >
          Clear
        </Clear>
      )}
    </Wrapper>
  );
}

export default Date;

const Wrapper = styled.div`
  position: relative;
`;

const Clear = styled.span`
  position: absolute;
  top: 5px;
  right: -10px;
  cursor: pointer;
`;
