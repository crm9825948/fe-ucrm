import Input from "antd/lib/input";
import _ from "lodash";

function Text({
  initAction,
  failureAction,
  successAction,
  overdueAction,
  alertAction,
  escalateAction,
  field,
  type,
  indexRule,
  indexAction,
  indexAlert,
  handleChangeFieldAction,
}) {
  return (
    <Input
      value={
        type === "failure"
          ? failureAction[indexAction].new_value
          : type === "init"
          ? initAction[indexAction].new_value
          : type === "success"
          ? successAction[indexAction].new_value
          : type === "overdue"
          ? overdueAction[indexAction].new_value
          : type === "alertAction"
          ? _.get(alertAction, `[${indexAction}].new_value`, "")
          : escalateAction[indexAction].new_value
      }
      onChange={(e) =>
        handleChangeFieldAction(
          e.target.value,
          indexRule,
          indexAction,
          field,
          type,
          indexAlert
        )
      }
    />
  );
}

export default Text;
