import { Breadcrumb, Button, Space, Table } from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import editImg from "assets/icons/common/icon-edit.png";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import picklistImg from "assets/images/picklist/Picklist dependency.png";
import ModalDelete from "components/Modal/ModalConfirmDelete";
import React, { useEffect, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deletePicklist,
  loadDetail,
  loadPicklist,
} from "redux/slices/picklist";
import styled from "styled-components";
import DrawerPicklist from "./drawerPicklist";
import ModalEdit from "./modalEdit";
import _ from "lodash";
import { changeTitlePage } from "redux/slices/authenticated";

const Picklist = () => {
  const { t } = useTranslation();
  const [visible, setVisible] = useState(false);
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.picklistReducer);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [picklistID, setPicklistID] = useState("");
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "picklist_dependency" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    dispatch(changeTitlePage(t("picklist.picklist")));
    //eslint-disable-next-line
  }, [t]);

  useEffect(() => {
    dispatch(loadPicklist());
  }, [dispatch]);
  const navigate = useNavigate();
  const columns = [
    {
      title: t("picklist.module"),
      dataIndex: "Object_ID",
      key: "Object_ID",
    },
    {
      title: t("picklist.sourceField"),
      dataIndex: "Source_ID",
      key: "Source_ID",
    },
    {
      title: t("picklist.targetField"),
      dataIndex: "Target_ID",
      key: "Target_ID",
    },
    {
      title: t("picklist.action"),
      dataIndex: "",
      key: "",
      render: (_, record) => {
        return (
          <Space size="middle">
            <a href>
              {checkRule("edit") ? (
                <img
                  alt=""
                  src={editImg}
                  style={{ width: "20px" }}
                  onClick={() => {
                    setIsModalVisible(true);
                    setPicklistID(record._id);
                    dispatch(
                      loadDetail({
                        ID: record._id,
                      })
                    );
                  }}
                />
              ) : (
                ""
              )}
            </a>
            <a className="ant-dropdown-link" href>
              {checkRule("delete") ? (
                <img
                  alt=""
                  src={deleteImg}
                  style={{ width: "20px" }}
                  onClick={() => {
                    setPicklistID(record._id);
                    dispatch(setShowModalConfirmDelete(true));
                  }}
                />
              ) : (
                ""
              )}
            </a>
          </Space>
        );
      },
    },
  ];

  return (
    <Wrapper>
      <CustomHeader>
        <Breadcrumb>
          <Breadcrumb.Item>
            {/* eslint-disable-next-line  */}
            <a onClick={() => navigate("/settings")}>Settings</a>
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {/* eslint-disable-next-line */}
            <a onClick={() => navigate("/picklist-dependency")}>
              {t("picklist.picklist")}
            </a>
          </BreadcrumbItem>
        </Breadcrumb>
      </CustomHeader>
      <CustomContent>
        {data.length > 0 ? (
          <div>
            <div
              style={{
                marginBottom: "20px",
                marginTop: "-50px",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <CustomButton
                size="large"
                onClick={() => setVisible(true)}
                style={{
                  visibility: `${checkRule("create") ? "" : "hidden"}`,
                }}
                disabled={!checkRule("create")}
              >
                <img alt="" src={plusIcon} />
                {t("picklist.addNew")}
              </CustomButton>
            </div>
            <Table columns={columns} dataSource={data} pagination={false} />
          </div>
        ) : (
          <div className="empty">
            <img alt="" src={picklistImg} />
            <div className="text">
              There's no any <span>picklist dependency</span>
            </div>
            {checkRule("create") ? (
              <CustomButton size="large" onClick={() => setVisible(true)}>
                <img alt="" src={plusIcon} />
                {t("picklist.addNew")}
              </CustomButton>
            ) : (
              ""
            )}
          </div>
        )}
      </CustomContent>
      <DrawerPicklist visible={visible} setVisible={setVisible} />
      {isModalVisible ? (
        <ModalEdit
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          picklistID={picklistID}
        />
      ) : (
        ""
      )}

      <ModalDelete
        // openConfirm={openDelete}
        // setOpenConfirm={setOpenDelete}
        title={"bản ghi này"}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deletePicklist}
        dataDelete={{
          data: {
            id: picklistID,
          },
          load: {},
        }}
        setShowModalDelete={() => {}}
      />
    </Wrapper>
  );
};

export default withTranslation()(Picklist);

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomContent = styled.div`
  .empty {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    > img {
      width: 100px;
      margin-top: 60px;
    }
    .text {
      margin-top: 8px;
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      span {
        color: ${(props) => props.theme.main};
      }
    }
  }
`;

const Wrapper = styled.div`
  padding: 24px;
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  /* align-items: center; */
  margin-bottom: 12px;
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  margin-top: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
