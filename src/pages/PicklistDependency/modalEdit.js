import { Button, Checkbox, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { resetDetail, updateDetail } from "redux/slices/picklist";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";

const ModalEdit = (props) => {
  const { isModalVisible, setIsModalVisible, picklistID } = props;
  const dispatch = useDispatch();
  const [body, setBody] = useState([{}]);
  const [option, setOption] = useState([]);
  const [key, setKey] = useState([]);
  const { details } = useSelector((state) => state.picklistReducer);
  const { t } = useTranslation();

  const handleOk = () => {
    dispatch(
      updateDetail({
        Matrix: body,
        ID: picklistID,
      })
    );
    dispatch(resetDetail());
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    dispatch(resetDetail());
  };

  useEffect(() => {
    setBody(details && details[0] && details[0].Matrix);
  }, [details]);

  useEffect(() => {
    let optionArr = [];
    let keyArr = [];

    if (body) {
      // eslint-disable-next-line
      Object.entries((body && body[0]) || {}).forEach(([key, value], idx) => {
        // eslint-disable-next-line
        Object.entries(value).forEach(([key, value], idx) => {
          optionArr.push(key);
        });
      });
      // eslint-disable-next-line
      body.forEach((item, idx) => {
        // eslint-disable-next-line
        Object.entries(item).forEach(([key, value], index) => {
          keyArr.push(key);
        });
      });
      setKey(keyArr);
      setOption(optionArr);
    }
  }, [body]);

  return (
    <>
      <Modal
        title={t("picklist.editPicklist")}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={1200}
        footer={
          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              onClick={() => {
                handleOk();
              }}
            >
              {t("common.save")}
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => setIsModalVisible(false)}
            >
              {t("common.cancel")}
            </CustomButtonCancel>
          </CustomFooter>
        }
      >
        <Wrapper>
          <div className="title">
            {t("picklist.module")}:{" "}
            <span className="value">{details[0] && details[0].Object_ID}</span>
          </div>
          <div className="title">
            {t("picklist.sourceField")}:{" "}
            <span className="value">{details[0] && details[0].Source_ID}</span>
          </div>
          <div className="title">
            {t("picklist.targetField")}:{" "}
            <span className="value">{details[0] && details[0].Target_ID}</span>
          </div>
          <CustomTable className="table">
            <thead>
              <tr>
                {details[0] &&
                  details[0].Matrix.map((item, idx) => {
                    return (
                      <th>
                        {Object.entries(item).map(([key, value], index) => {
                          return key;
                        })}
                      </th>
                    );
                  })}
              </tr>
            </thead>
            <tbody>
              {option.map((item, idx) => {
                return (
                  <tr>
                    {key.map((ele, idx) => {
                      return (
                        <td>
                          <Checkbox
                            checked={
                              body &&
                              body[idx] &&
                              body[idx][ele] &&
                              body[idx][ele][item]
                            }
                            style={{ marginRight: "8px" }}
                            onClick={() => {
                              let temp = { ...body[idx][ele] };
                              temp[item] = !temp[item];
                              let newArr = [];
                              // eslint-disable-next-line
                              body.forEach((item, idx) => {
                                newArr.push({ ...item });
                              });
                              newArr[idx][ele] = { ...temp };

                              setBody(newArr);
                            }}
                          />
                          {item}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </tbody>
          </CustomTable>
        </Wrapper>
      </Modal>
    </>
  );
};

export default withTranslation()(ModalEdit);

const Wrapper = styled.div`
  .ant-checkbox-checked {
    background-color: ${(props) => props.theme.main};
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* Character/Color text main */

    color: #2c2c2c;
    .value {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      /* Character / Primary .85 */

      color: rgba(0, 0, 0, 0.85);
      margin-left: 8px;
    }
    margin-bottom: 8px;
  }
  height: 900px;
  overflow: scroll;
`;

const CustomTable = styled.table`
  width: max-content;
  margin-top: 16px;
  max-height: 626px;

  th {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* Character/Color text main */

    color: #2c2c2c;
    border: 1px solid #d9d9d9;
    padding: 9px;
    background-color: #f0f0f0;
  }
  td {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    color: #2c2c2c;
    padding: 9px;
    border-bottom: 1px solid #f0f0f0;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
