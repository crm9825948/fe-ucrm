import { Button, Drawer, Form, Select } from "antd";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadFields } from "redux/slices/fieldsManagement";
import { loadObject } from "redux/slices/objectsManagement";
import { createPicklist } from "redux/slices/picklist";
import styled from "styled-components";

const { Option, OptGroup } = Select;

const DrawerPicklist = (props) => {
  const { visible, setVisible } = props;
  const [form] = Form.useForm();
  const [formValue, setFormValue] = useState({});
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { sections } = useSelector((state) => state.fieldsManagementReducer);

  const { isLoading, data } = useSelector((state) => state.picklistReducer);

  const onClose = () => {
    setVisible(false);
    setFormValue({});
    form.resetFields();
  };

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadObject());
    // dispatch(loadCategory());
  }, [dispatch]);

  const onFinish = (values) => {
    let flag = false;
    // eslint-disable-next-line
    data.map((item, idx) => {
      if (
        item.Source_id === values.target &&
        item.Target_id === values.source
      ) {
        flag = true;
        Notification("error", "Already exist!");
      }
    });
    if (!flag) {
      dispatch(
        createPicklist({
          Object_ID: formValue.module,
          Source_ID: formValue.source,
          Target_ID: formValue.target,
        })
      );
    }

    setFormValue({});
  };

  useEffect(() => {
    if (isLoading === false) {
      setVisible(false);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  return (
    <>
      <Drawer
        title="Add new picklist"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={952}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          onValuesChange={(value, values) => {
            setFormValue(values);
            if (values.module !== formValue.module) {
              setFormValue({ ...values, source: undefined, target: undefined });
            }

            if (
              values.module === formValue.module &&
              values.source !== formValue.source
            ) {
              setFormValue({ ...values, target: undefined });
            }
          }}
        >
          <Form.Item
            label="Object"
            name="module"
            rules={[{ required: true, message: "Please select object!" }]}
          >
            <Select
              placeholder="Please select object!"
              onChange={(e) => {
                dispatch(
                  loadFields({
                    object_id: e,
                  })
                );
                form.setFieldsValue({
                  module: e,
                  target: undefined,
                  source: undefined,
                });
              }}
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(category).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {/* eslint-disable-next-line */}
                    {value.map((object, index) => {
                      if (object.Status)
                        return (
                          <Option value={object._id} key={object._id}>
                            {object.Name}
                          </Option>
                        );
                    })}
                  </OptGroup>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            label="Source field"
            name="source"
            rules={[{ required: true, message: "Please select source field!" }]}
          >
            <Select
              disabled={form.getFieldValue("module") ? false : true}
              placeholder="Please select source!"
              onChange={(e) => {
                form.setFieldsValue({
                  module: formValue.module,
                  target: undefined,
                  source: e,
                });
              }}
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(sections).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {/* eslint-disable-next-line */}
                    {value.map((field, index) => {
                      if (field.type === "select")
                        return <Option value={field.ID}>{field.name}</Option>;
                    })}
                  </OptGroup>
                );
              })}
            </Select>
          </Form.Item>

          <Form.Item
            label="Target field"
            name="target"
            rules={[{ required: true, message: "Please select target field!" }]}
          >
            <Select
              disabled={formValue.source ? false : true}
              placeholder="Please select target !"
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(sections).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {/* eslint-disable-next-line */}
                    {value.map((field, index) => {
                      if (field.type === "select")
                        return (
                          <Option
                            value={field.ID}
                            disabled={
                              formValue.source === field.ID ? true : false
                            }
                          >
                            {field.name}
                          </Option>
                        );
                    })}
                  </OptGroup>
                );
              })}
            </Select>
          </Form.Item>

          <WrapperSave>
            <CustomButtonSave type="primary" htmlType="submit">
              Save
            </CustomButtonSave>
            <CustomButtonCancel
              onClick={() => {
                setVisible(false);
                setFormValue({});
                form.resetFields();
              }}
            >
              {" "}
              Cancel
            </CustomButtonCancel>
          </WrapperSave>
        </Form>
      </Drawer>
    </>
  );
};

export default DrawerPicklist;

const WrapperSave = styled.div`
  position: fixed;
  bottom: 10px;
  right: 0px;
  text-align: right;
  background-color: #fff;
  padding-top: 16px;
  border-top: 1px solid #f0f0f0;
  width: 952px;
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  border-color: ${(props) => props.theme.main};
  margin-right: 16px;
  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;
  margin-right: 24px;
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;
