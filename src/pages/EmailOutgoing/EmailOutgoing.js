import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router";
import { useSelector } from "redux/store";
import styled from "styled-components";
import _ from "lodash";

import { Breadcrumb, Button, Switch, Table, Tag, Tooltip } from "antd";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";

import { checkConnection } from "redux/slices/emailIncoming";
import {
  deleteEmailOutgoing,
  getListEmailOutgoing,
  setDefaultEmailOutgoing,
  unmountEmailOutgoing,
  updateEditEmail,
  updateEditEmailSuccess,
} from "redux/slices/emailOutgoing";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  getListAccountMapping,
  getListAccountMappingSuccess,
} from "redux/slices/googleIntegration";

import deleteIcon from "assets/icons/email/deleteIcon.svg";
import editIcon from "assets/icons/email/editIcon.svg";
import emptyEmail from "assets/icons/email/empty-email.svg";

import ModalEmailOutgoing from "./ModalEmailOutgoing";
import { changeTitlePage } from "redux/slices/authenticated";

const EmailOutgoing = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { listEmailOutgoing } = useSelector(
    (state) => state.emailOutgoingReducer
  );
  // const { listGoogleIntegrationSetting, listAccountMapping } = useSelector(
  //   (state) => state.googleIntegrationReducer
  // );
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const [dataDelete, setDataDelete] = useState({});

  // eslint-disable-next-line
  const [, setSearchText] = useState("");
  // eslint-disable-next-line
  const [, setSearchedColumn] = useState("");
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(changeTitlePage(t("emailOutgoing.emailOutgoing")));
    //eslint-disable-next-line
  }, [t]);
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "outbox" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const addOutgoing = () => {
    dispatch(
      updateEditEmailSuccess({
        user_name: "",
        pass_word: "",
        description: "",
        server: "",
        protocol: "SMTP",
        ssl_type: "",
        port: "",
        port_custom: "",
        authen: false,
        is_default: false,
        from_email: "",
        object_id: "",
        is_use_username: false,
        username_authentication: "",
        is_save_sent_email: false,
        folder_name: "",
      })
    );
  };

  const checkConnectionMail = (record) => {
    dispatch(
      checkConnection({
        email_type: 0,
        setting_id: record._id,
      })
    );
  };
  const getColumnSearchProps = (dataIndex) => ({
    // filterDropdown: ({
    //   setSelectedKeys,
    //   selectedKeys,
    //   confirm,
    //   clearFilters,
    // }) => (
    //   <div style={{ padding: 8 }}>
    //     <Input
    //       placeholder={`Search`}
    //       value={selectedKeys[0]}
    //       onChange={(e) =>
    //         setSelectedKeys(e.target.value ? [e.target.value] : [])
    //       }
    //       onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //       style={{ marginBottom: 8, display: "block" }}
    //     />
    //     <Space>
    //       <Button
    //         type="primary"
    //         onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //         icon={<SearchOutlined />}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.search")}
    //       </Button>
    //       <Button
    //         onClick={() => handleReset(clearFilters)}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.reset")}
    //       </Button>
    //       <Button
    //         type="link"
    //         size="small"
    //         onClick={() => {
    //           confirm({ closeDropdown: false });
    //           setSearchText(selectedKeys[0]);
    //           setSearchedColumn(dataIndex);
    //         }}
    //       >
    //         {t("emailIncoming.filterData")}
    //       </Button>
    //     </Space>
    //   </div>
    // ),
    // filterIcon: (filtered) => (
    //   <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    // ),
    // onFilter: (value, record) =>
    //   record[dataIndex]
    //     ? record[dataIndex]
    //         .toString()
    //         .toLowerCase()
    //         .includes(value.toLowerCase())
    //     : "",
    render: (text) => text,
  });

  const editOutcoming = (record) => {
    dispatch(
      updateEditEmail({
        _id: record._id,
      })
    );
  };
  const deleteOutcoming = (record) => {
    setDataDelete({
      _id: record._id,
    });
    dispatch(setShowModalConfirmDelete(true));
  };
  const columns = [
    {
      title: "",
      key: "operation",
      render: (record) => (
        <>
          <CustomTag onClick={() => checkConnectionMail(record)}>
            {t("emailIncoming.test")}
          </CustomTag>
        </>
      ),
    },
    {
      title: t("object.object"),
      dataIndex: "object",
      key: "object",
      ...getColumnSearchProps("object"),
    },
    {
      title: t("common.description"),
      dataIndex: "description",
      key: "description",
      ...getColumnSearchProps("description"),
    },
    {
      title: t("emailIncoming.serverName"),
      dataIndex: "server_name",
      key: "server_name",
      ...getColumnSearchProps("server_name"),
    },
    {
      title: t("emailIncoming.emailLinking"),
      dataIndex: "user_name",
      key: "user_name",
      ...getColumnSearchProps("user_name"),
    },
    {
      title: t("emailIncoming.default"),
      dataIndex: "is_default",
      key: "is_default",
      render: (status, record) => (
        <>
          {
            <Switch
              disabled={record.is_default || !checkRule("edit")}
              onChange={(e) => dispatch(setDefaultEmailOutgoing(record._id))}
              checked={record.is_default}
            />
          }
        </>
      ),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "right",
      width: 100,
      render: (record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                onClick={() => editOutcoming(record)}
                src={editIcon}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => {
                  deleteOutcoming(record);
                }}
                src={deleteIcon}
                alt="delete"
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];
  // const handleSearch = (selectedKeys, confirm, dataIndex) => {
  //   confirm();
  //   setSearchText(selectedKeys[0]);
  //   setSearchedColumn(dataIndex);
  // };
  // const handleReset = (clearFilters) => {
  //   clearFilters();
  //   setSearchText("");
  // };

  useEffect(() => {
    dispatch(getListEmailOutgoing());
    dispatch(getListAccountMappingSuccess([]));
    // eslint-disable-next-line
  }, []);
  // unmount
  useEffect(
    () => () => {
      dispatch(unmountEmailOutgoing());
    },
    // eslint-disable-next-line
    []
  );
  useEffect(() => {
    // if (credential_id && credential_id.length > 0) {
    //   dispatch(
    //     getListAccountMapping({
    //       credential_id: credential_id,
    //     })
    //   );
    // } else {
    //   dispatch(getListAccountMappingSuccess([]));
    // }
    dispatch(
      getListAccountMapping({
        credential_id: "",
      })
    );
  }, [dispatch]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("emailOutgoing.emailOutgoing")}</BreadcrumbItem>
        </Breadcrumb>
        {listEmailOutgoing.length > 0 && checkRule("create") && (
          <AddButton onClick={addOutgoing}>
            + {t("emailOutgoing.addMailBox")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listEmailOutgoing.length > 0 ? (
        <WrapTable>
          <Table
            dataSource={listEmailOutgoing}
            columns={columns}
            pagination={{
              position: ["bottomRight"],
              showSizeChanger: true,
              defaultPageSize: 10,
              showQuickJumper: true,
            }}
          />
        </WrapTable>
      ) : (
        <Empty>
          <img src={emptyEmail} alt="empty" />
          <p>
            {t("object.noObject")}{" "}
            <span>{t("emailOutgoing.emailOutgoing")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={addOutgoing}>
              + {t("emailOutgoing.addMailBox")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalEmailOutgoing />

      <ModalConfimDelete
        title={t("emailOutgoing.deleteEmail")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteEmailOutgoing}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};

export default EmailOutgoing;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
  /* background-color: white; */
  .addMailBox {
    background-color: ${(props) => props.theme.main};
    color: white;
    &:hover {
      border: 1px solid ${(props) => props.theme.main};
    }
  }
  .ant-table-wrapper {
    background-color: white;
    padding: 10px;
  }
  .ant-table-thead > tr > th {
    background-color: white;
  }
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-table-pagination.ant-pagination {
    margin: 0;
    background-color: white;
    padding: 16px 0;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-switch-disabled {
    opacity: 1;
  }

  .ant-col-3 {
    button {
      float: right;
    }
  }
`;

const CustomTag = styled(Tag)`
  border: 1px solid ${(props) => props.theme.main};
  color: ${(props) => props.theme.main};
  padding: 0 16px;
  cursor: pointer;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
