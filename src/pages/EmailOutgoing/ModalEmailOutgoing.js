import { useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";

import {
  addNewEmailOutgoing,
  setShowDrawer,
  updateEditEmailSuccess,
  updateField,
  updateMailBoxOutgoing,
} from "redux/slices/emailOutgoing";
import { getListGoogleIntegrationSetting } from "redux/slices/googleIntegration";
import { loadOutlookAccount } from "redux/slices/outlook";
import AvatarImg from "assets/images/header/avatar.png";

function ModalSetting() {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { editEmailOutgoing, showDrawer } = useSelector(
    (state) => state.emailOutgoingReducer
  );

  const { allAccount } = useSelector((state) => state.outlookReducer);

  const {
    _id,
    user_name,
    pass_word,
    description,
    default_alias,
    server,
    ssl_type,
    port,
    authen,
    object_id,
    port_custom,
    is_use_username,
    username_authentication,
    server_type,
    // credential_id,
    google_account_integration,
    is_save_sent_email,
    folder_name,
    // eslint-disable-next-line
    // protocol,
    // from_email,
    // is_default,
  } = editEmailOutgoing;
  const { listObject } = useSelector((state) => state.objectsManagementReducer);
  const { listAccountMapping } = useSelector(
    (state) => state.googleIntegrationReducer
  );

  const _onSubmit = () => {
    const payload = {
      ...editEmailOutgoing,
      ...(server_type === "owa" && { port: 0 }),
    };

    if (editEmailOutgoing._id) {
      dispatch(updateMailBoxOutgoing(payload));
    } else {
      dispatch(addNewEmailOutgoing(payload));
    }
  };

  const _onCancel = () => {
    dispatch(setShowDrawer(false));
    form.resetFields();
  };

  const changeFieldEmail = (key, e) => {
    dispatch(
      updateField({
        key: key,
        value: e,
      })
    );
  };

  useEffect(() => {
    if (server_type && !_id) {
      dispatch(
        updateEditEmailSuccess({
          ...editEmailOutgoing,
          user_name: "",
          pass_word: "",
          description: "",
          server: "",
          protocol: "SMTP",
          ssl_type: "",
          port: "",
          port_custom: "",
          authen: false,
          is_default: false,
          from_email: "",
          object_id: "",
          is_use_username: false,
          username_authentication: "",
          is_save_sent_email: false,
          folder_name: "",
        })
      );
    }

    if (server_type === "google") {
      dispatch(getListGoogleIntegrationSetting());
      dispatch(
        updateEditEmailSuccess({
          ...editEmailOutgoing,
          server_name: "smtp.gmail.com",
          port: 587,
        })
      );
    }

    if (server_type === "outlook") {
      dispatch(loadOutlookAccount());
      dispatch(
        updateEditEmailSuccess({
          ...editEmailOutgoing,
          server_name: "outlook.office365.com",
          port: 587,
        })
      );
    }

    if (server_type === "owa") {
      dispatch(
        updateEditEmailSuccess({
          ...editEmailOutgoing,
          is_use_username: true,
        })
      );
    }
    // eslint-disable-next-line
  }, [server_type, _id, dispatch]);

  useEffect(() => {
    if (!showDrawer) {
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [showDrawer]);

  return (
    <ModalCustom
      title={
        editEmailOutgoing._id
          ? t("emailOutgoing.updateMailBox")
          : t("emailOutgoing.addMailBox")
      }
      open={showDrawer}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
        initialValues={{
          object_id: object_id,
          description: description,
          default_alias: default_alias,
          server: server,
          ssl_type: ssl_type,
          port: port,
          port_custom: port_custom,
          user_name: user_name,
          pass_word: pass_word,
          authen: authen,
        }}
      >
        <Form.Item
          label="Server type"
          name="server_type"
          rules={[
            {
              validator: (rule, value = server_type, cb) => {
                server_type.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={server_type}
        >
          <Select
            onChange={(e) => changeFieldEmail("server_type", e)}
            value={server_type}
          >
            <Select.Option value={"custom"}>Custom</Select.Option>
            <Select.Option value={"google"}>Google</Select.Option>
            <Select.Option value={"outlook"}>Outlook</Select.Option>
            <Select.Option value={"owa"}>OWA</Select.Option>
          </Select>
        </Form.Item>
        {server_type === "custom" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => changeFieldEmail("object_id", e)}
                value={object_id}
                disabled={editEmailOutgoing._id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listObject &&
                  listObject.map((item) => {
                    if (item.Status) {
                      return (
                        <Select.Option value={item._id}>
                          {item.Name}
                        </Select.Option>
                      );
                    } else {
                      return <></>;
                    }
                  })}
              </Select>
            </Form.Item>

            <Form.Item
              label={t("common.description")}
              name="description"
              // rules={[{ required: true, message: 'Trường này là bắt buộc' }]}
              valuePropName={description}
            >
              <Input.TextArea
                value={description}
                onChange={(e) =>
                  changeFieldEmail("description", e.target.value)
                }
              />
            </Form.Item>

            <Form.Item
              label="Default Alias"
              name="default_alias"
              valuePropName={default_alias}
            >
              <Input
                value={default_alias}
                onChange={(e) =>
                  changeFieldEmail("default_alias", e.target.value)
                }
              />
            </Form.Item>

            <Form.Item
              label="Server"
              name="server"
              rules={[
                {
                  validator: (rule, value = server, cb) => {
                    server.length === 0 ? cb(t("common.requiredField")) : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={server}
            >
              <Input
                value={server}
                onChange={(e) => changeFieldEmail("server", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("emailOutgoing.SSLType")}
              name="ssl_type"
              rules={[
                {
                  validator: (rule, value = ssl_type, cb) => {
                    ssl_type.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={ssl_type}
            >
              <Select
                value={ssl_type}
                onChange={(e) => changeFieldEmail("ssl_type", e)}
              >
                <Select.Option value={"SSL"}>SSL</Select.Option>
                <Select.Option value={"TLS"}>TLS</Select.Option>
                <Select.Option value={"none"}>None</Select.Option>
              </Select>
            </Form.Item>
            <Form.Item
              label="Port"
              name="port"
              rules={[
                {
                  validator: (rule, value = port, cb) => {
                    port.length === 0 ? cb(t("common.requiredField")) : cb();
                  },
                  required: true,
                },
                {
                  validator: (rule, value = port, cb) => {
                    port === "custom" && port_custom.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={port}
            >
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "space-between",
                }}
              >
                <Select
                  value={port}
                  onChange={(e) => changeFieldEmail("port", e)}
                  style={{ width: port === "custom" ? "49%" : "100%" }}
                >
                  <Select.Option value={"23"}>23</Select.Option>
                  <Select.Option value={"465"}>465</Select.Option>
                  <Select.Option value={"587"}>587</Select.Option>
                  <Select.Option value={"custom"}>Custom</Select.Option>
                </Select>
                {port === "custom" && (
                  <Input
                    style={{ width: "49%" }}
                    onChange={(e) =>
                      changeFieldEmail("port_custom", e.target.value)
                    }
                    value={port_custom}
                  />
                )}
              </div>
            </Form.Item>
            <Form.Item
              label={t("emailOutgoing.emailLinking")}
              name="user_name"
              rules={[
                {
                  validator: (rule, value = user_name, cb) => {
                    user_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={user_name}
            >
              <Input
                autocomplete="new-password"
                value={user_name}
                onChange={(e) => changeFieldEmail("user_name", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("user.pass")}
              name="pass_word"
              valuePropName={pass_word}
            >
              <Input.Password
                autocomplete="new-password"
                value={pass_word}
                onChange={(e) => changeFieldEmail("pass_word", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              name={"is_use_username"}
              valuePropName={is_use_username}
              label={t("emailOutgoing.username")}
            >
              <Checkbox
                checked={is_use_username}
                onChange={(e) => {
                  changeFieldEmail("is_use_username", e.target.checked);
                  if (!e.target.checked) {
                    changeFieldEmail("username_authentication", "");
                  }
                }}
              ></Checkbox>
            </Form.Item>
            {is_use_username ? (
              <Form.Item
                label={t("emailOutgoing.userAuth")}
                name={"username_authentication"}
                valuePropName={username_authentication}
                rules={[
                  {
                    validator: (rule, value = username_authentication, cb) => {
                      username_authentication.length === 0
                        ? cb(t("common.requiredField"))
                        : cb();
                    },
                    required: true,
                  },
                ]}
              >
                <Input
                  value={username_authentication}
                  onChange={(e) =>
                    changeFieldEmail("username_authentication", e.target.value)
                  }
                ></Input>
              </Form.Item>
            ) : (
              ""
            )}
            <Form.Item
              label={t("emailOutgoing.authen")}
              name="authen"
              valuePropName={authen}
            >
              <Checkbox
                checked={authen}
                onChange={(e) => changeFieldEmail("authen", e.target.checked)}
              />
            </Form.Item>
          </>
        )}

        {server_type === "google" && (
          <>
            {/* <Form.Item
                name={"credential_id"}
                label={t("emailOutgoing.credentialId")}
                className={"credential_id"}
                valuePropName={credential_id}
                rules={[
                  {
                    validator: (rule, value = credential_id, cb) => {
                      credential_id.length === 0
                        ? cb(t("common.requiredField"))
                        : cb();
                    },
                    required: true,
                  },
                ]}
              >
                <Select
                  value={credential_id}
                  onChange={(e) => {
                    changeFieldEmail("credential_id", e);
                  }}
                >
                  {listGoogleIntegrationSetting.map((item) => {
                    return <Option value={item._id}>{item.config_name}</Option>;
                  })}
                </Select>
              </Form.Item> */}
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => changeFieldEmail("object_id", e)}
                value={object_id}
                disabled={editEmailOutgoing._id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {
                  // eslint-disable-next-line
                  listObject &&
                    listObject.map((item) => {
                      if (item.Status) {
                        return (
                          <Select.Option value={item._id}>
                            {item.Name}
                          </Select.Option>
                        );
                      } else return "";
                    })
                }
              </Select>
            </Form.Item>

            <Form.Item
              label="Default Alias"
              name="default_alias"
              valuePropName={default_alias}
            >
              <Input
                value={default_alias}
                onChange={(e) =>
                  changeFieldEmail("default_alias", e.target.value)
                }
              />
            </Form.Item>

            <div style={{ backgroundColor: "#F2F4F5", padding: "1rem" }}>
              {listAccountMapping.map((item) => {
                return (
                  <div
                    style={{
                      padding: "1rem",
                      backgroundColor:
                        item.email === google_account_integration
                          ? "#dbd8d8"
                          : "white",
                      marginBottom: "0.5rem",
                      borderRadius: "5px",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      changeFieldEmail(
                        "google_account_integration",
                        item.email
                      );
                    }}
                  >
                    <img
                      src={item.avatar_url}
                      alt=""
                      style={{
                        width: "2rem",
                        borderRadius: "50%",
                        marginRight: "1rem",
                      }}
                    />
                    {item.email}
                  </div>
                );
              })}
            </div>
          </>
        )}

        {server_type === "outlook" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => changeFieldEmail("object_id", e)}
                value={object_id}
                disabled={editEmailOutgoing._id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {
                  // eslint-disable-next-line
                  listObject &&
                    listObject.map((item) => {
                      if (item.Status) {
                        return (
                          <Select.Option value={item._id}>
                            {item.Name}
                          </Select.Option>
                        );
                      } else return "";
                    })
                }
              </Select>
            </Form.Item>

            <Form.Item
              label="Default Alias"
              name="default_alias"
              valuePropName={default_alias}
            >
              <Input
                value={default_alias}
                onChange={(e) =>
                  changeFieldEmail("default_alias", e.target.value)
                }
              />
            </Form.Item>
            <div style={{ backgroundColor: "#F2F4F5", padding: "1rem" }}>
              {allAccount.map((item) => {
                return (
                  <div
                    style={{
                      padding: "1rem",
                      backgroundColor:
                        item.email === google_account_integration
                          ? "#dbd8d8"
                          : "white",
                      marginBottom: "0.5rem",
                      borderRadius: "5px",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      changeFieldEmail(
                        "google_account_integration",
                        item.email
                      );
                    }}
                  >
                    <img
                      src={AvatarImg}
                      alt=""
                      style={{
                        width: "2rem",
                        borderRadius: "50%",
                        marginRight: "1rem",
                      }}
                    />
                    {item.email}
                  </div>
                );
              })}
            </div>
          </>
        )}

        {server_type === "owa" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => changeFieldEmail("object_id", e)}
                value={object_id}
                disabled={editEmailOutgoing._id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listObject &&
                  listObject.map((item) => {
                    if (item.Status) {
                      return (
                        <Select.Option value={item._id}>
                          {item.Name}
                        </Select.Option>
                      );
                    } else {
                      return <></>;
                    }
                  })}
              </Select>
            </Form.Item>

            <Form.Item
              label={t("common.description")}
              name="description"
              valuePropName={description}
            >
              <Input.TextArea
                value={description}
                onChange={(e) =>
                  changeFieldEmail("description", e.target.value)
                }
              />
            </Form.Item>

            <Form.Item
              label="Default Alias"
              name="default_alias"
              valuePropName={default_alias}
            >
              <Input
                value={default_alias}
                onChange={(e) =>
                  changeFieldEmail("default_alias", e.target.value)
                }
              />
            </Form.Item>

            <Form.Item
              label="Server"
              name="server"
              rules={[
                {
                  validator: (rule, value = server, cb) => {
                    server.length === 0 ? cb(t("common.requiredField")) : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={server}
            >
              <Input
                value={server}
                onChange={(e) => changeFieldEmail("server", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("emailOutgoing.emailLinking")}
              name="user_name"
              rules={[
                {
                  validator: (rule, value = user_name, cb) => {
                    user_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={user_name}
            >
              <Input
                autocomplete="new-password"
                value={user_name}
                onChange={(e) => changeFieldEmail("user_name", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("user.pass")}
              name="pass_word"
              valuePropName={pass_word}
            >
              <Input.Password
                autocomplete="new-password"
                value={pass_word}
                onChange={(e) => changeFieldEmail("pass_word", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("emailOutgoing.userAuth")}
              name={"username_authentication"}
              valuePropName={username_authentication}
              rules={[
                {
                  validator: (rule, value = username_authentication, cb) => {
                    username_authentication.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
            >
              <Input
                value={username_authentication}
                onChange={(e) =>
                  changeFieldEmail("username_authentication", e.target.value)
                }
              ></Input>
            </Form.Item>
            <Form.Item
              label={t("emailOutgoing.authen")}
              name="authen"
              valuePropName={authen}
            >
              <Checkbox
                checked={authen}
                onChange={(e) => changeFieldEmail("authen", e.target.checked)}
              />
            </Form.Item>
          </>
        )}

        <Form.Item
          label={t("emailOutgoing.saveSentEmail")}
          name="is_save_sent_email"
          valuePropName={is_save_sent_email}
        >
          <Checkbox
            checked={is_save_sent_email}
            onChange={(e) =>
              changeFieldEmail("is_save_sent_email", e.target.checked)
            }
          />
        </Form.Item>
        {is_save_sent_email && (
          <Form.Item
            label={t("emailOutgoing.folderName")}
            name="folder_name"
            valuePropName={folder_name}
          >
            <Input
              value={folder_name}
              onChange={(e) => changeFieldEmail("folder_name", e.target.value)}
            ></Input>
          </Form.Item>
        )}

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
