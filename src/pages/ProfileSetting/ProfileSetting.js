import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "redux/store";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import {
  Row,
  Col,
  Form,
  Input,
  DatePicker,
  Modal,
  Slider,
  Button,
  Upload,
  Tabs,
  Tooltip,
  Typography,
  Select,
  Checkbox,
} from "antd";

import {
  setShowModalEdit,
  unmountProfileSetting,
  updateUserSetting,
} from "redux/slices/user";
import { BASE_URL_API, BE_URL } from "../../constants/constants";
import moment from "moment";
import { PlusOutlined } from "@ant-design/icons";
import AvatarEditor from "react-avatar-editor";
import { setShowLoadingScreen } from "redux/slices/global";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import avatarImg from "../../assets/images/header/avatar.png";
import { updateFieldUserDetail } from "redux/slices/profileSetting";
import { Notification } from "components/Notification/Noti";
import CameraImg from "assets/icons/common/camera.png";
import EmptySignature from "assets/images/profile/Empty_Signature.png";
import ModalSignature from "./ModalSignature";
import Editor from "components/Editor/Editor2";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import { setShowModalConfirmDelete } from "redux/slices/global";

export const jwt = require("jsonwebtoken");

const { TabPane } = Tabs;
const { Text } = Typography;

const ProfileSetting = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const editorJodit = useRef(null);

  const { showModalEdit } = useSelector((state) => state.userReducer);
  const { userDetail } = useSelector((state) => state.profileSettingReducer);
  const {
    Last_Name,
    Middle_Name,
    First_Name,
    avatar_config,
    birthday,
    phone_number,
    address,
  } = userDetail;

  const [postionAvatar, setPositionLogo] = useState({
    scale: 1,
    position: {
      x: 0,
      y: 0,
    },
    url: "",
  });

  const [dataDelete, setDataDelete] = useState({});
  const [showModalSignature, $showModalSignature] = useState(false);
  const [initName, $initName] = useState("");
  const [emailSignatures, $emailSignatures] = useState([]);
  const [selectedItem, $selectedItem] = useState(0);
  const [content, $content] = useState("");
  const [isBeforeQuote, $isBeforeQuote] = useState(false);

  const openModal = () => {
    dispatch(setShowModalEdit(true));
  };

  const hideModal = () => {
    dispatch(setShowModalEdit(false));
    if (avatar_config) {
      setPositionLogo(avatar_config);
    }
  };

  const changePositionAvatar = (key, value) => {
    let temp = { ...postionAvatar };
    temp[key] = value;
    setPositionLogo(temp);
  };

  const saveAvatarSetting = () => {
    dispatch(setShowModalEdit(false));
    dispatch(
      updateFieldUserDetail({
        key: "avatar_config",
        value: postionAvatar,
      })
    );
  };

  const uploadAttachFile = ({ data }) => {
    let temp = { ...postionAvatar };
    temp.url = data[0];
    setPositionLogo(temp);
    dispatch(setShowLoadingScreen(false));
  };

  const _onSubmit = () => {
    _onBlurEditor("", "", true);
  };

  const _onSelectSignature = (idx, value) => {
    return () => {
      $selectedItem(idx);
      $content(value);
      editorJodit.current.value = value;
    };
  };

  const _onConfirmDeleteSignature = (idx) => {
    return (e) => {
      e.stopPropagation();
      setDataDelete(idx);
      dispatch(setShowModalConfirmDelete(true));
    };
  };

  const _onDeleteSignature = () => {
    let temp = [...emailSignatures];
    temp.splice(dataDelete, 1);
    $emailSignatures(temp);

    if (temp.length === 0) {
      dispatch(
        updateUserSetting({
          ...userDetail,
          email_signatures: [],
        })
      );
    }

    $selectedItem(0);
    $content(_.get(temp, "[0].content", ""));
    editorJodit.current.value = _.get(temp, "[0].content", "");
  };

  const _onBlurEditor = (value, event, isSubmit) => {
    let temp = [...emailSignatures];

    if (temp.length > 0) {
      temp[selectedItem] = {
        ...temp[selectedItem],
        content: editorJodit.current.value,
      };
      $emailSignatures(temp);
    }

    if (isSubmit) {
      const result = temp.filter(
        (item) => item.content !== "<p><br></p>" && item.content !== ""
      );

      dispatch(
        updateUserSetting({
          ...userDetail,
          email_signatures: result,
          show_signature_before_quote: isBeforeQuote,
        })
      );
    }
  };

  const _onAddSignature = () => {
    $showModalSignature(true);
  };

  const _onHideSignature = () => {
    $showModalSignature(false);
    $initName("");
  };

  const _onSubmitSignature = (value) => {
    let temp = [...emailSignatures];

    if (initName) {
      const index = emailSignatures.findIndex((item) => item.name === initName);
      temp[index] = {
        ...temp[index],
        name: value,
      };
    } else {
      temp.push({
        name: value,
        content: "",
        is_default: false,
      });

      $selectedItem(temp.length - 1);
      $content("");

      if (editorJodit?.current?.value) {
        editorJodit.current.value = "";
      }
    }
    $emailSignatures(temp);
    _onHideSignature();
  };

  const _onEditSignature = (value) => {
    return (e) => {
      e.stopPropagation();
      $initName(value);
      $showModalSignature(true);
    };
  };

  const handleDefaultSignature = (e, option) => {
    let temp = [...emailSignatures];

    if (e) {
      temp.forEach((item, index) => {
        if (index === parseInt(option.key)) {
          temp[index] = {
            ...temp[index],
            is_default: true,
          };
        } else {
          temp[index] = {
            ...temp[index],
            is_default: false,
          };
        }
      });
    } else {
      temp.forEach((item, index) => {
        temp[index] = {
          ...temp[index],
          is_default: false,
        };
      });
    }

    $emailSignatures(temp);
  };

  const onChangeBeforeQuote = (value) => {
    $isBeforeQuote(value.target.checked);
  };

  useEffect(() => {
    if (avatar_config) {
      setPositionLogo(avatar_config);
    }
  }, [avatar_config]);

  useEffect(() => {
    $emailSignatures(_.get(userDetail, "email_signatures", []));
    $isBeforeQuote(_.get(userDetail, "show_signature_before_quote", false));

    if (_.get(userDetail, "email_signatures", []).length > 0) {
      $content(_.get(userDetail, "email_signatures[0].content", ""));
      $selectedItem(0);
    }
  }, [userDetail]);

  useEffect(
    () => () => {
      dispatch(unmountProfileSetting());
    },
    // eslint-disable-next-line
    []
  );

  return (
    <Wrapper>
      <CustomContent>
        <ContentWrap>
          <div className="left-item">
            <div
              style={{
                height: "100%",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
              }}
            >
              {avatar_config && (
                <div
                  style={{
                    padding: "8px",
                    border: "1px solid #E5E5E5",
                    borderRadius: "50%",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {" "}
                  <AvatarEditor
                    image={
                      avatar_config.url && avatar_config.url.length > 0
                        ? BE_URL + avatar_config.url
                        : avatarImg
                    }
                    position={
                      avatar_config
                        ? avatar_config.position
                        : {
                            x: 0,
                            y: 0,
                          }
                    }
                    scale={avatar_config ? avatar_config.scale : 1}
                    border={0}
                  />
                </div>
              )}{" "}
              <button
                className="btn-change-avatar"
                onClick={() => {
                  openModal();
                }}
              >
                <img alt="" src={CameraImg} />
                Change avatar
              </button>
              <span className="description">
                Allowed *.jpeg, *.jpg, *.png max size of 50MB
              </span>
            </div>
          </div>

          <Right>
            <Tabs>
              <TabPane tab="Information" key="information">
                <Form
                  labelCol={{ span: 24 }}
                  wrapperCol={{ span: 24 }}
                  onFinish={_onSubmit}
                  layout="vertical"
                >
                  <Form.Item
                    name={"Last_Name"}
                    label={"Họ"}
                    valuePropName={Last_Name}
                    rules={[
                      {
                        validator: (rule, value = Last_Name, cb) => {
                          Last_Name.length === 0
                            ? cb(t("common.placeholderInput"))
                            : cb();
                        },
                        required: true,
                      },
                    ]}
                  >
                    <Input
                      value={Last_Name}
                      onChange={(e) => {
                        dispatch(
                          updateFieldUserDetail({
                            key: "Last_Name",
                            value: e.target.value,
                          })
                        );
                      }}
                      style={{ width: "100%" }}
                    ></Input>
                  </Form.Item>

                  <Form.Item
                    name={"Middle_Name"}
                    label={"Tên đệm"}
                    valuePropName={Middle_Name}
                  >
                    <Input
                      value={Middle_Name}
                      onChange={(e) => {
                        dispatch(
                          updateFieldUserDetail({
                            key: "Middle_Name",
                            value: e.target.value,
                          })
                        );
                      }}
                      style={{ width: "100%" }}
                    ></Input>
                  </Form.Item>

                  <Form.Item
                    name={"First_Name"}
                    label={"Tên"}
                    valuePropName={First_Name}
                    rules={[
                      {
                        validator: (rule, value = First_Name, cb) => {
                          First_Name.length === 0
                            ? cb(t("common.placeholderInput"))
                            : cb();
                        },
                        required: true,
                      },
                    ]}
                  >
                    <Input
                      style={{ width: "100%" }}
                      value={First_Name}
                      onChange={(e) => {
                        dispatch(
                          updateFieldUserDetail({
                            key: "First_Name",
                            value: e.target.value,
                          })
                        );
                      }}
                    ></Input>
                  </Form.Item>

                  <Row gutter={24}>
                    <Col span={12}>
                      <Form.Item
                        name={"birthday"}
                        label={"Ngày sinh"}
                        valuePropName={birthday}
                      >
                        <DatePicker
                          style={{ width: "100%", marginRight: "16px" }}
                          value={birthday ? moment(birthday) : ""}
                          onChange={(e, dateString) => {
                            dispatch(
                              updateFieldUserDetail({
                                key: "birthday",
                                value: dateString,
                              })
                            );
                          }}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={12}>
                      <Form.Item
                        name={"phone_number"}
                        label={"Số điện thoại"}
                        valuePropName={phone_number}
                      >
                        <Input
                          style={{ width: "100%" }}
                          min={0}
                          type={"number"}
                          value={phone_number}
                          onChange={(e) => {
                            dispatch(
                              updateFieldUserDetail({
                                key: "phone_number",
                                value: e.target.value,
                              })
                            );
                          }}
                        ></Input>
                      </Form.Item>
                    </Col>
                  </Row>

                  <Form.Item
                    name={"address"}
                    label={"Địa chỉ"}
                    valuePropName={address}
                  >
                    <Input
                      style={{ width: "100%" }}
                      value={address}
                      onChange={(e) => {
                        dispatch(
                          updateFieldUserDetail({
                            key: "address",
                            value: e.target.value,
                          })
                        );
                      }}
                    ></Input>
                  </Form.Item>
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "flex-end",
                    }}
                  >
                    <Button type="primary" htmlType="submit">
                      {t("common.save")}
                    </Button>
                  </div>
                </Form>
              </TabPane>
              <TabPane tab="Signature" key="signature">
                <WrapSignature>
                  {emailSignatures.length === 0 ? (
                    <Empty>
                      <img src={EmptySignature} alt="empty" />
                      <p>
                        No
                        <span> Signature</span>
                      </p>

                      <AddButton onClick={_onAddSignature}>Add new</AddButton>
                    </Empty>
                  ) : (
                    <>
                      <ListSignatures>
                        <LeftSignature>
                          <AddButton
                            style={{
                              width: "100%",
                              height: "40px",
                            }}
                            onClick={_onAddSignature}
                          >
                            + Add new
                          </AddButton>

                          {_.map(emailSignatures, (item, idx) => (
                            <Signature
                              onClick={_onSelectSignature(
                                idx,
                                _.get(item, "content", "")
                              )}
                              key={_.get(item, "id", idx)}
                              active={selectedItem === idx}
                            >
                              <Text
                                ellipsis={{
                                  tooltip: _.get(item, "name", ""),
                                }}
                                style={{ width: "60%" }}
                              >
                                <span>{_.get(item, "name", "")}</span>
                              </Text>

                              <WrapAction>
                                <Tooltip title={t("common.edit")}>
                                  <img
                                    onClick={_onEditSignature(
                                      _.get(item, "name", "")
                                    )}
                                    src={Edit}
                                    alt="edit"
                                  />
                                </Tooltip>
                                <Tooltip title={t("common.delete")}>
                                  <img
                                    onClick={_onConfirmDeleteSignature(idx)}
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              </WrapAction>
                            </Signature>
                          ))}
                        </LeftSignature>

                        <RightSignature>
                          <Editor
                            objectID="signature"
                            editorJodit={editorJodit}
                            minHeight="400px"
                            showAppend={false}
                            onBlur={_onBlurEditor}
                            content={content}
                          />
                        </RightSignature>
                      </ListSignatures>

                      <DefaultSignature>
                        <p>{t("profile.defaultSignature")}</p>
                        <Select
                          allowClear
                          placeholder={t("common.placeholderSelect")}
                          value={
                            emailSignatures.find((item) => item.is_default)
                              ?.content
                          }
                          onChange={handleDefaultSignature}
                        >
                          {_.map(emailSignatures, (item, idx) => (
                            <Select.Option
                              value={_.get(item, "content", "")}
                              key={idx}
                            >
                              {_.get(item, "name", "")}
                            </Select.Option>
                          ))}
                        </Select>
                      </DefaultSignature>

                      <Checkbox
                        onChange={onChangeBeforeQuote}
                        checked={isBeforeQuote}
                      >
                        {t("profile.alignSignature")}
                      </Checkbox>

                      <div
                        style={{
                          width: "100%",
                          display: "flex",
                          justifyContent: "flex-end",
                          marginTop: "8px",
                        }}
                      >
                        <Button
                          onClick={_onSubmit}
                          type="primary"
                          htmlType="submit"
                        >
                          {t("common.save")}
                        </Button>
                      </div>
                    </>
                  )}
                </WrapSignature>
              </TabPane>
            </Tabs>
          </Right>
        </ContentWrap>

        <CustomModal
          visible={showModalEdit}
          // onOk={() => saveAvatarSetting()}
          // onCancel={() => hideModal()}
          footer={null}
        >
          <Row>
            <Col span={24} style={{ textAlign: "center" }}>
              {avatar_config && (
                <AvatarEditor
                  image={postionAvatar.url ? BE_URL + postionAvatar.url : ""}
                  position={postionAvatar.position}
                  scale={postionAvatar.scale}
                  onPositionChange={(e) => changePositionAvatar("position", e)}
                  border={0}
                />
              )}
            </Col>
          </Row>
          <Row justify={"center"}>
            <Col span={10}>
              <Slider
                value={postionAvatar.scale}
                onChange={(e) => changePositionAvatar("scale", e)}
                min={1}
                max={2}
                step={0.1}
              />
            </Col>
          </Row>
          <Row justify={"center"}>
            <Upload
              action={BASE_URL_API + "upload-file"}
              method={"post"}
              showUploadList={false}
              headers={{
                Authorization: localStorage.getItem("setting_accessToken"),
              }}
              accept="image/png, image/jpeg, image/jpg"
              data={{
                obj: "avatar",
              }}
              beforeUpload={(file) => {
                const isPNG =
                  file.type === "image/png" ||
                  file.type === "image/jpeg" ||
                  file.type === "image/jpg";
                if (!isPNG) {
                  Notification("error", "Invalid file type!");
                }
                const isLt5M = file.size / 1024 / 1024 < 5;
                if (!isLt5M) {
                  Notification("error", "File must smaller than 5MB!");
                }
                return (isPNG && isLt5M) || Upload.LIST_IGNORE;
              }}
              onChange={(info) => {
                if (info.file.status === "uploading") {
                  dispatch(setShowLoadingScreen(true));
                }
                if (info.file.status === "done") {
                  uploadAttachFile(info.file.response);
                } else if (info.file.status === "error") {
                  dispatch(setShowLoadingScreen(false));
                }
              }}
              progress={{
                strokeColor: {
                  "0%": "#108ee9",
                  "100%": "#87d068",
                },
                strokeWidth: 3,
                format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
              }}
            >
              <Button className="add-logo" icon={<PlusOutlined />}></Button>
            </Upload>
          </Row>

          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              onClick={() => saveAvatarSetting()}
            >
              Save
            </CustomButtonSave>
            <CustomButtonCancel size="large" onClick={() => hideModal()}>
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        </CustomModal>

        <ModalSignature
          showModalSignature={showModalSignature}
          initName={initName}
          _onHideSignature={_onHideSignature}
          _onSubmitSignature={_onSubmitSignature}
        />

        <ModalConfirmDelete
          title={""}
          decs={t("common.descriptionDelete")}
          dataDelete={dataDelete}
          isLoading={false}
          onDelete={_onDeleteSignature}
        />
      </CustomContent>
    </Wrapper>
  );
};

export default withTranslation()(ProfileSetting);

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
  display: flex;
  justify-content: center;
  background-color: white;

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-top: 24px;
`;

const CustomContent = styled(Row)`
  .gutter-box {
    background: #00a0e9;
  }
  display: flex !important;
  align-items: center !important;
  justify-content: center;
  .ant-col-7 {
    height: calc(100% - 25%);
  }
  .ant-col-17 {
    height: calc(100% - 25%);
  }
  .left-item {
    background-color: white;
    display: flex;
    justify-content: center;
    height: 676px;

    canvas {
      width: 180px !important;
      height: 180px !important;
    }
    box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.15);
    border-radius: 10px;
  }

  height: calc(100vh - 10%);
  padding: 32px;
  border-radius: 5px;
  width: 95%;

  .row-field {
    width: 100%;
  }
  .ant-form-item {
    width: 100%;
  }
  canvas {
    border-radius: 50% !important;
    border: 1px solid #ececec;
    /* width: auto !important;
    height: 35vh !important; */
    width: 268px !important;
    height: auto !important;
  }
  .btn-change-avatar {
    background: rgba(185, 185, 185, 0.2);
    border-radius: 8px;
    display: flex;
    flex-direction: row;
    align-items: center;
    padding: 8px 12px;
    gap: 6px;
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */
    border: none;
    margin-top: 24px;
    color: #7c7c7c;
    :hover {
      cursor: pointer;
    }
    img {
      width: 22px;
    }
  }
  .description {
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 16px;
    text-align: center;

    /* 888 */
    margin-top: 47px;
    color: #888888;
    width: 60%;
    line-height: 20px;
  }
  .button-wrap {
    /* margin-top: -80px;
    display: flex;
    justify-content: space-between; */
    position: absolute;
    top: 0;
    left: 0;
  }
  .upload-avatar {
    border: 1px solid #ececec;
    /* width: 2rem;
    height: 2rem;
    padding: 0.2rem; */
    cursor: pointer;
    width: 50px;
    height: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 50%;
    background-color: #fafafa;
  }
  .ant-form-horizontal .ant-form-item-label {
    text-align: left;
  }
  .save-btn {
    button {
      float: right;
      padding: 0 40px;
    }
  }
`;

const Right = styled.div`
  margin-left: 24px;
  background: #ffffff;
  box-shadow: 2px 2px 12px rgba(0, 0, 0, 0.15);
  border-radius: 10px;
  padding: 16px;
  height: 676px;
  width: 800px;

  .ant-form-item-label {
    font-family: var(--roboto-700) !important;
    font-size: 16px !important;
  }

  .ant-form {
    margin-top: 20px;
  }

  .ant-tabs-nav {
    margin-bottom: 0;
  }

  .ant-tabs-nav-wrap {
    background: #fff;
    border-radius: 10px 10px 0 0;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(props) => props.theme.main} !important;
  }

  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }

  .ant-tabs-tab-btn:hover {
    color: ${(props) => props.theme.darker};
    background: #e6f7ff;
  }

  .ant-btn-primary {
    width: 178px;
    height: 48px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;
      border: 1px solid ${(props) => props.theme.darker}!important;
    }
  }

  .ant-btn:active {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn:focus {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    width: 45vw;
  }
  canvas {
    border-radius: 50% !important;
    width: auto !important;
    height: 35vh !important;
  }
`;

const ContentWrap = styled.div`
  display: flex;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapSignature = styled.div`
  margin-top: 20px;
`;

const ListSignatures = styled.div`
  display: flex;
`;

const LeftSignature = styled.div`
  width: 30%;
  max-height: 400px;
  overflow: auto;
  padding-right: 8px;
`;

const RightSignature = styled.div`
  width: 70%;
  margin-left: 8px;
  max-height: 400px;
  overflow: auto;
`;

const Signature = styled.div`
  padding: 16px;
  height: 58px;
  background: ${({ active }) => (active ? "#f5f5f5" : "#fff")};
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-top: 8px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  :hover {
    background: #f5f5f5;
    cursor: pointer;
  }

  img {
    margin-left: 8px;
  }

  span {
    font-size: 16px;
    color: #2c2c2c;
    line-height: 26px;
    font-family: var(--roboto-500);
  }
`;

const WrapAction = styled.div``;

const DefaultSignature = styled.div`
  margin-top: 16px;
  margin-bottom: 8px;

  p {
    margin-bottom: 8px;
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
    font-family: var(--roboto-500);
  }

  .ant-select {
    width: 100%;
  }
`;
