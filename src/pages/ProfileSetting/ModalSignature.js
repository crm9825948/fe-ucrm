import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import { useEffect } from "react";

function ModalSignature({
  showModalSignature,
  initName,
  _onHideSignature,
  _onSubmitSignature,
}) {
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const _onSubmit = (values) => {
    _onSubmitSignature(values.name);
    form.resetFields();
  };

  const _onCancel = () => {
    form.resetFields();
    _onHideSignature();
  };

  useEffect(() => {
    form.setFieldsValue({
      name: initName,
    });
  }, [initName, form]);

  return (
    <ModalCustom
      title={
        initName
          ? t("profile.editMailSignature")
          : t("profile.addMailSignature")
      }
      visible={showModalSignature}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("profile.nameSignature")}
          name="name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input
            placeholder={t("common.placeholderInput")}
            maxLength={200}
            autocomplete="new-password"
          />
        </Form.Item>
        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSignature);

ModalSignature.defaultProps = {
  showModalSignature: false,
  initName: "",
  _onHideSignature: () => {},
  _onSubmitSignature: () => {},
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
