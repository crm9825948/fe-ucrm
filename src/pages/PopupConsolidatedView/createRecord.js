import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import moment from "moment";
import _ from "lodash";
import styled from "styled-components";
import { Notification } from "components/Notification/Noti";

import Form from "antd/lib/form";
import Table from "antd/lib/table";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";
import Radio from "antd/lib/radio";

import ModalRecord from "pages/Objects/modal/modalRecord";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import Button from "components/Button";

import {
  setShowModal,
  loadListObjectField,
  globalSearch,
  viewObjectSearch,
} from "redux/slices/objects";
import { loadCallCenterSetting } from "redux/slices/consolidatedViewSettings";

import {
  createRecordCallCenter,
  createRecordCallCenterSuccess,
  updateRecordCallCenter,
  updateRecordCallCenterResult,
  getFieldsMappingCallCenter,
  updateInteraction,
  searchCoreCallCenter,
  createRecordCoreCallCenter,
} from "redux/slices/callCenter";
import { callEvent } from "redux/slices/voiceBiometric";

import { BASE_URL_API, BE_URL, FE_URL } from "constants/constants";

function CreateRecord(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();

  const { phoneNumber } = useParams();

  const { userDetail } = useSelector((state) => state.userReducer);

  const { listObjectField, listDuplicate, data, objectGlobalSearch } =
    useSelector((state) => state.objectsReducer);

  const { statusCreateRecord, fieldsMappingCallCenter, dataSearchCore } =
    useSelector((state) => state.callCenterReducer);
  const { configCallCenter } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [recordID, setRecordID] = useState("");
  const [formValues, $formValues] = useState({});
  const [column, $column] = useState([]);
  const [dataSource, $dataSource] = useState([]);
  const [isSearch, $isSearch] = useState(false);
  const [notiGlobal, setNotiGlobal] = useState("");
  const [isSearchCore, $isSearchCore] = useState(false);
  const { allObject } = useSelector((state) => state.tenantsReducer);

  useEffect(() => {
    if (_.get(userDetail, "search_type", "UCRM") === "UCRM") {
      $isSearchCore(false);
    } else {
      $isSearchCore(true);
    }
  }, [userDetail]);

  const [columnCore, $columnCore] = useState([
    {
      key: "Core",
      title: "Core",
      dataIndex: "Core",
    },
  ]);
  const [dataSourceCore, $dataSourceCore] = useState([]);
  const [userFilter, $userFilter] = useState({});

  const [, setEditingKey] = useState("");
  const [dataCall] = useState(localStorage.getItem("dataCall"));

  const normalizeLink = (value) => {
    if (
      allObject?.find(
        (object) =>
          object?.object_id === JSON.parse(dataCall).object_id && object?.status
      )
    ) {
      if (value.split(BE_URL)?.length > 1) {
        return value;
      } else {
        return BE_URL + value?.split(FE_URL)[1];
      }
    } else {
      return value;
    }
  };

  const _onSubmit = (values, isDuplicated, isOverWrite, recordOverWrite) => {
    $formValues(values);

    let fieldsObject = {};

    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
      ].forEach((section, idx) => {
        section.fields.forEach((item, index) => {
          fieldsObject[item.ID] = { ...item };
        });
      });
    let listValue = [];
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              if (item.url === undefined) {
                item.url =
                  BASE_URL_API + item &&
                  item.response &&
                  item.response.data &&
                  item.response.data[0];
                item.url = BE_URL + item.url;
              }
              result.push(normalizeLink(item.url));
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(normalizeLink(item));
                return null;
              });
            } else {
              // eslint-disable-next-line
              value.map((item, idx) => {
                if (item.url) {
                  result.push(normalizeLink(item.url));
                } else {
                  result.push(normalizeLink(item));
                }
              });
            }
          }
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    if (isOverWrite) {
      dispatch(
        updateRecordCallCenter({
          data: {
            data: listDuplicate[listDuplicate.length - 1]?.record_data,
            owner_id: listDuplicate[listDuplicate.length - 1]?.owner,
            id: recordOverWrite,
            object_id: JSON.parse(dataCall).object_id,
            ignore_duplication: true,
          },
          load: {
            object_id: JSON.parse(dataCall).object_id,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
          dataCall: {
            call_id: JSON.parse(dataCall).third_party_id,
            record_id: recordOverWrite,
            object_id: JSON.parse(dataCall).object_id,
            extension: JSON.parse(dataCall).extension,
          },
        })
      );
    } else {
      dispatch(
        createRecordCallCenter({
          data: {
            object_id: JSON.parse(dataCall).object_id,
            data: isDuplicated
              ? listDuplicate[listDuplicate.length - 1]?.record_data
              : listValue,
            owner: isDuplicated
              ? listDuplicate[listDuplicate.length - 1]?.owner
              : values["assignTo"],
            ignore_duplication: isDuplicated ? true : undefined,
          },
          load: {
            object_id: JSON.parse(dataCall).object_id,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
          dataCall: { ...JSON.parse(dataCall) },
        })
      );
    }
    setRecordID("");
    setEditingKey("");
  };

  const handleGlobalSearch = (value) => {
    if (isSearchCore) {
      dispatch(
        searchCoreCallCenter({
          setting_id: _.get(configCallCenter, "_id"),
          user_filter: userFilter,
        })
      );
    } else {
      if (value.trim().length > 100) {
        Notification("warning", "Chỉ tìm kiếm tối đa 100 ký tự!");
      } else {
        $isSearch(true);
        if (value.trim()) {
          dispatch(
            globalSearch({
              object_id: JSON.parse(dataCall).object_id,
              search_text: value.trim(),
            })
          );
        }
      }
    }
  };

  const onChangeFilter = (key, val) => {
    $userFilter({
      ...userFilter,
      [key]: val,
    });
  };

  const _onCreateRecordCore = (index) => {
    return () => {
      if (index === null) {
        dispatch(
          createRecordCoreCallCenter({
            data: {
              setting_id: _.get(configCallCenter, "_id", ""),
              core_data: {
                ...dataSearchCore,
              },
            },
            dataCall: { ...JSON.parse(dataCall) },
            object_id: _.get(
              configCallCenter,
              "search_core_config.object_create_record",
              ""
            ),
          })
        );
      } else {
        dispatch(
          createRecordCoreCallCenter({
            data: {
              setting_id: _.get(configCallCenter, "_id", ""),
              core_data: {
                ...dataSearchCore[index],
              },
            },
            dataCall: { ...JSON.parse(dataCall) },
            object_id: _.get(
              configCallCenter,
              "search_core_config.object_create_record",
              ""
            ),
          })
        );
      }
    };
  };

  const _onOpenModalRecord = () => {
    dispatch(setShowModal(true));

    if (
      Object.keys(fieldsMappingCallCenter).length > 0 &&
      fieldsMappingCallCenter.phone_fields.length > 0
    ) {
      fieldsMappingCallCenter.phone_fields.forEach((item, idx) => {
        if (idx === 0 && phoneNumber === JSON.parse(dataCall).phone) {
          form.setFieldsValue({
            [item]: JSON.parse(dataCall).phone,
          });
        }
      });
    }
  };

  const _onSelectRecord = (record) => {
    dispatch(
      updateInteraction({
        call_id: JSON.parse(dataCall).third_party_id,
        record_id: record.key,
        object_id: JSON.parse(dataCall).object_id,
        extension: JSON.parse(dataCall).extension,
      })
    );

    if (userDetail?.use_voice_biometric) {
      dispatch(
        callEvent({
          voice_biometric_event: "check_customer_registered_offcall",
          phone_incoming: JSON.parse(dataCall).phone,
          third_party_id: JSON.parse(dataCall).third_party_id,
          record_id: record.key,
          object_id: JSON.parse(dataCall).object_id,
          agent_name: JSON.parse(dataCall).username,
        })
      );
    }

    navigate(
      `/consolidated-view/${JSON.parse(dataCall).object_id}/${record.key}`
    );
  };

  useEffect(() => {
    if (
      !_.isEmpty(userDetail) &&
      _.get(userDetail, "popup_call", "default") !== "search" &&
      JSON.parse(dataCall).data?.length === 0
    ) {
      dispatch(setShowModal(true));
    }
  }, [dispatch, userDetail, dataCall]);

  useEffect(() => {
    if (dataCall) {
      dispatch(
        getFieldsMappingCallCenter({
          object_id: JSON.parse(dataCall).object_id,
        })
      );
    }
  }, [dataCall, dispatch]);

  useEffect(() => {
    if (
      Object.keys(fieldsMappingCallCenter).length > 0 &&
      fieldsMappingCallCenter.phone_fields.length > 0
    ) {
      fieldsMappingCallCenter.phone_fields.forEach((item, idx) => {
        if (idx === 0 && phoneNumber === JSON.parse(dataCall).phone) {
          form.setFieldsValue({
            [item]: JSON.parse(dataCall).phone,
          });
        }
      });
    }
  }, [dataCall, fieldsMappingCallCenter, form, phoneNumber]);

  useEffect(() => {
    if (statusCreateRecord !== null) {
      navigate(
        `/consolidated-view/${statusCreateRecord.object_id}/${statusCreateRecord.record_id}`
      );
    }
  }, [navigate, statusCreateRecord]);

  useEffect(() => {
    return () => {
      dispatch(createRecordCallCenterSuccess(null));
      dispatch(updateRecordCallCenterResult(null));
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: JSON.parse(dataCall).object_id,
        show_meta_fields: true,
      })
    );
    dispatch(viewObjectSearch({ object_id: JSON.parse(dataCall).object_id }));
    dispatch(
      loadCallCenterSetting({
        object_id: JSON.parse(dataCall).object_id,
      })
    );
  }, [dataCall, dispatch]);

  useEffect(() => {
    let tempColumn = [];
    JSON.parse(dataCall)?.field?.map((item) => {
      return tempColumn.push({
        key: item.ID,
        title: item.name === "Owner" ? "Assign to" : item.name,
        dataIndex: item.ID,
      });
    });

    $column(tempColumn);
  }, [dataCall]);

  useEffect(() => {
    let tempDataSource = [];
    let tempData = [];

    if (!isSearch) {
      tempData = [...JSON.parse(dataCall).data];
    } else {
      tempData = [...data];
    }

    tempData?.forEach((item) => {
      let newItem = {};
      newItem["key"] = item._id;
      Object.entries(item).forEach(([key, value]) => {
        if (typeof value === "object" && value !== null) {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      tempDataSource.push(newItem);
    });
    $dataSource(tempDataSource);
  }, [data, dataCall, isSearch]);

  useEffect(() => {
    if (Object.entries(objectGlobalSearch).length > 0) {
      if (objectGlobalSearch.searchable_fields.length > 0) {
        let noti = "Only ";
        objectGlobalSearch.searchable_fields.forEach((field, index) => {
          noti += `${
            index === objectGlobalSearch.searchable_fields.length - 1
              ? field.name
              : `${field.name},`
          } `;
        });
        noti += `${
          objectGlobalSearch.searchable_fields.length > 2 ? "are" : "is"
        } searchable.`;
        setNotiGlobal(noti);
      } else {
        setNotiGlobal(
          "Global search is not available. Please contact the administrator to index data and enable searching."
        );
      }
    }
  }, [objectGlobalSearch]);

  useEffect(() => {
    if (!_.isEmpty(dataSearchCore)) {
      let tempColumn = [];
      let tempDataSource = [];

      if (
        typeof dataSearchCore === "object" &&
        !Array.isArray(dataSearchCore)
      ) {
        Object.entries(dataSearchCore).forEach(([key, val]) => {
          tempColumn.push({
            key: key,
            title: key,
            dataIndex: key,
          });
        });

        let newItem = {};
        Object.entries(dataSearchCore).forEach(([key, val]) => {
          newItem[key] = val;
        });
        tempDataSource.push({
          ...newItem,
          action: (
            <Button
              type="primary"
              title="Create"
              onClick={_onCreateRecordCore(null)}
            />
          ),
        });
        $dataSourceCore(tempDataSource);
      } else {
        Object.entries(dataSearchCore[0]).forEach(([key, val]) => {
          tempColumn.push({
            key: key,
            title: key,
            dataIndex: key,
          });
        });

        dataSearchCore?.forEach((item, idx) => {
          let newItem = {};
          Object.entries(item).forEach(([key, value]) => {
            newItem[key] = value;
          });
          tempDataSource.push({
            ...newItem,
            action: (
              <Button
                type="primary"
                title="Create"
                onClick={_onCreateRecordCore(idx)}
              />
            ),
          });
        });

        $dataSourceCore(tempDataSource);
      }
      $columnCore([
        ...tempColumn,
        {
          key: "action",
          title: "Action",
          dataIndex: "action",
        },
      ]);
    } else {
      $columnCore([
        {
          key: "Core",
          title: "Core",
          dataIndex: "Core",
        },
      ]);
      $dataSourceCore([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataSearchCore]);

  return (
    <Wrapper>
      {_.get(userDetail, "popup_call", "default") === "search" ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
          }}
        >
          <>
            {_.get(objectGlobalSearch, "allow_global_search", false) && (
              <>
                <div
                  style={{ width: "150px", display: "flex", columnGap: "8px" }}
                >
                  <Radio.Group
                    options={[
                      {
                        label: "UCRM",
                        value: "UCRM",
                      },
                      {
                        label: "Core",
                        value: "Core",
                      },
                    ]}
                    defaultValue={
                      _.get(userDetail, "search_type", "UCRM") === "UCRM"
                        ? "UCRM"
                        : "Core"
                    }
                    onChange={(e) =>
                      $isSearchCore(e.target.value === "Core" ? true : false)
                    }
                    optionType="button"
                  />
                </div>
                <WrapSearch>
                  <div
                    style={{
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                      columnGap: "16px",
                      rowGap: "16px",
                    }}
                  >
                    {isSearchCore ? (
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          columnGap: "16px",
                          rowGap: "16px",
                          flexWrap: "wrap",
                          justifyContent: "center",
                        }}
                      >
                        {_.get(
                          configCallCenter,
                          "search_core_config.user_filter",
                          ""
                        )
                          .split(",")
                          .map((item, idx) => (
                            <CustomInputSearch
                              key={idx}
                              onSearch={handleGlobalSearch}
                              placeholder={item}
                              onChange={(e) =>
                                onChangeFilter(item, e.target.value)
                              }
                              core={true}
                            />
                          ))}
                      </div>
                    ) : (
                      <Tooltip
                        overlayInnerStyle={{
                          backgroundColor: notiGlobal.includes("not available")
                            ? "#d4b106"
                            : "rgba(0, 0, 0, 0.75)",
                        }}
                        trigger="hover"
                        placement="top"
                        title={notiGlobal}
                      >
                        <CustomInputSearch
                          allowClear
                          onSearch={handleGlobalSearch}
                          placeholder="Search..."
                        />
                      </Tooltip>
                    )}
                  </div>
                </WrapSearch>
                <Wrap>
                  <Table
                    dataSource={isSearchCore ? dataSourceCore : dataSource}
                    columns={isSearchCore ? columnCore : column}
                    pagination={false}
                    scroll={{
                      x: "max-content",
                    }}
                    onRow={(record) => {
                      return {
                        onDoubleClick: () => {
                          if (isSearchCore) return;
                          _onSelectRecord(record);
                        },
                      };
                    }}
                  />
                </Wrap>
              </>
            )}
          </>

          <Button
            style={{
              margin: "0 auto",
            }}
            title="+ Add record"
            type="primary"
            onClick={_onOpenModalRecord}
            size="large"
          />
        </div>
      ) : (
        <>
          {JSON.parse(dataCall).data?.length > 0 && (
            <Wrap>
              <Table
                dataSource={isSearchCore ? dataSourceCore : dataSource}
                columns={isSearchCore ? columnCore : column}
                pagination={false}
                scroll={{
                  x: "max-content",
                }}
                onRow={(record) => {
                  return {
                    onDoubleClick: () => {
                      if (isSearchCore) return;
                      _onSelectRecord(record);
                    },
                  };
                }}
              />
            </Wrap>
          )}
        </>
      )}

      <ModalRecord
        form={form}
        objectId={JSON.parse(dataCall).object_id}
        recordID={recordID}
        setRecordID={setRecordID}
        onFinish={_onSubmit}
        userDetail={userDetail}
        setEditingKey={setEditingKey}
      />

      <ModalDuplicate onFinishCall={_onSubmit} formValues={formValues} />
    </Wrapper>
  );
}

export default CreateRecord;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }
`;

const WrapSearch = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  .ant-input:placeholder-shown {
    text-transform: capitalize;
  }
`;

const Wrap = styled.div`
  padding-top: 24px;

  .ant-table-wrapper {
    padding: 16px 24px;
    background: #fff;
  }
`;

const CustomInputSearch = styled(Input.Search)`
  margin-right: 16px;
  width: 270px;
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: ${({ core }) => (core ? "40px" : "30px")} !important;
  }

  button {
    height: 40px;
  }
`;
