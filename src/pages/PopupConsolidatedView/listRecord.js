import styled from "styled-components/macro";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import Table from "antd/lib/table";

import { updateInteraction } from "redux/slices/callCenter";
import { callEvent } from "redux/slices/voiceBiometric";

function ListRecord(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { userDetail } = useSelector((state) => state.userReducer);

  const [dateSource, setDataSource] = useState([]);
  const [column, setColumn] = useState([]);

  let dataCall = localStorage.getItem("dataCall");

  const _onSelectRecord = (record) => {
    dispatch(
      updateInteraction({
        call_id: JSON.parse(dataCall).third_party_id,
        record_id: record.key,
        object_id: JSON.parse(dataCall).object_id,
        extension: JSON.parse(dataCall).extension,
      })
    );

    if (userDetail?.use_voice_biometric) {
      dispatch(
        callEvent({
          voice_biometric_event: "check_customer_registered_offcall",
          phone_incoming: JSON.parse(dataCall).phone,
          third_party_id: JSON.parse(dataCall).third_party_id,
          record_id: record.key,
          object_id: JSON.parse(dataCall).object_id,
          agent_name: JSON.parse(dataCall).username,
        })
      );
    }

    navigate(
      `/consolidated-view/${JSON.parse(dataCall).object_id}/${record.key}`
    );
  };

  useEffect(() => {
    let tempColumn = [];
    JSON.parse(dataCall).field.map((item) => {
      return tempColumn.push({
        key: item.ID,
        title: item.name === "Owner" ? "Assign to" : item.name,
        dataIndex: item.ID,
      });
    });
    setColumn(tempColumn);

    let tempData = [];
    JSON.parse(dataCall).data.forEach((item) => {
      let newItem = {};
      newItem["key"] = item._id;
      Object.entries(item).forEach(([key, value]) => {
        if (typeof value === "object" && value !== null) {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      tempData.push(newItem);
    });
    setDataSource(tempData);
  }, [dataCall]);

  return (
    <Wrapper>
      <Table
        dataSource={dateSource}
        columns={column}
        pagination={false}
        scroll={{
          x: "max-content",
        }}
        onRow={(record) => {
          return {
            onDoubleClick: () => {
              _onSelectRecord(record);
            },
          };
        }}
      />
    </Wrapper>
  );
}

export default ListRecord;

const Wrapper = styled.div`
  padding: 24px;

  .ant-table-wrapper {
    padding: 16px 24px;
    background: #fff;
  }
`;
