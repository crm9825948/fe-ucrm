import moment from "moment";
import React from "react";
import styled from "styled-components/macro";
import FileImg from "assets/icons/common/file.svg";
import { Progress } from "antd";
import { THEME_COLOR } from "configs/color";
import Highlighter from "react-highlight-words";

const ItemMonitor = ({ info, info_tenant, searchKey, t }) => {
  console.log(info_tenant);

  const formatNumber = (number) => {
    if (number !== null)
      if (number === 0) {
        return "0";
      } else {
        return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
    return "Không giới hạn";
  };

  return (
    <Wrap>
      <CustomText>
        {/* {info.Name} */}

        <Highlighter
          highlightClassName="highlighter"
          searchWords={[searchKey]}
          autoEscape={true}
          textToHighlight={info.Name}
        />
      </CustomText>
      <WrapPrefix>
        <ItemPrefix>
          <p>{t("monitoring.createdDate")}</p>
          <p>{moment(info.Created_Date).format("YYYY-MM-DD")}</p>
        </ItemPrefix>
        <ItemPrefixMiddle>
          <p>Prefix</p>
          <p>{info.prefix}</p>
        </ItemPrefixMiddle>
        <ItemPrefixEnd>
          <p>{t("monitoring.startSequence")}</p>
          <p>{info.record_number}</p>
        </ItemPrefixEnd>
      </WrapPrefix>
      <WrapperBottom>
        <div className="img-records">
          <img src={FileImg} alt="" />
        </div>
        <div className="info-records">
          <div className="details-record">
            <span style={{ fontWeight: "bold" }}>{t("monitoring.record")}</span>
            {info_tenant?.limit_record === null ? (
              ""
            ) : (
              <span>
                <span className="total-record">
                  {formatNumber(info?.total_record)}
                </span>
                <span className="max-record">
                  /{formatNumber(info_tenant?.limit_record)}
                </span>
              </span>
            )}
          </div>
          <div>
            {info_tenant?.limit_record === null ? (
              <div style={{ color: "#D48806", fontWeight: "bold" }}>
                {t("monitoring.Unlimited")}
              </div>
            ) : (
              <Progress
                percent={(info?.total_record / info_tenant?.limit_record) * 100}
                showInfo={false}
                strokeColor={
                  info?.record_number / info_tenant?.limit_record > 0.8
                    ? "#FF7E7E"
                    : `${console.log(THEME_COLOR)}`
                }
              />
            )}
          </div>
        </div>
      </WrapperBottom>
    </Wrap>
  );
};

export default ItemMonitor;

const Wrap = styled.div`
  border-radius: 8px;
  background: #fff;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  padding: 16px;
  width: calc((100% - 32px) / 3);
  @media screen and (max-width: 1566px) {
    width: calc((100% - 16px) / 2);
  }
  .highlighter {
    background-color: yellow;
  }
`;
const CustomText = styled.div`
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #000;
`;
const WrapPrefix = styled.div`
  width: 100%;
  display: flex;
  gap: 4;
  flex-wrap: wrap;
`;
const ItemPrefix = styled.div`
  width: calc((100% - 8px) / 3);
  background: #eef6fd;
  text-align: center;
  padding: 12px;
  margin: 16px 0;
  p {
    font-size: 14px;

    :first-child {
      color: #6b6b6b;
    }
    :last-child {
      color: #000;
      font-family: var(--roboto-500);
    }
  }
  border-top-left-radius: 8px;
  border-bottom-left-radius: 8px;
`;

const ItemPrefixMiddle = styled.div`
  width: calc((100% - 8px) / 3);
  background: #eef6fd;
  text-align: center;
  padding: 12px;
  p {
    font-size: 14px;

    :first-child {
      color: #6b6b6b;
    }
    :last-child {
      color: #000;
      font-family: var(--roboto-500);
    }
  }
  margin: 16px 4px;
`;

const ItemPrefixEnd = styled.div`
  width: calc((100% - 8px) / 3);
  background: #eef6fd;
  text-align: center;
  padding: 12px;
  margin: 16px 0;
  p {
    font-size: 14px;

    :first-child {
      color: #6b6b6b;
    }
    :last-child {
      color: #000;
      font-family: var(--roboto-500);
    }
  }
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
`;

const WrapperBottom = styled.div`
  display: flex;
  .img-records {
    padding: 8px;
    border-radius: 8px;
    background-color: #f2f4f5;
    width: 48px;
    height: 48px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .info-records {
    margin-left: 12px;
    width: 100%;
    .details-record {
      display: flex;
      justify-content: space-between;
      span:first-child {
      }
      .total-record {
        color: #2c2c2c;
        font-size: 16px;
        font-family: var(--roboto-500);
      }
      .max-record {
        color: #6b6b6b;
        font-size: 14px;
      }
    }
    .ant-progress-bg {
      background: ${(props) => props.theme.main};
    }
  }
`;
