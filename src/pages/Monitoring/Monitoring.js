import { ClockCircleOutlined, MailOutlined } from "@ant-design/icons";
import { Input, Progress } from "antd";
import defaultAvt from "assets/icons/common/defaultAvt.svg";
import FileImg from "assets/icons/common/file.svg";
import StatusImg from "assets/icons/common/status.svg";
import UCRMLogo from "assets/icons/common/ucrm-logo.png";
import moment from "moment";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadDataMonitor } from "redux/slices/monitoring";
import styled from "styled-components/macro";
import ItemMonitor from "./ItemMonitor";
import { useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";

const { Search } = Input;

const Monitoring = () => {
  const dispatch = useDispatch();
  const { dataMonitor } = useSelector((state) => state.monitorReducer);
  const [listObject, setListObject] = useState([]);
  const { info_record_object, info_tenant } = dataMonitor;
  const [searchKey, setSearchKey] = useState("");
  const { t } = useTranslation();
  useEffect(() => {
    dispatch(loadDataMonitor());
  }, [dispatch]);

  const formatNumber = (number) => {
    if (number !== null)
      if (number === 0) {
        return "0";
      } else {
        return number?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      }
    return t("monitoring.Unlimited");
  };
  useEffect(() => {
    setListObject(info_record_object);
  }, [info_record_object]);

  const renderListItem = (list) =>
    list?.map((item, index) => (
      <ItemMonitor
        key={index}
        info={item}
        info_tenant={info_tenant}
        searchKey={searchKey}
        t={t}
      />
    ));

  return (
    <>
      {Object.entries(dataMonitor).length > 0 && (
        <Wrap>
          <Left>
            <CommonInfo>
              <Name>
                <Logo>
                  <img src={UCRMLogo} alt="" />
                  <div>
                    <p>Unify CRM</p>
                    <Package>{info_tenant.name_package}</Package>
                  </div>
                </Logo>
                <Mail>
                  <p>{dataMonitor.package.full_name_tenant}</p>
                  <CustomSpan>
                    <MailOutlined />
                    {dataMonitor.package.email_tenant}
                  </CustomSpan>
                </Mail>
              </Name>
              <Status>
                <CustomSpan>
                  <img style={{ marginRight: "8px" }} src={StatusImg} alt="" />
                  {t("monitoring.Status")}
                </CustomSpan>
                <ItemStatus>{t("monitoring.Active")}</ItemStatus>
                <CustomSpan>
                  <ClockCircleOutlined /> {t("monitoring.createdDate")}
                </CustomSpan>
                <p>
                  {moment(dataMonitor.package.registration_date).format(
                    "YYYY-MM-DD"
                  )}
                </p>
              </Status>
              <Capacity>
                <User>
                  <CustomDisplay>
                    <img src={defaultAvt} alt="" />
                    <span>{t("monitoring.user")}</span>
                  </CustomDisplay>
                  <Percent>
                    <span>
                      {formatNumber(dataMonitor.package.number_of_users_use)}
                    </span>
                    <span>
                      /{formatNumber(dataMonitor.package.total_number_user)}
                    </span>
                  </Percent>
                  <Progress
                    strokeLinecap="butt"
                    percent={
                      (dataMonitor.package.number_of_users_use /
                        dataMonitor.package.total_number_user) *
                      100
                    }
                    format={() => ""}
                  />
                </User>

                <Limited>
                  <img src={FileImg} alt="" />
                  <span>
                    {t("monitoring.LimitOf")}{" "}
                    {formatNumber(info_tenant.limit_record)}{" "}
                    {t("monitoring.LimitOf1")}
                  </span>
                </Limited>
              </Capacity>
            </CommonInfo>
            <div className="search-component">
              <div className="list-objects">
                {t("monitoring.ListObjects")}
                <div>
                  <span>{listObject?.length}</span>
                </div>
              </div>

              <Search
                placeholder={t("monitoring.search")}
                onSearch={(e) => {
                  let tempArr = [];
                  console.log(e);
                  setSearchKey(e);
                  info_record_object.map((item, idx) => {
                    if (item["Name"].includes(e)) {
                      tempArr.push(item);
                    }
                    return null;
                  });
                  setListObject(tempArr);
                }}
                style={{
                  width: 300,
                }}
              />
            </div>
            <DetailInfo>{renderListItem(listObject)}</DetailInfo>
          </Left>
          {/* <Right>
            <Header>
              <img src={UCRMLogo} alt="" />
              <p>Unify CRM</p>
              <Flex>
                <Package>{info_tenant.name_package}</Package>
              </Flex>

              <InfoPackage>
                {packageInfo[`Professional`].map((item, idx) => {
                  return (
                    <div>
                      <img alt="" src={Tick} />
                      <span>{item}</span>
                    </div>
                  );
                })}
              </InfoPackage>
            </Header>
          </Right> */}
        </Wrap>
      )}
    </>
  );
};

export default withTranslation()(Monitoring);
// const InfoPackage = styled.div`
//   border-top: 1px solid #8c8c8c;
//   margin-top: 24px;
//   div {
//     width: 100%;
//     display: flex;
//     align-items: flex-start;
//     margin-bottom: 24px;
//     :first-child {
//       margin-top: 24px;
//     }
//     img {
//       width: 20px;
//       height: 20px;
//       margin-right: 8px;
//       margin-top: 5px;
//     }
//     span {
//       color: rgba(255, 255, 255, 0.8);
//       font-size: 16px;
//       text-align: left;
//     }
//   }
// `;
const Wrap = styled.div`
  background-color: #f9fafc;
  padding: 24px;
  display: flex;
  color: #000;
  font-size: 16px;
  p {
    margin-bottom: 0;
  }
`;
const Left = styled.div`
  width: 100%;
  .search-component {
    display: flex;
    justify-content: space-between;
    margin-top: 12px;
    .list-objects {
      display: flex;
      justify-content: space-between;
      font-family: var(--roboto-500);
      font-size: 18px;
      align-items: center;
      div {
        width: 24px;
        height: 24px;
        display: flex;
        justify-content: center;
        align-items: center;
        font-family: var(--roboto-500);
        font-size: 14px;
        border-radius: 28px;
        background: var(--primary-10, rgba(32, 162, 162, 0.1));
        color: var(--primary-5-primary-darker, #1c9292);
        margin-left: 8px;
      }
    }
  }
`;
// const Right = styled.div`
//   width: 30%;
//   padding: 24px;
//   background: linear-gradient(315deg, #0b5e66 0%, #0f314f 68.58%, #4670a9 100%);
//   margin-left: 16px;
//   border-radius: 8px;
// `;
const CommonInfo = styled.div`
  width: 100%;
  display: flex;
  padding: 16px;
  border-radius: 8px;
  background: #fff;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
`;

const Name = styled.div`
  padding-right: 16px;
  border-right: 1px solid #ececec;
  width: 40%;
`;

const Logo = styled.div`
  display: flex;
  font-family: var(--roboto-500);
  padding-bottom: 12px;
  border-bottom: 1px solid #ececec;
  p {
    font-size: 20px;
  }
  img {
    width: 68px;
    margin-right: 12px;
  }
`;

const Status = styled.div`
  width: 20%;
  margin-left: 16px;
  p {
    font-family: var(--roboto-500);
    margin-top: 8px;
  }
`;

const ItemStatus = styled.div`
  color: #0bb865;
  font-family: var(--roboto-500);
  font-size: 14px;
  border-radius: 24px;
  background: #e9fef4;
  padding: 4px 8px;
  margin: 8px 0 16px 0;
  width: fit-content;
`;

const Capacity = styled.div`
  width: 40%;
`;

const Package = styled.div`
  border-radius: 8px;
  background: #9254de;
  color: #fff;
  font-size: 16px;
  padding: 4px 12px;
  width: fit-content;
`;

const Mail = styled.div`
  margin-top: 12px;
  p {
    font-family: var(--roboto-500);
  }
`;
const CustomSpan = styled.div`
  display: flex;
  align-items: center;
  color: #6b6b6b;
  font-size: 14px;
  svg {
    font-size: 20px;
    margin-right: 8px;
    color: #a9a9a9;
  }
`;
const User = styled.div`
  width: 100%;
  border: 1px solid #ececec;
  border-bottom: none;
  padding: 16px;
  border-radius: 8px 8px 0px 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  img {
    width: 48px;
    height: 48px;
    object-fit: cover;
    border-radius: 8px;
    overflow: hidden;
    margin-right: 8px;
    min-width: 48px;
  }
  span {
    color: #2c2c2c;
    font-family: var(--roboto-500);
  }
  .ant-progress {
    position: absolute;
    height: 10px;
    bottom: 0;
    width: 100%;
    left: 0;
    .ant-progress-inner {
      background: #eaeaea;
    }
    .ant-progress-outer {
      padding: 0;
      margin: 0;
      .ant-progress-bg {
        height: 10px !important;
        background: ${(props) => props.theme.main};
      }
    }
  }
`;

const Percent = styled.div`
  span {
    :first-child {
      font-size: 18px;
      color: #000;
      font-family: var(--roboto-500);
    }
    :last-child {
      color: #6b6b6b;
      font-size: 14px;
      font-family: var(--roboto-400);
    }
  }
`;

const CustomDisplay = styled.div`
  display: flex;
  align-items: center;
  max-width: 70%;
`;

const Limited = styled.div`
  border-radius: 8px;
  border: 1px solid #ececec;
  padding: 10px 16px;
  width: 100%;
  margin-top: 22px;
  display: flex;
  align-items: center;
  img {
    margin-right: 8px;
  }
  span {
    color: #cf1322;
    font-size: 14px;
    font-family: var(--roboto-500);
    line-height: 20px;
  }
`;

// const Header = styled.div`
//   width: 100%;
//   text-align: center;

//   border-bottom: 1px solid #8c8c8c;
//   padding-bottom: 24px;
//   p {
//     font-family: var(--roboto-500);
//     font-size: 24px;
//     color: #fff;
//     margin: 16px 0;
//   }
//   img {
//     width: 120px;
//   }
// `;

// const Flex = styled.div`
//   width: 100%;
//   display: flex;
//   justify-content: center;
// `;

const DetailInfo = styled.div`
  display: flex;
  gap: 16px;
  margin-top: 16px;
  flex-wrap: wrap;
`;
