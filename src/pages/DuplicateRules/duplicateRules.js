import { Button, Checkbox, Select } from "antd";
import Breadcrumb from "antd/lib/breadcrumb";
import noData from "assets/icons/common/nodata.png";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  loadRules,
  updateRules,
  updatePosition,
  actionDuplicate,
  createRules,
} from "redux/slices/duplicateRules";
import { loadListObjectField } from "redux/slices/objects";
import styled from "styled-components/macro";
import RuleItem from "./ruleItem";
import _ from "lodash";
import App from "./dragAndDropPriority";
import duplicateImg from "assets/icons/common/duplicate.png";
import { changeTitlePage } from "redux/slices/authenticated";
import Conditions from "components/Conditions/conditions";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";
import { BASE_URL_API } from "constants/constants";

const { Option, OptGroup } = Select;

const DuplicateRules = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [objectId, setObjectId] = useState(undefined);
  const [listRules, setListRules] = useState([]);
  const [caseInsensetive, setCaseInsensetive] = useState(false);
  const [position, setPosition] = useState([]);
  const [action, setAction] = useState("block");
  //Condition
  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);
  const [crossRule, setCrossRule] = useState({});

  useEffect(() => {
    dispatch(changeTitlePage(t("duplicateRules.duplicateRules")));
    //eslint-disable-next-line
  }, [t]);

  const { objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  useEffect(() => {
    setAllCondition([]);
    setAnyCondition([]);
    setOperatorValueAnd([]);
    setOperatorValueOr([]);
    setValueAnd([]);
    setValueOr([]);
  }, [objectId]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "duplicate_rules" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { ruleList } = useSelector((state) => state.duplicateRulesReducer);

  const handleLoadRules = (id) => {
    dispatch(
      loadRules({
        object_id: id,
      })
    );
  };

  useEffect(() => {
    console.log(crossRule);
  }, [crossRule]);

  useEffect(() => {
    console.log("ruleList", ruleList);
    setAction(ruleList.action_duplicate ? ruleList.action_duplicate : "block");
    let arr = [];

    if (typeof ruleList.location_crm === "number") {
      arr[ruleList.location_crm] = {
        id: `item-${ruleList.location_crm}`,
        content: "Add UCRM rule",
      };
    } else {
      arr[0] = {
        id: "item-0",
        content: "Add UCRM rule",
      };
    }

    if (typeof ruleList.location_3rd === "number") {
      arr[ruleList.location_3rd] = {
        id: `item-${ruleList.location_3rd}`,
        content: "Add Thirdparty rule",
      };
    } else {
      arr[1] = {
        id: "item-1",
        content: "Add Thirdparty rule",
      };
    }
    setPosition([...arr]);

    setListRules((ruleList && ruleList.rules) || []);
    setCaseInsensetive((ruleList && ruleList.word_status) || false);
    if (
      (ruleList && ruleList.cross_rule) ||
      (ruleList && ruleList.cross_rule === 0)
    ) {
      if (ruleList.cross_rule === null) {
        setCrossRule({});
      } else {
        if (ruleList && ruleList.cross_rule === 0) {
          setCrossRule({
            position: 0,
          });
        } else {
          setCrossRule({
            position: ruleList.cross_rule,
          });
        }
      }
    }
    if (
      ruleList &&
      ruleList.filter_condition &&
      ruleList.filter_condition !== null &&
      ruleList._id &&
      Object.keys(ruleList.filter_condition).length > 0
    ) {
      setAllCondition(JSON.parse(ruleList.filter_condition).and_filter);
      setAnyCondition(JSON.parse(ruleList.filter_condition).or_filter);
    }
  }, [ruleList]);

  const handleAddRule = () => {
    let temp = [...listRules];
    temp.push([]);
    setListRules(temp);
  };
  const handleAddCrossRule = () => {
    let temp = [...listRules];
    temp.push([]);
    setListRules(temp);
    let obj = {};
    obj = {
      position: temp.length - 1,
    };
    setCrossRule(obj);
  };

  const handleRemoveRule = (index) => {
    let temp = [...listRules];
    temp.splice(index, 1);
    setListRules(temp);
    let flag = true;
    if (Object.keys(crossRule).length > 0 && crossRule.position === index) {
      setCrossRule({});
    } else if (
      Object.keys(crossRule).length > 0 &&
      index < crossRule.position
    ) {
      setCrossRule({
        position: crossRule.position - 1,
      });
    }
    temp.forEach((item, idx) => {
      if (item.length === 0) {
        flag = false;
      }
    });
    if (flag === true && temp.length === 0) {
      dispatch(
        updateRules({
          rules: temp,
          object_id: objectId,
          word_status: caseInsensetive,
          location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
          location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
        })
      );
    }
  };

  const handleUpdateRule = () => {
    let flag = true;
    listRules.forEach((item, idx) => {
      if (item.length === 0) {
        flag = false;
      }
    });

    if (flag === true) {
      if (ruleList?.rules?.length > 0) {
        dispatch(
          updateRules({
            rules: listRules,
            object_id: objectId,
            word_status: caseInsensetive,
            location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
            location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
            cross_rule:
              Object.keys(crossRule).length > 0 ? crossRule.position : null,
          })
        );
      } else {
        dispatch(
          createRules({
            rules: listRules,
            object_id: objectId,
            word_status: caseInsensetive,
            location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
            location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
            cross_rule:
              Object.keys(crossRule).length > 0 ? crossRule.position : null,
          })
        );
      }
    } else {
      Notification("warning", "Please fullfill required input!");
    }
  };

  useEffect(() => {
    setListRules([]);
    setObjectId(undefined);
    return () => {
      setListRules([]);
      setObjectId(undefined);
    };
  }, []);

  const updatePositionFunction = (position) => {
    dispatch(
      updatePosition({
        location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
        location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
        object_id: objectId,
      })
    );
  };

  const onFinishConditions = async () => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      for (let i = 0; i < operatorValueAnd.length; i++) {
        if (operatorValueAnd[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }

    if (operatorValueOr.length === anyCondition.length) {
      for (let i = 0; i < operatorValueOr.length; i++) {
        if (operatorValueOr[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }

    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "today" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-today" &&
        item === "" &&
        operatorValueAnd[idx] !== "yesterday" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-year" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        item === "" &&
        operatorValueOr[idx] !== "not-empty" &&
        item === "" &&
        operatorValueOr[idx] !== "mine" &&
        item === "" &&
        operatorValueOr[idx] !== "not-mine" &&
        item === "" &&
        operatorValueOr[idx] !== "today" &&
        item === "" &&
        operatorValueOr[idx] !== "not-today" &&
        item === "" &&
        operatorValueOr[idx] !== "yesterday" &&
        item === "" &&
        operatorValueOr[idx] !== "this-week" &&
        item === "" &&
        operatorValueOr[idx] !== "last-week" &&
        item === "" &&
        operatorValueOr[idx] !== "this-month" &&
        item === "" &&
        operatorValueOr[idx] !== "last-month" &&
        item === "" &&
        operatorValueOr[idx] !== "this-year" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill information!");
    } else {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object-duplicate-rules/condition",
          {
            object_id: objectId,
            filter_condition: JSON.stringify({
              and_filter: allCondition,
              or_filter: anyCondition,
            }),
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          Notification("success", "Successfully!");
        })
        .catch((error) => {
          Notification("error", error.response.data.error);
        });
    }
  };

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem> {t("duplicateRules.duplicateRules")}</BreadcrumbItem>
        </Breadcrumb>
      </WrapBreadcrumb>

      <WrapperContent>
        <Content>
          <Title>
            <span>
              <img src={duplicateImg} alt="duplicate" />{" "}
              {t("duplicateRules.create")}
            </span>
          </Title>

          <Introduce>
            <span>{t("duplicateRules.description")}</span>
          </Introduce>

          <ChooseObject>
            <p>{t("objectLayoutField.chooseObject")}</p>

            <CustomSelect
              placeholder={t("common.placeholderSelect")}
              value={objectId}
              // disabled={checkRule("create") ? false : true}
              onChange={(e) => {
                setObjectId(e);
                handleLoadRules(e);
                dispatch(
                  loadListObjectField({
                    api_version: "2",
                    object_id: e,
                  })
                );
              }}
            >
              {Object.entries(objectCategory).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {value.map((item, idx) => {
                      if (
                        Object.keys(item).includes("is_embed_iframe") ===
                          false ||
                        item?.is_embed_iframe === false
                      )
                        return <Option value={item._id}>{item.Name}</Option>;

                      return null;
                    })}
                  </OptGroup>
                );
              })}
            </CustomSelect>
          </ChooseObject>
          {objectId ? (
            <>
              <App
                objectId={objectId}
                listRules={listRules}
                position={position}
                setPosition={setPosition}
                updatePosition={updatePositionFunction}
                checkRule={checkRule}
                ruleList={ruleList}
                listRulesProps={
                  <>
                    {listRules.length === 0 ? (
                      ""
                    ) : (
                      <>
                        <ItemWrap>
                          {listRules.map((item, idx) => {
                            return (
                              <RuleItem
                                listObjectField={listObjectField}
                                item={item}
                                index={idx}
                                handleAddRule={handleAddRule}
                                listRules={listRules}
                                handleRemoveRule={handleRemoveRule}
                                setListRules={setListRules}
                                checkRule={checkRule}
                                crossRule={crossRule}
                              />
                            );
                          })}

                          {listRules.length > 0 ? (
                            checkRule("create") ? (
                              <>
                                <CustomAdd>
                                  <span
                                    onClick={() => {
                                      handleAddRule();
                                    }}
                                  >
                                    + {t("duplicateRules.add")}
                                  </span>
                                </CustomAdd>
                                {Object.keys(crossRule).length > 0 ? (
                                  ""
                                ) : (
                                  <CustomAdd>
                                    <span
                                      onClick={() => {
                                        handleAddCrossRule();
                                      }}
                                    >
                                      + Add cross rule
                                    </span>
                                  </CustomAdd>
                                )}
                              </>
                            ) : (
                              ""
                            )
                          ) : (
                            ""
                          )}

                          <Checkbox
                            checked={caseInsensetive}
                            onClick={() => {
                              setCaseInsensetive(!caseInsensetive);
                            }}
                          >
                            {t("duplicateRules.case")}
                          </Checkbox>
                        </ItemWrap>
                      </>
                    )}
                  </>
                }
                noRule={
                  listRules.length === 0 ? (
                    ""
                  ) : (
                    <ButtonGroup>
                      <div
                        className="btnGroup
               "
                      >
                        {checkRule("create") && checkRule("edit") ? (
                          <Button
                            onClick={() => {
                              handleUpdateRule();
                            }}
                          >
                            {t("common.save")}
                          </Button>
                        ) : (
                          ""
                        )}
                        <Button
                          onClick={() => {
                            setListRules([]);
                            setObjectId(undefined);
                          }}
                        >
                          {t("common.cancel")}
                        </Button>
                      </div>
                    </ButtonGroup>
                  )
                }
                addRule={
                  listRules.length === 0 && objectId !== undefined ? (
                    <Empty>
                      <img src={noData} alt="" />

                      <Note>
                        <span>{t("duplicateRules.note")}</span>
                      </Note>

                      {checkRule("create") ? (
                        <AddWrap>
                          <CustomButtonAddRecord
                            onClick={() => {
                              handleAddRule();
                            }}
                          >
                            + {t("duplicateRules.add")}
                          </CustomButtonAddRecord>
                        </AddWrap>
                      ) : (
                        ""
                      )}
                    </Empty>
                  ) : (
                    ""
                  )
                }
              />
              {listRules.length === 0 && ruleList.third_party === undefined ? (
                ""
              ) : (
                <>
                  {" "}
                  <div
                    style={{
                      fontSize: "16px",
                      fontWeight: "bold",
                      color: "#2c2c2c",
                      marginBottom: "8px",
                      marginTop: "16px",
                    }}
                  >
                    Setting duplicate <span style={{ color: "red" }}>*</span>
                  </div>
                  <Select
                    defaultValue={"block"}
                    onChange={(e) => {
                      dispatch(
                        actionDuplicate({
                          object_id: objectId,
                          action_duplicate: e,
                        })
                      );
                      setAction(e);
                    }}
                    value={action}
                  >
                    <Option value={"block"}>Block & Notify (pass)</Option>
                    <Option value={"create_override"}>
                      Create & Overwrite
                    </Option>
                  </Select>
                </>
              )}
              <div style={{ marginTop: "20px" }}>
                <Conditions
                  title={"AND condition"}
                  decs={"(All conditions must be met)"}
                  conditions={allCondition}
                  setConditions={setAllCondition}
                  ID={Object.keys(ruleList).length > 0 ? ruleList._id : ""}
                  dataDetails={Object.keys(ruleList).length > 0 ? ruleList : {}}
                  operatorValue={operatorValueAnd}
                  setOperatorValue={setOperatorValueAnd}
                  value={valueAnd}
                  setValue={setValueAnd}
                />
                <Conditions
                  title={"OR condition"}
                  decs={"(Any conditions must be met)"}
                  conditions={anyCondition}
                  setConditions={setAnyCondition}
                  ID={Object.keys(ruleList).length > 0 ? ruleList._id : ""}
                  dataDetails={Object.keys(ruleList).length > 0 ? ruleList : {}}
                  operatorValue={operatorValueOr}
                  setOperatorValue={setOperatorValueOr}
                  value={valueOr}
                  setValue={setValueOr}
                />

                <ButtonGroup>
                  <div
                    className="btnGroup
             "
                  >
                    {checkRule("create") && checkRule("edit") ? (
                      <Button onClick={() => onFinishConditions()}>
                        {t("common.save")}
                      </Button>
                    ) : (
                      ""
                    )}
                    <Button>{t("common.cancel")}</Button>
                  </div>
                </ButtonGroup>
              </div>
            </>
          ) : (
            ""
          )}
        </Content>
      </WrapperContent>
    </Wrapper>
  );
};

export default withTranslation()(DuplicateRules);

const Content = styled.div`
  width: 733px;
  padding: 24px;
  background: #ffffff;
`;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  border-color: ${(props) => props.theme.darker};
  color: #fff;
  font-size: 16px;
  display: flex;
  align-items: center;
  margin-top: 16px;
  border-radius: 2px;

  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const WrapperContent = styled.div`
  margin-top: 30px;
  width: 100%;
  display: flex;
  justify-content: center;
  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-checkbox-indeterminate .ant-checkbox-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
`;

const Title = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  span {
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    color: #000000;
    display: flex;
    align-items: center;
    img {
      margin-right: 12px;
    }
  }
`;

const Introduce = styled.div`
  text-align: center;
  padding: 0px 120px;
  margin-top: 16px;
  span {
    font-size: 16px;
    text-align: center;
    color: #000000;
  }
`;

const ChooseObject = styled.div`
  width: 100%;
  p {
    font-size: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-500);
    margin-bottom: 10px;
  }
`;

const CustomSelect = styled(Select)`
  width: 100%;
`;

const Empty = styled.div`
  width: 100%;
  text-align: center;
  img {
    width: 100px;
    height: 100px;
    object-fit: cover;
    margin: 16px 0 8px 0;
  }
`;

const Note = styled.div`
  text-align: center;
  padding: 0px 180px;

  span {
    font-size: 16px;
    text-align: center;
    color: #000000;
  }
`;

const AddWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const ItemWrap = styled.div`
  width: 100%;
  margin-top: 16px;
  .ant-checkbox + span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const CustomAdd = styled.div`
  width: 100%;
  span {
    cursor: pointer;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    :hover {
      text-decoration: underline;
    }
  }
`;

const ButtonGroup = styled.div`
  display: flex;
  justify-content: flex-end;
  button {
    border-radius: 2px;
    width: 114px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:first-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
        margin-right: 24px;
        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;
