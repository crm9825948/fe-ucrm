import React, { useEffect } from "react";
import { Collapse, Form, Input, Select, Tooltip, Button, Radio } from "antd";
import styled from "styled-components";
import PlusIcon from "assets/icons/common/plus.png";
import UCRMLogo from "assets/icons/common/ucrm-logo.png";
import ThirdParty from "assets/icons/common/third-party.png";
import Drag from "assets/icons/common/drag.png";
import deleteImg from "assets/icons/common/delete.svg";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import img from "assets/icons/common/confirm.png";
import { useState } from "react";
import {
  deleteDuplicate,
  createDuplicate3rd,
  updateDuplicate3rd,
  // updateDuplicate3rd,
} from "redux/slices/duplicateRules";
import { useTranslation, withTranslation } from "react-i18next";
import { Notification } from "components/Notification/Noti";
import { useDispatch, useSelector } from "react-redux";
const { Panel } = Collapse;

const AuthenticationType = [
  {
    label: "Basic",
    value: "basic",
  },
];

const Method = [
  {
    label: "POST",
    value: "POST",
  },
  {
    label: "GET",
    value: "GET",
  },
];

const PriorityItem = ({
  item,
  listRulesProps,
  noRule,
  addRule,
  thirdpartyRule,
  listRules,
  objectId,
  checkRule,
  position,
  // ruleList,/
}) => {
  const [open, setOpen] = useState(false);
  const [open3rd, setOpen3rd] = useState(false);
  const [form] = Form.useForm();
  const { t } = useTranslation();
  // const dispatch = useDispatch();
  const dispatch = useDispatch();
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { ruleList } = useSelector((state) => state.duplicateRulesReducer);
  const [requests, $requests] = useState([]);
  const [response, $response] = useState([]);
  const [listFields, $listFields] = useState([]);

  const _onSubmit = (values) => {
    if (
      requests.length === 0 ||
      requests.find(
        (item) =>
          item.field_3rd === undefined ||
          item.field_3rd === "" ||
          item.field_crm === undefined
      )
    ) {
      Notification("warning", "Please full field request!");
    } else if (
      requests.find(
        (item) =>
          item.field_crm.split("$")[0] !== "" && isNaN(Number(item.field_crm))
      )
    ) {
      Notification("warning", t("workflow.invalidNumber"));
    } else if (
      response.length > 0 &&
      response.find(
        (item) =>
          item.field_3rd === undefined ||
          item.field_3rd === "" ||
          item.field_crm === undefined
      )
    ) {
      Notification("warning", "Please full field response!");
    } else {
      const payload = {
        object_id: objectId,
        location_crm: position[0].id === "item-0" ? 0 : 1,
        location_3rd: position[1].id === "item-1" ? 1 : 0,
        third_party: [
          {
            pass_word: values.pass_word,
            authen_type: values.authen_type,
            method: values.method,
            api_url: values.api_url,
            user_name: values.user_name,
            name_component: values.name_component,
            field_request: requests,
            field_response: response,
          },
        ],
      };

      if (ruleList?.third_party?.length > 0) {
        dispatch(updateDuplicate3rd({ ...payload }));
      } else {
        dispatch(createDuplicate3rd({ ...payload }));
      }
    }
  };

  const _onAddRequest = () => {
    $requests([
      ...requests,
      {
        field_crm: undefined,
        field_3rd: undefined,
      },
    ]);
  };

  const _onDeleteRequest = (idx) => {
    let temp = [...requests];
    temp.splice(idx, 1);
    $requests(temp);
  };

  const handleChangeRequest = (value, index, type) => {
    let temp = [...requests];
    switch (type) {
      case "field_crm":
        temp[index] = {
          ...temp[index],
          field_crm: value,
        };
        break;
      case "field_3rd":
        temp[index] = {
          ...temp[index],
          field_3rd: value,
        };
        break;
      default:
        break;
    }
    $requests(temp);
  };
  const _onAddResponse = () => {
    $response([
      ...response,
      {
        field_crm: undefined,
        field_3rd: undefined,
      },
    ]);
  };

  const _onDeleteResponse = (idx) => {
    let temp = [...response];
    temp.splice(idx, 1);
    $response(temp);
  };

  const handleChangeResponse = (value, index, type) => {
    let temp = [...response];
    switch (type) {
      case "field_crm":
        temp[index] = {
          ...temp[index],
          field_crm: value,
        };
        break;
      case "field_3rd":
        temp[index] = {
          ...temp[index],
          field_3rd: value,
        };
        break;
      default:
        break;
    }
    $response(temp);
  };

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: "$" + field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    $listFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (ruleList?.third_party?.length > 0) {
      form.setFieldsValue({
        ...ruleList?.third_party[0],
      });
      $requests(ruleList?.third_party[0].field_request);
      $response(ruleList?.third_party[0].field_response);
    } else {
      form.resetFields();
      $requests([]);
      $response([]);
    }
  }, [ruleList, form]);

  return (
    <>
      <CustomColappse defaultActiveKey={["1"]}>
        <Panel
          showArrow={false}
          header={
            <div>
              <span>
                {listRules.length > 0 && item !== "Add Thirdparty rule" ? (
                  <img alt="" src={Drag} className="drag" />
                ) : (
                  ""
                )}
              </span>
              {listRules.length > 0 && item !== "Add Thirdparty rule" ? (
                <img alt="" src={UCRMLogo} className="ucrm-icon" />
              ) : (
                ""
              )}

              {listRules.length === 0 && item !== "Add Thirdparty rule" ? (
                <img alt="" src={PlusIcon} className="plus-icon" />
              ) : (
                ""
              )}

              {ruleList?.third_party?.length > 0 &&
              item === "Add Thirdparty rule" ? (
                <img alt="" src={Drag} className="drag" />
              ) : item === "Add Thirdparty rule" ? (
                <img alt="" src={PlusIcon} className="plus-icon" />
              ) : (
                ""
              )}

              {ruleList?.third_party?.length > 0 &&
              item === "Add Thirdparty rule" ? (
                <img alt="" src={ThirdParty} className="ucrm-icon" />
              ) : (
                ""
              )}

              <span className="title-priority-item">{item}</span>

              {checkRule("delete") ? (
                <img
                  alt=""
                  src={deleteImg}
                  className="delete-img"
                  onClick={() => {
                    if (item === "Add Thirdparty rule") {
                      setOpen3rd(true);
                    } else {
                      setOpen(true);
                    }
                  }}
                />
              ) : (
                ""
              )}
            </div>
          }
          key="2"
        >
          {item !== "Add Thirdparty rule" ? addRule : ""}
          {item !== "Add Thirdparty rule" ? listRulesProps : ""}
          {item !== "Add Thirdparty rule" ? noRule : ""}
          {item === "Add Thirdparty rule" && (
            <Form
              form={form}
              onFinish={_onSubmit}
              colon={false}
              layout="vertical"
              initialValues={{
                authen_type: "basic",
                method: "POST",
              }}
            >
              <Form.Item
                label="Name of component"
                name="name_component"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>

              <Form.Item
                label="API URL"
                name="api_url"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>

              <Form.Item
                label="Authentication type"
                name="authen_type"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={AuthenticationType}
                />
              </Form.Item>

              <Form.Item
                label="Username"
                name="user_name"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input
                  autocomplete="new-password"
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>

              <Form.Item
                label="Password"
                name="pass_word"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input.Password
                  autocomplete="new-password"
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>

              <Form.Item
                label="Method"
                name="method"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Radio.Group
                  optionType="button"
                  options={Method}
                  defaultValue="POST"
                />
              </Form.Item>

              <WrapRequests>
                <legend>Field request</legend>

                {requests.length > 0 && (
                  <>
                    {requests.map((item, idx) => {
                      return (
                        <WrapValue>
                          <p>3rd</p>
                          <Input
                            value={item.field_3rd}
                            placeholder={t("common.placeholderInput")}
                            onChange={(e) =>
                              handleChangeRequest(
                                e.target.value,
                                idx,
                                "field_3rd"
                              )
                            }
                          />
                          <p>CRM</p>
                          <Select
                            value={item.field_crm}
                            placeholder={t("common.placeholderInputSelect")}
                            options={listFields}
                            mode="tags"
                            onChange={(e) => {
                              if (e?.length > 1) {
                                e.shift();
                              }
                              handleChangeRequest(e[0], idx, "field_crm");
                            }}
                          />
                          <Delete onClick={() => _onDeleteRequest(idx)}>
                            <Tooltip title="Delete">
                              <img src={deleteImg} alt="delete" />
                            </Tooltip>
                          </Delete>
                        </WrapValue>
                      );
                    })}
                  </>
                )}
                <AddFields onClick={() => _onAddRequest()}>
                  <span>+ Add field</span>
                </AddFields>
              </WrapRequests>

              <WrapResponse>
                <legend>Field mapping</legend>

                {response.length > 0 && (
                  <>
                    {response.map((item, idx) => {
                      return (
                        <WrapValue>
                          <p>3rd</p>
                          <Input
                            value={item.field_3rd}
                            placeholder={t("common.placeholderInput")}
                            onChange={(e) =>
                              handleChangeResponse(
                                e.target.value,
                                idx,
                                "field_3rd"
                              )
                            }
                          />
                          <p>CRM</p>
                          <Select
                            value={item.field_crm}
                            placeholder={t("common.placeholderSelect")}
                            options={listFields}
                            onChange={(e) => {
                              handleChangeResponse(e, idx, "field_crm");
                            }}
                          />
                          <Delete onClick={() => _onDeleteResponse(idx)}>
                            <Tooltip title="Delete">
                              <img src={deleteImg} alt="delete" />
                            </Tooltip>
                          </Delete>
                        </WrapValue>
                      );
                    })}
                  </>
                )}
                <AddFields onClick={() => _onAddResponse()}>
                  <span>+ Add field</span>
                </AddFields>
              </WrapResponse>

              <WrapButton>
                <Button type="primary" htmlType="submit">
                  {t("common.save")}
                </Button>
              </WrapButton>
            </Form>
          )}
        </Panel>
      </CustomColappse>
      <ModalConfirm
        title={"Bạn có chắc chắn xóa cấu hình này?"}
        decs={"Dữ liệu sau khi xóa sẽ không được hoàn tác!"}
        open={open}
        setOpen={setOpen}
        method={deleteDuplicate}
        data={{
          location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
          location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
          object_id: objectId,
          rules: [],
        }}
        setOpenModal={() => {}}
        img={img}
      />

      <ModalConfirm
        title={"Bạn có chắc chắn xóa cấu hình này?"}
        decs={"Dữ liệu sau khi xóa sẽ không được hoàn tác!"}
        open={open3rd}
        setOpen={setOpen3rd}
        method={deleteDuplicate}
        data={{
          location_crm: position[0]?.content === "Add UCRM rule" ? 0 : 1,
          location_3rd: position[0]?.content !== "Add UCRM rule" ? 0 : 1,
          object_id: objectId,
          third_party: [],
        }}
        setOpenModal={() => {}}
        img={img}
      />
    </>
  );
};

export default withTranslation()(PriorityItem);

const CustomColappse = styled(Collapse)`
  border: 1px solid #1c9292;
  border-radius: 5px;
  background-color: #fff;

  :hover {
    box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
    .delete-img {
      visibility: visible;
      opacity: 1;
    }
    cursor: pointer;
    transition: all 0.4s ease-in-out;
  }
  .delete-img {
    position: absolute;
    right: 30px;
    top: 35%;
    width: 30px;
    margin-right: 0;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }
  .plus-icon {
    width: 20px;
  }
  .drag {
    width: 40px;
    margin-right: 28px;
  }
  .ucrm-icon {
    width: 48px;
  }
  > .ant-collapse-item.ant-collapse-no-arrow > .ant-collapse-header {
    padding: 30px !important;
  }
  .title-priority-item {
    font-style: normal;
    font-weight: bold;
    font-size: 18px;
    line-height: 22px;
    /* identical to box height, or 122% */

    /* Character/Body text */

    color: #2c2c2c;
    margin-left: 26px;
  }

  .ant-form-item-label > label {
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }
`;

const WrapRequests = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }

    :before {
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }
`;

const WrapResponse = styled(WrapRequests)`
  margin-top: 24px;

  legend {
    :before {
      content: "";
    }
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const WrapValue = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    margin-bottom: 0;
    margin-right: 16px;
  }

  .ant-input {
    width: 40%;
    margin-right: 8px;
  }

  .ant-select {
    width: 30%;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const WrapButton = styled.div`
  display: flex;
  margin-top: 16px;
  justify-content: flex-end;

  .ant-btn {
    font-size: 16px;
    height: unset;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;
