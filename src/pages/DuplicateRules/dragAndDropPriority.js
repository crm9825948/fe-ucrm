import React, { Component } from "react";
// import ReactDOM from "react-dom";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
// import { Collapse } from "antd";
import PriorityItem from "./priorityItem";
// const { Panel } = Collapse;

// fake data generator
// const getItems = (count) => [
//   {
//     id: "item-1",
//     content: "Add UCRM rule",
//   },
//   {
//     id: "item-0",
//     content: "Add Thirdparty rule",
//   },
// ];
//   Array.from({ length: count }, (v, k) => k).map((k) => ({
//     id: `item-${k}`,
//     content: `Add Thirdparty rule`,
//   }));

// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);

  return result;
};

const grid = 8;

const getItemStyle = (isDragging, draggableStyle) => ({
  // some basic styles to make the items look a bit nicer
  userSelect: "none",
  //   padding: grid * 2,
  margin: `0 0 ${grid}px 0`,

  // change background colour if dragging
  //   background: isDragging ? "lightgreen" : "grey",

  // styles we need to apply on draggables
  ...draggableStyle,
});

const getListStyle = (isDraggingOver) => ({
  //   background: isDraggingOver ? "lightblue" : "lightgrey",
  //   padding: grid,
  marginTop: "32px",
});

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: this.props.position,
    };
    this.onDragEnd = this.onDragEnd.bind(this);
  }

  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    const items = reorder(
      this.props.position,
      result.source.index,
      result.destination.index
    );
    this.props.setPosition([...items]);
    this.props.updatePosition([...items]);
    this.setState({
      items,
    });
  }

  // Normally you would want to split things out into separate components.
  // But in this example everything is just done in one place for simplicity
  render() {
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <div
              {...provided.droppableProps}
              ref={provided.innerRef}
              style={getListStyle(snapshot.isDraggingOver)}
            >
              {this.props.position.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                      )}
                    >
                      {/* <Collapse defaultActiveKey={["1"]}>
                        <Panel
                          showArrow={false}
                          header="This is panel header with no arrow icon"
                          key="2"
                        >
                          <p> {item.content}</p>
                        </Panel>
                      </Collapse> */}
                      <PriorityItem
                        checkRule={this.props.checkRule}
                        item={item.content}
                        listRulesProps={this.props.listRulesProps}
                        noRule={this.props.noRule}
                        addRule={this.props.addRule}
                        listRules={this.props.listRules}
                        objectId={this.props.objectId}
                        position={this.props.position}
                        ruleList={this.props.ruleList}
                      />
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    );
  }
}
