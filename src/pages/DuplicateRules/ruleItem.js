import { Select } from "antd";
import React from "react";
import styled from "styled-components";
import fileImg from "assets/icons/common/file.png";
import { useTranslation } from "react-i18next";
import deleteImg from "assets/icons/common/close.svg";
const { Option, OptGroup } = Select;

const RuleItem = (props) => {
  const {
    listObjectField,
    // item,
    index,
    // handleAddRule,
    listRules,
    handleRemoveRule,
    setListRules,
    checkRule,
    crossRule,
  } = props;

  const { t } = useTranslation();

  return (
    <ItemWrap>
      <ItemImg>
        <img src={fileImg} alt="file" />
      </ItemImg>

      <ItemContent>
        <HeaderContent>
          <span>
            {t("duplicateRules.rule")}{" "}
            {Object.keys(crossRule).length > 0 && crossRule.position === index
              ? "Cross"
              : ""}{" "}
            {index + 1}
          </span>
          <img
            src={deleteImg}
            alt="delete"
            onClick={() => {
              handleRemoveRule(index);
            }}
          />
        </HeaderContent>
        <SelectFeld>
          <p>{t("duplicateRules.explain")}</p>
          <CustomSelect
            mode={"multiple"}
            placeholder={t("common.placeholderSelect")}
            value={listRules.length > 0 ? listRules[index] : []}
            onChange={(e) => {
              let temp = [...listRules];
              temp[index] = [...e];
              setListRules(temp);
            }}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              console.log(inputValue, option.children);
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"] !==
                null &&
              listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                return (
                  <OptGroup label={section.section_name}>
                    {section.fields.map((field, idx) => {
                      console.log(field);
                      if (
                        field.type === "text" ||
                        field.type === "number" ||
                        field.type === "email" ||
                        (field.type === "select" && field.multiple === false)
                      )
                        if (field?.encrypted !== true)
                          return (
                            <Option
                              value={field.ID}
                              disabled={
                                !checkRule("edit") ||
                                (listRules[index].length === 5 &&
                                  !listRules[index].includes(field.ID))
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        else return null;
                      return null;
                    })}
                  </OptGroup>
                );
              })}
          </CustomSelect>
        </SelectFeld>
      </ItemContent>
    </ItemWrap>
  );
};

export default RuleItem;

/* {listRules.length === index + 1 ? (
  ""
) : (
  <div className="or">
    <div>|</div>
    <div>Or</div>
    <div>|</div>
  </div>
)} */

/* {checkRule("create") && checkRule("edit") ? (
    <Button
      size="small"
      onClick={() => {
        handleRemoveRule(index);
      }}
    >
      X
    </Button>
  ) : (
    ""
  )} */

/* 
{listRules.length === index + 1 ? (
  checkRule("create") ? (
    <button
    className="add-rules"
    onClick={() => {
      handleAddRule();
    }}
  >
    + Add rules
  </button>
   ) : (
""
   )
 ) : (
   ""
 )} */

const ItemWrap = styled.div`
  border: 1px solid #f0f0f0;
  border-radius: 5px;
  width: 100%;
  padding: 24px;
  margin-bottom: 8px;
  display: flex;
`;

const ItemImg = styled.div`
  img {
    margin-right: 24px;
  }
`;

const ItemContent = styled.div`
  flex: 1;
`;

const HeaderContent = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;

  span {
    font-family: var(--roboto-500);
    font-size: 18px;
    color: #2c2c2c;
  }
  img {
    cursor: pointer;
  }
`;

const SelectFeld = styled.div`
  p {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
    margin-bottom: 4px;
  }
`;

const CustomSelect = styled(Select)`
  width: 100%;
`;
