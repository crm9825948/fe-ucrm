import React, { useRef } from "react";
import { Editor } from "@tinymce/tinymce-react";
import { useDispatch } from "react-redux";
import { createService } from "redux/slices/enhancementView";
import { Button } from "antd";

function NewEditor() {
  const editorRef = useRef(null);
  const dispatch = useDispatch();

  const _onSubmit = () => {
    dispatch(
      createService({
        object_id: "obj_articearticle_49308314",
        service: "article",
        data: {
          fld_body: editorRef.current.getContent(),
          fld_category: "Thông báo của Vietinbank",
          fld_status: "Pin",
          fld_title: "Test new editor",
        },
      })
    );
  };

  return (
    <div style={{ position: "relative" }}>
      <Editor
        onInit={(evt, editor) => (editorRef.current = editor)}
        init={{
          height: 500,
          toolbar_mode: "sliding",
          plugins:
            "preview importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons",
          toolbar:
            "undo redo | bold italic underline strikethrough | fontfamily fontsize blocks | lineheight alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl",
          content_style:
            "body { font-family:Helvetica,Arial,sans-serif; font-size:14px }",
        }}
      />

      <Button style={{ margin: "16px 16px" }} onClick={_onSubmit}>
        Save
      </Button>
    </div>
  );
}

export default NewEditor;
