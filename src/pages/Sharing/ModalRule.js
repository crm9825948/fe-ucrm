import { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";

import { Notification } from "components/Notification/Noti";
import {
  createRule,
  updateRule,
  loadRuleObject,
  resetStatus,
} from "redux/slices/sharing";

function ModalRule({
  showModalRule,
  onHideModalRule,
  listAllGroups,
  objectID,
  isEdit,
  setIsEdit,
  IDRule,
  setActiveKey,
}) {
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { loading, status, allRules } = useSelector(
    (state) => state.sharingReducer
  );

  const [listGroups, setListGroups] = useState([]);

  const _onSubmit = (values) => {
    let data = {
      access_level: values.status,
      group_id: values.object_of,
      object_id: objectID,
      shared_to: values.share_to,
    };
    if (isEdit) {
      dispatch(
        updateRule({
          ...data,
          id: IDRule,
        })
      );
    } else {
      dispatch(
        createRule({
          ...data,
        })
      );
    }
  };

  const _onCancel = () => {
    onHideModalRule();
    form.resetFields();
    setIsEdit(false);
  };

  useEffect(() => {
    let tempGroup = [];
    if (listAllGroups.length > 0) {
      listAllGroups.map((item) => {
        return tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });
    }
    setListGroups(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    if (isEdit) {
      let detailsRule = allRules.filter((rule) => rule._id === IDRule);
      let tempShared = [];
      detailsRule[0].group_shared.map((item) => {
        return tempShared.push(item._id);
      });
      form.setFieldsValue({
        object_of: detailsRule[0].group_detail._id,
        share_to: tempShared,
        status: detailsRule[0].access_level,
      });
    }
  }, [isEdit, IDRule, allRules, form]);

  useEffect(() => {
    if (!loading.modalRule) {
      if (status.createRule === "success" || status.updateRule === "success") {
        onHideModalRule();
        form.resetFields();
        setIsEdit(false);

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetStatus());
        setActiveKey(objectID);
        dispatch(
          loadRuleObject({
            object_id: objectID,
          })
        );
      }

      if (status.createRule !== null && status.createRule !== "success") {
        Notification("error", status.createRule);
        dispatch(resetStatus());
      }

      if (status.updateRule !== null && status.updateRule !== "success") {
        Notification("error", status.updateRule);
        dispatch(resetStatus());
      }
    }
  }, [
    dispatch,
    form,
    isEdit,
    loading,
    objectID,
    onHideModalRule,
    setIsEdit,
    status,
    setActiveKey,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("sharing.editRule") : t("sharing.addRule")}
      visible={showModalRule}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("sharing.objectOf")}
          name="object_of"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={listGroups}
            placeholder={t("common.placeholderSelect")}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Form.Item
          label={t("sharing.shareTo")}
          name="share_to"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={listGroups}
            placeholder={t("common.placeholderSelect")}
            mode="multiple"
            optionFilterProp="label"
          />
        </Form.Item>

        <Form.Item
          label={t("common.status")}
          name="status"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Radio.Group>
            <Radio value={"read"}>{t("common.read")}</Radio>
            <Radio value={"write"}>
              {t("common.read")} & {t("common.write")}
            </Radio>
          </Radio.Group>
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={loading.modalRule}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalRule);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
