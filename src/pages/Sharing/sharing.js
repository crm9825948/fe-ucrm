import styled from "styled-components";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Collapse from "antd/lib/collapse";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Table from "antd/lib/table";
import { CaretRightOutlined } from "@ant-design/icons";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadRuleObject,
  deleteRule,
  resetStatus,
  loadDataNecessary,
} from "redux/slices/sharing";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalRule from "./ModalRule";
import { Notification } from "components/Notification/Noti";
import { changeTitlePage } from "redux/slices/authenticated";

function Sharing(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.sharing")));
    //eslint-disable-next-line
  }, [t]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { loading, status, allRules } = useSelector(
    (state) => state.sharingReducer
  );

  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const [activeKey, setActiveKey] = useState([]);
  const [showModalRule, setShowModalRule] = useState(false);
  const [objectID, setObjectID] = useState("");
  const [isEdit, setIsEdit] = useState(false);
  const [IDRule, setIDRule] = useState({});

  const [listRules, setListRules] = useState([]);
  const [dataDelete, setDataDelete] = useState({});

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "sharing" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleChangePanel = (key) => {
    setActiveKey(key);

    if (key) {
      dispatch(
        loadRuleObject({
          object_id: key,
        })
      );
    }
  };

  const _onAddRule = (e, id) => {
    e.stopPropagation();
    setShowModalRule(true);
    setObjectID(id);
  };

  const _onDeleteRule = (data, id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: data });
    setObjectID(id);
  };

  const _onEditRule = (data, id) => {
    setIDRule(data.key);
    setObjectID(id);
    setIsEdit(true);
    setShowModalRule(true);
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    let tempList = [];

    allRules.map((item, idx) => {
      let tempGroupShare = [];
      item.group_shared.map((group) => {
        return tempGroupShare.push(group.name);
      });

      return tempList.push({
        key: item._id,
        rule_number: idx + 1,
        owner: item.group_detail.name,
        share_to: tempGroupShare,
        privileges: item.access_level === "read" ? "Read" : "Read & Write",
      });
    });
    setListRules(tempList);
  }, [allRules]);

  useEffect(() => {
    if (!loading.modalDelete) {
      if (status.deleteRule === "success") {
        dispatch(setShowModalConfirmDelete(false));

        Notification("success", "Delete successfully!");
        dispatch(resetStatus());
        dispatch(
          loadRuleObject({
            object_id: objectID,
          })
        );
      }

      if (status.deleteRule !== null && status.deleteRule !== "success") {
        Notification("error", status.deleteRule);
        dispatch(resetStatus());
      }
    }
  }, [dispatch, loading.modalDelete, objectID, status.deleteRule]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.sharing")}</BreadcrumbItem>
      </Breadcrumb>

      <WrapCollapse>
        {Object.keys(category).length > 0 ? (
          <>
            <Title>
              <span>{t("object.objectName")}</span>
            </Title>
            <Collapse
              accordion
              expandIcon={({ isActive }) => (
                <CaretRightOutlined rotate={isActive ? -90 : 90} />
              )}
              activeKey={activeKey}
              onChange={handleChangePanel}
            >
              {Object.entries(category).map(([key, val]) => {
                return val.map((item) => {
                  return (
                    item.Status &&
                    item.active && (
                      <PanelCustom
                        header={item.Name}
                        key={item._id}
                        $active={activeKey === item._id}
                        extra={
                          activeKey === item._id &&
                          checkRule("create") && (
                            <AddRule
                              $active={activeKey === item._id}
                              onClick={(e) => _onAddRule(e, item._id)}
                            >
                              + {t("sharing.addRule")}
                            </AddRule>
                          )
                        }
                      >
                        {/* {loading.loadRule ? (
                            <Spin />
                          ) : ( */}
                        <Table pagination={false} dataSource={listRules}>
                          <Column
                            title={t("sharing.ruleNumber")}
                            dataIndex="rule_number"
                            key="rule_number"
                          />
                          <Column
                            title={t("sharing.owner")}
                            dataIndex="owner"
                            key="owner"
                            render={(text) => (
                              <Shared>
                                <span>{text}</span>
                              </Shared>
                            )}
                          />
                          <Column
                            title={t("sharing.shareTo")}
                            dataIndex="share_to"
                            key="share_to"
                            render={(text) => (
                              <Wrap>
                                {text.map((share, indexShare) => {
                                  return (
                                    <Shared key={indexShare}>
                                      <span>{share}</span>
                                    </Shared>
                                  );
                                })}
                              </Wrap>
                            )}
                          />
                          <Column
                            title={t("role.privileges")}
                            dataIndex="privileges"
                            key="privileges"
                            render={(text, record) => (
                              <WrapPrivileges>
                                <span>{text}</span>

                                {(checkRule("edit") || checkRule("delete")) && (
                                  <WrapAction>
                                    {checkRule("edit") && (
                                      <Tooltip title={t("common.edit")}>
                                        <img
                                          onClick={() =>
                                            _onEditRule(record, item._id)
                                          }
                                          src={Edit}
                                          alt="edit"
                                        />
                                      </Tooltip>
                                    )}
                                    {checkRule("delete") && (
                                      <Tooltip title={t("common.delete")}>
                                        <img
                                          onClick={() =>
                                            _onDeleteRule(record.key, item._id)
                                          }
                                          src={Delete}
                                          alt="delete"
                                        />
                                      </Tooltip>
                                    )}
                                  </WrapAction>
                                )}
                              </WrapPrivileges>
                            )}
                          />
                        </Table>
                        {/* )} */}
                      </PanelCustom>
                    )
                  );
                });
              })}
            </Collapse>
          </>
        ) : (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>
              {t("object.noObject")} {t("object.object")}
            </span>
          </Empty>
        )}
      </WrapCollapse>

      <ModalRule
        showModalRule={showModalRule}
        onHideModalRule={() => setShowModalRule(false)}
        listAllGroups={listAllGroups}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        IDRule={IDRule}
        setActiveKey={setActiveKey}
      />
      <ModalConfirmDelete
        title={t("sharing.rule")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteRule}
        dataDelete={dataDelete}
        isLoading={loading.modalDelete}
      />
    </Wrapper>
  );
}

export default withTranslation()(Sharing);

const { Panel } = Collapse;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-space {
    width: 100%;
  }

  .ant-collapse {
    border: none;
    background: #f2f8ff;
  }

  .ant-collapse-item {
    border-bottom: none;
    margin-bottom: 10px;
  }

  .ant-collapse-content {
    border-top: none;
  }

  .ant-collapse-extra {
    display: none;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-collapse-content > .ant-collapse-content-box {
    padding: 0;
  }

  .ant-table-thead > tr > th {
    background: #f0f0f0;
    border-bottom: 1px solid #d9d9d9;
    border-right: 1px solid #d9d9d9;
    padding: 8.5px 16px;
    font-size: 16px;
    color: #2c2c2c;

    :before {
      display: none;
    }

    :last-child {
      border-right: none;
    }
  }

  .ant-table-tbody > tr > td {
    padding: 8.5px 16px;
    border-bottom: 1px solid #ededed;
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-table-cell {
    :first-child {
      width: 10%;
    }

    :nth-child(2) {
      width: 30%;
    }

    :nth-child(4) {
      width: 20%;
    }
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapCollapse = styled.div`
  margin-top: 16px;
`;

const Title = styled.div`
  background: #fff;
  border-radius: 10px 10px 0px 0px;
  height: 46px;
  padding: 10px 0 10px 43px;
  margin-bottom: 10px;

  span {
    font-family: var(--roboto-500);
    font-size: 18px;
    color: #2c2c2c;
  }
`;

const PanelCustom = styled(Panel)`
  .ant-collapse-header {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    font-size: 16px;
    font-family: var(--roboto-500);
    color: ${(props) => (props.$active ? "#fff" : "#2c2c2c")}!important;
    border: 1px solid #ececec;
    padding: 9px 16px !important;

    .ant-collapse-extra {
      display: ${(props) => (props.$active ? "block" : "none")};
    }

    :hover {
      .ant-collapse-extra {
        display: block;
      }
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
    }
  }
`;

const AddRule = styled(Button)`
  border: ${(props) =>
    props.$active ? "1px solid #fff" : "1px solid #20a2a2"};
  padding: 0 15px;
  background: ${(props) => (props.$active ? props.theme.main : "#fff")};

  span {
    color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    font-size: 16px;
  }

  :hover {
    background: ${(props) =>
      props.$active ? "#fff" : props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker} !important;

    span {
      color: ${(props) =>
        props.$active ? props.theme.darker : "#fff"}!important;
    }
  }

  &.ant-btn:active {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    border-color: ${(props) => props.theme.main} span {
      color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    }
  }

  &.ant-btn:focus {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    border-color: ${(props) => (props.$active ? "#fff" : props.theme.main)};

    span {
      color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;

  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapPrivileges = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;
`;

const Shared = styled.div`
  background: #f2f4f5;
  border-radius: 5px;
  padding: 2px 8px;
  width: fit-content;
  margin-right: 8px;
`;
