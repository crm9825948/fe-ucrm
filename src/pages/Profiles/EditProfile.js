import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Checkbox from "antd/lib/checkbox";
import Button from "antd/lib/button";
import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";

import { Notification } from "components/Notification/Noti";
import {
  loadDetailProfile,
  updateProfile,
  resetProfileInfo,
  unMountEditProfile,
} from "redux/slices/profile";
import { objectStandardGetList } from "redux/slices/objectStandard";

import Edit from "assets/icons/common/edit.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import ModalEditFields from "./ModalEditFields";

function EditProfile(props) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { recordID } = useParams();

  const dispatch = useDispatch();
  const { isLoading, detailProfile, profileInfo } = useSelector(
    (state) => state.profileReducer
  );
  const { listSettingsOS } = useSelector(
    (state) => state.objectStandardReducer
  );

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const [showField, setShowField] = useState(false);
  const [privilege, setPrivilege] = useState([]);
  const [IDObject, setIDObject] = useState("");

  const [privilegeAll, setPrivilegeAll] = useState({
    Create: false,
    Delete: false,
    Read: false,
    Update: false,
  });

  const [dataSource, $dataSource] = useState([]);

  const [disableAllPrivilege, setDisableAllPrivilege] = useState(false);

  const _onEditField = (id) => {
    setShowField(true);
    setIDObject(id);
  };

  const _onHideEditField = () => {
    setShowField(false);
  };

  const onCheckAllPrivilege = (value) => {
    let tempPrivilege = [...privilege];

    if (value.target.name === "All") {
      tempPrivilege.forEach((item, idx) => {
        tempPrivilege[idx] = {
          ...tempPrivilege[idx],
          detail_permission: {
            Create: value.target.checked,
            Delete: value.target.checked,
            Read: value.target.checked,
            Update: value.target.checked,
            Import_Data: value.target.checked,
            Export_Data: value.target.checked,
            View_Report: value.target.checked,
            Create_Report: value.target.checked,
            Edit_Report: value.target.checked,
            Delete_Report: value.target.checked,
            View_ListView: value.target.checked,
            Create_ListView: value.target.checked,
            Edit_ListView: value.target.checked,
            Delete_ListView: value.target.checked,
          },
        };
      });

      setPrivilegeAll({
        Create: value.target.checked,
        Delete: value.target.checked,
        Read: value.target.checked,
        Update: value.target.checked,
        Import_Data: value.target.checked,
        Export_Data: value.target.checked,
        View_Report: value.target.checked,
        Create_Report: value.target.checked,
        Edit_Report: value.target.checked,
        Delete_Report: value.target.checked,
        View_ListView: value.target.checked,
        Create_ListView: value.target.checked,
        Edit_ListView: value.target.checked,
        Delete_ListView: value.target.checked,
      });
    } else if (value.target.name === "Read") {
      tempPrivilege.forEach((item, idx) => {
        tempPrivilege[idx] = {
          ...tempPrivilege[idx],
          detail_permission: {
            Read: value.target.checked,
            Create: value.target.checked === false ? false : true,
            Delete: value.target.checked === false ? false : true,
            Update: value.target.checked === false ? false : true,
            Import_Data: value.target.checked === false ? false : true,
            Export_Data: value.target.checked === false ? false : true,
            View_Report: value.target.checked === false ? false : true,
            Create_Report: value.target.checked === false ? false : true,
            Edit_Report: value.target.checked === false ? false : true,
            Delete_Report: value.target.checked === false ? false : true,
            View_ListView: value.target.checked === false ? false : true,
            Create_ListView: value.target.checked === false ? false : true,
            Edit_ListView: value.target.checked === false ? false : true,
            Delete_ListView: value.target.checked === false ? false : true,
          },
        };
      });

      setPrivilegeAll({
        Read: value.target.checked,
        Create: value.target.checked === false ? false : true,
        Delete: value.target.checked === false ? false : true,
        Update: value.target.checked === false ? false : true,
        Import_Data: value.target.checked === false ? false : true,
        Export_Data: value.target.checked === false ? false : true,
        View_Report: value.target.checked === false ? false : true,
        Create_Report: value.target.checked === false ? false : true,
        Edit_Report: value.target.checked === false ? false : true,
        Delete_Report: value.target.checked === false ? false : true,
        View_ListView: value.target.checked === false ? false : true,
        Create_ListView: value.target.checked === false ? false : true,
        Edit_ListView: value.target.checked === false ? false : true,
        Delete_ListView: value.target.checked === false ? false : true,
      });
    } else {
      tempPrivilege.forEach((item, idx) => {
        tempPrivilege[idx] = {
          ...tempPrivilege[idx],
          detail_permission: {
            ...tempPrivilege[idx].detail_permission,
            [value.target.name]: value.target.checked,
          },
        };
      });
      let temp = { ...privilegeAll };
      setPrivilegeAll({
        ...temp,
        [value.target.name]: value.target.checked,
      });
    }

    setPrivilege(tempPrivilege);
  };

  const onChangePrivilege = (value) => {
    let tempPrivilege = [...privilege];

    if (value.target.name.split("#")[0] === "All") {
      tempPrivilege.forEach((item, idx) => {
        if (item.id_object === value.target.name.split("#")[1]) {
          tempPrivilege[idx] = {
            ...tempPrivilege[idx],
            detail_permission: {
              Create: value.target.checked,
              Delete: value.target.checked,
              Read: value.target.checked,
              Update: value.target.checked,
              Import_Data: value.target.checked,
              Export_Data: value.target.checked,
              View_Report: value.target.checked,
              Create_Report: value.target.checked,
              Edit_Report: value.target.checked,
              Delete_Report: value.target.checked,
              View_ListView: value.target.checked,
              Create_ListView: value.target.checked,
              Edit_ListView: value.target.checked,
              Delete_ListView: value.target.checked,
            },
          };
        }
      });
    } else if (value.target.name.split("#")[0] === "Read") {
      tempPrivilege.forEach((item, idx) => {
        if (item.id_object === value.target.name.split("#")[1]) {
          tempPrivilege[idx] = {
            ...tempPrivilege[idx],
            detail_permission: {
              Read: value.target.checked,
              Create: false,
              Delete: false,
              Update: false,
              Import_Data: false,
              Export_Data: false,
              View_Report: false,
              Create_Report: false,
              Edit_Report: false,
              Delete_Report: false,
              View_ListView: false,
              Create_ListView: false,
              Edit_ListView: false,
              Delete_ListView: false,
            },
          };
        }
      });
    } else {
      tempPrivilege.forEach((item, idx) => {
        if (item.id_object === value.target.name.split("#")[1]) {
          tempPrivilege[idx] = {
            ...tempPrivilege[idx],
            detail_permission: {
              ...tempPrivilege[idx].detail_permission,
              [value.target.name.split("#")[0]]: value.target.checked,
            },
          };
        }
      });
    }

    setPrivilege(tempPrivilege);
  };

  const _onSubmit = () => {
    if (form.getFieldValue("profile_name") === "") {
      Notification("warning", "Please full field required!");
    } else {
      dispatch(
        updateProfile({
          ID: recordID,
          Description: form.getFieldValue("description") || "",
          Name: form.getFieldValue("profile_name").trim(),
          Profile_Obj: privilege,
        })
      );
    }
  };

  const _onCancel = () => {
    navigate("/profiles");
  };

  const columns = [
    {
      title: (
        <ObjectName>
          <Checkbox
            name="All"
            checked={privilegeAll.Read}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("object.objectName")}</span>
        </ObjectName>
      ),
      dataIndex: "object_name",
      key: "object_name",
      fixed: "left",
      render: (text, record) => (
        <ObjectNameDetail>
          <Checkbox
            name={`All#${record.key}`}
            checked={record.detail_permission.Read}
            onChange={onChangePrivilege}
          />
          <span>{record.object_name}</span>
        </ObjectNameDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Read"
            checked={privilegeAll.Read}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("common.view")}</span>
        </Role>
      ),
      dataIndex: "view",
      key: "view",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Read#${record.key}`}
            checked={record.detail_permission.Read}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Create"
            checked={privilegeAll.Create}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("common.create")}</span>
        </Role>
      ),
      dataIndex: "create",
      key: "create",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Create#${record.key}`}
            checked={record.detail_permission.Create}
            disabled={
              record.detail_permission.Read === false ||
              record.key === _.get(listSettingsOS, "[0].object_std")
            }
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Update"
            checked={privilegeAll.Update}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("common.edit")}</span>
        </Role>
      ),
      dataIndex: "edit",
      key: "edit",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Update#${record.key}`}
            checked={record.detail_permission.Update}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Delete"
            checked={privilegeAll.Delete}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("common.delete")}</span>
        </Role>
      ),
      dataIndex: "delete",
      key: "delete",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Delete#${record.key}`}
            checked={record.detail_permission.Delete}
            disabled={
              record.detail_permission.Read === false ||
              record.key === _.get(listSettingsOS, "[0].object_std")
            }
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Import_Data"
            checked={privilegeAll.Import_Data}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.importData")}</span>
        </Role>
      ),
      dataIndex: "Import_Data",
      key: "Import_Data",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Import_Data#${record.key}`}
            checked={record.detail_permission.Import_Data}
            disabled={
              record.detail_permission.Read === false ||
              record.key === _.get(listSettingsOS, "[0].object_std")
            }
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Export_Data"
            checked={privilegeAll.Export_Data}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.exportData")}</span>
        </Role>
      ),
      dataIndex: "Export_Data",
      key: "Export_Data",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Export_Data#${record.key}`}
            checked={record.detail_permission.Export_Data}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="View_Report"
            checked={privilegeAll.View_Report}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.viewReport")}</span>
        </Role>
      ),
      dataIndex: "View_Report",
      key: "View_Report",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`View_Report#${record.key}`}
            checked={record.detail_permission.View_Report}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Create_Report"
            checked={privilegeAll.Create_Report}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.createReport")}</span>
        </Role>
      ),
      dataIndex: "Create_Report",
      key: "Create_Report",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Create_Report#${record.key}`}
            checked={record.detail_permission.Create_Report}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Edit_Report"
            checked={privilegeAll.Edit_Report}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.editReport")}</span>
        </Role>
      ),
      dataIndex: "Edit_Report",
      key: "Edit_Report",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Edit_Report#${record.key}`}
            checked={record.detail_permission.Edit_Report}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Delete_Report"
            checked={privilegeAll.Delete_Report}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.deleteReport")}</span>
        </Role>
      ),
      dataIndex: "Delete_Report",
      key: "Delete_Report",
      width: 150,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Delete_Report#${record.key}`}
            checked={record.detail_permission.Delete_Report}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="View_ListView"
            checked={privilegeAll.View_ListView}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.viewListView")}</span>
        </Role>
      ),
      dataIndex: "View_ListView",
      key: "View_ListView",
      width: 200,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`View_ListView#${record.key}`}
            checked={record.detail_permission.View_ListView}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Create_ListView"
            checked={privilegeAll.Create_ListView}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.createListView")}</span>
        </Role>
      ),
      dataIndex: "Create_ListView",
      key: "Create_ListView",
      width: 200,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Create_ListView#${record.key}`}
            checked={record.detail_permission.Create_ListView}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Edit_ListView"
            checked={privilegeAll.Edit_ListView}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.editListView")}</span>
        </Role>
      ),
      dataIndex: "Edit_ListView",
      key: "Edit_ListView",
      width: 200,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Edit_ListView#${record.key}`}
            checked={record.detail_permission.Edit_ListView}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Role>
          <Checkbox
            name="Delete_ListView"
            checked={privilegeAll.Delete_ListView}
            disabled={disableAllPrivilege}
            onChange={onCheckAllPrivilege}
          />
          <span>{t("profile.deleteListView")}</span>
        </Role>
      ),
      dataIndex: "Delete_ListView",
      key: "Delete_ListView",
      width: 200,
      render: (text, record) => (
        <RoleDetail>
          <Checkbox
            name={`Delete_ListView#${record.key}`}
            checked={record.detail_permission.Delete_ListView}
            disabled={record.detail_permission.Read === false}
            onChange={onChangePrivilege}
          />
        </RoleDetail>
      ),
    },
    {
      title: (
        <Field>
          <span>{t("profile.fieldToolPrivilege")}</span>
        </Field>
      ),
      dataIndex: "fieldToolPrivilege",
      key: "fieldToolPrivilege",
      fixed: "right",
      width: 200,
      render: (text, record) => (
        <FieldDetail>
          <img onClick={() => _onEditField(record.key)} src={Edit} alt="edit" />
        </FieldDetail>
      ),
    },
  ];

  useEffect(() => {
    let check = false;
    privilege.forEach((item) => {
      if (item.detail_permission.Read === false) {
        setDisableAllPrivilege(true);
        check = true;
      }
    });
    if (check === false) {
      setDisableAllPrivilege(false);
    }

    let tempData = [];

    privilege.forEach((item) => {
      tempData.push({
        key: item.id_object,
        object_name: item.object_name,
        detail_permission: item.detail_permission,
        fieldToolPrivilege: "",
      });
    });

    $dataSource(tempData);
  }, [privilege]);

  useEffect(() => {
    dispatch(
      loadDetailProfile({
        _id: recordID,
      })
    );
  }, [dispatch, recordID]);

  useEffect(() => {
    dispatch(objectStandardGetList());
  }, [dispatch]);

  useEffect(() => {
    if (detailProfile !== null) {
      form.setFieldsValue({
        profile_name: detailProfile.Name,
        description: detailProfile.Description,
      });
      setPrivilege(detailProfile.Profile_Obj);
    }
  }, [detailProfile, form]);

  useEffect(() => {
    if (isLoading === false) {
      if (profileInfo === "success") {
        form.resetFields();

        Notification("success", "Update successfully!");
        dispatch(resetProfileInfo());
        navigate("/profiles");
      }

      if (profileInfo !== null && profileInfo !== "success") {
        Notification("error", profileInfo);
        dispatch(resetProfileInfo());
      }
    }
  }, [dispatch, form, isLoading, navigate, profileInfo]);

  useEffect(() => {
    return () => {
      dispatch(unMountEditProfile());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <Breadcrumb.Item onClick={() => navigate("/profiles")}>
          {t("settings.profiles")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("profile.editProfile")}</BreadcrumbItem>
      </Breadcrumb>

      {detailProfile !== null ? (
        <>
          <Content>
            <WrapInformation>
              <Information>
                <legend>{t("profile.editInformation")}</legend>

                <Form
                  form={form}
                  labelCol={{ span: 3 }}
                  wrapperCol={{ span: 5 }}
                  colon={false}
                  labelAlign="left"
                >
                  <Form.Item
                    label={t("profile.profileName")}
                    name="profile_name"
                    rules={[
                      {
                        required: true,
                        message: t("common.placeholderInput"),
                      },
                    ]}
                  >
                    <Input
                      placeholder={t("common.placeholderInput")}
                      maxLength={200}
                    />
                  </Form.Item>
                  <Form.Item label={t("common.description")} name="description">
                    <TextArea
                      rows={3}
                      placeholder={t("common.placeholderInput")}
                    />
                  </Form.Item>
                </Form>
              </Information>
            </WrapInformation>
          </Content>

          <Permission>
            <WrapPrivilege>
              <Privilege>
                <legend>{t("profile.editPrivilegeProfile")}</legend>
                <Table
                  dataSource={dataSource}
                  columns={columns}
                  scroll={{
                    x: "max-content",
                    y: "calc(100vh - 268px)",
                  }}
                  pagination={false}
                />
              </Privilege>
            </WrapPrivilege>
          </Permission>

          <WrapButton>
            <Button type="primary" loading={isLoading} onClick={_onSubmit}>
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </>
      ) : (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <span>
            {t("object.noObject")} {t("object.object")}
          </span>
        </Empty>
      )}

      <ModalEditFields
        showField={showField}
        onHideEditField={_onHideEditField}
        IDObject={IDObject}
      />
    </Wrapper>
  );
}

export default withTranslation()(EditProfile);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Content = styled.div`
  margin: 16px 0;
`;

const Permission = styled.div`
  margin: 16px 0;
  position: relative;
  max-height: calc(100vh - 150px);

  .ant-table-body {
    ::-webkit-scrollbar {
      width: 10px;
    }

    ::-webkit-scrollbar-thumb {
      background: #d3d3d3;
      border-radius: 20px;
    }
  }
`;

const WrapInformation = styled.div`
  padding: 24px;
  background-color: #fff;
`;

const Information = styled.fieldset`
  padding: 16px 16px 0 16px;
  border: 1px solid #ececec;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
  }
`;

const WrapPrivilege = styled.div`
  margin-top: 16px;
  padding: 24px;
  background-color: #fff;

  position: sticky;
  top: 0;
  height: 100%;

  .ant-table-thead > tr > th {
    padding: 0;
  }

  .ant-table-tbody > tr > td {
    padding: 0;
  }
`;

const Privilege = styled(Information)`
  padding-top: 0;
  position: relative;
  height: 100%;
`;

const ObjectName = styled.div`
  flex: 4;
  padding: 8px 8px;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const ObjectNameDetail = styled(ObjectName)`
  border-right: none;
`;

const Role = styled.div`
  flex: 1;
  padding: 8px 0 8px 8px;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const RoleDetail = styled(Role)`
  border-right: none;
`;

const Field = styled.div`
  flex: 3;
  padding: 8px 0 8px 16px;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;
`;

const FieldDetail = styled(Field)`
  img {
    cursor: pointer;
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-right: 71px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;

  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;
