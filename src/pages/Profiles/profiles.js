import React, { useEffect, useState, useCallback } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Button from "antd/lib/button";
import Pagination from "antd/lib/pagination";
import Table from "antd/lib/table";
import Breadcrumb from "antd/lib/breadcrumb";
import Tooltip from "antd/lib/tooltip";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import Permission from "assets/icons/users/Permission.svg";

import { loadProfiles } from "redux/slices/profile";

import ModalAddProfile from "./ModalAddProfile";
import ModalDelete from "./ModalDelete";
import { changeTitlePage } from "redux/slices/authenticated";

function Profiles(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;

  const { listProfile, totalRecords } = useSelector(
    (state) => state.profileReducer
  );
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  const [showModalAdd, setShowModalAdd] = useState(false);

  const [showModalDelete, setShowModalDelete] = useState(false);

  const [tableProfile, setTableProfile] = useState([]);

  const [profileID, setProfileID] = useState("");

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.profiles")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "profile" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onShowModalAdd = () => {
    setShowModalAdd(true);
  };

  const _onHideModalAdd = () => {
    setShowModalAdd(false);
  };

  const _onShowModalDelete = (id) => {
    setShowModalDelete(true);
    setProfileID(id);
  };

  const _onHideModalDelete = () => {
    setShowModalDelete(false);
  };

  const showTotal = () => {
    return `${t("common.total")} ${totalRecords} ${t("common.items")}`;
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  const _onEditProfile = (id) => {
    navigate(`/edit-profile/${id}`);
  };

  const getListProfiles = useCallback(() => {
    dispatch(
      loadProfiles({
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [currentPage, dispatch, recordPerPage]);

  useEffect(() => {
    getListProfiles();
  }, [getListProfiles]);

  useEffect(() => {
    let tempList = [];

    listProfile.map((item) => {
      return tempList.push({
        key: item._id,
        name: item.Name,
        description: item.Description || "",
        action: "",
      });
    });
    setTableProfile(tempList);
  }, [listProfile]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.profiles")}</BreadcrumbItem>
        </Breadcrumb>
        {checkRule("create") && (
          <AddButton onClick={_onShowModalAdd}>
            + {t("profile.addProfile")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      <WrapTable>
        <Table pagination={false} dataSource={tableProfile}>
          <Column
            title={t("profile.profileName")}
            dataIndex="name"
            key="name"
            sorter={(a, b) => a.name.localeCompare(b.name)}
          />
          <Column
            title={t("common.description")}
            dataIndex="description"
            key="description"
            sorter={(a, b) => a.description.localeCompare(b.description)}
          />

          {(checkRule("edit") || checkRule("delete")) && (
            <Column
              title={t("common.action")}
              dataIndex="action"
              key="action"
              render={(text, record) => (
                <WrapAction>
                  {checkRule("edit") && (
                    <Tooltip title={t("common.edit")}>
                      <img
                        onClick={() => _onEditProfile(record.key)}
                        src={Edit}
                        alt="edit"
                      />
                    </Tooltip>
                  )}
                  {checkRule("delete") && (
                    <Tooltip title={t("common.delete")}>
                      <img
                        onClick={() => _onShowModalDelete(record.key)}
                        src={Delete}
                        alt="delete"
                      />
                    </Tooltip>
                  )}

                  {checkRule("edit") && (
                    <Tooltip title="Permission">
                      <img
                        style={{ width: "22px" }}
                        onClick={() => {
                          localStorage.setItem(
                            "nameProfilePermission",
                            record.name
                          );
                          navigate(`/profile-permission/${record.key}`);
                        }}
                        src={Permission}
                        alt="permission"
                      />
                    </Tooltip>
                  )}
                </WrapAction>
              )}
            />
          )}
        </Table>
        <Pagination
          showTotal={showTotal}
          current={currentPage}
          pageSize={recordPerPage}
          total={totalRecords}
          onChange={handleSelectPage}
          showSizeChanger
          onShowSizeChange={_onSizeChange}
          showQuickJumper
        />
      </WrapTable>

      <ModalAddProfile
        showModalAdd={showModalAdd}
        getListProfiles={getListProfiles}
        _onHideModalAdd={_onHideModalAdd}
      />

      <ModalDelete
        showModalDelete={showModalDelete}
        setShowModalDelete={setShowModalDelete}
        _onHideModalDelete={_onHideModalDelete}
        getListProfiles={getListProfiles}
        profileID={profileID}
      />
    </Wrapper>
  );
}

export default withTranslation()(Profiles);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-cell {
    :first-child {
      width: 50%;
    }

    :nth-child(2) {
      width: 30%;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
