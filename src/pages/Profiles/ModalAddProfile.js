import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";

import { Notification } from "components/Notification/Noti";
import {
  createProfile,
  resetProfileInfo,
  loadAllProfiles,
} from "../../redux/slices/profile";

function ModalAddProfile({ showModalAdd, _onHideModalAdd, getListProfiles }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isLoading, profileInfo } = useSelector(
    (state) => state.profileReducer
  );

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const _onSubmit = (values) => {
    dispatch(
      createProfile({
        Description: values.description,
        Name: values.profile_name.trim(),
      })
    );
  };

  const _onCancel = () => {
    _onHideModalAdd();
    form.resetFields();
    dispatch(resetProfileInfo());
  };

  useEffect(() => {
    if (isLoading === false) {
      if (profileInfo === "success") {
        _onHideModalAdd();
        form.resetFields();
        getListProfiles();

        dispatch(
          loadAllProfiles({
            current_page: 1,
            record_per_page: 10000,
          })
        );

        Notification("success", "Create successfully!");
        dispatch(resetProfileInfo());
      }

      if (profileInfo !== null && profileInfo !== "success") {
        Notification("error", profileInfo);
        dispatch(resetProfileInfo());
      }
    }
  }, [
    _onHideModalAdd,
    dispatch,
    form,
    getListProfiles,
    isLoading,
    profileInfo,
  ]);

  return (
    <ModalCustom
      title={t("profile.addProfile")}
      visible={showModalAdd}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("profile.profileName")}
          name="profile_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} maxLength={200} />
        </Form.Item>

        <Form.Item label={t("common.description")} name="description">
          <TextArea rows={6} placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalAddProfile);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
