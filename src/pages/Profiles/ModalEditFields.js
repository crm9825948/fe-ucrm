import React, { useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Radio from "antd/lib/radio";
import Space from "antd/lib/space";

import { Notification } from "components/Notification/Noti";
import {
  loadProfileFields,
  resetProfileFields,
  updateProfileFields,
  resetFieldsInfo,
} from "redux/slices/profile";

function ModalEditFields({ showField, onHideEditField, IDObject }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { recordID } = useParams();

  const { isLoadingFields, isLoadingUpdate, profileFields, fieldsInfo } =
    useSelector((state) => state.profileReducer);

  const [form] = Form.useForm();

  const _onSubmit = (values) => {
    let profile_fields = [];
    Object.entries(values).forEach(([key, val]) => {
      profileFields.forEach((item) => {
        if (item.field_id === key) {
          let temp = { ...item };
          if (val === "write") {
            temp.write = true;
            temp.read = false;
            temp.hidden = false;
          } else if (val === "read") {
            temp.write = false;
            temp.read = true;
            temp.hidden = false;
          } else {
            temp.write = false;
            temp.read = false;
            temp.hidden = true;
          }
          profile_fields.push(temp);
        }
      });
    });
    dispatch(
      updateProfileFields({
        profile_fields,
      })
    );
  };

  const _onCancel = () => {
    onHideEditField();
    form.resetFields();
    dispatch(resetProfileFields());
  };

  const assignOptions = [
    {
      value: "write",
    },
    {
      value: "read",
    },
    {
      value: "hidden",
    },
  ];

  useEffect(() => {
    if (showField) {
      dispatch(
        loadProfileFields({
          object_id: IDObject,
          profile_id: recordID,
        })
      );
    }
  }, [IDObject, dispatch, showField, recordID]);

  useEffect(() => {
    if (isLoadingUpdate === false) {
      if (fieldsInfo === "success") {
        onHideEditField();
        form.resetFields();

        Notification("success", "Update successfully!");
        dispatch(resetFieldsInfo());
      }

      if (fieldsInfo !== null && fieldsInfo !== "success") {
        Notification("error", fieldsInfo);
        dispatch(resetFieldsInfo());
      }
    }
  }, [onHideEditField, dispatch, form, isLoadingUpdate, fieldsInfo]);

  useEffect(() => {
    if (profileFields !== null) {
      profileFields.map((item) => {
        return form.setFieldsValue({
          [item.field_id]:
            item.write === true
              ? "write"
              : item.read === true
              ? "read"
              : "hidden",
        });
      });
    }
  }, [form, profileFields]);

  return (
    <>
      {isLoadingFields ? (
        <p>hihi</p>
      ) : (
        <ModalCustom
          title={t("profile.editFieldToolPrivilege")}
          visible={showField}
          footer={null}
          width={800}
          onCancel={_onCancel}
        >
          <Form
            form={form}
            onFinish={_onSubmit}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            colon={false}
            labelAlign="left"
          >
            <Wrap>
              <div
                style={{
                  background: "#fff",
                  zIndex: 2,
                  height: 24,
                  width: "749px",
                  position: "sticky",
                  top: 0,
                }}
              />
              <Header label=" ">
                <Title>{t("common.write")}</Title>
                <Title>{t("common.readOnly")}</Title>
                <Title>{t("common.invisible")}</Title>
              </Header>
              {profileFields !== null && (
                <>
                  {profileFields.map((item, idx) => {
                    return (
                      <FormCustom
                        label={`${item.name}`}
                        name={item.field_id}
                        key={idx}
                      >
                        <Radio.Group>
                          <Space direction="horizontal">
                            {assignOptions.map((option) => {
                              return (
                                <Radio
                                  key={option.value}
                                  value={option.value}
                                  disabled={
                                    item.type === "id" || item.required === true
                                  }
                                />
                              );
                            })}
                          </Space>
                        </Radio.Group>
                      </FormCustom>
                    );
                  })}
                </>
              )}
            </Wrap>
            <WrapButton label=" ">
              <Button
                loading={isLoadingUpdate}
                type="primary"
                htmlType="submit"
              >
                {t("common.save")}
              </Button>
              <Button onClick={_onCancel}>{t("common.cancel")}</Button>
            </WrapButton>
          </Form>
        </ModalCustom>
      )}
    </>
  );
}

export default withTranslation()(ModalEditFields);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-modal-body {
    max-height: 700px;
    overflow: auto;
    padding: 0;
  }
`;

const Wrap = styled.div`
  padding: 0 24px 24px 24px;
  position: relative;
`;

const Header = styled(Form.Item)`
  position: sticky;
  top: 24px;
  z-index: 2;
  display: flex;
  background: #f0f0f0;
  height: 40px;
  border: 1px solid #d9d9d9;
  margin-bottom: 0;

  .ant-form-item-control-input {
    height: 100%;
  }

  .ant-form-item-control-input-content {
    display: flex;
    height: 100%;
  }

  .ant-row {
    width: 100%;
  }
`;

const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex: 1;
  border-right: 1px solid #d9d9d9;
  height: 100%;

  :first-child {
    border-left: 1px solid #d9d9d9;
  }

  :last-child {
    border-right: none;
  }
`;

const FormCustom = styled(Form.Item)`
  border: 1px solid #ededed;
  border-top: none;
  margin-bottom: 0;

  .ant-form-item-label > label {
    padding-left: 16px;
  }

  .ant-radio-group {
    width: 100%;
  }

  .ant-space {
    width: 100%;
    justify-content: center;
  }

  .ant-space-item {
    flex: 1;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  /* .ant-radio-disabled  */
  .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  position: sticky;
  bottom: 0;
  z-index: 2;
  background: #fff;
  height: 104px;
  display: flex;
  justify-content: flex-end;
  box-shadow: 0px -2px 17px rgba(0, 0, 0, 0.16);
  padding-right: 24px;

  .ant-form-item-control-input {
    height: 100%;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
