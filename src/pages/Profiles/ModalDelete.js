import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";

import Tranfer from "assets/icons/roles/Transfer.jpg";

import { Notification } from "components/Notification/Noti";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deleteProfile,
  resetStatusDelete,
  loadAllProfiles,
} from "redux/slices/profile";

function ModalDelete({
  showModalDelete,
  _onHideModalDelete,
  profileID,
  getListProfiles,
  setShowModalDelete,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isLoading, statusDelete, allProfile } = useSelector(
    (state) => state.profileReducer
  );

  const [form] = Form.useForm();

  const [dataDelete, setDataDelete] = useState({});

  const [listTransfer, setListTransfer] = useState([]);

  const _onSubmit = (values) => {
    dispatch(setShowModalConfirmDelete(true));
    _onHideModalDelete();
    let tempData = {
      ID: profileID,
      Profile_Switch: values.transfer,
    };
    setDataDelete(tempData);
  };

  const _onCancel = () => {
    _onHideModalDelete();
    form.resetFields();
  };

  useEffect(() => {
    let tempList = [];
    if (allProfile !== null) {
      allProfile.forEach((item) => {
        if (item._id !== profileID) {
          tempList.push({
            label: item.Name,
            value: item._id,
          });
        }
      });
    }
    setListTransfer(tempList);
  }, [allProfile, profileID]);

  useEffect(() => {
    if (showModalDelete && allProfile === null) {
      dispatch(
        loadAllProfiles({
          current_page: 1,
          record_per_page: 10000,
        })
      );
    }
  }, [dispatch, showModalDelete, allProfile]);

  useEffect(() => {
    if (statusDelete === "success") {
      Notification("success", "Delete successfully!");
      form.resetFields();
      getListProfiles();
      dispatch(
        loadAllProfiles({
          current_page: 1,
          record_per_page: 10000,
        })
      );
      _onHideModalDelete();
      dispatch(setShowModalConfirmDelete(false));

      dispatch(resetStatusDelete());
    }

    if (statusDelete !== "success" && statusDelete !== null) {
      Notification("error", statusDelete);

      dispatch(resetStatusDelete());
    }
  }, [_onHideModalDelete, dispatch, form, getListProfiles, statusDelete]);

  return (
    <>
      <ModalCustom
        title={t("profile.deleteProfile")}
        visible={showModalDelete}
        footer={null}
        width={450}
        onCancel={_onCancel}
      >
        <WrapNote>
          <img src={Tranfer} alt="transfer" />
          <Title>{t("user.transferData")}</Title>
          <SubTitle>{t("user.descriptionTransfer")}</SubTitle>
        </WrapNote>
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 12 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("user.transferDataTo")}
            name="transfer"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              options={listTransfer}
              placeholder={t("common.placeholderSelect")}
              loading={isLoading}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <WrapButton label=" ">
            <Button type="primary" htmlType="submit">
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      </ModalCustom>

      <ModalConfirmDelete
        setShowModalDelete={setShowModalDelete}
        title={t("profile.profile")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteProfile}
        dataDelete={dataDelete}
        isLoading={isLoading}
      />
    </>
  );
}

export default withTranslation()(ModalDelete);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    font-family: var(--roboto-700);
    color: #2c2c2c;
  }
`;

const WrapNote = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  img {
    margin-bottom: 24px;
  }
`;

const Title = styled.p`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
`;

const SubTitle = styled.p`
  font-size: 16px;
  color: #2c2c2c;
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
