import styled from "styled-components/macro";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import _ from "lodash";

import Button from "antd/lib/button";
import Dropdown from "antd/lib/dropdown";
import Menu from "antd/lib/menu";
import { MoreOutlined } from "@ant-design/icons";

import Excel from "assets/icons/report/Excel.svg";
import FilterIcon from "assets/icons/report/Filter.svg";
import PinGreen from "assets/icons/report/PinGreen.svg";
import EditGreen from "assets/icons/common/EditGreen.svg";
import ShareGreen from "assets/icons/common/ShareGreen.svg";
import DeleteWidget from "assets/icons/report/DeleteWidget1.svg";
import EmptyDashboard from "assets/images/reports/empty-dashboard.jpg";
// import RefreshIcon from "assets/icons/report/refresh.svg";
import QuickFilter from "./QuickFilter";

import { optionsTimeRangeReport2 } from "util/staticData";
import { useTranslation, withTranslation } from "react-i18next";

function DescriptionReport({
  infoReport,
  showFilter,
  _onShowFilter,
  _onCloseFilter,
  _onFilter,
  allCondition,
  setAllCondition,
  anyCondition,
  setAnyCondition,
  _onShowModalChart,
  _onShowModalWidget,
  _onShowReportSetting,
  _onPinReport,
  _onShowModalExport,
  _onShowModalShare,
  operatorValueAnd,
  setOperatorValueAnd,
  valueAnd,
  setValueAnd,
  operatorValueOr,
  setOperatorValueOr,
  valueOr,
  setValueOr,
  readonly,
  listObjectField,
}) {
  const { isLoadingPin } = useSelector((state) => state.reportDetailsReducer);
  const { reportPermission } = useSelector((state) => state.userReducer);
  const { t } = useTranslation();

  const { pathname } = useLocation();

  const [listConditionsAnd, setListConditionsAnd] = useState([]);
  const [listConditionsAny, setListConditionsAny] = useState([]);
  const [showAction, $showAction] = useState(false);
  const [listFieldsMain, setListFieldsMain] = useState([]);

  const checkReportPermission = (type) => {
    return _.get(
      reportPermission,
      `${_.get(infoReport, "report_info.main_object", "")}.${type}`,
      false
    );
  };

  const DropDownAdd = (
    <Menu>
      <Menu.Item onClick={_onShowModalChart}>
        <span>Add a new chart</span>
      </Menu.Item>
      <Menu.Item onClick={_onShowModalWidget}>
        <span>Add a new widget</span>
      </Menu.Item>
    </Menu>
  );

  const menuAction = () => {
    return (
      <WrapAction>
        <ActionReport onClick={_onPinReport}>
          <img src={DeleteWidget} alt="pin" />
          <span>Unpin</span>
        </ActionReport>
      </WrapAction>
    );
  };

  const showCondition = (conditions, fields) => {
    let listCondition = [];

    conditions.forEach((item, idx) => {
      Object.entries(item).forEach(([key, val]) => {
        if (key === "id_field") {
          let found = fields.find((ele) => ele.value === val);
          if (found) {
            listCondition.push({
              name: found.label,
              decimal_separator: _.get(found, "decimal_separator", 3),
            });
          } else {
            listCondition.push({
              name: val,
            });
          }
        }
        if (key === "type") {
          let tempType = {
            type: val,
          };
          listCondition[idx] = {
            ...listCondition[idx],
            ...tempType,
          };
        }
        if (key === "value") {
          let tempObj = {};

          if (Object.keys(val)[0] === "$not") {
            tempObj = {
              equal: "$not",
              condition: val.$not.$regex,
            };
          } else if (Object.keys(val).length > 2) {
            tempObj = {
              equal: Object.keys(val),
              condition: Object.values(val),
            };
          } else {
            tempObj = {
              equal: Object.keys(val)[0],
              condition: Object.values(val)[0],
            };
          }

          listCondition[idx] = {
            ...listCondition[idx],
            ...tempObj,
          };
        }
      });
    });

    listCondition.forEach((item, idx) => {
      if (item.status === "owner") {
        listCondition[idx] = {
          ...listCondition[idx],
          status: "Assign to",
        };
      }

      if (item.condition === null) {
        if (item.equal === "$eq") {
          listCondition[idx] = {
            ...listCondition[idx],
            equal: "Is Empty",
          };
        } else {
          listCondition[idx] = {
            ...listCondition[idx],
            equal: "Is not empty",
          };
        }
      } else if (Array.isArray(item.equal)) {
        listCondition[idx] = {
          ...listCondition[idx],
          equal: "Between",
          condition: item.condition[0] + " - " + item.condition[1],
        };
      } else if (item.condition === "mine") {
        listCondition[idx] = {
          ...listCondition[idx],
          equal: item.equal === "$eq" ? "Mine" : "Not mine",
          condition: null,
        };
      } else {
        switch (item.equal) {
          case "$eq":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Equals",
            };
            break;
          case "$ne":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Not Equals",
            };
            break;
          case "$lt":
            if (item.type === "date" || item.type === "datetime-local") {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Before",
              };
            } else {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Less than",
              };
            }
            break;
          case "$lte":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Less than or equal",
            };
            break;
          case "$gt":
            if (item.type === "date" || item.type === "datetime-local") {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "After",
              };
            } else {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Greater than",
              };
            }
            break;
          case "$gte":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Greater than or equal",
            };
            break;
          case "$regex":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Contains",
            };
            break;
          case "$not":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Not Contains",
            };
            break;

          default:
            break;
        }
      }
    });

    return listCondition;
  };

  function numberWithCommas(number, decimal_separator) {
    if (number === 0) {
      return "0";
    } else
      return number
        .toFixed(decimal_separator)
        .toString()
        .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  }

  useEffect(() => {
    if (
      Object.keys(infoReport).length > 0 &&
      (infoReport.report_info.filter_condition.and_filter.length > 0 ||
        infoReport.report_info.filter_condition.or_filter.length > 0)
    ) {
      let tempFields = [];

      listObjectField.forEach((item) => {
        if (
          Object.values(item)[0] !== null &&
          (Object.values(item)[0].readable || Object.values(item)[0].writeable)
        ) {
          let temp = [];
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                temp.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  decimal_separator: _.get(field, "decimal_separator", 3),
                });
              }
            });
          });
          if (temp.length > 0) {
            tempFields = [...tempFields, ...temp];
          }
        }
      });

      if (
        infoReport.report_info.filter_condition.and_filter.length > 0 &&
        tempFields.length > 0
      ) {
        let temp = [];
        temp = showCondition(
          infoReport.report_info.filter_condition.and_filter,
          tempFields
        );
        setListConditionsAnd(temp);
      }

      if (infoReport.report_info.filter_condition.or_filter.length > 0) {
        let temp = [];
        temp = showCondition(
          infoReport.report_info.filter_condition.or_filter,
          tempFields
        );
        setListConditionsAny(temp);
      }
    }
  }, [infoReport, listObjectField]);

  useEffect(() => {
    let tempFieldsMain = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempFieldsMain.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  is_indexed: _.get(field, "is_indexed", false),
                  formula_type:
                    field.type !== "formula"
                      ? ""
                      : field?.formula_type === "dateDiff"
                      ? !field?.in_words
                        ? field?.formula_type
                        : ""
                      : field?.formula_type,
                });
              }
            });
          });
        }
      }
    });
    setListFieldsMain(tempFieldsMain);
  }, [listObjectField]);

  return (
    <>
      {Object.keys(infoReport).length > 0 ? (
        <>
          <Description>
            <WrapTitle>
              <Title>{infoReport.report_info?.report_name}</Title>
              {!pathname.split("/").includes("dashboard") ? (
                <Action>
                  <Filter onClick={_onShowFilter}>
                    <img src={FilterIcon} alt="filter" />
                    <span>Filter</span>
                  </Filter>
                  {!infoReport.report_info?.readonly && (
                    <Dropdown
                      placement="bottomCenter"
                      trigger="click"
                      overlay={DropDownAdd}
                    >
                      <AddNew>
                        <p>+</p>
                        <span>Add new</span>
                      </AddNew>
                    </Dropdown>
                  )}
                  {!infoReport.report_info?.readonly && (
                    <Edit
                      onClick={_onShowReportSetting}
                      disabled={!checkReportPermission("Edit_Report")}
                    >
                      <img src={EditGreen} alt="edit" />
                      <span>Edit</span>
                    </Edit>
                  )}
                  <Pin
                    loading={isLoadingPin}
                    ispin={infoReport.report_info?.is_pinned_to_dashboard}
                    onClick={_onPinReport}
                  >
                    <img src={PinGreen} alt="pin" />
                    <span>Pin</span>
                  </Pin>
                  <Export
                    onClick={_onShowModalExport}
                    disabled={!checkReportPermission("Export_Data")}
                  >
                    <img src={Excel} alt="export" />
                    <span>Export</span>
                  </Export>
                  {!infoReport.report_info?.readonly && (
                    <Share
                      onClick={_onShowModalShare}
                      disabled={!checkReportPermission("Edit_Report")}
                    >
                      <img src={ShareGreen} alt="share" />
                      <span>Share</span>
                    </Share>
                  )}
                </Action>
              ) : (
                <>
                  {!readonly && (
                    <Dropdown
                      visible={showAction}
                      onVisibleChange={() => $showAction(!showAction)}
                      overlay={menuAction}
                      placement="bottomCenter"
                      trigger={["click"]}
                    >
                      <MoreOutlined
                        style={{
                          color: "#bbbbbb",
                          fontSize: 24,
                        }}
                      />
                    </Dropdown>
                  )}
                </>
              )}
            </WrapTitle>

            <WrapDes>
              <Des>
                <Desc>
                  <p>Report description</p>
                  <span>{infoReport.report_info?.description}</span>
                </Desc>

                <Desc>
                  <p>Report type</p>
                  <span style={{ textTransform: "capitalize" }}>
                    {infoReport.report_info?.report_type === "custom_report"
                      ? "Custom report"
                      : infoReport.report_info?.report_type ===
                        "row_summary_report"
                      ? "Row summary report"
                      : infoReport.report_info?.report_type}
                  </span>
                </Desc>

                <Desc>
                  <p>Folder name</p>
                  <span>{infoReport.report_info?.folder_name}</span>
                </Desc>
                <Desc>
                  <p>Folder description</p>
                  <span>{infoReport.report_info?.folder_description}</span>
                </Desc>
                <Desc>
                  <p>Main object</p>
                  <span>{infoReport.report_info?.main_object_name}</span>
                </Desc>
              </Des>

              <ListCondition>
                <TitleCondition>Time range</TitleCondition>

                <TimeRange>
                  <p>
                    {_.get(
                      listFieldsMain.find(
                        (item) =>
                          item.value ===
                          _.get(
                            infoReport,
                            "report_info.time_range_filter.filter_field_id",
                            ""
                          )
                      ),
                      "label",
                      ""
                    )}
                  </p>

                  {_.get(
                    infoReport,
                    "report_info.time_range_filter.filter_type",
                    ""
                  ) !== "custom" && (
                    <>
                      {_.get(
                        infoReport,
                        "report_info.time_range_filter.filter_type",
                        ""
                      ) === "last-n-days" ? (
                        <span>
                          Last{" "}
                          {_.get(
                            infoReport,
                            "report_info.time_range_filter.amount_of_time",
                            ""
                          )}{" "}
                          days
                        </span>
                      ) : _.get(
                          infoReport,
                          "report_info.time_range_filter.filter_type",
                          ""
                        ) === "last-n-weeks" ? (
                        <span>
                          Last{" "}
                          {_.get(
                            infoReport,
                            "report_info.time_range_filter.amount_of_time",
                            ""
                          )}{" "}
                          weeks
                        </span>
                      ) : _.get(
                          infoReport,
                          "report_info.time_range_filter.filter_type",
                          ""
                        ) === "last-n-months" ? (
                        <span>
                          Last{" "}
                          {_.get(
                            infoReport,
                            "report_info.time_range_filter.amount_of_time",
                            ""
                          )}{" "}
                          months
                        </span>
                      ) : _.get(
                          infoReport,
                          "report_info.time_range_filter.filter_type",
                          ""
                        ) === "last-n-years" ? (
                        <span>
                          Last{" "}
                          {_.get(
                            infoReport,
                            "report_info.time_range_filter.amount_of_time",
                            ""
                          )}{" "}
                          years
                        </span>
                      ) : (
                        <span>
                          {_.get(
                            optionsTimeRangeReport2.find(
                              (item) =>
                                item.value ===
                                _.get(
                                  infoReport,
                                  "report_info.time_range_filter.filter_type",
                                  ""
                                )
                            ),
                            "label",
                            ""
                          )}
                        </span>
                      )}
                    </>
                  )}

                  {_.get(
                    infoReport,
                    "report_info.time_range_filter.start_time",
                    ""
                  ) && (
                    <>
                      <span>
                        {t("emailIncoming.from")}:{" "}
                        {_.get(
                          infoReport,
                          "report_info.time_range_filter.start_time",
                          ""
                        )}
                      </span>
                    </>
                  )}

                  {_.get(
                    infoReport,
                    "report_info.time_range_filter.end_time",
                    ""
                  ) && (
                    <span>
                      {t("emailIncoming.to")}:{" "}
                      {_.get(
                        infoReport,
                        "report_info.time_range_filter.end_time",
                        ""
                      )}
                    </span>
                  )}
                </TimeRange>

                {listConditionsAnd.length > 0 ? (
                  <>
                    <TitleCondition>
                      And Conditions (All conditions will be met)
                    </TitleCondition>

                    {listConditionsAnd.map((item) => {
                      return (
                        <Condition>
                          <p>{item.name}</p>
                          <span>{item.equal}</span>
                          <span>
                            {typeof item.condition === "number"
                              ? numberWithCommas(
                                  item.condition,
                                  item.decimal_separator
                                )
                              : item.condition}
                          </span>
                        </Condition>
                      );
                    })}
                  </>
                ) : (
                  <>
                    <TitleCondition>
                      And Conditions (All conditions will be met)
                    </TitleCondition>
                    <p>No conditions</p>
                  </>
                )}

                {listConditionsAny.length > 0 ? (
                  <>
                    <TitleCondition>
                      Any Conditions (Any conditions will be met)
                    </TitleCondition>

                    {listConditionsAny.map((item) => {
                      return (
                        <Condition>
                          <p>{item.name}</p>
                          <span>{item.equal}</span>
                          <span>
                            {typeof item.condition === "number"
                              ? numberWithCommas(
                                  item.condition,
                                  item.decimal_separator
                                )
                              : item.condition}
                          </span>
                        </Condition>
                      );
                    })}
                  </>
                ) : (
                  <>
                    <TitleCondition>
                      Any Conditions (Any conditions will be met)
                    </TitleCondition>
                    <p>No conditions</p>
                  </>
                )}
              </ListCondition>
            </WrapDes>
          </Description>

          <QuickFilter
            onClose={_onCloseFilter}
            showFilter={showFilter}
            onFilter={_onFilter}
            onCloseFilter={_onCloseFilter}
            allCondition={allCondition}
            setAllCondition={setAllCondition}
            operatorValueAnd={operatorValueAnd}
            setOperatorValueAnd={setOperatorValueAnd}
            valueAnd={valueAnd}
            setValueAnd={setValueAnd}
            anyCondition={anyCondition}
            setAnyCondition={setAnyCondition}
            operatorValueOr={operatorValueOr}
            setOperatorValueOr={setOperatorValueOr}
            valueOr={valueOr}
            setValueOr={setValueOr}
          />
        </>
      ) : (
        <>
          <ImageEmpty onClick={_onPinReport} src={DeleteWidget} alt="remove" />

          <NotFound>
            <img src={EmptyDashboard} alt="empty" />
            <span>Report Not Found</span>
          </NotFound>
        </>
      )}
    </>
  );
}

export default withTranslation()(DescriptionReport);

DescriptionReport.defaultProps = {
  readonly: false,
};

const Description = styled.div`
  margin-top: 16px;
  padding: 24px;
  background: #fff;
`;

const WrapTitle = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #ececec;
  padding-bottom: 16px;
  margin-bottom: 20px;
`;

const Title = styled.div`
  font-family: var(--roboto-500);
  font-size: 24px;
  color: #252424;
`;

const WrapAction = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  padding: 5px 0;
`;

const ActionReport = styled.div`
  cursor: pointer;
  width: 121px;
  height: 32px;
  display: flex;
  align-items: center;
  padding-left: 13px;

  img {
    margin-right: 9px;
  }

  span {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  :hover {
    background: #f5f5f5;
  }
`;

const Action = styled.div`
  display: flex;
  cursor: pointer;
`;

const Filter = styled(Button)`
  width: 96px;
  height: 40px;
  margin-right: 10px;

  :hover {
    background: ${(props) => props.theme.darker};
    border: 1px solid ${(props) => props.theme.darker};
    color: #fff;

    img {
      filter: brightness(200);
    }
  }

  span {
    font-size: 16px;
  }

  img {
    margin-right: 8px;
  }
`;

const AddNew = styled(Button)`
  display: flex;
  align-items: center;
  width: 121px;
  height: 40px;
  margin-right: 10px;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main}!important;
  color: #fff !important;
  font-size: 16px;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }

  :active {
    background: ${(props) => props.theme.main};
  }

  :focus {
    background: ${(props) => props.theme.main};
  }

  p {
    margin-bottom: 0;
    margin-right: 8px;
  }
`;

const Edit = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 84px;
  height: 40px;
  color: ${(props) => props.theme.main};
  font-size: 16px;
  border: 1px solid ${(props) => props.theme.main};
  margin-right: 10px;

  &.ant-btn:active {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    color: ${(props) => props.theme.main};
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    color: ${(props) => props.theme.main};
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border: 1px solid ${(props) => props.theme.darker} !important;
    color: #fff !important;

    img {
      filter: brightness(200);
    }
  }

  img {
    margin-right: 8px;
  }

  span {
    margin-bottom: 2px;
  }
`;

const Pin = styled(Edit)`
  width: 79px;
  background: ${(props) => props.ispin && "#2AD181"}!important;
  color: ${(props) => props.ispin && "#fff"}!important;
  border-color: ${(props) => props.ispin && "#2AD181"}!important;

  img {
    filter: ${(props) => props.ispin && "brightness(200)"}!important;
  }
`;

// const UnpinDashBoard = styled(Pin)`
//   width: 90px;
// `;

const Export = styled(Edit)`
  width: 103px;

  :hover {
    img {
      filter: none;
    }
  }
`;

const Share = styled(Edit)`
  width: 97px;
  margin-right: 0;
`;

// const Refresh = styled(Edit)``;

const Desc = styled.div`
  display: flex;
  font-size: 16px;
  margin-bottom: 8px;

  :last-child {
    margin-bottom: 0;
  }

  p {
    font-family: var(--roboto-500);
    color: #252424;
    margin-bottom: 0;
    width: 20%;
  }

  span {
    color: #2c2c2c;
    width: 80%;
  }
`;

const ImageEmpty = styled.img`
  float: right;
  cursor: pointer;
`;

const NotFound = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;

  span {
    margin-top: 8px;
  }

  img {
    width: fit-content;
  }
`;

const TitleCondition = styled.div`
  color: #252424;
  font-family: var(--roboto-500);
  font-size: 16px;
`;

const TimeRange = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    width: 30%;
    color: #252424;
    font-family: var(--roboto-500);
    font-size: 16px;
    margin-bottom: 0;
    padding-right: 16px;
  }

  span {
    /* width: 30%; */
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-400);
    padding-right: 16px;
  }
`;

const WrapDes = styled.div`
  display: flex;
  justify-content: center;
`;

const Des = styled.div`
  width: 50%;
`;

const ListCondition = styled.div`
  width: 50%;
  max-height: 180px;
  overflow: auto;

  ::-webkit-scrollbar {
    width: 11px !important;
  }
`;

const Condition = styled.div`
  display: flex;
  align-items: center;
  p {
    width: 30%;
    color: #252424;
    font-family: var(--roboto-500);
    font-size: 16px;
    margin-bottom: 0;
    padding-right: 16px;
  }

  span {
    width: 30%;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-400);
    padding-right: 16px;
  }
`;
