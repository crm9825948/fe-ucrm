import Spin from "antd/lib/spin";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import styled from "styled-components/macro";
import _ from "lodash";

import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";

import { loadTotalRecords } from "redux/slices/reportDetails";

function DataReport({
  loadData,
  setCurrentPage,
  setRecordPerPage,
  currentPage,
  recordPerPage,
  infoReport,
  columnReport,
  setColumnReport,
  dataReportDashboard,
  arithmeticReportData,
  objectID,
  recordID,
  // setFirstID,
  // setLastID,
  // next,
  // setNext,
  // prev,
  // setPrev,
  loadReport,
  // firstID,
  // lastID,
  tempAllCondition,
  tempAnyCondition,
}) {
  const [next, setNext] = useState(0);
  const [prev, setPrev] = useState(0);
  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);

  const { pathname } = useLocation();
  const dispatch = useDispatch();
  const { Column } = Table;
  const { Text } = Typography;
  const {
    dataReport,
    reportArithmetic,
    totalRecords,
    isloadingNumberRecords,
    isLoadingDataWithOutPagi,
  } = useSelector((state) => state.reportDetailsReducer);

  const [hasMore, setHasMore] = useState(false);

  const [finalData, setFinalData] = useState({});
  const [finalArimetic, setFinalArimetic] = useState([]);

  const RECORD_PER_PAGE = 20;

  useEffect(() => {
    dispatch(setCurrentPage(1));
    // eslint-disable-next-line
  }, [recordID]);

  useEffect(() => {
    if (pathname.split("/")[1] === "dashboard") {
      setFinalArimetic(arithmeticReportData);
    } else {
      setFinalArimetic(reportArithmetic);
    }
  }, [reportArithmetic, arithmeticReportData, pathname]);

  function numberWithCommas(number, decimal_separator) {
    if (number === 0) {
      return "0";
    } else
      return number
        ?.toFixed(decimal_separator)
        ?.toString()
        .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
  }

  useEffect(() => {
    if (pathname.split("/")[1] === "dashboard") {
      setFinalData(dataReportDashboard);
    } else {
      setFinalData(dataReport);
    }
  }, [dataReport, dataReportDashboard, pathname]);

  useEffect(() => {
    if (finalData && finalData.data && finalData.data.length > 0) {
      let temPrev = currentPage * RECORD_PER_PAGE - 19;
      setPrev(temPrev);
      let temNext = temPrev + finalData.data.length - 1;
      setNext(temNext);
      const dataLenght = finalData.data.length;
      setFirstID(finalData.data[0]._id);
      setLastID(finalData.data[dataLenght - 1]._id);
      if (finalData.data.length === 20) {
        setHasMore(true);
      } else {
        setHasMore(false);
      }
    } else {
      setHasMore(false);
    }
    // eslint-disable-next-line
  }, [finalData]);

  // useEffect(() => {
  //   if (finalData) {
  //     setNext(
  //       finalData?.data?.length === recordPerPage
  //         ? currentPage * recordPerPage
  //         : (currentPage - 1) * recordPerPage + finalData?.data?.length
  //     );
  //   }
  //   /* eslint-disable-next-line */
  // }, [finalData]);

  // useEffect(() => {
  //   setPrev(currentPage * recordPerPage - recordPerPage + 1);
  //   /* eslint-disable-next-line */
  // }, [currentPage]);

  useEffect(() => {
    let tempColumn = [];
    if (Object.keys(infoReport).length > 0) {
      if (infoReport.report_info.report_type === "pagination") {
        if (Object.keys(finalData).length > 0) {
          finalData.data.forEach((item, idx) => {
            tempColumn.push({
              key: item._id,
            });
            item.data?.forEach((field) => {
              tempColumn[idx] = {
                ...tempColumn[idx],
                [field.id_field]: field.value,
              };
            });
          });
        }
        setColumnReport(tempColumn);
      } else if (infoReport.report_info.report_type === "detail") {
        if (Object.keys(finalData).length > 0) {
          finalData.data.forEach((item, idx) => {
            tempColumn.push({
              key: item._id,
            });
            item.data.forEach((field) => {
              tempColumn[idx] = {
                ...tempColumn[idx],
                [field.id_field]: field.value,
              };
            });
          });
        }

        setColumnReport((prev) => {
          if (prev.length === 0) {
            return tempColumn;
          } else {
            let listKey = [];
            prev.map((item) => {
              return listKey.push(item.key);
            });
            let newItems = tempColumn.filter(
              (item) => listKey.includes(item.key) === false
            );
            let result = prev.concat(newItems);
            return result;
          }
        });
      } else {
        // let tempData = [];
        // let numberOfEle = 0;
        // dataReport.index?.map((item, idx) => {
        //   tempData.push(item.concat(dataReport.data[idx]));
        //   if (idx === 0) {
        //     numberOfEle = item.length - 1;
        //   }
        // });
        // // eslint-disable-next-line array-callback-return
        // tempData.map((item, idx) => {
        //   if (idx === tempData.length - 1) {
        //     let NotGrandTotal = item.filter((ele) => ele !== "Grand Total");
        //     for (let i = 0; i < numberOfEle; i++) {
        //       NotGrandTotal.unshift("");
        //     }
        //     NotGrandTotal.unshift("Grand Total");
        //     tempData.splice(tempData.length - 1, 1);
        //     tempData.push(NotGrandTotal);
        //   }
        // });
        // setDataSummary(tempData);
      }
    }
  }, [finalData, infoReport, finalArimetic, setColumnReport, totalRecords]);

  return (
    <Wrapper>
      {Object.keys(infoReport).length > 0 && (
        <>
          {infoReport.report_info?.report_type === "pagination" ? (
            <>
              <WrapTablePagi>
                <Table
                  // scroll={{ x: 600 * infoReport.data?.length }}
                  scroll={{ x: "max-content" }}
                  dataSource={columnReport}
                  pagination={false}
                >
                  {infoReport.data?.map((item) => {
                    return (
                      <Column
                        title={item.name}
                        dataIndex={item.ID}
                        key={item.ID}
                        render={(text) => (
                          <Text ellipsis={{ tooltip: text }}>
                            {item?.type === "number" ||
                            (item?.type === "formula" &&
                              item?.formula_type === "advanceExpression")
                              ? numberWithCommas(
                                  text,
                                  _.get(item, "decimal_separator", 3)
                                )
                              : text}
                          </Text>
                        )}
                      />
                    );
                  })}
                </Table>

                <CustomPagination>
                  {isLoadingDataWithOutPagi ? (
                    <Spin />
                  ) : (
                    <>
                      <div className="total-record">
                        {next === 0 ? 0 : prev} - {next}
                        {isloadingNumberRecords ? (
                          <Spin style={{ marginLeft: "10px" }} />
                        ) : totalRecords === null ? (
                          <Tooltip title="Total records">
                            <div
                              className="reload-pagi"
                              onClick={() => {
                                if (
                                  tempAllCondition.length > 0 ||
                                  tempAnyCondition.length > 0
                                ) {
                                  dispatch(
                                    loadTotalRecords({
                                      current_page: 1,
                                      record_per_page: 10,
                                      object_id: objectID,
                                      report_id: recordID,
                                      quick_filter: {
                                        and_filter: tempAllCondition,
                                        or_filter: tempAnyCondition,
                                      },
                                      api_version: "2",
                                    })
                                  );
                                } else {
                                  dispatch(
                                    loadTotalRecords({
                                      current_page: 1,
                                      record_per_page: 10,
                                      object_id: objectID,
                                      report_id: recordID,
                                    })
                                  );
                                }
                              }}
                            >
                              <img alt="" src={Reload} />
                            </div>
                          </Tooltip>
                        ) : (
                          ` of ${
                            totalRecords
                              ? totalRecords === -1
                                ? "1M+"
                                : totalRecords
                              : 0
                          } records`
                        )}
                      </div>
                    </>
                  )}
                  <div
                    className="left-pagi"
                    style={{
                      pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                      cursor: `${
                        currentPage === 1 ? "not-allowed" : "pointer"
                      }`,
                      opacity: `${currentPage === 1 ? 0.5 : 1}`,
                    }}
                    onClick={() => {
                      if (finalData?.data?.length === 0) {
                        dispatch(setCurrentPage(1));
                        loadReport(null, null);
                      } else {
                        let currentPageTemp = currentPage;
                        currentPageTemp = currentPageTemp - 1;
                        dispatch(setCurrentPage(currentPageTemp));
                        loadReport(firstID, null);
                      }
                    }}
                  >
                    <img alt="" src={LeftPagi} />
                  </div>
                  <div
                    className="right-pagi"
                    style={{
                      pointerEvents: `${
                        finalData?.data?.length < 20 ? "none" : ""
                      }`,
                      cursor: `${
                        finalData?.data?.length < 20 ? "not-allowed" : "pointer"
                      }`,
                      opacity: `${finalData?.data?.length < 20 ? 0.5 : 1}`,
                    }}
                    onClick={() => {
                      // setLastID(
                      //   finalData?.data?.length > 0
                      //     ? finalData?.data[finalData?.data?.length - 1]._id
                      //     : null
                      // );
                      let currentPageTemp = currentPage;
                      currentPageTemp = currentPageTemp + 1;
                      dispatch(setCurrentPage(currentPageTemp));
                      // setFirstID(
                      //   finalData?.data?.length > 0
                      //     ? finalData?.data[0]?._id
                      //     : null
                      // );
                      // loadReport(
                      //   null,
                      //   finalData?.data?.length > 0
                      //     ? finalData?.data[finalData?.data?.length - 1]._id
                      //     : null
                      // );
                      loadReport(null, lastID);
                    }}
                  >
                    <img alt="" src={RightPagi} />
                  </div>
                </CustomPagination>
              </WrapTablePagi>
            </>
          ) : infoReport.report_info?.report_type === "summary" ||
            infoReport.report_info?.report_type === "row_summary_report" ? (
            <>
              {finalData?.columns?.length > 0 && (
                <WrapTableSummary
                  boldRow={infoReport?.report_info?.show_row_total}
                  boldCol={infoReport?.report_info?.show_col_total}
                >
                  <table>
                    <thead>
                      {finalData.columns.map((item) => {
                        return (
                          <>
                            <tr>
                              {item.map((column) => {
                                return <th>{column}</th>;
                              })}
                            </tr>
                          </>
                        );
                      })}
                    </thead>
                    <tbody>
                      {finalData.data.map((item, idx) => {
                        return (
                          <tr>
                            {item.map((column, idxColumn) => {
                              return (
                                <CustomTD
                                  bold={
                                    idxColumn <
                                    infoReport.report_info?.pivot_rows?.length
                                  }
                                >
                                  {column}
                                </CustomTD>
                              );
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </WrapTableSummary>
              )}
            </>
          ) : (
            <WrapTable
              id="infiniteScroll"
              height={columnReport.length}
              hasMore={hasMore}
            >
              <InfiniteScroll
                scrollableTarget="infiniteScroll"
                dataLength={columnReport.length}
                hasMore={hasMore}
                // next={loadData}
                next={() => {
                  loadReport(null, lastID);
                }}
                scrollThreshold="0px"
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b>You have seen it all!</b>
                  </p>
                }
              >
                <Table
                  scroll={{ x: "max-content" }}
                  dataSource={columnReport}
                  pagination={false}
                  summary={() => (
                    <Table.Summary>
                      <Table.Summary.Row>
                        {infoReport.data.map((field) => (
                          <>
                            {finalArimetic.find(
                              (item) => item.field_id === field.ID
                            ) ? (
                              <Table.Summary.Cell>
                                {finalArimetic.find(
                                  (item) => item.field_id === field.ID
                                ).min && (
                                  <>
                                    <p>
                                      Min:{" "}
                                      <span>
                                        {numberWithCommas(
                                          finalArimetic.find(
                                            (item) => item.field_id === field.ID
                                          ).min,
                                          3
                                        )}
                                      </span>
                                    </p>
                                  </>
                                )}
                                {finalArimetic.find(
                                  (item) => item.field_id === field.ID
                                ).max && (
                                  <>
                                    <p>
                                      Max:{" "}
                                      <span>
                                        {numberWithCommas(
                                          finalArimetic.find(
                                            (item) => item.field_id === field.ID
                                          ).max,
                                          3
                                        )}
                                      </span>
                                    </p>
                                  </>
                                )}
                                {finalArimetic.find(
                                  (item) => item.field_id === field.ID
                                ).sum && (
                                  <>
                                    <p>
                                      Sum:{" "}
                                      <span>
                                        {numberWithCommas(
                                          finalArimetic.find(
                                            (item) => item.field_id === field.ID
                                          ).sum,
                                          3
                                        )}
                                      </span>
                                    </p>
                                  </>
                                )}
                                {finalArimetic.find(
                                  (item) => item.field_id === field.ID
                                ).average && (
                                  <>
                                    <p>
                                      Average:{" "}
                                      <span>
                                        {numberWithCommas(
                                          finalArimetic.find(
                                            (item) => item.field_id === field.ID
                                          ).average,
                                          3
                                        )}
                                      </span>
                                    </p>
                                  </>
                                )}
                              </Table.Summary.Cell>
                            ) : (
                              <Table.Summary.Cell></Table.Summary.Cell>
                            )}
                          </>
                        ))}
                      </Table.Summary.Row>
                    </Table.Summary>
                  )}
                >
                  {infoReport.data?.map((item) => {
                    return (
                      <Column
                        title={item.name}
                        dataIndex={item.ID}
                        key={item.ID}
                        render={(text) => (
                          <Text ellipsis={{ tooltip: text }}>
                            {item?.type === "number" ||
                            (item?.type === "formula" &&
                              item?.formula_type === "advanceExpression")
                              ? numberWithCommas(
                                  text,
                                  _.get(item, "decimal_separator", 3)
                                )
                              : text}
                          </Text>
                        )}
                      />
                    );
                  })}
                </Table>
              </InfiniteScroll>
            </WrapTable>
          )}
        </>
      )}
    </Wrapper>
  );
}

export default DataReport;

const Wrapper = styled.div`
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapTablePagi = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;
  margin-bottom: 16px;

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 11px;
    }
  }

  .ant-table-thead > tr > th {
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-thead > tr {
    :first-child {
      th {
        padding: 8.5px 16px;
      }
    }

    :last-child {
      th {
        padding: 6.5px 16px;
      }
    }
  }

  .ant-table-tbody > tr > td {
    max-width: 500px;
    padding: 6px 16px;
    height: 35px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding-top: 16px;
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapTable = styled(WrapTablePagi)`
  overflow: auto;
  height: ${(props) =>
    props.height
      ? props.height < 10 && !props.hasMore
        ? "unset"
        : props.height > 9 && props.height * 35 + 80
      : 300}px;

  .infinite-scroll-component {
    display: flex;
    flex-direction: column;
  }

  ::-webkit-scrollbar {
    width: 8px;
  }

  .ant-spin {
    margin-top: 8px;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .ant-table-summary {
    .ant-table-cell {
      p {
        margin-bottom: 0;
        font-size: 16px;
        font-family: var(--roboto-500);

        span {
          font-family: var(--roboto-400);
        }
      }

      :first-child {
        border-left: 1px solid #d9d9d9;
      }

      :last-child {
        border-right: 1px solid #d9d9d9;
      }
    }
  }
`;

const WrapTableSummary = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;
  margin-bottom: 16px;
  overflow: auto;

  thead {
    tr {
      th {
        background: #f0f0f0;
        font-size: 16px;
        font-family: var(--roboto-500);
        color: #252424;
        text-align: left;
        width: 600px;
        height: 40px;
        padding: 8.5px 16px;
        border-bottom: 1px solid #d9d9d9;
        border-right: 1px solid #d9d9d9;

        :last-child {
          border-right: none;
        }
      }
    }
  }

  tbody {
    tr {
      td {
        width: 600px;
        height: 35px;
        padding: 6px 16px;
        border-bottom: 1px solid #d9d9d9;
        font-size: 16px;
        color: #2c2c2c;

        :last-child {
          font-family: ${({ boldRow }) => boldRow && "var(--roboto-700)"};
        }
      }

      :last-child {
        font-family: ${({ boldCol }) => boldCol && "var(--roboto-700)"};
      }
    }
  }
`;

const CustomTD = styled.td`
  background: ${({ bold }) => (bold ? "#f0f0f0" : "#fff")};
`;

const CustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  padding: 10px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .reload-pagi {
    img {
      width: 15px;
    }

    margin-left: 8px;
    width: 30px;
    height: 30px;

    display: flex;
    justify-content: center;
    align-items: center;

    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;
