import Button from "antd/lib/button";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Modal from "antd/lib/modal";
import Radio from "antd/lib/radio";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";
import Tag from "antd/lib/tag";
import Checkbox from "antd/lib/checkbox";
import _ from "lodash";

import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";
import BarCharts from "assets/images/reports/BarCharts.webp";
import BarChartsActive from "assets/images/reports/BarChartsActive.webp";
import ComboCharts from "assets/images/reports/ComboCharts.webp";
import ComboChartsActive from "assets/images/reports/ComboChartsActive.webp";
import LineCharts from "assets/images/reports/LineCharts.webp";
import LineChartsActive from "assets/images/reports/LineChartsActive.webp";
import PieCharts from "assets/images/reports/PieCharts.webp";
import PieChartsActive from "assets/images/reports/PieChartsActive.webp";
import BarHorizontalChart from "assets/images/reports/BarHorizontalChart.svg";
import BarHorizontalChartActive from "assets/images/reports/BarHorizontalChartActive.svg";
import FunnelChart from "assets/images/reports/FunnelChart.svg";
import FunnelChartActive from "assets/images/reports/FunnelChartActive.svg";

import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  addChart,
  getDataChart,
  loadCloumnValue,
  loadDetailsChartFail,
  resetStatus,
  updateChart,
} from "redux/slices/reportDetails";
import styled from "styled-components";

import { metaData } from "util/staticData";

function ModalChart({
  showModalChart,
  onHideModalChart,
  listFieldsObject,
  isEdit,
  setIsEdit,
  loadCharts,
  infoReport,
  report_id,
}) {
  const [form] = Form.useForm();
  const { CheckableTag } = Tag;
  const { Option } = Select;

  const dispatch = useDispatch();

  const {
    isLoading,
    isLoadingColumnValue,
    columnValue,
    chartInfo,
    isLoadingChart,
    detailsChart,
    isLoadingDataChart,
  } = useSelector((state) => state.reportDetailsReducer);

  const [field, setField] = useState([]);
  const [fieldWithoutMeta, setFieldWithoutMeta] = useState([]);
  const [labelValue, setLabelValue] = useState([]);
  const [listValueField, setListValueField] = useState([]);

  const [selectedValueField, setSelectedValueField] = useState([]);
  const [selectedColumnCount, setSelectedColumnCount] = useState([]);

  const [typeChart, setTypeChart] = useState("");

  const [barType, setBarType] = useState("");

  const [valueField, setValueField] = useState([]);

  const optionsChart = [
    {
      label:
        typeChart === "bar" ? (
          <img src={BarChartsActive} alt="barChart" />
        ) : (
          <img src={BarCharts} alt="barChart" />
        ),
      value: "bar",
    },
    {
      label:
        typeChart === "bar_horizontal" ? (
          <img src={BarHorizontalChartActive} alt="barHorizontalChart" />
        ) : (
          <img src={BarHorizontalChart} alt="barHorizontalChart" />
        ),
      value: "bar_horizontal",
    },
    {
      label:
        typeChart === "pie" ? (
          <img src={PieChartsActive} alt="pieChart" />
        ) : (
          <img src={PieCharts} alt="pieChart" />
        ),
      value: "pie",
    },
    {
      label:
        typeChart === "line" ? (
          <img src={LineChartsActive} alt="lineChart" />
        ) : (
          <img src={LineCharts} alt="lineChart" />
        ),
      value: "line",
    },

    {
      label:
        typeChart === "combo" ? (
          <img src={ComboChartsActive} alt="comboChart" />
        ) : (
          <img src={ComboCharts} alt="comboChart" />
        ),
      value: "combo",
    },
    {
      label:
        typeChart === "funnel" ? (
          <img src={FunnelChartActive} alt="comboChart" />
        ) : (
          <img src={FunnelChart} alt="comboChart" />
        ),
      value: "funnel",
    },
  ];

  const optionsType = [
    {
      label: "Count",
      value: "count",
    },
  ];

  const optionsTypeNumber = [
    {
      label: "Count",
      value: "count",
    },
    {
      label: "Sum",
      value: "sum",
    },
    {
      label: "Average",
      value: "average",
    },
    {
      label: "Max",
      value: "max",
    },
    {
      label: "Min",
      value: "min",
    },
  ];

  const handleSelectChart = (tag, checked) => {
    if (checked) {
      setTypeChart(tag);
      setBarType("");

      if (tag === "combo") {
        const item = [
          { field_id: "", operation: "count", field_type: "" },
          {
            field_id: "",
            operation: "count",
            field_type: "",
            formula_type: "",
            in_words: false,
          },
        ];
        setValueField(item);
      } else {
        setValueField([]);
      }

      form.setFieldsValue({
        label_field: undefined,
        value_field: undefined,
        label_value: undefined,
        column_count: undefined,
        bar_type: undefined,
      });
    }
  };

  const handleSelectType = (e) => {
    setBarType(e.target.value);

    form.setFieldsValue({
      column_count: undefined,
      label_field: undefined,
      value_field: undefined,
    });
  };

  const _onSubmit = (values) => {
    if (typeChart === "") {
      Notification("warning", "Please select type chart!");
    } else if (
      valueField.length === 0 &&
      (typeChart === "line" || typeChart === "combo")
    ) {
      Notification("warning", "Please full field requied!");
    } else {
      let data = {};

      if (typeChart === "line") {
        let tempLabelField = [
          {
            field_id: values.label_field,
            values: [...values.label_value],
          },
        ];

        data = {
          name: values.chart_name,
          chart_type: typeChart,
          chartjs_conf: [],
          report_id: report_id,
          value_fields: valueField,
          label_fields: tempLabelField,
          operation: "sum",
        };
      } else if (typeChart === "combo") {
        let tempLabelField = [
          {
            field_id: values.label_field,
          },
        ];

        data = {
          name: values.chart_name,
          chart_type: typeChart,
          chartjs_conf: [],
          report_id: report_id,
          value_fields: valueField,
          label_fields: tempLabelField,
          operation: "sum",
        };
      } else {
        data = {
          chart_type: typeChart,
          chartjs_conf: [],
          name: values.chart_name,
          operation: values.bar_type,
          report_id: report_id,
          label_fields: [selectedColumnCount],
          value_fields: selectedValueField,
          show_details:
            typeChart === "pie"
              ? _.get(values, "show_details", false)
              : undefined,
        };
      }

      if (isEdit) {
        dispatch(
          updateChart({
            ...data,
            _id: detailsChart._id,
          })
        );
      } else {
        dispatch(
          addChart({
            ...data,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    onHideModalChart();
    form.resetFields();
    setTypeChart("");
    setBarType("");
    setValueField([]);
    setLabelValue([]);
    setIsEdit(false);
    dispatch(loadDetailsChartFail());
  };

  const handleSelectField = (value) => {
    setLabelValue([]);
    form.setFieldsValue({
      label_value: undefined,
    });
    dispatch(
      loadCloumnValue({
        field_id: value,
        report_id: report_id,
      })
    );
  };

  const handleSelectValueField = (value, option) => {
    let tempOptions = [];
    option.map((item) => {
      return tempOptions.push({
        field_id: item.value,
        name: item.label,
      });
    });
    setSelectedValueField(tempOptions);
  };

  const handleSelectColumnCount = (value, option) => {
    setSelectedColumnCount({
      field_id: option.value,
      name: option.label,
    });
  };

  const _onAddValueCombo = () => {
    const item = [
      ...valueField,
      {
        field_id: "",
        operation: "count",
        field_type: "",
        formula_type: "",
        in_words: false,
      },
    ];
    setValueField(item);
  };

  const onDeleteField = (idx) => {
    let temp = [...valueField];
    temp.splice(idx, 1);
    setValueField(temp);
  };

  const handleChangeField = (e, option, idx, type) => {
    let arr = [...valueField];
    let tempField = {};

    field.forEach((item) => {
      if (item.value === e) tempField = item;
    });

    if (type === "field") {
      valueField.forEach((item, index) => {
        if (index === idx) {
          arr[index] = {
            ...item,
            field_id: e,
            field_type: tempField.type,
            formula_type: tempField?.formula_type || "",
            operation: "count",
            in_words: tempField?.in_words || false,
          };
        }
      });
    } else {
      valueField.forEach((item, index) => {
        if (index === idx) {
          arr[index] = {
            ...item,
            operation: e,
          };
        }
      });
    }
    setValueField(arr);
  };

  useEffect(() => {
    valueField.forEach((item, idx) => {
      if (item.field_id !== "") {
        form.setFieldsValue({
          [item.field_id + idx]: item.field_id,
        });
      } else {
        form.setFieldsValue({
          ["" + idx]: undefined,
        });
      }

      if (item.operation !== undefined) {
        form.setFieldsValue({
          ["operation_" + item.field_id + idx]: item.operation,
        });
      }
    });
  }, [form, valueField]);

  useEffect(() => {
    if (Object.keys(infoReport).length > 0) {
      let tempField = [];
      let tempFieldWithoutMeta = [];
      let tempFieldNumber = [];
      infoReport.data.forEach((item) => {
        if (!_.get(item, "encrypted", false)) {
          if (
            item.type === "number" ||
            (item.type === "formula" &&
              ((item.formula_type === "dateDiff" && !item?.in_words) ||
                item.formula_type === "dateToParts" ||
                item.formula_type === "advanceExpression"))
          ) {
            tempFieldNumber.push({
              label: item.name,
              value: item.ID,
              type: item.type,
            });
          }

          if (
            item.full_field_id !== "created_date" &&
            item.full_field_id !== "created_by" &&
            item.full_field_id !== "modify_time" &&
            item.full_field_id !== "modify_by" &&
            item.full_field_id !== "owner"
          ) {
            tempFieldWithoutMeta.push({
              label: item.name,
              value: item.ID,
              type: item.type,
              in_words: item?.in_words || false,
            });
          }
          tempField.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            formula_type: item?.formula_type || "",
            in_words: item?.in_words || false,
          });
        }
      });
      setField(tempField);
      setFieldWithoutMeta(_.uniqBy(tempFieldWithoutMeta, "value"));
      setListValueField(tempFieldNumber);
    }
  }, [infoReport]);

  useEffect(() => {
    if (columnValue.length > 0) {
      let templist = [];
      columnValue.map((item) => {
        return templist.push({
          label: item.value,
          value: item.key,
        });
      });
      setLabelValue(templist);
    }
  }, [columnValue]);

  useEffect(() => {
    if (isEdit && Object.keys(detailsChart).length > 0) {
      switch (detailsChart.chart_type) {
        case "line":
          // let tempValueField = [];
          // detailsChart.value_fields.map((item) => {
          //   return tempValueField.push(item.field_id);
          // });

          form.setFieldsValue({
            chart_name: detailsChart.name,
            label_field: detailsChart.label_fields[0].field_id,
            label_value: detailsChart.label_fields[0].values,
          });
          setTypeChart(detailsChart.chart_type);
          setValueField(detailsChart.value_fields);

          dispatch(
            loadCloumnValue({
              field_id: detailsChart.label_fields[0].field_id,
              report_id: report_id,
            })
          );
          break;
        case "combo":
          form.setFieldsValue({
            chart_name: detailsChart.name,
            label_field: detailsChart.label_fields[0].field_id,
          });
          setTypeChart(detailsChart.chart_type);
          setValueField(detailsChart.value_fields);

          dispatch(
            loadCloumnValue({
              field_id: detailsChart.label_fields[0].field_id,
              report_id: report_id,
            })
          );
          break;
        case "bar":
        case "bar_horizontal":
        case "pie":
        case "funnel":
          form.setFieldsValue({
            chart_name: detailsChart.name,
            bar_type: detailsChart.operation,
            show_details: _.get(detailsChart, "show_details", false),
          });

          if (detailsChart.operation === "count") {
            form.setFieldsValue({
              column_count: detailsChart.label_fields[0].field_id,
            });
          } else {
            let tempValueField = [];
            detailsChart.value_fields.map((item) => {
              return tempValueField.push(item.field_id);
            });

            form.setFieldsValue({
              label_field: detailsChart.label_fields[0].field_id,
              value_field: tempValueField,
            });
            setSelectedValueField(_.get(detailsChart, "value_fields", []));
          }

          setSelectedColumnCount(detailsChart.label_fields[0]);
          setBarType(detailsChart.operation);
          setTypeChart(detailsChart.chart_type);

          break;

        default:
          break;
      }
    }
  }, [detailsChart, dispatch, form, isEdit, report_id]);

  useEffect(() => {
    if (isLoading === false) {
      if (chartInfo === "success" && showModalChart) {
        if (isEdit === false) {
          loadCharts();
          onHideModalChart();
          form.resetFields();
          Notification("success", "Create successfully!");
          dispatch(resetStatus());
          setTypeChart("");
          setBarType("");
          setValueField([]);
          setLabelValue([]);
        }

        if (isEdit) {
          dispatch(
            getDataChart({
              _id: detailsChart._id,
              api_version: "2",
            })
          );
        }

        if (isEdit && isLoadingDataChart === false) {
          setIsEdit(false);
          onHideModalChart();
          form.resetFields();
          Notification("success", "Update successfully!");
          setTypeChart("");
          setBarType("");
          setValueField([]);
          setLabelValue([]);
          dispatch(resetStatus());
        }
      }

      if (chartInfo !== null && chartInfo !== "success") {
        Notification("error", chartInfo);
        dispatch(resetStatus());
      }
    }
  }, [
    chartInfo,
    detailsChart,
    dispatch,
    form,
    isEdit,
    isLoading,
    isLoadingDataChart,
    loadCharts,
    onHideModalChart,
    setIsEdit,
    showModalChart,
  ]);

  return (
    <ModalCustom
      title={`Add chart`}
      visible={showModalChart}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {isLoadingChart ? (
        <WrapSpin>
          <Spin />
        </WrapSpin>
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label="Chart name"
            name="chart_name"
            rules={[{ required: true, message: "Please input name of chart" }]}
          >
            <Input />
          </Form.Item>

          <div
            style={{
              display: "flex",
              justifyContent: "center",
              flexWrap: "wrap",
              gap: "24px",
              marginBottom: "24px",
            }}
          >
            {optionsChart.map((tag) => (
              <CheckableTag
                key={tag.value}
                // checked={typeChart.indexOf(tag.value) > -1}
                onChange={(checked) => handleSelectChart(tag.value, checked)}
              >
                {tag.label}
              </CheckableTag>
            ))}
          </div>

          {typeChart === "line" && (
            <>
              <Form.Item
                label="Label field"
                name="label_field"
                rules={[
                  { required: true, message: "Please select label field" },
                ]}
              >
                <Select
                  options={fieldWithoutMeta.concat(metaData)}
                  placeholder="Please select"
                  onChange={handleSelectField}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item
                label="Label values"
                name="label_value"
                rules={[
                  { required: true, message: "Please select label field" },
                ]}
              >
                <Select
                  loading={isLoadingColumnValue}
                  options={labelValue}
                  placeholder="Please select"
                  mode="multiple"
                  optionFilterProp="label"
                />
              </Form.Item>
              <WrapValue>
                <legend className="required">Add value</legend>

                {valueField.length > 0 && (
                  <>
                    {valueField.map((item, idx) => {
                      return (
                        <FormValueCombo>
                          <FormValue
                            // labelCol={{ span: 6 }}
                            // wrapperCol={{ span: 18 }}
                            label=""
                            name={item.field_id + idx}
                            rules={[
                              {
                                required: true,
                                message: "Please select field",
                              },
                            ]}
                          >
                            <Select
                              placeholder="Select field"
                              onChange={(e, option) => {
                                handleChangeField(e, option, idx, "field");
                              }}
                              showSearch
                              optionFilterProp="children"
                              filterOption={(inputValue, option) => {
                                if (option.children) {
                                  return option.children
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                } else if (option.label) {
                                  return option.label
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                }
                              }}
                            >
                              {fieldWithoutMeta
                                .concat(metaData)
                                .map((option) => {
                                  return (
                                    <Option
                                      disabled={
                                        valueField.find(
                                          (field) =>
                                            field.field_id === option.value
                                        )
                                          ? true
                                          : false
                                      }
                                      key={option.value}
                                    >
                                      {option.label}
                                    </Option>
                                  );
                                })}
                            </Select>
                          </FormValue>
                          <FormValue
                            label=""
                            name={`operation_${item.field_id}${idx}`}
                            rules={[
                              {
                                required: true,
                                message: "Please select operation",
                              },
                            ]}
                          >
                            <Select
                              placeholder="Select operation"
                              options={
                                item.field_type === "number" ||
                                (item.formula_type === "dateDiff" &&
                                  !item.in_words) ||
                                item.formula_type === "dateToParts" ||
                                item.formula_type === "advanceExpression"
                                  ? optionsTypeNumber
                                  : optionsType
                              }
                              defaultValue="count"
                              onChange={(e, option) => {
                                handleChangeField(e, option, idx, "operation");
                              }}
                            />
                          </FormValue>

                          <Delete>
                            <img
                              src={DeleteIcon}
                              onClick={() => onDeleteField(idx)}
                              alt="delete"
                            />
                          </Delete>
                        </FormValueCombo>
                      );
                    })}
                  </>
                )}

                {valueField.length < 5 && (
                  <AddNew onClick={() => _onAddValueCombo()}>
                    {/* <img src={PlusGreen} alt="plus" /> */}
                    <span>+ Add value</span>
                  </AddNew>
                )}
              </WrapValue>
              {/* <Form.Item
                label="Value field"
                name="value_field"
                rules={[
                  { required: true, message: "Please select value field" },
                ]}
              >
                <Select
                  options={listValueField}
                  onChange={handleSelectValueField}
                  placeholder="Please select"
                  mode="multiple"
                />
              </Form.Item> */}
            </>
          )}

          {typeChart === "combo" && (
            <>
              <Form.Item
                label="Label field"
                name="label_field"
                rules={[
                  { required: true, message: "Please select label field" },
                ]}
              >
                <Select
                  options={fieldWithoutMeta.concat(metaData)}
                  placeholder="Please select"
                  onChange={handleSelectField}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>

              <WrapValue>
                <legend>
                  Add value <span>(Last value is line, minimum 2 values)</span>
                </legend>

                {valueField.length > 0 && (
                  <>
                    {valueField.map((item, idx) => {
                      return (
                        <FormValueCombo>
                          <FormValue
                            // labelCol={{ span: 6 }}
                            // wrapperCol={{ span: 18 }}
                            label=""
                            name={item.field_id + idx}
                            rules={[
                              {
                                required: true,
                                message: "Please select field",
                              },
                            ]}
                          >
                            <Select
                              placeholder="Select field"
                              onChange={(e, option) => {
                                handleChangeField(e, option, idx, "field");
                              }}
                              showSearch
                              optionFilterProp="children"
                              filterOption={(inputValue, option) => {
                                if (option.children) {
                                  return option.children
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                } else if (option.label) {
                                  return option.label
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                }
                              }}
                            >
                              {fieldWithoutMeta
                                .concat(metaData)
                                .map((option) => {
                                  return (
                                    <Option
                                      disabled={
                                        valueField.find(
                                          (field) =>
                                            field.field_id === option.value
                                        )
                                          ? true
                                          : false
                                      }
                                      key={option.value}
                                    >
                                      {option.label}
                                    </Option>
                                  );
                                })}
                            </Select>
                          </FormValue>
                          <FormValue
                            label=""
                            name={`operation_${item.field_id}${idx}`}
                            rules={[
                              {
                                required: true,
                                message: "Please select operation",
                              },
                            ]}
                          >
                            <Select
                              placeholder="Select operation"
                              options={
                                item.field_type === "number" ||
                                (item.formula_type === "dateDiff" &&
                                  !item.in_words) ||
                                item.formula_type === "dateToParts" ||
                                item.formula_type === "advanceExpression"
                                  ? optionsTypeNumber
                                  : optionsType
                              }
                              defaultValue="count"
                              onChange={(e, option) => {
                                handleChangeField(e, option, idx, "operation");
                              }}
                            />
                          </FormValue>

                          {valueField.length > 2 && (
                            <Delete>
                              <img
                                src={DeleteIcon}
                                onClick={() => onDeleteField(idx)}
                                alt="delete"
                              />
                            </Delete>
                          )}
                        </FormValueCombo>
                      );
                    })}
                  </>
                )}

                {valueField.length < 5 && (
                  <AddNew onClick={() => _onAddValueCombo()}>
                    {/* <img src={PlusGreen} alt="plus" /> */}
                    <span>+ Add value</span>
                  </AddNew>
                )}
              </WrapValue>
            </>
          )}

          {(typeChart === "bar" ||
            typeChart === "pie" ||
            typeChart === "funnel" ||
            typeChart === "bar_horizontal") && (
            <>
              <Form.Item
                label="Type"
                name="bar_type"
                rules={[{ required: true, message: "Please select type" }]}
              >
                <Radio.Group
                  options={optionsTypeNumber}
                  onChange={handleSelectType}
                  optionType="button"
                />
              </Form.Item>

              {barType === "count" && (
                <>
                  <Form.Item
                    label="Column to count"
                    name="column_count"
                    rules={[
                      {
                        required: true,
                        message: "Please select column to count",
                      },
                    ]}
                  >
                    <Select
                      onChange={handleSelectColumnCount}
                      options={fieldWithoutMeta.concat(metaData)}
                      placeholder="Please select"
                      optionFilterProp="label"
                      showSearch
                    />
                  </Form.Item>
                </>
              )}

              {(barType === "sum" ||
                barType === "average" ||
                barType === "max" ||
                barType === "min") && (
                <>
                  <Form.Item
                    label="Label field"
                    name="label_field"
                    rules={[
                      { required: true, message: "Please select label field" },
                    ]}
                  >
                    <Select
                      onChange={handleSelectColumnCount}
                      options={fieldWithoutMeta.concat(metaData)}
                      placeholder="Please select"
                      optionFilterProp="label"
                      showSearch
                    />
                  </Form.Item>
                  <Form.Item
                    label="Value field"
                    name="value_field"
                    rules={[
                      { required: true, message: "Please select value field" },
                    ]}
                  >
                    <Select
                      options={listValueField}
                      placeholder="Please select"
                      onChange={handleSelectValueField}
                      mode="multiple"
                    />
                  </Form.Item>
                </>
              )}

              {typeChart === "pie" && (
                <Form.Item name="show_details" valuePropName="checked">
                  <Checkbox>Show details</Checkbox>
                </Form.Item>
              )}
            </>
          )}

          <WrapButton label=" ">
            <Button type="primary" htmlType="submit" loading={isLoading}>
              Save
            </Button>
            <Button onClick={_onCancel}>Cancel</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default ModalChart;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-tag {
    margin-right: 0;
    padding: 0 0;

    :hover {
      border-color: ${(props) => props.theme.main};
    }

    :last-of-type {
      margin-right: 0;
    }
  }

  .ant-tag-checkable-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-tag-checkable:active {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapValue = styled.fieldset`
  padding: 16px 16px 0 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    position: relative;
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-size: 14px;
      font-family: var(--roboto-400);
    }
  }

  .required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 8px;
      left: 0;
    }
  }
`;

const FormValueCombo = styled(Form.Item)`
  width: 100%;
  margin-bottom: 8px;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }

  .ant-col {
    max-width: 100% !important;
  }
`;

const FormValue = styled(Form.Item)`
  margin-bottom: 0;
  width: 20%;
  margin-right: 16px;

  :first-child {
    width: 70%;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 65%;
    &:hover {
      cursor: pointer;
    }
  }
`;

const AddNew = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;
