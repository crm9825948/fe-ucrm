import { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";
import { useSelector } from "react-redux";
import _ from "lodash";
import { Notification } from "components/Notification/Noti";
import { checkConditions } from "util/helper";

import Drawer from "antd/lib/drawer";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Form from "antd/lib/form";

import Conditions from "components/Conditions/conditions";
import { useTranslation, withTranslation } from "react-i18next";
import ApplyingIcon from "assets/icons/report/Applying.svg";

import ModalSaveFilter from "./ModalSaveFilter";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import { quickFilterFeat } from "redux/slices/reportDetails";

function QuickFilter({
  onClose,
  showFilter,
  onFilter,
  onCloseFilter,
  allCondition,
  setAllCondition,
  operatorValueAnd,
  setOperatorValueAnd,
  valueAnd,
  setValueAnd,
  anyCondition,
  setAnyCondition,
  operatorValueOr,
  setOperatorValueOr,
  valueOr,
  setValueOr,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { recordID } = useParams();
  const { userDetail } = useSelector((state) => state.userReducer);
  const { quickFilters } = useSelector((state) => state.reportDetailsReducer);
  const [form] = Form.useForm();

  const [open, $open] = useState(false);
  const [showAddCondition, $showAddCondition] = useState(false);
  const [dataDelete, $dataDelete] = useState({});
  const [openConfirm, $openConfirm] = useState(false);
  const [filterDetails, $filterDetails] = useState({});
  const [filterApplying, $filterApplying] = useState("");

  const _onUnapplyFilter = (e) => {
    e.stopPropagation();
    setAllCondition([]);
    setAnyCondition([]);
    $filterApplying("");
    onFilter([], []);
  };

  const _onApplyFilter = (e, item, filterNotSave) => {
    e.stopPropagation();
    if (!_.isEmpty(filterNotSave)) {
      $filterApplying(_.get(item, "_id", ""));
      onFilter();
    } else {
      const filter = item || filterDetails || {};
      if (_.isEmpty(filter) && quickFilters.length > 0 && !showAddCondition) {
        Notification("error", "Please select filter!");
        return;
      }
      $filterApplying(_.get(filter, "_id", ""));
      if (_.isEmpty(filter)) {
        onFilter();
      } else {
        onFilter(
          _.get(filter, "quick_filter.and_filter", []),
          _.get(filter, "quick_filter.or_filter", [])
        );
      }
    }
  };

  const _onSelectFilter = (filter) => {
    return () => {
      $filterDetails(filter);
      setAllCondition(_.get(filter, "quick_filter.and_filter", []));
      setAnyCondition(_.get(filter, "quick_filter.or_filter", []));
      form.setFieldsValue({
        name: _.get(filter, "name", ""),
        description: _.get(filter, "description", ""),
      });
    };
  };
  const _onEditFilter = (values) => {
    if (
      checkConditions(allCondition, operatorValueAnd, valueAnd) ||
      checkConditions(anyCondition, operatorValueOr, valueOr)
    ) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      dispatch(
        quickFilterFeat({
          report_type: "edit",
          id: _.get(filterDetails, "_id"),
          name: values.name,
          description: values.description ? values.description : "",
          quick_filter: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
          report_id: recordID,
          user_id: userDetail._id,
        })
      );
    }
  };

  const _onDeleteFilter = (id) => {
    return (e) => {
      e.stopPropagation();
      $openConfirm(true);
      $dataDelete({
        report_type: "delete",
        id: id,
        report_id: recordID,
        user_id: userDetail._id,
      });
    };
  };

  useEffect(() => {
    if (userDetail && recordID)
      dispatch(
        quickFilterFeat({
          report_type: "load",
          report_id: recordID,
          user_id: userDetail._id,
        })
      );
  }, [dispatch, recordID, userDetail]);

  useEffect(() => {
    if (showFilter) {
      if (quickFilters.length === 0) {
        $showAddCondition(true);
      } else {
        $showAddCondition(false);
      }
    }
  }, [quickFilters, showFilter]);

  return (
    <Wrapper>
      <Drawer
        title={
          <div style={{ display: "flex", alignItems: "center" }}>
            <span>Filter</span>

            {!showAddCondition && (
              <AddFilter
                onClick={() => {
                  $showAddCondition(true);
                  setAllCondition([]);
                  setAnyCondition([]);
                  $filterDetails({});
                  $filterApplying("");
                }}
              >
                + {t("report.addFilter")}
              </AddFilter>
            )}
          </div>
        }
        placement="right"
        width={1024}
        onClose={onClose}
        visible={showFilter}
        footer={
          <FooterDrawer>
            <Cancel onClick={onCloseFilter}>{t("common.cancel")}</Cancel>
            {showAddCondition && (
              <>
                <Cancel onClick={() => $open(true)}>
                  {t("report.saveFilter")}
                </Cancel>
                <Apply onClick={_onApplyFilter}>{t("CallView.apply")}</Apply>
              </>
            )}
          </FooterDrawer>
        }
      >
        {showAddCondition ? (
          <>
            <Conditions
              title={"AND condition"}
              decs={"(All conditions must be met)"}
              conditions={allCondition}
              setConditions={setAllCondition}
              operatorValue={operatorValueAnd}
              setOperatorValue={setOperatorValueAnd}
              value={valueAnd}
              setValue={setValueAnd}
            />
            <Conditions
              title={"OR condition"}
              decs={"(Any conditions must be met)"}
              conditions={anyCondition}
              setConditions={setAnyCondition}
              operatorValue={operatorValueOr}
              setOperatorValue={setOperatorValueOr}
              value={valueOr}
              setValue={setValueOr}
            />
          </>
        ) : (
          <div style={{ display: "flex" }}>
            <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
              {_.map(quickFilters, (item) => (
                <>
                  <Filter onClick={_onSelectFilter(item)}>
                    <span
                      style={{
                        fontSize: "16px",
                        fontFamily: "var(--roboto-700)",
                        color: "#2c2c2c",
                      }}
                    >
                      {_.get(item, "name", "")}
                    </span>
                    {filterApplying === _.get(item, "_id") && (
                      <Applying>
                        <img
                          src={ApplyingIcon}
                          alt=""
                          style={{ marginRight: "4px" }}
                        />
                        {t("CallView.applying")}
                      </Applying>
                    )}

                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        height: "24px",
                      }}
                    >
                      <span style={{ color: "#637381", minWidth: "130px" }}>
                        {_.get(item, "modified_date", "")}
                      </span>

                      <div
                        className="action"
                        style={{
                          visibility: !_.isEmpty(filterDetails) && "hidden",
                        }}
                      >
                        <div>
                          {filterApplying === _.get(item, "_id") ? (
                            <span onClick={_onUnapplyFilter}>
                              {t("report.unapply")}
                            </span>
                          ) : (
                            <span onClick={(e) => _onApplyFilter(e, item)}>
                              {t("CallView.apply")}
                            </span>
                          )}
                        </div>
                        <span>|</span>
                        <div onClick={_onDeleteFilter(_.get(item, "_id"))}>
                          {t("common.delete")}
                        </div>
                      </div>
                    </div>
                  </Filter>
                </>
              ))}
            </div>

            {!_.isEmpty(filterDetails) && (
              <FilterDetails>
                <CloseFilter onClick={() => $filterDetails({})}>X</CloseFilter>

                <Form
                  form={form}
                  layout="vertical"
                  onFinish={_onEditFilter}
                  style={{ marginBottom: "16px" }}
                >
                  <Form.Item
                    label={t("report.filterName")}
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: t("common.placeholderInput"),
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item label={t("common.description")} name="description">
                    <Input />
                  </Form.Item>
                </Form>

                <div style={{ marginBottom: "24px" }}>
                  <Conditions
                    title={"AND condition"}
                    decs={"(All conditions must be met)"}
                    conditions={allCondition}
                    setConditions={setAllCondition}
                    operatorValue={operatorValueAnd}
                    setOperatorValue={setOperatorValueAnd}
                    value={valueAnd}
                    setValue={setValueAnd}
                    ID={_.get(filterDetails, "_id", "")}
                    background="#fff"
                  />
                </div>

                <Conditions
                  title={"OR condition"}
                  decs={"(Any conditions must be met)"}
                  conditions={anyCondition}
                  setConditions={setAnyCondition}
                  operatorValue={operatorValueOr}
                  setOperatorValue={setOperatorValueOr}
                  value={valueOr}
                  setValue={setValueOr}
                  ID={_.get(filterDetails, "_id", "")}
                  background="#fff"
                />

                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    columnGap: "8px",
                  }}
                >
                  <Cancel onClick={() => form.submit()}>
                    {t("common.save")}
                  </Cancel>
                  <Apply
                    onClick={(e) => {
                      _onApplyFilter(e, filterDetails, {
                        allCondition: allCondition,
                        anyCondition: anyCondition,
                      });
                    }}
                  >
                    {t("CallView.apply")}
                  </Apply>
                </div>
              </FilterDetails>
            )}
          </div>
        )}
      </Drawer>

      <ModalSaveFilter
        open={open}
        $open={$open}
        allCondition={allCondition}
        operatorValueAnd={operatorValueAnd}
        valueAnd={valueAnd}
        anyCondition={anyCondition}
        operatorValueOr={operatorValueOr}
        valueOr={valueOr}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={quickFilterFeat}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={openConfirm}
        setOpenConfirm={$openConfirm}
      />
    </Wrapper>
  );
}

export default withTranslation()(QuickFilter);

const Wrapper = styled.div``;

const Apply = styled(Button)`
  background: ${(props) => props.theme.main};
  color: #fff;
  width: 130px;

  :hover {
    background: ${(props) => props.theme.darker};
    border: 1px solid ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const Cancel = styled(Button)`
  width: 130px;

  :hover {
    background: ${(props) => props.theme.darker};
    border: 1px solid ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const FooterDrawer = styled.div`
  display: flex;
  justify-content: flex-end;
  column-gap: 12px;
`;

const Applying = styled.div`
  border-radius: 12px;
  border: 1px solid #20a2a2;
  width: fit-content;
  padding: 0 8px;
  font-size: 12px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 8px 0;
`;

const AddFilter = styled.div`
  color: ${(props) => props.theme.main};
  margin-left: 16px;
  cursor: pointer;
`;

const Filter = styled.div`
  cursor: pointer;
  border-bottom: 1px solid #ececec;
  padding: 16px;
  transition: all 0.4s;

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
    display: flex;
    column-gap: 8px;
  }

  &:hover {
    border-radius: 5px 5px 0px 0px;
    border-bottom: 1px solid #ececec;
    background: #f0f0f0;

    .action {
      visibility: visible;
      opacity: 1;
      font-size: 15px;
      color: #2c2c2c;
    }
  }
`;

const FilterDetails = styled.div`
  border-radius: 5px;
  background: #f0f0f0;
  padding: 16px;
  flex: 2;
  position: relative;

  .ant-form {
    display: flex;
    column-gap: 16px;
  }

  .ant-form-item-label {
    font-family: var(--roboto-500);
  }

  .ant-form-item {
    width: calc(100% - 8px);
  }
`;

const CloseFilter = styled.div`
  position: absolute;
  width: 22px;
  height: 22px;
  top: 6px;
  right: 7px;
  background: #919eab;
  color: #fff;
  text-align: center;
  cursor: pointer;
  z-index: 2;
`;
