import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import Checkbox from "antd/lib/checkbox";
import Spin from "antd/lib/spin";
import Radio from "antd/lib/radio";

import { Notification } from "components/Notification/Noti";
import Conditions from "components/Conditions/conditions";

import {
  addWidget,
  resetStatus,
  updateWidget,
  getDataWidget,
  loadDetailsWidgetFail,
} from "redux/slices/reportDetails";

function ModalWidget({
  showModalWidget,
  onHideModalWidget,
  loadWidgets,
  isEdit,
  setIsEdit,
  listFieldsObject,
  report_id,
}) {
  const [form] = Form.useForm();

  const dispatch = useDispatch();

  const {
    isLoading,
    widgetInfo,
    isLoadingWidget,
    detailsWidget,
    isLoadingDataWidget,
  } = useSelector((state) => state.reportDetailsReducer);

  const [isCompared, setIsCompared] = useState(false);
  const [showValue, setShowValue] = useState(false);
  const [showPercent, setShowPercent] = useState(false);

  const [field, setField] = useState([]);
  const [typeWidget, setTypeWidget] = useState("dynamic");
  const [typeWidget2, setTypeWidget2] = useState("dynamic");

  const [measure, setMeasure] = useState({
    aggregation: "",
    field_id: "",
    field_type: "",
    formula_type: "",
    type: "default",
  });

  const [allCondition1, setAllCondition1] = useState([]);
  const [allCondition2, setAllCondition2] = useState([]);
  const [anyCondition1, setAnyCondition1] = useState([]);
  const [anyCondition2, setAnyCondition2] = useState([]);

  const [operatorValueAnd1, setOperatorValueAnd1] = useState([]);
  const [operatorValueOr1, setOperatorValueOr1] = useState([]);
  const [valueAnd1, setValueAnd1] = useState([]);
  const [valueOr1, setValueOr1] = useState([]);

  const [operatorValueAnd2, setOperatorValueAnd2] = useState([]);
  const [operatorValueOr2, setOperatorValueOr2] = useState([]);
  const [valueAnd2, setValueAnd2] = useState([]);
  const [valueOr2, setValueOr2] = useState([]);

  const optionsType = [
    {
      label: "Static",
      value: "static",
    },
    {
      label: "Dynamic",
      value: "dynamic",
    },
  ];

  const handleSelectCompared = (e) => {
    setIsCompared(e.target.checked);
  };

  const handleShowValue = (e) => {
    setShowValue(e.target.checked);
  };

  const handleShowPercent = (e) => {
    setShowPercent(e.target.checked);
  };

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd1.length === allCondition1.length) {
      operatorValueAnd1.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr1.length === anyCondition1.length) {
      operatorValueOr1.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd1.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd1[idx] !== "empty" &&
        operatorValueAnd1[idx] !== "not-empty" &&
        operatorValueAnd1[idx] !== "mine" &&
        operatorValueAnd1[idx] !== "not-mine" &&
        operatorValueAnd1[idx] !== "today" &&
        operatorValueAnd1[idx] !== "yesterday" &&
        operatorValueAnd1[idx] !== "this-week" &&
        operatorValueAnd1[idx] !== "last-week" &&
        operatorValueAnd1[idx] !== "this-month" &&
        operatorValueAnd1[idx] !== "last-month" &&
        operatorValueAnd1[idx] !== "this-year"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr1.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr1[idx] !== "empty" &&
        operatorValueOr1[idx] !== "not-empty" &&
        operatorValueOr1[idx] !== "mine" &&
        operatorValueOr1[idx] !== "not-mine" &&
        operatorValueOr1[idx] !== "today" &&
        operatorValueOr1[idx] !== "yesterday" &&
        operatorValueOr1[idx] !== "this-week" &&
        operatorValueOr1[idx] !== "last-week" &&
        operatorValueOr1[idx] !== "this-month" &&
        operatorValueOr1[idx] !== "last-month" &&
        operatorValueOr1[idx] !== "this-year"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    if (operatorValueAnd2.length === allCondition2.length) {
      operatorValueAnd2.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr2.length === anyCondition2.length) {
      operatorValueOr2.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd2.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd2[idx] !== "empty" &&
        operatorValueAnd2[idx] !== "not-empty" &&
        operatorValueAnd2[idx] !== "mine" &&
        operatorValueAnd2[idx] !== "not-mine" &&
        operatorValueAnd2[idx] !== "today" &&
        operatorValueAnd2[idx] !== "yesterday" &&
        operatorValueAnd2[idx] !== "this-week" &&
        operatorValueAnd2[idx] !== "last-week" &&
        operatorValueAnd2[idx] !== "this-month" &&
        operatorValueAnd2[idx] !== "last-month" &&
        operatorValueAnd2[idx] !== "this-year"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    operatorValueOr2.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr2[idx] !== "empty" &&
        operatorValueOr2[idx] !== "not-empty" &&
        operatorValueOr2[idx] !== "mine" &&
        operatorValueOr2[idx] !== "not-mine" &&
        operatorValueOr2[idx] !== "today" &&
        operatorValueOr2[idx] !== "yesterday" &&
        operatorValueOr2[idx] !== "this-week" &&
        operatorValueOr2[idx] !== "last-week" &&
        operatorValueOr2[idx] !== "this-month" &&
        operatorValueOr2[idx] !== "last-month" &&
        operatorValueOr2[idx] !== "this-year"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      let data = {
        report_id: report_id,
        name: values.widget_name,
        measure: measure,
        filter_conditions: {
          and_filter: allCondition1,
          or_filter: anyCondition1,
        },
        is_static_value: typeWidget === "static" ? true : false,
        static_value: values.number || 0,
      };

      if (isCompared) {
        data = {
          ...data,
          show_diff_by: showPercent ? "percentage" : "default",
          is_show_icon_compare: showValue,
          compare_widget: {
            name: values.widget_name_2,
            measure: measure,
            filter_conditions: {
              and_filter: allCondition2,
              or_filter: anyCondition2,
            },
            report_id: report_id,
            is_static_value: typeWidget2 === "static" ? true : false,
            static_value: values.number2 || 0,
          },
        };
      }

      if (isEdit) {
        dispatch(
          updateWidget({
            ...data,
            _id: detailsWidget._id,
          })
        );
      } else {
        dispatch(
          addWidget({
            ...data,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    onHideModalWidget();
    form.resetFields();
    setIsCompared(false);
    setShowValue(false);
    setIsEdit(false);
    setTypeWidget("dynamic");
    setTypeWidget2("dynamic");
    setAllCondition1([]);
    setAllCondition2([]);
    setAnyCondition1([]);
    setAnyCondition2([]);
    dispatch(loadDetailsWidgetFail());
  };

  const measuresOptions = [
    {
      label: "Count",
      value: "count",
    },
  ];

  const measuresOptionsNumber = [
    {
      label: "Count",
      value: "count",
    },
    {
      label: "Sum",
      value: "sum",
    },
    {
      label: "Average",
      value: "average",
    },
    {
      label: "Min",
      value: "min",
    },
    {
      label: "Max",
      value: "max",
    },
  ];

  const handleSelectField = (value, option) => {
    setMeasure({
      ...measure,
      field_id: option.value,
      field_type: option.type,
      formula_type: option.formula_type,
      aggregation: "",
    });

    form.setFieldsValue({
      measure_method: undefined,
    });
  };

  const handleSelectMeasure = (value) => {
    setMeasure({
      ...measure,
      aggregation: value,
    });
  };

  const handleSelectType = (e) => {
    setTypeWidget(e.target.value);
  };

  const handleSelectType2 = (e) => {
    setTypeWidget2(e.target.value);
  };

  useEffect(() => {
    if (listFieldsObject.length > 0) {
      let tempField = [];
      listFieldsObject.forEach((item) => {
        if (Object.keys(item)[0] === "main_object") {
          item.main_object.sections.forEach((section) => {
            section.fields.forEach((field) => {
              if (!_.get(field, "encrypted", false)) {
                tempField.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  formula_type: !field?.in_words && (field?.formula_type || ""),
                });
              }
            });
          });
        }
      });
      setField(tempField);
    }
  }, [listFieldsObject]);

  useEffect(() => {
    if (isEdit && Object.keys(detailsWidget).length > 0) {
      form.setFieldsValue({
        widget_name: detailsWidget.name,
        measure_field: detailsWidget.measure.field_id,
        measure_method: detailsWidget.measure.aggregation,
        value_type: detailsWidget.is_static_value ? "static" : "dynamic",
        number: detailsWidget.static_value,
      });

      setTypeWidget(detailsWidget.is_static_value ? "static" : "dynamic");
      setAllCondition1(detailsWidget.filter_conditions.and_filter);
      setAnyCondition1(detailsWidget.filter_conditions.or_filter);

      if (detailsWidget.compare_widget !== undefined) {
        form.setFieldsValue({
          widget_name_2: detailsWidget.compare_widget.name,
          number2: detailsWidget.compare_widget.static_value,
          value_type_2: detailsWidget.compare_widget.is_static_value
            ? "static"
            : "dynamic",
        });

        setTypeWidget2(
          detailsWidget.compare_widget.is_static_value ? "static" : "dynamic"
        );

        setIsCompared(true);
        setShowValue(detailsWidget.is_show_icon_compare);
        setAllCondition2(
          detailsWidget.compare_widget.filter_conditions.and_filter
        );
        setAnyCondition2(
          detailsWidget.compare_widget.filter_conditions.or_filter
        );

        if (detailsWidget.show_diff_by === "percentage") {
          setShowPercent(true);
        } else {
          setShowPercent(false);
        }
      }

      setMeasure({
        aggregation: detailsWidget.measure.aggregation,
        field_id: detailsWidget.measure.field_id,
        field_type: detailsWidget.measure.field_type,
        formula_type: detailsWidget.measure.formula_type,
        type: "default",
      });
    }
  }, [detailsWidget, isEdit, form]);

  useEffect(() => {
    if (isLoading === false) {
      if (widgetInfo === "success" && showModalWidget) {
        if (isEdit === false) {
          loadWidgets();
          onHideModalWidget();
          form.resetFields();
          setIsCompared(false);
          setShowValue(false);
          Notification("success", "Create successfully!");
          dispatch(resetStatus());
          setTypeWidget("dynamic");
          setTypeWidget2("dynamic");
          setAllCondition1([]);
          setAllCondition2([]);
          setAnyCondition1([]);
          setAnyCondition2([]);
        }

        if (isEdit) {
          dispatch(
            getDataWidget({
              _id: detailsWidget._id,
              api_version: "2",
            })
          );
        }

        if (isEdit && isLoadingDataWidget === false) {
          setIsEdit(false);
          onHideModalWidget();
          form.resetFields();
          setIsCompared(false);
          setShowValue(false);
          Notification("success", "Update successfully!");
          dispatch(resetStatus());
          setTypeWidget("dynamic");
          setTypeWidget2("dynamic");
          setAllCondition1([]);
          setAllCondition2([]);
          setAnyCondition1([]);
          setAnyCondition2([]);
        }
      }

      if (widgetInfo !== null && widgetInfo !== "success") {
        Notification("error", widgetInfo);
        dispatch(resetStatus());
      }
    }
  }, [
    detailsWidget,
    dispatch,
    form,
    isEdit,
    isLoading,
    isLoadingDataWidget,
    loadWidgets,
    onHideModalWidget,
    setIsEdit,
    showModalWidget,
    widgetInfo,
  ]);

  return (
    <ModalCustom
      title={isEdit ? "Edit widget" : "Add widget"}
      visible={showModalWidget}
      footer={null}
      width={isCompared ? 1200 : 600}
      onCancel={_onCancel}
    >
      {isLoadingWidget ? (
        <WrapSpin>
          <Spin />
        </WrapSpin>
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
        >
          <Wrapcondition>
            <Widget1 isCompared={isCompared}>
              <Form.Item
                label="Widget name"
                name="widget_name"
                rules={[
                  { required: true, message: "Please input name of chart" },
                ]}
              >
                <Input placeholder="Please input" />
              </Form.Item>

              <Form.Item label="Value" name="value_type">
                <Radio.Group
                  options={optionsType}
                  onChange={handleSelectType}
                  optionType="button"
                  defaultValue="dynamic"
                />
              </Form.Item>

              {typeWidget === "static" && (
                <Form.Item
                  label="Number"
                  name="number"
                  rules={[{ required: true, message: "Please input number" }]}
                >
                  <InputNumber placeholder="Please input" />
                </Form.Item>
              )}

              {typeWidget === "dynamic" && (
                <>
                  <Conditions
                    title={"AND condition"}
                    decs={"(All conditions must be met)"}
                    conditions={allCondition1}
                    setConditions={setAllCondition1}
                    ID={
                      isEdit &&
                      Object.keys(detailsWidget).length > 0 &&
                      detailsWidget._id
                        ? detailsWidget._id
                        : ""
                    }
                    dataDetails={
                      isEdit && Object.keys(detailsWidget).length > 0
                        ? detailsWidget
                        : {}
                    }
                    operatorValue={operatorValueAnd1}
                    setOperatorValue={setOperatorValueAnd1}
                    value={valueAnd1}
                    setValue={setValueAnd1}
                  />
                  <Conditions
                    title={"OR condition"}
                    decs={"(Any conditions must be met)"}
                    conditions={anyCondition1}
                    setConditions={setAnyCondition1}
                    ID={
                      isEdit &&
                      Object.keys(detailsWidget).length > 0 &&
                      detailsWidget._id
                        ? detailsWidget._id
                        : ""
                    }
                    dataDetails={
                      isEdit && Object.keys(detailsWidget).length > 0
                        ? detailsWidget
                        : {}
                    }
                    operatorValue={operatorValueOr1}
                    setOperatorValue={setOperatorValueOr1}
                    value={valueOr1}
                    setValue={setValueOr1}
                  />
                  <FormValueSummary>
                    <FormValue
                      labelCol={{ span: 6 }}
                      wrapperCol={{ span: 18 }}
                      label="Field"
                      name="measure_field"
                      rules={[
                        {
                          required: true,
                          message: "Please select measure field",
                        },
                      ]}
                    >
                      <Select
                        placeholder="Select field"
                        options={field}
                        onChange={handleSelectField}
                        optionFilterProp="label"
                        showSearch
                      />
                    </FormValue>
                    <FormValue
                      label="Measure"
                      name="measure_method"
                      rules={[
                        {
                          required: true,
                          message: "Please select measure",
                        },
                      ]}
                    >
                      <Select
                        onChange={handleSelectMeasure}
                        placeholder="Select measure"
                        options={
                          measure.field_type === ""
                            ? []
                            : measure.field_type === "number" ||
                              measure.formula_type === "dateDiff" ||
                              measure.formula_type === "dateToParts" ||
                              measure.formula_type === "advanceExpression"
                            ? measuresOptionsNumber
                            : measuresOptions
                        }
                      />
                    </FormValue>
                  </FormValueSummary>
                </>
              )}

              <Checkbox checked={isCompared} onChange={handleSelectCompared}>
                Compared with
              </Checkbox>
            </Widget1>
            {isCompared && (
              <Widget2>
                <Form.Item
                  label="Widget name"
                  name="widget_name_2"
                  rules={[
                    { required: true, message: "Please input name of chart" },
                  ]}
                >
                  <Input />
                </Form.Item>

                <Form.Item label="Value" name="value_type_2">
                  <Radio.Group
                    options={optionsType}
                    onChange={handleSelectType2}
                    optionType="button"
                    defaultValue="dynamic"
                  />
                </Form.Item>

                {typeWidget2 === "static" && (
                  <Form.Item
                    label="Number"
                    name="number2"
                    rules={[{ required: true, message: "Please input number" }]}
                  >
                    <InputNumber placeholder="Please input" />
                  </Form.Item>
                )}

                {typeWidget2 === "dynamic" && (
                  <>
                    <Conditions
                      title={"AND condition"}
                      decs={"(All conditions must be met)"}
                      conditions={allCondition2}
                      setConditions={setAllCondition2}
                      ID={
                        isEdit &&
                        Object.keys(detailsWidget).length > 0 &&
                        detailsWidget._id
                          ? detailsWidget._id
                          : ""
                      }
                      dataDetails={
                        isEdit && Object.keys(detailsWidget).length > 0
                          ? detailsWidget.compare_widget
                          : {}
                      }
                      operatorValue={operatorValueAnd2}
                      setOperatorValue={setOperatorValueAnd2}
                      value={valueAnd2}
                      setValue={setValueAnd2}
                    />
                    <Conditions
                      title={"OR condition"}
                      decs={"(Any conditions must be met)"}
                      conditions={anyCondition2}
                      setConditions={setAnyCondition2}
                      ID={
                        isEdit &&
                        Object.keys(detailsWidget).length > 0 &&
                        detailsWidget._id
                          ? detailsWidget._id
                          : ""
                      }
                      dataDetails={
                        isEdit && Object.keys(detailsWidget).length > 0
                          ? detailsWidget.compare_widget
                          : {}
                      }
                      operatorValue={operatorValueOr2}
                      setOperatorValue={setOperatorValueOr2}
                      value={valueOr2}
                      setValue={setValueOr2}
                    />
                  </>
                )}

                <ShowCompare>
                  <Checkbox checked={showValue} onChange={handleShowValue}>
                    Show compare value
                  </Checkbox>

                  {showValue && (
                    <Checkbox
                      checked={showPercent}
                      onChange={handleShowPercent}
                    >
                      Percent
                    </Checkbox>
                  )}
                </ShowCompare>
              </Widget2>
            )}
          </Wrapcondition>

          <WrapButton label=" ">
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading || isLoadingDataWidget}
            >
              Save
            </Button>
            <Button onClick={_onCancel}>Cancel</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default ModalWidget;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-tag {
    margin-right: 17px;
    margin-bottom: 24px;
    padding: 0 0;

    :last-of-type {
      margin-right: 0;
    }
  }

  .ant-input-number {
    width: 100%;
  }

  .ant-tag-checkable:active {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Wrapcondition = styled.div`
  display: flex;
`;

const Widget1 = styled.div`
  border-right: ${(props) => (props.isCompared ? "1px solid #ececec" : "none")};
  width: ${(props) => (props.isCompared ? "50%" : "100%")};
  padding-right: ${(props) => (props.isCompared ? "24px" : "0")};
`;

const Widget2 = styled.div`
  width: 50%;
  padding-left: 24px;
`;

const ShowCompare = styled.div`
  display: flex;
  flex-direction: column;

  .ant-checkbox-wrapper {
    margin-bottom: 16px;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }
`;

const FormValueSummary = styled(Form.Item)`
  width: 100%;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }

  .ant-col {
    max-width: 100% !important;
  }
`;

const FormValue = styled(Form.Item)`
  margin-bottom: 0;
  width: 50%;
  margin-right: 16px;

  :last-child {
    margin-right: 0;
  }
`;
