import { useDispatch } from "react-redux";
import styled from "styled-components";
import { useParams } from "react-router";
import { useSelector } from "react-redux";

import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import Form from "antd/lib/form";
import { quickFilterFeat } from "redux/slices/reportDetails";
import { useTranslation, withTranslation } from "react-i18next";
import { checkConditions } from "util/helper";
import { Notification } from "components/Notification/Noti";

const ModalSaveFilter = ({
  open,
  $open,
  allCondition,
  operatorValueAnd,
  valueAnd,
  anyCondition,
  operatorValueOr,
  valueOr,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { recordID } = useParams();
  const { userDetail } = useSelector((state) => state.userReducer);

  const _onSubmit = (values) => {
    if (
      checkConditions(allCondition, operatorValueAnd, valueAnd) ||
      checkConditions(anyCondition, operatorValueOr, valueOr)
    ) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      dispatch(
        quickFilterFeat({
          report_type: "save",
          name: values.name,
          description: values.description,
          quick_filter: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
          report_id: recordID,
          user_id: userDetail._id,
        })
      );
      $open(false);
      form.resetFields();
    }
  };

  return (
    <CustomModal
      title={t("report.saveFilter")}
      visible={open}
      onCancel={() => {
        $open(false);
        form.resetFields();
      }}
      width={400}
      cancelText={t("common.cancel")}
      okText={t("common.save")}
      onOk={() => form.submit()}
    >
      <Form form={form} layout="vertical" onFinish={_onSubmit}>
        <Form.Item
          label={t("report.filterName")}
          name="name"
          rules={[
            {
              required: true,
              message: t("common.placeholderInput"),
            },
          ]}
          style={{ marginBottom: "16px" }}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label={t("common.description")}
          name="description"
          style={{ marginBottom: "0" }}
        >
          <Input />
        </Form.Item>
      </Form>
    </CustomModal>
  );
};

export default withTranslation()(ModalSaveFilter);

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36px;
    line-height: 36px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;
    margin-left: 16px;
    width: 130px;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
