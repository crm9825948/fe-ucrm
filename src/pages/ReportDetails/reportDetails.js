import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router";

import Breadcrumb from "antd/lib/breadcrumb";

import ModalChart from "./ModalChart";
import ModalExport from "./ModalExport";
import ModalWidget from "./ModalWidget";
import Widget from "components/Widget/widget";
import Charts from "components/Charts/charts";
import ShareReport from "pages/Report/ModalShareReport";
import { Notification } from "components/Notification/Noti";
import ReportSetting from "pages/Report/ModalReportSetting";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import DescriptionReport from "./DescriptionReport";
import DataReport from "./DataReport";
import { checkConditions, setOperatorCondition } from "util/helper";

import {
  setShowModalConfirmDelete,
  setShowLoadingScreen,
} from "redux/slices/global";

import {
  loadSpecificReport,
  loadInfoReport,
  unpinReport,
  pinToDashBoard,
  resetStatus,
  loadAllWidgets,
  loadAllCharts,
  deleteWidget,
  loadDetailsWidget,
  deleteChart,
  loadReportArithmetic,
  loadDetailsChart,
  changeState,
  loadDataNecessary,
  filterReport,
  unMountReportDetails,
  loadSpecificReportSuccess,
  loadTotalRecordsSuccess,
} from "redux/slices/reportDetails";
import {
  loadDetailsReport,
  setCurrentPage,
  loadFolders,
} from "redux/slices/report";
import { loadListObjectField } from "redux/slices/objects";

function ReportDetails(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { objectID, recordID } = useParams();

  const {
    loadingScreen,
    isLoading,
    infoReport,
    statusPin,
    tempListWidgets,
    tempListCharts,
    deleteInfo,
    loadingFilter,
  } = useSelector((state) => state.reportDetailsReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { isLoadingReport, reportDetails, currentPage, listFolders } =
    useSelector((state) => state.reportReducer);

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [showFilter, setShowFilter] = useState(false);
  const [showModalShare, setShowModalShare] = useState(false);
  const [showModalExport, setShowModalExport] = useState(false);
  const [showModalChart, setShowModalChart] = useState(false);
  const [showModalWidget, setShowModalWidget] = useState(false);
  const [showReportSetting, setShowReportSetting] = useState(false);
  const [isEdit, setIsEdit] = useState(false);

  const [columnReport, setColumnReport] = useState([]);

  // const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(1);
  // const [firstID, setFirstID] = useState(null);
  // const [lastID, setLastID] = useState(null);
  // const [next, setNext] = useState(0);
  // const [prev, setPrev] = useState(0);

  const [dataDelete, setDataDelete] = useState({});

  const [deleteType, setDeleteType] = useState("");

  const [tempAllCondition, setTempAllCondition] = useState([]);
  const [tempAnyCondition, setTempAnyCondition] = useState([]);
  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onShowFilter = () => {
    setShowFilter(true);
  };

  const _onCloseFilter = useCallback(() => {
    setShowFilter(false);
    setAllCondition(tempAllCondition);
    setAnyCondition(tempAnyCondition);
  }, [tempAllCondition, tempAnyCondition]);

  const _onShowModalShare = () => {
    setShowModalShare(true);
  };

  const _onHideModalShare = () => {
    setShowModalShare(false);
  };

  const _onShowModalExport = () => {
    setShowModalExport(true);
  };

  const _onHideModalExport = () => {
    setShowModalExport(false);
  };

  const _onShowModalChart = () => {
    setShowModalChart(true);
  };

  const _onHideModalChart = () => {
    setShowModalChart(false);
  };

  const _onShowModalWidget = () => {
    setShowModalWidget(true);
  };

  const _onHideModalWidget = () => {
    setShowModalWidget(false);
  };

  const _onShowReportSetting = () => {
    setIsEdit(true);
    setShowReportSetting(true);
  };

  const _onHideReportSetting = () => {
    setShowReportSetting(false);
  };

  const loadReport = useCallback(
    (first, last, isEdit) => {
      let data = {
        // current_page: isEdit ? 1 : currentPage,
        // record_per_page: isEdit ? 10 : recordPerPage,
        // first_record_id: firstID,
        // last_record_id: lastID,
        first_record_id: first,
        last_record_id: last,
        object_id: objectID,
        report_id: recordID,
        api_version: "2",
      };

      if (tempAllCondition.length > 0 || tempAnyCondition.length > 0) {
        dispatch(
          loadSpecificReport({
            ...data,
            quick_filter: {
              and_filter: tempAllCondition,
              or_filter: tempAnyCondition,
            },
          })
        );
      } else {
        dispatch(
          loadSpecificReport({
            ...data,
          })
        );
      }
    },
    [
      // firstID,
      // lastID,
      objectID,
      recordID,
      tempAllCondition,
      tempAnyCondition,
      dispatch,
    ]
  );

  const loadInforReport = useCallback(() => {
    dispatch(
      loadInfoReport({
        report_id: recordID,
        api_version: "2",
      })
    );
  }, [dispatch, recordID]);

  const _onFilter = (allConditions, anyConditions) => {
    const andConditionResult = allConditions ? allConditions : allCondition;
    const orConditionsResult = anyConditions ? anyConditions : anyCondition;
    const operatorValueAndResult = allConditions
      ? setOperatorCondition(allConditions, [], []).operatorVal
      : operatorValueAnd;
    const operatorValueOrResult = anyConditions
      ? anyConditions
      : operatorValueOr;
    const valueAndResult = allConditions
      ? setOperatorCondition(allConditions, [], []).valueArray
      : valueAnd;
    const valueOrResult = anyConditions ? anyConditions : valueOr;

    console.log("andConditionResult :>> ", andConditionResult);
    console.log("operatorValueAndResult :>> ", operatorValueAndResult);
    console.log("valueAndResult :>> ", valueAndResult);
    if (
      checkConditions(
        andConditionResult,
        operatorValueAndResult,
        valueAndResult
      ) ||
      checkConditions(orConditionsResult, operatorValueOrResult, valueOrResult)
    ) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      dispatch(setCurrentPage(1));
      setRecordPerPage(10);
      setTempAllCondition(andConditionResult);
      setTempAnyCondition(orConditionsResult);

      setColumnReport([]);
      dispatch(loadSpecificReportSuccess([]));
      dispatch(loadTotalRecordsSuccess(null));

      dispatch(
        filterReport({
          // current_page: 1,
          // record_per_page: 10,
          first_record_id: null,
          last_record_id: null,
          object_id: objectID,
          report_id: recordID,
          quick_filter: {
            and_filter: andConditionResult,
            or_filter: orConditionsResult,
          },
          api_version: "2",
        })
      );

      if (infoReport.report_info.report_type === "detail") {
        dispatch(
          loadReportArithmetic({
            quick_filter: {
              and_filter: andConditionResult,
              or_filter: orConditionsResult,
            },
            object_id: objectID,
            report_id: recordID,
            api_version: "2",
          })
        );
      }
    }
  };

  const _onPinReport = () => {
    if (infoReport.report_info.is_pinned_to_dashboard) {
      dispatch(
        unpinReport({
          report_id: infoReport.report_info._id,
        })
      );
    } else {
      dispatch(
        pinToDashBoard({
          report_id: infoReport.report_info._id,
          type: "list",
        })
      );
    }
  };

  const _onDeleteWidget = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDeleteType("widget");
    let tempDataDelete = {
      _id: id,
    };
    setDataDelete(tempDataDelete);
  };

  const _onDeleteChart = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDeleteType("chart");
    let tempDataDelete = {
      _id: id,
    };
    setDataDelete(tempDataDelete);
  };

  const _onEditWidget = (id) => {
    _onShowModalWidget();
    setIsEdit(true);

    dispatch(
      loadDetailsWidget({
        _id: id,
      })
    );
  };

  const _onEditChart = (id) => {
    _onShowModalChart();
    setIsEdit(true);

    dispatch(
      loadDetailsChart({
        _id: id,
      })
    );
  };

  const folder = {
    id: infoReport.report_info?.folder_id,
    name: infoReport.report_info?.folder_name,
  };

  // useEffect(() => {
  //   loadReport();
  // }, [loadReport]);

  useEffect(() => {
    loadReport(null, null);
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    loadInforReport();
  }, [loadInforReport]);

  const loadDetailReport = useCallback(() => {
    dispatch(
      loadDetailsReport({
        report_id: recordID,
        api_version: "2",
      })
    );
  }, [dispatch, recordID]);

  const loadWidgets = useCallback(() => {
    dispatch(
      loadAllWidgets({
        report_id: recordID,
      })
    );
  }, [dispatch, recordID]);

  const loadCharts = useCallback(() => {
    dispatch(
      loadAllCharts({
        report_id: recordID,
      })
    );
  }, [dispatch, recordID]);

  const loadData = () => {
    let tempPage = currentPage + 1;
    dispatch(setCurrentPage(tempPage));
  };

  useEffect(() => {
    loadDetailReport();
  }, [loadDetailReport]);

  useEffect(() => {
    loadWidgets();
  }, [loadWidgets]);

  useEffect(() => {
    loadCharts();
  }, [loadCharts]);

  useEffect(() => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectID,
        show_meta_fields: true,
      })
    );
  }, [dispatch, objectID]);

  useEffect(() => {
    if (Object.keys(infoReport).length > 0) {
      if (infoReport.report_info.report_type === "detail") {
        dispatch(
          loadReportArithmetic({
            object_id: objectID,
            report_id: recordID,
            api_version: "2",
          })
        );
      }
    }
  }, [dispatch, infoReport, objectID, recordID]);

  useEffect(() => {
    if (isLoading === false) {
      if (
        statusPin?.status === "unpin success" ||
        statusPin?.status === "pin success"
      ) {
        Notification(
          "success",
          statusPin?.status === "unpin success"
            ? "Unpin successfully!"
            : "Pin successfully!"
        );
      }

      if (
        (statusPin?.status === "unpin success" ||
          statusPin?.status === "pin success") &&
        statusPin?.type === "list"
      ) {
        dispatch(
          changeState({
            type: "pin",
          })
        );
        dispatch(resetStatus());
      }

      if (
        statusPin?.status !== undefined &&
        statusPin?.status !== "unpin success" &&
        statusPin?.status !== "pin success"
      ) {
        Notification("error", statusPin?.status);
        dispatch(resetStatus());
      }
    }
  }, [dispatch, statusPin, isLoading]);

  useEffect(() => {
    if (isLoading === false) {
      if (deleteInfo === "success widget" || deleteInfo === "success chart") {
        Notification("success", "Delete successfully!");
        dispatch(setShowModalConfirmDelete(false));

        if (deleteInfo === "success widget") {
          loadWidgets();
        }

        if (deleteInfo === "success chart") {
          loadCharts();
        }
        dispatch(resetStatus());
      }

      if (
        deleteInfo !== null &&
        deleteInfo !== "success widget" &&
        deleteInfo !== "success chart"
      ) {
        Notification("error", deleteInfo);
        dispatch(resetStatus());
      }
    }
  }, [dispatch, deleteInfo, isLoading, loadWidgets, loadCharts]);

  useEffect(() => {
    if (loadingScreen || isLoadingReport) {
      dispatch(setShowLoadingScreen(true));
    } else {
      setTimeout(() => {
        dispatch(setShowLoadingScreen(false));
      }, 500);
    }
  }, [loadingScreen, isLoadingReport, dispatch]);

  useEffect(() => {
    dispatch(loadDataNecessary());
    dispatch(loadFolders());
  }, [dispatch]);

  useEffect(() => {
    if (loadingFilter === "success") {
      _onCloseFilter();
      dispatch(resetStatus());
    }
  }, [loadingFilter, dispatch, _onCloseFilter]);

  useEffect(() => {
    return () => {
      dispatch(unMountReportDetails());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/report#my_folders")}>
          Report
        </Breadcrumb.Item>
        <Breadcrumb.Item
          onClick={() => {
            if (infoReport.report_info?.readonly) {
              navigate("/report#shared_folders");
            } else {
              navigate("/report#my_folders");
            }
          }}
        >
          {infoReport.report_info?.readonly ? "Shared folder" : "My folder"}
        </Breadcrumb.Item>
        <BreadcrumbItem>{infoReport.report_info?.report_name}</BreadcrumbItem>
      </Breadcrumb>

      <DescriptionReport
        infoReport={infoReport}
        showFilter={showFilter}
        allCondition={allCondition}
        setAllCondition={setAllCondition}
        anyCondition={anyCondition}
        setAnyCondition={setAnyCondition}
        _onShowModalChart={_onShowModalChart}
        _onShowModalWidget={_onShowModalWidget}
        _onShowReportSetting={_onShowReportSetting}
        _onPinReport={_onPinReport}
        _onShowModalExport={_onShowModalExport}
        _onShowModalShare={_onShowModalShare}
        _onFilter={_onFilter}
        _onShowFilter={_onShowFilter}
        _onCloseFilter={_onCloseFilter}
        operatorValueAnd={operatorValueAnd}
        setOperatorValueAnd={setOperatorValueAnd}
        valueAnd={valueAnd}
        setValueAnd={setValueAnd}
        operatorValueOr={operatorValueOr}
        setOperatorValueOr={setOperatorValueOr}
        valueOr={valueOr}
        setValueOr={setValueOr}
        listObjectField={listObjectField}
      />

      <DataReport
        infoReport={infoReport}
        loadData={loadData}
        setCurrentPage={setCurrentPage}
        setRecordPerPage={setRecordPerPage}
        currentPage={currentPage}
        recordPerPage={recordPerPage}
        columnReport={columnReport}
        setColumnReport={setColumnReport}
        objectID={objectID}
        recordID={recordID}
        loadReport={loadReport}
        tempAllCondition={tempAllCondition}
        tempAnyCondition={tempAnyCondition}
      />

      <ShareReport
        showModalShare={showModalShare}
        onHideModalShare={_onHideModalShare}
        listAllGroups={listAllGroups}
        reportDetails={reportDetails}
        isEdit={false}
      />

      <ModalExport
        showModalExport={showModalExport}
        onHideModalExport={_onHideModalExport}
        infoReport={infoReport}
        allCondition={tempAllCondition}
        anyCondition={tempAnyCondition}
      />

      <ModalChart
        showModalChart={showModalChart}
        onHideModalChart={_onHideModalChart}
        listFieldsObject={listObjectField}
        loadCharts={loadCharts}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        infoReport={infoReport}
        report_id={recordID}
      />

      <ModalWidget
        showModalWidget={showModalWidget}
        onHideModalWidget={_onHideModalWidget}
        loadWidgets={loadWidgets}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listFieldsObject={listObjectField}
        report_id={recordID}
      />

      <ReportSetting
        showReportSetting={showReportSetting}
        onHideReportSetting={_onHideReportSetting}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllGroups={listAllGroups}
        reportDetails={reportDetails}
        getListReport={loadDetailReport}
        loadReport={loadReport}
        loadInfoReport={() => {
          dispatch(
            loadInfoReport({
              report_id: recordID,
              api_version: "2",
            })
          );
        }}
        folder={folder}
        objects={category}
        loadWidgets={loadWidgets}
        loadCharts={loadCharts}
        setColumnReport={setColumnReport}
        listFolders={listFolders}
      />

      <WrapWidgets>
        {tempListWidgets.map((widget) => {
          return (
            <Widget
              widget={widget}
              allCondition={tempAllCondition}
              anyCondition={tempAnyCondition}
              _onDeleteWidget={_onDeleteWidget}
              _onEditWidget={_onEditWidget}
            />
          );
        })}
      </WrapWidgets>

      <WrapCharts>
        {tempListCharts.map((chart) => {
          return (
            <Charts
              chart={chart}
              allCondition={tempAllCondition}
              anyCondition={tempAnyCondition}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
            />
          );
        })}
      </WrapCharts>

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteType === "widget" ? deleteWidget : deleteChart}
        dataDelete={dataDelete}
        isLoading={isLoading}
      />
    </Wrapper>
  );
}

export default ReportDetails;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapWidgets = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const WrapCharts = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  grid-gap: 16px;
`;
