import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";

import { Notification } from "components/Notification/Noti";
import { exportReport, resetStatus } from "redux/slices/reportDetails";
import { FE_URL } from "constants/constants";

function ModalExport({
  showModalExport,
  onHideModalExport,
  infoReport,
  allCondition,
  anyCondition,
}) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { isLoading, statusExport } = useSelector(
    (state) => state.reportDetailsReducer
  );

  const fileType = [
    {
      label: "csv",
      value: "csv",
    },
    {
      label: "pdf",
      value: "pdf",
    },
    {
      label: "xlsx",
      value: "xlsx",
    },
  ];

  const _onSubmit = (values) => {
    dispatch(
      exportReport({
        export_file_type: values.file_type,
        report_id: infoReport.report_info?._id,
        quick_filter: {
          and_filter: allCondition,
          or_filter: anyCondition,
        },
      })
    );
  };

  const _onCancel = () => {
    onHideModalExport();
    form.resetFields();
  };

  const _onViewLog = () => {
    window.open(`${FE_URL}/export-history`);
  };

  useEffect(() => {
    if (isLoading === false && showModalExport) {
      if (statusExport === "success") {
        onHideModalExport();
        form.resetFields();

        Notification(
          "success",
          "This file is being processed. We will notify you when it completed!"
        );
        dispatch(resetStatus());
      }

      if (statusExport !== null && statusExport !== "success") {
        Notification("error", statusExport);
        dispatch(resetStatus());
      }
    }
  }, [
    dispatch,
    form,
    statusExport,
    isLoading,
    onHideModalExport,
    showModalExport,
  ]);

  return (
    <ModalCustom
      title={`Export report - ${infoReport.report_info?.report_name}`}
      visible={showModalExport}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label="Select file type"
          name="file_type"
          rules={[{ required: true, message: "Please select file type" }]}
        >
          <Select options={fileType} placeholder="Please select" />
        </Form.Item>

        <ViewLog>
          <Button onClick={_onViewLog}>View export log &gt;</Button>
        </ViewLog>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            Export
          </Button>
          <Button onClick={_onCancel}>Cancel</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalExport;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const ViewLog = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 42px;

  .ant-btn {
    display: flex;
    align-items: center;

    :hover {
      background: ${(props) => props.theme.darker};
      border: 1px solid ${(props) => props.theme.darker};

      span {
        color: #fff;
      }
    }
  }

  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;
