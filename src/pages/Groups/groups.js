import { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Button from "antd/lib/button";
import Table from "antd/lib/table";
import Pagination from "antd/lib/pagination";
import Breadcrumb from "antd/lib/breadcrumb";
import Tooltip from "antd/lib/tooltip";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import { Notification } from "components/Notification/Noti";
import ModalGroup from "./ModalGroup";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadDataNecessary,
  loadGroups,
  loadDetailsGroup,
  resetGroupInfo,
  loadAllGroups,
  deleteGroup,
} from "redux/slices/group";
import { changeTitlePage } from "redux/slices/authenticated";

function Groups(props) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { Column } = Table;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { isLoading, listGroups, listAllGroups, totalRecords, statusDelete } =
    useSelector((state) => state.groupReducer);

  const { listRole, listAllUser } = useSelector((state) => state.userReducer);

  const [showModalGroup, setShowModalGroup] = useState(false);

  const [tableGroup, setTableGroup] = useState([]);

  const [isEdit, setIsEdit] = useState(false);
  const [dataDelete, setDataDelete] = useState({});

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.group")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "group" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onShowModalGroup = () => {
    setShowModalGroup(true);
  };

  const _onHideModalGroup = () => {
    setShowModalGroup(false);
  };

  const _onEditGroup = (id) => {
    setIsEdit(true);
    setShowModalGroup(true);
    dispatch(
      loadDetailsGroup({
        group_id: id,
      })
    );
  };

  const _onDeleteGroup = (id) => {
    setDataDelete({
      group_id: id,
    });
    dispatch(setShowModalConfirmDelete(true));
  };

  const getListGroups = useCallback(() => {
    dispatch(
      loadGroups({
        CurrentPage: currentPage,
        RecordPerPage: recordPerPage,
      })
    );
  }, [dispatch, currentPage, recordPerPage]);

  const showTotal = () => {
    return `${t("common.total")} ${totalRecords} ${t("common.items")}`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    getListGroups();
  }, [getListGroups]);

  useEffect(() => {
    let tempList = [];

    listGroups.map((item) => {
      return tempList.push({
        key: item._id,
        name: item.name,
        description: item.description,
        action: "",
      });
    });
    setTableGroup(tempList);
  }, [listGroups]);

  useEffect(() => {
    if (!isLoading) {
      if (statusDelete === "success") {
        dispatch(setShowModalConfirmDelete(false));

        Notification("success", "Delete successfully!");
        dispatch(resetGroupInfo());
        getListGroups();
        dispatch(
          loadAllGroups({
            CurrentPage: 1,
            RecordPerPage: 1000,
          })
        );
      }

      if (statusDelete !== null && statusDelete !== "success") {
        Notification("error", statusDelete);
        dispatch(resetGroupInfo());
      }
    }
  }, [dispatch, getListGroups, isLoading, statusDelete]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.group")}</BreadcrumbItem>
        </Breadcrumb>
        {tableGroup.length > 0 && checkRule("create") && (
          <AddButton onClick={_onShowModalGroup}>
            + {t("group.addGroup")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {tableGroup.length > 0 ? (
        <WrapTable>
          <Table pagination={false} dataSource={tableGroup}>
            <Column
              title={t("group.groupName")}
              dataIndex="name"
              key="name"
              sorter={(a, b) => a.name.localeCompare(b.name)}
            />
            <Column
              title={t("common.description")}
              dataIndex="description"
              sorter={(a, b) => a.description?.localeCompare(b.description)}
              key="description"
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() => _onEditGroup(record.key)}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDeleteGroup(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={totalRecords}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </WrapTable>
      ) : (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("settings.group")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={_onShowModalGroup}>
              + {t("group.addGroup")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalGroup
        showModalGroup={showModalGroup}
        onHideModalGroup={_onHideModalGroup}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        getListGroups={getListGroups}
        listAllGroups={listAllGroups}
        listRole={listRole}
        listAllUser={listAllUser}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteGroup}
        dataDelete={dataDelete}
        isLoading={isLoading}
      />
    </Wrapper>
  );
}

export default withTranslation()(Groups);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-cell {
    :first-child {
      width: 50%;
    }

    :nth-child(2) {
      width: 30%;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
