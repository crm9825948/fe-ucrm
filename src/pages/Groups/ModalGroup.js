import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";

import { Notification } from "components/Notification/Noti";
import {
  createGroup,
  resetGroupInfo,
  updateGroup,
  loadAllGroups,
} from "redux/slices/group";

function ModalGroup({
  showModalGroup,
  onHideModalGroup,
  isEdit,
  setIsEdit,
  getListGroups,
  listAllGroups,
  listRole,
  listAllUser,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isLoading, groupInfo, details, isLoadingData } = useSelector(
    (state) => state.groupReducer
  );

  const [listMembers, setListMembers] = useState([]);

  useEffect(() => {
    let tempMembers = [];
    if (listAllGroups.length > 0) {
      let tempGroup = [];

      let tempListGroup = [...listAllGroups];
      if (isEdit) {
        tempListGroup = tempListGroup.filter(
          (group) => group._id !== details._id
        );
      }

      tempListGroup.forEach((item) => {
        tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });

      tempMembers.push({
        label: "Groups",
        options: tempGroup,
      });
    }
    if (listRole.length > 0) {
      let tempRole = [];
      listRole.map((item) => {
        return tempRole.push({
          label: item.Name,
          value: item._id,
        });
      });
      tempMembers.push({
        label: "Roles",
        options: tempRole,
      });
    }
    if (listAllUser.length > 0) {
      let tempUser = [];
      listAllUser.forEach((item) => {
        if (item.Active) {
          tempUser.push({
            label:
              item.Middle_Name === ""
                ? item.Last_Name + " " + item.First_Name
                : item.Last_Name +
                  " " +
                  item.Middle_Name +
                  " " +
                  item.First_Name,
            value: item._id,
          });
        }
      });
      tempMembers.push({
        label: "Users",
        options: tempUser,
      });
    }
    setListMembers(tempMembers);
  }, [listAllGroups, listRole, listAllUser, isEdit, details]);

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const checkRoleMember = (arr, value) => {
    if (value) {
      const result = arr.filter((item) => value.includes(item._id));
      return result.map((item) => {
        return item._id;
      });
    }
  };

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateGroup({
          active: true,
          _id: details._id,
          description: values.description,
          name: values.group_name.trim(),
          groups: checkRoleMember(listAllGroups, values.group_member) || [],
          roles: checkRoleMember(listRole, values.group_member) || [],
          users: checkRoleMember(listAllUser, values.group_member) || [],
        })
      );
    } else {
      dispatch(
        createGroup({
          active: true,
          description: values.description,
          name: values.group_name.trim(),
          groups: checkRoleMember(listAllGroups, values.group_member) || [],
          roles: checkRoleMember(listRole, values.group_member) || [],
          users: checkRoleMember(listAllUser, values.group_member) || [],
        })
      );
    }
  };

  const _onCancel = () => {
    onHideModalGroup();
    form.resetFields();
    setIsEdit(false);
  };

  useEffect(() => {
    if (isEdit && Object.keys(details).length > 0) {
      form.setFieldsValue({
        group_name: details.name,
        description: details.description,
        group_member: details.groups
          .concat(details.users)
          .concat(details.roles),
      });
    }
  }, [dispatch, form, isEdit, details]);

  useEffect(() => {
    if (isLoading === false) {
      if (groupInfo === "success") {
        onHideModalGroup();
        form.resetFields();
        getListGroups();
        setIsEdit(false);

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetGroupInfo());

        dispatch(
          loadAllGroups({
            CurrentPage: 1,
            RecordPerPage: 1000,
          })
        );
      }

      if (groupInfo !== null && groupInfo !== "success") {
        Notification("error", groupInfo);
        dispatch(resetGroupInfo());
      }
    }
  }, [
    dispatch,
    form,
    getListGroups,
    groupInfo,
    isLoading,
    onHideModalGroup,
    setIsEdit,
    isEdit,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("group.editGroup") : t("group.addGroup")}
      visible={showModalGroup}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      {isLoadingData ? (
        <WrapSpin>
          <Spin />
        </WrapSpin>
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("group.groupName")}
            name="group_name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("common.placeholderInput")} />
          </Form.Item>
          <Form.Item label={t("common.description")} name="description">
            <TextArea rows={6} placeholder={t("common.placeholderInput")} />
          </Form.Item>

          <Form.Item label={t("group.groupMember")} name="group_member">
            <Select
              options={listMembers}
              placeholder={t("common.placeholderSelect")}
              mode="multiple"
              optionFilterProp="label"
            />
          </Form.Item>

          <WrapButton label=" ">
            <Button type="primary" htmlType="submit" loading={isLoading}>
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalGroup);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;
