import React, { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Divider from "antd/lib/divider";
import _ from "lodash";

import {
  objectStandardCreate,
  objectStandardUpdate,
  objectStandardGetFields,
  objectStandardGetFieldsResult,
} from "redux/slices/objectStandard";
import {
  loadListObjectField,
  loadListObjectFieldSuccess,
} from "redux/slices/objects";

const convertedName = (value) => {
  switch (value) {
    case "Created_By":
      return "Created by";
    case "Created_Date":
      return "Created date";
    case "First_Name":
      return "First name";
    case "Is_Admin":
      return "Is admin";
    case "Last_Name":
      return "Last name";
    case "Middle_Name":
      return "Middle name";
    case "Modify_By":
      return "Modify by";
    case "Modify_Time":
      return "Modify time";
    case "Number_Of_Users":
      return "Number of users";
    case "Report_To":
      return "Report to";
    case "User_Role":
      return "User role";
    case "address":
      return "Address";
    case "birthday":
      return "Birthday";
    case "phone_number":
      return "Phone number";
    case "Is_deleted":
      return "Is deleted";
    default:
      return value;
  }
};

function ModalUser({
  showModalConfig,
  $showModalConfig,
  detailsConfig,
  $detailsConfig,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { listOS, fieldsOS } = useSelector(
    (state) => state.objectStandardReducer
  );
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [listObjects, setListObjects] = useState([]);
  const [listMainFields, setListMainFields] = useState([]);
  const [selectValues, $selectValues] = useState({});

  const _onSubmit = (values) => {
    const newValues = {
      ...values,
      name: undefined,
      object_nstd: undefined,
      object_std: undefined,
    };

    const field_mapping = _.chain(newValues)
      .omitBy(_.isUndefined)
      .map((value, key) => {
        return {
          field_nstd: key,
          field_std: value,
        };
      })
      .value();

    if (_.isEmpty(detailsConfig)) {
      dispatch(
        objectStandardCreate({
          name: _.get(values, "name"),
          object_nstd: _.get(values, "object_nstd"),
          object_std: _.get(values, "object_std"),
          field_mapping: field_mapping,
        })
      );
    } else {
      dispatch(
        objectStandardUpdate({
          setting_id: _.get(detailsConfig, "_id", ""),
          name: _.get(values, "name"),
          object_nstd: _.get(values, "object_nstd"),
          object_std: _.get(values, "object_std"),
          field_mapping: field_mapping,
        })
      );
    }
    _onCancel();
  };

  const _onCancel = () => {
    $showModalConfig(false);
    $selectValues({});
    $detailsConfig({});
    form.resetFields();
    dispatch(objectStandardGetFieldsResult({}));
    dispatch(loadListObjectFieldSuccess([]));
    //
  };

  const onLoadFields = (value) => {
    $selectValues({});

    Object.entries(fieldsOS).forEach(([key, value]) => {
      form.setFieldValue(key, undefined);
    });

    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
        show_meta_fields: true,
      })
    );
  };

  const onLoadFieldsOS = (value) => {
    dispatch(
      objectStandardGetFields({
        object_name: value,
      })
    );
  };

  const handleSelectChange = (selectName, value) => {
    $selectValues((prevValues) => ({
      ...prevValues,
      [selectName]: value,
    }));
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListMainFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (!_.isEmpty(detailsConfig)) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: _.get(detailsConfig, "object_std"),
          show_meta_fields: true,
        })
      );

      dispatch(
        objectStandardGetFields({
          object_name: _.get(detailsConfig, "object_nstd"),
        })
      );

      form.setFieldsValue({
        name: _.get(detailsConfig, "name"),
        object_nstd: _.get(detailsConfig, "object_nstd"),
        object_std: _.get(detailsConfig, "object_std"),
      });

      let tempSelectValues = {};

      _.map(_.get(detailsConfig, "field_mapping", []), (item) => {
        form.setFieldsValue({
          [item.field_nstd]: _.get(item, "field_std"),
        });

        tempSelectValues = {
          ...tempSelectValues,
          [item.field_nstd]: _.get(item, "field_std"),
        };
      });

      $selectValues(tempSelectValues);
    }
  }, [detailsConfig, form, dispatch]);

  return (
    <ModalCustom
      title={t("settings.objectStandard")}
      visible={showModalConfig}
      footer={null}
      width={700}
      onCancel={_onCancel}
    >
      <Form form={form} onFinish={_onSubmit} colon={false} layout="vertical">
        <Form.Item
          label={t("user.name")}
          name="name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label={t("objectStandard.chooseObject")}
              name="object_nstd"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                options={_.map(
                  listOS,
                  (item) =>
                    (item = {
                      label: item,
                      value: item,
                    })
                )}
                onChange={onLoadFieldsOS}
                placeholder={t("common.placeholderInput")}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={t("objectStandard.chooseObjectStandard")}
              name="object_std"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                options={listObjects}
                onChange={onLoadFields}
                placeholder={t("common.placeholderInput")}
              />
            </Form.Item>
          </Col>
        </Row>

        <Divider style={{ marginTop: 0 }} />

        <Fields>
          {Object.entries(fieldsOS).map(([key, value], index) => (
            <Field key={index}>
              <Form.Item label={convertedName(key)} name={key}>
                <Select
                  placeholder={t("common.placeholderInput")}
                  onChange={(value) => handleSelectChange(key, value)}
                  allowClear
                >
                  {_.map(
                    listMainFields.filter((field) => field.type === value),
                    (field, idx) => (
                      <Select.Option
                        key={idx}
                        value={field.value}
                        disabled={_.values(selectValues)?.includes(field.value)}
                      >
                        {field.label}
                      </Select.Option>
                    )
                  )}
                </Select>
              </Form.Item>
            </Field>
          ))}
        </Fields>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalUser);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Fields = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
`;

const Field = styled.div`
  width: calc(50% - 8px);
`;
