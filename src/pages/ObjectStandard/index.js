import styled from "styled-components";
import { useState, useEffect } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import Sync from "assets/icons/common/Sync.svg";
import SyncActive from "assets/icons/common/SyncActive.svg";
import ModalConfig from "./ModalConfig";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  objectStandardDelete,
  objectStandardSyncData,
  objectStandardGetObjects,
  objectStandardGetList,
} from "redux/slices/objectStandard";

function ObjectStandard() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { listSettingsOS } = useSelector(
    (state) => state.objectStandardReducer
  );
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [showModalConfig, $showModalConfig] = useState(false);
  const [detailsConfig, $detailsConfig] = useState({});
  const [dataSource, $dataSource] = useState([]);
  const [dataDelete, $dataDelete] = useState({});

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "object_standard" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onDeleteConfig = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    $dataDelete({ setting_id: id });
  };

  const _onSyncData = (id) => {
    dispatch(
      objectStandardSyncData({
        setting_id: id,
      })
    );
  };

  const columns = [
    {
      title: t("user.name"),
      dataIndex: "name",
      key: "name",
    },
    {
      title: t("object.object"),
      dataIndex: "object_nstd",
      key: "object_nstd",
    },
    {
      title: t("settings.objectStandard"),
      dataIndex: "object_name_std",
      key: "object_name_std",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record) => (
        <WrapAction>
          {checkRule("edit") && (
            <>
              <Tooltip title={t("ldap.sync")}>
                <img
                  onClick={() => {
                    _onSyncData(record.key);
                  }}
                  src={record.is_sync ? SyncActive : Sync}
                  alt="sync"
                />
              </Tooltip>
              <Tooltip title={t("common.edit")}>
                <img
                  onClick={() => {
                    $detailsConfig(record.data);
                    $showModalConfig(true);
                  }}
                  src={Edit}
                  alt="edit"
                  style={{ width: "20px" }}
                />
              </Tooltip>
            </>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => _onDeleteConfig(record.key)}
                src={Delete}
                alt="delete"
                style={{ width: "20px" }}
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  useEffect(() => {
    dispatch(objectStandardGetList());
    dispatch(objectStandardGetObjects());
  }, [dispatch]);

  useEffect(() => {
    const tempList = listSettingsOS.map((item) => {
      return {
        key: _.get(item, "_id", ""),
        object_nstd: _.get(item, "object_nstd", ""),
        object_name_std: _.get(item, "object_name_std", ""),
        name: _.get(item, "name", ""),
        is_sync: _.get(item, "is_sync", false),
        data: item,
      };
    });
    $dataSource(tempList);
  }, [listSettingsOS]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.objectStandard")}</BreadcrumbItem>
      </Breadcrumb>

      {_.isEmpty(listSettingsOS) ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("settings.objectStandard")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={() => $showModalConfig(true)}>
              + {t("objectStandard.createOS")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table columns={columns} dataSource={dataSource} pagination={false} />
        </WrapTable>
      )}

      <ModalConfig
        showModalConfig={showModalConfig}
        $showModalConfig={$showModalConfig}
        detailsConfig={detailsConfig}
        $detailsConfig={$detailsConfig}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={objectStandardDelete}
        dataDelete={dataDelete}
      />
    </Wrapper>
  );
}

export default withTranslation()(ObjectStandard);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-right: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
