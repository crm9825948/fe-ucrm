import { useEffect, useState, useRef } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";

import {
  updateFieldEmailTemplate,
  setShowDrawer,
  getEmailTempateByIdSuccess,
  updateEmailTemplate,
  saveEmailTemplate,
} from "redux/slices/emailTemplate";

import { loadListObjectFieldSuccess } from "redux/slices/objects";

import { generalInfo } from "util/staticData";
import Editor from "components/Editor/Editor2";

function ModalSetting({ listOptionObjectField }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const editorJodit = useRef(null);

  const { editEmailTemplate, showDrawer } = useSelector(
    (state) => state.emailTemplateReducer
  );

  const {
    object,
    selected_general_fields,
    description,
    email_template_name,
    subject,
    keep_original_subject,
    body,
    selected_fields,
  } = editEmailTemplate;
  const { listObject } = useSelector((state) => state.objectsManagementReducer);

  const [optionsAppend, setOptionsAppend] = useState([]);
  // const [deselect, setDeselect] = useState("");

  const _onSubmit = () => {
    if (editEmailTemplate._id) {
      dispatch(
        updateEmailTemplate({
          ...editEmailTemplate,
          email_template_name: editEmailTemplate.email_template_name.trim(),
          body: editorJodit.current.value,
        })
      );
    } else {
      dispatch(
        saveEmailTemplate({
          ...editEmailTemplate,
          email_template_name: editEmailTemplate.email_template_name.trim(),
          body: editorJodit.current.value,
        })
      );
    }
  };

  const _onCancel = () => {
    dispatch(setShowDrawer(false));
    setOptionsAppend([]);
    dispatch(loadListObjectFieldSuccess([]));
    dispatch(getEmailTempateByIdSuccess({}));
    form.resetFields();
  };

  const changeFieldEmailTemplate = (key, value) => {
    dispatch(
      updateFieldEmailTemplate({
        key: key,
        value: value,
      })
    );
  };

  // const handleChangeEditor = (value) => {
  //   changeFieldEmailTemplate("body", value);
  // };

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    listOptionObjectField.length > 0 &&
      selected_fields &&
      // eslint-disable-next-line
      selected_fields.map((item) => {
        listOptionObjectField.forEach((object) => {
          object.sections.forEach((section) => {
            if (section) {
              let temp = section.fields.filter(
                (x) =>
                  (x.full_field_id && x.full_field_id === item) ||
                  (!x.full_field_id && x.field_id === item)
              )[0];
              if (temp) {
                arr.push({
                  label: temp.related_name ? temp.related_name : temp.name,
                  value: item,
                });
              }
            }
          });
        });
      });
    // eslint-disable-next-line
    selected_general_fields &&
      // eslint-disable-next-line
      selected_general_fields.map((item) => {
        arr.push(generalInfo.filter((x) => x.value === item)[0]);
      });
    setOptionsAppend(arr);
    // eslint-disable-next-line
  }, [selected_general_fields, selected_fields, listOptionObjectField]);

  return (
    <ModalCustom
      title={
        editEmailTemplate._id
          ? t("emailTemplate.updateTemplate")
          : t("emailTemplate.addTemplate")
      }
      visible={showDrawer}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          rules={[
            {
              validator: (rule, value = email_template_name, cb) => {
                email_template_name.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          label={t("emailTemplate.templateName")}
          name="email_template_name"
          valuePropName={email_template_name}
        >
          <Input
            onChange={(e) =>
              changeFieldEmailTemplate("email_template_name", e.target.value)
            }
            value={email_template_name}
          />
        </Form.Item>

        <Form.Item
          label={t("common.description")}
          name="description"
          valuePropName={description}
        >
          <Input
            autoComplete={false}
            value={description}
            onChange={(e) =>
              changeFieldEmailTemplate("description", e.target.value)
            }
          />
        </Form.Item>

        <Form.Item
          label={t("object.object")}
          name="object"
          rules={[
            {
              validator: (rule, value = object, cb) => {
                object.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={object}
        >
          <Select
            value={object}
            disabled={editEmailTemplate._id ? true : false}
            onChange={(e) => {
              changeFieldEmailTemplate("object", e);
              if (e) {
                changeFieldEmailTemplate("selected_fields", []);
                changeFieldEmailTemplate("selected_general_fields", []);
                changeFieldEmailTemplate("subject", []);
              }
            }}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listObject &&
              // eslint-disable-next-line
              listObject.map((item) => {
                if (item.Status) {
                  return (
                    <Select.Option value={item._id}>{item.Name}</Select.Option>
                  );
                }
              })}
          </Select>
        </Form.Item>

        <Form.Item
          label={t("emailTemplate.selectedField")}
          rules={[
            {
              validator: (rule, value = selected_fields, cb) => {
                selected_fields.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          name={"selected_fields"}
          valuePropName={selected_fields}
        >
          <Select
            mode="multiple"
            style={{ width: "100%" }}
            value={selected_fields}
            onDeselect={(e) => {
              changeFieldEmailTemplate("body", body.replaceAll("$" + e, ""));
              let temp = [...subject];
              _.remove(temp, function (n) {
                return n === "$" + e;
              });
              changeFieldEmailTemplate("subject", temp);
              // setDeselect("$" + e);
            }}
            onChange={(e) => changeFieldEmailTemplate("selected_fields", e)}
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listOptionObjectField &&
              listOptionObjectField.map((object) => {
                let temp = [];
                return (
                  object.sections && (
                    <Select.OptGroup label={object.object_name}>
                      {
                        // eslint-disable-next-line
                        object.sections.map((section) => {
                          if (section && section.fields.length > 0) {
                            // eslint-disable-next-line
                            return section.fields.map((field) => {
                              if (
                                field.type !== "file" &&
                                field.type !== "formula" &&
                                field.type !== "dynamic" &&
                                !temp.includes(field.full_field_id) &&
                                !field.hidden
                              ) {
                                temp.push(field.full_field_id);
                                return (
                                  <Select.Option
                                    value={
                                      field.full_field_id
                                        ? field.full_field_id
                                        : field.field_id
                                    }
                                    key={
                                      field.full_field_id
                                        ? field.full_field_id
                                        : field.field_id
                                    }
                                  >
                                    {field.type && !object.main_object
                                      ? object.object_name + "." + field.name
                                      : field.name}
                                  </Select.Option>
                                );
                              }
                            });
                          }
                        })
                      }
                    </Select.OptGroup>
                  )
                );
              })}
          </Select>
        </Form.Item>
        <Form.Item
          label={t("emailTemplate.generalField")}
          rules={[
            {
              validator: (rule, value = selected_general_fields, cb) => {
                selected_general_fields.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          name="selected_general_fields"
          valuePropName={selected_general_fields}
        >
          <Select
            mode="multiple"
            style={{ width: "100%" }}
            value={selected_general_fields}
            onDeselect={(e) => {
              changeFieldEmailTemplate("body", body.replaceAll("$" + e, ""));
              let temp = [...subject];
              _.remove(temp, function (n) {
                return n === "$" + e;
              });
              changeFieldEmailTemplate("subject", temp);
              // setDeselect("$" + e);
            }}
            onChange={(e) =>
              changeFieldEmailTemplate("selected_general_fields", e)
            }
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {generalInfo.map((item) => {
              return (
                <Select.Option value={item.value}>{item.label}</Select.Option>
              );
            })}
          </Select>
        </Form.Item>

        <Form.Item
          label={t("emailTemplate.subject")}
          name="subject"
          valuePropName={subject}
          rules={[
            {
              validator: (rule, value = subject, cb) => {
                subject.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
        >
          <Select
            mode="tags"
            value={subject}
            onChange={(e) => changeFieldEmailTemplate("subject", e)}
          >
            {optionsAppend &&
              optionsAppend.map((item) => {
                return (
                  <Select.Option value={"$" + item.value}>
                    {item.label}
                  </Select.Option>
                );
              })}
          </Select>
        </Form.Item>

        <Form.Item
          name="keep_original_subject"
          valuePropName={keep_original_subject}
        >
          <Checkbox
            checked={keep_original_subject}
            onChange={(e) => {
              changeFieldEmailTemplate(
                "keep_original_subject",
                e.target.checked
              );
            }}
          >
            {t("emailTemplate.keepOriginalSubject")}
          </Checkbox>
        </Form.Item>

        <Form.Item name="body" valuePropName={body} wrapperCol="24">
          <Editor
            editorJodit={editorJodit}
            objectID={object}
            content={body}
            optionsAppend={optionsAppend}
            minHeightInput={"300px"}
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
