import { useEffect, useState } from "react";
import { useDispatch } from "redux/store";
import styled from "styled-components/macro";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import { Button, Table, Input, Space, Breadcrumb, Tooltip } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import {
  getEmailTempateById,
  getListEmailTemplate,
  updateFieldEmailTemplate,
  setShowDrawer,
  getEmailTempateByIdSuccess,
  deleteEmailTemplate,
  unmountEmailTemplate,
} from "redux/slices/emailTemplate";
import { loadListObjectField } from "redux/slices/objects";
import { setShowModalConfirmDelete } from "redux/slices/global";

import Edit from "assets/icons/common/edit.svg";
import deleteIcon from "assets/icons/email/deleteIcon.svg";
import emptyEmail from "assets/icons/email/empty-email.svg";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import ModalEmailTemplate from "./ModalEmailTemplate";
import { changeTitlePage } from "redux/slices/authenticated";

const EmailTemplate = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { listEmailTemplate, editEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { object, body, selected_fields } = editEmailTemplate;

  const [dataDelete, setDataDelete] = useState({});
  // eslint-disable-next-line
  const [searchText, setSearchText] = useState("");
  // eslint-disable-next-line
  const [searchedColumn, setSearchedColumn] = useState("");
  const [listOptionObjectField, setListOptionObjectField] = useState([]);

  useEffect(() => {
    dispatch(changeTitlePage(t("workflow.emailTemplate")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "template_email" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const editTemplate = (record) => {
    dispatch(
      getEmailTempateById({
        id: record._id,
      })
    );
  };

  const changeFieldEmailTemplate = (key, value) => {
    dispatch(
      updateFieldEmailTemplate({
        key: key,
        value: value,
      })
    );
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
          >
            {t("common.search")}
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
          >
            {t("common.reset")}
          </CustomButtonCancel>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Lọc dữ liệu
          </Button> */}
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    render: (text) =>
      //   searchedColumn === dataIndex ? (
      //     <Highlighter
      //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //       searchWords={[searchText]}
      //       autoEscape
      //       textToHighlight={text ? text.toString() : ''}
      //     />
      //   ) : (
      text,
    //   ),
  });

  const columns = [
    {
      title: t("emailTemplate.templateName"),
      dataIndex: "email_template_name",
      key: "email_template_name",
      ...getColumnSearchProps("email_template_name"),
    },
    {
      title: t("object.object"),
      dataIndex: "object",
      key: "object",
      ...getColumnSearchProps("object"),
    },
    {
      title: t("common.description"),
      dataIndex: "description",
      key: "description",
      ...getColumnSearchProps("description"),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "left",
      width: 150,
      render: (record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                style={{ width: "18px" }}
                onClick={() => editTemplate(record)}
                src={Edit}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => {
                  setDataDelete({
                    id: record._id,
                  });
                  dispatch(setShowModalConfirmDelete(true));
                }}
                src={deleteIcon}
                alt="delete"
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const addEmailTemplate = () => {
    dispatch(
      getEmailTempateByIdSuccess({
        body: "",
        description: "",
        email_template_name: "",
        object: "",
        selected_fields: [],
        selected_general_fields: [],
        subject: [],
        keep_original_subject: false,
      })
    );
    dispatch(setShowDrawer(true));
    setListOptionObjectField([]);
  };

  useEffect(() => {
    dispatch(getListEmailTemplate());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (object) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object,
        })
      );
    }
    //eslint-disable-next-line
  }, [object]);

  useEffect(() => {
    if (listObjectField.length > 0) {
      let arr = [];
      // eslint-disable-next-line
      listObjectField.map((item) => {
        Object.keys(item).forEach((key) => {
          let isFirst = true;
          let temp = { ...item[key] };
          let object_name = temp.object_name;
          // eslint-disable-next-line
          temp.sections =
            temp.sections &&
            // eslint-disable-next-line
            temp.sections.map((section) => {
              if (section.fields.length > 0) {
                let t = { ...section };
                let temp = t.fields.filter((x) => x.hidden);
                temp.forEach((x) => {
                  let t1 = [...selected_fields];
                  _.remove(t1, (y) => {
                    return y === x.full_field_id;
                  });
                  changeFieldEmailTemplate("selected_fields", t1);
                  changeFieldEmailTemplate(
                    "body",
                    body.replaceAll("$" + x.full_field_id, "")
                  );
                });
                if (key !== "main_object" && isFirst) {
                  isFirst = false;
                  t.fields = [
                    ...t.fields,
                    {
                      full_field_id: section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "created_date"
                          )
                        : "created_date",
                      name: object_name + ".Created date",
                    },
                    {
                      full_field_id: section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "created_by"
                          )
                        : "created_by",
                      name: object_name + ".Created by",
                    },
                    {
                      full_field_id: section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "modify_by"
                          )
                        : "modify_by",
                      name: object_name + ".Modified by",
                    },
                    {
                      full_field_id: section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "modify_date"
                          )
                        : "modify_date",
                      name: object_name + ".Modify date",
                    },
                    {
                      full_field_id: section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "owner"
                          )
                        : "owner",
                      name: object_name + ".Assign to",
                    },
                  ];
                }
                return t;
              }
            });
          temp.main_object = key === "main_object" ? true : false;
          arr.push(temp);
        });
      });
      setListOptionObjectField(arr);
    }
    // eslint-disable-next-line
  }, [listObjectField]);

  // unmount
  useEffect(
    () => () => {
      dispatch(unmountEmailTemplate());
    },
    // eslint-disable-next-line
    []
  );

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("emailTemplate.emailTemplate")}</BreadcrumbItem>
        </Breadcrumb>
        {listEmailTemplate.length > 0 && checkRule("create") && (
          <AddButton onClick={addEmailTemplate}>
            + {t("emailTemplate.addTemplate")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listEmailTemplate.length > 0 ? (
        <WrapTable>
          <Table
            columns={columns}
            dataSource={listEmailTemplate}
            pagination={{
              position: ["bottomRight"],
              showSizeChanger: true,
              defaultPageSize: 10,
              showQuickJumper: true,
              showTotal: () =>
                `${t("common.total")} ${listEmailTemplate.length} ${t(
                  "common.items"
                )}`,
            }}
          />
        </WrapTable>
      ) : (
        <Empty>
          <img src={emptyEmail} alt="empty" />
          <p>
            {t("object.noObject")}{" "}
            <span>{t("emailTemplate.emailTemplate")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={addEmailTemplate}>
              + {t("emailTemplate.addTemplate")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalEmailTemplate listOptionObjectField={listOptionObjectField} />

      <ModalConfimDelete
        title={t("emailTemplate.deleteTemplate")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteEmailTemplate}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};

export default EmailTemplate;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.darker};
  border-color: ${(props) => props.theme.darker};
  color: #fff;
  margin-right: 16px;

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  color: #000;

  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;
