import { Col, Row } from "antd";
import Breadcrumb from "antd/lib/breadcrumb";
import noData from "assets/images/objectsManagement/pic-empty.png";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { viewLogs } from "redux/slices/objects";
import styled from "styled-components";

const ViewLog = () => {
  const dispatch = useDispatch();
  const { logs } = useSelector((state) => state.objectsReducer);
  const navigate = useNavigate();
  const [content, setContent] = useState({});
  useEffect(() => {
    dispatch(viewLogs());
  }, [dispatch]);

  useEffect(() => {
    if (logs.length >= 1) {
      setContent(logs[0]);
    }
  }, [logs]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            Report
          </Breadcrumb.Item>
          <BreadcrumbItem>View import log</BreadcrumbItem>
        </Breadcrumb>
      </WrapBreadcrumb>
      {logs.length === 0 ? (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            width: "100%",
            justifyContent: "center",
            alignItems: "center",
            fontStyle: "normal",
            fontWeight: "normal",
            fontSize: "16px",
            lineHeight: "22px",
            color: "rgba(0, 0, 0, 0.85)",
          }}
        >
          <img
            alt=""
            src={noData}
            style={{
              width: "100px",
              height: "100px",
              marginBottom: "8px",
              marginTop: "51px",
            }}
          />
          Hiện chưa có import logs
        </div>
      ) : (
        <Row gutter={32}>
          <Col span={10} className="gutter-row">
            <CustomLeftCol>
              {logs.map((item, idx) => {
                return (
                  <div
                    className="item"
                    onClick={() => {
                      setContent(item);
                    }}
                  >
                    {item.name}
                  </div>
                );
              })}
            </CustomLeftCol>
          </Col>
          <CustomRightCol span={14} className="gutter-row">
            <div className="title">{content.name}</div>
            <div className="complete">{content.modified_date}</div>
            <div className="file-name">
              <span>File name:</span>
              {content.filename}
            </div>
            <div className="header">
              <div className="total">
                <span className="total-record">Total record:</span>
                <span style={{ fontWeight: "bold" }}>
                  {" "}
                  {content && content.total_records}
                </span>
              </div>
              <div className="insert item">
                Inserted records:{" "}
                <span
                  style={{
                    fontWeight: "bold",
                    color: "#096DD9",
                    marginLeft: "5px",
                  }}
                >
                  {content && content.inserted_records}
                </span>
              </div>
              <div className="modified item">
                Modified records:{" "}
                <span
                  style={{
                    fontWeight: "bold",
                    color: "#52C41A",
                    marginLeft: "5px",
                  }}
                >
                  {content && content.modified_records}
                </span>
              </div>
              <div className="error item">
                Error records:{" "}
                <span
                  style={{
                    fontWeight: "bold",
                    marginLeft: "5px",
                    color: "#CF1322",
                  }}
                >
                  {" "}
                  {content &&
                    content.error_records &&
                    content.error_records.length}
                </span>
              </div>
              <div className="error item">
                Skipped records:{" "}
                <span
                  style={{
                    fontWeight: "bold",
                    color: "#EC7302",
                    marginLeft: "5px",
                  }}
                >
                  {" "}
                  {content &&
                    content.skipped_records &&
                    content.skipped_records}
                </span>
              </div>
            </div>
            <div className="container-error">
              {content &&
                content.error_records &&
                content.error_records.map((item) => {
                  return (
                    <div
                      style={{ marginBottom: "1rem", fontSize: "15px" }}
                      className="import-item-error"
                    >
                      {item.row_data},{" "}
                      {item &&
                        item.error_message &&
                        typeof item.error_message.value === "object" &&
                        item.error_message.value &&
                        item.error_message.value.map((ele) => {
                          return ele;
                        })}
                      {item &&
                        item.error_message &&
                        typeof item.error_message.value === "string" &&
                        item.error_message.value}
                    </div>
                  );
                })}
              {content &&
              content.error_records &&
              content.error_records.length === 0 ? (
                <div className="import-item-error">
                  Đã hoàn thành nhập dữ liệu!
                </div>
              ) : (
                ""
              )}
            </div>
          </CustomRightCol>
        </Row>
      )}
    </Wrapper>
  );
};

export default ViewLog;

const Wrapper = styled.div`
  padding: 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomLeftCol = styled.div`
  background-color: white;
  padding: 16px;
  margin-top: 20px;
  height: calc(100vh - 180px);
  overflow-y: scroll;
  .item {
    /* :first-child {
      background-color: #e6f7ff;
      cursor: pointer;
    } */
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */
    box-shadow: inset 0px -1px 0px #f0f0f0;

    color: #2c2c2c;
    padding: 12px;
    :hover {
      background-color: #e6f7ff;
      cursor: pointer;
    }
  }
`;

const CustomRightCol = styled(Col)`
  background-color: white;
  padding: 16px;
  margin-top: 20px;
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 22px;
    line-height: 30px;
    /* identical to box height, or 136% */

    /* Character/Body text */

    color: #2c2c2c;
    margin-bottom: 4px;
  }
  .complete {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* text xám */

    color: #6b6b6b;
    border-bottom: 1px solid #ececec;
    padding-bottom: 14px;
  }
  .file-name {
    display: flex;
    align-items: center;
    font-style: normal;
    /* font-family: var(--roboto-700); */
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    display: flex;
    align-items: center;

    /* Character/Body text */

    color: #2c2c2c;
    margin-bottom: 16px;
    margin-top: 24px;

    span {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      display: flex;
      align-items: center;

      /* Character/Body text */

      color: #2c2c2c;
      margin-right: 10px;
    }
  }
  .total {
    display: flex;
  }
  .total-record {
    display: flex;
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    display: flex;
    align-items: center;

    /* Character/Body text */

    color: #2c2c2c;
    margin-right: 10px;
    margin-bottom: 8px;
  }
  .item {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    display: flex;
    align-items: center;

    /* text xám */

    color: #2c2c2c;
    margin-left: 16px;
    margin-bottom: 8px;
  }
  .import-item-error {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    display: flex;
    align-items: center;

    /* Character/Body text */

    color: #2c2c2c;
    margin-top: 16px;
  }
`;
