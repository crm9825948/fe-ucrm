import React, { useEffect, useState } from "react";
import changelog from "./version.md";
import ReactMarkdown from "react-markdown";

const Version = () => {
  const [text, setText] = useState("");
  useEffect(() => {
    fetch(changelog)
      .then((response) => response.text())
      .then((text) => {
        setText(text);
      });
  }, []);

  return (
    <>
      <div>
        <ReactMarkdown source={text}>{text}</ReactMarkdown>
      </div>
    </>
  );
};
export default Version;
