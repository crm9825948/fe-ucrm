# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [2.24.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.4...v2.24.0) (2022-11-08)

### Features

- [Workflow/Dynamic Button] Add action update related record source target ([69f0fdb](https://10.189.120.23/crm/ucrm-fe/commit/69f0fdbbb016ae9d3b26ee156276b47029d26f0b))
- add manual tasks in SMS campaign (Campaign) ([fbbc4bf](https://10.189.120.23/crm/ucrm-fe/commit/fbbc4bf15f7318fe71a4c281a2d8267e5a5fd111))

### Bug Fixes

- [Comment] Add click link, open modal when click image ([a3c6b54](https://10.189.120.23/crm/ucrm-fe/commit/a3c6b548c63176ffcc075060858731269a3b2525))
- [Email/SMS setting] Remove dup API ([73f27d5](https://10.189.120.23/crm/ucrm-fe/commit/73f27d5056c5d85f5c00fc7eda1a43caca5e95c9))
- [Rule Incoming Email] remove field type linkingobject with !key ([4e0c7ca](https://10.189.120.23/crm/ucrm-fe/commit/4e0c7ca8c54c7432d122c564a49a9d33ae656e10))
- [tronglv-fix] fix language in list view, improvement report ([469ebed](https://10.189.120.23/crm/ucrm-fe/commit/469ebed041d9bc363d4322b8608cea2515126292))
- [tronglv-update] update header object view ([ec52f2f](https://10.189.120.23/crm/ucrm-fe/commit/ec52f2fb7ae31f302fc1d45337fee470ba972045))
- [tronglv-update] update header object view and modal in dashboard ([da376a6](https://10.189.120.23/crm/ucrm-fe/commit/da376a6d134b86860ed2119ef234843b7c4dd5dc))
- [tronglv-update] update page title ([5cd024f](https://10.189.120.23/crm/ucrm-fe/commit/5cd024feefbea4fe21cba660a860920aa715bd36))
- add swicth source sharing ([4839c7b](https://10.189.120.23/crm/ucrm-fe/commit/4839c7b182179a08bacba06d3527a5e7434ea8c8))
- fix upload 50mb ([2cc9ef4](https://10.189.120.23/crm/ucrm-fe/commit/2cc9ef4ab576f49b17e6376037b740525a1253a6))

### [2.23.4](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.3...v2.23.4) (2022-11-03)

### Bug Fixes

- fix detail no data in consolidated view ([3c57e01](https://10.189.120.23/crm/ucrm-fe/commit/3c57e01d803810d6e0cbeb7de71c8117413b15d6))
- fix search, fix value compare in widget ([1f4f359](https://10.189.120.23/crm/ucrm-fe/commit/1f4f359cc72ae732fb6784094c13f90a46b2f834))

### [2.23.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.2...v2.23.3) (2022-11-01)

### Bug Fixes

- [tronglv-fix] hidden manmual sharing ([cb17e4c](https://10.189.120.23/crm/ucrm-fe/commit/cb17e4cb70fa0db1548842d45160437a14445610))
- [tronglv-fix] update language full project ([3550d65](https://10.189.120.23/crm/ucrm-fe/commit/3550d65c6748dd8f29d9fc004e79151e5ecddd1b))
- [tronglv-fix] update pagination Kanban ([69d7e6b](https://10.189.120.23/crm/ucrm-fe/commit/69d7e6baaf2c3e79b47001009eaef664d909a991))
- [tronglv-fix] update pagination report ([f06f1ae](https://10.189.120.23/crm/ucrm-fe/commit/f06f1ae7ffa5ef344d29c2f18797c4b9b3b3bd1a))
- [tronglv-update] update another in list view, consolidated view, component SMS ([819ee49](https://10.189.120.23/crm/ucrm-fe/commit/819ee49a86cd199e516708a1a1f4ea4dbbdf489a))
- [WIP] new pagi report ([1019dda](https://10.189.120.23/crm/ucrm-fe/commit/1019dda81c3d5acfd58fa0eee17f2d89895b2260))
- add new scroll in object, fix delete system field of campaign ([b6a0878](https://10.189.120.23/crm/ucrm-fe/commit/b6a087814ce7a45cd4f1804c1ad23db7c383e233))
- fix search owner, pagination listview ([1585d89](https://10.189.120.23/crm/ucrm-fe/commit/1585d89ff38691f870098742c8ecee3d2efccc34))
- hidden user filter ([c8f737f](https://10.189.120.23/crm/ucrm-fe/commit/c8f737f5920de5cd7413d6f7c956e3bca3506ee5))

### [2.23.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.23.1...v2.23.2) (2022-10-26)

### Bug Fixes

- [tronglv-fix] fix bug workflow schedule ([7b0ec6c](https://10.189.120.23/crm/ucrm-fe/commit/7b0ec6c53fe2c176c4197ac95913f4a0db6eeb89))
- [tronglv-fix] fix call api in calendar ([e8ddbb4](https://10.189.120.23/crm/ucrm-fe/commit/e8ddbb45c4a57d94223557447f4ff0c21a0fb421))
- [tronglv-fix] update list view, manage you account ([45ece56](https://10.189.120.23/crm/ucrm-fe/commit/45ece560d3560d8707497381afff69355696bd57))
- [tronglv-language] Language calendar ([07664fd](https://10.189.120.23/crm/ucrm-fe/commit/07664fd67e60beeb0254d230498450aceedf50dd))
- add auto reload listview, component Details when trigger Dynamic button (action update/create) ([e7c13b8](https://10.189.120.23/crm/ucrm-fe/commit/e7c13b8e714759fd65ea9598c2aca1a30595874e))
- add from name [Rule Email Incoming] ([110d5f7](https://10.189.120.23/crm/ucrm-fe/commit/110d5f72f48e017f674baf4f9929f0af84f83a7b))
- add metrics fields type details [Report setting] ([d984f63](https://10.189.120.23/crm/ucrm-fe/commit/d984f63165e2bf38f1307b931aff6e25fe3b7726))
- delete console log ([0233c2f](https://10.189.120.23/crm/ucrm-fe/commit/0233c2f00df1b798080d24e2ff860309e601f8f2))
- fix email template has been overwrite ([b9c21a5](https://10.189.120.23/crm/ucrm-fe/commit/b9c21a5d9b279b8abb61e67c03c27cd175d28c2c))
- fix type number + create report (measures) ([efea33b](https://10.189.120.23/crm/ucrm-fe/commit/efea33bffdd0974c10a66bd65f9fec2499d265d5))

### [2.23.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.7...v2.23.1) (2022-10-20)

### Bug Fixes

- [tronglv-fix] fix calendar, update customview, fix type light in kanban ([d97caa0](https://10.189.120.23/crm/ucrm-fe/commit/d97caa01828006d37b298c77231a1fdefbc54752))
- add check use cti before call get fields cti ([a2823cf](https://10.189.120.23/crm/ucrm-fe/commit/a2823cf82c56410776e85c6661bed385140cca29))
- add reset all [IC / CTI] ([e5b3d12](https://10.189.120.23/crm/ucrm-fe/commit/e5b3d123672b6277240553f1b14b80719abb02df))
- fix nodata in detail component ([072f01c](https://10.189.120.23/crm/ucrm-fe/commit/072f01c7a89136362445735304628939c5808a2e))
- UI (SLA / Users) ([0fbe298](https://10.189.120.23/crm/ucrm-fe/commit/0fbe298ba94be92b6995a11328fce8e8eeceda6d))

### Others

- **release:** 2.23.0 ([31ece3a](https://10.189.120.23/crm/ucrm-fe/commit/31ece3a95beafd8dc2090a9ff82dc3b68ab6ca44))

## [2.23.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.20.0...v2.23.0) (2022-10-19)

### Features

- [Emails component] Add auto detect email ([a3f20bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a3f20bcb09a88f7e1686cf83cf71fac66df3a00d))
- [tronglv-feature] component SMS ([51525d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/51525d94bae4c03931a8f1f80971232fee9a1a92))

### Bug Fixes

- [Action workflow/dynamic] Hide select record lookup fields ([cd43400](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cd434000f61aafbb4504c9536ed5afcb3159511d))
- [Campaign] Add check required fields ([73fdb2b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73fdb2b867a685c60a188c4080d03eb850ad49c3))
- [Campaign] Add nodata mailchimp campaign; Fix reload template SMS ([179f820](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/179f82094211898552a76206a440105cf1d9f6ad))
- [Campaign] Text ([3377d15](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3377d157a0f50495488337910370dc46a2b09bd9))
- [Comments] Lower case search ([3ce41b0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3ce41b09a5382315fd5a7e5df064a3fdf46a0ea1))
- [Email Incoming] Add select folder scan ([91a3a94](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/91a3a94aa58c02bdaf5b172d42e04d162f6c19a0))
- [Notifications] Limit noti ([bd00689](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bd006896b3056a5e368b151d0e933ae45d481c8c))
- [RoleBaseGuard] Rule 403 ([c8b82d1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c8b82d1e5078ed9bc3b1f096c7b0d54b9f956090))
- [Roles] Add expand all roles by default ([a86f37b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a86f37b82e51edf6e48cbe1304b6c0c46db154cb))
- [Sider] Fix collapse group menu ([bc46f63](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bc46f631c3317bcf515fa41d53894d8f46ccca8b))
- [SMS template] Duplicate fields meta data ([6c2ba17](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6c2ba17a521f6ffc90b4a69b278a434df74dff74))
- [Timeline] Add email IC; [Email] 500 when select template, nodata when filter ([37aea26](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/37aea2666d5607c56dc6bddc98ccd1942561b7a1))
- [tronglv-fix] edit component SMS ([0ce9380](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0ce93806c63bc2917449f235731e557ba53d05fd))
- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- [tronglv-fix] update duplicate rule ([80683f1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/80683f1a6069444047e7c1714352b23fab46bbd2))
- [tronglv-fix] update kanban view, list view, list view with detail, manual sharing ([754771f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/754771fddc7ddf3bfdd93c8fa75889d4ea197ce7))
- [tronglv-fixbug] fix bug kanban ([c97580a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c97580abfa18d163500362b3a35bb8672a5eb8ca))
- [URL Popup setting] Add display input params ([f556c98](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f556c9871aff9bb01632b74d87a75d30cfbfcf3f))
- [Users] Add quick filter ([4e0b4d2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4e0b4d214e8c62f1746ff9e7800306b24df33f08))
- [Widget] Add responsive widget in Dashboard ([4450c60](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4450c60193be29ee99a8bbc955a48765ce9c6cfe))
- add modal check scan ([a4b5b85](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a4b5b8534c6d35a60487c083b471b9eadfb2b776))
- add new version page ([4955e18](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4955e189eb077099f7bf40f26aea363ac71cc30c))
- change API mass delete campaign ([fbe5b81](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change celery check ([9860e75](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9860e75fa1e1bd553643c7975b9434f0a151445f))
- change flow find auto detect email ([283c87f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- change modal folder scan [Email Incoming] ([07b9665](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/07b966597974640918eaf50e76053b9efaad4ddf))
- current page search users ([c43aeec](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c43aeecad9549ff353eb25799644cc875c7cc2b5))
- display JSON modal Check scan ([1ef0dde](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1ef0ddedcd2c54678036cf244e24c9fe1a0e1f85))
- fix file in table of consolidated view ([36132f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36132f0d4f9fd2e04f51b95e9616295ee83a6aa0))
- fix linking ([d3b46e8](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d3b46e8fd78e41b1aef405445d4b032a08577368))
- fix linking ([2ec564c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2ec564ccb9272e7e5b8a7183b66e1f2e84563421))
- fix table in consolidated view ([8314c1c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8314c1c43a9d365495363ba52179495222ab04e4))
- fix UI Comment ([f9c2ece](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f9c2ece9bd37f5062b00d8abcb812327c9780de6))
- fix user in detail consolidated view ([8948adb](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8948adbb18275a7599f7a7b13fde0b2191466d79))
- improve performance Pagination for listview ([5851681](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5851681c1e371e73142c545b398f6a6f8cb492d0))
- pagination new in consolidated view ([73c593b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/73c593b4cc2546789d44fa302ca61a66d75f0839))
- revert style widget ([a398401](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### Code Refactoring

- [Email] Change action email from dropdown to icons ([9bdd392](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9bdd3924c323c5cee78414f9991306da77b154f9))
- [Report] Change style of widgets and charts ([5ad68aa](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5ad68aa9c3832a9ea4d872f18971b3ca8e573ea2))

### Others

- **release:** 2.21.0 ([2f8764b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2f8764b6740dafeba5c25a5467600b06765d4be4))
- **release:** 2.22.0 ([1b0df3e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1b0df3ec806313d455cce3aa5cbdfe8b1eb4fcf8))
- **release:** 2.22.1 ([172726a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/172726a2150bd2151782703bfe362865de53351c))
- **release:** 2.22.2 ([c982d8a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c982d8a1ee89caac7431eb31820d9a5496fffe2c))
- **release:** 2.22.3 ([5465250](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5465250fc3a157ca3b3b53a11029edc7a8990db6))
- **release:** 2.22.4 ([bc1475c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bc1475cfbdc27247983f1ef058b7fcf547540f61))
- **release:** 2.22.5 ([bd873e6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/bd873e61d49c879c8e211d0677fc1555f77a1610))
- **release:** 2.22.6 ([36341a0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/36341a08c43fbb9c10ebac86e4438a1d30a7c6e5))
- **release:** 2.22.7 ([4a4074e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4a4074ef7dbba3d4386a2f8930e90c4eaf76a741))

### [2.22.7](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.6...v2.22.7) (2022-10-18)

### Bug Fixes

- change modal folder scan [Email Incoming] ([07b9665](https://10.189.120.23/crm/ucrm-fe/commit/07b966597974640918eaf50e76053b9efaad4ddf))
- fix user in detail consolidated view ([8948adb](https://10.189.120.23/crm/ucrm-fe/commit/8948adbb18275a7599f7a7b13fde0b2191466d79))

### [2.22.6](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.5...v2.22.6) (2022-10-18)

### [2.22.5](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.4...v2.22.5) (2022-10-17)

### Bug Fixes

- [Email Incoming] Add select folder scan ([91a3a94](https://10.189.120.23/crm/ucrm-fe/commit/91a3a94aa58c02bdaf5b172d42e04d162f6c19a0))
- add modal check scan ([a4b5b85](https://10.189.120.23/crm/ucrm-fe/commit/a4b5b8534c6d35a60487c083b471b9eadfb2b776))
- change celery check ([9860e75](https://10.189.120.23/crm/ucrm-fe/commit/9860e75fa1e1bd553643c7975b9434f0a151445f))
- current page search users ([c43aeec](https://10.189.120.23/crm/ucrm-fe/commit/c43aeecad9549ff353eb25799644cc875c7cc2b5))
- display JSON modal Check scan ([1ef0dde](https://10.189.120.23/crm/ucrm-fe/commit/1ef0ddedcd2c54678036cf244e24c9fe1a0e1f85))
- fix linking ([d3b46e8](https://10.189.120.23/crm/ucrm-fe/commit/d3b46e8fd78e41b1aef405445d4b032a08577368))

### [2.22.4](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.3...v2.22.4) (2022-10-13)

### Bug Fixes

- [Timeline] Add email IC; [Email] 500 when select template, nodata when filter ([37aea26](https://10.189.120.23/crm/ucrm-fe/commit/37aea2666d5607c56dc6bddc98ccd1942561b7a1))
- [tronglv-fix] update kanban view, list view, list view with detail, manual sharing ([754771f](https://10.189.120.23/crm/ucrm-fe/commit/754771fddc7ddf3bfdd93c8fa75889d4ea197ce7))
- [tronglv-fixbug] fix bug kanban ([c97580a](https://10.189.120.23/crm/ucrm-fe/commit/c97580abfa18d163500362b3a35bb8672a5eb8ca))
- fix file in table of consolidated view ([36132f0](https://10.189.120.23/crm/ucrm-fe/commit/36132f0d4f9fd2e04f51b95e9616295ee83a6aa0))
- fix UI Comment ([f9c2ece](https://10.189.120.23/crm/ucrm-fe/commit/f9c2ece9bd37f5062b00d8abcb812327c9780de6))

### [2.22.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.1...v2.22.3) (2022-10-12)

### Bug Fixes

- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://10.189.120.23/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- [tronglv-fix] update duplicate rule ([80683f1](https://10.189.120.23/crm/ucrm-fe/commit/80683f1a6069444047e7c1714352b23fab46bbd2))
- add new version page ([4955e18](https://10.189.120.23/crm/ucrm-fe/commit/4955e189eb077099f7bf40f26aea363ac71cc30c))
- change API mass delete campaign ([fbe5b81](https://10.189.120.23/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change flow find auto detect email ([283c87f](https://10.189.120.23/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- fix table in consolidated view ([8314c1c](https://10.189.120.23/crm/ucrm-fe/commit/8314c1c43a9d365495363ba52179495222ab04e4))
- revert style widget ([a398401](https://10.189.120.23/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://10.189.120.23/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### Others

- **release:** 2.22.2 ([c982d8a](https://10.189.120.23/crm/ucrm-fe/commit/c982d8a1ee89caac7431eb31820d9a5496fffe2c))

### [2.22.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.1...v2.22.2) (2022-10-11)

### Bug Fixes

- [tronglv-fix] fix bug Component SMS ([f56fb5d](https://10.189.120.23/crm/ucrm-fe/commit/f56fb5d3a6d3b7b42c603cfffeb3254acd5592b0))
- change API mass delete campaign ([fbe5b81](https://10.189.120.23/crm/ucrm-fe/commit/fbe5b8162b1b423e160b63140faa3db5cadfd039))
- change flow find auto detect email ([283c87f](https://10.189.120.23/crm/ucrm-fe/commit/283c87f922fe48ee1c7cc8b9d62538065dd9f38a))
- revert style widget ([a398401](https://10.189.120.23/crm/ucrm-fe/commit/a398401eb3f39d7602af674f68707a5c0c3957da))
- tronglv-update related object ([2166ee0](https://10.189.120.23/crm/ucrm-fe/commit/2166ee0f2a831e536d461830523fa34074eaf1a4))

### [2.22.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.22.0...v2.22.1) (2022-10-07)

### Bug Fixes

- [Action workflow/dynamic] Hide select record lookup fields ([cd43400](https://10.189.120.23/crm/ucrm-fe/commit/cd434000f61aafbb4504c9536ed5afcb3159511d))
- [Campaign] Add check required fields ([73fdb2b](https://10.189.120.23/crm/ucrm-fe/commit/73fdb2b867a685c60a188c4080d03eb850ad49c3))
- [tronglv-fix] edit component SMS ([0ce9380](https://10.189.120.23/crm/ucrm-fe/commit/0ce93806c63bc2917449f235731e557ba53d05fd))
- [URL Popup setting] Add display input params ([f556c98](https://10.189.120.23/crm/ucrm-fe/commit/f556c9871aff9bb01632b74d87a75d30cfbfcf3f))
- [Widget] Add responsive widget in Dashboard ([4450c60](https://10.189.120.23/crm/ucrm-fe/commit/4450c60193be29ee99a8bbc955a48765ce9c6cfe))
- fix linking ([2ec564c](https://10.189.120.23/crm/ucrm-fe/commit/2ec564ccb9272e7e5b8a7183b66e1f2e84563421))

## [2.22.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.21.0...v2.22.0) (2022-10-06)

### Features

- [Emails component] Add auto detect email ([a3f20bc](https://10.189.120.23/crm/ucrm-fe/commit/a3f20bcb09a88f7e1686cf83cf71fac66df3a00d))
- [tronglv-feature] component SMS ([51525d9](https://10.189.120.23/crm/ucrm-fe/commit/51525d94bae4c03931a8f1f80971232fee9a1a92))

### Bug Fixes

- [Campaign] Add nodata mailchimp campaign; Fix reload template SMS ([179f820](https://10.189.120.23/crm/ucrm-fe/commit/179f82094211898552a76206a440105cf1d9f6ad))
- [Comments] Lower case search ([3ce41b0](https://10.189.120.23/crm/ucrm-fe/commit/3ce41b09a5382315fd5a7e5df064a3fdf46a0ea1))
- [Notifications] Limit noti ([bd00689](https://10.189.120.23/crm/ucrm-fe/commit/bd006896b3056a5e368b151d0e933ae45d481c8c))
- [RoleBaseGuard] Rule 403 ([c8b82d1](https://10.189.120.23/crm/ucrm-fe/commit/c8b82d1e5078ed9bc3b1f096c7b0d54b9f956090))
- [Roles] Add expand all roles by default ([a86f37b](https://10.189.120.23/crm/ucrm-fe/commit/a86f37b82e51edf6e48cbe1304b6c0c46db154cb))
- [Sider] Fix collapse group menu ([bc46f63](https://10.189.120.23/crm/ucrm-fe/commit/bc46f631c3317bcf515fa41d53894d8f46ccca8b))
- [SMS template] Duplicate fields meta data ([6c2ba17](https://10.189.120.23/crm/ucrm-fe/commit/6c2ba17a521f6ffc90b4a69b278a434df74dff74))
- [Users] Add quick filter ([4e0b4d2](https://10.189.120.23/crm/ucrm-fe/commit/4e0b4d214e8c62f1746ff9e7800306b24df33f08))
- pagination new in consolidated view ([73c593b](https://10.189.120.23/crm/ucrm-fe/commit/73c593b4cc2546789d44fa302ca61a66d75f0839))

## [2.21.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.20.0...v2.21.0) (2022-10-05)

### Features

- [tronglv-feature] Manual sharing ([e41cb3a](https://10.189.120.23/crm/ucrm-fe/commit/e41cb3aeedbf7805e309474c37cd87bb9ca664fc))
- [tronglv-feature] Manual sharing ([216fc8d](https://10.189.120.23/crm/ucrm-fe/commit/216fc8d9bbf1c5908a3e4fc7004e84454f0ba2d2))
- campaign with mailchimp ([9c87a87](https://10.189.120.23/crm/ucrm-fe/commit/9c87a874ac2dcb94c95728b6ebc920b93ea80157))

### Bug Fixes

- (Action update related record) Select value linking ([647ab4e](https://10.189.120.23/crm/ucrm-fe/commit/647ab4ed85599833206db9b132b4c2cc01bb45d9))
- (Charts) Add no data Pie/Line chart ([bc7c7cc](https://10.189.120.23/crm/ucrm-fe/commit/bc7c7ccd7f598ed6e3f055b607e38d4a92c539dc))
- (Expose API create/update) Add copy payload ([0a8f380](https://10.189.120.23/crm/ucrm-fe/commit/0a8f380cf43f5cae1f1f68da430dd235a8576d4f))
- (SMS Template) display object ID when edit template ([72dcbf2](https://10.189.120.23/crm/ucrm-fe/commit/72dcbf2ddc4f85903e20a334e1da3f9a20bc1497))
- [Campaign] Text ([3377d15](https://10.189.120.23/crm/ucrm-fe/commit/3377d157a0f50495488337910370dc46a2b09bd9))
- add check object null ([fb4a4a6](https://10.189.120.23/crm/ucrm-fe/commit/fb4a4a63e061b90f5a19e471c4c939843e7b49fb))
- call API load pagi paralel ([acfea69](https://10.189.120.23/crm/ucrm-fe/commit/acfea690ef334a1d7b180737f55e5ffb58d92739))
- file env ([4e1e635](https://10.189.120.23/crm/ucrm-fe/commit/4e1e635e8a0c37ebfda9c467a9d279220c2364e2))
- improve performance Pagination for listview ([5851681](https://10.189.120.23/crm/ucrm-fe/commit/5851681c1e371e73142c545b398f6a6f8cb492d0))
- load active campaign ([3969eea](https://10.189.120.23/crm/ucrm-fe/commit/3969eea7eb04c321e3759eadbf771a34df0f0844))
- redirect noti ([e6953db](https://10.189.120.23/crm/ucrm-fe/commit/e6953dbe92e441802febb89822001a1d300ffacd))
- required conditions SLA ([f1cf533](https://10.189.120.23/crm/ucrm-fe/commit/f1cf533ecee447d45fa8d6f78f22e1c591341c6e))
- required field has change workflows ([ec20e2d](https://10.189.120.23/crm/ucrm-fe/commit/ec20e2d3b6feadc3ea49efb3f4d144d5a333e3d7))

### Code Refactoring

- [Email] Change action email from dropdown to icons ([9bdd392](https://10.189.120.23/crm/ucrm-fe/commit/9bdd3924c323c5cee78414f9991306da77b154f9))
- [Report] Change style of widgets and charts ([5ad68aa](https://10.189.120.23/crm/ucrm-fe/commit/5ad68aa9c3832a9ea4d872f18971b3ca8e573ea2))
- change style of component Email ([69eaee8](https://10.189.120.23/crm/ucrm-fe/commit/69eaee8ab148eecbf6b8830aae795e21cb10f7c0))

## [2.20.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.19.0...v2.20.0) (2022-09-28)

### Features

- [tronglv-feature] Workflow - Action thêm event vào calendar ([470ab1c](https://10.189.120.23/crm/ucrm-fe/commit/470ab1c3dba2e27b1a2a538c9ec011e72777ddd0))

### Bug Fixes

- add required from config email ([b6cf223](https://10.189.120.23/crm/ucrm-fe/commit/b6cf223253174605f4b9d49bdb85277d1c3f871b))
- add type file in mapping IC and Expose API create ([959c2d6](https://10.189.120.23/crm/ucrm-fe/commit/959c2d66a090e9dcf869d23a4144acb14daf415d))
- clear related object (Action workflow) ([d22a53b](https://10.189.120.23/crm/ucrm-fe/commit/d22a53b9c889662c99289671d7c3757097356fad))
- edit action update related record 500 ([efb59e2](https://10.189.120.23/crm/ucrm-fe/commit/efb59e21da4d94218b02367daf0d77f968aebb98))
- query user in comment ([0cb2c2a](https://10.189.120.23/crm/ucrm-fe/commit/0cb2c2a93362a34d864fdd26611414bdd42087d8))

## [2.19.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.18.1...v2.19.0) (2022-09-23)

### Features

- new campaign ([cc7992b](https://10.189.120.23/crm/ucrm-fe/commit/cc7992bb8b547c43beb4ef49ae1870c2225db50b))
- open component campaign ([709d624](https://10.189.120.23/crm/ucrm-fe/commit/709d6249b656dbc44647531248e7e7f479df62de))

### Bug Fixes

- change BASE URL API ([8f59087](https://10.189.120.23/crm/ucrm-fe/commit/8f59087358715cc45d8eff1e3103dab0e5d218e7))
- hot fix page 500 in lisview because highlighter ([2f12ff1](https://10.189.120.23/crm/ucrm-fe/commit/2f12ff1a44fc2a539d300cbd6efaa1de9267e0fb))

### [2.18.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.18.0...v2.18.1) (2022-09-22)

### Bug Fixes

- fix 403 forbiden ([7252c82](https://10.189.120.23/crm/ucrm-fe/commit/7252c828d8896aa0c6534a5730eb7b3e852f7428))
- fix bug can not copy content in third party table ([6c7d3c2](https://10.189.120.23/crm/ucrm-fe/commit/6c7d3c20bbeba7463df867c52b688b46ee229baa))

## [2.18.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.17.1...v2.18.0) (2022-09-22)

### Features

- action update related record ([31753c0](https://10.189.120.23/crm/ucrm-fe/commit/31753c01dda6340b862c1fc7190465c86ddc82d0))

### Bug Fixes

- [tronglv-fix bug] fix bug kanban view ([a12fe71](https://10.189.120.23/crm/ucrm-fe/commit/a12fe719074fbcfbf2d3dbdb61e0b95dd74363d0))
- add fix config sso ([739a1bd](https://10.189.120.23/crm/ucrm-fe/commit/739a1bd9b4d741f2f4b06c43d7518f48d593e35b))
- add highlight words after search in listview ([62b3293](https://10.189.120.23/crm/ucrm-fe/commit/62b329327eeecad5df64f7e677ce3fb8f42ca4db))
- hot fix file type upload ([4ffdfe0](https://10.189.120.23/crm/ucrm-fe/commit/4ffdfe0dca26ec95d5ae0d83295fa00911075322))

### [2.17.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.17.0...v2.17.1) (2022-09-21)

### Bug Fixes

- hot fix sso config ([40e77d7](https://10.189.120.23/crm/ucrm-fe/commit/40e77d73108e93bb95bc174afbab567864475fb5))

## [2.17.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.14.0...v2.17.0) (2022-09-20)

### Features

- add duplicate rule SLA ([d23cfc0](https://10.189.120.23/crm/ucrm-fe/commit/d23cfc0516a6e6b7c8f96cff9de6d06d33ea9b80))
- add on/off linking ([82c64f0](https://10.189.120.23/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- click to call in listview ([0bc8031](https://10.189.120.23/crm/ucrm-fe/commit/0bc8031df3ba67c4ca27cc8339d6021e440143e9))

### Bug Fixes

- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://10.189.120.23/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://10.189.120.23/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://10.189.120.23/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://10.189.120.23/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-fix bug] Bị nhảy về trang listview Default sau khi chọn Search data có Custom View ([2b04778](https://10.189.120.23/crm/ucrm-fe/commit/2b04778ae9896bdce1dd52b3384376e08462be56))
- [tronglv-fix bug] Bug UI ([62d1c16](https://10.189.120.23/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix bug] fix bug consolidated view and update search meta list view ([13de2fb](https://10.189.120.23/crm/ucrm-fe/commit/13de2fb488ef587e4f1d1e95337ac181fb725546))
- [tronglv-fix/ui]update consolidated view setting ([b45018c](https://10.189.120.23/crm/ucrm-fe/commit/b45018c46abc9d9898a020e85c03dd57d81fd326))
- [tronglv-language] update language SLA setting ([2b5d1ea](https://10.189.120.23/crm/ucrm-fe/commit/2b5d1ea5eb675029408695146383d84a3c9e3983))
- [tronglv-language] update language Workflow ([90d3e0e](https://10.189.120.23/crm/ucrm-fe/commit/90d3e0ee32c9aa4327f77fb08b6a6ba48bde9446))
- add type when pin chart; remove formula date diff with in work ([6d63b71](https://10.189.120.23/crm/ucrm-fe/commit/6d63b71feec00cef26fd6d3594f89df53d93924f))
- arimetic one line; Number with commas; Height (Report) ([a264632](https://10.189.120.23/crm/ucrm-fe/commit/a26463298135f6ae4eefc458c48fff2b310429da))
- clear value when change field in widget ([7aa2e7c](https://10.189.120.23/crm/ucrm-fe/commit/7aa2e7c89b50655f85f5b4d17115c8d8647efdba))
- dynamic button display wrong object (Consolidated View); clear value chart ([8d27a5d](https://10.189.120.23/crm/ucrm-fe/commit/8d27a5d2feca7cd6d5516f4ec077e5f9bf11d508))
- fix bug linking ([fed6c87](https://10.189.120.23/crm/ucrm-fe/commit/fed6c8739e65b9e3ebe72fab81fd2fa64cfafd96))
- fix bug linking final ([4a1e415](https://10.189.120.23/crm/ucrm-fe/commit/4a1e415ce46e84985e7758e06e29b3a82c548164))
- fix condition ([3aab316](https://10.189.120.23/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://10.189.120.23/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- fix format date ([7646f93](https://10.189.120.23/crm/ucrm-fe/commit/7646f93fec4ecc1f07327872aeac16f652135b71))
- fix layout dashboard and sonarque ([b473445](https://10.189.120.23/crm/ucrm-fe/commit/b47344568709a51b1ceb45c604b97b80517e6cec))
- fix linking 2 ([fa8084f](https://10.189.120.23/crm/ucrm-fe/commit/fa8084fb3643f1d0d813f350d2ff3144e2eaf91a))
- fix new UI for account setting ([778bdc7](https://10.189.120.23/crm/ucrm-fe/commit/778bdc7c188d0f52fd1ead884a24c79434f4fee9))
- fix phan quyen consolidated view ([7c7a7a8](https://10.189.120.23/crm/ucrm-fe/commit/7c7a7a89b773ec5b0eb5a1462d45aea2fecea112))
- formular Datediff in words in combo chart ([431b186](https://10.189.120.23/crm/ucrm-fe/commit/431b1866fd94d78ce69611b46ffe20580df6b502))
- save mapping expose api IC integration ([f2edf37](https://10.189.120.23/crm/ucrm-fe/commit/f2edf372d6aad8b56cd31026e089156855cbbe6a))
- sonar bug ([3262fe0](https://10.189.120.23/crm/ucrm-fe/commit/3262fe0c1ad3c093f1c9cdfed9e64277f4087c52))
- UI Escalate rule SLA ([621f8de](https://10.189.120.23/crm/ucrm-fe/commit/621f8de37e4db79239394c372ecca3ea32ec097f))

### Others

- **release:** 2.15.0 ([a2e623b](https://10.189.120.23/crm/ucrm-fe/commit/a2e623be2ef6d0d22b1882dd96373a3fa822ef56))
- **release:** 2.16.0 ([e06d20c](https://10.189.120.23/crm/ucrm-fe/commit/e06d20c85c2f9a08c8cd45da1323b91cb3080ee3))

### Code Refactoring

- email ([d070c39](https://10.189.120.23/crm/ucrm-fe/commit/d070c3985df1dba7048d419de46788f64c62d05d))
- routes ([ad4cbd8](https://10.189.120.23/crm/ucrm-fe/commit/ad4cbd8661257a6322647cfefeb6f3c5f7b4192f))

## [2.16.0](https://gitlab.smbbasebs.com/crm/ucrm-fe/compare/v2.12.1...v2.16.0) (2022-09-13)

### Features

- [tronglv-assginment rule] ([cac1411](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))
- add custom admin in profile ([1596855](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add duplicate rule SLA ([d23cfc0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d23cfc0516a6e6b7c8f96cff9de6d06d33ea9b80))
- add export roles ([c70d517](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- add on/off linking ([82c64f0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))
- click to call in listview ([0bc8031](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/0bc8031df3ba67c4ca27cc8339d6021e440143e9))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-assignment rule] update assignment rule ([4e7b010](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-feature] assignment rule ([cae8da9](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- [tronglv-fix bug] Bug UI ([62d1c16](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-fix/ui]update consolidated view setting ([b45018c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b45018c46abc9d9898a020e85c03dd57d81fd326))
- [tronglv-language] update language SLA setting ([2b5d1ea](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/2b5d1ea5eb675029408695146383d84a3c9e3983))
- [tronglv-language] update language Workflow ([90d3e0e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/90d3e0ee32c9aa4327f77fb08b6a6ba48bde9446))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- add search user report to; add repeat escalate rule ([b99299c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- add type when pin chart; remove formula date diff with in work ([6d63b71](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6d63b71feec00cef26fd6d3594f89df53d93924f))
- fix cannot delete record ([97b7ce2](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))
- fix condition ([3aab316](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- linking object & condition ([52eb49d](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))
- type download export roles ([b63d203](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))
- UI Escalate rule SLA ([621f8de](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/621f8de37e4db79239394c372ecca3ea32ec097f))

### Others

- **release:** 2.12.2 ([96dc0d1](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/96dc0d15c7a98923a4450fd8741c4a1b2a3f058d))
- **release:** 2.13.0 ([6f19491](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/6f1949144f14b9b300c915bd6516bb14b8be3c10))
- **release:** 2.14.0 ([5e98e45](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/5e98e452f341a631b925dbf3c29d8856ac33bacd))
- **release:** 2.15.0 ([a2e623b](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/a2e623be2ef6d0d22b1882dd96373a3fa822ef56))

### Code Refactoring

- email ([d070c39](https://gitlab.smbbasebs.com/crm/ucrm-fe/commit/d070c3985df1dba7048d419de46788f64c62d05d))

## [2.15.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.13.0...v2.15.0) (2022-09-08)

### Features

- add custom admin in profile ([1596855](https://10.189.120.23/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add export roles ([c70d517](https://10.189.120.23/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- add on/off linking ([82c64f0](https://10.189.120.23/crm/ucrm-fe/commit/82c64f09da9fc07789391570f02cdbdaa881f9ea))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://10.189.120.23/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://10.189.120.23/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://10.189.120.23/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom admin] CTI setting, khi không có quyền Create thì nút Save chưa Disable ([7beaf8f](https://10.189.120.23/crm/ucrm-fe/commit/7beaf8f5abc487a04feff72172d01c96f3611557))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://10.189.120.23/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://10.189.120.23/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Look up] - Không reload data khi chọn lại Field Look up trong mapping field của Email Incoming ([6659f80](https://10.189.120.23/crm/ucrm-fe/commit/6659f80a60adae0991433f65d5b8b8ae1b594c56))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://10.189.120.23/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [tronglv-assignment rules] Add custom admin Assignment rules ([46c07d3](https://10.189.120.23/crm/ucrm-fe/commit/46c07d383513a9947e1ea5c5108173af6ff4a54b))
- [tronglv-bug/ui] fix bug login and lazy load ([53d961b](https://10.189.120.23/crm/ucrm-fe/commit/53d961be29d90b7c79299f3a4ec286b40087c35c))
- [tronglv-fix bug] Bug UI ([62d1c16](https://10.189.120.23/crm/ucrm-fe/commit/62d1c161b1ea1a36032b1720e157bab73561efe1))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://10.189.120.23/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://10.189.120.23/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://10.189.120.23/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://10.189.120.23/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://10.189.120.23/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://10.189.120.23/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://10.189.120.23/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- fix condition ([3aab316](https://10.189.120.23/crm/ucrm-fe/commit/3aab316156fc4861231d360dafea3aff371cb151))
- fix duplicated ([8d6e71d](https://10.189.120.23/crm/ucrm-fe/commit/8d6e71d7669a211c6d3c9193334451c0fea026b1))
- linking object & condition ([52eb49d](https://10.189.120.23/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- type download export roles ([b63d203](https://10.189.120.23/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))

### Others

- **release:** 2.14.0 ([5e98e45](https://10.189.120.23/crm/ucrm-fe/commit/5e98e452f341a631b925dbf3c29d8856ac33bacd))

## [2.14.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.1...v2.14.0) (2022-09-06)

### Features

- [tronglv-assginment rule] ([cac1411](https://10.189.120.23/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))
- add custom admin in profile ([1596855](https://10.189.120.23/crm/ucrm-fe/commit/1596855a32f2272c3871ef34f133a21abd679c75))
- add export roles ([c70d517](https://10.189.120.23/crm/ucrm-fe/commit/c70d5171f814890296f828fb83face06c3e46890))
- allow FormulaField with type DateDiff, DateParts, Number in Report/Chart/Widget measures ([6beb372](https://10.189.120.23/crm/ucrm-fe/commit/6beb37234a4c464cb0106d10292788b23f7dada3))

### Bug Fixes

- [Create record in interaction] Add required type, hide type when cancel ([50ad8d9](https://10.189.120.23/crm/ucrm-fe/commit/50ad8d9a2d8d13ae7bccec1eab00a8d2ca3f10cf))
- [Custom admin] Chức năng Edit của Oauth Google, Oauth Outlook chưa disable lại button ([33a3dc9](https://10.189.120.23/crm/ucrm-fe/commit/33a3dc909d9c41ecd1c645d6fa250b952994275f))
- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://10.189.120.23/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Email incoming - UI/UX] Dữ liệu field Mapping không clean sau khi hủy tạo Email Incoming ([18f7613](https://10.189.120.23/crm/ucrm-fe/commit/18f761315c4bb6c90a84cf9a3cb63dab32bbe111))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://10.189.120.23/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [LDAP] - Lỗi 500 khi chọn sort ở Colunm Description, Port ([f8b3e29](https://10.189.120.23/crm/ucrm-fe/commit/f8b3e29b389db13602ea5b848075798e59638762))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://10.189.120.23/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Management Object] Khi Object bị off nhưng vẫn hiển thị trong Setting Email Outgoing ([3b38558](https://10.189.120.23/crm/ucrm-fe/commit/3b3855895287a7f8f95b3736bf3d5fb5a344b9b5))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://10.189.120.23/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://10.189.120.23/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-assignment rule] update assignment rule ([4e7b010](https://10.189.120.23/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-feature] assignment rule ([cae8da9](https://10.189.120.23/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://10.189.120.23/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- [tronglv-fix/bug] replace table lookup and fix logs component ([eb09a0b](https://10.189.120.23/crm/ucrm-fe/commit/eb09a0bac25eaf3d35de7bce29d24e252c5101ee))
- [tronglv-replaceTable] replace table linking list ([9fd65d3](https://10.189.120.23/crm/ucrm-fe/commit/9fd65d306d92780d86c15590915ad6200f31600b))
- [UI/UX - OAuth google, Oauth Oulook] Chỉnh lại kích thước hiển thị khung Email cho thống nhất ([04b6cd6](https://10.189.120.23/crm/ucrm-fe/commit/04b6cd6809da99719779f46dfd48a09edc3f85b9))
- [UI/UX- Workflow] Field dạng ngày giao diện không đồng nhất với các field còn lại ([aace803](https://10.189.120.23/crm/ucrm-fe/commit/aace8038689da4e7f66b7efa35251f9cafed5c62))
- [User Setting] - Chưa tách biệt List Log và detail Log trong Log User ([007c8bc](https://10.189.120.23/crm/ucrm-fe/commit/007c8bc12d623a35f77360e052be421a5651d8f0))
- [Work flow] Action Send SMS khi Edit không hiển thị các Append field ([fff472e](https://10.189.120.23/crm/ucrm-fe/commit/fff472ea1efbee268492b6e407e51447b2e133f7))
- [Workflow/ Dynamic button] Khi chưa chọn Object vẫn hiển thị field trong condition để chọn ([96e26e0](https://10.189.120.23/crm/ucrm-fe/commit/96e26e0cfabd56a6dc9c38cace60f1797d4470a0))
- add search user report to; add repeat escalate rule ([b99299c](https://10.189.120.23/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- fix cannot delete record ([97b7ce2](https://10.189.120.23/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://10.189.120.23/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- linking object & condition ([52eb49d](https://10.189.120.23/crm/ucrm-fe/commit/52eb49d8e08bd4f9fd55ed0d34656e8943be595b))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://10.189.120.23/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))
- type download export roles ([b63d203](https://10.189.120.23/crm/ucrm-fe/commit/b63d203fb814953e2e160d4acb2e7b2ab7dd3810))

### Others

- **release:** 2.12.2 ([96dc0d1](https://10.189.120.23/crm/ucrm-fe/commit/96dc0d15c7a98923a4450fd8741c4a1b2a3f058d))
- **release:** 2.13.0 ([6f19491](https://10.189.120.23/crm/ucrm-fe/commit/6f1949144f14b9b300c915bd6516bb14b8be3c10))

## [2.13.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.2...v2.13.0) (2022-08-31)

### Features

- [tronglv-assginment rule] ([cac1411](https://10.189.120.23/crm/ucrm-fe/commit/cac14117019f507f180bf298f38711dea36c2d4c))

### Bug Fixes

- [tronglv-assignment rule] update assignment rule ([4e7b010](https://10.189.120.23/crm/ucrm-fe/commit/4e7b0108717c7a74c4ed5086229694be56d0b8e4))
- [tronglv-feature] assignment rule ([cae8da9](https://10.189.120.23/crm/ucrm-fe/commit/cae8da9fcf8d25485115c0afe3618ea22970a9dc))
- fix cannot delete record ([97b7ce2](https://10.189.120.23/crm/ucrm-fe/commit/97b7ce217f0551d6459a8abe965d5b53a8202fb2))

### [2.12.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.1...v2.12.2) (2022-08-25)

### Bug Fixes

- [Custom View] Khi xóa View mặc định trong Table view chưa có Noti rõ ràng ([3f5857a](https://10.189.120.23/crm/ucrm-fe/commit/3f5857aeb3e7078ec05c2d18c83d7099311b3171))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([caf7a5f](https://10.189.120.23/crm/ucrm-fe/commit/caf7a5f0336d7df3223ec1f7364cd99d4bc71e79))
- [Listview with detail] - Thêm ":" sau field thông tin ([9ecc0e0](https://10.189.120.23/crm/ucrm-fe/commit/9ecc0e0110b7d3c1d5122ac967957f5d5b79de85))
- [Mass Edit] - Picklist - Không hiển thị ra thông tin Object Target ([6b38944](https://10.189.120.23/crm/ucrm-fe/commit/6b3894424e20a502c6c80d40a766ff830928046a))
- [Profile] Chỉ phân quyền View nhưng vẫn Edit và Xóa được Record trên ListView ([7e7cde9](https://10.189.120.23/crm/ucrm-fe/commit/7e7cde93bb836d2fe2094ddb223bc83fe0a77f16))
- [tronglv-fix bug] fix other bug UI ([99d81f4](https://10.189.120.23/crm/ucrm-fe/commit/99d81f4c886c3ca2caa26f18261f91669c9b296d))
- add search user report to; add repeat escalate rule ([b99299c](https://10.189.120.23/crm/ucrm-fe/commit/b99299c2c3610fb113ef57968766999641861947))
- khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền ([1c1cb1c](https://10.189.120.23/crm/ucrm-fe/commit/1c1cb1c04ee4b15674bbb194590fc1f44aff4682))
- record có Update lại dữ liệu thì khi chọn lại Field lookup không Save được ([e46dd10](https://10.189.120.23/crm/ucrm-fe/commit/e46dd107fc36e958027086b6bb30ca9d06c698fd))

### [2.12.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.12.0...v2.12.1) (2022-08-19)

### Bug Fixes

- [tronglv-fix bug] fix list view and toggle object ([19344e3](https://10.189.120.23/crm/ucrm-fe/commit/19344e398163dd7231a168800ad387877e94279c))
- option chọn Type View đang dư chức năng chọn ListView ([eaf52ac](https://10.189.120.23/crm/ucrm-fe/commit/eaf52ac4bf5ac619e66e0c360b45fb60c1cf8c50))
- search in condition ([8774a22](https://10.189.120.23/crm/ucrm-fe/commit/8774a22abe316b18a709bd767fb47e10979843f3))

## [2.12.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.11.1...v2.12.0) (2022-08-18)

### Features

- [Email Incoming Rule] - Config email. Các trường dạng dynamic-field ko hiện droplist ([39608f8](https://10.189.120.23/crm/ucrm-fe/commit/39608f8d2f47945f97796085cc612e0cb8f2ed3a))
- add status call in interaction ([176cc23](https://10.189.120.23/crm/ucrm-fe/commit/176cc23c6724f295434116a9daf65dcd75a0a2d1))

### Bug Fixes

- "tronglv-fix bug] list view" ([2df70db](https://10.189.120.23/crm/ucrm-fe/commit/2df70db4ae0a4907fbd04b66b0a8b122ed532113))
- [Formular field - UI/UX] Field Formular Type Number Canh lề chưa đúng với định dạng ([511a8cc](https://10.189.120.23/crm/ucrm-fe/commit/511a8cc794a06859a1062f13f996965b346df337))
- [ListviewDetails] Reload comments; [Campaign] Undef record id ([56356ee](https://10.189.120.23/crm/ucrm-fe/commit/56356ee94a9ea1d66ee7612b485ff0a94d04f2dd))
- [tronglv-fix bug] list view and update calendar ([5fc0b84](https://10.189.120.23/crm/ucrm-fe/commit/5fc0b841db82c4bebe3eb12f47cd66f4a7e9de5c))
- [tronglv-fix bug] listview and other bug ([e0ac945](https://10.189.120.23/crm/ucrm-fe/commit/e0ac945f13cdfaa8d0d3b7ca010cc4ebef1aec32))
- create record in interaction (field linking) ([2a7f18a](https://10.189.120.23/crm/ucrm-fe/commit/2a7f18ab01b1427e654cf8c59f62f9672110f747))
- fix some bugs ([f19a7d0](https://10.189.120.23/crm/ucrm-fe/commit/f19a7d01aafb4aabc0c954b244e073c5165c3115))

### [2.11.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.11.0...v2.11.1) (2022-08-12)

### Bug Fixes

- [trongvl-fixui] Header view ([41e0e43](https://10.189.120.23/crm/ucrm-fe/commit/41e0e4363af8b5dad9a5cbff23bc971751e626c4))

## [2.11.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.10.0...v2.11.0) (2022-08-12)

### Features

- (FE) Tạo record trong interaction ([f79a8ff](https://10.189.120.23/crm/ucrm-fe/commit/f79a8ff8841bfc9bb71638a624429749387b8332))

### Bug Fixes

- [Knowledge base] Khi phân quyền View + Edit cho user thì lưu lại bị lỗi ([b74b254](https://10.189.120.23/crm/ucrm-fe/commit/b74b254812e01e9a73ef0ad03188c49bec6e8232))
- [Listview] - UI/UX - Chưa Disable Merge record khi chưa Tick chọn record ([549e117](https://10.189.120.23/crm/ucrm-fe/commit/549e11730851dcfef9fea692596558374081c144))
- [SLA Alert] Khi xóa Alert của SLA thì dữ liệu không clean ([715790c](https://10.189.120.23/crm/ucrm-fe/commit/715790c733876ee579c75ba669369c5ce30dcd95))
- [Ucrm Lab] Report: Step 3 chon Equal field select, -> step 2 -> step 3 bi loi field Equal ([c1b76ae](https://10.189.120.23/crm/ucrm-fe/commit/c1b76ae519cdf6cf7488c43b26bb96528ea8e692))
- component Consolidate view thì hiện thông báo thành công nhưng component vẫn còn không bị xóa ([85a2f80](https://10.189.120.23/crm/ucrm-fe/commit/85a2f8009c068ba874a93cde117797d1cc6060f2))
- edit record thì những Field không hiển thị trên View sẽ không Load được dữ liệu lên form Edit ([5943e6f](https://10.189.120.23/crm/ucrm-fe/commit/5943e6f1fa80ec3db1662be2189f20fa7e91e43f))
- fix change object id in listview with detail ([1829c04](https://10.189.120.23/crm/ucrm-fe/commit/1829c0452fd2109111216b1b154f0a8ed33206d9))
- fix type of field duplicate, type email, text, number ([8642b00](https://10.189.120.23/crm/ucrm-fe/commit/8642b00a1ca19f4118b85a95e9d6308fcb439bef))

### Code Refactoring

- [UI/UX] Change tab CTI setting, IC integration ([7da77ba](https://10.189.120.23/crm/ucrm-fe/commit/7da77ba88f0924343e36bd71e2e7f4a66c38c730))

## [2.10.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.3...v2.10.0) (2022-08-11)

### Features

- [Look up] - Chưa gắn thông tin Field Look up trong Action on read all của Email Incoming ([50a045c](https://10.189.120.23/crm/ucrm-fe/commit/50a045ce73f40d6bb4fd5cdc359d608c10d32f3d))
- [Look up] - Thêm tính năng cho chọn Trigger kế thừa data từ Object Parent ([3e05622](https://10.189.120.23/crm/ucrm-fe/commit/3e05622d07a711bd50ff145f3169d86fa7989552))
- [tronglv-newtable] custom view table ([af357f2](https://10.189.120.23/crm/ucrm-fe/commit/af357f2333bc9f3d4c6bbd1422c65c0dc2058a0e))
- create record in interaction ([1b3002c](https://10.189.120.23/crm/ucrm-fe/commit/1b3002c572feeebb57da4e6238c38b82288df124))
- tronglv-custom table ([f212f70](https://10.189.120.23/crm/ucrm-fe/commit/f212f701e82162f25f55d13a7b439280550a6e3f))

### Bug Fixes

- - Hiển thị câu thông báo yêu cầu chọn Object có field Require sau khi chọn Object bên Kanban ([a237e28](https://10.189.120.23/crm/ucrm-fe/commit/a237e282ea2a589c8d0e4d64e544413c92348009))
- [Consolidateb view] - UI/UX - Bị dư dấu ":" sau khi Field hiển thị trong Component Detail bị xó ([fe832f3](https://10.189.120.23/crm/ucrm-fe/commit/fe832f3d159b8cf3a85b7ce8af72629e03ca7000))
- [Dashboard] - Không Collapse lại được group "My dahsboard" ([ecd46b9](https://10.189.120.23/crm/ucrm-fe/commit/ecd46b919c2faf118927f65a49224540e577faa3))
- [Listview] - UI/UX - Thông tin bị lệch nhau trong Merge record ([034956f](https://10.189.120.23/crm/ucrm-fe/commit/034956fa6c26fb9990a4b93f727178ae697e939e))
- [Look up] - Field vẫn cho edit bình thường khi set quyền Field là Read only ([a3dce80](https://10.189.120.23/crm/ucrm-fe/commit/a3dce809164cdac2a277e6bd2f611d74e6003d0b))
- [Object Management] - UI/UX - Chữ "Add Object" chạy lên xuống theo scroll ([52f67f5](https://10.189.120.23/crm/ucrm-fe/commit/52f67f59aaf5b26b0550444a3bbc7929a68be134))
- [Object Management] Khi Object bị ẩn nhưng edit save tại thì Object bị bỏ ẩn ([52caee7](https://10.189.120.23/crm/ucrm-fe/commit/52caee7e6a79b93a6b4b74a830ae6eea5e24c7fc))
- [Report] - UI/UX - List danh sách với detail log scroll cùng lúc ([f7de9bc](https://10.189.120.23/crm/ucrm-fe/commit/f7de9bcf4f5be96ca09a897379c5a9cc80b6b477))
- [System log] Khi chuyển Action thì số Item/ Page ở bên dưới chưa reload lại ([333c40d](https://10.189.120.23/crm/ucrm-fe/commit/333c40d79208c7e76c2113fe031eae86451de440))
- [tronglv-fix bug] bug sider ([4a3f73b](https://10.189.120.23/crm/ucrm-fe/commit/4a3f73b44eda89340230cf7c94d00f0d8c50b6b3))
- [UI/UX - Consolidate view] CoreS: Nút thùng rác có kích thước quá lớn so với các Setting khác ([658dc2f](https://10.189.120.23/crm/ucrm-fe/commit/658dc2fe29194ea07d60cdf4b9fef769b5c5854c))
- fix new list view in consolidated view ([1c4d490](https://10.189.120.23/crm/ucrm-fe/commit/1c4d490cecb7d4f068f493f90f29f709c37d001d))
- fix: [Language] init language ([3fd6d29](https://10.189.120.23/crm/ucrm-fe/commit/3fd6d29d1880c5a3a2794b42880c8b0eb0ce6a74))

### [2.9.3](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.2...v2.9.3) (2022-08-09)

### Bug Fixes

- [fix-bug - UI/UX General List view] ([fd9a26b](https://10.189.120.23/crm/ucrm-fe/commit/fd9a26bac3b0fc68f0abbc48e8d083f09fc3efc4))
- [UI/UX - Picklist] Giao diện Add new/ Edit chưa có in đậm tiêu đề, thiếu dấu : sau Target field ([8c37928](https://10.189.120.23/crm/ucrm-fe/commit/8c379283eeaaf08ff36d07375bc66de2c35a1c93))
- [UI/UX General List view] Giao diện list view hiển thị quá ít còn dư quá nhiều khoảng trống ([e78c8ea](https://10.189.120.23/crm/ucrm-fe/commit/e78c8ea1242e31ab1789e13c029f941063f1ec11))
- [UI/UX-Consolidatedview] Finesse integration ([5de9810](https://10.189.120.23/crm/ucrm-fe/commit/5de9810d94a48409e0e35cf32951dda6ec9c74ab))
- bi loi chon Field là Attached file ([b95d102](https://10.189.120.23/crm/ucrm-fe/commit/b95d102d09115dad93a00cab7d921fccc477124e))
- delete file custom Table ([1f577ba](https://10.189.120.23/crm/ucrm-fe/commit/1f577ba16999cabb1ee6507ad89ad521f1e6f858))
- fix Invalid date after import Field Date ([b14b129](https://10.189.120.23/crm/ucrm-fe/commit/b14b1293db15fdaf6b3421b52f0a74665a2f8f91))
- import lookup field ([82b2855](https://10.189.120.23/crm/ucrm-fe/commit/82b28556a5f03e18fc72709f14f84faef6b46899))
- khi setting quyen view cho User thi Section chua duoc thiet lap dung phan quyen ([8bf5ff0](https://10.189.120.23/crm/ucrm-fe/commit/8bf5ff01845b58031a414babf66be00ad99dc4df))

### [2.9.2](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.1...v2.9.2) (2022-08-08)

### Bug Fixes

- test commit ([4b84b45](https://10.189.120.23/crm/ucrm-fe/commit/4b84b4529f5e57a5c5be8a879ca45d66da5193b8))

### [2.9.1](https://10.189.120.23/crm/ucrm-fe/compare/v2.9.0...v2.9.1) (2022-08-08)

### Bug Fixes

- fix conflict ([e300043](https://10.189.120.23/crm/ucrm-fe/commit/e3000433684d2c5cb033f0ddb593bd36c7082d60))
- fix import trigger SLA and Workflow ([b1b0a42](https://10.189.120.23/crm/ucrm-fe/commit/b1b0a42bfc2b131cda65af93ceec5ef5e79248e1))

## [2.9.0](https://10.189.120.23/crm/ucrm-fe/compare/v2.8.9...v2.9.0) (2022-08-08)

### Features

- # test ([b7c7c1d](https://10.189.120.23/crm/ucrm-fe/commit/b7c7c1d0ba91d206b1a1b294b62095f3c44688cc))
  ---------------------8/8/2022---------------------
  Bug fixes:

* [UI/UX General List view] Khi thêm View không nhập các field require, lỗi không hiển thị đầy đủ nội dung cảnh báo
* [Duplicated Rule] Hiển thị cảnh báo yêu cầu nhập Field mặc dù là đang xóa Rule
* [UI/UX General List view] Giao diện list view hiển thị quá ít còn dư quá nhiều khoảng trống
* [UI/UX General List view] - Lỗi UI/UX ở trong Header Listview
* [UI/UX General List view] - Bị dư "New listview" trong option view hiển thị
* [General List view] - Hiển thị ra button Delete sau khi check vào ô record

- [SMS Out going] Khi Addnew trùng Object hệ thống trả về lỗi 500
- [Custom admin - Workflow] User được set quyền View + Create + Delete, không có quyền edit, nhưng khi Copy trang Edit thì vẫn có quyền truy cập được trang
- [Custom admin ] Disable các tính năng không dùng trong setting Custom admin, cần Dissable lại
- [Custom admin] Khi user thường bị bỏ phân quyền Setting, reset form lại vẫn ở trang setting
- [Custom Admin] Giao diện phân quyền khi chưa tick quyền view nên cho tô xám các chức năng Create, Edit, Delete như bên phân quyền của Profile
- [Workflow] Khi Update Type Number từ Workflow vào Record thì dữ liệu Update là kiểu String không phải Type Number

* Features: Edit new avatar header

---------------------5/8/2022---------------------

- Bug fixes:
  - [Custom admin - Consolidated view] User được setting quyền View, nhưng vẫn hiện các nút thêm sửa xóa Layout setting
  - [Custom admin - Object&Layout] Khi set quyền view nhưng vẫn Edit được Name của field&layout
  - [Audit log] - System - UI/UX - Phân chia lại cột hiển thị Header
- Features: New listview
  ---------------------4/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [ General Object ] - Search không ra dữ liệu ở từng cột ở ngoài list view
  - [Consolidated View] - Không hiển thị thông báo sau khi record được chọn xem Consolidated đã bị xóa
  - [Custom admin - Knowledge base] User được phân quyền View + Create, thì hệ thống cho nhập liệu nhưng không có hiện nút Save để lưu
  - [Custom admin - Object&Layout] Khi setting quyền view cho User thì Section chưa được thiết lập đúng phân quyền
- Features:
  ---------------------3/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Audit log] - Không hiển thị đúng cột thông tin Type of action, Action được chọn
  - [Mass Edit] - Bị mất thông tin của Field Look up 1 khi chọn Field Look up 2
  - [Audit log] - System - UI/UX - Hiển thị icon load sau mỗi lần nhập thông tin tìm kiếm
- Features:
  ---------------------2/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Object & Field Layout] - Hiển thị ra ID sau khi chọn xem Edit Field Look up
  - [UI/UX] Khi xóa Role, Profile, Object&layout field, sửa lại nút Save thành Delete cho thống nhất
  - [Mass Edit] - Không hiển thị thông tin các trường chỉnh sửa của Field Look up
- Features: Lookup field
  ---------------------1/8/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Object & Field Layout] - Bị mất checkbox Ghi log
  - [Object Ticket - Add new Record] Assign to/ Search key word không thực hiện được trên hệ điều hành Win 7
  - [Consolidated View] - Không tạo được record trong Tags, Table component
  - [Trash] - Ra trang 404 sau khi chọn Breakcrum Recyclibin
  - [Trash] - Button Delete Forever không Disable đi sau khi chọn xóa record thành công
  - [Consolidated View] - Bị mất Component sau khi chọn ở Left Screen, Right Screen trong Layout setting đối với Object mới tạo lần đầu
  - [Field & Layout Field] - Formula Field - Không xóa được Field đã chọn trong Type Formula (Date)
- Features:
  ---------------------31/7/2022---------------------
- Bug fixes:
- Features: Duplicate rule
  ---------------------30/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------29/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------28/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------27/7/2022---------------------
- Bug fixes:
- Features:
  ---------------------26/7/2022---------------------
- Bug fixes:
  - [Consolidated View Setting] Type Layout Setting không xóa được tab
  - [Field & Layout Field] - Formula Field - Không xóa Field sau khi chọn qua type Formula field khác
  - [ General Object ] - Hiển thị sai tên field trên màn hình Hình list view
  - [UI/UX - Picklist dependency] Giao diện Add new/ Edit chưa có in đậm tiêu đề, thiếu dấu : sau Target field
  - [ListView with detail] - Create By, Assign to, Modified by hiển thi ra mã ID trong component hệ thống Details
  - [ Object & Layout field ] - Không có dữ liệu trong Record giống như Thông tin đã setting trong Object & Layout field
  - [Setting Object] Sau khi Delete Object không thành công mà nhấn Double Edit Object thì dữ liệu đã lưu không load lên form Edit
- Features:
  - List view with details
    ---------------------25/7/2022---------------------
- Bug fixes:
  - [Listview with detail] - Không hiển thị thông tin của field meta data sau khi setting có chọn field metadata
  - [Related Object] - Xung đột 2 field có type là linking object trong cùng 1 Object
  - [SLA] - Phải chọn lại User khi thay đổi điều kiện Equals và Not Equals thì mới Lưu được
  - [Component -Detail] Không Edit được thông tin Field linking trên Component Detail
  - [Import Excel] - Bị lỗi khi nhập thông tin của Field Email bên cột "Default Value"
- Features:
  > > > > > > > 03f1e6a61d03383cafd4206afcbb6dfeeb48ce19
