import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Tooltip from "antd/lib/tooltip";

import {
  saveSMSOutgoing,
  setEditSMSOutgoing,
  setShowDrawer,
  updateFieldSMSOutgoing,
  updateSMSOutgoing,
} from "redux/slices/smsOutgoing";

import DeleteIcon from "assets/icons/common/delete.svg";
import { Notification } from "components/Notification/Noti";

function ModalSetting({ listOptionObjectField }) {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { showDrawer, editSMSOutgoing } = useSelector(
    (state) => state.smsOutgoingReducer
  );
  const { listObject } = useSelector((state) => state.objectsManagementReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const {
    object_id,
    authentication_type,
    description,
    endpoint_api,
    field_mobile_phone_to,
    sms_from,
    token,
    password,
    username,
    method,
    body_type,
    body,
    params,
  } = editSMSOutgoing;

  const [listFields, setListFields] = useState([]);

  const methods = [
    {
      label: "GET",
      value: "GET",
    },
    {
      label: "POST",
      value: "POST",
    },
    {
      label: "DELETE",
      value: "DELETE",
    },
    {
      label: "PATCH",
      value: "PATCH",
    },
    {
      label: "PUT",
      value: "PUT",
    },
  ];

  const bodyType = [
    {
      label: "JSON",
      value: "json",
    },
    {
      label: "Form",
      value: "form",
    },
  ];

  const _onSubmit = () => {
    if (
      params.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field params!");
    } else if (
      body.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field body!");
    } else {
      if (editSMSOutgoing._id) {
        dispatch(updateSMSOutgoing(editSMSOutgoing));
      } else {
        dispatch(saveSMSOutgoing(editSMSOutgoing));
      }
    }
  };

  const _onCancel = () => {
    dispatch(setShowDrawer(false));
    dispatch(setEditSMSOutgoing({}));
    form.resetFields();
  };

  const _onAddParam = () => {
    dispatch(
      updateFieldSMSOutgoing({
        key: "params",
        value: [
          ...params,
          {
            key: undefined,
            value: undefined,
          },
        ],
      })
    );
  };

  const _onDeleteParam = (idx) => {
    let temp = [...params];
    temp.splice(idx, 1);

    dispatch(
      updateFieldSMSOutgoing({
        key: "params",
        value: temp,
      })
    );
  };

  const handleChangeParam = (value, index, type) => {
    let temp = [...params];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }

    dispatch(
      updateFieldSMSOutgoing({
        key: "params",
        value: temp,
      })
    );
  };

  const _onAddBody = () => {
    dispatch(
      updateFieldSMSOutgoing({
        key: "body",
        value: [
          ...body,
          {
            key: undefined,
            value: undefined,
          },
        ],
      })
    );
  };

  const _onDeleteBody = (idx) => {
    let temp = [...body];
    temp.splice(idx, 1);
    dispatch(
      updateFieldSMSOutgoing({
        key: "body",
        value: temp,
      })
    );
  };

  const handleChangeBody = (value, index, type) => {
    let temp = [...body];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }

    dispatch(
      updateFieldSMSOutgoing({
        key: "body",
        value: temp,
      })
    );
  };

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: "$" + field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  return (
    <ModalCustom
      title={t("SMSOutgoing.settingOutgoing")}
      visible={showDrawer}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          name={"object_id"}
          label={t("object.object")}
          rules={[
            {
              validator: (rule, value = object_id, cb) => {
                object_id.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={object_id}
        >
          <Select
            value={object_id}
            onChange={(e) => {
              dispatch(
                updateFieldSMSOutgoing({
                  key: "object_id",
                  value: e,
                })
              );
              dispatch(
                updateFieldSMSOutgoing({
                  key: "field_mobile_phone_to",
                  value: "",
                })
              );
            }}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {
              // eslint-disable-next-line
              listObject &&
                listObject.map((item) => {
                  if (item.Status) {
                    return (
                      <Select.Option value={item._id}>
                        {item.Name}
                      </Select.Option>
                    );
                  } else return "";
                })
            }
          </Select>
        </Form.Item>

        <Form.Item
          name={"description"}
          label={t("common.description")}
          valuePropName={description}
        >
          <Input
            value={description}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "description",
                  value: e.target.value,
                })
              )
            }
          />
        </Form.Item>

        <Form.Item
          name={"field_mobile_phone_to"}
          label={t("SMSOutgoing.fieldPhone")}
          rules={[
            {
              validator: (rule, value = field_mobile_phone_to, cb) => {
                field_mobile_phone_to.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          valuePropName={field_mobile_phone_to}
        >
          <Select
            value={field_mobile_phone_to}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "field_mobile_phone_to",
                  value: e,
                })
              )
            }
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listOptionObjectField &&
              listOptionObjectField.map((item) => {
                return (
                  <Select.Option value={item.field_id}>
                    {item.name}
                  </Select.Option>
                );
              })}
          </Select>
        </Form.Item>
        <Form.Item
          name={"endpoint_api"}
          label={"Endpoint API"}
          rules={[
            {
              validator: (rule, value = endpoint_api, cb) => {
                endpoint_api.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          valuePropName={endpoint_api}
        >
          <Input
            value={endpoint_api}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "endpoint_api",
                  value: e.target.value,
                })
              )
            }
          />
        </Form.Item>
        <Form.Item
          name={"authentication_type"}
          label={t("SMSOutgoing.authType")}
          rules={[
            {
              validator: (rule, value = authentication_type, cb) => {
                authentication_type.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          valuePropName={authentication_type}
        >
          <Radio.Group
            value={authentication_type}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "authentication_type",
                  value: e.target.value,
                })
              )
            }
          >
            <Radio.Button value={"basic"}>Basic</Radio.Button>
            <Radio.Button value={"token"}>Token</Radio.Button>
            <Radio.Button value={"none"}>None</Radio.Button>
          </Radio.Group>
        </Form.Item>
        {authentication_type === "basic" && (
          <>
            <Form.Item
              name={"username"}
              label={t("SMSOutgoing.username")}
              rules={[
                {
                  validator: (rule, value = username, cb) => {
                    username.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={username}
            >
              <Input
                value={username}
                onChange={(e) =>
                  dispatch(
                    updateFieldSMSOutgoing({
                      key: "username",
                      value: e.target.value,
                    })
                  )
                }
              />
            </Form.Item>
            <Form.Item
              name={"password"}
              label={t("SMSOutgoing.password")}
              rules={[
                {
                  validator: (rule, value = password, cb) => {
                    password.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={password}
            >
              <Input
                type={"password"}
                value={password}
                onChange={(e) =>
                  dispatch(
                    updateFieldSMSOutgoing({
                      key: "password",
                      value: e.target.value,
                    })
                  )
                }
              />
            </Form.Item>
          </>
        )}

        {authentication_type === "token" && (
          <Form.Item
            name={"token"}
            label={"Token"}
            rules={[
              {
                validator: (rule, value = token, cb) => {
                  token.length === 0 ? cb(t("common.requiredField")) : cb();
                },
                required: true,
              },
            ]}
            valuePropName={token}
          >
            <Input
              value={token}
              onChange={(e) =>
                dispatch(
                  updateFieldSMSOutgoing({
                    key: "token",
                    value: e.target.value,
                  })
                )
              }
            />
          </Form.Item>
        )}

        <Form.Item
          name={"sms_from"}
          label={"SMS From"}
          rules={[
            {
              validator: (rule, value = sms_from, cb) => {
                sms_from.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={sms_from}
        >
          <Input
            value={sms_from}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "sms_from",
                  value: e.target.value,
                })
              )
            }
          />
        </Form.Item>

        <Form.Item
          name={"method"}
          label="Method"
          rules={[
            {
              validator: (rule, value = method, cb) => {
                method.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={method}
        >
          <Select
            value={method}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "method",
                  value: e,
                })
              )
            }
            options={methods}
          />
        </Form.Item>

        <Form.Item
          name={"body_type"}
          label="Body type"
          rules={[
            {
              validator: (rule, value = body_type, cb) => {
                body_type.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={body_type}
        >
          <Select
            value={body_type}
            onChange={(e) =>
              dispatch(
                updateFieldSMSOutgoing({
                  key: "body_type",
                  value: e,
                })
              )
            }
            options={bodyType}
          />
        </Form.Item>

        <WrapParams>
          <legend>Params</legend>

          {params && params.length > 0 && (
            <>
              {params.map((item, idx) => {
                return (
                  <WrapValue>
                    <p>Key</p>
                    <Input
                      value={item.key}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeParam(e.target.value, idx, "key")
                      }
                    />
                    <p>Value</p>
                    <Select
                      value={item.value}
                      placeholder={t("common.placeholderInputSelect")}
                      options={listFields}
                      mode="tags"
                      onChange={(e) => {
                        if (e?.length > 1) {
                          e.shift();
                        }
                        handleChangeParam(e[0], idx, "value");
                      }}
                    />
                    <Delete>
                      <Tooltip title="Delete">
                        <img
                          src={DeleteIcon}
                          onClick={() => _onDeleteParam(idx)}
                          alt="delete"
                        />
                      </Tooltip>
                    </Delete>
                  </WrapValue>
                );
              })}
            </>
          )}
          <AddFields onClick={() => _onAddParam()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ {t("workflow.addParam")}</span>
          </AddFields>
        </WrapParams>

        <WrapBody>
          <legend>Body</legend>

          {body && body.length > 0 && (
            <>
              {body.map((item, idx) => {
                return (
                  <WrapValue>
                    <p>Key</p>
                    <Input
                      value={item.key}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeBody(e.target.value, idx, "key")
                      }
                    />
                    <p>Value</p>
                    <Select
                      value={item.value}
                      placeholder={t("common.placeholderInputSelect")}
                      options={listFields}
                      mode="tags"
                      onChange={(e) => {
                        if (e?.length > 1) {
                          e.shift();
                        }
                        handleChangeBody(e[0], idx, "value");
                      }}
                    />
                    <Delete>
                      <Tooltip title="Delete">
                        <img
                          src={DeleteIcon}
                          onClick={() => _onDeleteBody(idx)}
                          alt="delete"
                        />
                      </Tooltip>
                    </Delete>
                  </WrapValue>
                );
              })}
            </>
          )}
          <AddFields onClick={() => _onAddBody()}>
            <span>+ {t("workflow.addBody")}</span>
          </AddFields>
        </WrapBody>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapParams = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }
  }
`;

const WrapBody = styled(WrapParams)`
  margin-top: 24px;
`;

const WrapValue = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    margin-bottom: 0;
    margin-right: 16px;
  }

  .ant-input {
    width: 40%;
    margin-right: 8px;
  }

  .ant-select {
    width: 40%;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;
