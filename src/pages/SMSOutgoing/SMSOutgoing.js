import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import _ from "lodash";

import { Button, Table, Input, Space, Breadcrumb, Tooltip, Tag } from "antd";
import { SearchOutlined } from "@ant-design/icons";

import {
  getListSMSOutgoing,
  setEditSMSOutgoing,
  setShowDrawer,
  setShowModalTest,
  deleteSMSOutgoing,
  getSMSOutgoingById,
  unmountSMSOutgoing,
} from "redux/slices/smsOutgoing";
import { loadListObjectField } from "redux/slices/objects";
import { setShowModalConfirmDelete } from "redux/slices/global";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import emptyEmail from "assets/icons/email/empty-email.svg";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import ModalSMSOutgoing from "./ModalSMSOutgoing";
import ModalTest from "./ModalTest";
import { changeTitlePage } from "redux/slices/authenticated";

const SMSOutgoing = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { listSMSOutgoing, editSMSOutgoing } = useSelector(
    (state) => state.smsOutgoingReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const { object_id } = editSMSOutgoing;

  const [listOptionObjectField, setListOptionObjectField] = useState([]);
  // eslint-disable-next-line
  const [searchText, setSearchText] = useState("");
  // eslint-disable-next-line
  const [searchedColumn, setSearchedColumn] = useState("");
  const [dataDelete, setDataDelete] = useState({});
  const [dataTest, setDataTest] = useState("");

  useEffect(() => {
    dispatch(changeTitlePage(t("SMSOutgoing.smsOutgoing")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "sms_outgoing" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
          >
            {t("common.search")}
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
          >
            {t("common.reset")}
          </CustomButtonCancel>
          {/* <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText(selectedKeys[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            {t("SMSOutgoing.filterData")}
          </Button> */}
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    render: (text) =>
      //   searchedColumn === dataIndex ? (
      //     <Highlighter
      //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //       searchWords={[searchText]}
      //       autoEscape
      //       textToHighlight={text ? text.toString() : ''}
      //     />
      //   ) : (
      text,
    //   ),
  });

  const columns = [
    {
      title: "",
      key: "test",
      render: (record) => (
        <>
          <CustomTag onClick={() => _onTest(record)}>Test</CustomTag>
        </>
      ),
    },
    {
      title: "Object name",
      dataIndex: "object_name",
      key: "object_name",
      ...getColumnSearchProps("object_name"),
    },
    {
      title: t("SMSOutgoing.fieldName"),
      dataIndex: "field_name",
      key: "field_name",
      ...getColumnSearchProps("field_name"),
    },
    {
      title: "End point API",
      dataIndex: "endpoint_api",
      key: "endpoint_api",
      ...getColumnSearchProps("endpoint_api"),
    },
    {
      title: t("SMSOutgoing.authType"),
      dataIndex: "authentication_type",
      key: "authentication_type",
      ...getColumnSearchProps("authentication_type"),
      render: (text) => (
        <span style={{ textTransform: "capitalize" }}>{text}</span>
      ),
    },
    {
      title: "SMS From",
      dataIndex: "sms_from",
      key: "sms_from",
      ...getColumnSearchProps("sms_from"),
    },
    {
      title: t("common.description"),
      dataIndex: "description",
      key: "description",
      ...getColumnSearchProps("description"),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "right",
      width: 150,
      render: (record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                onClick={() => editSMSOutgoing1(record)}
                src={Edit}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => deleteSMSOutgoing1(record)}
                src={Delete}
                alt="delete"
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const _onTest = (record) => {
    dispatch(setShowModalTest(true));
    setDataTest(record._id);
  };

  const addNew = () => {
    dispatch(setShowDrawer(true));
    setListOptionObjectField([]);
    dispatch(
      setEditSMSOutgoing({
        object_id: "",
        authentication_type: "none",
        description: "",
        endpoint_api: "",
        field_mobile_phone_to: "",
        sms_from: "",
        method: "POST",
        body_type: "json",
        body: [],
        params: [],
      })
    );
  };

  const editSMSOutgoing1 = (record) => {
    dispatch(
      getSMSOutgoingById({
        _id: record._id,
      })
    );
  };

  const deleteSMSOutgoing1 = (record) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: record._id,
    });
  };

  useEffect(() => {
    dispatch(getListSMSOutgoing());
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (object_id) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object_id,
        })
      );
    }
    // eslint-disable-next-line
  }, [object_id]);

  useEffect(() => {
    let arr = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                arr.push(field);
              }
            });
          });
        }
      }
    });

    setListOptionObjectField(arr);
  }, [listObjectField]);
  // unmount
  useEffect(
    () => () => {
      dispatch(unmountSMSOutgoing());
    },
    // eslint-disable-next-line
    []
  );
  return (
    <Wrapper>
      <ModalConfimDelete
        title={"SMS Outgoing"}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteSMSOutgoing}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />

      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("SMSOutgoing.smsOutgoing")}</BreadcrumbItem>
        </Breadcrumb>
        {listSMSOutgoing.length > 0 && checkRule("create") && (
          <AddButton onClick={addNew}>+ {t("SMSOutgoing.addNew")}</AddButton>
        )}
      </WrapBreadcrumb>

      {listSMSOutgoing.length === 0 ? (
        <Empty>
          <img src={emptyEmail} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("SMSOutgoing.smsOutgoing")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={addNew}>+ {t("SMSOutgoing.addNew")}</AddButton>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table
            columns={columns}
            dataSource={listSMSOutgoing}
            pagination={{
              position: ["bottomRight"],
              showSizeChanger: true,
              defaultPageSize: 10,
              showQuickJumper: true,
            }}
          />
        </WrapTable>
      )}

      <ModalSMSOutgoing listOptionObjectField={listOptionObjectField} />
      <ModalTest dataTest={dataTest} setDataTest={setDataTest} />
    </Wrapper>
  );
};

export default SMSOutgoing;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.darker};
  border-color: ${(props) => props.theme.darker};
  color: #fff;
  margin-right: 16px;

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  color: #000;

  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const CustomTag = styled(Tag)`
  border: 1px solid ${(props) => props.theme.main};
  color: ${(props) => props.theme.main};
  padding: 0 16px;
  cursor: pointer;
`;
