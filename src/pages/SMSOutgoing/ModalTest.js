import { useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";

import { testSMSOutgoing, setShowModalTest } from "redux/slices/smsOutgoing";

import DeleteIcon from "assets/icons/common/delete.svg";
import { Notification } from "components/Notification/Noti";

function ModalSetting({ dataTest, setDataTest }) {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { showModalTest } = useSelector((state) => state.smsOutgoingReducer);

  const [params, setParams] = useState([]);
  const [body, setBody] = useState([]);

  const _onSubmit = () => {
    if (
      params.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field params!");
    } else if (
      body.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field body!");
    } else {
      let tempData = {
        params: {},
        body: {},
      };

      params.forEach((item) => {
        tempData.params = {
          ...tempData.params,
          [item.key]: item.value,
        };
      });

      body.forEach((item) => {
        tempData.body = {
          ...tempData.body,
          [item.key]: item.value,
        };
      });

      dispatch(
        testSMSOutgoing({
          setting_id: dataTest,
          data: tempData,
        })
      );
    }
  };

  const _onCancel = () => {
    setParams([]);
    setBody([]);
    setDataTest("");
    form.resetFields();
    dispatch(setShowModalTest(false));
  };

  const _onAddParam = () => {
    setParams([
      ...params,
      {
        key: undefined,
        value: undefined,
      },
    ]);
  };

  const _onDeleteParam = (idx) => {
    let temp = [...params];
    temp.splice(idx, 1);
    setParams(temp);
  };

  const handleChangeParam = (value, index, type) => {
    let temp = [...params];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }

    setParams(temp);
  };

  const _onAddBody = () => {
    setBody([
      ...body,
      {
        key: undefined,
        value: undefined,
      },
    ]);
  };

  const _onDeleteBody = (idx) => {
    let temp = [...body];
    temp.splice(idx, 1);
    setBody(temp);
  };

  const handleChangeBody = (value, index, type) => {
    let temp = [...body];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }

    setBody(temp);
  };

  return (
    <ModalCustom
      title="Test"
      visible={showModalTest}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <WrapParams>
          <legend>Params</legend>

          {params.length > 0 && (
            <>
              {params.map((item, idx) => {
                return (
                  <WrapValue>
                    <p>Key</p>
                    <Input
                      value={item.key}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeParam(e.target.value, idx, "key")
                      }
                    />
                    <p>Value</p>
                    <Input
                      value={item.value}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeParam(e.target.value, idx, "value")
                      }
                    />
                    <Delete>
                      <Tooltip title="Delete">
                        <img
                          src={DeleteIcon}
                          onClick={() => _onDeleteParam(idx)}
                          alt="delete"
                        />
                      </Tooltip>
                    </Delete>
                  </WrapValue>
                );
              })}
            </>
          )}
          <AddFields onClick={() => _onAddParam()}>
            <span>+ {t("workflow.addParam")}</span>
          </AddFields>
        </WrapParams>

        <WrapBody>
          <legend>Body</legend>

          {body && body.length > 0 && (
            <>
              {body.map((item, idx) => {
                return (
                  <WrapValue>
                    <p>Key</p>
                    <Input
                      value={item.key}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeBody(e.target.value, idx, "key")
                      }
                    />
                    <p>Value</p>
                    <Input
                      value={item.value}
                      placeholder={t("common.placeholderInput")}
                      onChange={(e) =>
                        handleChangeBody(e.target.value, idx, "value")
                      }
                    />
                    <Delete>
                      <Tooltip title="Delete">
                        <img
                          src={DeleteIcon}
                          onClick={() => _onDeleteBody(idx)}
                          alt="delete"
                        />
                      </Tooltip>
                    </Delete>
                  </WrapValue>
                );
              })}
            </>
          )}
          <AddFields onClick={() => _onAddBody()}>
            <span>+ {t("workflow.addBody")}</span>
          </AddFields>
        </WrapBody>

        <WrapButton label=" ">
          {(params.length !== 0 || body.length !== 0) && (
            <Button type="primary" htmlType="submit">
              {t("common.save")}
            </Button>
          )}

          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapParams = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }
  }
`;

const WrapBody = styled(WrapParams)`
  margin-top: 24px;
`;

const WrapValue = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    margin-bottom: 0;
    margin-right: 16px;
  }

  .ant-input {
    width: 40%;
    margin-right: 8px;
  }

  .ant-select {
    width: 40%;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;
