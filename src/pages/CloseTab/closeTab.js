import React, { useEffect } from "react";

function Close(props) {
  useEffect(() => {
    if (window) {
      window.close();
    }
  }, []);

  return <div></div>;
}

export default Close;
