import React from "react";
import styled from "styled-components";
import { Button, Form, Input, Breadcrumb, InputNumber } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { createPackage } from "redux/slices/tenants";
import { useTranslation } from "react-i18next";

const PackageCreate = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    dispatch(
      createPackage({
        ...values,
      })
    );
  };

  return (
    <Wrapper>
      <Breadcrumb>
        <CustomBreadCrumb>{t("tenants.name")}</CustomBreadCrumb>
        <CustomBreadCrumb>{t("tenants.addPackage")}</CustomBreadCrumb>
      </Breadcrumb>
      <div className="wrapper">
        <Form
          name="basic"
          layout="vertical"
          autoComplete="off"
          form={form}
          onFinish={onFinish}
        >
          <Form.Item
            label={t("tenants.name")}
            name="name_edition"
            rules={[
              {
                required: true,
                message: t("tenants.require1"),
              },
            ]}
          >
            <Input style={{ width: "553px" }} />
          </Form.Item>

          <Form.Item
            label={t("tenants.totalUsageRecord")}
            name="limit_record"
            rules={[
              {
                required: true,
                message: t("tenants.require2"),
              },
            ]}
          >
            <InputNumber min={1} style={{ width: "553px" }} />
          </Form.Item>

          <Form.Item
            label={t("tenants.notificationThreshold")}
            name="allow_warning"
            // rules={[
            //   {
            //     required: true,
            //     message: "Tổng bản ghi sử dụng!",
            //   },
            // ]}
          >
            <InputNumber style={{ width: "553px" }} max={100} min={1} />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className="add-btn">
              {t("common.save")}
            </Button>
            <Button
              type="primary"
              htmlType="submit"
              className="cancel-btn"
              onClick={() => {
                navigate("/package");
              }}
            >
              {t("common.cancel")}
            </Button>
          </Form.Item>
        </Form>
      </div>
    </Wrapper>
  );
};

export default PackageCreate;

const Wrapper = styled.div`
  padding: 24px;
  width: 100%;

  .wrapper {
    background-color: white;
    width: 100%;
    padding: 16px 24px;
    margin-top: 24px;
    label {
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #2c2c2c;
    }
    .add-btn {
      background: #20a2a2;
      border: 1px solid #20a2a2;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
      border-radius: 2px;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #ffffff;
      margin-bottom: 16px;
      width: 115px;
      margin-right: 24px;
    }
    .cancel-btn {
      background: #fff;
      border: 1px solid #d9d9d9;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
      border-radius: 2px;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #2c2c2c;
      margin-bottom: 16px;
      width: 115px;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    }
  }
`;
const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: default;
`;
