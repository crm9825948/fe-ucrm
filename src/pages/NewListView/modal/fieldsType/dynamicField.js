import { Form, Select } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setHiddenArray } from "redux/slices/objects";

const { Option } = Select;

const DynamicField = (props) => {
  const { field } = props;
  const { hiddenArray } = useSelector((state) => state.objectsReducer);

  const dispatch = useDispatch();

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Select
        allowClear
        onClear={() => {
          // dispatch(setFields(rootFields));
          let hiddenArrayTmp = [...hiddenArray];
          /*eslint-disable-next-line*/
          Object.entries(field.list_items).forEach(([key, value], index) => {
            hiddenArrayTmp = [...hiddenArrayTmp, ...value];
          });
          hiddenArrayTmp = [...new Set(hiddenArrayTmp)];
          dispatch(setHiddenArray(hiddenArrayTmp));
        }}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        onChange={(e) => {
          //get hidden array
          if (e) {
            let hiddenArrayTmp = [...hiddenArray];
            /*eslint-disable-next-line*/
            Object.entries(field.list_items).forEach(([key, value], index) => {
              hiddenArrayTmp = [...hiddenArrayTmp, ...value];
            });
            hiddenArrayTmp = [...new Set(hiddenArrayTmp)];
            /*eslint-disable-next-line*/
            field.list_items[e].map((item, idx) => {
              hiddenArrayTmp = hiddenArrayTmp.filter((ele) => ele !== item);
            });
            dispatch(setHiddenArray(hiddenArrayTmp));
          }
        }}
      >
        {field.option.map((item, idx) => {
          return (
            <Option value={item.value} key={idx}>
              {item.label}
            </Option>
          );
        })}
      </Select>
    </Form.Item>
  );
};

export default DynamicField;
