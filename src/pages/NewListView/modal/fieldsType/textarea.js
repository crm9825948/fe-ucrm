import React from "react";
import { Form, Input } from "antd";

const { TextArea } = Input;

const Textarea = (props) => {
  const { field } = props;
  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <TextArea
        rows={field.rows}
        showCount
        maxLength={
          field.object_id !== "obj_crm_campaign_task_00001" && field.max_length
        }
        placeholder={field.placeholder}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        autoComplete={field.auto_fill ? "on" : "off"}
      />
    </Form.Item>
  );
};

export default Textarea;
