import { Form, Input } from "antd";
import React from "react";

const ID = (props) => {
  const { field } = props;
  return (
    <Form.Item label={field.name} name={field.ID}>
      <Input
        maxLength={field.max_length}
        placeholder={field.placeholder}
        disabled={true}
      />
    </Form.Item>
  );
};

export default ID;
