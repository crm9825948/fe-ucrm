import { Form, Input } from "antd";
import React from "react";

const Text = (props) => {
  const { field } = props;

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Input
        maxLength={field.max_length}
        placeholder={field.placeholder}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        autoComplete={field.auto_fill ? "on" : "off"}
      />
    </Form.Item>
  );
};

export default Text;
