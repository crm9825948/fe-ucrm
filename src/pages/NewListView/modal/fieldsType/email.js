import React from "react";
import { Form, Input } from "antd";

const Email = (props) => {
  const { field } = props;

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          type: "email",
          message: "The input is not valid E-mail!",
        },
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Input
        maxLength={field.max_length}
        placeholder={field.placeholder}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
      />
    </Form.Item>
  );
};

export default Email;
