import { UploadOutlined } from "@ant-design/icons";
import { Button, Form, Upload } from "antd";
import { BASE_URL_API, BE_URL } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";

const File = (props) => {
  const { field, form, recordID, open } = props;
  const { objectId } = useParams();
  const { showModal } = useSelector((state) => state.objectsReducer);
  const [fileList, setFileList] = useState([]);
  // const [defaultFileList, setDefaultFileList] = useState([]);

  useEffect(() => {
    if (showModal === true || open === true) {
      let value = form.getFieldValue(field.ID);
      if (recordID && value) {
        let newArrayFile = [];
        // eslint-disable-next-line
        if (typeof value === "string") {
          value = [value];
        }
        value &&
          // eslint-disable-next-line
          value.map((item, idx) => {
            if (typeof item === "string") {
              let nameArray = item.split("/");
              let newItem = {
                uid: Math.random() * 10000 + "",
                name: nameArray[nameArray.length - 1],
                status: "success",
                url: item,
              };
              newArrayFile.push(newItem);
            }
          });
        // setDefaultFileList(newArrayFile);
        setFileList(newArrayFile);
        form.setFieldsValue({
          [field.ID]: newArrayFile,
        });
      } else {
        setFileList([]);
        form.setFieldsValue({
          [field.ID]: [],
        });
      }
    }
    // eslint-disable-next-line
  }, [showModal, recordID, open]);

  // useEffect(() => {
  // if (!showModal && !open) {
  //   setFileList([]);
  //   form.setFieldsValue({
  //     [field.ID]: [],
  //   });
  // }
  // if (!open) {
  //   setFileList([]);
  //   form.setFieldsValue({
  //     [field.ID]: [],
  //   });
  // }
  // eslint-disable-next-line
  // }, [showModal, open]);

  const info = {
    name: "file",
    action: BASE_URL_API + "upload-file",
    headers: {
      Authorization: localStorage.getItem("setting_accessToken"),
    },
    data: {
      obj: objectId,
    },
    defaultFileList: fileList,
    onChange(info) {
      const { fileList: newFileList } = info;

      /* eslint-disable-next-line */
      newFileList.map((file, idx) => {
        if (file.url === undefined) {
          file.url =
            BASE_URL_API + file &&
            file.response &&
            file.response.data &&
            file.response.data[0];
          file.url = BE_URL + file.url;
        }
      });
      setFileList(newFileList);
    },
  };

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Upload
        {...info}
        maxCount={10}
        multiple
        fileList={fileList}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
      >
        <Button
          icon={<UploadOutlined />}
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
        >
          Click to upload
        </Button>
      </Upload>
    </Form.Item>
  );
};

export default File;
