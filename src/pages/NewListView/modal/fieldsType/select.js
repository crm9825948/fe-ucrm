import { Form, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { useParams } from "react-router";
import { loadValue } from "redux/slices/picklist";
import { loadTarget } from "redux/slices/objects";

const { Option } = Select;

const SelectType = (props) => {
  const { field, form, formValue, setFormValue, recordID, objectId } = props;
  const dispatch = useDispatch();
  // const { objectId } = useParams();
  const { listValue } = useSelector((state) => state.picklistReducer);
  const { recordData, target } = useSelector((state) => state.objectsReducer);
  const [test, setTest] = useState({});

  useEffect(() => {
    if (field.source && formValue[field.source] === null) {
      let newObj = { ...formValue };
      newObj[field.ID] = null;
      form.setFieldsValue(newObj);
    }
    /* eslint-disable-next-line */
  }, [formValue]);

  useEffect(() => {
    let value = form.getFieldsValue();
    /* eslint-disable-next-line */
    target.map((item, idx) => {
      form.setFieldsValue({ value, [item]: null });
    });
    setFormValue(form.getFieldsValue());
    /* eslint-disable-next-line */
  }, [target]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      setFormValue(form.getFieldsValue());
      if (field.target && field.target.length > 0) {
        /* eslint-disable-next-line */
        field.target.map((item, idx) => {
          dispatch(
            loadValue({
              data: {
                Object_ID: objectId,
                Source_ID: field.ID,
                Target_ID: item,
                Value: form.getFieldValue(field.ID),
              },
              ID: item,
              listValue: { ...listValue },
            })
          );
        });
      }
      //  else if (field.source && field.target && field.target.length > 0) {
      //   /* eslint-disable-next-line */
      //   field.target.map((item, idx) => {
      //     dispatch(
      //       loadValue({
      //         data: {
      //           Object_ID: objectId,
      //           Source_ID: field.ID,
      //           Target_ID: item,
      //           Value: form.getFieldValue(field.ID),
      //         },
      //         ID: item,
      //         listValue: { ...listValue },
      //       })
      //     );
      //   });
      // }
      else if (field.source && field.target && field.target.length === 0) {
      }
    }
    /* eslint-disable-next-line */
  }, [recordData, recordID]);

  useEffect(() => {
    let newObj = { ...test, ...listValue };
    setTest(newObj);
    /* eslint-disable-next-line */
  }, [listValue]);

  if (!field.source && !field.target) {
    //Select thường
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: field.required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
          mode={`${field.multiple === true ? "multiple" : ""}`}
        >
          {field.option.map((item, idx) => {
            return (
              <Option value={item.value} key={idx}>
                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    );
  } else if (
    (field.source === null && field.target && field.target.length > 0) ||
    (field.source === undefined && field.target && field.target.length > 0) ||
    (field.source === "" && field.target && field.target.length > 0)
  ) {
    //Level 1
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: field.required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
          onChange={(e) => {
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              dispatch(
                loadValue({
                  data: {
                    Object_ID: objectId,
                    Source_ID: field.ID,
                    Target_ID: item,
                    Value: e,
                  },
                  ID: item,
                  listValue: listValue,
                })
              );
            });

            let tempFormValue = { ...form.getFieldsValue() };
            tempFormValue[field.ID] = e;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
          onClear={() => {
            let tempFormValue = { ...formValue };
            tempFormValue[field.ID] = null;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });

            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
        >
          {field.option.map((item, idx) => {
            return (
              <Option value={item.value} key={idx}>
                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    );
  } else if (field.source && field.target && field.target.length > 0) {
    //Level 2
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: field.required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          onClear={() => {
            let tempFormValue = { ...formValue };
            tempFormValue[field.ID] = null;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
          disabled={
            formValue[field.source] ||
            (field.permission_read && field.permission_write === false)
              ? false
              : true
          }
          onChange={(e) => {
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              dispatch(
                loadValue({
                  data: {
                    Object_ID: objectId,
                    Source_ID: field.ID,
                    Target_ID: item,
                    Value: e,
                  },
                  ID: item,
                  listValue: listValue,
                })
              );
            });

            let tempFormValue = { ...form.getFieldsValue() };
            tempFormValue[field.ID] = e;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);

            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
        >
          {test && test[field.ID]
            ? Object.entries(test && test[field.ID]).map(
                ([key, value], idx) => {
                  return (
                    <Option value={key} key={idx}>
                      {key}
                    </Option>
                  );
                }
              )
            : field.option.map((item, idx) => {
                return (
                  <Option value={item.value} key={idx}>
                    {item.label}
                  </Option>
                );
              })}
        </Select>
      </Form.Item>
    );
  } else if (
    (field.source && field?.target?.length === 0) ||
    (field.source && field?.target === "")
  ) {
    //Level 3
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: field.required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          disabled={
            formValue[field.source] && field.permission_write ? false : true
          }
          allowClear
        >
          {test && test[field.ID] && Object.keys(test[field.ID]).length > 0
            ? Object.entries(test && test[field.ID]).map(
                ([key, value], idx) => {
                  return (
                    <Option value={key} key={idx}>
                      {key}
                    </Option>
                  );
                }
              )
            : ""}
        </Select>
      </Form.Item>
    );
  } else {
    return <></>;
  }
};

export default SelectType;
