import { DatePicker, Form } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

const DateType = (props) => {
  const { field } = props;

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);
  const [format, setFormat] = useState("");
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(loadConfig());
  // }, [dispatch]);

  useEffect(() => {
    let formatTemp = optionsDate.find(
      (ele) => ele.value === allConfig.tenant_date_format
    );
    setFormat(formatTemp && formatTemp.label);
    // eslint-disable-next-line
  }, [allConfig]);

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "YYYY MM DD",
      value: "%Y %m %d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "MM DD YYYY",
      value: "%m %d %Y",
    },

    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
    {
      label: "DD MM YYYY",
      value: "%d %m %Y",
    },
  ];

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <DatePicker
        format={format}
        placeholder={field.placeholder}
        style={{ width: "100%" }}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
      />
    </Form.Item>
  );
};

export default DateType;
