import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Form, Input } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadDataLinking,
  loadHeaderLinking,
  loadLinkingFieldValue,
  loadPaginationLinking,
  setLinkingFieldValue,
} from "redux/slices/objects";
import LinkingList from "./linking/linkingList";

const Linking = (props) => {
  const {
    field,
    form,
    setDataFilterLinking,
    dataFilterLinking,
    recordData,
    selectedRowKeys: selectedRowKeysOutside,
  } = props;

  const [visible, setVisible] = useState(false);

  const dispatch = useDispatch();
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);

  const { linkingFieldValue, isLoadLinking } = useSelector(
    (state) => state.objectsReducer
  );

  useEffect(() => {
    setSelectedRowKeys([]);
  }, []);

  useEffect(() => {
    if (recordData) {
      let arr = [];
      let value =
        recordData[field.ID] && recordData[field.ID].id_related_record;
      arr.push(value);
      setSelectedRowKeys(arr);
    }
    /* eslint-disable-next-line */
  }, [recordData]);

  useEffect(() => {
    if (visible === true) {
      let dataFilter = [];
      /* eslint-disable-next-line */
      Object.entries(dataFilterLinking).forEach(([key, value], index) => {
        if (value && value.id_field_related_record !== null) {
          dataFilter.push(value);
        }
      });
      dispatch(
        loadHeaderLinking({
          object_id: field.objectname,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: dataFilter,
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: dataFilter,
        })
      );
    }
    /* eslint-disable-next-line */
  }, [dispatch, visible, dataFilterLinking]);

  const handleSelect = (selectedRowKeys) => {
    dispatch(
      loadLinkingFieldValue({
        object_related: field.objectname,
        record_id: selectedRowKeys[0],
        object_create_record: field.object_id,
      })
    );
    // setVisible(false);
  };

  useEffect(() => {
    if (isLoadLinking === false) {
      setVisible(false);
    }
  }, [isLoadLinking]);

  useEffect(() => {
    // let temp = { ...dataFilterLinking };
    setDataFilterLinking({
      // ...temp,
      ...linkingFieldValue,
    });
    // form.setFieldsValue(linkingFieldValue);
    /* eslint-disable-next-line */
  }, [linkingFieldValue]);

  useEffect(() => {
    if (
      typeof (
        linkingFieldValue &&
        linkingFieldValue[field.ID] &&
        linkingFieldValue[field.ID].value
      ) === "undefined"
    ) {
      form.setFieldsValue({
        [field.ID]: null,
      });
    }
    if (
      linkingFieldValue &&
      linkingFieldValue[field.ID] &&
      linkingFieldValue[field.ID].value === null
    ) {
      setSelectedRowKeys([]);
    }
    /* eslint-disable-next-line */
  }, [linkingFieldValue]);

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      {field.key === true ? (
        <Input
          value={
            (linkingFieldValue &&
              linkingFieldValue[field.ID] &&
              linkingFieldValue[field.ID].value) ||
            null
          }
          maxLength={field.max_length}
          placeholder={field.placeholder}
          disabled={
            !field.key ||
            (field.permission_read && field.permission_write === false)
          }
          addonBefore={
            linkingFieldValue &&
            linkingFieldValue[field.ID] &&
            linkingFieldValue[field.ID].value ? (
              <ClearOutlined
                onClick={() => {
                  let newObject = {};
                  /* eslint-disable-next-line */
                  Object.entries(linkingFieldValue).forEach(
                    // eslint-disable-next-line
                    ([key, value], index) => {
                      newObject[key] = {
                        id_field_related_record: null,
                        id_related_record: null,
                        object_related: null,
                        value: null,
                      };
                    }
                  );

                  let temp = { ...dataFilterLinking };
                  setDataFilterLinking({
                    ...temp,
                    ...newObject,
                  });

                  dispatch(setLinkingFieldValue(newObject));
                  form.setFieldsValue(newObject);
                }}
              />
            ) : (
              ""
            )
          }
          addonAfter={
            <>
              {!field.permission_read || field.permission_write === true ? (
                <SearchOutlined
                  onClick={() => {
                    setVisible(true);
                  }}
                  disabled={field.readonly}
                />
              ) : (
                ""
              )}
            </>
          }
        />
      ) : (
        <Input
          value={
            (linkingFieldValue &&
              linkingFieldValue[field.ID] &&
              linkingFieldValue[field.ID].value) ||
            null
          }
          maxLength={field.max_length}
          placeholder={field.placeholder}
          disabled={!field.key}
        />
      )}
      {visible === true ? (
        <LinkingList
          setVisible={setVisible}
          visible={visible}
          field={field}
          selectedRowKeys={selectedRowKeys}
          setSelectedRowKeys={setSelectedRowKeys}
          handleSelect={handleSelect}
          dataFilterLinking={dataFilterLinking}
          recordData={recordData}
          selectedRowKeysOutside={selectedRowKeysOutside}
        />
      ) : (
        ""
      )}
    </Form.Item>
  );
};

export default Linking;
