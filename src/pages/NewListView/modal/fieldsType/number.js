import { Form, InputNumber } from "antd";
import React from "react";

const NumberType = (props) => {
  const { field } = props;

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
        () => ({
          validator(_, value) {
            if (value && isNaN(value)) {
              return Promise.reject(`${field.name} has to be a number.`);
            }

            return Promise.resolve();
          },
        }),
      ]}
    >
      <InputNumber
        step={field.step}
        placeholder={field.placeholder}
        style={{ width: "100%" }}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        autoComplete={field.auto_fill ? "on" : "off"}
        formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
      />
    </Form.Item>
  );
};

export default NumberType;
