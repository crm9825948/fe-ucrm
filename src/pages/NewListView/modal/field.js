import { Col, DatePicker, Input, InputNumber, Row, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import moment from "moment";

const { Option } = Select;

const Field = (props) => {
  const {
    data,
    headers,
    mappingField,
    index,
    setConverter,
    setMappingField,
    converter,
    mappingDefault,
    setMappingDefault,
  } = props;
  const [flagHeading, setFlagHeading] = useState(false);
  const { users } = useSelector((state) => state.objectsReducer);
  const [defaultFlag, setDefaultFlag] = useState(false);

  useEffect(() => {
    if (mappingField && mappingField[index] && mappingField[index].header) {
      setFlagHeading(true);
    }
    /* eslint-disable-next-line */
  }, [mappingField]);

  return (
    <>
      <Row gutter={[24, 24]} style={{ marginBottom: "24px" }}>
        <Col span={6}>
          {data.name}
          {data.required ? <span style={{ color: "red" }}>*</span> : ""}
        </Col>
        <Col span={10}>
          <Select
            style={{ width: "100%" }}
            value={
              mappingField && mappingField[index] && mappingField[index].header
            }
            allowClear
            disabled={defaultFlag}
            onClear={() => {}}
            onChange={(e) => {
              let tmp = { ...converter };
              tmp[e] = data.type;
              setConverter(tmp);
              if (e) {
                setFlagHeading(true);
                let newArr = [...mappingField];
                let newEle = {
                  id: data.ID,
                  header: e,
                };
                newArr[index] = newEle;
                setMappingField(newArr);
              } else {
                setFlagHeading(false);
                let newArr = [...mappingField];
                newArr[index] = undefined;
                setMappingField(newArr);
              }
            }}
          >
            {headers.map((item, idx) => {
              return <Option value={item}>{item}</Option>;
            })}
          </Select>
        </Col>
        {/* {linkingobject} */}
        {data.type === "linkingobject" ? (
          <Col span={8}>
            <Input disabled />
          </Col>
        ) : (
          ""
        )}
        {/* {text} */}
        {data.type === "text" || data.type === "textarea" ? (
          <Col span={8}>
            <Input
              maxLength={data.maxLength || 100}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e.target.value,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            />
          </Col>
        ) : (
          ""
        )}
        {/* {select} */}
        {data.type === "select" || data.type === "dynamic-field" ? (
          <Col span={8}>
            <Select
              style={{ width: "100%" }}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            >
              {data.option.map((item, idx) => {
                return <Option value={item.value}>{item.label}</Option>;
              })}
            </Select>
          </Col>
        ) : (
          ""
        )}
        {data.type === "date" ? (
          <Col span={8}>
            <DatePicker
              placeholder={data.placeholder}
              style={{ width: "100%" }}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e ? moment(e).format("YYYY-MM-DD") : null,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            />
          </Col>
        ) : (
          ""
        )}
        {data.type === "datetime-local" ? (
          <Col span={8}>
            <DatePicker
              placeholder={data.placeholder}
              style={{ width: "100%" }}
              showTime
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e ? moment(e).format("YYYY-MM-DD HH:mm:ss") : null,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            />
          </Col>
        ) : (
          ""
        )}
        {data.type === "user" ? (
          <Col span={8}>
            <Select
              style={{ width: "100%" }}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            >
              {users.records.map((item, idx) => {
                return <Option value={item._id}>{item.Full_Name}</Option>;
              })}
            </Select>
          </Col>
        ) : (
          ""
        )}
        {data.type === "email" ? (
          <Col span={8}>
            <Input
              type={"email"}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            />
          </Col>
        ) : (
          ""
        )}
        {data.type === "number" ? (
          <Col span={8}>
            <InputNumber
              step={data.step}
              placeholder={data.placeholder}
              style={{ width: "100%" }}
              disabled={flagHeading}
              onChange={(e) => {
                if (e) {
                  setDefaultFlag(true);
                  let newArr = [...mappingDefault];
                  let newEle = {
                    id: data.ID,
                    value: e,
                  };
                  newArr[index] = newEle;
                  setMappingDefault(newArr);
                } else {
                  setDefaultFlag(false);
                  let newArr = [...mappingDefault];
                  newArr.splice(index, 1);
                  setMappingDefault(newArr);
                }
              }}
            />
          </Col>
        ) : (
          ""
        )}
      </Row>
    </>
  );
};

export default Field;
