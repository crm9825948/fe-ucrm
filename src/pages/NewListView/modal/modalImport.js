import { UploadOutlined } from "@ant-design/icons";
import { Button, Col, Modal, Radio, Row, Select, Steps, Upload } from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { loadUser, mappingFieldAndUpdate } from "redux/slices/objects";
import styled from "styled-components";
import Field from "./field";

const { Step } = Steps;
const { Option, OptGroup } = Select;

const LinkingRule = (props) => {
  /* eslint-disable-next-line */
  const { listObjectField, setLinkingRule, linkingRule, objectId } = props;
  return (
    <WrapperLinkingRule>
      {/* eslint-disable-next-line */}
      {listObjectField.map((item, idx) => {
        if (Object.keys(item)[0] !== "main_object")
          return (
            <CustomRowLinkingRule>
              <Col span={6}>
                Match {Object.values(item)[0]["object_name"]} by
              </Col>
              <Col span={18}>
                <Select
                  style={{ width: "100%" }}
                  value={
                    (linkingRule &&
                      linkingRule[idx] &&
                      linkingRule[idx]["match_by"]) ||
                    null
                  }
                  onChange={(e) => {
                    let tmp = [...linkingRule];
                    let primary_linking_field = "";
                    let secondary_linking_fields = [];

                    let key = Object.keys(item);

                    listObjectField[listObjectField.length - 1]["main_object"][
                      "sections"
                      /* eslint-disable-next-line */
                    ].map((item, idx) => {
                      /* eslint-disable-next-line */
                      item.fields.map((field, index) => {
                        if (
                          field.type === "linkingobject" &&
                          field.key === true &&
                          field.objectname === key[0]
                        ) {
                          primary_linking_field = field.ID;
                        }
                        if (
                          field.type === "linkingobject" &&
                          field.key === false &&
                          field.objectname === key[0]
                        ) {
                          secondary_linking_fields.push(field.ID);
                        }
                      });
                    });

                    tmp[idx] = {
                      object_id: key[0],
                      match_by: e,
                      primary_linking_field: primary_linking_field,
                      secondary_linking_fields: secondary_linking_fields,
                    };
                    setLinkingRule(tmp);
                  }}
                >
                  {Object.values(item)[0].sections.map((ele, index) => {
                    return (
                      <OptGroup label={ele.section_name}>
                        {ele.fields.map((field, index) => {
                          return (
                            <Option value={field.ID} key={field.name}>
                              {field.name}
                            </Option>
                          );
                        })}
                      </OptGroup>
                    );
                  })}
                </Select>
              </Col>
            </CustomRowLinkingRule>
          );
      })}
    </WrapperLinkingRule>
  );
};

const CustomRowLinkingRule = styled(Row)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 28px;
`;

const WrapperLinkingRule = styled.div`
  padding: 47px;
`;

const Rule = (props) => {
  const { listField, listRule, setListRule } = props;
  return (
    <div>
      {listRule.map((item, idx) => {
        return (
          <CustomRow>
            <CustomCol span={2}>Rule {idx + 1}</CustomCol>
            <Col span={3}>
              <Button
                size="small"
                onClick={() => {
                  let tmp = [...listRule];
                  tmp.splice(idx, 1);
                  setListRule(tmp);
                }}
              >
                {/* <DeleteOutlined /> */}
                <img alt="" style={{ width: "16px" }} src={deleteImg} />
              </Button>
            </Col>
            <Col span={19}>
              <RuleDecs>Select fields to compare when detect</RuleDecs>
              <Select
                style={{ width: "100%", marginTop: "8px" }}
                placeholder="Please select"
                mode="multiple"
                value={listRule[idx]}
                onChange={(e) => {
                  let tmp = [...listRule];
                  tmp[idx] = e;
                  setListRule(tmp);
                }}
              >
                {listField.map((item, idx) => {
                  return <Option value={item.ID}>{item.name}</Option>;
                })}
              </Select>
            </Col>
          </CustomRow>
        );
      })}
    </div>
  );
};

const CustomRow = styled(Row)`
  padding: 24px;
  background-color: #f2f4f5;
`;

const RuleDecs = styled.div`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  text-align: left;

  /* Character/Color text main */

  color: #2c2c2c;
`;

const CustomCol = styled(Col)`
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 24px;
  color: #000000;
`;

const ModalImport = ({ isModalVisible, setIsModalVisible, objectId }) => {
  const [headers, setHeaders] = useState([]);
  const [action, setAction] = useState("skip");
  const { fields, listObjectField } = useSelector(
    (state) => state.objectsReducer
  );
  const [listField, setListField] = useState([]);
  const [listRule, setListRule] = useState([[]]);
  const [linkingRule, setLinkingRule] = useState([]);
  //
  const [mappingField, setMappingField] = useState([]);
  const [mappingDefault, setMappingDefault] = useState([]);
  const [converter, setConverter] = useState({});
  const [file, setFile] = useState(null);
  const dispatch = useDispatch();
  const { isLoadingMapping } = useSelector((state) => state.objectsReducer);
  useEffect(() => {
    if (!isLoadingMapping) {
      handleCancel();
    }
    /* eslint-disable-next-line */
  }, [isLoadingMapping]);
  useEffect(() => {
    if (isModalVisible)
      dispatch(
        loadUser({
          current_page: 1,
          record_per_page: 1000,
        })
      );
  }, [dispatch, isModalVisible]);

  useEffect(() => {
    let newArr = [...mappingField];
    let tmp = { ...converter };
    /* eslint-disable-next-line */
    listField.forEach((item, idx) => {
      let ele = headers.find(
        (element) =>
          (element !== null &&
            element.toUpperCase() === item.name.toUpperCase() + " IMPORT" &&
            item.hidden === false &&
            item.readonly === false) ||
          (element !== null &&
            element.toUpperCase() ===
              item.name.toUpperCase() + " IMPORT (REQUIRED)" &&
            item.hidden === false &&
            item.readonly === false)
      );
      if (ele) {
        tmp[ele] = item.type;
        setConverter(tmp);
        if (ele) {
          // setFlagHeading(true)
          let newEle = {
            id: item.ID,
            header: ele,
          };
          newArr[idx] = newEle;
        } else {
          // setFlagHeading(false)
          newArr.splice(idx, 1);
        }
      }
    });
    setMappingField(newArr);
    /* eslint-disable-next-line */
  }, [listField, headers]);

  useEffect(() => {
    let arr = [];
    /* eslint-disable-next-line */
    fields.map((item, idx) => {
      arr = [...arr, ...item.fields];
    });
    let ele = {
      ID: "owner",
      hidden: false,
      name: "Assign to",
      readonly: false,
      required: false,
      type: "user",
      1: true,
    };
    arr.push(ele);
    setListField(arr);
  }, [objectId, fields]);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setCurrent(0);
    setFile(null);
    setMappingDefault([]);
    setConverter({});
    setMappingField([]);
    setListRule([]);
    setLinkingRule([]);
    setAction("skip");
    setHeaders([]);
  };
  const props = {
    name: "file",
    action: BASE_URL_API + "crm/get-file-headers",
    headers: {
      Authorization: localStorage.getItem("setting_accessToken"),
    },
    data: {
      obj: objectId,
    },
    onChange(info) {
      if (info.file.status !== "uploading") {
      }
      if (info.file.status === "done") {
        const formDataSave = new FormData();
        formDataSave.append("file", info.file.originFileObj);
        setFile(formDataSave);
        setHeaders(info.file.response.data.headers || []);
      } else if (info.file.status === "error") {
      }
    },
  };

  const [current, setCurrent] = useState(0);

  const _onPrev = () => {
    setCurrent(current - 1);
  };
  let navigate = useNavigate();

  return (
    <>
      <CustomModal
        title="Import record"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
        footer={false}
      >
        <Steps current={current} percent={60}>
          <Step title="Tải lên" />
          <Step title="Xử lý" />
          <Step title="Linking rule" />
          <Step title="Liên kết trường" />
        </Steps>
        {current === 0 ? (
          <Wrapper>
            <Upload
              {...props}
              accept=".xlsx, .xls, .csv"
              maxCount={1}
              style={{ display: "flex", justifyContent: "center" }}
            >
              <Button icon={<UploadOutlined />}>Click to Upload</Button>
            </Upload>
            <div style={{ marginTop: "32px" }}>
              <span style={{ marginRight: "16px" }}>Import Logs:</span>
              <span
                onClick={() => {
                  navigate("/view-logs");
                }}
                style={{ cursor: "pointer" }}
              >
                View here
              </span>
            </div>
          </Wrapper>
        ) : (
          ""
        )}

        {current === 1 ? (
          <div>
            <TitleAction>Action for dupllicate record</TitleAction>
            <Radio.Group
              value={action}
              onChange={(e) => {
                setAction(e.target.value);
              }}
            >
              <Radio.Button value="skip">Skip</Radio.Button>
              <Radio.Button value="overwrite">Overwrite</Radio.Button>
              <Radio.Button value="merge">Merge</Radio.Button>
            </Radio.Group>
            <Header>
              <div>Dupplicate</div>
              <Button
                style={{ marginLeft: "16px" }}
                onClick={() => {
                  let tmp = [...listRule];
                  tmp.push([]);
                  setListRule(tmp);
                }}
              >
                + Add
              </Button>
            </Header>
            <Rule
              listField={listField}
              listRule={listRule}
              setListRule={setListRule}
            />
          </div>
        ) : (
          ""
        )}

        {current === 2 ? (
          <LinkingRule
            listObjectField={listObjectField}
            linkingRule={linkingRule}
            setLinkingRule={setLinkingRule}
            objectId={objectId}
          />
        ) : (
          ""
        )}

        {current === 3 ? (
          <div>
            <Row
              style={{
                marginBottom: "24px",
                fontStyle: "normal",
                fontWeight: "bold",
                fontSize: "16px",
                lineHeight: "24px",
                color: "#2C2C2C",
                marginTop: "54px",
              }}
              gutter={[24, 24]}
            >
              <Col span={6}>Field</Col>
              <Col span={10}>Heading</Col>
              <Col span={8}>Default value</Col>
            </Row>
            {/* eslint-disable-next-line */}
            {listField.map((item, idx) => {
              if (
                item["hidden"] === false &&
                item["readonly"] === false &&
                item["type"] !== "id" &&
                item["type"] !== "formula" &&
                item["type"] !== "file"
              )
                return (
                  <Field
                    data={item}
                    headers={headers}
                    mappingField={mappingField}
                    index={idx}
                    setConverter={setConverter}
                    converter={converter}
                    setMappingField={setMappingField}
                    mappingDefault={mappingDefault}
                    setMappingDefault={setMappingDefault}
                  />
                );
            })}
          </div>
        ) : (
          ""
        )}

        <WrapButton label=" ">
          {current > 0 && <Back onClick={() => _onPrev()}>Back</Back>}
          {current === 3 ? (
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => {
                let mapping_field = [...mappingField];
                mapping_field = mapping_field.filter(
                  (item) => typeof item === "object"
                );

                let mapping_default = [...mappingDefault];
                mapping_default = mapping_default.filter(
                  (item) => typeof item === "object"
                );

                const fieldRequired = [];
                listField.forEach((item, idx) => {
                  if (
                    item.required &&
                    item.type !== "id" &&
                    item.hidden === false
                  ) {
                    fieldRequired.push(item.ID);
                  }
                });

                let checkRequired = [];
                fieldRequired.forEach((item) => {
                  mapping_field.forEach((ele) => {
                    if (ele.id === item) {
                      checkRequired.push(item);
                    }
                  });
                });
                fieldRequired.forEach((item) => {
                  mapping_default.forEach((ele) => {
                    if (ele.id === item) {
                      checkRequired.push(item);
                    }
                  });
                });
                checkRequired = [...new Set(checkRequired)];
                let fieldRequiredTemp = [...new Set(fieldRequired)];
                if (checkRequired.length === fieldRequiredTemp.length) {
                  file.append("mapping_field", JSON.stringify(mapping_field));
                  file.append(
                    "mapping_default",
                    JSON.stringify(mapping_default)
                  );
                  file.append("object_id", objectId);
                  file.append("converters", JSON.stringify(converter));
                  file.append("duplicate_rule", action);
                  file.append("unique_field_rules", JSON.stringify(listRule));
                  file.append("linking_rules", JSON.stringify(linkingRule));
                  file.append("api_version", "2");

                  dispatch(mappingFieldAndUpdate(file));
                } else {
                  Notification("warning", "Please fullfill required!");
                }
              }}
            >
              Save
            </Button>
          ) : (
            <Button
              type="primary"
              onClick={() => {
                setCurrent(current + 1);
              }}
              disabled={headers.length === 0 ? true : false}
            >
              Next
            </Button>
          )}
          <CancelButton onClick={() => handleCancel()}>Cancel</CancelButton>
        </WrapButton>
      </CustomModal>
    </>
  );
};

export default ModalImport;

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 54px;
`;

const CustomModal = styled(Modal)`
  /* .ant-progress-circle {
    .ant-progress-inner {
      background-color: ${(props) => props.theme.main}!important;
    }
  }

  .ant-progress-inner:not(.ant-progress-circle-gradient) {
    .ant-progress-circle-path {
      stroke: ${(props) => props.theme.main}!important;
    }
  } */
  .ant-progress-inner:not(.ant-progress-circle-gradient) {
    .ant-progress-circle-path {
      stroke: ${(props) => props.theme.main}!important;
    }
  }
  .ant-steps-item-process > .ant-steps-item-container > .ant-steps-item-icon {
    background-color: ${(props) => props.theme.main}!important;
  }
  .ant-steps-item-process {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main}!important;
    }
  }
  .ant-btn:active {
    background-color: ${(props) => props.theme.main}!important;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.white};
  }

  .ant-btn:focus {
    background-color: ${(props) => props.theme.main}!important;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.white};
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled)::before {
    background-color: ${(props) => props.theme.main};
  }

  .ant-steps-item-finish {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main};
    }
    .ant-steps-item-icon > .ant-steps-icon {
      color: ${(props) => props.theme.main};
    }
  }
`;

const WrapButton = styled.div`
  margin-bottom: 0;
  margin-top: 40px;
  text-align: right;
  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin: 0 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const CancelButton = styled(Button)`
  .ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  .ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;

const Back = styled(Button)`
  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;

const Header = styled.div`
  height: 64px;
  background-color: #d9d9d9;
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 24px;
  /* or 150% */

  display: flex;
  align-items: center;

  /* Character/Color text main */

  color: #2c2c2c;
  padding-left: 24px;
  display: flex;
  margin-top: 32px;
`;

const TitleAction = styled.div`
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 24px;
  /* or 150% */

  display: flex;
  align-items: center;

  /* Character/Color text main */

  color: #2c2c2c;
  margin-top: 54px;
  margin-bottom: 8px;
`;
