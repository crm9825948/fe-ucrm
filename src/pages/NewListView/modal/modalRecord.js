import { Button, Col, Dropdown, Form, Menu, Modal, Row, Select } from "antd";
import { useLocation } from "react-router-dom";

import Call from "assets/icons/common/call.svg";
import IconDown from "assets/icons/common/icon-down.svg";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFieldsMappingCallCenter, makeCall } from "redux/slices/callCenter";
import {
  loadUserDynamicButton,
  loadUserDynamicButtonSuccess,
  runDynamicButtonResult,
} from "redux/slices/dynamicButton";
import {
  loadFormCreate,
  loadRecordDataSuccess,
  loadUserAssignTo,
  setHiddenArray,
  setLinkingFieldValue,
  setShowModal,
} from "redux/slices/objects";
import styled from "styled-components";
import Date from "./fieldsType/date";
import Datetime from "./fieldsType/dateTime";
import DynamicField from "./fieldsType/dynamicField";
import Email from "./fieldsType/email";
import File from "./fieldsType/file";
import FormulaField from "./fieldsType/formulaField";
import IDComp from "./fieldsType/ID";
import LinkingObject from "./fieldsType/linkingObject";
import Number from "./fieldsType/number";
import SelectType from "./fieldsType/select";
import Text from "./fieldsType/text";
import Textarea from "./fieldsType/textarea";
import User from "./fieldsType/user";

const { Option } = Select;

const ModalRecord = (props) => {
  // const [form] = Form.useForm();
  const {
    form,
    recordID,
    setRecordID,
    onFinish,
    objectId,
    userDetail,
    setEditingKey,
    setDataConfirm,
    setShowConfirm,
    reload,
  } = props;
  // const { objectId } = useParams();
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  // const [form] = Form.useForm();

  const { showModal } = useSelector((state) => state.objectsReducer);
  const { userDynamicButton, isRunSuccess, isReload } = useSelector(
    (state) => state.dynamicButtonReducer
  );

  const { fieldsMappingCallCenter } = useSelector(
    (state) => state.callCenterReducer
  );

  const [dataFilterLinking, setDataFilterLinking] = useState({});

  const [numberOfFields, setNumberOfFields] = useState(0);
  const [formValue, setFormValue] = useState({});
  const {
    fields,
    isLoading,
    userAssignTo,
    // searchList,
    linkingFieldValue,
    recordData,
    originalFields,
    hiddenArray,
    hiddenDynamic,
    listObjectField,
  } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    if (isLoading === false && !pathname.split("/").includes("create-record")) {
      dispatch(setShowModal(false));
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    // dispatch(
    //   loadFormCreate({
    //     object_id: objectId,
    //   })
    // );
    dispatch(loadUserAssignTo());
  }, [objectId, dispatch]);

  useEffect(() => {
    form.setFieldsValue({
      ...form.getFieldsValue(),
      ...linkingFieldValue,
    });
  }, [linkingFieldValue, form]);

  useEffect(() => {
    let newObject = {};
    /* eslint-disable-next-line */
    Object.entries(linkingFieldValue).forEach(([key, value], index) => {
      newObject[key] = {
        id_field_related_record: null,
        id_related_record: null,
        object_related: null,
        value: null,
      };
    });

    let temp = {};
    setDataFilterLinking({
      ...temp,
      ...newObject,
    });
    if (!recordID) {
      // Quân thêm fix bug BUG10953
      dispatch(setLinkingFieldValue(newObject));
    }
    form.setFieldsValue(newObject);
    if (!showModal) {
      form.resetFields();
    }

    /* eslint-disable-next-line */
  }, [showModal]);

  useEffect(() => {
    let numberOfFields = 0;
    /* eslint-disable-next-line */
    originalFields.map((field, idx) => {
      numberOfFields = numberOfFields + field.fields.length;
    });
    setNumberOfFields(numberOfFields);
  }, [originalFields]);

  useEffect(() => {
    if (showModal) {
      if (recordID) {
      } else {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });
      }
    }
    /*eslint-disable-next-line*/
  }, [userDetail, showModal]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      form.setFieldsValue({
        assignTo: recordData.owner,
      });
      setFormValue(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (form.getFieldValue(field.ID)) {
              /*eslint-disable-next-line*/
              field.list_items[form.getFieldValue(field.ID)].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      dispatch(setHiddenArray(tmpHidden));
    }
    /* eslint-disable-next-line */
  }, [recordData, recordID, fields]);

  useEffect(() => {
    if (recordID === "") {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, index) => {
          if (field.type === "dynamic-field" && field.default_value) {
            form.setFieldsValue({
              [field.ID]: field.default_value,
            });
            /*eslint-disable-next-line*/
            field.list_items[field.default_value].map((item, idx) => {
              hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
            });
            dispatch(setHiddenArray(hiddenTmp));
          } else if (field.type === "select" && field.default_value) {
            form.setFieldsValue({
              [field.ID]: field.default_value,
            });
          }
        });
      });
      setFormValue("");
    }
    /*eslint-disable-next-line*/
  }, [recordID, showModal]);

  useEffect(() => {
    originalFields &&
      /*eslint-disable-next-line*/
      originalFields.map((section, idx) => {
        /*eslint-disable-next-line*/
        section.fields.map((field, idx) => {
          if (field.hidden === false && field.type === "dynamic-field") {
            form.setFieldsValue({
              [field.ID]: "",
            });
          }
        });
      });
    /*eslint-disable-next-line*/
  }, [originalFields]);

  useEffect(() => {
    if (showModal && recordID) {
      dispatch(
        loadUserDynamicButton({
          object_id: objectId,
          record_id: recordID,
        })
      );
    } else {
      dispatch(loadUserDynamicButtonSuccess([]));
    }
  }, [dispatch, objectId, recordID, showModal]);

  useEffect(() => {
    if (isRunSuccess) {
      dispatch(setShowModal(false));
      dispatch(
        runDynamicButtonResult({
          isRunSuccess: false,
          isReload: false,
        })
      );
      setEditingKey("");
      setRecordID("");
      if (isReload && reload) {
        reload();
      }
      form.resetFields();
    }
  }, [
    dispatch,
    form,
    isRunSuccess,
    isReload,
    reload,
    setEditingKey,
    setRecordID,
  ]);

  const _onRunButton = (id, reloadable) => {
    setDataConfirm({
      data: {
        object_id: objectId,
        record_id: recordID,
        button_id: id,
      },
      reloadable: reloadable || false,
    });
    setShowConfirm(true);
  };

  const _onMakeCall = (value, hotline) => {
    if (localStorage.getItem("inCall") === "true") {
      Notification("error", "On call!");
    } else {
      let data = {};
      if (hotline) {
        data = {
          phone: value,
          hotline: hotline,
        };
      } else {
        data = {
          phone: value,
        };
      }
      dispatch(
        makeCall({
          ...data,
        })
      );
      dispatch(setShowModal(false));
    }
    setEditingKey("");
    setRecordID("");
  };

  useEffect(() => {
    if (userDetail.use_cti) {
      dispatch(
        getFieldsMappingCallCenter({
          object_id: objectId,
        })
      );
    }
  }, [userDetail, objectId, dispatch]);

  const menuActions = (
    <Menu>
      {userDynamicButton.map((button, idx) => {
        return (
          <Menu.Item key={idx}>
            <span onClick={() => _onRunButton(button._id, button?.reloadable)}>
              {button.name}
            </span>
          </Menu.Item>
        );
      })}
    </Menu>
  );

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "id":
        return (
          <IDComp field={field} recordData={recordData} recordID={recordID} />
        );
      case "text":
        return <Text field={field} />;
      case "textarea":
        return <Textarea field={field} />;
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} form={form} />;
      case "date":
        return <Date field={field} form={form} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={showModal}
            recordID={recordID}
            setRecordID={setRecordID}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            open={showModal}
          />
        );
      case "dynamic-field":
        return <DynamicField field={field} form={form} recordID={recordID} />;
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={recordData}
          />
        );
      case "formula":
        return <FormulaField field={field} />;
      default:
        break;
    }
  };

  const handleCancel = () => {
    dispatch(setShowModal(false));
    setRecordID("");
    setEditingKey("");
    form.resetFields();
    dispatch(loadRecordDataSuccess({}));
    // dispatch(
    //   loadFormCreate({
    //     object_id: objectId,
    //   })
    // );
  };

  if (numberOfFields > 10) {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID
              ? "Chỉnh sửa bản ghi"
              : "Thêm bản ghi"
          }
          visible={showModal}
          onCancel={() => {
            // dispatch(setShowModal(false));
            // setRecordID("");
            // setEditingKey("");
            // form.resetFields();
            // dispatch(loadRecordDataSuccess({}));
            handleCancel();
          }}
          width={1200}
          footer={false}
          maskClosable={false}
          mask={true}
        >
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 18 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            onValuesChange={(value, values) => {
              setFormValue(values);
            }}
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.Last_Name} {user.Middle_Name} {user.First_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection key={idx}>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        <Row gutter={[70, 24]}>
                          {/* eslint-disable-next-line */}
                          {section.fields.map((field, index) => {
                            if (
                              field.hidden === false &&
                              field.permission_hidden === false &&
                              hiddenArray.findIndex(
                                (ele) => ele === field._id
                              ) < 0 &&
                              field.type !== "formula"
                            ) {
                              if (
                                Object.entries(recordData).length === 0 &&
                                field.type === "id"
                              ) {
                              } else {
                                return (
                                  <CustomCol span={12} key={field._id}>
                                    <div style={{ width: "100%" }}>
                                      {handleFieldType(field, showModal)}
                                    </div>
                                    {Object.entries(recordData).length > 0 &&
                                      recordID &&
                                      Object.keys(fieldsMappingCallCenter)
                                        .length > 0 &&
                                      fieldsMappingCallCenter.phone_fields
                                        .length > 0 &&
                                      fieldsMappingCallCenter.phone_fields.includes(
                                        field.field_id
                                      ) && (
                                        <Dropdown
                                          overlay={
                                            <Menu>
                                              {fieldsMappingCallCenter.phone_fields.map(
                                                (item) => {
                                                  return Object.entries(
                                                    recordData
                                                  ).map(([key, val]) => {
                                                    return (
                                                      <>
                                                        {item === key &&
                                                          item ===
                                                            field.field_id &&
                                                          val.value !==
                                                            null && (
                                                            <>
                                                              {fieldsMappingCallCenter
                                                                .custom_hotlines
                                                                .length > 0 ? (
                                                                <>
                                                                  {fieldsMappingCallCenter.custom_hotlines.map(
                                                                    (
                                                                      hotline
                                                                    ) => {
                                                                      return (
                                                                        <Menu.Item
                                                                          onClick={() =>
                                                                            _onMakeCall(
                                                                              val.value,
                                                                              hotline.hotline
                                                                            )
                                                                          }
                                                                          key={
                                                                            item
                                                                          }
                                                                        >
                                                                          {
                                                                            hotline.prefix
                                                                          }
                                                                          {
                                                                            val.value
                                                                          }
                                                                        </Menu.Item>
                                                                      );
                                                                    }
                                                                  )}
                                                                </>
                                                              ) : (
                                                                <Menu.Item
                                                                  onClick={() =>
                                                                    _onMakeCall(
                                                                      val.value
                                                                    )
                                                                  }
                                                                  key={item}
                                                                >
                                                                  {val.value}
                                                                </Menu.Item>
                                                              )}
                                                            </>
                                                          )}
                                                      </>
                                                    );
                                                  });
                                                }
                                              )}
                                            </Menu>
                                          }
                                          placement="bottom"
                                        >
                                          <ButtonCall size="large">
                                            <img src={Call} alt="makeCall" />
                                          </ButtonCall>
                                        </Dropdown>
                                      )}
                                  </CustomCol>
                                );
                              }
                            }
                          })}
                        </Row>
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                loading={isLoading}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  // dispatch(setShowModal(false));
                  // setRecordID("");
                  // setEditingKey("");
                  // form.resetFields();
                  // dispatch(loadRecordDataSuccess({}));
                  handleCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>
      </>
    );
  } else {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID
              ? "Chỉnh sửa bản ghi"
              : "Thêm bản ghi"
          }
          visible={showModal}
          onCancel={() => {
            // dispatch(setShowModal(false));
            // setRecordID("");
            // form.resetFields();

            // dispatch(setShowModal(false));
            // setRecordID("");
            // setEditingKey("");
            // form.resetFields();
            // dispatch(loadRecordDataSuccess({}));
            handleCancel();
          }}
          width={600}
          footer={false}
          maskClosable={false}
          mask={true}
        >
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 18 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            onValuesChange={(value, values) => {
              setFormValue(values);
            }}
            labelAlign="left"
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.First_Name} {user.Middle_Name} {user.Last_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"] &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection key={idx}>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        {/* eslint-disable-next-line */}
                        {section.fields.map((field, index) => {
                          if (
                            field.hidden === false &&
                            field.permission_hidden === false &&
                            hiddenArray.findIndex((ele) => ele === field._id) <
                              0 &&
                            field.type !== "formula"
                          ) {
                            if (
                              Object.entries(recordData).length === 0 &&
                              field.type === "id"
                            ) {
                            } else
                              return (
                                <div style={{ display: "flex" }}>
                                  <div style={{ width: "100%" }}>
                                    {handleFieldType(field, showModal)}
                                  </div>
                                  {Object.entries(recordData).length > 0 &&
                                    recordID &&
                                    Object.keys(fieldsMappingCallCenter)
                                      .length > 0 &&
                                    fieldsMappingCallCenter.phone_fields
                                      .length > 0 &&
                                    fieldsMappingCallCenter.phone_fields.includes(
                                      field.field_id
                                    ) && (
                                      <Dropdown
                                        overlay={
                                          <Menu>
                                            {fieldsMappingCallCenter.phone_fields.map(
                                              (item) => {
                                                return Object.entries(
                                                  recordData
                                                ).map(([key, val]) => {
                                                  return (
                                                    <>
                                                      {item === key &&
                                                        item ===
                                                          field.field_id &&
                                                        val.value !== null && (
                                                          <>
                                                            {fieldsMappingCallCenter
                                                              .custom_hotlines
                                                              .length > 0 ? (
                                                              <>
                                                                {fieldsMappingCallCenter.custom_hotlines.map(
                                                                  (hotline) => {
                                                                    return (
                                                                      <Menu.Item
                                                                        onClick={() =>
                                                                          _onMakeCall(
                                                                            val.value,
                                                                            hotline.hotline
                                                                          )
                                                                        }
                                                                        key={
                                                                          item
                                                                        }
                                                                      >
                                                                        {
                                                                          hotline.prefix
                                                                        }
                                                                        {
                                                                          val.value
                                                                        }
                                                                      </Menu.Item>
                                                                    );
                                                                  }
                                                                )}
                                                              </>
                                                            ) : (
                                                              <Menu.Item
                                                                onClick={() =>
                                                                  _onMakeCall(
                                                                    val.value
                                                                  )
                                                                }
                                                                key={item}
                                                              >
                                                                {val.value}
                                                              </Menu.Item>
                                                            )}
                                                          </>
                                                        )}
                                                    </>
                                                  );
                                                });
                                              }
                                            )}
                                          </Menu>
                                        }
                                        placement="bottom"
                                      >
                                        <ButtonCall size="large">
                                          <img src={Call} alt="makeCall" />
                                        </ButtonCall>
                                      </Dropdown>
                                    )}
                                </div>
                              );
                          }
                        })}
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                loading={isLoading}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  // dispatch(setShowModal(false));
                  // dispatch(loadRecordDataSuccess({}));
                  // setRecordID("");
                  // setEditingKey("");
                  // form.resetFields();
                  handleCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>
      </>
    );
  }
};

export default ModalRecord;

const WrapperSection = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 15px;
  padding: 24px;
  position: relative;
`;

const CustomTitleSection = styled.div`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 20px;
  /* identical to box height, or 143% */

  display: flex;
  align-items: center;
  color: ${(props) => props.theme.main};

  margin-bottom: 24px;
  position: absolute;
  top: -11px;
  left: 24px;
  background-color: #fff;
  padding: 0 10px;
`;

const CustomModal = styled(Modal)`
  .ant-form-item-label {
    text-align: left;
  }
  label {
    font-size: 16px;
    white-space: normal;
    word-break: normal;
    height: fit-content;
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-modal-body {
    max-height: calc(100vh - 200px);
    padding-bottom: 0;
    overflow-y: scroll;
    /* width */
    ::-webkit-scrollbar {
      width: 5px !important;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px transparent;
      border-radius: 100px;
      background: transparent !important;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #d8d6d6;
      border-radius: 100px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #d8d6d6;
    }
    ::-webkit-scrollbar-track-piece {
      margin-bottom: 10px;
      background: transparent !important;
    }
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-bottom: 24px;
  padding-top: 24px;
`;

const CustomButtonAction = styled(Button)`
  width: 102px;
  margin-right: 16px;

  img {
    margin-right: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;

    img {
      filter: brightness(200);
    }
  }
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const ButtonCall = styled(Button)`
  border-radius: 2px;
  padding: 0 24px;
  border-color: ${(props) => props.theme.main};
  height: 32px;
  margin-left: 8px;

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker} !important;

    img {
      filter: brightness(200);
    }
  }

  :active {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }

  :focus {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }
`;

const CustomCol = styled(Col)`
  display: flex;
`;
