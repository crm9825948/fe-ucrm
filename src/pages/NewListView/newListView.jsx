import "pages/Campaign/campaign.scss";
import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  useMemo,
} from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  loadAllData,
  loadFavouriteObjects,
  loadListObjectField,
} from "redux/slices/objects";
import { render } from "react-dom";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

const NewListView = () => {
  const { objectId, customViewId } = useParams();
  const dispatch = useDispatch();
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const { header, data } = useSelector((state) => state.objectsReducer);
  const [columnDef, setColumnDefs] = useState([]);
  const [dataRender, setDataRender] = useState([]);
  const gridRef = useRef();

  //Load data
  useEffect(() => {
    dispatch(loadFavouriteObjects());
    if (customViewId === "default-view") {
      dispatch(
        loadAllData({
          object_id: {
            object_id: objectId,
          },
          data: {
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    } else {
      dispatch(
        loadAllData({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }

    /* eslint-disable-next-line */
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );

    /*eslint-disable-next-line*/
  }, [dispatch, objectId, customViewId]);

  //Load column
  useEffect(() => {
    let cloneColumnDef = [];
    header.map((item, idx) => {
      if (item.type === "file") {
        return cloneColumnDef.push({
          headerName: item.name,
          field: `${item.ID}.value`,
          resizable: true,
        });
      } else
        return cloneColumnDef.push({
          headerName: item.name,
          field: `${item.ID}.value`,
          resizable: true,
          filter: "agTextColumnFilter",
        });
    });
    setColumnDefs(cloneColumnDef);
  }, [header]);

  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 200,
      resizable: true,
      floatingFilter: true,
    };
  }, []);

  return (
    <div className="ag-theme-alpine" style={{ height: 400, padding: "20px" }}>
      <AgGridReact
        columnDefs={columnDef}
        rowData={data}
        suppressDragLeaveHidesColumns={true}
        defaultColDef={defaultColDef}
      ></AgGridReact>
    </div>
  );
};

export default NewListView;
