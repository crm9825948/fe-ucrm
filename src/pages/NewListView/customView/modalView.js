import { Button, Checkbox, Form, Input, Modal, Select } from "antd";
import Conditions from "components/Conditions/conditions";
// import { META_DATA } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setShowLoadingScreen } from "redux/slices/global";
import {
  createCustomView,
  loadListObjectField,
  updateCustomView,
  loadDetailsCustomViewSuccess,
} from "redux/slices/objects";
import styled from "styled-components";
import Columns from "./dragAndDropColumn/columns";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { Notification } from "components/Notification/Noti";

const { Option, OptGroup } = Select;

const ModalView = (props) => {
  const {
    isModalVisible,
    setIsModalVisible,
    customViewID,
    customViewDetail,
    objectId,
    setCustomViewID,
    reload,
  } = props;
  const { customViewId } = useParams();
  const [columns, setColumns] = useState([]);
  const [form] = Form.useForm();
  let navigate = useNavigate();
  // const { objectId: objectID, customViewId } = useParams();

  const handleOk = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    if (customViewID && Object.keys(customViewDetail).length > 0) {
      form.setFieldsValue({
        default: customViewDetail.is_default,
        view_name: customViewDetail.view_name,
      });
      setAllCondition(customViewDetail.filter_condition.and_filter);
      setAnyCondition(customViewDetail.filter_condition.or_filter);
      setColumns(customViewDetail.show_fields);
      let colArray = [];
      customViewDetail &&
        /*eslint-disable-next-line*/
        customViewDetail.show_fields.map((item, idx) => {
          colArray.push(item.ID);
        });
      // setColumns(colArray);
      form.setFieldsValue({
        column: colArray,
      });
    } else {
      form.resetFields();
      setColumns([]);
    }
    /*eslint-disable-next-line*/
  }, [customViewID, customViewDetail]);

  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
    setAllCondition([]);
    setAnyCondition([]);
    setCustomViewID("");
    setColumns([]);
    dispatch(loadDetailsCustomViewSuccess({}));
  };

  const dispatch = useDispatch();

  const {
    listObjectField,
    isLoading,
    loadingDetails,
    loadingUpdateCustomview,
  } = useSelector((state) => state.objectsReducer);

  const [allFields, setAllFields] = useState([]);

  useEffect(() => {
    if (listObjectField.length > 0) {
      setAllFields(listObjectField[listObjectField.length - 1].main_object);
    }
  }, [listObjectField]);

  useEffect(() => {
    if (isLoading === false || loadingUpdateCustomview === false) {
      setIsModalVisible(false);
      form.resetFields();
      setColumns([]);
    }
    /*eslint-disable-next-line*/
  }, [isLoading, loadingUpdateCustomview]);

  useEffect(() => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );
  }, [dispatch, objectId]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingDetails));
    /*eslint-disable-next-line*/
  }, [loadingDetails]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoading));
    /*eslint-disable-next-line*/
  }, [isLoading]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingUpdateCustomview));
    /*eslint-disable-next-line*/
  }, [loadingUpdateCustomview]);

  //Condition
  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const onFinish = (values) => {
    let dataColumns = [];
    /*eslint-disable-next-line*/
    columns.forEach((field, idx) => {
      dataColumns.push({
        field_ID: field.ID,
      });
    });

    let data = {
      view_name: values.view_name,
      filter_condition: {
        and_filter: allCondition,
        or_filter: anyCondition,
      },
      show_fields: dataColumns,
      is_default: values.default,
      object_id: objectId,
    };

    let dataUpdate = {
      view_name: values.view_name,
      filter_condition: {
        and_filter: allCondition,
        or_filter: anyCondition,
      },
      show_fields: dataColumns,
      is_default: values.default,
    };

    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-mine"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        item === "" &&
        operatorValueOr[idx] !== "not-empty" &&
        item === "" &&
        operatorValueOr[idx] !== "mine" &&
        item === "" &&
        operatorValueOr[idx] !== "not-mine"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill information!");
    } else {
      if (customViewID && Object.keys(customViewDetail).length > 0) {
        if (values.default) {
          navigate(`/objects/${objectId}/${customViewID}`);
          // reload();
        }
        dispatch(
          updateCustomView({
            data: {
              custom_view_id: customViewID,
              data: dataUpdate,
            },
            load: {
              object_id: objectId,
            },
          })
        );

        if (customViewId === customViewID) {
          reload();
        }
        handleCancel();
      } else {
        dispatch(
          createCustomView({
            data: data,
            load: {
              object_id: objectId,
            },
          })
        );
        if (values.default) {
          // navigate(`/objects/${objectId}/default-view`);
          reload();
        }
        handleCancel();
      }
    }
  };

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  return (
    <>
      <Modal
        title={customViewID ? "Chỉnh sửa view" : "Thêm view"}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={600}
        footer={false}
      >
        <Wrapper>
          <Form
            name="basic"
            labelCol={{ span: 6 }}
            wrapperCol={{ span: 18 }}
            onFinish={onFinish}
            onFinishFailed={() => {}}
            autoComplete="off"
            form={form}
            onValuesChange={(value, values) => {}}
          >
            <Form.Item
              label="Tên view"
              name="view_name"
              rules={[
                { required: true, message: "Please input name of view!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Cột hiển thị"
              name="column"
              rules={[{ required: true, message: "Please select columns!" }]}
            >
              <Select
                mode="multiple"
                onChange={(e, values) => {
                  let newArr = [];
                  /*eslint-disable-next-line*/
                  values.map((item, idx) => {
                    newArr.push(item.field);
                  });
                  setColumns(newArr);
                }}
              >
                {allFields &&
                  allFields.sections &&
                  allFields.sections.map((item, idx) => {
                    return (
                      <OptGroup label={item.section_name}>
                        {/*eslint-disable-next-line*/}
                        {item.fields.map((field, index) => {
                          if (!field.hidden)
                            return (
                              <Option value={field.ID} field={field}>
                                {field.name}
                              </Option>
                            );
                        })}
                      </OptGroup>
                    );
                  })}
                {/* {
                  <OptGroup label="Meta data">
                    {META_DATA.map((item, idx) => {
                      return (
                        <Option value={item.value} field={item}>
                          {item.label}
                        </Option>
                      );
                    })}
                  </OptGroup>
                } */}
              </Select>
            </Form.Item>
            <Columns columns={columns} setColumns={setColumns} form={form} />
            <Conditions
              title={"AND condition"}
              decs={"(All conditions must be met)"}
              conditions={allCondition}
              setConditions={setAllCondition}
              ID={customViewID}
              dataDetails={customViewDetail}
              operatorValue={operatorValueAnd}
              setOperatorValue={setOperatorValueAnd}
              value={valueAnd}
              setValue={setValueAnd}
            />
            <Conditions
              title={"OR condition"}
              decs={"(Any conditions must be met)"}
              conditions={anyCondition}
              setConditions={setAnyCondition}
              ID={customViewID}
              dataDetails={customViewDetail}
              operatorValue={operatorValueOr}
              setOperatorValue={setOperatorValueOr}
              value={valueOr}
              setValue={setValueOr}
            />
            <Form.Item label="" name="default" valuePropName="checked">
              <Checkbox>Mặc định</Checkbox>
            </Form.Item>

            <CustomFooter>
              <CustomButtonSave
                size="large"
                htmlType="submit"
                // isLoading={isLoading}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  setIsModalVisible(false);
                  form.resetFields();
                  setAllCondition([]);
                  setAnyCondition([]);
                  setColumns([]);
                  dispatch(loadDetailsCustomViewSuccess({}));
                  setCustomViewID("");
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </Wrapper>
      </Modal>
    </>
  );
};

export default ModalView;

const Wrapper = styled.div`
  max-height: 600px;
  overflow-y: scroll;
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;
  &:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  &:focus {
    color: #2c2c2c !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;
