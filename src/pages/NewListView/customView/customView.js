import { Button, Col, Drawer, Row, Typography } from "antd";
import deleteIcon from "assets/icons/common/delete-icon.png";
import editIcon from "assets/icons/common/icon-edit.png";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import ModalDelete from "components/Modal/ModalConfirmDelete";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deleteCustomView,
  loadCustomView,
  loadDetailsCustomView,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalView from "./modalView";

const { Text } = Typography;

const CustomView = (props) => {
  const { visible, setVisible, objectId, mode, setFilterID, reload } = props;

  // const { objectId } = useParams();
  const [customViewID, setCustomViewID] = useState("");
  const { customView, customViewDetail, isLoadingDelete } = useSelector(
    (state) => state.objectsReducer
  );
  let navigate = useNavigate();

  const onClose = () => {
    setVisible(false);
  };

  useEffect(() => {
    if (isLoadingDelete === false) {
      dispatch(setShowModalConfirmDelete(false));
      if (mode === "kanban") {
        // navigate(
        //   `/kanban-view?object_id=${objectId}&custom_view_id=${
        //     customViewID.length > 0 ? customViewID : "default_view"
        //   }`
        // );
        navigate(
          `/kanban-view/${objectId}/${
            customViewID.length > 0 ? customViewID : "default-view"
          }`
        );
      } else if (mode === "consolidated-view") {
      } else {
        navigate(`/objects/${objectId}/default-view`);
      }
    }
    /*eslint-disable-next-line*/
  }, [isLoadingDelete]);

  const dispatch = useDispatch();

  const [openView, setOpenView] = useState(false);

  useEffect(() => {
    if (visible)
      dispatch(
        loadCustomView({
          object_id: objectId,
        })
      );
    /*eslint-disable-next-line*/
  }, [objectId, visible]);

  return (
    <>
      <Drawer
        title="Tùy chọn view"
        placement="left"
        onClose={onClose}
        visible={visible}
        width={367}
      >
        <div style={{ display: "flex", justifyContent: "right" }}>
          <CustomButtonAddRecord
            size="large"
            onClick={() => {
              setOpenView(true);
            }}
          >
            <img alt="" src={plusIcon} />
            New
          </CustomButtonAddRecord>
        </div>
        <div>
          {customView &&
            customView.custom_views &&
            customView.custom_views.map((view, idx) => {
              return (
                <CustomRow gutter={16}>
                  <Col
                    span={18}
                    onClick={() => {
                      if (mode === "kanban") {
                        // navigate(
                        //   `/kanban-view?object_id=${objectId}&custom_view_id=${view._id}`
                        // );
                        navigate(`/kanban-view/${objectId}/${view._id}`);
                      } else if (mode === "consolidated-view") {
                        setFilterID(view._id);
                      } else {
                        navigate(`/objects/${objectId}/${view._id}`);
                      }
                      setVisible(false);
                    }}
                  >
                    <Title className="">
                      <Text
                        ellipsis={{
                          tooltip: view.view_name,
                        }}
                        style={{ width: "100%" }}
                      >
                        {view.view_name}
                      </Text>
                    </Title>
                  </Col>
                  <Col span={3}>
                    <img
                      alt=""
                      src={editIcon}
                      style={{ width: "20px" }}
                      onClick={() => {
                        setCustomViewID(view._id);
                        dispatch(
                          loadDetailsCustomView({
                            custom_view_id: view._id,
                            object_id: objectId,
                          })
                        );
                        setOpenView(true);
                      }}
                    />
                  </Col>
                  <Col span={3}>
                    <img
                      alt=""
                      src={deleteIcon}
                      style={{ width: "20px" }}
                      onClick={() => {
                        setCustomViewID(view._id);
                        dispatch(setShowModalConfirmDelete(true));
                      }}
                    />
                  </Col>
                </CustomRow>
              );
            })}
          <CustomRow gutter={16}>
            <Col
              span={24}
              onClick={() => {
                if (mode === "kanban") {
                  // navigate(
                  //   `/kanban-view?object_id=${objectId}&custom_view_id=default-view`
                  // );
                  navigate(`/kanban-view/${objectId}/default-view`);
                } else if (mode === "consolidated-view") {
                  setFilterID("default-view");
                } else {
                  navigate(`/objects/${objectId}/default-view`);
                }
                setVisible(false);
              }}
            >
              <Title className="">
                <Text
                  ellipsis={{
                    tooltip: "Default view",
                  }}
                  style={{ width: "100%" }}
                >
                  Default
                </Text>
              </Title>
            </Col>
          </CustomRow>
        </div>
        <ModalView
          isModalVisible={openView}
          setIsModalVisible={setOpenView}
          customViewDetail={customViewDetail}
          customViewID={customViewID}
          objectId={objectId}
          setCustomViewID={setCustomViewID}
          reload={reload}
        />
        {customViewID ? (
          <ModalDelete
            // openConfirm={openMassDelete}
            // setOpenConfirm={setOpenMassDelete}
            title={"những bản ghi này"}
            decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
            methodDelete={deleteCustomView}
            dataDelete={{
              data: {
                custom_view_id: customViewID,
                object_id: objectId,
              },
              load: {
                object_id: objectId,
              },
            }}
            setShowModalDelete={() => {}}
          />
        ) : (
          ""
        )}
      </Drawer>
    </>
  );
};

export default CustomView;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  &:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  &:focus {
    color: #2c2c2c !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
  margin-bottom: 16px;
`;

const Title = styled.span`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  display: flex;
  align-items: center;

  /* Character/Color text main */

  color: #2c2c2c;
`;

const CustomRow = styled(Row)`
  /* margin-bottom: 20px; */
  padding: 10px 10px 10px 10px;
  :hover {
    background-color: #f5f5f5;
    cursor: pointer;
  }
  display: flex;
  justify-content: center;
  align-items: center;
`;
