import update from "immutability-helper";
import { useCallback } from "react";
import { Card } from "./Card";

export const Container = (props) => {
  const { cards, setCards, form } = props;
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = cards[dragIndex];
      setCards(
        update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    /*eslint-disable-next-line*/
    [cards]
  );
  const renderCard = (card, index) => {
    return (
      <Card
        key={card.ID}
        index={index}
        id={card.ID}
        text={card.name}
        moveCard={moveCard}
        cards={cards}
        setCards={setCards}
        form={form}
      />
    );
  };
  return (
    <>
      <div style={{ marginBottom: "24px" }}>
        {cards.map((card, i) => renderCard(card, i))}
      </div>
    </>
  );
};
