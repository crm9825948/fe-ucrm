import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import Breadcrumb from "antd/lib/breadcrumb";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Checkbox from "antd/lib/checkbox";
import Tag from "antd/lib/tag";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";

import { getListTemplate } from "redux/slices/templateFile";
import { getListAccountMapping } from "redux/slices/googleIntegration";
import {
  updateDynamicButton,
  loadDataNecessaryEdit,
  unMountEditDynamicButton,
  setStatus,
  loadListActions,
} from "redux/slices/dynamicButton";
import Conditions from "components/Conditions/conditions";
import Actions from "pages/Workflows/Actions/actions";
import { Notification } from "components/Notification/Noti";

function EditDynamicButton(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { recordID } = useParams();
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { userFields } = useSelector((state) => state.workflowsReducer);

  const { detailsDynamicButton, listActions, status } = useSelector(
    (state) => state.dynamicButtonReducer
  );

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const [listGroups, setListGroups] = useState([]);
  const [listMainFields, setListMainFields] = useState([]);
  const [dynamicField, $dynamicField] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const [listObjects, setListObjects] = useState([]);
  const [isEdit, setIsEdit] = useState(false);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "dynamic_button" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSubmit = () => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    let flagForm = false;
    Object.values(form.getFieldsValue()).forEach((item, index) => {
      if (index !== 1 && index !== 4) {
        if (Array.isArray(item)) {
          if (item.length === 0) {
            flagForm = true;
          }
        } else {
          if (!item) {
            flagForm = true;
          }
        }
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else if (flagForm) {
      Notification("warning", "Please fullfill required!");
    } else {
      const dataForm = form.getFieldsValue();
      dispatch(
        updateDynamicButton({
          data: {
            object_id: dataForm.object,
            name: dataForm.name,
            description: dataForm.description,
            accessed_by: dataForm.access_by,
            dynamic_field: dynamicField,
            filter_condition: {
              and_filter: allCondition,
              or_filter: anyCondition,
            },
          },
          _id: detailsDynamicButton._id,
        })
      );
    }
  };

  const _onCancel = () => {
    form.resetFields();
    navigate("/dynamic-button");
  };

  const tagRender = (props) => {
    const { closable, onClose } = props;
    return (
      <Tag
        closable={closable}
        onClose={onClose}
        style={{
          marginRight: "4px",
          marginTop: "2px",
          marginBottom: "2px",
          height: "24px",
          background: "#f5f5f5",
          border: "1px solid #f0f0f0",
          borderRadius: "2px",
          fontSize: "14px",
          display: "flex",
          alignItems: "center",
        }}
      >
        {_.get(props, "label.props.children[0]", "")}
      </Tag>
    );
  };

  const onChangeDynamicField = (value) => {
    const newValues = value.map((item) => ({
      field_id: item,
      required: _.find(dynamicField, (field) => field.field_id === item)
        ? _.find(dynamicField, (field) => field.field_id === item).required
        : false,
    }));
    $dynamicField(newValues);
  };

  const onChangeRequired = (value) => {
    return (e) => {
      const index = dynamicField.findIndex((field) => field.field_id === value);
      if (index !== -1) {
        let newDynamicField = [...dynamicField];
        newDynamicField[index] = {
          ...newDynamicField[index],
          required: e.target.checked,
        };
        $dynamicField(newDynamicField);
      }
    };
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    if (Object.keys(detailsDynamicButton).length > 0) {
      form.setFieldsValue({
        name: detailsDynamicButton.name,
        description: detailsDynamicButton.description,
        object: detailsDynamicButton.object_id,
        access_by: detailsDynamicButton.accessed_by,
      });
      $dynamicField(_.get(detailsDynamicButton, "dynamic_field", []));
      setAllCondition(detailsDynamicButton.filter_condition.and_filter);
      setAnyCondition(detailsDynamicButton.filter_condition.or_filter);
    }
  }, [detailsDynamicButton, form]);

  useEffect(() => {
    dispatch(
      loadDataNecessaryEdit({
        button_id: recordID,
      })
    );
  }, [dispatch, recordID]);

  useEffect(() => {
    let tempGroup = [];

    listAllGroups.map((item) => {
      return tempGroup.push({
        label: item.name,
        value: item._id,
      });
    });
    setListGroups(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    return () => {
      dispatch(unMountEditDynamicButton());
    };
  }, [dispatch]);

  useEffect(() => {
    if (status === "Create action successfully!") {
      dispatch(
        loadListActions({
          button_id: recordID,
        })
      );
      dispatch(setStatus(null));
    }

    if (status === "Update action successfully!") {
      setIsEdit(false);
      dispatch(setStatus(null));
    }
  }, [dispatch, status, recordID]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  is_editor: _.get(field, "is_editor", false),
                });
              }
            });
          });
        }
      }
    });

    setListMainFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    dispatch(getListTemplate());
    dispatch(getListAccountMapping());
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <Breadcrumb.Item onClick={() => navigate("/dynamic-button")}>
          Dynamic button
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("dynamicButton.editDynamic")}</BreadcrumbItem>
      </Breadcrumb>

      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <WrapInfo>
          <WrapBasic>
            <BasicInfo>
              <legend>{t("workflow.basicInfo")}</legend>
              <Form.Item
                label={t("dynamicButton.buttonName")}
                name="name"
                rules={[
                  {
                    required: true,
                    message: t("common.placeholderInput"),
                  },
                ]}
              >
                <Input
                  disabled={!checkRule("edit")}
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>
              <Form.Item label={t("common.description")} name="description">
                <Input
                  disabled={!checkRule("edit")}
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>
              <Form.Item
                label={t("object.object")}
                name="object"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listObjects}
                  disabled
                />
              </Form.Item>
              <Form.Item
                label={t("dynamicButton.accessBy")}
                name="access_by"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listGroups}
                  mode="multiple"
                  optionFilterProp="label"
                  disabled={!checkRule("edit")}
                />
              </Form.Item>

              <Form.Item
                label={t("dynamicButton.updateFields")}
                name="dynamic_field"
                valuePropName="option"
              >
                <Select
                  onChange={onChangeDynamicField}
                  placeholder={t("common.placeholderSelect")}
                  options={listMainFields
                    ?.filter(
                      (item) =>
                        (item.type === "text" ||
                          item.type === "textarea" ||
                          item.type === "email" ||
                          item.type === "number" ||
                          item.type === "select" ||
                          item.type === "date" ||
                          item.type === "datetime-local") &&
                        item.value !== "created_date" &&
                        item.value !== "modify_time"
                    )
                    ?.map(
                      (item) =>
                        (item = {
                          label: (
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              {item.label}
                              <Checkbox
                                disabled={item.is_editor}
                                checked={dynamicField.find(
                                  (field) =>
                                    field.field_id === item.value &&
                                    field.required
                                )}
                                onChange={onChangeRequired(item.value)}
                              >
                                {t("form.isRequired")}
                              </Checkbox>
                            </div>
                          ),
                          value: item.value,
                        })
                    )}
                  tagRender={tagRender}
                  mode="multiple"
                  optionFilterProp="label"
                  value={dynamicField.map((item) => item.field_id)}
                />
              </Form.Item>
            </BasicInfo>
          </WrapBasic>

          <WrapCondition disabled={!checkRule("edit")}>
            <Wrap>
              <legend>{t("dynamicButton.dynamicConditions")}</legend>
              <Conditions
                title={t("common.allCondition")}
                decs={`(${t("common.descAllCondition")})`}
                conditions={allCondition}
                setConditions={setAllCondition}
                ID={
                  Object.keys(detailsDynamicButton).length > 0 &&
                  detailsDynamicButton._id
                    ? detailsDynamicButton._id
                    : ""
                }
                dataDetails={
                  Object.keys(detailsDynamicButton).length > 0 &&
                  detailsDynamicButton
                    ? detailsDynamicButton
                    : {}
                }
                operatorValue={operatorValueAnd}
                setOperatorValue={setOperatorValueAnd}
                value={valueAnd}
                setValue={setValueAnd}
              />
              <Conditions
                title={t("common.anyCondition")}
                decs={`(${t("common.descAnyCondition")})`}
                conditions={anyCondition}
                setConditions={setAnyCondition}
                ID={
                  Object.keys(detailsDynamicButton).length > 0 &&
                  detailsDynamicButton._id
                    ? detailsDynamicButton._id
                    : ""
                }
                dataDetails={
                  Object.keys(detailsDynamicButton).length > 0 &&
                  detailsDynamicButton
                    ? detailsDynamicButton
                    : {}
                }
                operatorValue={operatorValueOr}
                setOperatorValue={setOperatorValueOr}
                value={valueOr}
                setValue={setValueOr}
              />
            </Wrap>
          </WrapCondition>
        </WrapInfo>
      </Form>

      <Actions
        onSave={_onSubmit}
        onCancel={_onCancel}
        objectID={detailsDynamicButton.object_id}
        userFields={userFields}
        fields_related={fields_related}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        isDynamicButton={true}
        allActions={listActions}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        status={status}
        listMainFields={listMainFields}
        details={detailsDynamicButton}
      />
    </Wrapper>
  );
}

export default withTranslation()(EditDynamicButton);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-form-item-label > label {
    font-size: 16px;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapInfo = styled.div`
  background: #fff;
  margin-top: 16px;
  padding: 24px;
  display: flex;
`;

const WrapBasic = styled.div`
  width: 50%;
  margin-right: 16px;
  height: 285px;
  overflow-y: auto;
`;

const BasicInfo = styled.fieldset`
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 10px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const Wrap = styled(BasicInfo)`
  margin-bottom: 0;
`;

const WrapCondition = styled(WrapBasic)`
  margin-right: 0;
  pointer-events: ${({ disabled }) => disabled && "none"};
`;
