import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Switch from "antd/lib/switch";

import EmptyDynamicButton from "assets/images/dynamicButton/empty-dynamic-button.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadDataNecessary,
  setShowModalAdd,
  activeDynamicButton,
  deleteDynamicButton,
} from "redux/slices/dynamicButton";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalAdd from "pages/Workflows/ModalAddWorkflow";
import { changeTitlePage } from "redux/slices/authenticated";

function DynamicButton(props) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const { Column } = Table;
  const { Text } = Typography;

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.dynamicButton")));
    //eslint-disable-next-line
  }, [t]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { loading, listDynamicButton, showModalAdd } = useSelector(
    (state) => state.dynamicButtonReducer
  );

  const [dataTable, setDataTable] = useState([]);

  const [dataDelete, setDataDelete] = useState({});

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "dynamic_button" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActive = (checked, id) => {
    dispatch(
      activeDynamicButton({
        status: checked,
        _id: id,
      })
    );
  };

  const _onDelete = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    let tempList = [];
    listDynamicButton.map((item) => {
      return tempList.push({
        key: item._id,
        active: item.status,
        name: item.name,
        description: item.description,
        object: item.object_name,
        access_by: item.accessed_by.join(", "),
        create_date: item.created_date,
      });
    });

    setTimeout(() => {
      setDataTable(tempList);
    }, 50);
  }, [dispatch, listDynamicButton]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>Dynamic Button</BreadcrumbItem>
        </Breadcrumb>
        {listDynamicButton.length > 0 && checkRule("create") && (
          <AddButton onClick={() => dispatch(setShowModalAdd(true))}>
            + {t("dynamicButton.addDynamic")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listDynamicButton.length === 0 ? (
        <Empty>
          <img src={EmptyDynamicButton} alt="empty" />
          <p>
            {t("object.noObject")} <span>Dynamic button</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={() => dispatch(setShowModalAdd(true))}>
              + {t("dynamicButton.addDynamic")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapWorkflows>
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title={t("workflow.active")}
              dataIndex="active"
              key="active"
              width="120px"
              render={(text, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checkedChildren={t("workflow.on")}
                  unCheckedChildren={t("workflow.off")}
                  checked={text}
                  onChange={(checked) => handleActive(checked, record.key)}
                />
              )}
            />
            <Column
              title={t("dynamicButton.buttonName")}
              dataIndex="name"
              key="name"
              width="500px"
              sorter={(a, b) => a.name.localeCompare(b.name)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("common.description")}
              dataIndex="description"
              key="description"
              width="300px"
              sorter={(a, b) => a.description.localeCompare(b.description)}
              render={(text) => (
                <Text style={{ width: "300px" }} ellipsis={{ tooltip: text }}>
                  {text}
                </Text>
              )}
            />
            <Column
              title={t("object.object")}
              dataIndex="object"
              key="object"
              sorter={(a, b) => a.object.localeCompare(b.object)}
              width="200px"
            />
            <Column
              title={t("dynamicButton.accessBy")}
              dataIndex="access_by"
              key="access_by"
              width="396px"
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("common.createdDate")}
              dataIndex="create_date"
              key="create_date"
              width="200px"
            />

            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                fixed="right"
                width="150px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() =>
                            navigate(`/edit-dynamic-button/${record.key}`)
                          }
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDelete(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
        </WrapWorkflows>
      )}

      <ModalAdd
        showModalAdd={showModalAdd}
        objects={category}
        isDynamicButton
        listAllGroups={listAllGroups}
        setShowModalAdd={setShowModalAdd}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteDynamicButton}
        dataDelete={dataDelete}
        isLoading={loading.deleteDynamicButton}
      />
    </Wrapper>
  );
}

export default withTranslation()(DynamicButton);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
    border-radius: 50%;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapWorkflows = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
