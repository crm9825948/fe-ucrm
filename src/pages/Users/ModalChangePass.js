import React, { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";
import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import { RANDOM_VARIABLE } from "constants/constants";
import { changePass, resetStatusChangePass } from "redux/slices/user";
import { changePass as changePassword } from "redux/slices/tenants";

function ModalChangePass({
  showModalChangePass,
  onHideModalChangePass,
  getListUser,
  user,
  isAdmin,
  root,
  email,
  firstPass,
}) {
  const jwt = require("jsonwebtoken");
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isLoading, statusChangePass, userDetail } = useSelector(
    (state) => state.userReducer
  );

  const [form] = Form.useForm();

  const _onSubmit = (values) => {
    if (root) {
      dispatch(
        changePassword({
          firstPass: firstPass,
          email: email,
          pwd: values.password,
        })
      );
      onHideModalChangePass();
    } else {
      dispatch(
        changePass({
          user_root: _.get(userDetail, "user_root", false),
          firstPass: firstPass,
          isAdmin: isAdmin,
          data: {
            user_id: user._id,
            password: jwt.sign({ password: values.password }, RANDOM_VARIABLE, {
              algorithm: "HS256",
            }),
            confirm_password: jwt.sign(
              { password: values.verify_password },
              RANDOM_VARIABLE,
              {
                algorithm: "HS256",
              }
            ),
          },
        })
      );
    }
  };

  const _onCancel = () => {
    onHideModalChangePass();
    form.resetFields();
  };

  useEffect(() => {
    if (statusChangePass === "success") {
      form.resetFields();
      if (isAdmin) {
        getListUser();
      }
      onHideModalChangePass();

      dispatch(resetStatusChangePass());
    }

    if (statusChangePass !== "success" && statusChangePass !== null) {
      dispatch(resetStatusChangePass());
    }
  }, [
    dispatch,
    form,
    getListUser,
    isAdmin,
    onHideModalChangePass,
    statusChangePass,
  ]);

  return (
    <>
      <ModalCustom
        title={t("common.changePass")}
        visible={showModalChangePass}
        footer={null}
        width={600}
        onCancel={_onCancel}
      >
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("user.newPass")}
            name="password"
            rules={[
              { required: true, message: t("common.placeholderInput") },
              () => ({
                validator(_, value) {
                  if (value) {
                    if (value.length < 9) {
                      return Promise.reject(new Error(t("user.errorPass")));
                    } else if (!value.match(/(?=.*?[a-z])/)) {
                      return Promise.reject(new Error(t("user.errorPass2")));
                    } else if (!value.match(/(?=.*?[0-9])/)) {
                      return Promise.reject(new Error(t("user.errorPass5")));
                    } else if (
                      !value.match(/(?=.*?[,./=+<({})>[|!@#$%^&*?_-])/) &&
                      !value.includes("]") &&
                      !value.includes("\\")
                    ) {
                      return Promise.reject(new Error(t("user.errorPass4")));
                    } else if (!value.match(/(?=.*?[A-Z])/)) {
                      return Promise.reject(new Error(t("user.errorPass3")));
                    } else return Promise.resolve();
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password placeholder={t("common.placeholderInput")} />
          </Form.Item>

          <Form.Item
            label={t("user.verifyPass")}
            name="verify_password"
            dependencies={["password"]}
            rules={[
              { required: true, message: t("common.placeholderInput") },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error(t("user.errorPass6")));
                },
              }),
            ]}
          >
            <Input.Password placeholder={t("common.placeholderInput")} />
          </Form.Item>

          <WrapButton label=" ">
            <Button
              loading={isLoading}
              type="primary"
              htmlType="submit"
              className="btn-save"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      </ModalCustom>
    </>
  );
}

export default withTranslation()(ModalChangePass);

ModalChangePass.defaultProps = {
  isAdmin: true,
  root: false,
  email: "",
  firstPass: false,
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  .btn-save {
    background-color: ${(props) => props.theme.main};
  }

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: #1c1c1c !important;
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
