import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import { Notification } from "components/Notification/Noti";

import Tranfer from "assets/icons/roles/Transfer.jpg";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { deleteUser, resetStatusDelete } from "redux/slices/user";

function ModalDelete({
  showModalDelete,
  onHideModalDelete,
  getListUser,
  setShowModalDelete,
  user,
  setIsDelete,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoading, statusDelete, listAllUser } = useSelector(
    (state) => state.userReducer
  );

  const [form] = Form.useForm();

  const [listUser, setListUser] = useState([]);

  useEffect(() => {
    if (listAllUser !== null) {
      let tempList = [];
      listAllUser.forEach((item) => {
        if (item._id !== user._id && item.Active) {
          tempList.push({
            label:
              item.Middle_Name === ""
                ? item.Last_Name + " " + item.First_Name
                : item.Last_Name +
                  " " +
                  item.Middle_Name +
                  " " +
                  item.First_Name,
            value: item._id,
          });
        }
      });
      setListUser(tempList);
    }
  }, [listAllUser, user]);

  const [dataDelete, setDataDelete] = useState({});

  const _onSubmit = (values) => {
    dispatch(setShowModalConfirmDelete(true));
    onHideModalDelete();
    let tempData = {
      ID: user._id,
      id_user_receive_data: values.transfer,
    };
    setDataDelete(tempData);
  };

  const _onCancel = () => {
    onHideModalDelete();
    form.resetFields();
    setIsDelete(false);
  };

  useEffect(() => {
    if (statusDelete === "success") {
      Notification("success", "Delete successfully!");
      form.resetFields();
      getListUser();
      onHideModalDelete();
      dispatch(setShowModalConfirmDelete(false));
      setIsDelete(false);
      dispatch(resetStatusDelete());
      form.setFieldsValue({
        transfer: undefined,
      });
    }

    if (statusDelete !== "success" && statusDelete !== null) {
      Notification("error", statusDelete);
      dispatch(resetStatusDelete());
      setIsDelete(false);
      form.setFieldsValue({
        transfer: undefined,
      });
    }
  }, [
    dispatch,
    form,
    getListUser,
    onHideModalDelete,
    setIsDelete,
    statusDelete,
  ]);

  return (
    <>
      <ModalCustom
        title={t("user.deleteUser")}
        visible={showModalDelete}
        footer={null}
        width={450}
        onCancel={_onCancel}
      >
        <WrapNote>
          <img src={Tranfer} alt="transfer" />
          <Title>{t("user.transferData")}</Title>
          <SubTitle>{t("user.descriptionTransfer")}</SubTitle>
        </WrapNote>
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 12 }}
          wrapperCol={{ span: 12 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("user.transferDataTo")}
            name="transfer"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              options={listUser}
              placeholder={t("common.placeholderSelect")}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <WrapButton label=" ">
            <Button type="primary" htmlType="submit">
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      </ModalCustom>

      <ModalConfirmDelete
        setShowModalDelete={setShowModalDelete}
        title={`${
          user.Middle_Name === "" || user.Middle_Name === undefined
            ? user.Last_Name + " " + user.First_Name
            : user.Last_Name + " " + user.Middle_Name + " " + user.First_Name
        }`}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteUser}
        dataDelete={dataDelete}
        isLoading={isLoading}
      />
    </>
  );
}

export default withTranslation()(ModalDelete);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    font-family: var(--roboto-700);
    color: #2c2c2c;
  }
`;

const WrapNote = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  img {
    margin-bottom: 24px;
  }
`;

const Title = styled.p`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
`;

const SubTitle = styled.p`
  font-size: 15px;
  color: #2c2c2c;
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
