import { useCallback, useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Empty from "antd/lib/empty";
import Menu from "antd/lib/menu";
import Breadcrumb from "antd/lib/breadcrumb";
import Pagination from "antd/lib/pagination";

import Empty_folder from "assets/images/reports/empty-folder.webp";
// import Refresh from "assets/icons/exportHistory/Refresh.svg";
// import Delete from "assets/icons/common/delete.svg";
// import CSV from "assets/icons/exportHistory/CSV.svg";
// import PDF from "assets/icons/exportHistory/PDF.svg";
import XLSX from "assets/icons/exportHistory/XLSX.svg";

import { getLogsUser } from "redux/slices/user";

// import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
// import { setShowModalConfirmDelete } from "redux/slices/global";
// import { BE_URL } from "constants/constants";

function LogImportUsers(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { logsUser } = useSelector((state) => state.userReducer);

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  const [defaultActive, setDefaultActive] = useState("0");
  const [detailsExport, setDetailsExport] = useState({});
  // const [timePresent, setTimePresent] = useState("");

  // const [dataDelete, setDataDelete] = useState("");

  const _onSelectLog = (value) => {
    setDefaultActive(value.key);
  };

  const showTotal = () => {
    return `Total ${logsUser.total} items`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    if (logsUser.total > 0) {
      setDetailsExport(logsUser.results[parseInt(defaultActive)]);
    }
  }, [defaultActive, logsUser]);

  // const onRefreshTime = () => {
  //   const today = new Date();
  //   const time = today.getHours() + ":" + today.getMinutes();
  //   setTimePresent(time);
  // };

  // const _onRefresh = () => {
  //   onRefreshTime();
  //   dispatch(
  //     loadExportHistory({
  //       export_history_id: detailsExport._id,
  //     })
  //   );
  // };

  // const _onDelete = () => {
  //   setDataDelete({
  //     export_history_id: detailsExport._id,
  //   });
  //   dispatch(setShowModalConfirmDelete(true));
  // };

  const getLogs = useCallback(() => {
    dispatch(
      getLogsUser({
        limit: recordPerPage,
        search_data: {},
        page: currentPage,
      })
    );
  }, [currentPage, dispatch, recordPerPage]);

  // useEffect(() => {
  //   onRefreshTime();
  // }, []);

  useEffect(() => {
    getLogs();
  }, [getLogs]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/users")}>
          Users
        </Breadcrumb.Item>
        <BreadcrumbItem>Logs import</BreadcrumbItem>
      </Breadcrumb>

      {logsUser.total === 0 ? (
        <Empty
          image={Empty_folder}
          description={
            <p>
              Hiện chưa có <span>Logs import</span>
            </p>
          }
        ></Empty>
      ) : (
        <WrapLogs>
          <Wrap>
            <MenuCustom onClick={_onSelectLog} selectedKeys={defaultActive}>
              {logsUser.results.map((item, idx) => {
                return (
                  <Menu.Item key={idx}>
                    <span>{item.file_name}</span>
                  </Menu.Item>
                );
              })}
            </MenuCustom>
            {Object.keys(detailsExport).length > 0 && (
              <DetailsExport>
                <WrapTitle>
                  {/* {detailsExport.file_type === "xlsx" ? (
                  <img src={XLSX} alt="xlsx" />
                ) : detailsExport.file_type === "csv" ? (
                  <img src={CSV} alt="csv" />
                ) : (
                  <img src={PDF} alt="pdf" />
                )} */}
                  <img src={XLSX} alt="xlsx" />
                  <Title>
                    <span>{detailsExport.file_name}</span>
                    <SubTitle>
                      <span>Start time: {detailsExport.created_date}</span>
                      {/* <span>Refreshed at {timePresent}</span> */}
                      {/* {detailsExport.is_running && (
                      <>
                        {loading ? (
                          <Spin />
                        ) : (
                          <img
                            onClick={_onRefresh}
                            src={Refresh}
                            alt="refresh"
                          />
                        )}
                      </>
                    )} */}
                      <p>•</p>

                      {/* {detailsExport.is_running ? (
                      <p>
                        Exporting {detailsExport.filenames.length}/
                        {detailsExport.total_files}
                      </p>
                    ) : (
                      <p>Completed</p>
                    )} */}
                      <p style={{ textTransform: "capitalize" }}>
                        {detailsExport.status}
                      </p>
                    </SubTitle>
                  </Title>
                  {/* <DeleteHistory>
                  <img onClick={_onDelete} src={Delete} alt="delete" />
                </DeleteHistory> */}
                </WrapTitle>
                <ListFile>
                  <span>Total success: {detailsExport.total_success}</span>
                  <span>Total failure: {detailsExport.total_failure}</span>
                </ListFile>
                {detailsExport.log_failure.length > 0 && (
                  <ListFile>
                    {detailsExport.log_failure.map((log) => {
                      return (
                        <span>{log}</span>
                        // <a href={`${BE_URL}${file}`}>
                        //   <Tooltip title="Download now">
                        //     {detailsExport.name}.{detailsExport.file_type}_
                        //     {idx + 1}
                        //   </Tooltip>
                        // </a>
                      );
                    })}
                  </ListFile>
                )}
              </DetailsExport>
            )}
          </Wrap>

          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={logsUser.total}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </WrapLogs>
      )}

      {/* <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteExportHistory}
        dataDelete={dataDelete}
        isLoading={false}
      /> */}
    </Wrapper>
  );
}

export default LogImportUsers;

const Wrapper = styled.div`
  padding: 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }

  .ant-spin {
    margin: 0 16px;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  img {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapLogs = styled.div`
  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const Wrap = styled.div`
  display: flex;
  margin-top: 8px;
  background: #fff;
`;

const MenuCustom = styled(Menu)`
  width: 42.5rem;
  box-shadow: inset 0px -1px 0px #f0f0f0;
  border-radius: 0px 0px 0px 10px;
  height: fit-content;
  max-height: calc(100vh - 280px);
  overflow: auto;

  .ant-menu-item {
    height: 46px !important;
    line-height: 46px !important;
    border-bottom: 1px solid #f0f0f0;
    margin: 0 !important;
    transition: border-color 0.5s ease;
  }

  .ant-menu-item-selected {
    background: #f0f0f0 !important;
    color: #2c2c2c;
  }

  .ant-menu-item {
    :hover {
      color: #2c2c2c;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
      border: 1px solid #d9d9d9;
    }
  }

  .ant-menu-title-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

const DetailsExport = styled.div`
  padding: 24px;
  background: #f5f5f5;
  border: 1px solid #ececec;
  flex: 1;
  max-height: calc(100vh - 280px);
  overflow: auto;
`;

const WrapTitle = styled.div`
  border-bottom: 1px solid #ececec;
  padding-bottom: 16px;
  display: flex;
`;

const Title = styled.div`
  padding-left: 24px;

  span {
    font-family: var(--roboto-500);
    font-size: 22px;
    color: #2c2c2c;
  }
`;

const SubTitle = styled.div`
  display: flex;
  align-items: center;

  img {
    margin-left: 16px;
  }

  span {
    font-family: var(--roboto-400);
    font-size: 16px;
  }

  p {
    font-size: 14px;
    color: #6b6b6b;
    margin-bottom: 0;
    margin-left: 16px;
  }
`;

const ListFile = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 24px;
  padding-left: 66px;

  a {
    color: #1267fb;

    :hover {
      text-decoration-line: underline;
    }
  }
`;

// const DeleteHistory = styled.div`
//   margin-left: auto;
// `;
