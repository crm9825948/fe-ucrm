// import { SearchOutlined, DownOutlined } from "@ant-design/icons";
// import { Button, Input, Table } from "antd";
// import Breadcrumb from "antd/lib/breadcrumb";
// import Pagination from "antd/lib/pagination";
// import Tooltip from "antd/lib/tooltip";
// import Typography from "antd/lib/typography";
// import Switch from "antd/lib/switch";
// import Dropdown from "antd/lib/dropdown";
// import Menu from "antd/lib/menu";
// import React, { useCallback, useEffect, useState } from "react";
// import { useTranslation, withTranslation } from "react-i18next";
// import { useDispatch, useSelector } from "react-redux";
// import { useNavigate } from "react-router-dom";
// import styled from "styled-components/macro";
// // import SearchOutlined from "@ant-design/icons/SearchOutlined";
// // import Input from "antd/lib/input";
// import _ from "lodash";

// import Edit from "assets/icons/common/edit.svg";
// import ChangePass from "assets/icons/users/change-pass.svg";
// import Delete from "assets/icons/users/delete-user.svg";
// import Permission from "assets/icons/users/Permission.svg";
// import ImgConfirm from "assets/icons/common/confirm.png";

// import {
//   loadAllRole,
//   loadAllUser,
//   loadUser,
//   loadUserReport,
//   activeUser,
// } from "redux/slices/user";
// import { setShowModalConfirm } from "redux/slices/global";
// import { generateSampleData, exportUsers } from "redux/slices/user";

// import ModalChangePass from "./ModalChangePass";
// import ModalDelete from "./ModalDelete";
// import ModalUser from "./ModalUser";
// import ModalConfirm from "components/Modal/ModalConfirm";
// import ModalImportUsers from "./ModalImportUsers";
// import CustomTable from "components/CustomTable/CustomTable";

// function Users(props) {
//   const { t } = useTranslation();
//   const dispatch = useDispatch();
//   const navigate = useNavigate();
//   const { Column } = Table;
//   const { Text } = Typography;

//   const {
//     userDetail,
//     listUser,
//     listRole,
//     isLoadingUser,
//     totalRecords,
//     userRuleGlobal,
//   } = useSelector((state) => state.userReducer);

//   const [showModalImport, setShowModalImport] = useState(false);
//   const [showModalUser, setShowModalUser] = useState(false);
//   const [showModalDelete, setShowModalDelete] = useState(false);
//   const [showModalChangePass, setShowModalChangePass] = useState(false);

//   const [isEdit, setIsEdit] = useState(false);
//   const [isDelete, setIsDelete] = useState(false);
//   const [user, setUser] = useState({});
//   const [tableUser, setTableUser] = useState([]);
//   const [dataConfirm, setDataConfirm] = useState({});

//   const [currentPage, setCurrentPage] = useState(1);
//   const [recordPerPage, setRecordPerPage] = useState(10);

//   const [sortBy, setSortBy] = useState({});

//   const listHeader = [
//     {
//       id: "active",
//       name: "Active",
//       type: "text",
//       option: null,
//       isSearch: false,
//       isSort: false,
//     },
//     {
//       id: "Full_Name",
//       name: "Name",
//       type: "text",
//       option: null,
//       isSearch: true,
//       isSort: true,
//     },
//     {
//       id: "Email",
//       name: "Email",
//       type: "text",
//       option: null,
//       isSearch: true,
//       isSort: true,
//     },
//     {
//       id: "Report_To",
//       name: "Report to",
//       type: "text",
//       option: null,
//       isSearch: true,
//       isSort: true,
//     },
//     {
//       id: "User_Role",
//       name: "User role",
//       type: "text",
//       option: null,
//       isSearch: true,
//       isSort: true,
//     },
//     {
//       id: "id",
//       name: "User ID",
//       type: "text",
//       option: null,
//       isSearch: false,
//       isSort: true,
//     },
//     {
//       id: "is_admin",
//       name: "Is admin",
//       type: "text",
//       option: null,
//       isSearch: false,
//       isSort: false,
//     },
//     {
//       id: "action",
//       name: "Action",
//       type: "text",
//       option: null,
//       isSearch: false,
//       isSort: false,
//     },
//   ];

//   const [dataSource, setDataSource] = useState([]);

//   useEffect(() => {
//     let temp = [];
//     tableUser.map((record) => {
//       return temp.push({
//         active: (
//           <>
//             {listRole[0]?._id !== record.user_role_id && (
//               <Switch
//                 disabled={!checkRule("edit")}
//                 checkedChildren={t("workflow.on")}
//                 unCheckedChildren={t("workflow.off")}
//                 checked={record.active}
//                 onChange={(checked) => handleActive(checked, record.key)}
//               />
//             )}
//           </>
//         ),
//         Full_Name: (
//           <Text ellipsis={{ tooltip: record.Full_Name }}>
//             {record.Full_Name}
//           </Text>
//         ),
//         Email: <Text ellipsis={{ tooltip: record.Email }}>{record.Email}</Text>,
//         Report_To: (
//           <Text ellipsis={{ tooltip: record.Report_To }}>
//             {record.Report_To}
//           </Text>
//         ),
//         User_Role: (
//           <Text ellipsis={{ tooltip: record.User_Role }}>
//             {record.User_Role}
//           </Text>
//         ),
//         id: <Text ellipsis={{ tooltip: record.id }}>{record.id}</Text>,

//         is_admin: (
//           <>
//             {record.is_admin !== "" && (
//               <IsAdmin>
//                 <span>{record.is_admin}</span>
//               </IsAdmin>
//             )}
//           </>
//         ),
//         action: (
//           <>
//             {checkRule("edit") && (
//               <WrapAction>
//                 <Tooltip title={t("common.edit")}>
//                   <img
//                     style={{ width: "18px" }}
//                     onClick={() => _onEditUser(record.item)}
//                     src={Edit}
//                     alt="edit"
//                   />
//                 </Tooltip>
//                 {!record.active &&
//                   listRole[0]?._id !== record.user_role_id &&
//                   checkRule("delete") && (
//                     <Tooltip title={t("common.delete")}>
//                       <img
//                         onClick={() => _onDeleteUser(record.item)}
//                         src={Delete}
//                         alt="delete"
//                       />
//                     </Tooltip>
//                   )}
//                 <Tooltip title={t("common.changePass")}>
//                   <img
//                     onClick={() => _onChangePass(record.item)}
//                     src={ChangePass}
//                     alt="changePass"
//                   />
//                 </Tooltip>
//                 {!record.is_admin && userDetail._id !== record.id && (
//                   <Tooltip title="Permission">
//                     <img
//                       onClick={() => {
//                         localStorage.setItem("nameUserPermission", record.name);
//                         navigate(`/user-permission/${record.id}`);
//                       }}
//                       src={Permission}
//                       alt="permission"
//                     />
//                   </Tooltip>
//                 )}
//               </WrapAction>
//             )}
//             {!checkRule("edit") && checkRule("delete") && (
//               <WrapAction>
//                 {!record.active && listRole[0]?._id !== record.user_role_id && (
//                   <Tooltip title={t("common.delete")}>
//                     <img
//                       onClick={() => _onDeleteUser(record.item)}
//                       src={Delete}
//                       alt="delete"
//                     />
//                   </Tooltip>
//                 )}
//               </WrapAction>
//             )}
//           </>
//         ),
//       });
//     });

//     setDataSource(temp);
//     // eslint-disable-next-line
//   }, [tableUser, listRole]);

//   const [valueSearch, setValueSearch] = useState({
//     Created_By: "",
//     Created_Date: "",
//     Email: "",
//     First_Name: "",
//     Is_Admin: "",
//     Last_Name: "",
//     Middle_Name: "",
//     Modify_By: "",
//     Modify_Time: "",
//     Report_To: "",
//     User_Role: "",
//     Full_Name: "",
//   });

//   const _onShowModalAdd = () => {
//     setShowModalUser(true);
//   };

//   const _onHideModalUser = () => {
//     setShowModalUser(false);
//     setUser({});
//   };

//   const _onEditUser = (data) => {
//     setIsEdit(true);
//     setUser(data);

//     dispatch(
//       loadUserReport({
//         role_id: data.User_Role,
//       })
//     );
//   };

//   // const getListUser = useCallback(() => {
//   //   dispatch(
//   //     loadUser({
//   //       current_page: currentPage,
//   //       record_per_page: recordPerPage,
//   //     })
//   //   );
//   // }, [currentPage, dispatch, recordPerPage]);

//   useEffect(() => {
//     dispatch(
//       loadUser({
//         current_page: 1,
//         record_per_page: 10,
//       })
//     );
//   }, [dispatch]);

//   const getListUser = (page, size) => {
//     dispatch(
//       loadUser({
//         current_page: page,
//         record_per_page: size,
//         search_data: { ...valueSearch },
//       })
//     );
//   };

//   const handleSearch = (page, size) => {
//     dispatch(
//       loadUser({
//         current_page: page,
//         record_per_page: size,
//         search_data: { ...valueSearch },
//       })
//     );
//   };

//   const handleReset = () => {
//     dispatch(
//       loadUser({
//         current_page: currentPage,
//         record_per_page: recordPerPage,
//         search_data: {
//           Created_By: "",
//           Created_Date: "",
//           Email: "",
//           First_Name: "",
//           Is_Admin: "",
//           Last_Name: "",
//           Middle_Name: "",
//           Modify_By: "",
//           Modify_Time: "",
//           Report_To: "",
//           User_Role: "",
//           Full_Name: "",
//         },
//       })
//     );
//   };

//   const _onHideModalDelete = () => {
//     setShowModalDelete(false);
//   };

//   const _onDeleteUser = (data) => {
//     setIsDelete(true);
//     setUser(data);

//     dispatch(
//       loadAllUser({
//         current_page: 1,
//         record_per_page: 10000,
//       })
//     );
//   };

//   const _onChangePass = (data) => {
//     setShowModalChangePass(true);
//     setUser(data);
//   };

//   const _onHideModalChangePass = () => {
//     setShowModalChangePass(false);
//   };

//   const showTotal = () => {
//     return `${t("common.total")} ${totalRecords} ${t("common.items")}`;
//   };

//   const handleSelectPage = (e) => {
//     setCurrentPage(e);
//   };

//   const _onSizeChange = (current, pageSize) => {
//     setRecordPerPage(pageSize);
//   };

//   const handleActive = (checked, id) => {
//     setDataConfirm({
//       status: checked,
//       id: id,
//     });
//     dispatch(setShowModalConfirm(true));
//   };

//   const checkRule = (rule) => {
//     if (
//       _.get(userDetail, "Is_Admin") ||
//       userRuleGlobal.find(
//         (item) =>
//           _.get(item, "domain", "") === "user" && item.actions.includes(rule)
//       )
//     )
//       return true;
//     else return false;
//   };

//   const menuExcel = (
//     <Menu mode="inline">
//       {checkRule("create") && (
//         <Menu.Item key="0" onClick={() => setShowModalImport(true)}>
//           Import users
//         </Menu.Item>
//       )}
//       {checkRule("create") && (
//         <Menu.Item key="1" onClick={() => dispatch(generateSampleData())}>
//           Generate sample data
//         </Menu.Item>
//       )}
//       <Menu.Item key="3" onClick={() => navigate("/logs-user")}>
//         View logs
//       </Menu.Item>
//       {checkRule("edit") && (
//         <Menu.Item key="4" onClick={() => dispatch(exportUsers())}>
//           Export users
//         </Menu.Item>
//       )}
//     </Menu>
//   );

//   useEffect(() => {
//     if (isDelete) {
//       setShowModalDelete(true);
//     }
//   }, [isDelete]);

//   useEffect(() => {
//     if (isLoadingUser === false && isEdit) {
//       setShowModalUser(true);
//     }
//   }, [isEdit, isLoadingUser]);

//   // useEffect(() => {
//   //   getListUser(1, 10);
//   //   dispatch(loadAllRole());
//   // }, [dispatch, getListUser]);

//   useEffect(() => {
//     let tempList = [];

//     listUser.map((item) => {
//       return tempList.push({
//         key: item._id,
//         action: "",
//         active: item?.Active,
//         Full_Name:
//           item.Middle_Name && item.Middle_Name !== ""
//             ? item.Last_Name + " " + item.Middle_Name + " " + item.First_Name
//             : item.Last_Name + " " + item.First_Name,
//         Email: item.Email,
//         Report_To: item.Report_To_Name,
//         User_Role: item.User_Role_Name,
//         is_admin: item.Is_Admin ? "Admin" : "",
//         item: item,
//         user_role_id: item.User_Role,
//         id: item._id,
//       });
//     });
//     setTimeout(() => {
//       setTableUser(tempList);
//     });
//   }, [listUser]);

//   const handleSort = (sortTmp, id) => {
//     const temp = [...tableUser];

//     if (sortTmp[id] === 1) {
//       temp.sort((a, b) => (a[id] > b[id] ? 1 : b[id] > a[id] ? -1 : 0));
//     } else if (sortTmp[id] === -1) {
//       temp.sort((a, b) => (a[id] > b[id] ? -1 : b[id] > a[id] ? 1 : 0));
//     } else {
//       dispatch(
//         loadUser({
//           current_page: currentPage,
//           record_per_page: recordPerPage,
//           search_data: { ...valueSearch },
//         })
//       );
//     }
//     setTableUser(temp);
//   };

//   return (
//     <Wrapper>
//       <WrapBreadcrumb>
//         <Breadcrumb>
//           <Breadcrumb.Item onClick={() => navigate("/settings")}>
//             {t("settings.settings")}
//           </Breadcrumb.Item>
//           <BreadcrumbItem>{t("settings.users")}</BreadcrumbItem>
//         </Breadcrumb>
//         <div>
//           <Dropdown overlay={menuExcel} trigger="click">
//             <ButtonExcel size="large">
//               Thao tác với Excel <DownOutlined />
//             </ButtonExcel>
//           </Dropdown>
//           {checkRule("create") && (
//             <AddButton onClick={_onShowModalAdd}>
//               + {t("user.addUser")}
//             </AddButton>
//           )}
//         </div>
//       </WrapBreadcrumb>

//       {/* <WrapTable>
//         <Table
//           pagination={false}
//           dataSource={tableUser}
//           scroll={{ x: "max-content" }}
//         >
//           <Column
//             title={t("workflow.active")}
//             dataIndex="active"
//             key="active"
//             width="120px"
//             render={(text, record) => (
//               <>
//                 {listRole[0]?._id !== record.user_role_id && (
//                   <Switch
//                     disabled={!checkRule("edit")}
//                     checkedChildren={t("workflow.on")}
//                     unCheckedChildren={t("workflow.off")}
//                     checked={text}
//                     onChange={(checked) => handleActive(checked, record.key)}
//                   />
//                 )}
//               </>
//             )}
//           />
//           <Column
//             filterIcon={() => <SearchOutlined />}
//             filterDropdown={() => {
//               return (
//                 <WrapSearch>
//                   <Input
//                     placeholder="Search by name"
//                     onChange={(e) => {
//                       let tmp = { ...valueSearch };
//                       tmp.First_Name = e.target.value;
//                       setValueSearch(tmp);
//                     }}
//                     value={valueSearch.First_Name}
//                     onPressEnter={() => {
//                       handleSearch();
//                     }}
//                   />
//                   <WrapButtonSearch>
//                     <ButtonSearch
//                       type="primary"
//                       icon={<SearchOutlined />}
//                       size="small"
//                       onClick={() => {
//                         handleSearch();
//                       }}
//                     >
//                       Search
//                     </ButtonSearch>
//                     <ButtonReset
//                       size="small"
//                       onClick={(e) => {
//                         let tmp = { ...valueSearch };
//                         tmp.First_Name = "";
//                         setValueSearch(tmp);
//                         handleReset();
//                       }}
//                     >
//                       Reset
//                     </ButtonReset>
//                   </WrapButtonSearch>
//                 </WrapSearch>
//               );
//             }}
//             width="380px"
//             title={t("user.name")}
//             dataIndex="Full_Name"
//             key="Full_Name"
//             sorter={(a, b) => a.Full_Name.localeCompare(b.Full_Name)}
//             render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
//           />
//           <Column
//             filterIcon={() => <SearchOutlined />}
//             filterDropdown={() => {
//               return (
//                 <WrapSearch>
//                   <Input
//                     placeholder="Search by Email"
//                     onChange={(e) => {
//                       let tmp = { ...valueSearch };
//                       tmp.Email = e.target.value;
//                       setValueSearch(tmp);
//                     }}
//                     onPressEnter={() => {
//                       handleSearch();
//                     }}
//                     value={valueSearch.Email}
//                   />
//                   <WrapButtonSearch>
//                     <ButtonSearch
//                       type="primary"
//                       icon={<SearchOutlined />}
//                       size="small"
//                       onClick={() => {
//                         handleSearch();
//                       }}
//                     >
//                       Search
//                     </ButtonSearch>
//                     <ButtonReset
//                       size="small"
//                       onClick={(e) => {
//                         let tmp = { ...valueSearch };
//                         tmp.Email = "";
//                         setValueSearch(tmp);
//                         handleReset();
//                       }}
//                     >
//                       Reset
//                     </ButtonReset>
//                   </WrapButtonSearch>
//                 </WrapSearch>
//               );
//             }}
//             width="400px"
//             title="Email"
//             dataIndex="Email"
//             key="Email"
//             sorter={(a, b) => a.Email.localeCompare(b.Email)}
//           />
//           <Column
//             filterIcon={() => <SearchOutlined />}
//             filterDropdown={() => {
//               return (
//                 <WrapSearch>
//                   <Input
//                     placeholder="Search by Report To"
//                     onChange={(e) => {
//                       let tmp = { ...valueSearch };
//                       tmp.Report_To = e.target.value;
//                       setValueSearch(tmp);
//                     }}
//                     onPressEnter={() => {
//                       handleSearch();
//                     }}
//                     value={valueSearch.Report_To}
//                   />
//                   <WrapButtonSearch>
//                     <ButtonSearch
//                       type="primary"
//                       icon={<SearchOutlined />}
//                       size="small"
//                       onClick={() => {
//                         handleSearch();
//                       }}
//                     >
//                       Search
//                     </ButtonSearch>
//                     <ButtonReset
//                       size="small"
//                       onClick={(e) => {
//                         let tmp = { ...valueSearch };
//                         tmp.Report_To = "";
//                         setValueSearch(tmp);
//                         handleReset();
//                       }}
//                     >
//                       Reset
//                     </ButtonReset>
//                   </WrapButtonSearch>
//                 </WrapSearch>
//               );
//             }}
//             width="270px"
//             title={t("user.reportTo")}
//             dataIndex="Report_To"
//             key="Report_To"
//             sorter={(a, b) => a.Report_To.localeCompare(b.Report_To)}
//           />
//           <Column
//             filterIcon={() => <SearchOutlined />}
//             filterDropdown={() => {
//               return (
//                 <WrapSearch>
//                   <Input
//                     placeholder="Search by User Role"
//                     onChange={(e) => {
//                       let tmp = { ...valueSearch };
//                       tmp.User_Role = e.target.value;
//                       setValueSearch(tmp);
//                     }}
//                     onPressEnter={() => {
//                       handleSearch();
//                     }}
//                     value={valueSearch.User_Role}
//                   />
//                   <WrapButtonSearch>
//                     <ButtonSearch
//                       type="primary"
//                       icon={<SearchOutlined />}
//                       size="small"
//                       onClick={() => {
//                         handleSearch();
//                       }}
//                     >
//                       Search
//                     </ButtonSearch>
//                     <ButtonReset
//                       size="small"
//                       onClick={(e) => {
//                         let tmp = { ...valueSearch };
//                         tmp.User_Role = "";
//                         setValueSearch(tmp);
//                         handleReset();
//                       }}
//                     >
//                       Reset
//                     </ButtonReset>
//                   </WrapButtonSearch>
//                 </WrapSearch>
//               );
//             }}
//             width="260px"
//             title={t("user.userRole")}
//             dataIndex="User_Role"
//             key="User_Role"
//             sorter={(a, b) => a.User_Role.localeCompare(b.User_Role)}
//             render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
//           />
//           <Column
//             width="260px"
//             title="User ID"
//             dataIndex="id"
//             key="id"
//             sorter={(a, b) => a.id.localeCompare(b.id)}
//             render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
//           />
//           <Column
//             // filterIcon={() => <SearchOutlined />}
//             // filterDropdown={() => {
//             //   return (
//             //     <WrapSearch>
//             //       <Input placeholder="Search by admin" />
//             //       <WrapButtonSearch>
//             //         <ButtonSearch type="primary" icon={<SearchOutlined />}>
//             //           Search
//             //         </ButtonSearch>
//             //         <ButtonReset>Reset</ButtonReset>
//             //       </WrapButtonSearch>
//             //     </WrapSearch>
//             //   );
//             // }}
//             width="150px"
//             title={t("user.isAdmin")}
//             dataIndex="is_admin"
//             key="is_admin"
//             render={(text) => (
//               <>
//                 {text !== "" && (
//                   <IsAdmin>
//                     <span>{text}</span>
//                   </IsAdmin>
//                 )}
//               </>
//             )}
//           />
//           {checkRule("edit") && (
//             <Column
//               title={t("common.action")}
//               dataIndex="action"
//               key="action"
//               fixed="right"
//               width="150px"
//               render={(text, record) => (
//                 <WrapAction>
//                   <Tooltip title={t("common.edit")}>
//                     <img
//                       style={{ width: "18px" }}
//                       onClick={() => _onEditUser(record.item)}
//                       src={Edit}
//                       alt="edit"
//                     />
//                   </Tooltip>
//                   {!record.active &&
//                     listRole[0]?._id !== record.user_role_id &&
//                     checkRule("delete") && (
//                       <Tooltip title={t("common.delete")}>
//                         <img
//                           onClick={() => _onDeleteUser(record.item)}
//                           src={Delete}
//                           alt="delete"
//                         />
//                       </Tooltip>
//                     )}
//                   <Tooltip title={t("common.changePass")}>
//                     <img
//                       onClick={() => _onChangePass(record.item)}
//                       src={ChangePass}
//                       alt="changePass"
//                     />
//                   </Tooltip>
//                   {!record.is_admin && userDetail._id !== record.id && (
//                     <Tooltip title="Permission">
//                       <img
//                         onClick={() => {
//                           localStorage.setItem(
//                             "nameUserPermission",
//                             record.name
//                           );
//                           navigate(`/user-permission/${record.id}`);
//                         }}
//                         src={Permission}
//                         alt="permission"
//                       />
//                     </Tooltip>
//                   )}
//                 </WrapAction>
//               )}
//             />
//           )}
//           {!checkRule("edit") && checkRule("delete") && (
//             <Column
//               title={t("common.action")}
//               dataIndex="action"
//               key="action"
//               fixed="right"
//               width="150px"
//               render={(text, record) => (
//                 <WrapAction>
//                   {!record.active && listRole[0]?._id !== record.user_role_id && (
//                     <Tooltip title={t("common.delete")}>
//                       <img
//                         onClick={() => _onDeleteUser(record.item)}
//                         src={Delete}
//                         alt="delete"
//                       />
//                     </Tooltip>
//                   )}
//                 </WrapAction>
//               )}
//             />
//           )}
//         </Table>
//         <Pagination
//           showTotal={showTotal}
//           current={currentPage}
//           pageSize={recordPerPage}
//           total={totalRecords}
//           onChange={handleSelectPage}
//           showSizeChanger
//           onShowSizeChange={_onSizeChange}
//           showQuickJumper
//         />
//       </WrapTable> */}

//       <CustomTable
//         listColumns={listHeader}
//         dataSource={dataSource}
//         totalRecords={totalRecords}
//         sortBy={sortBy}
//         setSortBy={setSortBy}
//         handleSort={handleSort}
//         handleSearch={handleSearch}
//         valueSearch={valueSearch}
//         setValueSearch={setValueSearch}
//         getListUser={getListUser}
//       />

//       <ModalUser
//         showModalUser={showModalUser}
//         onHideModalUser={_onHideModalUser}
//         isEdit={isEdit}
//         setIsEdit={setIsEdit}
//         getListUser={getListUser}
//         listRole={listRole}
//         user={user}
//       />

//       <ModalDelete
//         showModalDelete={showModalDelete}
//         setShowModalDelete={setShowModalDelete}
//         onHideModalDelete={_onHideModalDelete}
//         getListUser={getListUser}
//         user={user}
//         setIsDelete={setIsDelete}
//       />

//       <ModalChangePass
//         showModalChangePass={showModalChangePass}
//         onHideModalChangePass={_onHideModalChangePass}
//         getListUser={getListUser}
//         user={user}
//       />

//       <ModalConfirm
//         title="Change Status User Confirmation"
//         decs="Are you sure you want to proceed?"
//         method={activeUser}
//         data={dataConfirm}
//         img={ImgConfirm}
//       />

//       <ModalImportUsers
//         showModalImport={showModalImport}
//         setShowModalImport={setShowModalImport}
//       />
//     </Wrapper>
//   );
// }

// export default withTranslation()(Users);

// const Wrapper = styled.div`
//   padding: 16px 24px;

//   .ant-breadcrumb-link {
//     cursor: pointer;
//   }

//   .ant-switch-checked {
//     background-color: ${(props) => props.theme.main};
//   }
// `;

// const WrapBreadcrumb = styled.div`
//   display: flex;
//   justify-content: space-between;
//   align-items: center;
// `;

// const BreadcrumbItem = styled(Breadcrumb.Item)`
//   font-family: var(--roboto-500);
//   font-size: 18px !important;
//   color: #2c2c2c;
//   cursor: default;
// `;

// const AddButton = styled(Button)`
//   background: ${(props) => props.theme.main};
//   border: 1px solid ${(props) => props.theme.main};
//   height: 40px;

//   span {
//     color: #fff;
//     font-size: 16px;
//   }

//   :hover {
//     background: ${(props) => props.theme.darker}!important;
//     color: #fff !important;
//   }

//   :active {
//     background: ${(props) => props.theme.main};
//     color: #fff;
//   }

//   :focus {
//     background: ${(props) => props.theme.main};
//     color: #fff;
//   }
// `;

// const WrapTable = styled.div`
//   background: #fff;
//   padding: 24px;
//   margin-top: 16px;

//   .ant-table-thead > tr > th {
//     font-size: 16px;
//     color: #2c2c2c;
//     background: #f0f0f0;
//     border-right: 1px solid #d9d9d9;
//     border-bottom: 1px solid #d9d9d9;
//     padding: 8.5px 16px;

//     :before {
//       display: none;
//     }

//     :first-child {
//       border-left: 1px solid #d9d9d9;
//     }
//   }

//   .ant-table-content {
//     ::-webkit-scrollbar {
//       height: 8px;
//     }
//   }

//   .ant-table-tbody > tr > td {
//     font-size: 16px;
//     color: #2c2c2c;
//     padding: 8.5px 16px;

//     :last-child {
//       border-right: 1px solid #d9d9d9;
//     }
//   }

//   .ant-pagination {
//     width: 100%;
//     background: #fff;
//     padding: 16px 24px 0 16px;
//     display: flex;
//     justify-content: flex-end;
//   }

//   .ant-pagination-item-active {
//     border-color: ${(props) => props.theme.main};

//     a {
//       color: ${(props) => props.theme.main};
//     }
//   }

//   .ant-pagination-item:hover {
//     border-color: ${(props) => props.theme.darker};

//     a {
//       color: ${(props) => props.theme.darker};
//     }
//   }

//   .ant-pagination-next:hover .ant-pagination-item-link {
//     color: ${(props) => props.theme.darker};
//     border-color: ${(props) => props.theme.darker};
//   }

//   .ant-pagination-prev:hover .ant-pagination-item-link {
//     color: ${(props) => props.theme.darker};
//     border-color: ${(props) => props.theme.darker};
//   }
// `;

// const IsAdmin = styled.div`
//   background: #fa541c;
//   border-radius: 10px;
//   padding: 0 8px;
//   width: fit-content;
//   color: #fff;
// `;

// const WrapAction = styled.div`
//   img {
//     cursor: pointer;
//     margin-left: 8px;

//     :hover {
//       background: #eeeeee;
//     }
//   }
// `;

// const WrapSearch = styled.div`
//   width: 250px;
//   padding: 8px;
// `;

// const WrapButtonSearch = styled.div`
//   margin-top: 8px;
// `;

// const ButtonSearch = styled(Button)`
//   margin-right: 16px;
//   background: ${(props) => props.theme.main};
//   border: 1px solid ${(props) => props.theme.main};

//   span {
//     color: #fff;
//   }

//   :hover {
//     background: ${(props) => props.theme.darker}!important;
//     border: 1px solid ${(props) => props.theme.darker}!important;
//   }

//   :active {
//     background: ${(props) => props.theme.main};
//     border: 1px solid ${(props) => props.theme.main};

//     span {
//       color: #fff;
//     }
//   }

//   :focus {
//     background: ${(props) => props.theme.main};
//     border: 1px solid ${(props) => props.theme.main};

//     span {
//       color: #fff;
//     }
//   }
// `;

// const ButtonReset = styled(Button)`
//   background: #fff;
//   border: 1px solid #d9d9d9;

//   span {
//     color: #000;
//   }

//   :hover {
//     background: ${(props) => props.theme.darker}!important;
//     border: 1px solid ${(props) => props.theme.darker}!important;

//     span {
//       color: #fff !important;
//     }
//   }

//   :active {
//     background: #fff;
//     border: 1px solid #d9d9d9;

//     span {
//       color: #000;
//     }
//   }

//   :focus {
//     background: #fff;
//     border: 1px solid #d9d9d9;

//     span {
//       color: #000;
//     }
//   }
// `;

// const ButtonExcel = styled(Button)`
//   margin-right: 16px;

//   :hover {
//     background: ${(props) => props.theme.darker}!important;
//     border: 1px solid ${(props) => props.theme.darker}!important;

//     span {
//       color: #fff !important;
//     }
//   }

//   :active {
//     background: #fff;
//     border: 1px solid #d9d9d9;

//     span {
//       color: #000;
//     }
//   }

//   :focus {
//     background: #fff;
//     border: 1px solid #d9d9d9;

//     span {
//       color: #000;
//     }
//   }
// `;
