import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import isEmail from "validator/lib/isEmail";
import { useTranslation, withTranslation } from "react-i18next";
import { RANDOM_VARIABLE } from "constants/constants";
import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Space from "antd/lib/space";
import { Notification } from "components/Notification/Noti";

import {
  loadUserReport,
  resetUserInfo,
  createUser,
  updateUser,
} from "redux/slices/user";

function ModalUser({
  showModalUser,
  onHideModalUser,
  user,
  getListUser,
  isEdit,
  setIsEdit,
  listRole,
}) {
  const jwt = require("jsonwebtoken");
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoadingUser, listUserReport, isLoading, userInfo } = useSelector(
    (state) => state.userReducer
  );

  const [form] = Form.useForm();
  const { Option } = Select;

  const [roleOptions, setRoleOptions] = useState([]);
  const [roleAdmin, setRoleAdmin] = useState("");

  const [userReport, setUserReport] = useState([]);

  const [disableField, setDisableField] = useState(true);

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateUser({
          ID: user._id,
          First_Name: values.first_name.trim(),
          Last_Name: values.last_name.trim(),
          Middle_Name: values.middle_name ? values.middle_name.trim() : "",
          Report_To:
            user.User_Role === roleAdmin ? user.Report_To : values.report_to,
          User_Role:
            user.User_Role === roleAdmin ? user.User_Role : values.roles,
          Is_Admin: values.is_admin === "admin" ? true : false,
        })
      );
    } else {
      dispatch(
        createUser({
          Email: values.email,
          First_Name: values.first_name.trim(),
          Last_Name: values.last_name.trim(),
          Middle_Name: values.middle_name ? values.middle_name.trim() : "",
          Pass_Word: jwt.sign({ password: values.password }, RANDOM_VARIABLE, {
            algorithm: "HS256",
          }),
          Report_To: values.report_to,
          User_Role: values.roles,
          Is_Admin: values.is_admin === "admin" ? true : false,
        })
      );
    }
  };

  const _onCancel = () => {
    onHideModalUser();
    form.resetFields();
    setIsEdit(false);
    setDisableField(true);
  };

  const onGetUserReport = () => {
    setDisableField(false);
    dispatch(
      loadUserReport({
        role_id: form.getFieldValue("roles"),
      })
    );

    form.setFieldsValue({
      report_to: undefined,
    });
  };

  const systemRoles = [
    {
      label: "Admin",
      value: "admin",
    },
    {
      label: t("user.user"),
      value: "user",
    },
  ];

  useEffect(() => {
    if (isLoading === false) {
      if (userInfo === "success") {
        onHideModalUser();
        form.resetFields();
        setIsEdit(false);
        getListUser();
        setDisableField(true);
        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );

        dispatch(resetUserInfo());
      }

      if (userInfo !== null && userInfo !== "success") {
        Notification("error", userInfo);
        dispatch(resetUserInfo());
      }
    }
  }, [
    dispatch,
    form,
    getListUser,
    isEdit,
    isLoading,
    onHideModalUser,
    setIsEdit,
    userInfo,
  ]);

  useEffect(() => {
    if (isEdit) {
      form.setFieldsValue({
        is_admin: user.Is_Admin === true ? "admin" : "user",
        first_name: user.First_Name,
        middle_name: user.Middle_Name,
        last_name: user.Last_Name,
        roles: user.User_Role === roleAdmin ? undefined : user.User_Role,
        report_to: user.User_Role === roleAdmin ? undefined : user.Report_To,
      });

      setDisableField(false);
    }
  }, [dispatch, form, isEdit, roleAdmin, user]);

  useEffect(() => {
    if (listRole.length > 0) {
      let tempList = [];
      let tempRoleAdmin = "";
      listRole.forEach((item, idx) => {
        if (idx === 0) {
          tempRoleAdmin = item._id;
        } else {
          tempList.push({
            label: item.Name,
            value: item._id,
          });
        }
      });
      setRoleAdmin(tempRoleAdmin);
      setRoleOptions(tempList);
    }
  }, [listRole]);

  useEffect(() => {
    let tempList = [];

    listUserReport.forEach((item) => {
      if (user._id !== item._id) {
        tempList.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name + ` (${item.Email})`
              : item.Last_Name +
                " " +
                item.Middle_Name +
                " " +
                item.First_Name +
                ` (${item.Email})`,
          value: item._id,
        });
      }
    });
    setUserReport(tempList);
  }, [listUserReport, user]);

  return (
    <ModalCustom
      title={isEdit ? t("user.editUser") : t("user.createUser")}
      visible={showModalUser}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item label={null} name="is_admin">
          <Radio.Group
            disabled={user?.User_Role === roleAdmin}
            defaultValue="user"
          >
            <Space>
              {systemRoles.map((item) => {
                return (
                  <Radio key={item.value} value={item.value}>
                    {item.label}
                  </Radio>
                );
              })}
            </Space>
          </Radio.Group>
        </Form.Item>

        <Form.Item
          label={t("user.firstName")}
          name="first_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} maxLength={200} />
        </Form.Item>

        <Form.Item label={t("user.middleName")} name="middle_name">
          <Input placeholder={t("common.placeholderInput")} maxLength={200} />
        </Form.Item>

        <Form.Item
          label={t("user.lastName")}
          name="last_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} maxLength={200} />
        </Form.Item>

        {isEdit === false && (
          <>
            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  required: true,
                  message: t("common.placeholderInput"),
                },
                () => ({
                  validator(_, value) {
                    if (value) {
                      if (isEmail(value) === false) {
                        return Promise.reject(new Error(t("user.errorEmail")));
                      }
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
            >
              <Input
                placeholder={t("common.placeholderInput")}
                autocomplete="new-password"
              />
            </Form.Item>

            <Form.Item
              label={t("user.pass")}
              name="password"
              rules={[
                { required: true, message: t("common.placeholderInput") },
                () => ({
                  validator(_, value) {
                    if (value) {
                      if (value.length < 9) {
                        return Promise.reject(new Error(t("user.errorPass")));
                      } else if (!value.match(/(?=.*?[a-z])/)) {
                        return Promise.reject(new Error(t("user.errorPass2")));
                      } else if (!value.match(/(?=.*?[0-9])/)) {
                        return Promise.reject(new Error(t("user.errorPass5")));
                      } else if (
                        !value.match(/(?=.*?[,./=+<({})>[|!@#$%^&*?_-])/) &&
                        !value.includes("]") &&
                        !value.includes("\\")
                      ) {
                        return Promise.reject(new Error(t("user.errorPass4")));
                      } else if (!value.match(/(?=.*?[A-Z])/)) {
                        return Promise.reject(new Error(t("user.errorPass3")));
                      } else return Promise.resolve();
                    }
                    return Promise.resolve();
                  },
                }),
              ]}
            >
              <Input.Password
                placeholder={t("common.placeholderInput")}
                autocomplete="new-password"
              />
            </Form.Item>

            <Form.Item
              label={t("user.verifyPass")}
              name="verify_password"
              dependencies={["password"]}
              rules={[
                { required: true, message: t("common.placeholderInput") },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue("password") === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(
                      new Error(
                        "The two passwords that you entered do not match!"
                      )
                    );
                  },
                }),
              ]}
            >
              <Input.Password placeholder={t("common.placeholderInput")} />
            </Form.Item>
          </>
        )}

        {user?.User_Role !== roleAdmin && (
          <>
            <Form.Item
              label={t("settings.role")}
              name="roles"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                options={roleOptions}
                onChange={onGetUserReport}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
            <Form.Item
              label={t("user.reportTo")}
              name="report_to"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                loading={isLoadingUser}
                placeholder={t("common.placeholderSelect")}
                disabled={disableField}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {userReport.map((item) => {
                  return (
                    <Option value={item.value} key={item.value}>
                      {item.label}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>
          </>
        )}

        <WrapButton label=" ">
          <Button loading={isLoading} type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalUser);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
