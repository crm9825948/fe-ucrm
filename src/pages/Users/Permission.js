import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import { useParams } from "react-router";

import Breadcrumb from "antd/lib/breadcrumb";
import Checkbox from "antd/lib/checkbox";

import {
  getDomain,
  getUserRule,
  setUserRule,
  setUserRuleAll,
} from "redux/slices/user";
import { useEffect } from "react";

function Permission() {
  const { profileID } = useParams();
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { domains, userRule } = useSelector((state) => state.userReducer);

  const handleChangeRule = (value) => {
    if (value.target.name.split("#")[0] !== "all") {
      if (
        value.target.name.split("#")[1] === "brand_and_color" &&
        (value.target.name.split("#")[0] === "edit" ||
          value.target.name.split("#")[0] === "delete")
      ) {
        dispatch(
          setUserRule({
            domain: value.target.name.split("#")[1],
            action: "edit",
            profile_id: profileID,
          })
        );

        setTimeout(() => {
          dispatch(
            setUserRule({
              domain: value.target.name.split("#")[1],
              action: "delete",
              profile_id: profileID,
            })
          );
        }, 500);
      } else {
        dispatch(
          setUserRule({
            domain: value.target.name.split("#")[1],
            action: value.target.name.split("#")[0],
            profile_id: profileID,
          })
        );
      }
    } else {
      dispatch(
        setUserRuleAll({
          domain: value.target.name.split("#")[1],
          toggle: value.target.checked,
          profile_id: profileID,
        })
      );
    }
  };

  useEffect(() => {
    dispatch(getDomain());
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      getUserRule({
        profile_id: profileID,
      })
    );
  }, [dispatch, profileID]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <Breadcrumb.Item onClick={() => navigate("/profiles")}>
          {t("settings.profiles")}
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          {localStorage.getItem("nameProfilePermission")}
        </Breadcrumb.Item>
        <BreadcrumbItem>Permission</BreadcrumbItem>
      </Breadcrumb>

      <WrapRule>
        <div
          style={{
            background: "#fff",
            zIndex: 2,
            position: "sticky",
            height: "24px",
            top: 0,
          }}
        />
        <Header>
          <FeatureName>
            <span>Feature</span>
          </FeatureName>
          <Rule>
            <span>{t("common.view")}</span>
          </Rule>
          <Rule>
            <span>{t("common.create")}</span>
          </Rule>
          <Rule>
            <span>{t("common.edit")}</span>
          </Rule>
          <Rule>
            <span>{t("common.delete")}</span>
          </Rule>
        </Header>

        {domains.map((item) => {
          return (
            <Wrap key={item.name}>
              <ObjectNameDetail>
                <Checkbox
                  name={`all#${item.name}`}
                  checked={userRule.find(
                    (rule) =>
                      rule.domain === item.name && rule.actions.includes("view")
                  )}
                  onChange={handleChangeRule}
                />
                <span>{item.label}</span>
              </ObjectNameDetail>
              <RoleDetail>
                <Checkbox
                  name={`view#${item.name}`}
                  checked={userRule.find(
                    (rule) =>
                      rule.domain === item.name && rule.actions.includes("view")
                  )}
                  onChange={handleChangeRule}
                />
              </RoleDetail>
              <RoleDetail>
                <Checkbox
                  name={`create#${item.name}`}
                  checked={userRule.find(
                    (rule) =>
                      rule.domain === item.name &&
                      rule.actions.includes("create")
                  )}
                  disabled={
                    item.name === "quality_management" ||
                    item.name === "date_time_setting" ||
                    item.name === "campaign" ||
                    item.name === "agent_monitor" ||
                    item.name === "url_popup_setting" ||
                    item.name === "expose_api_check" ||
                    !userRule.find(
                      (rule) =>
                        rule.domain === item.name &&
                        rule.actions.includes("view")
                    )
                  }
                  onChange={handleChangeRule}
                />
              </RoleDetail>
              <RoleDetail>
                <Checkbox
                  name={`edit#${item.name}`}
                  checked={userRule.find(
                    (rule) =>
                      rule.domain === item.name && rule.actions.includes("edit")
                  )}
                  disabled={
                    item.name === "related_object" ||
                    item.name === "oauth2" ||
                    item.name === "o365_integration" ||
                    item.name === "agent_monitor" ||
                    !userRule.find(
                      (rule) =>
                        rule.domain === item.name &&
                        rule.actions.includes("view")
                    )
                  }
                  onChange={handleChangeRule}
                />
              </RoleDetail>
              <RoleDetail>
                <Checkbox
                  name={`delete#${item.name}`}
                  checked={userRule.find(
                    (rule) =>
                      rule.domain === item.name &&
                      rule.actions.includes("delete")
                  )}
                  disabled={
                    item.name === "quality_management" ||
                    item.name === "date_time_setting" ||
                    item.name === "related_object" ||
                    item.name === "campaign" ||
                    item.name === "duplicate_rules" ||
                    item.name === "agent_monitor" ||
                    item.name === "url_popup_setting" ||
                    item.name === "voice_bot" ||
                    item.name === "expose_api_check" ||
                    !userRule.find(
                      (rule) =>
                        rule.domain === item.name &&
                        rule.actions.includes("view")
                    )
                  }
                  onChange={handleChangeRule}
                />
              </RoleDetail>
            </Wrap>
          );
        })}
      </WrapRule>
    </Wrapper>
  );
}

export default withTranslation()(Permission);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-breadcrumb {
    margin-bottom: 24px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapRule = styled.div`
  padding: 0 24px 24px 24px;
  background: #fff;
  height: calc(100vh - 220px);
  overflow: auto;
`;

const Header = styled.div`
  position: sticky;
  top: 24px;
  z-index: 2;
  display: flex;
  background: #fafafa;
  border: 1px solid #ececec;
  padding: 0 16px;
`;

const FeatureName = styled.div`
  flex: 2;
  padding: 8px 0;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;
  border-right: 1px solid #d9d9d9;

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const ObjectNameDetail = styled(FeatureName)`
  border-right: none;
`;

const Rule = styled.div`
  flex: 1;
  border-right: 1px solid #d9d9d9;
  padding: 8px 0 8px 16px;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;

  :last-child {
    border-right: none;
  }

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const RoleDetail = styled(Rule)`
  border-right: none;
`;

const Wrap = styled.div`
  display: flex;
  padding: 0 16px;
  border: 1px solid #eeeeee;
  border-top: none;
`;
