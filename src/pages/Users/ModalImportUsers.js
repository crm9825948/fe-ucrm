import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import Button from "antd/lib/button";

import { importUsers } from "redux/slices/user";

import { Notification } from "components/Notification/Noti";

function ModalImportUsers({ showModalImport, setShowModalImport }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [file, setFile] = useState(null);

  const generateOptions = [
    {
      label: "Random",
      value: "random",
    },
  ];

  const handleSelectFile = (e) => {
    setFile(e.target.files[0]);
  };

  const _onSubmit = (values) => {
    if (
      file.name.split(".")[file.name.split(".").length - 1] !== "xlsx" &&
      file.name.split(".")[file.name.split(".").length - 1] !== "xls"
    ) {
      Notification("error", "Please choose Excel file!");
    } else {
      const formData = new FormData();
      formData.append("file", file);
      formData.append("send_mail", values.send_mail || false);
      formData.append("generate_password", "random");

      dispatch(importUsers(formData));
      setTimeout(() => {
        setShowModalImport(false);
        setFile(null);
        form.resetFields();
      }, 1000);
    }
  };

  const _onCancel = () => {
    setShowModalImport(false);
    setFile(null);
    form.resetFields();
  };

  useEffect(() => {
    if (showModalImport) {
      form.setFieldsValue({
        generate_password: "random",
      });
    }
  }, [form, showModalImport]);

  return (
    <ModalImport
      title="Import users"
      visible={showModalImport}
      width={600}
      footer={false}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item label="Generate password" name="generate_password">
          <Select disabled options={generateOptions} />
        </Form.Item>

        <Form.Item
          label="File"
          name="file"
          rules={[
            {
              required: true,
              message: t("common.placeholderSelect"),
            },
          ]}
        >
          <input type="file" onChange={handleSelectFile} accept=".xlsx, .xls" />
        </Form.Item>

        <Form.Item label="" name="send_mail" valuePropName="checked">
          <Checkbox>Send mail</Checkbox>
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalImport>
  );
}

export default withTranslation()(ModalImportUsers);

const ModalImport = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
