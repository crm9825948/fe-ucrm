import { DatePicker, Input, Pagination, Select, Typography } from "antd";
import Breadcrumb from "antd/lib/breadcrumb";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import { CSVLink } from "react-csv";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { changeTitlePage } from "redux/slices/authenticated";
import { loadLogs } from "redux/slices/systemLog";
import styled from "styled-components";
import { checkTokenExpiration } from "contexts/TokenCheck";
const { Option } = Select;
const { Text } = Typography;

// const { RangePicker } = DatePicker;

const SystemLog = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { logs } = useSelector((state) => state.systemLogReducer);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [searchData, setSearchData] = useState({});
  const [dataExport, setDataExport] = useState([]);

  useEffect(() => {
    dispatch(changeTitlePage(t("systemLog.systemLog")));
    //eslint-disable-next-line
  }, [t]);

  const handleLoadLog = useCallback(() => {
    dispatch(
      loadLogs({
        limit: recordPerPage,
        page: currentPage,
        search_data: searchData,
      })
    );
    /* eslint-disable-next-line */
  }, [currentPage, recordPerPage, dispatch]);

  useEffect(() => {
    handleLoadLog();
  }, [handleLoadLog]);

  const handleSearch = () => {
    dispatch(
      loadLogs({
        limit: recordPerPage,
        page: currentPage,
        search_data: searchData,
      })
    );
  };

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "log-system/get",
          {
            limit: 10000,
            page: 1,
            search_data: {},
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setDataExport(res.data.data.results);
        })
        .catch((err) => console.log(err));
    };
    checkToken();
  }, []);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("systemLog.systemLog")}</BreadcrumbItem>
        </Breadcrumb>
      </WrapBreadcrumb>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <ClearButton
          onClick={() => {
            setSearchData({});
            dispatch(
              loadLogs({
                limit: recordPerPage,
                page: currentPage,
                search_data: {},
              })
            );
          }}
        >
          {t("systemLog.clearFilter")}
        </ClearButton>
        <CSVLink data={dataExport} filename={"system-log.csv"}>
          <ExportButton>
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M21 15V19C21 19.5304 20.7893 20.0391 20.4142 20.4142C20.0391 20.7893 19.5304 21 19 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V15"
                stroke="#2C2C2C"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M7 10L12 15L17 10"
                stroke="#2C2C2C"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M12 15V3"
                stroke="#2C2C2C"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            {t("systemLog.export")}
          </ExportButton>
        </CSVLink>
      </div>
      <WrapperContent>
        <table className="table">
          <thead>
            <tr>
              <th> {t("systemLog.author")}</th>
              <th> {t("systemLog.type")}</th>
              <th> {t("systemLog.action")}</th>
              <th> {t("systemLog.date")}</th>
              <th> {t("systemLog.content")}</th>
            </tr>
            <tr>
              <td>
                <Input
                  style={{ width: "100%" }}
                  onChange={(e) => {
                    let temp = { ...searchData };
                    temp["email"] = e.target.value;
                    setSearchData(temp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                    setCurrentPage(1);
                  }}
                  value={searchData["email"]}
                />
              </td>
              <td>
                <Select
                  style={{ width: "100%" }}
                  onChange={(e) => {
                    let temp = { ...searchData };
                    temp["type_of_action"] = e;
                    setSearchData(temp);
                    setCurrentPage(1);
                    dispatch(
                      loadLogs({
                        limit: recordPerPage,
                        page: 1,
                        search_data: temp,
                      })
                    );
                  }}
                  onPressEnter={() => {
                    handleLoadLog();
                  }}
                  allowClear
                  value={searchData["type_of_action"]}
                >
                  <Option value={"User"}>User</Option>
                  <Option value={"Role"}>Role</Option>
                  <Option value={"Profile"}>Profile</Option>
                  <Option value={"Object"}>Object</Option>
                  <Option value={"Fields"}>Fields</Option>
                  <Option value={"Dynamic button"}>Dynamic button</Option>
                  <Option value={"Workflow"}>Workflow</Option>
                </Select>
              </td>
              <td>
                <Select
                  value={searchData["action"]}
                  style={{ width: "100%" }}
                  onChange={(e) => {
                    let temp = { ...searchData };
                    temp["action"] = e;
                    setSearchData(temp);
                    setCurrentPage(1);
                    dispatch(
                      loadLogs({
                        limit: recordPerPage,
                        page: 1,
                        search_data: temp,
                      })
                    );
                  }}
                  onPressEnter={() => {
                    handleLoadLog();
                  }}
                  allowClear
                >
                  <Option value={"Create"}>Create</Option>
                  <Option value={"Update"}>Update</Option>
                  <Option value={"Delete"}>Delete</Option>
                  <Option value={"Login"}>Login</Option>
                  <Option value={"Logout"}>Logout</Option>
                </Select>
              </td>
              <td>
                <div style={{ display: "flex" }}>
                  {" "}
                  <DatePicker
                    showTime
                    placeholder={t("systemLog.from")}
                    value={
                      searchData["date_from"]
                        ? moment(searchData["date_from"], "YYYY-MM-DD HH:mm:ss")
                        : null
                    }
                    onChange={(e, dateString) => {
                      let temp = { ...searchData };
                      temp["date_from"] = dateString;
                      setSearchData(temp);
                      setCurrentPage(1);
                      dispatch(
                        loadLogs({
                          limit: recordPerPage,
                          page: 1,
                          search_data: temp,
                        })
                      );
                    }}
                    onPressEnter={() => {
                      handleLoadLog();
                    }}
                    style={{ marginRight: "10px", width: "100%" }}
                  />
                  <DatePicker
                    showTime
                    placeholder={t("systemLog.to")}
                    value={
                      searchData["date_to"]
                        ? moment(searchData["date_to"], "YYYY-MM-DD HH:mm:ss")
                        : null
                    }
                    onChange={(e, dateString) => {
                      let temp = { ...searchData };
                      temp["date_to"] = dateString;
                      setSearchData(temp);
                      setCurrentPage(1);
                      dispatch(
                        loadLogs({
                          limit: recordPerPage,
                          page: 1,
                          search_data: temp,
                        })
                      );
                    }}
                    onPressEnter={() => {
                      handleLoadLog();
                    }}
                    style={{ width: "100%" }}
                  />
                </div>
                {/* <RangePicker
                  style={{ width: "100%" }}
                  showTime
                  onChange={(e, dateString) => {
                    let temp = { ...searchData };
                    temp["date_from"] = dateString[0];
                    temp["date_to"] = dateString[1];
                    setSearchData(temp);
                    handleSearch();
                  }}
                  onPressEnter={() => {
                    handleLoadLog();
                  }}
                /> */}
              </td>
              <td>
                <Input
                  style={{ width: "100%" }}
                  value={searchData["content"]}
                  onChange={(e) => {
                    let temp = { ...searchData };
                    temp["content"] = e.target.value;
                    setSearchData(temp);
                  }}
                  onPressEnter={() => {
                    handleLoadLog();
                    handleSearch();
                    setCurrentPage(1);
                  }}
                />
              </td>
            </tr>
          </thead>
          <tbody>
            {logs &&
              logs.results &&
              logs.results.map((item, idx) => {
                return (
                  <tr>
                    <td>
                      <Text ellipsis={{ tooltip: item.email }}>
                        {item.email}
                      </Text>
                    </td>
                    <td>
                      <Text ellipsis={{ tooltip: item.type_of_action }}>
                        {item.type_of_action}
                      </Text>
                    </td>
                    <td>
                      <Text ellipsis={{ tooltip: item.action }}>
                        {item.action}
                      </Text>
                    </td>
                    <td>
                      <Text ellipsis={{ tooltip: item.modified_date }}>
                        {item.modified_date}
                      </Text>
                    </td>
                    <td>
                      <Text
                        ellipsis={{ tooltip: item.content }}
                        style={{ width: "400px" }}
                      >
                        {item.content}
                      </Text>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <CustomPagination
          showQuickJumper
          current={currentPage}
          total={logs && logs.total_records}
          showSizeChanger
          showTotal={(total, range) => {
            return `${range[0]}-${range[1]}  ${t("systemLog.of")} ${total}  ${t(
              "systemLog.record"
            )}`;
          }}
          // pageSize={recordPerPage}
          onChange={(e, pageSize) => {
            setCurrentPage(e);
            setRecordPerPage(pageSize);
          }}
        />
      </WrapperContent>
    </Wrapper>
  );
};

export default withTranslation()(SystemLog);

const Wrapper = styled.div`
  padding: 24px;
`;
const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapperContent = styled.div`
  margin-top: 21px;
  height: 580px;
  overflow: scroll;
  .table {
    width: 100%;
    background-color: white;
    thead {
      position: sticky;
      top: 0;
      th {
        padding: 9px;
        text-align: left;
        min-width: 100px;
        background-color: #ececec;

        font-style: normal;
        font-size: 16px;
        line-height: 22px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
      td {
        padding: 9px;
        text-align: left;
        min-width: 100px;
        background-color: #ececec;
        padding-top: 0;
      }
    }
    tbody {
      tr {
        td {
          padding: 9px;
          font-size: 16px;
          line-height: 22px;
          /* identical to box height, or 138% *

          /* Character/Body text */
          border-bottom: 0.5px solid #ececec;
          color: #2c2c2c;
        }
      }
    }
  }
`;

const CustomPagination = styled(Pagination)`
  /* position: fixed; */
  /* bottom: 50px; */
  /* right: 16px; */
  position: sticky;
  bottom: 0;
  text-align: right;
  background-color: #fff;
  padding: 16px;
  margin: auto;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;
const ExportButton = styled.button`
  img {
    width: 18px;
    margin-right: 12px;
  }
  a {
    text-decoration: none;
    color: #20a2a2;
  }
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  background: #ffffff;
  /* Neutral/5 */

  border: 1px solid #d9d9d9;
  /* drop-shadow / button-secondary */

  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  padding: 5px 16px;

  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 17px;
  line-height: 20px;
  /* identical to box height */
  display: flex;
  align-items: center;
  color: #2c2c2c;
  cursor: pointer;
  svg {
    margin-right: 12px;
  }
  :hover {
    img {
      width: 18px;
      margin-right: 12px;
    }
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border: 1px solid #20a2a2;
    border-radius: 2px;
    padding: 5px 16px;
    background-color: #20a2a2;

    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 17px;
    line-height: 20px;
    /* identical to box height */

    display: flex;
    align-items: center;

    color: white;
    svg {
      filter: brightness(200);
    }
  }
`;

const ClearButton = styled.button`
  padding: 5px 16px;
  font-style: normal;
  font-weight: 400;
  font-size: 17px;
  line-height: 22px;
  /* identical to box height, or 138% */

  text-align: center;

  /* Character/Body text */

  color: #2c2c2c;
  background: #ffffff;
  /* Neutral/5 */

  border: 1px solid #d9d9d9;
  /* drop-shadow / button-secondary */

  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  margin-right: 10px;
  :hover {
    cursor: pointer;
  }
`;
