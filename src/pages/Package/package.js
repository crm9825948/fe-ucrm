import { Table, Popconfirm } from "antd";
import React from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deletePackage, loadPackage } from "redux/slices/tenants";
import editImg from "assets/icons/common/edit.png";
import deleteImg from "assets/icons/common/delete.png";
import { useTranslation } from "react-i18next";

const Package = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { packageList } = useSelector((state) => state.tenantsReducer);

  useEffect(() => {
    dispatch(loadPackage({}));
  }, [dispatch]);
  const columns = [
    {
      title: t("tenants.name"),
      with: 200,
      dataIndex: "name_edition",
      key: "name_edition",
    },
    {
      title: t("tenants.totalUsageRecord"),
      with: 200,
      dataIndex: "limit_record",
      key: "limit_record",
    },
    {
      title: t("tenants.notificationThreshold"),
      with: 200,
      dataIndex: "allow_warning",
      key: "allow_warning",
    },
    {
      title: t("tenants.action"),
      with: 200,
      render: (record) => (
        <div>
          <img
            alt=""
            src={editImg}
            style={{ color: "grey", width: "18px", marginRight: "16px" }}
            onClick={() => {
              navigate(`/package/${record._id}`);
            }}
          />
          <Popconfirm
            placement="topLeft"
            title={t("tenants.confirm1")}
            onConfirm={() => {
              dispatch(
                deletePackage({
                  id_edition: record._id,
                })
              );
            }}
            okText={t("tenants.yes")}
            cancelText={t("tenants.no")}
          >
            <img
              alt=""
              src={deleteImg}
              style={{ width: "20px" }}
              onClick={() => {}}
            />
          </Popconfirm>
        </div>
      ),
    },
  ];
  return (
    <Wrapper>
      <div>
        <button
          className="add-btn"
          onClick={() => {
            navigate("/add-new-package");
          }}
        >
          {t("tenants.addPackage")}
        </button>
      </div>
      <Table columns={columns} dataSource={packageList} pagination={false} />
    </Wrapper>
  );
};

export default Package;

const Wrapper = styled.div`
  padding: 24px;
  .add-btn {
    background: #20a2a2;
    border: 1px solid #20a2a2;
    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
    border-radius: 2px;
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    color: #ffffff;
    padding: 8px 16px;
    margin-bottom: 16px;
  }
`;
