import { Tabs } from "antd";
import close from "assets/images/consolidatedView/close.png";
import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import _ from "lodash";
import { default as React, useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { loadConfig } from "redux/slices/datetimeSetting";
import {
  loadLayouts,
  updateLayout,
} from "redux/slices/consolidatedViewSettings";
import { setShowLoadingScreen } from "redux/slices/global";
import styled from "styled-components";
import TabComponent from "./tabComponent";
import TableComponent from "./componentType/table1";
import TagsComponent from "./componentType/tags";
import LogsComponent from "./componentType/logs";
import SLAComponent from "./componentType/sla";
import DetailComponent from "./componentType/details";
import MultiDetail from "./componentType/multiDetail";
import TableThirdParty from "./componentType/tableThirdParty";
import TimelineHorizontal from "./componentType/timeLine/timelineHorizontal";
import TimelineVertical from "./componentType/timeLine/timelineVertical";
import DetailsThirdParty from "./componentType/detailsThirdParty";
import ComponentKnowledgeBase from "./componentType/knowledgeBase";
import Comment from "./componentType/comment";
import CampaignGeneral from "./componentType/campaignGeneral";
import CampaignTemplate from "./componentType/campaignTemplate";
import { Notification } from "components/Notification/Noti";
import ComponentSMS from "./componentType/componentSMS";
import Enhancement from "./componentType/Enhancement";
import ICChatWidget from "./componentType/ICChatWidget/ICChatWidget";

const ReactGridLayout = WidthProvider(RGL);

const { TabPane } = Tabs;

const CenterScreen = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  onLayoutChange = function () {},
  ...props
}) => {
  let dispatch = useDispatch();

  const { edit, drag, setEditDetail, editDetail, checkRule, setDrag } = props;

  const { layouts, loadingComponents, isLoadingUpdate, isLoadingLoadLayout } =
    useSelector((state) => state.consolidatedViewSettingsReducer);
  // eslint-disable-next-line
  const { objectId, recordID } = useParams();
  const { userDetail } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(loadConfig());
  }, [dispatch]);

  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = [...layouts.components];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItems(tmpArr);
    }
  }, [layouts]);

  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_top_component ? [...layouts.fix_top_component] : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsTop(tmpArr);
    } else if (
      layouts?.placement === "middle" &&
      layouts?.fix_top_component?.length === 0
    ) {
      setItemsTop([]);
    }
  }, [layouts]);

  //left
  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_left_component
        ? [...layouts.fix_left_component]
        : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsLeft(tmpArr);
    } else if (
      layouts?.placement === "middle" &&
      layouts?.fix_left_component?.length === 0
    ) {
      setItemsLeft([]);
    }
  }, [layouts]);

  //right
  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_right_component
        ? [...layouts.fix_right_component]
        : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsRight(tmpArr);
    } else if (
      layouts?.placement === "middle" &&
      layouts?.fix_right_component?.length === 0
    ) {
      setItemsRight([]);
    }
  }, [layouts]);

  useEffect(() => {
    dispatch(
      loadLayouts({
        object_id: objectId,
        placement: "middle",
      })
    );
  }, [objectId, dispatch]);

  const handleUpdate = (items) => {
    if (edit) {
      let result = [];
      /*eslint-disable-next-line*/
      items.map((item, idx) => {
        let newItem = {
          x: item.x,
          y: item.y,
          w: item.w,
          h: item.h,
          i: item.i,
        };
        result.push(newItem);
      });
      dispatch(
        updateLayout({
          object_id: objectId,
          placement: "middle",
          components: result,
          fix_top_component: itemsTop,
          fix_left_component: itemsLeft,
          fix_right_component: itemsRight,
        })
      );
    }
  };

  const handleUpdateTop = (itemsTop) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsTop.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: objectId,
        placement: "middle",
        components: items,
        fix_top_component: result,
        fix_left_component: itemsLeft,
        fix_right_component: itemsRight,
      })
    );
  };

  const handleUpdateLeft = (itemsLeft) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsLeft.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: objectId,
        placement: "middle",
        components: items,
        fix_top_component: itemsTop,
        fix_left_component: result,
        fix_right_component: itemsRight,
      })
    );
  };

  const handleUpdateRight = (itemsRight) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsRight.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: objectId,
        placement: "middle",
        components: items,
        fix_top_component: itemsTop,
        fix_left_component: itemsLeft,
        fix_right_component: result,
      })
    );
  };

  const [items, setItems] = useState([]);
  const [itemsTop, setItemsTop] = useState([]);
  const [itemsLeft, setItemsLeft] = useState([]);
  const [itemsRight, setItemsRight] = useState([]);
  const [layout, setLayout] = useState([]);

  useEffect(() => {
    console.log(layout, itemsRight, itemsLeft, itemsTop);
  }, [layout, itemsRight, itemsLeft, itemsTop]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
  }, [loadingComponents, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingUpdate));
  }, [isLoadingUpdate, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingLoadLayout));
  }, [isLoadingLoadLayout, dispatch]);

  const [key] = useState(0);

  // function callback(key) {
  //   setKey(key);
  // }

  const createElement = (el, position) => {
    if (JSON.parse(el.i).type === "tab") {
      let data = JSON.parse(el.i);
      return (
        <CustomItem
          key={el.i}
          data-grid={el}
          style={{
            overflow: "hidden",
            // boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
            height: "100%",
            position: "relative",
          }}
        >
          {userDetail.Is_Admin || checkRule("delete") ? (
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => {
                let flag = false;

                JSON.parse(el.i).tab_name.map((item, idx) => {
                  if (JSON.parse(el.i).tab_component[item].length > 0) {
                    flag = true;
                  }
                  return null;
                });

                // Object.entries(JSON.parse(el.i).tab_component).forEach(
                //   /* eslint-disable-next-line */
                //   ([key, value], index) => {}
                // );
                if (flag === false) {
                  onRemoveItem(el.i);
                } else {
                  Notification("error", "Can not delete this component!");
                }
              }}
              className="close-png"
            />
          ) : (
            ""
          )}

          <Tabs defaultActiveKey={key}>
            {data.tab_name.map((item, idx) => {
              return (
                <TabPane tab={item} key={idx}>
                  <TabComponent
                    recordId={recordID}
                    data={JSON.parse(el.i)}
                    index={idx}
                    name={item}
                    itemParent={items}
                    setItemParent={setItems}
                    objectId={objectId}
                    recordID={recordID}
                    edit={edit}
                    drag={drag}
                    setDrag={setDrag}
                    checkRule={checkRule}
                    onRemoveItem={onRemoveItem}
                    editDetail={editDetail}
                    setEditDetail={setEditDetail}
                  />
                </TabPane>
              );
            })}
          </Tabs>
        </CustomItem>
      );
    } else
      return (
        <WrapComponent key={el.i} data-grid={el}>
          <div style={{ padding: "0", height: "100%" }}>
            <div
              style={{
                height: "100%",
                background: `${
                  JSON.parse(el.i).type === "table" ||
                  JSON.parse(el.i).type === "tags" ||
                  JSON.parse(el.i).type === "details" ||
                  JSON.parse(el.i).type === "logs" ||
                  JSON.parse(el.i).component_type === "TABLE"
                    ? "white"
                    : "#c4c4c4" || JSON.parse(el.i).component_type === "DETAILS"
                    ? "white"
                    : "#c4c4c4"
                }`,
                // background: "white",
                display: "flex",
                justifyContent:
                  JSON.parse(el.i).type !== "knowledgeBase"
                    ? "center"
                    : "unset",
                alignItems:
                  JSON.parse(el.i).type !== "knowledgeBase"
                    ? "center"
                    : "unset",
              }}
            >
              {JSON.parse(el.i).type === "table" ? (
                <TableComponent
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).type === "tags" ? (
                <TagsComponent
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).type === "logs" ? (
                <LogsComponent
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  checkRule={checkRule}
                  onRemoveItem={handleRemove(position)}
                />
              ) : JSON.parse(el.i).type === "sla" ? (
                <SLAComponent
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "details" ? (
                <DetailComponent
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  setEditDetail={setEditDetail}
                  editDetail={editDetail}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).type === "multi_detail" ? (
                <MultiDetail
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  setEditDetail={setEditDetail}
                  editDetail={editDetail}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).component_type === "TABLE" ? (
                <TableThirdParty
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).component_type === "DETAILS" ? (
                <DetailsThirdParty
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  edit={edit}
                  onRemoveItem={handleRemove(position)}
                  checkRule={checkRule}
                  setDrag={setDrag}
                />
              ) : JSON.parse(el.i).type === "horizontal-timeline" ? (
                <TimelineHorizontal
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  checkRule={checkRule}
                  type="horizontal-timeline"
                />
              ) : JSON.parse(el.i).type === "horizontal-timeline-new" ? (
                <TimelineHorizontal
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  checkRule={checkRule}
                  type="horizontal-timeline-new"
                />
              ) : JSON.parse(el.i).type === "vertical-timeline" ? (
                <TimelineVertical
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  checkRule={checkRule}
                  type="vertical-timeline"
                />
              ) : JSON.parse(el.i).type === "vertical-timeline-new" ? (
                <TimelineVertical
                  data={JSON.parse(el.i)}
                  recordID={recordID}
                  objectId={objectId}
                  checkRule={checkRule}
                  type="vertical-timeline-new"
                />
              ) : JSON.parse(el.i).type === "knowledgeBase" ? (
                <ComponentKnowledgeBase
                  data={JSON.parse(el.i)}
                  recordId={recordID}
                  objectId={objectId}
                  checkRule={checkRule}
                  layout={
                    layout.filter(
                      (x) => JSON.parse(x.i).type === "knowledgeBase"
                    )[0]
                  }
                />
              ) : JSON.parse(el.i).type === "comment" ? (
                <Comment
                  objectId={objectId}
                  recordID={recordID}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "general-configuration" ? (
                <CampaignGeneral
                  objectId={objectId}
                  recordID={recordID}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "template-configuration" ? (
                <CampaignTemplate
                  objectId={objectId}
                  recordID={recordID}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "sms-component" ? (
                <ComponentSMS
                  objectId={objectId}
                  recordID={recordID}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "knowledge" ? (
                <Enhancement
                  objectId={objectId}
                  recordID={recordID}
                  data={JSON.parse(el.i)}
                  checkRule={checkRule}
                />
              ) : JSON.parse(el.i).type === "ic-chat-widget" ? (
                <ICChatWidget recordID={recordID} objectId={objectId} />
              ) : (
                // <div style={{ height: "100%" }}>
                <img
                  alt=""
                  src={Triangle}
                  style={{ width: "50%" }}
                  checkRule={checkRule}
                />
                // </div>
              )}
            </div>
          </div>
        </WrapComponent>
      );
  };
  // eslint-disable-next-line
  const onAddItem = (newItem) => {
    setItems(items.concat(newItem));
    handleUpdate(items.concat(newItem));
  };
  const [reload, setReload] = useState(0);
  const onLayoutChangeNew = (layout) => {
    setLayout(layout);
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  const handleRemove = (position) => {
    switch (position) {
      case "top":
        return onRemoveItemTop;
      case "left":
        return onRemoveItemLeft;
      case "right":
        return onRemoveItemRight;
      case "center":
        return onRemoveItem;
      default:
        break;
    }
  };

  const onRemoveItem = (i) => {
    if (reload < 2) {
      handleUpdate(
        _.reject(items, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItems(
        _.reject(items, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    } else if (reload >= 2) {
      handleUpdate(
        _.reject(items, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItems(
        _.reject(items, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    }
  };

  const onRemoveItemLeft = (i) => {
    if (reloadLeft < 2) {
      handleUpdateLeft(
        _.reject(itemsLeft, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsLeft(
        _.reject(itemsLeft, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    } else if (reloadLeft >= 2) {
      handleUpdateLeft(
        _.reject(itemsLeft, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsLeft(
        _.reject(itemsLeft, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    }
  };

  const onRemoveItemTop = (i) => {
    if (reloadTop < 2) {
      handleUpdateTop(
        _.reject(itemsTop, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsTop(
        _.reject(itemsTop, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    } else if (reloadTop >= 2) {
      handleUpdateTop(
        _.reject(itemsTop, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsTop(
        _.reject(itemsTop, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    }
  };

  const onRemoveItemRight = (i) => {
    if (reloadRight < 2) {
      handleUpdateRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    } else if (reloadRight >= 2) {
      handleUpdateRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    }
  };

  const [reloadTop, setReloadTop] = useState(0);
  const onLayoutChangeNewCenterTop = (layout) => {
    if (reloadTop === 0) {
    } else if (reloadTop === 1) {
      onLayoutChange(layout);
      setItemsTop(layout);
      handleUpdateTop(layout);
    } else if (reloadTop >= 2 && reloadTop % 2 === 0) {
      onLayoutChange(layout);
      setItemsTop(layout);
      handleUpdateTop(layout);
    }
    setReloadTop(reloadTop + 1);
  };

  const [reloadLeft, setReloadLeft] = useState(0);
  const onLayoutChangeNewCenterLeft = (layout) => {
    if (reloadLeft === 0) {
    } else if (reloadLeft === 1) {
      onLayoutChange(layout);
      setItemsLeft(layout);
      handleUpdateLeft(layout);
    } else if (reloadLeft >= 2 && reloadLeft % 2 === 0) {
      onLayoutChange(layout);
      setItemsLeft(layout);
      handleUpdateLeft(layout);
    }
    setReloadLeft(reloadTop + 1);
  };

  const [reloadRight, setReloadRight] = useState(0);
  const onLayoutChangeNewCenterRight = (layout) => {
    if (reloadRight === 0) {
    } else if (reloadRight === 1) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdateRight(layout);
    } else if (reloadRight >= 2 && reloadRight % 2 === 0) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdateRight(layout);
    }
    setReloadRight(reloadTop + 1);
  };

  return (
    <>
      <Wrapper>
        {items.length === 0 ? (
          <div className="no-data-container">
            <img alt="" src={emptyImg} className="empty-layout-img" />
            <div className="no-data-decs">No data. Select component to add</div>
          </div>
        ) : (
          <div style={{ overflow: "auto" }}>
            {/* <ReactGridLayout
              margin={[8, 8]}
              onLayoutChange={onLayoutChangeNew}
              {...props}
              isDraggable={drag}
              isResizable={drag}
              rowHeight={10}
            >
              {_.map(items, (el) => createElement(el))}
            </ReactGridLayout> */}
            <div
              className="top-fixed"
              style={{ display: `${itemsTop.length === 0 ? "none" : ""}` }}
            >
              {itemsTop.length === 0 ? (
                <div
                  style={{
                    width: "100%",
                    minHeight: "200px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                ></div>
              ) : (
                <ReactGridLayout
                  // isBounded={true}
                  onLayoutChange={onLayoutChangeNewCenterTop}
                  {...props}
                  isDraggable={drag && edit && checkRule("edit")}
                  isResizable={drag && edit && checkRule("edit")}
                  rowHeight={10}
                  margin={[5, 5]}
                  // className="layout"
                  // maxRows={9}
                >
                  {_.map(itemsTop, (el) => createElement(el, "top"))}
                </ReactGridLayout>
              )}
            </div>
            <div className="combine-layout">
              <div
                className="left-fixed"
                style={{
                  display: `${itemsLeft.length === 0 ? "none" : ""}`,
                  height: `calc(100vh - ${
                    itemsTop.length === 0 ? "0" : "300px"
                  })`,
                }}
              >
                {itemsLeft.length === 0 ? (
                  <div
                    style={{
                      width: "100%",
                      minHeight: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  ></div>
                ) : (
                  <>
                    {" "}
                    <ReactGridLayout
                      // isBounded={true}
                      onLayoutChange={onLayoutChangeNewCenterLeft}
                      {...props}
                      isDraggable={drag && edit && checkRule("edit")}
                      isResizable={drag && edit && checkRule("edit")}
                      rowHeight={10}
                      cols={1}
                      margin={[5, 5]}
                    >
                      {_.map(itemsLeft, (el) => createElement(el, "left"))}
                    </ReactGridLayout>{" "}
                    <div
                      style={{
                        width: "100%",
                        minHeight: "200px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    ></div>
                  </>
                )}
              </div>
              <div
                className="middle-fixed"
                style={{
                  width: `${
                    itemsLeft.length === 0 && itemsRight.length === 0
                      ? "100%"
                      : (itemsLeft.length > 0 && itemsRight.length === 0) ||
                        (itemsRight.length > 0 && itemsLeft.length === 0)
                      ? "85%"
                      : "70%"
                  }`,
                  height: `calc(100vh - ${
                    itemsTop.length === 0 ? "0" : "300px"
                  })`,
                }}
              >
                <ReactGridLayout
                  onLayoutChange={onLayoutChangeNew}
                  {...props}
                  isDraggable={drag && edit && checkRule("edit")}
                  isResizable={drag && edit && checkRule("edit")}
                  rowHeight={10}
                  margin={[5, 5]}
                >
                  {_.map(items, (el) => createElement(el, "center"))}
                </ReactGridLayout>
              </div>
              <div
                className="right-fixed"
                style={{
                  display: `${itemsRight.length === 0 ? "none" : ""}`,
                  height: `calc(100vh - ${
                    itemsTop.length === 0 ? "0" : "300px"
                  })`,
                }}
              >
                {itemsRight.length === 0 ? (
                  <div
                    style={{
                      width: "100%",
                      minHeight: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  ></div>
                ) : (
                  <>
                    {" "}
                    <ReactGridLayout
                      // isBounded={true}
                      onLayoutChange={onLayoutChangeNewCenterRight}
                      {...props}
                      isDraggable={drag && edit && checkRule("edit")}
                      isResizable={drag && edit && checkRule("edit")}
                      rowHeight={10}
                      cols={1}
                      margin={[5, 5]}
                    >
                      {_.map(itemsRight, (el) => createElement(el, "right"))}
                    </ReactGridLayout>{" "}
                    <div
                      style={{
                        width: "100%",
                        minHeight: "200px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    ></div>
                  </>
                )}
              </div>
            </div>
          </div>
        )}
      </Wrapper>
    </>
  );
};

export default CenterScreen;

const Wrapper = styled.div`
  /* padding-top: 10px; */
  position: relative;
  height: 100%;
  ::-webkit-scrollbar {
    width: 10px;
  }
  .ant-tabs-nav {
    margin-bottom: 0 !important;
  }
  .top-fixed {
    /* border: 2px dashed ${(props) => props.theme.main}; */
    border-radius: 10px;
    min-height: 200px;
    height: fit-content;
    width: 100%;
  }
  .combine-layout {
    display: flex;
    width: 100%;
    .left-fixed {
      width: 15%;
      /* border: 2px dashed ${(props) => props.theme.main}; */
      border-radius: 10px;
      /* min-height: 100vh; */
      /* height: calc(100vh - 300px); */
      overflow-y: scroll;
    }
    .middle-fixed {
      /* height: calc(100vh - 300px); */
      overflow: hidden;
      overflow-y: scroll;
    }
    .right-fixed {
      width: 15%;
      border-radius: 10px;
      /* min-height: 100vh; */
      /* height: calc(100vh - 300px); */
      overflow-y: scroll;
    }
  }

  .ant-tabs-nav {
    height: 36px;
  }
  .fixed-btn {
    position: fixed;
    right: -11px;
    background-color: ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    z-index: 10;
    display: flex;
    justify-content: center;
    align-items: center;
    border-top-left-radius: 60px;
    border-bottom-left-radius: 60px;
    top: 290px;
    img {
      width: 15px;
      margin-right: 8px;
    }
    &:hover {
      background-color: ${(props) => props.theme.darker} !important;
      border-color: ${(props) => props.theme.darker};
      color: #fff;
      /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
    }
  }
  .no-data-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    z-index: 10;
    width: 100%;
    margin-top: 136px;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      margin-bottom: 16px;
    }
  }

  .no-data-container-tab {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    z-index: 10;
    width: 100%;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      margin-bottom: 16px;
    }
  }

  .components {
    padding: 10px;
    border: 1px solid #d9d9d9;
    box-sizing: border-box;
    border-radius: 5px;
    .active {
      border: 1px solid ${(props) => props.theme.main};
    }
  }
  .components-title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;
    margin-top: -25px;
    background-color: white;
    /* Character/Color text main */
    width: fit-content;
    padding-left: 10px;
    padding-right: 10px;
    color: #2c2c2c;
    margin-bottom: 10px;
  }
  .component-item {
    width: 100%;
    background: #ffffff;
    border: 1px solid #ececec;
    box-sizing: border-box;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      align-items: center;
      color: #2c2c2c;
    }
    .decs {
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      display: flex;
      align-items: center;
      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
  .react-grid-layout {
    background-color: transparent;
  }
  .react-grid-item:not(.react-grid-placeholder) {
    background: #fff;
    border: 1px solid #d9d9d9;
  }
  .close-png {
    :hover {
      cursor: pointer;
    }
  }
`;

const CustomItem = styled.div`
  .ant-tabs > .ant-tabs-nav,
  .ant-tabs > div > .ant-tabs-nav {
    justify-content: unset;
  }
  .ant-tabs > div > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
  }
  .ant-tabs > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
    padding-left: 10px;
  }
  .ant-tabs-top > .ant-tabs-nav::before {
    /* display: none; */
    display: block;
  }
  .ant-tabs-tab {
    position: relative;
    display: inline-flex;
    align-items: center;
    padding: 12px 0;
    font-size: 14px;
    background: 0 0;
    border: 0;
    outline: none;
    cursor: pointer;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;

    /* Character/Color text main */

    color: #2c2c2c;
  }
  .ant-tabs-nav-list {
    position: relative;
    display: flex;
    transition: transform 0.3s;
    border: 0 !important;
    padding: 4px 8px !important;
  }
  .ant-tabs-ink-bar {
    display: unset !important;
    background: ${(props) => props.theme.main} !important;
  }
  .ant-tabs-tab-active {
    background: #fff !important;
    .ant-tabs-tab-btn {
      color: ${(props) => props.theme.main} !important;
    }
  }
`;

const WrapComponent = styled.div`
  overflow: hidden;
  height: 100%;
  background: #fff;
  border-radius: 8px;
  box-shadow: 0px 0px 6px 0px rgba(32, 28, 28, 0.08);
  padding: 4px;
`;
