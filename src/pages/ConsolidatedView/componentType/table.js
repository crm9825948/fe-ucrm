import {
  CaretDownOutlined,
  CaretUpOutlined,
  EllipsisOutlined,
} from "@ant-design/icons";
import {
  Button,
  Dropdown,
  Form,
  Input,
  Menu,
  Pagination,
  Row,
  Tooltip,
  Typography,
} from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import fiterIcon from "assets/icons/common/filter.png";
import editImg from "assets/icons/common/icon-edit.png";
import Details from "assets/icons/users/details.svg";
import axios from "axios";
import { Notification } from "components/Notification/Noti";
import { BASENAME, BASE_URL_API } from "constants/constants";
import moment from "moment";
import ModalComponent from "pages/ConsolidatedViewSettings/ComponentSettings/modalComponent";
import CustomView from "pages/Objects/customView/customView";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadUserDynamicButton } from "redux/slices/dynamicButton";
import { loadRelatedObject } from "redux/slices/fieldsManagement";
import {
  deleteRecord,
  loadLinkingFieldValue,
  loadListObjectField,
  loadRecordData,
  loadSourceObjects,
  loadTargetObjects,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalRecord from "../modalRecord";
import ModalDelete from "./delete/modalConfirmDelete";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import { checkTokenExpiration } from "contexts/TokenCheck";

const { Text: TextComponent } = Typography;

const TableComponent = (props) => {
  const {
    recordID: record_id,
    data,
    objectId,
    edit: editProp,
    onRemoveItem,
  } = props;
  const {
    linkingFieldValue,
    hiddenArray,
    // isLoading,
    // recordData,
    // listObjectField,
  } = useSelector((state) => state.objectsReducer);
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const [listHeaders, setListHeaders] = useState([]);
  const [listData, setListData] = useState([]);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [sortBy, setSortBy] = useState({});
  const [searchList, setSearchList] = useState({});
  const [totalRecord, setTotalRecord] = useState(0);
  const dispatch = useDispatch();
  const [recordID, setRecordID] = useState("");
  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [form] = Form.useForm();
  const [fields, setFields] = useState([]);
  const [allFields, setAllFields] = useState({});
  /*eslint-disable-next-line*/
  const [listObjectField, setListObjectField] = useState([]);
  /*eslint-disable-next-line*/
  const [write, setWrite] = useState(false);
  /*eslint-disable-next-line*/
  const [read, setRead] = useState(false);
  /* eslint-disable */
  const [update, setUpdate] = useState(false);
  const [deleteRole, setDeleteRole] = useState(false);
  const [filterID, setFilterID] = useState("");
  const [openDelete, setOpenDelete] = useState(false);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [componentID, setComponentID] = useState("");
  const [, setDataItem] = useState({});
  const [openCustomView, setOpenCustomView] = useState(false);
  const [right, setRight] = useState("accepted");
  const loadData = useCallback(async () => {
    let dataPost = filterID
      ? {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          custom_view_id: filterID,
          search_with: {
            meta: [],
            data: [],
          },
          // sort_by: sortBy,
        }
      : {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: [],
          },
          // sort_by: sortBy,
        };
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        if (Array.isArray(res.data.data)) {
          Notification("error", "You do not have right to see this data!");
          setRight("noAccepted");
        } else {
          setRight("accepted");
          setListHeaders(res.data.data.fields);
          setListData(res.data.data.record_data);
        }
      })
      .catch((err) => {});

    //pagination
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setTotalRecord(res.data.data.total_record);
        // setTotalPage(res.data.data.total_page);
      })
      .catch((err) => {});
  }, [currentPage, recordPerPage, record_id, filterID]);

  const handleSearch = async () => {
    // setLoading(true);
    setCurrentPage(1);
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setListHeaders(res.data.data.fields);
        setListData(res.data.data.record_data);
      })
      .catch((err) => {});

    //pagination
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: true,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setTotalRecord(res.data.data.total_record);
        // setTotalPage(res.data.data.total_page);
      })
      .catch((err) => {});
  };

  const handleSort = async () => {
    const isTokenValid = await checkTokenExpiration();
    // setLoading(true);
    setCurrentPage(1);
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setListHeaders(res.data.data.fields);
        setListData(res.data.data.record_data);
      })
      .catch((err) => {});

    //pagination
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: true,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setTotalRecord(res.data.data.total_record);
        // setTotalPage(res.data.data.total_page);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    loadData();
  }, [loadData]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: data.related_object,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let tempField = [];
          let tempFieldHidden = [];
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sections);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              if (hiddenArray.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFields(tempFieldHidden);
        })
        .catch((err) => {});
    };
    if (data?.related_object) {
      checkToken();
    }
    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    form.setFieldsValue(linkingFieldValue);
    // eslint-disable-next-line
  }, [linkingFieldValue]);

  useEffect(() => {
    setRecordID(record_id);
  }, [record_id]);

  useEffect(() => {
    if (openModalRecord) {
      dispatch(
        loadLinkingFieldValue({
          object_related: objectId,
          record_id: record_id,
          object_create_record: data.related_object,
        })
      );
    }
    // eslint-disable-next-line
  }, [objectId, data, openModalRecord]);

  useEffect(() => {
    dispatch(
      loadUserDynamicButton({
        object_id: objectId,
        record_id: record_id,
      })
    );
  }, [dispatch, objectId, record_id]);

  const tempEdit = (record, allFields) => {
    let tempObject = {};
    /*eslint-disable-next-line*/
    Object.entries(record !== null && record).forEach(([key, value], index) => {
      // let tempObject = {};
      if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "datetime-local" &&
        value !== null
      ) {
        tempObject[key] = value.value
          ? moment(value.value, "YYYY-MM-DD HH:mm:ss", true)
          : null;
      } else if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "date" &&
        value !== null
      ) {
        tempObject[key] = value.value
          ? moment(value.value, "YYYY-MM-DD")
          : null;
      } else if (typeof value === "object" && value !== null) {
        tempObject[key] = value.value;
      } else {
        tempObject[key] = value;
      }
    });

    // form.resetFields();
    form.setFieldsValue(tempObject);
    setOpenModalRecord(true);
  };

  const edit = async (record, allFields) => {
    //hihi
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: data.related_object,
          show_meta_fields: true,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setListObjectField(res.data.data);
        let newObj = {};
        res.data.data.length > 0 &&
          res.data.data[res.data.data.length - 1]["main_object"][
            "sections"
            /*eslint-disable-next-line*/
          ].map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((item, idx) => {
              newObj[item.ID] = { ...item };
              newObj[item.ID].name = "";
            });
          });

        setAllFields(newObj);
        let flagEdit = false;

        res.data.data.map((item, idx) => {
          if (Object.keys(item)[0] === "main_object") {
            // setWrite(item[Object.keys(item)[0]]?.create_permission);
            // setRead(item[Object.keys(item)[0]]?.read_permission);
            // setUpdate(item[Object.keys(item)[0]]?.edit_permission);
            // setDeleteRole(item[Object.keys(item)[0]]?.delete_permission);
            flagEdit = item[Object.keys(item)[0]]?.edit_permission;
          }
        });
        if (flagEdit) {
          tempEdit(record, newObj);
        } else {
          Notification("error", "Bạn không có quyền để chỉnh sửa bản ghi này!");
        }
      })
      .catch((err) => {});
  };

  const [, setWidth] = useState(0);

  useEffect(() => {
    setWidth(
      document.getElementById(data._id) &&
        document.getElementById(data._id).clientWidth
    );
    // eslint-disable-next-line
  }, [
    // eslint-disable-next-line
    document.getElementById(data._id) &&
      document.getElementById(data._id).clientWidth,
  ]);

  // useEffect(() => {
  //   if (filterID !== "") {
  //     let searchData = [];

  //     /* eslint-disable-next-line */
  //     Object.entries(searchList).forEach(([key, value], index) => {
  //       if (value) {
  //         let newItem = {
  //           id_field: key,
  //           value: value,
  //         };
  //         searchData.push(newItem);
  //       }
  //     });

  //     //   initData(currentPage, recordPerPage, searchData, sortBy, filterID);
  //     loadData();
  //   }
  //   /* eslint-disable-next-line */
  // }, [filterID]);

  useEffect(() => {
    if (isModalVisible) {
      dispatch(
        loadRelatedObject({
          object_id: objectId,
        })
      );
      dispatch(
        loadSourceObjects({
          object_id: objectId,
        })
      );
      dispatch(
        loadTargetObjects({
          object_id: objectId,
        })
      );
    }
  }, [objectId, isModalVisible, dispatch]);

  const numberWithCommas = (x, item) => {
    if (x === 0) {
      return "0";
    } else
      x = x.toFixed(
        item?.decimal_separator === 0 ? 0 : item?.decimal_separator || 3
      );
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  return (
    <Wrapper>
      <div
        style={{
          height: "36px",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : "fixed"
          }`,
          top: 0,
          left: 0,
          zIndex: 9,
          boxShadow: "inset 0px -1px 0px #F0F0F0",
          marginBottom: "16px",
        }}
      >
        <Row
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            {data.name}{" "}
            {right === "noAccepted" ? (
              ""
            ) : (
              <Tooltip title="Add new view">
                <button
                  className="button-filter"
                  style={{
                    marginLeft: "10px",
                    textAlign: "center",
                  }}
                  onClick={() => {
                    setOpenCustomView(true);
                  }}
                >
                  <img
                    alt=""
                    src={fiterIcon}
                    style={{ width: "12px", textAlign: "center" }}
                  />
                </button>
              </Tooltip>
            )}
          </div>
          {/* <Col span={12}> </Col> */}
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            {/* <CustomButton
              shape="circle"
              onClick={() => {
                setOpenCustomView(true);
              }}
            >
              -
            </CustomButton> */}
            <Tooltip title="Add new record">
              {right === "noAccepted" ? (
                ""
              ) : (
                <CustomButton
                  shape="circle"
                  onClick={() => {
                    dispatch(
                      loadListObjectField({
                        api_version: "2",
                        object_id: data.related_object,
                        show_meta_fields: true,
                      })
                    );
                    setRecordID("");
                    setOpenModalRecord(true);
                  }}
                >
                  +
                </CustomButton>
              )}
            </Tooltip>
            {right === "noAccepted" ? (
              ""
            ) : (
              <CustomPagination
                showQuickJumper
                current={currentPage}
                defaultPageSize={recordPerPage}
                total={totalRecord}
                showSizeChanger
                showTotal={(total, range) =>
                  `${range[0]}-${range[1]} of ${total} records`
                }
                onChange={(e, pageSize) => {
                  setCurrentPage(e);
                  setRecordPerPage(pageSize);
                }}
              />
            )}

            {editProp ? (
              <Dropdown
                trigger="click"
                overlay={
                  <Menu>
                    {window.location.pathname.includes(
                      "list-view-with-details"
                    ) ? (
                      ""
                    ) : (
                      <Menu.Item
                        onClick={() => {
                          dispatch(
                            loadListObjectField({
                              api_version: "2",
                              object_id: data.related_object,
                              show_meta_fields: true,
                            })
                          );
                          setIsModalVisible(true);
                          setComponentID(data._id);
                        }}
                      >
                        Edit
                      </Menu.Item>
                    )}
                    <Menu.Item
                      onClick={() => onRemoveItem(JSON.stringify(data))}
                    >
                      Remove
                    </Menu.Item>
                  </Menu>
                }
              >
                <Button style={{ marginLeft: "8px" }}>
                  <EllipsisOutlined />
                </Button>
              </Dropdown>
            ) : (
              ""
            )}
          </div>
          {/* <Col
            span={12}
            style={{
              textAlign: "right",
              display: `${width > 520 ? "" : "none"}`,
            }}
          >
           
          </Col> */}
        </Row>
      </div>
      <div
        className="table-wrapper"
        style={{
          background: "white",
          padding: "10px",
          height: "calc(100vh - 300px)",
        }}
      >
        {" "}
        <table className="table">
          <thead className="table-header">
            <tr>
              {listHeaders &&
                listHeaders.map((item, idx) => {
                  return (
                    <th>
                      <span className="title">{item.name}</span>
                      <span
                        className="sort-item"
                        style={{ display: "flex", flexDirection: "column" }}
                      >
                        <CaretUpOutlined
                          style={{
                            color: `${
                              sortBy[item.ID] === -1
                                ? defaultBrandName.theme_color
                                : "#D9D9D9"
                            }`,
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            let sortTmp = { ...sortBy };
                            if (sortTmp[item.ID] === -1) {
                              delete sortTmp[item.ID];
                            } else {
                              sortTmp[item.ID] = -1;
                            }
                            handleSort();
                            setSortBy(sortTmp);
                          }}
                        />
                        <CaretDownOutlined
                          style={{
                            color: `${
                              sortBy[item.ID] === 1
                                ? defaultBrandName.theme_color
                                : "#D9D9D9"
                            }`,
                            cursor: "pointer",
                          }}
                          onClick={() => {
                            let sortTmp = { ...sortBy };
                            if (sortTmp[item.ID] === 1) {
                              delete sortTmp[item.ID];
                            } else {
                              sortTmp[item.ID] = 1;
                            }
                            handleSort();
                            setSortBy(sortTmp);
                          }}
                        />
                      </span>
                    </th>
                  );
                })}
              {right === "noAccepted" ? (
                ""
              ) : (
                <th rowSpan={2} style={{ zIndex: 99999, textAlign: "center" }}>
                  Action
                </th>
              )}
            </tr>
            <tr>
              {listHeaders &&
                listHeaders.map((item, idx) => {
                  return (
                    <td>
                      <CustomSearch
                        onChange={(e) => {
                          let temp = { ...searchList };
                          temp[item.ID] = e.target.value;
                          setSearchList(temp);
                        }}
                        onPressEnter={() => {
                          handleSearch();
                        }}
                      />
                    </td>
                  );
                })}
              {/* <td></td> */}
            </tr>
          </thead>
          <tbody className="table-body">
            {right === "noAccepted"
              ? "You do not have right to see this data!"
              : ""}
            {listData &&
              listData.data &&
              listData.data.map((item, idx) => {
                return (
                  <tr>
                    {listHeaders &&
                      listHeaders.map((ele, idx) => {
                        return (
                          <td>
                            <TextComponent
                              ellipsis={{
                                tooltip:
                                  ele.type !== "file"
                                    ? item[ele.ID]?.value
                                    : "",
                              }}
                              style={{
                                width: "100%",
                                textAlign: `${
                                  typeof item[ele.ID]?.value === "number"
                                    ? "right"
                                    : "left"
                                }`,
                              }}
                            >
                              {ele.type === "number" &&
                              item[ele.ID]?.value !== null ? (
                                numberWithCommas(item[ele.ID].value, ele)
                              ) : ele.type === "file" ? (
                                item[ele.ID]?.value !== "" &&
                                item[ele.ID]?.value !== null ? (
                                  item[ele.ID]?.value.map((file, idx) => {
                                    let fileArray = file.split("/");
                                    return (
                                      <a
                                        style={{ display: "block" }}
                                        href={file}
                                        target={"_blank"}
                                        rel="noreferrer"
                                      >
                                        {fileArray[fileArray.length - 1]}
                                      </a>
                                    );
                                  })
                                ) : (
                                  ""
                                )
                              ) : ele.type === "select" &&
                                typeof item[ele.ID]?.value === "object" ? (
                                <div>
                                  {item[ele.ID]?.value.map((item, idx) => {
                                    return (
                                      <div
                                        style={{
                                          background: "rgb(243 239 239)",
                                          marginRight: "5px",
                                          padding: "2px",
                                          borderRadius: "5px",
                                          float: "left",
                                          marginBottom: "5px",
                                        }}
                                      >
                                        {item}
                                      </div>
                                    );
                                  })}
                                </div>
                              ) : (
                                item[ele.ID]?.value
                              )}
                            </TextComponent>
                          </td>
                        );
                      })}
                    <td>
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "space-around",
                        }}
                      >
                        <Tooltip title={"Consolidated view"}>
                          <a
                            rel="noreferrer"
                            target="_blank"
                            href={
                              BASENAME +
                              `consolidated-view/${data.related_object}/${item._id}`
                            }
                          >
                            <img alt="" src={Details} />
                          </a>
                        </Tooltip>
                        <Tooltip title={"Edit"}>
                          <a href>
                            <img
                              alt=""
                              src={editImg}
                              style={{ width: "20px" }}
                              onClick={() => {
                                dispatch(
                                  loadListObjectField({
                                    api_version: "2",
                                    object_id: data.related_object,
                                    show_meta_fields: true,
                                  })
                                );
                                dispatch(
                                  loadRecordData({
                                    id: item._id,
                                    object_id: data.related_object,
                                  })
                                );
                                setRecordID(item._id);
                                edit(item, allFields);
                              }}
                            />
                          </a>
                        </Tooltip>
                        <Tooltip title={"Delete"}>
                          <a className="ant-dropdown-link" href>
                            <img
                              alt=""
                              src={deleteImg}
                              style={{ width: "20px" }}
                              onClick={() => {
                                //   if (deleteRole) {
                                setOpenDelete(true);
                                dispatch(
                                  loadRecordData({
                                    id: item._id,
                                    object_id: data.related_object,
                                  })
                                );
                                setRecordID(item._id);
                                //   }
                              }}
                            />
                          </a>
                        </Tooltip>
                      </div>
                    </td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>

      <ModalRecord
        open={openModalRecord}
        setOpen={setOpenModalRecord}
        initData={loadData}
        recordPerPage={recordPerPage}
        currentPage={currentPage}
        form={form}
        recordID={recordID}
        setRecordID={setRecordID}
        objectId={data.related_object}
        fields={fields}
        sortBy={sortBy}
      />

      <ModalDelete
        openConfirm={openDelete}
        setOpenConfirm={setOpenDelete}
        title={"bản ghi này"}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deleteRecord}
        dataDelete={{
          data: {
            id: recordID,
            object_id: data.related_object,
          },
          load: {
            object_id: data.related_object,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: [],
            },
          },
          type: window.location.pathname.includes("list-view-with-details")
            ? "reload"
            : "",
        }}
        loadData={loadData}
        setShowModalDelete={() => {}}
      />

      <ModalComponent
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        selectedObject={objectId}
        dataItem={data}
        componentID={componentID}
        setComponentID={setComponentID}
        setDataItem={setDataItem}
        initData={loadData}
      />

      <CustomView
        visible={openCustomView}
        setVisible={setOpenCustomView}
        objectId={data.related_object}
        mode={"consolidated-view"}
        filterID={filterID}
        setFilterID={setFilterID}
      />
      <ModalDuplicate />
    </Wrapper>
  );
};

export default TableComponent;

const Wrapper = styled.div`
  .button-filter {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    margin-right: 8px;
    border: 1px solid ${(props) => props.theme.main};
    :hover {
      background: ${(props) => props.theme.main};
      color: ${(props) => props.theme.white};
      border: 1px solid ${(props) => props.theme.main};
      cursor: pointer;
    }
  }

  width: 100%;
  /* height: calc(100% - 187px); */
  height: 100%;

  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  background: #fff;
  overflow-x: auto;
  overflow-y: auto;
  .table-wrapper {
    width: calc(100% - 32px);
    margin: auto;
    overflow-x: auto;
    overflow-y: auto;
  }
  table {
    /* width: 100%; */
    min-width: 100%;
    width: max-content;
    table-layout: auto;
    margin-top: 84px;
    thead {
      position: sticky;
      top: 54px;
      z-index: 3;
    }
    .table-header th {
      text-align: left;
      /* border: 1px solid #ddd;
       */
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      .title {
        float: left;
        margin-right: 10px;
      }
      .sort-item {
        width: fit-content;
        margin-left: 10px;
      }

      overflow: hidden;
      resize: horizontal;
      background: #f0f0f0;
      min-width: 100px;
      white-space: nowrap;
      padding: 8px;
      font-family: var(--roboto-500);
      font-size: 16px;
      color: #252424;
      font-weight: normal;
      &:last-child {
        position: sticky;
        z-index: 4;
        right: -10px;
        border-right: none;
        resize: none;
        width: 150px;
        max-width: 150px;
      }
    }
    .table-header td {
      text-align: left;
      /* border: 1px solid #ddd;
       */
      box-shadow: none;
      overflow: hidden;
      background: #f0f0f0;
      min-width: 100px;
      white-space: nowrap;
      padding: 8px;
      font-size: 16px;
      color: #252424;
      font-weight: normal;
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      &:last-child {
        position: sticky;
        z-index: 4;
        right: -10px;
        border-right: none;
        resize: none;
        width: 150px;
        max-width: 150px;
      }
    }

    .table-body td {
      background: #fff;
      border-bottom: 1px solid #ddd;
      padding: 16px;

      max-width: 200px;

      &:first-child {
        border-left: 1px solid #ddd;
        text-align: center;
      }
      &:last-child {
        /* border-right: 1px solid #ddd; */
        position: sticky;
        z-index: 2;
        right: -10px;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }
      img {
        margin-right: 8px;
      }
    }
  }
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  padding: 10px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;

const CustomSearch = styled(Input)`
  border-radius: 5px;
  &:hover,
  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
`;

const CustomPagination = styled(Pagination)``;

const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;
