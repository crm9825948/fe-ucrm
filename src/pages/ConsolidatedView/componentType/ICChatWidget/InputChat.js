import _ from "lodash";
import styled from "styled-components/macro";
import { useDispatch } from "react-redux";
import { useState } from "react";
import AudioReactRecorder, { RecordState } from "audio-react-recorder";
import data from "@emoji-mart/data";
import Picker from "@emoji-mart/react";
import { useStopwatch } from "react-timer-hook";
import axios from "axios";

import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";
import Upload from "antd/lib/upload";
import Modal from "antd/lib/modal";
import Popover from "antd/lib/popover";
import Button from "components/Button";

import {
  CloseOutlined,
  PauseOutlined,
  CaretRightFilled,
} from "@ant-design/icons";
import "./Input.scss";

import AttachFileIcon from "assets/icons/icChatWidget/AttachFile.svg";
import AttachAudioIcon from "assets/icons/icChatWidget/Audio.svg";
import EmojiIcon from "assets/icons/icChatWidget/Emoji.svg";
import AttachImageIcon from "assets/icons/icChatWidget/Image.svg";
import AttachMusicIcon from "assets/icons/icChatWidget/Music.svg";
import QuickMessageIcon from "assets/icons/icChatWidget/QuickMessage.svg";
import SendIcon from "assets/icons/icChatWidget/Send.svg";
import AttachVideoIcon from "assets/icons/icChatWidget/Video.svg";
import FileIcon from "assets/icons/icChatWidget/FileIcon.svg";
import AudioIcon from "assets/icons/icChatWidget/AudioIcon.svg";
import VideoIcon from "assets/icons/icChatWidget/VideoIcon.svg";

import {
  sendMessageIC,
  sendMessageWithFileIC,
  getQuickMessagesIC,
} from "redux/slices/icChatWidget";
import { setShowLoadingScreen } from "redux/slices/global";

import ModalQuickMessage from "./Modal/QuickMessage";
import { checkTokenExpirationIC } from "services/apiIC";

const { TextArea } = Input;

const actions = [
  {
    label: AttachFileIcon,
    value: "attachment",
    tooltip: "Attachment",
  },

  {
    label: AttachImageIcon,
    value: "images",
    tooltip: "Images",
  },
  {
    label: AttachVideoIcon,
    value: "videos",
    tooltip: "Videos",
  },
  {
    label: AttachMusicIcon,
    value: "audios",
    tooltip: "Audios",
  },
  {
    label: EmojiIcon,
    value: "emoji",
    tooltip: "Emoji",
  },
  {
    label: AttachAudioIcon,
    value: "audio-mic",
    tooltip: "Audio - mic",
  },
];

function InputChat({
  selectedMessage,
  quoteMessage,
  $quoteMessage,
  disabled,
  messageDetails,
}) {
  const dispatch = useDispatch();
  const { hours, seconds, minutes, start, pause, reset } = useStopwatch({
    autoStart: false,
  });

  const [previewVisible, $previewVisible] = useState(false);
  const [previewImage, $previewImage] = useState("");
  const [previewTitle, $previewTitle] = useState("");
  const [files, $files] = useState([]);
  const [inputMessage, $inputMessage] = useState("");
  const [showQuickMessage, $showQuickMessage] = useState(false);

  const [recordState, $recordState] = useState(null);
  const [showButtonRecord, $showButtonRecord] = useState(false);
  const [recordData, $recordData] = useState("");
  const [showRecord, $showRecord] = useState(false);

  const startRecord = () => {
    $recordState(RecordState.START);
    $showButtonRecord(true);
    reset();
  };

  const resumeRecord = () => {
    $recordState(RecordState.START);
    start();
  };

  const pauseRecord = () => {
    $recordState(RecordState.PAUSE);
    pause();
  };

  const stopRecord = () => {
    $recordState(RecordState.STOP);
    $showButtonRecord(false);
    pause();
  };

  const onStop = (audioData) => {
    $recordData(audioData);
  };

  const handlePreview = (file) => {
    return () => {
      $previewImage(_.get(file, "response.data.items[0].url", ""));
      $previewVisible(true);
      $previewTitle(_.get(file, "name", ""));
    };
  };

  const _onRemoveFile = (idx) => {
    return (e) => {
      e.stopPropagation();
      let tempFiles = [...files];
      tempFiles.splice(idx, 1);
      $files(tempFiles);
    };
  };

  const handleChange = ({ fileList: newFileList, file }) => {
    $files(newFileList);
    if (
      file.status === "done" ||
      file.status === "error" ||
      file.status === "success"
    ) {
      dispatch(setShowLoadingScreen(false));
    }
  };

  const handleCancel = () => $previewVisible(false);

  const onSelectEmoji = (value) => {
    $inputMessage(inputMessage + _.get(value, "native", ""));
  };

  const _onEditQuickMessage = (message) => {
    $inputMessage(_.get(message, "content", ""));

    if (_.get(message, "message_type", "") === "file") {
      $files([
        {
          type: _.get(message, "content_file_type", ""),
          name: _.get(message, "file_name", ""),
          response: {
            data: {
              items: [
                {
                  file_content: _.get(message, "content_type", ""),
                  url: _.get(message, "file_url", ""),
                  download_url: _.get(message, "download_url", ""),
                  size: _.get(message, "size", null),
                  thumbnail_url: _.get(message, "file_url", ""),
                },
              ],
            },
          },
        },
      ]);
    }
  };

  const onUploadRecord = async () => {
    $showRecord(false);
    const file = new File(
      [recordData?.blob],
      `audio-${Math.random() * 10000000000}.wav`
    );

    const formData = new FormData();
    formData.append("channel_type", _.get(selectedMessage, "channel_type", ""));
    formData.append("file", file);
    const token = await checkTokenExpirationIC();

    try {
      dispatch(setShowLoadingScreen(true));
      const res = await axios.post(
        localStorage.getItem("icIntegration_link_api") +
          "resource/api/custom-api/upload-file",
        formData,
        {
          headers: {
            Authorization: "Bearer " + token,
          },
        }
      );

      let tempFiles = [...files];
      tempFiles.push({
        name: _.get(res, "data.data.items[0].file_name"),
        response: {
          data: {
            items: [
              {
                ..._.get(res, "data.data.items[0]"),
                file_content: "audio",
              },
            ],
          },
        },
        uid: _.get(res, "data.data.items[0].uid", ""),
      });
      $files(tempFiles);
      dispatch(setShowLoadingScreen(false));
    } catch (error) {
      console.error(error);
      dispatch(setShowLoadingScreen(false));
    }
  };

  const props = {
    name: "file",
    multiple: true,
    showUploadList: false,
    action:
      localStorage.getItem("icIntegration_link_api") +
      "resource/api/custom-api/upload-file",
    headers: {
      Authorization:
        "Bearer " + localStorage.getItem("icIntegration_accessToken"),
    },
    data: {
      channel_type: _.get(selectedMessage, "channel_type", ""),
    },
    beforeUpload() {
      dispatch(setShowLoadingScreen(true));
    },
  };

  const formatTime = (time) => {
    return String(time).padStart(2, "0");
  };

  const disabledAction = (type) => {
    if (
      (_.get(selectedMessage, "channel_type", "") === "facebook_comment" ||
        _.get(selectedMessage, "channel_type", "") === "instagram_comment") &&
      (type === "attachment" ||
        type === "audio-mic" ||
        type === "videos" ||
        type === "audios")
    )
      return true;
    else return false;
  };

  const listActions = (
    <Actions>
      {_.map(actions, (action) => (
        <Tooltip title={action.tooltip}>
          {action.value === "emoji" ? (
            <Popover
              content={<Picker data={data} onEmojiSelect={onSelectEmoji} />}
              trigger="click"
              overlayClassName="popoverEmojiIC"
              placement="top"
            >
              <img
                src={action.label}
                alt={action.tooltip}
                style={{
                  width: "24px",
                  height: "24px",
                  cursor: "pointer",
                }}
              />
            </Popover>
          ) : action.value === "audio-mic" ? (
            <Popover
              content={
                <div>
                  <div
                    style={{ backgroundColor: "#c8c8c8", marginBottom: "8px" }}
                  >
                    <AudioReactRecorder
                      state={recordState}
                      onStop={onStop}
                      backgroundColor="#c8c8c8"
                    />
                  </div>

                  {showButtonRecord ? (
                    <div
                      style={{
                        display: "flex",
                        margin: "0 auto",
                        rowGap: "4px",
                        width: "fit-content",
                        flexDirection: "column",
                        alignItems:
                          _.isEqual(recordState, "stop") ||
                          _.isEqual(recordState, null)
                            ? "flex-start"
                            : "center",
                      }}
                    >
                      <div>
                        {hours > 0 && <>{formatTime(hours)}:</>}
                        {formatTime(minutes)}:{formatTime(seconds)}
                      </div>

                      <div style={{ display: "flex", columnGap: "8px" }}>
                        {_.isEqual(recordState, "pause") ? (
                          <div
                            onClick={resumeRecord}
                            style={{
                              background: "#f50057",
                              border: "1px solid #f50057",
                              width: "58px",
                              height: "58px",
                              borderRadius: "50%",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer",
                            }}
                          >
                            <CaretRightFilled
                              style={{ color: "#fff", fontSize: "24px" }}
                            />
                          </div>
                        ) : (
                          <div
                            onClick={pauseRecord}
                            style={{
                              background: "#f50057",
                              border: "1px solid #f50057",
                              width: "58px",
                              height: "58px",
                              borderRadius: "50%",
                              display: "flex",
                              justifyContent: "center",
                              alignItems: "center",
                              cursor: "pointer",
                            }}
                          >
                            <PauseOutlined
                              style={{ color: "#fff", fontSize: "24px" }}
                            />
                          </div>
                        )}
                        <div
                          onClick={stopRecord}
                          style={{
                            background: "#f50057",
                            border: "1px solid #f50057",
                            width: "58px",
                            height: "58px",
                            borderRadius: "50%",
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            cursor: "pointer",
                          }}
                        >
                          <div
                            style={{
                              backgroundColor: "#fff",
                              width: "20px",
                              height: "20px",
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  ) : (
                    <div style={{ display: "flex", flexDirection: "column" }}>
                      {recordData && (
                        <div
                          style={{
                            display: "flex",
                            justifyContent: "space-between",
                            alignItems: "center",
                            marginBottom: "8px",
                          }}
                        >
                          <audio controls src={_.get(recordData, "url")} />
                          <Button
                            type="primary"
                            title="Save"
                            onClick={onUploadRecord}
                          />
                        </div>
                      )}

                      <div
                        onClick={startRecord}
                        style={{
                          background: "#f50057",
                          border: "1px solid #f50057",
                          width: "58px",
                          height: "58px",
                          borderRadius: "50%",
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                          margin: "0 auto",
                          cursor: "pointer",
                        }}
                      >
                        <img
                          src={AttachAudioIcon}
                          alt=""
                          style={{ filter: "brightness(200)" }}
                        />
                      </div>
                    </div>
                  )}
                </div>
              }
              trigger="click"
              visible={showRecord}
              onVisibleChange={() => {
                if (disabledAction(action.value)) return;
                $showRecord(!showRecord);
              }}
            >
              <img
                src={action.label}
                alt={action.tooltip}
                style={{
                  width: "24px",
                  height: "24px",
                  cursor: disabledAction(action.value)
                    ? "not-allowed"
                    : "pointer",
                  filter: disabledAction(action.value)
                    ? "opacity(0.5)"
                    : "none",
                }}
              />
            </Popover>
          ) : (
            <Upload
              {...props}
              fileList={files}
              onChange={handleChange}
              accept={
                action.value === "images"
                  ? "image/*, image/apng,image/bmp,image/gif,image/jpeg,image/pjpeg,image/png,image/svg+xml,image/tiff,image/webp,image/x-icon, image/vnd.microsoft.icon"
                  : action.value === "videos"
                  ? "video/*, video/x-msvideo, video/mp4, video/mpeg, video/ogg, video/mp2t, video/webm, video/3gpp, video/3gpp2"
                  : action.value === "audios"
                  ? "audio/*, audio/aac, audio/midi audio/x-midi, audio/mpeg, audio/ogg, 	audio/opus, audio/wav, 	audio/webm, audio/3gpp, audio/3gpp2"
                  : "/*"
              }
              disabled={disabledAction(action.value)}
            >
              <img
                src={action.label}
                alt={action.tooltip}
                style={{
                  width: "24px",
                  height: "24px",
                  cursor: disabledAction(action.value)
                    ? "not-allowed"
                    : "pointer",
                  filter: disabledAction(action.value)
                    ? "opacity(0.5)"
                    : "none",
                }}
              />
            </Upload>
          )}
        </Tooltip>
      ))}
    </Actions>
  );

  const onInputChange = (e) => {
    $inputMessage(e.target.value);
  };

  const _onDeleteQuote = () => {
    $quoteMessage({
      owner: "",
      reply_content_file_name: null,
      reply_content_file_url: "",
      reply_create_by: "",
      reply_interaction_type: "",
      reply_location_lat: null,
      reply_location_long: null,
      reply_mess_text: "",
      reply_sender_type: "",
      reply_to_id: "",
    });
  };

  const _onOpenQuickMessage = () => {
    $showQuickMessage(true);
    dispatch(
      getQuickMessagesIC({
        sorts: [
          {
            name_field: "position",
            type_sort: "ASC",
          },
        ],
        search_list: [
          {
            conjunction: "AND",
            name_field: "channel_type",
            upper_bound: null,
            value_search: _.get(selectedMessage, "channel_type", ""),
          },
        ],
      })
    );
  };

  const _onSendMess = (e) => {
    if (!e.shiftKey) {
      e.preventDefault();
      if (files?.length > 0) {
        let payload = [];
        _.map(files, (file, idx) => {
          payload.push({
            interaction_id: _.get(selectedMessage, "interaction_id", ""),
            content_file_type: _.get(file, "type", ""),
            content_file_name: _.get(file, "name", ""),
            interaction_type:
              _.get(file, "response.data.items[0].file_content", "") !==
                "image" &&
              _.get(file, "response.data.items[0].file_content", "") !==
                "video" &&
              _.get(file, "response.data.items[0].file_content", "") !== "audio"
                ? "file"
                : _.get(file, "response.data.items[0].file_content", ""),
            content_file_url: _.get(file, "response.data.items[0].url", ""),
            content_download_url: _.get(
              file,
              "response.data.items[0].download_url",
              ""
            ),
            size:
              _.get(selectedMessage, "channel_type", "") === "viber"
                ? _.get(file, "size", 0)
                : null,
            duration: null,
            content_text:
              idx === files.length - 1 && inputMessage ? inputMessage : "",
            message_parent_id:
              _.get(selectedMessage, "channel_type", "") === "zalo" ||
              _.get(selectedMessage, "channel_type", "") ===
                "facebook_comment" ||
              _.get(selectedMessage, "channel_type", "") === "instagram_comment"
                ? messageDetails[messageDetails?.length - 1]?.message_id
                : "",
            // duration:
            //   "recordSelected?.channel_type === 'viber' ? element?.duration : null",
          });
        });
        dispatch(sendMessageWithFileIC(payload));
      } else {
        if (!inputMessage) return;

        if (_.get(quoteMessage, "reply_to_id")) {
          dispatch(
            sendMessageIC({
              interaction_type: "text",
              content_text: inputMessage,
              content_file_url: "",
              content_file_type: "",
              interaction_id: _.get(selectedMessage, "interaction_id", ""),
              message_parent_id: "",
              ...quoteMessage,
              owner: undefined,
              // message_parent_id:
              //     recordSelected?.channel_type === 'zalo' ||
              //     recordSelected?.channel_type === 'facebook_comment' ||
              //     recordSelected?.channel_type === 'instagram_comment'
              //         ? getMessageParentID(interactionDetail)
              //         : ''
            })
          );
        } else {
          dispatch(
            sendMessageIC({
              interaction_type: "text",
              content_text: inputMessage,
              content_file_url: "",
              content_file_type: "",
              interaction_id: _.get(selectedMessage, "interaction_id", ""),
              message_parent_id:
                _.get(selectedMessage, "channel_type", "") === "zalo" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "facebook_comment" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "instagram_comment"
                  ? messageDetails[messageDetails?.length - 1]?.message_id
                  : "",
            })
          );
        }
      }
      $inputMessage("");
      _onDeleteQuote();
      $files([]);
    }
  };

  return (
    <Wrapper disabled={disabled}>
      {listActions}

      {files?.length > 0 && (
        <Files>
          {_.map(files, (file, idx) => (
            <RenderFile onClick={handlePreview(file)}>
              <img
                src={
                  _.isEqual(
                    _.get(file, "response.data.items[0].file_content", ""),
                    "image"
                  )
                    ? _.get(file, "response.data.items[0].thumbnail_url", "")
                    : _.isEqual(
                        _.get(file, "response.data.items[0].file_content", ""),
                        "audio"
                      )
                    ? AudioIcon
                    : _.isEqual(
                        _.get(file, "response.data.items[0].file_content", ""),
                        "video"
                      )
                    ? VideoIcon
                    : FileIcon
                }
                alt=""
                style={{ width: "40px", height: "40px", marginRight: "8px" }}
              />
              {_.get(file, "name", "")}
              <CloseOutlined
                style={{ marginLeft: "8px" }}
                onClick={_onRemoveFile(idx)}
              />
            </RenderFile>
          ))}
        </Files>
      )}

      {!_.isEmpty(_.get(quoteMessage, "reply_to_id", "")) && (
        <QuoteMessage>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <p>{_.get(quoteMessage, "owner", "")}</p>
            {_.get(quoteMessage, "reply_content_file_name", "") && (
              <span>
                Shared a file:{" "}
                {_.get(quoteMessage, "reply_content_file_name", "")}
              </span>
            )}
            <span>{_.get(quoteMessage, "reply_mess_text", "")}</span>
          </div>

          <Tooltip title="Delete quote">
            <RemoveQuote onClick={_onDeleteQuote}>
              <CloseOutlined style={{ color: "#fff" }} />
            </RemoveQuote>
          </Tooltip>
        </QuoteMessage>
      )}

      <WrapInput>
        <TextArea
          autoSize={{ minRows: 1, maxRows: 5 }}
          placeholder="Type a message (Shift + Enter for a new line)"
          onPressEnter={_onSendMess}
          value={inputMessage}
          onChange={onInputChange}
        />

        <Tooltip title="Send">
          <SendButton onClick={_onSendMess}>
            <img src={SendIcon} alt="Send" />
          </SendButton>
        </Tooltip>

        <Tooltip title="Quick message (Tips: Type #shortcuts to search response template quickly)">
          <QuickMessage onClick={_onOpenQuickMessage}>
            <img src={QuickMessageIcon} alt="Quick" />
          </QuickMessage>
        </Tooltip>
      </WrapInput>

      <ModalQuickMessage
        showQuickMessage={showQuickMessage}
        $showQuickMessage={$showQuickMessage}
        selectedMessage={selectedMessage}
        _onEditQuickMessage={_onEditQuickMessage}
        listTo={[]}
        listCc={[]}
        messageDetails={[]}
      />

      <Modal
        visible={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img
          alt="example"
          style={{
            width: "100%",
          }}
          src={previewImage}
        />
      </Modal>
    </Wrapper>
  );
}

export default InputChat;

const Wrapper = styled.div`
  background: #fff;
  padding: 12px 16px;
  border-radius: 8px;
  position: absolute;
  bottom: 16px;
  width: calc(100% - 58px);
  pointer-events: ${({ disabled }) => (disabled ? "none" : "unset")};
  z-index: 3;

  textarea {
    resize: none;
    padding-left: 0;
    padding-right: 0;
    border: none;
    border-bottom: 1px solid #e4e8eb;
    font-size: 16px;
    border-color: #e4e8eb !important;
    box-shadow: none !important;
    align-self: flex-end;
  }
`;

const Actions = styled.div`
  display: flex;
  column-gap: 24px;
`;

const QuoteMessage = styled.div`
  background: #eee;
  border-radius: 10px;
  padding: 8px 8px 8px 17px;
  margin-top: 16px;
  margin-bottom: 8px;
  position: relative;
  display: flex;
  justify-content: space-between;

  p {
    margin-bottom: 4px;
    font-size: 14px;
    color: #6b6b6b;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  span {
    color: #2c2c2c;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  &:after {
    content: "";
    position: absolute;
    top: 8px;
    left: 8px;
    width: 3px;
    height: calc(100% - 16px);
    background: #bfbfbf;
    border-radius: 10px;
  }
`;

const RemoveQuote = styled.div`
  min-width: 20px;
  height: 20px;
  background: #acacac;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  margin-left: 4px;
`;

const WrapInput = styled.div`
  display: flex;
`;

const SendButton = styled.div`
  background: #1976d2;
  border-radius: 10px;
  min-width: 50px;
  height: 50px;
  margin-right: 16px;
  margin-left: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const QuickMessage = styled(SendButton)`
  margin-right: 0;
  margin-left: 0;
  background: #eee;
`;

const Files = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 8px;
  row-gap: 8px;
  margin-top: 8px;
`;

const RenderFile = styled.div`
  background: #f0f0f0;
  border-radius: 8px;
  padding: 8px 5px;
  width: fit-content;
  cursor: pointer;
`;
