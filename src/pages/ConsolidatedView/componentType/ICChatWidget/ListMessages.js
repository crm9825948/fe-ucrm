import { useState } from "react";
import styled from "styled-components/macro";
import _ from "lodash";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";

import Tooltip from "antd/lib/tooltip";

import Viber from "assets/icons/timeline/viber.svg";
import Webchat from "assets/icons/timeline/webchat.svg";
import Zalo from "assets/icons/timeline/zalo.svg";
import FacebookMessage from "assets/icons/timeline/facebook_message.svg";
import FacebookComment from "assets/icons/timeline/facebook_comment.svg";
import InstagramMessage from "assets/icons/timeline/instagram_message.svg";
import InstagramComment from "assets/icons/timeline/instagram_comment.svg";
import Telegram from "assets/icons/timeline/telegram.svg";
import ThirdPartyChannel from "assets/icons/timeline/third_party_channel.svg";
import Whatsapp from "assets/icons/timeline/whatsapp.svg";
import IconAddTag from "assets/icons/icChatWidget/AddTag.svg";
import ChatBot from "assets/icons/icChatWidget/ChatBot2.gif";
import Email from "assets/icons/timeline/Email.svg";

import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import ModalAddTag from "./Modal/AddTag";

import {
  deleteTagIC,
  $addTag,
  $selectedMessage,
  $isActive,
  $currentPageIC,
  getIndexMessageICResult,
  $recordPerPageIC,
  searchMessagesICResult,
  getMessageDetailsICResult,
} from "redux/slices/icChatWidget";

import { channelTypeIC } from "util/staticData";

const iconIC = (key) => {
  switch (key) {
    case "facebook_message":
      return FacebookMessage;
    case "facebook_comment":
      return FacebookComment;
    case "instagram_message":
      return InstagramMessage;
    case "instagram_comment":
      return InstagramComment;
    case "zalo":
      return Zalo;
    case "viber":
      return Viber;
    case "email":
      return Email;
    case "telegram":
      return Telegram;
    case "whatsapp":
      return Whatsapp;
    case "webchat":
      return Webchat;
    case "third_party_channel":
      return ThirdPartyChannel;
    default:
      return "";
  }
};

function ListMessages({
  isAssigned,
  assignedMessages,
  completedMessages,
  $isSearch,
  $showDetails,
  isCollapse,
}) {
  const dispatch = useDispatch();

  const { isActive } = useSelector((state) => state.icChatWidgetReducer);

  const [showConfirmDelete, $showConfirmDelete] = useState(false);
  const [dataDelete, $dataDelete] = useState({});

  const _onSelectChat = (idx, mes) => {
    return () => {
      if (isCollapse) {
        $showDetails(true);
        dispatch($selectedMessage(mes));
        dispatch($currentPageIC(1));
        dispatch(getIndexMessageICResult(null));
        dispatch($recordPerPageIC(null));
        dispatch(searchMessagesICResult([]));
        $isSearch(false);
      } else {
        if (isActive !== idx) {
          dispatch(
            getMessageDetailsICResult({
              data: [],
              page: 1,
            })
          );
          dispatch($isActive(idx));
          dispatch($selectedMessage(mes));
          dispatch($currentPageIC(1));
          dispatch(getIndexMessageICResult(null));
          dispatch($recordPerPageIC(null));
          dispatch(searchMessagesICResult([]));
          $isSearch(false);
        }
      }
    };
  };

  const _onAddTag = (interaction_id) => {
    return (e) => {
      e.stopPropagation();
      dispatch(
        $addTag({
          visible: true,
          interaction_id: interaction_id,
        })
      );
    };
  };

  const _onDeleteTag = (interaction_id, interaction_tag_id) => {
    return (e) => {
      e.stopPropagation();
      $dataDelete({
        interaction_id: interaction_id,
        interaction_tag_id: interaction_tag_id,
        interactiondetail_id: null,
      });
      $showConfirmDelete(true);
    };
  };

  const message = (mes, idx) => (
    <Message
      active={_.isEqual(isActive, idx)}
      onClick={_onSelectChat(idx, mes)}
      key={_.get(mes, "interaction_id", idx)}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "space-between",
          marginBottom: "6px",
        }}
      >
        <div style={{ display: "flex", alignItems: "center" }}>
          <img
            src={iconIC(_.get(mes, "channel_type", ""))}
            alt=""
            style={{ marginRight: "6px", width: "24px", height: "24px" }}
          />
          <Title
            isUnseen={
              _.isEqual(_.get(mes, "sender_type", ""), "customer") &&
              !_.isEqual(_.get(mes, "interaction_state", ""), "seen")
            }
          >
            {_.get(
              channelTypeIC.find(
                (item) => item.value === _.get(mes, "channel_type", "")
              ),
              "label"
            )}
          </Title>

          {!_.isEmpty(_.get(mes, "owner_username", "")) && (
            <Owner
              isUnseen={
                _.isEqual(_.get(mes, "sender_type", ""), "customer") &&
                !_.isEqual(_.get(mes, "interaction_state", ""), "seen")
              }
            >
              Owner: {_.get(mes, "owner_username", "")}
            </Owner>
          )}
        </div>

        {!_.isEmpty(_.get(mes, "create_time", "")) && (
          <Time
            isUnseen={
              _.isEqual(_.get(mes, "sender_type", ""), "customer") &&
              !_.isEqual(_.get(mes, "interaction_state", ""), "seen")
            }
          >
            {moment(_.get(mes, "create_time", "")).fromNow()}
          </Time>
        )}
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <Content
          isUnseen={
            _.isEqual(_.get(mes, "sender_type", ""), "customer") &&
            !_.isEqual(_.get(mes, "interaction_state", ""), "seen")
          }
        >
          {_.get(mes, "interaction_type", "") !== "text" ? (
            <>
              {_.get(mes, "content_text", "")
                ? _.get(mes, "content_text", "")
                : _.get(mes, "content_file_name", "")}
            </>
          ) : (
            <>{_.get(mes, "content_text", "")}</>
          )}
        </Content>
        {/* <UnseenNumber>9+</UnseenNumber> */}
      </div>

      <Tags>
        <div
          style={{
            width: "calc(100% - 46px)",
            display: "flex",
            alignItems: "center",
            flexWrap: "wrap",
            rowGap: "8px",
          }}
        >
          {_.map(_.get(mes, "interactionTags", []), (tag, indexTag) => (
            <Tag
              active={_.isEqual(isActive, idx)}
              key={_.get(tag, "id", indexTag)}
            >
              {_.get(tag, "name", "")}

              <DeleteTag
                className="delete-tag"
                onClick={_onDeleteTag(
                  _.get(mes, "interaction_id", ""),
                  _.get(tag, "id", "")
                )}
              >
                x
              </DeleteTag>
            </Tag>
          ))}
          <Tooltip title="Add tag">
            <img
              src={IconAddTag}
              alt="add"
              style={{ cursor: "pointer" }}
              onClick={_onAddTag(_.get(mes, "interaction_id", ""))}
            />
          </Tooltip>
        </div>

        {_.get(mes, "is_botprogress", false) && (
          <div
            style={{
              background: "#fff",
              width: "fit-content",
              height: "fit-content",
              borderRadius: "50%",
            }}
          >
            <Tooltip title="Chatbot inprogress">
              <img
                src={ChatBot}
                alt="chatbot"
                style={{ width: "30px", height: "30px" }}
              />
            </Tooltip>
          </div>
        )}
      </Tags>
    </Message>
  );

  return (
    <Wrapper>
      {_.map(isAssigned ? assignedMessages : completedMessages, (mes, idx) =>
        message(mes, idx)
      )}

      <ModalConfirmDelete
        title={""}
        decs="After deleting data will not be undone"
        methodDelete={deleteTagIC}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={showConfirmDelete}
        setOpenConfirm={$showConfirmDelete}
      />

      <ModalAddTag />
    </Wrapper>
  );
}

export default ListMessages;

const Wrapper = styled.div`
  overflow: auto;
  height: calc(100% - 48px);
  padding-right: 8px;

  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    background: #d9d9d9;
  }

  ::-webkit-scrollbar-thumb {
    background: #bfbfbf;
  }

  .delete-tag {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }
`;

const Message = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  padding: 8px;
  background: ${({ active }) => (active ? "rgba(32, 162, 162, 0.16)" : "#fff")};
  border-radius: 10px;
  margin-bottom: 15px;

  &:hover {
    background: #f2f4f5;
    cursor: pointer;
  }
`;

const Title = styled.div`
  margin-right: 6px;
  color: ${({ isUnseen }) => (isUnseen ? "#ff0000" : "#252424")};
`;

const Owner = styled.div`
  font-size: 12px;
  color: #637381;
  color: ${({ isUnseen }) => (isUnseen ? "#ff0000" : "#252424")};
`;

const Time = styled.div`
  font-size: 12px;
  color: #637381;
  color: ${({ isUnseen }) => (isUnseen ? "#ff0000" : "#252424")};
`;

const Content = styled.div`
  margin-bottom: 9px;
  color: ${({ isUnseen }) => (isUnseen ? "#ff0000" : "#252424")};
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  margin-right: 6px;
  width: calc(100% - 46px);
`;

// const UnseenNumber = styled.div`
//   width: 24px;
//   height: 20px;
//   padding: 3px;
//   background: #ee0707;
//   border-radius: 20px;
//   color: #fff;
//   display: flex;
//   justify-content: center;
//   align-items: center;
// `;

const Tags = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`;

const Tag = styled.div`
  background: ${({ active }) => (active ? "#6181FF" : "#E2E8FF")};
  color: ${({ active }) => (active ? "#fff" : "#434BD2")};
  font-size: 14px;
  border-radius: 5px;
  padding: 4px 8px;
  margin-right: 8px;
  position: relative;
  width: fit-content;

  &:hover .delete-tag {
    visibility: visible;
    opacity: 1;
  }
`;

const DeleteTag = styled.div`
  position: absolute;
  top: -5px;
  right: -5px;
  width: 15px;
  height: 15px;
  border-radius: 50%;
  border: 1px solid #979797;
  color: #979797;
  background: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
`;
