import { useRef, useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Button from "components/Button";
import Empty from "antd/lib/empty";
import { LoadingOutlined } from "@ant-design/icons";

import ListMessages from "./ListMessages";
import ChatScreen from "./ChatScreen";

import EmptyMessage from "assets/icons/icChatWidget/EmptyMessages.svg";

import {
  getConfigIC,
  getChatBotIC,
  getAllUserIC,
  getListSocialRegisterIC,
  getListMessagesAssignedIC,
  getListMessagesCompletedIC,
  getListMessagesAssignedICResult,
  getListMessagesCompletedICResult,
} from "redux/slices/icChatWidget";

function ICChatWidget({ recordID, objectId }) {
  const dispatch = useDispatch();
  const componentRef = useRef(null);

  const { userDetail } = useSelector((state) => state.userReducer);
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const {
    configIC,
    assignedMessages,
    completedMessages,
    selectedMessage,
    isLoadingMessages,
  } = useSelector((state) => state.icChatWidgetReducer);

  const [isAssigned, $isAssigned] = useState(true);
  const [isSearch, $isSearch] = useState(false);
  const [disabled, $disabled] = useState(false);
  const [showDetails, $showDetails] = useState(false);

  const _onFilter = (value) => {
    return () => {
      if (value === "complete") {
        $isAssigned(false);
      } else {
        $isAssigned(true);
      }
    };
  };

  useEffect(() => {
    dispatch(
      getConfigIC({
        object_id: objectId,
        id: recordID,
      })
    );
  }, [dispatch, objectId, recordID]);

  useEffect(() => {
    if (!_.isEmpty(configIC.value)) {
      if (isAssigned) {
        dispatch(
          getListMessagesAssignedIC({
            extention: "assigned",
            limit: 1000,
            page: 1,
            view_type: "",
            // sorts: [
            //   {
            //     name_field: "state",
            //     type_sort: "ASC",
            //   },
            //   {
            //     name_field: "create_time",
            //     type_sort: "desc",
            //   },
            // ],
            search_list: [
              {
                name_field: "customer_id",
                value_search: _.get(configIC, "value", ""),
              },
            ],
          })
        );
      } else {
        dispatch(
          getListMessagesCompletedIC({
            extention: "complete",
            limit: 1000,
            page: 1,
            view_type: "",
            // sorts: [
            //   {
            //     name_field: "state",
            //     type_sort: "ASC",
            //   },
            //   {
            //     name_field: "create_time",
            //     type_sort: "desc",
            //   },
            // ],
            search_list: [
              {
                name_field: "customer_id",
                value_search: _.get(configIC, "value", ""),
              },
            ],
          })
        );
      }
    } else {
      dispatch(getListMessagesAssignedICResult([]));
      dispatch(getListMessagesCompletedICResult([]));
    }
  }, [configIC, dispatch, isAssigned]);

  useEffect(() => {
    if (!_.get(userDetail, "use_ic", false) || !isAssigned) {
      $disabled(true);
    } else {
      $disabled(false);
    }
  }, [userDetail, isAssigned]);

  useEffect(() => {
    dispatch(getChatBotIC());
    dispatch(getAllUserIC());
    dispatch(getListSocialRegisterIC());
  }, [dispatch]);

  return (
    <Wrapper ref={componentRef}>
      {isLoadingMessages ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            flexDirection: "column",
            background: "white",
            width: "100%",
          }}
        >
          <LoadingOutlined
            style={{ fontSize: 60, color: defaultBrandName.theme_color }}
            spin
          />
          <div style={{ textAlign: "center", marginTop: "20px" }}>
            Loading...
          </div>
        </div>
      ) : (
        <>
          {componentRef?.current?.offsetWidth >= 880 ? (
            <>
              {(isAssigned && assignedMessages?.length === 0) ||
              (!isAssigned && completedMessages?.length === 0) ? (
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    width: "100%",
                    flexDirection: "column",
                  }}
                >
                  <Filter style={{ alignSelf: "flex-start" }}>
                    <Button
                      title="Assigned"
                      type={isAssigned ? "primary" : "default"}
                      onClick={!isAssigned ? _onFilter("assigned") : () => {}}
                    />
                    <Button
                      title="Completed"
                      type={isAssigned ? "default" : "primary"}
                      onClick={isAssigned ? _onFilter("complete") : () => {}}
                    />
                  </Filter>
                  <Empty
                    image={EmptyMessage}
                    description={<span>No messages yet</span>}
                  />
                </div>
              ) : (
                <>
                  <Left collapse={componentRef?.current?.offsetWidth < 880}>
                    <Filter>
                      <Button
                        title="Assigned"
                        type={isAssigned ? "primary" : "default"}
                        onClick={!isAssigned ? _onFilter("assigned") : () => {}}
                      />
                      <Button
                        title="Completed"
                        type={isAssigned ? "default" : "primary"}
                        onClick={isAssigned ? _onFilter("complete") : () => {}}
                      />
                    </Filter>

                    <ListMessages
                      isAssigned={isAssigned}
                      assignedMessages={assignedMessages}
                      completedMessages={completedMessages}
                      $isSearch={$isSearch}
                      $showDetails={$showDetails}
                    />
                  </Left>

                  <Right collapse={componentRef?.current?.offsetWidth < 880}>
                    <ChatScreen
                      selectedMessage={selectedMessage}
                      assignedMessages={assignedMessages}
                      isSearch={isSearch}
                      $isSearch={$isSearch}
                      disabled={disabled}
                    />
                  </Right>
                </>
              )}
            </>
          ) : (
            <>
              {showDetails ? (
                <RightMini>
                  <ChatScreen
                    selectedMessage={selectedMessage}
                    assignedMessages={assignedMessages}
                    isSearch={isSearch}
                    $isSearch={$isSearch}
                    disabled={disabled}
                    showDetails={showDetails}
                    $showDetails={$showDetails}
                  />
                </RightMini>
              ) : (
                <LeftMini>
                  <Filter>
                    <Button
                      title="Assigned"
                      type={isAssigned ? "primary" : "default"}
                      onClick={!isAssigned ? _onFilter("assigned") : () => {}}
                    />
                    <Button
                      title="Completed"
                      type={isAssigned ? "default" : "primary"}
                      onClick={isAssigned ? _onFilter("complete") : () => {}}
                    />
                  </Filter>

                  <ListMessages
                    isAssigned={isAssigned}
                    assignedMessages={assignedMessages}
                    completedMessages={completedMessages}
                    $isSearch={$isSearch}
                    $showDetails={$showDetails}
                    isCollapse={componentRef?.current?.offsetWidth < 880}
                  />
                </LeftMini>
              )}
            </>
          )}
        </>
      )}
    </Wrapper>
  );
}

export default ICChatWidget;

ICChatWidget.defaultProps = {
  recordID: "",
  objectId: "",
  layoutIC: {},
};

const Wrapper = styled.div`
  padding: 16px;
  display: flex;
  column-gap: 16px;
  width: 100%;
  height: 100%;
`;

const Left = styled.div`
  width: ${({ collapse }) => (collapse ? "100%" : "43%")};
  max-width: ${({ collapse }) => (collapse ? "100%" : "500px")};
`;

const LeftMini = styled.div`
  width: 100%;
`;

const RightMini = styled.div`
  width: 100%;
`;

const Filter = styled.div`
  margin-bottom: 16px;
`;

const Right = styled.div`
  /* width: ${({ collapse }) => (collapse ? "100%" : "57%")}; */
  flex: 1;
  padding-left: 8px;
`;
