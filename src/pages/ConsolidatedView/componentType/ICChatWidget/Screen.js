import _ from "lodash";
import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components/macro";
import InfiniteScroll from "react-infinite-scroll-component";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import ReactPlayer from "react-player";
import Linkify from "react-linkify";
import parse from "html-react-parser";
import Editor from "components/Editor/Editor2";
import Button from "components/Button";
import { Notification } from "components/Notification/Noti";

import Tooltip from "antd/lib/tooltip";
import Image from "antd/lib/image";
import Spin from "antd/lib/spin";
import Input from "antd/lib/input";
import Upload from "antd/lib/upload";
import { EditOutlined, CloseOutlined } from "@ant-design/icons";

import AvatarImg from "assets/images/header/avatar.png";
import ReplyIcon from "assets/icons/icChatWidget/Reply.svg";
import fileIcon from "assets/icons/common/attach-file.svg";
import QuickMessageIcon from "assets/icons/icChatWidget/QuickMessage.svg";
import SendIcon from "assets/icons/icChatWidget/Send.svg";
import FileIcon from "assets/icons/icChatWidget/File.svg";
import AttachFileIcon from "assets/icons/icChatWidget/AttachFile.svg";

import {
  getMessageDetailsIC,
  $currentPageIC,
  addNewMessage,
  $newMessage,
  getQuickMessagesIC,
  sendMessageIC,
} from "redux/slices/icChatWidget";

import InputChat from "./InputChat";
import ModalQuickMessage from "./Modal/QuickMessage";
import { allowedTags } from "util/staticData";
import { setShowLoadingScreen } from "redux/slices/global";

function Screen({ selectedMessage, selectedSearchMessage, disabled }) {
  const dispatch = useDispatch();
  const {
    messageDetails,
    hasMoreIC,
    currentPageIC,
    allChatBot,
    allUserIC,
    newMessage,
    recordPerPageIC,
    facebookPost,
    listSocialRegister,
  } = useSelector((state) => state.icChatWidgetReducer);

  const editorJodit = useRef(null);
  const refTo = useRef(null);
  const refCc = useRef(null);

  const [quoteMessage, $quoteMessage] = useState({
    owner: "",
    reply_content_file_name: null,
    reply_content_file_url: "",
    reply_create_by: "",
    reply_interaction_type: "",
    reply_location_lat: null,
    reply_location_long: null,
    reply_mess_text: "",
    reply_sender_type: "",
    reply_to_id: "",
  });

  const [preview, $preview] = useState({
    visible: false,
    current: 0,
  });

  const [showEditor, $showEditor] = useState(false);
  const [listTo, $listTo] = useState([]);
  const [listCc, $listCc] = useState([]);
  const [toInput, setToInput] = useState("");
  const [ccInput, setCcInput] = useState("");
  const [showQuickMessage, $showQuickMessage] = useState(false);
  const [files, $files] = useState([]);

  const loadMore = () => {
    let tempPage = currentPageIC + 1;
    dispatch($currentPageIC(tempPage));
  };

  const _onReply = (owner, message) => {
    return () => {
      $quoteMessage({
        owner: owner,
        reply_content_file_name: _.get(message, "content_file_name", null),
        reply_content_file_url: _.get(message, "content_file_url", ""),
        reply_create_by: _.get(message, "create_by", ""),
        reply_interaction_type: _.get(message, "interaction_type", ""),
        reply_location_lat: _.get(message, "location_lat", null),
        reply_location_long: _.get(message, "location_long", null),
        reply_mess_text: _.get(message, "content_text", ""),
        reply_sender_type: _.get(message, "sender_type", ""),
        reply_to_id: _.get(message, "id", ""),
      });
    };
  };

  const _onOpenFile = (url) => {
    window.open(`${url}`);
  };

  const componentDecorator = (href, text, key) => (
    <a href={href} key={key} target="_blank noopener">
      {text}
    </a>
  );

  const renderOwner = (type, value, reply) => {
    switch (type) {
      case "bot":
        return <Owner isReply={reply}>Auto message</Owner>;

      case "chatbot":
        return (
          <Owner isReply={reply}>
            {allChatBot?.find((item) => item?.id === value)?.display_name}
          </Owner>
        );

      case "agent":
        return (
          <Owner isReply={reply}>
            {allUserIC?.find((item) => item?.email === value)?.fullname ||
              listSocialRegister?.find((item) => item?.social_id === value)
                ?.social_name ||
              ""}
          </Owner>
        );

      default:
        if (reply) {
          return (
            <Owner isReply={reply}>
              {_.get(selectedMessage, "customer_name", "")}
            </Owner>
          );
        } else return "";
    }
  };

  const renderMessage = (type, value) => {
    switch (type) {
      case "image":
        return (
          <>
            <Image src={_.get(value, "content_file_url", "")} />

            {_.get(value, "content_text", "") && (
              <Linkify componentDecorator={componentDecorator}>
                <Text>{_.get(value, "content_text", "")}</Text>
              </Linkify>
            )}
          </>
        );

      case "video":
        return (
          <>
            <ReactPlayer
              url={_.get(value, "content_file_url", "")}
              controls
              width="100%"
              height="100%"
            />
            {_.get(value, "content_text", "") && (
              <Linkify componentDecorator={componentDecorator}>
                <Text>{_.get(value, "content_text", "")}</Text>
              </Linkify>
            )}
          </>
        );

      case "audio":
        return (
          <>
            <audio controls src={_.get(value, "content_file_url", "")} />
            {_.get(value, "content_text", "") && (
              <Linkify componentDecorator={componentDecorator}>
                <Text>{_.get(value, "content_text", "")}</Text>
              </Linkify>
            )}
          </>
        );

      case "file":
        return (
          <div
            style={{
              display: "flex",
              textDecoration: "underline",
              alignItems: "center",
              cursor: "pointer",
            }}
          >
            <img src={fileIcon} alt="file" style={{ marginRight: "8px" }} />
            <Text
              onClick={() => _onOpenFile(_.get(value, "content_file_url", ""))}
            >
              {_.get(value, "content_file_name", "")}
            </Text>

            {_.get(value, "content_text", "") && (
              <Linkify componentDecorator={componentDecorator}>
                <Text>{_.get(value, "content_text", "")}</Text>
              </Linkify>
            )}
          </div>
        );

      case "location":
        return (
          <>
            <iframe
              title="location"
              src={`https://maps.google.com/maps?hl=en&q=${_.get(
                value,
                "location_lat",
                ""
              )},${_.get(
                value,
                "location_long",
                ""
              )}&t=&z=14&ie=UTF8&iwloc=B&output=embed`}
            />
            {_.get(value, "content_text", "") && (
              <Linkify componentDecorator={componentDecorator}>
                <Text>{_.get(value, "content_text", "")}</Text>
              </Linkify>
            )}
          </>
        );

      default:
        return (
          <Linkify componentDecorator={componentDecorator}>
            <Text>{_.get(value, "content_text", "")}</Text>
          </Linkify>
        );
    }
  };

  const renderAttachmentPost = (attachments) => (
    <>
      {_.map(attachments, (attachment, idx) => (
        <>
          {_.get(attachment, "type", "") === "video" ? (
            <>
              <ReactPlayer
                url={_.get(attachment, "content_file_url", "")}
                controls
                width="100%"
                height="100%"
              />
              {_.get(attachment, "content_text", "") && (
                <Linkify componentDecorator={componentDecorator}>
                  <Text>{_.get(attachment, "content_text", "")}</Text>
                </Linkify>
              )}
            </>
          ) : (
            <>
              {idx < 3 && (
                <Image
                  preview={{
                    visible: false,
                  }}
                  src={_.get(attachment, "url", "")}
                  onClick={() =>
                    $preview({
                      visible: true,
                      current: idx,
                    })
                  }
                />
              )}

              {idx === 3 && (
                <WrapPostImage>
                  <Image
                    preview={{
                      visible: false,
                    }}
                    src={_.get(attachment, "url", "")}
                    onClick={() =>
                      $preview({
                        visible: true,
                        current: idx,
                      })
                    }
                  />

                  {attachments?.length > 4 && (
                    <ShowMorePostImage
                      onClick={() =>
                        $preview({
                          visible: true,
                          current: idx,
                        })
                      }
                    >
                      +{attachments?.length - 4}
                    </ShowMorePostImage>
                  )}
                </WrapPostImage>
              )}
            </>
          )}
        </>
      ))}

      <div
        style={{
          display: "none",
        }}
      >
        <Image.PreviewGroup
          preview={{
            visible: preview.visible,
            onVisibleChange: (vis) =>
              $preview({
                ...preview,
                visible: vis,
              }),
            current: preview.current,
          }}
        >
          {_.map(attachments, (attachment) => (
            <Image src={_.get(attachment, "url", "")} />
          ))}
        </Image.PreviewGroup>
      </div>
    </>
  );

  const _onSendMess = () => {
    console.log("editorJodit.current.value", editorJodit.current.value);
    if (
      !editorJodit?.current?.value ||
      editorJodit?.current?.value === "<p><br></p>"
    ) {
      Notification("warning", "Input message is empty!");
    } else {
      const body = editorJodit.current.value.replaceAll(
        '<table style="border-collapse:collapse;width: 100%;">',
        '<table border="1" style="border-collapse:collapse;width: 100%;">'
      );

      let tempTo = [];
      let tempCc = [];
      let tempAttachments = [];

      _.map(listTo, (item) => {
        tempTo.push({
          address: item,
        });
      });
      _.map(listCc, (item) => {
        tempCc.push({
          address: item,
        });
      });

      _.map(files, (file) => {
        tempAttachments.push({
          file_content_type:
            _.get(file, "response.data.items[0].file_content", "") !==
              "image" &&
            _.get(file, "response.data.items[0].file_content", "") !==
              "video" &&
            _.get(file, "response.data.items[0].file_content", "") !== "audio"
              ? "file"
              : _.get(file, "response.data.items[0].file_content", ""),
          file_extension: _.get(file, "response.data.items[0].file_type", ""),
          file_name: _.get(file, "name", ""),
          file_upload_url: _.get(file, "response.data.items[0].url", ""),
          is_inline: true,
        });
      });

      dispatch(
        sendMessageIC({
          interaction_id: _.get(selectedMessage, "interaction_id", ""),
          interaction_type: "text",
          channel_type: _.get(selectedMessage, "channel_type", ""),
          to: listTo,
          cc: listCc,
          data_mail: {
            attachments: tempAttachments,
            cc_mails: tempCc,
            to_mails: tempTo,
            from_mail: _.get(selectedMessage, "page_social_id", ""),
            is_image_body: true,
            subject: "",
            message_id:
              _.get(messageDetails, "[0].reply_to") ||
              _.get(messageDetails, "[0].id"),
            server_email_type: _.get(
              messageDetails,
              "[0].server_email_type",
              "google"
            ),
            content: body,
          },
        })
      );

      editorJodit.current.value = "";
      $files([]);
      $listTo([_.get(selectedMessage, "user_social_id", "")]);
      $listCc([]);
      $showEditor(false);
    }
  };

  console.log("messageDetails", messageDetails);

  const _onOpenQuickMessage = () => {
    $showQuickMessage(true);
    dispatch(
      getQuickMessagesIC({
        sorts: [
          {
            name_field: "position",
            type_sort: "ASC",
          },
        ],
        search_list: [
          {
            conjunction: "AND",
            name_field: "channel_type",
            upper_bound: null,
            value_search: _.get(selectedMessage, "channel_type", ""),
          },
        ],
      })
    );
  };

  const props = {
    name: "file",
    multiple: true,
    showUploadList: false,
    action:
      localStorage.getItem("icIntegration_link_api") +
      "resource/api/custom-api/upload-file",
    headers: {
      Authorization:
        "Bearer " + localStorage.getItem("icIntegration_accessToken"),
    },
    data: {
      channel_type: _.get(selectedMessage, "channel_type", ""),
    },
    beforeUpload() {
      dispatch(setShowLoadingScreen(true));
    },
  };

  const handleChange = ({ fileList: newFileList, file }) => {
    $files(newFileList);
    if (
      file.status === "done" ||
      file.status === "error" ||
      file.status === "success"
    ) {
      dispatch(setShowLoadingScreen(false));
    }
  };

  const _onRemoveFile = (idx) => {
    return (e) => {
      e.stopPropagation();
      let tempFiles = [...files];
      tempFiles.splice(idx, 1);
      $files(tempFiles);
    };
  };

  useEffect(() => {
    if (!_.isEmpty(_.get(selectedMessage, "interaction_id", ""))) {
      dispatch(
        getMessageDetailsIC({
          channel_type: _.get(selectedMessage, "channel_type", ""),
          interaction_id: _.get(selectedMessage, "interaction_id", ""),
          is_all: true,
          limit:
            _.get(selectedMessage, "channel_type", "") === "facebook_comment" ||
            _.get(selectedMessage, "channel_type", "") === "instagram_comment"
              ? 0
              : recordPerPageIC
              ? recordPerPageIC
              : 20,
          page:
            _.get(selectedMessage, "channel_type", "") === "facebook_comment" ||
            _.get(selectedMessage, "channel_type", "") === "instagram_comment"
              ? 0
              : currentPageIC,
          page_social_id: _.get(selectedMessage, "page_social_id", ""),
          user_social_id: _.get(selectedMessage, "user_social_id", ""),
          post_id: _.get(selectedMessage, "post_id", ""),
        })
      );
    }
  }, [currentPageIC, dispatch, selectedMessage, recordPerPageIC]);

  useEffect(() => {
    if (
      !_.isEmpty(newMessage) &&
      !_.isEmpty(_.get(selectedMessage, "interaction_id", ""))
    ) {
      if (
        _.get(newMessage, "interaction_id") ===
        _.get(selectedMessage, "interaction_id", "")
      ) {
        dispatch(addNewMessage(newMessage));
        dispatch($newMessage({}));
      }
    }
  }, [newMessage, selectedMessage, dispatch]);

  useEffect(() => {
    if (messageDetails?.length <= 20) {
      const element = document.getElementById(messageDetails[0]?.id);
      if (element) {
        element.scrollIntoView({ behavior: "smooth" });
      }
    }
  }, [messageDetails]);

  const options = {
    replace: (domNode) => {
      if (_.get(domNode, "name", "") === "style") {
        return <style></style>;
      }

      if (
        _.get(domNode, "name", "") !== "" &&
        !allowedTags.includes(_.get(domNode, "name", ""))
      ) {
        return (
          <span>
            {"< "}
            {_.get(domNode, "name", "")}
            {" >"} {_.get(domNode, "children[0].data", "")}
          </span>
        );
      }
    },
  };

  useEffect(() => {
    if (
      _.get(selectedMessage, "channel_type", "") === "email" &&
      _.get(selectedMessage, "user_social_id", "")
    ) {
      $listTo([_.get(selectedMessage, "user_social_id", "")]);
    }
  }, [selectedMessage]);

  return (
    <Wrapper>
      <Messages
        id="icIntegrationTarget"
        isEmail={_.get(selectedMessage, "channel_type", "") === "email"}
        showEditor={showEditor}
      >
        <InfiniteScroll
          dataLength={messageDetails.length}
          scrollableTarget="icIntegrationTarget"
          loader={<Spin />}
          hasMore={hasMoreIC}
          next={loadMore}
          inverse={true}
          style={{
            display: "flex",
            flexDirection: "column-reverse",
          }}
        >
          {_.get(selectedMessage, "channel_type", "") === "facebook_comment" ||
          _.get(selectedMessage, "channel_type", "") === "instagram_comment" ? (
            <>
              {_.map(messageDetails, (message, idx) => (
                <WrapMessagePost
                  isCustomer={_.isEqual(
                    _.get(message, "sender_type", ""),
                    "customer"
                  )}
                  isLog={_.isEqual(
                    _.get(message, "sender_type", ""),
                    "activity_log"
                  )}
                  style={{
                    marginBottom: 0,
                    paddingBottom: _.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    )
                      ? "16px"
                      : "40px",
                    paddingLeft:
                      idx === messageDetails?.length - 1 ? 0 : "16px",
                    marginLeft: idx === messageDetails?.length - 1 ? 0 : "16px",
                  }}
                  borderLeft={idx !== messageDetails?.length - 1 && idx !== 0}
                  key={idx}
                >
                  <div
                    style={{
                      display: "flex",
                      columnGap: "10px",
                      alignItems: "center",
                    }}
                  >
                    {!_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) && (
                      <>
                        {idx === messageDetails?.length - 1 ? (
                          <div className="link-line-root" />
                        ) : (
                          <div className="link-line" />
                        )}
                      </>
                    )}
                    {!_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) && (
                      <Avatar
                        src={
                          _.isEqual(
                            _.get(message, "sender_type", ""),
                            "customer"
                          )
                            ? _.get(selectedMessage, "avatar", "") || AvatarImg
                            : AvatarImg
                        }
                        alt="avatar"
                        style={{
                          alignSelf: "flex-start",
                          zIndex: 2,
                          backgroundColor: "#fff8f8",
                        }}
                      />
                    )}

                    {_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) ? (
                      <Log
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                      >
                        <div
                          style={{
                            position: "absolute",
                            width: "100%",
                            borderTop: "1px solid #c9c9c9",
                          }}
                        ></div>
                        <span>
                          {_.get(message, "content_text", "")}&nbsp;-&nbsp;
                          {moment(_.get(message, "sent_time", "")).format(
                            "HH:mm • DD/MM/YYYY"
                          )}
                        </span>
                      </Log>
                    ) : (
                      <Comment
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                        isCustomer={_.isEqual(
                          _.get(message, "sender_type", ""),
                          "customer"
                        )}
                      >
                        <div
                          style={{
                            display: "flex",
                            columnGap: "16px",
                            alignItems: "center",
                          }}
                        >
                          {renderOwner(
                            _.get(message, "sender_type", ""),
                            _.get(message, "create_by", ""),
                            true
                          )}
                          <PostTime
                            isCustomer={_.isEqual(
                              _.get(message, "sender_type", ""),
                              "customer"
                            )}
                            isError={
                              _.get(message, "interaction_state", "") ===
                              "error"
                            }
                          >
                            {moment(_.get(message, "sent_time", "")).format(
                              "HH:mm • DD/MM/YYYY"
                            )}

                            {_.get(message, "interaction_state", "") ===
                              "error" && (
                              <span style={{ color: "#c4314b" }}>
                                &nbsp;•&nbsp;Cannot send message!
                              </span>
                            )}
                          </PostTime>
                        </div>

                        {renderMessage(
                          _.get(message, "interaction_type", ""),
                          message
                        )}
                      </Comment>
                    )}
                  </div>
                </WrapMessagePost>
              ))}
            </>
          ) : _.get(selectedMessage, "channel_type", "") === "email" ? (
            <>
              {_.map(messageDetails, (message, idx) => (
                <WrapMessageEmail
                  isLog={_.isEqual(
                    _.get(message, "sender_type", ""),
                    "activity_log"
                  )}
                  lastMessage={idx === 0}
                  key={idx}
                >
                  <div
                    style={{
                      display: "flex",
                      columnGap: "10px",
                      alignItems: " center",
                    }}
                  >
                    {_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) ? (
                      <Log
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                      >
                        <div
                          style={{
                            position: "absolute",
                            width: "100%",
                            borderTop: "1px solid #c9c9c9",
                          }}
                        ></div>
                        <span>
                          {_.get(message, "content_text", "")}&nbsp;-&nbsp;
                          {moment(_.get(message, "sent_time", "")).format(
                            "HH:mm • DD/MM/YYYY"
                          )}
                        </span>
                      </Log>
                    ) : (
                      <MessageEmail
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                        isCustomer={_.isEqual(
                          _.get(message, "direction", ""),
                          "receive"
                        )}
                      >
                        <EmailInfor
                          isCustomer={_.isEqual(
                            _.get(message, "direction", ""),
                            "receive"
                          )}
                        >
                          <div>
                            <EmailSubject>
                              {_.get(message, "subject", "") && (
                                <Subject>
                                  {_.get(message, "subject", "")}
                                </Subject>
                              )}

                              <Sender>
                                &#60;{_.get(message, "from", "")}&#62;
                              </Sender>
                            </EmailSubject>
                            <To>To: {_.get(message, "to", "")}</To>

                            {_.get(message, "list_cc", "") && (
                              <CC>CC: {_.get(message, "list_cc", "")}</CC>
                            )}
                          </div>

                          <EmailTime>
                            {moment(_.get(message, "create_time", "")).format(
                              "HH:mm • DD/MM/YYYY"
                            )}
                          </EmailTime>
                        </EmailInfor>

                        <EmailContent>
                          {parse(_.get(message, "content", ""), options)}
                        </EmailContent>

                        <EmailAttachment>
                          {_.map(
                            _.get(message, "attachments", []),
                            (attachment) => (
                              <Attachment
                                onClick={() =>
                                  window.open(
                                    _.get(message, "file_upload_url", "")
                                  )
                                }
                              >
                                <img src={FileIcon} alt="" />
                                <span>
                                  {_.get(attachment, "file_name", "")}
                                </span>
                              </Attachment>
                            )
                          )}
                        </EmailAttachment>
                      </MessageEmail>
                    )}
                  </div>
                </WrapMessageEmail>
              ))}
            </>
          ) : (
            <>
              {_.map(messageDetails, (message, idx) => (
                <WrapMessage
                  isCustomer={_.isEqual(
                    _.get(message, "sender_type", ""),
                    "customer"
                  )}
                  isLog={_.isEqual(
                    _.get(message, "sender_type", ""),
                    "activity_log"
                  )}
                  key={idx}
                >
                  <div
                    style={{
                      display: "flex",
                      columnGap: "10px",
                      alignItems: " center",
                      flexDirection: _.isEqual(
                        _.get(message, "sender_type", ""),
                        "customer"
                      )
                        ? "row"
                        : "row-reverse",
                    }}
                  >
                    {!_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) && (
                      <Avatar
                        src={
                          _.isEqual(
                            _.get(message, "sender_type", ""),
                            "customer"
                          )
                            ? _.get(selectedMessage, "avatar", "") || AvatarImg
                            : AvatarImg
                        }
                        alt="avatar"
                      />
                    )}

                    {_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) ? (
                      <Log
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                      >
                        <div
                          style={{
                            position: "absolute",
                            width: "100%",
                            borderTop: "1px solid #c9c9c9",
                          }}
                        ></div>
                        <span>
                          {_.get(message, "content_text", "")}&nbsp;-&nbsp;
                          {moment(_.get(message, "sent_time", "")).format(
                            "HH:mm • DD/MM/YYYY"
                          )}
                        </span>
                      </Log>
                    ) : (
                      <Message
                        id={_.get(message, "id", idx)}
                        isHightlight={
                          _.get(message, "id") ===
                          _.get(selectedSearchMessage, "id")
                        }
                        isCustomer={_.isEqual(
                          _.get(message, "sender_type", ""),
                          "customer"
                        )}
                      >
                        {renderOwner(
                          _.get(message, "sender_type", ""),
                          _.get(message, "create_by", ""),
                          false
                        )}

                        {_.get(message, "reply_to_id", "") && (
                          <WrapMessageReply>
                            <span>
                              -{" "}
                              {renderOwner(
                                _.get(message, "reply_sender_type", ""),
                                _.get(message, "reply_create_by", ""),
                                true
                              )}{" "}
                              -
                            </span>

                            <MessageReply>
                              {_.get(message, "reply_interaction_type", "") !==
                              "text" ? (
                                <span>
                                  Shared a file:{" "}
                                  {_.get(
                                    message,
                                    "reply_content_file_name",
                                    ""
                                  )}
                                </span>
                              ) : (
                                <span>
                                  {_.get(message, "content_text_reply", "") ||
                                    _.get(message, "reply_mess_text", "")}
                                </span>
                              )}
                            </MessageReply>
                          </WrapMessageReply>
                        )}

                        {renderMessage(
                          _.get(message, "interaction_type", ""),
                          message
                        )}

                        <Time
                          isCustomer={_.isEqual(
                            _.get(message, "sender_type", ""),
                            "customer"
                          )}
                          isError={
                            _.get(message, "interaction_state", "") === "error"
                          }
                        >
                          {moment(_.get(message, "sent_time", "")).format(
                            "HH:mm • DD/MM/YYYY"
                          )}

                          {_.get(message, "interaction_state", "") ===
                            "error" && (
                            <span style={{ color: "#c4314b" }}>
                              &nbsp;•&nbsp;Cannot send message!
                            </span>
                          )}
                        </Time>
                      </Message>
                    )}

                    {!_.isEqual(
                      _.get(message, "sender_type", ""),
                      "activity_log"
                    ) &&
                      !disabled && (
                        <WrapActions>
                          <Tooltip title="Reply">
                            <Reply
                              className="action"
                              onClick={_onReply(
                                renderOwner(
                                  _.get(message, "sender_type", ""),
                                  _.get(message, "create_by", ""),
                                  true
                                ),
                                message
                              )}
                            >
                              <img src={ReplyIcon} alt="" />
                            </Reply>
                          </Tooltip>
                        </WrapActions>
                      )}
                  </div>
                </WrapMessage>
              ))}
            </>
          )}

          {!_.isEmpty(facebookPost) && (
            <WrapPost>
              <PostInfo>
                <Avatar
                  src={
                    _.get(facebookPost, "created_by.avatar", "") || AvatarImg
                  }
                  alt="avatar"
                  style={{ width: "48px", height: "48px" }}
                />
                <PostName>
                  <span>{_.get(facebookPost, "created_by.name", "")}</span>

                  <PostTime>
                    {moment(_.get(facebookPost, "created_time", "")).format(
                      "HH:mm • DD/MM/YYYY"
                    )}
                  </PostTime>
                </PostName>
              </PostInfo>
              <PostContent>
                <Linkify componentDecorator={componentDecorator}>
                  <Text>{_.get(facebookPost, "content", "")}</Text>
                </Linkify>
              </PostContent>
              <PostAttachment>
                {renderAttachmentPost(_.get(facebookPost, "attachments", []))}
              </PostAttachment>
            </WrapPost>
          )}
        </InfiniteScroll>
      </Messages>

      {_.get(selectedMessage, "channel_type", "") === "email" ? (
        <WrapEditor showEditor={showEditor}>
          {showEditor && (
            <>
              <div
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  marginBottom: "4px",
                }}
              >
                <div>
                  <SendTo
                    onClick={() => {
                      refTo.current.focus({
                        cursor: "end",
                      });
                    }}
                  >
                    <span style={{ marginRight: "8px" }}>To:</span>
                    <WrapInputTo>
                      <InputTo>
                        {listTo.map((item, index) => {
                          return (
                            <LabelTo key={index}>
                              {item}
                              {index !== 0 && (
                                <CloseOutlined
                                  onClick={() => {
                                    let tempListTo = [...listTo];
                                    tempListTo.splice(index, 1);
                                    $listTo(tempListTo);
                                  }}
                                />
                              )}
                            </LabelTo>
                          );
                        })}

                        <CustomInput
                          onPressEnter={(e) => {
                            if (listTo.includes(e.target.value)) {
                              Notification("error", "Email exist!");
                            } else {
                              let tempListTo = [...listTo];
                              if (e.target.value.trim().split(" ")[0]) {
                                tempListTo.push(
                                  e.target.value.trim().split(" ")[0]
                                );
                                $listTo(tempListTo);
                              }
                              setToInput("");
                            }
                          }}
                          onBlur={(e) => {
                            if (listTo.includes(e.target.value)) {
                              Notification("error", "Email exist!");
                            } else {
                              let tempListTo = [...listTo];
                              if (e.target.value.trim().split(" ")[0]) {
                                tempListTo.push(
                                  e.target.value.trim().split(" ")[0]
                                );
                                $listTo(tempListTo);
                              }
                              setToInput("");
                            }
                          }}
                          ref={refTo}
                          onChange={(e) => setToInput(e.target.value)}
                          value={toInput}
                          bordered={false}
                          style={{
                            width:
                              toInput.length > 5 ? toInput.length * 6 + 50 : 50,
                          }}
                        />
                      </InputTo>
                    </WrapInputTo>
                  </SendTo>
                  <SendTo
                    onClick={() => {
                      refCc.current.focus({
                        cursor: "end",
                      });
                    }}
                    style={{ marginTop: "4px" }}
                  >
                    <span style={{ marginRight: "8px" }}>CC:</span>
                    <WrapInputCC>
                      <InputTo>
                        {listCc.map((item, index) => {
                          return (
                            <LabelTo key={index}>
                              {item}
                              <CloseOutlined
                                onClick={() => {
                                  let tempListCc = [...listCc];
                                  tempListCc.splice(index, 1);
                                  $listCc(tempListCc);
                                }}
                              />
                            </LabelTo>
                          );
                        })}

                        <CustomInput
                          onPressEnter={(e) => {
                            if (listCc.includes(e.target.value)) {
                              Notification("error", "Email exist!");
                            } else {
                              let tempListCc = [...listCc];
                              console.log("tempListCC", tempListCc);
                              if (e.target.value.trim().split(" ")[0]) {
                                tempListCc.push(
                                  e.target.value.trim().split(" ")[0]
                                );
                                console.log("tempListCC2", tempListCc);

                                $listCc(tempListCc);
                              }
                              setCcInput("");
                            }
                          }}
                          onBlur={(e) => {
                            if (listCc.includes(e.target.value)) {
                              Notification("error", "Email exist!");
                            } else {
                              let tempListCc = [...listCc];
                              if (e.target.value.trim().split(" ")[0]) {
                                tempListCc.push(
                                  e.target.value.trim().split(" ")[0]
                                );
                                $listCc(tempListCc);
                              }
                              setCcInput("");
                            }
                          }}
                          ref={refCc}
                          onChange={(e) => setCcInput(e.target.value)}
                          value={ccInput}
                          bordered={false}
                          style={{
                            width:
                              ccInput.length > 5 ? ccInput.length * 6 + 50 : 50,
                          }}
                        />
                      </InputTo>
                    </WrapInputCC>
                  </SendTo>
                </div>

                <div style={{ display: "flex", alignItems: "flex-end" }}>
                  <Tooltip title="Attachment">
                    <Upload {...props} fileList={files} onChange={handleChange}>
                      <div
                        style={{
                          background: "#eee",
                          minWidth: "40px",
                          height: "40px",
                          display: "flex",
                          alignItems: "center",
                          justifyContent: "center",
                          cursor: "pointer",
                          borderRadius: "10px",
                        }}
                      >
                        <img
                          src={AttachFileIcon}
                          alt=""
                          style={{
                            width: "24px",
                            height: "24px",
                          }}
                        />
                      </div>
                    </Upload>
                  </Tooltip>
                  <Tooltip title="Send">
                    <SendButton onClick={_onSendMess}>
                      <img src={SendIcon} alt="Send" />
                    </SendButton>
                  </Tooltip>

                  <Tooltip title="Quick message (Tips: Type #shortcuts to search response template quickly)">
                    <QuickMessage onClick={_onOpenQuickMessage}>
                      <img src={QuickMessageIcon} alt="Quick" />
                    </QuickMessage>
                  </Tooltip>
                </div>
              </div>

              {files?.length > 0 && (
                <Files>
                  {_.map(files, (file, idx) => (
                    <RenderFile>
                      <img
                        src={
                          _.isEqual(
                            _.get(
                              file,
                              "response.data.items[0].file_content",
                              ""
                            ),
                            "image"
                          )
                            ? _.get(
                                file,
                                "response.data.items[0].thumbnail_url",
                                ""
                              )
                            : FileIcon
                        }
                        alt=""
                        style={{
                          width: "40px",
                          height: "40px",
                          marginRight: "8px",
                        }}
                      />
                      {_.get(file, "name", "")}
                      <CloseOutlined
                        style={{ marginLeft: "8px" }}
                        onClick={_onRemoveFile(idx)}
                      />
                    </RenderFile>
                  ))}
                </Files>
              )}

              <Editor
                editorJodit={editorJodit}
                objectID={""}
                showAppend={false}
                minHeight={"236px"}
                readonly={disabled}
                isIC={true}
              />
            </>
          )}

          <Button
            type="text"
            disabled={disabled}
            onClick={() => {
              if (disabled) return;
              $showEditor(!showEditor);
            }}
            style={{
              position: "fixed",
              right: showEditor
                ? files?.length > 0
                  ? "48px"
                  : "40px"
                : "48px",
              bottom: showEditor
                ? files?.length > 0
                  ? "32px"
                  : "35px"
                : "unset",
              zIndex: 2,
            }}
            title={
              <>
                {showEditor ? (
                  <EditOutlined />
                ) : (
                  <>
                    <EditOutlined style={{ marginRight: "4px" }} /> New Email
                  </>
                )}
              </>
            }
          />

          <ModalQuickMessage
            showQuickMessage={showQuickMessage}
            $showQuickMessage={$showQuickMessage}
            selectedMessage={selectedMessage}
            _onEditQuickMessage={() => {}}
            listTo={listTo}
            listCc={listCc}
            messageDetails={messageDetails}
          />
        </WrapEditor>
      ) : (
        <InputChat
          selectedMessage={selectedMessage}
          quoteMessage={quoteMessage}
          $quoteMessage={$quoteMessage}
          disabled={disabled}
          messageDetails={messageDetails}
        />
      )}
    </Wrapper>
  );
}

export default Screen;

const Wrapper = styled.div`
  padding: 16px 14px 16px 24px;
  background: #f3f3f3;
  border-radius: 10px;
  position: relative;
  height: calc(100% - 48px);

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .jodit-wysiwyg {
    height: 160px !important;
  }
`;

const WrapPost = styled.div`
  background: #fff;
  padding: 16px;
  margin-bottom: 16px;
  border-radius: 10px;
`;

const PostInfo = styled.div`
  display: flex;
  margin-bottom: 16px;
`;

const PostName = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  span {
    font-size: 20px;
    font-family: var(--roboto-500);
  }
`;

const PostTime = styled.div`
  font-size: 14px;
  color: #888;
`;

const PostContent = styled.div`
  margin-bottom: 16px;
`;

const PostAttachment = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  column-gap: 16px;
  row-gap: 16px;

  .ant-image {
    display: flex;
    justify-content: center;
    align-items: center;
    max-height: 500px;
    width: calc(50% - 16px);
  }

  img {
    height: 100%;
  }
`;

const WrapPostImage = styled.div`
  width: calc(50% - 16px);
  position: relative;
  display: flex;
  max-height: 500px;

  .ant-image {
    width: 100%;
  }
`;

const ShowMorePostImage = styled.div`
  cursor: pointer;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  transform: translateY(-100%);
  background: rgba(0, 0, 0, 0.7);
  font-weight: var(--roboto-500);
  font-size: 18px;
  color: #fff;
`;

const Messages = styled.div`
  height: ${({ isEmail, showEditor }) =>
    isEmail
      ? showEditor
        ? "calc(100% - 321px)"
        : "calc(100% - 35px)"
      : "calc(100% - 114px)"};
  overflow: auto;
  padding-right: 10px;
  display: flex;
  flex-direction: column-reverse;

  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    background: #d9d9d9;
  }

  ::-webkit-scrollbar-thumb {
    background: #bfbfbf;
  }
`;

const WrapMessage = styled.div`
  margin-bottom: ${({ isLog }) => (isLog ? "16px" : "40px")};

  :hover .action {
    visibility: visible;
    opacity: 1;

    /* img {
        :hover {
          background: #eeeeee;
        }
      } */
  }
`;

const WrapMessagePost = styled.div`
  position: relative;
  margin-bottom: ${({ isLog }) => (isLog ? "16px" : "40px")};

  :hover .action {
    visibility: visible;
    opacity: 1;

    /* img {
      :hover {
        background: #eeeeee;
      }
    } */
  }

  &::after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    height: ${({ borderLeft }) => (borderLeft ? "100%" : 0)};
    width: 1px;
    background: #9f9494;
  }

  .link-line {
    width: 17px;
    height: 20px;
    position: absolute;
    left: 0;
    top: 0;
    border-left: 1px solid #9f9494;
    border-bottom: 1px solid #9f9494;
    border-bottom-left-radius: 0.625rem;
  }

  .link-line-root {
    position: absolute;
    width: 1px;
    height: 100%;
    left: 16px;
    top: 0;
    border-left: 1px solid #9f9494;
    border-bottom: unset;
    border-bottom-left-radius: unset;
  }
`;

const WrapMessageEmail = styled.div`
  margin-bottom: ${({ isLog, lastMessage }) =>
    isLog ? "16px" : lastMessage ? "0" : "40px"};
`;

const MessageEmail = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  background: ${({ isCustomer, isHightlight }) =>
    isHightlight ? "#20A2A2" : isCustomer ? "#fff" : "#E3F0FF"};
  border-radius: ${({ isCustomer }) =>
    isCustomer ? "14px 14px 14px 0px" : "14px 14px 0px 14px"};
  border: 1px solid ${({ isCustomer }) => (isCustomer ? "#ddd" : "#C1DEFF")};
  padding: 12px 16px;
  /* max-width: 60%; */
  /* position: relative; */
  /* white-space: pre-line; */
`;

const EmailInfor = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-bottom: 8px;
  margin-bottom: 8px;
  border-bottom: ${({ isCustomer }) =>
    isCustomer ? "1px solid #ececec" : "1px solid #fff"};
`;

const EmailSubject = styled.div`
  display: flex;
  align-items: center;
`;

const Subject = styled.div`
  margin-right: 8px;
  font-size: 16px;
  font-family: var(--roboto-700);
`;

const Sender = styled.div`
  color: #5c5757;
`;

const To = styled.div`
  color: #5c5757;
`;

const CC = styled.div`
  color: #5c5757;
`;

const EmailTime = styled.div`
  color: #5c5757;
`;

const EmailContent = styled.div`
  img {
    max-width: 100%;
  }
  picture {
    max-width: 100%;
  }
`;

const EmailAttachment = styled.div``;

const Attachment = styled.div`
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-bottom: 4px;

  img {
    margin-right: 8px;
    width: 24px;
  }

  span {
    color: #20a2a2;
    word-break: break-word;
  }
`;

const WrapActions = styled.div`
  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }
`;

const Reply = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 8px;
  width: 30px;
  height: 30px;
  border: 1px solid #ececec;
  border-radius: 8px;
  background: #fff;
  cursor: pointer;

  &:hover {
    img {
      filter: brightness(0);
    }
  }
`;

const Avatar = styled.img`
  width: 38px;
  height: 38px;
  border: 1px solid #ccc9c9;
  border-radius: 50%;
  object-fit: cover;
  margin-right: 10px;
  align-self: flex-end;
`;

const Owner = styled.span`
  color: ${({ isReply }) => (isReply ? "#252424" : "#2805b7")};
  font-size: ${({ isReply }) => (isReply ? "14px" : "12px")};
  font-weight: ${({ isReply }) => (isReply ? "700" : "400")};
`;

const Text = styled.div`
  font-size: 16px;
  color: #2c2c2c;
`;

const Log = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    font-size: 12px;
    text-align: center;
    color: #5c5757;
    padding: 2px 8px;
    background: ${({ isHightlight }) => (isHightlight ? "#20A2A2" : "#dcdcdc")};
    border-radius: 10px;
    max-width: 85%;
    z-index: 2;
  }
`;

const Message = styled.div`
  display: flex;
  flex-direction: column;
  background: ${({ isCustomer, isHightlight }) =>
    isHightlight ? "#20A2A2" : isCustomer ? "#fff" : "#E3F0FF"};
  border-radius: ${({ isCustomer }) =>
    isCustomer ? "14px 14px 14px 0px" : "14px 14px 0px 14px"};
  border: 1px solid ${({ isCustomer }) => (isCustomer ? "#ddd" : "#C1DEFF")};
  padding: 12px 16px;
  max-width: 60%;
  position: relative;
  white-space: pre-line;
`;

const Comment = styled.div`
  display: flex;
  flex-direction: column;
  background: ${({ isCustomer, isHightlight }) =>
    isHightlight ? "#20A2A2" : isCustomer ? "#fff" : "#E3F0FF"};
  border-radius: ${({ isCustomer }) =>
    isCustomer ? "14px 14px 14px 0px" : "14px 14px 0px 14px"};
  border: 1px solid ${({ isCustomer }) => (isCustomer ? "#ddd" : "#C1DEFF")};
  padding: 12px 16px;
  position: relative;
  white-space: pre-line;
  width: 100%;
`;

const Time = styled.span`
  font-size: 14px;
  color: #888;
  position: absolute;
  bottom: -25px;
  right: ${({ isCustomer }) => (isCustomer ? "unset" : 0)};
  left: ${({ isCustomer }) => (isCustomer ? 0 : "unset")};
  min-width: ${({ isError }) => (isError ? "280px" : "122px")};
`;

const WrapMessageReply = styled.div`
  background: #d0e3f9;
  border-radius: 8px;
  padding: 8px 12px 8px 24px;
  margin-bottom: 10px;
  position: relative;

  ::before {
    content: "";
    background: #86a9d1;
    width: 4px;
    height: calc(100% - 16px);
    position: absolute;
    border-radius: 4px;
    left: 12px;
    top: 8px;
  }
`;

const MessageReply = styled.div`
  color: #464545;
  font-size: 16px;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const WrapEditor = styled.div`
  padding: ${({ showEditor }) => (showEditor ? "10px 10px 0 10px" : "0")};
  position: relative;
  margin-top: 4px;
  background: ${({ showEditor }) => (showEditor ? "#fff" : "unset")};
  overflow: auto;
  height: ${({ showEditor }) => (showEditor ? "317px" : "40px")};

  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    background: #d9d9d9;
  }

  ::-webkit-scrollbar-thumb {
    background: #bfbfbf;
  }
`;

const SendTo = styled.div`
  display: flex;
  align-items: center;
`;

const WrapInputTo = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const WrapInputCC = styled(WrapInputTo)``;

const InputTo = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 4px;
  row-gap: 4px;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
`;

const LabelTo = styled.div`
  display: flex;
  align-items: center;
  padding: 2px 4px 2px 8px;
  width: fit-content;
  background: #f5f5f5;
  border: 1px solid #f0f0f0;
  border-radius: 2px;
  color: #2c2c2c;
  line-height: 20px;

  :last-child {
    margin-right: 0;
  }

  .anticon-close {
    margin-left: 4px;
    width: 24px;
    height: 24px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const CustomInput = styled(Input)`
  display: flex;
  margin-left: 4px;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  overflow: hidden;
  padding: 4px 0;
`;

const SendButton = styled.div`
  background: #1976d2;
  border-radius: 10px;
  min-width: 40px;
  height: 40px;
  margin-right: 16px;
  margin-left: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const QuickMessage = styled(SendButton)`
  margin-right: 0;
  margin-left: 0;
  background: #eee;
`;

const Files = styled.div`
  display: flex;
  flex-wrap: wrap;
  column-gap: 8px;
  row-gap: 8px;
  margin-top: 8px;
  margin-bottom: 4px;
`;

const RenderFile = styled.div`
  background: #f0f0f0;
  border-radius: 8px;
  padding: 8px 5px;
  width: fit-content;
  cursor: pointer;
`;
