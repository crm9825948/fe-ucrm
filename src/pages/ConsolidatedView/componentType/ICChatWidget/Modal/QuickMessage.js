import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { useMemo, useCallback } from "react";
import parse from "html-react-parser";

import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";

import Edit from "assets/icons/common/edit.svg";
import FileIcon from "assets/icons/icChatWidget/File.svg";

import { sendMessageIC, getQuickMessagesIC } from "redux/slices/icChatWidget";
import { allowedTags } from "util/staticData";

function QuickMessage({
  showQuickMessage,
  $showQuickMessage,
  selectedMessage,
  _onEditQuickMessage,
  listTo,
  listCc,
  messageDetails,
}) {
  const dispatch = useDispatch();
  const { quickMessages } = useSelector((state) => state.icChatWidgetReducer);

  const _onCancel = () => {
    $showQuickMessage(false);
  };

  const _onSendQuickMessage = (message) => {
    return () => {
      if (_.get(selectedMessage, "channel_type", "") === "email") {
        let tempTo = [];
        let tempCc = [];

        _.map(listTo, (item) => {
          tempTo.push({
            address: item,
          });
        });
        _.map(listCc, (item) => {
          tempCc.push({
            address: item,
          });
        });

        dispatch(
          sendMessageIC({
            interaction_id: _.get(selectedMessage, "interaction_id", ""),
            interaction_type: "text",
            channel_type: _.get(selectedMessage, "channel_type", ""),
            to: listTo,
            cc: listCc,
            data_mail: {
              attachments: [],
              cc_mails: tempCc,
              to_mails: tempTo,
              from_mail: _.get(selectedMessage, "page_social_id", ""),
              is_image_body: true,
              subject: "",
              message_id:
                _.get(messageDetails, "[0].reply_to") ||
                _.get(messageDetails, "[0].id"),
              server_email_type: _.get(
                messageDetails,
                "[0].server_email_type",
                "google"
              ),
              content: _.get(message, "content", ""),
            },
          })
        );
      } else {
        if (_.get(message, "message_type", "") === "file") {
          dispatch(
            sendMessageIC({
              interaction_id: _.get(selectedMessage, "interaction_id", ""),
              content_file_type: _.get(message, "content_type", ""),
              content_file_name: _.get(message, "file_name", ""),
              interaction_type:
                _.get(message, "content_type", "") !== "image" &&
                _.get(message, "content_type", "") !== "video" &&
                _.get(message, "content_type", "") !== "audio"
                  ? "file"
                  : _.get(message, "content_type", ""),
              content_file_url: _.get(message, "file_url", ""),
              content_download_url: _.get(message, "download_url", ""),
              size:
                _.get(selectedMessage, "channel_type", "") === "viber"
                  ? _.get(message, "size", "")
                  : null,
              duration: null,
              content_text: _.get(message, "content", ""),
              message_parent_id:
                _.get(selectedMessage, "channel_type", "") === "zalo" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "facebook_comment" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "instagram_comment"
                  ? messageDetails[messageDetails?.length - 1]?.message_id
                  : "",
              // duration:
              //   "recordSelected?.channel_type === 'viber' ? element?.duration : null",
            })
          );
        } else {
          dispatch(
            sendMessageIC({
              interaction_type: "text",
              content_text: _.get(message, "content", ""),
              content_file_url: "",
              content_file_type: "",
              interaction_id: _.get(selectedMessage, "interaction_id", ""),
              message_parent_id:
                _.get(selectedMessage, "channel_type", "") === "zalo" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "facebook_comment" ||
                _.get(selectedMessage, "channel_type", "") ===
                  "instagram_comment"
                  ? messageDetails[messageDetails?.length - 1]?.message_id
                  : "",
            })
          );
        }
      }

      _onCancel();
    };
  };

  const _onEdit = (message) => {
    return (e) => {
      e.stopPropagation();
      _onEditQuickMessage(message);
      _onCancel();
    };
  };

  const onSearchHandle = useCallback(
    (searchText) => {
      dispatch(
        getQuickMessagesIC({
          search_list: [
            {
              conjunction: "AND",
              name_field: "quick_typing_words",
              upper_bound: null,
              value_search: searchText.target.value,
            },
            {
              conjunction: "OR",
              name_field: "content",
              upper_bound: null,
              value_search: searchText.target.value,
            },
            {
              conjunction: "AND",
              name_field: "channel_type",
              upper_bound: null,
              value_search: _.get(selectedMessage, "channel_type", ""),
            },
          ],
        })
      );
    },
    [dispatch, selectedMessage]
  );

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 1000),
    [onSearchHandle]
  );

  const options = {
    replace: (domNode) => {
      if (_.get(domNode, "name", "") === "style") {
        return <style></style>;
      }

      if (
        _.get(domNode, "name", "") !== "" &&
        !allowedTags.includes(_.get(domNode, "name", ""))
      ) {
        return (
          <span>
            {"< "}
            {_.get(domNode, "name", "")}
            {" >"} {_.get(domNode, "children[0].data", "")}
          </span>
        );
      }
    },
  };

  return (
    <CustomModal
      title="Quick message"
      width={800}
      visible={showQuickMessage}
      onCancel={_onCancel}
      footer={null}
    >
      <Wrap>
        <WrapSearch>
          <Input.Search
            placeholder="Search..."
            onChange={debouncedSearchHandler}
          />
        </WrapSearch>

        {_.map(quickMessages, (message, idx) => (
          <Message
            key={_.get(message, "id", idx)}
            style={{
              borderBottom:
                idx !== quickMessages?.length - 1
                  ? "1px solid #ECECEC"
                  : "none",
            }}
            onClick={_onSendQuickMessage(message)}
          >
            <div>
              <span>{_.get(message, "quick_typing_words", "")}</span>

              {_.get(selectedMessage, "channel_type", "") === "email" ? (
                <div>{parse(_.get(message, "content", ""), options)}</div>
              ) : (
                <p>{_.get(message, "content", "")}</p>
              )}
              {_.get(message, "file_name", "") && (
                <div
                  style={{
                    display: "flex",
                    alignItems: "center",
                    marginTop: "8px",
                  }}
                >
                  <img src={FileIcon} alt="" style={{ marginRight: "4px" }} />
                  <p>{_.get(message, "file_name", "")}</p>
                </div>
              )}
            </div>

            {_.get(selectedMessage, "channel_type", "") !== "email" && (
              <Tooltip title="Edit">
                <img
                  style={{
                    width: "24px",
                    height: "24px",
                    marginLeft: "16px",
                  }}
                  src={Edit}
                  alt="edit"
                  onClick={_onEdit(message)}
                />
              </Tooltip>
            )}
          </Message>
        ))}
      </Wrap>
    </CustomModal>
  );
}

export default QuickMessage;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-body {
    border-radius: 10px;
    padding: 24px 20px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0px 0px;
    padding: 6.5px 24px;
    background: #f2f4f5;
  }

  .ant-modal-close-x {
    height: 36px;
    line-height: 36px;

    .anticon {
      color: #141414;
    }
  }

  .ant-input-group-wrapper {
    width: 50%;
  }
`;

const Wrap = styled.div``;

const WrapSearch = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 16px;
`;

const Message = styled.div`
  font-size: 16px;
  padding: 16px;
  display: flex;
  justify-content: space-between;
  word-break: break-word;
  overflow: hidden;

  span {
    color: #888;
  }

  p {
    color: #2c2c2c;
    margin-bottom: 0;
  }

  &:hover {
    background: #f2f4f5;
    border-radius: 6px;
    cursor: pointer;
  }
`;
