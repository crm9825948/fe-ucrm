import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";

import Button from "components/Button";

import { $addAgent, addAgentIC } from "redux/slices/icChatWidget";

function ModalGroup() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { userToAdd, addAgent } = useSelector(
    (state) => state.icChatWidgetReducer
  );

  const [form] = Form.useForm();

  const _onSubmit = (values) => {
    dispatch(
      addAgentIC({
        interaction_id: _.get(addAgent, "interaction_id", ""),
        note_content: _.get(values, "note", ""),
        username_dest: _.get(values, "username_dest", ""),
        username_source: _.get(addAgent, "username_source", ""),
      })
    );
  };

  const _onCancel = () => {
    dispatch(
      $addAgent({
        isLoading: false,
        visible: false,
        interaction_id: "",
        note_content: "",
        username_dest: "",
        username_source: "",
      })
    );
    form.resetFields();
  };

  return (
    <ModalCustom
      title="Add agent"
      visible={addAgent.visible}
      footer={null}
      width={600}
      onCancel={_onCancel}
      afterClose={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label="Online agent"
          name="username_dest"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={userToAdd}
            placeholder={t("common.placeholderSelect")}
            optionFilterProp="label"
          />
        </Form.Item>

        <Form.Item label="Note" name="note">
          <Input.TextArea placeholder={t("common.placeholderInput")} rows={4} />
        </Form.Item>

        <WrapButton label=" ">
          <Button
            title={t("common.save")}
            type="primary"
            htmlType="submit"
            loading={addAgent.isLoading}
          />
          <Button title={t("common.cancel")} onClick={_onCancel} />
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalGroup);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
