import React, { useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Divider from "antd/lib/divider";
import Space from "antd/lib/space";
import Input from "antd/lib/input";
import { PlusOutlined } from "@ant-design/icons";

import Button from "components/Button";

import {
  getTagsIC,
  addTagIC,
  createTagIC,
  $addTag,
} from "redux/slices/icChatWidget";

function ModalGroup() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { tags, addTag } = useSelector((state) => state.icChatWidgetReducer);
  const { userDetail } = useSelector((state) => state.userReducer);

  const [form] = Form.useForm();
  const inputRef = useRef(null);
  const [name, $name] = useState("");

  const onNameChange = (event) => {
    $name(event.target.value);
  };

  const _onAddTag = (e) => {
    e.preventDefault();

    if (name.trim()) {
      dispatch(
        createTagIC({
          is_active: true,
          name: name.trim(),
        })
      );
    }

    $name("");
    setTimeout(() => {
      inputRef.current?.focus();
    }, 0);
  };

  const _onSubmit = (values) => {
    dispatch(
      addTagIC({
        id: _.get(values, "id", ""),
        interaction_id: _.get(addTag, "interaction_id", ""),
        interactiondetail_id: null,
      })
    );
  };

  const _onCancel = () => {
    dispatch(
      $addTag({
        visible: false,
        interaction_id: "",
      })
    );
    $name("");
    form.resetFields();
  };

  useEffect(() => {
    if (addTag.visible) {
      dispatch(getTagsIC());
    }
  }, [dispatch, userDetail, addTag.visible]);

  return (
    <ModalCustom
      title="Add tag"
      visible={addTag.visible}
      footer={null}
      width={600}
      onCancel={_onCancel}
      afterClose={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label="Tag name"
          name="id"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={tags}
            placeholder={t("common.placeholderSelect")}
            optionFilterProp="label"
            dropdownRender={(menu) => (
              <>
                {menu}
                <Divider
                  style={{
                    margin: "8px 0",
                  }}
                />
                <Space
                  style={{
                    padding: "0 8px 4px",
                  }}
                >
                  <Input
                    placeholder="New tag name"
                    ref={inputRef}
                    value={name}
                    onChange={onNameChange}
                  />
                  <Button
                    type="text"
                    icon={<PlusOutlined />}
                    onClick={_onAddTag}
                  >
                    Add tag
                  </Button>
                </Space>
              </>
            )}
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button title={t("common.save")} type="primary" htmlType="submit" />
          <Button title={t("common.cancel")} onClick={_onCancel} />
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalGroup);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
