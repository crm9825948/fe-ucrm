import { useState, useMemo, useCallback, useEffect } from "react";
import styled from "styled-components/macro";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";

import Input from "antd/lib/input";
import Dropdown from "antd/lib/dropdown";
import Menu from "antd/lib/menu";

import {
  PlusOutlined,
  ArrowRightOutlined,
  CheckOutlined,
  // InfoOutlined,
  MoreOutlined,
  DownOutlined,
  UpOutlined,
  SearchOutlined,
  LeftOutlined,
} from "@ant-design/icons";

import ImgConfirm from "assets/icons/common/confirm.png";

import {
  disableBotIC,
  completeConversationIC,
  $addAgent,
  $transferAgent,
  getUserToAddIC,
  searchMessagesIC,
  searchMessagesICResult,
  getIndexMessageIC,
  $recordPerPageIC,
  $currentPageIC,
} from "redux/slices/icChatWidget";

import Button from "components/Button";
import Screen from "./Screen";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import ModalAddAgent from "./Modal/AddAgent";
import ModalTransferAgent from "./Modal/TransferAgent";

function findNextDivisibleBy20(number) {
  return number + (20 - (number % 20));
}

function ChatScreen({
  selectedMessage,
  assignedMessages,
  isSearch,
  $isSearch,
  disabled,
  showDetails,
  $showDetails,
}) {
  const dispatch = useDispatch();

  const { searchMessages, indexMessage, messageDetails } = useSelector(
    (state) => state.icChatWidgetReducer
  );

  const [showAction, $showAction] = useState(false);
  const [dataConfirm, $dataConfirm] = useState({});
  const [showConfirm, $showConfirm] = useState(false);
  const [selectedSearchMessage, $selectedSearchMessage] = useState({
    id: "",
    position: 0,
  });

  const _onComplete = () => {
    $showAction(false);
    $showConfirm(true);
    $dataConfirm({
      interaction_id: _.get(selectedMessage, "interaction_id", ""),
    });
  };

  const _onAddAgent = () => {
    $showAction(false);
    dispatch(
      $addAgent({
        isLoading: false,
        visible: true,
        interaction_id: _.get(selectedMessage, "interaction_id", ""),
        note_content: "",
        username_dest: "",
        username_source: localStorage.getItem("icIntegration_username"),
      })
    );

    dispatch(
      getUserToAddIC({
        interaction_id: _.get(selectedMessage, "interaction_id", ""),
        type: "add",
      })
    );
  };

  const _onTransferAgent = () => {
    $showAction(false);
    dispatch(
      $transferAgent({
        isLoading: false,
        visible: true,
        interaction_id: _.get(selectedMessage, "interaction_id", ""),
        note_content: "",
        username_dest: "",
        username_source: localStorage.getItem("icIntegration_username"),
      })
    );

    dispatch(
      getUserToAddIC({
        interaction_id: _.get(selectedMessage, "interaction_id", ""),
        type: "transfer",
      })
    );
  };

  const actions = [
    {
      label: (
        <div>
          <PlusOutlined style={{ color: "#1890FF", marginRight: "4px" }} /> Add
          agent
        </div>
      ),
      key: "add-agent",
      action: _onAddAgent,
    },
    {
      label: (
        <div>
          <ArrowRightOutlined
            style={{ color: "#FF5B05", marginRight: "4px" }}
          />{" "}
          Transfer
        </div>
      ),
      key: "transfer",
      action: _onTransferAgent,
    },
    {
      label: (
        <div>
          <CheckOutlined style={{ color: "#389E0D", marginRight: "4px" }} />{" "}
          Make as complete
        </div>
      ),
      key: "make-complete",
      action: _onComplete,
    },
    // {
    //   label: (
    //     <div>
    //       <InfoOutlined style={{ color: "#597EF7", marginRight: "4px" }} />{" "}
    //       Information
    //     </div>
    //   ),
    //   key: "information",
    //   action: () => {},
    // },
  ];

  const menuAction = (
    <Menu>
      {_.map(actions, (item) => (
        <Menu.Item
          key={item.key}
          // onClick={() => {
          //   $showAction(false);

          // }}
          onClick={item.action}
        >
          {item.label}
        </Menu.Item>
      ))}
    </Menu>
  );

  const _onStopBot = () => {
    dispatch(
      disableBotIC({
        interaction_id: _.get(selectedMessage, "interaction_id", ""),
      })
    );
  };

  const onSearchHandle = useCallback(
    (searchText) => {
      if (_.get(searchText, "target.value")) {
        $isSearch(true);
        dispatch(
          searchMessagesIC({
            channel_type: _.get(selectedMessage, "channel_type", ""),
            interaction_id: _.get(selectedMessage, "interaction_id", ""),
            is_all: true,
            search_text: searchText.target.value,
            page_social_id: _.get(selectedMessage, "page_social_id", ""),
            user_social_id: _.get(selectedMessage, "user_social_id", ""),
          })
        );
      } else {
        dispatch(searchMessagesICResult([]));
        $isSearch(false);
        $selectedSearchMessage({
          id: "",
          position: 0,
        });
      }
    },
    [$isSearch, dispatch, selectedMessage]
  );

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 1000),
    [onSearchHandle]
  );

  const _onNextSearch = () => {
    if (selectedSearchMessage.position !== searchMessages?.length - 1) {
      dispatch(
        getIndexMessageIC({
          interaction_detail_id:
            searchMessages[selectedSearchMessage.position + 1]?.id,
          interaction_id: _.get(selectedMessage, "interaction_id"),
        })
      );
    } else {
      dispatch(
        getIndexMessageIC({
          interaction_detail_id: searchMessages[0]?.id,
          interaction_id: _.get(selectedMessage, "interaction_id"),
        })
      );
    }
  };

  const _onPrevSearch = () => {
    if (selectedSearchMessage.position !== 0) {
      dispatch(
        getIndexMessageIC({
          interaction_detail_id:
            searchMessages[selectedSearchMessage.position - 1]?.id,
          interaction_id: _.get(selectedMessage, "interaction_id"),
        })
      );
    } else {
      dispatch(
        getIndexMessageIC({
          interaction_detail_id: searchMessages[searchMessages.length - 1]?.id,
          interaction_id: _.get(selectedMessage, "interaction_id"),
        })
      );
    }
  };

  useEffect(() => {
    if (searchMessages?.length > 0) {
      $selectedSearchMessage({
        id: searchMessages[searchMessages.length - 1]?.id,
        position: searchMessages.length - 1,
      });
      const element = document.getElementById(
        searchMessages[searchMessages.length - 1]?.id
      );
      if (element) {
        element.scrollIntoView({ behavior: "smooth" });
      }
    } else {
      $selectedSearchMessage({
        id: "",
        position: 0,
      });
    }
  }, [searchMessages]);

  useEffect(() => {
    if (indexMessage) {
      if (typeof messageDetails[indexMessage - 1] === "undefined") {
        dispatch($recordPerPageIC(findNextDivisibleBy20(indexMessage)));
        dispatch($currentPageIC(1));
      } else {
        setTimeout(() => {
          $selectedSearchMessage({
            id: messageDetails[indexMessage - 1]?.id,
            position: searchMessages.findIndex(
              (item) => item?.id === messageDetails[indexMessage - 1]?.id
            ),
          });
          const element = document.getElementById(
            messageDetails[indexMessage - 1]?.id
          );
          if (element) {
            element.scrollIntoView({ behavior: "smooth" });
          }
        }, 500);
      }
    }
  }, [dispatch, indexMessage, messageDetails, searchMessages]);

  return (
    <Wrapper>
      <Actions>
        {showDetails && (
          <div
            onClick={() => $showDetails(false)}
            style={{
              width: "32px",
              height: "32px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              background: "#F5F5F5",
              borderRadius: "2px",
              cursor: "pointer",
            }}
          >
            <LeftOutlined />
          </div>
        )}
        <Input
          placeholder="Search..."
          onChange={debouncedSearchHandler}
          addonAfter={
            <>
              {searchMessages?.length > 0 ? (
                <div>
                  <span>
                    Result {selectedSearchMessage.position + 1}&nbsp;/&nbsp;
                    {searchMessages?.length}
                  </span>
                  <DownOutlined
                    style={{ marginLeft: "16px", cursor: "pointer" }}
                    onClick={_onNextSearch}
                  />
                  <UpOutlined
                    style={{ marginLeft: "8px", cursor: "pointer" }}
                    onClick={_onPrevSearch}
                  />
                </div>
              ) : searchMessages?.length === 0 && isSearch ? (
                <span>Result 0 / 0</span>
              ) : (
                <SearchOutlined />
              )}
            </>
          }
        />
        {/* <Button title="Create ticket" type="primary" /> */}
        {assignedMessages?.find(
          (item) =>
            _.get(item, "interaction_id", "") ===
              _.get(selectedMessage, "interaction_id") &&
            _.get(item, "is_botprogress", false)
        ) && <Button title="Stop BOT" onClick={_onStopBot} />}

        {_.get(selectedMessage, "owner_username", "") ===
          localStorage.getItem("icIntegration_username") &&
          !disabled && (
            <Dropdown
              visible={showAction}
              onVisibleChange={() => $showAction(!showAction)}
              overlay={menuAction}
              trigger={["click"]}
              placement="bottomRight"
            >
              <div
                style={{
                  width: "32px",
                  minWidth: "32px",
                  height: "32px",
                  border: "1px solid #d9d9d9",
                  borderRadius: "2px",
                  boxShadow: "0px 2px 0px rgba(0, 0, 0, 0.016)",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  fontSize: "20px",
                  cursor: "pointer",
                }}
              >
                <MoreOutlined />
              </div>
            </Dropdown>
          )}
      </Actions>

      <Screen
        selectedMessage={selectedMessage}
        selectedSearchMessage={selectedSearchMessage}
        disabled={disabled}
      />

      <ModalConfirm
        title="This conversation will be completed if you take this action"
        decs="Are you sure you want to close this conversation ?"
        method={completeConversationIC}
        data={dataConfirm}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={$showConfirm}
      />

      <ModalAddAgent />
      <ModalTransferAgent />
    </Wrapper>
  );
}

export default ChatScreen;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
`;

const Actions = styled.div`
  display: flex;
  column-gap: 8px;
  margin-bottom: 16px;
`;
