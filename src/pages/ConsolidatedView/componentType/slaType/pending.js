import React from "react";
import styled from "styled-components";
import link from "assets/icons/consolidatedViewSettings/link.png";
import link1 from "assets/icons/consolidatedViewSettings/link1.png";
import deadline from "assets/icons/common/deadline.png";

const Pending = (props) => {
  const { data } = props;

  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear(),
      hour = d.getHours(),
      minite = d.getMinutes();

    if (month.length < 2) month = "0" + month.toString();
    if (day.length < 2) day = "0" + day.toString();
    if (minite < 10) minite = "0" + minite.toString();
    return `${[year, month, day].join("-")} ${[hour, minite].join(":")}`;
  }

  return (
    <SLAItem>
      <div className="title-component">
        <div className="title">
          {data.sla_title} <span className="badge">Pending</span>
        </div>
        <div className="rule-name">
          <span>Rule:</span>
          {data.rule_name}
        </div>
      </div>
      <div className="rule-name is_closed">
        <img
          src={deadline}
          alt=""
          style={{ width: "17px", marginRight: "10px" }}
        />{" "}
        <div> Pending at: {formatDate(data.pending_time)}</div>
      </div>
      {data.is_dependent_on_children === false &&
      data.is_dependent_on_parent === false &&
      data &&
      data.sla_instances &&
      data.sla_instances.length > 0 ? (
        <img alt="" src={link} style={{ width: "15px" }} className="link" />
      ) : data && data.sla_instances && data.sla_instances.length > 0 ? (
        <img alt="" src={link1} style={{ width: "15px" }} className="link" />
      ) : (
        ""
      )}
    </SLAItem>
  );
};

export default Pending;

const SLAItem = styled.div`
  padding: 16px;
  box-shadow: inset 0px -1px 0px #f0f0f0;
  /* background: #fafafa; */
  margin-bottom: 16px;
  border: 1px solid #d6d6d6;
  /* sh v3 */

  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 10px;
  position: relative;
  .title-component {
    /* background-color: white; */
    /* padding: 12px; */
    /* border-radius: 8px; */
    margin-bottom: 16px;
    border-bottom: 1px solid #cccccc;
  }
  .link {
    position: absolute;
    bottom: -40px;
    left: 10px;
    z-index: 999;
  }

  .badge {
    width: 70px;
    height: 22px;
    left: 283px;
    top: 0px;

    /* Dust Red / 6 */

    background: #ffd591;
    display: flex;
    justify-content: center;
    align-items: center;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 20px;
    color: #fa541c;
    /* identical to box height, or 143% */

    text-align: center;

    /* Neutral/1 */

    color: #ffffff;
    border-radius: 10px;
    margin-left: 8px;
  }
  .title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 21px */

    display: flex;
    align-items: center;
    letter-spacing: 0.01em;

    /* text - main */

    color: #252424;
  }
  .rule-name {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 130%;
    /* identical to box height, or 21px */

    display: flex;
    align-items: center;
    letter-spacing: 0.01em;

    /* text - main */
    color: #252424;
    span {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 14px;
      line-height: 22px;
      /* identical to box height, or 157% */

      display: flex;
      align-items: center;

      /* text */

      color: #252424;
      margin-right: 10px;
    }
    margin-bottom: 8px;
  }
  .escalate-time {
    display: flex;
    .item-time {
      margin-right: 8px;
      display: flex;
      flex-direction: column;
      align-items: center;
      .time {
        width: 55px;
        height: 55px;
        border-radius: 5px;
        color: #237804;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #d9f7be;
        font-style: normal;
        font-family: var(--roboto-400);
        font-size: 22px;
        line-height: 22px;
        /* identical to box height, or 100% */

        display: flex;
        align-items: center;
        text-align: center;

        /* Dust Red / 6 */

        color: #237804;
      }
      .decs {
        margin-top: 4px;
        font-style: normal;
        font-family: var(--roboto-400);
        font-size: 14px;
        line-height: 20px;
        /* identical to box height, or 143% */

        display: flex;
        align-items: center;

        /* Neutral / 7 */

        color: #8c8c8c;
      }
    }
  }
  .escalation_status {
    margin-bottom: 8px;
  }
  .is_closed {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 157% */

    display: flex;
    align-items: center;

    /* xam -old */

    color: #2c2c2c;
    margin-bottom: 8px;
  }
`;
