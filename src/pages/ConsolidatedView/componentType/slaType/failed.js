import React, { useState, useEffect } from "react";
import styled from "styled-components";
import link from "assets/icons/consolidatedViewSettings/link.png";
import link1 from "assets/icons/consolidatedViewSettings/link1.png";
import deadline from "assets/icons/common/deadline.png";
import fail from "assets/icons/common/fail.png";

const Failed = (props) => {
  const { data, reloadSLA } = props;

  const [day, setDay] = useState(0);
  const [hour, setHour] = useState(0);
  const [minute, setMinute] = useState(0);
  const [second, setSecond] = useState(0);

  // const renderEscalate = () => {
  //   return (
  //     <div className="escalate-time">
  //       <div className="item-time">
  //         <div className="time">{Math.abs(day)}</div>
  //         <div className="decs">Day</div>
  //       </div>
  //       <div className="item-time">
  //         <div className="time">{Math.abs(hour)}</div>
  //         <div className="decs">Hour</div>
  //       </div>
  //       <div className="item-time">
  //         <div className="time">{Math.abs(minute)}</div>
  //         <div className="decs">Minute</div>
  //       </div>
  //       <div className="item-time">
  //         <div className="time">{Math.abs(second)}</div>
  //         <div className="decs">Second</div>
  //       </div>
  //     </div>
  //   );
  // };

  useEffect(() => {
    let days = 0;
    let minutes = 0;
    let seconds = 0;
    let hours = 0;

    let a = setInterval(function () {
      let now = new Date().getTime();
      let distance = new Date(data.success_milestone).getTime();
      distance = distance - now + 1000;
      if (distance < 1000) {
        clearInterval(a);
        reloadSLA();
      } else {
        days = Math.floor(distance / (1000 * 60 * 60 * 24));
        hours = Math.floor(
          (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
        );
        minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        seconds = Math.floor((distance % (1000 * 60)) / 1000);
        setDay(days);
        setHour(hours);
        setMinute(minutes);
        setSecond(seconds);
      }
    }, 1000);
    // eslint-disable-next-line
  }, []);

  const [hour1, setHour1] = useState(0);
  const [minite1, setMinute1] = useState(0);
  const [second1, setSecond1] = useState(0);
  const [day1, setDay1] = useState(0);

  const renderEscalateFail = (success_milestone) => {
    setInterval(function () {
      let now = new Date().getTime();
      let distance = new Date(success_milestone).getTime();
      distance = now - distance + 1000;
      let days = Math.floor(distance / (1000 * 60 * 60 * 24));
      let hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      let seconds = Math.floor((distance % (1000 * 60)) / 1000);
      setDay1(days);
      setSecond1(seconds);
      setMinute1(minutes);
      setHour1(hours);
    }, 1000);
    // return `${Math.abs(day1)}d:${Math.abs(hour1)}h:${Math.abs(
    //   minite1
    // )}min:${Math.abs(second1)}s`;

    return (
      <div className="escalate-time">
        <div className={`${day < 0 ? "item-time-1" : "item-time"}`}>
          <div className="time">{Math.abs(day1)}</div>
          <div className="decs">Day</div>
        </div>
        <div className={`${hour < 0 ? "item-time-1" : "item-time"}`}>
          <div className="time">{Math.abs(hour1)}</div>
          <div className="decs">Hour</div>
        </div>
        <div className={`${minute < 0 ? "item-time-1" : "item-time"}`}>
          <div className="time">{Math.abs(minite1)}</div>
          <div className="decs">Minute</div>
        </div>
        <div className={`${second < 0 ? "item-time-1" : "item-time"}`}>
          <div className="time">{Math.abs(second1)}</div>
          <div className="decs">Second</div>
        </div>
      </div>
    );
  };

  function formatDate(date) {
    var d = new Date(date),
      month = "" + (d.getMonth() + 1),
      day = "" + d.getDate(),
      year = d.getFullYear(),
      hour = d.getHours(),
      minite = d.getMinutes();

    if (month.length < 2) month = "0" + month.toString();
    if (day.length < 2) day = "0" + day.toString();
    if (minite < 10) minite = "0" + minite.toString();
    return `${[year, month, day].join("-")} ${[hour, minite].join(":")}`;
  }

  return (
    <>
      <SLAItem escalation_status={data.escalate_rules.length > 0}>
        <div className="title-component">
          <div className="title">
            <span>{data.sla_title} </span>
            <span className="badge">Failed</span>
          </div>
          <div className="rule-name">
            <span>Rule:</span>
            {data.rule_name}
          </div>
          {data.hasOwnProperty("escalation_status") ? (
            <div className="rule-name escalation_status">
              <span>Escalation status:</span> {data.escalation_status}
            </div>
          ) : (
            ""
          )}
        </div>
        {data.hasOwnProperty("is_closed") && data.is_closed === false ? (
          <>
            <div
              style={{
                marginBottom: "20px",
                float: "left",
                marginRight: "20px",
              }}
            >
              {renderEscalateFail(data.success_milestone)}
            </div>
          </>
        ) : data.hasOwnProperty("is_closed") && data.is_closed === true ? (
          <div className="rule-name is_closed">
            <img
              src={fail}
              alt=""
              style={{ width: "17px", marginRight: "10px" }}
            />{" "}
            <div style={{ marginRight: "10px" }}>
              {" "}
              Failed at: {formatDate(data.success_milestone)}
            </div>{" "}
            |
            <div style={{ marginLeft: "10px", marginRight: "5px" }}>
              Closed at: {data.modified_date}
            </div>{" "}
          </div>
        ) : (
          ""
        )}
        <div className="rule-name is_closed">
          <img
            src={deadline}
            alt=""
            style={{ width: "17px", marginRight: "10px" }}
          />{" "}
          <div> Deadline: {formatDate(data.success_milestone)}</div>
        </div>

        {data.is_dependent_on_children === false &&
        data.is_dependent_on_parent === false &&
        data &&
        data.sla_instances &&
        data.sla_instances.length > 0 ? (
          <img alt="" src={link} style={{ width: "15px" }} className="link" />
        ) : data && data.sla_instances && data.sla_instances.length > 0 ? (
          <img alt="" src={link1} style={{ width: "15px" }} className="link" />
        ) : (
          ""
        )}
      </SLAItem>
    </>
  );
};

export default Failed;

const SLAItem = styled.div`
  padding: 8px;
  /* box-shadow: inset 0px -1px 0px #f0f0f0; */
  /* background: #f2f4f5;
   */
  padding-bottom: ${(props) => (props.escalation_status ? "64px" : "8px")};
  border: 1px solid #d6d6d6;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  margin-bottom: 16px;
  border-radius: 10px;
  position: relative;
  .title-component {
    /* background-color: white; */
    /* padding: 12px; */
    /* border-radius: 8px; */
    margin-bottom: 16px;
    border-bottom: 1px solid #cccccc;
    padding-bottom: 8px;
  }
  .line {
    position: absolute;
    top: 130px;
    left: -16px;
  }
  .link {
    position: absolute;
    bottom: -40px;
    left: 10px;
    z-index: 999;
  }
  .verLink {
    position: absolute;
    /* bottom: -40px; */
    left: -16px;
    top: 40px;
  }

  .badge {
    width: 70px;
    height: 22px;
    left: 283px;
    top: 0px;

    /* Dust Red / 6 */

    background: #f5222d;
    display: flex;
    justify-content: center;
    align-items: center;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    text-align: center;

    /* Neutral/1 */

    color: #ffffff;
    border-radius: 10px;
    margin-left: 8px;
  }
  .title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 21px */

    display: flex;
    align-items: center;
    letter-spacing: 0.01em;

    /* text - main */

    color: #252424;
    display: flex;
    /* justify-content: space-between; */
  }
  .rule-name {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 130%;
    /* identical to box height, or 21px */

    display: flex;
    align-items: center;
    letter-spacing: 0.01em;

    /* text - main */

    color: #252424;
    span {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 14px;
      line-height: 22px;
      /* identical to box height, or 157% */

      display: flex;
      align-items: center;

      /* text */

      color: #252424;
      margin-right: 5px;
    }
  }
  .escalate-time {
    display: flex;
    .item-time {
      margin-right: 8px;
      display: flex;
      flex-direction: column;
      align-items: center;
      .time {
        width: 55px;
        height: 55px;
        border-radius: 5px;
        color: #f5222d;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #fff1f0;
        font-style: normal;
        font-family: var(--roboto-400);
        font-size: 22px;
        line-height: 22px;
        /* identical to box height, or 100% */

        display: flex;
        align-items: center;
        text-align: center;

        /* Dust Red / 6 */

        color: #f5222d;
      }
      .decs {
        margin-top: 4px;
        font-style: normal;
        font-family: var(--roboto-400);
        font-size: 14px;
        line-height: 20px;
        /* identical to box height, or 143% */

        display: flex;
        align-items: center;

        /* Neutral / 7 */

        color: #8c8c8c;
      }
    }
  }
  .escalation_status {
    /* margin-bottom: 8px; */
  }
  .is_closed {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 157% */

    display: flex;
    align-items: center;

    /* xam -old */

    color: #2c2c2c;
    margin-bottom: 10px;
  }
`;
