import React, { useRef } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getListArticle,
  updateDataSearch,
  updateLoadMore,
} from "redux/slices/enhancementComponent";
import styled from "styled-components";

import { Radio, Dropdown, Checkbox, Input, Spin, Row } from "antd";
import FilterIcon from "assets/icons/knowledgeEnhancement/fillter.svg";
import InfiniteScroll from "react-infinite-scroll-component";
import moment from "moment";

import EmptyImg from "assets/icons/knowledgeEnhancement/empty.png";
import ItemArticle from "pages/EnhancementView/ItemArticle";
import { useTranslation } from "react-i18next";
import _ from "lodash";

const { Search } = Input;

const Enhancement = ({ objectId, recordID, data }) => {
  const dateFormatSecond = "YYYY-MM-DD HH:mm:ss";
  const dispatch = useDispatch();
  const { dataSearch, objectSearch, isLoadMore } = useSelector(
    (state) => state.enhancementComponentReducer
  );
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { t } = useTranslation();
  const [config, setConfig] = useState({});
  const { article_setting } = config;
  const [open, setOpen] = useState(false);
  const [listArticle, setListArticle] = useState([]);
  const [objectFilter, setObjecFilter] = useState({
    text_fields: ["title", "body", "tag", "article_id"],
    created_date: "all",
  });
  const [isSearch, setIsSearch] = useState(false);

  const ref = useRef(null);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === data.obj_article) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, data]);

  useEffect(() => {
    dispatch(
      getListArticle({
        object_id: data.obj_article,
        data: {
          page: 0,
          size: 10,
          text_fields: ["title", "body", "tag", "article_id"],
          source_record_id: recordID,
          component_id: data._id,
        },
      })
    );

    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (objectSearch.records) {
      if (isLoadMore) {
        const temp = listArticle.concat(objectSearch.records);
        setListArticle(temp);
      } else {
        setListArticle(objectSearch.records);
      }
    }
    //eslint-disable-next-line
  }, [objectSearch]);

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  const renderListArticle = (list, setting) => {
    if (list && setting) {
      return list.map((article, index) => {
        return (
          <ItemArticle
            key={index}
            rf={ref}
            config={config}
            setting={setting}
            article={article}
          />
        );
      });
    }
  };

  const handleSearch = (search) => {
    let tem = { ...search };
    if (!tem.text_search) {
      delete tem.text_search;
    }
    if (tem.created_date === "all") {
      delete tem.created_date;
    } else {
      let objectTime = {};
      switch (tem.created_date) {
        case "day":
          objectTime = {
            gte: moment()
              .subtract(1, "day")
              .startOf("day")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "day")
              .endOf("day")
              .format(dateFormatSecond),
          };
          break;
        case "week":
          objectTime = {
            gte: moment()
              .subtract(1, "week")
              .startOf("isoWeek")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "week")
              .endOf("isoWeek")
              .format(dateFormatSecond),
          };
          break;
        case "month":
          objectTime = {
            gte: moment()
              .subtract(1, "month")
              .startOf("month")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "month")
              .endOf("month")
              .format(dateFormatSecond),
          };
          break;
        case "year":
          objectTime = {
            gte: moment()
              .subtract(1, "year")
              .startOf("year")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "year")
              .endOf("year")
              .format(dateFormatSecond),
          };
          break;
        default:
          break;
      }
      tem.created_date = objectTime;
    }
    if (tem) {
      dispatch(
        getListArticle({
          object_id: data.obj_article,
          data: tem,
        })
      );
    }
  };

  const loadMoreArticle = () => {
    dispatch(updateLoadMore(true));
    let next = dataSearch.page + 1;
    dispatch(
      updateDataSearch({
        page: next,
      })
    );
    if (!isSearch) {
      dispatch(
        getListArticle({
          object_id: data.obj_article,
          data: {
            page: next,
            size: 10,
            text_fields: ["title", "body", "tag", "article_id"],
            source_record_id: recordID,
            component_id: data._id,
          },
        })
      );
    } else {
      let temp = { ...dataSearch, page: next };
      handleSearch(temp);
    }
  };

  const menu = (
    <PopupWrap>
      <TitlePopup>{t("knowledgeBase.sortBy")}</TitlePopup>
      <Checkbox.Group
        defaultValue={["title", "body", "tag", "article_id"]}
        value={objectFilter.text_fields}
        onChange={(value) => {
          setObjecFilter((prev) => ({
            ...prev,
            text_fields: value,
          }));
        }}
      >
        <Checkbox value="title">{t("knowledgeBase.title")}</Checkbox>
        <Checkbox value="body">{t("knowledgeBase.content")}</Checkbox>
        <Checkbox value="tag">{t("knowledgeBase.tag")}</Checkbox>
        <Checkbox value="article_id">ID</Checkbox>
      </Checkbox.Group>
      <Title>{t("knowledgeBase.time")}</Title>
      <CustomRadio
        defaultValue="all"
        value={objectFilter.created_date}
        onChange={(value) => {
          setObjecFilter((prev) => ({
            ...prev,
            created_date: value.target.value,
          }));
        }}
        color={hexToRGB(defaultBrandName.theme_color)}
      >
        <Radio.Button value="all">{t("knowledgeBase.all")}</Radio.Button>
        <Radio.Button value="day">{t("knowledgeBase.day")}</Radio.Button>
        <Radio.Button value="week">{t("knowledgeBase.week")}</Radio.Button>
        <Break />
        <Radio.Button value="month">{t("knowledgeBase.month")}</Radio.Button>
        <Radio.Button value="year">{t("knowledgeBase.year")}</Radio.Button>
      </CustomRadio>
      <ButtonWrap>
        <Button
          onClick={() => {
            setObjecFilter({
              text_fields: ["title", "body", "tag", "article_id"],
              created_date: "all",
            });
            dispatch(
              updateDataSearch({
                text_fields: ["title", "body", "tag", "article_id"],
                created_date: "all",
              })
            );
            setOpen(false);
          }}
        >
          {t("common.cancel")}
        </Button>
        <Button
          onClick={() => {
            dispatch(updateLoadMore(false));
            let temp = {
              ...dataSearch,
              text_fields: objectFilter.text_fields,
              created_date: objectFilter.created_date,
              page: 0,
            };

            if (!isSearch) {
              handleSearch({
                ...temp,
                source_record_id: recordID,
                component_id: data._id,
              });
            } else {
              handleSearch(temp);
            }
            dispatch(
              updateDataSearch({
                text_fields: objectFilter.text_fields,
                created_date: objectFilter.created_date,
                page: 0,
              })
            );
            setOpen(false);
          }}
        >
          {t("knowledgeBase.done")}
        </Button>
      </ButtonWrap>
    </PopupWrap>
  );
  return (
    <Wrap ref={ref} color={hexToRGB(defaultBrandName.theme_color)}>
      <Header>{data.name}</Header>
      <FilterWrap>
        <CustomInputSearch
          allowClear
          placeholder="Search..."
          onSearch={(e) => {
            setIsSearch(true);
            dispatch(updateLoadMore(false));
            let temp = { ...dataSearch, text_search: e, page: 0 };
            dispatch(
              updateDataSearch({ ...dataSearch, text_search: e, page: 0 })
            );
            handleSearch(temp);
          }}
        />
        <CustomDropdown
          visible={open}
          overlay={menu}
          placement="bottomCenter"
          trigger="click"
          overlayClassName="object-view"
          onVisibleChange={() => setOpen(!open)}
        >
          <ButtonFilter>
            <img src={FilterIcon} alt="filter" />
          </ButtonFilter>
        </CustomDropdown>
        <Customspan>
          {listArticle.length} of {_.get(objectSearch, "total", 0)} articles
        </Customspan>
      </FilterWrap>
      {listArticle.length === 0 ? (
        <NoData>
          <DataWrap>
            <span>
              <img src={EmptyImg} alt="no data " />
            </span>
            <p>{t("knowledgeBase.noArticle")}</p>
          </DataWrap>
        </NoData>
      ) : (
        <ContentWrap id="scrollableDivKnowledgeBase">
          <InfiniteScroll
            hasMore={listArticle.length < objectSearch.total}
            dataLength={listArticle.length}
            loader={<Spin />}
            next={loadMoreArticle}
            scrollableTarget="scrollableDivKnowledgeBase"
          >
            <Row gutter={[24, 0]}>
              {renderListArticle(listArticle, article_setting)}
            </Row>
          </InfiniteScroll>
        </ContentWrap>
      )}
    </Wrap>
  );
};

export default Enhancement;
const Wrap = styled.div`
  height: 100%;
  width: 100%;
  padding: 24px;
  color: #2c2c2c;
  line-height: 22px;
  background: #fff;
`;
const ContentWrap = styled.div`
  width: 100%;
  max-height: calc(100% - 90px);
  overflow: auto;
  overflow-x: hidden;
  .infinite-scroll-component {
    overflow: unset !important;
  }
`;

const Title = styled.div`
  font-size: 18px;
  font-family: var(--roboto-500);
  margin: 10px;
  display: flex;
  align-items: flex-end;
  span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }

  img {
    margin-bottom: 3px;
    margin-left: 6px;
    width: 12px !important;
  }
`;

const FilterWrap = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 16px;
`;

const CustomInputSearch = styled(Search)`
  flex: 1;
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 30px;
  }
  button {
    height: 40px;
  }
`;

const ButtonFilter = styled.div`
  border: 1px solid #d9d9d9 !important;
  width: 48px;
  cursor: pointer;
  margin-left: 16px;
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  text-align: center;

  color: #2c2c2c;

  display: flex;
  justify-content: center;
  align-items: center;

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const PopupWrap = styled.div`
  padding: 16px;
  font-size: 16px;
  line-height: 22px;
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main}!important;
  }
  .ant-checkbox + span {
    font-size: 16px;
  }
  .ant-checkbox + span {
    margin-bottom: 8px;
  }
`;

const TitlePopup = styled.div`
  font-family: var(--roboto-500);
  color: #2c2c2c;
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 8px;
`;

const CustomRadio = styled(Radio.Group)`
  .ant-radio-button-wrapper {
    border: 1px solid #d9d9d9 !important;
    margin-right: 8px;
    border-radius: 5px;
    font-size: 16px;
    :hover {
      border-color: ${(props) => props.theme.main} !important ;
    }
  }
  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    background: ${(props) => props.color};
    border-color: ${(props) => props.theme.main} !important ;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-input:focus + .ant-radio-inner {
    box-shadow: none;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper:not(:first-child)::before {
    width: 0;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):focus-within {
    box-shadow: none;
  }
`;

const Break = styled.div`
  margin-bottom: 8px;
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: end;
  margin-top: 16px;
`;

const Button = styled.div`
  width: 70px;
  height: 32px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 16px;
  cursor: pointer;
  transition: all 0.5s;

  :last-child {
    border-color: ${(props) => props.theme.darker};
    background: ${(props) => props.theme.darker};
    color: #fff;
  }

  :hover {
    border-color: ${(props) => props.theme.darker};
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const Header = styled.div`
  font-family: var(--roboto-500);
  font-size: 24px;
  margin-bottom: 16px;
`;
const NoData = styled.div`
  display: flex;
  justify-content: center;

  img {
    width: 100px;
  }
`;
const DataWrap = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  flex-direction: column;
  p {
    font-size: 16px;
    line-height: 22px;
    margin-top: 8px;
    span {
      color: ${(props) => props.theme.main};
    }
  }
`;
const Customspan = styled.span`
  color: #637381;
  font-size: 14px;
  margin-left: 8px;
  width: 130px;
  margin-top: 10px;
`;
