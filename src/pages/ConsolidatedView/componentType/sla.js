import { Col, Row } from "antd";
import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllSla } from "redux/slices/consolidatedViewSettings";
import styled from "styled-components";
import Failed from "./slaType/failed";
import Pending from "./slaType/pending";
import Running from "./slaType/running";
import Success from "./slaType/success";

const SLA = (props) => {
  const { recordID: record_id, data, objectId } = props;
  const { sla } = useSelector((state) => state.consolidatedViewSettingsReducer);

  const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(
  //     getAllSla({
  //       current_page: 1,
  //       object_id: objectId,
  //       record_id: record_id,
  //       record_per_page: 100,
  //     })
  //   );
  //   // eslint-disable-next-line
  // }, [dispatch]);

  const reloadSLA = useCallback(() => {
    if (record_id && objectId)
      dispatch(
        getAllSla({
          current_page: 1,
          object_id: objectId,
          record_id: record_id,
          record_per_page: 100,
        })
      );
  }, [record_id, objectId, dispatch]);

  useEffect(() => {
    reloadSLA();
  }, [reloadSLA]);

  return (
    <Wrapper
      style={{
        paddingTop: `${
          window.location.pathname.includes("list-view-with-details")
            ? ""
            : "36px"
        }`,
      }}
    >
      <div
        style={{
          height: "36px",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: "fixed",
          top: 0,
          left: 0,
          zIndex: 9,
          borderBottom: "1px solid #d9d9d9",
        }}
      >
        <Row style={{ width: "100%" }}>
          <Col span={24}> {data.name}</Col>
        </Row>
      </div>
      <div
        style={{ paddingRight: "8px", paddingLeft: "8px", marginTop: "8px" }}
      >
        {sla &&
          sla.data &&
          // eslint-disable-next-line
          sla.data.map((item, idx) => {
            if (item.is_fail) {
              return (
                <>
                  <Failed data={item} reloadSLA={reloadSLA} />
                  <div style={{ paddingLeft: "32px" }}>
                    {/* eslint-disable-next-line */}
                    {item.sla_instances.map((el, index) => {
                      if (el.is_fail) {
                        return <Failed data={el} reloadSLA={reloadSLA} />;
                      } else if (el.is_success) {
                        return <Success data={el} reloadSLA={reloadSLA} />;
                      } else if (!el.is_pending) {
                        return <Running data={el} reloadSLA={reloadSLA} />;
                      } else {
                        return <Pending data={el} reloadSLA={reloadSLA} />;
                      }
                    })}
                  </div>
                </>
              );
            } else if (item.is_success) {
              return (
                <>
                  <Success data={item} reloadSLA={reloadSLA} />
                  <div style={{ paddingLeft: "32px" }}>
                    {/* eslint-disable-next-line */}
                    {item.sla_instances.map((el, index) => {
                      if (el.is_fail) {
                        return <Failed data={el} reloadSLA={reloadSLA} />;
                      } else if (el.is_success) {
                        return <Success data={el} reloadSLA={reloadSLA} />;
                      } else if (!el.is_pending) {
                        return <Running data={el} reloadSLA={reloadSLA} />;
                      } else {
                        return <Pending data={el} reloadSLA={reloadSLA} />;
                      }
                    })}
                  </div>
                </>
              );
            } else if (!item.is_pending) {
              return (
                <>
                  <Running data={item} reloadSLA={reloadSLA} />
                  <div style={{ paddingLeft: "32px" }}>
                    {/* eslint-disable-next-line */}
                    {item.sla_instances.map((el, index) => {
                      if (el.is_fail) {
                        return <Failed data={el} reloadSLA={reloadSLA} />;
                      } else if (el.is_success) {
                        return <Success data={el} reloadSLA={reloadSLA} />;
                      } else if (!el.is_pending) {
                        return <Running data={el} reloadSLA={reloadSLA} />;
                      } else {
                        return <Pending data={el} reloadSLA={reloadSLA} />;
                      }
                    })}
                  </div>
                </>
              );
            } else {
              return (
                <>
                  <Pending data={item} reloadSLA={reloadSLA} />
                  <div style={{ paddingLeft: "32px" }}>
                    {/* eslint-disable-next-line */}
                    {item.sla_instances.map((el, index) => {
                      if (el.is_fail) {
                        return <Failed data={el} reloadSLA={reloadSLA} />;
                      } else if (el.is_success) {
                        return <Success data={el} reloadSLA={reloadSLA} />;
                      } else if (!el.is_pending) {
                        return <Running data={el} reloadSLA={reloadSLA} />;
                      } else {
                        return <Pending data={el} reloadSLA={reloadSLA} />;
                      }
                    })}
                  </div>
                </>
              );
            }
            // return <div>{item.rule_name}</div>;
          })}
        {sla &&
        sla.data &&
        // eslint-disable-next-line
        sla.data.length === 0 ? (
          <div style={{ padding: "8px", paddingLeft: "8px" }}>
            No existing SLA applied for this ticket
          </div>
        ) : (
          ""
        )}
      </div>
    </Wrapper>
  );
};

export default SLA;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  background-color: white;
  /* padding-top: 48px; */
`;
