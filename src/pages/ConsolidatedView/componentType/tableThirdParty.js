import { EllipsisOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Dropdown,
  Menu,
  Row,
  // Table,
  Tooltip,
  Input,
  Popover,
  Tabs,
  Typography,
} from "antd";
import img from "assets/icons/common/confirm.png";
import axios from "axios";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, FE_URL } from "constants/constants";
import React, {
  useEffect,
  useState,
  useMemo,
  useCallback,
  useRef,
} from "react";
import { updateRecord } from "redux/slices/consolidatedViewSettings";
import styled from "styled-components";
import fiterIcon from "assets/icons/common/filter.png";
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import Refresh from "assets/icons/common/refresh.png";
import { useSelector } from "react-redux";
import _ from "lodash";
import { checkTokenExpiration } from "contexts/TokenCheck";
import DetailsThirdParty from "./detailsThirdParty";
import TabComponent from "../tabComponent";
import { useParams } from "react-router";
const { TabPane } = Tabs;
const { Text: TextComponent } = Typography;

const TableThirdParty = ({
  data,
  recordID,
  // objectId,
  edit: editProp,
  onRemoveItem,
  isEmbed,
}) => {
  const [value, setValue] = useState([]);
  const [header, setHeader] = useState([]);
  // const dispatch = useDispatch();
  const [, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [dataItem, setDataItem] = useState({});

  const { objectId, recordID: recordIDRouter } = useParams();

  let newItem = {};

  const [requesBody, setRequestBody] = useState([]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const loadConfig = useCallback(async () => {
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "third-party/load-setting-details",
        {
          api_id: data._id,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setRequestBody(res.data.data.request_body);
      })
      .catch((err) => console.log(err));
  }, [data._id]);

  useEffect(() => {
    loadConfig();
  }, [loadConfig]);

  const loadRecordData = async () => {
    const isTokenValid = await checkTokenExpiration();
    axios
      .get(
        BASE_URL_API +
          `load-record-data?id=${recordIDRouter}&object_id=${objectId}`,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        let tmp = { ...dataFilter };
        setDataFilter(tmp);
        requesBody.forEach((item, idx) => {
          tmp[item.selected_value] = res.data.data[item.value].value;
        });
        console.log(tmp);
        setDataFilter(tmp);
      })
      .catch((err) => {});
  };

  useEffect(() => {
    loadRecordData();
    /*eslint-disable-next-line*/
  }, [requesBody]);

  const loadData = async () => {
    setLoading(true);
    const isTokenValid = await checkTokenExpiration();
    if (data && recordID)
      axios
        .post(
          BASE_URL_API + "third-party/load-data",
          {
            api_id: data._id,
            record_id: recordID,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          setValue(res.data.data);
          let tempHeader = [];
          if (res.data.data.length > 0) {
            /* eslint-disable-next-line */
            Object.keys(res.data.data[0]).forEach((item, idx) => {
              tempHeader.push({
                title: item,
                dataIndex: item,
                key: item,
              });
            });
            // tempHeader.push({
            //   title: "Action",
            //   key: "action",
            //   fixed: "right",
            //   render: (_, record) => {
            //     return (
            //       <div>
            //         <a
            //           href
            //           onClick={() => {
            //             /* eslint-disable-next-line */
            //             Object.entries(record).forEach(
            //               ([key, value], index) => {
            //                 value = value + "";
            //                 newItem[key] = value.trim();
            //               }
            //             );
            //             setDataItem(newItem);
            //             setOpen(true);
            //           }}
            //         >
            //           Update
            //         </a>
            //         <a
            //           href
            //           onClick={() => {
            //             let newItem = {};
            //             /* eslint-disable-next-line */
            //             Object.entries(record).forEach(
            //               ([key, value], index) => {
            //                 value = value + "";
            //                 newItem[key] = value.trim();
            //               }
            //             );

            //             setLoading(true);
            //             axios({
            //               method: "post",
            //               url: BASE_URL_API + "third-party/load-related-record",
            //               headers: {
            //                 Authorization: `Bearer ${localStorage.getItem(
            //                   "setting_accessToken"
            //                 )}`,
            //               },
            //               data: {
            //                 // object_id: object_id,
            //                 record_id: recordID,
            //                 api_id: data._id,
            //                 third_party_data: newItem,
            //               },
            //             })
            //               .then((res) => {
            //                 setLoading(false);
            //                 if (
            //                   Object.prototype.toString.call(res.data.data) !==
            //                   "[object Array]"
            //                 ) {
            //                   window.open(
            //                     FE_URL +
            //                       `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
            //                   );
            //                 }
            //               })
            //               .catch((err) => {
            //                 setLoading(false);
            //                 Notification(
            //                   "error",
            //                   err.response.data.error.value
            //                 );
            //               });
            //           }}
            //           style={{ marginLeft: "10px" }}
            //         >
            //           View
            //         </a>
            //       </div>
            //     );
            //   },
            // });
            setHeader(tempHeader);
          }
        })
        .catch((err) => {
          setLoading(false);
        });
  };

  useEffect(() => {
    loadData();
    /* eslint-disable-next-line */
  }, []);

  const containerStyle = useMemo(() => ({ width: "100%", height: "100%" }), []);
  const gridStyle = useMemo(() => ({ height: "100%", width: "100%" }), []);
  // const [rowData, setRowData] = useState();

  const [columnDefs, setColumnDefs] = useState([]);

  const [dataFilter, setDataFilter] = useState([]);
  const content = () => {
    return (
      <div>
        {requesBody.map((item, idx) => {
          return (
            <div>
              <div>
                {item.label} {`(${item.selected_value})`}:
              </div>
              <div>
                <Input
                  value={dataFilter && dataFilter[item.selected_value]}
                  onChange={(e) => {
                    let tmp = { ...dataFilter };
                    if (e.target.value === "") {
                      delete tmp[item.selected_value];
                    } else {
                      tmp[item.selected_value] = e.target.value;
                    }
                    setDataFilter(tmp);
                  }}
                />
              </div>
            </div>
          );
        })}
        <div style={{ display: "flex", justifyContent: "flex-end" }}>
          <CustomButton
            onClick={() => {
              loadMoreData(dataFilter);
            }}
            style={{ marginRight: "8px" }}
          >
            Save
          </CustomButton>
          <CustomButton
            onClick={() => {
              setDataFilter([]);
              loadMoreData([]);
            }}
          >
            Clear
          </CustomButton>
        </div>
      </div>
    );
  };

  const loadMoreData = async (dataFilter) => {
    setLoading(true);
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "third-party/load-data",
        {
          api_id: data._id,
          record_id: recordID,
          search_more: dataFilter,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setLoading(false);
        setValue(res.data.data);
        let tempHeader = [];
        if (res.data.data.length > 0) {
          /* eslint-disable-next-line */
          Object.keys(res.data.data[0]).forEach((item, idx) => {
            tempHeader.push({
              title: item,
              dataIndex: item,
              key: item,
            });
          });
          setHeader(tempHeader);
        }
      })
      .catch((err) => {
        setValue([]);
        setLoading(false);
        Notification("error", err.response.data.error);
      });
  };
  function checkNumberFormat(input) {
    const pattern = /^[-+]?[0-9]+([.,][0-9]+)*$/;
    return pattern.test(input);
  }
  const MyCustomCellRenderer = ({ value }) => (
    <CustomCell
      isNumber={checkNumberFormat(value !== null && value.toString().trim())}
    >
      <TextComponent ellipsis={{ tooltip: value }}>{value}</TextComponent>
    </CustomCell>
  );
  useEffect(() => {
    let arr = [];
    header.forEach((item, idx) => {
      let newItem = {
        field: item.title,
        resizable: true,
        sortable: true,
        cellRenderer: "myCustomCellRenderer",
        // cellRenderer: MyRenderer,
      };
      arr.push(newItem);
    });
    setColumnDefs(arr);
  }, [header]);

  const defaultColDef = useMemo(() => {
    return {
      flex: 1,
      minWidth: 150,
      filter: true,
    };
  }, []);

  const gridRef = useRef();
  const [rowSelected, setRowSelected] = useState([]);
  const [recordDetail, setRecordDetail] = useState("");
  const [listComponent, setListComponent] = useState([]);
  const [isEnable, setIsEnable] = useState(false);

  const onSelectionChanged = useCallback(() => {
    var selectedRows = gridRef.current.api.getSelectedRows();
    setRowSelected(selectedRows);
  }, []);

  const loadSetting = useCallback(async () => {
    if (data._id) {
      setLoading(true);
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "third-party/load-setting-details",
          {
            api_id: data._id,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          setIsEnable(res.data.data.enable_listview_with_details);
          if (res.data.data.list_of_components) {
            setListComponent(res.data.data.list_of_components);
          }
        })
        .catch((err) => {
          setLoading(false);
        });
    }
  }, [data._id]);
  useEffect(() => {
    loadSetting();
  }, [loadSetting]);
  useEffect(() => {
    if (rowSelected.length > 0 && listComponent.length > 0) {
      axios({
        method: "post",
        url: BASE_URL_API + "third-party/load-related-record",
        headers: {
          Authorization: `Bearer ${localStorage.getItem(
            "setting_accessToken"
          )}`,
        },
        data: {
          record_id: recordID,
          api_id: data._id,
          third_party_data: rowSelected[0],
        },
      })
        .then((res) => {
          setLoading(false);
          if (res.data.data.record_id) {
            setRecordDetail(res.data.data.record_id);
          }
        })
        .catch((err) => {
          setLoading(false);
          Notification("error", err.response.data.error.value);
        });
    }
    //eslint-disable-next-line
  }, [rowSelected, listComponent]);

  // const renderDetailSection = (setting, data) => {
  //   return (
  //     <TableWrap columns={setting.length}>
  //       {setting.map((detail, index) => (
  //         <BasicInfo className="item" key={index}>
  //           <legend>{detail.section_name}</legend>
  //           {Object.entries(data).map(
  //             ([key, value]) =>
  //               detail.display_fields.includes(key) && (
  //                 <ItemSection>
  //                   <div className="item-name">{key}</div>
  //                   <div className="item-value">{value}</div>
  //                 </ItemSection>
  //               )
  //           )}
  //         </BasicInfo>
  //       ))}
  //     </TableWrap>
  //   );
  // };
  const [key] = useState(0);

  const renderListComponent = (list, recordID) => {
    //eslint-disable-next-line
    return list.map((item, index) => {
      let temp = JSON.parse(item);

      if (temp.component_type === "DETAILS") {
        return (
          <div style={{ height: "fit-content" }}>
            <DetailsThirdParty
              data={temp}
              recordID={recordID}
              objectId={temp.object_id}
              isEmbed={true}
            />
          </div>
        );
      }
      if (temp.component_type === "TABLE") {
        return (
          <div style={{ height: "fit-content" }}>
            <TableThirdParty
              data={temp}
              recordID={recordID}
              isEmbed={true}
              objectId={temp.object_id}
            />
          </div>
        );
      }
      if (temp.type === "tab") {
        return (
          <div style={{ height: "fit-content" }}>
            <Tabs defaultActiveKey={key}>
              {temp.tab_name.map((item, idx) => {
                return (
                  <TabPane tab={item} key={idx}>
                    <TabComponent
                      data={temp}
                      index={idx}
                      name={item}
                      recordID={recordID}
                      objectId={temp.object_id}
                    />
                  </TabPane>
                );
              })}
            </Tabs>{" "}
          </div>
        );
      }
    });
  };

  return (
    <Wrapper
      style={{
        height: "calc(100% - 30px)",
        paddingTop: `${
          window.location.pathname.includes("list-view-with-details")
            ? ""
            : "36px"
        }`,
      }}
    >
      <div
        style={{
          height: "36px",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : isEmbed
              ? ""
              : "fixed"
          }`,
          top: 0,
          left: 0,
          zIndex: 9,
          boxShadow: "inset 0px -1px 0px #F0F0F0",
        }}
      >
        <Row style={{ width: "100%" }}>
          <Col span={12}>
            {" "}
            {data.name}{" "}
            <Tooltip title="Load more">
              <Popover content={content} title="Load more" trigger="click">
                <button
                  className="button-filter"
                  style={{
                    marginLeft: "10px",
                    textAlign: "center",
                  }}
                  // onClick={() => {
                  //   setOpenCustomView(true);
                  // }}
                >
                  <img
                    alt=""
                    src={fiterIcon}
                    style={{ width: "12px", textAlign: "center" }}
                  />
                </button>
              </Popover>
            </Tooltip>
          </Col>

          <Col
            span={12}
            style={{
              textAlign: "right",
            }}
          >
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center",
              }}
            >
              <Tooltip title="Reload">
                <CustomButtonReload
                  shape="circle"
                  // disabled={
                  //   Object.keys(selectedRecord).length === 0 ? true : false
                  // }
                  onClick={() => {
                    // window.open(
                    //   BASENAME +
                    //     `consolidated-view/${data.related_object}/${selectedRecord._id}`
                    // );
                    loadData();
                  }}
                >
                  <img alt="" src={Refresh} style={{ width: "18px" }} />
                </CustomButtonReload>
              </Tooltip>
              <CustomButton
                style={{ marginTop: 0, marginRight: "8px" }}
                disabled={rowSelected.length === 0}
                onClick={async () => {
                  window.open(
                    FE_URL +
                      `/consolidated-view/${data.object_create_record}/${recordDetail}`
                  );
                  let newItem = {};
                  /* eslint-disable-next-line */
                  Object.entries(rowSelected[0]).forEach(
                    ([key, value], index) => {
                      value = value + "";
                      newItem[key] = value.trim();
                    }
                  );
                  const isTokenValid = await checkTokenExpiration();
                  setLoading(true);
                  axios({
                    method: "post",
                    url: BASE_URL_API + "third-party/load-related-record",
                    headers: {
                      Authorization: `Bearer ${isTokenValid}`,
                    },
                    data: {
                      // object_id: object_id,
                      record_id: recordID,
                      api_id: data._id,
                      third_party_data: newItem,
                    },
                  })
                    .then((res) => {
                      setLoading(false);
                      if (
                        Object.prototype.toString.call(res.data.data) !==
                        "[object Array]"
                      ) {
                        console.log(
                          FE_URL +
                            `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
                        );
                        window.open(
                          FE_URL +
                            `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
                        );
                      }
                    })
                    .catch((err) => {
                      setLoading(false);
                      Notification("error", err.response.data.error.value);
                    });
                }}
              >
                View
              </CustomButton>
              <CustomButton
                style={{ marginTop: 0, marginRight: "0px" }}
                disabled={rowSelected.length === 0}
                onClick={() => {
                  /* eslint-disable-next-line */
                  Object.entries(rowSelected[0]).forEach(
                    ([key, value], index) => {
                      value = value + "";
                      newItem[key] = value.trim();
                    }
                  );
                  setDataItem(newItem);
                  setOpen(true);
                }}
              >
                Update
              </CustomButton>
              {editProp ? (
                <Dropdown
                  trigger="click"
                  overlay={
                    <Menu>
                      {checkRule("delete") ? (
                        <Menu.Item
                          onClick={() => onRemoveItem(JSON.stringify(data))}
                        >
                          Remove
                        </Menu.Item>
                      ) : (
                        ""
                      )}
                    </Menu>
                  }
                >
                  <Button style={{ marginLeft: "8px" }}>
                    <EllipsisOutlined />
                  </Button>
                </Dropdown>
              ) : (
                ""
              )}
            </div>
          </Col>
        </Row>
      </div>
      <div
        style={{
          width: "100%",
          overflow: "auto",
          marginTop: "8px",
          height: "calc(100% - 8px)",
          display: "flex",
        }}
      >
        {/* <Table
          dataSource={value}
          columns={header}
          scroll={{
            x: "max-content",
          }}
          pagination={false}
        /> */}
        <WrapTable width={listComponent.length > 0 && isEnable}>
          <div style={containerStyle}>
            <div style={gridStyle} className="ag-theme-alpine">
              <AgGridReact
                gridOptions={{
                  onRowDoubleClicked: async (value) => {
                    let newItem = {};
                    /* eslint-disable-next-line */
                    Object.entries(value.data).forEach(
                      ([key, value], index) => {
                        value = value + "";
                        newItem[key] = value.trim();
                      }
                    );
                    const isTokenValid = await checkTokenExpiration();
                    setLoading(true);
                    axios({
                      method: "post",
                      url: BASE_URL_API + "third-party/load-related-record",
                      headers: {
                        Authorization: `Bearer ${isTokenValid}`,
                      },
                      data: {
                        // object_id: object_id,
                        record_id: recordID,
                        api_id: data._id,
                        third_party_data: newItem,
                      },
                    })
                      .then((res) => {
                        setLoading(false);
                        if (
                          Object.prototype.toString.call(res.data.data) !==
                          "[object Array]"
                        ) {
                          window.open(
                            FE_URL +
                              `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
                          );
                        }
                      })
                      .catch((err) => {
                        setLoading(false);
                        Notification("error", err.response.data.error.value);
                      });
                  },
                }}
                frameworkComponents={{
                  myCustomCellRenderer: MyCustomCellRenderer,
                }}
                ref={gridRef}
                rowData={value}
                columnDefs={columnDefs}
                defaultColDef={defaultColDef}
                rowSelection={"single"}
                // onGridReady={onGridReady}
                onSelectionChanged={onSelectionChanged}
                copyHeadersToClipboard={true}
                enableCellTextSelection={true}
                enableRangeSelection={true}
              ></AgGridReact>
            </div>
          </div>
        </WrapTable>
        <WrapDetail>
          {/* {detailColumn.length > 0 &&
            rowSelected.length > 0 &&
            renderDetailSection(detailColumn, rowSelected[0])} */}

          {listComponent.length > 0 &&
            recordDetail &&
            renderListComponent(listComponent, recordDetail)}
        </WrapDetail>
      </div>
      <ModalConfirm
        title={"Bạn có chắc chắn thay đổi bản ghi này?"}
        decs={"Do you want to update this record with the exist setting?"}
        open={open}
        setOpen={setOpen}
        method={updateRecord}
        data={{
          api_id: data._id,
          record_id: recordID,
          third_party_data: dataItem,
          // api_id: apiID,
          // third_party_data: newItem,
          // record_id: recordID,
        }}
        setOpenModal={() => {}}
        img={img}
      />
    </Wrapper>
  );
};

export default TableThirdParty;

const CustomButtonReload = styled(Button)`
  background: ${(props) => props.theme.white};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  /* padding-top: 48px; */
  &::-webkit-scrollbar {
    height: 12px !important;
    width: 12px !important;
  }
  &::-webkit-scrollbar-thumb {
    background: #d9d9d9;
  }
  table {
    width: max-content;
  }
  .button-filter {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    margin-right: 8px;
    border: 1px solid ${(props) => props.theme.main};
    :hover {
      background: ${(props) => props.theme.main};
      color: ${(props) => props.theme.white};
      border: 1px solid ${(props) => props.theme.main};
      cursor: pointer;
    }
  }
  .ag-theme-alpine .ag-row {
    height: 30px !important;
  }
  .ag-cell-wrapper {
    color: #2c2c2c;
    font-family: var(--roboto-400);
    font-size: 16px;
  }
  .ag-header-cell-label {
    color: #2c2c2c;
    font-family: var(--roboto-500);
    font-size: 16px;
    font-weight: 300;
  }

  .ag-theme-alpine .ag-root-wrapper {
    border: none;
  }
  .ag-row-hover,
  .ag-row-selected {
    background-color: #f5f5f5 !important;
  }
  .ag-cell-value {
    width: 100%;
  }
`;

const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-top: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const WrapTable = styled.div`
  width: ${(props) => (props.width ? "30%" : "100%")};
  border-right: 1px solid #ececec;
`;
const WrapDetail = styled.div`
  flex: 1;
  overflow: auto;
  .ant-tabs > .ant-tabs-nav,
  .ant-tabs > div > .ant-tabs-nav {
    justify-content: unset;
  }
  .ant-tabs > div > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
  }
  .ant-tabs > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
    padding-left: 10px;
  }
  .ant-tabs-top > .ant-tabs-nav::before {
    /* display: none; */
    display: block;
  }
  .ant-tabs-tab {
    position: relative;
    display: inline-flex;
    align-items: center;
    padding: 12px 0;
    font-size: 14px;
    background: 0 0;
    border: 0;
    outline: none;
    cursor: pointer;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;

    /* Character/Color text main */

    color: #2c2c2c;
  }
  .ant-tabs-nav-list {
    position: relative;
    display: flex;
    transition: transform 0.3s;
    border: 0 !important;
    padding: 4px 8px !important;
  }
  .ant-tabs-ink-bar {
    display: unset !important;
    background: ${(props) => props.theme.main} !important;
  }
  .ant-tabs-tab-active {
    background: #fff !important;
    .ant-tabs-tab-btn {
      color: ${(props) => props.theme.main} !important;
    }
  }
`;
const CustomCell = styled.div`
  font-size: 16px;
  text-align: ${(props) => (props.isNumber ? "right" : "unset")};
`;
