import { Row, Col, Checkbox, Input, Modal, Button } from "antd";
import React, { useEffect, useState } from "react";
import {
  deleteArticle,
  getListArticle,
  loadMoreArticle,
  selectArticleDetail,
  updatePopupView,
  updateSearchArticle,
} from "redux/slices/componentKnowledgeBase";
import { getKnowledgeBaseSetting } from "redux/slices/knowledgeBaseSetting";
import { useDispatch, useSelector } from "redux/store";
import { PlusOutlined } from "@ant-design/icons";
import styled from "styled-components";
import editIcon from "../../../assets/icons/consolidatedViewSettings/editKb.svg";
import deleteIcon from "../../../assets/icons/consolidatedViewSettings/deleteKb.svg";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { useNavigate } from "react-router";
import emptyEmail from "../../../assets/icons/settings/Consolidated-View-Settings.png";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
const { Search } = Input;
const ComponentKnowledgeBase = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // eslint-disable-next-line
  const { recordId, objectId, layout } = props;
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );

  // eslint-disable-next-line
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const {
    listArticle,
    pagination,
    totalPage,
    searchArticle,
    popupView,
    articleDetail,
  } = useSelector((state) => state.componentKnowledgeBaseReducer);
  const { page, limit } = pagination;
  const [span, setSpan] = useState(23);
  const [showModal, setShowModal] = useState(false);
  const [dataDelete, setDataDelete] = useState({});
  const onSearch = () => {
    let temp = [];
    if (searchArticle.length > 0) {
      temp.push({
        id_field: editKnowledgeBaseSetting.title,
        value: searchArticle,
      });
    }
    if (editKnowledgeBaseSetting.object_id) {
      dispatch(
        getListArticle({
          data: {
            current_page: 1,
            object_id: editKnowledgeBaseSetting.object_id,
            record_per_page: 12,
            search_with: {
              meta: [],
              data: temp,
            },
          },
          pagination: {
            object_id: editKnowledgeBaseSetting.object_id,
            current_page: 1,
            record_per_page: 12,
            search_with: {
              meta: [],
              data: temp,
            },
          },
        })
      );
    }
  };
  const hideModal = () => {
    setShowModal(false);
  };
  const handleDeleteArticle = (id) => {
    setDataDelete({
      ids: [id],
      object_id: objectId,
    });
    dispatch(setShowModalConfirmDelete(true));
  };
  useEffect(() => {
    if (editKnowledgeBaseSetting.object_id) {
      dispatch(
        getListArticle({
          data: {
            current_page: 1,
            object_id: editKnowledgeBaseSetting.object_id,
            record_per_page: 12,
            search_with: {
              meta: [],
              data: [],
            },
          },
          pagination: {
            object_id: editKnowledgeBaseSetting.object_id,
            current_page: 1,
            record_per_page: 12,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }
    // eslint-disable-next-line
  }, [editKnowledgeBaseSetting]);
  useEffect(() => {
    dispatch(updateSearchArticle(""));
    dispatch(getKnowledgeBaseSetting());
    // eslint-disable-next-line
  }, []);
  useEffect(() => {
    if (layout) {
      if (layout.w <= 5) {
        setSpan(23);
      } else if (layout.w > 5 && layout.w <= 9) {
        setSpan(11);
      } else if (layout.w > 9) {
        setSpan(6);
      }
    }
  }, [layout]);
  return (
    <CustomContent>
      <Row style={{ width: "100%", padding: "1rem" }}>
        <Col
          span={
            layout && layout.w <= 5
              ? 17
              : layout && layout.w > 5 && layout.w <= 9
              ? 18
              : 19
          }
          className={"title-knowledge-base"}
        >
          Knowledge Base
        </Col>
        {editKnowledgeBaseSetting.object_id && (
          <>
            <Col span={2}>
              <Button
                style={{ backgroundColor: "#20a2a2" }}
                shape={"circle"}
                icon={<PlusOutlined style={{ color: "white" }} />}
                onClick={() => {
                  navigate("/articles ");
                }}
              />
            </Col>
            <Col span={3}>
              <Checkbox
                checked={popupView}
                onChange={(e) => {
                  dispatch(updatePopupView(e.target.checked));
                }}
              >
                Popup View
              </Checkbox>
            </Col>
          </>
        )}
      </Row>
      {editKnowledgeBaseSetting.object_id ? (
        <Row
          style={{
            backgroundColor: "white",
            height: "100%",
            overflowY: "scroll",
          }}
          onScroll={(e) => {
            if (
              e.target.scrollTop + e.target.clientHeight >=
                e.target.scrollHeight &&
              totalPage - 1 >= page
            ) {
              let temp = [];
              if (searchArticle.length > 0) {
                temp.push({
                  id_field: editKnowledgeBaseSetting.title,
                  value: searchArticle,
                });
              }
              dispatch(
                loadMoreArticle({
                  data: {
                    object_id: editKnowledgeBaseSetting.object_id,
                    current_page: page + 1,
                    record_per_page: limit,
                    search_with: {
                      meta: [],
                      data: temp,
                    },
                  },
                  pagination: {
                    object_id: editKnowledgeBaseSetting.object_id,
                    current_page: page + 1,
                    record_per_page: 12,
                    search_with: {
                      meta: [],
                      data: temp,
                    },
                    sort_by: {},
                  },
                })
              );
            }
          }}
        >
          <Col span={24}>
            <Row justify={"center"} style={{ marginTop: "1rem" }}>
              <Col span={23}>
                <Search
                  placeholder="Tìm kiếm bài viết"
                  onSearch={onSearch}
                  enterButton
                  value={searchArticle}
                  onChange={(e) => {
                    dispatch(updateSearchArticle(e.target.value));
                  }}
                />
              </Col>
            </Row>
            <CustomListArticle>
              <Row
                style={{ padding: "0rem 1rem" }}
                justify={layout && layout.w > 5 ? "start" : "center"}
              >
                {listArticle &&
                  listArticle.map((article) => {
                    return (
                      <Col span={span} className="article">
                        <Row
                          className={"row-article"}
                          onClick={() => {
                            if (!popupView) {
                              navigate(
                                "/article-detail?id=" +
                                  article._id +
                                  "&object_id=" +
                                  editKnowledgeBaseSetting.object_id
                              );
                            } else {
                              dispatch(selectArticleDetail(article));
                              setShowModal(true);
                            }
                          }}
                        >
                          <Col span={24}>
                            <Row style={{ paddingBottom: "1rem" }}>
                              <Col span={19}>
                                <span className={"section-article"}>
                                  {" "}
                                  {article[
                                    editKnowledgeBaseSetting.section_name
                                  ] &&
                                    article[
                                      editKnowledgeBaseSetting.section_name
                                    ].value}{" "}
                                  &nbsp;/&nbsp;{" "}
                                </span>{" "}
                                <span className={"category-article"}>
                                  {" "}
                                  {article[
                                    editKnowledgeBaseSetting.category_name
                                  ] &&
                                    article[
                                      editKnowledgeBaseSetting.category_name
                                    ].value}
                                </span>
                              </Col>
                              <Col span={5} className={"edit-delete-article"}>
                                <Row justify={"end"}>
                                  <Col>
                                    <img
                                      alt=""
                                      src={editIcon}
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        navigate(
                                          "/articles?object_id=" +
                                            objectId +
                                            "&id=" +
                                            article._id
                                        );
                                      }}
                                    />
                                  </Col>
                                  <Col>
                                    <img
                                      alt=""
                                      src={deleteIcon}
                                      onClick={(e) => {
                                        e.stopPropagation();
                                        e.preventDefault();
                                        handleDeleteArticle(article._id);
                                      }}
                                    />
                                  </Col>
                                </Row>
                              </Col>
                            </Row>
                            <Row>
                              <span className={"title-article"}>
                                {" "}
                                {article[editKnowledgeBaseSetting.title] &&
                                  article[editKnowledgeBaseSetting.title].value}
                              </span>
                            </Row>
                            <Row>
                              <span className={"body-article"}>
                                {" "}
                                {article[editKnowledgeBaseSetting.body] && (
                                  <p>
                                    {parse(
                                      article[editKnowledgeBaseSetting.body]
                                        .value,
                                      optionsParse
                                    )}
                                  </p>
                                )}
                              </span>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                    );
                  })}
              </Row>
            </CustomListArticle>
          </Col>
        </Row>
      ) : (
        <>
          <Row style={{ width: "100%" }} justify={"center"}>
            <img alt="Empty" src={emptyEmail} style={{ width: "5rem" }} />
          </Row>
          <Row style={{ width: "100%" }} justify={"center"}>
            <p>Vui lòng cấu hình knowledge base </p>
          </Row>
        </>
      )}
      <ModalConfimDelete
        title={"bài viết này"}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteArticle}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
      <CustomModal
        visible={showModal}
        width={1000}
        footer={[
          <Button
            onClick={() => {
              navigate(
                "/articles?object_id=" + objectId + "&id=" + articleDetail._id
              );
            }}
          >
            Edit
          </Button>,
          <Button onClick={() => handleDeleteArticle(articleDetail._id)}>
            Delete
          </Button>,
          <Button onClick={hideModal}>Close</Button>,
        ]}
        onCancel={hideModal}
        title={"Chi tiết bài viết"}
      >
        <Row>
          <Col span={24} style={{ textAlign: "center", fontSize: "24px" }}>
            <p>
              {" "}
              {articleDetail[editKnowledgeBaseSetting.title] &&
                articleDetail[editKnowledgeBaseSetting.title].value}{" "}
            </p>
          </Col>
        </Row>

        <span>
          {" "}
          {articleDetail[editKnowledgeBaseSetting.body] && (
            <p>
              {parse(
                articleDetail[editKnowledgeBaseSetting.body].value,
                optionsParse
              )}
            </p>
          )}
        </span>
      </CustomModal>
    </CustomContent>
  );
};

export default ComponentKnowledgeBase;

const CustomContent = styled.div`
  .title-knowledge-base {
    font-family: var(--roboto-500);
    font-size: 18px;
  }
  .article {
    background-color: white;
    margin-top: 1rem;
    // margin-right: 1rem;
    padding-right: 0.5rem;
    border-radius: 4px;
    .row-article {
      border-radius: 4px;
      box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
      padding: 0.7rem 1rem;
      &:hover {
        .edit-delete-article {
          display: block;
        }
        border-right: 0.3rem solid rgba(32, 162, 162, 1);
      }
    }

    .edit-delete-article {
      display: none;
    }
    .section-article {
      font-size: 16px;
      color: rgba(0, 0, 0, 0.45);
    }
    .category-article {
      font-size: 16px;
      color: #2c2c2c;
    }
    .title-article {
      font-family: var(--roboto-700);
      font-size: 16px;
      line-height: 22px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 1;
      -webkit-box-orient: vertical;
    }
    .body-article {
      font-family: var(--roboto-400);
      font-size: 16px;
      overflow: hidden;
      text-overflow: ellipsis;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
    }
  }

  width: 100%;
`;

const CustomListArticle = styled.div`
  height: 100%;
  overflow-y: scroll;
  padding-bottom: 10%;
`;

const CustomModal = styled(Modal)`
  label {
    font-size: 16px;
    white-space: normal;
    word-break: normal;
    height: fit-content;
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
    text-align: center;
    box-shadow: 0px -2px 17px rgba(0, 0, 0, 0.16);
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-modal-body {
    max-height: calc(100vh - 300px);
    overflow-y: scroll;
    /* width */
    ::-webkit-scrollbar {
      width: 5px !important;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px transparent;
      border-radius: 100px;
      background: transparent !important;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #d8d6d6;
      border-radius: 100px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #d8d6d6;
    }
    ::-webkit-scrollbar-track-piece {
      margin-bottom: 10px;
      background: transparent !important;
    }
  }
`;
