import { EllipsisOutlined, LoadingOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Dropdown,
  Menu,
  Row,
  Timeline,
  Tooltip,
  Typography,
} from "antd";
import DownloadIcon from "assets/icons/common/down.png";
import EmptyIcon from "assets/icons/common/emptyComponent.png";
import addIcon from "assets/icons/consolidatedViewSettings/add-icon-log.png";
import deleteIcon from "assets/icons/consolidatedViewSettings/delete-icon-log.png";
import updateIcon from "assets/icons/consolidatedViewSettings/edit-icon-log.png";
import _ from "lodash";
import ModalCompare from "pages/EnhancementView/Modalcompare/ModalCompare";
import React, { useCallback, useEffect, useState } from "react";
import { CSVLink } from "react-csv";
import { useDispatch, useSelector } from "react-redux";
import { getLogs } from "redux/slices/consolidatedViewSettings";
import styled from "styled-components";

const { Paragraph } = Typography;
const Logs = (props) => {
  const {
    recordID: record_id,
    data,
    objectId,
    onRemoveItem,
    edit: editProp,
  } = props;
  const { logs, isloadingLogs } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const [config, setConfig] = useState({});
  const [open, setOpen] = useState(false);
  const [itemLog, setItemLog] = useState({});

  useEffect(() => {
    if (!open) {
      setItemLog({});
    }
  }, [open]);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  const checkIsArticleChange = (config, field) => {
    if (Object.entries(config).length > 0) {
      if (config.article_setting?.fld_body === field) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  };
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const dispatch = useDispatch();
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const loadLogs = useCallback(() => {
    if (record_id && objectId)
      dispatch(
        getLogs({
          current_page: 1,
          object_id: objectId,
          record_id: record_id,
          record_per_page: 1000,
        })
      );
  }, [record_id, objectId, dispatch]);

  useEffect(() => {
    loadLogs();
    // eslint-disable-next-line
  }, [loadLogs]);

  useEffect(() => {
    setWidth(
      document.getElementById(data._id) &&
        document.getElementById(data._id).clientWidth
    );
    // eslint-disable-next-line
  }, [
    // eslint-disable-next-line
    document.getElementById(data._id) &&
      document.getElementById(data._id).clientWidth,
  ]);

  const [width, setWidth] = useState(0);
  const [mode, setMode] = useState("alternate");

  useEffect(() => {
    if (width < 350) {
      setMode("left");
    } else {
      setMode("alternate");
    }
  }, [width]);
  const handleClick = (item) => {
    if (checkIsArticleChange(config, item.field_id)) {
      setItemLog(item);
      setOpen(true);
    }
  };
  return (
    <Wrapper id={data._id}>
      {logs.length > 0 ? (
        <>
          {" "}
          <div
            style={{
              height: "36px",
              background: "#fff",
              display: "flex",
              alignItems: "center",
              fontStyle: "normal",
              fontWeight: "500",
              fontSize: "17px",
              lineHeight: "26px",
              color: "#252424",
              paddingLeft: "15px",
              width: "100%",
              justifyContent: "space-between",
              paddingRight: "15px",
              position: "fixed",
              top: 0,
              left: 0,
              zIndex: 9,
              borderBottom: "1px solid #d9d9d9",
            }}
          >
            <Row style={{ width: "100%" }}>
              <Col span={18}> {data.name}</Col>
              <Col
                span={6}
                style={{ display: "flex", justifyContent: "right" }}
              >
                {" "}
                {editProp ? (
                  <Dropdown
                    trigger="click"
                    overlay={
                      <Menu>
                        {checkRule("delete") ? (
                          <Menu.Item
                            onClick={() => onRemoveItem(JSON.stringify(data))}
                          >
                            Remove
                          </Menu.Item>
                        ) : (
                          ""
                        )}
                      </Menu>
                    }
                  >
                    <Button style={{ marginLeft: "8px" }}>
                      <EllipsisOutlined />
                    </Button>
                  </Dropdown>
                ) : (
                  ""
                )}
              </Col>
            </Row>
          </div>
          {!isloadingLogs ? (
            <ContentWrap>
              <Timeline mode={mode}>
                {/* eslint-disable-next-line */}
                {logs.map((item, idx) => {
                  if (
                    item.type === "create" ||
                    item.type === "create from import" ||
                    item.type === "create from email" ||
                    item.type === "create from workflow"
                  ) {
                    return (
                      <Timeline.Item
                        dot={
                          <img alt="" src={addIcon} style={{ width: "20px" }} />
                        }
                      >
                        <HeaderLog>
                          <div className="title create">
                            {item.type !== "create from email" &&
                              item.type !== "create from workflow" &&
                              item.user_name}{" "}
                            {item.type}
                          </div>

                          <div className="date">{item.created_date}</div>
                        </HeaderLog>
                      </Timeline.Item>
                    );
                  } else if (
                    item.type === "update" ||
                    item.type === "update from import" ||
                    item.type === "update from email" ||
                    item.type === "update from workflow"
                  ) {
                    return (
                      <Timeline.Item
                        dot={
                          <img
                            alt=""
                            src={updateIcon}
                            style={{ width: "20px" }}
                          />
                        }
                      >
                        <HeaderLog
                          isArticle={checkIsArticleChange(
                            config,
                            item.field_id
                          )}
                          onClick={() => handleClick(item)}
                        >
                          <div className="title update">
                            {item.type !== "update from email" &&
                              item.type !== "update from workflow" &&
                              item.user_name}{" "}
                            {item.type}
                          </div>
                          <div className="field">
                            Field:{" "}
                            <span>
                              {item.field_name === "owner"
                                ? "Assign to"
                                : item.field_name}
                            </span>
                          </div>
                          <div className="from">
                            <span>
                              <Paragraph
                                ellipsis={{
                                  rows: 3,
                                  expandable: true,
                                  symbol: "show more",
                                }}
                              >
                                <strong> From: </strong>
                                {typeof item?.old_value === "object" &&
                                item?.old_value !== null
                                  ? item?.old_value?.value
                                  : item?.old_value}
                              </Paragraph>
                            </span>
                          </div>
                          <div className="to">
                            <span>
                              <Paragraph
                                ellipsis={{
                                  rows: 3,
                                  expandable: true,
                                  symbol: "show more",
                                }}
                              >
                                <strong> To: </strong>
                                {typeof item?.new_value === "object" &&
                                item?.new_value !== null
                                  ? item?.new_value?.value
                                  : item?.new_value}
                              </Paragraph>
                            </span>
                          </div>
                          <div className="date">{item.created_date}</div>
                        </HeaderLog>
                      </Timeline.Item>
                    );
                  } else if (item.type === "delete") {
                    return (
                      <Timeline.Item
                        dot={
                          <img
                            alt=""
                            src={deleteIcon}
                            style={{ width: "20px" }}
                          />
                        }
                      >
                        <HeaderLog>
                          <div className="title delete">
                            {item.user_name} delete
                          </div>

                          <div className="date">{item.time_log}</div>
                        </HeaderLog>
                      </Timeline.Item>
                    );
                  }
                })}
              </Timeline>
            </ContentWrap>
          ) : (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                marginTop: "10%",
                flexDirection: "column",
              }}
            >
              <LoadingOutlined
                style={{ fontSize: 60, color: defaultBrandName.theme_color }}
                spin
              />
              <div style={{ textAlign: "center", marginTop: "20px" }}>
                Loading. . .{" "}
              </div>
            </div>
          )}
          <CustomDownLoad>
            <Tooltip title="Export excel">
              <CSVLink data={logs} filename="Logs.csv">
                <img src={DownloadIcon} alt="download Icon" />
              </CSVLink>
            </Tooltip>
          </CustomDownLoad>
        </>
      ) : (
        <NoData>
          <img src={EmptyIcon} alt="" />
          <span>No data</span>
        </NoData>
      )}
      {Object.entries(config).length > 0 && (
        <ModalCompare data={itemLog} open={open} setOpen={setOpen} />
      )}
    </Wrapper>
  );
};

export default Logs;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  padding-top: 48px;
`;

const HeaderLog = styled.div`
  cursor: ${(props) => (props.isArticle ? "pointer" : "unset")};
  .title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    /* or 157% */

    /* DayBreak Blue/8 */
  }
  .create {
    color: #389e0d;
  }
  .update {
    color: #0050b3;
  }
  .delete {
    color: #cf1322;
  }
  .field {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-400);
    }
  }
  .value {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-400);
    }
  }
  .to {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-400);
    }
    .ant-typography {
      margin-bottom: 0;
    }
  }
  .from {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-400);
    }
    .ant-typography {
      margin-bottom: 0;
    }
  }
  .date {
    font-style: normal;
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-400);
    }
  }
`;

const NoData = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  span {
    margin-top: 16px;
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }
`;

const ContentWrap = styled.div`
  padding: 8px;
  width: 400px;
`;

const CustomDownLoad = styled.div`
  position: sticky;
  width: 44px;
  height: 44px;
  /* right: 16px;
   */
  left: calc(100% - 60px);
  border-radius: 50%;
  box-shadow: 0px 0px 12px rgb(0 0 0 / 8%);
  display: flex;
  align-items: center;
  justify-content: center;
  bottom: 16px;
`;
