import { EllipsisOutlined } from "@ant-design/icons";
import {
  Button,
  Dropdown,
  Form,
  Menu,
  Popconfirm,
  Row,
  Space,
  Spin,
  Tooltip,
  Typography,
} from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import fiterIcon from "assets/icons/common/filter.png";
import editImg from "assets/icons/common/icon-edit.png";
import Refresh from "assets/icons/common/refresh.png";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import axios from "axios";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, FE_URL, BE_URL } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
import _ from "lodash";
import moment from "moment";
import ModalComponent from "pages/ConsolidatedViewSettings/ComponentSettings/modalComponent";
import CustomView from "pages/Objects/customView/customView";
import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadUserDynamicButton } from "redux/slices/dynamicButton";
import { loadRelatedObject } from "redux/slices/fieldsManagement";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deleteRecord,
  loadLinkingFieldValue,
  loadListObjectField,
  loadListObjectFieldSuccess,
  loadRecordData,
  loadRecordDataSuccess,
  loadSourceObjects,
  loadTargetObjects,
  setLinkingFieldValue,
  updateRecord,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalRecord from "../modalRecord";
import ModalDelete from "./delete/modalConfirmDelete";
import WithDetails from "./withDetails";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";

const TagsComponent = (props) => {
  const {
    recordID: record_id,
    data,
    objectId,
    edit: editProp,
    onRemoveItem,
    setDrag,
  } = props;
  const [allFields, setAllFields] = useState({});
  const [recordPerPage] = useState(50);
  const [currentPage, setCurrentPage] = useState(1);
  // const [setTotalPage] = useState(0);
  // const [totalRecord, setTotalRecord] = useState(0);
  const [isLoadingPagi, setIsLoadingPage] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [columns, setColumns] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [recordID, setRecordID] = useState("");
  const [form] = Form.useForm();
  const [editingKey, setEditingKey] = useState("");
  // const [openModal, setOpenModal] = useState(null);
  const [setFormValue] = useState({});
  // const [dataFilterLinking, setDataFilterLinking] = useState({});
  const [recordData] = useState({});
  const [fields, setFields] = useState([]);
  const [open, setOpen] = useState(false);
  const { linkingFieldValue, isLoadingDeleteRecord } = useSelector(
    (state) => state.objectsReducer
  );
  const [componentID, setComponentID] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [, setDataItem] = useState({});
  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);
  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);
  const [totalRecord, setTotalRecord] = useState(null);
  const [loadingDetails, setLoadingDetails] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState("");
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const [hiddenArray, setHiddenArray] = useState([]);
  const [recordDataNew, setRecordDataNew] = useState({});
  const [listObjectField, setListObjectField] = useState([]);
  const { allObject } = useSelector((state) => state.tenantsReducer);

  const dispatch = useDispatch();
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const [leftWidth] = useState(
    localStorage.getItem("crm_width_tag") ||
      localStorage.getItem("crm_width_tag") === "0px"
      ? "30%"
      : localStorage.getItem("crm_width_tag") || "30%"
  );

  const divRefTag = useRef(null);
  useEffect(() => {
    const observer = new ResizeObserver((entries) => {
      entries.forEach((entry) => {
        const newWidth = entry.contentRect.width;
        localStorage.setItem("crm_width_tag", `${newWidth}px`);
      });
    });

    if (divRefTag.current) {
      observer.observe(divRefTag.current);
    }

    return () => {
      observer.disconnect();
    };
  }, []);
  useEffect(() => {
    if (isLoadingDeleteRecord === false) {
      onCancel();
    }
    /*eslint-disable-next-line*/
  }, [isLoadingDeleteRecord]);

  const onCancel = () => {
    setSelectedRowKeys("");
    form.resetFields();
    setRecordID("");
    setDrag(false);
  };

  //custom view
  const [openCustomView, setOpenCustomView] = useState(false);
  //handle customview
  const [filterID, setFilterID] = useState("");
  useEffect(() => {
    setComponentID(data._id);
    setDataItem(data);
  }, [data, isModalVisible]);

  useEffect(() => {
    dispatch(
      loadRelatedObject({
        object_id: objectId,
      })
    );
    dispatch(
      loadSourceObjects({
        object_id: objectId,
      })
    );
    dispatch(
      loadTargetObjects({
        object_id: objectId,
      })
    );
    // dispatch(
    //   loadComponents({
    //     object_id: objectId,
    //   })
    // );
    // dispatch(
    //   loadListObjectField({
    //     api_version: "2",
    //     object_id: objectId,
    //   })
    // );
    /*eslint-disable-next-line*/
  }, [objectId, isModalVisible]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: data.related_object,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let hiddenArray1 = [];
          /*eslint-disable-next-line*/
          response.data.data.forEach((sections, idx) => {
            /*eslint-disable-next-line*/
            sections.fields.forEach((item, idx) => {
              if (item.type === "dynamic-field") {
                /*eslint-disable-next-line*/
                if (item.list_items)
                  /*eslint-disable-next-line*/
                  Object.entries(item.list_items).forEach(
                    ([key, value], idx) => {
                      hiddenArray1 = [...hiddenArray1, ...value];
                    }
                  );
              }
            });
          });

          let tempField = [];
          let tempFieldHidden = [];
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sections);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              if (hiddenArray1.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFields(tempFieldHidden);
          setHiddenArray(hiddenArray1);
        })
        .catch((err) => {});
    };
    if (data?.related_object) {
      checkToken();
    }
    // eslint-disable-next-line
  }, []);

  const edit = async (record, type) => {
    setLoadingDetails(true);
    const isTokenValid = await checkTokenExpiration();
    //hihi
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: data.related_object,
          show_meta_fields: true,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        dispatch(loadListObjectFieldSuccess(res.data.data));
        setListObjectField(res.data.data);
        let newObj = {};
        res.data.data.length > 0 &&
          res.data.data[res.data.data.length - 1]["main_object"][
            "sections"
            /*eslint-disable-next-line*/
          ].map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((item, idx) => {
              newObj[item.ID] = { ...item };
              newObj[item.ID].name = "";
            });
          });

        setAllFields(newObj);
        let flagEdit = false;
        /* eslint-disable-next-line */
        res.data.data.map((item, idx) => {
          if (Object.keys(item)[0] === "main_object") {
            flagEdit = item[Object.keys(item)[0]]?.edit_permission;
          }
        });
        if (flagEdit) {
          axios
            .get(
              BASE_URL_API +
                `load-record-data?id=${record._id}&object_id=${data.related_object}`,
              {
                headers: {
                  Authorization: isTokenValid,
                },
              }
            )
            .then((res) => {
              let newObj2 = {};
              /* eslint-disable-next-line */
              Object.entries(newObj).forEach(([key, value], index) => {
                if (value.type === "linkingobject") {
                  newObj2[key] = { ...res.data.data[key] };
                }
              });
              dispatch(loadRecordDataSuccess({ ...res.data.data }));
              dispatch(setLinkingFieldValue(newObj2));
              setRecordDataNew({ ...res.data.data });

              let tempObject = {};
              let newObj1 = {};
              /* eslint-disable-next-line */
              Object.entries(newObj).forEach(([key, value], index) => {
                if (value.type === "linkingobject") {
                  newObj1[key] = {
                    ...(res.data.data !== null && res.data.data[key]),
                  };
                }
              });

              /*eslint-disable-next-line*/
              Object.entries(res.data.data !== null && res.data.data).forEach(
                ([key, value], index) => {
                  // let tempObject = {};
                  if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "datetime-local" &&
                    value.value !== null
                  ) {
                    tempObject[key] = value ? moment(value.value) : null;
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "date" &&
                    value.value !== null
                  ) {
                    tempObject[key] = value ? moment(value.value) : null;
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "linkingobject"
                  ) {
                    tempObject[key] = {
                      ...res.data.data[key],
                    };
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "lookup"
                  ) {
                    tempObject[key] = res.data.data[key];
                  } else if (typeof value === "object") {
                    tempObject[key] = value.value;
                  } else {
                    tempObject[key] = value;
                  }
                }
              );
              form.setFieldsValue({ ...tempObject, ...newObj1, ...newObj2 });
              if (type === "normal") {
                setOpenModalRecord(true);
              }
            })
            .catch((err) => console.log(err));
        } else {
          Notification("error", "Bạn không có quyền để chỉnh sửa bản ghi này!");
        }
      })
      .catch((err) => {});
    setLoadingDetails(false);
  };

  useEffect(() => {
    let newObj = {};
    /* eslint-disable-next-line */
    fields.forEach((section, idx) => {
      /* eslint-disable-next-line */
      section.fields.map((item, idx) => {
        newObj[item.ID] = { ...item };
        newObj[item.ID].name = "";
      });
    });
    setAllFields(newObj);
  }, [fields]);

  useEffect(() => {
    form.setFieldsValue(linkingFieldValue);
    // eslint-disable-next-line
  }, [linkingFieldValue]);

  useEffect(() => {
    setRecordID(record_id);
  }, [record_id]);

  const cancel = () => {
    setEditingKey("");
    form.resetFields();
  };

  useEffect(() => {
    if (openModalRecord && recordID === "") {
      dispatch(
        loadLinkingFieldValue({
          object_related: objectId,
          record_id: record_id,
          object_create_record: data.related_object,
        })
      );
    }
    // eslint-disable-next-line
  }, [objectId, data, openModalRecord, recordID]);

  useEffect(() => {
    let comlumnsTemp = [];
    // eslint-disable-next-line
    columns.forEach((item, idx) => {
      if (item.title !== "Action") {
        let newItem = {
          ...item,
        };
        comlumnsTemp.push(newItem);
      }
    });
    comlumnsTemp.push({
      title: "Action",
      key: "action",
      fixed: "right",
      render: (_, record) => {
        return editingKey === record.key ? (
          <span>
            <Typography.Link
              onClick={() => save()}
              style={{
                marginRight: 8,
              }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a href>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ""}>
            <Space
              size="middle"
              style={{
                pointerEvents: `${editingKey !== "" ? "none" : ""}`,
              }}
            >
              <a href>
                <img
                  alt=""
                  src={editImg}
                  style={{ width: "20px" }}
                  onClick={() => {
                    // dispatch(
                    //   loadRecordData({
                    //     id: record.key,
                    //     object_id: data.related_object,
                    //   })
                    // );
                    setRecordID(record.key);
                    edit(record, "normal");
                  }}
                />
              </a>
              <a className="ant-dropdown-link" href>
                <img
                  alt=""
                  src={deleteImg}
                  style={{ width: "20px" }}
                  onClick={() => {
                    dispatch(setShowModalConfirmDelete(true));
                    dispatch(
                      loadRecordData({
                        id: record.key,
                        object_id: data.related_object,
                      })
                    );
                    setRecordID(record.key);
                    // setOpenDelete(true);
                  }}
                />
              </a>
            </Space>
          </Typography.Link>
        );
      },
    });
    setColumns(comlumnsTemp);
    // eslint-disable-next-line
  }, [recordData, editingKey]);

  useEffect(() => {
    dispatch(
      loadUserDynamicButton({
        object_id: objectId,
        record_id: record_id,
      })
    );
  }, [dispatch, objectId, record_id]);

  useEffect(() => {
    const checkToken = async () => {
      setLoading(true);
      setIsLoadingPage(true);
      const isTokenValid = await checkTokenExpiration();
      if (record_id)
        axios
          .post(
            BASE_URL_API + "consolidated-view/load-specific-component",
            {
              id: data._id,
              record_id: record_id,
              pagination: false,
              current_page: currentPage,
              record_per_page: recordPerPage,
              first_record_id: firstID,
              last_record_id: lastID,
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            setLoading(false);
            //columns
            let comlumnsTemp = [];
            // eslint-disable-next-line
            res.data.data.fields.map((item, idx) => {
              let newItem = {
                ...item,
                title: item.name,
                dataIndex: item.ID,
                key: item.ID,
                editable: true,
              };
              comlumnsTemp.push(newItem);
            });
            comlumnsTemp.push({
              title: "Action",
              key: "action",
              fixed: "right",
              render: (_, record) => {
                return editingKey === record.key ? (
                  <span>
                    <Typography.Link
                      onClick={() => save()}
                      style={{
                        marginRight: 8,
                      }}
                    >
                      Save
                    </Typography.Link>
                    <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
                      <a href>Cancel</a>
                    </Popconfirm>
                  </span>
                ) : (
                  <Typography.Link disabled={editingKey !== ""}>
                    <Space
                      size="middle"
                      style={{
                        pointerEvents: `${editingKey !== "" ? "none" : ""}`,
                      }}
                    >
                      <a href>
                        <img
                          alt=""
                          src={editImg}
                          style={{ width: "20px" }}
                          onClick={() => {
                            dispatch(
                              loadRecordData({
                                id: record.key,
                                object_id: data.related_object,
                              })
                            );
                            setRecordID(record.key);
                            edit(record, "normal");
                          }}
                        />
                      </a>
                      <a className="ant-dropdown-link" href>
                        <img
                          alt=""
                          src={deleteImg}
                          style={{ width: "20px" }}
                          onClick={() => {
                            dispatch(setShowModalConfirmDelete(true));
                            dispatch(
                              loadRecordData({
                                id: record.key,
                                object_id: data.related_object,
                              })
                            );
                            setRecordID(record.key);
                            // setOpenDelete(true);
                          }}
                        />
                      </a>
                    </Space>
                  </Typography.Link>
                );
              },
            });
            setColumns(comlumnsTemp);
            //dataSource
            let dataSourceTmp = [];
            // eslint-disable-next-line
            res.data.data.record_data.data.map((item, idx) => {
              let newItem = {};
              newItem["key"] = item._id;
              /* eslint-disable-next-line */
              Object.entries(item).forEach(([key, value], idx) => {
                if (typeof value === "object" && value !== null) {
                  newItem[key] = value.value;
                } else {
                  newItem[key] = value;
                }
              });
              dataSourceTmp.push(newItem);
            });
            setDataSource(dataSourceTmp);
            setIsLoadingPage(false);
          })
          .catch((err) => {
            setIsLoadingPage(false);
            setLoading(false);
          });
    };
    checkToken();
    // eslint-disable-next-line
  }, [record_id]);

  const save = async () => {
    let listValue = [];
    const isTokenValid = await checkTokenExpiration();
    /* eslint-disable-next-line */
    Object.entries(form.getFieldsValue()).forEach(([key, value], idx) => {
      if (key !== "assignTo") {
        if (allFields[key].type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              result.push(item.url);
            });
          } else {
            // eslint-disable-next-line
            value.map((item, idx) => {
              result.push(item.url);
            });
          }
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (allFields[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (allFields[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (allFields[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    dispatch(
      updateRecord({
        data: {
          data: listValue,
          owner_id: recordData.owner,
          id: recordData._id,
          object_id: objectId,
        },
        load: {
          object_id: objectId,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: [],
          },
        },
      })
    );
    // initData(undefined, undefined);
    cancel();

    setLoading(true);
    setIsLoadingPage(true);
    if (record_id)
      axios
        .post(
          BASE_URL_API + "consolidated-view/load-specific-component",
          {
            id: data._id,
            record_id: record_id,
            pagination: false,
            current_page: currentPage,
            record_per_page: recordPerPage,
            first_record_id: firstID,
            last_record_id: lastID,
            custom_view_id: filterID,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          //columns
          let dataSourceTmp = [];
          if (Array.isArray(res.data.data.record_data.data)) {
            // eslint-disable-next-line
            res.data.data.record_data.data.map((item, idx) => {
              let newItem = {};
              newItem["key"] = item._id;
              /* eslint-disable-next-line */
              Object.entries(item).forEach(([key, value], idx) => {
                if (typeof value === "object" && value !== null) {
                  newItem[key] = value.value;
                } else {
                  newItem[key] = value;
                }
              });
              dataSourceTmp.push(newItem);
            });
          }
          setDataSource(dataSourceTmp);
          setIsLoadingPage(false);
        })
        .catch((err) => {
          setLoading(false);
          setIsLoadingPage(false);
        });
  };

  const initData = async (currentPage, recordPerPage, filterID) => {
    if (!currentPage && !recordPerPage) {
      currentPage = 1;
      recordPerPage = 50;
    }
    setLoading(true);
    setIsLoadingPage(true);
    const isTokenValid = await checkTokenExpiration();
    if (record_id)
      axios
        .post(
          BASE_URL_API + "consolidated-view/load-specific-component",
          {
            id: data._id,
            record_id: record_id,
            pagination: false,
            current_page: currentPage,
            record_per_page: recordPerPage,
            first_record_id: firstID,
            last_record_id: lastID,
            custom_view_id: filterID,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          //columns
          let comlumnsTemp = [];
          // eslint-disable-next-line
          res.data.data.fields.map((item, idx) => {
            let newItem = {
              ...item,
              title: item.name,
              dataIndex: item.ID,
              key: item.ID,
              editable: true,
            };
            comlumnsTemp.push(newItem);
          });
          comlumnsTemp.push({
            title: "Action",
            key: "action",
            fixed: "right",
            render: (_, record) => {
              return editingKey === record.key ? (
                <span>
                  <Typography.Link
                    onClick={() => save()}
                    style={{
                      marginRight: 8,
                    }}
                  >
                    Save
                  </Typography.Link>
                  <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
                    <a href>Cancel</a>
                  </Popconfirm>
                </span>
              ) : (
                <Typography.Link disabled={editingKey !== ""}>
                  <Space
                    size="middle"
                    style={{
                      pointerEvents: `${editingKey !== "" ? "none" : ""}`,
                    }}
                  >
                    <a href>
                      <img
                        alt=""
                        src={editImg}
                        style={{ width: "20px" }}
                        onClick={() => {
                          dispatch(
                            loadRecordData({
                              id: record.key,
                              object_id: data.related_object,
                            })
                          );
                          setRecordID(record.key);
                          edit(record, "normal");
                        }}
                      />
                    </a>
                    <a className="ant-dropdown-link" href>
                      <img
                        alt=""
                        src={deleteImg}
                        style={{ width: "20px" }}
                        onClick={() => {
                          dispatch(setShowModalConfirmDelete(true));
                          dispatch(
                            loadRecordData({
                              id: record.key,
                              object_id: data.related_object,
                            })
                          );
                          setRecordID(record.key);
                          // setOpenDelete(true);
                        }}
                      />
                    </a>
                  </Space>
                </Typography.Link>
              );
            },
          });
          setColumns(comlumnsTemp);
          //dataSource
          let dataSourceTmp = [];
          if (Array.isArray(res.data.data.record_data.data)) {
            // eslint-disable-next-line
            res.data.data.record_data.data.map((item, idx) => {
              let newItem = {};
              newItem["key"] = item._id;
              /* eslint-disable-next-line */
              Object.entries(item).forEach(([key, value], idx) => {
                if (typeof value === "object" && value !== null) {
                  newItem[key] = value.value;
                } else {
                  newItem[key] = value;
                }
              });
              dataSourceTmp.push(newItem);
            });
          }
          setDataSource(dataSourceTmp);
          setIsLoadingPage(false);
        })
        .catch((err) => {
          setIsLoadingPage(false);
          setLoading(false);
        });
  };
  const ref = useRef(null);

  useEffect(() => {
    setWidth(
      document.getElementById(data._id) &&
        document.getElementById(data._id).clientWidth
    );
    // eslint-disable-next-line
  }, [
    // eslint-disable-next-line
    document.getElementById(data._id) &&
      document.getElementById(data._id).clientWidth,
  ]);

  const [, setWidth] = useState(0);

  useEffect(() => {
    if (filterID !== "") {
      initData(currentPage, recordPerPage, filterID);
    }
    /* eslint-disable-next-line */
  }, [filterID]);

  const loadData = () => {
    initData(currentPage, recordPerPage, filterID);
  };

  useEffect(() => {
    setNext(
      dataSource.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + dataSource.length
    );
    /* eslint-disable-next-line */
  }, [dataSource]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
    /* eslint-disable-next-line */
  }, [currentPage]);

  const [loadPagi, setLoadPagi] = useState(false);

  const handleGetTotalRecord = async () => {
    const isTokenValid = await checkTokenExpiration();
    setLoadPagi(true);
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: true,
          current_page: currentPage,
          record_per_page: recordPerPage,
          custom_view_id: filterID,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setLoadPagi(false);
        setTotalRecord(res.data.data.total_record);
      })
      .catch((err) => {
        setLoadPagi(false);
      });
  };

  useEffect(() => {
    initData(50, currentPage);
    /* eslint-disable-next-line */
  }, [firstID, lastID]);

  function numberWithCommas(x, col) {
    if (x === 0) {
      return "0";
    } else {
      x = x.toFixed(
        col?.decimal_separator === 0 ? 0 : col?.decimal_separator || 3
      );
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  return (
    <Wrapper
      ref={ref}
      id={data._id}
      style={{
        paddingTop: `${
          window.location.pathname.includes("list-view-with-details")
            ? ""
            : "36px"
        }`,
      }}
    >
      <div
        style={{
          height: "36px",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : "fixed"
          }`,
          top: 0,
          left: 0,
          zIndex: 9,
          boxShadow: "inset 0px -1px 0px #F0F0F0",
        }}
      >
        <Row
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <div>
            {data.name}
            <Tooltip title="Add new view">
              <button
                className="button-filter"
                style={{
                  marginLeft: "10px",
                  textAlign: "center",
                }}
                onClick={() => {
                  setOpenCustomView(true);
                }}
              >
                <img
                  alt=""
                  src={fiterIcon}
                  style={{ width: "12px", textAlign: "center" }}
                />
              </button>
            </Tooltip>
          </div>

          {/* <Col
            span={16}
            style={{
              textAlign: "right",
              display: `${width > 520 ? "" : "none"}`,
            }}
            className="pagination-col"
          > */}
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <Tooltip title="Reload">
              <CustomButtonReload
                shape="circle"
                // disabled={
                //   Object.keys(selectedRecord).length === 0 ? true : false
                // }
                onClick={() => {
                  loadData();
                }}
              >
                <img alt="" src={Refresh} style={{ width: "18px" }} />
              </CustomButtonReload>
            </Tooltip>
            <Tooltip title="Add new record">
              <CustomButton
                shape="circle"
                onClick={() => {
                  dispatch(
                    loadListObjectField({
                      api_version: "2",
                      object_id: data.related_object,
                      show_meta_fields: true,
                    })
                  );
                  setRecordID("");
                  setOpenModalRecord(true);
                }}
              >
                +
              </CustomButton>
            </Tooltip>
            {/* <CustomPagination
              showQuickJumper
              current={currentPage}
              defaultPageSize={recordPerPage}
              total={dataSource.length === 0 ? 10 : dataSource.length + 1}
              showSizeChanger
              simple
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              onChange={(e, pageSize) => {
                setCurrentPage(e);
                //loadData
                setRecordPerPage(pageSize);
                // let searchData = [];
                initData(e, pageSize);
              }}
            /> */}
            <NewCustomPagination>
              {isLoadingPagi ? (
                <Spin />
              ) : (
                <>
                  <div className="total-record">
                    {next === 0 ? 0 : prev} - {next}{" "}
                    {loadPagi ? (
                      <Spin style={{ marginLeft: "10px" }} />
                    ) : totalRecord === null ? (
                      <div
                        className="reload-pagi"
                        onClick={() => {
                          handleGetTotalRecord();
                        }}
                      >
                        <img alt="" src={Reload} />
                      </div>
                    ) : (
                      `of ${totalRecord ? totalRecord : 0} records`
                    )}
                  </div>

                  <div
                    className="left-pagi"
                    style={{
                      pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                      cursor: `${
                        currentPage === 1 ? "not-allowed" : "pointer"
                      }`,
                      opacity: `${currentPage === 1 ? 0.5 : 1}`,
                    }}
                    onClick={() => {
                      let currentPageTemp = currentPage;
                      currentPageTemp = currentPageTemp - 1;
                      setCurrentPage(currentPageTemp);
                      setFirstID(
                        dataSource.length > 0 ? dataSource[0]._id : null
                      );
                      setLastID(null);
                      // initData(currentPageTemp, 50);
                    }}
                  >
                    <img alt="" src={LeftPagi} />
                  </div>
                  <div
                    className="right-pagi"
                    style={{
                      pointerEvents: `${
                        dataSource.length < recordPerPage ? "none" : ""
                      }`,
                      cursor: `${
                        dataSource.length < recordPerPage
                          ? "not-allowed"
                          : "pointer"
                      }`,
                      opacity: `${dataSource.length < recordPerPage ? 0.5 : 1}`,
                    }}
                    onClick={() => {
                      let currentPageTemp = currentPage;
                      currentPageTemp = currentPageTemp + 1;
                      setCurrentPage(currentPageTemp);
                      // initData(currentPageTemp, 50);
                      setLastID(
                        dataSource.length > 0
                          ? dataSource[dataSource.length - 1]._id
                          : null
                      );
                      setFirstID(null);
                    }}
                  >
                    <img alt="" src={RightPagi} />
                  </div>
                </>
              )}
            </NewCustomPagination>
            {editProp ? (
              <Dropdown
                trigger="click"
                overlay={
                  <Menu>
                    {window.location.pathname.includes(
                      "list-view-with-details"
                    ) ? (
                      ""
                    ) : (
                      <Menu.Item
                        onClick={() => {
                          dispatch(
                            loadListObjectField({
                              api_version: "2",
                              object_id: data.related_object,
                              show_meta_fields: true,
                            })
                          );
                          setIsModalVisible(true);
                          setComponentID(data._id);
                          setDrag(false);
                        }}
                      >
                        Edit component
                      </Menu.Item>
                    )}

                    {checkRule("delete") ? (
                      <Menu.Item
                        onClick={() => {
                          setDrag(false);
                          onRemoveItem(JSON.stringify(data));
                        }}
                      >
                        Remove
                      </Menu.Item>
                    ) : (
                      ""
                    )}
                  </Menu>
                }
              >
                <Button style={{ marginLeft: "8px" }}>
                  <EllipsisOutlined />
                </Button>
              </Dropdown>
            ) : (
              ""
            )}
          </div>
          {/* </Col> */}
        </Row>
      </div>
      {loading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            marginTop: "10%",
            flexDirection: "column",
          }}
        >
          <Spin />
        </div>
      ) : data?.with_details ? (
        <div
          style={{
            width: "100%",
            display: "flex",
            height: "100%",
          }}
        >
          <div
            style={{
              width: "30%",
              overflow: "auto",
              borderRight: "1px solid #ececec",
            }}
            ref={divRefTag}
          >
            <Form
              form={form}
              component={false}
              onValuesChange={(value, values) => {
                setFormValue(values);
              }}
            >
              {dataSource.length > 0
                ? dataSource.map((item, idx) => {
                    return (
                      <CustomTag
                        onDoubleClick={() => {
                          dispatch(
                            loadRecordData({
                              id: item.key,
                              object_id: data.related_object,
                            })
                          );
                          setRecordID(item.key);
                          edit(item, "normal");
                        }}
                        onClick={() => {
                          setRecordID(item.key);
                          edit(item, "details");
                          setSelectedRowKeys(item.key);
                        }}
                        className={recordID === item.key ? "active" : ""}
                      >
                        <div className="custom-bg"></div>
                        <div style={{ zIndex: "4" }}>
                          {/* eslint-disable-next-line */}
                          {columns.map((column, index) => {
                            if (column.title !== "Action") {
                              return (
                                <div className="item-tag">
                                  <div style={{ display: "flex" }}>
                                    <div className="title">
                                      {column.title === "Owner"
                                        ? "Assign to"
                                        : column.title}
                                      :{" "}
                                    </div>
                                    <div className="value">
                                      {typeof item[column.key] === "object" &&
                                      column.type === "file" &&
                                      item[column.key] !== null
                                        ? item[column.key].map((ele, idx) => {
                                            let name = ele && ele.split("/");
                                            if (name !== null)
                                              return (
                                                <div>
                                                  <a
                                                    href={
                                                      allObject?.find(
                                                        (object) =>
                                                          object?.object_id ===
                                                            objectId &&
                                                          object?.status
                                                      ) && ele?.split(BE_URL)[1]
                                                        ? `${
                                                            FE_URL +
                                                            ele?.split(
                                                              BE_URL
                                                            )[1]
                                                          }`
                                                        : ele
                                                    }
                                                  >
                                                    {name[name.length - 1]}
                                                  </a>
                                                </div>
                                              );
                                            else return null;
                                          })
                                        : typeof item[column.key] ===
                                            "object" &&
                                          column.type === "select" &&
                                          item[column.key] !== null
                                        ? item[column.key].map((ele, idx) => {
                                            return (
                                              <div
                                                style={{
                                                  background: "white",
                                                  marginRight: "5px",
                                                  marginBottom: "5px",
                                                  padding: "2px",
                                                  borderRadius: "5px",
                                                  color: "black",
                                                  float: "left",
                                                }}
                                              >
                                                {ele}
                                              </div>
                                            );
                                          })
                                        : typeof item[column.key] === "number"
                                        ? numberWithCommas(
                                            item[column.key],
                                            column
                                          )
                                        : parse(
                                            item[column.key]
                                              ? item[column.key]
                                              : "",
                                            optionsParse
                                          )}
                                    </div>
                                  </div>
                                </div>
                              );
                            }
                          })}
                        </div>
                        <Dropdown
                          overlay={
                            <Menu>
                              <Menu.Item
                                onClick={() => {
                                  dispatch(
                                    loadRecordData({
                                      id: item.key,
                                      object_id: data.related_object,
                                    })
                                  );
                                  setRecordID(item.key);
                                  edit(item, "normal");
                                  setDrag(false);
                                }}
                                disabled={data?.with_details}
                              >
                                Edit
                              </Menu.Item>
                              <Menu.Item
                                onClick={async () => {
                                  // dispatch(setShowModalConfirmDelete(true));
                                  setDrag(false);
                                  const isTokenValid =
                                    await checkTokenExpiration();
                                  axios
                                    .post(
                                      BASE_URL_API +
                                        "object/objects-fields-permission",
                                      {
                                        api_version: "2",
                                        object_id: data.related_object,
                                        show_meta_fields: true,
                                      },
                                      {
                                        headers: {
                                          Authorization: isTokenValid,
                                        },
                                      }
                                    )
                                    .then((res) => {
                                      let flagEdit = false;

                                      res.data.data.map((item, idx) => {
                                        if (
                                          Object.keys(item)[0] === "main_object"
                                        ) {
                                          flagEdit =
                                            item[Object.keys(item)[0]]
                                              ?.delete_permission;
                                        }
                                        return null;
                                      });
                                      if (flagEdit) {
                                        dispatch(
                                          loadRecordData({
                                            id: item.key,
                                            object_id: data.related_object,
                                          })
                                        );
                                        setRecordID(item.key);
                                        // setOpenDelete(true);
                                        setOpen(true);
                                      } else {
                                        Notification(
                                          "error",
                                          "Bạn không có quyền để xóa bản bản ghi này!"
                                        );
                                      }
                                    })
                                    .catch(() => {});
                                }}
                              >
                                Delete
                              </Menu.Item>
                            </Menu>
                          }
                          placement="bottomLeft"
                        >
                          <Button size="small">
                            <EllipsisOutlined />
                          </Button>
                        </Dropdown>
                      </CustomTag>
                    );
                  })
                : "No data! Click button above to add record!"}
            </Form>
          </div>
          <div
            style={{
              // width: `calc(100% - ${
              //   localStorage.getItem("crm_width_tag") || "30%"
              // })`,
              width: "70%",
              overflowY: "auto",
              height: "100%",
            }}
          >
            <WithDetails
              objectId={data.related_object}
              form={form}
              recordID={recordID}
              setRecordID={setRecordID}
              recordPerPage={recordPerPage}
              currentPage={currentPage}
              loadingDetails={loadingDetails}
              selectedRowKeys={selectedRowKeys}
              onCancel={onCancel}
              leftWidth={leftWidth}
              recordData={recordDataNew}
              hiddenArray={hiddenArray}
              setHiddenArray={setHiddenArray}
              fields={fields}
              setFields={setFields}
              loadData={loadData}
              listObjectField={listObjectField}
            />
          </div>
        </div>
      ) : (
        <div style={{ width: "100%", overflow: "auto", padding: "8px" }}>
          <Form
            form={form}
            component={false}
            onValuesChange={(value, values) => {
              setFormValue(values);
            }}
          >
            {dataSource.length > 0
              ? dataSource.map((item, idx) => {
                  return (
                    <CustomTag
                      onDoubleClick={() => {
                        // dispatch(
                        //   loadListObjectField({
                        //     api_version: "2",
                        //     object_id: data.related_object,
                        //     show_meta_fields: true,
                        //   })
                        // );
                        dispatch(
                          loadRecordData({
                            id: item.key,
                            object_id: data.related_object,
                          })
                        );
                        setRecordID(item.key);
                        edit(item, "normal");
                      }}
                    >
                      <div className="custom-bg"></div>
                      <div style={{ zIndex: "4" }}>
                        {/* eslint-disable-next-line */}
                        {columns.map((column, index) => {
                          if (column.title !== "Action") {
                            return (
                              <div className="item-tag">
                                <div style={{ display: "flex" }}>
                                  <div className="title">
                                    {column.title === "Owner"
                                      ? "Assign to"
                                      : column.title}
                                    :{" "}
                                  </div>
                                  <div className="value">
                                    {typeof item[column.key] === "object" &&
                                    column.type === "file" &&
                                    item[column.key] !== null
                                      ? item[column.key].map((ele, idx) => {
                                          let name = ele && ele.split("/");
                                          if (name !== null)
                                            return (
                                              <div>
                                                <a
                                                  href={
                                                    allObject?.find(
                                                      (object) =>
                                                        object?.object_id ===
                                                          objectId &&
                                                        object?.status
                                                    ) && ele?.split(BE_URL)[1]
                                                      ? `${
                                                          FE_URL +
                                                          ele?.split(BE_URL)[1]
                                                        }`
                                                      : ele
                                                  }
                                                >
                                                  {name[name.length - 1]}
                                                </a>
                                              </div>
                                            );
                                          else return null;
                                        })
                                      : typeof item[column.key] === "object" &&
                                        column.type === "select" &&
                                        item[column.key] !== null
                                      ? item[column.key].map((ele, idx) => {
                                          return (
                                            <div
                                              style={{
                                                background: "white",
                                                marginRight: "5px",
                                                marginBottom: "5px",
                                                padding: "2px",
                                                borderRadius: "5px",
                                                color: "black",
                                                float: "left",
                                              }}
                                            >
                                              {ele}
                                            </div>
                                          );
                                        })
                                      : typeof item[column.key] === "number"
                                      ? numberWithCommas(
                                          item[column.key],
                                          column
                                        )
                                      : parse(
                                          item[column.key]
                                            ? item[column.key]
                                            : "",
                                          optionsParse
                                        )}
                                  </div>
                                </div>
                              </div>
                            );
                          }
                        })}
                      </div>
                      <Dropdown
                        overlay={
                          <Menu>
                            <Menu.Item
                              onClick={() => {
                                dispatch(
                                  loadRecordData({
                                    id: item.key,
                                    object_id: data.related_object,
                                  })
                                );
                                setRecordID(item.key);
                                edit(item, "normal");
                                setDrag(false);
                              }}
                              disabled={data?.with_details}
                            >
                              Edit
                            </Menu.Item>
                            <Menu.Item
                              onClick={async () => {
                                // dispatch(setShowModalConfirmDelete(true));
                                setDrag(false);
                                const isTokenValid =
                                  await checkTokenExpiration();
                                axios
                                  .post(
                                    BASE_URL_API +
                                      "object/objects-fields-permission",
                                    {
                                      api_version: "2",
                                      object_id: data.related_object,
                                      show_meta_fields: true,
                                    },
                                    {
                                      headers: {
                                        Authorization: isTokenValid,
                                      },
                                    }
                                  )
                                  .then((res) => {
                                    let flagEdit = false;

                                    res.data.data.map((item, idx) => {
                                      if (
                                        Object.keys(item)[0] === "main_object"
                                      ) {
                                        flagEdit =
                                          item[Object.keys(item)[0]]
                                            ?.delete_permission;
                                      }
                                      return null;
                                    });
                                    if (flagEdit) {
                                      dispatch(
                                        loadRecordData({
                                          id: item.key,
                                          object_id: data.related_object,
                                        })
                                      );
                                      setRecordID(item.key);
                                      // setOpenDelete(true);
                                      setOpen(true);
                                    } else {
                                      Notification(
                                        "error",
                                        "Bạn không có quyền để xóa bản bản ghi này!"
                                      );
                                    }
                                  })
                                  .catch(() => {});
                              }}
                            >
                              Delete
                            </Menu.Item>
                          </Menu>
                        }
                        placement="bottomLeft"
                      >
                        <Button size="small">
                          <EllipsisOutlined />
                        </Button>
                      </Dropdown>
                    </CustomTag>
                  );
                })
              : "No data! Click button above to add record!"}
          </Form>
        </div>
      )}

      <ModalRecord
        open={openModalRecord}
        setOpen={setOpenModalRecord}
        initData={initData}
        recordPerPage={recordPerPage}
        currentPage={currentPage}
        form={form}
        recordID={recordID}
        setRecordID={setRecordID}
        fields={fields}
        objectId={data.related_object}
        loadData={loadData}
      />

      <ModalDelete
        openConfirm={open}
        setOpenConfirm={setOpen}
        title={"bản ghi này"}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deleteRecord}
        dataDelete={{
          data: {
            id: recordID,
            object_id: data.related_object,
          },
          load: {
            object_id: data.related_object,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: [],
            },
          },
          type: window.location.pathname.includes("list-view-with-details")
            ? "reload"
            : "",
        }}
        setShowModalDelete={() => {}}
        loadData={loadData}
      />

      <ModalComponent
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        selectedObject={objectId}
        dataItem={data}
        componentID={componentID}
        setComponentID={setComponentID}
        setDataItem={setDataItem}
        initData={initData}
      />

      <CustomView
        visible={openCustomView}
        setVisible={setOpenCustomView}
        objectId={data.related_object}
        mode={"consolidated-view"}
        filterID={filterID}
        setFilterID={setFilterID}
      />
    </Wrapper>
  );
};

export default TagsComponent;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  .active {
    border: 2px solid ${(props) => props.theme.main};
  }
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  table {
    width: max-content;
  }
  .button-filter {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    margin-right: 8px;
    border: 1px solid ${(props) => props.theme.main};
    :hover {
      background: ${(props) => props.theme.main};
      color: ${(props) => props.theme.white};
      border: 1px solid ${(props) => props.theme.main};
      cursor: pointer;
    }
  }
  @media screen and (max-width: 400px) {
    .pagination-col {
      display: none;
    }
  }
`;
// const CustomPagination = styled(Pagination)`
//   display: flex;
//   justify-content: center;
//   align-items: center;
//   .ant-pagination-simple-pager {
//     display: none;
//   }
// `;

const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const CustomTag = styled.div`
  margin-bottom: 8px;
  padding: 8px;
  /* background-color: #f2f4f5; */
  display: flex;
  justify-content: space-between;
  overflow: auto;
  position: relative;
  background: rgb(0 171 85 / 15%);
  cursor: pointer;
  .custom-bg {
    width: 100%;
    height: 100%;
    /* background-color: ${(props) => props.theme.main}; */
    position: absolute;
    top: 0;
    left: 0;
    z-index: 0;
    /* opacity: 0.08; */
  }
  .item-tag {
    display: flex;
    margin-bottom: 8px;
    z-index: 99999;
    .title {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 16px;
      line-height: 130%;
      /* identical to box height, or 21px */

      letter-spacing: 0.01em;

      /* text - main */

      color: #252424;
    }
    .value {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 130%;
      /* identical to box height, or 21px */
      margin-left: 8px;
      letter-spacing: 0.01em;

      /* text - main */

      color: #252424;
    }
  }
`;
const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;

const CustomButtonReload = styled(Button)`
  background: ${(props) => props.theme.white};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;
