import { useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { loadInfoComponent } from "redux/slices/campaign";
import _ from "lodash";

import { LoadingOutlined } from "@ant-design/icons";
import Switch from "antd/lib/switch";
import Checkbox from "antd/lib/checkbox";

import IconName from "assets/icons/campaign/CampaignName.svg";
import IconSchedule from "assets/icons/campaign/CampaignSchedule.svg";
import IconTime from "assets/icons/campaign/CampaignTime.svg";
import IconType from "assets/icons/campaign/CampaignType.svg";
import moment from "moment";

import { updateCampaignConfig } from "redux/slices/campaign";

function CampaignGeneral({ objectId, recordID }) {
  const dispatch = useDispatch();

  const { isLoading, infoComponent } = useSelector(
    (state) => state.campaignReducer
  );
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const handleActive = (type, checked) => {
    let payload = {
      oldData: { ...infoComponent },
      newData: {
        campaign_config: {
          campaign_id: recordID,
          is_one_time: _.get(infoComponent, "is_one_time", false),
          checking_workflow_sla: _.get(
            infoComponent,
            "checking_workflow_sla",
            false
          ),
        },
        fld_campaign_running_status_01: _.get(
          infoComponent,
          "fld_campaign_running_status_01",
          ""
        ),
      },
    };
    switch (type) {
      case "active":
        payload.newData = {
          ...payload.newData,
          fld_campaign_running_status_01: checked ? "Running" : "Stop",
        };
        dispatch(updateCampaignConfig(payload));
        break;
      case "checking":
        payload.newData = {
          ...payload.newData,
          campaign_config: {
            ...payload.newData.campaign_config,
            checking_workflow_sla: checked,
          },
        };
        dispatch(updateCampaignConfig(payload));
        break;
      case "close":
        payload.newData = {
          ...payload.newData,
          campaign_config: {
            ...payload.newData.campaign_config,
            is_one_time: checked,
          },
        };
        dispatch(updateCampaignConfig(payload));
        break;

      default:
        break;
    }
  };

  const renderSchedule = (value) => {
    if (_.isEqual(value, [0, 1, 2, 3, 4, 5, 6])) {
      return <span>All week,&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3, 4])) {
      return <span>Weekdays,&nbsp;</span>;
    } else if (_.isEqual(value, [5, 6])) {
      return <span>Weekends,&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1])) {
      return <span>Mon-Tue,&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2])) {
      return <span>Mon-Wed,&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3])) {
      return <span>Mon-Thu,&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3, 4, 5])) {
      return <span>Mon-Sat,&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2])) {
      return <span>Tue-Wed,&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3])) {
      return <span>Tue-Thu,&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4])) {
      return <span>Tue-Fri,&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4, 5])) {
      return <span>Tue-Sat,&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4, 5, 6])) {
      return <span>Tue-Sun,&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3])) {
      return <span>Wed-Thu,&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4])) {
      return <span>Wed-Fri,&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4, 5])) {
      return <span>Wed-Sat,&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4, 5, 6])) {
      return <span>Wed-Sun,&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4])) {
      return <span>Thu-Fri,&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4, 5])) {
      return <span>Thu-Sat,&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4, 5, 6])) {
      return <span>Thu-Sun,&nbsp;</span>;
    } else if (_.isEqual(value, [4, 5])) {
      return <span>Fri-Sat,&nbsp;</span>;
    } else if (_.isEqual(value, [4, 5, 6])) {
      return <span>Fri-Sun,&nbsp;</span>;
    } else {
      let result = [];
      value.forEach((item) => {
        switch (item) {
          case 0:
            result.push("Mon, ");
            break;
          case 1:
            result.push("Tue, ");
            break;
          case 2:
            result.push("Wed, ");
            break;
          case 3:
            result.push("Thu, ");
            break;
          case 4:
            result.push("Fri, ");
            break;
          case 5:
            result.push("Sat, ");
            break;
          case 6:
            result.push("Sun, ");
            break;

          default:
            break;
        }
      });
      return <span>{result}&nbsp;</span>;
    }
  };

  useEffect(() => {
    if (recordID) {
      dispatch(
        loadInfoComponent({
          campaign_id: recordID,
        })
      );
    }
  }, [dispatch, recordID]);

  return (
    <Wrapper>
      {isLoading.infoComponent ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            marginTop: "10%",
            flexDirection: "column",
            background: "white",
          }}
        >
          <LoadingOutlined
            style={{ fontSize: 60, color: defaultBrandName.theme_color }}
            spin
          />
          <div style={{ textAlign: "center", marginTop: "20px" }}>
            Loading. . .{" "}
          </div>
        </div>
      ) : (
        <>
          <div
            style={{
              border: "1px solid rgba(89, 106, 106, 0.25)",
              borderRadius: "5px",
              display: "flex",
              marginBottom: "16px",
            }}
          >
            <CampaignStatus
              active={
                _.get(infoComponent, "fld_campaign_status_01", "") === "Draft"
              }
              border={
                _.get(infoComponent, "fld_campaign_status_01", "") !==
                "In Queue"
              }
            >
              <span>Draft</span>
            </CampaignStatus>
            <CampaignStatus
              active={
                _.get(infoComponent, "fld_campaign_status_01", "") ===
                "In Queue"
              }
              border={
                _.get(infoComponent, "fld_campaign_status_01", "") !== "Running"
              }
            >
              <span>In Queue</span>
            </CampaignStatus>
            <CampaignStatus
              active={
                _.get(infoComponent, "fld_campaign_status_01", "") === "Running"
              }
              border={
                _.get(infoComponent, "fld_campaign_status_01", "") !== "Closed"
              }
            >
              <span>Running</span>
            </CampaignStatus>
            <CampaignStatus
              active={
                _.get(infoComponent, "fld_campaign_status_01", "") === "Closed"
              }
            >
              <span>Closed</span>
            </CampaignStatus>
          </div>

          <WrapInfo>
            <Type>
              <div style={{ display: "flex" }}>
                <img src={IconType} alt="IconType" />
                <p>Type</p>

                <span>{_.get(infoComponent, "fld_campaign_type_01", "")}</span>
              </div>

              <div style={{ display: "flex" }}>
                <Switch
                  checked={
                    _.get(
                      infoComponent,
                      "fld_campaign_running_status_01",
                      ""
                    ) === "Running"
                  }
                  onChange={(e) => handleActive("active", e)}
                />
                <p>Active</p>
              </div>
            </Type>

            <Name>
              <img src={IconName} alt="IconName" />
              <p>Name</p>

              <span>{_.get(infoComponent, "fld_campaign_name_01", "")}</span>
            </Name>

            <Time>
              <img src={IconTime} alt="IconTime" />
              <p>Date</p>

              <WrapTime>
                {!_.isEmpty(
                  _.get(infoComponent, "fld_campaign_start_date_01", "")
                ) && (
                  <>
                    <span>
                      {moment(
                        _.get(infoComponent, "fld_campaign_start_date_01", "")
                      ).format("dddd, DD MM YYYY")}
                    </span>
                    &nbsp;&nbsp;-&nbsp;&nbsp;
                  </>
                )}

                {!_.isEmpty(
                  _.get(infoComponent, "fld_campaign_end_date_01", "")
                ) && (
                  <span>
                    {moment(
                      _.get(infoComponent, "fld_campaign_end_date_01", "")
                    ).format("dddd, DD MM YYYY")}
                  </span>
                )}
              </WrapTime>
            </Time>

            <Schedule>
              <img src={IconSchedule} alt="IconSchedule" />
              <p>Schedule</p>

              <WrapSchedule>
                {_.map(_.get(infoComponent, "schedule_configs", []), (item) => (
                  <>
                    {_.size(_.get(item, "day", [])) > 0 && (
                      <div style={{ display: "flex" }}>
                        <span>{renderSchedule(_.get(item, "day", []))}</span>
                        <span>
                          {item.time[0]} - {item.time[1]}
                        </span>
                      </div>
                    )}
                  </>
                ))}
              </WrapSchedule>
            </Schedule>

            <Checkbox
              style={{ width: "100%", marginBottom: 16, marginTop: 16 }}
              onChange={(e) => handleActive("checking", e.target.checked)}
              checked={_.get(infoComponent, "checking_workflow_sla", false)}
            >
              Checking workflow SLA
            </Checkbox>

            <Checkbox
              onChange={(e) => handleActive("close", e.target.checked)}
              checked={_.get(infoComponent, "is_one_time", false)}
            >
              Close Campaign when all tasks are finished
            </Checkbox>
          </WrapInfo>
        </>
      )}
    </Wrapper>
  );
}

export default CampaignGeneral;

const Wrapper = styled.div`
  padding: 16px;
  width: 100%;

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0;
  }

  .ant-checkbox-wrapper {
    span {
      font-size: 16px;
      line-height: 22px;
      color: #2c2c2c;
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const CampaignStatus = styled.div`
  width: calc(100% / 4);
  height: 36px;
  background: ${(props) => (props.active ? props.theme.main : "#f6fbff")};
  position: relative;
  color: ${(props) => (props.active ? "#fff" : "#8c8c8c")};
  font-size: 16px;
  font-family: var(--roboto-500);
  line-height: 22px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: ${({ border }) =>
    border && "1px solid rgba(89, 106, 106, 0.25)"};

  span {
    margin-left: ${(props) => (props.active ? "20px" : "0")};
  }

  :after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 24px solid #f6fbff;
    border-top: 24px solid transparent;
    border-bottom: 24px solid transparent;
  }

  :before {
    content: "";
    position: absolute;
    right: -24px;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: ${(props) =>
      props.active ? `24px solid ${props.theme.main}` : "none"};
    border-top: 24px solid transparent;
    border-bottom: 24px solid transparent;
    z-index: 2;
  }

  :first-child {
    :after {
      display: none;
    }
  }

  :last-child {
    border-right: none;
    :before {
      display: none;
    }
  }
`;

const WrapInfo = styled.div``;

const Name = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;

  p {
    margin-bottom: 0;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
    margin: 0 8px;
  }

  span {
    font-size: 16px;
    line-height: 24px;
    color: #6b6b6b;
  }
`;

const Type = styled(Name)`
  width: 100%;
  justify-content: space-between;
`;

const Time = styled(Name)``;

const WrapTime = styled.div`
  display: flex;
`;

const Schedule = styled(Name)`
  align-items: unset;
  margin-bottom: 0;

  img {
    height: fit-content;
  }
`;

const WrapSchedule = styled.div`
  display: flex;
  flex-direction: column;
`;
