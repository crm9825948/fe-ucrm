import { Button, Col, Dropdown, Form, Menu, Row, Select, Spin } from "antd";
import ImgConfirm from "assets/icons/common/confirm.png";
import IconDown from "assets/icons/common/icon-down.svg";
import axios from "axios";
import EmailComp from "components/Email/Email2";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import { BASE_URL_API, BE_URL } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
import moment from "moment";
import Date from "pages/Objects/modal/fieldsType/date";
import Datetime from "pages/Objects/modal/fieldsType/dateTime";
import Email from "pages/Objects/modal/fieldsType/email";
import File from "../file";
import FormulaField from "pages/Objects/modal/fieldsType/formulaField";
import IDComp from "pages/Objects/modal/fieldsType/ID";
import LinkingObject from "pages/Objects/modal/fieldsType/linkingObject";
import Lookup from "pages/Objects/modal/fieldsType/lookup";
import Number from "pages/Objects/modal/fieldsType/number";
import SelectType from "pages/Objects/modal/fieldsType/select";
import Text from "pages/Objects/modal/fieldsType/text";
import Textarea from "pages/Objects/modal/fieldsType/textarea";
import User from "pages/Objects/modal/fieldsType/user";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { runDynamicButton } from "redux/slices/dynamicButton";
import { loadEmails } from "redux/slices/email";
import { loadListObjectFieldSuccess, updateRecord } from "redux/slices/objects";
import styled from "styled-components";
import DynamicField from "../dynamicField";
import ModalRunDynamicButton from "components/Modal/ModalRunDynamicButton";
import _ from "lodash";
const { Option } = Select;

const WithDetails = ({
  objectId,
  form,
  recordID,
  setRecordID,
  currentPage,
  recordPerPage,
  loadingDetails,
  selectedRowKeys,
  onCancel,
  leftWidth,
  recordData,
  hiddenArray,
  setHiddenArray,
  fields,
  searchList,
  loadData,
  listObjectField,
}) => {
  const dispatch = useDispatch();
  const [dataConfirm, setDataConfirm] = useState({});
  const [userDynamicButton, $userDynamicButton] = useState([]);
  const [mode, setMode] = useState(false);
  const { listEmail } = useSelector((state) => state.componentEmail);
  const {
    // fields,
    isLoading,
    userAssignTo,
    // searchList,
    // recordData,
    // hiddenArray,
    hiddenDynamic,
    // listObjectField,
  } = useSelector((state) => state.objectsReducer);

  const [formValue, setFormValue] = useState({});
  const [, setValueModal] = useState({});
  const [dataFilterLinking, setDataFilterLinking] = useState({});
  const [showConfirm, setShowConfirm] = useState(false);
  const [isDisabled, setIsDisabled] = useState(true);
  const [visibleModalRDB, $visibleModalRDB] = useState(false);

  useEffect(() => {
    dispatch(loadEmails(recordID));
  }, [recordID, dispatch]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && selectedRowKeys) {
      form.setFieldsValue({
        assignTo: recordData.owner,
      });
      setFormValue(form.getFieldsValue());
      setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (recordData[field.ID]?.value) {
              /*eslint-disable-next-line*/

              field.list_items[recordData[field.ID].value].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      setHiddenArray(tmpHidden);
    }
    setIsDisabled(true);

    /* eslint-disable-next-line */
  }, [recordData, selectedRowKeys, fields]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (recordID && objectId) {
        axios
          .post(
            BASE_URL_API + "dynamic-button/load-user-button",
            {
              object_id: objectId,
              record_id: recordID,
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((response) => {
            $userDynamicButton(response.data.data);
          })
          .catch((err) => {
            Notification("error", err.response.data.error);
          });
      }
    };
    checkToken();
  }, [objectId, recordID]);

  const menuActions = (
    <Menu>
      {userDynamicButton.map((button, idx) => {
        return (
          <Menu.Item key={idx} onClick={() => _onRunButton(button)}>
            <span>{button.name}</span>
          </Menu.Item>
        );
      })}
    </Menu>
  );

  const _onRunButton = (button) => {
    if (_.get(button, "dynamic_field", []).length > 0) {
      $visibleModalRDB(true);
      setDataConfirm({
        button: button,
        data: {
          data: {
            object_id: objectId,
            record_id: recordID,
            button_id: button._id,
          },
          reloadable: button?.reloadable || false,
        },
      });
    } else {
      setDataConfirm({
        data: {
          object_id: objectId,
          record_id: recordID,
          button_id: button._id,
        },
        reloadable: button?.reloadable || false,
      });
      setShowConfirm(true);
    }
  };

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "id":
        return <IDComp field={field} />;
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            listObjectField={listObjectField}
            setIsDisabled={setIsDisabled}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} />;
      case "date":
        return <Date field={field} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={open}
            recordID={recordID}
            setRecordID={setRecordID}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            open={true}
            recordData={recordData}
          />
        );
      case "dynamic-field":
        return (
          <DynamicField
            field={field}
            form={form}
            recordID={recordID}
            hiddenArray={hiddenArray}
            setHiddenArray={setHiddenArray}
          />
        );
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={recordData}
            setIsDisabled={setIsDisabled}
          />
        );
      case "formula":
        return <FormulaField field={field} form={form} />;

      case "lookup":
        return (
          <Lookup
            setIsDisabled={setIsDisabled}
            field={field}
            form={form}
            recordData={recordData}
          />
        );
      default:
        break;
    }
  };

  useEffect(() => {
    const loadFields = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/objects-fields-permission",
          {
            api_version: "2",
            object_id: objectId,
            show_meta_fields: true,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          dispatch(loadListObjectFieldSuccess(res.data.data));
        })
        .catch((err) => {
          console.log(err);
        });
    };
    loadFields();
  }, [objectId, dispatch]);

  const onFinish = (values) => {
    values = { ...formValue, ...values };
    let fieldsObject = {};
    /* eslint-disable-next-line */

    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        // section.fields.map((section, idx) => {
        /* eslint-disable-next-line */
        section.fields.map((item, index) => {
          fieldsObject[item.ID] = { ...item };
        });
        // });
      });

    let listValue = [];
    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              if (item.url === undefined) {
                item.url =
                  BASE_URL_API + item &&
                  item.response &&
                  item.response.data &&
                  item.response.data[0];
                item.url = BE_URL + item.url;
              }
              result.push(item.url);
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(item);
                return null;
              });
            } else {
              // eslint-disable-next-line
              value.map((item, idx) => {
                if (item.url) {
                  result.push(item.url);
                } else {
                  result.push(item);
                }
              });
            }
          }

          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    if (Object.entries(recordData).length > 0 && recordID) {
      dispatch(
        updateRecord({
          data: {
            data: listValue,
            owner_id: values["assignTo"],
            id: recordData._id,
            object_id: objectId,
            share_to: values.share_to,
            action_type: values.action_type,
            subject: values.subject,
            permission: values.permission,
          },
          load: {
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: searchData,
            },
          },
          type: "no-reload",
        })
      );
      loadData();
      //   setRecordID("");
      //   dispatch(loadRecordDataSuccess({}));
      //   setOpen(false);
      // onCancel();
      // initData(currentPage, recordPerPage, searchData, sortBy);
    }
    // form.resetFields();
    setIsDisabled(true);
  };

  return (
    <Wrapper>
      {recordID && selectedRowKeys ? (
        loadingDetails ? (
          <div className="spin-loading">
            <Spin />
          </div>
        ) : mode ? (
          <div>
            <Button className="back" onClick={() => setMode(false)}>
              <svg
                width="16"
                height="16"
                viewBox="0 0 16 16"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M3.828 7.00066H16V9.00066H3.828L9.192 14.3647L7.778 15.7787L0 8.00066L7.778 0.222656L9.192 1.63666L3.828 7.00066Z"
                  fill="black"
                />
              </svg>
              Back
            </Button>
            <EmailComp record_id={recordID} object_id={objectId} />
          </div>
        ) : (
          <Form
            name="basic"
            form={form}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            labelAlign="left"
            layout="vertical"
            onValuesChange={(value, values) => {
              setFormValue(values);
              setIsDisabled(false);
            }}
          >
            <AssignToWrapper>
              <div style={{ width: "100px", fontFamily: "var(--roboto-500)" }}>
                Assign to<span style={{ color: "red" }}>*</span>
              </div>
              <Form.Item
                // label="Assign to"
                name="assignTo"
                rules={[
                  { required: true, message: "Please select assign to!" },
                ]}
              >
                <Select
                  showSearch
                  optionFilterProp="children"
                  style={{ width: "100%" }}
                  filterOption={(inputValue, option) => {
                    return option.children
                      .join("")
                      .toLowerCase()
                      .includes(inputValue.toLowerCase());
                  }}
                >
                  {userAssignTo &&
                    userAssignTo.map((user, idx) => {
                      return (
                        <Option
                          value={user._id}
                          key={user._id}
                          disabled={user.disable}
                        >
                          {user.Last_Name} {user.Middle_Name} {user.First_Name}
                        </Option>
                      );
                    })}
                </Select>
              </Form.Item>
              <Button
                className="email"
                onClick={() => {
                  setMode(true);
                }}
              >
                Email <span className="mail">{listEmail.length}</span>
              </Button>
            </AssignToWrapper>

            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        <Row>
                          {/* eslint-disable-next-line */}
                          {section.fields.map((field, index) => {
                            if (
                              field.hidden === false &&
                              field.permission_hidden === false &&
                              hiddenArray.findIndex(
                                (ele) => ele === field._id
                              ) < 0
                            ) {
                              if (
                                (Object.entries(recordData).length === 0 ||
                                  recordID === "") &&
                                field.type === "id"
                              ) {
                              } else
                                return (
                                  <Col
                                    span={field.type === "textarea" ? 24 : 8}
                                    key={field._id}
                                  >
                                    {handleFieldType(field, true)}
                                  </Col>
                                );
                            }
                          })}
                        </Row>
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {recordID && userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction>
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonCancel
                onClick={() => {
                  onCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
              <CustomButtonSave
                disabled={isDisabled}
                htmlType="submit"
                loading={isLoading}
              >
                Save
              </CustomButtonSave>
            </CustomFooter>
          </Form>
        )
      ) : (
        <div className="spin-loading">
          No data! Please select record to handle!
        </div>
      )}
      <ModalConfirm
        title="Confirm"
        decs="Are you sure you want to proceed?"
        method={runDynamicButton}
        data={dataConfirm}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={setShowConfirm}
      />
      <ModalRunDynamicButton
        data={dataConfirm}
        method={runDynamicButton}
        visibleModalRDB={visibleModalRDB}
        $visibleModalRDB={$visibleModalRDB}
        dataRecord={recordData}
      />
    </Wrapper>
  );
};

export default WithDetails;

WithDetails.defaultProps = {
  recordData: {},
  loadData: () => {},
  searchList: {},
};

const Wrapper = styled.div`
  background-color: #fff;
  padding-bottom: 30px;
  .custom-label {
    font-size: 16px;
    display: inherit;
    margin-bottom: 6px;
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }
  .ant-form-item-label {
    padding-bottom: 0;
  }
  .back {
    display: flex;
    align-items: center;
    font-family: var(--roboto-700);
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    /* identical to box height */

    display: flex;
    align-items: center;

    /* text - main */

    color: #252424;
    border: none;
    svg {
      margin-right: 16px;
    }
  }
  .email {
    background: ${(props) => props.theme.main};
    border-radius: 34px;
    font-family: var(--roboto-700);
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 26px;
    /* identical to box height, or 162% */

    display: flex;
    align-items: center;

    /* bg - block/fff */
    margin-left: 10px;
    color: #ffffff;
    .mail {
      padding: 0 8px;
      margin-left: 5px;
      background-color: white;
      color: ${(props) => props.theme.main};
      border-radius: 10px;
      font-family: var(--roboto-700);
      font-style: normal;
      font-weight: 700;
      font-size: 12px;
      line-height: 20px;
    }
  }
  .spin-loading {
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 100px;
  }
  .ant-col {
    padding-right: 10px !important;
    padding-bottom: 0;
    .ant-form-item {
      margin-bottom: 12px !important;
    }
    label {
      font-family: var(--roboto-500);
      font-style: normal;
      font-weight: 500;
      line-height: 22px;
      color: #2c2c2c;
      display: inline-block;
      white-space: normal;
    }
  }
  .ant-form-item-label > label::after {
    visibility: hidden;
  }
`;

const WrapperSection = styled.div`
  border-bottom: 1px solid #ececec;
  padding: 12px 0px 12px 12px;
`;

const CustomTitleSection = styled.div`
  font-size: 16px;
  font-family: var(--roboto-500);
  color: ${(props) => props.theme.main};
  margin-bottom: 8px;
`;
const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background: #eff6f6;
  padding: 12px;
  position: fixed;
  bottom: 0;
  right: 0;
  width: calc(100% - 3px - 30%);
`;
const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  width: 80px;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonAction = styled(Button)`
  width: 102px;
  margin-right: 16px;
  img {
    margin-right: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    img {
      filter: brightness(200);
    }
  }
`;

const AssignToWrapper = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  padding: 10px;

  .ant-form-item {
    width: 100%;
    margin-bottom: 0px;
  }
  .ant-form-item-label {
    padding: 0;
  }
  label {
    display: none;
  }
`;
