import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import deleteObject from "assets/images/objectsManagement/deleteObject.png";
import React from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";

const ModalConfimDelete = ({
  title,
  decs,
  methodDelete,
  dataDelete,
  setShowModalDelete,
  isLoading,
  mask,
  openConfirm,
  setOpenConfirm,
  loadData,
}) => {
  const dispatch = useDispatch();
  // const { showModalConfirmDelete } = useSelector(
  //   (state) => state.globalReducer
  // );
  const _onSubmit = () => {
    setOpenConfirm(false);
    dispatch(
      methodDelete({
        ...dataDelete,
      })
    );
    // onClick();
    loadData();
  };
  const onClick = () => {
    setOpenConfirm(false);
    if (openConfirm) {
      setShowModalDelete(true);
    }
  };
  return (
    <CustomModal
      title="Confirm action"
      visible={openConfirm}
      mask={mask}
      onCancel={() => {
        onClick();
      }}
      width={400}
      footer={null}
    >
      <CustomContent>
        <img alt="" src={deleteObject} />
        <Title>Bạn chắc chắn muốn xóa {title}?</Title>
        <Decs>{decs}</Decs>
      </CustomContent>
      <CustomFooter>
        <CustomButtonSave
          size="large"
          htmlType="submit"
          loading={isLoading}
          onClick={() => {
            _onSubmit();
          }}
        >
          Delete
        </CustomButtonSave>
        <CustomButtonCancel
          size="large"
          onClick={() => {
            onClick();
          }}
        >
          Cancel
        </CustomButtonCancel>
      </CustomFooter>
    </CustomModal>
  );
};

export default ModalConfimDelete;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  img {
    width: 70px;
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #2c2c2c;
  text-align: center;
`;

const Decs = styled.span`
  font-size: 14px;
  line-height: 22px;
  text-align: center;
  color: #595959;
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
