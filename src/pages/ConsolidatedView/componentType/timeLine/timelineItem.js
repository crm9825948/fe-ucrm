import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import CallIn from "assets/icons/timeline/call_in.svg";
import CallOut from "assets/icons/timeline/call_out.svg";
import MailIn from "assets/icons/timeline/mail_in.svg";
import MailOut from "assets/icons/timeline/mail_out.svg";
import SMSIn from "assets/icons/timeline/SMS_in.svg";
import SMSOut from "assets/icons/timeline/SMS_out.svg";
import VBEEIn from "assets/icons/timeline/VBEE_in.svg";
import VBEEOut from "assets/icons/timeline/VBEE_out.svg";

import Viber from "assets/icons/timeline/viber.svg";
import Webchat from "assets/icons/timeline/webchat.svg";
import Zalo from "assets/icons/timeline/zalo.svg";
import EmailIC from "assets/icons/timeline/IC_Email.svg";
import Facebook from "assets/icons/timeline/facebook.svg";
import FacebookMessage from "assets/icons/timeline/facebook_message.svg";
import FacebookComment from "assets/icons/timeline/facebook_comment.svg";
import Instagram from "assets/icons/timeline/instagram.svg";
import InstagramMessage from "assets/icons/timeline/instagram_message.svg";
import InstagramComment from "assets/icons/timeline/instagram_comment.svg";
import Telegram from "assets/icons/timeline/telegram.svg";
import ThirdPartyChannel from "assets/icons/timeline/third_party_channel.svg";
import Whatsapp from "assets/icons/timeline/whatsapp.svg";

import ModalTimeline from "./modalTimeline";

import {
  resetStatus,
  loadRecordInteraction,
} from "redux/slices/consolidatedViewSettings";

function TimelineItem({ item, vertical, objectId }) {
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);

  const { statusUpdateTimeline, recordInteraction } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const _onShowModal = () => {
    setShowModal(true);
    dispatch(
      loadRecordInteraction({
        interaction_id: item._id,
        object_id: objectId,
      })
    );
  };

  useEffect(() => {
    if (statusUpdateTimeline !== null) {
      setShowModal(false);
      dispatch(resetStatus());
    }
  }, [statusUpdateTimeline, dispatch]);

  return (
    <Wrapper vertical={vertical}>
      {item.incoming_type === "0" || item.outgoing_type === "0" ? (
        <WrapItem vertical={vertical}>
          <WrapIcon vertical={vertical}>
            <img
              onDoubleClick={_onShowModal}
              src={item.incoming_type ? CallIn : CallOut}
              alt="call"
            />
            <hr />
          </WrapIcon>
          <WrapText>
            <Title>
              {item.incoming_type ? "Incoming call" : "Outgoing call"}
            </Title>
            {item.status_call && <span>{item.status_call}</span>}
            <CreateDate>
              {item.created_date} • {item.created_by_username}
            </CreateDate>
          </WrapText>
        </WrapItem>
      ) : item.incoming_type === "1" || item.outgoing_type === "1" ? (
        <WrapItem vertical={vertical}>
          <WrapIcon vertical={vertical}>
            <img
              onDoubleClick={_onShowModal}
              src={item.incoming_type ? MailIn : MailOut}
              alt="mail"
            />
            <hr />
          </WrapIcon>
          <WrapText>
            <Title>{item.subject}</Title>
            <CreateDate>
              {item.created_date} • {item.created_by_username}
            </CreateDate>
          </WrapText>
        </WrapItem>
      ) : item.incoming_type === "2" || item.outgoing_type === "2" ? (
        <WrapItem vertical={vertical}>
          <WrapIcon vertical={vertical}>
            <img
              onDoubleClick={_onShowModal}
              src={item.incoming_type ? SMSIn : SMSOut}
              alt="sms"
            />
            <hr />
          </WrapIcon>
          <WrapText>
            <Title>
              {item.incoming_type ? "Incoming SMS" : "Outgoing SMS"}
            </Title>
            <CreateDate>
              {item.created_date} • {item.created_by_username}
            </CreateDate>
          </WrapText>
        </WrapItem>
      ) : item.incoming_type === "3" || item.outgoing_type === "3" ? (
        <WrapItem vertical={vertical}>
          <WrapIcon vertical={vertical}>
            <img
              onDoubleClick={_onShowModal}
              src={item.incoming_type ? VBEEIn : VBEEOut}
              alt="vbee"
            />
            <hr />
          </WrapIcon>
          <WrapText>
            <Title>
              {item.incoming_type ? "Incoming Voice bot" : "Outgoing Voice bot"}
            </Title>
            <CreateDate>
              {item.created_date} • {item.created_by_username}
            </CreateDate>
          </WrapText>
        </WrapItem>
      ) : item.incoming_type === "4" ? (
        <WrapItem vertical={vertical}>
          <WrapIcon vertical={vertical}>
            <img
              onDoubleClick={_onShowModal}
              src={
                item.channel_type === "facebook"
                  ? Facebook
                  : item.channel_type === "facebook_comment"
                  ? FacebookComment
                  : item.channel_type === "facebook_message"
                  ? FacebookMessage
                  : item.channel_type === "instagram"
                  ? Instagram
                  : item.channel_type === "instagram_message"
                  ? InstagramMessage
                  : item.channel_type === "instagram_comment"
                  ? InstagramComment
                  : item.channel_type === "telegram"
                  ? Telegram
                  : item.channel_type === "third_party_channel"
                  ? ThirdPartyChannel
                  : item.channel_type === "whatsapp"
                  ? Whatsapp
                  : item.channel_type === "webchat"
                  ? Webchat
                  : item.channel_type === "viber"
                  ? Viber
                  : item.channel_type === "zalo"
                  ? Zalo
                  : item.channel_type === "email"
                  ? EmailIC
                  : ""
              }
              alt="IC"
            />
            <hr />
          </WrapIcon>
          <WrapText>
            <Title>Interaction IC</Title>
            <CreateDate>
              {item.created_date} • {item.created_by_username}
            </CreateDate>
          </WrapText>
        </WrapItem>
      ) : (
        ""
      )}

      <ModalTimeline
        item={item}
        showModal={showModal}
        setShowModal={setShowModal}
        objectId={objectId}
        recordInteraction={recordInteraction}
      />
    </Wrapper>
  );
}

export default TimelineItem;

const Wrapper = styled.div`
  width: ${({ vertical }) => (vertical ? "350px" : "100%")};

  img {
    width: fit-content !important;
    margin: ${({ vertical }) => (vertical ? "8px" : "0")};
  }
`;

const Title = styled.div`
  /* margin-top: 8px; */
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  color: #252424;
  font-size: 17px;
  font-family: var(--roboto-700);
  margin-right: 32px;
`;

const CreateDate = styled.div`
  color: #979797;
  font-size: 16px;
  margin-right: 32px;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
`;

const WrapItem = styled.div`
  display: ${(props) => props.vertical && "flex"};
`;

const WrapText = styled.div``;

const WrapIcon = styled.div`
  display: flex;
  align-items: center;

  hr {
    height: 1.5px;
    width: 100%;
    border-top: 1px dashed #648bd8;
    display: ${(props) => props.vertical && "none"};
  }

  img {
    width: 24px;
    height: 24px;
  }
`;
