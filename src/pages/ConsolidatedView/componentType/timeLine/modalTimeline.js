import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { BASE_URL_API, BASENAME } from "constants/constants";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import SearchOutlined from "@ant-design/icons/SearchOutlined";
import Table from "antd/lib/table";

import Call from "./detailsInteraction/call";
import Email from "./detailsInteraction/email";
import SMS from "./detailsInteraction/sms";
import VBEE from "./detailsInteraction/vbee";
import IC from "./detailsInteraction/ic";
import Details from "assets/icons/users/details.svg";

import { updateInteractionDetails } from "redux/slices/consolidatedViewSettings";
import {
  loadDataLinking,
  loadHeaderLinking,
  loadPaginationLinking,
  loadListObjectField,
} from "redux/slices/objects";

import LinkingList from "pages/Objects/modal/fieldsType/linking/linkingList";
import ModalRecord from "pages/ConsolidatedView/modalRecord";
import { Notification } from "components/Notification/Noti";
import { checkTokenExpiration } from "contexts/TokenCheck";

function ModalTimeline({
  item,
  showModal,
  setShowModal,
  objectId,
  recordInteraction,
}) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [formRecord] = Form.useForm();
  const { Column } = Table;

  const { TextArea } = Input;

  const { recordData, hiddenArray, linkingFieldValue } = useSelector(
    (state) => state.objectsReducer
  );

  const { objectsTarget } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [listObject, setListObject] = useState([]);
  const [showAddNote, setShowAddNote] = useState(false);
  const [showAddNoteCall, setShowAddNoteCall] = useState(false);
  const [showType, setShowType] = useState(false);
  const [visible, setVisible] = useState(false);
  const [objectSelect, setObjectSelect] = useState("");
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [listRecord, setListRecord] = useState([]);

  const [typeCreate, setTypeCreate] = useState("");

  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [fields, setFields] = useState([]);
  const [sortBy] = useState({});
  const [rowData, setRowData] = useState({});

  const type = [
    {
      label: "Create",
      value: "create",
    },
    {
      label: "Linking",
      value: "linking",
    },
  ];

  const _onSubmit = (value) => {
    if (selectedRowKeys.length === 0 && typeCreate === "linking") {
      Notification("error", "Please select target record!");
    } else {
      let data = {
        interaction_id: item._id,
        object_id: objectId,
        source_object_id: value.objectTarget ? value.objectTarget : null,
        source_record_id:
          selectedRowKeys.length > 0 ? selectedRowKeys[0] : null,
        note: value.newNote ? value.newNote : null,
        data: { ...item },
      };

      if (item.incoming_type === "0" || item.outgoing_type === "0") {
        data = {
          ...data,
          subject: value.subjectCall ? value.subjectCall : null,
          status_call: value.status_call,
        };
      }

      dispatch(
        updateInteractionDetails({
          ...data,
        })
      );
      setShowAddNote(false);
      setShowAddNoteCall(false);
      setTypeCreate("");
      setShowType(false);
      form.setFieldsValue({
        objectTarget: undefined,
        type: undefined,
      });
    }
  };

  const handleSelectObject = (value) => {
    setShowType(true);
    setObjectSelect(value);
  };

  const handleSelectLinking = (selectedRowKeys) => {
    // Axios.post(
    //   BASE_URL_API + "object/load-linking-field-value",
    //   {
    //     object_related: objectSelect,
    //     record_id: selectedRowKeys[0],
    //     object_create_record: objectId,
    //   },
    //   {
    //     headers: {
    //       Authorization: localStorage.getItem("setting_accessToken"),
    //     },
    //   }
    // )
    //   .then((res) => {
    //     setLinkingFieldValue(res.data.data);
    //   })
    //   .catch(() => {});

    setVisible(false);
  };

  useEffect(() => {
    let tempList = [];
    objectsTarget.map((item) => {
      return tempList.push({
        label: item.Name,
        value: item._id,
      });
    });
    setListObject(tempList);
  }, [objectsTarget]);

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadHeaderLinking({
          object_id: objectSelect,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: objectSelect,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: objectSelect,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
    }
  }, [dispatch, objectSelect, visible]);

  useEffect(() => {
    if (!showAddNote) {
      form.setFieldsValue({
        newNote: undefined,
      });
    }
  }, [showAddNote, form]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (!objectSelect) return;
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: objectSelect,
          show_meta_fields: true,
        })
      );

      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: objectSelect,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let tempField = [];
          let tempFieldHidden = [];
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sections);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              if (hiddenArray.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFields(tempFieldHidden);
        })
        .catch((err) => {});
    };
    checkToken();
    // eslint-disable-next-line
  }, [dispatch, objectSelect]);

  useEffect(() => {
    formRecord.setFieldsValue(linkingFieldValue);
    // eslint-disable-next-line
  }, [linkingFieldValue]);

  useEffect(() => {
    let tempList = [];
    recordInteraction.map((ele) => {
      return tempList.push({
        key: ele._id,
        record_id: ele._id,
        id: ele.value,
        object_id: ele.object_id,
      });
    });
    setListRecord(tempList);
  }, [recordInteraction]);

  return (
    <ModalCustom
      title="Interaction"
      visible={showModal}
      footer={null}
      width={
        item.incoming_type === "0" || item.outgoing_type === "0"
          ? 1000
          : item?.incoming_type === "4" && item?.channel_type === "email"
          ? 960
          : 600
      }
      onCancel={() => {
        setShowModal(false);
        setShowAddNote(false);
        setShowAddNoteCall(false);
        setShowType(false);
        formRecord.resetFields();
        setTypeCreate("");
        form.setFieldsValue({
          objectTarget: undefined,
          type: undefined,
        });
      }}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        layout={
          item.incoming_type === "0" || item.outgoing_type === "0"
            ? "vertical"
            : "horizontal"
        }
        labelCol={{
          span:
            item?.incoming_type === "4" && item?.channel_type === "email"
              ? 4
              : item.incoming_type === "0" || item.outgoing_type === "0"
              ? 24
              : 7,
        }}
        wrapperCol={{
          span:
            item?.incoming_type === "4" && item?.channel_type === "email"
              ? 20
              : item.incoming_type === "0" || item.outgoing_type === "0"
              ? 24
              : 17,
        }}
        colon={false}
        labelAlign="left"
      >
        {item.incoming_type === "0" || item.outgoing_type === "0" ? (
          <Call
            item={item}
            form={form}
            listObject={listObject}
            objectId={objectId}
            selectedRowKeys={selectedRowKeys}
            setSelectedRowKeys={setSelectedRowKeys}
            showAddNoteCall={showAddNoteCall}
            setShowAddNoteCall={setShowAddNoteCall}
            recordInteraction={recordInteraction}
            formRecord={formRecord}
          />
        ) : item.incoming_type === "1" || item.outgoing_type === "1" ? (
          <Email item={item} form={form} />
        ) : item.incoming_type === "2" || item.outgoing_type === "2" ? (
          <SMS item={item} form={form} />
        ) : item.incoming_type === "3" || item.outgoing_type === "3" ? (
          <VBEE item={item} form={form} />
        ) : item.incoming_type === "4" ? (
          <IC item={item} form={form} showModal={showModal} />
        ) : (
          ""
        )}

        {item.incoming_type !== "0" && item.outgoing_type !== "0" && (
          <>
            <Form.Item label="Note" name="note">
              <TextArea rows={5} disabled />
            </Form.Item>

            {showAddNote ? (
              <Form.Item label="New note" name="newNote">
                <TextArea rows={5} />
              </Form.Item>
            ) : (
              <AddNote id="btnsavelogin" onClick={() => setShowAddNote(true)}>
                + Add Note
              </AddNote>
            )}
            <Form.Item label="Object Target" name="objectTarget">
              <Select
                placeholder="Select target object"
                options={listObject}
                onChange={handleSelectObject}
              />
            </Form.Item>

            {showType && (
              <Form.Item
                label="Type"
                name="type"
                rules={[{ required: true, message: "Please select" }]}
              >
                <Select
                  placeholder="Please select type"
                  options={type}
                  onSelect={(e) => {
                    setTypeCreate(e);

                    if (e === "create") {
                      setOpenModalRecord(true);
                    }
                  }}
                />
              </Form.Item>
            )}

            {typeCreate === "create" && (
              <ModalRecord
                open={openModalRecord}
                setOpen={setOpenModalRecord}
                initData={() => {}}
                recordPerPage={10}
                currentPage={0}
                form={formRecord}
                recordID={""}
                setRecordID={() => {}}
                objectId={objectSelect}
                fields={fields}
                sortBy={sortBy}
                interaction={item}
                objectIDInteraction={objectId}
              />
            )}

            {typeCreate === "linking" && (
              <Form.Item label="Select target record">
                {selectedRowKeys.length > 0 ? (
                  <Input
                    disabled
                    placeholder="Selected"
                    addonAfter={
                      <>
                        <SearchOutlined
                          onClick={() => {
                            setVisible(true);
                          }}
                        />
                      </>
                    }
                  />
                ) : (
                  <Input
                    placeholder={`Please select`}
                    disabled
                    addonAfter={
                      <>
                        <SearchOutlined
                          onClick={() => {
                            setVisible(true);
                          }}
                        />
                      </>
                    }
                  />
                )}
              </Form.Item>
            )}

            {visible && (
              <LinkingList
                setVisible={setVisible}
                visible={visible}
                field={{ objectname: objectSelect }}
                selectedRowKeys={selectedRowKeys}
                setSelectedRowKeys={setSelectedRowKeys}
                handleSelect={handleSelectLinking}
                dataFilterLinking={{}}
                onCancelModal={() => setVisible(false)}
                recordData={recordData}
                rowData={rowData}
                setRowData={setRowData}
              />
            )}

            {recordInteraction.length > 0 && (
              <Table pagination={false} dataSource={listRecord}>
                <Column title="ID" dataIndex="id" key="id" />
                <Column
                  title="View details"
                  dataIndex="action"
                  key="action"
                  render={(text, record) => (
                    <WrapAction>
                      <img
                        onClick={() => {
                          window.open(
                            BASENAME +
                              `consolidated-view/${record.object_id}/${record.record_id}`
                          );
                        }}
                        src={Details}
                        alt="Consolidate view"
                      />
                    </WrapAction>
                  )}
                />
              </Table>
            )}
          </>
        )}

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            Save
          </Button>
          <Button
            onClick={() => {
              setShowModal(false);
              setShowAddNote(false);
              setShowAddNoteCall(false);
              setShowType(false);
              formRecord.resetFields();
              setTypeCreate("");
              form.setFieldsValue({
                objectTarget: undefined,
                type: undefined,
              });
            }}
          >
            Cancel
          </Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalTimeline;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    font-size: 16px;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .ant-table-wrapper {
    height: 307px;
    overflow: auto;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const AddNote = styled(Button)`
  margin-bottom: 16px;
  font-size: 16px;
  height: unset;
  width: 8.125rem;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }

  :focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
