import { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import styled from "styled-components/macro";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Spin from "antd/lib/spin";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import {
  resetStatus,
  loadObjectsTarget,
} from "redux/slices/consolidatedViewSettings";

import TimelineItem from "./timelineItem";

function TimelineVertical({ recordID, objectId, type }) {
  const dispatch = useDispatch();
  //   const { isLoadingTimelineVertical, timelinesVertical, hasMore } = useSelector(
  //     (state) => state.consolidatedViewSettingsReducer
  //   );
  // const { timelinesVertical, hasMore } = useSelector(
  //   (state) => state.consolidatedViewSettingsReducer
  // );

  const { statusUpdateTimeline } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [currentPage, setCurrentPage] = useState(1);
  const [timelinesVertical, setTimelinesVertical] = useState([]);
  const [hasMore, setHasMore] = useState(true);
  const [tempRecordID, setTempRecordID] = useState("");

  //   const [isUpdate, setIsUpdate] = useState(false);
  // const [isLoadingTimelineVertical, setIsLoadingTimelineVertical] =
  //   useState(false);
  // const [totalTimelinesVertical, setTotalTimelinesVertical] = useState(0);

  const loadMore = () => {
    let tempPage = currentPage + 1;
    setCurrentPage(tempPage);
  };

  const loadTimeline = useCallback(async () => {
    // setIsLoadingTimelineVertical(true);
    const isTokenValid = await checkTokenExpiration();
    setTempRecordID(recordID);
    axios
      .post(
        type === "vertical-timeline-new"
          ? BASE_URL_API + "interactions/view-all-interactions"
          : BASE_URL_API + "interactions/view-all",
        {
          object_id: objectId,
          record_id: recordID,
          current_page: currentPage,
          record_per_page: 20,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((response) => {
        // setIsLoadingTimelineVertical(false);

        if (timelinesVertical.length === 0 || tempRecordID !== recordID) {
          setTimelinesVertical(response.data.data.data);
          if (
            response.data.data.data.length === response.data.data.total_record
          ) {
            setHasMore(false);
          }
        } else {
          let temp = [...timelinesVertical];
          response.data.data.data.map((item) => {
            return temp.push(item);
          });

          setTimelinesVertical(temp);
          if (temp.length === response.data.data.total_record) {
            setHasMore(false);
          }
        }
        // setTotalTimelinesVertical(response.data.data.total_record);
      })
      .catch((err) => {
        // setIsLoadingTimelineVertical(false);
      });
    // dispatch(
    //   getInteractionVertical({
    //     object_id: objectId,
    //     record_id: recordID,
    //     current_page: currentPage,
    //     record_per_page: 20,
    //   })
    // );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, objectId, recordID]);

  useEffect(() => {
    loadTimeline();
  }, [loadTimeline]);

  useEffect(() => {
    if (statusUpdateTimeline !== null) {
      loadTimeline();
      dispatch(resetStatus());
    }
  }, [statusUpdateTimeline, dispatch, loadTimeline]);

  useEffect(() => {
    dispatch(
      loadObjectsTarget({
        object_id: objectId,
      })
    );
  }, [dispatch, objectId]);

  return (
    <Wrapper>
      <Title>Vertical timeline</Title>
      <Content id="scrollTarget">
        {/* <SubTitle>Historical Activities</SubTitle> */}

        {timelinesVertical.length > 0 ? (
          <InfiniteScroll
            dataLength={timelinesVertical.length}
            scrollableTarget="scrollTarget"
            loader={<Spin />}
            hasMore={hasMore}
            next={loadMore}
          >
            {timelinesVertical.map((item, idx) => {
              return (
                <TimelineItem
                  item={item}
                  vertical={true}
                  key={idx}
                  objectId={objectId}
                />
              );
            })}
          </InfiniteScroll>
        ) : (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <p>
              There's no any <span>Historical Activities</span>
            </p>
          </Empty>
        )}
      </Content>
    </Wrapper>
  );
}

export default TimelineVertical;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;
`;

const Title = styled.div`
  padding: 16px;
  background: #fff;
  height: 36px;
  color: #2c2c2c;
  /* font-family: var(--roboto-500); */
  font-size: 17px;
  font-style: normal;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  border-bottom: 1px solid #d9d9d9;
`;

const Content = styled.div`
  padding: 8px;
  width: 100%;
  height: 80%;
  overflow: auto;
`;

// const SubTitle = styled.div`
//   font-size: 16px;
//   font-family: var(--roboto-700);
//   color: #252424;
//   margin-bottom: 24px;
// `;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
