import { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import Carousel from "react-multi-carousel";
import { BASE_URL_API } from "constants/constants";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Spin from "antd/lib/spin";

import {
  // getInteraction,
  loadObjectsTarget,
  resetStatus,
} from "redux/slices/consolidatedViewSettings";
import ArrowLeft from "assets/icons/timeline/arrow-left.svg";
import ArrowRight from "assets/icons/timeline/arrow-right.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import TimelineItem from "./timelineItem";

function TimelineHorizontal({ recordID, objectId, type }) {
  const dispatch = useDispatch();
  // const { isLoadingTimeline, timelines, totalTimelines } = useSelector(
  //   (state) => state.consolidatedViewSettingsReducer
  // );

  const { statusUpdateTimeline } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [timelines, setTimelines] = useState([]);
  const [isLoadingTimeline, setIsLoadingTimeline] = useState(false);
  const [totalTimelines, setTotalTimelines] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [isUpdate, setIsUpdate] = useState(false);
  const [tempRecordID, setTempRecordID] = useState("");

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 5,
      paritialVisibilityGutter: 60,
      slidesToSlide: 5,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 5,
      paritialVisibilityGutter: 50,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      paritialVisibilityGutter: 30,
    },
  };

  const loadTimeline = useCallback(async () => {
    setIsLoadingTimeline(true);
    setTempRecordID(recordID);
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        type === "horizontal-timeline-new"
          ? BASE_URL_API + "interactions/view-all-interactions"
          : BASE_URL_API + "interactions/view-all",
        {
          object_id: objectId,
          record_id: recordID,
          current_page: currentPage,
          record_per_page: 20,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((response) => {
        setIsLoadingTimeline(false);

        if (timelines.length === 0 || tempRecordID !== recordID) {
          setTimelines(response.data.data.data);
        } else {
          let temp = [...timelines];
          response.data.data.data.map((item) => {
            return temp.push(item);
          });

          setTimelines(temp);
        }
        setTotalTimelines(response.data.data.total_record);
      })
      .catch((err) => {
        setIsLoadingTimeline(false);
      });
    // dispatch(
    //   getInteractionVertical({
    //     object_id: objectId,
    //     record_id: recordID,
    //     current_page: currentPage,
    //     record_per_page: 20,
    //   })
    // );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, objectId, recordID]);

  const loadMore = () => {
    let tempPage = currentPage + 1;
    setCurrentPage(tempPage);
  };

  // const loadTimeline = useCallback(() => {
  //   dispatch(
  //     getInteraction({
  //       object_id: objectId,
  //       record_id: recordID,
  //       current_page: currentPage,
  //       record_per_page: 20,
  //     })
  //   );
  // }, [currentPage, dispatch, objectId, recordID]);

  const ButtonGroup = ({ next, previous, ...rest }) => {
    useEffect(() => {
      if (isUpdate) {
        rest.carouselState.currentSlide = 0;
      }
    }, [rest.carouselState]);

    setIsUpdate(false);

    const {
      carouselState: { currentSlide },
    } = rest;
    const lastItem = timelines.length - 5;

    return (
      <WrapArrow>
        {isLoadingTimeline ? (
          <Spin />
        ) : (
          <>
            {currentSlide !== 0 && (
              <img
                onClick={() => {
                  previous();
                }}
                src={ArrowLeft}
                alt="arrowLeft"
              />
            )}
            {totalTimelines !== timelines.length ||
            currentSlide !== lastItem ? (
              <img
                onClick={() => {
                  currentSlide === lastItem &&
                  totalTimelines !== timelines.length
                    ? loadMore()
                    : next();
                }}
                src={ArrowRight}
                alt="arrowRight"
              />
            ) : (
              <NotAllow src={ArrowRight} alt="arrowRight" />
            )}
          </>
        )}
      </WrapArrow>
    );
  };

  useEffect(() => {
    loadTimeline();
  }, [loadTimeline]);

  useEffect(() => {
    if (statusUpdateTimeline !== null) {
      loadTimeline();
      dispatch(resetStatus());
    }
  }, [statusUpdateTimeline, dispatch, loadTimeline]);

  useEffect(() => {
    dispatch(
      loadObjectsTarget({
        object_id: objectId,
      })
    );
  }, [dispatch, objectId]);

  return (
    <Wrapper>
      <Title>Horizontal timeline</Title>
      <Content>
        {/* <SubTitle>Historical Activities</SubTitle> */}

        {timelines.length === 0 ? (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <p>
              There's no any <span>Historical Activities</span>
            </p>
          </Empty>
        ) : timelines.length >= 6 ? (
          <Carousel
            ssr
            // itemClass="image-item"
            arrows={false}
            responsive={responsive}
            draggable={true}
            // itemClass="carousel-item-padding-0-px"
            className="animation-timeline"
            customButtonGroup={<ButtonGroup />}
          >
            {timelines.map((item, idx) => {
              return <TimelineItem item={item} key={idx} objectId={objectId} />;
            })}
          </Carousel>
        ) : (
          <Wrap>
            {timelines.map((item, idx) => {
              return <TimelineItem item={item} key={idx} objectId={objectId} />;
            })}
          </Wrap>
        )}
      </Content>
    </Wrapper>
  );
}

export default TimelineHorizontal;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;

  .react-multi-carousel-list {
    position: unset;
  }
`;

const Content = styled.div`
  padding: 8px;
`;

const Title = styled.div`
  padding: 16px;
  background: #fff;
  height: 36px;
  color: #2c2c2c;
  /* font-family: var(--roboto-500); */
  font-size: 17px;
  border-bottom: 1px solid #d9d9d9;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

// const SubTitle = styled.div`
//   font-size: 16px;
//   font-family: var(--roboto-700);
//   color: #252424;
//   margin-bottom: 24px;
// `;

const WrapArrow = styled.div`
  position: absolute;
  top: 18px;
  right: 50px;

  img {
    cursor: pointer;
  }
`;
const NotAllow = styled.img`
  cursor: not-allowed !important;
`;

const Wrap = styled.div`
  display: flex;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
