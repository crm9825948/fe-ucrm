import { useEffect, useState, useCallback } from "react";
import styled from "styled-components/macro";
import moment from "moment";
import InfiniteScroll from "react-infinite-scroll-component";
import ReactPlayer from "react-player";
import Axios from "axios";
import AttachFileIcon from "assets/icons/common/attach-file.svg";
import _ from "lodash";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import Form from "antd/lib/form";
import Spin from "antd/lib/spin";
import Tooltip from "antd/lib/tooltip";
import { checkTokenExpiration } from "contexts/TokenCheck";
import { BASE_URL_API } from "constants/constants";

function IC({ item, form, showModal }) {
  const [chatIC, setChatIC] = useState([]);
  const [pageIC, setPageIC] = useState(1);
  const [hasMore, setHasMore] = useState(true);

  const _onLoadIC = useCallback(async (id, page, channelType) => {
    const isTokenValid = await checkTokenExpiration();
    Axios({
      method: "POST",
      url:
        channelType === "email"
          ? BASE_URL_API + "interactions/load-email-interaction"
          : BASE_URL_API + "interactions/load-ic-chat",
      headers: {
        Authorization: isTokenValid,
      },
      data: {
        interaction_id: id,
        current_page: page,
      },
    })
      .then((res) => {
        // let temp = [...chatIC];
        // // eslint-disable-next-line array-callback-return
        // res.data.data.messages.map((ele) => {
        //   if (page === 1) {
        //     temp.unshift(ele);
        //   } else {
        //     temp.push(ele);
        //   }
        // });

        // let resultChat = temp.filter(
        //   (ele, idx, arr) =>
        //     arr.findIndex((chat) => chat._id === ele._id) === idx
        // );

        // setChatIC(resultChat);

        setChatIC((prev) => {
          let temp = [...prev];
          res.data.data.messages.forEach((ele) => {
            if (page === 1) {
              temp.unshift(ele);
            } else {
              temp.push(ele);
            }
          });

          let resultChat = temp.filter(
            (ele, idx, arr) =>
              arr.findIndex((chat) => chat._id === ele._id) === idx
          );
          if (temp.length >= res.data.data.total_message) {
            setHasMore(false);
          }

          return resultChat;
        });
      })
      .catch((err) => console.log(err));
  }, []);

  const _onOpenFile = (url) => {
    window.open(`${url}`);
  };

  const loadMoreChat = () => {
    if (showModal) {
      setTimeout(() => {
        let tempPage = pageIC + 1;
        setPageIC(tempPage);
        _onLoadIC(item.interaction_id, tempPage, item.channel_type);
      }, 1000);
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      note: item.notes?.join(""),
    });
  }, [form, item]);

  useEffect(() => {
    _onLoadIC(item.interaction_id, 1, item.channel_type);
  }, [_onLoadIC, item]);

  return (
    <Wrapper>
      <Form.Item label="Content" name="content">
        {chatIC.length > 0 ? (
          <WrapContent id="contentIC" height={item.channel_type === "email"}>
            <InfiniteScroll
              dataLength={chatIC.length}
              scrollableTarget="contentIC"
              loader={<Spin />}
              style={{
                display: "flex",
                flexDirection: "column-reverse",
                overflow: "hidden",
              }}
              endMessage={
                <p style={{ textAlign: "center" }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
              hasMore={hasMore}
              inverse={true}
              next={loadMoreChat}
            >
              {item.channel_type === "email"
                ? chatIC.map((item) => {
                    return (
                      <div style={{ display: "flex", flexDirection: "column" }}>
                        <WrapText
                          align={
                            item.sender_type === "agent" ||
                            item.sender_type === "bot" ||
                            item.sender_type === "activity_log"
                          }
                        >
                          <WrapEmail
                            align={
                              item.sender_type === "agent" ||
                              item.sender_type === "bot" ||
                              item.sender_type === "activity_log"
                            }
                          >
                            {item.subject && (
                              <SubjectEmail>{item.subject}</SubjectEmail>
                            )}
                            <ContentICEmail
                              align={
                                item.sender_type === "agent" ||
                                item.sender_type === "bot" ||
                                item.sender_type === "activity_log"
                              }
                            >
                              {parse(
                                item.content || item.content_text,
                                optionsParse
                              )}
                            </ContentICEmail>

                            {item.attachments &&
                              item.attachments.map((file) => (
                                <WrapFileEmail>
                                  <img src={AttachFileIcon} alt="AttachFile" />
                                  <span
                                    onClick={() =>
                                      _onOpenFile(file.file_upload_url)
                                    }
                                  >
                                    {file.file_name}
                                  </span>
                                </WrapFileEmail>
                              ))}

                            {item.from && (
                              <From>
                                <span>From:&nbsp;</span>
                                <p>{item.from}</p>
                              </From>
                            )}
                            {item.to && (
                              <To>
                                <span>To:&nbsp;</span>
                                <p>{item.to}</p>
                              </To>
                            )}
                          </WrapEmail>
                        </WrapText>
                        <DateEmail
                          align={
                            item.sender_type === "agent" ||
                            item.sender_type === "bot" ||
                            item.sender_type === "activity_log"
                          }
                        >
                          {moment(item.sent_time).format("h:mm | DD/MM/YYYY")}
                        </DateEmail>
                      </div>
                    );
                  })
                : chatIC.map((item, idx) => {
                    return (
                      <>
                        {item?.sender_type === "activity_log" ? (
                          <Tooltip
                            placement="bottom"
                            title={moment(item.sent_time).format(
                              "h:mm:ss | DD/MM/YYYY"
                            )}
                          >
                            <Log key={_.get(item, "id", idx)}>
                              <div
                                style={{
                                  position: "absolute",
                                  width: "100%",
                                  borderTop: "1px solid #c9c9c9",
                                }}
                              ></div>
                              <span>{_.get(item, "content_text", "")}</span>
                            </Log>
                          </Tooltip>
                        ) : (
                          <WrapText
                            align={
                              item.sender_type === "agent" ||
                              item.sender_type === "bot"
                            }
                          >
                            <Wrap>
                              <Tooltip
                                placement="bottom"
                                title={moment(item.sent_time).format(
                                  "h:mm:ss | DD/MM/YYYY"
                                )}
                              >
                                <ContentIC
                                  align={
                                    item.sender_type === "agent" ||
                                    item.sender_type === "bot"
                                  }
                                >
                                  {item.interaction_type === "image" ? (
                                    <img
                                      onClick={() =>
                                        window.open(item.content_file_url)
                                      }
                                      style={{
                                        width: "100%",
                                        objectFit: "cover",
                                        cursor: "pointer",
                                      }}
                                      src={item.content_file_url}
                                      alt="imageic"
                                    />
                                  ) : item.interaction_type === "file" ? (
                                    <WrapFile>
                                      <img src={File} alt="file" />
                                      <span
                                        onClick={() =>
                                          _onOpenFile(item.content_file_url)
                                        }
                                      >
                                        {item.content_file_name}
                                      </span>
                                    </WrapFile>
                                  ) : item.interaction_type === "link" ? (
                                    <WrapFile>
                                      <img src={File} alt="file" />
                                      <span
                                        onClick={() =>
                                          _onOpenFile(item.content_file_url)
                                        }
                                      >
                                        {item.content_file_name}
                                      </span>
                                      <p
                                        onClick={() =>
                                          _onOpenFile(item.content_file_url)
                                        }
                                      >
                                        {item.content_text}
                                      </p>
                                    </WrapFile>
                                  ) : item.interaction_type === "location" ? (
                                    <iframe
                                      title="location"
                                      src={`https://maps.google.com/maps?hl=en&q=${item.location_lat},${item.location_long}&t=&z=14&ie=UTF8&iwloc=B&output=embed`}
                                    />
                                  ) : item.interaction_type === "video" ? (
                                    <ReactPlayer
                                      url={item.content_file_url}
                                      controls
                                      width="100%"
                                      height="100%"
                                    />
                                  ) : item.interaction_type === "audio" ? (
                                    <audio
                                      controls
                                      src={item.content_file_url}
                                    />
                                  ) : (
                                    <p>{item.content_text}</p>
                                  )}
                                </ContentIC>
                              </Tooltip>
                            </Wrap>
                          </WrapText>
                        )}
                      </>
                    );
                  })}
            </InfiniteScroll>
          </WrapContent>
        ) : (
          <div
            style={{
              backgroundColor: "#f5f5f5",
              border: "1px solid #d9d9d9",
              padding: "4px 11px",
            }}
          >
            No content
          </div>
        )}
      </Form.Item>
    </Wrapper>
  );
}

export default IC;

const hexToRGB = (color) => {
  if (color) {
    const r = parseInt(color.slice(1, 3), 16);
    const g = parseInt(color.slice(3, 5), 16);
    const b = parseInt(color.slice(5, 7), 16);

    return `rgb(${r}, ${g}, ${b},0.12)`;
  }
};

const Wrapper = styled.div`
  .ant-input[disabled] {
    color: #2c2c2c;
    font-size: 16px;
  }
`;

const WrapContent = styled.div`
  background-color: #f5f5f5;
  border: 1px solid #d9d9d9;
  padding: 4px 11px;
  height: ${({ height }) => (height ? "616px" : "350px")};
  overflow: auto;
  display: flex;
  flex-direction: column-reverse;
`;

const WrapText = styled.div`
  display: flex;
  justify-content: ${(props) => (props.align ? "flex-end" : "flex-start")};
`;
const Wrap = styled.div`
  width: fit-content;
  max-width: 49%;
`;

const WrapEmail = styled.div`
  width: fit-content;
  max-width: 590px;
  background-color: ${(props) =>
    props.align ? `${hexToRGB(props.theme.main)}` : "#fff"};
  border-radius: ${(props) =>
    props.align ? "12px 12px 0px 12px" : "12px 12px 12px 0px"};
  padding: 24px 16px;

  p {
    margin-bottom: 0;
  }
`;

const Text = styled.p`
  background-color: ${(props) => (props.align ? "#D3E1F4" : "#fff")};
  border-radius: ${(props) =>
    props.align ? "12px 12px 0px 12px" : "12px 12px 12px 0px"};
  padding: 8px 12px;
  color: #000;
  margin-bottom: 6px;
`;

const ContentIC = styled(Text)`
  word-break: break-word;

  span {
    cursor: pointer;
  }

  p {
    margin-bottom: 0 !important;
  }
`;

const ContentICEmail = styled(Text)`
  padding: 0;
  background: unset;

  img {
    width: 100%;
    object-fit: cover;
  }
`;

const DateEmail = styled.div`
  font-size: 14px;
  line-height: 20px;
  color: #8c8c8c;
  margin: 8px 0;
  display: flex;
  justify-content: ${(props) => (props.align ? "flex-end" : "flex-start")};
`;

const SubjectEmail = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: #252424;
  font-family: var(--roboto-500);
  padding-bottom: 14px;
  margin-bottom: 14px;
  border-bottom: 1px solid #e4e8eb;
`;

const WrapFile = styled.div`
  img {
    margin-right: 10px;
  }
`;

const WrapFileEmail = styled(WrapFile)`
  cursor: pointer;
  display: flex;
  align-items: center;
  margin: 8px 0;

  span {
    color: ${(props) => props.theme.main};
  }
`;

const From = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;

  span {
    line-height: 20px;
    color: #8c8c8c;
  }

  p {
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    color: #252424;
    margin-bottom: 0;
  }
`;

const To = styled(From)``;

const Log = styled.div`
  position: relative;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  span {
    font-size: 12px;
    text-align: center;
    color: #5c5757;
    padding: 2px 8px;
    background: ${({ isHightlight }) => (isHightlight ? "#20A2A2" : "#dcdcdc")};
    border-radius: 10px;
    max-width: 85%;
    z-index: 2;
  }
`;
