import { useEffect } from "react";
import styled from "styled-components/macro";
import moment from "moment";

import Form from "antd/lib/form";
import Input from "antd/lib/input";

function VBEE({ item, form }) {
  useEffect(() => {
    form.setFieldsValue({
      note: item.notes?.join(""),
      created_by_username: item.created_by_username,
      created_date: item.created_date,
      CallPhone: item.CallPhone,
    });
  }, [form, item]);

  return (
    <Wrapper>
      <Form.Item label="Created by" name="created_by_username">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Created date" name="created_date">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Call phone" name="CallPhone">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Content" name="content" className="formItemNote">
        <WrapContent>
          {item.ContentOfBotCallers.map((item) => {
            return (
              <WrapText align={item.Isbot}>
                <Wrap>
                  <Text align={item.Isbot}>{item.Content}</Text>
                  <Time align={item.Isbot}>
                    {moment(item.CreateAt).format("MMMM Do YYYY, h:mm:ss a")}
                  </Time>
                </Wrap>
              </WrapText>
            );
          })}
        </WrapContent>
      </Form.Item>
    </Wrapper>
  );
}

export default VBEE;

const Wrapper = styled.div`
  .ant-input[disabled] {
    color: #2c2c2c;
    font-size: 16px;
  }
`;

const WrapContent = styled.div`
  background-color: #f5f5f5;
  border: 1px solid #d9d9d9;
  padding: 4px 11px;
  height: 350px;
  overflow: auto;
`;

const WrapText = styled.div`
  display: flex;
  justify-content: ${(props) => (props.align ? "flex-end" : "flex-start")};
`;

const Wrap = styled.div`
  width: fit-content;
  max-width: 49%;
`;

const Text = styled.p`
  background-color: ${(props) => (props.align ? "#D3E1F4" : "#fff")};
  border-radius: ${(props) =>
    props.align ? "12px 12px 0px 12px" : "12px 12px 12px 0px"};
  padding: 8px 12px;
  color: #000;
  margin-bottom: 6px;
`;

const Time = styled.p`
  margin: 0;
  color: #979797;
  font-size: 12px;
  text-align: ${(props) => (props.align ? "right" : "left")};
`;
