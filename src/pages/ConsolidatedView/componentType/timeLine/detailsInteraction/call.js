import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { BASE_URL_API, BASENAME } from "constants/constants";
import _ from "lodash";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Table from "antd/lib/table";
import Collapse from "antd/lib/collapse";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

import SearchOutlined from "@ant-design/icons/SearchOutlined";
import Details from "assets/icons/users/details.svg";

import {
  loadDataLinking,
  loadHeaderLinking,
  loadPaginationLinking,
  loadListObjectField,
} from "redux/slices/objects";

import LinkingList from "pages/Objects/modal/fieldsType/linking/linkingList";
// import { BASE_URL_API } from "constants/constants";
import ModalRecord from "pages/ConsolidatedView/modalRecord";

const eventCalculations = [
  {
    title: "Talkover",
    key: "talkover",
  },
  {
    title: "Silence",
    key: "silence",
  },
  {
    title: "Pause",
    key: "pause",
  },
  {
    title: "Hold",
    key: "hold",
  },
];

function Call({
  item,
  form,
  listObject,
  objectId,
  selectedRowKeys,
  setSelectedRowKeys,
  showAddNoteCall,
  setShowAddNoteCall,
  recordInteraction,
  formRecord,
}) {
  const { TextArea } = Input;
  const { Column } = Table;
  const { Panel } = Collapse;

  const dispatch = useDispatch();
  const { hiddenArray, recordData } = useSelector(
    (state) => state.objectsReducer
  );

  const [showType, setShowType] = useState(false);
  const [visible, setVisible] = useState(false);
  //   const [linkingFieldValue, setLinkingFieldValue] = useState({});
  const [objectSelect, setObjectSelect] = useState("");
  const [typeCreate, setTypeCreate] = useState("");

  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [fields, setFields] = useState([]);
  const [sortBy] = useState({});
  const [listRecord, setListRecord] = useState([]);
  const [rowData, setRowData] = useState({});

  const type = [
    {
      label: "Create",
      value: "create",
    },
    {
      label: "Linking",
      value: "linking",
    },
  ];

  const statusCall = [
    {
      label: "Hẹn gọi lại",
      value: "Hẹn gọi lại",
    },
    {
      label: "Không nghe máy",
      value: "Không nghe máy",
    },
    {
      label: "Đang lấy thông tin",
      value: "Đang lấy thông tin",
    },
    {
      label: "Suy nghĩ thêm",
      value: "Suy nghĩ thêm",
    },
    {
      label: "Đồng ý",
      value: "Đồng ý",
    },
    {
      label: "Không có nhu cầu",
      value: "Không có nhu cầu",
    },
    {
      label: "Máy bận",
      value: "Máy bận",
    },
    {
      label: "Không gọi được",
      value: "Không gọi được",
    },
    {
      label: "Khác",
      value: "Khác",
    },
  ];

  const handleSelectObject = (value) => {
    setShowType(true);
    setObjectSelect(value);
  };

  const handleSelectLinking = (selectedRowKeys) => {
    // Axios.post(
    //   BASE_URL_API + "object/load-linking-field-value",
    //   {
    //     object_related: objectSelect,
    //     record_id: selectedRowKeys[0],
    //     object_create_record: objectId,
    //   },
    //   {
    //     headers: {
    //       Authorization: localStorage.getItem("setting_accessToken"),
    //     },
    //   }
    // )
    //   .then((res) => {
    //     setLinkingFieldValue(res.data.data);
    //   })
    //   .catch(() => {});

    setVisible(false);
  };

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadHeaderLinking({
          object_id: objectSelect,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: objectSelect,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: objectSelect,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
    }
  }, [dispatch, objectSelect, visible]);

  useEffect(() => {
    form.setFieldsValue({
      note: item.notes?.join(""),
      subjectCall: item.subject,
      status_call: _.get(item, "status_call", undefined),
      username: item.username,
      total_duration: item.total_duration,
      bill_duration: item.bill_duration,
      connect_time: item.connect_time,
      created_by_username: item.created_by_username,
      created_date: item.created_date,
      extension: item.extension,
      extension_transfer: item.extension_transfer,
      hold_end_time: item.hold_end_time,
      hold_start_time: item.hold_start_time,
      start_time: item.start_time,
      end_time: item.end_time,
      link_record: item.link_record,
    });

    _.forEach(eventCalculations, (eventCalculation) => {
      form.setFieldsValue({
        [eventCalculation.key + "_avgDuration"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.avgDuration`,
          ""
        ),
        [eventCalculation.key + "_maxDuration"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.maxDuration`,
          ""
        ),
        [eventCalculation.key + "_minDuration"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.minDuration`,
          ""
        ),
        [eventCalculation.key + "_percentOfCall"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.percentOfCall`,
          ""
        ),
        [eventCalculation.key + "_totalDuration"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.totalDuration`,
          ""
        ),
        [eventCalculation.key + "_totalEvents"]: _.get(
          item,
          `eventCalculations.${eventCalculation.key}.totalEvents`,
          ""
        ),
      });
    });
  }, [form, item]);

  useEffect(() => {
    if (!showAddNoteCall) {
      form.setFieldsValue({
        newNote: undefined,
      });
    }
  }, [showAddNoteCall, form]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (!objectSelect) return;
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: objectSelect,
          show_meta_fields: true,
        })
      );

      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: objectSelect,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let tempField = [];
          let tempFieldHidden = [];
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sections);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              if (hiddenArray.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFields(tempFieldHidden);
        })
        .catch((err) => {});
    };
    checkToken();
    // eslint-disable-next-line
  }, [dispatch, objectSelect]);

  useEffect(() => {
    let tempList = [];
    recordInteraction.map((ele) => {
      return tempList.push({
        key: ele._id,
        record_id: ele._id,
        id: ele.value,
        object_id: ele.object_id,
      });
    });
    setListRecord(tempList);
  }, [recordInteraction]);

  return (
    <Wrapper>
      <Row gutter={8}>
        <Col span={8}>
          <Form.Item label="Username" name="username">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Total duration" name="total_duration">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Bill duration" name="bill_duration">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Connect time" name="connect_time">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Created by" name="created_by_username">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Created date" name="created_date">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Extension" name="extension">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Extension transfer" name="extension_transfer">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Hold end time" name="hold_end_time">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Hold start time" name="hold_start_time">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Start time" name="start_time">
            <Input disabled />
          </Form.Item>
          <Form.Item label="End time" name="end_time">
            <Input disabled />
          </Form.Item>
          <Form.Item label="Link record" name="link_record">
            <Input disabled />
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item label="Subject" name="subjectCall">
            <Input />
          </Form.Item>
          <Form.Item label="Status call" name="status_call">
            <Select
              placeholder="Please select"
              options={statusCall}
              disabled={_.get(item, "status_call", undefined) !== undefined}
            />
          </Form.Item>
          <Form.Item label="Note" name="note">
            <TextArea rows={5} disabled />
          </Form.Item>

          {showAddNoteCall ? (
            <Form.Item label="New note" name="newNote">
              <TextArea rows={5} />
            </Form.Item>
          ) : (
            <AddNote id="btnsavelogin" onClick={() => setShowAddNoteCall(true)}>
              + Add Note
            </AddNote>
          )}
          <Form.Item label="Object Target" name="objectTarget">
            <Select
              placeholder="Select target object"
              options={listObject}
              onChange={handleSelectObject}
            />
          </Form.Item>

          {showType && (
            <Form.Item label="Type" name="type">
              <Select
                placeholder="Please select type"
                options={type}
                onSelect={(e) => {
                  setTypeCreate(e);

                  if (e === "create") {
                    setOpenModalRecord(true);
                  }
                }}
              />
            </Form.Item>
          )}

          {typeCreate === "create" && (
            <ModalRecord
              open={openModalRecord}
              setOpen={setOpenModalRecord}
              initData={() => {}}
              recordPerPage={10}
              currentPage={0}
              form={formRecord}
              recordID={""}
              setRecordID={() => {}}
              objectId={objectSelect}
              fields={fields}
              sortBy={sortBy}
              interaction={item}
              objectIDInteraction={objectId}
            />
          )}

          {typeCreate === "linking" && (
            <Form.Item label="Select target record">
              {selectedRowKeys.length > 0 ? (
                <Input
                  disabled
                  placeholder="Selected"
                  addonAfter={
                    <>
                      <SearchOutlined
                        onClick={() => {
                          setVisible(true);
                        }}
                      />
                    </>
                  }
                />
              ) : (
                <Input
                  placeholder={`Please select`}
                  disabled
                  addonAfter={
                    <>
                      <SearchOutlined
                        onClick={() => {
                          setVisible(true);
                        }}
                      />
                    </>
                  }
                />
              )}
            </Form.Item>
          )}

          {recordInteraction.length > 0 && (
            <Table pagination={false} dataSource={listRecord}>
              <Column title="ID" dataIndex="id" key="id" />
              <Column
                title="View details"
                dataIndex="action"
                key="action"
                render={(text, record) => (
                  <WrapAction>
                    <img
                      onClick={() => {
                        window.open(
                          BASENAME +
                            `consolidated-view/${record.object_id}/${record.record_id}`
                        );
                      }}
                      src={Details}
                      alt="Consolidate view"
                    />
                  </WrapAction>
                )}
              />
            </Table>
          )}
        </Col>

        <Col span={8}>
          <Collapse defaultActiveKey={["talkover"]} bordered={false}>
            {_.map(eventCalculations, (eventCalculation) => (
              <Panel
                header={eventCalculation.title}
                key={eventCalculation.key}
                style={{ border: "none" }}
              >
                <Form.Item
                  label="Average duration"
                  name={`${eventCalculation.key}_avgDuration`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
                <Form.Item
                  label="Max duration"
                  name={`${eventCalculation.key}_maxDuration`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
                <Form.Item
                  label="Min duration"
                  name={`${eventCalculation.key}_minDuration`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
                <Form.Item
                  label="Total duration"
                  name={`${eventCalculation.key}_totalDuration`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
                <Form.Item
                  label="Percent of call"
                  name={`${eventCalculation.key}_percentOfCall`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
                <Form.Item
                  label="Total events"
                  name={`${eventCalculation.key}_totalEvents`}
                  style={{ marginBottom: "8px" }}
                >
                  <Input disabled />
                </Form.Item>
              </Panel>
            ))}
          </Collapse>
        </Col>
      </Row>

      {visible && (
        <LinkingList
          setVisible={setVisible}
          visible={visible}
          field={{ objectname: objectSelect }}
          selectedRowKeys={selectedRowKeys}
          setSelectedRowKeys={setSelectedRowKeys}
          handleSelect={handleSelectLinking}
          dataFilterLinking={{}}
          onCancelModal={() => setVisible(false)}
          recordData={recordData}
          rowData={rowData}
          setRowData={setRowData}
        />
      )}
    </Wrapper>
  );
}

export default Call;

const Wrapper = styled.div`
  .ant-input[disabled] {
    color: #2c2c2c;
    font-size: 16px;
  }

  .ant-collapse-header {
    background: #e9f6f6;
    padding: 8px 8px 8px 16px !important;
  }

  .ant-collapse-item {
    margin-bottom: 8px;
  }

  .ant-collapse-content-box {
    border: 1px solid #ececec;
    border-top: none;
  }
`;

const AddNote = styled(Button)`
  margin-bottom: 16px;
  font-size: 16px;
  height: unset;
  width: 8.125rem;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }

  :focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
