import { useEffect } from "react";
import styled from "styled-components/macro";

import Form from "antd/lib/form";
import Input from "antd/lib/input";

function SMS({ item, form }) {
  useEffect(() => {
    form.setFieldsValue({
      note: item.notes?.join(""),
      created_by_username: item.created_by_username,
      created_date: item.created_date,
      from: item.from,
      to: item.to,
      text: item.text,
      description: item.description,
      status: item.status === 0 ? "Failed" : "Successfully",
    });
  }, [form, item]);

  return (
    <Wrapper>
      <Form.Item label="Created by" name="created_by_username">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Created date" name="created_date">
        <Input disabled />
      </Form.Item>
      <Form.Item label="From" name="from">
        <Input disabled />
      </Form.Item>
      <Form.Item label="To" name="to">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Text" name="text">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Status" name="status">
        <Input disabled />
      </Form.Item>
      {item.status === 0 && (
        <Form.Item label="Description" name="description">
          <Input disabled />
        </Form.Item>
      )}
    </Wrapper>
  );
}

export default SMS;

const Wrapper = styled.div`
  .ant-input[disabled] {
    color: #2c2c2c;
    font-size: 16px;
  }
`;
