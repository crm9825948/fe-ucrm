import { useEffect } from "react";
import styled from "styled-components/macro";
import parse from "html-react-parser";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import { allowedTags } from "util/staticData";
import { CopyOutlined } from "@ant-design/icons";
import { Notification } from "components/Notification/Noti";

const options = {
  replace: (domNode) => {
    if (_.get(domNode, "name", "") === "style") {
      return <style></style>;
    }

    if (
      _.get(domNode, "name", "") !== "" &&
      !allowedTags.includes(_.get(domNode, "name", ""))
    ) {
      return (
        <span>
          {"< "}
          {_.get(domNode, "name", "")}
          {" >"} {_.get(domNode, "children[0].data", "")}
        </span>
      );
    }
  },
};

function Email({ item, form }) {
  useEffect(() => {
    form.setFieldsValue({
      note: item.notes?.join(""),
      created_by_username: item.created_by_username,
      created_date: item.created_date,
      from: item.from,
      to: item.to,
      bcc_email: item.bcc_email,
      cc_email: item.cc_email,
      reply_to: item.reply_to,
      subject: item.subject,
    });
  }, [form, item]);

  return (
    <Wrapper>
      <Form.Item label="Created by" name="created_by_username">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Created date" name="created_date">
        <Input disabled />
      </Form.Item>
      <Form.Item label="From" name="from">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Reply to" name="reply_to">
        <Input disabled />
      </Form.Item>
      <Form.Item label="To" name="to">
        <Input disabled />
      </Form.Item>
      <Form.Item label="CC" name="cc_email">
        <Input disabled />
      </Form.Item>
      <Form.Item label="BCC" name="bcc_email">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Subject" name="subject">
        <Input disabled />
      </Form.Item>
      <Form.Item label="Body" name="body">
        <Body>
          {parse(_.get(item, "body", ""), options)}
          <CustomButton
            onClick={() => {
              navigator.clipboard.writeText(_.get(item, "body", ""));
              Notification("success", "Copy body success");
            }}
          >
            <CopyOutlined />
          </CustomButton>
        </Body>
      </Form.Item>
    </Wrapper>
  );
}

export default Email;

const Wrapper = styled.div`
  .ant-input[disabled] {
    color: #2c2c2c;
    font-size: 16px;
  }
`;

const Body = styled.div`
  background-color: #f5f5f5;
  border: 1px solid #d9d9d9;
  padding: 4px 11px;
  position: relative;
  min-height: 32px;
  overflow: auto;

  img {
    max-width: 100%;
  }

  p {
    margin-bottom: 0;
  }

  table,
  td,
  tr,
  tbody {
    width: 367px !important;
    max-width: 367px !important;
  }
`;

const CustomButton = styled.div`
  position: absolute;
  right: 4px;
  bottom: 0px;
  cursor: pointer;
  svg {
    color: #acacac;
  }
`;
