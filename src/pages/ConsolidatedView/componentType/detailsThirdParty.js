import { EllipsisOutlined } from "@ant-design/icons";
import { Button, Col, Dropdown, Menu, Row, Tooltip, Spin } from "antd";
import axios from "axios";
import { BASE_URL_API, FE_URL } from "constants/constants";
import React, { useCallback, useEffect, useState } from "react";
// import { useSelector } from "react-redux";
import styled from "styled-components";
import { updateRecord } from "redux/slices/consolidatedViewSettings";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import img from "assets/icons/common/confirm.png";
import Refresh from "assets/icons/common/refresh.png";
import noDataImg from "assets/icons/common/nodata.png";
import { useSelector } from "react-redux";
import _ from "lodash";
import { checkTokenExpiration } from "contexts/TokenCheck";

const DetailsThirdParty = (props) => {
  const { data, recordID, onRemoveItem, edit: editProps, isEmbed } = props;

  const [value, setValue] = useState({});
  const [loading, setLoading] = useState(false);
  const [open, setOpen] = useState(false);
  const [columns, setColumns] = useState(1);
  const [detailSection, setDetailSection] = useState([]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const loadData = useCallback(async () => {
    setLoading(true);
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "third-party/load-data",
        {
          api_id: data._id,
          record_id: recordID,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        if (res.data.data) {
          setValue(res.data.data);
        }
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, [data._id, recordID]);

  useEffect(() => {
    loadData();
  }, [loadData]);

  const loadSetting = useCallback(async () => {
    if (data._id) {
      setLoading(true);
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "third-party/load-setting-details",
          {
            api_id: data._id,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          if (res.data.data.columns) {
            setColumns(res.data.data.columns);
          }
          if (res.data.data.details_column_setting) {
            setDetailSection(res.data.data.details_column_setting);
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
        });
    }
  }, [data._id]);

  useEffect(() => {
    loadSetting();
  }, [loadSetting]);

  return (
    <Wrapper
      style={{
        paddingTop: `${
          window.location.pathname.includes("list-view-with-details")
            ? ""
            : "36px"
        }`,
      }}
    >
      <div
        style={{
          height: "36px",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : isEmbed
              ? ""
              : "fixed"
          }`,
          top: 0,
          left: 0,
          zIndex: 9,
          boxShadow: "inset 0px -1px 0px #F0F0F0",
        }}
      >
        <Row style={{ width: "100%" }}>
          <Col span={18}> {data.name} </Col>

          <Col span={6} className="pagination-col">
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                alignItems: "center",
              }}
            >
              <Tooltip title="Reload">
                <CustomButtonReload
                  shape="circle"
                  // disabled={
                  //   Object.keys(selectedRecord).length === 0 ? true : false
                  // }
                  onClick={async () => {
                    // window.open(
                    //   BASENAME +
                    //     `consolidated-view/${data.related_object}/${selectedRecord._id}`
                    // );
                    const isTokenValid = await checkTokenExpiration();
                    setLoading(true);
                    axios
                      .post(
                        BASE_URL_API + "third-party/load-data",
                        {
                          api_id: data._id,
                          record_id: recordID,
                        },
                        {
                          headers: {
                            Authorization: isTokenValid,
                          },
                        }
                      )
                      .then((res) => {
                        if (res.data.data) {
                          setValue(res.data.data);
                        }
                        setLoading(false);
                      })
                      .catch((err) => {
                        setLoading(false);
                      });
                  }}
                >
                  <img alt="" src={Refresh} style={{ width: "18px" }} />
                </CustomButtonReload>
              </Tooltip>
              {editProps ? (
                <>
                  {" "}
                  {Object.keys(value).length > 0 ? (
                    <>
                      {" "}
                      <CustomButton
                        onClick={async () => {
                          let newItem = {};
                          /* eslint-disable-next-line */
                          Object.entries(value).forEach(
                            ([key, value], index) => {
                              value = value + "";
                              newItem[key] = value.trim();
                            }
                          );

                          setLoading(true);
                          const isTokenValid = await checkTokenExpiration();
                          axios({
                            method: "post",
                            url:
                              BASE_URL_API + "third-party/load-related-record",
                            headers: {
                              Authorization: `Bearer ${isTokenValid}`,
                            },
                            data: {
                              // object_id: object_id,
                              record_id: recordID,
                              api_id: data._id,
                              third_party_data: newItem,
                            },
                          })
                            .then((res) => {
                              setLoading(false);
                              if (
                                Object.prototype.toString.call(
                                  res.data.data
                                ) !== "[object Array]"
                              ) {
                                window.open(
                                  FE_URL +
                                    `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
                                );
                              }
                            })
                            .catch((err) => {
                              setLoading(false);
                              Notification(
                                "error",
                                err.response.data.error.value
                              );
                            });
                        }}
                      >
                        View
                      </CustomButton>
                      <CustomButton
                        onClick={() => {
                          setOpen(true);
                        }}
                      >
                        Update
                      </CustomButton>
                    </>
                  ) : (
                    ""
                  )}
                  <Dropdown
                    overlay={
                      <Menu>
                        {checkRule("delete") ? (
                          <Menu.Item
                            onClick={() => onRemoveItem(JSON.stringify(data))}
                          >
                            Delete
                          </Menu.Item>
                        ) : (
                          ""
                        )}
                        {Object.keys(value).length > 0 ? (
                          <>
                            <Menu.Item
                              onClick={async () => {
                                let newItem = {};
                                /* eslint-disable-next-line */
                                Object.entries(record).forEach(
                                  ([key, value], index) => {
                                    value = value + "";
                                    newItem[key] = value.trim();
                                  }
                                );

                                setLoading(true);
                                const isTokenValid =
                                  await checkTokenExpiration();
                                axios({
                                  method: "post",
                                  url:
                                    BASE_URL_API +
                                    "third-party/load-related-record",
                                  headers: {
                                    Authorization: `Bearer ${isTokenValid}`,
                                  },
                                  data: {
                                    // object_id: object_id,
                                    record_id: recordID,
                                    api_id: data._id,
                                    third_party_data: newItem,
                                  },
                                })
                                  .then((res) => {
                                    setLoading(false);
                                    if (
                                      Object.prototype.toString.call(
                                        res.data.data
                                      ) !== "[object Array]"
                                    ) {
                                      window.open(
                                        FE_URL +
                                          `/consolidated-view/${data.object_create_record}/${res.data.data.record_id}`
                                      );
                                    }
                                  })
                                  .catch((err) => {
                                    setLoading(false);
                                    Notification(
                                      "error",
                                      err.response.data.error.value
                                    );
                                  });
                              }}
                            >
                              View
                            </Menu.Item>
                            <Menu.Item
                              onClick={() => {
                                setOpen(true);
                              }}
                            >
                              Update
                            </Menu.Item>
                          </>
                        ) : (
                          ""
                        )}
                      </Menu>
                    }
                    placement="bottomLeft"
                  >
                    <Button>
                      <EllipsisOutlined />
                    </Button>
                  </Dropdown>
                </>
              ) : (
                ""
              )}
            </div>
          </Col>
        </Row>
      </div>
      {loading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            marginTop: "10%",
            flexDirection: "column",
            background: "white",
          }}
        >
          {/* <LoadingOutlined
            style={{ fontSize: 60, color: defaultBrandName.theme_color }}
            spin
          /> */}
          <Spin />
          <div style={{ textAlign: "center", marginTop: "20px" }}>
            Loading. . .{" "}
          </div>
        </div>
      ) : (
        <>
          {detailSection.length > 0 ? (
            <div style={{ minWidth: "100%", padding: "0px 8px" }}>
              <NewWrap columns={columns}>
                {Object.keys(value).length > 0 ? (
                  <>
                    {detailSection.map((detail, index) => (
                      <BasicInfo className="item" key={index}>
                        <legend>{detail.section_name}</legend>
                        <table style={{ width: "100%" }}>
                          <thead>
                            <tr>
                              <th></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                            {Object.entries(value).map(
                              ([key, value]) =>
                                detail.display_fields.includes(key) && (
                                  <tr>
                                    <td className="item-name">{key}</td>
                                    <td className="item-value">{value}</td>
                                  </tr>
                                )
                            )}
                          </tbody>
                        </table>
                      </BasicInfo>
                    ))}
                  </>
                ) : (
                  // Object.entries(value).map(([key, value], index) => {
                  //   return (
                  //     <div className="item">
                  //       <div className="item-name">{key}</div>
                  //       <div className="item-value">{value}</div>
                  //     </div>
                  //   );
                  // })
                  <div
                    style={{
                      padding: "20px",
                      textAlign: "center",
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      alt=""
                      src={noDataImg}
                      style={{ width: "100px", marginBottom: "10px" }}
                    />
                    No data
                  </div>
                )}
              </NewWrap>
            </div>
          ) : (
            <div style={{ minWidth: "100%", padding: "0px 8px" }}>
              <WrapTable columns={columns}>
                {Object.keys(value).length > 0 ? (
                  Object.entries(value).map(([key, value], index) => {
                    return (
                      <div className="item">
                        <div className="item-name">{key}</div>
                        <div className="item-value">{value}</div>
                      </div>
                    );
                  })
                ) : (
                  <div
                    style={{
                      padding: "20px",
                      textAlign: "center",
                      width: "100%",
                      display: "flex",
                      flexDirection: "column",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <img
                      alt=""
                      src={noDataImg}
                      style={{ width: "100px", marginBottom: "10px" }}
                    />
                    No data
                  </div>
                )}
              </WrapTable>
            </div>
          )}
        </>
      )}
      <ModalConfirm
        title={"Bạn có chắc chắn thay đổi bản ghi này?"}
        decs={"Do you want to update this record with the exist setting?"}
        open={open}
        setOpen={setOpen}
        method={updateRecord}
        data={{
          api_id: data._id,
          record_id: recordID,
          third_party_data: value,
          // api_id: apiID,
          // third_party_data: newItem,
          // record_id: recordID,
        }}
        setOpenModal={() => {}}
        img={img}
      />
    </Wrapper>
  );
};

export default DetailsThirdParty;

const CustomButtonReload = styled(Button)`
  background: ${(props) => props.theme.white};
  color: ${(props) => props.theme.white};
  margin-left: 8px;
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const WrapTable = styled.div`
  background: #fff;
  display: flex;
  flex-wrap: wrap;
  min-width: 100%;
  gap: 0 8px;
  .item {
    width: ${(props) =>
      props.columns
        ? `calc((100% - ${props.columns * 8}px) / ${props.columns})`
        : "100%"};
    display: flex;
    margin-top: -1px;
    flex-wrap: wrap;
    height: fit-content;
    min-height: 31px;

    .item-name {
      font-style: normal;
      font-family: var(--roboto-500);
      font-size: 16px;
      line-height: 130%;
      padding: 4px 8px;
      color: #2c2c2c;
      vertical-align: top;
      width: 155px;
      word-break: break-word;
    }
    .item-value {
      line-height: 130%;
      font-family: var(--roboto-400);
      font-size: 16px;
      letter-spacing: 0.01em;
      color: #2c2c2c;
      white-space: pre-line;
      padding: 4px 8px;
      vertical-align: top;
      flex: 1;
      border: 1px solid #ececec;
      word-break: break-word;
    }
  }
`;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
`;
const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;
const BasicInfo = styled.fieldset`
  /* height: 518px; */
  border: 1px solid #ebebeb;
  max-height: 497px;
  overflow-y: auto;
  background: #fff;
  padding: 8px;
  border-radius: 10px;

  legend {
    background: #fff;
    padding: 5px 10px;
    width: fit-content;
    color: ${(props) => props.theme.main};
    border-radius: 5px;
    font-size: 16px;
    margin-bottom: -20px;
    border-bottom: none;
  }
`;

const NewWrap = styled.div`
  background: #fff;
  display: flex;
  flex-wrap: wrap;
  min-width: 100%;
  margin-top: 1px;
  /* table {
    table-layout: fixed;
  } */
  .item {
    width: ${(props) =>
      props.columns
        ? `calc((100% - ${props.columns * 8}px) / ${props.columns})`
        : "100%"};
    display: flex;
    margin-top: -1px;
    flex-wrap: wrap;
    height: fit-content;
    min-height: 31px;

    .item-name {
      font-style: normal;
      font-family: var(--roboto-500);
      font-size: 16px;
      line-height: 130%;
      padding: 4px;
      color: #2c2c2c;
      vertical-align: top;
      word-break: break-word;
      border: solid 1px #ececec;
      min-width: 92px;
      max-width: 100px;
    }
    .item-value {
      line-height: 130%;
      font-family: var(--roboto-400);
      font-size: 16px;
      letter-spacing: 0.01em;
      color: #2c2c2c;
      white-space: pre-line;
      padding: 4px 8px;
      vertical-align: top;
      border: 1px solid #ececec;
      word-break: break-word;
    }
  }
`;
