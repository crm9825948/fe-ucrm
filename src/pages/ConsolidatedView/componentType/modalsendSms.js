import React, { useEffect, useState } from "react";
import { Modal, Button, Input, Select, Radio } from "antd";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import {
  actionSendSMS,
  getListSMSTemplate,
  getSmsTemplateDetail,
  changeLoadMore,
} from "redux/slices/componentSMS";
import { useTranslation } from "react-i18next";

const { Option } = Select;
const { TextArea } = Input;

const options = [
  { label: "Tiếng Việt không dấu", value: 0 },
  { label: "Tiếng Việt có dấu", value: 1 },
];

const ModalsendSms = ({
  openModal,
  setOpenModal,
  objectId,
  recordID,
  setInputSearch,
  setPage,
  setValueSearch,
}) => {
  const dispatch = useDispatch();

  const { listSMSTemplate, smsTemplateDetail } = useSelector(
    (state) => state.componentSMSReducer
  );

  const { t } = useTranslation();
  const [idTeamplate, setIdTempalte] = useState(undefined);
  const [send, setSend] = useState(null);
  const [content, setContent] = useState("");
  const [unicode, setUnicode] = useState(0);
  const [disabled, setDisabled] = useState(true);
  const [smsTemplate, setSmsTemplate] = useState([]);
  useEffect(() => {
    let temp = [...listSMSTemplate];

    temp = temp.filter((item) => {
      return item.object_id === objectId;
    });

    setSmsTemplate(temp);
  }, [listSMSTemplate, objectId]);

  useEffect(() => {
    if (Object.keys(smsTemplateDetail).length > 0) {
      if (unicode === 0) {
        setContent(smsTemplateDetail.content.slice(0, 160));
      } else {
        setContent(smsTemplateDetail.content.slice(0, 70));
      }

      setSend(smsTemplateDetail.send_to);
    }
    //eslint-disable-next-line
  }, [smsTemplateDetail]);

  useEffect(() => {
    dispatch(getListSMSTemplate());
  }, [dispatch]);

  useEffect(() => {
    if (!content) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [content]);

  useEffect(() => {
    if (idTeamplate && recordID) {
      dispatch(
        getSmsTemplateDetail({
          templateId: idTeamplate,
          recordId: recordID,
        })
      );
    }
  }, [idTeamplate, recordID, dispatch]);

  useEffect(() => {
    if (!openModal) {
      setContent("");
      setSend(null);
      setUnicode(0);
      setIdTempalte(undefined);
    }
  }, [openModal]);

  const handleSendSMS = () => {
    setInputSearch("");
    setPage(0);
    setValueSearch("");
    dispatch(changeLoadMore(false));
    dispatch(
      actionSendSMS({
        dataSms: {
          object_id: objectId,
          unicode: unicode,
          content: content,
          send_to: send,
          record_id: recordID,
        },
        objectId: objectId,
        record_id: recordID,
        data: {
          limit: 10,
          page: 0,
          search_data: {},
        },
      })
    );
  };

  return (
    <CustomModal
      title={t("workflow.sendSMS")}
      width={960}
      visible={openModal}
      onCancel={() => setOpenModal(false)}
      footer={[
        <ButtonGroup key="button">
          <div className="btnGroup">
            <Button
              disabled={disabled}
              onClick={() => {
                // dispatch(changeListSMS([]));
                handleSendSMS();
                setOpenModal(false);
              }}
              key="send"
            >
              {t("common.send")}
            </Button>
            <Button key="cancel" onClick={() => setOpenModal(false)}>
              {t("common.cancel")}
            </Button>
          </div>
        </ButtonGroup>,
      ]}
    >
      {/* <ItemWrap>
        <CustomText>Send to</CustomText>
        <CustomInput
          placeholder="Send to..."
          value={send}
          onChange={(e) => {
            setSend(e.target.value);
          }}
        />
      </ItemWrap> */}
      <ItemWrap>
        <CustomText>{t("workflow.smsTemplate")}</CustomText>
        <CustomSelect
          value={idTeamplate}
          onChange={(value) => setIdTempalte(value)}
          placeholder={t("workflow.chooseTemplate")}
        >
          {smsTemplate.map((item, index) => {
            return (
              <Option key={index} value={item._id}>
                {item.sms_template_name}
              </Option>
            );
          })}
        </CustomSelect>
      </ItemWrap>
      <ItemUnicode>
        <CustomText>Unicode</CustomText>
        <Radio.Group
          options={options}
          onChange={(e) => {
            if (e.target.value === 0) {
              const temp = content.slice(0, 160);
              setContent(temp);
            } else {
              const temp = content.slice(0, 70);
              setContent(temp);
            }
            setUnicode(e.target.value);
          }}
          value={unicode}
          optionType="button"
        />
      </ItemUnicode>

      <TextArea
        showCount
        maxLength={unicode === 0 ? 160 : 70}
        rows={8}
        value={content}
        placeholder={t("common.placeholderInput")}
        onChange={(e) => setContent(e.target.value)}
      />
    </CustomModal>
  );
};

export default ModalsendSms;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0px 0px;
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    background: #f2f4f5;
  }

  .ant-input:focus,
  .ant-input-focused {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }

  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
`;

const ButtonGroup = styled.div`
  button {
    border-radius: 2px;
    width: 114px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:disabled {
        color: rgba(0, 0, 0, 0.25) !important;
        border-color: #d9d9d9 !important;
        background: #f5f5f5 !important;
      }
      &:first-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;

const ItemWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
`;

const CustomText = styled.span`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  width: 30%;
`;

// const CustomInput = styled(Input)`
//   width: 70%;
// `;

const CustomSelect = styled(Select)`
  width: 70%;
`;

const ItemUnicode = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 24px;

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled):hover,
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: ${(props) => props.theme.main} !important;
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled):focus-within {
    box-shadow: none;
  }
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled)::before {
    background-color: ${(props) => props.theme.main};
  }
`;
