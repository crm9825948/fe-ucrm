import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";

import { LoadingOutlined } from "@ant-design/icons";
import { loadTemplateComponent } from "redux/slices/campaign";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
function CampaignTemplate({ recordID }) {
  const dispatch = useDispatch();

  const { isLoading, templateComponent } = useSelector(
    (state) => state.campaignReducer
  );
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const [activeTemplate, $activeTemplate] = useState({
    object: "",
    template: "",
  });

  const _onSelectTemplate = (objectID, idx) => {
    $activeTemplate({
      object: templateComponent?.recipient_objects[idx]?.object_id,
      template: templateComponent?.template_info[objectID]?.body
        ? templateComponent?.template_info[objectID]?.body
        : templateComponent?.template_info[objectID]?.content,
    });
  };

  useEffect(() => {
    if (recordID) {
      dispatch(
        loadTemplateComponent({
          campaign_id: recordID,
        })
      );
    }
  }, [dispatch, recordID]);

  useEffect(() => {
    if (!_.isEmpty(templateComponent)) {
      if (templateComponent.campaign_type === "Email") {
        $activeTemplate({
          object: "",
          template: templateComponent?.template_info[0]?.archive_html,
        });
      } else if (templateComponent.campaign_type === "SMS") {
        $activeTemplate({
          object: templateComponent?.recipient_objects[0]?.object_id,
          template: templateComponent?.template_info[
            templateComponent?.recipient_objects[0]?.object_id
          ]?.body
            ? templateComponent?.template_info[
                templateComponent?.recipient_objects[0]?.object_id
              ]?.body
            : templateComponent?.template_info[
                templateComponent?.recipient_objects[0]?.object_id
              ]?.content,
        });
      }
    }
  }, [templateComponent]);

  return (
    <Wrapper>
      {isLoading.templateComponent ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            marginTop: "10%",
            flexDirection: "column",
            background: "white",
          }}
        >
          <LoadingOutlined
            style={{ fontSize: 60, color: defaultBrandName.theme_color }}
            spin
          />
          <div style={{ textAlign: "center", marginTop: "20px" }}>
            Loading. . .{" "}
          </div>
        </div>
      ) : (
        <>
          {!_.isEmpty(templateComponent) && (
            <>
              <WrapTitle>
                <p>Template</p>

                {templateComponent?.campaign_type !== "Email" && (
                  <>
                    {_.map(
                      _.get(templateComponent, "recipient_objects", []),
                      (object, idx) => (
                        <ObjectName
                          active={activeTemplate.object === object.object_id}
                          onClick={() =>
                            _onSelectTemplate(object.object_id, idx)
                          }
                        >
                          {object.object_name}
                        </ObjectName>
                      )
                    )}
                  </>
                )}
              </WrapTitle>

              {activeTemplate.template && (
                <PreviewTemplate>
                  {parse(activeTemplate.template, optionsParse)}
                </PreviewTemplate>
              )}
            </>
          )}
        </>
      )}
    </Wrapper>
  );
}

export default CampaignTemplate;

const Wrapper = styled.div`
  padding: 16px;
  width: 100%;
  height: 100%;
`;

const WrapTitle = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;

  p {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
    font-family: var(--roboto-500);
    margin-right: 24px;
    margin-bottom: 0;
  }
`;

const ObjectName = styled.div`
  display: flex;
  align-items: center;
  color: ${(props) => (props.active ? "#fff" : "rgba(0, 0, 0, 0.85)")};
  width: fit-content;
  height: 26px;
  padding: 6px 12px;
  background: ${(props) => (props.active ? props.theme.main : "#E6E6E6")};
  border-radius: 8px;
  margin-right: 8px;
  font-size: 12px;
  cursor: pointer;
`;

const PreviewTemplate = styled.div`
  max-height: calc(100% - 40px);
  overflow: auto;
  padding: 10px;
  border: 1px solid #ececec;
  box-shadow: inset 0px 0px 26px rgba(138, 138, 138, 0.25);
  border-radius: 8px;

  ::-webkit-scrollbar {
    height: 10px;
  }
`;
