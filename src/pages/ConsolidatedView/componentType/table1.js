import {
  // CaretDownOutlined,
  // CaretUpOutlined,
  EllipsisOutlined,
  LoadingOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import {
  Button,
  Dropdown,
  Form,
  Input,
  Menu,
  Pagination,
  Row,
  Select,
  Space,
  Table,
  Tooltip,
  Typography,
  Col,
  Spin,
  InputNumber,
  DatePicker,
} from "antd";
import deleteImg from "assets/icons/common/deleteImg.svg";
import editImg from "assets/icons/common/editSVG.svg";
import Details from "assets/icons/common/eye.svg";
import Refresh from "assets/icons/common/refresh.png";
import fiterIcon from "assets/icons/common/filter.png";
import axios from "axios";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, FE_URL, BE_URL } from "constants/constants";
import moment from "moment";
import ModalComponent from "pages/ConsolidatedViewSettings/ComponentSettings/modalComponent";
import CustomView from "pages/Objects/customView/customView";
import React, { useCallback, useEffect, useState, useRef } from "react";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import { loadUserDynamicButton } from "redux/slices/dynamicButton";
import { loadRelatedObject } from "redux/slices/fieldsManagement";
import {
  deleteRecord,
  loadLinkingFieldValue,
  loadListObjectField,
  loadRecordData,
  loadSourceObjects,
  loadTargetObjects,
  loadRecordDataSuccess,
  setLinkingFieldValue,
  loadListObjectFieldSuccess,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalRecord from "../modalRecord";
import ModalDelete from "./delete/modalConfirmDelete";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import { BASENAME } from "constants/constants";
import _ from "lodash";
import WithDetails from "./withDetails";
import { checkTokenExpiration } from "contexts/TokenCheck";

const { Text: TextComponent } = Typography;
const { Option } = Select;

const TableComponent = (props) => {
  const {
    recordID: record_id,
    data,
    objectId,
    edit: editProp,
    onRemoveItem,
    setDrag,
  } = props;
  const { allObject } = useSelector((state) => state.tenantsReducer);
  const [allFields, setAllFields] = useState({});
  const [recordPerPage, setRecordPerPage] = useState(50);
  const [currentPage, setCurrentPage] = useState(1);
  const [isLoadingPagi, setIsLoadingPage] = useState(false);
  const [, setDataSource] = useState([]);
  const [, setColumns] = useState([]);
  const [loading, setLoading] = useState(false);
  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [recordID, setRecordID] = useState("");
  const [form] = Form.useForm();
  const [, setEditingKey] = useState("");
  const [loadingDetails, setLoadingDetails] = useState(false);
  // const [, setFormValue] = useState({});

  // const [recordData, setRecordData] = useState({});
  const [fields, setFields] = useState([]);
  const [searchList, setSearchList] = useState({});
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearcherColumn] = useState("");
  const [componentID, setComponentID] = useState("");
  //modal component
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [, setDataItem] = useState({});
  //custom view
  const [openCustomView, setOpenCustomView] = useState(false);
  //handle customview
  const [filterID, setFilterID] = useState("");
  const [openDelete, setOpenDelete] = useState(false);
  const [right, setRight] = useState("accepted");
  const [listHeaders, setListHeaders] = useState([]);
  const [listData, setListData] = useState([]);
  const [selectedRecord, setSelectedRecord] = useState({});
  const [selectedRowKeys, setSelectedRowKeys] = useState("");
  /*eslint-disable-next-line*/
  const [sortBy, setSortBy] = useState({});
  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);
  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);
  const [totalRecord, setTotalRecord] = useState(null);
  const [recordDataNew, setRecordDataNew] = useState({});

  // useEffect(() => {
  //   if (listData.length > 0 && data?.with_details) {
  //     setSelectedRowKeys([listData[0]._id]);
  //     setSelectedRecord(listData[0]);
  //     form.resetFields();
  //     setRecordID(listData[0]._id);
  //     if (data?.with_details) {
  //       edit(listData[0], allFields, "details");
  //     }
  //     setDrag(false);
  //   }
  //   //eslint-disable-next-line
  // }, [listData, data]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [leftWidth] = useState(
    localStorage.getItem("crm_width") ||
      localStorage.getItem("crm_width") === "0px"
      ? "30%"
      : localStorage.getItem("crm_width") || "30%"
  );
  const [hiddenArray, setHiddenArray] = useState([]);
  const divRef = useRef(null);
  useEffect(() => {
    const observer = new ResizeObserver((entries) => {
      entries.forEach((entry) => {
        const newWidth = entry.contentRect.width;
        localStorage.setItem("crm_width", `${newWidth}px`);
      });
    });

    if (divRef.current) {
      observer.observe(divRef.current);
    }

    return () => {
      observer.disconnect();
    };
  }, []);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    setComponentID(data._id);
    setDataItem(data);
  }, [data]);

  // useEffect(() => {
  //   setFirstID(listData.length > 0 ? listData[0]._id : null);
  //   setLastID(listData.length > 0 ? listData[listData.length - 1] : null);
  // }, [listData]);

  const [listObjectField, setListObjectField] = useState([]);

  const dispatch = useDispatch();

  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const {
    linkingFieldValue,
    /*eslint-disable-next-line*/
    hiddenArray: hiddenArrayReducer,
    // listObjectField,
    isLoadingUpdateRecord,
    isLoadingCreateRecord,
    isLoadingDeleteRecord,
    // recordData,
  } = useSelector((state) => state.objectsReducer);
  /*eslint-disable-next-line*/
  const [write, setWrite] = useState(false);
  /*eslint-disable-next-line*/
  const [read, setRead] = useState(false);
  /* eslint-disable */
  const [update, setUpdate] = useState(false);
  const [deleteRole, setDeleteRole] = useState(false);

  useEffect(() => {
    /* eslint-disable-next-line */
    listObjectField.map((item, idx) => {
      if (Object.keys(item)[0] === "main_object") {
        setWrite(item[Object.keys(item)[0]]?.create_permission);
        setRead(item[Object.keys(item)[0]]?.read_permission);
        setUpdate(item[Object.keys(item)[0]]?.edit_permission);
        setDeleteRole(item[Object.keys(item)[0]]?.delete_permission);
      }
    });
  }, [listObjectField]);

  useEffect(() => {
    if (isLoadingUpdateRecord === false || isLoadingCreateRecord === false) {
      if (data?.with_details) {
      } else {
        setRecordID("");
        setSelectedRecord({});
        setSelectedRowKeys("");
      }
    }
  }, [isLoadingUpdateRecord, isLoadingCreateRecord, isLoadingDeleteRecord]);

  useEffect(() => {
    if (isLoadingDeleteRecord === false) {
      onCancel();
    }
  }, [isLoadingDeleteRecord]);

  const loadDataCallBack = useCallback(async () => {
    let searchData = [];
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    let dataPost = filterID
      ? {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          first_record_id: firstID,
          last_record_id: lastID,
          custom_view_id: filterID,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        }
      : {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          first_record_id: firstID,
          last_record_id: lastID,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        };
    setIsLoadingPage(true);
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        if (Array.isArray(res.data.data)) {
          Notification("error", "You do not have right to see this data!");
          setRight("noAccepted");
        } else {
          setRight("accepted");
          let tmpHeaders = res.data.data.fields.map((item, idx) => {
            return {
              ...item,
              key: item.ID,
              dataIndex: item.ID,
              title: (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <span style={{ marginRight: "8px" }}>
                    {" "}
                    {item.name === "Owner" ? "Assign to" : item.name}
                  </span>
                </div>
              ),
              ...getColumnSearchProps(
                item.ID,
                idx,
                item.name,
                searchList,
                item,
                searchText,
                searchedColumn,
                item.type
              ),
            };
          });
          setListHeaders(tmpHeaders);

          let tmpData = [];
          if (Array.isArray(res.data.data.record_data?.data)) {
            tmpData = res.data.data.record_data?.data.map((item, idx) => {
              let newObject = {};
              Object.entries(item).map(([key, value], index) => {
                if (typeof value === "object" && value !== null) {
                  newObject[key] = value.value;
                } else {
                  newObject[key] = value;
                }
              });
              return { ...newObject, key: newObject._id };
            });
          }
          setListData(tmpData);
        }
        setIsLoadingPage(false);
      })
      .catch((err) => {
        setIsLoadingPage(false);
      });
    setTotalRecord(null);
  }, [currentPage, recordPerPage, record_id, filterID, sortBy]);

  const loadDataNew = async () => {
    let searchData = [];
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    let dataPost = filterID
      ? {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          first_record_id: firstID,
          last_record_id: lastID,
          custom_view_id: filterID,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        }
      : {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: currentPage,
          record_per_page: recordPerPage,
          first_record_id: firstID,
          last_record_id: lastID,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        };
    setIsLoadingPage(true);
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        if (Array.isArray(res.data.data)) {
          Notification("error", "You do not have right to see this data!");
          setRight("noAccepted");
        } else {
          setRight("accepted");
          let tmpHeaders = res.data.data.fields.map((item, idx) => {
            return {
              ...item,
              key: item.ID,
              dataIndex: item.ID,
              title: (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <span style={{ marginRight: "8px" }}>
                    {" "}
                    {item.name === "Owner" ? "Assign to" : item.name}
                  </span>
                </div>
              ),
              ...getColumnSearchProps(
                item.ID,
                idx,
                item.name,
                searchList,
                item,
                searchText,
                searchedColumn,
                item.type
              ),
            };
          });
          setListHeaders(tmpHeaders);

          let tmpData = res.data.data.record_data?.data.map((item, idx) => {
            let newObject = {};
            Object.entries(item).map(([key, value], index) => {
              if (typeof value === "object" && value !== null) {
                newObject[key] = value.value;
              } else {
                newObject[key] = value;
              }
            });
            return { ...newObject, key: newObject._id };
          });
          setListData(tmpData);
        }
        setIsLoadingPage(false);
      })
      .catch((err) => {
        setIsLoadingPage(false);
      });
    setTotalRecord(null);
  };

  useEffect(() => {
    loadDataCallBack();
  }, [loadDataCallBack]);

  useEffect(() => {
    let tmpHeaders = listHeaders.map((item, idx) => {
      return {
        ...item,
        ...getColumnSearchProps(
          item.ID,
          idx,
          item.name,
          searchList,
          item,
          searchText,
          searchedColumn,
          item.type
        ),
      };
    });
    setListHeaders(tmpHeaders);
  }, [searchList, searchText, searchedColumn]);

  function numberWithCommas(x, item) {
    if (x === 0) {
      return "0";
    } else
      x = x.toFixed(
        item?.decimal_separator === 0 ? 0 : item?.decimal_separator || 3
      );
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: data.related_object,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let hiddenArray1 = [];
          /*eslint-disable-next-line*/
          response.data.data.forEach((sections, idx) => {
            /*eslint-disable-next-line*/
            sections.fields.forEach((item, idx) => {
              if (item.type === "dynamic-field") {
                /*eslint-disable-next-line*/
                if (item.list_items)
                  /*eslint-disable-next-line*/
                  Object.entries(item.list_items).forEach(
                    ([key, value], idx) => {
                      hiddenArray1 = [...hiddenArray1, ...value];
                    }
                  );
              }
            });
          });

          let tempField = [];
          let tempFieldHidden = [];
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sections);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              if (hiddenArray1.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFields(tempFieldHidden);
          setHiddenArray(hiddenArray1);
        })
        .catch((err) => {});
    };
    if (data?.related_object) {
      checkToken();
    }
    // eslint-disable-next-line
  }, []);

  const getColumnSearchProps = (
    dataIndex,
    idx,
    name,
    searchList,
    item,
    searchText,
    searchedColumn,
    type
  ) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        {item.type === "select" || item.type === "dynamic-field" ? (
          <Select
            mode="multiple"
            placeholder={`Search ${name}`}
            value={searchList[dataIndex]}
            onChange={(e) => {
              let tempSearchList = { ...searchList };

              tempSearchList[dataIndex] = e.length !== 0 ? e : undefined;
              setSearchList(tempSearchList);
            }}
            onPressEnter={() => {
              handleSearch(confirm, dataIndex);
            }}
            style={{ marginBottom: 8, display: "block" }}
          >
            {item &&
              item.option &&
              item.option.map((ele, idx) => {
                return <Option value={ele.label}>{ele.label}</Option>;
              })}
          </Select>
        ) : item.type === "date" ||
          item.type === "datetime-local" ||
          item.ID === "created_date" ||
          item.ID === "modify_time" ? (
          <DatePicker
            style={{ display: "block", marginBottom: "8px" }}
            value={
              searchList[dataIndex] ? moment(searchList[dataIndex]) : undefined
            }
            onChange={(date, dateSting) => {
              let tempSearchList = { ...searchList };
              tempSearchList[dataIndex] = dateSting ? dateSting : "";

              setSearchList(tempSearchList);
            }}
            onPressEnter={() => {
              handleSearch(confirm, dataIndex);
            }}
          />
        ) : item.type === "number" ||
          (item.type === "formula" && item.formula_type === "dateToParts") ||
          (item.type === "formula" && item.formula_type === "dateDiff") ||
          (item.type === "formula" && item.formula_type === "add") ||
          (item.type === "formula" && item.formula_type === "subtract") ||
          (item.type === "formula" && item.formula_type === "multiply") ||
          (item.type === "formula" && item.formula_type === "devide") ||
          (item.type === "formula" && item.formula_type === "mod") ||
          (item.type === "formula" && item.formula_type === "max") ||
          (item.type === "formula" && item.formula_type === "min") ||
          (item.type === "formula" &&
            item.formula_type === "advanceExpression") ? (
          <InputNumber
            placeholder={`Search ${name}`}
            value={searchList[dataIndex]}
            onChange={(e) => {
              let tempSearchList = { ...searchList };
              console.log(e);
              tempSearchList[dataIndex] = e || e === 0 ? e : "";
              console.log({ ...tempSearchList });
              setSearchList({ ...tempSearchList });
            }}
            onPressEnter={() => {
              handleSearch(confirm, dataIndex);
            }}
            style={{ marginBottom: 8, display: "block", width: "100%" }}
          />
        ) : (
          <Input
            placeholder={`Search ${name}`}
            value={searchList[dataIndex]}
            onChange={(e) => {
              let tempSearchList = { ...searchList };

              tempSearchList[dataIndex] = e.target.value ? e.target.value : "";
              setSearchList({ ...tempSearchList });
            }}
            onPressEnter={() => {
              handleSearch(confirm, dataIndex);
            }}
            style={{ marginBottom: 8, display: "block" }}
          />
        )}
        <Space>
          <ButtonSearch
            // type="primary"
            // className="search-btn"
            onClick={() => {
              handleSearch(confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </ButtonSearch>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined
        style={{ color: searchList[dataIndex] ? "#1890ff" : undefined }}
      />
    ),

    render: (text, ...props) => {
      /* eslint-disable-next-line */
      const expression =
        /[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]+)?/gi;
      const regex = new RegExp(expression);
      if (text && typeof text === "object" && type === "file") {
        return text.map((ele, idx) => {
          if (ele !== null && ele.match(regex)) {
            let fileNew = ele.split("/");
            return (
              <TextComponent
                ellipsis={{ tooltip: fileNew[fileNew.length - 1] }}
                id={`ucrm_table_comp_${idx}`}
              >
                <a
                  style={{ display: "block" }}
                  href={
                    allObject?.find(
                      (object) =>
                        object?.object_id === objectId && object?.status
                    ) && ele?.split(BE_URL)[1]
                      ? `${FE_URL + ele?.split(BE_URL)[1]}`
                      : ele
                  }
                  target={"_blank"}
                  rel="noreferrer"
                >
                  {fileNew[fileNew.length - 1]}
                </a>
              </TextComponent>
            );
          } else {
            return (
              <div
                id={`ucrm_table_comp_${idx}`}
                style={{
                  background: "rgb(243 239 239)",
                  marginRight: "5px",
                  padding: "2px",
                  borderRadius: "5px",
                  float: "left",
                  marginBottom: "5px",
                }}
              >
                {ele}
              </div>
            );
          }
        });
      } else if (text && typeof text === "string" && type === "file") {
        let listFile = text.split(",");
        return listFile.map((item, idx) => {
          let splitFile = item.split("/");
          return (
            <TextComponent
              id={`ucrm_table_comp_${idx}`}
              ellipsis={{ tooltip: splitFile[splitFile.length - 1] }}
            >
              <a
                key={idx}
                style={{ display: "block" }}
                href={
                  allObject?.find(
                    (object) => object?.object_id === objectId && object?.status
                  ) && item?.split(BE_URL)[1]
                    ? `${FE_URL + item?.split(BE_URL)[1]}`
                    : item
                }
                target={"_blank"}
                rel="noreferrer"
              >
                {splitFile[splitFile.length - 1]}
              </a>
            </TextComponent>
          );
        });
      } else {
        return searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
            searchWords={[searchText?.toString()]}
            autoEscape
            textToHighlight={text === 0 || text ? text.toString() : ""}
          />
        ) : (
          <TextComponent
            id={`ucrm_table_comp_${idx}`}
            ellipsis={{ tooltip: text }}
            style={{
              width: "100%",
              textAlign: `${typeof text === "number" ? "right" : ""}`,
            }}
          >
            {typeof text === "number" ? numberWithCommas(text, item) : text}
          </TextComponent>
        );
      }
    },
  });

  const handleReset = async (clearFilters) => {
    const isTokenValid = await checkTokenExpiration();
    clearFilters();
    setSearchList({});
    setSearchText("");
    setSearcherColumn("");
    new Promise((resolve, reject) => {
      clearFilters();
      setSearchList({});
      setSearchText("");
      setSearcherColumn("");
      resolve("done");
    }).then((res) => {
      if (res === "done") {
        let searchData = [];

        let dataPost = filterID
          ? {
              id: data._id,
              record_id: record_id,
              pagination: false,
              current_page: 1,
              record_per_page: 50,
              first_record_id: null,
              last_record_id: null,
              custom_view_id: filterID,
              search_with: {
                meta: [],
                data: searchData,
              },
              // sort_by: sortBy,
            }
          : {
              id: data._id,
              record_id: record_id,
              pagination: false,
              current_page: 1,
              record_per_page: 50,
              first_record_id: null,
              last_record_id: null,
              search_with: {
                meta: [],
                data: searchData,
              },
              // sort_by: sortBy,
            };
        setIsLoadingPage(true);
        axios
          .post(
            BASE_URL_API + "consolidated-view/load-specific-component",
            dataPost,
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            if (Array.isArray(res.data.data)) {
              Notification("error", "You do not have right to see this data!");
              setRight("noAccepted");
            } else {
              setRight("accepted");
              let tmpHeaders = res.data.data.fields.map((item, idx) => {
                return {
                  ...item,
                  key: item.ID,
                  dataIndex: item.ID,
                  title: (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <span style={{ marginRight: "8px" }}>
                        {" "}
                        {item.name === "Owner" ? "Assign to" : item.name}
                      </span>
                    </div>
                  ),
                  ...getColumnSearchProps(
                    item.ID,
                    idx,
                    item.name,
                    {},
                    item,
                    "",
                    searchedColumn,
                    item.type
                  ),
                };
              });

              setListHeaders(tmpHeaders);

              let tmpData = res.data.data.record_data?.data.map((item, idx) => {
                let newObject = {};
                Object.entries(item).map(([key, value], index) => {
                  if (typeof value === "object" && value !== null) {
                    newObject[key] = value.value;
                  } else {
                    newObject[key] = value;
                  }
                });
                return { ...newObject, key: newObject._id };
              });
              setListData(tmpData);
            }
            setIsLoadingPage(false);
          })
          .catch((err) => {
            setIsLoadingPage(false);
          });
      }
    });

    cancel();
  };

  const handleSort = (sortTmp) => {
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    initData(currentPage, recordPerPage, searchData, sortTmp);
  };

  const handleSearch = async (confirm, dataIndex) => {
    const isTokenValid = await checkTokenExpiration();
    setCurrentPage(1);
    setRecordPerPage(50);
    console.log(searchList);
    let searchListTmp = { ...searchList };
    let searchData = [];
    let searchMeta = [];
    /* eslint-disable-next-line */
    Object.entries(searchListTmp).forEach(([key, value], index) => {
      if (value === 0 || value) {
        let newItem = {
          id_field: key,
          value: value,
        };

        if (
          key === "created_date" ||
          key === "created_by" ||
          key === "modify_time" ||
          key === "owner" ||
          key === "modify_by"
        ) {
          searchMeta.push(newItem);
        } else {
          searchData.push(newItem);
        }
      }
    });
    // loadDataCallBack();
    let dataPost = filterID
      ? {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: 1,
          record_per_page: 50,
          first_record_id: null,
          last_record_id: null,
          custom_view_id: filterID,
          search_with: {
            meta: searchMeta,
            data: searchData,
          },
          // sort_by: sortBy,
        }
      : {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: 1,
          record_per_page: 50,
          first_record_id: null,
          last_record_id: null,
          search_with: {
            meta: searchMeta,
            data: searchData,
          },
          // sort_by: sortBy,
        };
    setIsLoadingPage(true);
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        if (Array.isArray(res.data.data)) {
          Notification("error", "You do not have right to see this data!");
          setRight("noAccepted");
        } else {
          setRight("accepted");
          let tmpHeaders = res.data.data.fields.map((item, idx) => {
            return {
              ...item,
              key: item.ID,
              dataIndex: item.ID,
              title: (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <span style={{ marginRight: "8px" }}>
                    {" "}
                    {item.name === "Owner" ? "Assign to" : item.name}
                  </span>
                </div>
              ),
              ...getColumnSearchProps(
                item.ID,
                idx,
                item.name,
                searchList,
                item,
                searchList[dataIndex],
                dataIndex,
                item.type
              ),
            };
          });
          setListHeaders(tmpHeaders);
          let tmpData = [];
          if (Array.isArray(res.data.data.record_data?.data)) {
            tmpData = res.data.data.record_data?.data.map((item, idx) => {
              let newObject = {};
              Object.entries(item).map(([key, value], index) => {
                if (typeof value === "object" && value !== null) {
                  newObject[key] = value.value;
                } else {
                  newObject[key] = value;
                }
              });
              return { ...newObject, key: newObject._id };
            });
          }
          setListData(tmpData);
        }
        setIsLoadingPage(false);
      })
      .catch((err) => {
        setIsLoadingPage(false);
      });
    // initData(currentPage, recordPerPage, searchData, sortBy);
    setSearchText(searchList[dataIndex]);
    setSearcherColumn(dataIndex);
    confirm();
    setTotalRecord(null);
  };

  useEffect(() => {
    console.log("listData", listData);
  }, [listData]);

  const edit = async (record, allFields, type) => {
    setLoadingDetails(true);
    const isTokenValid = await checkTokenExpiration();
    //hihi
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: data.related_object,
          show_meta_fields: true,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        dispatch(loadListObjectFieldSuccess(res.data.data));
        setListObjectField(res.data.data);
        let newObj = {};
        res.data.data.length > 0 &&
          res.data.data[res.data.data.length - 1]["main_object"][
            "sections"
            /*eslint-disable-next-line*/
          ].map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((item, idx) => {
              newObj[item.ID] = { ...item };
              newObj[item.ID].name = "";
            });
          });

        setAllFields(newObj);
        let flagEdit = false;

        res.data.data.map((item, idx) => {
          if (Object.keys(item)[0] === "main_object") {
            flagEdit = item[Object.keys(item)[0]]?.edit_permission;
          }
        });
        if (flagEdit) {
          axios
            .get(
              BASE_URL_API +
                `load-record-data?id=${record._id}&object_id=${data.related_object}`,
              {
                headers: {
                  Authorization: isTokenValid,
                },
              }
            )
            .then((res) => {
              let newObj2 = {};
              /* eslint-disable-next-line */
              Object.entries(newObj).forEach(([key, value], index) => {
                if (value.type === "linkingobject") {
                  newObj2[key] = { ...res.data.data[key] };
                }
              });
              dispatch(loadRecordDataSuccess({ ...res.data.data }));
              setRecordDataNew({ ...res.data.data });
              dispatch(setLinkingFieldValue(newObj2));

              let tempObject = {};
              let newObj1 = {};
              /* eslint-disable-next-line */
              Object.entries(newObj).forEach(([key, value], index) => {
                if (value.type === "linkingobject") {
                  newObj1[key] = {
                    ...(res.data.data !== null && res.data.data[key]),
                  };
                }
              });

              /*eslint-disable-next-line*/
              Object.entries(res.data.data !== null && res.data.data).forEach(
                ([key, value], index) => {
                  // let tempObject = {};
                  if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "datetime-local" &&
                    value.value !== null
                  ) {
                    tempObject[key] = value ? moment(value.value) : null;
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "date" &&
                    value.value !== null
                  ) {
                    tempObject[key] = value ? moment(value.value) : null;
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "linkingobject"
                  ) {
                    tempObject[key] = {
                      ...res.data.data[key],
                    };
                  } else if (
                    newObj &&
                    newObj[key] &&
                    newObj[key].type === "lookup"
                  ) {
                    tempObject[key] = res.data.data[key];
                  } else if (typeof value === "object") {
                    tempObject[key] = value.value;
                  } else {
                    tempObject[key] = value;
                  }
                }
              );
              form.setFieldsValue({ ...tempObject, ...newObj1, ...newObj2 });
              if (type === "normal") {
                setOpenModalRecord(true);
              }
            })
            .catch((err) => console.log(err));
        } else {
          Notification("error", "Bạn không có quyền để chỉnh sửa bản ghi này!");
        }
      })
      .catch((err) => {});
    setLoadingDetails(false);
  };

  const tempEdit = (record, allFields) => {
    let tempObject = {};
    /*eslint-disable-next-line*/
    Object.entries(record !== null && record).forEach(([key, value], index) => {
      // let tempObject = {};
      if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "datetime-local" &&
        value !== null
      ) {
        tempObject[key] = value
          ? moment(value, "YYYY-MM-DD HH:mm:ss", true)
          : null;
      } else if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "date" &&
        value !== null
      ) {
        tempObject[key] = value ? moment(value, "YYYY-MM-DD") : null;
      } else if (typeof value === "object" && value !== null) {
        tempObject[key] = value;
      } else {
        tempObject[key] = value;
      }
    });

    // form.resetFields();
    form.setFieldsValue(tempObject);
    setOpenModalRecord(true);
  };

  useEffect(() => {
    form.setFieldsValue(linkingFieldValue);
    // eslint-disable-next-line
  }, [linkingFieldValue]);

  useEffect(() => {
    setRecordID(record_id);
  }, [record_id]);

  const cancel = () => {
    setEditingKey("");
    form.resetFields();
  };

  useEffect(() => {
    if (openModalRecord && recordID === "") {
      dispatch(
        loadLinkingFieldValue({
          object_related: objectId,
          record_id: record_id,
          object_create_record: data.related_object,
        })
      );
    }
    // eslint-disable-next-line
  }, [objectId, data, openModalRecord, recordID]);

  useEffect(() => {
    if (isModalVisible) {
      dispatch(
        loadRelatedObject({
          object_id: objectId,
        })
      );
      dispatch(
        loadSourceObjects({
          object_id: objectId,
        })
      );
      dispatch(
        loadTargetObjects({
          object_id: objectId,
        })
      );
    }
  }, [objectId, isModalVisible, dispatch]);

  // const loadData = () => {
  //   let searchData = [];

  //   /* eslint-disable-next-line */
  //   Object.entries(searchList).forEach(([key, value], index) => {
  //     if (value) {
  //       let newItem = {
  //         id_field: key,
  //         value: value,
  //       };
  //       searchData.push(newItem);
  //     }
  //   });

  //   initData(currentPage, recordPerPage, searchData, sortBy);
  // };

  useEffect(() => {
    dispatch(
      loadUserDynamicButton({
        object_id: objectId,
        record_id: record_id,
      })
    );
  }, [dispatch, objectId, record_id]);

  const initData = async (
    currentPage,
    recordPerPage,
    searchData,
    sortBy,
    filterID
  ) => {
    if (!currentPage && !recordPerPage) {
      currentPage = 1;
      recordPerPage = 10;
    }
    setLoading(true);
    setIsLoadingPage(true);
    const isTokenValid = await checkTokenExpiration();
    if (record_id)
      axios
        .post(
          BASE_URL_API + "consolidated-view/load-specific-component",
          {
            id: data._id,
            record_id: record_id,
            pagination: false,
            current_page: 1,
            record_per_page: 10,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: searchData,
            },
            // sort_by: sortBy,
            custom_view_id: filterID,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setLoading(false);
          //columns
          let comlumnsTemp = [];
          // eslint-disable-next-line
          res.data.data.fields.map((item, idx) => {
            let newItem = {
              title: (
                <div style={{ display: "flex", alignItems: "center" }}>
                  <span style={{ marginRight: "8px" }}>
                    {" "}
                    {item.name === "Owner" ? "Assign to" : item.name}
                  </span>
                </div>
              ),
              dataIndex: item.ID,
              key: item.ID,
              editable: true,
              ...getColumnSearchProps(
                item.ID,
                idx,
                item.name,
                searchList,
                item,
                searchText,
                searchedColumn,
                item.type
              ),
            };
            comlumnsTemp.push(newItem);
          });
          //dataSource
          let dataSourceTmp = [];
          // eslint-disable-next-line
          res.data.data.record_data.data.map((item, idx) => {
            let newItem = {};
            newItem["key"] = item._id;
            /* eslint-disable-next-line */
            Object.entries(item).forEach(([key, value], idx) => {
              if (typeof value === "object" && value !== null) {
                newItem[key] = value.value;
              } else {
                newItem[key] = value;
              }
            });
            dataSourceTmp.push(newItem);
          });
          setDataSource(dataSourceTmp);
          setColumns(comlumnsTemp);
          setIsLoadingPage(false);
        })
        .catch((err) => {
          setLoading(false);
          setIsLoadingPage(false);
        });
  };

  useEffect(() => {
    setWidth(
      document.getElementById(data._id) &&
        document.getElementById(data._id).clientWidth
    );
    // eslint-disable-next-line
  }, [
    // eslint-disable-next-line
    document.getElementById(data._id) &&
      document.getElementById(data._id).clientWidth,
  ]);

  const [, setWidth] = useState(0);

  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      if (selectedRowKeys.length > 0) {
        setSelectedRowKeys(selectedRowKeys);
        setSelectedRecord({
          ...selectedRows[0],
        });
        form.resetFields();
        setRecordID(selectedRows[0]._id);
        if (data?.with_details) {
          edit(selectedRows[0], allFields, "details");
        }

        setDrag(false);
      } else {
        setSelectedRowKeys("");
        setSelectedRecord({});
        form.resetFields();
        setRecordID("");

        setDrag(false);
      }
    },
  };

  const onCancel = () => {
    setSelectedRowKeys("");
    setSelectedRecord({});
    form.resetFields();
    setRecordID("");
    setDrag(false);
  };

  useEffect(() => {
    setNext(
      listData.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + listData.length
    );
    /* eslint-disable-next-line */
  }, [listData]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
    /* eslint-disable-next-line */
  }, [currentPage]);

  const [loadPagi, setLoadPagi] = useState(false);
  const handleGetTotalRecord = async () => {
    const isTokenValid = await checkTokenExpiration();
    let searchData = [];
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    let dataPost = filterID
      ? {
          id: data._id,
          record_id: record_id,
          pagination: true,
          current_page: currentPage,
          record_per_page: recordPerPage,
          custom_view_id: filterID,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        }
      : {
          id: data._id,
          record_id: record_id,
          pagination: true,
          current_page: currentPage,
          record_per_page: recordPerPage,
          search_with: {
            meta: [],
            data: searchData,
          },
          // sort_by: sortBy,
        };
    setLoadPagi(true);
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        dataPost,
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setLoadPagi(false);
        setTotalRecord(res?.data?.data?.total_record);
      })
      .catch((err) => {
        setLoadPagi(false);
      });
  };

  return (
    <Wrapper
      id={data._id}
      style={{
        paddingTop: `${
          window.location.pathname.includes("list-view-with-details")
            ? ""
            : "36px"
        }`,
      }}
    >
      <div
        style={{
          minHeight: "36px",
          maxHeight: "fit-content",
          background: "#fff",
          display: "flex",
          alignItems: "center",
          fontStyle: "normal",
          fontWeight: "500",
          fontSize: "17px",
          lineHeight: "26px",
          color: "#252424",
          paddingLeft: "15px",
          width: "100%",
          justifyContent: "space-between",
          paddingRight: "15px",
          position: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : "fixed"
          }`,
          top: 0,
          left: 0,
          zIndex: 9,
          boxShadow: "inset 0px -1px 0px #F0F0F0",
          marginBottom: window.location.pathname.includes(
            "list-view-with-details"
          )
            ? "0"
            : "16px",
        }}
      >
        <Row
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Col>
            {data.name}{" "}
            {right === "noAccepted" ? (
              ""
            ) : (
              <Tooltip title="Add new view">
                <button
                  className="button-filter"
                  style={{
                    marginLeft: "10px",
                    textAlign: "center",
                  }}
                  onClick={() => {
                    setOpenCustomView(true);
                  }}
                >
                  <img
                    alt=""
                    src={fiterIcon}
                    style={{ width: "12px", textAlign: "center" }}
                  />
                </button>
              </Tooltip>
            )}
          </Col>
          {/* <Col span={12}> </Col> */}
          <Col style={{ display: "flex", justifyContent: "flex-end" }}>
            {/* <CustomButton
              shape="circle"
              onClick={() => {
                setOpenCustomView(true);
              }}
            >
              -
            </CustomButton> */}
            {right === "noAccepted" ? (
              ""
            ) : (
              <>
                <Tooltip title="Reload">
                  <CustomButtonReload
                    shape="circle"
                    // disabled={
                    //   Object.keys(selectedRecord).length === 0 ? true : false
                    // }
                    onClick={() => {
                      // window.open(
                      //   BASENAME +
                      //     `consolidated-view/${data.related_object}/${selectedRecord._id}`
                      // );
                      loadDataCallBack();
                    }}
                  >
                    <img alt="" src={Refresh} style={{ width: "18px" }} />
                  </CustomButtonReload>
                </Tooltip>
                <Tooltip title="Consolidated view">
                  <CustomButton
                    shape="circle"
                    disabled={
                      Object.keys(selectedRecord).length === 0 ? true : false
                    }
                    onClick={() => {
                      window.open(
                        BASENAME +
                          `consolidated-view/${data.related_object}/${selectedRecord._id}`
                      );
                    }}
                  >
                    <img alt="" src={Details} style={{ width: "18px" }} />
                  </CustomButton>
                </Tooltip>
                <Tooltip title="Edit record">
                  <CustomButton
                    shape="circle"
                    onClick={() => {
                      form.resetFields();
                      // dispatch(
                      //   loadListObjectField({
                      //     api_version: "2",
                      //     object_id: data.related_object,
                      //     show_meta_fields: true,
                      //   })
                      // );
                      // dispatch(
                      //   loadRecordData({
                      //     id: selectedRecord._id,
                      //     object_id: data.related_object,
                      //   })
                      // );
                      setRecordID(selectedRecord._id);
                      edit(selectedRecord, allFields, "normal");
                      setDrag(false);
                    }}
                    style={{ width: "18px" }}
                    disabled={
                      Object.keys(selectedRecord).length === 0 ||
                      (selectedRowKeys && data?.with_details)
                        ? true
                        : false
                    }
                  >
                    <img alt="" src={editImg} />
                  </CustomButton>
                </Tooltip>
                <Tooltip title="Delete record">
                  <CustomButton
                    shape="circle"
                    onClick={async () => {
                      setDrag(false);
                      const isTokenValid = await checkTokenExpiration();
                      axios
                        .post(
                          BASE_URL_API + "object/objects-fields-permission",
                          {
                            api_version: "2",
                            object_id: data.related_object,
                            show_meta_fields: true,
                          },
                          {
                            headers: {
                              Authorization: isTokenValid,
                            },
                          }
                        )
                        .then((res) => {
                          let flagEdit = false;

                          res.data.data.map((item, idx) => {
                            if (Object.keys(item)[0] === "main_object") {
                              flagEdit =
                                item[Object.keys(item)[0]]?.delete_permission;
                            } else return null;
                          });
                          if (flagEdit) {
                            setOpenDelete(true);
                            dispatch(
                              loadRecordData({
                                id: selectedRecord.key,
                                object_id: data.related_object,
                              })
                            );
                            setRecordID(selectedRecord.key);
                          } else {
                            Notification(
                              "error",
                              "Bạn không có quyền để xóa bản bản ghi này!"
                            );
                          }
                        })
                        .catch(() => {});
                    }}
                    disabled={
                      Object.keys(selectedRecord).length === 0 ? true : false
                    }
                  >
                    <img alt="" src={deleteImg} style={{ width: "17px" }} />
                  </CustomButton>
                </Tooltip>
                <Tooltip title="Add new record">
                  <CustomButton
                    shape="circle"
                    onClick={() => {
                      setRecordID("");
                      setSelectedRecord({});
                      setSelectedRowKeys("");
                      dispatch(
                        loadListObjectField({
                          api_version: "2",
                          object_id: data.related_object,
                          show_meta_fields: true,
                        })
                      );
                      setRecordID("");
                      setOpenModalRecord(true);
                    }}
                    disabled={selectedRowKeys}
                  >
                    +
                  </CustomButton>
                </Tooltip>
              </>
            )}
            {right === "noAccepted" ? (
              ""
            ) : (
              <NewCustomPagination>
                {isLoadingPagi ? (
                  <Spin />
                ) : (
                  <>
                    <div className="total-record">
                      {next === 0 ? 0 : prev} - {next}{" "}
                      {loadPagi ? (
                        <Spin style={{ marginLeft: "10px" }} />
                      ) : totalRecord === null ? (
                        <div
                          className="reload-pagi"
                          onClick={() => {
                            handleGetTotalRecord();
                          }}
                        >
                          <img alt="" src={Reload} />
                        </div>
                      ) : (
                        `of ${totalRecord ? totalRecord : 0} records`
                      )}
                    </div>

                    <div
                      className="left-pagi"
                      style={{
                        pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                        cursor: `${
                          currentPage === 1 ? "not-allowed" : "pointer"
                        }`,
                        opacity: `${currentPage === 1 ? 0.5 : 1}`,
                      }}
                      onClick={() => {
                        let currentPageTemp = currentPage;
                        currentPageTemp = currentPageTemp - 1;
                        setCurrentPage(currentPageTemp);
                        setFirstID(
                          listData.length > 0 ? listData[0]._id : null
                        );
                        setLastID(null);
                      }}
                    >
                      <img alt="" src={LeftPagi} />
                    </div>
                    <div
                      className="right-pagi"
                      style={{
                        pointerEvents: `${
                          listData.length < recordPerPage ? "none" : ""
                        }`,
                        cursor: `${
                          listData.length < recordPerPage
                            ? "not-allowed"
                            : "pointer"
                        }`,
                        opacity: `${listData.length < recordPerPage ? 0.5 : 1}`,
                      }}
                      onClick={() => {
                        let currentPageTemp = currentPage;
                        currentPageTemp = currentPageTemp + 1;
                        setCurrentPage(currentPageTemp);
                        setLastID(
                          listData.length > 0
                            ? listData[listData.length - 1]._id
                            : null
                        );
                        setFirstID(null);
                      }}
                    >
                      <img alt="" src={RightPagi} />
                    </div>
                  </>
                )}
              </NewCustomPagination>
            )}
            {editProp ? (
              <Dropdown
                trigger="click"
                overlay={
                  <Menu>
                    {window.location.pathname.includes(
                      "list-view-with-details"
                    ) ? (
                      ""
                    ) : (
                      <Menu.Item
                        onClick={() => {
                          dispatch(
                            loadListObjectField({
                              api_version: "2",
                              object_id: data.related_object,
                              show_meta_fields: true,
                            })
                          );
                          setIsModalVisible(true);
                          setComponentID(data._id);
                          setDrag(false);
                        }}
                      >
                        Edit
                      </Menu.Item>
                    )}

                    {checkRule("delete") ? (
                      <Menu.Item
                        onClick={() => {
                          setDrag(false);
                          onRemoveItem(JSON.stringify(data));
                        }}
                      >
                        Remove
                      </Menu.Item>
                    ) : (
                      ""
                    )}
                  </Menu>
                }
              >
                <Button style={{ marginLeft: "8px" }}>
                  <EllipsisOutlined />
                </Button>
              </Dropdown>
            ) : (
              ""
            )}
          </Col>
        </Row>
      </div>
      {loading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignContent: "center",
            marginTop: "10%",
            flexDirection: "column",
          }}
        >
          <LoadingOutlined
            style={{ fontSize: 60, color: defaultBrandName.theme_color }}
            spin
          />
          <div style={{ textAlign: "center", marginTop: "20px" }}>
            Loading. . .{" "}
          </div>
        </div>
      ) : (
        <>
          <div
            style={{
              height: "8px",
              background: "#fff",
              zIndex: 3,
              position: "sticky",
              top: 0,
              display: window.location.pathname.includes(
                "list-view-with-details"
              )
                ? "none"
                : "",
            }}
          />

          <div
            style={{
              height: "calc(100% - 8px)",
              width: "100%",
              padding: "8px",
              paddingTop: "0",
              paddingLeft: `${
                window.location.pathname.includes("list-view-with-details")
                  ? "0"
                  : "8px"
              }`,
              paddingRight: `${
                window.location.pathname.includes("list-view-with-details")
                  ? "0"
                  : "8px"
              }`,
            }}
          >
            {/* <Form
              form={form}
              component={false}
              onValuesChange={(value, values) => {
                setFormValue(values);
              }}
            > */}
            {right === "noAccepted" ? (
              "You do not have right to see this data!"
            ) : data?.with_details ? (
              <div
                style={{
                  display: "flex",
                  width: "100%",
                  height: "100%",
                }}
              >
                <div
                  style={{
                    width: "30%",
                    overflowX: "auto",
                    borderRight: "1px solid #ececec",
                  }}
                  ref={divRef}
                >
                  <Table
                    rowSelection={{
                      type: "checkbox",
                      selectedRowKeys: selectedRowKeys,
                      onChange: rowSelection.onChange,
                      hideSelectAll: true,
                      getCheckboxProps: (record) => ({
                        disabled:
                          selectedRowKeys.length === 1 &&
                          !selectedRowKeys.includes(record.key),
                      }),
                    }}
                    onRow={(record) => {
                      return {
                        onClick: () => {
                          if (selectedRowKeys[0] !== record._id) {
                            setSelectedRowKeys([record._id]);
                            setSelectedRecord(record);
                            form.resetFields();
                            setRecordID(record._id);
                            if (data?.with_details) {
                              edit(record, allFields, "details");
                            }

                            setDrag(false);
                          }
                        },
                        onDoubleClick: () => {
                          window.open(
                            BASENAME +
                              `consolidated-view/${data.related_object}/${record._id}`
                          );
                        },
                      };
                    }}
                    rowClassName="editable-row"
                    // onRow={onRow}
                    dataSource={listData}
                    columns={listHeaders}
                    pagination={false}
                  />
                </div>
                <div
                  style={{
                    // width: `calc(100% - ${
                    //   localStorage.getItem("crm_width") || "30%"
                    // })`,
                    width: "70%",
                    overflowY: "auto",
                    height: "100%",
                  }}
                >
                  <WithDetails
                    objectId={data.related_object}
                    form={form}
                    recordID={recordID}
                    setRecordID={setRecordID}
                    recordPerPage={recordPerPage}
                    currentPage={currentPage}
                    loadingDetails={loadingDetails}
                    selectedRowKeys={selectedRowKeys}
                    onCancel={onCancel}
                    leftWidth={leftWidth}
                    recordData={recordDataNew}
                    hiddenArray={hiddenArray}
                    setHiddenArray={setHiddenArray}
                    fields={fields}
                    setFields={setFields}
                    searchList={searchList}
                    loadData={loadDataNew}
                    listObjectField={listObjectField}
                  />
                </div>
              </div>
            ) : (
              <Table
                rowSelection={{
                  type: "checkbox",
                  selectedRowKeys: selectedRowKeys,
                  onChange: rowSelection.onChange,
                  hideSelectAll: true,
                  getCheckboxProps: (record) => ({
                    disabled:
                      selectedRowKeys.length === 1 &&
                      !selectedRowKeys.includes(record.key),
                  }),
                }}
                onRow={(record) => {
                  return {
                    onClick: () => {
                      if (selectedRowKeys[0] !== record._id) {
                        setSelectedRowKeys([record._id]);
                        setSelectedRecord(record);
                        form.resetFields();
                        setRecordID(record._id);
                        if (data?.with_details) {
                          edit(record, allFields, "details");
                        }

                        setDrag(false);
                      }
                    },
                    onDoubleClick: () => {
                      window.open(
                        BASENAME +
                          `consolidated-view/${data.related_object}/${record._id}`
                      );
                    },
                  };
                }}
                rowClassName="editable-row"
                // onRow={onRow}
                dataSource={listData}
                columns={listHeaders}
                pagination={false}
              />
            )}
            {/* </Form> */}
          </div>
        </>
      )}

      <ModalRecord
        open={openModalRecord}
        setOpen={setOpenModalRecord}
        initData={loadDataCallBack}
        recordPerPage={recordPerPage}
        currentPage={currentPage}
        form={form}
        recordID={recordID}
        setRecordID={setRecordID}
        objectId={data.related_object}
        fields={fields}
        sortBy={sortBy}
        loadData={loadDataNew}
      />

      <ModalDelete
        openConfirm={openDelete}
        setOpenConfirm={setOpenDelete}
        title={"bản ghi này"}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deleteRecord}
        dataDelete={{
          data: {
            id: recordID,
            object_id: data.related_object,
          },
          load: {
            object_id: data.related_object,
            current_page: 1,
            record_per_page: 10,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
          type: window.location.pathname.includes("list-view-with-details")
            ? "reload"
            : "",
        }}
        loadData={loadDataCallBack}
        setShowModalDelete={() => {}}
      />

      <ModalComponent
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        selectedObject={objectId}
        dataItem={data}
        componentID={componentID}
        setComponentID={setComponentID}
        setDataItem={setDataItem}
        initData={loadDataNew}
      />

      <CustomView
        visible={openCustomView}
        setVisible={setOpenCustomView}
        objectId={data.related_object}
        mode={"consolidated-view"}
        filterID={filterID}
        setFilterID={setFilterID}
      />

      <ModalDuplicate />
    </Wrapper>
  );
};

export default TableComponent;

const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  .ant-checkbox-checked {
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
  /* .left {
    resize: horizontal;
    overflow: auto;
    width: 30%;
  } */
  &::-webkit-scrollbar {
    height: 8px !important;
  }
  .search-btn {
    background-color: ${(props) => props.theme.main}!important;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
  .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:active {
    border: none;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    text-align: center;

    /* Character/Color text main */

    color: #2c2c2c;
    background-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    background-color: ${(props) => props.theme.main};
  }

  .btn {
    position: relative;
    /* transition: all 200ms cubic-bezier(0.77, 0, 0.175, 1); */
    transition: all 0.5s;
    cursor: pointer;
    border-radius: 2px;
  }

  .btn:before,
  .btn:after {
    content: "";
    position: absolute;
    /* transition: inherit; */
    z-index: -1;
  }
  table {
    width: max-content;
    min-width: 100%;
    td {
      padding: 8px;
      max-width: 100px;
    }
  }

  .ant-table-thead {
    position: sticky;
    /* top: ${(props) =>
      window.location.pathname.includes("list-view-with-details")
        ? "0"
        : "8px"}; */
    top: 0px;
    z-index: 3;
    .ant-table-column-title {
      font-size: 16px;
    }
    th {
      padding: 8px;
    }
  }

  .button-filter {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    margin-right: 8px;
    border: 1px solid ${(props) => props.theme.main};
    :hover {
      background: ${(props) => props.theme.main};
      color: ${(props) => props.theme.white};
      border: 1px solid ${(props) => props.theme.main};
      cursor: pointer;
    }
  }
  .ant-table-tbody > tr > td {
    font-size: 16px;
    border-top: none;
    border-left: none;
    border-right: none;
  }
`;
const CustomPagination = styled(Pagination)`
  display: flex;
  justify-content: center;
  align-items: center;
  .ant-pagination-simple-pager {
    display: none;
  }
`;

const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const CustomButtonReload = styled(Button)`
  background: ${(props) => props.theme.white};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;

const ButtonSearch = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  :active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  :focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  :hover {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;
