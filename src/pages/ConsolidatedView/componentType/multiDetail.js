import { SettingOutlined } from "@ant-design/icons";
import {
  Button,
  Col,
  Dropdown,
  Form,
  Menu,
  Row,
  Select,
  Spin,
  Tooltip,
  Typography,
} from "antd";
import Call from "assets/icons/common/call.svg";
import ImgConfirm from "assets/icons/common/confirm.png";
import noDataImg from "assets/icons/common/nodata.png";
import Refresh from "assets/icons/common/refresh.png";
import axios from "axios";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, FE_URL, BE_URL } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";
import _ from "lodash";
import moment from "moment";
import ModalComponent from "pages/ConsolidatedViewSettings/ComponentSettings/modalComponent";
import Date from "pages/Objects/modal/fieldsType/date";
import Datetime from "pages/Objects/modal/fieldsType/dateTime";
import DynamicField from "pages/ConsolidatedView/dynamicField";
import Email from "pages/Objects/modal/fieldsType/email";
import File from "pages/Objects/modal/fieldsType/file";
import FormulaField from "pages/Objects/modal/fieldsType/formulaField";
import IDComp from "pages/Objects/modal/fieldsType/ID";
import LinkingObject from "pages/Objects/modal/fieldsType/linkingObject";
import Lookup from "pages/Objects/modal/fieldsType/lookup";
import Number from "pages/Objects/modal/fieldsType/number";
import SelectType from "pages/Objects/modal/fieldsType/select";
import Text from "pages/Objects/modal/fieldsType/text";
import Textarea from "pages/Objects/modal/fieldsType/textarea";
import User from "pages/Objects/modal/fieldsType/user";
import React, { useCallback, useEffect, useState } from "react";
import Linkify from "react-linkify";
import { useDispatch, useSelector } from "react-redux";
import { getFieldsMappingCallCenter, makeCall } from "redux/slices/callCenter";
import {
  loadUserDynamicButton,
  runDynamicButton,
  runDynamicButtonResult,
} from "redux/slices/dynamicButton";
import { loadRelatedObject } from "redux/slices/fieldsManagement";
import {
  loadListObjectField,
  loadListObjectFieldSuccess,
  loadRecordData,
  loadRecordDataSuccess,
  setLinkingFieldValue,
  updateRecord,
  loadFormCreate,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalRecord from "../modalRecord";
import ModalRunDynamicButton from "components/Modal/ModalRunDynamicButton";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
const { Text: TextComponent } = Typography;
const { Option } = Select;
const Details = (props) => {
  const {
    recordID: record_id,
    data,
    objectId,
    onRemoveItem,
    setEditDetail,
    editDetail,
    edit: editProps,
    setDrag,
  } = props;

  const [loading, setLoading] = useState(false);
  const { allObject } = useSelector((state) => state.tenantsReducer);

  const { userDynamicButton, isRunSuccess, isReload } = useSelector(
    (state) => state.dynamicButtonReducer
  );
  const { fieldsMappingCallCenter } = useSelector(
    (state) => state.callCenterReducer
  );
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const dispatch = useDispatch();
  const [visibleModalRDB, $visibleModalRDB] = useState(false);

  const [details, setDetails] = useState({});
  const [openModalRecord, setOpenModalRecord] = useState(false);
  const [form] = Form.useForm();
  const [recordID, setRecordID] = useState("");
  const [fields, setFields] = useState([]);
  // const [recordData, setRecordData] = useState({});
  const [openEditLinkingRecord, setOpenEditLinkingRecord] = useState(false);
  const [recordLinkingID, setRecordLinkingID] = useState("");
  // const [fieldsLinking, setFieldsLinking] = useState([]);
  const [allFields, setAllFields] = useState({});
  const [componentID, setComponentID] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [, setDataItem] = useState({});
  const [objectLinking, setObjectLinking] = useState("");
  const [dataConfirm, setDataConfirm] = useState({});
  const [showConfirm, setShowConfirm] = useState(false);
  const [showFields, setShowFields] = useState({});
  const [cols, setCols] = useState([]);
  const [dataDetail, setDataDetail] = useState({});
  const [initRecordData, $initRecordData] = useState({});
  const [loadingData, setLoadingData] = useState(false);
  const [loadingDataObjectField, setLoadingDataObjectField] = useState(false);

  const {
    linkingFieldValue,
    recordData,
    listObjectField,
    userAssignTo,
    // hiddenDynamic,
  } = useSelector(
    /*eslint-disable-next-line*/
    (state) => state.objectsReducer
  );

  const [hiddenArray, setHiddenArray] = useState([]);
  const [hiddenDynamic, setHiddenDynamic] = useState([]);

  useEffect(() => {
    if (
      Object.entries(details).length > 0 &&
      Object.entries(details && details.record_data).length > 0
    ) {
      form.setFieldsValue({
        assignTo: details && details.record_data?.owner,
      });
      setFormValue(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (details && details.record_data[field.ID]?.value) {
              /*eslint-disable-next-line*/
              field.list_items[
                details && details.record_data[field.ID]?.value
              ].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      setHiddenArray(tmpHidden);
    }
    /* eslint-disable-next-line */
  }, [details, fields, recordID]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    if (listObjectField.length > 0) {
      let objectFields = {};
      /*eslint-disable-next-line*/
      listObjectField.map((object, idx) => {
        if (listObjectField.length - 1 === idx) {
          objectFields["main_object"] = [];
          /*eslint-disable-next-line*/
          object["main_object"].sections.map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((field, index) => {
              objectFields["main_object"].push(field);
            });
          });
        } else {
          /*eslint-disable-next-line*/
          Object.entries(object).forEach(([key, value], idx) => {
            objectFields[object[key].object_name] = [];
            /*eslint-disable-next-line*/
            return object[key].sections.forEach((section, idx) => {
              /*eslint-disable-next-line*/
              return section.fields.forEach((field, index) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false
                ) {
                  objectFields[object[key].object_name].push(field);
                } else if (field.permission_hidden === undefined) {
                  objectFields[object[key].object_name].push(field);
                }
              });
            });
          });
        }
      });

      // setFields(objectFields);
      let newObj = {};
      objectFields["main_object"].forEach((item, idx) => {
        newObj[item.ID] = { ...item };
        newObj[item.ID].name = "";
      });
      setAllFields(newObj);
    }
  }, [listObjectField]);
  const [fieldObject, setFieldObject] = useState({});

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: data.related_object,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let hiddenArray1 = [];
          /*eslint-disable-next-line*/
          response.data.data.forEach((sections, idx) => {
            /*eslint-disable-next-line*/
            sections.fields.forEach((item, idx) => {
              if (item.type === "dynamic-field") {
                /*eslint-disable-next-line*/
                if (item.list_items)
                  /*eslint-disable-next-line*/
                  Object.entries(item.list_items).forEach(
                    ([key, value], idx) => {
                      hiddenArray1 = [...hiddenArray1, ...value];
                    }
                  );
              }
            });
          });

          let tempField = [];
          let tempFieldHidden = [];
          let newObj = {};
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sectiosetFieldObjectns);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              newObj[item.ID] = { ...item };
              if (hiddenArray1.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });
          setFieldObject({ ...newObj });
          setFields(tempFieldHidden);
          setHiddenArray(hiddenArray1);
          setHiddenDynamic(hiddenArray1);
        })
        .catch((err) => {});
    };
    if (data?.related_object) {
      checkToken();
    }
    /*eslint-disable-next-line*/
  }, []);

  const initData = async () => {
    setLoading(true);
    const isTokenValid = await checkTokenExpiration();
    if (record_id)
      axios
        .post(
          BASE_URL_API + "consolidated-view/load-specific-component",
          {
            id: data._id,
            record_id: record_id,
            pagination: false,
            current_page: 1,
            record_per_page: 20,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setDetails(res.data.data);
          setDataDetail(res.data.data.record_data);
          $initRecordData(res.data.data.record_data);
          setCols(res.data.data.show_field_multi_details);
          let newObject = {};
          res.data.data.fields.map((item, idx) => {
            newObject[item.ID] = { ...item };
            return null;
          });
          setShowFields(newObject);
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
        });
  };

  const loadData = useCallback(async () => {
    const isTokenValid = await checkTokenExpiration();
    axios
      .post(
        BASE_URL_API + "consolidated-view/load-specific-component",
        {
          id: data._id,
          record_id: record_id,
          pagination: false,
          current_page: 1,
          record_per_page: 20,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((res) => {
        setDetails(res.data.data);
        setDataDetail(res.data.data.record_data);
        $initRecordData(res.data.data.record_data);
        setCols(res.data.data.show_field_multi_details);
        let newObject = {};
        res.data.data.fields.map((item, idx) => {
          newObject[item.ID] = { ...item };
          return null;
        });
        setShowFields(newObject);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
      });
  }, [record_id, data._id]);

  useEffect(() => {
    setLoading(true);
    loadData();
    /*eslint-disable-next-line*/
  }, [loadData]);

  // useEffect(() => {
  //   let newObj = {};
  //   /* eslint-disable-next-line */
  //   Object.entries(allFields).forEach(([key, value], index) => {
  //     if (value.type === "linkingobject") {
  //       newObj[key] = { ...recordData[key] };
  //     }
  //   });
  //   dispatch(setLinkingFieldValue(newObj));
  //   /* eslint-disable-next-line */
  // }, [recordData]);

  const edit = async (record) => {
    const isTokenValid = await checkTokenExpiration();
    setLoadingDataObjectField(true);
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: data.related_object,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((result) => {
        setLoadingDataObjectField(false);
        let objectFields = {};
        dispatch(loadListObjectFieldSuccess(result.data.data));

        /*eslint-disable-next-line*/
        result.data.data.map((object, idx) => {
          if (result.data.data.length - 1 === idx) {
            objectFields["main_object"] = [];
            /*eslint-disable-next-line*/
            object["main_object"].sections.map((section, idx) => {
              /*eslint-disable-next-line*/
              section.fields.map((field, index) => {
                objectFields["main_object"].push(field);
              });
            });
          } else {
            /*eslint-disable-next-line*/
            Object.entries(object).forEach(([key, value], idx) => {
              objectFields[object[key].object_name] = [];
              /*eslint-disable-next-line*/
              return object[key].sections.forEach((section, idx) => {
                /*eslint-disable-next-line*/
                return section.fields.forEach((field, index) => {
                  if (
                    field.hidden === false &&
                    field.permission_hidden === false
                  ) {
                    objectFields[object[key].object_name].push(field);
                  } else if (field.permission_hidden === undefined) {
                    objectFields[object[key].object_name].push(field);
                  }
                });
              });
            });
          }
        });

        // setFields(objectFields);
        let newObj = {};
        objectFields["main_object"].forEach((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });

        setAllFields(newObj);
        setLoadingData(true);
        axios
          .get(
            BASE_URL_API +
              `load-record-data?id=${record._id}&object_id=${data.related_object}`,
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            setLoadingData(false);
            let newObj2 = {};
            /* eslint-disable-next-line */
            Object.entries(allFields).forEach(([key, value], index) => {
              if (value.type === "linkingobject") {
                newObj2[key] = { ...res.data.data[key] };
              }
            });
            dispatch(loadRecordDataSuccess({ ...res.data.data }));
            dispatch(setLinkingFieldValue(newObj2));

            let tempObject = {};
            let newObj1 = {};
            /* eslint-disable-next-line */
            Object.entries(newObj).forEach(([key, value], index) => {
              if (value.type === "linkingobject") {
                newObj1[key] = {
                  ...(res.data.data !== null && res.data.data[key]),
                };
              }
            });

            /*eslint-disable-next-line*/
            Object.entries(res.data.data !== null && res.data.data).forEach(
              ([key, value], index) => {
                // let tempObject = {};
                if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "datetime-local" &&
                  value.value !== null
                ) {
                  tempObject[key] = value ? moment(value.value) : null;
                } else if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "date" &&
                  value.value !== null
                ) {
                  tempObject[key] = value ? moment(value.value) : null;
                } else if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "linkingobject"
                ) {
                  tempObject[key] = {
                    ...res.data.data[key],
                  };
                } else if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "lookup"
                ) {
                  tempObject[key] = res.data.data[key];
                } else if (typeof value === "object") {
                  tempObject[key] = value.value;
                } else {
                  tempObject[key] = value;
                }
              }
            );
            form.setFieldsValue({ ...tempObject, ...newObj1, ...newObj2 });
            setOpenModalRecord(true);
          })
          .catch((err) => setLoadingData(false));
      })
      .catch((err) => setLoadingDataObjectField(false));
  };

  const editLinking = async (id, objectId) => {
    const isTokenValid = await checkTokenExpiration();
    setLoadingDataObjectField(true);
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: objectId,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((result) => {
        setLoadingDataObjectField(false);
        let objectFields = {};
        dispatch(loadListObjectFieldSuccess(result.data.data));
        /*eslint-disable-next-line*/
        result.data.data.map((object, idx) => {
          if (result.data.data.length - 1 === idx) {
            objectFields["main_object"] = [];
            /*eslint-disable-next-line*/
            object["main_object"].sections.map((section, idx) => {
              /*eslint-disable-next-line*/
              section.fields.map((field, index) => {
                objectFields["main_object"].push(field);
              });
            });
          } else {
            /*eslint-disable-next-line*/
            Object.entries(object).forEach(([key, value], idx) => {
              objectFields[object[key].object_name] = [];
              /*eslint-disable-next-line*/
              return object[key].sections.forEach((section, idx) => {
                /*eslint-disable-next-line*/
                return section.fields.forEach((field, index) => {
                  if (
                    field.hidden === false &&
                    field.permission_hidden === false
                  ) {
                    objectFields[object[key].object_name].push(field);
                  } else if (field.permission_hidden === undefined) {
                    objectFields[object[key].object_name].push(field);
                  }
                });
              });
            });
          }
        });

        // setFields(objectFields);
        let newObj = {};
        objectFields["main_object"].forEach((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });

        setAllFields(newObj);
        setLoadingData(true);
        axios
          .get(
            BASE_URL_API + `load-record-data?id=${id}&object_id=${objectId}`,
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            setLoadingData(false);
            let tempObject = {};

            let newObj1 = {};
            dispatch(loadRecordDataSuccess({ ...res.data.data }));
            /* eslint-disable-next-line */
            Object.entries(allFields).forEach(([key, value], index) => {
              if (value.type === "linkingobject") {
                newObj1[key] = {
                  ...(res.data.data !== null && res.data.data[key]),
                };
              }
            });
            let newObjLinking = {};
            /*eslint-disable-next-line*/
            Object.entries(res.data.data !== null && res.data.data).forEach(
              ([key, value], index) => {
                // let tempObject = {};
                if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "linkingobject"
                ) {
                  newObjLinking[key] = { ...value };
                }

                if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "datetime-local" &&
                  value.value !== null
                ) {
                  tempObject[key] = value
                    ? moment(value.value, "YYYY-MM-DD HH:mm:ss", true)
                    : null;
                } else if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "date" &&
                  value.value !== null
                ) {
                  tempObject[key] = value
                    ? moment(value.value, "YYYY-MM-DD HH:mm:ss", true)
                    : null;
                } else if (typeof value === "object") {
                  tempObject[key] = value.value;
                } else {
                  tempObject[key] = value;
                }
              }
            );
            form.setFieldsValue({
              ...tempObject,
              ...newObj1,
              ...newObjLinking,
            });
            setOpenEditLinkingRecord(true);
          })
          .catch((err) => setLoadingData(false));
      })
      .catch((err) => setLoadingDataObjectField(false));
  };

  const _onRunButton = (button) => {
    if (_.get(button, "dynamic_field", []).length > 0) {
      $visibleModalRDB(true);
      setDataConfirm({
        button: button,
        data: {
          data: {
            object_id: objectId,
            record_id: record_id,
            button_id: button._id,
          },
          reloadable: button?.reloadable || false,
        },
      });
    } else {
      setDataConfirm({
        data: {
          object_id: objectId,
          record_id: record_id,
          button_id: button._id,
        },
        reloadable: button?.reloadable || false,
      });
      setShowConfirm(true);
    }
  };

  useEffect(() => {
    if (isRunSuccess) {
      dispatch(
        runDynamicButtonResult({
          isRunSuccess: false,
          isReload: false,
        })
      );
      if (isReload) {
        initData();
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, isReload, isRunSuccess]);

  const numberWithCommas = (x, item) => {
    if (x === 0 || (x !== null && x))
      if (x === 0) {
        return "0";
      } else {
        x = x.toFixed(
          item?.decimal_separator === 0 ? 0 : item?.decimal_separator || 3
        );
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
  };

  const _onMakeCall = (value, hotline) => {
    if (localStorage.getItem("inCall") === "true") {
      Notification("error", "On call!");
    } else {
      let data = {};
      if (hotline) {
        data = {
          phone: value,
          hotline: hotline,
        };
      } else {
        data = {
          phone: value,
        };
      }
      dispatch(
        makeCall({
          ...data,
        })
      );
    }
  };

  useEffect(() => {
    let tempObject = {};
    /*eslint-disable-next-line*/

    Object.entries(recordData !== null && recordData).forEach(
      ([key, value], index) => {
        // let tempObject = {};
        if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "datetime-local" &&
          value.value !== null
        ) {
          tempObject[key] = value ? moment(value.value) : null;
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "date" &&
          value.value !== null
        ) {
          tempObject[key] = value ? moment(value.value) : null;
        } else if (
          typeof value === "object" &&
          allFields &&
          allFields[key] &&
          allFields[key].type !== "linkingobject"
        ) {
          tempObject[key] = value.value;
        } else if (allFields[key] && allFields[key].type !== "linkingobject") {
          tempObject[key] = value;
        }
      }
    );
    form.setFieldsValue({
      ...tempObject,
      // ...linkingFieldValue,
      assignTo: recordData.owner,
    });
    /*eslint-disable-next-line*/
  }, [recordData]);

  // useEffect(() => {
  //   form.setFieldsValue({
  //     ...form.getFieldsValue(),
  //     ...linkingFieldValue,
  //   });
  // }, [linkingFieldValue, form]);

  useEffect(() => {
    form.setFieldsValue(linkingFieldValue);
    // eslint-disable-next-line
  }, [linkingFieldValue]);

  useEffect(() => {
    dispatch(
      loadUserDynamicButton({
        object_id: objectId,
        record_id: record_id,
      })
    );
  }, [dispatch, objectId, record_id]);

  useEffect(() => {
    if (userDetail.use_cti) {
      dispatch(
        getFieldsMappingCallCenter({
          object_id: objectId,
        })
      );
    }
  }, [userDetail, objectId, dispatch]);
  const componentDecorator = (href, text, key) => (
    <a href={href} key={key} target="_blank noopener">
      {text}
    </a>
  );

  const [editRow, setEditRow] = useState(false);
  //eslint-disable-next-line
  const [render, setRender] = useState("");
  const handleEditOnRow = async (record) => {
    form.resetFields();
    dispatch(
      loadRecordData({
        id: details && details.record_data && details.record_data._id,
        object_id: data.related_object,
      })
    );
    setRecordID(details && details.record_data && details.record_data._id);
    const isTokenValid = await checkTokenExpiration();
    setLoadingDataObjectField(true);
    axios
      .post(
        BASE_URL_API + "object/objects-fields-permission",
        {
          api_version: "2",
          object_id: data.related_object,
        },
        {
          headers: {
            Authorization: isTokenValid,
          },
        }
      )
      .then((result) => {
        setLoadingDataObjectField(false);
        let objectFields = {};
        dispatch(loadListObjectFieldSuccess(result.data.data));
        /*eslint-disable-next-line*/
        result.data.data.map((object, idx) => {
          if (result.data.data.length - 1 === idx) {
            objectFields["main_object"] = [];
            /*eslint-disable-next-line*/
            object["main_object"].sections.map((section, idx) => {
              /*eslint-disable-next-line*/
              section.fields.map((field, index) => {
                objectFields["main_object"].push(field);
              });
            });
          } else {
            /*eslint-disable-next-line*/
            Object.entries(object).forEach(([key, value], idx) => {
              objectFields[object[key].object_name] = [];
              /*eslint-disable-next-line*/
              return object[key].sections.forEach((section, idx) => {
                /*eslint-disable-next-line*/
                return section.fields.forEach((field, index) => {
                  if (
                    field.hidden === false &&
                    field.permission_hidden === false
                  ) {
                    objectFields[object[key].object_name].push(field);
                  } else if (field.permission_hidden === undefined) {
                    objectFields[object[key].object_name].push(field);
                  }
                });
              });
            });
          }
        });

        // setFields(objectFields);
        let newObj = {};
        objectFields["main_object"].forEach((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });

        setAllFields(newObj);
        setLoadingData(true);
        axios
          .get(
            BASE_URL_API +
              `load-record-data?id=${record._id}&object_id=${data.related_object}`,
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            setLoadingData(false);
            let newObj2 = {};
            /* eslint-disable-next-line */
            Object.entries(newObj).forEach(([key, value], index) => {
              if (value.type === "linkingobject") {
                newObj2[key] = { ...res.data.data[key] };
              }
            });
            dispatch(setLinkingFieldValue(newObj2));

            let tempObject = {};
            let newObj1 = {};
            /* eslint-disable-next-line */
            Object.entries(newObj).forEach(([key, value], index) => {
              if (value.type === "linkingobject" || value.type === "lookup") {
                newObj1[key] = {
                  ...(res.data.data !== null && res.data.data[key]),
                };
              }
            });
            /*eslint-disable-next-line*/
            Object.entries(res.data.data !== null && res.data.data).forEach(
              ([key, value], index) => {
                // let tempObject = {};
                if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "datetime-local" &&
                  value.value !== null
                ) {
                  tempObject[key] = value ? moment(value.value) : null;
                } else if (
                  newObj &&
                  newObj[key] &&
                  newObj[key].type === "date" &&
                  value.value !== null
                ) {
                  tempObject[key] = value ? moment(value.value) : null;
                } else if (typeof value === "object") {
                  tempObject[key] = value.value;
                } else {
                  tempObject[key] = value;
                }
              }
            );
            form.setFieldsValue({ ...tempObject, ...newObj1, ...newObj2 });
            setRender(Math.random());
          })
          .catch((err) => setLoadingData(false));
      })
      .catch((err) => setLoadingDataObjectField(false));
  };
  const [dataFilterLinking, setDataFilterLinking] = useState({});
  const [formValue, setFormValue] = useState({});

  const handleFieldType = (field, open, id) => {
    switch (id) {
      case "created_date":
        return dataDetail[id];
      case "created_by":
        return dataDetail[id];
      case "modify_by":
        return dataDetail[id];
      case "modify_time":
        return dataDetail[id];
      default:
        break;
    }

    if (id === "owner") {
      return (
        <Form.Item
          label="Assign to"
          name="assignTo"
          rules={[{ required: true, message: "Please select assign to!" }]}
        >
          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              return option.children
                .join("")
                .toLowerCase()
                .includes(inputValue.toLowerCase());
            }}
          >
            {userAssignTo &&
              userAssignTo.map((user, idx) => {
                return (
                  <Option
                    value={user._id}
                    key={user._id}
                    disabled={user.disable}
                  >
                    {user.Last_Name} {user.Middle_Name} {user.First_Name}
                  </Option>
                );
              })}
          </Select>
        </Form.Item>
      );
    }
    switch (field?.type || id) {
      case "id":
        return <IDComp field={field} />;
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            isLabel
            listObjectField={listObjectField}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} />;

      case "date":
        return <Date field={field} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={open}
            recordID={recordID}
            setRecordID={setRecordID}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            open={editRow}
          />
        );
      case "dynamic-field":
        return (
          <DynamicField
            field={field}
            form={form}
            recordID={recordID}
            hiddenArray={hiddenArray}
            setHiddenArray={setHiddenArray}
          />
        );
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={recordData}
          />
        );
      case "formula":
        return <FormulaField field={field} />;

      case "lookup":
        return <Lookup field={field} form={form} recordData={recordData} />;
      default:
        break;
    }
  };

  const onFinish = async (values) => {
    let fieldsObject = {};
    /* eslint-disable-next-line */

    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        // section.fields.map((section, idx) => {
        /* eslint-disable-next-line */
        section.fields.map((item, index) => {
          fieldsObject[item.ID] = { ...item };
        });
        // });
      });

    let listValue = [];

    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              if (item.url) {
                result.push(item.url);
              } else {
                result.push(FE_URL + item.response.data[0]);
              }
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(item);
                return null;
              });
            } else {
              // eslint-disable-next-line
              value.map((item, idx) => {
                if (item.url) {
                  result.push(item.url);
                } else {
                  result.push(item);
                }
              });
            }
          }
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    // let searchData = [];
    /* eslint-disable-next-line */
    if (Object.entries(recordData).length > 0 && recordID) {
      await new Promise((resolve) => {
        dispatch(
          updateRecord({
            data: {
              data: listValue,
              owner_id: values["assignTo"],
              id: recordData._id,
              object_id: data.related_object,
              share_to: values.share_to,
              action_type: values.action_type,
              subject: values.subject,
              permission: values.permission,
            },
            load: {
              object_id: data.related_object,
              current_page: 1,
              record_per_page: 50,
              search_with: {
                meta: [],
                data: [],
              },
            },
            type: window.location.pathname.includes("list-view-with-details")
              ? "reload"
              : "",
          })
        );
        resolve();
      });
      setRecordID("");
      dispatch(loadRecordDataSuccess({}));
      setEditRow(false);
      initData();
    } else {
      dispatch(loadRecordDataSuccess({}));
      setEditRow(false);
      form.resetFields();
      setRecordID("");
    }
    setEditDetail(true);
    form.resetFields();
  };

  function formatString(str) {
    // Split the string into an array of words
    const words = str.split("_");

    // Capitalize the first letter of each word
    const capitalizedWords = words.map(
      (word) => word.charAt(0).toUpperCase() + word.slice(1)
    );

    // Join the capitalized words with a space between them
    const formattedString = capitalizedWords.join(" ");

    return formattedString === "Owner" ? "Assign to" : formattedString;
  }

  return (
    <Linkify componentDecorator={componentDecorator}>
      <Wrapper
        style={{
          paddingTop: `${
            window.location.pathname.includes("list-view-with-details")
              ? ""
              : "24px"
          }`,
        }}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          autoComplete="off"
          labelAlign="left"
          onValuesChange={(value, values) => {
            setFormValue(values);
            // setValueModal(values);
          }}
        >
          <CustomHeader
            style={{
              position: `${
                window.location.pathname.includes("list-view-with-details")
                  ? ""
                  : "fixed"
              }`,
            }}
          >
            <Row style={{ width: "100%" }}>
              <Col span={18}> {data.name} </Col>
              <Col span={6} className="pagination-col">
                <div
                  style={{
                    display: "flex",
                    justifyContent: "flex-end",
                    alignItems: "center",
                  }}
                >
                  {editRow ? (
                    <>
                      <CustomButton
                        onClick={() => {}}
                        style={{ marginRight: "8px" }}
                        htmlType="submit"
                        disabled={loadingData || loadingDataObjectField}
                      >
                        Save
                      </CustomButton>
                      <CustomButton
                        onClick={() => {
                          initData();
                          setEditRow(false);
                          setEditDetail(true);
                        }}
                      >
                        Cancel
                      </CustomButton>
                    </>
                  ) : (
                    ""
                  )}
                  <Tooltip title="Reload">
                    <CustomButtonReload
                      onClick={() => {
                        initData();
                      }}
                    >
                      <img alt="" src={Refresh} />
                    </CustomButtonReload>
                  </Tooltip>
                  <Dropdown
                    overlay={
                      <Menu>
                        {Array.isArray(details.record_data) ||
                        Object.keys(details).length === 0 ||
                        (details &&
                          details.record_data &&
                          Object.keys(details.record_data).length === 0) ? (
                          ""
                        ) : (
                          <Menu.Item
                            onClick={() => {
                              form.resetFields();
                              // dispatch(
                              //   loadRecordData({
                              //     id:
                              //       details &&
                              //       details.record_data &&
                              //       details.record_data._id,
                              //     object_id: data.related_object,
                              //   })
                              // );
                              if (objectId) {
                                dispatch(
                                  loadFormCreate({
                                    object_id: objectId,
                                  })
                                );
                              }
                              setRecordID(
                                details &&
                                  details.record_data &&
                                  details.record_data._id
                              );
                              edit(
                                details &&
                                  details.record_data &&
                                  details.record_data
                              );
                              setDrag(false);
                            }}
                          >
                            Edit record
                          </Menu.Item>
                        )}

                        {editProps ? (
                          window.location.pathname.includes(
                            "list-view-with-details"
                          ) ? (
                            ""
                          ) : (
                            <>
                              <Menu.Item
                                onClick={() => {
                                  setDrag(false);
                                  dispatch(
                                    loadListObjectField({
                                      api_version: "2",
                                      object_id: data.related_object,
                                      show_meta_fields: true,
                                    })
                                  );
                                  setIsModalVisible(true);
                                  setComponentID(data._id);
                                  dispatch(
                                    loadRelatedObject({
                                      object_id: data.related_object,
                                    })
                                  );
                                }}
                              >
                                Edit component
                              </Menu.Item>

                              {userDetail.Is_Admin || checkRule("delete") ? (
                                <Menu.Item
                                  onClick={() => {
                                    onRemoveItem(JSON.stringify(data));
                                    setDrag(false);
                                  }}
                                  disabled={onRemoveItem ? false : true}
                                >
                                  Remove
                                </Menu.Item>
                              ) : (
                                ""
                              )}
                            </>
                          )
                        ) : (
                          ""
                        )}

                        {window.location.pathname.includes(
                          "list-view-with-details"
                        ) ? (
                          <>
                            <Menu.Item
                              onClick={() => {
                                setDrag(false);
                                dispatch(
                                  loadListObjectField({
                                    api_version: "2",
                                    object_id: data.related_object,
                                    show_meta_fields: true,
                                  })
                                );
                                setIsModalVisible(true);
                                setComponentID(data._id);
                                dispatch(
                                  loadRelatedObject({
                                    object_id: data.related_object,
                                  })
                                );
                              }}
                            >
                              Edit component
                            </Menu.Item>

                            {userDetail.Is_Admin || checkRule("delete") ? (
                              <Menu.Item
                                onClick={() => {
                                  onRemoveItem(JSON.stringify(data));
                                  setDrag(false);
                                }}
                                disabled={onRemoveItem ? false : true}
                              >
                                Remove
                              </Menu.Item>
                            ) : (
                              ""
                            )}
                          </>
                        ) : (
                          ""
                        )}
                        {data.related_object === objectId &&
                          userDynamicButton.map((button, idx) => {
                            return (
                              <Menu.Item
                                key={idx}
                                onClick={() => _onRunButton(button)}
                              >
                                <span>{button.name}</span>
                              </Menu.Item>
                            );
                          })}
                      </Menu>
                    }
                    placement="bottomLeft"
                  >
                    <WrapAction>
                      <SettingOutlined />
                    </WrapAction>
                  </Dropdown>
                </div>
              </Col>
            </Row>
          </CustomHeader>
          {}
          {Object.keys(details).length === 0 ||
          Array.isArray(details.record_data) ||
          Object.keys(details.record_data).length === 0 ? (
            <div
              style={{
                padding: "20px",
                textAlign: "center",
                width: "100%",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                alt=""
                src={noDataImg}
                style={{ width: "100px", marginBottom: "10px" }}
              />
              No data
            </div>
          ) : loading ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignContent: "center",
                marginTop: "10%",
                flexDirection: "column",
                background: "white",
              }}
            >
              <Spin />
            </div>
          ) : (
            //here
            <WrapperCols>
              <Row style={{ width: "100%" }} gutter={[10, 10]}>
                {cols.map((col, idx) => {
                  if (cols.length === 5) {
                    return (
                      <div>
                        <table style={{ width: "100%" }}>
                          <tbody>
                            {col.map((itemCol, index) => {
                              if (
                                hiddenArray.findIndex(
                                  (ele) => ele === fieldObject[itemCol]?._id
                                ) < 0
                              )
                                return (
                                  <tr className="col-wrapper">
                                    <td
                                      className="col-key"
                                      onDoubleClick={() => {
                                        if (editDetail) {
                                          setEditRow(true);
                                          setEditDetail(false);
                                          handleEditOnRow(dataDetail);
                                        } else {
                                          Notification(
                                            "error",
                                            "Please save first!"
                                          );
                                        }
                                      }}
                                    >
                                      {showFields[itemCol]
                                        ? showFields[itemCol].name
                                        : formatString(itemCol)}
                                    </td>

                                    <td
                                      className="col-value"
                                      id={`ucrm_multiDetails_${idx}`}
                                    >
                                      {typeof dataDetail[itemCol] ===
                                        "string" && editRow === false
                                        ? dataDetail[itemCol]
                                        : ""}
                                      {dataDetail[itemCol]
                                        ?.id_field_related_record ? (
                                        <>
                                          {editRow === false ? (
                                            <Tooltip
                                              title={
                                                <>
                                                  <div
                                                    style={{ display: "flex" }}
                                                  >
                                                    <div
                                                      style={{
                                                        marginRight: "16px",
                                                        cursor: "pointer",
                                                      }}
                                                      onClick={() => {}}
                                                    >
                                                      <a
                                                        rel="noreferrer"
                                                        target="_blank"
                                                        href={
                                                          FE_URL +
                                                          `/consolidated-view/${dataDetail[itemCol].object_related}/${dataDetail[itemCol].id_related_record}`
                                                        }
                                                      >
                                                        View
                                                      </a>
                                                    </div>
                                                    <div
                                                      style={{
                                                        cursor: "pointer",
                                                      }}
                                                      onClick={() => {
                                                        setDrag(false);
                                                        setRecordLinkingID(
                                                          dataDetail[itemCol]
                                                            ?.id_related_record
                                                        );
                                                        setObjectLinking(
                                                          dataDetail[itemCol]
                                                            ?.object_related
                                                        );
                                                        // setOpenEditLinkingRecord(true);
                                                        editLinking(
                                                          dataDetail[itemCol]
                                                            ?.id_related_record,
                                                          dataDetail[itemCol]
                                                            ?.object_related
                                                        );
                                                      }}
                                                    >
                                                      Edit
                                                    </div>
                                                  </div>
                                                </>
                                              }
                                            >
                                              <span
                                                style={{
                                                  textDecoration: "underline",
                                                  cursor: "pointer",
                                                }}
                                              >
                                                {dataDetail[itemCol]?.value}
                                              </span>
                                            </Tooltip>
                                          ) : (
                                            handleFieldType(
                                              showFields[itemCol],
                                              editRow,
                                              itemCol
                                            )
                                          )}
                                        </>
                                      ) : editRow ? (
                                        handleFieldType(
                                          showFields[itemCol],
                                          editRow,
                                          itemCol
                                        )
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "object" &&
                                        showFields[itemCol]?.type === "file" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value.map(
                                          (item, idx) => {
                                            let name =
                                              (item && item.split("/")) || "";

                                            return (
                                              <div>
                                                <a
                                                  href={
                                                    allObject?.find(
                                                      (object) =>
                                                        object?.object_id ===
                                                          objectId &&
                                                        object?.status
                                                    )
                                                      ? `${
                                                          FE_URL +
                                                          item?.split(BE_URL)[1]
                                                        }`
                                                      : item
                                                  }
                                                  rel="noreferrer"
                                                  target="_blank"
                                                >
                                                  {name[name.length - 1]}
                                                </a>
                                              </div>
                                            );
                                          }
                                        )
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "string" &&
                                        showFields[itemCol]?.type === "file" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value
                                          .split(",")
                                          .map((link, idx) => {
                                            let splitFile = link.split("/");
                                            return (
                                              <a
                                                key={idx}
                                                style={{ display: "block" }}
                                                href={
                                                  allObject?.find(
                                                    (object) =>
                                                      object?.object_id ===
                                                        objectId &&
                                                      object?.status
                                                  )
                                                    ? `${
                                                        FE_URL +
                                                        link?.split(BE_URL)[1]
                                                      }`
                                                    : link
                                                }
                                                target={"_blank"}
                                                rel="noreferrer"
                                              >
                                                <TextComponent
                                                  ellipsis={{
                                                    tooltip:
                                                      splitFile[
                                                        splitFile.length - 1
                                                      ],
                                                  }}
                                                >
                                                  <span
                                                    style={{ color: "#1890ff" }}
                                                  >
                                                    {
                                                      splitFile[
                                                        splitFile.length - 1
                                                      ]
                                                    }
                                                  </span>
                                                </TextComponent>
                                              </a>
                                            );
                                          })
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "object" &&
                                        showFields[itemCol]?.type ===
                                          "select" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value.map(
                                          (item, idx) => {
                                            return (
                                              <div
                                                style={{
                                                  background:
                                                    "rgb(243 239 239)",
                                                  marginRight: "5px",
                                                  padding: "2px",
                                                  borderRadius: "5px",
                                                  float: "left",
                                                  marginBottom: "5px",
                                                }}
                                              >
                                                {item}
                                              </div>
                                            );
                                          }
                                        )
                                      ) : editRow ? (
                                        handleFieldType(
                                          showFields[itemCol],
                                          editRow
                                        )
                                      ) : (
                                        <div
                                          onDoubleClick={() => {
                                            if (editDetail) {
                                              setEditRow(true);
                                              setEditDetail(false);
                                              handleEditOnRow(dataDetail);
                                            } else {
                                              Notification(
                                                "error",
                                                "Please save first!"
                                              );
                                            }
                                          }}
                                        >
                                          {typeof dataDetail[itemCol]?.value ===
                                          "number"
                                            ? numberWithCommas(
                                                dataDetail[itemCol]?.value,
                                                showFields[itemCol]
                                              )
                                            : parse(
                                                dataDetail[itemCol]?.value
                                                  ? dataDetail[itemCol]?.value
                                                  : "",
                                                optionsParse
                                              )}

                                          {Object.keys(fieldsMappingCallCenter)
                                            .length > 0 &&
                                            fieldsMappingCallCenter.phone_fields
                                              .length > 0 &&
                                            fieldsMappingCallCenter.phone_fields.includes(
                                              showFields[itemCol]?.ID
                                            ) && (
                                              <Dropdown
                                                overlay={
                                                  <Menu>
                                                    {fieldsMappingCallCenter.phone_fields.map(
                                                      (fieldCall) => {
                                                        return (
                                                          details &&
                                                          details.fields &&
                                                          details.fields.map(
                                                            (field) => {
                                                              return (
                                                                <>
                                                                  {fieldCall ===
                                                                    field.ID &&
                                                                    fieldCall ===
                                                                      showFields[
                                                                        itemCol
                                                                      ].ID &&
                                                                    !!details.record_data &&
                                                                    !!details
                                                                      .record_data[
                                                                      field.ID
                                                                    ] && (
                                                                      <>
                                                                        {fieldsMappingCallCenter
                                                                          .custom_hotlines
                                                                          .length >
                                                                        0 ? (
                                                                          <>
                                                                            {fieldsMappingCallCenter.custom_hotlines.map(
                                                                              (
                                                                                hotline
                                                                              ) => {
                                                                                return (
                                                                                  <Menu.Item
                                                                                    onClick={() =>
                                                                                      _onMakeCall(
                                                                                        details
                                                                                          .record_data[
                                                                                          field
                                                                                            .ID
                                                                                        ]
                                                                                          .value,
                                                                                        hotline.hotline
                                                                                      )
                                                                                    }
                                                                                    key={
                                                                                      fieldCall
                                                                                    }
                                                                                  >
                                                                                    {
                                                                                      hotline.prefix
                                                                                    }
                                                                                    {
                                                                                      details
                                                                                        .record_data[
                                                                                        field
                                                                                          .ID
                                                                                      ]
                                                                                        .value
                                                                                    }
                                                                                  </Menu.Item>
                                                                                );
                                                                              }
                                                                            )}
                                                                          </>
                                                                        ) : (
                                                                          <Menu.Item
                                                                            onClick={() =>
                                                                              _onMakeCall(
                                                                                details
                                                                                  .record_data[
                                                                                  field
                                                                                    .ID
                                                                                ]
                                                                                  .value
                                                                              )
                                                                            }
                                                                            key={
                                                                              fieldCall
                                                                            }
                                                                          >
                                                                            {
                                                                              details
                                                                                .record_data[
                                                                                field
                                                                                  .ID
                                                                              ]
                                                                                .value
                                                                            }
                                                                          </Menu.Item>
                                                                        )}
                                                                      </>
                                                                    )}
                                                                </>
                                                              );
                                                            }
                                                          )
                                                        );
                                                      }
                                                    )}
                                                  </Menu>
                                                }
                                                placement="bottom"
                                              >
                                                <ButtonCall>
                                                  <img
                                                    src={Call}
                                                    alt="makeCall"
                                                  />
                                                </ButtonCall>
                                              </Dropdown>
                                            )}
                                        </div>
                                      )}
                                    </td>
                                  </tr>
                                );
                              return null;
                            })}
                          </tbody>
                        </table>
                      </div>
                    );
                  } else
                    return (
                      <Col span={24 / cols.length}>
                        <table style={{ width: "100%" }}>
                          <tbody>
                            {col.map((itemCol, index) => {
                              if (
                                hiddenArray.findIndex(
                                  (ele) => ele === fieldObject[itemCol]?._id
                                ) < 0
                              )
                                return (
                                  <tr className="col-wrapper">
                                    <td
                                      className="col-key"
                                      onDoubleClick={() => {
                                        if (editDetail) {
                                          setEditRow(true);
                                          setEditDetail(false);
                                          handleEditOnRow(dataDetail);
                                        } else {
                                          Notification(
                                            "error",
                                            "Please save first!"
                                          );
                                        }
                                      }}
                                    >
                                      {showFields[itemCol]
                                        ? showFields[itemCol].name
                                        : formatString(itemCol)}
                                    </td>

                                    <td
                                      className="col-value"
                                      id={`ucrm_multiDetails_${idx}`}
                                    >
                                      {typeof dataDetail[itemCol] ===
                                        "string" && editRow === false
                                        ? dataDetail[itemCol]
                                        : ""}
                                      {dataDetail[itemCol]
                                        ?.id_field_related_record ? (
                                        <>
                                          {editRow === false ? (
                                            <Tooltip
                                              title={
                                                <>
                                                  <div
                                                    style={{ display: "flex" }}
                                                  >
                                                    <div
                                                      style={{
                                                        marginRight: "16px",
                                                        cursor: "pointer",
                                                      }}
                                                      onClick={() => {}}
                                                    >
                                                      <a
                                                        rel="noreferrer"
                                                        target="_blank"
                                                        href={
                                                          FE_URL +
                                                          `/consolidated-view/${dataDetail[itemCol].object_related}/${dataDetail[itemCol].id_related_record}`
                                                        }
                                                      >
                                                        View
                                                      </a>
                                                    </div>
                                                    <div
                                                      style={{
                                                        cursor: "pointer",
                                                      }}
                                                      onClick={() => {
                                                        setDrag(false);
                                                        setRecordLinkingID(
                                                          dataDetail[itemCol]
                                                            ?.id_related_record
                                                        );
                                                        setObjectLinking(
                                                          dataDetail[itemCol]
                                                            ?.object_related
                                                        );
                                                        // setOpenEditLinkingRecord(true);
                                                        editLinking(
                                                          dataDetail[itemCol]
                                                            ?.id_related_record,
                                                          dataDetail[itemCol]
                                                            ?.object_related
                                                        );
                                                      }}
                                                    >
                                                      Edit
                                                    </div>
                                                  </div>
                                                </>
                                              }
                                            >
                                              <span
                                                style={{
                                                  textDecoration: "underline",
                                                  cursor: "pointer",
                                                }}
                                              >
                                                {dataDetail[itemCol]?.value}
                                              </span>
                                            </Tooltip>
                                          ) : (
                                            handleFieldType(
                                              showFields[itemCol],
                                              editRow,
                                              itemCol
                                            )
                                          )}
                                        </>
                                      ) : editRow ? (
                                        handleFieldType(
                                          showFields[itemCol],
                                          editRow,
                                          itemCol
                                        )
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "object" &&
                                        showFields[itemCol]?.type === "file" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value.map(
                                          (item, idx) => {
                                            let name =
                                              (item && item.split("/")) || "";

                                            return (
                                              <div>
                                                <a
                                                  href={
                                                    allObject?.find(
                                                      (object) =>
                                                        object?.object_id ===
                                                          objectId &&
                                                        object?.status
                                                    )
                                                      ? `${
                                                          FE_URL +
                                                          item?.split(BE_URL)[1]
                                                        }`
                                                      : item
                                                  }
                                                  rel="noreferrer"
                                                  target="_blank"
                                                >
                                                  {name[name.length - 1]}
                                                </a>
                                              </div>
                                            );
                                          }
                                        )
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "string" &&
                                        showFields[itemCol]?.type === "file" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value
                                          .split(",")
                                          .map((link, idx) => {
                                            let splitFile = link.split("/");
                                            return (
                                              <a
                                                key={idx}
                                                style={{ display: "block" }}
                                                href={
                                                  allObject?.find(
                                                    (object) =>
                                                      object?.object_id ===
                                                        objectId &&
                                                      object?.status
                                                  )
                                                    ? `${
                                                        FE_URL +
                                                        link?.split(BE_URL)[1]
                                                      }`
                                                    : link
                                                }
                                                target={"_blank"}
                                                rel="noreferrer"
                                              >
                                                <TextComponent
                                                  ellipsis={{
                                                    tooltip:
                                                      splitFile[
                                                        splitFile.length - 1
                                                      ],
                                                  }}
                                                >
                                                  <span
                                                    style={{ color: "#1890ff" }}
                                                  >
                                                    {
                                                      splitFile[
                                                        splitFile.length - 1
                                                      ]
                                                    }
                                                  </span>
                                                </TextComponent>
                                              </a>
                                            );
                                          })
                                      ) : typeof dataDetail[itemCol]?.value ===
                                          "object" &&
                                        showFields[itemCol]?.type ===
                                          "select" &&
                                        dataDetail[itemCol]?.value !== null ? (
                                        dataDetail[itemCol]?.value.map(
                                          (item, idx) => {
                                            return (
                                              <div
                                                style={{
                                                  background:
                                                    "rgb(243 239 239)",
                                                  marginRight: "5px",
                                                  padding: "2px",
                                                  borderRadius: "5px",
                                                  float: "left",
                                                  marginBottom: "5px",
                                                }}
                                              >
                                                {item}
                                              </div>
                                            );
                                          }
                                        )
                                      ) : editRow ? (
                                        handleFieldType(
                                          showFields[itemCol],
                                          editRow
                                        )
                                      ) : (
                                        <div
                                          onDoubleClick={() => {
                                            if (editDetail) {
                                              setEditRow(true);
                                              setEditDetail(false);
                                              handleEditOnRow(dataDetail);
                                            } else {
                                              Notification(
                                                "error",
                                                "Please save first!"
                                              );
                                            }
                                          }}
                                        >
                                          {typeof dataDetail[itemCol]?.value ===
                                          "number"
                                            ? numberWithCommas(
                                                dataDetail[itemCol]?.value,
                                                showFields[itemCol]
                                              )
                                            : parse(
                                                dataDetail[itemCol]?.value
                                                  ? dataDetail[itemCol]?.value
                                                  : "",
                                                optionsParse
                                              )}
                                          {Object.keys(fieldsMappingCallCenter)
                                            .length > 0 &&
                                            fieldsMappingCallCenter.phone_fields
                                              .length > 0 &&
                                            fieldsMappingCallCenter.phone_fields.includes(
                                              showFields[itemCol]?.ID
                                            ) && (
                                              <Dropdown
                                                overlay={
                                                  <Menu>
                                                    {fieldsMappingCallCenter.phone_fields.map(
                                                      (fieldCall) => {
                                                        return (
                                                          details &&
                                                          details.fields &&
                                                          details.fields.map(
                                                            (field) => {
                                                              return (
                                                                <>
                                                                  {fieldCall ===
                                                                    field.ID &&
                                                                    fieldCall ===
                                                                      showFields[
                                                                        itemCol
                                                                      ].ID &&
                                                                    !!details.record_data &&
                                                                    !!details
                                                                      .record_data[
                                                                      field.ID
                                                                    ] && (
                                                                      <>
                                                                        {fieldsMappingCallCenter
                                                                          .custom_hotlines
                                                                          .length >
                                                                        0 ? (
                                                                          <>
                                                                            {fieldsMappingCallCenter.custom_hotlines.map(
                                                                              (
                                                                                hotline
                                                                              ) => {
                                                                                return (
                                                                                  <Menu.Item
                                                                                    onClick={() =>
                                                                                      _onMakeCall(
                                                                                        details
                                                                                          .record_data[
                                                                                          field
                                                                                            .ID
                                                                                        ]
                                                                                          .value,
                                                                                        hotline.hotline
                                                                                      )
                                                                                    }
                                                                                    key={
                                                                                      fieldCall
                                                                                    }
                                                                                  >
                                                                                    {
                                                                                      hotline.prefix
                                                                                    }
                                                                                    {
                                                                                      details
                                                                                        .record_data[
                                                                                        field
                                                                                          .ID
                                                                                      ]
                                                                                        .value
                                                                                    }
                                                                                  </Menu.Item>
                                                                                );
                                                                              }
                                                                            )}
                                                                          </>
                                                                        ) : (
                                                                          <Menu.Item
                                                                            onClick={() =>
                                                                              _onMakeCall(
                                                                                details
                                                                                  .record_data[
                                                                                  field
                                                                                    .ID
                                                                                ]
                                                                                  .value
                                                                              )
                                                                            }
                                                                            key={
                                                                              fieldCall
                                                                            }
                                                                          >
                                                                            {
                                                                              details
                                                                                .record_data[
                                                                                field
                                                                                  .ID
                                                                              ]
                                                                                .value
                                                                            }
                                                                          </Menu.Item>
                                                                        )}
                                                                      </>
                                                                    )}
                                                                </>
                                                              );
                                                            }
                                                          )
                                                        );
                                                      }
                                                    )}
                                                  </Menu>
                                                }
                                                placement="bottom"
                                              >
                                                <ButtonCall>
                                                  <img
                                                    src={Call}
                                                    alt="makeCall"
                                                  />
                                                </ButtonCall>
                                              </Dropdown>
                                            )}
                                        </div>
                                      )}
                                    </td>
                                  </tr>
                                );
                              return null;
                            })}
                          </tbody>
                        </table>
                      </Col>
                    );
                })}
              </Row>
            </WrapperCols>
          )}
        </Form>
        <ModalRecord
          open={openModalRecord}
          setOpen={setOpenModalRecord}
          initData={initData}
          recordPerPage={20}
          currentPage={1}
          form={form}
          recordID={recordID}
          setRecordID={setRecordID}
          fields={fields}
          objectId={data.related_object}
          loadData={initData}
          setFields={setFields}
        />

        <ModalRecord
          open={openEditLinkingRecord}
          setOpen={setOpenEditLinkingRecord}
          initData={() => {}}
          recordPerPage={20}
          currentPage={1}
          form={form}
          recordID={recordLinkingID}
          setRecordID={setRecordLinkingID}
          fields={fields}
          objectId={objectLinking}
          setFields={setFields}
        />

        <ModalConfirm
          title="Confirm"
          decs="Are you sure you want to proceed?"
          method={runDynamicButton}
          data={dataConfirm}
          img={ImgConfirm}
          showConfirm={showConfirm}
          setShowConfirm={setShowConfirm}
        />

        <ModalRunDynamicButton
          data={dataConfirm}
          method={runDynamicButton}
          visibleModalRDB={visibleModalRDB}
          $visibleModalRDB={$visibleModalRDB}
          dataRecord={initRecordData}
          isFormatSystem={true}
        />

        <ModalComponent
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          selectedObject={objectId}
          dataItem={data}
          componentID={componentID}
          setComponentID={setComponentID}
          setDataItem={setDataItem}
          initData={initData}
          objectName={data && data.related_object_name}
        />

        {/* <ModalRecord
        open={openEditLinkingRecord}
        setOpen={setOpenEditLinkingRecord}
        initData={initData}
        recordPerPage={20}
        currentPage={1}
        form={form}
        recordID={recordLinkingID}
        setRecordID={setRecordLinkingID}
        fields={fieldsLinking}
        objectId={objectLinking}
      /> */}
        <ModalDuplicate />
      </Wrapper>
    </Linkify>
  );
};

export default Details;
const Wrapper = styled.div`
  overflow: auto;
  width: 100%;
  height: 100%;
  position: relative;
  .ant-form-item {
    margin-bottom: 0;
  }
  label {
    display: none;
  }
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .item {
    width: 100%;
    border: solid 1px #ececec;
    border-bottom: none;
    :first-child {
      border-radius: 5px 5px 0 0;
    }
    :last-child {
      border-radius: 0 0 5px 5px;
      border-bottom: solid 1px #ececec;
    }

    .item-name {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 16px;
      line-height: 130%;
      border-right: solid 1px #ececec;
      padding: 4px 8px;
      letter-spacing: 0.01em;
      color: #2c2c2c;
      vertical-align: top;
    }
    .item-value {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 130%;
      letter-spacing: 0.01em;
      color: #2c2c2c;
      white-space: pre-line;
      padding: 4px 8px;
      vertical-align: top;
    }
  }

  .item-default {
    display: flex;
    width: 100%;
    margin-bottom: 8px;
    .name-default {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 16px;
      line-height: 130%;
      letter-spacing: 0.01em;
      color: #2c2c2c;
    }
    .value-default {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 130%;
      letter-spacing: 0.01em;
      color: #2c2c2c;
      white-space: pre-line;
      margin-left: 8px;
    }
  }
`;

const CustomButtonReload = styled.div`
  cursor: pointer;
  margin-right: 8px;
  img {
    width: 18px;
    margin-bottom: 4px;
  }
`;

const ButtonCall = styled(Button)`
  border-radius: 2px;
  padding: 0 24px;
  border-color: ${(props) => props.theme.main};
  height: 21px;
  margin-left: 24px;

  img {
    margin-bottom: 4px;
    width: 13px;
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker} !important;

    img {
      filter: brightness(200);
    }
  }

  :active {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }

  :focus {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }
`;
const CustomButton = styled(Button)`
  background: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  margin-right: 8px;
  height: unset;
  border-color: ${(props) => props.theme.main};
  padding: 0 8px;
  :hover {
    background: ${(props) => props.theme.main};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
`;

const WrapperCols = styled.div`
  display: flex;
  justify-content: space-between;
  margin-top: 10px;
  width: 100%;
  table {
    border-collapse: collapse;
    margin-right: 10px;
    border-right: 1px solid #ececec;
  }

  tr:first-child {
    border-top: none;
  }

  .col-wrapper {
    .col-key {
      padding: 4px 8px;
      letter-spacing: 0.01em;
      color: #6b6b6b;
      vertical-align: top;
    }
    .col-value {
      overflow: auto;
      font-family: var(--roboto-500);
      word-wrap: break-word;
      max-width: 250px;
      min-width: 100px;
      color: #2c2c2c;
      white-space: pre-line;
      padding: 4px 8px;
      vertical-align: top;
    }
  }
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  top: 0px;
  left: 0;
  z-index: 10;
  background: #fff;
  width: 100%;
  padding: 8px 12px;
  font-size: 16px;
  font-family: var(--roboto-500);
`;
const WrapAction = styled.div`
  cursor: pointer;
`;
