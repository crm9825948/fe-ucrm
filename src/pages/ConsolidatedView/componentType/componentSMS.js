import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { Input, Select } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  getListMessage,
  changeListSMS,
  // deleteSMS,
  changeLoadMore,
} from "redux/slices/componentSMS";
import { BE_URL } from "constants/constants";
import InfiniteScroll from "react-infinite-scroll-component";
import Spin from "antd/lib/spin";

import FailImg from "assets/icons/common/failSMS.png";
import SuccessImg from "assets/icons/common/doubleCheck.png";
import WaitImg from "assets/icons/common/waitSMS.png";

import DefaultAvatarAgent from "assets/icons/common/avtAgentDefault.png";
import NoDataImg from "assets/icons/consolidatedViewSettings/SMS.png";
import ModalsendSms from "./modalsendSms";

const { Search } = Input;

const { Option } = Select;

const ComponentSMS = ({ objectId, recordID }) => {
  const dispatch = useDispatch();

  const { listMessage, listSMS, totalRecord, isLoadMore } = useSelector(
    (state) => state.componentSMSReducer
  );
  const { listAllUser } = useSelector((state) => state.userReducer);
  const [itemActive, setItemActive] = useState({});
  const [optionSearch, setOptionSearch] = useState("content");
  const [openModal, setOpenModal] = useState(false);
  const [page, setPage] = useState(0);
  const [valueSearch, setValueSearch] = useState("");
  const [inputSearch, setInputSearch] = useState("");

  useEffect(() => {
    if (listSMS.length > 0) {
      setItemActive(listSMS[0]);
    }
  }, [listSMS]);

  useEffect(() => {
    // if (listMessage.length > 0) {
    // const temp = listSMS.concat(listMessage);
    // const filteredArr = temp.reduce((acc, current) => {
    //   const x = acc.find((item) => item._id === current._id);
    //   if (!x) {
    //     return acc.concat([current]);
    //   } else {
    //     return acc;
    //   }
    // }, []);
    // dispatch(changeListSMS(filteredArr));

    if (isLoadMore) {
      const temp = listSMS.concat(listMessage);
      dispatch(changeListSMS(temp));
    } else {
      dispatch(changeListSMS(listMessage));
    }
    // }
    //eslint-disable-next-line
  }, [listMessage]);

  useEffect(() => {
    dispatch(
      getListMessage({
        objectId: objectId,
        record_id: recordID,
        data: {
          limit: 10,
          page: 0,
          search_data: {},
        },
      })
    );
    // dispatch(
    //   loadUser({
    //     current_page: 1,
    //     record_per_page: 100000,
    //   })
    // );
    //eslint-disable-next-line
  }, [dispatch, objectId]);

  const renderImg = (status, statusCode) => {
    if (status === "pending") {
      return <img src={WaitImg} alt="Pending" />;
    } else if (status === "failed") {
      return <img src={FailImg} alt="Failed" />;
    } else {
      if (statusCode > 199 && statusCode < 300) {
        return <img src={SuccessImg} alt="Success" />;
      } else {
        return <img src={FailImg} alt="Failed" />;
      }
    }
  };

  const loadMoreSms = () => {
    const temp = page + 1;

    dispatch(changeLoadMore(true));
    dispatch(
      getListMessage({
        objectId: objectId,
        record_id: recordID,
        data: {
          limit: 10,
          page: temp,
          search_data: { [optionSearch]: valueSearch },
        },
      })
    );
    setPage(temp);
  };

  const renderItem = (list) => {
    if (list) {
      return list.map((item, index) => {
        return (
          <ItemContent
            key={index}
            onClick={() => {
              setItemActive(item);
            }}
            isActive={item._id === itemActive._id}
            // onDoubleClick={() => {
            //   dispatch(
            //     deleteSMS({
            //       dataDelete: {
            //         message_ids: [item._id],
            //       },
            //     })
            //   );
            // }}
          >
            <Top isActive={item._id === itemActive._id}>
              <span>{item.send_to}</span>

              <BottomRight>
                {renderImg(item.status, item.status_code)}
              </BottomRight>
            </Top>
            <Center>{item.content}</Center>
            <Bottom>
              <BottomLeft>
                Sender: <span>{handleName(listAllUser, item.user_id)}</span>
              </BottomLeft>
            </Bottom>
            <Bottom>
              <BottomLeft>
                Date: <span>{item.created_date}</span>
              </BottomLeft>
            </Bottom>
          </ItemContent>
        );
      });
    }
  };
  const handleName = (list, id) => {
    let name = "System";
    if (id && list.length > 0) {
      list.forEach((item) => {
        if (item._id === id) {
          name = item.Full_Name;
        }
      });
    }
    return name;
  };
  const handleAvatar = (list, id) => {
    let name = "";
    if (id && list.length > 0) {
      list.forEach((item) => {
        if (item._id === id) {
          if (item.avatar_config) {
            name = item.avatar_config.url;
          }
        }
      });
    }
    return name;
  };
  return (
    <Wrap>
      <Title>Component SMS</Title>
      <ContentWrap>
        <LeftWrap>
          <CustomButtonAdd onClick={() => setOpenModal(true)}>
            + Add new
          </CustomButtonAdd>

          <SearchWrap>
            <CustomSelect
              value={optionSearch}
              onChange={(value) => setOptionSearch(value)}
            >
              <Option value="content">Content</Option>
              <Option value="send_to">Send to</Option>
            </CustomSelect>
            <CustomInputSearch
              allowClear
              placeholder="Search..."
              value={inputSearch}
              onChange={(e) => {
                if (!e.target.value) {
                  setValueSearch("");
                  // dispatch(changeListSMS([]));
                  dispatch(changeLoadMore(false));
                  dispatch(
                    getListMessage({
                      objectId: objectId,
                      record_id: recordID,
                      data: {
                        limit: 10,
                        page: 0,
                        search_data: {},
                      },
                    })
                  );
                }
                setInputSearch(e.target.value);
              }}
              onSearch={(e) => {
                dispatch(changeLoadMore(false));
                // dispatch(changeListSMS([]));
                setValueSearch(e);
                setPage(0);
                dispatch(
                  getListMessage({
                    objectId: objectId,
                    record_id: recordID,
                    data: {
                      limit: 10,
                      page: 0,
                      search_data: { [optionSearch]: e },
                    },
                  })
                );
              }}
            />
          </SearchWrap>

          <ItemWrap id="scrollableDiv">
            {listSMS.length > 0 ? (
              <InfiniteScroll
                hasMore={listSMS.length > 0 && listSMS.length < totalRecord}
                dataLength={listSMS.length}
                loader={<Spin />}
                next={loadMoreSms}
                scrollableTarget="scrollableDiv"
              >
                {renderItem(listSMS)}
              </InfiniteScroll>
            ) : (
              <NoData>
                <DataWrap>
                  <img src={NoDataImg} alt="nodata" />
                  <p>
                    No <span>SMS</span>
                  </p>
                </DataWrap>
              </NoData>
            )}
          </ItemWrap>
        </LeftWrap>

        <RightWrap>
          {listSMS.length > 0 ? (
            <RightContent>
              <Header>
                <HeaderWrap>
                  {handleAvatar(listAllUser, itemActive.user_id) !== "" &&
                  handleAvatar(listAllUser, itemActive.user_id) ? (
                    <img
                      src={`${BE_URL}${handleAvatar(
                        listAllUser,
                        itemActive.user_id
                      )}`}
                      alt="Img"
                    />
                  ) : (
                    <img src={DefaultAvatarAgent} alt="img" />
                  )}
                  <InfoHeader>
                    <span>
                      Sender: {handleName(listAllUser, itemActive.user_id)}
                    </span>
                    <span>
                      Send to: {itemActive.send_to}
                      {renderImg(itemActive.status, itemActive.status_code)}
                    </span>
                  </InfoHeader>
                </HeaderWrap>
                <p> {itemActive.created_date}</p>
              </Header>
              <Content>
                <p>Content :</p>
                <span>{itemActive.content}</span>
              </Content>
            </RightContent>
          ) : (
            <NoData>
              <DataWrap>
                <img src={NoDataImg} alt="nodata" />
                <p>
                  No <span>SMS</span>
                </p>
              </DataWrap>
            </NoData>
          )}
        </RightWrap>

        <ModalsendSms
          openModal={openModal}
          setOpenModal={setOpenModal}
          objectId={objectId}
          recordID={recordID}
          setInputSearch={setInputSearch}
          setPage={setPage}
          setValueSearch={setValueSearch}
        />
      </ContentWrap>
    </Wrap>
  );
};

export default ComponentSMS;

const ContentWrap = styled.div`
  width: 100%;
  height: 100%;
  padding: 8px;
  display: flex;
  flex-wrap: nowrap;
  position: relative;
  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-checkbox-indeterminate .ant-checkbox-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const LeftWrap = styled.div`
  width: 40%;
`;

const RightWrap = styled.div`
  width: 60%;
  height: calc(100% - 48px);
  border: 1px solid #ececec;
`;

const RightContent = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;
`;

const CustomButtonAdd = styled.div`
  /* width: calc(100% - 24px); */

  width: 150px;
  cursor: pointer;
  border-radius: 2px;
  background: ${(props) => props.theme.main};
  :hover {
    background: ${(props) => props.theme.darker};
  }
  color: #fff;
  font-size: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.5s;
  padding: 8px 0;
`;

const SearchWrap = styled.div`
  width: calc(100% - 21px);
  margin-top: 16px;
  justify-content: space-between;
  display: flex;
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  gap: 16px;
  flex-wrap: wrap;
`;

const CustomInputSearch = styled(Search)`
  /* width: calc(70% - 17px);
   */
  flex: 2;
  min-width: 160px;
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 30px;
  }
  button {
    height: 40px;
  }
`;

const CustomSelect = styled(Select)`
  /* width: 30%;
   */

  flex: 1;
  min-width: 90px;

  .ant-select-selector {
    height: 40px !important;
    /* border-right: none !important; */
    align-items: center;
  }

  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const ItemWrap = styled.div`
  width: 100%;
  height: calc(100% - 168px);
  overflow-y: auto;
  margin-top: 24px;
`;

const ItemContent = styled.div`
  width: ${(props) => (props.isActive ? "100%" : "calc(100% - 21px)")};
  background: ${(props) => (props.isActive ? "#f0f0f0" : "#ffffff")};
  border: 1px solid #ececec;
  border-radius: ${(props) => (props.isActive ? "5px 0px 0px 5px" : "5px")};
  margin-bottom: 8px;
  padding: ${(props) => (props.isActive ? "16px 37px 16px 16px" : "16px")};
  cursor: pointer;
  transition: all 0.5s;

  box-shadow: ${(props) =>
    props.isActive ? `inset 3px 0px 0px ${props.theme.main}` : "none"};

  :hover {
    background: #f0f0f0;
    box-shadow: inset 3px 0px 0px ${(props) => props.theme.main};
  }
`;

const Top = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;

  span {
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    color: ${(props) => (props.isActive ? props.theme.main : "#2c2c2c")};
  }
`;

const Center = styled.div`
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  word-break: break-all;
  white-space: nowrap;
  font-size: 16px;
  color: #2c2c2c;
`;

const Bottom = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  color: #252424;
  span {
    color: #6b6b6b;
  }
`;

const BottomLeft = styled.div``;

const BottomRight = styled.div``;

const Header = styled.div`
  width: 100%;
  display: flex;
  padding: 16px;
  justify-content: space-between;
  img {
    width: 40px;
    height: 40px;
    object-fit: cover;
    margin-right: 12px;
    border-radius: 50%;
  }
`;

const InfoHeader = styled.div`
  display: flex;
  flex-direction: column;

  span {
    color: #2c2c2c;

    img {
      width: unset;
      height: unset;
      margin-bottom: 4px;
      margin-left: 14px;
    }

    :nth-child(1) {
      font-family: var(--roboto-500);
      font-size: 18px;
      line-height: 26px;
    }
    :nth-child(2) {
      font-size: 16px;
      line-height: 24px;
    }
  }
`;

const Content = styled.div`
  width: 100%;
  padding-left: 16px;
  color: #000000;

  p {
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    margin-bottom: 0px;
  }

  span {
    font-size: 16px;
    white-space: pre-line;
  }
`;

const HeaderWrap = styled.div`
  display: flex;
  align-items: center;
`;

const NoData = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const DataWrap = styled.div`
  p {
    margin-top: 8px;
    text-align: center;
    font-size: 16px;
    line-height: 22px;

    color: #2c2c2c;

    span {
      color: ${(props) => props.theme.main};
    }
  }
`;

const Title = styled.div`
  width: 100%;
  padding: 16px;
  background: #fff;
  height: 36px;
  color: #2c2c2c;
  /* font-family: var(--roboto-500); */
  font-size: 17px;
  border-bottom: 1px solid #d9d9d9;
  display: flex;
  justify-content: flex-start;
  align-items: center;
`;

const Wrap = styled.div`
  width: 100%;
  height: 100%;
`;
