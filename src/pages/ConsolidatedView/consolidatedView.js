import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { Button, Dropdown, Menu, Switch } from "antd";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import CenterScreen from "./centerScreen";
import DrawerLeft from "./drawerLeft";
import DrawerRight from "./drawerRight";
import Draggable from "react-draggable";
import { unmountObjects } from "redux/slices/objects";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import settingIcon from "assets/icons/common/setting-icon.png";
import { changeTitlePage } from "redux/slices/authenticated";
import _ from "lodash";
import { useParams } from "react-router";
import { loadPageTitle } from "redux/slices/consolidatedViewSettings";
import { loadAllUser } from "redux/slices/user";

const ConsolidatedView = () => {
  const [visible, setVisible] = useState(false);
  const [visibleRight, setVisibleRight] = useState(false);
  const [edit, setEdit] = useState(true);
  const [drag, setDrag] = useState(false);
  const dispatch = useDispatch();
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { pageTitle, loadingComponents, isLoadingLoadLayout } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const [editDetail, setEditDetail] = useState(true);
  const { objectId, recordID } = useParams();
  useEffect(() => {
    if (objectId && recordID) {
      dispatch(
        loadPageTitle({
          record_id: recordID,
          object_id: objectId,
        })
      );
    }
  }, [objectId, recordID, dispatch]);

  useEffect(() => {
    if (pageTitle.length > 0) {
      if (pageTitle[0].page_title) {
        dispatch(changeTitlePage(pageTitle[0].page_title));
      } else {
        dispatch(changeTitlePage("Consolidated view"));
      }
    } else {
      dispatch(changeTitlePage("Consolidated view"));
    }
  }, [pageTitle, dispatch]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    setEdit(userDetail.Is_Admin || checkRule("edit"));
    // eslint-disable-next-line
  }, [userDetail]);

  useEffect(() => {
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(
    () => () => {
      dispatch(unmountObjects());
    },
    // eslint-disable-next-line
    []
  );

  return (
    <Wrapper>
      {loadingComponents === false && isLoadingLoadLayout === false ? (
        <>
          <Button
            size="large"
            onClick={() => setVisible(true)}
            className="fixed-btn-left"
          >
            <RightOutlined color="white" />
          </Button>
          <Button
            size="large"
            onClick={() => setVisibleRight(true)}
            className="fixed-btn-right"
          >
            <LeftOutlined color="white" />
          </Button>
        </>
      ) : (
        ""
      )}

      <CenterScreen
        edit={edit}
        drag={drag}
        setEditDetail={setEditDetail}
        editDetail={editDetail}
        checkRule={checkRule}
        setDrag={setDrag}
      />
      {userDetail.Is_Admin && checkRule("edit") ? (
        <Draggable bounds="parent">
          <Dropdown
            trigger="click"
            overlay={
              <div>
                <CustomMenu>
                  <Menu.Item style={{ display: "flex", alignItems: "center" }}>
                    <div>
                      <div>
                        <Switch
                          style={{ marginRight: "10px" }}
                          onClick={() => {
                            setEdit(!edit);
                          }}
                          checked={edit}
                        />
                        Chế độ chỉnh sửa
                      </div>
                    </div>
                  </Menu.Item>
                  <Menu.Item style={{ display: "flex", alignItems: "center" }}>
                    <div>
                      <div>
                        <Switch
                          style={{ marginRight: "10px" }}
                          onClick={() => {
                            setDrag(!drag);
                          }}
                          checked={drag}
                        />
                        Kéo thả
                      </div>
                    </div>
                  </Menu.Item>
                </CustomMenu>
              </div>
            }
            placement="bottomLeft"
          >
            <CustomButton className="draggable" shape="circle" size="large">
              {/* <CloseOutlined style={{ color: "white" }} /> */}
              <img alt="" src={settingIcon} style={{ width: "24px" }} />
            </CustomButton>
          </Dropdown>
        </Draggable>
      ) : (
        ""
      )}
      <DrawerLeft
        visible={visible}
        setVisible={setVisible}
        edit={edit}
        drag={drag}
        setEditDetail={setEditDetail}
        editDetail={editDetail}
      />
      <DrawerRight
        visible={visibleRight}
        setVisible={setVisibleRight}
        edit={edit}
        drag={drag}
        setEditDetail={setEditDetail}
        editDetail={editDetail}
      />
    </Wrapper>
  );
};

export default ConsolidatedView;

const Wrapper = styled.div`
  .fixed-btn-right {
    position: fixed;
    right: -5px;
    background-color: ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    z-index: 10;
    display: flex;
    justify-content: center;
    align-items: center;
    border-top-left-radius: 60px;
    border-bottom-left-radius: 60px;
    top: 290px;
    img {
      width: 15px;
      margin-right: 8px;
    }
    &:hover {
      background-color: ${(props) => props.theme.darker} !important;
      border-color: ${(props) => props.theme.darker};
      color: #fff;
      /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
    }
  }
  .fixed-btn-left {
    position: fixed;
    left: 70px;
    background-color: ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    z-index: 10;
    display: flex;
    justify-content: center;
    align-items: center;
    border-top-right-radius: 60px;
    border-bottom-right-radius: 60px;
    top: 290px;
    img {
      width: 15px;
      margin-right: 8px;
    }
    &:hover {
      background-color: ${(props) => props.theme.darker} !important;
      border-color: ${(props) => props.theme.darker};
      color: #fff;
      /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
    }
  }
`;

const CustomMenu = styled(Menu)`
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  border: none;
  z-index: 20;
  position: absolute;
  bottom: 80px;
  right: 12px;
  filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  :hover {
    background-color: ${(props) => props.theme.main};
    border: none;
    filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  }
  :focus {
    background-color: ${(props) => props.theme.main};
    border: none;
    filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  }
`;
