import { Col, Row } from "antd";
import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  loadLayouts,
  updateLayout,
} from "redux/slices/consolidatedViewSettings";
import { setShowLoadingScreen } from "redux/slices/global";
import styled from "styled-components";
import DetailComponent from "./componentType/details";
import LogsComponent from "./componentType/logs";
import SLAComponent from "./componentType/sla";
import TagsComponent from "./componentType/tags";
import TimelineVertical from "./componentType/timeLine/timelineVertical";
import Comment from "./componentType/comment";
import Enhancement from "./componentType/Enhancement";

const ReactGridLayout = WidthProvider(RGL);

const RightScreen = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  onLayoutChange = function () {},
  edit,
  drag,
  ...props
}) => {
  // eslint-disable-next-line
  const { objectId, recordID } = useParams();
  const {
    // eslint-disable-next-line
    components,
    layouts,
    loadingComponents,
    isLoadingUpdate,
    isLoadingLoadLayout,
  } = useSelector((state) => state.consolidatedViewSettingsReducer);

  const [itemsRight, setItemsRight] = useState([]);

  let dispatch = useDispatch();

  const handleUpdate = (itemsRight) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsRight.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: objectId,
        placement: "right",
        components: result,
      })
    );
  };

  useEffect(() => {
    if (layouts.placement === "right") {
      let tmp = [...layouts.components];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsRight(tmpArr);
    }
  }, [layouts]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
  }, [loadingComponents, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingUpdate));
  }, [isLoadingUpdate, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingLoadLayout));
  }, [isLoadingLoadLayout, dispatch]);

  useEffect(() => {
    dispatch(
      loadLayouts({
        object_id: objectId,
        placement: "right",
      })
    );
  }, [objectId, dispatch]);

  const onRemoveItem = (i) => {
    if (reload < 2) {
      handleUpdate(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    } else if (reload >= 2) {
      handleUpdate(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
      setItemsRight(
        _.reject(itemsRight, function (item) {
          return JSON.parse(item.i)._id === JSON.parse(i)._id;
        })
      );
    }
  };

  const createElement = (el) => {
    return (
      <CustomDiv
        key={el.i}
        data-grid={el}
        style={{
          overflow: "hidden",
          boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
          borderRadius: "10px",
          height: "100%",
        }}
      >
        <div style={{ padding: "0", height: "100%" }}>
          <div
            style={{
              height: "100%",
              background: `${
                JSON.parse(el.i).type === "table" ||
                JSON.parse(el.i).type === "tags" ||
                JSON.parse(el.i).type === "details" ||
                JSON.parse(el.i).type === "logs"
                  ? "white"
                  : "#c4c4c4"
              }`,
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {JSON.parse(el.i).type === "tags" ? (
              <TagsComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                edit={edit}
                onRemoveItem={onRemoveItem}
              />
            ) : JSON.parse(el.i).type === "logs" ? (
              <LogsComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
              />
            ) : JSON.parse(el.i).type === "sla" ? (
              <SLAComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
              />
            ) : JSON.parse(el.i).type === "details" ? (
              <DetailComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                edit={edit}
                onRemoveItem={onRemoveItem}
              />
            ) : JSON.parse(el.i).type === "vertical-timeline" ? (
              <TimelineVertical
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
              />
            ) : JSON.parse(el.i).type === "comment" ? (
              <Comment objectId={objectId} recordID={recordID} />
            ) : JSON.parse(el.i).type === "knowledge" ? (
              <Enhancement
                objectId={objectId}
                recordID={recordID}
                data={JSON.parse(el.i)}
                onRemoveItem={onRemoveItem}
              />
            ) : (
              <img alt="" src={Triangle} style={{ width: "50%" }} />
            )}
          </div>
        </div>
      </CustomDiv>
    );
  };
  // eslint-disable-next-line
  const onAddItem = (newItem) => {
    setItemsRight(itemsRight.concat(newItem));
    handleUpdate(itemsRight.concat(newItem));
  };
  const [reload, setReload] = useState(0);

  const onLayoutChangeNew = (layout) => {
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  // const onRemoveItem = (i) => {
  //   if (reload < 2) {
  //     handleUpdate(_.reject(itemsRight, { i: i }));
  //     setItemsRight(_.reject(itemsRight, { i: i }));
  //   } else if (reload >= 2) {
  //     setItemsRight(_.reject(itemsRight, { i: i }));
  //   }
  // };

  return (
    <Wrapper>
      <Row gutter={16} style={{ height: "100%" }}>
        <Col span={24} style={{ height: "100%" }}>
          <div className="components">
            <div className="components-title">Selected component</div>

            {itemsRight.length === 0 ? (
              <div className="no-data-container">
                <img alt="" src={emptyImg} className="empty-layout-img" />
                <div className="no-data-decs">
                  No data. Select component to add
                </div>
              </div>
            ) : (
              ""
            )}

            <div style={{ height: "100%", overflow: "auto" }}>
              <ReactGridLayout
                onLayoutChange={onLayoutChangeNew}
                {...props}
                isDraggable={drag}
                isResizable={drag}
                rowHeight={10}
                cols={1}
              >
                {_.map(itemsRight, (el) => createElement(el))}
              </ReactGridLayout>
            </div>
          </div>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default RightScreen;

const Wrapper = styled.div`
  height: 100%;
  .no-data-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    position: absolute;
    top: 223px;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 10;
    width: 100%;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
    }
  }
  .components {
    padding: 10px;
    border: 1px solid #d9d9d9;
    box-sizing: border-box;
    border-radius: 5px;
    .active {
      border: 1px solid ${(props) => props.theme.main};
    }
    height: 100%;
  }
  .components-title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;
    margin-top: -25px;
    background-color: white;
    /* Character/Color text main */
    width: fit-content;
    padding-left: 10px;
    padding-right: 10px;
    color: #2c2c2c;
    margin-bottom: 10px;
  }
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
  .react-grid-layout {
    background-color: white;
  }
  .react-grid-item:not(.react-grid-placeholder) {
    background: #fff;
    border: none;
  }
  .close-png {
    :hover {
      cursor: pointer;
    }
  }
`;

// const CustomItem = styled.div`
//   position: relative;
//   :hover {
//     .blur {
//       width: 100%;
//       height: 100%;
//       background: rgba(0, 0, 0, 0.7);
//       visibility: visible;
//       opacity: 1;
//     }

//     .delete-container {
//       visibility: visible;
//       opacity: 1;
//       height: 100%;
//       width: 100%;
//       position: absolute;
//       top: 50%;
//       left: 50%;
//       transform: translate(-50%, -50%);
//       display: flex;
//       justify-content: center;
//       align-items: center;
//       button {
//         cursor: pointer;
//         width: 156px;
//         height: 46px;
//         display: flex;
//         justify-content: center;
//         align-items: center;
//         background: #ffffff;
//         border-radius: 3px;
//         border: none;
//         img {
//           width: 20px;
//         }
//       }
//     }
//   }
//   .blur {
//     visibility: hidden;
//     opacity: 0;
//     transition: visibility 0s, opacity 0.5s linear;
//   }

//   .delete-container {
//     visibility: hidden;
//     opacity: 0;
//     transition: visibility 0s, opacity 0.5s linear;
//   }
// `;
const CustomDiv = styled.div`
  width: calc(100% - 24px) !important;
`;
