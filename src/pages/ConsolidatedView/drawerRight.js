import { Drawer } from "antd";
import React from "react";
import RightSceen from "./rightScreen";
import styled from "styled-components";

const DrawerRight = (props) => {
  const { visible, setVisible, edit, drag, setEditDetail, editDetail } = props;

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <CustomDrawer
        title="Drawer right components"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={600}
      >
        <RightSceen
          edit={edit}
          drag={drag}
          setEditDetail={setEditDetail}
          editDetail={editDetail}
        />
      </CustomDrawer>
    </>
  );
};

export default DrawerRight;

const CustomDrawer = styled(Drawer)`
  .ant-drawer-content-wrapper {
    transform: none !important;
  }
`;
