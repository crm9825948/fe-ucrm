import { Drawer } from "antd";
import React from "react";
import LeftScreen from "./leftScreen";
import styled from "styled-components";

const DrawerLeft = (props) => {
  const { visible, setVisible, edit, drag, setEditDetail, editDetail } = props;

  const onClose = () => {
    setVisible(false);
  };

  return (
    <>
      <CustomDrawer
        title="Drawer left components"
        placement="left"
        onClose={onClose}
        visible={visible}
        width={600}
      >
        <LeftScreen
          edit={edit}
          drag={drag}
          setEditDetail={setEditDetail}
          editDetail={editDetail}
        />
      </CustomDrawer>
    </>
  );
};

export default DrawerLeft;

const CustomDrawer = styled(Drawer)`
  .ant-drawer-content-wrapper {
    transform: none !important;
  }
`;
