import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import Email from "components/Email/Email2";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch } from "react-redux";
import { updateTab } from "redux/slices/consolidatedViewSettings";
import DetailComponent from "./componentType/details";
import DetailsThirdParty from "./componentType/detailsThirdParty";
import LogsComponent from "./componentType/logs";
import SLAComponent from "./componentType/sla";
import TableComponent from "./componentType/table1";
import TableThirdParty from "./componentType/tableThirdParty";
import TagsComponent from "./componentType/tags";
import TimelineHorizontal from "./componentType/timeLine/timelineHorizontal";
import TimelineVertical from "./componentType/timeLine/timelineVertical";
import DrawerComponentToTab from "./drawerAddComponentToTab";
import Comment from "./componentType/comment";
import MultiDetail from "./componentType/multiDetail";

const ReactGridLayout = WidthProvider(RGL);

const TabComponent = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  onLayoutChange = function () {},
  edit,
  drag,
  setDrag,
  ...props
}) => {
  const [visibleTab, setVisibleTab] = useState(false);
  const [items, setItems] = useState([]);
  const {
    data,
    name,
    itemParent,
    setItemParent,
    objectId,
    recordID,
    editDetail,
    setEditDetail,
  } = props;
  let dispatch = useDispatch();

  const handleUpdate = (items) => {
    let result = [];
    /*eslint-disable-next-line*/
    items.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });

    let dataTemp = { ...data };

    dataTemp.tab_component = { ...(data.tab_component || {}), [name]: result };

    let tmpItems = [...itemParent];
    let newItems = [];
    /*eslint-disable-next-line*/
    tmpItems.forEach((ele, i) => {
      let newEle = { ...ele };
      if (data._id === JSON.parse(ele.i)._id) {
        newEle = { ...ele, i: JSON.stringify(dataTemp) };
      }
      newItems.push(newEle);
    });

    setItemParent(newItems);

    dispatch(
      updateTab({
        ...dataTemp,
        id: data._id,
      })
    );
  };

  useEffect(() => {
    let tmp = [
      ...((data && data.tab_component && data.tab_component[name]) || []),
    ];
    let tmpArr = [];
    /*eslint-disable-next-line*/
    tmp.forEach((item, idx) => {
      if (item.y === null) {
        let newItem = {
          ...item,
          y: Infinity,
        };
        tmpArr.push(newItem);
      } else {
        tmpArr.push(item);
      }
    });

    setItems(tmpArr);
    /*eslint-disable-next-line*/
  }, [data]);

  const createElement = (el) => {
    return (
      <div
        key={el.i}
        data-grid={el}
        style={{
          overflow: "auto",
          // boxShadow:
          //   JSON.parse(el.i).type === "email"
          //     ? ""
          //     : "0px 0px 16px rgba(0, 0, 0, 0.16)",
          border:
            JSON.parse(el.i).type === "email"
              ? ""
              : "1px solid rgba(0, 0, 0, 0.16)",
          borderRadius: "10px",
          height: "100%",
        }}
      >
        <div style={{ padding: "0", height: "100%" }}>
          <div
            style={{
              height: "100%",
              background: `${
                JSON.parse(el.i).type === "table" ||
                JSON.parse(el.i).type === "tags" ||
                JSON.parse(el.i).type === "details" ||
                JSON.parse(el.i).type === "logs" ||
                JSON.parse(el.i).component_type === "TABLE"
                  ? "white"
                  : "#c4c4c4" || JSON.parse(el.i).component_type === "DETAILS"
                  ? "white"
                  : "#c4c4c4"
              }`,
              display: JSON.parse(el.i).type === "email" ? "" : "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            {JSON.parse(el.i).type === "table" ? (
              <TableComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                edit={edit}
                setDrag={setDrag}
              />
            ) : JSON.parse(el.i).type === "tags" ? (
              <TagsComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                edit={edit}
                setDrag={setDrag}
              />
            ) : JSON.parse(el.i).type === "logs" ? (
              <LogsComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
              />
            ) : JSON.parse(el.i).type === "sla" ? (
              <SLAComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
              />
            ) : JSON.parse(el.i).type === "details" ? (
              <DetailComponent
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
                edit={edit}
                editDetail={editDetail}
                setEditDetail={setEditDetail}
              />
            ) : JSON.parse(el.i).type === "multi_detail" ? (
              <MultiDetail
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
                edit={edit}
                editDetail={editDetail}
                setEditDetail={setEditDetail}
                // onRemoveItem={onRemoveItem}
              />
            ) : JSON.parse(el.i).component_type === "TABLE" ? (
              <TableThirdParty
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
                edit={edit}
                // onRemoveItem={onRemoveItem}
              />
            ) : JSON.parse(el.i).component_type === "DETAILS" ? (
              <DetailsThirdParty
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                setDrag={setDrag}
                edit={edit}
                // onRemoveItem={onRemoveItem}
              />
            ) : JSON.parse(el.i).type === "email" ? (
              <Email record_id={recordID} object_id={objectId} />
            ) : JSON.parse(el.i).type === "horizontal-timeline" ? (
              <TimelineHorizontal
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                type="horizontal-timeline"
              />
            ) : JSON.parse(el.i).type === "horizontal-timeline-new" ? (
              <TimelineHorizontal
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                type="horizontal-timeline-new"
              />
            ) : JSON.parse(el.i).type === "vertical-timeline" ? (
              <TimelineVertical
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                type="vertical-timeline"
              />
            ) : JSON.parse(el.i).type === "vertical-timeline-new" ? (
              <TimelineVertical
                data={JSON.parse(el.i)}
                recordID={recordID}
                objectId={objectId}
                type="vertical-timeline-new"
              />
            ) : JSON.parse(el.i).type === "comment" ? (
              <Comment recordID={recordID} objectId={objectId} />
            ) : (
              <img alt="" src={Triangle} style={{ width: "50%" }} />
            )}
          </div>
        </div>
      </div>
    );
  };

  const onAddItem = (newItem) => {
    setItems(items.concat(newItem));
    handleUpdate(items.concat(newItem));
  };
  const [reload, setReload] = useState(0);

  const onLayoutChangeNew = (layout) => {
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  return (
    <div>
      {items.length === 0 ? (
        <div className="no-data-container-tab">
          <img alt="" src={emptyImg} className="empty-layout-img" />
          <div className="no-data-decs">No data.</div>
          {/* <CustomButtonAddRecord
            size="large"
            onClick={() => {
              setVisibleTab(true);
            }}
          >
            <img alt="" src={plusIcon} />
            Add new
          </CustomButtonAddRecord> */}
        </div>
      ) : (
        <div style={{ height: "1300px", overflow: "auto" }}>
          <ReactGridLayout
            onLayoutChange={onLayoutChangeNew}
            {...props}
            isDraggable={false}
            isResizable={drag}
          >
            {_.map(items, (el) => createElement(el))}
          </ReactGridLayout>
        </div>
      )}
      <DrawerComponentToTab
        visible={visibleTab}
        setVisible={setVisibleTab}
        onAddItem={onAddItem}
        items={[]}
      />
    </div>
  );
};

export default TabComponent;

// const CustomButtonAddRecord = styled(Button)`
//   background-color: ${(props) => props.theme.main};
//   color: #fff;
//   font-size: 16px;
//   img {
//     width: 15px;
//     margin-right: 8px;
//   }
//   &:hover {
//     background-color: ${(props) => props.theme.darker} !important;
//     border-color: ${(props) => props.theme.darker};
//     color: #fff;
//     /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
//     transition: 0.5s; */
//   }
// `;
