import { UploadOutlined } from "@ant-design/icons";
import { Button, Form, Upload } from "antd";
import { BASE_URL_API, BE_URL } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { Notification } from "components/Notification/Noti";

const File = (props) => {
  const { field, form, recordID, open, recordData } = props;
  const { objectId } = useParams();
  const { showModal } = useSelector((state) => state.objectsReducer);
  const [fileList, setFileList] = useState([]);

  useEffect(() => {
    if (showModal === true || open === true) {
      let value = recordData[field.ID]?.value;
      if (recordID && value) {
        let newArrayFile = [];
        let newListFile = [];
        // eslint-disable-next-line
        if (typeof value === "string") {
          value = [value];
        }
        value &&
          // eslint-disable-next-line
          value.map((item, idx) => {
            if (typeof item === "string") {
              let nameArray = item.split("/");
              let newItem = {
                uid: Math.random() * 10000 + "",
                name: nameArray[nameArray.length - 1],
                status: "success",
                url: item,
              };
              newArrayFile.push(newItem);
              newListFile.push(newItem.url);
            }
          });
        // setDefaultFileList(newArrayFile);
        setFileList(newArrayFile);
        form.setFieldsValue({
          [field.ID]: newListFile,
        });
      } else {
        setFileList([]);
        form.setFieldsValue({
          [field.ID]: [],
        });
      }
    }
    // eslint-disable-next-line
  }, [showModal, recordID, open, form, recordData]);

  // useEffect(() => {
  // if (!showModal && !open) {
  //   setFileList([]);
  //   form.setFieldsValue({
  //     [field.ID]: [],
  //   });
  // }
  // if (!open) {
  //   setFileList([]);
  //   form.setFieldsValue({
  //     [field.ID]: [],
  //   });
  // }
  // eslint-disable-next-line
  // }, [showModal, open]);

  const info = {
    name: "file",
    action: BASE_URL_API + "upload-file",
    headers: {
      Authorization: localStorage.getItem("setting_accessToken"),
    },
    data: {
      obj: objectId,
    },
    defaultFileList: fileList,
    onChange(info) {
      const { fileList: newFileList } = info;

      /* eslint-disable-next-line */
      newFileList.map((file, idx) => {
        if (file.url === undefined) {
          file.url =
            BASE_URL_API + file &&
            file.response &&
            file.response.data &&
            file.response.data[0];
          file.url = BE_URL + file.url;
        }
      });
      setFileList(newFileList);
    },
  };

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Upload
        {...info}
        maxCount={10}
        multiple
        fileList={fileList}
        // accept="'image/*',
        // '.png, .jpg, .jpeg, .jfif'
        // 'audio/*',
        // 'video/*',
        // '.pdf, application/pdf',
        // '.doc,.docx,.xml,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        // '.xlsx, .xls, .csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        beforeUpload={(file) => {
          const isPNG =
            file.type === "image/png" ||
            file.type === "image/jpeg" ||
            file.type === "image/jpg" ||
            file.type.includes("csv") ||
            file.type.includes("doc") ||
            file.type.includes("pdf") ||
            file.type.includes("txt") ||
            file.type.includes("pptx") ||
            file.type.includes("xlsx") ||
            file.type.includes("gif") ||
            file.type.includes("docx") ||
            file.type.includes("ppsx") ||
            file.type.includes("mp3") ||
            file.type.includes("docm") ||
            file.type.includes("xml") ||
            file.type.includes("xls") ||
            file.type.includes("jfif") ||
            file.type.includes("mp4") ||
            file.type === "" ||
            file.type.includes("img") ||
            file.type.includes("ppt");
          const isLt5M = file.size / 1024 / 1024 < 50;
          if (!isLt5M) {
            Notification("error", "File must smaller than 50MB!");
          }

          if (!isPNG) {
            Notification("error", "Invalid file type!");
          }
          return (isLt5M && isPNG) || Upload.LIST_IGNORE;
        }}
      >
        <Button
          icon={<UploadOutlined />}
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
        >
          Click to upload
        </Button>
      </Upload>
    </Form.Item>
  );
};

export default File;
