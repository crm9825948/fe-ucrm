import { Button, Col, Form, Modal, Row, Select, Menu, Dropdown } from "antd";
import moment from "moment";
import Date1 from "pages/Objects/modal/fieldsType/date";
import Datetime from "pages/Objects/modal/fieldsType/dateTime";
import DynamicField from "pages/Objects/modal/fieldsType/dynamicField";
import Email from "pages/Objects/modal/fieldsType/email";
import File from "pages/Objects/modal/fieldsType/file";
import FormulaField from "pages/Objects/modal/fieldsType/formulaField";
import IDComp from "pages/Objects/modal/fieldsType/ID";
import LinkingObject from "pages/Objects/modal/fieldsType/linkingObject";
import Number from "pages/Objects/modal/fieldsType/number";
import SelectType from "pages/Objects/modal/fieldsType/select";
import Text from "pages/Objects/modal/fieldsType/text";
import Textarea from "pages/Objects/modal/fieldsType/textarea";
import User from "pages/Objects/modal/fieldsType/user";
import Lookup from "pages/Objects/modal/fieldsType/lookup";
import axios from "axios";
import { BASE_URL_API, BE_URL, FE_URL } from "constants/constants";
import { Notification } from "components/Notification/Noti";
import { checkTokenExpiration } from "contexts/TokenCheck";
import React, { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createRecord,
  loadUserAssignTo,
  setHiddenArray,
  setLinkingFieldValue,
  updateRecord,
  loadRecordDataSuccess,
  loadFormCreate,
  getManualSharing,
  loadTemplate,
} from "redux/slices/objects";
import styled from "styled-components";
import { runDynamicButtonResult } from "redux/slices/dynamicButton";
import IconDown from "assets/icons/common/icon-down.svg";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import { runDynamicButton } from "redux/slices/dynamicButton";
import ImgConfirm from "assets/icons/common/confirm.png";
import ShareImg from "assets/icons/common/share.png";
import ModalManualShare from "pages/Objects/modal/ModalManualShare";
import { useCallback } from "react";
import _ from "lodash";
import ModalRunDynamicButton from "components/Modal/ModalRunDynamicButton";

const { Option } = Select;

const ModalRecord = (props) => {
  // const [form] = Form.useForm();
  const {
    open,
    setOpen,
    recordPerPage,
    currentPage,
    form,
    recordID,
    setRecordID,
    objectId,
    initData,
    fields,
    sortBy,
    interaction,
    objectIDInteraction,
    reload,
    loadData,
    setFields,
  } = props;
  //   const { objectId } = useParams();
  const [openModal, setOpenModal] = useState(null);
  const [dataConfirm, setDataConfirm] = useState({});
  const [showConfirm, setShowConfirm] = useState(false);
  const [modalManual, setModalManual] = useState(false);
  const dispatch = useDispatch();
  // const [form] = Form.useForm();

  const { isRunSuccess, isReload } = useSelector(
    (state) => state.dynamicButtonReducer
  );
  const { allObject } = useSelector((state) => state.tenantsReducer);

  const [userDynamicButton, $userDynamicButton] = useState([]);
  const [visibleModalRDB, $visibleModalRDB] = useState(false);

  const { userDetail } = useSelector((state) => state.userReducer);
  const [dataFilterLinking, setDataFilterLinking] = useState({});

  const [numberOfFields, setNumberOfFields] = useState(0);
  const [formValue, setFormValue] = useState({});
  const [valueModal, setValueModal] = useState({});
  const {
    // fields,
    isLoading,
    userAssignTo,
    searchList,
    linkingFieldValue,
    recordData,
    originalFields,
    hiddenArray,
    hiddenDynamic,
    isLoadingUpdateRecord,
    isLoadingCreateRecord,
    isLoadingDeleteRecord,
    listObjectField,
    template,
  } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    setValueModal({
      ...form.getFieldsValue(),
      ...linkingFieldValue,
    });
  }, [linkingFieldValue, form]);

  const [allFields, setAllFields] = useState({});

  const [header, setHeader] = useState([]);
  const [show, setShow] = useState(false);

  const [templateId, setTemplateId] = useState("");
  const [templateData, setTemplateData] = useState({});

  const handleFormatToDay = (type) => {
    var currentdate = new Date();
    if (type === "date") {
      let result = "%Y-%m-%d";
      result = result.replace("%Y", currentdate.getFullYear().toString());
      result = result.replace("%m", (currentdate.getMonth() + 1).toString());
      result = result.replace("%d", currentdate.getDate());
      return result;
    } else if (type === "datetime-local") {
      let result = "%Y-%m-%d %H:%M:%S";
      result = result.replace("%Y", currentdate.getFullYear().toString());
      result = result.replace("%m", (currentdate.getMonth() + 1).toString());
      result = result.replace("%d", currentdate.getDate().toString());

      result = result.replace("%H", currentdate.getHours().toString());
      result = result.replace("%M", currentdate.getMinutes().toString());
      result = result.replace("%S", currentdate.getSeconds().toString());
      return result;
    }
  };

  useEffect(() => {
    if (Object.entries(recordData).length > 0) {
      if (recordData.owner === userDetail._id) {
        setShow(true);
      } else {
        setShow(false);
      }
    }
  }, [recordData, userDetail]);

  useEffect(() => {
    if (listObjectField.length > 0) {
      let objectFields = {};
      /*eslint-disable-next-line*/
      listObjectField.map((object, idx) => {
        if (listObjectField.length - 1 === idx) {
          objectFields["main_object"] = [];
          /*eslint-disable-next-line*/
          object["main_object"].sections.map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((field, index) => {
              objectFields["main_object"].push(field);
            });
          });
        } else {
          /*eslint-disable-next-line*/
          Object.entries(object).forEach(([key, value], idx) => {
            objectFields[object[key].object_name] = [];
            /*eslint-disable-next-line*/
            return object[key].sections.forEach((section, idx) => {
              /*eslint-disable-next-line*/
              return section.fields.forEach((field, index) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false
                ) {
                  objectFields[object[key].object_name].push(field);
                } else if (field.permission_hidden === undefined) {
                  objectFields[object[key].object_name].push(field);
                }
              });
            });
          });
        }
      });

      // setFields(objectFields);
      let newObj = {};
      objectFields["main_object"].forEach((item, idx) => {
        newObj[item.ID] = { ...item };
        newObj[item.ID].name = "";
      });
      setAllFields(newObj);
      setHeader(objectFields["main_object"]);
    }
  }, [listObjectField]);

  useEffect(() => {
    if (openModal) {
      let newObj = {};
      /* eslint-disable-next-line */
      Object.entries(allFields).forEach(([key, value], index) => {
        if (value.type === "linkingobject") {
          newObj[key] = { ...recordData[key] };
        }
      });
      dispatch(setLinkingFieldValue(newObj));
    }

    /* eslint-disable-next-line */
  }, [recordData, openModal]);

  const loadForm = useCallback(() => {
    if (objectId)
      dispatch(
        loadFormCreate({
          object_id: objectId,
        })
      );
    dispatch(loadUserAssignTo());
  }, [objectId, dispatch]);

  useEffect(() => {
    loadForm();
  }, [loadForm]);

  useEffect(() => {
    if (open) {
      dispatch(
        loadTemplate({
          object_id: objectId,
        })
      );
    }
  }, [open, dispatch, objectId]);

  useEffect(() => {
    let numberOfFields = 0;
    /* eslint-disable-next-line */
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((field, idx) => {
        if (field.section_name !== "Meta fields") {
          numberOfFields = numberOfFields + field.fields.length;
        }
      });
    setNumberOfFields(numberOfFields);
  }, [listObjectField]);
  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      form.setFieldsValue({
        assignTo: recordData.owner,
      });
      setFormValue(form.getFieldsValue());
      setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"] &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((sections, idx) => {
          /*eslint-disable-next-line*/
          sections.fields.map((field, idx) => {
            if (field.type === "dynamic-field") {
              if (recordData[field.ID]?.value) {
                /*eslint-disable-next-line*/
                field.list_items[recordData[field.ID].value].map(
                  /*eslint-disable-next-line*/
                  (item, idx) => {
                    tmpHidden = tmpHidden.filter((ele) => ele !== item);
                  }
                );
              }
            }
          });
        });
      dispatch(setHiddenArray(tmpHidden));
    }
    /* eslint-disable-next-line */
  }, [recordData, recordID, fields]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/load-form-create",
          {
            object_id: objectId,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((response) => {
          let hiddenArray1 = [];
          /*eslint-disable-next-line*/
          response.data.data.forEach((sections, idx) => {
            /*eslint-disable-next-line*/
            sections.fields.forEach((item, idx) => {
              if (item.type === "dynamic-field") {
                /*eslint-disable-next-line*/
                if (item.list_items)
                  /*eslint-disable-next-line*/
                  Object.entries(item.list_items).forEach(
                    ([key, value], idx) => {
                      hiddenArray1 = [...hiddenArray1, ...value];
                    }
                  );
              }
            });
          });

          let tempField = [];
          let tempFieldHidden = [];
          let newObj = {};
          /*eslint-disable-next-line*/
          response.data.data.map((sections, idx) => {
            // tempField.push(sectiosetFieldObjectns);
            tempField[idx] = { ...sections };
            tempField[idx].fields = [];

            tempFieldHidden[idx] = { ...sections };
            tempFieldHidden[idx].fields = [];
            /*eslint-disable-next-line*/
            sections.fields.map((item, index) => {
              newObj[item.ID] = { ...item };
              if (hiddenArray1.findIndex((ele) => ele === item._id) < 0) {
                tempField[idx].fields.push(item);
              }
              tempFieldHidden[idx].fields.push(item);
            });
          });

          setFields(tempFieldHidden);
          setHiddenArray(hiddenArray1);
        })
        .catch((err) => {});
    };
    if (objectId) {
      checkToken();
    }
    /*eslint-disable-next-line*/
  }, [objectId]);

  useEffect(() => {
    if (recordID === "") {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, index) => {
          if (field.type === "dynamic-field" && field.default_value) {
            form.setFieldsValue({
              [field.ID]: field.default_value,
            });
            /*eslint-disable-next-line*/
            field.list_items[field.default_value].map((item, idx) => {
              hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
            });
            dispatch(setHiddenArray(hiddenTmp));
          } else if (field.type === "select" && field.default_value) {
            form.setFieldsValue({
              [field.ID]: field.default_value,
            });
          }
        });
      });
      setFormValue(form.getFieldsValue());
    }
    if (open) {
      setValueModal(form.getFieldsValue());
    }
    /*eslint-disable-next-line*/
  }, [recordID, open]);

  useEffect(() => {
    originalFields &&
      /*eslint-disable-next-line*/
      originalFields.map((section, idx) => {
        /*eslint-disable-next-line*/
        section.fields.map((field, idx) => {
          if (field.hidden === false && field.type === "dynamic-field") {
            form.setFieldsValue({
              [field.ID]: "",
            });
          }
        });
      });
    /*eslint-disable-next-line*/
  }, [originalFields]);

  useEffect(() => {
    //find default template
    let defaultTemplate = false;
    let templateDataDefault = {};
    template.forEach((item, idx) => {
      if (item.is_default && recordID === "") {
        setTemplateId(item._id);
        setTemplateData({ ...item });
        templateDataDefault = { ...item.record_values };
        defaultTemplate = true;
      } else return null;
    });

    if (recordID === "" && defaultTemplate === false) {
      // let hiddenTmp = [...hiddenArray];
      // console.log("i cuming");
      // /*eslint-disable-next-line*/
      // listObjectField.length > 0 &&
      //   listObjectField[listObjectField.length - 1]["main_object"][
      //     "sections"
      //     /*eslint-disable-next-line*/
      //   ].map((sections, idx) => {
      //     /*eslint-disable-next-line*/
      //     sections.fields.map((field, index) => {
      //       if (field.type === "dynamic-field" && field.default_value) {
      //         form.setFieldsValue({
      //           [field.ID]: field.default_value,
      //         });
      //         /*eslint-disable-next-line*/
      //         field.list_items[field.default_value].map((item, idx) => {
      //           hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
      //         });
      //         dispatch(setHiddenArray(hiddenTmp));
      //       } else if (field.type === "select" && field.default_value) {
      //         form.setFieldsValue({
      //           [field.ID]: field.default_value,
      //         });
      //       }
      //     });
      //   });
      // setFormValue("");
    } else if (defaultTemplate) {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((sections, idx) => {
          /*eslint-disable-next-line*/
          sections.fields.map((field, index) => {
            if (
              field.type === "dynamic-field" &&
              templateDataDefault[field.ID] &&
              templateDataDefault[field.ID].value
            ) {
              form.setFieldsValue({
                [field.ID]: templateDataDefault[field.ID].value,
              });
              /*eslint-disable-next-line*/
              field.list_items[templateDataDefault[field.ID].value]?.map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
                }
              );
              dispatch(setHiddenArray(hiddenTmp));
            } else if (
              field.type === "select" &&
              templateDataDefault[field.ID] &&
              templateDataDefault[field.ID].value
            ) {
              form.setFieldsValue({
                [field.ID]: templateDataDefault[field.ID].value,
              });
            }
          });
        });
    }
    if (open) {
      setValueModal(form.getFieldsValue());
    }
    /*eslint-disable-next-line*/
  }, [recordID, open, listObjectField]);

  useEffect(() => {
    let editorField = {};
    console.log(templateId);
    if (
      templateId &&
      Object.keys(templateData).length > 0 &&
      open &&
      Object.keys(allFields).length > 0
    ) {
      // dispatch(
      //   loadRecordDataSuccess({ ...recordData, ...templateData.record_values })
      // );

      //handle dynamic field
      // setFormValue(form.getFieldsValue());
      // setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];

      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (templateData.record_values[field.ID]?.value) {
              /*eslint-disable-next-line*/
              field.list_items[templateData.record_values[field.ID]?.value].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      dispatch(setHiddenArray(tmpHidden));

      let record = {};

      let objectCombine = { ...recordData, ...form.getFieldsValue() };

      if (recordID) {
        Object.entries(objectCombine).forEach(([key, value]) => {
          // console.log(value);
          if (value !== null && typeof value === "object") {
            record[key] = value.value;
          } else {
            record[key] = value;
          }
        });
      }

      Object.entries(templateData.record_values).forEach(([key, value]) => {
        if (value !== null && typeof value === "object") {
          record[key] = value.value;
        } else {
          record[key] = value;
        }
      });

      let newObj = {};
      /* eslint-disable-next-line */
      Object.entries(allFields).forEach(([key, value], index) => {
        if (
          value.type === "linkingobject" &&
          templateData.record_values[key]?.id_field_related_record !== null
        ) {
          newObj[key] = {
            ...templateData.record_values[key],
          };
        } else if (
          value.type === "lookup" &&
          templateData.record_values[key]?.id_field_related_record !== null
        ) {
          record[key] = {
            ...templateData.record_values[key],
          };
        }
      });

      let temp = { ...linkingFieldValue, ...newObj };
      dispatch(setLinkingFieldValue(temp));

      // form.resetFields();
      let newRecord = { ...record };

      /* eslint-disable-next-line */
      Object.entries(record).forEach(([key, value], index) => {
        if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "datetime-local"
        ) {
          if (record[key]) {
            if (record[key] === "today") {
              // newRecord[key] = "today";
              handleFormatToDay("datetime-local");
              newRecord[key] = record[key]
                ? moment(handleFormatToDay("datetime-local"))
                : null;
            } else {
              newRecord[key] = record[key] ? moment(record[key]) : null;
            }
          } else {
            newRecord[key] = null;
          }
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "date"
        ) {
          if (record[key]) {
            if (record[key] === "today") {
              // newRecord[key] = "today";
              newRecord[key] = record[key]
                ? moment(handleFormatToDay("date"))
                : null;
            } else {
              newRecord[key] = record[key] ? moment(record[key]) : null;
            }
          } else {
            newRecord[key] = null;
          }
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "textarea" &&
          allFields &&
          allFields[key] &&
          allFields[key].is_editor === true
        ) {
          editorField = { ...allFields[key] };
        }
      });
      console.log("new new new Record", newRecord, newObj);
      if (recordID) {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });

        let finalTempObject = { ...newRecord, ...newObj };
        console.log("finalTempObject", finalTempObject);
        Object.keys(finalTempObject).forEach((key) => {
          if (
            typeof finalTempObject[key] === "object" &&
            finalTempObject[key]?.value === null
          ) {
            delete finalTempObject[key];
          } else if (
            finalTempObject[key] === null ||
            finalTempObject[key] === "<p><br></p>"
          ) {
            delete finalTempObject[key];
          }
        });

        form.setFieldsValue({
          assignTo: userDetail._id,
        });
        // setFormValue({
        //   ...formValue,
        //   assignTo: userDetail._id,
        //   ...finalTempObject,
        // });
        console.log("formValue", formValue);
        let oldValue;
        if (Object.keys(editorField).length > 0) {
          let newObject = {
            ...formValue,
            assignTo: userDetail._id,
            ...form.getFieldsValue(),
            ...finalTempObject,
          };
          oldValue = newObject[editorField.ID];

          console.log(newObject);

          Object.entries(newObject).map(([key, value], index) => {
            if (
              (allFields &&
                allFields[key] &&
                allFields[key].type === "datetime") ||
              (allFields &&
                allFields[key] &&
                allFields[key].type === "datetime-local")
            ) {
              oldValue = oldValue?.replace(
                `$${key}`,
                typeof value !== "object" && value
                  ? ""
                  : moment(value).format(
                      allFields &&
                        allFields[key] &&
                        allFields[key].type === "datetime"
                        ? "YYYY-MM-DD"
                        : "YYYY-MM-DD HH:mm:ss"
                    )
              );
            } else {
              oldValue = oldValue?.replace(
                `$${key}`,
                typeof value !== "object" && value ? value : value?.value
              );
            }
            oldValue = oldValue?.replace("undefined", "");
            oldValue = oldValue?.replace("null", "");
            return null;
          });
        }
        if (Object.keys(editorField).length > 0) {
          form.setFieldsValue({
            assignTo: userDetail._id,
            ...formValue,
            ...finalTempObject,
            [editorField["ID"]]: oldValue,
          });
        } else {
          form.setFieldsValue({
            assignTo: userDetail._id,
            ...formValue,
            ...finalTempObject,
          });
        }
      } else {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });

        setFormValue({ assignTo: userDetail._id, ...newRecord, ...newObj });
        form.setFieldsValue({ ...newRecord, ...newObj });
      }
      // setEditingKey(record.key);
    }
    /* eslint-disable-next-line */
  }, [
    templateId,
    templateData,
    userDetail,
    recordID,
    dispatch,
    allFields,
    form,
  ]);

  const normalizeLink = (value) => {
    if (
      allObject?.find(
        (object) => object?.object_id === objectId && object?.status
      )
    ) {
      if (value.split(BE_URL)?.length > 1) {
        return value;
      } else {
        return BE_URL + value?.split(FE_URL)[1];
      }
    } else {
      return value;
    }
  };

  const onFinish = (values) => {
    values = { ...formValue, ...values };
    let fieldsObject = {};
    /* eslint-disable-next-line */

    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        // section.fields.map((section, idx) => {
        /* eslint-disable-next-line */
        section.fields.map((item, index) => {
          fieldsObject[item.ID] = { ...item };
        });
        // });
      });

    let listValue = [];
    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key]?.type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              if (item.url === undefined) {
                item.url =
                  BASE_URL_API + item &&
                  item.response &&
                  item.response.data &&
                  item.response.data[0];
                item.url = BE_URL + item.url;
              }
              result.push(normalizeLink(item.url));
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(normalizeLink(item));
                return null;
              });
            } else {
              // eslint-disable-next-line
              value.map((item, idx) => {
                if (item.url) {
                  result.push(normalizeLink(item.url));
                } else {
                  result.push(normalizeLink(item));
                }
              });
            }
          }

          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key]?.type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key]?.type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key]?.type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key]?.type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    if (Object.entries(recordData).length > 0 && recordID) {
      dispatch(
        updateRecord({
          data: {
            data: listValue,
            owner_id: values["assignTo"],
            id: recordData._id,
            object_id: objectId,
            share_to: values.share_to,
            action_type: values.action_type,
            subject: values.subject,
            permission: values.permission,
          },
          load: {
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
            search_with: {
              meta: [],
              data: searchData,
            },
          },
          type: window.location.pathname.includes("list-view-with-details")
            ? "reload"
            : "",
        })
      );
      loadData();
      setRecordID("");
      dispatch(loadRecordDataSuccess({}));
      setOpen(false);
      setTemplateId("");
      // initData(currentPage, recordPerPage, searchData, sortBy);
    } else {
      if (interaction) {
        dispatch(
          createRecord({
            data: {
              object_id: objectId,
              data: listValue,
              owner: values["assignTo"],
              interaction_data: interaction,
            },
            loadInteraction: {
              object_id: objectIDInteraction,
              interaction_id: interaction._id,
            },
          })
        );
      } else {
        dispatch(
          createRecord({
            data: {
              object_id: objectId,
              data: listValue,
              owner: values["assignTo"],
            },
            load: {
              object_id: objectId,
              current_page: currentPage,
              record_per_page: recordPerPage,
              search_with: {
                meta: [],
                data: searchData,
              },
            },
            type: window.location.pathname.includes("list-view-with-details")
              ? "reload"
              : "",
          })
        );
      }

      dispatch(loadRecordDataSuccess({}));
      setOpen(false);
      form.resetFields();
      setRecordID("");
      setTemplateId("");
      // initData(currentPage, recordPerPage, searchData, sortBy);
    }
    form.resetFields();
  };

  useEffect(() => {
    if (
      (isLoadingCreateRecord === false ||
        isLoadingUpdateRecord === false ||
        isLoadingDeleteRecord === false) &&
      open
    ) {
      let searchData = [];
      /* eslint-disable-next-line */
      Object.entries(searchList).forEach(([key, value], index) => {
        if (value) {
          let newItem = {
            id_field: key,
            value: value,
          };
          searchData.push(newItem);
        }
      });
      initData(currentPage, recordPerPage, searchData, sortBy);
    }
    /* eslint-disable-next-line */
  }, [isLoadingUpdateRecord, isLoadingCreateRecord]);

  useEffect(() => {
    setOpenModal(open);
  }, [open]);

  useEffect(() => {
    if (open) {
      if (recordID) {
      } else {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });
      }
    }
    /*eslint-disable-next-line*/
  }, [userDetail, open]);

  const menuActions = (
    <Menu>
      {userDynamicButton.map((button, idx) => {
        return (
          <Menu.Item key={idx} onClick={() => _onRunButton(button)}>
            <span>{button.name}</span>
          </Menu.Item>
        );
      })}
    </Menu>
  );

  const _onRunButton = (button) => {
    if (_.get(button, "dynamic_field", []).length > 0) {
      $visibleModalRDB(true);
      setDataConfirm({
        button: button,
        data: {
          data: {
            object_id: objectId,
            record_id: recordID,
            button_id: button._id,
          },
          reloadable: button?.reloadable || false,
        },
      });
    } else {
      setDataConfirm({
        data: {
          object_id: objectId,
          record_id: recordID,
          button_id: button._id,
        },
        reloadable: button?.reloadable || false,
      });
      setShowConfirm(true);
    }
  };

  useEffect(() => {
    if (open && isRunSuccess) {
      setOpen(false);
      dispatch(
        runDynamicButtonResult({
          isRunSuccess: false,
          isReload: false,
        })
      );
      setRecordID("");
      form.resetFields();
      if (isReload && reload) {
        reload();
      }
    }
  }, [
    dispatch,
    form,
    isRunSuccess,
    isReload,
    reload,
    open,
    setOpen,
    setRecordID,
  ]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (recordID && objectId) {
        axios
          .post(
            BASE_URL_API + "dynamic-button/load-user-button",
            {
              object_id: objectId,
              record_id: recordID,
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((response) => {
            $userDynamicButton(response.data.data);
          })
          .catch((err) => {
            Notification("error", err.response.data.error);
          });
      }
    };
    checkToken();
  }, [objectId, recordID]);

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "id":
        return <IDComp field={field} />;
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            listObjectField={listObjectField}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} form={form} />;
      case "date":
        return <Date1 field={field} form={form} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={open}
            recordID={recordID}
            setRecordID={setRecordID}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            open={openModal}
          />
        );
      case "dynamic-field":
        return <DynamicField field={field} form={form} recordID={recordID} />;
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={recordData}
          />
        );
      case "formula":
        return <FormulaField field={field} form={form} />;

      case "lookup":
        return <Lookup field={field} form={form} recordData={recordData} />;
      default:
        break;
    }
  };

  const onChangeHandle = useCallback((values) => {
    setFormValue(values);
    setValueModal(values);
  }, []);

  const debouncedChangeHandler = useMemo(
    () => _.debounce(onChangeHandle, 500),
    [onChangeHandle]
  );
  const CustomHeader = (
    <Header>
      <span>Chỉnh sửa bản ghi</span>
      {show && (
        <span
          style={{ display: "none" }}
          onClick={() => {
            dispatch(getManualSharing({ record_id: recordID }));
            setOpen(false);
            setModalManual(true);
          }}
        >
          <img src={ShareImg} alt="Share img" />
        </span>
      )}
    </Header>
  );

  if (numberOfFields > 10) {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID !== ""
              ? CustomHeader
              : "Thêm bản ghi"
          }
          visible={open}
          onCancel={() => {
            setOpen(false);
            setRecordID("");
            form.resetFields();
            dispatch(loadRecordDataSuccess({}));
            setTemplateData({});
            setTemplateId("");
          }}
          width={"100%"}
          footer={false}
          maskClosable={false}
          // zIndex={9999999999}
        >
          {template.length > 0 ? (
            <div>
              <div style={{ fontSize: "16px" }}>Choose template:</div>
              <div>
                <Select
                  style={{ width: "100%", marginBottom: "10px" }}
                  value={templateId}
                  onChange={(e) => {
                    setTemplateId(e);
                    template.forEach((item, idx) => {
                      if (item._id === e) {
                        setTemplateData({ ...item });
                      }
                    });
                  }}
                >
                  {template.map((item, idx) => {
                    return (
                      <Option value={item._id}>{item.name_template}</Option>
                    );
                  })}
                </Select>
              </div>
            </div>
          ) : (
            ""
          )}
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            labelAlign="left"
            onValuesChange={(value, values) => {
              // setFormValue(values);
              // setValueModal(values);
              debouncedChangeHandler(values);
            }}
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.Last_Name} {user.Middle_Name} {user.First_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        <Row gutter={[70, 0]}>
                          {/* eslint-disable-next-line */}
                          {section.fields.map((field, index) => {
                            if (
                              field.hidden === false &&
                              field.permission_hidden === false &&
                              hiddenArray.findIndex(
                                (ele) => ele === field._id
                              ) < 0
                            ) {
                              if (
                                (Object.entries(recordData).length === 0 ||
                                  recordID === "") &&
                                field.type === "id"
                              ) {
                              } else
                                return (
                                  <Col span={4} key={field._id}>
                                    {handleFieldType(field, open)}
                                  </Col>
                                );
                            }
                          })}
                        </Row>
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {recordID && userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                loading={isLoading}
                onClick={() => {}}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  setOpen(false);
                  setRecordID("");
                  form.resetFields();
                  dispatch(loadRecordDataSuccess({}));
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>

        <ModalConfirm
          title="Confirm"
          decs="Are you sure you want to proceed?"
          method={runDynamicButton}
          data={dataConfirm}
          img={ImgConfirm}
          showConfirm={showConfirm}
          setShowConfirm={setShowConfirm}
        />
        <ModalManualShare
          recordID={recordID}
          open={modalManual}
          setOpen={setModalManual}
          header={header}
          recordData={recordData}
          data={valueModal}
          onFinish={onFinish}
        />

        <ModalRunDynamicButton
          data={dataConfirm}
          method={runDynamicButton}
          visibleModalRDB={visibleModalRDB}
          $visibleModalRDB={$visibleModalRDB}
          dataRecord={recordData}
        />
      </>
    );
  } else {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID
              ? CustomHeader
              : "Thêm bản ghi"
          }
          visible={open}
          onCancel={() => {
            setOpen(false);
            setRecordID("");
            form.resetFields();
            dispatch(loadRecordDataSuccess({}));
          }}
          width={600}
          footer={false}
          maskClosable={false}
        >
          {template.length > 0 ? (
            <div>
              <div style={{ fontSize: "16px" }}>Choose template:</div>
              <div>
                <Select
                  style={{ width: "100%", marginBottom: "10px" }}
                  value={templateId}
                  onChange={(e) => {
                    setTemplateId(e);
                    template.forEach((item, idx) => {
                      if (item._id === e) {
                        setTemplateData({ ...item });
                      }
                    });
                  }}
                >
                  {template.map((item, idx) => {
                    return (
                      <Option value={item._id}>{item.name_template}</Option>
                    );
                  })}
                </Select>
              </div>
            </div>
          ) : (
            ""
          )}
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            labelAlign="left"
            onValuesChange={(value, values) => {
              // setFormValue(values);
              // setValueModal(values);
              debouncedChangeHandler(values);
            }}
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.Last_Name} {user.Middle_Name} {user.First_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        {/* eslint-disable-next-line */}
                        {section.fields.map((field, index) => {
                          if (
                            field.hidden === false &&
                            field.permission_hidden === false &&
                            hiddenArray.findIndex((ele) => ele === field._id) <
                              0
                          ) {
                            if (
                              (Object.entries(recordData).length === 0 ||
                                recordID === "") &&
                              field.type === "id"
                            ) {
                            } else return handleFieldType(field, open);
                          }
                        })}
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {recordID && userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                loading={isLoading}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  setOpen(false);
                  setRecordID("");
                  form.resetFields();
                  dispatch(loadRecordDataSuccess({}));
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>

        <ModalConfirm
          title="Confirm"
          decs="Are you sure you want to proceed?"
          method={runDynamicButton}
          data={dataConfirm}
          img={ImgConfirm}
          showConfirm={showConfirm}
          setShowConfirm={setShowConfirm}
        />
        <ModalManualShare
          recordID={recordID}
          open={modalManual}
          setOpen={setModalManual}
          header={header}
          recordData={recordData}
          data={valueModal}
          onFinish={onFinish}
        />

        <ModalRunDynamicButton
          data={dataConfirm}
          method={runDynamicButton}
          visibleModalRDB={visibleModalRDB}
          $visibleModalRDB={$visibleModalRDB}
          dataRecord={recordData}
        />
      </>
    );
  }
};

export default ModalRecord;

ModalRecord.defaultProps = {
  loadData: () => {},
};

const WrapperSection = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 15px;
  position: relative;
  padding-bottom: 0;
  padding-left: 36px;
  padding-right: 36px;
  padding-top: 10px;
  padding-bottom: 10px;
  .custom-label {
    font-size: 16px;
    display: inherit;
    margin-bottom: 6px;
    font-family: var(--roboto-700);
  }
`;

const CustomTitleSection = styled.div`
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 20px;
  /* identical to box height, or 143% */

  display: flex;
  align-items: center;
  color: ${(props) => props.theme.main};

  margin-bottom: 24px;
  position: absolute;
  top: -11px;
  left: 24px;
  background-color: #fff;
  padding: 0 10px;
`;

const CustomModal = styled(Modal)`
  .ant-row {
    row-gap: 0 !important;
  }
  .ant-col-6 {
    padding-left: 10px !important;
    padding-right: 10px !important;
    padding-bottom: 0;
    .ant-form-item {
      margin-bottom: 16px !important;
    }
    label {
      font-family: var(--roboto-700);
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #2c2c2c;
    }
  }

  .ant-col-4 {
    padding-left: 10px !important;
    padding-right: 10px !important;
    padding-bottom: 0;
    .ant-form-item {
      margin-bottom: 16px !important;
    }
    label {
      font-family: var(--roboto-700);
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #2c2c2c;
    }
  }

  label {
    font-size: 16px;
    white-space: normal;
    word-break: normal;
    height: fit-content;
  }

  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-modal-body {
    max-height: calc(100vh - 200px);
    padding-bottom: 0;
    overflow-y: scroll;
    /* width */
    ::-webkit-scrollbar {
      width: 5px !important;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px transparent;
      border-radius: 100px;
      background: transparent !important;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #d8d6d6;
      border-radius: 100px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #d8d6d6;
    }
    ::-webkit-scrollbar-track-piece {
      margin-bottom: 10px;
      background: transparent !important;
    }
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-bottom: 24px;
  padding-top: 24px;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonAction = styled(Button)`
  width: 102px;
  margin-right: 16px;

  img {
    margin-right: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;

    img {
      filter: brightness(200);
    }
  }
`;

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding-right: 30px;
  img {
    cursor: pointer;
  }
`;
