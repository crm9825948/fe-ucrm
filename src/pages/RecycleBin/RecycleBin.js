import {
  Breadcrumb,
  Button,
  Pagination,
  Popconfirm,
  Table,
  Tooltip,
} from "antd";
import img from "assets/icons/common/confirm.png";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import React, { useCallback, useEffect, useState } from "react";
import { withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import {
  deleteMultiple,
  emptyTrash,
  loadDataListViewRecycleBin,
  loadListObjectField,
  restoreRecord,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalDuplicate from "components/Modal/ModalDuplicated";
const RecycleBin = () => {
  // const { t } = useTranslation();
  const dispatch = useDispatch();
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const { objectId } = useParams();
  const [headers, setHeaders] = useState([]);
  const [dataShow, setDataShow] = useState([]);
  const [open, setOpen] = useState(false);
  const [openEmptyTrash, setOpenEmptyTrash] = useState(false);
  const [multipleId, setMultipleId] = useState([]);
  const [, setTotalPage] = useState(0);
  const [totalRecord, setTotalRecord] = useState(0);
  let navigate = useNavigate();
  const { dataRecycleBinListView, listObjectField } = useSelector(
    (state) => state.objectsReducer
  );
  const loadDataListView = useCallback(() => {
    dispatch(
      loadDataListViewRecycleBin({
        object_id: objectId,
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [objectId, recordPerPage, currentPage, dispatch]);

  const loadFields = useCallback(() => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );
  }, [objectId, dispatch]);

  useEffect(() => {
    loadDataListView();
  }, [loadDataListView]);

  useEffect(() => {
    loadFields();
  }, [loadFields]);

  /*eslint-disable-next-line*/
  const handleRestoreRecord = (Id) => {
    setMultipleId([]);
    dispatch(
      restoreRecord({
        load: {
          object_id: objectId,
          current_page: currentPage,
          record_per_page: recordPerPage,
        },
        data: {
          object_id: objectId,
          id: Id,
        },
      })
    );
  };

  useEffect(() => {
    let arr = [];
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((sections, idx) => {
        /*eslint-disable-next-line*/
        return sections.fields.map((item, idx) => {
          let newValue = {
            title: item.name,
            dataIndex: item.ID,
            width: 200,
            ellipsis: {
              showTitle: false,
            },
            render: (address) => (
              <Tooltip placement="topLeft" title={address}>
                {address}
              </Tooltip>
            ),
          };
          arr.push(newValue);
        });
      });
    arr.push({
      title: "Deleted time",
      dataIndex: "deleted_time",
      width: 200,
      render: (address) => (
        <Tooltip placement="topLeft" title={address}>
          {address}
        </Tooltip>
      ),
    });
    arr.push({
      title: "User delete",
      dataIndex: "user_delete",
      width: 200,
      render: (address) => (
        <Tooltip placement="topLeft" title={address}>
          {address}
        </Tooltip>
      ),
    });
    arr.push({
      title: "Action",
      key: "action",
      fixed: "right",
      width: 100,
      render: (_, record) => {
        return (
          <Popconfirm
            title="Are you sure to restore this record?"
            onConfirm={() => handleRestoreRecord(record._id)}
            onCancel={() => {}}
            okText="Yes"
            cancelText="No"
            placement="left"
          >
            <Button size="small" disabled={multipleId.length > 0}>
              Restore
            </Button>{" "}
          </Popconfirm>
        );
      },
    });
    setHeaders(arr);
    /* eslint-disable-next-line */
  }, [listObjectField, currentPage, recordPerPage]);

  useEffect(() => {
    let newData = [];
    /* eslint-disable-next-line */
    dataRecycleBinListView.map((item, index) => {
      let newItem = {};
      newItem["key"] = item._id;
      /* eslint-disable-next-line */
      Object.entries(item).forEach(([key, value], idx) => {
        if (typeof value === "object" && value !== null) {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      newData.push(newItem);
    });
    setTotalPage(
      newData &&
        newData[newData.length - 1] &&
        newData[newData.length - 1].total_page
    );
    setTotalRecord(
      newData &&
        newData[newData.length - 1] &&
        newData[newData.length - 1].total_record
    );

    newData.splice(newData.length - 1, 1);
    setDataShow(newData);
  }, [dataRecycleBinListView]);

  const onSelectChange = (selectedRowKeys, selectedRows) => {
    setMultipleId(selectedRowKeys);
  };

  useEffect(() => {
    if (open === false) {
      setMultipleId([]);
    }
  }, [open]);

  return (
    <Wrapper>
      <div className="header">
        <CustomHeader>
          <Breadcrumb>
            <Breadcrumb.Item>
              {/* eslint-disable-next-line  */}
              <a onClick={() => navigate(`/objects/${objectId}/default-view`)}>
                Object
              </a>
            </Breadcrumb.Item>
            <BreadcrumbItem>
              {/* eslint-disable-next-line */}
              <a onClick={() => window.location.reload()}>Recycle bin</a>
            </BreadcrumbItem>
          </Breadcrumb>
        </CustomHeader>
        <div className="decs">
          Data that have been in Trash more than 30 days will be automatically
          deleted.
        </div>
        <CustomHeaderButton>
          <CustomButtonDelete
            size="large"
            onClick={() => {
              setOpen(true);
            }}
            disabled={multipleId.length === 0 || dataShow.length === 0}
          >
            Delete forever
          </CustomButtonDelete>
          <CustomButtonEmpty
            size="large"
            onClick={() => {
              setOpenEmptyTrash(true);
            }}
            disabled={dataShow.length === 0}
          >
            Empty trash
          </CustomButtonEmpty>
        </CustomHeaderButton>
      </div>
      <div className="wrapper-table">
        <Table
          columns={headers}
          dataSource={dataShow}
          pagination={false}
          rowSelection={{
            selectedRowKeys: multipleId,
            onChange: onSelectChange,
          }}
        />
        <CustomPagination
          showQuickJumper
          current={currentPage}
          total={totalRecord}
          showSizeChanger
          showTotal={(total, range) =>
            `${range[0]}-${range[1]} of ${total} records`
          }
          // pageSize={recordPerPage}
          onChange={(e, pageSize) => {
            setCurrentPage(e);
            setRecordPerPage(pageSize);
          }}
        />
      </div>
      <ModalConfirm
        title={"Bạn có chắc chắn xóa những bản ghi này?"}
        decs={"Các dữ liệu sau khi xóa sẽ không thể khôi phục"}
        open={open}
        setOpen={setOpen}
        method={deleteMultiple}
        data={{
          load: {
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
          },
          data: {
            object_id: objectId,
            ids: multipleId,
          },
        }}
        setOpenModal={() => {}}
        img={img}
      />
      <ModalConfirm
        title={"Bạn có chắc chắn xóa những bản ghi này?"}
        decs={"Các dữ liệu sau khi xóa sẽ không thể khôi phục!"}
        open={openEmptyTrash}
        setOpen={setOpenEmptyTrash}
        method={emptyTrash}
        data={{
          load: {
            object_id: objectId,
            current_page: currentPage,
            record_per_page: recordPerPage,
          },
          data: {
            object_id: objectId,
          },
        }}
        setOpenModal={() => {}}
        img={img}
      />{" "}
      <ModalDuplicate />
    </Wrapper>
  );
};

export default withTranslation()(RecycleBin);

const Wrapper = styled.div`
  padding: 24px;
  .decs {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */

    /* text xam */

    color: #6b6b6b;
  }
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 16px;
  }
  .wrapper-table {
    padding: 8px;
    overflow: auto;
    background-color: #fff;
    width: 100%;
    padding-right: 0;
    table {
      /* width: max-content; */
      width: 100%;
    }
    width: 100%;
    height: calc(100% - 127px);
    overflow: auto;
    padding: 0 24px;
    margin-top: 24px;

    .ant-table-wrapper {
      padding: 16px;
      background: #fff;
      border-radius: 5px;
      height: 85%;
    }

    .ant-spin-nested-loading {
      height: 100%;
    }
    .ant-spin-container {
      height: 100%;
    }

    .ant-table {
      height: 100%;
    }

    .ant-table-container {
      height: 100%;
    }

    .ant-table-content {
      overflow: auto !important;
      height: 100%;

      .ant-row {
        .ant-form-item {
          margin-bottom: 0;
        }
      }
      .ant-form-item {
        margin-bottom: 0;
      }
    }

    .ant-table-thead {
      position: sticky;
      top: 0;
      z-index: 2;
    }

    .ant-checkbox-wrapper:hover {
      .ant-checkbox-inner {
        border-color: ${(props) => props.theme.main};
      }
    }
    .ant-checkbox-wrapper:active {
      .ant-checkbox-inner {
        border-color: ${(props) => props.theme.main};
      }
    }
    .ant-checkbox-input:active {
      border-color: ${(props) => props.theme.main};
    }
    .ant-checkbox-indeterminate {
      .ant-checkbox-inner::after {
        background-color: ${(props) => props.theme.main};
      }
    }
    .ant-checkbox-checked::after {
      border: 1px solid ${(props) => props.theme.main};
    }
    .ant-checkbox-input:focus {
      .ant-checkbox-inner {
        border: 1px solid ${(props) => props.theme.main};
      }
    }

    table {
      width: max-content;
      /* overflow: hidden; */
      th {
        width: max-content;
        .ant-table-column-title {
          margin-right: 30px;
        }
      }
      td {
        max-width: 200px;
      }
      .ant-table-cell {
        background-color: white;
      }
      th.ant-table-selection-column {
        text-align: left !important;
        padding-left: 16px !important;
      }
    }
    a {
      font-size: 16px;
    }
    .ant-btn:active {
      color: #fff;
      background-color: ${(props) => props.theme.darker}!important;
      border-color: ${(props) => props.theme.darker}!important;
    }
    .ant-btn:focus {
      color: #2c2c2c !important;
      background-color: ${(props) => props.theme.darker}!important;
      border-color: ${(props) => props.theme.darker}!important;
    }
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: white !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  /* align-items: center; */
  /* margin-bottom: 24px; */
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomHeaderButton = styled.div``;

const CustomButtonDelete = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonEmpty = styled(Button)`
  background-color: ${(props) => props.theme.white};
  color: #000;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomPagination = styled(Pagination)`
  /* position: fixed; */
  /* bottom: 50px; */
  /* right: 16px; */
  width: calc(100% - 32px);
  text-align: right;
  background-color: #fff;
  padding: 16px;
  border: 1px solid #ececec;
  margin: auto;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;
