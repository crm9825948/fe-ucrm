import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import { useNavigate } from "react-router-dom";

import Breadcrumb from "antd/lib/breadcrumb";
import Button from "antd/lib/button";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import {
  getListConfig,
  setShowModalConfig,
  deleteConfig,
} from "redux/slices/approvalProcesses";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfig from "./ModalConfig";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

function ApprovalProcesses(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { listConfig } = useSelector((state) => state.approvalProcessesReducer);

  const [listObjects, $listObjects] = useState([]);
  const [dataSource, $dataSource] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [detailsConfig, $detailsConfig] = useState({});

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "approval" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onDeleteConfig = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: id });
  };

  const columns = [
    {
      title: "Object request",
      dataIndex: "object_request_name",
      key: "object_request_name",
    },
    {
      title: "Object approval",
      dataIndex: "object_approval_name",
      key: "object_approval_name",
    },
    {
      title: "Approval type",
      dataIndex: "approval_type",
      key: "approval_type",
      render: (text) => (
        <span style={{ textTransform: "capitalize" }}>{text}</span>
      ),
    },
    {
      title: "Created date",
      dataIndex: "created_date",
      key: "created_date",
    },
    {
      title: "Action",
      dataIndex: "action",
      key: "action",
      render: (text, record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                onClick={() => {
                  $detailsConfig(record.data);
                  dispatch(setShowModalConfig(true));
                }}
                src={Edit}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => _onDeleteConfig(record.key)}
                src={Delete}
                alt="delete"
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  useEffect(() => {
    dispatch(getListConfig());
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempList = [];
    listConfig.map((item) => {
      return tempList.push({
        key: _.get(item, "_id", ""),
        object_request_name: _.get(item, "object_request_name", ""),
        object_approval_name: _.get(item, "object_approval_name", ""),
        approval_type: _.get(item, "approval_type", ""),
        created_date: _.get(item, "created_date", ""),
        data: item,
      });
    });
    $dataSource(tempList);
  }, [listConfig]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.approvalProcesses")}</BreadcrumbItem>
        </Breadcrumb>
        {listConfig.length > 0 && checkRule("create") && (
          <AddButton onClick={() => dispatch(setShowModalConfig(true))}>
            + {t("approvalProcesses.addSetting")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listConfig.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")}{" "}
            <span>{t("settings.approvalProcesses")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={() => dispatch(setShowModalConfig(true))}>
              + {t("approvalProcesses.addSetting")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table columns={columns} dataSource={dataSource} pagination={false} />
        </WrapTable>
      )}

      <ModalConfig
        listObjects={listObjects}
        detailsConfig={detailsConfig}
        $detailsConfig={$detailsConfig}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteConfig}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default ApprovalProcesses;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;
