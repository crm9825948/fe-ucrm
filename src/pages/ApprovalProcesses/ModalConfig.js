import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

import { loadListObjectField } from "redux/slices/objects";
import {
  getFieldsObject,
  getFieldsObjectResult,
  createConfig,
  updateConfig,
  setShowModalConfig,
} from "redux/slices/approvalProcesses";

import Conditions from "components/Conditions/conditions";
import { Notification } from "components/Notification/Noti";
import _ from "lodash";

const approvalType = [
  {
    label: "Email",
    value: "email",
  },
  {
    label: "SMS",
    value: "sms",
  },
];

function ModalConfig({ listObjects, detailsConfig, $detailsConfig }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showModalConfig, fieldsObjectArroval } = useSelector(
    (state) => state.approvalProcessesReducer
  );

  const [fieldsRequest, $fieldsRequest] = useState([]);
  const [optionsStatus, $optionsStatus] = useState([]);
  const [selectedStatus, $selectedStatus] = useState({
    approve_status: undefined,
    reject_status: undefined,
    submitted_status: undefined,
  });
  const [selectedText, $selectedText] = useState({
    field_message_id: undefined,
    field_link_details: undefined,
    field_description: undefined,
  });

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      let payload = {
        object_request: _.get(values, "object_request", ""),
        object_approval: _.get(values, "object_approval", ""),
        approval_type: _.get(values, "approval_type", ""),
        approval_mapping_fields: {
          field_status: _.get(values, "field_status", ""),
          field_user_request: _.get(values, "field_user_request", ""),
          field_record_related: _.get(values, "field_record_related", ""),
          description_template: _.get(values, "description_template", ""),
          approve_status: _.get(values, "approve_status", ""),
          field_link_details: _.get(values, "field_link_details", ""),
          reject_status: _.get(values, "reject_status", ""),
          field_description: _.get(values, "field_description", ""),
          field_message_id: _.get(values, "field_message_id", ""),
          submitted_status: _.get(values, "submitted_status", ""),
        },

        condition: JSON.stringify({
          and_filter: allCondition,
          or_filter: anyCondition,
        }),
      };

      if (_.isEmpty(detailsConfig)) {
        dispatch(createConfig(payload));
      } else {
        dispatch(
          updateConfig({
            payload: payload,
            id: _.get(detailsConfig, "_id", ""),
          })
        );
      }
    }
  };

  const _onCancel = () => {
    dispatch(setShowModalConfig(false));
  };

  useEffect(() => {
    if (!showModalConfig) {
      form.resetFields();
      setAllCondition([]);
      setAnyCondition([]);
      setOperatorValueAnd([]);
      setOperatorValueOr([]);
      setValueAnd([]);
      setValueOr([]);
      $fieldsRequest([]);
      $detailsConfig({});
      $optionsStatus([]);

      dispatch(getFieldsObjectResult([]));
    }
  }, [$detailsConfig, dispatch, form, showModalConfig]);

  const loadFieldsObjectRequest = (value) => {
    form.setFieldsValue({
      description_template: undefined,
    });
    setAllCondition([]);
    setAnyCondition([]);
    setOperatorValueAnd([]);
    setOperatorValueOr([]);
    setValueAnd([]);
    setValueOr([]);

    dispatch(
      loadListObjectField({
        object_id: value,
        api_version: "2",
        show_meta_fields: true,
      })
    );
  };

  const loadFieldsObjectApproval = (value) => {
    form.setFieldsValue({
      field_status: undefined,
      field_user_request: undefined,
      submitted_status: undefined,
      field_message_id: undefined,
      approve_status: undefined,
      field_link_details: undefined,
      reject_status: undefined,
      field_description: undefined,
      field_record_related: undefined,
    });
    $optionsStatus([]);
    $selectedStatus({
      approve_status: undefined,
      reject_status: undefined,
      submitted_status: undefined,
    });
    $selectedText({
      field_message_id: undefined,
      field_link_details: undefined,
      field_description: undefined,
    });

    dispatch(
      getFieldsObject({
        object_id: value,
        api_version: "2",
      })
    );
  };

  const onSelectStatus = (value, values) => {
    $optionsStatus(_.get(values, "option", []));
  };

  const onSelectedOption = (value, type) => {
    $selectedStatus({
      ...selectedStatus,
      [type]: value,
    });
  };

  const onSelectFieldText = (value, type) => {
    $selectedText({
      ...selectedText,
      [type]: value,
    });
  };

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: "$" + field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    $fieldsRequest(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (!_.isEmpty(detailsConfig)) {
      const tempCondition = JSON.parse(_.get(detailsConfig, "condition", ""));

      dispatch(
        loadListObjectField({
          object_id: _.get(detailsConfig, "object_request", ""),
          api_version: "2",
          show_meta_fields: true,
        })
      );
      dispatch(
        getFieldsObject({
          object_id: _.get(detailsConfig, "object_approval", ""),
          api_version: "2",
        })
      );

      setAllCondition(_.get(tempCondition, "and_filter", []));
      setAnyCondition(_.get(tempCondition, "or_filter", []));
      $selectedStatus({
        approve_status: _.get(
          detailsConfig,
          "approval_mapping_fields.approve_status",
          undefined
        ),
        reject_status: _.get(
          detailsConfig,
          "approval_mapping_fields.reject_status",
          undefined
        ),
        submitted_status: _.get(
          detailsConfig,
          "approval_mapping_fields.submitted_status",
          undefined
        ),
      });
      $selectedText({
        field_message_id: _.get(
          detailsConfig,
          "approval_mapping_fields.field_message_id",
          undefined
        ),
        field_link_details: _.get(
          detailsConfig,
          "approval_mapping_fields.field_link_details",
          undefined
        ),
        field_description: _.get(
          detailsConfig,
          "approval_mapping_fields.field_description",
          undefined
        ),
      });

      form.setFieldsValue({
        ...detailsConfig,
        ...detailsConfig.approval_mapping_fields,
      });
    }
  }, [detailsConfig, form, dispatch]);

  useEffect(() => {
    if (!_.isEmpty(detailsConfig) && fieldsObjectArroval?.length > 0) {
      const field = fieldsObjectArroval.find(
        (item) =>
          item?.value === detailsConfig?.approval_mapping_fields?.field_status
      );
      if (field) {
        $optionsStatus(_.get(field, "option", []));
      }
    }
  }, [fieldsObjectArroval, detailsConfig]);

  return (
    <ModalCustom
      title={t("settings.approvalProcesses")}
      visible={showModalConfig}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        colon={false}
        labelAlign="left"
        layout="vertical"
      >
        <Form.Item
          label="Object request"
          name="object_request"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            placeholder={t("workflow.selectObject")}
            options={listObjects}
            onChange={loadFieldsObjectRequest}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Form.Item
          label="Object approval"
          name="object_approval"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            placeholder={t("workflow.selectObject")}
            options={listObjects}
            onChange={loadFieldsObjectApproval}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Form.Item
          label="Approval type"
          name="approval_type"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            placeholder={t("common.placeholderSelect")}
            options={approvalType}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Field status"
              name="field_status"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                options={fieldsObjectArroval?.filter(
                  (field) => _.get(field, "type", "") === "select"
                )}
                onChange={onSelectStatus}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Field user request"
              name="field_user_request"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                options={fieldsObjectArroval?.filter(
                  (field) => _.get(field, "type", "") === "user"
                )}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Submitted status"
              name="submitted_status"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                onChange={(value) =>
                  onSelectedOption(value, "submitted_status")
                }
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(optionsStatus, (ele) => (
                  <Select.Option
                    disabled={
                      ele.value === selectedStatus.approve_status ||
                      ele.value === selectedStatus.reject_status
                        ? true
                        : false
                    }
                    key={ele.value}
                  >
                    {ele.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Field message id"
              name="field_message_id"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                onChange={(value) =>
                  onSelectFieldText(value, "field_message_id")
                }
                placeholder={t("common.placeholderSelect")}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(
                  fieldsObjectArroval?.filter(
                    (field) => _.get(field, "type", "") === "text"
                  ),
                  (ele) => (
                    <Select.Option
                      disabled={
                        ele.value === selectedText.field_link_details ||
                        ele.value === selectedText.field_description
                          ? true
                          : false
                      }
                      key={ele.value}
                    >
                      {ele.label}
                    </Select.Option>
                  )
                )}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Approve status"
              name="approve_status"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                onChange={(value) => onSelectedOption(value, "approve_status")}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(optionsStatus, (ele) => (
                  <Select.Option
                    disabled={
                      ele.value === selectedStatus.submitted_status ||
                      ele.value === selectedStatus.reject_status
                        ? true
                        : false
                    }
                    key={ele.value}
                  >
                    {ele.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Field link details"
              name="field_link_details"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                onChange={(value) =>
                  onSelectFieldText(value, "field_link_details")
                }
                placeholder={t("common.placeholderSelect")}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(
                  fieldsObjectArroval?.filter(
                    (field) => _.get(field, "type", "") === "text"
                  ),
                  (ele) => (
                    <Select.Option
                      disabled={
                        ele.value === selectedText.field_message_id ||
                        ele.value === selectedText.field_description
                          ? true
                          : false
                      }
                      key={ele.value}
                    >
                      {ele.label}
                    </Select.Option>
                  )
                )}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Reject status"
              name="reject_status"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                onChange={(value) => onSelectedOption(value, "reject_status")}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(optionsStatus, (ele) => (
                  <Select.Option
                    disabled={
                      ele.value === selectedStatus.submitted_status ||
                      ele.value === selectedStatus.approve_status
                        ? true
                        : false
                    }
                    key={ele.value}
                  >
                    {ele.label}
                  </Select.Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Field description"
              name="field_description"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                onChange={(value) =>
                  onSelectFieldText(value, "field_description")
                }
                placeholder={t("common.placeholderSelect")}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                showSearch
              >
                {_.map(
                  fieldsObjectArroval?.filter(
                    (field) => _.get(field, "type", "") === "text"
                  ),
                  (ele) => (
                    <Select.Option
                      disabled={
                        ele.value === selectedText.field_message_id ||
                        ele.value === selectedText.field_link_details
                          ? true
                          : false
                      }
                      key={ele.value}
                    >
                      {ele.label}
                    </Select.Option>
                  )
                )}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="Description template"
              name="description_template"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                mode="tags"
                placeholder={t("common.placeholderSelect")}
                options={fieldsRequest}
                optionFilterProp="label"
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="Field record related"
              name="field_record_related"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                placeholder={t("common.placeholderSelect")}
                options={fieldsObjectArroval?.filter(
                  (field) => _.get(field, "type", "") === "lookup"
                )}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
          </Col>
        </Row>

        <Condition>
          <legend>{t("workflow.conditions")}</legend>
          <Conditions
            title={t("common.allCondition")}
            decs={`(${t("common.descAllCondition")})`}
            conditions={allCondition}
            setConditions={setAllCondition}
            operatorValue={operatorValueAnd}
            setOperatorValue={setOperatorValueAnd}
            value={valueAnd}
            setValue={setValueAnd}
            ID={!_.isEmpty(detailsConfig) && _.get(detailsConfig, "_id", "")}
            dataDetails={
              !_.isEmpty(detailsConfig) && _.get(detailsConfig, "_id", "")
            }
          />
          <Conditions
            title={t("common.anyCondition")}
            decs={`(${t("common.descAnyCondition")})`}
            conditions={anyCondition}
            setConditions={setAnyCondition}
            operatorValue={operatorValueOr}
            setOperatorValue={setOperatorValueOr}
            value={valueOr}
            setValue={setValueOr}
            ID={!_.isEmpty(detailsConfig) && _.get(detailsConfig, "_id", "")}
            dataDetails={
              !_.isEmpty(detailsConfig) && _.get(detailsConfig, "_id", "")
            }
          />
        </Condition>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalConfig);

ModalConfig.defaultProps = {
  listObjects: [],
  detailsConfig: {},
  $detailsConfig: () => {},
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const Condition = styled.fieldset`
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 14px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
