import { Col, Row, Typography } from "antd";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import React, { useCallback, useEffect, useState } from "react";
// import { useSelector } from "react-redux";
import styled from "styled-components";
import { checkTokenExpiration } from "contexts/TokenCheck";
const { Text: TextComponent } = Typography;
const DetailsComp = (props) => {
  const { recordID, objectId } = props;

  const [details, setDetails] = useState({});
  // const { listObjectField } = useSelector(
  //   /*eslint-disable-next-line*/
  //   (state) => state.objectsReducer
  // );

  const loadData = useCallback(async () => {
    const isTokenValid = await checkTokenExpiration();
    if (recordID)
      axios
        .get(
          BASE_URL_API +
            `load-record-meta-data?id=${recordID}&object_id=${objectId}`,
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setDetails(res.data.data);
        })
        .catch((err) => console.log(err));
  }, [recordID, objectId]);

  useEffect(() => {
    loadData();
  }, [loadData]);

  const [listField, setListField] = useState({});

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/objects-fields-permission",
          {
            api_version: "2",
            object_id: objectId,
            show_meta_fields: true,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          let temp = {};
          res.data.data.forEach((item, idx) => {
            temp = { ...temp, ...item };
          });
          setListField(temp);
        })
        .catch((err) => {});
    };
    checkToken();
  }, [objectId]);

  return (
    <Wrapper>
      {listField &&
        listField["main_object"] &&
        listField["main_object"]["sections"] &&
        listField["main_object"]["sections"].map((section, idx) => {
          if (Object.entries(details && details !== null && details).length > 0)
            return (
              <Row>
                {section.fields.map((item, idx) => {
                  return (
                    <Col span={12} style={{ marginBottom: "16px" }}>
                      <Title>{item.name}</Title>
                      <div>
                        {item.type !== "file"
                          ? details &&
                            details[item.ID] &&
                            details[item.ID].value === undefined
                            ? details && details[item.ID]
                            : details &&
                              details[item.ID] &&
                              details[item.ID].value
                          : ""}
                        {item.type === "file" &&
                        details &&
                        details[item.ID] &&
                        details[item.ID].value !== null
                          ? details &&
                            details[item.ID] &&
                            typeof details[item.ID].value === "object" &&
                            details[item.ID].value.map((file, idx) => {
                              let fileArray = file.split("/");
                              return (
                                <div>
                                  <a
                                    href={file}
                                    style={{ display: "block" }}
                                    target={"_blank"}
                                    rel="noreferrer"
                                  >
                                    {fileArray[fileArray.length - 1]}
                                  </a>
                                </div>
                              );
                            })
                          : ""}
                        {item.type === "file" &&
                        details &&
                        details[item.ID] &&
                        details[item.ID].value !== null
                          ? details &&
                            details[item.ID] &&
                            typeof details[item.ID].value === "string" &&
                            details[item.ID].value
                              .split(",")
                              .map((link, idx) => {
                                let splitFile = link.split("/");
                                return (
                                  <a
                                    key={idx}
                                    style={{ display: "block" }}
                                    href={link}
                                    target={"_blank"}
                                    rel="noreferrer"
                                  >
                                    <TextComponent
                                      ellipsis={{
                                        tooltip:
                                          splitFile[splitFile.length - 1],
                                      }}
                                    >
                                      <span style={{ color: "#1890ff" }}>
                                        {splitFile[splitFile.length - 1]}
                                      </span>
                                    </TextComponent>
                                  </a>
                                );
                              })
                          : ""}
                      </div>
                    </Col>
                  );
                })}
              </Row>
            );
          else return null;
        })}
    </Wrapper>
  );
};

export default DetailsComp;

const Wrapper = styled.div`
  padding: 16px 56px;
`;

const Title = styled.div`
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 24px;
  /* identical to box height, or 150% */

  /* Character/Body text */

  color: #2c2c2c;
`;
