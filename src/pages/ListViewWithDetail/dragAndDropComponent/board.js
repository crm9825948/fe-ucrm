import React, { useEffect, useState } from "react";
import styled from "styled-components";
// import { Global, css } from "@emotion/core";
import { colors } from "@atlaskit/theme";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { updateListViewDetails } from "redux/slices/objects";
import Column from "./column";
import reorder, { reorderQuoteMap } from "./reorder";

const ParentContainer = styled.div`
  /* height: ${({ height }) => height}; */
  /* overflow-x: hidden;
  overflow-y: auto; */
`;

const Container = styled.div`
  /* background-color: ${colors.B100}; */
  width: 100%;
  /* min-height: 100vh; */
  /* like display:flex but will allow bleeding over the window width */
  /* min-width: 100vw; */
  display: inline-flex;
`;

const Board = ({ isCombineEnabled = false, ...props }) => {
  const {
    initial,
    order,
    containerHeight,
    setValue,
    updateLayout,
    recordID,
    setRecordID,
    loadListData,
    editDetail,
    setEditDetail,
  } = props;
  const [columns, setColumns] = useState([]);
  const [ordered, setOrdered] = useState([]);
  const dispatch = useDispatch();
  const { objectId } = useParams();

  const { listViewWithDetail } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    setColumns(initial);
    setOrdered(Object.keys(initial));
  }, [initial, order]);

  const onDragEnd = (result) => {
    if (result.combine) {
      if (result.type === "COLUMN") {
        const shallow = [...ordered];
        shallow.splice(result.source.index, 1);
        setOrdered(shallow);
        return;
      }
      // eslint-disable-next-line
      const column = columns[result.source.droppableId];
      const withQuoteRemoved = [...column];
      withQuoteRemoved.splice(result.source.index, 1);
      const columns = {
        // eslint-disable-next-line
        ...columns,
        [result.source.droppableId]: withQuoteRemoved,
      };
      setColumns(columns);
      return;
    }

    // dropped nowhere
    if (!result.destination) {
      return;
    }

    const source = result.source;
    const destination = result.destination;

    // did not move anywhere - can bail early
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    // reordering column
    if (result.type === "COLUMN") {
      const orderedTemp = reorder(ordered, source.index, destination.index);
      setOrdered(orderedTemp);

      return;
    }

    const data = reorderQuoteMap({
      quoteMap: columns,
      source,
      destination,
    });
    dispatch(
      updateListViewDetails({
        primary_field: listViewWithDetail.primary_field,
        object_id: objectId,
        field_ids: listViewWithDetail.field_ids,
        layouts: data.quoteMap,
      })
    );
    setColumns(data.quoteMap);
  };

  const board = (
    <Droppable
      droppableId="board"
      type="COLUMN"
      direction="horizontal"
      ignoreContainerClipping={Boolean(containerHeight)}
      isCombineEnabled={props.isCombineEnabled}
    >
      {(provided) => (
        <Container ref={provided.innerRef} {...provided.droppableProps}>
          {ordered &&
            ordered.map((key, index) => (
              <Column
                key={key}
                index={index}
                title={key}
                quotes={columns[key]}
                isScrollable={props.withScrollableColumns}
                isCombineEnabled={props.isCombineEnabled}
                columns={columns}
                ordered={ordered}
                value={initial}
                setValue={setValue}
                updateLayout={updateLayout}
                recordID={recordID}
                setRecordID={setRecordID}
                loadListData={loadListData}
                objectId={objectId}
                setEditDetail={setEditDetail}
                editDetail={editDetail}
              />
            ))}
          {provided.placeholder}
        </Container>
      )}
    </Droppable>
  );

  return (
    <React.Fragment>
      <DragDropContext onDragEnd={onDragEnd}>
        {containerHeight ? (
          <ParentContainer height={containerHeight}>{board}</ParentContainer>
        ) : (
          board
        )}
      </DragDropContext>
    </React.Fragment>
  );
};

export default Board;
