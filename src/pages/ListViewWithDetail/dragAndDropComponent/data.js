import { colors } from "@atlaskit/theme";
// import "" from "./assets/Left.png";
// import "" from "./assets/finn.png";
// import "" from "./assets/bmo.png";
// import "" from "./assets/princess.png";

const Left = {
  id: "1",
  name: "Left",
  url: "http://adventuretime.wikia.com/wiki/Jake",
  avatarUrl: "",
  colors: {
    soft: colors.Y50,
    hard: colors.Y200,
  },
};

const Right = {
  id: "2",
  name: "Right",
  url: "http://adventuretime.wikia.com/wiki/Right",
  avatarUrl: "",
  colors: {
    soft: colors.G50,
    hard: colors.G200,
  },
};

// const finn = {
//   id: "3",
//   name: "Finn",
//   url: "http://adventuretime.wikia.com/wiki/Finn",
//   avatarUrl: "",
//   colors: {
//     soft: colors.B50,
//     hard: colors.B200,
//   },
// };

// const princess = {
//   id: "4",
//   name: "Princess bubblegum",
//   url: "http://adventuretime.wikia.com/wiki/Princess_Bubblegum",
//   avatarUrl: "",
//   colors: {
//     soft: colors.P50,
//     hard: colors.P200,
//   },
// };

export const authors = [Left, Right];

export const quotes = [
  // {
  //   id: "1",
  //   content: "Sometimes life is scary and dark",
  //   author: Right,
  // },
  // {
  //   id: "2",
  //   content:
  //     "Sucking at something is the first step towards being sorta good at something.",
  //   author: Left,
  // },
  //   {
  //     id: "3",
  //     content: "You got to focus on what's real, man",
  //     author: Left,
  //   },
  //   {
  //     id: "4",
  //     content: "Is that where creativity comes from? From sad biz?",
  //     author: finn,
  //   },
  //   {
  //     id: "5",
  //     content: "Homies help homies. Always",
  //     author: finn,
  //   },
  //   {
  //     id: "6",
  //     content: "Responsibility demands sacrifice",
  //     author: princess,
  //   },
  //   {
  //     id: "7",
  //     content: "That's it! The answer was so simple, I was too smart to see it!",
  //     author: princess,
  //   },
  //   {
  //     id: "8",
  //     content: "People make mistakes. It’s a part of growing up",
  //     author: finn,
  //   },
  //   {
  //     id: "9",
  //     content: "Don't you always call sweatpants 'give up on life pants,' Jake?",
  //     author: finn,
  //   },
  //   {
  //     id: "10",
  //     content: "I should not have drunk that much tea!",
  //     author: princess,
  //   },
  //   {
  //     id: "11",
  //     content: "Please! I need the real you!",
  //     author: princess,
  //   },
  //   {
  //     id: "12",
  //     content: "Haven't slept for a solid 83 hours, but, yeah, I'm good.",
  //     author: princess,
  //   },
];

const getByAuthor = (author, items) =>
  items.filter((quote) => quote.author === author);

export const authorQuoteMap = authors.reduce(
  (previous, author) => ({
    ...previous,
    [author.name]: getByAuthor(author, quotes),
  }),
  {}
);
