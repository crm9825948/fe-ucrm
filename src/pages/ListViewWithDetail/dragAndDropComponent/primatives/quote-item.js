import { colors } from "@atlaskit/theme";
import close from "assets/images/consolidatedView/close.png";
import Comment from "pages/ConsolidatedView/componentType/comment";
import DetailComponent from "pages/ConsolidatedView/componentType/details";
import MultiDetail from "pages/ConsolidatedView/componentType/multiDetail";
import DetailsThirdParty from "pages/ConsolidatedView/componentType/detailsThirdParty";
import LogsComponent from "pages/ConsolidatedView/componentType/logs";
import SLAComponent from "pages/ConsolidatedView/componentType/sla";
import TableComponent from "pages/ConsolidatedView/componentType/table1";
import TableThirdParty from "pages/ConsolidatedView/componentType/tableThirdParty";
import TagsComponent from "pages/ConsolidatedView/componentType/tags";
import TimelineHorizontal from "pages/ConsolidatedView/componentType/timeLine/timelineHorizontal";
import TimelineVertical from "pages/ConsolidatedView/componentType/timeLine/timelineVertical";
import CampaignGeneral from "pages/ConsolidatedView/componentType/campaignGeneral";
import CampaignTemplate from "pages/ConsolidatedView/componentType/campaignTemplate";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { updateListViewDetails } from "redux/slices/objects";
import styled from "styled-components";
import DetailsComp from "../../detailsComp";
import { borderRadius, grid } from "../constants";
import Email from "components/Email/Email2";
import _ from "lodash";

const getBackgroundColor = (isDragging, isGroupedOver, authorColors) => {
  if (isDragging) {
    return authorColors.soft;
  }

  if (isGroupedOver) {
    return colors.N30;
  }

  return colors.N0;
};

const getBorderColor = (isDragging, authorColors) =>
  isDragging ? authorColors.hard : "transparent";

const Container = styled.div`
  border-radius: ${borderRadius}px;
  border: 2px solid transparent;
  /* border-color: ${(props) =>
    getBorderColor(props.isDragging, props.colors)}; */
  /* background-color: ${(props) =>
    getBackgroundColor(props.isDragging, props.isGroupedOver, props.colors)};
  box-shadow: ${({ isDragging }) =>
    isDragging ? `2px 2px 1px ${colors.N70}` : "none"}; */
  /* padding: ${grid}px; */
  min-height: 40px;
  /* margin-bottom: ${grid}px; */
  user-select: none;

  /* anchor overrides */
  color: ${colors.N900};

  &:hover,
  &:active {
    color: ${colors.N900};
    text-decoration: none;
  }

  &:focus {
    outline: none;
    border-color: ${(props) => props.colors.hard};
    box-shadow: none;
  }

  /* flexbox */
  display: flex;
`;

const QuoteItem = (props) => {
  const {
    quote,
    isDragging,
    isGroupedOver,
    provided,
    recordID,
    loadListData,
    setEditDetail,
    editDetail,
  } = props;
  const { objectId } = useParams();
  const { listViewWithDetail } = useSelector((state) => state.objectsReducer);
  const dispatch = useDispatch();

  const removeItem = (quote) => {
    let tempLeft = [...listViewWithDetail.layouts["Left"]];
    let tempRight = [...listViewWithDetail.layouts["Right"]];
    tempLeft = tempLeft.filter((ele) => ele._id !== quote._id);
    tempRight = tempRight.filter((ele) => ele._id !== quote._id);

    dispatch(
      updateListViewDetails({
        primary_field: listViewWithDetail.primary_field,
        object_id: objectId,
        field_ids: listViewWithDetail.field_ids,
        layouts: {
          Left: tempLeft,
          Right: tempRight,
        },
      })
    );
  };
  const [, setDrag] = useState(false);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleSwitchCase = () => {
    switch ((quote && quote.type) || quote.component_type) {
      case "sla":
        return (
          <WrapperComponent>
            <Header>
              SLA{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header>
            <SLAComponent
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
            />
          </WrapperComponent>
        );
      case "logs":
        return (
          <WrapperComponent>
            <Header>
              Logs{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header>
            <LogsComponent
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
            />
          </WrapperComponent>
        );
      // break;
      case "comment":
        return (
          <WrapperComponent type={"comment"}>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <Comment objectId={objectId} recordID={recordID} />
          </WrapperComponent>
        );
      // break;
      case "vertical-timeline":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <TimelineVertical
              data={quote}
              objectId={objectId}
              recordID={recordID}
              type="vertical-timeline"
            />
          </WrapperComponent>
        );
      case "vertical-timeline-new":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <TimelineVertical
              data={quote}
              objectId={objectId}
              recordID={recordID}
              type="vertical-timeline-new"
            />
          </WrapperComponent>
        );
      case "horizontal-timeline":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <TimelineHorizontal
              data={quote}
              objectId={objectId}
              recordID={recordID}
              type="horizontal-timeline"
            />
          </WrapperComponent>
        );
      case "horizontal-timeline-new":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <TimelineHorizontal
              data={quote}
              objectId={objectId}
              recordID={recordID}
              type="horizontal-timeline-new"
            />
          </WrapperComponent>
        );
      case "details":
        return (
          <WrapperComponent>
            {/* <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header> */}
            <DetailComponent
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              setEditDetail={setEditDetail}
              editDetail={editDetail}
              onRemoveItem={() => {
                removeItem(quote);
              }}
              checkRule={checkRule}
              setDrag={setDrag}
            />
          </WrapperComponent>
        );
      case "multi_detail":
        return (
          <WrapperComponent>
            {/* <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header> */}
            <MultiDetail
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              setEditDetail={setEditDetail}
              editDetail={editDetail}
              onRemoveItem={() => {
                removeItem(quote);
              }}
              checkRule={checkRule}
              setDrag={setDrag}
            />
          </WrapperComponent>
        );
      case "tags":
        return (
          <WrapperComponent>
            {/* <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header> */}
            <TagsComponent
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              onRemoveItem={() => {
                removeItem(quote);
              }}
              checkRule={checkRule}
              setDrag={setDrag}
            />
          </WrapperComponent>
        );
      case "table":
        return (
          <WrapperComponent>
            {/* <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header> */}
            <TableComponent
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              onRemoveItem={() => {
                removeItem(quote);
              }}
              checkRule={checkRule}
              setDrag={setDrag}
            />
          </WrapperComponent>
        );
      case "DETAILS":
        return (
          <WrapperComponent>
            <DetailsThirdParty
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              onRemoveItem={() => {
                removeItem(quote);
              }}
            />
          </WrapperComponent>
        );
      case "TABLE":
        return (
          <WrapperComponent>
            <TableThirdParty
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              onRemoveItem={() => {
                removeItem(quote);
              }}
            />
          </WrapperComponent>
        );
      case "detailsComp":
        return (
          <WrapperComponent>
            <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header>
            <DetailsComp
              data={quote}
              recordID={recordID}
              objectId={objectId}
              edit={() => {}}
              onRemoveItem={() => {
                removeItem(quote);
              }}
            />
          </WrapperComponent>
        );
      case "email":
        return (
          <WrapperComponent>
            <Header>
              {quote.name}{" "}
              <img
                alt=""
                src={close}
                style={{
                  width: "24px",
                  position: "absolute",
                  right: "10px",
                  top: "10px",
                  cursor: "pointer",
                  zIndex: 11,
                }}
                onClick={() => removeItem(quote)}
              />
            </Header>
            <div style={{ marginTop: "12px" }}>
              <Email
                record_id={recordID}
                object_id={objectId}
                edit={() => {}}
                onRemoveItem={() => {
                  removeItem(quote);
                }}
                loadListData={loadListData}
              />
            </div>
          </WrapperComponent>
        );

      case "general-configuration":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <CampaignGeneral objectId={objectId} recordID={recordID} />
          </WrapperComponent>
        );

      case "template-configuration":
        return (
          <WrapperComponent>
            {/* <Header>Comments</Header> */}
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => removeItem(quote)}
            />
            <CampaignTemplate objectId={objectId} recordID={recordID} />
          </WrapperComponent>
        );
      default:
        break;
    }
  };

  return (
    <Container
      // href={quote.author.url}
      isDragging={isDragging}
      isGroupedOver={isGroupedOver}
      colors={"fff"}
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
    >
      {handleSwitchCase()}
    </Container>
  );
};

export default QuoteItem;

const WrapperComponent = styled.div`
  /* box-shadow: 8px 5px 8px rgba(0, 0, 0, 0.07); */
  border-radius: 10px;
  border: 1px solid #ececec;
  /* overflow-y: scroll; */
  margin-bottom: 12px;
  width: 100%;
  position: relative;
  /* height: 500px; */
  resize: vertical;
  min-height: 100px;
  max-height: 500px;

  overflow: ${(props) => (props.type === "comment" ? "hidden" : "auto")};
  width: 100%;
  height: ${(props) => (props.type === "comment" ? "unset" : "100%")};
  &::-webkit-scrollbar {
    height: 8px !important;
  }

  .ant-table-wrapper {
    width: 100%;
    overflow: scroll;
  }
  /* padding: 8px; */
`;

const Header = styled.div`
  background: white;
  /* border & divider/divider ↓ */
  position: sticky;
  top: 0;
  box-shadow: inset 0px -1px 0px #f0f0f0;
  border-radius: 10px 10px 0px 0px;
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  padding: 10px 0px 10px 16px;
  gap: 10px;
  font-style: normal;
  font-family: var(--roboto-500);
  font-size: 16px;
  line-height: 19px;
  /* identical to box height */

  /* text - main */

  color: #252424;
  z-index: 10;
`;
