import { colors } from "@atlaskit/theme";
import { Button } from "antd";
import emptyImg from "assets/images/objectsManagement/pic-empty.png";
import React, { useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useSelector } from "react-redux";
import styled from "styled-components";
import DrawerComponent from "../drawerComponent";
import { borderRadius, grid } from "./constants";
import QuoteList from "./primatives/quote-list";
import Title from "./primatives/title";

const Container = styled.div`
  /* margin: ${grid}px; */
  display: flex;
  flex-direction: column;
  width: 50%;
  resize: horizontal;
  overflow: auto;
  height: 100vh;
  background-color: #fff;
  /* padding: 0 12px; */
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-top-left-radius: ${borderRadius}px;
  border-top-right-radius: ${borderRadius}px;
  /* background-color: ${({ isDragging }) =>
    isDragging ? colors.G50 : colors.N30}; */
  transition: background-color 0.2s ease;
  /* &:hover {
    background-color: ${colors.G50};
  } */
`;

const Column = (props) => {
  const {
    title,
    quotes,
    index,
    value,
    setValue,
    updateLayout,
    recordID,
    setRecordID,
    loadListData,
    objectId,
    setEditDetail,
    editDetail,
  } = props;
  const [visible, setVisible] = useState(false);

  const { data } = useSelector((state) => state.objectsReducer);
  const { userDetail } = useSelector((state) => state.userReducer);

  return (
    <>
      <Draggable draggableId={title} index={index} isDragDisabled>
        {(provided, snapshot) => (
          <Container
            ref={provided.innerRef}
            {...provided.draggableProps}
            style={{
              width: `${index === 1 ? "30%" : "70%"}`,
              marginRight: `${index === 0 ? "8px" : ""}`,
            }}
          >
            <Header isDragging={snapshot.isDragging}>
              <Title
                isDragging={snapshot.isDragging}
                {...provided.dragHandleProps}
              >
                {/* {title} */}
              </Title>
            </Header>
            <QuoteList
              listId={title}
              listType="QUOTE"
              style={{
                backgroundColor: snapshot.isDragging ? colors.G50 : null,
              }}
              quotes={quotes}
              internalScroll={props.isScrollable}
              isCombineEnabled={Boolean(props.isCombineEnabled)}
              recordID={recordID}
              setRecordID={setRecordID}
              loadListData={loadListData}
              setEditDetail={setEditDetail}
              editDetail={editDetail}
            />
            <div
              style={{
                minHeight: "300px",
                borderRadius: "5px",
                backgroundImage: `url("data:image/svg+xml,%3csvg width='100%25' height='100%25' xmlns='http://www.w3.org/2000/svg'%3e%3crect width='100%25' height='100%25' fill='none' stroke='%234296FFFF' stroke-width='4' stroke-dasharray='6%2c 14' stroke-dashoffset='0' stroke-linecap='square'/%3e%3c/svg%3e")`,
                marginBottom: "20px",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                alt=""
                src={emptyImg}
                style={{ width: "100px", marginBottom: "8px" }}
              />
              <div style={{ marginBottom: "16px" }}>No component</div>
              {data.length === 0 || recordID === "" ? (
                ""
              ) : userDetail.Is_Admin ? (
                <CustomButton
                  size="large"
                  onClick={() => {
                    setVisible(true);
                  }}
                >
                  {" "}
                  + Add component
                </CustomButton>
              ) : (
                ""
              )}
            </div>
          </Container>
        )}
      </Draggable>
      <DrawerComponent
        visible={visible}
        setVisible={setVisible}
        value={value}
        setValue={setValue}
        title={title}
        updateLayout={updateLayout}
        selectedObject={objectId}
      />
    </>
  );
};

export default Column;

const CustomButton = styled(Button)`
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 19px;
  /* identical to box height */

  display: flex;
  align-items: center;
  letter-spacing: 0.01em;

  color: #83baff;
  border: 1px solid #83baff;
  border-radius: 5px;
`;
