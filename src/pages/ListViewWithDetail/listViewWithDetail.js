import { DownOutlined, CaretDownOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Col,
  Dropdown,
  Menu,
  Row,
  Tooltip,
  Typography,
} from "antd";
// import emailIcon from "assets/icons/common/email.png";
import settingIcon from "assets/icons/common/setting-icon.png";
import pinImg from "assets/icons/objects/png.svg";
import unpinImg from "assets/icons/objects/unpin.svg";
import emptyImg from "assets/images/objectsManagement/pic-empty.png";
import _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { loadComponents } from "redux/slices/consolidatedViewSettings";
import {
  // loadAllData,
  loadDataWithOutPagi,
  // loadData,
  loadFavouriteObjects,
  loadListObjectField,
  loadListViewWithDetails,
  loadPagination,
  toggleFavouriteObject,
  updateListViewDetails,
} from "redux/slices/objects";
import {
  loadCategory,
  loadObjectCategory,
} from "redux/slices/objectsManagement";
import styled from "styled-components";
import Board from "./dragAndDropComponent/board";
import ListRecord from "./listRecord";
import ModalEmail from "./modalEmail";
import ModalSetting from "./modalSetting";
// import arrow from "assets/icons/common/Union.png";
import { Spin } from "antd";
import IconBottom from "assets/icons/common/iconBottom.png";
import IconTop from "assets/icons/common/iconTop.png";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import CustomView from "pages/Objects/customView/customView";
import Carousel from "react-multi-carousel";
import { loadAllSetting } from "redux/slices/highlightSetting";
import KanbanViewImg from "assets/icons/common/kanbanView.png";
import ListViewImg from "assets/icons/common/listView.png";
import ListViewDetailImg from "assets/icons/common/listViewDetail.png";
import { changeTitlePage } from "redux/slices/authenticated";

const { SubMenu } = Menu;

const ListViewWithDetail = () => {
  /*eslint-disable-next-line*/
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { Text: TextComponent } = Typography;
  const { objectId, customViewId } = useParams();
  const { listViewWithDetail, data: dataSource } = useSelector(
    (state) => state.objectsReducer
  );
  const [isModalVisible, setIsModalVisible] = useState(false);
  // const [, setTotalPage] = useState(0);
  // const [totalRecord, setTotalRecord] = useState(0);s
  /*eslint-disable-next-line*/
  const [recordPerPage, setRecordPerPage] = useState(50);
  const { userDetail } = useSelector((state) => state.userReducer);
  /*eslint-disable-next-line*/
  const [currentPage, setCurrentPage] = useState(1);
  const [recordID, setRecordID] = useState("");
  /*eslint-disable-next-line*/
  const [value, setValue] = useState({
    Left: [],
    Right: [],
  });
  const [editDetail, setEditDetail] = useState(true);
  let navigate = useNavigate();
  /*eslint-disable-next-line*/
  const [openCustomView, setOpenCustomView] = useState(false);
  const { visibleList, objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { allSetting } = useSelector((state) => state.highlightSettingReducer);
  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);

  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);
  useEffect(() => {
    dispatch(changeTitlePage("List view with detail"));
    //eslint-disable-next-line
  }, [t]);

  const [styleCustom, setStyleCustom] = useState({});
  useEffect(() => {
    dispatch(
      loadAllSetting({
        current_page: 1,
        record_per_page: 1000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (objectId && allSetting.length > 0) {
      const tempStyle = allSetting.filter(
        (setting) => setting.status && setting.object_id === objectId
      );
      if (tempStyle) {
        setStyleCustom(tempStyle);
      }
    }
  }, [allSetting, objectId]);

  useEffect(() => {
    setCurrentPage(1);
    setRecordPerPage(50);
  }, [objectId]);

  const {
    objectsFavourite,
    totalRecord,
    customView,
    data,
    isLoadingDataWithOutPagi,
    isLoadingPagi,
  } = useSelector((state) => state.objectsReducer);

  const [open, setOpen] = useState(false);
  const [openEmail, setOpenMail] = useState(false);
  const menu = (
    <CustimMenuWrapper triggerSubMenuAction="click" style={{ width: "200px" }}>
      {/*eslint-disable-next-line*/}
      {Object.entries(objectCategory).map(([key, value], idx) => {
        if (visibleList[key])
          return (
            <CustomSubMenuWrapper key={key} title={key}>
              {/*eslint-disable-next-line*/}
              {value.map((item, index) => {
                if (item.visible === true && item.Status === true)
                  return (
                    <CustomMenuItem
                      key={item._id}
                      style={{
                        whiteSpace: "normal",
                        wordBreak: "break-all",
                        width: "200px",
                      }}
                    >
                      <CustomOption>
                        <div> {item.Name}</div>
                        {objectsFavourite.findIndex(
                          (object) => object.object_id === item._id
                        ) >= 0 ? (
                          <img
                            className="unpin-img"
                            alt=""
                            src={unpinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        ) : (
                          <img
                            className="pin-img"
                            alt=""
                            src={pinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        )}
                      </CustomOption>
                    </CustomMenuItem>
                  );
              })}
            </CustomSubMenuWrapper>
          );
      })}
    </CustimMenuWrapper>
  );

  useEffect(() => {
    dispatch(loadCategory());
    dispatch(loadObjectCategory());
    dispatch(loadFavouriteObjects());
    dispatch(
      loadComponents({
        object_id: objectId,
      })
    );
    dispatch(
      loadListViewWithDetails({
        object_id: objectId,
      })
    );
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );
    loadListData();
    /*eslint-disable-next-line*/
  }, [objectId, dispatch, customViewId]);

  const updateLayout = (value) => {
    dispatch(
      updateListViewDetails({
        primary_field: listViewWithDetail.primary_field,
        object_id: objectId,
        field_ids: listViewWithDetail.field_ids,
        layouts: value,
      })
    );
  };
  const loadLayout = useCallback(() => {
    dispatch(
      loadListViewWithDetails({
        object_id: objectId,
      })
    );
  }, [objectId, dispatch]);

  useEffect(() => {
    if (recordID) {
      loadLayout();
    }
  }, [recordID, loadLayout]);

  useEffect(() => {
    if (customViewId === "default-view")
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    else
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
  }, [objectId, firstID, lastID, dispatch, customViewId]);

  const handleCustomViewName = (id) => {
    if (id === "default-view") {
      return "Default";
    }
    let name =
      customView &&
      customView.custom_views &&
      customView.custom_views.find((item) => item._id === id);
    return name && name.view_name;
  };

  const loadListData = () => {
    if (customViewId === "default-view")
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    else
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
  };

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1880 },
      items: 12,
      slidesToSlide: 12,
    },
    tablet: {
      breakpoint: { max: 1880, min: 1400 },
      items: 9,
      slidesToSlide: 9,
    },
    mobile: {
      breakpoint: { max: 1400, min: 0 },
      items: 7,
      slidesToSlide: 7,
    },
  };

  const menuView = (
    <Menu>
      <CustomItem
        onClick={() => {
          navigate("/objects/" + objectId + "/" + customViewId);
        }}
      >
        <ItemText> List view </ItemText>
        <img src={ListViewImg} alt="List view" />
      </CustomItem>
      <CustomItem
        onClick={() => {
          navigate(`/kanban-view/${objectId}/${customViewId}`);
        }}
      >
        <ItemText>Kanban view</ItemText>
        <img src={KanbanViewImg} alt=" Kanban view" />
      </CustomItem>
    </Menu>
  );

  useEffect(() => {
    setNext(
      dataSource.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + dataSource.length
    );
    /* eslint-disable-next-line */
  }, [dataSource]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
    /* eslint-disable-next-line */
  }, [currentPage]);

  return (
    <>
      {" "}
      <CustomHeader>
        {open === false ? (
          ""
        ) : (
          <CustomMenuHeader>
            <CarouselWrap>
              <Carousel responsive={responsive}>
                {objectsFavourite.map((item, idx) => {
                  if (item && item.object_name)
                    return (
                      <MenuItem
                        className={`btn btn-top ${
                          objectId === item.object_id ? "active" : ""
                        }`}
                        key={item.object_id}
                        onClick={() =>
                          navigate(
                            `/list-view-with-details/objects/${item.object_id}/default-view`
                          )
                        }
                      >
                        <TextComponent
                          style={{
                            color: `${
                              objectId === item.object_id ? "#fff" : "#2c2c2c"
                            }`,
                          }}
                          ellipsis={{ tooltip: item.object_name }}
                        >
                          {item.object_name}
                        </TextComponent>
                      </MenuItem>
                    );
                  return null;
                })}
              </Carousel>
            </CarouselWrap>
            <CustomDropdown
              overlay={menu}
              placement="bottomLeft"
              trigger="click"
            >
              <CustomNewButton>
                Tất cả <DownOutlined />
              </CustomNewButton>
            </CustomDropdown>
            <CustomDisplay>
              Hiển thị:
              <span
                onClick={() => {
                  setOpenCustomView(true);
                }}
              >
                {handleCustomViewName(customViewId)}
              </span>
            </CustomDisplay>
          </CustomMenuHeader>
        )}

        <div style={{ display: "none" }}>
          <CustomClose
            onClick={() => {
              setOpen(!open);
            }}
          >
            {open === false ? (
              <>
                <img src={IconBottom} alt="" />
              </>
            ) : (
              <>
                <img src={IconTop} alt="" />
              </>
            )}
          </CustomClose>
        </div>
      </CustomHeader>
      <Wrapper>
        <ContentWrapper>
          <div
            style={{
              display: "flex",
              justifyContent: "space-between",
              marginBottom: "16px",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                marginTop: "16px",
              }}
            >
              <CustomHeaderBreadcum>
                <Breadcrumb>
                  {/* <Breadcrumb.Item> */}
                  {/* eslint-disable-next-line  */}
                  {/* <a onClick={() => navigate("/settings")}> */}
                  {/* {t("common.settings")} */}
                  {/* </a> */}
                  {/* </Breadcrumb.Item> */}
                  <BreadcrumbItem>
                    {/* eslint-disable-next-line */}
                    List view with details
                  </BreadcrumbItem>
                </Breadcrumb>
              </CustomHeaderBreadcum>
              {/* <CustomButtonMenu
                size="small"
                style={{ marginLeft: "16px" }}
                onClick={() => navigate(`/objects/${objectId}/${customViewId}`)}
              >
                <img
                  alt=""
                  src={arrow}
                  style={{ width: "10px", marginRight: "4px" }}
                />
                Back to list view
              </CustomButtonMenu> */}
              {/*eslint-disable-next-line*/}
              {/* <a
                href="#"
                onClick={() => navigate(`/objects/${objectId}/${customViewId}`)}
                style={{ marginLeft: "20px" }}
              >
                <img
                  alt=""
                  src={arrow}
                  style={{ width: "10px", marginRight: "4px" }}
                />
                Back to list view
              </a> */}
            </div>

            {/* <CustomPagination
              showQuickJumper
              current={currentPage}
              total={totalRecord}
              showSizeChanger
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              // pageSize={recordPerPage}
              onChange={(e, pageSize) => {
                setCurrentPage(e);
                setRecordPerPage(pageSize);
              }}
            /> */}

            <NewCustomPagination>
              <ViewWrap>
                <Dropdown overlay={menuView} arrow trigger={["click"]}>
                  <OptionView>
                    <img src={ListViewDetailImg} alt="" />
                    <Tooltip title="List view with detail">
                      <Text>List view with detail</Text>
                    </Tooltip>{" "}
                    <CaretDownOutlined />
                  </OptionView>
                </Dropdown>
              </ViewWrap>
              {isLoadingDataWithOutPagi ? (
                <Spin />
              ) : (
                <>
                  <div className="total-record">
                    {prev} - {next}{" "}
                    {isLoadingPagi ? (
                      <Spin style={{ marginLeft: "10px" }} />
                    ) : totalRecord === null ? (
                      <div
                        className="reload-pagi"
                        onClick={() => {
                          dispatch(
                            loadPagination({
                              current_page: 1,
                              record_per_page: 50,
                              object_id: objectId,
                            })
                          );
                        }}
                      >
                        <img alt="" src={Reload} />
                      </div>
                    ) : (
                      `of ${totalRecord} records`
                    )}
                  </div>
                </>
              )}
              <div
                className="left-pagi"
                style={{
                  pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                  cursor: `${currentPage === 1 ? "not-allowed" : "pointer"}`,
                  opacity: `${currentPage === 1 ? 0.5 : 1}`,
                }}
                onClick={() => {
                  setLastID(null);
                  let currentPageTemp = currentPage;
                  currentPageTemp = currentPageTemp - 1;
                  setCurrentPage(currentPageTemp);
                  // setPrev(currentPageTemp * recordPerPage - recordPerPage + 1);
                  // setNext(
                  //   dataSource.length === recordPerPage
                  //     ? currentPageTemp * recordPerPage
                  //     : (currentPageTemp - 1) * recordPerPage + dataSource.length
                  // );
                  setFirstID(dataSource.length > 0 ? dataSource[0]._id : null);
                }}
              >
                <img alt="" src={LeftPagi} />
              </div>
              <div
                className="right-pagi"
                style={{
                  pointerEvents: `${
                    dataSource.length < recordPerPage ? "none" : ""
                  }`,
                  cursor: `${
                    dataSource.length < recordPerPage
                      ? "not-allowed"
                      : "pointer"
                  }`,
                  opacity: `${dataSource.length < recordPerPage ? 0.5 : 1}`,
                }}
                onClick={() => {
                  setLastID(
                    dataSource.length > 0
                      ? dataSource[dataSource.length - 1]._id
                      : null
                  );
                  let currentPageTemp = currentPage;
                  currentPageTemp = currentPageTemp + 1;
                  setCurrentPage(currentPageTemp);
                  // setPrev(currentPageTemp * recordPerPage - recordPerPage + 1);
                  // setNext(
                  //   dataSource.length === recordPerPage
                  //     ? currentPageTemp * recordPerPage
                  //     : (currentPageTemp - 1) * recordPerPage + dataSource.length
                  // );
                  setFirstID(null);
                }}
              >
                <img alt="" src={RightPagi} />
              </div>
            </NewCustomPagination>
          </div>
          {_.isArray(listViewWithDetail) === false ? (
            <Row>
              <Col span={5}>
                <ListRecord
                  recordID={recordID}
                  setRecordID={setRecordID}
                  objectId={objectId}
                  styleCustom={styleCustom}
                  loadListData={loadListData}
                />
              </Col>
              <Col span={19} style={{ overflowY: "scroll" }}>
                <Board
                  initial={
                    data.length === 0 || recordID === ""
                      ? { Left: [], Right: [] }
                      : listViewWithDetail.layouts
                  }
                  setValue={setValue}
                  updateLayout={updateLayout}
                  recordID={recordID}
                  setRecordID={setRecordID}
                  loadListData={loadListData}
                  setEditDetail={setEditDetail}
                  editDetail={editDetail}
                />
              </Col>
            </Row>
          ) : (
            <div
              style={{
                height: "300px",
                borderRadius: "5px",
                marginBottom: "20px",
                display: "flex",
                flexDirection: "column",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img
                alt=""
                src={emptyImg}
                style={{ width: "100px", marginBottom: "8px" }}
              />
              <div style={{ marginBottom: "16px", fontSize: "16px" }}>
                Hiện chưa{" "}
                <span className="setting-display">Cài đặt hiển thị</span>
              </div>
              {userDetail.Is_Admin ? (
                <CustomButton
                  size="large"
                  onClick={() => {
                    setIsModalVisible(true);
                  }}
                >
                  Cài đặt ngay
                </CustomButton>
              ) : (
                ""
              )}
            </div>
          )}
        </ContentWrapper>
        <ModalSetting
          isModalVisible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
          objectId={objectId}
        />
        {/* <Tooltip title="Email" placement="leftBottom">
          <CustomButtonEmail
            shape="circle"
            size="large"
            onClick={() => {
              setOpenMail(true);
            }}
          >
            <img alt="" src={emailIcon} style={{ width: "24px" }} />
          </CustomButtonEmail>
        </Tooltip> */}
        {userDetail.Is_Admin ? (
          <Tooltip title="Settings" placement="leftBottom">
            <CustomButtonSetting
              shape="circle"
              size="large"
              onClick={() => {
                setIsModalVisible(true);
              }}
            >
              <img alt="" src={settingIcon} style={{ width: "24px" }} />
            </CustomButtonSetting>
          </Tooltip>
        ) : (
          ""
        )}

        <CustomView
          visible={openCustomView}
          setVisible={setOpenCustomView}
          objectId={objectId}
          reload={loadListData}
          mode="list-view-with-details"
        />
        <ModalEmail
          recordID={recordID}
          objectId={objectId}
          isModalVisible={openEmail}
          setIsModalVisible={setOpenMail}
        />
      </Wrapper>
    </>
  );
};

export default ListViewWithDetail;

const Wrapper = styled.div`
  /* position: relative; */
  padding: 24px;
  padding-top: 0;
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .setting-display {
    color: ${(props) => props.theme.main};
  }
  a {
    :hover {
      text-decoration: underline;
    }
  }
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  :hover {
    color: #fff;
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.main};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 16px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomHeaderBreadcum = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  /* align-items: center; */
  /* margin-bottom: 24px; */
`;

const ContentWrapper = styled.div``;

const CustomButtonSetting = styled(Button)`
  background-color: ${(props) => props.theme.main};
  border: none;
  z-index: 20;
  position: absolute;
  bottom: 80px;
  right: 40px;
  filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  :hover {
    background-color: ${(props) => props.theme.main};
    border: none;
    filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  }
  :focus {
    background-color: ${(props) => props.theme.main};
    border: none;
    filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
  }
`;

// const CustomButtonEmail = styled(Button)`
//   background-color: ${(props) => props.theme.main};
//   border: none;
//   z-index: 20;
//   position: absolute;
//   bottom: 130px;
//   right: 40px;
//   filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
//   :hover {
//     background-color: ${(props) => props.theme.main};
//     border: none;
//     filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
//   }
//   :focus {
//     background-color: ${(props) => props.theme.main};
//     border: none;
//     filter: drop-shadow(0px 0px 12px rgba(0, 0, 0, 0.08));
//   }
// `;

const CustimMenuWrapper = styled(Menu)`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
    border-right: 2px solid ${(props) => props.theme.darker};
  }
  li {
    padding-top: 8px;
    padding-bottom: 8px;
  }
`;

const CustomSubMenuWrapper = styled(SubMenu)`
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomOption = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .pin-img {
    display: none;
  }
  .unpin-img {
    width: 18px;
    margin-right: 8px;
  }
`;

const CustomMenuItem = styled.div`
  cursor: pointer;
  :hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
    border-right: 2px solid ${(props) => props.theme.darker};

    .pin-img {
      display: block;
      width: 16px;
      margin-right: 8px;
    }
  }
  padding: 8px 8px 8px 12px;
`;

const CustomMenuHeader = styled.div`
  background-color: #fff;
  padding: 11px;
  width: 100%;
  display: flex;
  padding-left: 24px;
  .active {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    color: #fff;
  }
  -webkit-animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  @-webkit-keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
`;

const MenuItem = styled.div`
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #2c2c2c;
  padding: 5px 16px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  margin-right: 8px;
  /* width: 120px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden; */

  :hover {
    /* background-color: ${(props) => props.theme.darker}; */
    border: 1px solid ${(props) => props.theme.darker};

    border-radius: 2px;
    cursor: pointer;
  }
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  text-align: center;

  /* Character/Color text main */
  color: #2c2c2c;
  margin-right: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px 16px;
  /* :hover {
    color: ${(props) => props.theme.main};
  } */
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomHeader = styled.div`
  /* max-width: max-content; */
  /* width: max-content; */
  min-width: 100%;
  /* overflow-x: scroll; */
  .ant-btn:active {
    border: none;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    text-align: center;

    /* Character/Color text main */

    color: #2c2c2c;
    background-color: #fff;
  }

  .btn {
    position: relative;
    /* transition: all 200ms cubic-bezier(0.77, 0, 0.175, 1); */
    transition: all 0.5s;
    cursor: pointer;
    border-radius: 2px;
  }

  .btn:before,
  .btn:after {
    content: "";
    position: absolute;
    /* transition: inherit; */
    z-index: -1;
  }

  /* .btn:hover {
    color: #ffffff;
    transition-delay: 0.5s;
  }

  .btn:hover:before {
    transition-delay: 0s;
  }

  .btn:hover:after {
    background: ${(props) => props.theme.main};
    transition-delay: 0.35s;
  } */

  /* From Top */

  .btn-top:before,
  .btn-top:after {
    left: 0;
    height: 0;
    width: 100%;
  }

  .btn-top:before {
    bottom: 0;
    /* border: 1px solid ${(props) => props.theme.main}; */
    border-top: 0;
    border-bottom: 0;
  }

  .btn-top:after {
    top: 0;
    height: 0;
  }

  .btn-top:hover:before,
  .btn-top:hover:after {
    height: 100%;
  }

  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const CustomNewButton = styled(Button)`
  border: 1px solid #d9d9d9 !important;
  height: 34px;
  :hover {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }
`;

// const CustomButtonMenu = styled(Button)`
//   border: none;
//   font-style: normal;
//   font-family: var(--roboto-400);
//   font-size: 16px;
//   line-height: 22px;
//   /* identical to box height, or 137% */

//   text-align: center;

//   /* Character/Color text main */

//   color: #2c2c2c;
//   background-color: #fff;
//   :hover {
//     background-color: ${(props) => props.theme.main};
//     color: #fff;
//   }
// `;

// const CustomPagination = styled(Pagination)`
//   /* position: fixed; */
//   /* bottom: 50px; */
//   /* right: 16px; */
//   /* width: calc(100% - 32px); */
//   text-align: right;
//   /* background-color: #fff; */
//   /* padding: 16px; */
//   /* border: 1px solid #ececec; */
//   /* margin: auto; */
//   .ant-pagination-item-active {
//     border-color: ${(props) => props.theme.main};
//     a {
//       color: ${(props) => props.theme.main};
//     }
//   }
//   .ant-pagination-item:hover {
//     border-color: ${(props) => props.theme.main};
//     a {
//       color: ${(props) => props.theme.main};
//     }
//   }
// `;
const CarouselWrap = styled.div`
  width: calc(100% - 260px);
  .react-multiple-carousel__arrow--right {
    right: 0;
    background: linear-gradient(
      268deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 90.08%
    );
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow--left {
    background: linear-gradient(
      90deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 87.93%
    );
    left: 0;
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow::before {
    color: ${(props) => props.theme.main};
    font-size: 20px;
  }
  .react-multiple-carousel__arrow--left::before {
    margin-left: -50px;
  }
  .react-multiple-carousel__arrow--right::before {
    margin-right: -50px;
  }
`;

const CustomDisplay = styled.div`
  margin-left: 10px;
  padding-top: 6px;
  font-size: 16px;
  line-height: 22px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 150px;
  span {
    margin-left: 6px;
    text-decoration: underline;
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;

const CustomClose = styled.div`
  display: flex;
  justify-content: center;
  img {
    cursor: pointer;
  }
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  /* background-color: white; */
  padding: 10px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;

const CustomItem = styled(Menu.Item)`
  .ant-dropdown-menu-title-content {
    display: flex;
    justify-content: space-between;
  }

  img {
    width: 19px;
  }
`;

const ItemText = styled.span`
  font-size: 16px;
  line-height: 22px;
  margin-right: 30px;
  margin-left: 8px;
`;

const ViewWrap = styled.div`
  margin-right: 16px;
  width: 140px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  cursor: pointer;
`;

const OptionView = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Text = styled.div`
  margin-left: 6px;
  font-size: 16px;
  width: 85px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;
