import { Typography } from "antd";
/*eslint-disable-next-line*/
import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";

const { Text } = Typography;
const ListRecord = (props) => {
  //   const { data } = props;
  /*eslint-disable-next-line*/
  const { recordID, setRecordID, objectId, styleCustom, loadListData } = props;
  const { listViewWithDetail, data } = useSelector(
    (state) => state.objectsReducer
  );

  useEffect(() => {
    if (data.length > 0) {
      setRecordID(data[0]._id);
    }
    /*eslint-disable-next-line*/
  }, [objectId]);

  const handleLabelName = (id) => {
    let name = "";
    switch (id) {
      case "created_date":
        name = "Created date :";
        break;

      case "created_by":
        name = "Created by :";
        break;

      case "modify_time":
        name = "Modify time :";
        break;

      case "modify_by":
        name = "Modify by :";
        break;

      case "owner":
        name = "Assign to :";
        break;

      default:
        listViewWithDetail.field_names.map((item, idx) => {
          if (item.ID === id) {
            name = item.name + ":";
            return null;
          } else return null;
        });
    }
    return <span style={{ marginRight: "8px" }}>{name} </span>;
  };

  return (
    <ListRecordComp
      style={{ height: "100vh", overflowY: "scroll" }}
      className="list-record"
    >
      {data.map((item, idx) => {
        return (
          <RecordItem
            onClick={() => {
              setRecordID(item._id);
            }}
            className={`${item._id === recordID ? "active" : ""}`}
            background={
              styleCustom.length > 0 && styleCustom[0].type === "row"
                ? styleCustom[0].style.filter(
                    (ele) => ele.value === item[styleCustom[0].field_id]?.value
                  )[0]?.background
                : "#fff"
            }
            fontWeight={
              styleCustom.length > 0 && styleCustom[0].type === "row"
                ? styleCustom[0].style.filter(
                    (ele) => ele.value === item[styleCustom[0].field_id]?.value
                  )[0]?.font_weight
                : "400"
            }
            color={
              styleCustom.length > 0 && styleCustom[0].type === "cell"
                ? styleCustom[0].style.filter(
                    (ele) => ele.value === item[styleCustom[0].field_id]?.value
                  )[0]?.color
                : "#000"
            }
          >
            <div className="title">
              <Text
                ellipsis={{
                  tooltip:
                    typeof (
                      item &&
                      item[listViewWithDetail.primary_field] &&
                      item[listViewWithDetail.primary_field]
                    ) === "object"
                      ? item &&
                        item[listViewWithDetail.primary_field] &&
                        item[listViewWithDetail.primary_field].value
                      : item &&
                        item[listViewWithDetail.primary_field] &&
                        item[listViewWithDetail.primary_field],
                }}
              >
                {/* {handleLabelName(listViewWithDetail.primary_field)} */}
                {typeof (
                  item &&
                  item[listViewWithDetail.primary_field] &&
                  item[listViewWithDetail.primary_field]
                ) === "object"
                  ? item &&
                    item[listViewWithDetail.primary_field] &&
                    item[listViewWithDetail.primary_field].value
                  : item &&
                    item[listViewWithDetail.primary_field] &&
                    item[listViewWithDetail.primary_field]}
              </Text>
            </div>
            <div className="sub-value">
              <Text
                ellipsis={{
                  tooltip:
                    typeof (
                      item &&
                      item[listViewWithDetail.field_ids[0]] &&
                      item[listViewWithDetail.field_ids[0]]
                    ) === "object"
                      ? item &&
                        item[listViewWithDetail.field_ids[0]] &&
                        item[listViewWithDetail.field_ids[0]].value
                      : item &&
                        item[listViewWithDetail.field_ids[0]] &&
                        item[listViewWithDetail.field_ids[0]],
                }}
              >
                {handleLabelName(listViewWithDetail.field_ids[0])}
                {typeof (
                  item &&
                  item[listViewWithDetail.field_ids[0]] &&
                  item[listViewWithDetail.field_ids[0]]
                ) === "object"
                  ? item &&
                    item[listViewWithDetail.field_ids[0]] &&
                    item[listViewWithDetail.field_ids[0]].value
                  : item &&
                    item[listViewWithDetail.field_ids[0]] &&
                    item[listViewWithDetail.field_ids[0]]}
              </Text>
            </div>
            <div className="sub-value">
              <Text
                ellipsis={{
                  tooltip:
                    typeof (
                      item &&
                      item[listViewWithDetail.field_ids[1]] &&
                      item[listViewWithDetail.field_ids[1]]
                    ) === "object"
                      ? item &&
                        item[listViewWithDetail.field_ids[1]] &&
                        item[listViewWithDetail.field_ids[1]].value
                      : item &&
                        item[listViewWithDetail.field_ids[1]] &&
                        item[listViewWithDetail.field_ids[1]],
                }}
              >
                {handleLabelName(listViewWithDetail.field_ids[1])}
                {typeof (
                  item &&
                  item[listViewWithDetail.field_ids[1]] &&
                  item[listViewWithDetail.field_ids[1]]
                ) === "object"
                  ? item &&
                    item[listViewWithDetail.field_ids[1]] &&
                    item[listViewWithDetail.field_ids[1]].value
                  : item &&
                    item[listViewWithDetail.field_ids[1]] &&
                    item[listViewWithDetail.field_ids[1]]}
              </Text>
            </div>
            <div className="sub-value">
              <Text
                ellipsis={{
                  tooltip:
                    typeof (
                      item &&
                      item[listViewWithDetail.field_ids[2]] &&
                      item[listViewWithDetail.field_ids[2]]
                    ) === "object"
                      ? item &&
                        item[listViewWithDetail.field_ids[2]] &&
                        item[listViewWithDetail.field_ids[2]].value
                      : item &&
                        item[listViewWithDetail.field_ids[2]] &&
                        item[listViewWithDetail.field_ids[2]],
                }}
              >
                {handleLabelName(listViewWithDetail.field_ids[2])}
                {typeof (
                  item &&
                  item[listViewWithDetail.field_ids[2]] &&
                  item[listViewWithDetail.field_ids[2]]
                ) === "object"
                  ? item &&
                    item[listViewWithDetail.field_ids[2]] &&
                    item[listViewWithDetail.field_ids[2]].value
                  : item &&
                    item[listViewWithDetail.field_ids[2]] &&
                    item[listViewWithDetail.field_ids[2]]}
              </Text>
            </div>
          </RecordItem>
        );
      })}
    </ListRecordComp>
  );
};

export default ListRecord;

const ListRecordComp = styled.div`
  width: 100%;
  height: 100vh;
  overflow-y: scroll;
  /* Hide scrollbar for Chrome, Safari and Opera */

  ::-webkit-scrollbar {
    display: none;
    width: 0;
  }

  /* Hide scrollbar for IE, Edge and Firefox */
  -ms-overflow-style: none; /* IE and Edge */
  scrollbar-width: none; /* Firefox */

  .active {
    width: 100%;
    border-left: 5px solid ${(props) => props.theme.main};
    transition: 0.5s;
    .title {
      .ant-typography {
        color: ${(props) => props.theme.main} !important;
      }
    }
  }
`;

const RecordItem = styled.div`
  height: 150px;

  /* Neutral/1 */
  width: calc(100% - 10px);
  background: #ffffff;
  padding: 16px;
  border-bottom-left-radius: 5px;
  border-top-left-radius: 5px;
  border-left: 5px solid ${(props) => props.theme.white};
  margin-bottom: 8px;
  overflow-y: auto;
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* margin-bottom: 4px; */
    /* identical to box height, or 144% */

    /* Primary/6 */
    color: red !important;
    /* color: ${(props) => props.theme.main}; */
  }
  .sub-value {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 24px;
    /* or 150% */

    /* Character/Body text */

    color: #2c2c2c;
    margin-bottom: 4px;
  }
  :hover {
    cursor: pointer;
    border-left: 5px solid ${(props) => props.theme.main};
    transition: 0.8s;
    box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    .title {
      font-style: normal;
      font-family: var(--roboto-500);
      font-size: 18px;
      line-height: 26px;
      /* margin-bottom: 4px; */
      /* identical to box height, or 144% */
      color: red !important;
      /* Primary/6 */

      /* color: ${(props) => props.theme.main}; */
      .ant-typography {
        color: ${(props) => props.theme.main} !important;
      }
    }
  }
  background: ${({ background }) => background}!important;

  .ant-typography {
    color: ${({ color }) => color}!important;
    font-family: ${({ fontWeight }) =>
      fontWeight === "300"
        ? "var(--roboto-300)"
        : fontWeight === "700"
        ? "var(--roboto-700)"
        : "var(--roboto-400)"}!important;
    font-size: ${({ fontSize }) => fontSize}px!important;
  }
`;
