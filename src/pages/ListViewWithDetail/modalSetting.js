import { Button, Form, Modal, Select } from "antd";
import React, { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateListViewDetails } from "redux/slices/objects";
import styled from "styled-components";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

const { Option, OptGroup } = Select;

const ModalSetting = (props) => {
  const { isModalVisible, setIsModalVisible, objectId } = props;
  const [form] = Form.useForm();
  const {
    listViewWithDetail,
    // loadListViewDetails,
    // listObjectField,
    updateListViewDetail,
  } = useSelector((state) => state.objectsReducer);
  const dispatch = useDispatch();

  const [multipleId, setMultipleId] = useState([]);

  useEffect(() => {
    setMultipleId([]);
  }, [objectId]);

  const handleOk = (values) => {
    dispatch(
      updateListViewDetails({
        primary_field: values.title,
        object_id: objectId,
        field_ids: values.fields,
        layouts: (listViewWithDetail && listViewWithDetail.layouts) || {},
      })
    );
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setMultipleId([]);
  };

  useEffect(() => {
    if (updateListViewDetail === false) {
      setIsModalVisible(updateListViewDetail);
    }
  }, [updateListViewDetail, setIsModalVisible]);

  useEffect(() => {
    if (isModalVisible)
      if (listViewWithDetail.primary_field && listViewWithDetail.field_ids) {
        form.setFieldsValue({
          title: listViewWithDetail.primary_field,
          fields: listViewWithDetail.field_ids,
        });
        setMultipleId(listViewWithDetail.field_ids);
      } else {
        form.setFieldsValue({
          title: "",
          fields: [],
        });
      }
  }, [listViewWithDetail, isModalVisible, form]);

  const [listField, setListField] = useState({});

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/objects-fields-permission",
          {
            api_version: "2",
            object_id: objectId,
            show_meta_fields: true,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          let temp = {};
          res.data.data.forEach((item, idx) => {
            temp = { ...temp, ...item };
          });
          setListField(temp);
        })
        .catch((err) => {});
    };
    checkToken();
  }, [objectId]);

  return (
    <>
      <Modal
        title="Cài đặt hiển thị"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          defaultValue={{
            title: listViewWithDetail.primary_field,
            fields: listViewWithDetail.field_ids,
          }}
          onFinish={handleOk}
          onFinishFailed={() => {}}
          autoComplete="off"
          layout="horizontal"
          labelAlign="left"
          form={form}
        >
          <Form.Item
            label="Title"
            name="title"
            rules={[
              {
                required: true,
                message: "Please select field!",
              },
            ]}
          >
            <Select allowClear>
              {listField &&
                listField["main_object"] &&
                listField["main_object"]["sections"] &&
                listField["main_object"]["sections"].map((section, idx) => {
                  return (
                    <OptGroup label={section.section_name}>
                      {section.fields.map((item, idx) => {
                        return <Option value={item.ID}>{item.name}</Option>;
                      })}
                    </OptGroup>
                  );
                })}
            </Select>
          </Form.Item>

          <Form.Item
            label="Fields"
            name="fields"
            rules={[
              {
                required: true,
                message: "Please select fields!",
              },
            ]}
          >
            <Select
              allowClear
              mode="multiple"
              onChange={(e) => {
                setMultipleId(e);
              }}
            >
              {listField &&
                listField["main_object"] &&
                listField["main_object"]["sections"] &&
                listField["main_object"]["sections"].map((section, idx) => {
                  return (
                    <OptGroup label={section.section_name}>
                      {section.fields.map((item, idx) => {
                        return (
                          <Option
                            value={item.ID}
                            // disabled={multipleId >= 3}
                            disabled={
                              multipleId.length > 2
                                ? multipleId.includes(item.ID)
                                  ? false
                                  : true
                                : false
                            }
                          >
                            {item.name}
                          </Option>
                        );
                      })}
                    </OptGroup>
                  );
                })}
            </Select>
          </Form.Item>

          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              //   loading={isLoading}
            >
              Save
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                // dispatch(setShowModal(false));
                // dispatch(loadRecordDataSuccess({}));
                // setRecordID("");
                // setEditingKey("");
                // form.resetFields();
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </Modal>
    </>
  );
};

export default ModalSetting;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  /* margin-top: 34px; */
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
  /* margin-bottom: 10px; */
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background-color: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
