import { Button, Drawer } from "antd";
import Comment from "assets/images/consolidatedView/comment.png";
import detailsImg from "assets/images/consolidatedView/detail.png";
import LogsImg from "assets/images/consolidatedView/Log.png";
import OtherImg from "assets/images/consolidatedView/orther.png";
import SLAImg from "assets/images/consolidatedView/SLA.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import TimelineHorizontalImg from "assets/images/consolidatedView/timeline-horizontal.png";
import TimelineVerticalImg from "assets/images/consolidatedView/timeline-vertical.png";
import Vector from "assets/images/consolidatedView/Vector.png";
import detailIcon from "assets/icons/common/detailIcon.png";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import { Notification } from "components/Notification/Noti";
import General from "assets/images/consolidatedView/General configuration.png";
import Template from "assets/images/consolidatedView/template configuration.png";
// import { useState } from "react";

const AddComponent = (props) => {
  const {
    visible,
    setVisible,
    value,
    setValue,
    title,
    updateLayout,
    selectedObject,
  } = props;
  const { listViewWithDetail } = useSelector((state) => state.objectsReducer);
  const [allComponents, setAllComponents] = useState([]);
  // let items = [];
  const onClose = () => {
    setVisible(false);
  };

  const { components } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  useEffect(() => {
    let arr = [];
    listViewWithDetail.layouts["Left"].map((item, idx) => {
      return arr.push(item);
    });
    listViewWithDetail.layouts["Right"].map((item, idx) => {
      return arr.push(item);
    });
    setAllComponents(arr);
  }, [listViewWithDetail]);

  const handleTypeImg = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return detailsImg;
      case "table":
      case "TABLE":
        return tableImg;
      case "tags":
        return tagsImg;
      case "tab":
        return tabsImg;
      default:
        break;
    }
  };
  const handleTypeTitle = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
        return "Details";
      case "table":
      case "TABLE":
        return "Table";
      case "tags":
        return "Tags";
      case "tab":
        return "Tab";
      default:
        break;
    }
  };

  return (
    <>
      <CustomDrawer
        title="Component"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={665}
        footer={
          <CustomFooter>
            {/* <CustomButtonSave
              size="large"
              htmlType="submit"
              //   isLoading={isLoadingCreate}
            >
              Save
            </CustomButtonSave> */}
            <CustomButtonCancel
              size="large"
              onClick={() => {
                onClose();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        }
      >
        <Wrapper>
          {/* eslint-disable */}
          {components.map((item, idx) => {
            let activate = listViewWithDetail.layouts["Left"].find(
              (ele) => ele._id === item._id
            )
              ? "active"
              : "";

            if (activate) {
            } else {
              activate = listViewWithDetail.layouts["Right"].find(
                (ele) => ele._id === item._id
              )
                ? "active"
                : "";
            }
            //   ? "active"
            //   : "";
            // let activate = "";
            if (item.type !== "tab")
              return (
                <div
                  className={`component-item droppable-element ${activate}`}
                  onClick={() => {
                    if (activate) {
                      Notification("error", "Component has already added!");
                    } else {
                      let newItem = {
                        _id: item._id,
                        id: item._id,
                      };
                      // onAddItem(newItem);
                      let newValue = { ...value };
                      newValue[title] = newValue[title].concat(newItem);
                      setValue(newValue);
                      updateLayout(newValue);
                    }
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <img
                      alt=""
                      src={handleTypeImg(item.type || item.component_type)}
                    />
                    <div className="info">
                      <div className="title">{item.name}</div>
                      <div className="decs">
                        {handleTypeTitle(item.type || item.component_type)}
                        {item.component_type ? "" : "• Related object: "}
                        {item.related_object_name}
                      </div>
                    </div>
                  </div>
                  {activate ? (
                    <div style={{ width: "22px" }}>
                      <img
                        alt=""
                        src={Vector}
                        className="vector-img"
                        style={{ width: "100%" }}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              );
          })}

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex((ele) => ele._id === "sla") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find((ele) => ele._id === "sla")
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "sla",
                  type: "sla",
                  name: "SLA Component",
                  author: title,
                  id: "sla",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={SLAImg} />
              <div className="info">
                <div className="title">SLA</div>
                <div className="decs">Service-level agreement</div>
              </div>
            </div>
            {allComponents.findIndex((ele) => ele._id === "sla") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex(
                (ele) => ele._id === "vertical-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find(
                (ele) => ele._id === "vertical-timeline"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "vertical-timeline",
                  type: "vertical-timeline",
                  name: "Vertical Timeline Component",
                  author: title,
                  id: "vertical-timeline",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineVerticalImg} />
              <div className="info">
                <div className="title">Vertical Timeline</div>
                <div className="decs">Vertical Timeline component</div>
              </div>
            </div>
            {allComponents.findIndex(
              (ele) => ele._id === "vertical-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex(
                (ele) => ele._id === "horizontal-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find(
                (ele) => ele._id === "horizontal-timeline"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "horizontal-timeline",
                  type: "horizontal-timeline",
                  name: "Horizontal Timeline Component",
                  author: title,
                  id: "horizontal-timeline",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineHorizontalImg} />
              <div className="info">
                <div className="title">Horizontal Vertical Timeline</div>
                <div className="decs">Horizontal Timeline component</div>
              </div>
            </div>
            {allComponents.findIndex(
              (ele) => ele._id === "horizontal-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex(
                (ele) => ele._id === "vertical-timeline-new"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find(
                (ele) => ele._id === "vertical-timeline-new"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "vertical-timeline-new",
                  type: "vertical-timeline-new",
                  name: "Vertical Timeline Component New",
                  author: title,
                  id: "vertical-timeline-new",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineVerticalImg} />
              <div className="info">
                <div className="title">Vertical Timeline New</div>
                <div className="decs">Vertical Timeline component New</div>
              </div>
            </div>
            {allComponents.findIndex(
              (ele) => ele._id === "vertical-timeline-new"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex(
                (ele) => ele._id === "horizontal-timeline-new"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find(
                (ele) => ele._id === "horizontal-timeline-new"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "horizontal-timeline-new",
                  type: "horizontal-timeline-new",
                  name: "Horizontal Timeline Component New",
                  author: title,
                  id: "horizontal-timeline-new",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineHorizontalImg} />
              <div className="info">
                <div className="title">Horizontal Vertical Timeline New</div>
                <div className="decs">Horizontal Timeline component New</div>
              </div>
            </div>
            {allComponents.findIndex(
              (ele) => ele._id === "horizontal-timeline-new"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex((ele) => ele._id === "logs") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find((ele) => ele._id === "logs")
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                // let newItem = {
                //   i: JSON.stringify({
                //     _id: "logs",
                //     type: "logs",
                //     name: "Logs Component",
                //   }),
                //   x: (items.length * 2) % 12,
                //   y: Infinity, // puts it at the bottom
                //   w: 12,
                //   h: 2,
                // };
                let newItem = {
                  _id: "logs",
                  type: "logs",
                  name: "Logs Component",
                  author: title,
                  id: "logs",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={LogsImg} />
              <div className="info">
                <div className="title">Logs</div>
                <div className="decs">Logs component</div>
              </div>
            </div>
            {allComponents.findIndex((ele) => ele._id === "logs") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          {/* <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => ele._id === "knowledgeBase"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => ele._id === "knowledgeBase"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "knowledgeBase",
                    type: "knowledgeBase",
                    name: "knowledgeBase Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 12,
                  h: 2,
                };
                // onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={KnowledgeBaseImg} />
              <div className="info">
                <div className="title">Knowledge Base</div>
                <div className="decs">Knowledge base component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => ele._id === "knowledgeBase"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div> */}

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex((ele) => ele._id === "comment") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find((ele) => ele._id === "comment")
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                // let newItem = {
                //   i: JSON.stringify({
                //     _id: "comment",
                //     type: "comment",
                //     name: "Comment Component",
                //   }),
                //   x: (items.length * 2) % 12,
                //   y: Infinity, // puts it at the bottom
                //   w: 12,
                //   h: 2,
                // };
                let newItem = {
                  _id: "comment",
                  type: "comment",
                  name: "Comment Component",
                  author: title,
                  id: "comment",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }

              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={Comment} />
              <div className="info">
                <div className="title">Comment</div>
                <div className="decs">Comment</div>
              </div>
            </div>
            {allComponents.findIndex((ele) => ele._id === "comment") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex((ele) => ele._id === "detailsComp") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find(
                (ele) => ele._id === "detailsComp"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                // let newItem = {
                //   i: JSON.stringify({
                //     _id: "comment",
                //     type: "comment",
                //     name: "Comment Component",
                //   }),
                //   x: (items.length * 2) % 12,
                //   y: Infinity, // puts it at the bottom
                //   w: 12,
                //   h: 2,
                // };
                let newItem = {
                  _id: "detailsComp",
                  type: "detailsComp",
                  name: "Details",
                  author: title,
                  id: "detailsComp",
                };
                // onAddItem(newItem);
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
                // onAddItem(newItem);
              }

              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={detailIcon} />
              <div className="info">
                <div className="title">Details</div>
                <div className="decs">Details</div>
              </div>
            </div>
            {allComponents.findIndex((ele) => ele._id === "detailsComp") >=
            0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              allComponents.findIndex((ele) => ele._id === "email") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = allComponents.find((ele) => ele._id === "email")
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  _id: "email",
                  type: "email",
                  name: "Email Component",
                  author: title,
                  id: "email",
                };
                let newValue = { ...value };
                newValue[title] = newValue[title].concat(newItem);
                setValue(newValue);
                updateLayout(newValue);
              }
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={OtherImg} />
              <div className="info">
                <div className="title">Email</div>
                <div className="decs">Email</div>
              </div>
            </div>
            {allComponents.findIndex((ele) => ele._id === "email") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          {selectedObject === "obj_crm_campaign_00001" ? (
            <>
              {" "}
              <div
                className={`component-item droppable-element ${
                  allComponents.findIndex(
                    (ele) => ele._id === "general-configuration"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = allComponents.find(
                    (ele) => ele._id === "general-configuration"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      _id: "general-configuration",
                      type: "general-configuration",
                      name: "General Configuration",
                      author: title,
                      id: "general-configuration",
                      // x: (allComponents.length * 2) % 12,
                      // y: Infinity, // puts it at the bottom
                      // w: 12,
                      // h: 2,
                    };
                    let newValue = { ...value };
                    newValue[title] = newValue[title].concat(newItem);
                    setValue(newValue);
                    updateLayout(newValue);
                  }
                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={General} />
                  <div className="info">
                    <div className="title">General configuration</div>
                    <div className="decs">General configuration</div>
                  </div>
                </div>
                {allComponents.findIndex(
                  (ele) => ele._id === "general-configuration"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div
                className={`component-item droppable-element ${
                  allComponents.findIndex(
                    (ele) => ele._id === "template-configuration"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = allComponents.find(
                    (ele) => ele._id === "template-configuration"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      _id: "template-configuration",
                      type: "template-configuration",
                      name: "Template configuration",
                      author: title,
                      id: "template-configuration",
                    };
                    let newValue = { ...value };
                    newValue[title] = newValue[title].concat(newItem);
                    setValue(newValue);
                    updateLayout(newValue);

                    // let newItem = {
                    //   i: JSON.stringify({
                    //     _id: "template-configuration",
                    //     type: "template-configuration",
                    //     name: "Template configuration",
                    //   }),
                    //   x: (allComponents.length * 2) % 12,
                    //   y: Infinity, // puts it at the bottom
                    //   w: 12,
                    //   h: 2,
                    // };
                    // onAddItem(newItem);
                  }
                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={Template} />
                  <div className="info">
                    <div className="title">Template configuration</div>
                    <div className="decs">Template configuration</div>
                  </div>
                </div>
                {allComponents.findIndex(
                  (ele) => ele._id === "template-configuration"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </>
          ) : (
            ""
          )}
        </Wrapper>
      </CustomDrawer>
    </>
  );
};

export default AddComponent;

const CustomDrawer = styled(Drawer)`
  .ant-drawer-body {
    padding-bottom: 0;
  }
  .active {
    border: 1px solid ${(props) => props.theme.main} !important;
  }
`;
const Wrapper = styled.div`
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
`;
const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  /* margin-top: 34px; */
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
  /* margin-bottom: 10px; */
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: #1c9292;
    color: #fff;
  }
`;
