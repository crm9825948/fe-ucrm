import { Modal } from "antd";
import Email from "components/Email/Email2";
import React from "react";

const ModalEmail = (props) => {
  const { isModalVisible, setIsModalVisible, recordID, objectId } = props;
  //   const [isModalVisible, setIsModalVisible] = useState(false);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <>
      <Modal
        title="Email"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"100%"}
        footer={null}
      >
        <div
          style={{
            minHeight: "500px",
            overflow: "auto",
            height: "calc(100vh - 300px)",
          }}
        >
          <Email record_id={recordID} object_id={objectId} />
        </div>
      </Modal>
    </>
  );
};

export default ModalEmail;
