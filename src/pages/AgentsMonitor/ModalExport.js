import { Button, Form, Modal, DatePicker, Radio, Checkbox, Select } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import CsvIcon from "assets/icons/common/csv.svg";
import ExcelIcon from "assets/icons/common/excel.svg";
import { useState } from "react";
import { exportAgentMonitor } from "redux/slices/agentMonitor";
import { Notification } from "components/Notification/Noti";
import moment from "moment";

const { Option } = Select;

const { RangePicker } = DatePicker;
const ModalExport = ({ visible, setVisible }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { listAllUser } = useSelector((state) => state.userReducer);
  const [form] = Form.useForm();
  const [change, setChange] = useState(false);

  const onCancel = () => {
    setVisible(false);
    form.resetFields();
  };
  const handleFinish = (values) => {
    if (!values.all_user && values.user_ids.length === 0) {
      Notification("warning", "Please choose agent!");
    } else {
      dispatch(
        exportAgentMonitor({
          ...values,
          log_range: values.log_range
            ? [
                moment(values.log_range[0])
                  .startOf("day")
                  .format("YYYY-MM-DD HH:mm:ss"),
                moment(values.log_range[1])
                  .endOf("day")
                  .format("YYYY-MM-DD HH:mm:ss"),
              ]
            : undefined,
        })
      );
      onCancel();
    }
  };
  const disabledDate = (current) => {
    if (!form.getFieldValue("log_range")) {
      return false;
    }
    const tooLate =
      form.getFieldValue("log_range")[0] &&
      current.diff(form.getFieldValue("log_range")[0], "days") >= 30;
    const tooEarly =
      form.getFieldValue("log_range")[1] &&
      form.getFieldValue("log_range")[1].diff(current, "days") >= 30;
    return !!tooEarly || !!tooLate;
  };
  const onOpenChange = (open) => {
    if (!open) {
      if (form.getFieldValue("log_range")) {
        if (form.getFieldValue("log_range")?.includes(null)) {
          form.setFieldsValue({ log_range: undefined });
        }
      }
    }
  };
  return (
    <CustomModal
      title={t("systemLog.export")}
      visible={visible}
      //   visible={true}
      onCancel={onCancel}
      width={600}
      footer={[
        <CustomButton onClick={() => form.submit()}>
          {t("systemLog.export")}
        </CustomButton>,
        <CustomButton onClick={() => onCancel()}>
          {t("common.cancel")}
        </CustomButton>,
      ]}
    >
      <Form
        form={form}
        layout="vertical"
        onFinish={handleFinish}
        initialValues={{
          format_type: "csv",
          all_user: false,
          user_ids: [],
          log_range: [moment().subtract(29, "days"), moment()],
        }}
      >
        <WrapCenter>
          <Form.Item
            rules={[{ required: true, message: t("form.required") }]}
            name="log_range"
            label={t("agentMonitor.chooseTime")}
          >
            <RangePicker
              disabledDate={disabledDate}
              onCalendarChange={(val) => {
                form.setFieldsValue({ log_range: val });
              }}
              onOpenChange={onOpenChange}
            />
          </Form.Item>
          <Form.Item name="format_type" label={t("agentMonitor.format")}>
            <Radio.Group>
              <CustomRadio value="csv">
                <img src={CsvIcon} alt="csv" />
                csv
              </CustomRadio>
              <CustomRadio value="xlsx">
                <img src={ExcelIcon} alt="xlsx" />
                xlsx
              </CustomRadio>
            </Radio.Group>
          </Form.Item>
        </WrapCenter>
        <WrapLabel>
          <label>{t("agentMonitor.chooseAgent")}</label>
          <Form.Item name="all_user" valuePropName="checked">
            <Checkbox
              onChange={(e) => {
                if (e.target.checked) {
                  form.setFieldsValue({ user_ids: [] });
                }
                setChange(!change);
              }}
            >
              {t("agentMonitor.selectAll")}
            </Checkbox>
          </Form.Item>
        </WrapLabel>
        <Form.Item name="user_ids">
          <Select
            disabled={form.getFieldValue("all_user")}
            placeholder={t("common.placeholderSelect")}
            showSearch
            mode="multiple"
            optionFilterProp="label"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listAllUser.map((user, index) => (
              <Option key={index} value={user._id}>
                {user.Full_Name}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </CustomModal>
  );
};

export default ModalExport;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-footer {
    display: flex;
    justify-content: flex-end;
    padding: 18px 24px;
    box-shadow: 0px -2px 17px 0px rgba(0, 0, 0, 0.16);
    border: none;
  }
  .ant-picker-focused,
  .ant-picker:hover {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
  .ant-picker-range .ant-picker-active-bar {
    background: ${(props) => props.theme.main};
  }
  label {
    font-size: 16px;
    font-family: var(--roboto-500);
  }
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: #000;
    box-shadow: none;
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-checkbox-wrapper {
    font-family: var(--roboto-400);
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
`;

const CustomButton = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border-color: ${(props) => props.theme.darker};
  }

  &:first-child {
    background: ${(props) => props.theme.main};
    color: #fff;
    border-color: ${(props) => props.theme.main};
    margin-right: 8px;
  }
`;

const CustomRadio = styled(Radio.Button)`
  width: 112px;
  margin: 0px 8px;
  border-radius: 4px !important;
  border: 1px solid #d9d9d9;
  background: #fff;
  overflow: hidden;
  padding: 0px 4px;
  :hover {
    color: #000;
  }
  img {
    height: 20px;
    margin-bottom: 2px;
    margin-right: 8px;
  }
`;

const WrapCenter = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const WrapLabel = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  .ant-form-item {
    margin-bottom: 0;
  }
`;
