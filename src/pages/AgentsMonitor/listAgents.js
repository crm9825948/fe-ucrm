import styled from "styled-components/macro";
import _ from "lodash";

import Divider from "antd/lib/divider";
import Avatar from "antd/lib/avatar";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

import MonitorCall from "assets/icons/monitor/monitorCall.svg";
import MonitorCallNotReady from "assets/icons/monitor/monitorCallNotReady.svg";
import MonitorIC from "assets/icons/monitor/monitorIC.svg";
import AvatarImg from "assets/images/header/avatar.png";

import { BE_URL } from "constants/constants";

function ListAgents({ _onGetUserLog, agentDetails, userStatus }) {
  return (
    <Wrapper>
      <Row gutter={[16, 16]}>
        {_.map(_.get(userStatus, "data"), (agent, idx) => (
          <Col key={_.get(agent, "_id", idx)}>
            <Wrap
              onClick={_onGetUserLog(agent)}
              active={_.isEqual(
                _.get(agentDetails, "_id", ""),
                _.get(agent, "_id", idx)
              )}
            >
              <div
                style={{ display: "flex", alignItems: "center", width: "100%" }}
              >
                <WrapAvatar status={_.get(agent, "user_status.crm_status", "")}>
                  <Avatar
                    src={
                      <img
                        src={
                          _.get(agent, "avatar_config.url", "")
                            ? BE_URL + _.get(agent, "avatar_config.url", "")
                            : AvatarImg
                        }
                        alt=""
                      />
                    }
                    style={{ marginRight: "8px" }}
                    size="large"
                  />
                </WrapAvatar>

                <div style={{ flex: 1 }}>
                  <Name status={_.get(agent, "status", "")}>
                    {_.get(agent, "Full_Name", "")}
                  </Name>

                  {_.isEqual(
                    _.get(agent, "user_status.crm_status", ""),
                    "offline"
                  ) && (
                    <LastOnline>
                      Truy cập lần cuối:{" "}
                      {_.get(agent, "user_status.recent_access", "")}
                    </LastOnline>
                  )}
                </div>
              </div>

              <Divider style={{ marginTop: "16px", marginBottom: "11px" }} />

              <Status3rd>
                {(!_.isEmpty(_.get(agent, "cti_integration", [])) ||
                  !_.isEmpty(_.get(agent, "finesse_integration", []))) && (
                  <StatusCall>
                    <img
                      src={
                        _.isEqual(
                          _.get(agent, "user_status.crm_status", ""),
                          "offline"
                        ) ||
                        _.get(agent, "user_status.call_status", "")?.includes(
                          "Not ready"
                        ) ||
                        _.get(agent, "user_status.call_status", "")?.includes(
                          "Not Ready"
                        )
                          ? MonitorCallNotReady
                          : MonitorCall
                      }
                      alt="MonitorCall"
                    />

                    {_.isEqual(
                      _.get(agent, "user_status.crm_status", ""),
                      "offline"
                    ) ? (
                      <>
                        Ext:&nbsp;
                        {_.get(
                          agent,
                          "finesse_integration[0].extension",
                          _.get(agent, "cti_integration[0].extension", "")
                        )}
                      </>
                    ) : (
                      <>{_.get(agent, "user_status.call_status", "")}</>
                    )}
                  </StatusCall>
                )}

                {(_.get(agent, "ic_integration[0].authen_type", "") ===
                  "basic" ||
                  _.get(agent, "ic_integration[0].authen_type", "") ===
                    "sso") && (
                  <StatusIC>
                    <img src={MonitorIC} alt="MonitorIC" />

                    {_.get(agent, "user_status.ic_status", "")}
                  </StatusIC>
                )}
              </Status3rd>
            </Wrap>
          </Col>
        ))}
      </Row>
    </Wrapper>
  );
}

export default ListAgents;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

const WrapAvatar = styled.div`
  position: relative;

  &::after {
    position: absolute;
    background: ${({ status }) =>
      status === "online"
        ? "#0bb865"
        : status === "busy"
        ? "#D48806"
        : "#8C8C8C"};
    content: "";
    width: 13px;
    height: 13px;
    border-radius: 50%;
    right: 8px;
    bottom: 0;
    border: 1px solid #fff;
  }
`;

const Wrap = styled.div`
  padding: 16px 24px 8px 16px;
  width: 392px;
  height: 115px;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 10px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  cursor: pointer;
  background: ${({ active }) => (active ? "rgba(32, 162, 162, 0.05)" : "#fff")};
  border: ${(props) =>
    props.active ? `1px solid ${props.theme.main}` : "none"};

  :hover {
    background: rgba(32, 162, 162, 0.05);
    box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  }
`;

const Name = styled.div`
  color: #2c2c2c;
  font-family: var(--roboto-500);
  font-size: 16px;
  line-height: 22px;
`;

const LastOnline = styled.div`
  font-size: 12px;
  line-height: 16px;
  color: #6b6b6b;
`;

const Status3rd = styled.div`
  display: flex;
`;

const StatusCall = styled.div`
  display: flex;
  align-items: center;
  font-family: var(--roboto-500);
  font-size: 14px;
  flex: 1;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  img {
    margin-right: 8px;
  }
`;

const StatusIC = styled(StatusCall)``;
