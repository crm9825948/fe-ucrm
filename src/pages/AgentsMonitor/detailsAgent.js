import styled from "styled-components/macro";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import moment from "moment";

import Divider from "antd/lib/divider";
import Avatar from "antd/lib/avatar";
import Tooltip from "antd/lib/tooltip";
import Spin from "antd/lib/spin";

import MonitorCall from "assets/icons/monitor/monitorCall.svg";
import MonitorCallNotReady from "assets/icons/monitor/monitorCallNotReady.svg";
import MonitorIC from "assets/icons/monitor/monitorIC.svg";
import AvatarImg from "assets/images/header/avatar.png";

import { getUserStatusLog, setCurrentPageLog } from "redux/slices/agentMonitor";
import { BE_URL } from "constants/constants";

function DetailsAgent({ agentDetails }) {
  const dispatch = useDispatch();

  const { userStatusLog, hasMoreLog, currentPageLog } = useSelector(
    (state) => state.agentMonitorReducer
  );

  const loadMoreLog = () => {
    let tempPage = currentPageLog + 1;

    if (userStatusLog.length > 0) {
      dispatch(setCurrentPageLog(tempPage));

      dispatch(
        getUserStatusLog({
          payload: {
            search_data: {},
            limit: 20,
            page: tempPage,
          },
          user_id: _.get(userStatusLog[0], "user_id", ""),
        })
      );
    }
  };

  return (
    <Wrapper show={!_.isEmpty(agentDetails)}>
      <Wrap show={!_.isEmpty(agentDetails)}>
        <div style={{ display: "flex", alignItems: "center", width: "100%" }}>
          <Avatar
            src={
              _.get(agentDetails, "avatar_config.url", "")
                ? BE_URL + _.get(agentDetails, "avatar_config.url", "")
                : AvatarImg
            }
            style={{ marginRight: "8px" }}
            size="large"
          />

          <div style={{ flex: 1 }}>
            <Name>{_.get(agentDetails, "Full_Name", "")}</Name>

            <div style={{ display: "flex" }}>
              {(!_.isEmpty(_.get(agentDetails, "cti_integration", [])) ||
                !_.isEmpty(_.get(agentDetails, "finesse_integration", []))) && (
                <Extension>
                  Extension:{" "}
                  {_.get(
                    agentDetails,
                    "finesse_integration[0].extension",
                    _.get(agentDetails, "cti_integration[0].extension", "")
                  )}
                  &nbsp; &nbsp;
                </Extension>
              )}

              {/* {!_.isEmpty(_.get(agentDetails, "ic_integration", [])) && (
                <Extension>
                  &nbsp; &nbsp; IC: {_.get(dumbData, "ic_integration[0]", "")}
                  &nbsp; &nbsp; IC
                </Extension>
              )} */}
            </div>
          </div>

          <Status status={_.get(agentDetails, "user_status.crm_status", "")}>
            {_.get(agentDetails, "user_status.crm_status", "")}
            {/* <span style={{ color: "#6b6b6b", fontSize: "16px" }}> | 20:45</span> */}
          </Status>
        </div>

        <Divider style={{ marginTop: "16px", marginBottom: "11px" }} />

        <Status3rd>
          {(!_.isEmpty(_.get(agentDetails, "cti_integration", [])) ||
            !_.isEmpty(_.get(agentDetails, "finesse_integration", []))) && (
            <StatusCall>
              <img
                src={
                  _.isEqual(
                    _.get(agentDetails, "user_status.crm_status", ""),
                    "offline"
                  ) ||
                  _.get(agentDetails, "user_status.call_status", "")?.includes(
                    "Not ready"
                  ) ||
                  _.get(agentDetails, "user_status.call_status", "")?.includes(
                    "Not Ready"
                  )
                    ? MonitorCallNotReady
                    : MonitorCall
                }
                alt="MonitorCall"
              />

              {_.get(agentDetails, "user_status.call_status", "")}
            </StatusCall>
          )}

          {(_.get(agentDetails, "ic_integration[0].authen_type", "") ===
            "basic" ||
            _.get(agentDetails, "ic_integration[0].authen_type", "") ===
              "sso") && (
            <StatusIC>
              <img src={MonitorIC} alt="MonitorIC" />

              {_.get(agentDetails, "user_status.ic_status", "")}
            </StatusIC>
          )}
        </Status3rd>
      </Wrap>

      <WrapHistory>
        <Header>
          <Title>Status</Title>
          <Title>Time</Title>
          <Title>Duration</Title>
        </Header>

        <Content id="statusLog">
          <InfiniteScroll
            dataLength={userStatusLog.length}
            scrollableTarget="statusLog"
            loader={
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  marginTop: "8px",
                }}
              >
                <Spin />
              </div>
            }
            hasMore={hasMoreLog}
            next={loadMoreLog}
          >
            {userStatusLog.map((item, idx) => {
              return (
                <History key={_.get(item, "_id", idx)}>
                  <div
                    style={{
                      flex: 1,
                      display: "flex",
                      alignItems: "center",
                      marginLeft: "16px",
                    }}
                  >
                    <ColorStatus status={_.get(item, "status_name", "")}>
                      <Tooltip title={_.get(item, "status_name", "")}>
                        {_.get(item, "status_name", "")}
                      </Tooltip>

                      <span style={{ textTransform: "uppercase" }}>
                        &nbsp; - {_.get(item, "status_type", "")}
                      </span>
                    </ColorStatus>
                  </div>

                  <Time>
                    {moment(_.get(item, "created_date", "")).isSame(
                      moment(),
                      "day"
                    ) ? (
                      <>
                        {moment(_.get(item, "created_date", "")).format(
                          "HH:mm:ss"
                        )}
                      </>
                    ) : (
                      <>
                        {moment(_.get(item, "created_date", "")).format(
                          "HH:mm:ss, DD-MM-YYYY"
                        )}
                      </>
                    )}
                  </Time>

                  <Duration>{_.get(item, "duration", "")}</Duration>
                </History>
              );
            })}
          </InfiniteScroll>
        </Content>
      </WrapHistory>
    </Wrapper>
  );
}

export default DetailsAgent;

const Wrapper = styled.div`
  background: #fff;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 10px;
  transition: all 0.4s ease-in-out;
  visibility: ${({ show }) => (show ? "visible" : "hidden")};
  opacity: ${({ show }) => (show ? "1" : "0")};
  width: ${({ show }) => (show ? "570px" : "0")};
  min-width: ${({ show }) => (show ? "570px" : "0")};
  height: ${({ show }) => (show ? "calc(100vh - 220px)" : "0")};
  padding: ${({ show }) => (show ? "16px" : "0")};
  margin-left: ${({ show }) => (show ? "24px" : "0")};

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: ${({ show }) => (show ? "fit-content" : "0")};
`;

const Name = styled.div`
  color: #2c2c2c;
  font-family: var(--roboto-500);
  font-size: 16px;
  line-height: 22px;
`;

const Extension = styled.div`
  font-size: 12px;
  line-height: 16px;
  color: #6b6b6b;
`;

const Status3rd = styled.div`
  display: flex;
  margin-bottom: 20px;
`;

const StatusCall = styled.div`
  display: flex;
  align-items: center;
  font-family: var(--roboto-500);
  font-size: 14px;
  margin-right: 24px;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  span {
    font-family: var(--roboto-400);
  }

  img {
    margin-right: 8px;
  }
`;

const StatusIC = styled(StatusCall)`
  margin-right: 0;
`;

const Status = styled.div`
  position: relative;
  font-size: 16px;
  letter-spacing: 0.01em;
  line-height: 130%;
  color: #2c2c2c;
  text-transform: capitalize;

  &::after {
    position: absolute;
    background: ${({ status }) =>
      status === "online"
        ? "#0BB865"
        : status === "busy"
        ? "#D48806"
        : "#8C8C8C"};
    content: "";
    width: 10px;
    height: 10px;
    border-radius: 50%;
    left: -15px;
    top: 5px;
  }
`;

const ColorStatus = styled.div`
  padding: 2px 8px;
  width: fit-content;
  height: 25px;

  background: ${({ status }) =>
    status === "online" || status === "Ready" || status === "available"
      ? "rgba(11, 184, 101, 0.1)"
      : status?.includes("Not ready") ||
        status?.includes("Not Ready") ||
        status?.includes("not_ready")
      ? "rgba(207, 19, 34, 0.1)"
      : status === "busy"
      ? "rgba(212, 136, 6, 0.1)"
      : "#D9D9D9"};
  color: ${({ status }) =>
    status === "online" || status === "Ready" || status === "available"
      ? "#0bb865"
      : status?.includes("Not ready") ||
        status?.includes("Not Ready") ||
        status?.includes("not_ready")
      ? "#CF1322"
      : status === "busy"
      ? "#D48806"
      : "#2C2C2C"};

  border-radius: 2px;
  text-align: center;
  font-size: 16px;
  line-height: 130%;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  text-transform: capitalize;
`;

const WrapHistory = styled.div``;

const Header = styled.div`
  display: flex;
`;

const Content = styled.div`
  height: calc(100vh - 380px);
  overflow: auto;

  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    background: #d9d9d9;
  }

  ::-webkit-scrollbar-thumb {
    background: #bfbfbf;
  }
`;

const Title = styled.div`
  background: #fafafa;
  box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
  flex: 1;
  font-weight: 500;
  font-size: 16px;
  color: #2c2c2c;
  height: 40px;
  display: flex;
  align-items: center;
  border-right: 1px solid #d9d9d9;
  /* margin-left: 16px; */
  padding-left: 16px;

  &:first-child {
    margin-left: 0;
  }

  &:last-child {
    border-right: none;
  }
`;

const History = styled.div`
  display: flex;
  height: 38px;
  border-bottom: 1px solid #ededed;
`;

const Time = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  margin-left: 16px;
`;

const Duration = styled(Time)``;
