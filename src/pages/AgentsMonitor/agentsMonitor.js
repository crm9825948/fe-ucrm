import { useState, useEffect, useRef } from "react";
import styled from "styled-components/macro";
import { useTranslation, withTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
// import useWebSocket from "react-use-websocket";

import Breadcrumb from "antd/lib/breadcrumb";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Pagination from "antd/lib/pagination";
import { DownloadOutlined } from "@ant-design/icons";

import {
  getUserStatusLog,
  getUserStatusLogResult,
  // getUserStatusResult,
  setHasMore,
  setCurrentPageLog,
  getUserStatus,
} from "redux/slices/agentMonitor";
import { loadAllGroups } from "redux/slices/group";

import ListAgents from "./listAgents";
import DetailsAgent from "./detailsAgent";

import { filterStatus } from "util/staticData";
import { loadAllUser } from "redux/slices/user";
import { Button } from "antd";
import ModalExport from "./ModalExport";
// import { WS_URL } from "constants/constants";

function AgentsMonitor() {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);

  const { userStatus } = useSelector((state) => state.agentMonitorReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const refSearch = useRef(null);

  // const [dataWebSocket, $dataWebSocket] = useState({});
  const [searchData, $searchData] = useState({
    Full_Name: "",
    group_id: "",
    crm_status: "",
  });
  const [agentDetails, $agentDetails] = useState({});
  const [listGroups, $listGroups] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(20);

  // const endpoint =
  //   WS_URL +
  //   "agent-monitor/" +
  //   localStorage.getItem("setting_accessToken") +
  //   "/";

  // // eslint-disable-next-line no-unused-vars
  // const { sendJsonMessage } = useWebSocket(endpoint, {
  //   onMessage: (message) => handleMessage(message),
  // });

  // const handleMessage = (message) => {
  //   const data = JSON.parse(message.data);
  //   // $dataWebSocket(data);

  //   if (_.isEqual(_.get(data, "event", ""), "user_change_status")) {
  //     const userIndex = _.get(userStatus, "data", []).findIndex(
  //       (item) =>
  //         _.get(item, "user_status.user_id") === _.get(data, "data.user_id")
  //     );

  //     if (userIndex !== -1) {
  //       let newUserStatus = [...userStatus.data];
  //       newUserStatus[userIndex] = {
  //         ...newUserStatus[userIndex],
  //         user_status: _.get(data, "data"),
  //       };

  //       dispatch(
  //         getUserStatusResult({
  //           ...userStatus,
  //           data: newUserStatus,
  //         })
  //       );
  //     }
  //   }
  // };

  const showTotal = () => {
    return `${t("common.total")} ${_.get(
      userStatus,
      "pagination.total",
      0
    )} ${t("common.items")}`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  const _onGetUserLog = (user) => {
    return () => {
      if (_.get(agentDetails, "_id", "") === _.get(user, "_id", "")) {
        $agentDetails({});
        dispatch(
          getUserStatusLogResult({
            type: "remove",
            data: [],
          })
        );
      } else {
        $agentDetails(user);
        dispatch(setHasMore(true));
        dispatch(setCurrentPageLog(0));

        dispatch(
          getUserStatusLogResult({
            type: "remove",
            data: [],
          })
        );

        dispatch(
          getUserStatusLog({
            payload: {
              search_data: {},
              limit: 20,
              page: 0,
            },
            user_id: _.get(user, "_id", ""),
          })
        );
      }
    };
  };

  const _onSearch = (type) => {
    return (e) => {
      $searchData((prev) => {
        let newData = { ...prev };

        switch (type) {
          case "Full_Name":
            newData[type] = _.get(refSearch, "current.input.value", "");
            break;

          case "group_id":
          case "crm_status":
            newData[type] = e;

            break;

          default:
            break;
        }
        return newData;
      });
    };
  };

  useEffect(() => {
    dispatch(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    let tempGroup = [];

    listAllGroups.forEach((item) => {
      tempGroup.push({
        label: item.name,
        value: item._id,
      });
    });
    $listGroups(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    $agentDetails({});

    dispatch(
      getUserStatusLogResult({
        type: "remove",
        data: [],
      })
    );

    dispatch(
      getUserStatus({
        limit: recordPerPage,
        search_data: searchData,
        page: currentPage - 1,
      })
    );
  }, [$agentDetails, currentPage, dispatch, recordPerPage, searchData]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.agentsMonitor")}</BreadcrumbItem>
      </Breadcrumb>

      <WrapHeader>
        <Filter>
          <Input
            style={{ width: "241px" }}
            placeholder="Name"
            onPressEnter={_onSearch("Full_Name")}
            allowClear
            ref={refSearch}
          />
          <Select
            style={{ width: "234px" }}
            placeholder="Group"
            options={listGroups}
            onChange={_onSearch("group_id")}
            allowClear
          />
          <Select
            style={{ width: "234px" }}
            placeholder="Status"
            options={filterStatus}
            onChange={_onSearch("crm_status")}
            allowClear
          />

          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={_.get(userStatus, "pagination.total")}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </Filter>
        <ButtonExport onClick={() => setVisible(true)}>
          <DownloadOutlined /> {t("systemLog.export")}
        </ButtonExport>
      </WrapHeader>

      <Wrap>
        <ListAgents
          _onGetUserLog={_onGetUserLog}
          agentDetails={agentDetails}
          userStatus={userStatus}
        />
        <DetailsAgent agentDetails={agentDetails} />
      </Wrap>
      <ModalExport visible={visible} setVisible={setVisible} />
    </Wrapper>
  );
}

export default withTranslation()(AgentsMonitor);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-pagination {
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Filter = styled.div`
  margin-top: 16px;
  margin-bottom: 16px;
  display: flex;
  flex-wrap: wrap;
  column-gap: 16px;
  row-gap: 16px;
`;

const Wrap = styled.div`
  display: flex;
  justify-content: space-between;
`;

const WrapHeader = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  align-items: center;
`;
const ButtonExport = styled(Button)`
  border-radius: 2px;
  border: 1px solid ${(props) => props.theme.main};
  background: ${(props) => props.theme.main};
  color: #fff;
  box-shadow: 0px 2px 0px 0px rgba(0, 0, 0, 0.04);
  :hover,
  :focus {
    border: 1px solid ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;
