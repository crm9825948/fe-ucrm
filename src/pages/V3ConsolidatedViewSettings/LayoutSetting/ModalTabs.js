import React from "react";
import styled from "styled-components/macro";
import Modal from "antd/lib/modal";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Typography from "antd/lib/typography";
import Select from "antd/lib/select";
import General from "assets/images/consolidatedView/General configuration.png";
import Template from "assets/images/consolidatedView/template configuration.png";
import SMS from "assets/images/consolidatedView/SMS component.svg";
import { componentsLayoutSettings, iconTabComp } from "util/staticData";
import { useSelector } from "redux/store";
import { useDispatch } from "react-redux";
import Form from "antd/lib/form";
import { CheckCircleFilled } from "@ant-design/icons";
import { Notification } from "components/Notification/Noti";
import { loadConfigComponents } from "redux/slices/consolidatedViewSettings";
import { useTranslation } from "react-i18next";
import _ from "lodash";
const { Text } = Typography;

const ModalTabs = ({
  selectedTabComp,
  $selectedTabComp,
  dataTabComp,
  indexTab,
  layoutsCenter,
  activeTabComp,
  selectedObject,
  layoutsLeft,
  layoutsRight,
  form,
  visible,
  $visible,
  $showComponents,
  $indexTab,
  componentsCreated,
  $dataTabComp,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { listSMSOutgoing } = useSelector((state) => state.smsOutgoingReducer);

  const renderComponent = (icon, title, des, objectName, id) => (
    <Component
      active={title === selectedTabComp || id === selectedTabComp}
      onClick={() => $selectedTabComp(id || title)}
      color={hexToRGB(defaultBrandName.theme_color)}
    >
      <div style={{ display: "flex" }}>
        <img src={icon} alt="" />
        <div>
          <p>{title}</p>
          <Text
            style={{ textTransform: "capitalize" }}
            ellipsis={{ tooltip: des }}
          >
            {des.toLowerCase().replaceAll("_", " ")}

            {objectName && <> &#8226; Related object: {objectName}</>}
          </Text>
        </div>
      </div>
      <CheckCircleFilled />
    </Component>
  );
  const handleOk = (values) => {
    if (
      dataTabComp.tab_name[indexTab] !== values.tabName.trim() &&
      dataTabComp.tab_name.includes(values.tabName.trim())
    ) {
      Notification("warning", "Tab name already exist");
    } else {
      let temTabs = [...layoutsCenter["tabs"]];
      let indexComp = temTabs.findIndex((item) => dataTabComp._id === item._id);
      if (indexTab === "") {
        let tem_tab_comp = {
          ...dataTabComp.tab_component,
          [values.tabName.trim()]: [
            {
              h: 12,
              w: 12,
              x: 0,
              y: 0,
              i: selectedTabComp,
              icon: values.icon,
            },
          ],
        };
        let tem_tab_name = [...dataTabComp.tab_name, values.tabName];
        let tabUpdate = {
          ...dataTabComp,
          tab_component: tem_tab_comp,
          tab_name: tem_tab_name,
        };
        temTabs[indexComp] = tabUpdate;
      } else {
        let tem_tab_comp = {};
        Object.entries(dataTabComp.tab_component).forEach(([key, value]) => {
          if (key === activeTabComp) {
            tem_tab_comp[values.tabName.trim()] = [
              {
                h: 12,
                w: 12,
                x: 0,
                y: 0,
                i: selectedTabComp,
                icon: values.icon,
              },
            ];
          } else {
            tem_tab_comp[key] = value;
          }
        });
        let tem_tab_name = [...dataTabComp.tab_name];
        tem_tab_name[indexTab] = values.tabName;
        let tabUpdate = {
          ...dataTabComp,
          tab_component: tem_tab_comp,
          tab_name: tem_tab_name,
        };

        temTabs[indexComp] = tabUpdate;
      }
      dispatch(
        loadConfigComponents({
          object_id: selectedObject,
          is_update: true,
          data: {
            layouts_center: {
              ...layoutsCenter,
              tabs: temTabs,
            },
            layouts_left: layoutsLeft,
            layouts_right: layoutsRight,
          },
        })
      );
      handleCancel();
    }
  };

  const handleRemove = () => {
    let temTabs = [...layoutsCenter["tabs"]];
    let indexComp = temTabs.findIndex((item) => dataTabComp._id === item._id);
    let tem_tab_comp = { ...dataTabComp.tab_component };
    let tem_tab_name = [...dataTabComp.tab_name];
    delete tem_tab_comp[tem_tab_name[indexTab]];
    tem_tab_name = tem_tab_name.filter((item, index) => index !== indexTab);
    let tabUpdate = {
      ...dataTabComp,
      tab_component: tem_tab_comp,
      tab_name: tem_tab_name,
    };
    temTabs[indexComp] = tabUpdate;

    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: true,
        data: {
          layouts_center: {
            ...layoutsCenter,
            tabs: temTabs,
          },
          layouts_left: layoutsLeft,
          layouts_right: layoutsRight,
        },
      })
    );
    handleCancel();
  };

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);
      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  const handleCancel = () => {
    $visible(false);
    $showComponents(true);
    $indexTab("");
    $selectedTabComp("");
    $dataTabComp({});
    form.resetFields();
  };

  return (
    <ModalCustom
      visible={visible}
      // visible={true}
      title="New tab"
      onCancel={handleCancel}
      width={651}
      onOk={handleOk}
      footer={[
        <Button key="submit" type="primary" onClick={() => form.submit()}>
          {t("common.save")}
        </Button>,
        <Button onClick={handleCancel}>{t("common.cancel")}</Button>,
        <Button
          style={{ display: indexTab !== "" ? "unset" : "none" }}
          onClick={handleRemove}
        >
          {t("common.remove")}
        </Button>,
      ]}
    >
      <Form
        form={form}
        onFinish={handleOk}
        initialValues={{
          icon: "tab-comp-1",
        }}
      >
        <WrapFormItem>
          <span>{t("layoutSetting.tabName")}</span>
          <WrapItem>
            <FormItem name="icon">
              <Select>
                {_.map(iconTabComp, (icon) => (
                  <Select.Option value={_.get(icon, "key")}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      {_.get(icon, "icon")}
                    </div>
                  </Select.Option>
                ))}
              </Select>
            </FormItem>
            <FormItem
              style={{ flex: 1 }}
              name="tabName"
              rules={[
                {
                  required: true,
                  message: `${t("common.placeholderInput")}`,
                },
              ]}
            >
              <Input placeholder={t("common.placeholderInput")} />
            </FormItem>
          </WrapItem>
        </WrapFormItem>
        <ComponentWrap>
          {_.map(componentsLayoutSettings, (item, idx) => (
            <div key={idx}>
              {renderComponent(item.icon, item.title, item.des)}
            </div>
          ))}

          {_.size(componentsCreated) > 0 &&
            _.map(
              componentsCreated,
              (item, idx) =>
                item.type !== "tab" && (
                  <div key={idx}>
                    {renderComponent(
                      _.get(item, "icon"),
                      _.get(item, "title"),
                      _.get(item, "type") || _.get(item, "component_type"),
                      _.get(item, "related_object_name") ||
                        _.get(item, "object_name") ||
                        "",
                      item._id
                    )}
                  </div>
                )
            )}

          {selectedObject === "obj_crm_campaign_00001" && (
            <>
              {renderComponent(
                General,
                "General Configuration",
                "General configuration component"
              )}
              {renderComponent(
                Template,
                "Template Configuration",
                "Template configuration component"
              )}
            </>
          )}

          {listSMSOutgoing.find(
            (item) => item.object_id === selectedObject
          ) && <>{renderComponent(SMS, "SMS component", "SMS component")}</>}
        </ComponentWrap>
      </Form>
    </ModalCustom>
  );
};

export default ModalTabs;
const ModalCustom = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-label {
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }

  .ant-modal-body {
    padding: 16px 0;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
`;
const WrapFormItem = styled.div`
  padding: 0 12px;
  span {
    font-family: var(--roboto-500);
  }
`;
const WrapItem = styled.div`
  display: flex;
  margin-top: 6px;
`;
const FormItem = styled(Form.Item)`
  margin-bottom: 16px !important;
  .ant-select-selection-item {
    display: flex;
  }
`;

const ComponentWrap = styled.div`
  max-height: 500px;
  overflow: auto;
`;
const Component = styled.div`
  border-bottom: 1px solid rgb(236, 236, 236);
  padding: 12px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.5s;
  background: ${(props) => (props.active ? props.color : "#fff")};

  :hover {
    background: ${(props) => (props.active ? props.color : "#f1f3f6")};
  }
  img {
    width: 40px;
    margin-right: 8px;
  }

  p {
    margin-bottom: 0;
    color: #2c2c2c;
    font-family: var(--roboto-500);
  }

  span {
    color: #6b6b6b;
  }
  svg {
    font-size: 24px;
    color: ${(props) => props.theme.main};
    display: ${(props) => (props.active ? "block" : "none")};
  }
`;
