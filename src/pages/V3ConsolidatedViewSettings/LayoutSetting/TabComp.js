import { useState, useEffect } from "react";
import _ from "lodash";
import styled from "styled-components/macro";
import { useTranslation } from "react-i18next";
import Icon from "components/Icon/Icon";
import { Typography } from "antd";
const { Text: TextComponent } = Typography;

function TabComp({
  dataComp,
  components,
  componentsCreated,
  $showComponents,
  onRemoveComp,
  layoutsCenter,
  layoutsLeft,
  layoutsRight,
  $layoutsCenter,
  onDragStart,
  onResizeStart,
  onResizeStop,
  onDragStop,
  onDrop,
  tabRef,
  selectedObject,
  visible,
  $visible,
  $indexTab,
  $selectedTabComp,
  form,
  $dataTabComp,
  $activeTabComp,
}) {
  const { t } = useTranslation();
  const [componentHeight, $componentHeight] = useState(0);
  const [activeTab, $activeTab] = useState("");
  useEffect(() => {}, [activeTab]);
  useEffect(() => {
    if (dataComp && Object.entries(dataComp).length > 0) {
      $activeTab(Object.keys(dataComp.tab_component)[0]);
    }
  }, [dataComp]);

  useEffect(() => {
    if (document.getElementById("Tabs")) {
      const element = document.getElementById("Tabs");

      const resizeObserver = new ResizeObserver((entries) => {
        $componentHeight(element.offsetHeight);
      });

      resizeObserver.observe(element);

      return () => {
        resizeObserver.disconnect();
      };
    }
  }, []);

  return (
    <Wrapper>
      <div
        style={{
          borderRadius: "8px 8px 0px 0px",
          background: "#CFE9F4",
          height: "36px",
          padding: "6px 12px",
          fontFamily: "var(--roboto-500)",
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        <TabWrap>
          {Object.entries(dataComp.tab_component).map(
            ([key, value = [{}]], idx) => (
              <TabTitle
                isActive={activeTab === key}
                key={idx}
                onClick={() => {
                  $showComponents(true);
                  $activeTab(key);
                }}
                onDoubleClick={() => {
                  $visible(true);
                  $indexTab(
                    dataComp.tab_name.findIndex((item) => item === key)
                  );
                  $dataTabComp(dataComp);
                  $activeTabComp(key);
                  form.setFieldsValue({
                    tabName: key,
                  });
                  if (value[0]) {
                    $selectedTabComp(value[0].i);
                    form.setFieldsValue({
                      icon: value[0].icon ? value[0].icon : "tab-comp-1",
                    });
                  }
                }}
              >
                <Icon
                  icon={
                    value[0] && value[0].icon ? value[0].icon : "tab-comp-1"
                  }
                  size={18}
                />
                <TextComponent ellipsis={{ tooltip: key }}>{key}</TextComponent>
              </TabTitle>
            )
          )}

          <Icon
            icon="plus"
            size={20}
            color="#637381"
            style={{ cursor: "pointer", minWidth: "16px" }}
            onClick={() => {
              $dataTabComp(dataComp);
              $visible(true);
            }}
          />
        </TabWrap>

        <Icon
          icon="close"
          size={24}
          onClick={onRemoveComp()}
          style={{ cursor: "pointer" }}
        />
      </div>

      <Wrap
        componentHeight={componentHeight}
        isEmpty={_.isEmpty(dataComp.tab_component[activeTab])}
      >
        {dataComp.tab_component[activeTab] &&
        dataComp.tab_component[activeTab][0] &&
        dataComp.tab_component[activeTab][0].i ? (
          <NoData>
            <img
              src={_.get(
                components
                  .concat(componentsCreated)
                  .find(
                    (ele) =>
                      ele.title === dataComp.tab_component[activeTab][0].i ||
                      ele._id === dataComp.tab_component[activeTab][0].i
                  ),
                "icon"
              )}
              alt=""
              style={{ width: "40px", marginBottom: "8px" }}
            />
            <p
              style={{
                fontSize: "14px",
                marginBottom: 0,
                fontFamily: "var(--roboto-500)",
              }}
            >
              {_.get(
                components
                  .concat(componentsCreated)
                  .find(
                    (ele) =>
                      ele.title === dataComp.tab_component[activeTab][0].i ||
                      ele._id === dataComp.tab_component[activeTab][0].i
                  ),
                "title"
              )}
            </p>
          </NoData>
        ) : (
          <NoData>
            <Icon icon="drag-component" size={49} color="#7DBEFA" />
            <p>{t("layoutSetting.addTab")}</p>
          </NoData>
        )}
      </Wrap>
    </Wrapper>
  );
}

export default TabComp;

const Wrapper = styled.div`
  height: 90%;
`;

const TabTitle = styled.div`
  border-right: 1px solid #ccd4dd;
  margin-right: 11px;
  padding-right: 11px;
  display: flex;
  justify-content: center;
  /* align-items: center;
  padding-bottom: 2px; */
  cursor: pointer;
  color: ${({ isActive }) => (isActive ? "#000" : "#6b6b6b")};
  font-family: ${({ isActive }) =>
    isActive ? "var(--roboto-500)" : "var(--roboto-400)"};
  box-shadow: ${({ isActive }) =>
    isActive && "0px 8px 16px 0px rgba(0, 0, 0, 0.05)"};
  position: relative;
  min-width: 42px;
  &::before {
    position: absolute;
    content: "";
    bottom: 0;
    left: 0;
    width: calc(100% - 11px);
    height: 2px;
    background: ${({ isActive }) => (isActive ? "#000" : "#CFE9F4")};
  }
  svg {
    margin-right: 4px;
    min-width: 16px;
  }
`;

const Wrap = styled.div`
  height: 100%;
`;
const TabWrap = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
  overflow: auto;
  &::-webkit-scrollbar {
    height: 4px !important;
  }
`;

const NoData = styled.div`
  display: flex;
  justify-content: center;
  height: 100%;
  align-items: center;
  flex-direction: column;
`;
