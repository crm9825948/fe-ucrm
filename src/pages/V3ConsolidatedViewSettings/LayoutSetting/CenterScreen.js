import { Responsive, WidthProvider } from "react-grid-layout";
import React, { useState } from "react";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import styled from "styled-components/macro";
import Icon from "components/Icon/Icon";

import { components } from "util/staticData";
import TabComp from "./TabComp";
import { useDispatch, useSelector } from "react-redux";
import { loadConfigComponents } from "redux/slices/consolidatedViewSettings";
import Form from "antd/lib/form";
import ModalTabs from "./ModalTabs";

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const layoutEmpty = {
  center: [{ x: 0, y: 0, w: 2, h: 2, i: "emptyCenter" }],
};

function CenterScreen({
  selectedObject,
  selectedComp,
  $selectedComp,
  $showComponents,
  componentsCreated,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { layoutsCenter, layoutsLeft, layoutsRight } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const [visible, $visible] = useState(false);
  const [selectedTabComp, $selectedTabComp] = useState("");
  const [indexTab, $indexTab] = useState("");
  const [dataTabComp, $dataTabComp] = useState({});
  const [activeTabComp, $activeTabComp] = useState("");
  const [form] = Form.useForm();

  const _onRemoveComp = (type, idx, id) => {
    return () => {
      const ele = document.getElementById(id);
      ele.className = "card";

      setTimeout(() => {
        const layoutType = [...layoutsCenter[type]];
        layoutType.splice(idx, 1);
        let temTabs = layoutsCenter["tabs"].filter((item) => item._id !== id);
        dispatch(
          loadConfigComponents({
            object_id: selectedObject,
            is_update: true,
            data: {
              layouts_center: {
                ...layoutsCenter,
                [type]: [...layoutType],
                tabs: temTabs,
              },
              layouts_left: layoutsLeft,
              layouts_right: layoutsRight,
            },
          })
        );
        $showComponents(true);
      }, 300);
    };
  };

  const generateDOMCenter = (type) => {
    let resultLayouts = [];

    if (type === "center") {
      resultLayouts = [...layoutsCenter.center];
    } else if (type === "left") {
      resultLayouts = [...layoutsCenter.left];
    } else if (type === "right") {
      resultLayouts = [...layoutsCenter.right];
    } else if (type === "top") {
      resultLayouts = [...layoutsCenter.top];
    }

    if (_.size(resultLayouts) > 0) {
      return _.map(resultLayouts, function (item, idx) {
        return (
          <div
            id={item.i}
            key={item.i}
            data-grid={item}
            style={{ background: "#F5FCFF" }}
          >
            {_.get(
              components
                .concat(componentsCreated)
                .find((ele) => ele._id === item.i),
              "type"
            ) === "tab" ? (
              <>
                <TabComp
                  dataComp={layoutsCenter["tabs"].find(
                    (ele) => ele._id === item.i
                  )}
                  selectedObject={selectedObject}
                  components={components}
                  componentsCreated={componentsCreated}
                  $showComponents={$showComponents}
                  onRemoveComp={() => _onRemoveComp(type, idx, item.i)}
                  layoutsCenter={layoutsCenter}
                  layoutsLeft={layoutsLeft}
                  layoutsRight={layoutsRight}
                  onDragStart={onDragStart}
                  onResizeStart={onResizeStart}
                  onResizeStop={onResizeStop}
                  onDragStop={onDragStop}
                  onDrop={onDrop}
                  visible={visible}
                  $visible={$visible}
                  $selectedTabComp={$selectedTabComp}
                  $indexTab={$indexTab}
                  form={form}
                  $dataTabComp={$dataTabComp}
                  $activeTabComp={$activeTabComp}
                />
              </>
            ) : (
              <>
                <div
                  style={{
                    borderRadius: "8px 8px 0px 0px",
                    background: "#CFE9F4",
                    height: "36px",
                    padding: "6px 12px",
                    fontFamily: "var(--roboto-500)",
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  {_.get(
                    components
                      .concat(componentsCreated)
                      .find(
                        (ele) => ele.title === item.i || ele._id === item.i
                      ),
                    "title"
                  )}

                  <Icon
                    icon="close"
                    size={24}
                    onClick={_onRemoveComp(type, idx, item.i)}
                    style={{ cursor: "pointer" }}
                  />
                </div>

                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                    textAlign: "center",
                    height: "calc(100% - 36px)",
                  }}
                >
                  <img
                    src={_.get(
                      components
                        .concat(componentsCreated)
                        .find(
                          (ele) => ele.title === item.i || ele._id === item.i
                        ),
                      "icon"
                    )}
                    alt=""
                    style={{ width: "40px" }}
                  />
                  <p
                    style={{
                      fontSize: "14px",
                      marginBottom: 0,
                      fontFamily: "var(--roboto-500)",
                    }}
                  >
                    {_.get(
                      components
                        .concat(componentsCreated)
                        .find(
                          (ele) => ele.title === item.i || ele._id === item.i
                        ),
                      "title"
                    )}
                  </p>
                </div>
              </>
            )}
          </div>
        );
      });
    } else {
      return _.map(layoutEmpty.center, function (l, i) {
        return (
          <div key={l.i} data-grid={l}>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                height: "100%",
              }}
            >
              <Icon icon="drag-component" size={49} color="#7DBEFA" />
              <p>{t("layoutSetting.dragHere")}</p>

              {type !== "center" && (
                <span>{t("layoutSetting.fixedComponent")}</span>
              )}
            </div>
          </div>
        );
      });
    }
  };

  const onResizeStart = () => {
    $showComponents(false);
  };

  const onResizeStop = (type, layout) => {
    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: true,
        data: {
          layouts_center: {
            ...layoutsCenter,
            [type]: layout,
          },
          layouts_left: layoutsLeft,
          layouts_right: layoutsRight,
        },
      })
    );
    $showComponents(true);
  };

  const onDragStart = () => {
    $showComponents(false);
  };

  const onDragStop = (type, layout) => {
    // $layoutsCenter({
    //   ...layoutsCenter,
    //   [type]: layout,
    // });
    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: true,
        data: {
          layouts_center: {
            ...layoutsCenter,
            [type]: layout,
          },
          layouts_left: layoutsLeft,
          layouts_right: layoutsRight,
        },
      })
    );
    $showComponents(true);
  };

  const onDrop = (type, layout, layoutItem, activeTab) => {
    let temp = components
      .concat(componentsCreated)
      .find((ele) => ele._id === selectedComp && ele.type === "tab");
    let temComponent = temp ? { ...temp } : null;
    let temTabs = [...layoutsCenter["tabs"]];
    if (temComponent) {
      let temp = temComponent.tab_component
        ? { ...temComponent.tab_component }
        : {};
      temComponent.tab_name.forEach((item) => {
        if (!temp[item]) {
          temp[item] = [];
        }
      });
      temComponent["tab_component"] = temp;
      temTabs.push(temComponent);
    }
    const newLayout = [
      ...layout.filter(
        (item) => item.i !== "emptyCenter" && item.i !== "__dropping-elem__"
      ),
      {
        ...layoutItem,
        i: selectedComp,
        w: type === "top" ? 12 : 2,
        h: type !== "top" ? 2 : 1,
      },
    ];
    const result = newLayout.slice(0);
    result.sort(function (a, b) {
      return a.y - b.y;
    });

    $selectedComp("");
    $showComponents(true);

    if (type === "top" && temComponent) {
    } else {
      dispatch(
        loadConfigComponents({
          object_id: selectedObject,
          is_update: true,
          data: {
            layouts_center: {
              ...layoutsCenter,
              [type]: result,
              tabs: temTabs,
            },
            layouts_left: layoutsLeft,
            layouts_right: layoutsRight,
          },
        })
      );
    }
  };

  return (
    <Wrapper>
      <Top isEmpty={!_.size(layoutsCenter.top) > 0}>
        <ResponsiveReactGridLayout
          className="layout"
          cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
          layouts={layoutsCenter}
          onDragStart={onDragStart}
          onResizeStart={onResizeStart}
          onResizeStop={onResizeStop}
          onDragStop={(layout) => onDragStop("top", layout)}
          onDrop={(layout, layoutItem) => onDrop("top", layout, layoutItem)}
          measureBeforeMount={true}
          useCSSTransforms={true}
          compactType="vertical"
          isDroppable={!_.size(layoutsCenter.top) > 0}
          isDraggable={false}
          isResizable={false}
        >
          {generateDOMCenter("top")}
        </ResponsiveReactGridLayout>
      </Top>

      <div style={{ display: "flex", margin: "8px 12px 0 12px" }}>
        <Left isEmpty={!_.size(layoutsCenter.left) > 0}>
          <ResponsiveReactGridLayout
            className="layout"
            cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
            layouts={layoutsCenter}
            onDragStart={onDragStart}
            onResizeStart={onResizeStart}
            onResizeStop={onResizeStop}
            onDragStop={(layout) => onDragStop("left", layout)}
            onDrop={(layout, layoutItem) => onDrop("left", layout, layoutItem)}
            measureBeforeMount={true}
            useCSSTransforms={true}
            compactType="vertical"
            isDroppable={!_.size(layoutsCenter.left) > 0}
            isDraggable={false}
            isResizable={false}
          >
            {generateDOMCenter("left")}
          </ResponsiveReactGridLayout>
        </Left>

        <Center isEmpty={!_.size(layoutsCenter.center) > 0}>
          <ResponsiveReactGridLayout
            className="layout"
            cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
            layouts={layoutsCenter}
            onDragStart={onDragStart}
            onResizeStart={onResizeStart}
            onResizeStop={(layout) => onResizeStop("center", layout)}
            onDragStop={(layout) => onDragStop("center", layout)}
            onDrop={(layout, layoutItem) =>
              onDrop("center", layout, layoutItem)
            }
            measureBeforeMount={true}
            useCSSTransforms={true}
            compactType="vertical"
            isDroppable={true}
            isDraggable={_.size(layoutsCenter.center) > 0}
            isResizable={_.size(layoutsCenter.center) > 0}
          >
            {generateDOMCenter("center")}
          </ResponsiveReactGridLayout>
        </Center>

        <Right isEmpty={!_.size(layoutsCenter.right) > 0}>
          <ResponsiveReactGridLayout
            className="layout"
            cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
            layouts={layoutsCenter}
            onDragStart={onDragStart}
            onResizeStart={onResizeStart}
            onResizeStop={onResizeStop}
            onDragStop={(layout) => onDragStop("right", layout)}
            onDrop={(layout, layoutItem) => onDrop("right", layout, layoutItem)}
            measureBeforeMount={true}
            useCSSTransforms={true}
            compactType="vertical"
            isDroppable={!_.size(layoutsCenter.right) > 0}
            isDraggable={false}
            isResizable={false}
          >
            {generateDOMCenter("right")}
          </ResponsiveReactGridLayout>
        </Right>
      </div>
      <ModalTabs
        selectedTabComp={selectedTabComp}
        $selectedTabComp={$selectedTabComp}
        dataTabComp={dataTabComp}
        indexTab={indexTab}
        layoutsCenter={layoutsCenter}
        activeTabComp={activeTabComp}
        selectedObject={selectedObject}
        layoutsLeft={layoutsLeft}
        layoutsRight={layoutsRight}
        form={form}
        visible={visible}
        $visible={$visible}
        $showComponents={$showComponents}
        $indexTab={$indexTab}
        componentsCreated={componentsCreated}
        $dataTabComp={$dataTabComp}
      />
    </Wrapper>
  );
}

export default CenterScreen;

const Wrapper = styled.div`
  .card {
    animation: fade-out 0.2s forwards;
  }

  @keyframes fade-out {
    100% {
      opacity: 0;
      display: none;
    }
  }
`;

const Top = styled.div`
  border-radius: 8px;
  background: #f5fcff;
  height: 170px;
  margin: 0 12px;

  p {
    margin-bottom: 0;
    margin-top: 8px;
    color: #2c2c2c;
  }

  span {
    color: #919eab;
  }

  .react-grid-layout {
    height: 170px !important;
    border: 1px dashed rgba(28, 104, 146, 0.5);
    border-radius: 8px;
  }

  .react-grid-item {
    border-radius: 8px;
    border: 1px solid #ececec;
    box-shadow: 0px 0px 6px 0px rgba(32, 28, 28, 0.08);

    width: ${({ isEmpty }) => isEmpty && "100%"} !important;
    height: ${({ isEmpty }) => isEmpty && "100%"} !important;
    background: ${({ isEmpty }) => isEmpty && "#f5fcff"};
    border: ${({ isEmpty }) => isEmpty && "none"};
    transform: ${({ isEmpty }) => isEmpty && "none"} !important;
  }
`;

const Left = styled.div`
  background: #f5fcff;
  width: 230px;
  height: 330px;

  .react-grid-layout {
    height: 330px !important;
    border: 1px dashed rgba(28, 104, 146, 0.5);
    border-radius: 8px;
  }

  .react-grid-item {
    border-radius: 8px;
    border: 1px solid #ececec;
    box-shadow: 0px 0px 6px 0px rgba(32, 28, 28, 0.08);

    width: ${({ isEmpty }) => isEmpty && "100%"} !important;
    height: ${({ isEmpty }) => isEmpty && "100%"} !important;
    background: ${({ isEmpty }) => isEmpty && "#f5fcff"};
    border: ${({ isEmpty }) => isEmpty && "none"};
    transform: ${({ isEmpty }) => isEmpty && "none"} !important;
  }

  p {
    margin-bottom: 0;
    margin-top: 8px;
    color: #2c2c2c;
  }

  span {
    color: #919eab;
  }
`;

const Right = styled(Left)``;

const Center = styled.div`
  flex: 1;
  margin: 0 8px;
  border-radius: 8px;
  border: 1px dashed rgba(28, 104, 146, 0.5);
  background: #f5fcff;
  height: fit-content;

  .react-grid-item {
    border-radius: 8px;
    border: 1px solid #ececec;
    box-shadow: 0px 0px 6px 0px rgba(32, 28, 28, 0.08);

    width: ${({ isEmpty }) => isEmpty && "100%"} !important;
    height: ${({ isEmpty }) => isEmpty && "100%"} !important;
    background: ${({ isEmpty }) => isEmpty && "#f5fcff"};
    border: ${({ isEmpty }) => isEmpty && "none"};
    transform: ${({ isEmpty }) => isEmpty && "none"} !important;
  }
`;
