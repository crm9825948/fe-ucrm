import { Responsive, WidthProvider } from "react-grid-layout";
import React, { useEffect } from "react";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import styled from "styled-components/macro";
import Icon from "components/Icon/Icon";

import { components } from "util/staticData";
import { useDispatch, useSelector } from "react-redux";
import { loadConfigComponents } from "redux/slices/consolidatedViewSettings";

const ResponsiveReactGridLayout = WidthProvider(Responsive);

const layoutEmpty = {
  center: [{ x: 0, y: 0, w: 2, h: 2, i: "emptyCenter" }],
};

function LeftScreen({
  selectedObject,
  selectedComp,
  $selectedComp,
  componentsCreated,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { layoutsCenter, layoutsLeft, layoutsRight } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const _onRemoveComp = (type, idx, id) => {
    return () => {
      const ele = document.getElementById(id);
      ele.className = "card";

      setTimeout(() => {
        const layoutType = [...layoutsLeft[type]];
        layoutType.splice(idx, 1);
        // $layoutsLeft({
        //   ...layoutsLeft,
        //   [type]: [...layoutType],
        // });
        dispatch(
          loadConfigComponents({
            object_id: selectedObject,
            is_update: true,
            data: {
              layouts_center: layoutsCenter,
              layouts_left: { ...layoutsLeft, [type]: [...layoutType] },
              layouts_right: layoutsRight,
            },
          })
        );
      }, 300);
    };
  };

  const generateDOMCenter = (type) => {
    if (_.size(layoutsLeft.left) > 0) {
      return _.map(layoutsLeft.left, function (item, idx) {
        return (
          <div
            id={item.i}
            key={item.i}
            data-grid={item}
            style={{ background: "#F5FCFF" }}
          >
            <div
              style={{
                borderRadius: "8px 8px 0px 0px",
                background: "#CFE9F4",
                height: "36px",
                padding: "6px 12px",
                fontFamily: "var(--roboto-500)",
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              {_.get(
                components
                  .concat(componentsCreated)
                  .find((ele) => ele.title === item.i || ele._id === item.i),
                "title"
              )}

              <Icon
                icon="close"
                size={24}
                onClick={_onRemoveComp(type, idx, item.i)}
                style={{ cursor: "pointer" }}
              />
            </div>

            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                textAlign: "center",
                height: "calc(100% - 36px)",
              }}
            >
              <img
                src={_.get(
                  components
                    .concat(componentsCreated)
                    .find((ele) => ele.title === item.i || ele._id === item.i),
                  "icon"
                )}
                alt=""
                style={{ width: "40px" }}
              />
              <p
                style={{
                  fontSize: "14px",
                  marginBottom: 0,
                  fontFamily: "var(--roboto-500)",
                }}
              >
                {_.get(
                  components
                    .concat(componentsCreated)
                    .find((ele) => ele.title === item.i || ele._id === item.i),
                  "title"
                )}
              </p>
            </div>
          </div>
        );
      });
    } else {
      return _.map(layoutEmpty.center, function (l, i) {
        return (
          <div key={l.i} data-grid={l}>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                flexDirection: "column",
                height: "100%",
              }}
            >
              <Icon icon="drag-component" size={49} color="#7DBEFA" />
              <p>{t("layoutSetting.dragHere")}</p>
            </div>
          </div>
        );
      });
    }
  };

  const onDragStop = (type, layout) => {
    // $layoutsLeft({
    //   ...layoutsLeft,
    //   [type]: layout,
    // });
    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: true,
        data: {
          layouts_center: layoutsCenter,
          layouts_left: { ...layoutsLeft, [type]: layout },
          layouts_right: layoutsRight,
        },
      })
    );
  };

  const onDrop = (type, layout, layoutItem, activeTab) => {
    const newLayout = [
      ...layout.filter(
        (item) => item.i !== "emptyCenter" && item.i !== "__dropping-elem__"
      ),
      {
        ...layoutItem,
        i: selectedComp,
        w: 6,
        h: 2,
        // comp: activeTab + "-" + selectedComp,
      },
    ];

    const result = newLayout.slice(0);
    result.sort(function (a, b) {
      return a.y - b.y;
    });

    $selectedComp("");
    // $layoutsLeft({
    //   ...layoutsLeft,
    //   [type]: result,
    // });
    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: true,
        data: {
          layouts_center: layoutsCenter,
          layouts_left: { ...layoutsLeft, [type]: result },
          layouts_right: layoutsRight,
        },
      })
    );
  };

  useEffect(() => {
    console.log("layoutsLeft :>> ", layoutsLeft);
  }, [layoutsLeft]);

  return (
    <Wrapper isEmpty={!_.size(layoutsLeft.left) > 0}>
      <ResponsiveReactGridLayout
        className="layout"
        cols={{ lg: 12, md: 10, sm: 6, xs: 4, xxs: 2 }}
        layouts={layoutsLeft}
        onDragStop={(layout) => onDragStop("left", layout)}
        onDrop={(layout, layoutItem) => onDrop("left", layout, layoutItem)}
        measureBeforeMount={true}
        useCSSTransforms={true}
        compactType="vertical"
        isDroppable={true}
        isDraggable={_.size(layoutsLeft.left) > 0}
        isResizable={_.size(layoutsLeft.left) > 0}
      >
        {generateDOMCenter("left")}
      </ResponsiveReactGridLayout>
    </Wrapper>
  );
}

export default LeftScreen;

const Wrapper = styled.div`
  .card {
    animation: fade-out 0.2s forwards;
  }

  @keyframes fade-out {
    100% {
      opacity: 0;
      display: none;
    }
  }

  margin: 0 8px;
  border-radius: 8px;
  border: 1px dashed rgba(28, 104, 146, 0.5);
  background: #f5fcff;
  height: ${({ isEmpty }) =>
    isEmpty ? "calc(100vh - 220px)" : "fit-content"} !important;

  .react-grid-layout {
    height: ${({ isEmpty }) =>
      isEmpty ? "100%" : "calc(100vh - 220px)"} !important;
    overflow: auto;
  }

  .react-grid-item {
    border-radius: 8px;
    border: 1px solid #ececec;
    box-shadow: 0px 0px 6px 0px rgba(32, 28, 28, 0.08);
    width: calc(100% - 24px) !important;
    height: ${({ isEmpty }) => isEmpty && "97%"} !important;

    background: ${({ isEmpty }) => isEmpty && "#f5fcff"};
    border: ${({ isEmpty }) => isEmpty && "none"};
  }
`;
