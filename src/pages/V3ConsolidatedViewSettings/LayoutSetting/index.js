import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "redux/store";
import _ from "lodash";

import Tabs from "antd/lib/tabs";
import Typography from "antd/lib/typography";
import Icon from "components/Icon/Icon";
import CenterScreen from "./CenterScreen";
import LeftScreen from "./LeftScreen";
import RightScreen from "./RightScreen";

import detailsImg from "assets/images/consolidatedView/detail.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import KnowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";
import General from "assets/images/consolidatedView/General configuration.png";
import Template from "assets/images/consolidatedView/template configuration.png";
import SMS from "assets/images/consolidatedView/SMS component.svg";
import tabsImg from "assets/images/consolidatedView/tabs.png";

import { getListSMSOutgoing } from "redux/slices/smsOutgoing";
import {
  loadComponents,
  loadConfigComponents,
} from "redux/slices/consolidatedViewSettings";
import { componentsLayoutSettings } from "util/staticData";

const { Text } = Typography;

const handlerIconComp = (type) => {
  switch (type) {
    case "details":
    case "DETAILS":
    case "multi_detail":
      return detailsImg;

    case "table":
    case "TABLE":
      return tableImg;

    case "tags":
      return tagsImg;

    case "knowledge":
      return KnowledgeBaseImg;
    case "tab":
      return tabsImg;
    default:
      break;
  }
};

const normalizeComponents = (items) => {
  return _.map(
    items,
    (item) =>
      (item = {
        ...item,
        title: item.name,
        icon: handlerIconComp(
          _.get(item, "type") || _.get(item, "component_type")
        ),
      })
  );
};

const filterComponentsLayoutSettings = (layouts, activeKey) => {
  let tem = [...componentsLayoutSettings];
  if (activeKey !== "center-screen") {
    tem.shift();
  }
  return tem.filter(
    (item) =>
      !layouts.center.map((ele) => ele.i).includes(item.title) &&
      !layouts.left.map((ele) => ele.i).includes(item.title) &&
      !layouts.right.map((ele) => ele.i).includes(item.title) &&
      !layouts.top.map((ele) => ele.i).includes(item.title)
  );
};

const filterComponentsCreated = (componentsCreated, layouts) => {
  return componentsCreated.filter(
    (item) =>
      !layouts.center.map((ele) => ele.i).includes(item._id) &&
      !layouts.left.map((ele) => ele.i).includes(item._id) &&
      !layouts.right.map((ele) => ele.i).includes(item._id) &&
      !layouts.top.map((ele) => ele.i).includes(item._id)
  );
};

const LayoutSetting = ({ selectedObject, collapsed }) => {
  const { TabPane } = Tabs;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { listSMSOutgoing } = useSelector((state) => state.smsOutgoingReducer);
  const { components, layoutsCenter, layoutsLeft, layoutsRight } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [activeKey, $activeKey] = useState("center-screen");
  const [selectedComp, $selectedComp] = useState("");

  const [showComponents, $showComponents] = useState(true);
  const [componentsCreated, $componentsCreated] = useState([]);

  const handleChangeTab = (key) => {
    $activeKey(key);
  };
  const renderComponent = (icon, title, des, objectName, id) => (
    <Component
      showComponents={showComponents}
      draggable={true}
      unselectable="on"
      onDragStart={(e) => {
        e.dataTransfer.setData("text/plain", "");
        $selectedComp(id ? id : title);
        if (activeKey === "center-screen") $showComponents(false);
      }}
      onDragEnd={() => {
        $selectedComp("");
        if (activeKey === "center-screen") $showComponents(true);
      }}
    >
      <div
        style={{
          display: "flex",
          alignItems: "center",
          borderBottom: "1px solid #ececec",
          paddingBottom: "8px",
        }}
      >
        <img src={icon} alt="" />

        <div>
          <p>{title}</p>
          <Text
            style={{ textTransform: "capitalize" }}
            ellipsis={{ tooltip: des }}
          >
            {des.toLowerCase().replaceAll("_", " ")}
          </Text>
        </div>
      </div>
      {objectName ? (
        <ObjectRelated isObject>Object: {objectName}</ObjectRelated>
      ) : (
        <ObjectRelated>Default</ObjectRelated>
      )}
    </Component>
  );

  useEffect(() => {
    dispatch(getListSMSOutgoing());
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      loadComponents({
        object_id: selectedObject,
      })
    );
    dispatch(
      loadConfigComponents({
        object_id: selectedObject,
        is_update: false,
      })
    );
  }, [selectedObject, dispatch]);

  useEffect(() => {
    $componentsCreated(normalizeComponents(components));
  }, [components]);

  return (
    <Wrapper activeKey={activeKey}>
      <Tabs activeKey={activeKey} onChange={handleChangeTab}>
        <TabPane
          tab={
            <Title>
              <Icon icon="left-screen" size={20} />
              {t("layoutSetting.left")}
            </Title>
          }
          key="left-screen"
        >
          <LeftScreen
            selectedObject={selectedObject}
            selectedComp={selectedComp}
            $selectedComp={$selectedComp}
            componentsCreated={componentsCreated}
          />
        </TabPane>
        <TabPane
          tab={
            <Title>
              <Icon icon="center-screen" size={20} />
              {t("layoutSetting.center")}
            </Title>
          }
          key="center-screen"
        >
          <CenterScreen
            selectedObject={selectedObject}
            selectedComp={selectedComp}
            $selectedComp={$selectedComp}
            $showComponents={$showComponents}
            componentsCreated={componentsCreated}
          />
        </TabPane>
        <TabPane
          tab={
            <Title>
              <Icon icon="right-screen" size={20} />
              {t("layoutSetting.right")}
            </Title>
          }
          key="right-screen"
        >
          <RightScreen
            selectedObject={selectedObject}
            selectedComp={selectedComp}
            $selectedComp={$selectedComp}
            componentsCreated={componentsCreated}
          />
        </TabPane>
      </Tabs>

      <Components collapsed={collapsed} activeKey={activeKey}>
        {_.map(
          filterComponentsLayoutSettings(
            activeKey === "center-screen"
              ? layoutsCenter
              : activeKey === "left-screen"
              ? layoutsLeft
              : layoutsRight,
            activeKey
          ),
          (item, idx) => (
            <div key={idx}>
              {renderComponent(item.icon, item.title, item.des)}
            </div>
          )
        )}

        {_.size(componentsCreated) > 0 &&
          _.map(
            filterComponentsCreated(
              componentsCreated,
              activeKey === "center-screen"
                ? layoutsCenter
                : activeKey === "left-screen"
                ? layoutsLeft
                : layoutsRight
            ),
            (item, idx) => (
              <div key={idx}>
                {renderComponent(
                  _.get(item, "icon"),
                  _.get(item, "title"),
                  _.get(item, "type") || _.get(item, "component_type"),
                  _.get(item, "related_object_name") ||
                    _.get(item, "object_name") ||
                    "",
                  _.get(item, "_id")
                )}
              </div>
            )
          )}

        {selectedObject === "obj_crm_campaign_00001" && (
          <>
            {renderComponent(
              General,
              "General Configuration",
              "General configuration component"
            )}
            {renderComponent(
              Template,
              "Template Configuration",
              "Template configuration component"
            )}
          </>
        )}

        {listSMSOutgoing.find((item) => item.object_id === selectedObject) && (
          <>{renderComponent(SMS, "SMS component", "SMS component")}</>
        )}
      </Components>
    </Wrapper>
  );
};

export default LayoutSetting;

const Wrapper = styled.div`
  width: 100%;
  background: #fff;
  overflow-x: hidden;
  position: relative;
  display: ${({ activeKey }) =>
    (activeKey === "left-screen" || activeKey === "right-screen") && "flex"};
  flex-direction: ${({ activeKey }) =>
    activeKey === "left-screen"
      ? "row-reverse"
      : activeKey === "right-screen"
      ? "row"
      : "unset"};
  padding-bottom: ${({ activeKey }) =>
    (activeKey === "left-screen" || activeKey === "right-screen") && "12px"};

  .ant-tabs {
    margin: ${({ activeKey }) =>
      activeKey === "center-screen" ? "8px auto 0 auto" : "8px 0 0 0"};
    width: ${({ activeKey }) =>
      (activeKey === "left-screen" || activeKey === "right-screen") &&
      "calc(100% - 255px)"};
  }

  .ant-tabs-nav {
    ::before {
      display: none;
    }
  }

  .ant-tabs-nav-list {
    padding: 0 4px;
    border-radius: 4px;
    border: 1px solid var(--neutral-5, #d9d9d9);
    background: #fff;
    box-shadow: 0px 0px 16px 0px rgba(0, 0, 0, 0.08);
    margin: 0 auto;
  }

  .ant-tabs-tab {
    padding: 4px 0;
  }

  .ant-tabs-tab-btn {
    padding: 4px 12px;
    border-radius: 4px;
    transition: all 0.7s;
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    background: ${(props) => props.theme.main} !important;
    color: #fff;
  }

  .ant-tabs-ink-bar {
    display: none;
  }

  .ant-tabs-content-holder {
    margin-bottom: ${({ activeKey }) =>
      activeKey === "center-screen" && "145px"};
  }

  .ant-tabs-content {
    height: ${({ activeKey }) =>
      (activeKey === "left-screen" || activeKey === "right-screen") && "100%"};
  }

  .ant-tabs-tabpane {
    height: ${({ activeKey }) =>
      (activeKey === "left-screen" || activeKey === "right-screen") && "100%"};
  }

  .ant-tabs-tab-btn:hover {
    color: ${(props) => props.theme.darker};
    background: #e6f7ff;
  }
`;

const Title = styled.div`
  display: flex;
  align-items: center;
`;

const Components = styled.div`
  display: flex;
  overflow: auto;
  column-gap: 8px;
  position: fixed;
  background: #fff;
  padding-left: 12px;
  padding-bottom: 8px;
  margin-top: ${({ activeKey }) =>
    (activeKey === "left-screen" || activeKey === "right-screen") && "8px"};
  flex-direction: ${({ activeKey }) =>
    activeKey === "left-screen" || activeKey === "right-screen"
      ? "column"
      : "row"};
  width: ${({ collapsed, activeKey }) =>
    activeKey === "left-screen" || activeKey === "right-screen"
      ? "255px"
      : collapsed
      ? "100%"
      : "calc(100% - 355px)"};
  height: ${({ activeKey }) =>
    (activeKey === "left-screen" || activeKey === "right-screen") &&
    "calc(100vh - 220px)"};
  bottom: ${({ activeKey }) => activeKey === "center-screen" && 0};
  left: ${({ activeKey, collapsed }) =>
    collapsed && activeKey === "left-screen"
      ? "129px"
      : activeKey === "left-screen"
      ? "336px"
      : "unset"};
  right: ${({ activeKey, collapsed }) =>
    activeKey === "right-screen" ? "16px" : "unset"};
  top: ${({ activeKey }) =>
    (activeKey === "left-screen" || activeKey === "right-screen") && "200px"};
  transition: all 0.5s ease-in-out;

  &::-webkit-scrollbar {
    height: 8px !important;
  }
  ::-webkit-scrollbar-thumb {
    background: #bfbfbf;
  }

  ::-webkit-scrollbar-track {
    background: #d9d9d9;
  }
`;

const Component = styled.div`
  border-radius: 8px;
  border: 1px solid #ececec;
  background: #fff;
  display: flex;
  padding: 12px;
  flex-direction: column;
  min-width: 230px;
  width: 230px;
  cursor: pointer;
  opacity: ${({ showComponents }) => (showComponents ? "1" : "0")};
  transition: opacity 0.5s ease-in-out;

  :hover {
    border: 1px solid #1c9292;
    box-shadow: 0px 4px 10px 0px rgba(0, 0, 0, 0.07);
    transition: all 0.5s;
  }

  .ant-typography {
    width: 155px;
  }

  img {
    width: 40px;
    margin-right: 8px;
  }

  p {
    color: #2c2c2c;
    font-family: var(--roboto-500);
    margin-bottom: 0;
  }

  span {
    color: #6b6b6b;
  }
`;

const ObjectRelated = styled.div`
  color: #6b6b6b;
  margin-top: 8px;
  border-radius: 4px;
  border: ${({ isObject }) => !isObject && "1px solid #ececec"};
  background: ${({ isObject }) => !isObject && "#f0f0f0"};
  padding: ${({ isObject }) => !isObject && "2px 8px"};
  width: fit-content;
`;
