import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Pagination from "antd/lib/pagination";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import ModalSetting from "./ModalSetting";

import {
  WrapBreadcrumb,
  BreadcrumbItem,
  AddButtonBreadcrumb,
} from "components/Style/common";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import { setShowModalConfirmDelete } from "redux/slices/global";

import { loadAllRole } from "redux/slices/user";

import {
  showLdapSetting,
  loadAllSetting,
  deleteSetting,
  syncData,
} from "redux/slices/ldap";
import { changeTitlePage } from "redux/slices/authenticated";

function Ldap(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;
  const { Text } = Typography;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { allSetting, totalSetting } = useSelector(
    (state) => state.ldapReducer
  );

  const [listSetting, setListSetting] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [dataDelete, setDataDelete] = useState({});
  const [dataEdit, setDataEdit] = useState({});

  useEffect(() => {
    dispatch(changeTitlePage("LDAP"));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "ldap" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  const showTotal = () => {
    return `${t("common.total")} ${totalSetting} ${t("common.items")}`;
  };

  const _onDeleteSetting = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: id });
  };

  const getListSetting = useCallback(() => {
    dispatch(
      loadAllSetting({
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [dispatch, currentPage, recordPerPage]);

  const _onSyncData = () => {
    dispatch(syncData());
  };

  useEffect(() => {
    getListSetting();
  }, [getListSetting]);

  useEffect(() => {
    let tempList = [];
    allSetting.map((item) => {
      return tempList.push({
        key: item._id,
        domain_name: item.domain_name,
        description: item.description,
        dn: item.dn,
        port: item.port,
        action: "",
        fullData: item,
      });
    });

    setTimeout(() => {
      setListSetting(tempList);
    }, 50);
  }, [dispatch, allSetting]);

  useEffect(() => {
    dispatch(loadAllRole());
  }, [dispatch]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>LDAP</BreadcrumbItem>
        </Breadcrumb>
        {listSetting.length > 0 && checkRule("create") && (
          <WrapButton>
            <SyncData onClick={_onSyncData}>{t("ldap.sync")}</SyncData>
            <AddButtonBreadcrumb
              onClick={() => dispatch(showLdapSetting(true))}
            >
              + {t("ldap.add")}
            </AddButtonBreadcrumb>
          </WrapButton>
        )}
      </WrapBreadcrumb>

      {listSetting.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("ldap.setting")}</span>
          </p>
          {checkRule("create") && (
            <AddButtonBreadcrumb
              onClick={() => dispatch(showLdapSetting(true))}
            >
              + {t("ldap.add")}
            </AddButtonBreadcrumb>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table
            pagination={false}
            dataSource={listSetting}
            scroll={{ x: "max-content" }}
          >
            <Column
              title={t("ldap.domain")}
              dataIndex="domain_name"
              key="domain_name"
              width="300px"
              sorter={(a, b) => a.domain_name.localeCompare(b.domain_name)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("ldap.description")}
              dataIndex="description"
              key="description"
              width="200px"
            />
            <Column
              title="Dn"
              dataIndex="dn"
              key="dn"
              width="300px"
              sorter={(a, b) => a.dn.localeCompare(b.dn)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("ldap.port")}
              dataIndex="port"
              key="port"
              width="200px"
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                fixed="right"
                width="150px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() => {
                            setDataEdit(record.fullData);
                            dispatch(showLdapSetting(true));
                          }}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDeleteSetting(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={totalSetting}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </WrapTable>
      )}

      <ModalSetting dataEdit={dataEdit} setDataEdit={setDataEdit} />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteSetting}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default withTranslation()(Ldap);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled.div``;

const SyncData = styled(AddButtonBreadcrumb)`
  margin-right: 16px;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
