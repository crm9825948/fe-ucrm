import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import Checkbox from "antd/lib/checkbox";
import Select from "antd/lib/select";

import { loadUserReport } from "redux/slices/user";

import {
  showLdapSetting,
  createSetting,
  updateSetting,
} from "redux/slices/ldap";

function ModalSetting({ dataEdit, setDataEdit }) {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [roleOptions, setRoleOptions] = useState([]);
  const [userReport, setUserReport] = useState([]);
  const [disableField, setDisableField] = useState(true);
  const [isAlias, $isAlias] = useState(false);

  const { userDetail } = useSelector((state) => state.userReducer);
  const { showSetting } = useSelector((state) => state.ldapReducer);

  const { listRole, isLoadingUser, listUserReport } = useSelector(
    (state) => state.userReducer
  );

  const _onSubmit = (values) => {
    if (Object.keys(dataEdit).length > 0) {
      dispatch(
        updateSetting({
          is_ssl: values.is_ssl,
          description: values.description,
          domain_name: values.domain_name,
          username: values.username,
          dn: values.dn,
          port: values.port,
          password: values.password,
          email: values.email,
          firstName: values.first_name,
          lastName: values.last_name,
          role: values.role,
          report_to: values.report_to,
          id: dataEdit._id,
          is_alias: _.get(values, "is_alias", null),
        })
      );
    } else {
      dispatch(
        createSetting({
          is_ssl: values.is_ssl,
          description: values.description,
          domain_name: values.domain_name,
          username: values.username,
          dn: values.dn,
          port: values.port,
          password: values.password,
          email: values.email,
          firstName: values.first_name,
          lastName: values.last_name,
          role: values.role,
          report_to: values.report_to,
          is_alias: _.get(values, "is_alias", null),
        })
      );
    }
  };

  const _onCancel = () => {
    dispatch(showLdapSetting(false));
    form.resetFields();
    setDataEdit({});
    $isAlias(false);
  };

  const handleGetUserReport = () => {
    setDisableField(false);
    dispatch(
      loadUserReport({
        role_id: form.getFieldValue("role"),
      })
    );

    form.setFieldsValue({
      report_to: undefined,
    });
  };

  useEffect(() => {
    if (!showSetting) {
      form.resetFields();
      setDataEdit({});
    }
  }, [form, setDataEdit, showSetting]);

  useEffect(() => {
    if (Object.keys(dataEdit).length > 0) {
      dispatch(
        loadUserReport({
          role_id: dataEdit.role,
        })
      );
      setDisableField(false);
      if (_.get(dataEdit, "is_alias", "")) {
        $isAlias(true);
      }

      form.setFieldsValue({
        domain_name: dataEdit.domain_name,
        description: dataEdit.description,
        dn: dataEdit.dn,
        port: dataEdit.port,
        username: dataEdit.username,
        password: dataEdit.password,
        is_ssl: dataEdit.is_ssl,
        role: dataEdit.role,
        report_to: dataEdit.report_to,
        email: dataEdit.email,
        first_name: dataEdit.first_name,
        last_name: dataEdit.last_name,
        is_alias: _.get(dataEdit, "is_alias", ""),
        use_alias: _.get(dataEdit, "is_alias") ? true : false,
      });
    }
  }, [dataEdit, dispatch, form]);

  useEffect(() => {
    if (listRole.length > 0) {
      let tempList = [];

      listRole.forEach((item, idx) => {
        if (idx !== 0) {
          tempList.push({
            label: item.Name,
            value: item._id,
          });
        }
      });
      setRoleOptions(tempList);
    }
  }, [listRole]);

  useEffect(() => {
    let tempList = [];

    listUserReport.forEach((item) => {
      tempList.push({
        label:
          item.Middle_Name === ""
            ? item.Last_Name + " " + item.First_Name
            : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
        value: item._id,
      });
    });
    setUserReport(tempList);
  }, [listUserReport, userDetail]);

  return (
    <ModalCustom
      title={t("ldap.setting")}
      visible={showSetting}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("ldap.domain")}
          name="domain_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item label="Description" name="description">
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item
          label="Dn"
          name="dn"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item
          label={t("ldap.port")}
          name="port"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <InputNumber placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item
          label={t("ldap.userName")}
          name="username"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input
            autocomplete="new-password"
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>

        <Form.Item
          label={t("ldap.password")}
          name="password"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input.Password
            autocomplete="new-password"
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>

        <Form.Item
          label="Email"
          name="email"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input
            autocomplete="new-password"
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>

        <Form.Item
          label={t("ldap.firstName")}
          name="first_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item
          label={t("ldap.lastName")}
          name="last_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>

        <Form.Item
          label={t("settings.role")}
          name="role"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            placeholder={t("common.placeholderSelect")}
            options={roleOptions}
            onChange={handleGetUserReport}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>
        <Form.Item
          label={t("user.reportTo")}
          name="report_to"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            loading={isLoadingUser}
            placeholder={t("common.placeholderSelect")}
            options={userReport}
            disabled={disableField}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Form.Item label="Is SSL" name="is_ssl" valuePropName="checked">
          <Checkbox />
        </Form.Item>

        <Form.Item label="Is Alias" name="use_alias" valuePropName="checked">
          <Checkbox onChange={(e) => $isAlias(e.target.checked)} />
        </Form.Item>

        {isAlias && (
          <Form.Item
            label="Use Alias"
            name="is_alias"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("common.placeholderInput")} />
          </Form.Item>
        )}

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-collapse {
    border: none;
    background: #fff;
  }

  .ant-collapse-item {
    border-bottom: none;
    margin-bottom: 16px;
  }

  .ant-collapse-content {
    border-top: none;
  }

  .ant-collapse-content-box {
    background: #fafafa;
    margin-top: 16px;
    display: flex;
    flex-wrap: wrap;
  }

  .ant-input-number {
    width: 100%;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
