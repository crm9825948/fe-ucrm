import React, { useEffect } from "react";
import _ from "lodash";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import { Row, Col } from "antd";
import { changeTitlePage } from "redux/slices/authenticated";

import AgentMonitorSetting from "assets/icons/settings/AgentMonitorSetting.svg";
import AssignmentRuleSetting from "assets/icons/settings/AssignmentRuleSetting.svg";
import BrandNameSetting from "assets/icons/settings/BrandNameSetting.svg";
import CampaignSetting from "assets/icons/settings/CampaignSetting.svg";
import ConsolidatedViewSetting from "assets/icons/settings/ConsolidatedViewSetting.svg";
import CriteriaSharingSetting from "assets/icons/settings/CriteriaSharingSetting.svg";
import CTISetting from "assets/icons/settings/CTISetting.svg";
import CreateTemplateRecord from "assets/icons/settings/CreateTemplateRecord.svg";
import DateTimeSetting from "assets/icons/settings/DateTimeSetting.svg";
import DuplicateRuleSetting from "assets/icons/settings/DuplicateRuleSetting.svg";
import DynamicButtonSetting from "assets/icons/settings/DynamicButtonSetting.svg";
import ApprovalProcessesSetting from "assets/icons/settings/ApprovalProcessesSetting.svg";
import EmailIncomingSetting from "assets/icons/settings/EmailIncomingSetting.svg";
import EmailOutgoingSetting from "assets/icons/settings/EmailOutgoingSetting.svg";
import EmailTemplateSetting from "assets/icons/settings/EmailTemplateSetting.svg";
import ExposeAPICheckSetting from "assets/icons/settings/ExposeAPICheckSetting.svg";
import ExposeAPICreateSetting from "assets/icons/settings/ExposeAPICreateSetting.svg";
import ExposeAPIUpdateSetting from "assets/icons/settings/ExposeAPIUpdateSetting.svg";
import ExposeAPIDeleteSetting from "assets/icons/settings/ExposeAPIDeleteSetting.svg";
import ExposeAPIViewSetting from "assets/icons/settings/ExposeAPIViewSetting.svg";
import ExposeAPIUploadSetting from "assets/icons/settings/ExposeAPIUploadSetting.svg";
import ExternalAppIntegrationSetting from "assets/icons/settings/ExternalAppIntegrationSetting.svg";
import FinesseSetting from "assets/icons/settings/FinesseSetting.svg";
import GoogleSetting from "assets/icons/settings/GoogleSetting.svg";
import GroupSetting from "assets/icons/settings/GroupSetting.svg";
import HighlightSetting from "assets/icons/settings/HighlightSetting.svg";
import ICSetting from "assets/icons/settings/ICSetting.svg";
import KanbanSetting from "assets/icons/settings/KanbanSetting.svg";
import KnowledgeBaseSetting from "assets/icons/settings/KnowledgeBaseSetting.svg";
import LDAPSetting from "assets/icons/settings/LDAPSetting.svg";
import LogSetting from "assets/icons/settings/LogSetting.svg";
import ObjectFieldSetting from "assets/icons/settings/ObjectFieldSetting.svg";
import ObjectSetting from "assets/icons/settings/ObjectSetting.svg";
import OutlookSetting from "assets/icons/settings/OutlookSetting.svg";
import PicklistSetting from "assets/icons/settings/PicklistSetting.svg";
import ProfileSetting from "assets/icons/settings/ProfileSetting.svg";
import RelatedObjectSetting from "assets/icons/settings/RelatedObjectSetting.svg";
import RoleSetting from "assets/icons/settings/RoleSetting.svg";
import SharingSetting from "assets/icons/settings/SharingSetting.svg";
import SLASetting from "assets/icons/settings/SLASetting.svg";
import SMSOutgoingSetting from "assets/icons/settings/SMSOutgoingSetting.svg";
import SMSTemplateSetting from "assets/icons/settings/SMSTemplateSetting.svg";
import URLPopupSetting from "assets/icons/settings/URLPopupSetting.svg";
import UserSetting from "assets/icons/settings/UserSetting.svg";
import VoiceBiometricSetting from "assets/icons/settings/VoiceBiometricSetting.svg";
import VoiceBotSetting from "assets/icons/settings/VoiceBotSetting.svg";
import WorkflowSetting from "assets/icons/settings/WorkflowSetting.svg";
import InteractionSetting from "assets/icons/settings/interaction_settings.png";
import FormSetting from "assets/icons/settings/formSetting.svg";
// import TemplateFileSetting from "assets/icons/settings/TemplateFileSetting.svg";
import ObjectStandard from "assets/icons/settings/ObjectStandard.svg";
import QualityManagementSetting from "assets/icons/qualityManagement/IconQualityManagement.svg";

const Settings = () => {
  let navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (setting) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === setting && item.actions.includes("view")
      )
    )
      return true;
    else return false;
  };

  const settings_menu = [
    {
      group: t("settings.account"),
      items: [
        {
          title: t("settings.users"),
          description: t("user.description"),
          url: "/users",
          icon: UserSetting,
          isAccess: checkRule("user"),
        },
        {
          title: t("settings.role"),
          description: t("role.description"),
          url: "/roles",
          icon: RoleSetting,
          isAccess: checkRule("role"),
        },
        {
          title: t("settings.profiles"),
          description: t("profile.description"),
          url: "/profiles",
          icon: ProfileSetting,
          isAccess: checkRule("profile"),
        },
        {
          title: t("settings.group"),
          description: t("group.description"),
          url: "/groups",
          icon: GroupSetting,
          isAccess: checkRule("group"),
        },
        {
          title: t("settings.sharing"),
          description: t("sharing.description"),
          url: "/sharing",
          icon: SharingSetting,
          isAccess: checkRule("sharing"),
        },
        {
          title: t("settings.criteriaSharing"),
          description: t("criteriaSharing.description"),
          url: "/criteria-sharing",
          icon: CriteriaSharingSetting,
          isAccess: checkRule("criteria_sharing"),
        },

        {
          title: t("settings.brandName"),
          description: t("settings.brandNameDescription"),
          url: "/brand-name",
          icon: BrandNameSetting,
          isAccess: checkRule("brand_and_color"),
        },
        {
          title: t("settings.assignmentRule"),
          description: t("settings.descAssignmentRule"),
          url: "/assignment-rule",
          icon: AssignmentRuleSetting,
          isAccess: checkRule("assignment_rules"),
        },
        {
          title: t("settings.agentsMonitor"),
          description: t("settings.descAgentsMonitor"),
          url: "/agents-monitor",
          icon: AgentMonitorSetting,
          isAccess: checkRule("agent_monitor"),
        },
      ],
    },
    {
      group: t("settings.objectSetting"),
      items: [
        {
          title: t("settings.objectManagement"),
          description: t("settings.objectDescription"),
          url: "/objects-management",
          icon: ObjectSetting,
          isAccess: checkRule("object_management"),
        },
        {
          title: t("objectLayoutField.objectLayoutField"),
          description: t("objectLayoutField.description"),
          url: "/fields-management",
          icon: ObjectFieldSetting,
          isAccess: checkRule("object_layout"),
        },
        {
          title: t("templateRecord.createTemplateRecord"),
          description: t("templateRecord.createTemplateRecord"),
          url: "/create-template-record",
          icon: CreateTemplateRecord,
          isAccess: checkRule("create_record_from_template"),
        },
        {
          title: t("form.formSetting"),
          description: t("form.formDescription"),
          url: "/form-setting",
          icon: FormSetting,
          isAccess: checkRule("internet_form"),
        },
        {
          title: t("relatedObject.relatedObject"),
          description: t("relatedObject.description"),
          url: "/related-objects",
          icon: RelatedObjectSetting,
          isAccess: checkRule("related_object"),
        },
        {
          title: t("picklist.picklist"),
          description: t("picklist.description"),
          url: "/picklist-dependency",
          icon: PicklistSetting,
          isAccess: checkRule("picklist_dependency"),
        },
        {
          title: t("kanbanView.display"),
          description: t("kanbanView.des"),
          url: "/kanban-view-setting",
          icon: KanbanSetting,
          isAccess: checkRule("kanban_view"),
        },
        {
          title: "Knowledge Base",
          description: t("knowledgeBase.des"),
          url: "/knowledge-base-setting",
          icon: KnowledgeBaseSetting,
          isAccess: checkRule("knowledge_view"),
        },
        {
          title: t("highlightSetting.highlightSetting"),
          description: t("highlightSetting.des"),
          url: "/highlight-setting",
          icon: HighlightSetting,
          isAccess: checkRule("highlight_setting"),
        },
        {
          title: t("settings.campaignSetting"),
          description: t("campaignSetting.description"),
          url: "/active-campaign",
          icon: CampaignSetting,
          isAccess: checkRule("campaign"),
        },
        {
          title: t("duplicateRules.duplicateRules"),
          description: t("duplicateRules.des"),
          url: "/duplicate-rules",
          icon: DuplicateRuleSetting,
          isAccess: checkRule("duplicate_rules"),
        },
        {
          title: t("knowledgeBase.enhancement"),
          description: t("knowledgeBase.des"),
          url: "/knowledge-base-enhancement-setting",
          icon: KnowledgeBaseSetting,
          isAccess: checkRule("knowledge_base_enhancement"),
        },
        {
          title: t("Interaction.interactionSetting"),
          description: t("Interaction.decs"),
          url: "/interaction",
          icon: InteractionSetting,
          isAccess: checkRule("interaction_setting"),
        },
        {
          title: t("settings.objectStandard"),
          description: t("settings.objectStandard"),
          url: "/object-standard",
          icon: ObjectStandard,
          isAccess: checkRule("object_standard"),
        },
        {
          title: t("QualityManagement.QualityManagementSettings"),
          description: t("QualityManagement.decs"),
          url: "/quality-management-settings",
          icon: QualityManagementSetting,
          isAccess: checkRule("quality_management"),
        },
      ],
    },
    {
      group: t("settings.auto"),
      items: [
        {
          title: t("settings.workflow"),
          description: t("workflow.description"),
          url: "/workflows",
          icon: WorkflowSetting,
          isAccess: checkRule("workflow"),
        },
        {
          title: t("settings.slaSetting"),
          description: t("slaSetting.description"),
          url: "/sla-setting",
          icon: SLASetting,
          isAccess: checkRule("sla_setting"),
        },
        {
          title: t("settings.dynamicButton"),
          description: t("dynamicButton.description"),
          url: "/dynamic-button",
          icon: DynamicButtonSetting,
          isAccess: checkRule("dynamic_button"),
        },
        {
          title: t("settings.approvalProcesses"),
          description: t("settings.descApprovalProcesses"),
          url: "/approval-processes",
          icon: ApprovalProcessesSetting,
          isAccess: checkRule("approval"),
        },
      ],
    },
    {
      group: t("settings.email"),
      items: [
        {
          title: t("emailTemplate.emailTemplate"),
          description: t("emailTemplate.des"),
          url: "/email-template",
          icon: EmailTemplateSetting,
          isAccess: checkRule("template_email"),
        },
        {
          title: t("emailIncoming.emailIncoming"),
          description: t("emailIncoming.des"),
          url: "/email-incoming",
          icon: EmailIncomingSetting,
          isAccess: checkRule("inbox"),
        },
        {
          title: t("emailOutgoing.emailOutgoing"),
          description: t("emailOutgoing.des"),
          url: "/email-outgoing",
          icon: EmailOutgoingSetting,
          isAccess: checkRule("outbox"),
        },
      ],
    },
    {
      group: "SMS",
      items: [
        {
          title: t("SMSTemplate.smsTemplate"),
          description: t("SMSTemplate.des"),
          url: "/sms-template",
          icon: SMSTemplateSetting,
          isAccess: checkRule("sms_template"),
        },
        {
          title: t("SMSOutgoing.smsOutgoing"),
          description: t("SMSOutgoing.des"),
          url: "/sms-outgoing",
          icon: SMSOutgoingSetting,
          isAccess: checkRule("sms_outgoing"),
        },
      ],
    },
    {
      group: "Consolidated Setting",
      items: [
        {
          title: t("settings.consolidatedViewSetting"),
          description: t("consolidatedView.des"),
          url: "/consolidated-view-settings",
          icon: ConsolidatedViewSetting,
          isAccess: checkRule("consolidate_view"),
        },
      ],
    },
    {
      group: t("settings.integrated"),
      items: [
        {
          title: "OAuth Google",
          description: t("settings.desGoogle"),
          url: "/oauth-client-ids",
          icon: GoogleSetting,
          isAccess: checkRule("oauth2"),
        },
        {
          title: "OAuth Outlook",
          description: t("settings.desOutlook"),
          url: "/oauth-outlook",
          icon: OutlookSetting,
          isAccess: checkRule("o365_integration"),
        },
        {
          title: "CTI Setting",
          description: "CTI Setting",
          url: "/cti-setting",
          icon: CTISetting,
          isAccess: checkRule("cti_setting"),
        },
        {
          title: "Finesse integration",
          description: "Finesse integration",
          url: "/finesse-integration",
          icon: FinesseSetting,
          isAccess: checkRule("finesse_setting"),
        },
        {
          title: "URL popup setting",
          description: "URL popup setting",
          url: "/url-popup-setting",
          icon: URLPopupSetting,
          isAccess: checkRule("url_popup_setting"),
        },
        {
          title: "Voice biometric",
          description: "Voice biometric",
          url: "/voice-biometric",
          icon: VoiceBiometricSetting,
          isAccess: checkRule("voice_biometric"),
        },
        {
          title: "Voice bot setting",
          description: "Voice bot setting",
          url: "/voice-bot-setting",
          icon: VoiceBotSetting,
          isAccess: checkRule("voice_bot"),
        },
        {
          title: "Expose API - Check",
          description: "Expose API - Check",
          url: "/expose-api-check",
          icon: ExposeAPICheckSetting,
          isAccess: checkRule("expose_api_check"),
        },
        {
          title: "Expose API - Create",
          description: "Expose API - Create",
          url: "/expose-api-create",
          icon: ExposeAPICreateSetting,
          isAccess: checkRule("expose_api_create"),
        },
        {
          title: "Expose API - Update",
          description: "Expose API - Update",
          url: "/expose-api-update",
          icon: ExposeAPIUpdateSetting,
          isAccess: checkRule("expose_api_update"),
        },
        {
          title: "Expose API - Delete file",
          description: "Expose API - Delete file",
          url: "/expose-api-delete",
          icon: ExposeAPIDeleteSetting,
          isAccess: checkRule("expose_api_delete_file"),
        },
        {
          title: "Expose API - View file",
          description: "Expose API - View file",
          url: "/expose-api-view",
          icon: ExposeAPIViewSetting,
          isAccess: checkRule("expose_api_view_file"),
        },
        {
          title: "Expose API - Upload file",
          description: "Expose API - Upload file",
          url: "/expose-api-upload",
          icon: ExposeAPIUploadSetting,
          isAccess: checkRule("expose_api_upload_file"),
        },
        {
          title: "External app integration",
          description: "External app integration",
          url: "/external-app-integration",
          icon: ExternalAppIntegrationSetting,
          isAccess: checkRule("external_app_integration"),
        },
        {
          title: "IC integration",
          description: "IC integration",
          url: "/ic-integration",
          icon: ICSetting,
          isAccess: checkRule("ic_integration"),
        },
        // {
        //   title: "Template file",
        //   description: "Template file",
        //   url: "/template-file",
        //   icon: TemplateFileSetting,
        //   isAccess: checkRule("template_file"),
        // },
      ],
    },
    {
      group: t("settings.system"),
      items: [
        {
          title: t("settings.dateTimeSetting"),
          description: t("dateTimeSetting.description"),
          url: "/setting-datetime",
          icon: DateTimeSetting,
          isAccess: checkRule("date_time_setting"),
        },
        {
          title: "LDAP",
          description: t("ldap.ldap"),
          url: "/ldap",
          icon: LDAPSetting,
          isAccess: checkRule("ldap"),
        },
        {
          title: t("systemLog.systemLog"),
          description: t("systemLog.description"),
          url: "/system-log",
          icon: LogSetting,
          isAccess: checkRule("log_system"),
        },
      ],
    },
  ];

  useEffect(() => {
    dispatch(changeTitlePage(t("common.setting")));
    //eslint-disable-next-line
  }, [t]);

  return (
    <Wrapper>
      {settings_menu.map((setting, index) => {
        return (
          <div key={index}>
            {setting.items.find((ele) => ele.isAccess) && (
              <CustomTitle>
                <CustomSettingTitle> {setting.group}</CustomSettingTitle>
                <CustomLine></CustomLine>
              </CustomTitle>
            )}

            <CustomItem empty={!setting.items.find((ele) => ele.isAccess)}>
              <Row gutter={[24, 24]}>
                {setting.items.map((item, idx) => {
                  return (
                    <>
                      {item.isAccess && (
                        <Col id={`ucrm_setting_${index}_${idx}`}>
                          <CustomCol onClick={() => navigate(item.url)}>
                            <img alt="user.icon" src={item.icon} />

                            <div style={{ marginLeft: "21px" }}>
                              <div className="title">{item.title}</div>
                              <div className="decs">{item.description}</div>
                            </div>
                          </CustomCol>
                        </Col>
                      )}
                    </>
                  );
                })}
              </Row>
            </CustomItem>
          </div>
        );
      })}
    </Wrapper>
  );
};

const Wrapper = styled.div`
  overflow: scroll;
  padding: 24px 32px;
  padding-right: 0px;
  background-color: #f9fafc;
`;

const CustomCol = styled.div`
  display: flex;
  align-items: center;
  width: 304px;
  height: 92px;
  background: #fff;
  border-radius: 10px;
  padding: 16px 24px;
  transition: all 0.5s;

  .title {
    color: #000;
    font-size: 16px;
    line-height: 24px;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .decs {
    color: #595959;
    font-size: 14px;
    line-height: 20px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  &:hover {
    cursor: pointer;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  }
`;

const CustomTitle = styled.div`
  display: flex;
  align-items: center;
`;
const CustomSettingTitle = styled.div`
  margin-right: 16px;
  font-style: normal;
  font-family: var(--roboto-500);
  font-size: 22px;
  line-height: 30px;
  min-width: max-content;
  /* identical to box height, or 136% */

  /* Character/Color text main */

  color: #2c2c2c;
`;
const CustomLine = styled.div`
  width: 100%;
  height: 1px;
  background-color: #d9d9d9;
`;

const CustomItem = styled.div`
  padding-right: 24px;
  margin-top: ${({ empty }) => (empty ? "0px" : "32px")};
  margin-bottom: ${({ empty }) => (empty ? "0px" : "32px")};
`;

export default withTranslation()(Settings);
