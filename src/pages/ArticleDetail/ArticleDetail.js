import { Breadcrumb, Col, Row } from "antd";
import Search from "antd/lib/input/Search";
import { useEffect } from "react";
import { useLocation, useNavigate } from "react-router";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import {
  getArticleById,
  getArticleByIdSuccess,
  unmountArticle,
} from "redux/slices/articles";
import {
  getKnowledgeBaseSetting,
  unmountKnowledgeBaseSetting,
} from "redux/slices/knowledgeBaseSetting";
import {
  getListArticle,
  loadMoreArticle,
  unmountKnowledgeBaseView,
  updateSearchArticle,
} from "redux/slices/knowledgeBaseView";
import { getUserName } from "redux/slices/user";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";

const ArticleDetail = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );
  const { editArticle } = useSelector((state) => state.articlesReducer);
  const { listUserName } = useSelector((state) => state.userReducer);
  // eslint-disable-next-line
  const { listArticle, searchArticle, pagination, totalPage, totalRecord } =
    useSelector((state) => state.knowledgeBaseViewReducer);
  // eslint-disable-next-line
  const { page, limit } = pagination;
  const id = query.get("id");
  const object_id = query.get("object_id");
  useEffect(() => {
    dispatch(getUserName());
    dispatch(getKnowledgeBaseSetting());
    if (id) {
      dispatch(
        getArticleById({
          data: {
            id: id,
            object_id: object_id,
          },
          navigate,
        })
      );
    }
    if (searchArticle.length > 0) {
      let dataSearch = [];
      dataSearch.push({
        id_field: editKnowledgeBaseSetting.title,
        value: searchArticle,
      });
      dispatch(
        getListArticle({
          data: {
            object_id: object_id,
            page: 0,
            limit: 16,
            search_with_title: searchArticle,
          },
          pagination: {
            object_id: object_id,
            current_page: page,
            record_per_page: 16,
            search_with: {
              meta: [],
              data: dataSearch,
            },
          },
        })
      );
    }
    // eslint-disable-next-line
  }, [id, object_id]);
  useEffect(
    () => () => {
      dispatch(unmountKnowledgeBaseSetting());
      dispatch(unmountKnowledgeBaseView());
      dispatch(unmountArticle());
      dispatch(updateSearchArticle(""));
    },
    // eslint-disable-next-line
    []
  );

  return (
    <CustomContent>
      <Col span={24} style={{ height: "100%" }}>
        <Breadcrumb>
          <BreadcrumbItem>Articles</BreadcrumbItem>
        </Breadcrumb>
        <Row justify={"space-between"} align="top" style={{ height: "95%" }}>
          <Col
            style={{
              backgroundColor: "#FFFFFF",
              padding: "0.5rem",
              maxHeight: "80%",
              overflow: "hidden scroll",
            }}
            span={6}
            onScroll={(e) => {
              if (
                e.target.scrollTop + e.target.clientHeight >=
                  e.target.scrollHeight - 100 &&
                totalPage - 1 > page
              ) {
                let temp = [];
                if (searchArticle.length > 0) {
                  temp.push({
                    id_field: editKnowledgeBaseSetting.title,
                    value: searchArticle,
                  });
                }
                dispatch(
                  loadMoreArticle({
                    data: {
                      object_id: object_id,
                      page: page + 1,
                      limit: limit,
                      search_with_title: searchArticle,
                    },
                    pagination: {
                      object_id: object_id,
                      current_page: page,
                      record_per_page: 16,
                      search_with: {
                        meta: [],
                        data: temp,
                      },
                      sort_by: {},
                    },
                  })
                );
              }
            }}
          >
            <Search
              placeholder={"Nhập tiêu đề muốn tìm kiếm"}
              value={searchArticle}
              onChange={(e) => {
                dispatch(updateSearchArticle(e.target.value));
                let dataSearch = [];
                if (e.target.value.length > 0) {
                  dataSearch.push({
                    id_field: editKnowledgeBaseSetting.title,
                    value: e.target.value,
                  });
                  dispatch(
                    getListArticle({
                      data: {
                        object_id: object_id,
                        page: 0,
                        limit: 16,
                        search_with_title: e.target.value,
                      },
                      pagination: {
                        object_id: object_id,
                        current_page: page,
                        record_per_page: 16,
                        search_with: {
                          meta: [],
                          data: dataSearch,
                        },
                      },
                    })
                  );
                }
              }}
            />
            {searchArticle.length > 0 && (
              <>
                <p style={{ fontSize: "18px", fontWeight: 700 }}>
                  Có {totalRecord} kết quả tìm kiếm cho "{" "}
                  <span style={{ color: "#20a2a2" }}>{searchArticle}</span> "
                </p>
                <Row>
                  <Col span={24}>
                    {listArticle.length > 0 &&
                      listArticle.map((item) => {
                        return (
                          <Row
                            onClick={() => {
                              dispatch(getArticleByIdSuccess(item));
                            }}
                            style={{
                              padding: "0.5rem",
                              border: "1px solid #ECECEC",
                              borderRadius: "4px",
                            }}
                          >
                            <Col span={24}>{item.title}</Col>
                          </Row>
                        );
                      })}
                  </Col>
                </Row>
              </>
            )}
          </Col>
          <Col
            span={12}
            style={{
              backgroundColor: "#FFFFFF",
              padding: "1rem 2.5rem",
              height: "100%",
              overflow: "scroll",
            }}
          >
            <p style={{ fontSize: "24px", fontWeight: 700 }}>
              {editArticle.title}
            </p>
            <p>{parse(editArticle.body, optionsParse)}</p>
          </Col>
          <Col
            span={5}
            style={{ backgroundColor: "#FFFFFF", padding: "0.5rem" }}
          >
            <p style={{ fontSize: "16px", fontWeight: 700 }}>
              {" "}
              Thông tin chi tiết{" "}
            </p>
            <hr
              style={{
                width: "50%",
                textAlign: "left",
                marginLeft: 0,
                height: "3px",
                borderWidth: 0,
                color: "rgba(32, 162, 162, 1)",
                backgroundColor: "rgba(32, 162, 162, 1)",
              }}
            ></hr>
            <p>
              <TitleDetail> Create by: </TitleDetail>{" "}
              {listUserName[editArticle.created_by]}{" "}
            </p>
            <p>
              <TitleDetail> Create time: </TitleDetail>{" "}
              {editArticle.created_date}{" "}
            </p>
            <p>
              <TitleDetail> Expiration date: </TitleDetail>{" "}
              {editArticle.start_date} - {editArticle.end_date}{" "}
            </p>
          </Col>
        </Row>
      </Col>
    </CustomContent>
  );
};

export default ArticleDetail;

const CustomContent = styled(Row)`
  padding: 1rem;
  height: 100%;
  overflow: hidden;
`;
const TitleDetail = styled.span`
  font-size: 16px;
  font-family: var(--roboto-700);
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
