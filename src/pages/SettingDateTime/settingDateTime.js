import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Collapse from "antd/lib/collapse";
import Button from "antd/lib/button";
import Icon from "components/Icon/Icon";
import BusinessHours from "pages/SLASetting/ServiceType/BusinessHours";
import { Notification } from "components/Notification/Noti";

import {
  loadConfig,
  editConfig,
  loadConfigFail,
} from "redux/slices/datetimeSetting";
import { changeTitlePage } from "redux/slices/authenticated";

const { Panel } = Collapse;

function SettingDateTime(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.dateTimeSetting")));
    //eslint-disable-next-line
  }, [t]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);

  const [dataBusinessHours, $dataBusinessHours] = useState({
    holidays: [],
    shifts: [],
    workingDays: [],
  });

  const [previewDate, setPreviewDate] = useState({
    label: "",
    value: "",
  });

  const [previewTime, setPreviewTime] = useState({
    label: "",
    value: "",
  });

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
  ];

  const optionsTime = [
    {
      label: "HH:mm:ss",
      value: "%H:%M:%S",
    },
    {
      label: "HH:mm",
      value: "%H:%M",
    },
  ];

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "date_time_setting" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handlePreviewDate = (data) => {
    switch (data) {
      case "%Y-%m-%d":
        setPreviewDate({
          label: "2022-12-30",
          value: "%Y-%m-%d",
        });
        break;
      case "%Y/%m/%d":
        setPreviewDate({
          label: "2022/12/30",
          value: "%Y/%m/%d",
        });
        break;
      case "%Y %m %d":
        setPreviewDate({
          label: "2022 12 30",
          value: "%Y %m %d",
        });
        break;
      case "%m-%d-%Y":
        setPreviewDate({
          label: "12-30-2022",
          value: "%m-%d-%Y",
        });
        break;
      case "%m/%d/%Y":
        setPreviewDate({
          label: "12/30/2022",
          value: "%m/%d/%Y",
        });
        break;
      case "%m %d %Y":
        setPreviewDate({
          label: "12 30 2022",
          value: "%m %d %Y",
        });
        break;
      case "%d-%m-%Y":
        setPreviewDate({
          label: "30-12-2022",
          value: "%d-%m-%Y",
        });
        break;
      case "%d/%m/%Y":
        setPreviewDate({
          label: "30/12/2022",
          value: "%d/%m/%Y",
        });
        break;
      case "%d %m %Y":
        setPreviewDate("30 12 2022");
        setPreviewDate({
          label: "30 12 2022",
          value: "%d %m %Y",
        });
        break;

      default:
        break;
    }
  };

  const handlePreviewTime = (data) => {
    switch (data) {
      case "%H:%M:%S":
        setPreviewTime({
          label: "12:60:60",
          value: "%H:%M:%S",
        });
        break;
      case "%H:%M":
        setPreviewTime({
          label: "12:60",
          value: "%H:%M",
        });
        break;

      default:
        break;
    }
  };

  const handleChangeFormat = (e, type) => {
    if (type === "date") {
      handlePreviewDate(e);
    } else {
      handlePreviewTime(e);
    }
  };

  const _onSubmit = (values) => {
    if (
      dataBusinessHours.workingDays.length === 0 ||
      (dataBusinessHours.workingDays.length > 0 &&
        dataBusinessHours.shifts.length > 0 &&
        dataBusinessHours.shifts.every((shift) => shift.time.length !== 0))
    ) {
      let resultShifts = [];
      dataBusinessHours.shifts.forEach((shift) => {
        let applicable_weekdays = [
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
        ];

        shift.day.forEach((item) => {
          applicable_weekdays[item] = "True";
        });
        resultShifts.push({
          from_time: {
            hour: parseInt(shift.time[0].slice(0, 2)),
            minute: parseInt(shift.time[0].slice(3, 5)),
          },
          to_time: {
            hour: parseInt(shift.time[1].slice(0, 2)),
            minute: parseInt(shift.time[1].slice(3, 5)),
          },
          applicable_weekdays: [...applicable_weekdays],
        });
      });

      dispatch(
        editConfig({
          tenant_date_format: previewDate.value,
          tenant_datetime_format: previewDate.value + " " + previewTime.value,
          working_days: _.get(values, "working_day", []),
          holidays: _.get(dataBusinessHours, "holidays", []),
          shift_work: resultShifts || [],
        })
      );
    } else {
      Notification("warning", "Please full fields shift work!");
    }
  };

  useEffect(() => {
    dispatch(loadConfig());
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(allConfig).length > 0) {
      form.setFieldsValue({
        date_format: allConfig.tenant_date_format,
        time_format: allConfig.tenant_datetime_format.slice(9),
        working_day: _.get(allConfig, "working_days", []) || [],
      });

      handlePreviewDate(allConfig.tenant_date_format);
      handlePreviewTime(allConfig.tenant_datetime_format.slice(9));
      $dataBusinessHours({
        holidays: _.get(allConfig, "holidays", []) || [],
        shifts: _.get(allConfig, "shift_work", []) || [],
        workingDays: _.get(allConfig, "working_days", []) || [],
      });
    }
  }, [allConfig, form]);

  useEffect(() => {
    return () => {
      dispatch(loadConfigFail());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.dateTimeSetting")}</BreadcrumbItem>
      </Breadcrumb>

      <Config>
        <div
          style={{
            fontSize: "18px",
            fontFamily: "var(--roboto-500)",
          }}
        >
          {t("settings.dateTimeSetting")}
        </div>
        <Note>{t("dateTimeSetting.description2")}</Note>
        <Form
          form={form}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
          onFinish={_onSubmit}
        >
          <CollapseCustom
            defaultActiveKey="date_time_format"
            expandIconPosition="end"
            size="large"
          >
            <Panel
              key="date_time_format"
              header={
                <div style={{ display: "flex", columnGap: "8px" }}>
                  <Icon icon="time" size={24} />
                  <span
                    style={{
                      fontSize: "16px",
                      fontFamily: "var(--roboto-700)",
                    }}
                  >
                    Date time format
                  </span>
                </div>
              }
            >
              <Form.Item
                label={t("dateTimeSetting.dateFormat")}
                name="date_format"
              >
                <Select
                  disabled={!checkRule("edit")}
                  options={optionsDate}
                  onSelect={(e) => handleChangeFormat(e, "date")}
                />
              </Form.Item>

              <Form.Item
                label={t("dateTimeSetting.timeFormat")}
                name="time_format"
              >
                <Select
                  disabled={!checkRule("edit")}
                  options={optionsTime}
                  onSelect={(e) => handleChangeFormat(e, "time")}
                />
              </Form.Item>

              <Preview>
                <p>{t("dateTimeSetting.preview")}: &nbsp;</p>
                <span>
                  {previewDate.label} {previewTime.label}
                </span>
              </Preview>
            </Panel>
          </CollapseCustom>

          <BusinessHours
            form={form}
            dataBusinessHours={dataBusinessHours}
            $dataBusinessHours={$dataBusinessHours}
            required={false}
          />

          <WrapButton>
            <Button type="primary" htmlType="submit">
              {t("common.save")}
            </Button>
            {/* <Button>{t("common.cancel")}</Button> */}
          </WrapButton>
        </Form>
      </Config>
    </Wrapper>
  );
}

export default withTranslation()(SettingDateTime);

const Wrapper = styled.div`
  padding: 16px 24px;
  background: #f9fafc;
  height: 100%;
  overflow: auto;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-form {
    width: 100%;
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-500);
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CollapseCustom = styled(Collapse)`
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  border: 1px solid #ececec !important;
  border-bottom: 0 !important;
  border-radius: 8px !important;
  background-color: #fff !important;
  margin-bottom: 16px;

  .ant-collapse-item {
    border-bottom: 1px solid #ececec !important;
    border-radius: 0 0 8px 8px !important;
  }

  .ant-collapse-content {
    border-top: 1px solid #ececec !important;
    border-radius: 0 0 8px 8px !important;
  }

  .ant-form-item {
    margin-bottom: 16px;
  }
`;

const Config = styled.div`
  width: 638px;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
`;

const Note = styled.span`
  font-size: 16px;
  font-family: var(--roboto-400-i);
  color: #8c8c8c;
  margin-bottom: 24px;
`;

const Preview = styled.div`
  font-size: 16px;
  color: #2c2c2c;
  display: flex;

  p {
    margin-bottom: 0;
    color: #919eab;
  }

  span {
    font-family: var(--roboto-500);
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;
    width: 121px;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    /* margin-right: 16px; */
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
