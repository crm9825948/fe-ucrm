import { Select, Row, Col, Form, Breadcrumb } from "antd";
import ThemeButton from "components/CustomThemes/ThemeButton";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { unmountKanbanViewSetting } from "redux/slices/kanbanViewSetting";
import {
  deleteKnowledgeBaseSetting,
  getKnowledgeBaseSetting,
  updateFieldKnowledgeBaseSetting,
  updateKnowledgeBaseSetting,
} from "redux/slices/knowledgeBaseSetting";
import {
  loadListObjectField,
  loadListObjectFieldSuccess,
} from "redux/slices/objects";
import { getListObject } from "redux/slices/objectsManagement";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
import _ from "lodash";

const { Option } = Select;
const KnowledgeBaseSetting = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const { listObject } = useSelector((state) => state.objectsManagementReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );
  const {
    body,
    category_name,
    object_id,
    section_name,
    title,
    start_date,
    end_date,
  } = editKnowledgeBaseSetting;
  const [arr, setArr] = useState(["", "", "", ""]);
  const [listOption, setListOption] = useState([]);
  const [dataDelete, setDataDelete] = useState({});

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "knowledge_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const submit = () => {
    dispatch(updateKnowledgeBaseSetting(editKnowledgeBaseSetting));
  };
  const deleteSetting = () => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: editKnowledgeBaseSetting._id,
    });
  };
  useEffect(() => {
    dispatch(getListObject());
    dispatch(getKnowledgeBaseSetting());

    // eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (object_id) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object_id,
        })
      );
      setArr(["", "", "", ""]);
    }
    // eslint-disable-next-line
  }, [object_id]);

  useEffect(() => {
    return () => {
      dispatch(loadListObjectFieldSuccess([]));
    };
  }, [dispatch]);

  useEffect(() => {
    if (
      listObjectField.length > 0 &&
      window.location.pathname.includes("knowledge-base-setting")
    ) {
      let arr = [];
      let count = 0;
      // eslint-disable-next-line
      listObjectField.map((object) => {
        // eslint-disable-next-line
        object.main_object &&
          object.main_object.sections.map((section) => {
            // eslint-disable-next-line
            return section.fields.map((field) => {
              if (!field.hidden) {
                if (field.required && field.type !== "id") {
                  count += 1;
                }
                arr.push(field);
              }
            });
          });
      });

      if (count < 4) {
        Notification(
          "error",
          "Vui lòng chọn đối tượng có tối thiểu 4 trường bắt buộc"
        );
        dispatch(
          updateFieldKnowledgeBaseSetting({
            key: "object_id",
            value: "",
          })
        );
      } else {
        setListOption(arr);
      }
    }
    // eslint-disable-next-line
  }, [listObjectField]);

  // unmount
  useEffect(
    () => () => {
      dispatch(unmountKanbanViewSetting());
      setArr(["", "", "", ""]);
      setListOption([]);
      form.resetFields();
    },
    // eslint-disable-next-line
    []
  );

  return (
    <>
      <Row style={{ padding: "1rem 1rem 0rem 1rem" }}>
        <Col>
          <Breadcrumb>
            <Breadcrumb.Item onClick={() => navigate("/settings")}>
              {t("common.settings")}
            </Breadcrumb.Item>
            <BreadcrumbItem>
              {" "}
              {t("knowledgeBase.knowledgeBase")}{" "}
            </BreadcrumbItem>
          </Breadcrumb>
        </Col>
      </Row>
      <CustomContent>
        <Form
          labelCol={{ span: 4 }}
          form={form}
          wrapperCol={{ span: 24 }}
          onFinish={submit}
          name="knowledge-base"
        >
          <Row>
            <CustomCol
              span={object_id ? 4 : 24}
              style={{ marginRight: "1rem" }}
            >
              <p> {t("knowledgeBase.selectObject")} </p>
              <Form.Item
                name={"object_id"}
                valuePropName={object_id}
                rules={[
                  {
                    validator: (rule, value = object_id, cb) => {
                      object_id.length === 0
                        ? cb("Trường này là bắt buộc")
                        : cb();
                    },
                    required: true,
                  },
                ]}
              >
                <Select
                  onChange={(e) => {
                    dispatch(
                      updateFieldKnowledgeBaseSetting({
                        key: "object_id",
                        value: e,
                      })
                    );
                  }}
                  disabled={checkRule("create") === false ? true : false}
                  value={object_id}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {
                    // eslint-disable-next-line
                    listObject &&
                      listObject.map((item) => {
                        if (item.Status) {
                          return <Option value={item._id}>{item.Name}</Option>;
                        } else {
                          return null;
                        }
                      })
                  }
                </Select>
              </Form.Item>
            </CustomCol>
            {object_id && (
              <CustomCol span={19}>
                <Row justify="space-between">
                  <Col span={11}>
                    <Form.Item
                      label={t("knowledgeBase.section")}
                      name={"section_name"}
                      valuePropName={section_name}
                      rules={[
                        {
                          validator: (rule, value = section_name, cb) => {
                            section_name.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "section_name",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[0] = e;
                          } else {
                            temp[0] = "";
                          }
                          setArr(temp);
                        }}
                        value={section_name}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (field.target && field.target.length > 0) {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={11}>
                    <Form.Item
                      label={t("knowledgeBase.category")}
                      name={"category_name"}
                      valuePropName={category_name}
                      rules={[
                        {
                          validator: (rule, value = category_name, cb) => {
                            category_name.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "category_name",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[1] = e;
                          } else {
                            temp[1] = "";
                          }
                          setArr(temp);
                        }}
                        value={category_name}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (field.source === section_name) {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row justify="space-between">
                  <Col span={11}>
                    <Form.Item
                      label={t("knowledgeBase.title")}
                      name={"title"}
                      valuePropName={title}
                      rules={[
                        {
                          validator: (rule, value = title, cb) => {
                            title.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        value={title}
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "title",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[2] = e;
                          } else {
                            temp[2] = "";
                          }
                          setArr(temp);
                        }}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (
                              field.type === "text" ||
                              field.type === "textarea"
                            ) {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={11}>
                    <Form.Item
                      label={t("knowledgeBase.body")}
                      name={"body"}
                      valuePropName={body}
                      rules={[
                        {
                          validator: (rule, value = body, cb) => {
                            body.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        value={body}
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "body",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[3] = e;
                          } else {
                            temp[3] = "";
                          }
                          setArr(temp);
                        }}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (
                              field.type === "text" ||
                              field.type === "textarea"
                            ) {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row justify={"space-between"}>
                  <Col span={11}>
                    <Form.Item
                      name={"start_date"}
                      label={t("knowledgeBase.startDate")}
                      valuePropName={start_date}
                      rules={[
                        {
                          validator: (rule, value = start_date, cb) => {
                            start_date.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        value={start_date}
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "start_date",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[2] = e;
                          } else {
                            temp[2] = "";
                          }
                          setArr(temp);
                        }}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (field.type === "date") {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                  <Col span={11}>
                    <Form.Item
                      name={"end_date"}
                      label={t("knowledgeBase.endDate")}
                      valuePropName={end_date}
                      rules={[
                        {
                          validator: (rule, value = end_date, cb) => {
                            end_date.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        value={end_date}
                        onChange={(e) => {
                          dispatch(
                            updateFieldKnowledgeBaseSetting({
                              key: "end_date",
                              value: e,
                            })
                          );
                          let temp = [...arr];
                          if (e) {
                            temp[3] = e;
                          } else {
                            temp[3] = "";
                          }
                          setArr(temp);
                        }}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {
                          // eslint-disable-next-line
                          listOption.map((field) => {
                            if (field.type === "date") {
                              return (
                                <Option
                                  disabled={
                                    arr.indexOf(field.field_id) > -1
                                      ? true
                                      : false
                                  }
                                  value={field.field_id}
                                >
                                  {field.name}
                                </Option>
                              );
                            }
                          })
                        }
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row justify={"end"}>
                  {/* <Col span={2} style={{ marginRight: "1rem" }}> */}
                  {checkRule("edit") && checkRule("create") ? (
                    <ThemeButton
                      label={t("common.save")}
                      htmlType={"submit"}
                      style={{ marginRight: "16px" }}
                    />
                  ) : (
                    ""
                  )}
                  {/* </Col> */}
                  {editKnowledgeBaseSetting.object_id === object_id &&
                    checkRule("delete") && (
                      // <Col span={2}>
                      <ThemeButton
                        label={t("common.delete")}
                        clickButton={deleteSetting}
                      />
                      // </Col>
                    )}
                </Row>
              </CustomCol>
            )}
          </Row>
        </Form>
        <ModalConfimDelete
          title={"cấu hình knowledge base này"}
          decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
          methodDelete={deleteKnowledgeBaseSetting}
          dataDelete={dataDelete}
          isLoading={showLoadingScreen}
        />
      </CustomContent>
    </>
  );
};

export default KnowledgeBaseSetting;

const CustomContent = styled.div`
  padding: 1rem;
`;

const CustomCol = styled(Col)`
  background-color: white;
  padding: 1rem;
  .ant-select {
    width: 100%;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
