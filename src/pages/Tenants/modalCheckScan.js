import { useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";

import { setShowModalScan, getLogIntervalSuccess } from "redux/slices/tenants";

function ModalCheckScan() {
  const dispatch = useDispatch();
  const { showModalScan, logInterval } = useSelector(
    (state) => state.tenantsReducer
  );

  const [form] = Form.useForm();

  const _onCancel = () => {
    form.resetFields();
    dispatch(setShowModalScan(false));
    dispatch(getLogIntervalSuccess({}));
  };

  useEffect(() => {
    if (Object.keys(logInterval).length > 0) {
      form.setFieldsValue({
        last_scan: logInterval.last_scan,
        status: logInterval.status,
      });
    }
  }, [form, logInterval]);

  return (
    <ModalCustom
      title={"Check Scan"}
      visible={showModalScan}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item label="Last Scan" name="last_scan">
          <Input disabled />
        </Form.Item>

        <Form.Item label="Status" name="status">
          <Input disabled />
        </Form.Item>

        <WrapButton label=" ">
          <Button onClick={_onCancel}>Ok</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalCheckScan;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-input[disabled] {
    color: #000;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;
