import { SearchOutlined } from "@ant-design/icons";
import _ from "lodash";
import {
  Button,
  Dropdown,
  Form,
  Input,
  InputNumber,
  Pagination,
  Popconfirm,
  Space,
  Switch,
  Table,
  Typography,
} from "antd";
import copyImg from "assets/icons/common/copy.png";
import deleteImg from "assets/icons/common/delete.png";
import Play from "assets/icons/common/Play.svg";
import editImg from "assets/icons/common/edit.png";
import searchImg from "assets/icons/common/searchManagement.png";
import Index from "assets/icons/common/Index.svg";
import Lock from "assets/icons/common/lockClone.svg";
import UnLock from "assets/icons/common/unlockClone.svg";
// import lockImg from "assets/icons/common/lock.png";
// import unlockImg from "assets/icons/common/unlock.png";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import React, { useEffect, useState } from "react";
import { CSVLink } from "react-csv";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
// import { logout } from "redux/slices/authenticated";
import { setShowLoadingScreen } from "redux/slices/global";
import {
  // disableTenant,
  getLogInterval,
  getLogCelery,
  loadData,
  loadDataToExport,
  healthCheckInteraction,
  loadPackage,
  runIndex,
  allowCloneTenant,
  updateStatusTenant,
  // deleteTenant,
} from "redux/slices/tenants";
// import { loadUserDetailSuccess } from "redux/slices/user";
import AuthService from "services/AuthService";
import styled from "styled-components";
import ModalCheckScan from "./modalCheckScan";
import ModalCheckCelery from "./modalCheckCelery";
import ModalConfimDelete from "./ModalConfirmDelete";
import ModalTenant from "./modalTenant";
import { EllipsisOutlined, LockOutlined } from "@ant-design/icons";
import ModalMedia from "./modalMedia";

const { Text: TextComponent } = Typography;

function numberWithCommas(x) {
  if (x === 0) {
    return "0";
  } else return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const Tenants = () => {
  const dispatch = useDispatch();
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearcherColumn] = useState("");
  const [editingKey, setEditingKey] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [clone, setClone] = useState(false);
  const [email, setEmail] = useState("");
  const [update, setUpdate] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [recordClone, $recordClone] = useState("");
  let navigate = useNavigate();
  const [, setModalSignout] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [isDelete, setIsDelete] = useState(false);
  // let navigate = useNavigate();

  const {
    tenants,
    // isLoadingCreate,
    // total_page,
    total_record,
    isLoading,
    isLoadingDisable,
    dataExport,
  } = useSelector((state) => state.tenantsReducer);
  const [form] = Form.useForm();
  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoading));
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    if (isLoadingDisable === false) {
      setIsModalVisible(false);
    }
  }, [isLoadingDisable]);

  useEffect(() => {
    dispatch(loadPackage({}));
  }, [dispatch]);

  const cancel = () => {
    setEditingKey("");
    // form.resetFields();
  };

  const [searchField, setSearchField] = useState([
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
    "",
  ]);
  const { userDetail } = useSelector((state) => state.userReducer);
  const [open, setOpen] = useState("");
  useEffect(() => {
    if (userDetail.user_root)
      dispatch(
        loadData({
          Record_per_page: recordPerPage,
          Current_page: currentPage,
          Query_field: [
            {
              _id: searchField[0],
              Type: "String",
            },
            {
              Name: searchField[1],
              Type: "String",
            },
            {
              First_Name: searchField[2],
              Type: "String",
            },
            {
              Middle_Name: searchField[3],
              Type: "String",
            },
            {
              Last_Name: searchField[4],
              Type: "String",
            },
            {
              Description: searchField[5],
              Type: "String",
            },
            {
              Number_Of_Users: searchField[6],
              Type: "Num",
            },
            {
              Email: searchField[7],
              Type: "String",
            },
            {
              Created_Date: searchField[8],
              Type: "String",
            },
            {
              Created_By: searchField[9],
              Type: "String",
            },
            {
              Modify_Time: searchField[10],
              Type: "String",
            },
            {
              Modify_By: searchField[11],
              Type: "String",
            },
            {
              URL: searchField[12],
              Type: "String",
            },
            {
              License: searchField[13],
              Type: "String",
            },
            {
              Expire_Day: searchField[14],
              Type: "String",
            },
          ],
        })
      );
  }, [dispatch, currentPage, recordPerPage, searchField, userDetail]);

  useEffect(() => {
    if (userDetail.user_root)
      dispatch(
        loadDataToExport({
          Record_per_page: 99999,
          Current_page: 1,
          Query_field: [],
        })
      );
  }, [dispatch, userDetail]);

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearcherColumn(dataIndex);
  };

  // useEffect(() => {
  //   if (Object.keys(userDetail).length > 0) {
  //     let result = userDetail.user_root === undefined;
  //     if (result === true) {
  //       // navigate("/404");
  //       window.open(`${FE_URL}/404`, "_self");
  //     }
  //   }
  // }, [userDetail, navigate]);

  const getColumnSearchProps = (dataIndex, idx) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        {dataIndex === "Number_Of_Users" ? (
          <InputNumber
            step={1}
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            style={{ marginBottom: 8, display: "block", width: "100%" }}
            formatter={(value) =>
              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }
            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
          />
        ) : (
          <Input
            placeholder={`Search ${dataIndex}`}
            value={selectedKeys[0]}
            onChange={(e) =>
              setSelectedKeys(e.target.value ? [e.target.value] : [])
            }
            onPressEnter={() => {
              let tmp = [...searchField];
              tmp[idx] = selectedKeys.length === 0 ? "" : selectedKeys[0];
              if (tmp[idx] === "") {
                setSearchField(tmp);
                clearFilters();
                handleSearch("", confirm, dataIndex);
              } else {
                setSearchField(tmp);
                handleSearch(selectedKeys, confirm, dataIndex);
              }
            }}
            style={{ marginBottom: 8, display: "block" }}
          />
        )}

        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => {
              let tmp = [...searchField];
              tmp[idx] = selectedKeys.length === 0 ? "" : selectedKeys[0];
              if (tmp[idx] === "") {
                setSearchField(tmp);
                clearFilters();
                handleSearch("", confirm, dataIndex);
              } else {
                setSearchField(tmp);
                handleSearch(selectedKeys, confirm, dataIndex);
              }
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </CustomButtonSave>
          <CustomButtonCancel
            // onClick={() => handleReset(clearFilters)}
            onClick={() => {
              let tmp = [...searchField];
              tmp[idx] = "";
              setSearchField(tmp);
              clearFilters();
              handleSearch("", confirm, dataIndex);
            }}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </CustomButtonCancel>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        <TextComponent
          ellipsis={{ tooltip: text }}
          style={{
            width: "300px",
            textAlign: `${typeof text === "number" ? "right" : ""}`,
          }}
        >
          {typeof text === "number" ? numberWithCommas(text) : text}
          {/* </div> */}
        </TextComponent>
      ),
  });

  const [recordID, setRecordID] = useState("");

  // const edit = (record) => {
  //   setRecordID(record._id);
  //   form.setFieldsValue(record);
  //   setIsModalVisible(true);
  // };
  const renderMenu = (record) => {
    return (
      <WrapAction>
        <ItemAction
          onClick={() => {
            dispatch(
              allowCloneTenant({
                data: {
                  tenant_id: record._id,
                },
                load: {
                  Record_per_page: recordPerPage,
                  Current_page: currentPage,
                  Query_field: [
                    {
                      _id: searchField[0],
                      Type: "String",
                    },
                    {
                      Name: searchField[1],
                      Type: "String",
                    },
                    {
                      First_Name: searchField[2],
                      Type: "String",
                    },
                    {
                      Middle_Name: searchField[3],
                      Type: "String",
                    },
                    {
                      Last_Name: searchField[4],
                      Type: "String",
                    },
                    {
                      Description: searchField[5],
                      Type: "String",
                    },
                    {
                      Number_Of_Users: searchField[6],
                      Type: "Num",
                    },
                    {
                      Email: searchField[7],
                      Type: "String",
                    },
                    {
                      Created_Date: searchField[8],
                      Type: "String",
                    },
                    {
                      Created_By: searchField[9],
                      Type: "String",
                    },
                    {
                      Modify_Time: searchField[10],
                      Type: "String",
                    },
                    {
                      Modify_By: searchField[11],
                      Type: "String",
                    },
                    {
                      URL: searchField[12],
                      Type: "String",
                    },
                    {
                      License: searchField[13],
                      Type: "String",
                    },
                    {
                      Expire_Day: searchField[14],
                      Type: "String",
                    },
                  ],
                },
              })
            );
            setOpen("");
          }}
        >
          <img src={record.Allow_Clone ? Lock : UnLock} alt="index" /> Allow
          clone tenant
        </ItemAction>
        <ItemAction
          onClick={() => {
            dispatch(
              runIndex({
                tenant_id: record?._id,
              })
            );
            setOpen("");
          }}
        >
          <img src={Index} alt="index" /> Re index
        </ItemAction>
        <Popconfirm
          title="Check interaction?"
          onCancel={() => {
            setOpen("");
          }}
          onConfirm={() => {
            dispatch(
              healthCheckInteraction({
                tenant_id: record._id,
              })
            );
            setOpen("");
          }}
        >
          {userDetail &&
            userDetail.permission.create_tenant &&
            userDetail &&
            userDetail.permission.update_tenant && (
              <ItemAction>
                <img alt="" src={Play} />
                Check interaction
              </ItemAction>
            )}
        </Popconfirm>
        <ItemAction
          onClick={() => {
            navigate(`/${record._id}/search-management`);
          }}
        >
          <img src={searchImg} alt="Search" /> Global search
        </ItemAction>
        {userDetail &&
          userDetail.permission.create_tenant &&
          userDetail &&
          userDetail.permission.update_tenant &&
          record.Allow_Clone && (
            <ItemAction
              onClick={() => {
                setClone(true);
                setEmail(record.Email);
                let tempRecord = {
                  ...record,
                  ..._.get(record, "Default_Email_Config", {}),
                  password: undefined,
                };
                tempRecord.Name = "(Copy) " + record.Name;
                tempRecord.Email = "";
                form.setFieldsValue(tempRecord);
                setIsModalVisible(true);
                $recordClone(record._id);
                setOpen("");
              }}
            >
              <img alt="" src={copyImg} /> Clone tenant
            </ItemAction>
          )}
        <ItemAction
          onClick={() => {
            setRecordID(record._id);
            setShowModal(true);
            setOpen("");
          }}
        >
          <LockOutlined style={{ fontSize: "20px", marginRight: "8px" }} />
          Protect media
        </ItemAction>
      </WrapAction>
    );
  };
  const columns = [
    {
      title: "Status",
      dataIndex: "Status",
      key: "Status",
      width: "100px",
      render: (value, record) => (
        <>
          <Switch
            checkedChildren="ON"
            unCheckedChildren="OFF"
            checked={value}
            onChange={() =>
              dispatch(
                updateStatusTenant({
                  data: { tenant_id: record._id },
                  load: {
                    Record_per_page: recordPerPage,
                    Current_page: currentPage,
                    Query_field: [
                      {
                        _id: searchField[0],
                        Type: "String",
                      },
                      {
                        Name: searchField[1],
                        Type: "String",
                      },
                      {
                        First_Name: searchField[2],
                        Type: "String",
                      },
                      {
                        Middle_Name: searchField[3],
                        Type: "String",
                      },
                      {
                        Last_Name: searchField[4],
                        Type: "String",
                      },
                      {
                        Description: searchField[5],
                        Type: "String",
                      },
                      {
                        Number_Of_Users: searchField[6],
                        Type: "Num",
                      },
                      {
                        Email: searchField[7],
                        Type: "String",
                      },
                      {
                        Created_Date: searchField[8],
                        Type: "String",
                      },
                      {
                        Created_By: searchField[9],
                        Type: "String",
                      },
                      {
                        Modify_Time: searchField[10],
                        Type: "String",
                      },
                      {
                        Modify_By: searchField[11],
                        Type: "String",
                      },
                      {
                        URL: searchField[12],
                        Type: "String",
                      },
                      {
                        License: searchField[13],
                        Type: "String",
                      },
                      {
                        Expire_Day: searchField[14],
                        Type: "String",
                      },
                    ],
                  },
                })
              )
            }
          />
        </>
      ),
    },
    {
      title: "ID",
      dataIndex: "_id",
      key: "_id",
      width: "300px",
      ...getColumnSearchProps("_id", 0),
    },
    {
      title: "Name",
      dataIndex: "Name",
      key: "Name",
      width: "300px",
      ...getColumnSearchProps("Name", 1),
    },
    {
      title: "First_Name",
      dataIndex: "First_Name",
      key: "First_Name",
      width: "300px",
      ...getColumnSearchProps("First_Name", 2),
    },
    {
      title: "Middle_Name",
      dataIndex: "Middle_Name",
      key: "Middle_Name",
      width: "300px",
      ...getColumnSearchProps("Middle_Name", 3),
    },
    {
      title: "Last_Name",
      dataIndex: "Last_Name",
      key: "Last_Name",
      width: "300px",
      ...getColumnSearchProps("Last_Name", 4),
    },

    {
      title: "Description",
      dataIndex: "Description",
      key: "Description",
      width: "300px",
      ...getColumnSearchProps("Description", 5),
    },
    {
      title: "Number_Of_Users",
      dataIndex: "Number_Of_Users",
      key: "Number_Of_Users",
      width: "300px",
      ...getColumnSearchProps("Number_Of_Users", 6),
    },
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      width: "300px",
      ...getColumnSearchProps("Email", 7),
    },
    {
      title: "Created_Date",
      dataIndex: "Created_Date",
      key: "Created_Date",
      width: "300px",
      ...getColumnSearchProps("Created_Date", 8),
    },
    {
      title: "Created_By",
      dataIndex: "Created_By",
      key: "Created_By",
      width: "300px",
      ...getColumnSearchProps("Created_By", 9),
    },
    {
      title: "Modify_Time",
      dataIndex: "Modify_Time",
      key: "Modify_Time",
      width: "300px",
      ...getColumnSearchProps("Modify_Time", 10),
    },
    {
      title: "Modify_By",
      dataIndex: "Modify_By",
      key: "Modify_By",
      width: "300px",
      ...getColumnSearchProps("Modify_By", 11),
    },
    {
      title: "URL",
      dataIndex: "URL",
      key: "URL",
      width: "300px",
      ...getColumnSearchProps("URL", 12),
    },
    {
      title: "License",
      dataIndex: "License",
      key: "License",
      width: "300px",
      ...getColumnSearchProps("License", 13),
    },
    {
      title: "Expire_Day",
      dataIndex: "Expire_Day",
      key: "Expire_Day",
      width: "300px",
      ...getColumnSearchProps("Expire_Day", 14),
    },
    {
      title: "Action",
      key: "action",
      fixed: "right",
      width: 100,
      render: (text, record) => {
        return editingKey === record.key ? (
          <span>
            <Typography.Link
              onClick={() => {}}
              style={{
                marginRight: 8,
              }}
            >
              Save
            </Typography.Link>
            <Popconfirm title="Sure to cancel?" onConfirm={cancel}>
              <a href>Cancel</a>
            </Popconfirm>
          </span>
        ) : (
          <Typography.Link disabled={editingKey !== ""}>
            <Space
              size="middle"
              style={{ pointerEvents: `${editingKey !== "" ? "none" : ""}` }}
            >
              {userDetail && userDetail.permission.update_tenant ? (
                <img
                  alt=""
                  src={editImg}
                  style={{ color: "grey", width: "18px" }}
                  onClick={() => {
                    setUpdate(true);
                    setEmail(record.Email);
                    let tempRecord = {
                      ...record,
                      ..._.get(record, "Default_Email_Config", {}),
                      password: undefined,
                    };
                    tempRecord["Number_Of_Users"] = parseInt(
                      record.Number_Of_Users.split("/")[1]
                    );
                    form.setFieldsValue(tempRecord);
                    setIsModalVisible(true);
                    setRecordID(record._id);
                  }}
                />
              ) : (
                ""
              )}
              {!record.Status &&
              userDetail &&
              userDetail.permission.create_tenant &&
              userDetail &&
              userDetail.permission.update_tenant ? (
                <img
                  onClick={() => {
                    setEmail(record.Email);
                    let tempRecord = {
                      ...record,
                      ..._.get(record, "Default_Email_Config", {}),
                      password: undefined,
                    };
                    tempRecord["Number_Of_Users"] = parseInt(
                      record.Number_Of_Users.split("/")[1]
                    );
                    form.setFieldsValue(tempRecord);
                    setIsModalVisible(true);
                    setRecordID(record._id);
                    setIsDelete(true);
                  }}
                  alt=""
                  src={deleteImg}
                  style={{ color: "grey", width: "20px" }}
                />
              ) : (
                ""
              )}

              <CustomDropdown
                visible={open === record._id}
                overlay={() => renderMenu(record)}
                placement="bottom"
                trigger="click"
                overlayClassName="object-view"
                onVisibleChange={() => {
                  if (open) {
                    setOpen("");
                  }
                }}
              >
                <EllipsisOutlined
                  style={{
                    paddingTop: "8px",
                  }}
                  onClick={() => {
                    setOpen(record._id);
                  }}
                />
              </CustomDropdown>
            </Space>
          </Typography.Link>
        );
      },
    },
  ];

  const [dataSource, setDataSource] = useState([]);

  const _onCheckScan = () => {
    dispatch(getLogInterval());
  };

  const _onCheckCelery = () => {
    dispatch(getLogCelery());
  };

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    Object.entries(tenants).forEach(([, value]) => {
      arr.push(value);
    });
    setDataSource(arr);
  }, [tenants]);

  useEffect(() => {
    if (AuthService) {
      AuthService.eventSignOut(() => {
        setModalSignout(true);
        localStorage.removeItem("setting_accessToken");
        localStorage.removeItem("setting_refreshToken");
        localStorage.removeItem("themeColor");
        localStorage.removeItem("i18nextLng");
        localStorage.removeItem("redux-root");
        localStorage.removeItem("settings");

        //IC
        localStorage.removeItem("icIntegration_accessToken");
        localStorage.removeItem("icIntegration_tenantID");
        localStorage.removeItem("icIntegration_username");
        localStorage.removeItem("icIntegration_is_administrator");
        localStorage.removeItem("icIntegration_link_api");
        localStorage.removeItem("icIntegration_refreshToken");
        localStorage.removeItem("icIntegration_current_connectionID");
        localStorage.removeItem("icIntegration_useIC");
        // dispatch(logout())
        // navigate('/login')
      });
    }
  }, []);

  return (
    <Wrapper>
      <div className="header">
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          {userDetail &&
          userDetail.permission &&
          userDetail.permission.create_tenant ? (
            <CustomButton
              size="large"
              onClick={() => {
                setIsModalVisible(true);
              }}
            >
              <img alt="" src={plusIcon} />
              Thêm công ty
            </CustomButton>
          ) : (
            ""
          )}

          <CustomButton
            size="large"
            style={{ marginLeft: "16px" }}
            onClick={_onCheckScan}
          >
            System health check
          </CustomButton>
          <CustomButton
            size="large"
            style={{ marginLeft: "16px" }}
            onClick={_onCheckCelery}
          >
            System celery check
          </CustomButton>
          {/* <CustomButton
            size="large"
            style={{ marginLeft: "16px" }}
            onClick={() => {
              AuthService.checkSession()
                .then(() => {
                  AuthService.logout();
                })
                .catch(() => {
                  dispatch(logout());
                });
              dispatch(logout());
              dispatch(loadUserDetailSuccess({}));
              navigate("/login");
            }}
          >
            Log out
          </CustomButton> */}
        </div>
        <div>
          {userDetail &&
          userDetail.permission &&
          userDetail.permission.view_tenant ? (
            <CustomButton size="large">
              <CSVLink data={dataExport} filename={"tenants.csv"}>
                Export
              </CSVLink>
            </CustomButton>
          ) : (
            ""
          )}
          {/* <Search
            onSearch={() => {}}
            style={{ width: 200, marginLeft: "16px" }}
            size="large"
          /> */}
        </div>
      </div>
      <div className="table">
        <Table
          columns={columns}
          scroll={{ x: "max-content" }}
          dataSource={dataSource}
          pagination={false}
        />
        <CustomPagination
          showQuickJumper
          current={currentPage}
          total={total_record}
          showSizeChanger
          showTotal={(total, range) =>
            `${range[0]}-${range[1]} of ${total} records`
          }
          // pageSize={recordPerPage}
          onChange={(e, pageSize) => {
            setCurrentPage(e);
            setRecordPerPage(pageSize);
            //loadData
          }}
        />
      </div>
      <ModalTenant
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        form={form}
        recordID={recordID}
        setRecordID={setRecordID}
        recordClone={recordClone}
        $recordClone={$recordClone}
        searchField={searchField}
        recordPerPage={recordPerPage}
        currentPage={currentPage}
        clone={clone}
        setClone={setClone}
        email={email}
        update={update}
        setUpdate={setUpdate}
        isDelete={isDelete}
        setIsDelete={setIsDelete}
      />
      <ModalConfimDelete
        title={"this tenant?"}
        decs="After deleting data will not be undone. Subdata will also be deleted."
        methodDelete={{}}
        dataDelete={{}}
        isLoading={false}
        open={openDelete}
        setOpen={setOpenDelete}
      />
      <ModalCheckScan />
      <ModalCheckCelery />
      <ModalMedia
        showModal={showModal}
        setShowModal={setShowModal}
        recordID={recordID}
        setRecordID={setRecordID}
      />
    </Wrapper>
  );
};

export default Tenants;

const CustomPagination = styled(Pagination)`
  position: fixed;
  bottom: 50px;
  right: 0px;
  width: 100%;
  text-align: right;
  background-color: #fff;
  padding-top: 16px;
  padding-right: 24px;
  z-index: 1000;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;

const Wrapper = styled.div`
  padding: 24px;
  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .table {
    margin-top: 16px;
    margin-bottom: 50px;
  }
  .ant-switch-checked {
    background: ${(props) => props.theme.main};
  }
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }

  &:active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  &:focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.darker};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.darker};
  color: #fff;
  border-color: ${(props) => props.theme.darker};
  margin-right: 16px;
  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;

  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #2c2c2c;
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;
const WrapAction = styled.div``;
const ItemAction = styled.div`
  width: 244px;
  padding: 8px;
  cursor: pointer;
  img {
    margin-right: 8px;
    width: 20px;
  }
  :hover {
    background: #eeeeee;
  }
`;
