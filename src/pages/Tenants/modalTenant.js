import {
  Button,
  Form,
  Input,
  InputNumber,
  Modal,
  Popconfirm,
  Radio,
  Select,
} from "antd";
import React, { useEffect } from "react";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import {
  cloneTenant,
  createTenants,
  deleteTenant,
  disableTenant,
} from "redux/slices/tenants";
import styled from "styled-components";
import { useState } from "react";

const { Option } = Select;

const ModalTenant = (props) => {
  const {
    isModalVisible,
    setIsModalVisible,
    form,
    recordID,
    setRecordID,
    recordClone,
    $recordClone,
    searchField,
    recordPerPage,
    currentPage,
    clone,
    setClone,
    email,
    update,
    setUpdate,
    isDelete,
    setIsDelete,
  } = props;

  const dispatch = useDispatch();
  //eslint-disable-next-line
  const [change, setChange] = useState("");
  const { isLoadingCreate, isLoadingDisable, packageList } = useSelector(
    (state) => state.tenantsReducer
  );

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    setRecordID("");
    $recordClone("");
    form.resetFields();
    setUpdate(false);
    setIsDelete(false);
  };

  const onFinish = (value) => {
    let data = {};
    data["Name"] = value.Name.trim();
    data["First_Name"] = value.First_Name.trim();
    data["Middle_Name"] = value.Middle_Name ? value.Middle_Name.trim() : "";
    data["Last_Name"] = value.Last_Name.trim();
    data["Description"] = value.Description ? value.Description.trim() : "";
    data["Number_Of_Users"] = value.Number_Of_Users;
    data["Email"] = value.Email.trim();
    data["Alow_Failed_Login_Attempts"] = value.Alow_Failed_Login_Attempts;
    data["Password_Expiration"] = value.Password_Expiration;
    data["Change_Pwd_First_Time"] = value.Change_Pwd_First_Time;
    data["Use_Edition"] = value.Use_Edition;

    if (recordID) {
    } else {
      data["Pass"] = value.Pass.trim();
    }
    if (recordID) {
      data["_id"] = recordID;
      data["Default_Email_Config"] = {
        host: _.get(value, "host"),
        email: _.get(value, "email"),
        protocol: _.get(value, "protocol"),
        imap_port: _.get(value, "imap_port"),
        password: _.get(value, "password"),
        smtp_port: _.get(value, "smtp_port"),
        username: _.get(value, "username"),
      };
      dispatch(
        disableTenant({
          type: "edit",
          data: data,
          load: {
            Record_per_page: recordPerPage,
            Current_page: currentPage,
            Query_field: [
              {
                _id: searchField[0],
                Type: "String",
              },
              {
                Name: searchField[1],
                Type: "String",
              },
              {
                First_Name: searchField[2],
                Type: "String",
              },
              {
                Middle_Name: searchField[3],
                Type: "String",
              },
              {
                Last_Name: searchField[4],
                Type: "String",
              },
              {
                Description: searchField[5],
                Type: "String",
              },
              {
                Number_Of_Users: searchField[6],
                Type: "Num",
              },
              {
                Email: searchField[7],
                Type: "String",
              },
              {
                Created_Date: searchField[8],
                Type: "String",
              },
              {
                Created_By: searchField[9],
                Type: "String",
              },
              {
                Modify_Time: searchField[10],
                Type: "String",
              },
              {
                Modify_By: searchField[11],
                Type: "String",
              },
              {
                URL: searchField[12],
                Type: "String",
              },
              {
                License: searchField[13],
                Type: "String",
              },
              {
                Expire_Day: searchField[14],
                Type: "String",
              },
            ],
          },
        })
      );
      handleCancel();
    } else {
      if (clone) {
        data["Default_Email_Config"] = {
          host: _.get(value, "host"),
          email: _.get(value, "email"),
          protocol: _.get(value, "protocol"),
          imap_port: _.get(value, "imap_port"),
          password: _.get(value, "password"),
          smtp_port: _.get(value, "smtp_port"),
          username: _.get(value, "username"),
        };
        data["tenant_id_source"] = recordClone;

        dispatch(
          cloneTenant({
            data: {
              target_tenant_email: data.Email,
              source_tenant_email: email,
              api_version: "2",
            },
            dataCreate: data,
          })
        );
        setClone(false);
        handleCancel();
      } else {
        dispatch(createTenants(data));
        handleCancel();
      }
    }
  };

  useEffect(() => {
    if (isLoadingCreate === false) {
      setIsModalVisible(isLoadingCreate);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoadingCreate]);

  return (
    <>
      <Modal
        title={`${
          isDelete
            ? "Xóa công ty"
            : recordID
            ? "Chỉnh sửa công ty"
            : "Thêm công ty"
        }`}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
        footer={null}
      >
        <Form
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          onFinish={onFinish}
          labelAlign={"left"}
          initialValues={{
            Alow_Failed_Login_Attempts: 5,
            Password_Expiration: 60,
            Change_Pwd_First_Time: true,
          }}
          onValuesChange={(value) => {
            setChange(value);
          }}
        >
          <div
            style={{
              border: "1px solid #20acac",
              borderRadius: "10px",
              padding: "16px",
              paddingBottom: "-16px",
              position: "relative",
            }}
          >
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#20acac",
                background: "white",
                position: "absolute",
                top: "-20px",
              }}
            >
              Company information
            </div>
            <Form.Item
              label="Company Name"
              name="Name"
              rules={[
                { required: true, message: "Please input your name!" },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete} />
            </Form.Item>

            <Form.Item label="Package" name="Use_Edition">
              <Select disabled={isDelete}>
                <Option value={null}>Unlimited</Option>
                {packageList.map((item, idx) => {
                  return <Option value={item._id}>{item.name_edition}</Option>;
                })}
              </Select>
            </Form.Item>

            <Form.Item
              label="Email"
              name="Email"
              rules={[
                {
                  required: true,
                  message: "Please input Email!",
                },
                {
                  type: "email",
                  message: "Please input valid email!",
                },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete || recordID || update ? true : false} />
            </Form.Item>
            {recordID ? (
              ""
            ) : (
              <Form.Item
                label="Password"
                name="Pass"
                rules={[
                  { required: true, message: "Please input password!" },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (getFieldValue("Pass").length < 9) {
                        return Promise.reject(
                          new Error("Password at least 9 characters!")
                        );
                      } else if (
                        /[A-Z]/.test(getFieldValue("Pass")) +
                          /[a-z]/.test(getFieldValue("Pass")) +
                          /\d/.test(getFieldValue("Pass")) +
                          /\W/.test(getFieldValue("Pass")) <
                        4
                      ) {
                        return Promise.reject(
                          new Error(
                            "Password must have at least one upper case, lower case, number and non-alpha numeric!"
                          )
                        );
                      } else {
                        return Promise.resolve();
                      }
                    },
                  }),
                ]}
                hasFeedback
              >
                <Input.Password type="password" disabled={isDelete} />
              </Form.Item>
            )}
            <Form.Item
              label="Description"
              name="Description"
              rules={[
                // { required: true, message: "Please input description!" },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete} />
            </Form.Item>
            <Form.Item
              label="Number of users"
              name="Number_Of_Users"
              rules={[
                { required: true, message: "Please input number of users!" },
              ]}
              style={{ marginBottom: 0 }}
            >
              <InputNumber
                min={1}
                max={10000}
                style={{ width: "100%" }}
                disabled={isDelete}
              />
            </Form.Item>
          </div>
          <div
            style={{
              border: "1px solid #20acac",
              borderRadius: "10px",
              padding: "16px",
              paddingBottom: "-16px",
              marginTop: "32px",
              position: "relative",
            }}
          >
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#20acac",
                background: "white",
                position: "absolute",
                top: "-20px",
              }}
            >
              Admin information
            </div>
            <Form.Item
              label="Admin First name"
              name="First_Name"
              rules={[
                { required: true, message: "Please input your first name!" },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete} />
            </Form.Item>
            <Form.Item
              label="Admin Middle name"
              name="Middle_Name"
              rules={[
                // { required: true, message: "Please input your middle name!" },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete} />
            </Form.Item>
            <Form.Item
              label="Admin Last name"
              name="Last_Name"
              rules={[
                { required: true, message: "Please input your last name!" },
                { whitespace: true },
              ]}
            >
              <Input disabled={isDelete} />
            </Form.Item>

            <Form.Item
              label="Allow failed login attempts"
              name="Alow_Failed_Login_Attempts"
            >
              <InputNumber
                min={1}
                max={100}
                style={{ width: "100%" }}
                disabled={isDelete}
              />
            </Form.Item>

            <Form.Item label="Password Expiration" name="Password_Expiration">
              <InputNumber
                min={1}
                max={100}
                style={{ width: "100%" }}
                disabled={isDelete}
              />
            </Form.Item>

            <Form.Item
              label="Change pwd first time"
              name="Change_Pwd_First_Time"
            >
              <Radio.Group disabled={isDelete}>
                <Radio value={true}>Yes</Radio>
                <Radio value={false}>No</Radio>
              </Radio.Group>
            </Form.Item>
          </div>

          {(update || clone) && (
            <div
              style={{
                border: "1px solid #20acac",
                borderRadius: "10px",
                padding: "16px",
                paddingBottom: "-16px",
                position: "relative",
                marginTop: "32px",
              }}
            >
              <div
                style={{
                  fontSize: "20px",
                  fontWeight: "bold",
                  color: "#20acac",
                  background: "white",
                  position: "absolute",
                  top: "-20px",
                }}
              >
                Email config
              </div>
              <Form.Item label="Host" name="host">
                <Input disabled={isDelete} />
              </Form.Item>
              <Form.Item label="Email" name="email">
                <Input disabled={isDelete} />
              </Form.Item>
              <Form.Item label="Protocol" name="protocol">
                <Select
                  disabled={isDelete}
                  options={[
                    {
                      label: "SSL",
                      value: "SSL",
                    },
                    {
                      label: "TLS",
                      value: "TLS",
                    },
                    {
                      label: "None",
                      value: "None",
                    },
                  ]}
                />
              </Form.Item>

              <Form.Item label="IMAP port" name="imap_port">
                <InputNumber style={{ width: "100%" }} disabled={isDelete} />
              </Form.Item>
              <Form.Item label="SMTP port" name="smtp_port">
                <InputNumber style={{ width: "100%" }} disabled={isDelete} />
              </Form.Item>
              <Form.Item label="User name" name="username">
                <Input disabled={isDelete} />
              </Form.Item>
              <Form.Item label="Password" name="password">
                <Input.Password
                  autocomplete="new-password"
                  disabled={isDelete}
                />
              </Form.Item>
            </div>
          )}

          {/* <Form.Item
            label="Use LCM"
            name="Use_LCM"
            rules={[{ required: true, message: "Please choose use LCM!" }]}
          >
            <Radio.Group>
              <Radio value={true}>Yes</Radio>
              <Radio value={false}>No</Radio>
            </Radio.Group>
          </Form.Item> */}
          {isDelete ? (
            <WrapDisplay>
              <Form.Item name="confirm_delete">
                <Input placeholder="Input Tenant ID " />
              </Form.Item>
              <ButtonDelete>
                {form.getFieldValue("confirm_delete") === recordID && (
                  <Popconfirm
                    title="Sure to delete?"
                    onConfirm={() => {
                      dispatch(
                        deleteTenant({
                          data: {
                            tenant_id: recordID,
                            confirm_tenant_id:
                              form.getFieldValue("confirm_delete"),
                          },
                          load: {
                            Record_per_page: recordPerPage,
                            Current_page: currentPage,
                            Query_field: [
                              {
                                _id: searchField[0],
                                Type: "String",
                              },
                              {
                                Name: searchField[1],
                                Type: "String",
                              },
                              {
                                First_Name: searchField[2],
                                Type: "String",
                              },
                              {
                                Middle_Name: searchField[3],
                                Type: "String",
                              },
                              {
                                Last_Name: searchField[4],
                                Type: "String",
                              },
                              {
                                Description: searchField[5],
                                Type: "String",
                              },
                              {
                                Number_Of_Users: searchField[6],
                                Type: "Num",
                              },
                              {
                                Email: searchField[7],
                                Type: "String",
                              },
                              {
                                Created_Date: searchField[8],
                                Type: "String",
                              },
                              {
                                Created_By: searchField[9],
                                Type: "String",
                              },
                              {
                                Modify_Time: searchField[10],
                                Type: "String",
                              },
                              {
                                Modify_By: searchField[11],
                                Type: "String",
                              },
                              {
                                URL: searchField[12],
                                Type: "String",
                              },
                              {
                                License: searchField[13],
                                Type: "String",
                              },
                              {
                                Expire_Day: searchField[14],
                                Type: "String",
                              },
                            ],
                          },
                        })
                      );
                      handleCancel();
                    }}
                  >
                    <CustomButton>Delete</CustomButton>
                  </Popconfirm>
                )}
                <CustomButton onClick={() => handleCancel()}>
                  Cancel
                </CustomButton>
              </ButtonDelete>
            </WrapDisplay>
          ) : (
            <CustomFooter>
              <CustomButtonSave
                size="large"
                htmlType="submit"
                loading={isLoadingCreate || isLoadingDisable}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  handleCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          )}
        </Form>
      </Modal>
    </>
  );
};

export default ModalTenant;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-bottom: 24px;
  padding-top: 24px;
`;
const WrapDisplay = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: 24px;
  input {
    width: 340px;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
`;
const ButtonDelete = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
const CustomButton = styled.div`
  border: 1px solid #ececec;
  width: 90px;
  padding: 6px;
  border-radius: 2px;
  cursor: pointer;
  display: flex;
  justify-content: center;
  margin: 0 8px;
  background: red;
  color: #fff;
  :last-child {
    background: #fff;
    color: #2c2c2c;
  }
`;
const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
