import React from "react";
import styled from "styled-components";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadAllObject,
  loadAllObjectSuccess,
  updateObjectMedia,
} from "redux/slices/tenants";
import { Checkbox } from "antd";

function ModalMedia({ showModal, setShowModal, recordID, setRecordID }) {
  const [form] = Form.useForm();
  const { allObject } = useSelector((state) => state.tenantsReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    if (allObject.length > 0) {
      let tem = [];
      allObject.forEach((item) => {
        if (item.status) {
          tem.push(item.object_id);
        }
      });
      form.setFieldsValue({ object_ids: tem });
    }
  }, [form, allObject]);

  const _onSubmit = (values) => {
    dispatch(
      updateObjectMedia({ object_ids: values.object_ids, tenant_id: recordID })
    );
    setShowModal(false);
    form.resetFields();
    dispatch(loadAllObjectSuccess([]));
    setRecordID("");
  };

  const _onCancel = () => {
    form.resetFields();
    setShowModal(false);
    dispatch(loadAllObjectSuccess([]));
    setRecordID("");
  };

  useEffect(() => {
    if (recordID) {
      dispatch(
        loadAllObject({
          tenant_id: recordID,
        })
      );
    }
  }, [recordID, dispatch]);

  return (
    <ModalCustom
      title={"Protect media"}
      visible={showModal}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form form={form} onFinish={_onSubmit} colon={false} labelAlign="left">
        <Form.Item name="object_ids">
          <Checkbox.Group>
            {allObject.map((item, index) => (
              <Checkbox key={index} value={item.object_id}>
                {item.object_name}
              </Checkbox>
            ))}
          </Checkbox.Group>
        </Form.Item>
        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            Save
          </Button>
          <Button onClick={_onCancel}>Cancel</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalMedia;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-checkbox-group {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
  }
  .ant-checkbox-wrapper {
    width: 25%;
    margin: 0 !important;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
