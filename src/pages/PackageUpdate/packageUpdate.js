import React, { useEffect } from "react";
import styled from "styled-components";
import {
  Button,
  Form,
  Input,
  Breadcrumb,
  InputNumber,
  Row,
  Col,
  Checkbox,
} from "antd";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  loadPackageDetail,
  updatePackage,
  getTenantRule,
  setTenantRule,
  setTenantRuleAll,
} from "redux/slices/tenants";
import { useTranslation } from "react-i18next";
import { getDomain } from "redux/slices/user";

const PackageUpdate = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { packageId } = useParams();
  const { packageDetail, tenantRule } = useSelector(
    (state) => state.tenantsReducer
  );
  const { domains } = useSelector((state) => state.userReducer);

  const onFinish = (values) => {
    dispatch(
      updatePackage({
        id_edition: packageId,
        ...values,
      })
    );
  };

  const handleChangeRule = (value) => {
    if (value.target.name.split("#")[0] !== "all") {
      if (
        value.target.name.split("#")[1] === "brand_and_color" &&
        (value.target.name.split("#")[0] === "edit" ||
          value.target.name.split("#")[0] === "delete")
      ) {
        dispatch(
          setTenantRule({
            domain: value.target.name.split("#")[1],
            action: "edit",
            subscription_id: packageId,
          })
        );

        setTimeout(() => {
          dispatch(
            setTenantRule({
              domain: value.target.name.split("#")[1],
              action: "delete",
              subscription_id: packageId,
            })
          );
        }, 500);
      } else {
        dispatch(
          setTenantRule({
            domain: value.target.name.split("#")[1],
            action: value.target.name.split("#")[0],
            subscription_id: packageId,
          })
        );
      }
    } else {
      dispatch(
        setTenantRuleAll({
          domain: value.target.name.split("#")[1],
          subscription_id: packageId,
        })
      );
    }
  };

  useEffect(() => {
    dispatch(getDomain());
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      getTenantRule({
        subscription_id: packageId,
      })
    );
  }, [dispatch, packageId]);

  useEffect(() => {
    if (packageId) {
      dispatch(
        loadPackageDetail({
          id_edition: packageId,
        })
      );
    }
  }, [packageId, dispatch]);

  useEffect(() => {
    form.setFieldsValue({ ...packageDetail });
  }, [packageDetail, form]);

  return (
    <Wrapper>
      <Breadcrumb>
        <CustomBreadCrumb>{t("tenants.name")}</CustomBreadCrumb>
        <CustomBreadCrumb>{t("tenants.updatePackage")}</CustomBreadCrumb>
      </Breadcrumb>
      <div className="wrapper">
        <Row gutter={16}>
          <Col span={10}>
            <Form
              name="basic"
              layout="vertical"
              autoComplete="off"
              form={form}
              onFinish={onFinish}
            >
              <Form.Item
                label={t("tenants.name")}
                name="name_edition"
                rules={[
                  {
                    required: true,
                    message: t("tenants.require1"),
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label={t("tenants.totalUsageRecord")}
                name="limit_record"
                rules={[
                  {
                    required: true,
                    message: t("tenants.require2"),
                  },
                ]}
              >
                <InputNumber min={1} style={{ width: "100%" }} />
              </Form.Item>

              <Form.Item
                label={t("tenants.notificationThreshold")}
                name="allow_warning"
                // rules={[
                //   {
                //     required: true,
                //     message: "Tổng bản ghi sử dụng!",
                //   },
                // ]}
              >
                <InputNumber style={{ width: "100%" }} max={100} min={1} />
              </Form.Item>

              <Form.Item>
                <Button type="primary" htmlType="submit" className="add-btn">
                  {t("common.save")}
                </Button>
                <Button
                  type="primary"
                  htmlType="submit"
                  className="cancel-btn"
                  onClick={() => {
                    navigate("/package");
                  }}
                >
                  {t("common.cancel")}
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col span={10}>
            <WrapRule>
              <div
                style={{
                  background: "#fff",
                  zIndex: 2,
                  position: "sticky",
                  height: "24px",
                  top: 0,
                }}
              />
              <Header>
                <FeatureName>
                  <span>Feature</span>
                </FeatureName>
                {/* <Rule>
                  <span>{t("common.view")}</span>
                </Rule>
                <Rule>
                  <span>{t("common.create")}</span>
                </Rule>
                <Rule>
                  <span>{t("common.edit")}</span>
                </Rule>
                <Rule>
                  <span>{t("common.delete")}</span>
                </Rule> */}
              </Header>

              {domains.map((item) => {
                return (
                  <Wrap key={item.name}>
                    <ObjectNameDetail>
                      {/* <Checkbox
                        name={`all#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("view")
                        )}
                        onChange={handleChangeRule}
                      /> */}
                      <span>{item.label}</span>
                    </ObjectNameDetail>
                    <RoleDetail>
                      <Checkbox
                        name={`all#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("view")
                        )}
                        onChange={handleChangeRule}
                      />
                    </RoleDetail>

                    {/* <RoleDetail>
                      <Checkbox
                        name={`view#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("view")
                        )}
                        onChange={handleChangeRule}
                      />
                    </RoleDetail>
                    <RoleDetail>
                      <Checkbox
                        name={`create#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("create")
                        )}
                        disabled={
                          item.name === "date_time_setting" ||
                          item.name === "campaign" ||
                          item.name === "agent_monitor" ||
                          item.name === "url_popup_setting" ||
                          item.name === "expose_api_check" ||
                          !tenantRule.find(
                            (rule) =>
                              rule.domain === item.name &&
                              rule.actions.includes("view")
                          )
                        }
                        onChange={handleChangeRule}
                      />
                    </RoleDetail>
                    <RoleDetail>
                      <Checkbox
                        name={`edit#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("edit")
                        )}
                        disabled={
                          item.name === "related_object" ||
                          item.name === "oauth2" ||
                          item.name === "o365_integration" ||
                          item.name === "agent_monitor" ||
                          !tenantRule.find(
                            (rule) =>
                              rule.domain === item.name &&
                              rule.actions.includes("view")
                          )
                        }
                        onChange={handleChangeRule}
                      />
                    </RoleDetail>
                    <RoleDetail>
                      <Checkbox
                        name={`delete#${item.name}`}
                        checked={tenantRule.find(
                          (rule) =>
                            rule.domain === item.name &&
                            rule.actions.includes("delete")
                        )}
                        disabled={
                          item.name === "date_time_setting" ||
                          item.name === "related_object" ||
                          item.name === "campaign" ||
                          item.name === "duplicate_rules" ||
                          item.name === "agent_monitor" ||
                          item.name === "url_popup_setting" ||
                          item.name === "voice_bot" ||
                          item.name === "expose_api_check" ||
                          !tenantRule.find(
                            (rule) =>
                              rule.domain === item.name &&
                              rule.actions.includes("view")
                          )
                        }
                        onChange={handleChangeRule}
                      />
                    </RoleDetail> */}
                  </Wrap>
                );
              })}
            </WrapRule>
          </Col>
        </Row>
      </div>
    </Wrapper>
  );
};

export default PackageUpdate;

const Wrapper = styled.div`
  padding: 24px;
  width: 100%;

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .wrapper {
    background-color: white;
    width: 100%;
    padding: 16px 24px;
    margin-top: 24px;
    label {
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      color: #2c2c2c;
    }
    .add-btn {
      background: #20a2a2;
      border: 1px solid #20a2a2;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
      border-radius: 2px;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #ffffff;
      margin-bottom: 16px;
      width: 115px;
      margin-right: 24px;
    }
    .cancel-btn {
      background: #fff;
      border: 1px solid #d9d9d9;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
      border-radius: 2px;
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 24px;
      text-align: center;
      color: #2c2c2c;
      margin-bottom: 16px;
      width: 115px;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    }
  }
`;
const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: default;
`;

const WrapRule = styled.div`
  padding: 0 24px 24px 24px;
  background: #fff;
  height: calc(100vh - 220px);
  overflow: auto;
`;

const Header = styled.div`
  position: sticky;
  top: 24px;
  z-index: 2;
  display: flex;
  background: #fafafa;
  border: 1px solid #ececec;
  padding: 0 16px;
`;

const FeatureName = styled.div`
  flex: 2;
  padding: 8px 0;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;
  border-right: 1px solid #d9d9d9;

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const ObjectNameDetail = styled(FeatureName)`
  border-right: none;
`;

const Rule = styled.div`
  flex: 1;
  border-right: 1px solid #d9d9d9;
  padding: 8px 0 8px 16px;
  font-size: 16px;
  font-family: var(--roboto-500);
  color: #2c2c2c;

  :last-child {
    border-right: none;
  }

  .ant-checkbox-wrapper {
    padding-right: 8px;
  }
`;

const RoleDetail = styled(Rule)`
  border-right: none;
`;

const Wrap = styled.div`
  display: flex;
  padding: 0 16px;
  border: 1px solid #eeeeee;
  border-top: none;
`;
