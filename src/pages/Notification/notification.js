import { useState, useEffect, useCallback } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import styled from "styled-components/macro";
import moment from "moment";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import Spin from "antd/lib/spin";
import Breadcrumb from "antd/lib/breadcrumb";
import Button from "antd/lib/button";

import MarkRead from "assets/icons/header/mark.svg";

import {
  getListNoti,
  seenOneNoti,
  seenAllNoti,
} from "redux/slices/notification";

function Notifications() {
  let navigate = useNavigate();
  const dispatch = useDispatch();

  const { dataNoti } = useSelector((state) => state.notificationReducer);
  const [listNoti, setListNoti] = useState([]);
  const [pageNoti, setPageNoti] = useState(0);
  const [hasMoreNoti, setHasMoreNoti] = useState(true);
  const [showMore, setShowMore] = useState({});

  const loadNoti = useCallback(() => {
    dispatch(
      getListNoti({
        record_per_page: 20,
        page: pageNoti,
      })
    );
  }, [dispatch, pageNoti]);

  const handleSeenNoti = (id) => {
    dispatch(
      seenOneNoti({
        _id: id,
      })
    );
  };

  const handleSeenAllNoti = () => {
    dispatch(seenAllNoti());
  };

  const _onShowMore = (e, id) => {
    e.preventDefault();
    let tempShowMore = { ...showMore };
    tempShowMore[id] = !tempShowMore[id];
    setShowMore(tempShowMore);
  };

  const onClickNoti = (title, objectID, recordID, idNoti, item) => {
    handleSeenNoti(idNoti);
    if (title.includes("The import process of file")) {
      navigate("/view-logs");
    } else if (
      title.includes("exported successfully") ||
      title.includes("is exporting. We will notify you when it is finished.")
    ) {
      navigate("/export-history");
    } else if (
      title.includes(
        "Error occurs when exporting report, Please contact the Admin."
      )
    ) {
      //
    } else if (objectID === "User") {
      navigate("/logs-user");
    } else if (objectID === "CalendarEvent") {
      if (item?.record_id) {
        localStorage.setItem("id_event", item.record_id);
        navigate("/calendar-detail");
      } else {
        navigate("/404");
      }
    } else {
      if (
        (item?.category === "email" || item?.category === "send_email") &&
        item?.notification_type &&
        item?.notification_type === "error"
      ) {
        let regex = new RegExp(/style="(.*?)"/);
        let style = item.additional_info.match(regex);
        let body = {};
        if (style) {
          let temp = style[0].replaceAll('"', "'");
          body = JSON.parse(
            item.additional_info.replaceAll("'", '"').replace(style[0], temp)
          );
        } else {
          body = JSON.parse(item.additional_info.replaceAll("'", '"'));
        }
        let typeEmail = body.action_email ? body.action_email : "send_email";
        delete body.action_email;

        localStorage.setItem("emailError", JSON.stringify(body));
        localStorage.setItem("typeEmailError", typeEmail);

        if (typeEmail !== "send_email") {
          navigate(`/consolidated-view/${objectID}/${body.record_id_main}`);
        } else {
          navigate(`/consolidated-view/${objectID}/${recordID}`);
        }
      } else if (objectID && recordID) {
        navigate(`/consolidated-view/${objectID}/${recordID}`);
      }
    }
  };

  const onMarkAllRead = () => {
    setListNoti([]);
    handleSeenAllNoti();
    loadNoti();
    setPageNoti(0);
  };

  const loadMoreNoti = () => {
    let tempPage = pageNoti + 1;

    setTimeout(() => {
      setPageNoti(tempPage);
    }, 1000);
  };

  const realTimeNoti = useCallback(() => {
    let tempNoti = [...listNoti];
    tempNoti.forEach((item, idx) => {
      let timeNew = moment(item.created_date).fromNow();
      tempNoti[idx].time = timeNew;
    });
    setListNoti(tempNoti);
  }, [listNoti]);

  useEffect(() => {
    let realTime;
    if (listNoti.length > 0) {
      realTime = setInterval(realTimeNoti, 60000);
    }
    return () => clearInterval(realTime);
  }, [listNoti, realTimeNoti]);

  useEffect(() => {
    loadNoti();
  }, [loadNoti]);

  useEffect(() => {
    if (dataNoti?.list_notification) {
      let tempListNoti = [];

      // if (dataNoti.status !== "Initial") {
      // let title = "New notification";
      // let decs = dataNoti.data.list_notification[0].content;
      // let objectID = dataNoti.data.list_notification[0].object_id;
      // let recorID = dataNoti.data.list_notification[0].record_id;
      // let ID = dataNoti.data.list_notification[0]._id;
      // openInfo({ title, decs, onClickNoti, objectID, recorID, ID })
      // }

      dataNoti.list_notification.map((item) => {
        let temp = { ...item, time: "" };
        return tempListNoti.push(temp);
      });

      tempListNoti.forEach((item, idx) => {
        tempListNoti[idx].time = moment(item.created_date).fromNow();
      });

      setListNoti((preState) => {
        let tempState = preState.concat(tempListNoti);
        const resultListNoti = tempState.filter(
          (ele, idx, arr) =>
            arr.findIndex((noti) => noti._id === ele._id) === idx
        );

        if (
          dataNoti.list_notification.length < 20 ||
          resultListNoti.length === 1000
        ) {
          setHasMoreNoti(false);
        }

        return resultListNoti;
      });
    }
  }, [dataNoti]);

  return (
    <Wrapper id="scrollableDiv">
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/dashboard")}>
          Dasboard
        </Breadcrumb.Item>
        <BreadcrumbItem>Notifications</BreadcrumbItem>
      </Breadcrumb>
      <Wrap>
        {listNoti.length === 0 ? (
          <EmptyNoti>No notification</EmptyNoti>
        ) : (
          <EarlierNoti>
            <WrapTitle>
              <MarkAll onClick={() => onMarkAllRead()}>
                <img src={MarkRead} alt="" />
                <span>Mark all as read</span>
              </MarkAll>
            </WrapTitle>
            <InfiniteScroll
              dataLength={listNoti.length}
              loader={<Spin />}
              hasMore={hasMoreNoti}
              next={loadMoreNoti}
              scrollableTarget="scrollableDiv"
            >
              {listNoti.map((item, idx) => {
                return (
                  <Noti key={idx} isSeen={item.is_seen}>
                    <TextNoti
                      showMore={showMore[item._id]}
                      overflow={item.content.length > 500}
                      onClick={() =>
                        onClickNoti(
                          item.content,
                          item.object_id,
                          item.record_id,
                          item._id,
                          item
                        )
                      }
                    >
                      {parse(item.content, optionsParse)}
                    </TextNoti>
                    {item.content.length > 500 && (
                      <p onClick={(e) => _onShowMore(e, item._id)}>
                        {showMore[item._id] ? "Show less" : "Show More"}
                      </p>
                    )}
                    <TimeNoti>{item.time}</TimeNoti>
                  </Noti>
                );
              })}
            </InfiniteScroll>
          </EarlierNoti>
        )}
      </Wrap>
    </Wrapper>
  );
}

export default Notifications;

const Wrapper = styled.div`
  padding: 24px;
  overflow-y: auto;
  height: calc(100vh - 130px);

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const Wrap = styled.div`
  width: 67rem;
  padding-top: 24px;
  margin: 16px auto 0 auto;
  background: #fff;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const EmptyNoti = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  font-size: 20px;
  font-family: var(--roboto-700);
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: flex-end;
  padding-right: 24px;
`;

const EarlierNoti = styled.div`
  .infinite-scroll-component__outerdiv {
    padding-bottom: 25px;
  }

  .infinite-scroll-component {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

const Noti = styled.div`
  padding: 8px 24px;
  overflow: hidden;
  text-overflow: ellipsis;
  position: relative;

  p {
    font-size: 16px;
    margin: 8px 0;
    color: ${(props) => props.theme.main};
  }

  :hover {
    background: #f2f4f5;
    cursor: pointer;
  }

  ::after {
    display: ${(props) => (props.isSeen ? "none" : "unset")};
    position: absolute;
    content: "";
    width: 12px;
    height: 12px;
    border-radius: 50%;
    background: #2ad181;
    top: 40%;
    right: 24px;
  }
`;

const TextNoti = styled.div`
  display: ${({ overflow, showMore }) =>
    overflow && !showMore ? "-webkit-box" : "inline-block"};
  -webkit-line-clamp: 4;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 16px;
  width: 90%;
  color: #252424;
`;

const TimeNoti = styled.div`
  color: ${(props) => props.theme.main};
  font-size: 14px;
`;

const MarkAll = styled(Button)`
  margin-bottom: 16px;

  &.ant-btn {
    font-size: 16px;
    color: #2c2c2c;

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;

      img {
        filter: brightness(200);
      }
    }
  }

  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }

  img {
    margin-right: 10px;
  }
`;
