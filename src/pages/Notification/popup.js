import styled from "styled-components/macro";
import InfiniteScroll from "react-infinite-scroll-component";

import "./index.scss";
import Spin from "antd/lib/spin";
import Popover from "antd/lib/popover";

import Menudot from "assets/icons/header/more_vertical.svg";
import MarkRead from "assets/icons/header/mark.svg";
import FullScreen from "assets/icons/header/openNoti.svg";
import notification from "assets/icons/header/noti.svg";
import HasNotification from "assets/icons/header/has_noti.svg";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";

function Notification({
  showNoti,
  handleShowNoti,
  onClickNoti,
  listNoti,
  loadMoreNoti,
  hasMoreNoti,
  handleShowMenuNoti,
  showMenuNoti,
  onOpenNotifications,
  onMarkAllRead,
  isLoading,
  unseenNoti,
}) {
  return (
    <Popover
      content={
        <WrapperNoti isLoading={isLoading}>
          {isLoading ? (
            <Spin />
          ) : (
            <>
              {listNoti.length === 0 ? (
                <EmptyNoti>No notification</EmptyNoti>
              ) : (
                <EarlierNoti>
                  <div
                    style={{
                      background: "#fff",
                      height: "24px",
                      position: "sticky",
                      top: 0,
                      zIndex: 2,
                    }}
                  />
                  <WrapTitle>
                    <Title>Notification</Title>
                    <Popover
                      content={
                        <WrapMenuNoti>
                          <MarkAll onClick={() => onMarkAllRead()}>
                            <img src={MarkRead} alt="" />
                            <span>Mark all as read</span>
                          </MarkAll>
                          <OpenNotifications
                            onClick={() => onOpenNotifications()}
                          >
                            <img src={FullScreen} alt="" />
                            {/* <span>Open notifications</span> */}
                            <span>View all</span>
                          </OpenNotifications>
                        </WrapMenuNoti>
                      }
                      trigger="click"
                      placement="bottomRight"
                      overlayClassName="popoverMenuNoti"
                      visible={showMenuNoti}
                      onVisibleChange={handleShowMenuNoti}
                    >
                      <MenuNoti src={Menudot} alt="" />
                    </Popover>
                  </WrapTitle>
                  <InfiniteScroll
                    scrollableTarget="popoverNoti"
                    dataLength={listNoti.length}
                    loader={<Spin />}
                    hasMore={hasMoreNoti}
                    next={() => loadMoreNoti()}
                  >
                    {listNoti.map((item, idx) => {
                      return (
                        <Noti
                          key={idx}
                          isSeen={item.is_seen}
                          onClick={() =>
                            onClickNoti(
                              item.content,
                              item.object_id,
                              item.record_id,
                              item._id,
                              item
                            )
                          }
                        >
                          <TextNoti>
                            {parse(item.content, optionsParse)}
                          </TextNoti>
                          <TimeNoti>{item.time}</TimeNoti>
                        </Noti>
                      );
                    })}
                  </InfiniteScroll>
                </EarlierNoti>
              )}
            </>
          )}
        </WrapperNoti>
      }
      id="popoverNoti"
      trigger="click"
      visible={showNoti}
      onVisibleChange={handleShowNoti}
      overlayClassName="popoverNoti"
      placement="bottom"
    >
      <NotiButton>
        {unseenNoti > 0 ? (
          <img src={HasNotification} alt="hasnoti" />
        ) : (
          <img src={notification} alt="noti" />
        )}
      </NotiButton>
    </Popover>
  );
}

export default Notification;

const WrapperNoti = styled.div`
  height: 100%;

  .ant-spin {
    display: ${(props) => (props.isLoading ? "flex" : "unset")};
    justify-content: center;
    align-items: center;
    height: ${(props) => (props.isLoading ? "100%" : "unset")};
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const EmptyNoti = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  font-size: 20px;
  font-family: var(--roboto-700);
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 24px 16px 24px;
  border-bottom: 2px solid #ececec;
  position: sticky;
  top: 24px;
  z-index: 2;
  background: #fff;
`;

const Title = styled.div`
  /* font-size: 24px; */
  font-size: 18px;
  font-family: var(--roboto-500);
`;

const EarlierNoti = styled.div`
  .infinite-scroll-component__outerdiv {
    padding-bottom: 25px;
  }

  .infinite-scroll-component {
    display: flex;
    flex-direction: column;
    justify-content: center;
  }
`;

const Noti = styled.div`
  padding: 8px 24px;
  overflow: hidden;
  text-overflow: ellipsis;
  position: relative;

  :hover {
    background: #f2f4f5;
    cursor: pointer;
  }

  ::after {
    display: ${(props) => (props.isSeen ? "none" : "unset")};
    position: absolute;
    content: "";
    width: 12px;
    height: 12px;
    border-radius: 50%;
    background: #2ad181;
    top: 40%;
    right: 24px;
  }
`;

const TextNoti = styled.div`
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;
  font-size: 16px;
  width: 90%;
  color: #252424;
`;

const TimeNoti = styled.div`
  color: ${(props) => props.theme.main};
  font-size: 14px;
`;

const NotiButton = styled.div`
  margin-right: 30px;
  margin-left: 8px;
  cursor: pointer;
  position: relative;
  width: 44px;
  height: 44px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;

  img {
    width: fit-content;
  }

  :hover {
    background: #e8eaeb;
  }

  /* span {
    margin-right: 0 !important;
    position: absolute;
    color: red;
    top: 0;
    right: 0;
  } */
`;

const MenuNoti = styled.img`
  width: 28px;
  height: 28px;

  :hover {
    cursor: pointer;
    background: #f5f5f5;
  }
`;

const WrapMenuNoti = styled.div``;

const MarkAll = styled.div`
  display: flex;
  align-items: center;
  font-size: 16px;
  padding: 3px 8px;

  img {
    margin-right: 8px;
  }

  :hover {
    cursor: pointer;
    background: #f5f5f5;
  }
`;

const OpenNotifications = styled(MarkAll)`
  margin-bottom: 0;
`;
