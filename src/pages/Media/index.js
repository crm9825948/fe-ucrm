import styled from "styled-components/macro";
import { getMedia } from "redux/slices/authenticated";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useParams } from "react-router";
import { Image } from "antd";
import { useTranslation, withTranslation } from "react-i18next";
import { useLocation } from "react-router-dom";

function Media() {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { pathname } = useLocation();
  const { objectID, tenantID } = useParams();
  const { media } = useSelector((state) => state.authenticatedReducer);

  useEffect(() => {
    if (objectID && tenantID && pathname) {
      dispatch(
        getMedia({
          link: `media/${
            tenantID + "/" + objectID + pathname?.split(objectID)[1]
          }`,
          fileName: pathname?.split(objectID)[1]?.split("/")[
            pathname?.split(objectID)[1]?.split("/").length - 1
          ],
        })
      );
    }
  }, [dispatch, objectID, tenantID, pathname]);

  useEffect(() => {
    if (window && media === "otherType") {
      setTimeout(() => {
        window.close();
      }, 500);
    }
  }, [media]);

  return (
    <Wrapper>
      {media === "otherType" ? (
        <span>{t("media.content")}</span>
      ) : (
        <Image src={media} alt="media" />
      )}
    </Wrapper>
  );
}

export default withTranslation()(Media);

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 100%;

  img {
    max-height: calc(100vh - 80px);
    max-width: calc(100vw - 80px);
  }
`;
