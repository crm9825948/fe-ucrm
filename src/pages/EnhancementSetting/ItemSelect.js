import React from "react";
import { Form, Select } from "antd";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { updateValueModal } from "redux/slices/knowledgeEnhancement";
import _ from "lodash";
const ItemSelect = ({ name, label, listField, isDisable }) => {
  const { t } = useTranslation();
  const { Option } = Select;
  const dispatch = useDispatch();
  const { valueModal } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );

  const renderOption = () => {
    if (
      name === "fld_category" ||
      name === "fld_tag" ||
      name === "fld_title" ||
      name === "fld_body" ||
      name === "article_tag" ||
      name === "fld_version"
    ) {
      return listField.map((field, index) => (
        <>
          {(field.type === "text" || field.type === "textarea") &&
            !_.get(field, "encrypted", false) && (
              <Option
                disabled={Object.values(valueModal).includes(field.ID)}
                key={index}
                value={field.ID}
              >
                {field.name}
              </Option>
            )}
        </>
      ));
    } else if (
      name === "fld_parent_category" ||
      name === "article_category" ||
      name === "fld_newest_version"
    ) {
      return listField.map((field, index) => (
        <>
          {field.type === "lookup" && !_.get(field, "encrypted", false) && (
            <Option
              disabled={Object.values(valueModal).includes(field.ID)}
              key={index}
              value={field.ID}
            >
              {field.name}
            </Option>
          )}
        </>
      ));
    } else if (
      name === "fld_upvotes" ||
      name === "fld_downvotes" ||
      name === "fld_total_views"
    ) {
      return listField.map((field, index) => (
        <>
          {field.type === "number" && !_.get(field, "encrypted", false) && (
            <Option
              disabled={Object.values(valueModal).includes(field.ID)}
              key={index}
              value={field.ID}
            >
              {field.name}
            </Option>
          )}
        </>
      ));
    } else if (name === "fld_expired_date") {
      return listField.map((field, index) => (
        <>
          {field.type === "date" && !_.get(field, "encrypted", false) && (
            <Option
              disabled={Object.values(valueModal).includes(field.ID)}
              key={index}
              value={field.ID}
            >
              {field.name}
            </Option>
          )}
        </>
      ));
    } else if (name === "fld_thumbnail" || name === "fld_attachments") {
      return listField.map((field, index) => (
        <>
          {field.type === "file" && !_.get(field, "encrypted", false) && (
            <Option
              disabled={Object.values(valueModal).includes(field.ID)}
              key={index}
              value={field.ID}
            >
              {field.name}
            </Option>
          )}
        </>
      ));
    } else {
      return listField.map((field, index) => {
        return (
          <>
            {!_.get(field, "encrypted", false) && (
              <Option
                disabled={Object.values(valueModal).includes(field.ID)}
                key={index}
                value={field.ID}
              >
                {field.name}
              </Option>
            )}
          </>
        );
      });
    }
  };

  return (
    <Item
      name={name}
      label={label}
      rules={[{ required: true, message: t("knowledgeBase.required") }]}
    >
      <Select
        disabled={isDisable}
        allowClear
        showSearch
        optionFilterProp="children"
        filterOption={(inputValue, option) => {
          if (option.children) {
            return option.children
              .toLowerCase()
              .indexOf(inputValue.toLowerCase()) >= 0
              ? true
              : false;
          } else if (option.label) {
            return option.label
              .toLowerCase()
              .indexOf(inputValue.toLowerCase()) >= 0
              ? true
              : false;
          }
        }}
        placeholder={t("common.placeholderSelect")}
        onChange={(value) => {
          dispatch(
            updateValueModal({
              key: name,
              value: value,
            })
          );
        }}
      >
        {renderOption()}
      </Select>
    </Item>
  );
};

export default ItemSelect;

const Item = styled(Form.Item)``;
