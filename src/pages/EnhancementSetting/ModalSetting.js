import { Modal, Steps, Form, Input, Switch, Select } from "antd";
import _ from "lodash";
import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import {
  createKnowledgeSetting,
  getFieldsObject,
  resetValueModal,
  unmountModalSetting,
  updateSettingEnhancement,
  updateValueModal,
} from "redux/slices/knowledgeEnhancement";
import styled from "styled-components";
import ItemSelect from "./ItemSelect";
const { Step } = Steps;
const { TextArea } = Input;
const { Option } = Select;
const ModalSetting = ({ showModal, setShowModal, form, setValueForm }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { valueModal, fieldsCategory, fieldsTag, fieldsArticle } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [listObject, setListObject] = useState([]);
  const [step, setStep] = useState(0);
  const [formValue] = Form.useForm();
  const [optionStatus, setOptionStatus] = useState([]);
  const [objectSetting, setObjectSetting] = useState([]);
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "knowledge_base_enhancement" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };
  useEffect(() => {
    if (Object.entries(form).length > 0) {
      let temp = [];
      handleDataField(fieldsArticle).forEach((field) => {
        if (field.ID === form.article_setting?.fld_status) {
          field.option.forEach((op) => {
            temp.push({ label: op.label, value: op.value });
          });
        }
      });
      setOptionStatus(temp);
    }
  }, [fieldsArticle, form]);
  useEffect(() => {
    let temp = [];
    listKnowledgeSetting.forEach((setting) => {
      setting.object_registered.forEach((object) => {
        temp.push(object);
      });
    });
    setObjectSetting(temp);
  }, [listKnowledgeSetting]);

  useEffect(() => {
    if (!showModal) {
      setStep(0);
      dispatch(unmountModalSetting());
      formValue.resetFields();
      setValueForm({});
      setOptionStatus([]);
    }
  }, [dispatch, showModal, formValue, setValueForm]);

  useEffect(() => {
    if (Object.entries(form).length > 0) {
      dispatch(
        getFieldsObject({
          data: {
            object_id: form.article_setting.obj_article,
            api_version: "2",
            show_meta_fields: false,
          },
          key: "article",
        })
      );
      dispatch(
        getFieldsObject({
          data: {
            object_id: form.category_setting.obj_category,
            api_version: "2",
            show_meta_fields: false,
          },
          key: "category",
        })
      );
      dispatch(
        getFieldsObject({
          data: {
            object_id: form.tag_setting.obj_tag,
            api_version: "2",
            show_meta_fields: false,
          },
          key: "tag",
        })
      );
      formValue.setFieldsValue({
        description: form.description,
        show_votes: form.show_votes,
        show_total_views: form.show_total_views,
        number_of_attachments: form.number_of_attachments,
        obj_category: form.category_setting.obj_category,
        fld_category: form.category_setting.fld_category,
        fld_parent_category: form.category_setting.fld_parent_category,
        obj_tag: form.tag_setting.obj_tag,
        fld_tag: form.tag_setting.fld_tag,
        obj_article: form.article_setting.obj_article,
        fld_title: form.article_setting.fld_title,
        fld_body: form.article_setting.fld_body,
        article_category: form.article_setting.fld_category,
        article_tag: form.article_setting.fld_tag,
        fld_upvotes: form.article_setting.fld_upvotes,
        fld_downvotes: form.article_setting.fld_downvotes,
        fld_version: form.article_setting.fld_version,
        fld_total_views: form.article_setting.fld_total_views,
        fld_expired_date: form.article_setting.fld_expired_date,
        fld_thumbnail: form.article_setting.fld_thumbnail,
        fld_attachments: form.article_setting.fld_attachments,
        fld_status: form.article_setting.fld_status,
        display_status_options: form.article_setting.display_status_options,
        fld_newest_version: form.article_setting.fld_newest_version,
      });
    }
    //eslint-disable-next-line
  }, [form]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObject(tempObjects);
    }
  }, [category]);

  const renderDescription = (step, position) => {
    let description = "";
    if (step === position) {
      description = t("common.inprogress");
    } else if (step > position) {
      description = t("common.finished");
    } else {
      description = t("common.waiting");
    }
    return description;
  };

  const handleDataField = (listField) => {
    let temp = [];
    if (
      listField.length > 0 &&
      listField[listField.length - 1]["main_object"] &&
      listField[listField.length - 1]["main_object"]["sections"]
    ) {
      listField[listField.length - 1]["main_object"]["sections"].forEach(
        (section) => {
          section.fields.forEach((field) => {
            if (!field.hidden) {
              temp.push(field);
            }
          });
        }
      );
    }
    return temp;
  };

  const handleListObject = (key) => {
    let temp = [];
    if (Object.entries(form).length > 0) {
      if (key === "category") {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_tag &&
            object.value !== valueModal.obj_article
          ) {
            temp.push(object);
          }
        });
      } else if (key === "tag") {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_category &&
            object.value !== valueModal.obj_article
          ) {
            temp.push(object);
          }
        });
      } else {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_category &&
            object.value !== valueModal.obj_tag
          ) {
            temp.push(object);
          }
        });
      }
    } else {
      if (key === "category") {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_tag &&
            object.value !== valueModal.obj_article &&
            !objectSetting.includes(object.value)
          ) {
            temp.push(object);
          }
        });
      } else if (key === "tag") {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_category &&
            object.value !== valueModal.obj_article &&
            !objectSetting.includes(object.value)
          ) {
            temp.push(object);
          }
        });
      } else {
        listObject.forEach((object) => {
          if (
            object.value !== valueModal.obj_category &&
            object.value !== valueModal.obj_tag &&
            !objectSetting.includes(object.value)
          ) {
            temp.push(object);
          }
        });
      }
    }
    return temp;
  };

  const handleChooseObject = (value, key) => {
    dispatch(
      getFieldsObject({
        data: {
          object_id: value,
          api_version: "2",
          show_meta_fields: false,
        },
        key: key,
      })
    );
    if (key === "category") {
      let temp = {
        ...valueModal,
        obj_category: value,
        fld_category: undefined,
        fld_parent_category: undefined,
      };
      formValue.setFieldsValue({
        fld_category: undefined,
        fld_parent_category: undefined,
      });
      dispatch(resetValueModal(temp));
    } else if (key === "tag") {
      let temp = { ...valueModal, obj_tag: value, fld_tag: undefined };
      formValue.setFieldsValue({
        fld_tag: undefined,
      });
      dispatch(resetValueModal(temp));
    } else {
      setOptionStatus([]);
      let temp = {
        ...valueModal,
        obj_article: value,
        fld_title: undefined,
        fld_newest_version: undefined,
        display_status_options: undefined,
        fld_thumbnail: undefined,
        fld_body: undefined,
        article_category: undefined,
        fld_upvotes: undefined,
        fld_attachments: undefined,
        fld_status: undefined,
        fld_version: undefined,
        fld_downvotes: undefined,
        fld_expired_date: undefined,
        fld_total_views: undefined,
        article_tag: undefined,
      };
      formValue.setFieldsValue({
        fld_title: undefined,
        fld_newest_version: undefined,
        display_status_options: undefined,
        fld_thumbnail: undefined,
        fld_body: undefined,
        article_category: undefined,
        fld_upvotes: undefined,
        fld_attachments: undefined,
        fld_status: undefined,
        fld_version: undefined,
        fld_downvotes: undefined,
        fld_expired_date: undefined,
        fld_total_views: undefined,
        article_tag: undefined,
      });
      dispatch(resetValueModal(temp));
    }
  };

  const renderContent = () => {
    switch (step) {
      case 0:
        return (
          <>
            <div>
              <Item name="description" label={t("common.description")}>
                <TextArea
                  disabled={Object.entries(form).length > 0}
                  rows={2}
                  placeholder={t("common.placeholderInput")}
                  onChange={(value) => {
                    dispatch(
                      updateValueModal({
                        key: "description",
                        value: value.target.value,
                      })
                    );
                  }}
                ></TextArea>
              </Item>
              <Item
                name="number_of_attachments"
                label={t("knowledgeBase.attachments")}
                // rules={[
                //   { required: true, message: t("knowledgeBase.required") },
                // ]}
              >
                <Input
                  // defaultValue={"5"}
                  disabled={Object.entries(form).length > 0}
                  placeholder={t("common.placeholderInput")}
                  type="number"
                  min={1}
                  onChange={(value) => {
                    dispatch(
                      updateValueModal({
                        key: "number_of_attachments",
                        value: value.target.value,
                      })
                    );
                  }}
                />
              </Item>
              <SwitchWrap>
                <Item
                  name="show_votes"
                  label={t("knowledgeBase.showVote")}
                  valuePropName="checked"
                >
                  <Switch
                    disabled={Object.entries(form).length > 0}
                    checkedChildren={t("knowledgeBase.on")}
                    unCheckedChildren={t("knowledgeBase.off")}
                    defaultChecked
                    onChange={(value) => {
                      dispatch(
                        updateValueModal({
                          key: "show_votes",
                          value: value,
                        })
                      );
                    }}
                  />
                </Item>
                <Item
                  name="show_total_views"
                  label={t("knowledgeBase.showTotal")}
                  valuePropName="checked"
                >
                  <Switch
                    disabled={Object.entries(form).length > 0}
                    checkedChildren={t("knowledgeBase.on")}
                    unCheckedChildren={t("knowledgeBase.off")}
                    defaultChecked
                    onChange={(value) => {
                      dispatch(
                        updateValueModal({
                          key: "show_total_views",
                          value: value,
                        })
                      );
                    }}
                  />
                </Item>
              </SwitchWrap>
            </div>
            <div>
              <ObjectWrap style={{ marginTop: "0" }}>
                <Item
                  name="obj_category"
                  label={t("knowledgeBase.categoryObject")}
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                >
                  <Select
                    disabled={Object.entries(form).length > 0}
                    allowClear
                    showSearch
                    optionFilterProp="label"
                    placeholder={t("objectLayoutField.chooseObject")}
                    options={handleListObject("category")}
                    onChange={(value) => {
                      handleChooseObject(value, "category");
                    }}
                  ></Select>
                </Item>
              </ObjectWrap>
              <WrapField>
                <ItemWrap>
                  <ItemSelect
                    isDisable={Object.entries(form).length > 0}
                    name="fld_category"
                    key="fld_category"
                    label={t("knowledgeBase.categoryField")}
                    listField={handleDataField(fieldsCategory)}
                  />
                </ItemWrap>
                <ItemWrap>
                  <ItemSelect
                    isDisable={Object.entries(form).length > 0}
                    key="fld_parent_category"
                    name="fld_parent_category"
                    label={t("knowledgeBase.parentCategory")}
                    listField={handleDataField(fieldsCategory)}
                  />
                </ItemWrap>
              </WrapField>
            </div>
            <div>
              <ObjectWrap>
                <Item
                  name="obj_tag"
                  label={t("knowledgeBase.tagObjet")}
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                >
                  <Select
                    disabled={Object.entries(form).length > 0}
                    allowClear
                    showSearch
                    optionFilterProp="label"
                    placeholder={t("objectLayoutField.chooseObject")}
                    options={handleListObject("tag")}
                    onChange={(value) => {
                      handleChooseObject(value, "tag");
                    }}
                  ></Select>
                </Item>
              </ObjectWrap>
              <WrapField>
                <ItemFull>
                  <ItemSelect
                    isDisable={Object.entries(form).length > 0}
                    name="fld_tag"
                    key="fld_tag"
                    label={t("knowledgeBase.tagField")}
                    listField={handleDataField(fieldsTag)}
                  />
                </ItemFull>
              </WrapField>
            </div>
          </>
        );
      case 1:
        return (
          <div>
            <ObjectWrap>
              <Item
                name="obj_article"
                label={t("knowledgeBase.articleObject")}
                rules={[
                  { required: true, message: t("knowledgeBase.required") },
                ]}
              >
                <Select
                  disabled={Object.entries(form).length > 0}
                  allowClear
                  showSearch
                  optionFilterProp="label"
                  placeholder={t("objectLayoutField.chooseObject")}
                  options={handleListObject("article")}
                  onChange={(value) => {
                    handleChooseObject(value, "article");
                  }}
                ></Select>
              </Item>
            </ObjectWrap>
            <ArticleWrap>
              <ArticleContent>
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  name="fld_title"
                  key="fld_title"
                  label={t("knowledgeBase.titleField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_body"
                  name="fld_body"
                  label={t("knowledgeBase.bodyField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="article_category"
                  name="article_category"
                  label={t("knowledgeBase.categoryField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="article_tag"
                  name="article_tag"
                  label={t("knowledgeBase.tagField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_upvotes"
                  name="fld_upvotes"
                  label={t("knowledgeBase.upvoteField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_downvotes"
                  name="fld_downvotes"
                  label={t("knowledgeBase.downvoteField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_version"
                  name="fld_version"
                  label={t("knowledgeBase.versionField")}
                  listField={handleDataField(fieldsArticle)}
                />
              </ArticleContent>

              <ArticleContent>
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_total_views"
                  name="fld_total_views"
                  label={t("knowledgeBase.totalViewField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_expired_date"
                  name="fld_expired_date"
                  label={t("knowledgeBase.expriedDateField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_thumbnail"
                  name="fld_thumbnail"
                  label={t("knowledgeBase.thumbnailField")}
                  listField={handleDataField(fieldsArticle)}
                />
                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_attachments"
                  name="fld_attachments"
                  label={t("knowledgeBase.attachmentField")}
                  listField={handleDataField(fieldsArticle)}
                />

                <Item
                  key="fld_status"
                  name="fld_status"
                  label={t("knowledgeBase.statusFields")}
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                >
                  <Select
                    disabled={Object.entries(form).length > 0}
                    allowClear
                    showSearch
                    optionFilterProp="label"
                    placeholder={t("common.placeholderSelect")}
                    onChange={(value) => {
                      dispatch(
                        updateValueModal({
                          key: "fld_status",
                          value: value,
                        })
                      );
                      let temp = [];
                      handleDataField(fieldsArticle).forEach((field) => {
                        if (field.ID === value) {
                          field.option.forEach((op) => {
                            temp.push({ label: op.label, value: op.value });
                          });
                        }
                      });

                      formValue.setFieldsValue({
                        display_status_options: undefined,
                      });
                      setOptionStatus(temp);
                    }}
                  >
                    {handleDataField(fieldsArticle).map((field, index) => {
                      if (
                        field.type === "select" &&
                        !_.get(field, "encrypted", false)
                      ) {
                        return (
                          <Option
                            disabled={Object.values(valueModal).includes(
                              field.ID
                            )}
                            key={index}
                            value={field.ID}
                          >
                            {field.name}
                          </Option>
                        );
                      } else return <></>;
                    })}
                  </Select>
                </Item>
                <Item
                  key="display_status_options"
                  name="display_status_options"
                  label={t("knowledgeBase.displayStatusOption")}
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                >
                  <Select
                    disabled={
                      Object.entries(form).length > 0 && !checkRule("edit")
                    }
                    showSearch
                    optionFilterProp="label"
                    mode="multiple"
                    placeholder={t("common.placeholderSelect")}
                    onChange={(value) => {
                      dispatch(
                        updateValueModal({
                          key: "display_status_options",
                          value: value,
                        })
                      );
                    }}
                  >
                    {optionStatus.map((field, index) => {
                      return (
                        <Option key={index} value={field.value}>
                          {field.label}
                        </Option>
                      );
                    })}
                  </Select>
                </Item>

                <ItemSelect
                  isDisable={Object.entries(form).length > 0}
                  key="fld_newest_version"
                  name="fld_newest_version"
                  label={t("knowledgeBase.newestVersionField")}
                  listField={handleDataField(fieldsArticle)}
                />
              </ArticleContent>
            </ArticleWrap>
          </div>
        );
      default:
        break;
    }
  };

  const handleFinish = () => {
    if (step < 1) {
      setStep(step + 1);
    } else {
      if (Object.entries(form).length > 0) {
        dispatch(
          updateSettingEnhancement({
            record_id: form._id,
            field: "display_status_options",
            value: formValue.getFieldValue("display_status_options"),
          })
        );
      } else {
        let {
          description,
          number_of_attachments,
          show_votes,
          show_total_views,
          obj_category,
          fld_category,
          fld_parent_category,
          obj_tag,
          fld_tag,
          obj_article,
          fld_title,
          fld_body,
          article_category,
          article_tag,
          fld_upvotes,
          fld_downvotes,
          fld_version,
          fld_total_views,
          fld_expired_date,
          fld_thumbnail,
          fld_attachments,
          fld_status,
          display_status_options,
          fld_newest_version,
        } = valueModal;
        dispatch(
          createKnowledgeSetting({
            description: description,
            number_of_attachments: number_of_attachments,
            show_votes: show_votes === false ? show_votes : true,
            show_total_views:
              show_total_views === false ? show_total_views : true,
            category_setting: {
              obj_category: obj_category,
              fld_category,
              fld_parent_category: fld_parent_category,
            },
            tag_setting: {
              obj_tag: obj_tag,
              fld_tag: fld_tag,
            },
            article_setting: {
              obj_article: obj_article,
              fld_title: fld_title,
              fld_body: fld_body,
              fld_category: article_category,
              fld_tag: article_tag,
              fld_upvotes: fld_upvotes,
              fld_downvotes: fld_downvotes,
              fld_version: fld_version,
              fld_total_views: fld_total_views,
              fld_expired_date: fld_expired_date,
              fld_thumbnail: fld_thumbnail,
              fld_attachments: fld_attachments,
              fld_status: fld_status,
              display_status_options: display_status_options,
              fld_newest_version: fld_newest_version,
            },
          })
        );
      }
      setShowModal(false);
    }
  };

  return (
    <CustomModal
      title={
        Object.entries(form).length > 0
          ? "Detail knowledge base setting"
          : "Add new knowledge base setting"
      }
      visible={showModal}
      footer={
        <ButtonWrap>
          <CustomButton onClick={() => setShowModal(false)}>
            {t("common.cancel")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 0 ? "none" : "flex" }}
            onClick={() => setStep(step - 1)}
          >
            {t("common.back")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 1 ? "none" : "flex" }}
            onClick={() => formValue.submit()}
          >
            {t("common.next")}
          </CustomButton>
          {/* {Object.entries(form).length === 0 && ( */}
          {((Object.entries(form).length > 0 && checkRule("edit")) ||
            (Object.entries(form).length === 0 && checkRule("create"))) && (
            <CustomButton
              style={{ display: step === 1 ? "flex" : "none" }}
              onClick={() => {
                formValue.submit();
              }}
            >
              {t("common.save")}
            </CustomButton>
          )}
          {/* )} */}
        </ButtonWrap>
      }
      width={800}
      onCancel={() => setShowModal(false)}
    >
      <StepWrap>
        <Steps current={step}>
          <Step
            title={t("knowledgeBase.information")}
            description={renderDescription(step, 0)}
          />

          <Step
            title={t("knowledgeBase.article")}
            description={renderDescription(step, 1)}
          />
        </Steps>
      </StepWrap>

      <FormWrap>
        <Form
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          name="step"
          onFinish={handleFinish}
          form={formValue}
        >
          <FormContent>{renderContent()}</FormContent>
        </Form>
      </FormWrap>
    </CustomModal>
  );
};

export default ModalSetting;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-modal-body {
    height: 700px;
    overflow: auto;
    padding: 24px 24px 0 24px;
  }
  .ant-modal-footer {
    border: none;
    padding: 0 24px 24px 0;
    margin-top: 24px;
  }
`;

const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  cursor: pointer;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  :nth-child(3),
  :nth-child(4) {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;

const StepWrap = styled.div`
  display: flex;
  justify-content: center;
  .ant-steps {
    width: 400px;
  }
  .ant-steps-item-active {
    .ant-steps-item-icon {
      background: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }

    .ant-steps-item-title {
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-item-wait
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }
  .ant-steps-item-finish {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main};
    }
    svg {
      color: ${(props) => props.theme.main};
    }
    .ant-steps-item-title {
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
    .ant-steps-item-title::after {
      background-color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-icon {
    top: -1.5px;
  }
  .ant-steps-item-description {
    font-size: 16px;
  }
  .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description,
  .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description {
    color: #2c2c2c;
  }
`;

const FormWrap = styled.div`
  margin-top: 20px;
`;

const FormContent = styled.div`
  .ant-col-24.ant-form-item-label {
    padding: 0;
  }
  .ant-form-item-control-input {
    min-height: unset;
  }
  .ant-form-item-label > label {
    height: unset;
    font-size: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-700);
    margin-bottom: 8px;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-input:focus,
  .ant-input-focused {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }

  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const Item = styled(Form.Item)``;

const ObjectWrap = styled.div`
  margin-top: 24px;
  background: #f2f4f5;
  border-radius: 5px;
  padding: 16px;
  margin-bottom: 16px;
  .ant-form-item {
    margin-bottom: 0;
  }
`;

const ArticleWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const ArticleContent = styled.div`
  width: 50%;
  :first-child {
    border-right: solid 1px #ececec;
    padding-right: 16px;
  }
  :last-child {
    padding-left: 16px;
  }
  .ant-form-item {
    :last-child {
      margin: 0px;
    }
  }
`;

const SwitchWrap = styled.div`
  display: flex;
  .ant-switch {
    width: 54px;
  }
`;

const WrapField = styled.div`
  display: flex;
  justify-content: space-between;
  border: solid 1px #ececec;
  border-radius: 5px;
  padding: 16px 16px 0 16px;
  margin-top: -23px;
  border-top: none;
`;

const ItemWrap = styled.div`
  width: 49%;
`;

const ItemFull = styled.div`
  width: 100%;
`;
