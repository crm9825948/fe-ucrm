import { Breadcrumb, Typography, Tooltip, Switch, Tag } from "antd";
import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import styled from "styled-components";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import ModalSetting from "./ModalSetting";
import { useDispatch, useSelector } from "react-redux";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import deleteImg from "assets/icons/common/delete-icon.png";
import Details from "assets/icons/users/details.svg";

import {
  deleteSettingEnhancement,
  updateSettingEnhancement,
} from "redux/slices/knowledgeEnhancement";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { reIndex, testConnection } from "redux/slices/enhancementView";
import _ from "lodash";
import ImgConfirm from "assets/icons/common/warning.png";

const { Text: TextComponent } = Typography;

const EnhancementSetting = () => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [recordID, setRecordID] = useState("");
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const [showConfirm, setShowConfirm] = useState(false);
  const [idActive, setIdActive] = useState("");

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "knowledge_base_enhancement" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleNameObject = (list, id) => {
    let name = "";
    if (Object.keys(list).length > 0) {
      Object.entries(list).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object._id === id) {
            name = object.Name;
          }
        });
      });
    }
    return name;
  };
  const [showModal, setShowModal] = useState(false);
  const [valueForm, setValueForm] = useState({});

  const renderTable = (list) => {
    return (
      <>
        <table>
          <thead className="table-header">
            <tr>
              <th></th>
              <th>{t("knowledgeBase.description")}</th>
              <th>{t("knowledgeBase.objectArticle")}</th>
              <th>{t("knowledgeBase.showVote")}</th>
              <th>{t("knowledgeBase.showTotal")}</th>
              <th>{t("knowledgeBase.action")}</th>
            </tr>
          </thead>
          <tbody className="table-body">
            {list.map((setting, index) => {
              return (
                <tr key={index}>
                  <td>
                    <TagWrap>
                      <Tooltip title={t("knowledgeBase.checkConnect")}>
                        <Tag
                          color="green"
                          onClick={() =>
                            dispatch(
                              testConnection({
                                object_id: setting.article_setting.obj_article,
                              })
                            )
                          }
                        >
                          Test
                        </Tag>
                      </Tooltip>
                      <Tag
                        color="green"
                        onClick={() => {
                          setIdActive(setting.article_setting.obj_article);
                          setShowConfirm(true);
                        }}
                      >
                        Re-index
                      </Tag>
                    </TagWrap>
                  </td>
                  <td>
                    <TextComponent ellipsis={{ tooltip: setting.description }}>
                      {setting.description}
                    </TextComponent>
                  </td>
                  <td>
                    {handleNameObject(
                      category,
                      setting.article_setting.obj_article
                    )}
                  </td>
                  <td>
                    <Switch
                      disabled={!checkRule("edit")}
                      checkedChildren={t("knowledgeBase.on")}
                      unCheckedChildren={t("knowledgeBase.off")}
                      defaultChecked={setting.show_votes}
                      onChange={(checked) => {
                        dispatch(
                          updateSettingEnhancement({
                            record_id: setting._id,
                            field: "show_votes",
                            value: checked,
                          })
                        );
                      }}
                    />
                  </td>
                  <td>
                    <Switch
                      disabled={!checkRule("edit")}
                      checkedChildren={t("knowledgeBase.on")}
                      unCheckedChildren={t("knowledgeBase.off")}
                      defaultChecked={setting.show_total_views}
                      onChange={(checked) => {
                        dispatch(
                          updateSettingEnhancement({
                            record_id: setting._id,
                            field: "show_total_views",
                            value: checked,
                          })
                        );
                      }}
                    />
                  </td>
                  <td>
                    <Tooltip title={t("knowledgeBase.view")}>
                      <img
                        src={Details}
                        alt="Details"
                        onClick={() => {
                          setShowModal(true);
                          setValueForm(setting);
                        }}
                      />
                    </Tooltip>
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          src={deleteImg}
                          alt="Delete"
                          onClick={() => {
                            setRecordID(setting._id);
                            dispatch(setShowModalConfirmDelete(true));
                          }}
                        />
                      </Tooltip>
                    )}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  };

  return (
    <Wrapper>
      <HeaderWrap>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("common.setting")}
          </Breadcrumb.Item>
          <Breadcrumb.Item> {t("knowledgeBase.setting")} </Breadcrumb.Item>
        </Breadcrumb>
        {checkRule("create") && (
          <ButtonAdd onClick={() => setShowModal(true)}>
            <img src={plusIcon} alt="Add new" />
            {t("kanbanView.addNew")}
          </ButtonAdd>
        )}
      </HeaderWrap>

      <TableWrap>{renderTable(listKnowledgeSetting)}</TableWrap>

      <ModalSetting
        form={valueForm}
        setValueForm={setValueForm}
        showModal={showModal}
        setShowModal={setShowModal}
      />
      <ModalConfimDelete
        title={t("knowledgeBase.titleDelete")}
        decs={t("knowledgeBase.desDelete")}
        methodDelete={deleteSettingEnhancement}
        dataDelete={{ record_id: recordID }}
        isLoading={showLoadingScreen}
      />
      <ModalConfirm
        title="Confirm"
        decs="This action will delete all data in the current index and import it to a new index. Proceed with caution and at your own risk !!!"
        method={reIndex}
        data={{ object_id: idActive }}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={setShowConfirm}
      />
    </Wrapper>
  );
};

export default EnhancementSetting;

const Wrapper = styled.div`
  padding: 24px;
  .ant-tag {
    cursor: pointer;
    padding: 0 16px;
  }
`;

const HeaderWrap = styled.div`
  display: flex;
  justify-content: space-between;
  .ant-breadcrumb li {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.45);
    cursor: pointer;
    :last-child {
      color: #2c2c2c;
      font-size: 18px;
      font-family: var(--roboto-500);
      cursor: default;
    }
  }
`;

const ButtonAdd = styled.div`
  cursor: pointer;
  border-radius: 2px;
  background: ${(props) => props.theme.main};
  border: solid 1px ${(props) => props.theme.darker};
  transition: all 0.5s;
  display: flex;
  align-items: center;
  font-size: 16px;
  color: #fff;
  padding: 8px 12px;
  img {
    margin-right: 8px;
  }
  :hover {
    background: ${(props) => props.theme.darker};
  }
`;

const TableWrap = styled.div`
  width: 100%;
  background: #fff;
  margin-top: 20px;
  border-radius: 5px;
  padding: 24px;

  table {
    width: 100%;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;

    .ant-switch {
      width: 54px;
    }
    .ant-switch-checked {
      background-color: ${(props) => props.theme.main};
    }

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      font-size: 16px;
      padding: 10px 16px;
      text-align: left;
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      background: #f0f0f0;
      &:first-child {
        border-left: none;
        width: 54px;
        max-width: 54px;
        box-shadow: inset -1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:nth-child(2) {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }
      &:last-child {
        border-right: none;
        resize: none;
        width: 122px;
        max-width: 122px;
      }
    }

    .table-body td {
      border-bottom: 1px solid #ddd;
      padding: 6px 16px;
      max-width: 200px;
      &:last-child {
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
        img {
          width: 18px;
          margin-right: 16px;
          &:hover {
            cursor: pointer;
          }
        }
      }
    }

    tr {
      :hover {
        background: #f5f5f5;
      }
    }
  }
`;

const TagWrap = styled.div`
  display: flex;
  .ant-tag {
    :last-child {
      padding: 0 6px;
    }
  }
`;
