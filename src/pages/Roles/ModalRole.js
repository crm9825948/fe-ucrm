import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Space from "antd/lib/space";

import { Notification } from "components/Notification/Noti";
import {
  loadListRoles,
  createRole,
  resetRoleInfo,
  loadDetailsRole,
  resetRoleDetails,
  updateRole,
} from "redux/slices/roles";

function ModalRole({
  showModalRole,
  _onHideModalRole,
  roleName,
  roleID,
  isEdit,
  setIsEdit,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { listProfile } = useSelector((state) => state.profileReducer);
  const { isLoading, roleInfo, details } = useSelector(
    (state) => state.rolesReducer
  );

  const [form] = Form.useForm();

  const [optionPrivileges, setOptionPrivileges] = useState([]);

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateRole({
          Name: values.name.trim(),
          ID: details._id,
          Assign_Records_To: values.assign,
          Role_Profile: values.privileges,
        })
      );
    } else {
      dispatch(
        createRole({
          Name: values.name.trim(),
          Parent_Role: roleID,
          Assign_Records_To: values.assign,
          Role_Profile: values.privileges,
        })
      );
    }
  };

  const _onCancel = () => {
    _onHideModalRole();
    form.resetFields();
    setIsEdit(false);
    dispatch(resetRoleDetails());
  };

  const assignOptions = [
    {
      label: t("role.allUsers"),
      value: 1,
    },
    {
      label: t("role.sameRole"),
      value: 2,
    },
    {
      label: t("role.subordinateRole"),
      value: 3,
    },
  ];

  useEffect(() => {
    if (showModalRole) {
      if (isEdit) {
        dispatch(loadDetailsRole({ _id: roleID }));
      } else {
        form.setFieldsValue({
          report_to: roleName,
        });
      }
    }
  }, [dispatch, form, isEdit, roleID, roleName, showModalRole]);

  useEffect(() => {
    if (details._id !== undefined) {
      form.setFieldsValue({
        report_to: details.Parent_Name || details.Name,
        name: details.Name,
        privileges: details.Role_Profile,
        assign: details.Assign_Records_To,
      });
    }
  }, [details, form, roleName]);

  useEffect(() => {
    if (listProfile.length > 0) {
      let tempOptions = [];
      listProfile.map((item) => {
        return tempOptions.push({
          label: item.Name,
          value: item._id,
        });
      });
      setOptionPrivileges(tempOptions);
    }
  }, [listProfile]);

  useEffect(() => {
    if (isLoading === false) {
      if (roleInfo === "success") {
        _onHideModalRole();
        form.resetFields();
        setIsEdit(false);
        dispatch(loadListRoles());
        dispatch(resetRoleDetails());

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetRoleInfo());
      }

      if (roleInfo !== null && roleInfo !== "success") {
        Notification("error", roleInfo);
        dispatch(resetRoleInfo());
      }
    }
  }, [
    _onHideModalRole,
    dispatch,
    form,
    isEdit,
    isLoading,
    roleInfo,
    setIsEdit,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("role.editRole") : t("role.createRole")}
      visible={showModalRole}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("user.name")}
          name="name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} maxLength={200} />
        </Form.Item>
        <Form.Item
          label={t("user.reportTo")}
          name="report_to"
          rules={[{ required: true }]}
        >
          <Input disabled />
        </Form.Item>
        <Form.Item
          label={t("role.canAssignRecordsTo")}
          name="assign"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Radio.Group>
            <Space direction="vertical">
              {assignOptions.map((item) => {
                return (
                  <Radio key={item.value} value={item.value}>
                    {item.label}
                  </Radio>
                );
              })}
            </Space>
          </Radio.Group>
        </Form.Item>
        <Form.Item
          label={t("role.privileges")}
          name="privileges"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            mode="multiple"
            placeholder={t("common.placeholderSelect")}
            options={optionPrivileges}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalRole);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
