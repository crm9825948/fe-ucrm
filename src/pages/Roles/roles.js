import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import "./roles.scss";
import Tree from "antd/lib/tree";
import Popover from "antd/lib/popover";
import { CaretDownOutlined } from "@ant-design/icons";
import Breadcrumb from "antd/lib/breadcrumb";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Button from "antd/lib/button";

import DeleteRoles from "assets/icons/roles/DeleteRoles.svg";
import EditRoles from "assets/icons/roles/EditRoles.svg";
import PlusRoles from "assets/icons/roles/PlusRoles.svg";

import ModalRole from "./ModalRole";
import ModalDelete from "./ModalDelete";
import { loadDataNecessary, exportRoles } from "redux/slices/roles";
import { changeTitlePage } from "redux/slices/authenticated";

function Roles(props) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { Text } = Typography;

  const { listRoles } = useSelector((state) => state.rolesReducer);
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [showModalRole, setShowModalRole] = useState(false);
  const [roleName, setRoleName] = useState("");
  const [roleID, setRoleID] = useState("");
  const [expandedKeys, $expandedKeys] = useState([]);

  const [isEdit, setIsEdit] = useState(false);

  const [showModalDelete, setShowModalDelete] = useState(false);
  useEffect(() => {
    dispatch(changeTitlePage(t("settings.role")));
    //eslint-disable-next-line
  }, [t]);

  const _onShowModalRole = (name, id, edit) => {
    setShowModalRole(true);
    setRoleName(name);
    setRoleID(id);
    setIsEdit(edit);
  };

  const _onHideModalRole = () => {
    setShowModalRole(false);
  };

  const _onShowModalDelete = (name, id) => {
    setShowModalDelete(true);
    setRoleName(name);
    setRoleID(id);
  };

  const _onHideModalDelete = () => {
    setShowModalDelete(false);
  };

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "role" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onExpand = (value) => {
    $expandedKeys(value);
  };

  const action = (name, id) => (
    <WrapAction>
      {checkRule("create") && (
        <Tooltip title={t("common.add")}>
          <img
            onClick={() => _onShowModalRole(name, id, false)}
            src={PlusRoles}
            alt="add"
          />
        </Tooltip>
      )}

      {checkRule("edit") && (
        <Tooltip title={t("common.edit")}>
          <img
            onClick={() => _onShowModalRole(name, id, true)}
            src={EditRoles}
            alt="edit"
          />
        </Tooltip>
      )}

      {listRoles[0].key !== id && checkRule("delete") && (
        <Tooltip title={t("common.delete")}>
          <img
            onClick={() => _onShowModalDelete(name, id)}
            src={DeleteRoles}
            alt="delete"
          />
        </Tooltip>
      )}
    </WrapAction>
  );

  useEffect(() => {
    if (!_.isEmpty(listRoles)) {
      const expand = [];
      const expandMethod = (arr) => {
        arr.forEach((data) => {
          expand.push(data.key);
          if (data.children) {
            expandMethod(data.children);
          }
        });
      };
      expandMethod(listRoles);
      $expandedKeys(expand);
    }
  }, [listRoles]);

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.role")}</BreadcrumbItem>
        </Breadcrumb>
        {checkRule("edit") && (
          <ExportButton onClick={() => dispatch(exportRoles())}>
            Export roles
          </ExportButton>
        )}
      </WrapBreadcrumb>

      <Tree
        onExpand={_onExpand}
        expandedKeys={expandedKeys}
        treeData={listRoles}
        showLine={{ showLeafIcon: false }}
        switcherIcon={<CaretDownOutlined />}
        titleRender={(nodeData) => (
          <Popover
            content={action(nodeData.title, nodeData.key)}
            placement="right"
            trigger="hover"
            overlayClassName="popoverRoles"
          >
            <Text ellipsis={{ tooltip: nodeData.title }}>{nodeData.title}</Text>
          </Popover>
        )}
      />

      <ModalRole
        showModalRole={showModalRole}
        _onHideModalRole={_onHideModalRole}
        roleName={roleName}
        roleID={roleID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
      />

      <ModalDelete
        showModalDelete={showModalDelete}
        setShowModalDelete={setShowModalDelete}
        _onHideModalDelete={_onHideModalDelete}
        roleID={roleID}
        roleName={roleName}
      />
    </Wrapper>
  );
}

export default withTranslation()(Roles);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-tree {
    margin-top: 16px;
    min-height: calc(100vh - 210px);
    padding: 24px;

    .ant-tree-treenode {
      width: 90%;
    }
  }

  .ant-tree-title {
    span {
      font-size: 16px;
      color: #2c2c2c;
    }
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapAction = styled.div`
  img {
    margin-right: 10px;

    :hover {
      background: #ececec;
      cursor: pointer;
    }
  }
`;

const ExportButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;
