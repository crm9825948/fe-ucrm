import { Input, Pagination, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadLog } from "redux/slices/tenants";
import styled from "styled-components";
import { BASE_URL_API, FE_URL } from "constants/constants";
import axios from "axios";
import { CSVLink } from "react-csv";
import { checkTokenExpiration } from "contexts/TokenCheck";
const { Option } = Select;

const AuditLog = (props) => {
  const { auditLog } = useSelector((state) => state.tenantsReducer);
  const { userDetail } = useSelector((state) => state.userReducer);
  const dispatch = useDispatch();
  const [recordPerPage, setRecordPerPage] = useState(20);
  const [currentPage, setCurrentPage] = useState(1);
  const [dataExport, setDataExport] = useState([]);
  const [searchData, setSearchData] = useState({
    email: "",
    type_of_action: "",
    action: "",
    modified_date: "",
    content: "",
  });
  useEffect(() => {
    if (
      userDetail &&
      userDetail.permission &&
      userDetail.permission.role === "normal"
    ) {
      window.open(FE_URL + "/tenants", "_self");
    }
  }, [userDetail]);

  useEffect(() => {
    dispatch(
      loadLog({
        page: currentPage,
        limit: recordPerPage,
        search_data: searchData,
      })
    );
    // eslint-disable-next-line
  }, [currentPage, recordPerPage, dispatch]);

  const handleSearch = () => {
    dispatch(
      loadLog({
        page: currentPage,
        limit: recordPerPage,
        search_data: searchData,
      })
    );
  };

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "audit-log/get",
          {
            page: 1,
            limit: 10000,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          setDataExport(res.data.data.results);
        })
        .catch((err) => {});
    };
    checkToken();
  }, []);

  return (
    <Wrapper>
      <div style={{ display: "flex", justifyContent: "right" }}>
        <CSVLink data={dataExport} filename={"tenants.csv"}>
          <ExportButton>
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M21 15V19C21 19.5304 20.7893 20.0391 20.4142 20.4142C20.0391 20.7893 19.5304 21 19 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V15"
                stroke="#20A2A2"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M7 10L12 15L17 10"
                stroke="#20A2A2"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
              <path
                d="M12 15V3"
                stroke="#20A2A2"
                stroke-width="1.5"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
            Export
          </ExportButton>
        </CSVLink>
      </div>

      <div className="container">
        <table>
          <thead>
            <tr>
              <th className="th1">Author</th>
              <th className="th1">Type of action</th>
              <th className="th1">Action</th>
              <th className="th1">Date</th>
              <th className="th1">Content</th>
            </tr>
            <tr>
              <th className="th2">
                <Input
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["email"] = e.target.value;
                    setSearchData(tmp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                  }}
                />
              </th>
              <th className="th2">
                {/* <Input
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["type_of_action"] = e.target.value;
                    setSearchData(tmp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                  }}
                /> */}
                <Select
                  style={{ width: "100%" }}
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["type_of_action"] = e;
                    setSearchData(tmp);
                    dispatch(
                      loadLog({
                        page: currentPage,
                        limit: recordPerPage,
                        search_data: { ...searchData, type_of_action: e },
                      })
                    );
                  }}
                  allowClear
                  onClear={() => {
                    let tmp = { ...searchData };
                    tmp["type_of_action"] = "";
                    setSearchData(tmp);
                  }}
                >
                  <Option value={"effect_tenant"}>effect_tenant</Option>
                  <Option value={"effect_permission"}>effect_permission</Option>
                </Select>
              </th>
              <th className="th2">
                {/* <Input
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["action"] = e.target.value;
                    setSearchData(tmp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                  }}
                /> */}
                <Select
                  style={{ width: "100%" }}
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["action"] = e;
                    setSearchData(tmp);
                    dispatch(
                      loadLog({
                        page: currentPage,
                        limit: recordPerPage,
                        search_data: { ...searchData, action: e },
                      })
                    );
                  }}
                  allowClear
                  onClear={() => {
                    let tmp = { ...searchData };
                    tmp["action"] = "";
                    setSearchData(tmp);
                  }}
                >
                  <Option value="login_root">login_root</Option>
                  <Option value="enable_tenant">enable_tenant</Option>
                  <Option value="create_tenant">create_tenant</Option>
                  <Option value="update_tenant">update_tenant</Option>
                  <Option value="delete_tenant">delete_tenant</Option>
                  <Option value="disable_tenant">disable_tenant</Option>
                  <Option value="clone_tenant">clone_tenant</Option>
                  <Option value="update_role">update_role</Option>
                  <Option value="update_permission">update_permission</Option>
                  <Option value="create_permission">create_permission</Option>
                  <Option value="delete_permission">delete_permission</Option>
                  <Option value="reset_root_password">
                    reset_root_password
                  </Option>
                </Select>
              </th>
              <th className="th2">
                <Input
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["modified_date"] = e.target.value;
                    setSearchData(tmp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                  }}
                />
              </th>
              <th className="th2">
                <Input
                  onChange={(e) => {
                    let tmp = { ...searchData };
                    tmp["content"] = e.target.value;
                    setSearchData(tmp);
                  }}
                  onPressEnter={() => {
                    handleSearch();
                  }}
                />
              </th>
            </tr>
          </thead>
          <tbody>
            {auditLog &&
              auditLog.results &&
              auditLog.results.map((item, idx) => {
                return (
                  <tr>
                    <td>{item.email}</td>
                    <td>{item.type_of_action}</td>
                    <td>{item.action}</td>
                    <td>{item.created_date}</td>
                    <td>{item.content}</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
        <div
          style={{
            width: "100%",
            background: "white",
            paddingBottom: "16px",
          }}
        >
          <CustomPagination
            showQuickJumper
            current={currentPage}
            total={auditLog && auditLog.total_records}
            showSizeChanger
            showTotal={(total, range) =>
              `${range[0]}-${range[1]} of ${total} records`
            }
            pageSize={recordPerPage}
            onChange={(e, pageSize) => {
              setCurrentPage(e);
              setRecordPerPage(pageSize);
            }}
          />
        </div>
      </div>
    </Wrapper>
  );
};

export default AuditLog;

const Wrapper = styled.div`
  padding: 24px;
  .container {
    padding: 24px;
    background-color: white;
  }
  table {
    border-collapse: collapse;
    width: 100%;
  }
  td {
    border-bottom: 1px solid #ededed;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    text-align: left;
    padding: 8px;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 21px */

    /* Character/Body text */

    color: #2c2c2c;
  }
  th {
    border-bottom: 1px solid #ededed;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    text-align: left;
    padding: 8px;
    /* background-color: #fff; */
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */

    color: #2c2c2c;
    background: #fafafa;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    /* Character/Body text */
  }
  .ant-checkbox-checked {
    background-color: ${(props) => props.theme.main};
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }

  table {
    text-align: left;
    position: relative;
    border-collapse: collapse;
  }
  .th1 {
    background: white;
    position: sticky;
    top: 0; /* Don't forget this, required for the stickiness */
    box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
  }
  .th2 {
    background: white;
    position: sticky;
    top: 36px; /* Don't forget this, required for the stickiness */
    box-shadow: 0 2px 2px -1px rgba(0, 0, 0, 0.4);
  }
`;

const ExportButton = styled.button`
  img {
    width: 18px;
    margin-right: 12px;
  }
  a {
    text-decoration: none;
    color: #20a2a2;
  }
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border: 1px solid #10827b;
  border-radius: 5px;
  padding: 8px 16px;
  background-color: transparent;

  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 17px;
  line-height: 20px;
  /* identical to box height */
  margin-bottom: 24px;
  display: flex;
  align-items: center;
  color: #10827b;
  cursor: pointer;
  svg {
    margin-right: 12px;
  }
  :hover {
    img {
      width: 18px;
      margin-right: 12px;
    }
    display: flex;
    flex-direction: row;
    justify-content: center;
    align-items: center;
    border: 1px solid #20a2a2;
    border-radius: 5px;
    padding: 8px 16px;
    background-color: #20a2a2;

    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 17px;
    line-height: 20px;
    /* identical to box height */

    display: flex;
    align-items: center;

    color: white;
    svg {
      filter: brightness(200);
    }
  }
`;

const CustomPagination = styled(Pagination)`
  /* position: fixed; */
  /* bottom: 50px; */
  text-align: right;
  background-color: #fff;
  padding: 16px;
  border: 1px solid #ececec;
  margin: auto;
  margin-top: 16px;
  margin-left: 0;
  margin-right: 0;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;
