import { useEffect, useState } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Table from "antd/lib/table";
import Breadcrumb from "antd/lib/breadcrumb";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";

import { changeTitlePage } from "redux/slices/authenticated";
import {
  modalConfig,
  getListTemplate,
  deleteTemplate,
} from "redux/slices/templateFile";
import { getListAccountMapping } from "redux/slices/googleIntegration";
import { loadObject } from "redux/slices/objectsManagement";

import ModalConfig from "./ModalConfig";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";

function TemplateFile(props) {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listTemplate } = useSelector((state) => state.templateFileReducer);
  const [templateDetail, $templateDetail] = useState({});
  const [idDelete, $idDelete] = useState("");

  const columns = [
    {
      title: t("TemplateFile.fileName"),
      dataIndex: "file_name",
      key: "file_name",
    },
    {
      title: t("TemplateFile.fileType"),
      dataIndex: "file_type",
      key: "file_type",
      render: (text) => (
        <span style={{ textTransform: "capitalize" }}>{text}</span>
      ),
    },
    {
      title: t("common.object"),
      dataIndex: "object_name",
      key: "object_name",
    },
    {
      title: t("common.action"),
      dataIndex: "action",
      key: "action",
      render: (text, record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                onClick={() => {
                  $templateDetail(record);
                  dispatch(modalConfig(true));
                }}
                src={Edit}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => {
                  $idDelete(record._id);

                  dispatch(setShowModalConfirmDelete(true));
                }}
                src={Delete}
                alt="delete"
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  const [listObjects, setListObjects] = useState([]);
  const [dataTable, $dataTable] = useState([]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "template_file" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    const result = listTemplate.map((item) => {
      return (item = {
        ...item,
        key: item._id,
      });
    });
    $dataTable(result);
  }, [listTemplate]);

  useEffect(() => {
    dispatch(getListAccountMapping());
    dispatch(loadObject());
    dispatch(getListTemplate());
  }, [dispatch]);

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.templateFile")));
    //eslint-disable-next-line
  }, [t]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.templateFile")}</BreadcrumbItem>
        </Breadcrumb>
        {listTemplate.length > 0 && checkRule("create") && (
          <AddButton onClick={() => dispatch(modalConfig(true))}>
            + {t("TemplateFile.addTemplateFile")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listTemplate.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("settings.templateFile")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={() => dispatch(modalConfig(true))}>
              + {t("TemplateFile.addTemplateFile")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table columns={columns} dataSource={dataTable} />
        </WrapTable>
      )}

      <ModalConfig
        listObjects={listObjects}
        templateDetail={templateDetail}
        $templateDetail={$templateDetail}
      />
      <ModalConfimDelete
        title={t("knowledgeBase.titleDelete")}
        decs={t("knowledgeBase.desDelete")}
        methodDelete={deleteTemplate}
        dataDelete={{ _id: idDelete }}
      />
    </Wrapper>
  );
}

export default withTranslation()(TemplateFile);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;

    :last-child {
      margin-left: 8px;
    }

    :hover {
      background: #eeeeee;
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;
