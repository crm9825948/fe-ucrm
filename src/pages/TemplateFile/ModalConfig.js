import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Tooltip from "antd/lib/tooltip";

import {
  modalConfig,
  newFile,
  copyFile,
  uploadTemplate,
  editTemplate,
} from "redux/slices/templateFile";
import { loadListObjectField } from "redux/slices/objects";
import { Notification } from "components/Notification/Noti";

import DeleteIcon from "assets/icons/common/delete.svg";
import { CopyOutlined } from "@ant-design/icons";
const fileTypeOptions = [
  {
    label: "Sheets",
    value: "sheets",
  },
  {
    label: "Docs",
    value: "docs",
  },
];

function ModalConfig({ listObjects, templateDetail, $templateDetail }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { showModalConfig, isLoading, targetURL } = useSelector(
    (state) => state.templateFileReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { listAccountMapping } = useSelector(
    (state) => state.googleIntegrationReducer
  );
  const [listFields, $listFields] = useState([]);
  const [fields, $fields] = useState({
    selected_fields: [],
    named_fields: [],
  });
  const [fileType, $fileType] = useState("");
  const [url, $url] = useState("");

  const _onSubmit = (values) => {
    let checkRequired = false;

    if (
      !_.isEmpty(fields.selected_fields.filter((item) => !item)) ||
      !_.isEmpty(fields.named_fields.filter((item) => !item))
    ) {
      checkRequired = true;
    }

    if (checkRequired) {
      Notification("warning", "Please fullfill or remove empty field!");
    } else {
      if (!_.isEmpty(templateDetail)) {
        dispatch(
          editTemplate({
            _id: templateDetail._id,
            file_name: _.get(values, "file_name"),
            file_type: _.get(values, "file_type"),
            object: _.get(values, "object"),
            selected_fields: fields.selected_fields,
            named_fields: fields.named_fields,
            file_id: _.get(values, "file_id"),
            oauth2_id: _.get(values, "oauth2_id"),
          })
        );
      } else {
        dispatch(
          uploadTemplate({
            file_name: _.get(values, "file_name"),
            file_type: _.get(values, "file_type"),
            object: _.get(values, "object"),
            selected_fields: fields.selected_fields,
            named_fields: fields.named_fields,
            file_id: _.get(values, "file_id"),
            oauth2_id: _.get(values, "oauth2_id"),
          })
        );
      }
    }
  };

  const _onCancel = () => {
    dispatch(modalConfig(false));
    form.resetFields();
    $fields({
      selected_fields: [],
      named_fields: [],
    });
    $listFields([]);
  };

  const _onAddField = () => {
    const selected_fields = [...fields.selected_fields];
    const named_fields = [...fields.named_fields];

    $fields({
      selected_fields: [...selected_fields, undefined],
      named_fields: [...named_fields, ""],
    });
  };

  const _onRemoveField = (index) => {
    const selected_fields = [...fields.selected_fields];
    const named_fields = [...fields.named_fields];

    selected_fields.splice(index, 1);
    named_fields.splice(index, 1);

    $fields({
      selected_fields: [...selected_fields],
      named_fields: [...named_fields],
    });
  };

  const onChangeFields = (type, index) => {
    return (e) => {
      switch (type) {
        case "selected_fields":
          let selected_fields = [...fields.selected_fields];
          selected_fields[index] = e;
          $fields({
            selected_fields: [...selected_fields],
            named_fields: [...fields.named_fields],
          });
          break;

        default:
          let named_fields = [...fields.named_fields];
          named_fields[index] = e.target.value;
          $fields({
            selected_fields: [...fields.selected_fields],
            named_fields: [...named_fields],
          });
          break;
      }
    };
  };

  const handleSelectObject = (value) => {
    $fields({
      selected_fields: [],
      named_fields: [],
    });
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
        show_meta_fields: false,
      })
    );
  };

  const _onNew = () => {
    dispatch(
      newFile({
        file_name: form.getFieldValue("file_name"),
        file_type: form.getFieldValue("file_type"),
        oauth2_id: form.getFieldValue("oauth2_id"),
      })
    );
  };

  const _onMakeCopy = () => {
    dispatch(
      copyFile({
        file_id: form.getFieldValue("template_url")?.split("/")[5],
        file_name: form.getFieldValue("file_name"),
        oauth2_id: form.getFieldValue("oauth2_id"),
      })
    );
  };

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempOptionsFields.push({
                label: field.related_name,
                value: field.full_field_id,
                type: field.type,
              });
            }
          });
        });
      }
    });

    $listFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (!_.isEmpty(templateDetail)) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: templateDetail.object,
          show_meta_fields: false,
        })
      );
      $fields({
        selected_fields: templateDetail.selected_fields,
        named_fields: templateDetail.named_fields,
      });
      $fileType(templateDetail.file_type);
      $url(templateDetail.file_id);
      form.setFieldsValue({
        file_name: templateDetail.file_name,
        file_type: templateDetail.file_type,
        object: templateDetail.object,
        file_id: templateDetail.file_id,
        oauth2_id: templateDetail.oauth2_id,
      });
    }
    //eslint-disable-next-line
  }, [templateDetail]);

  useEffect(() => {
    form.setFieldValue("file_id", targetURL);
  }, [form, targetURL]);
  useEffect(() => {
    if (!showModalConfig) {
      _onCancel();
      $templateDetail({});
      $fileType("");
      $url("");
    }
    //eslint-disable-next-line
  }, [showModalConfig]);

  return (
    <ModalCustom
      title={t("settings.templateFile")}
      visible={showModalConfig}
      footer={null}
      onCancel={_onCancel}
      width={700}
    >
      <Form form={form} onFinish={_onSubmit} colon={false} layout="vertical">
        <Form.Item
          label={t("TemplateFile.fileName")}
          name="file_name"
          rules={[
            {
              required: true,
              message: t("common.placeholderInput"),
            },
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label={t("TemplateFile.fileType")}
          name="file_type"
          rules={[
            {
              required: true,
              message: t("common.placeholderSelect"),
            },
          ]}
        >
          <Select
            onChange={(value) => {
              $fileType(value);
            }}
            disabled={!_.isEmpty(templateDetail)}
            options={fileTypeOptions}
          />
        </Form.Item>

        <Form.Item
          label="Oauth2"
          name="oauth2_id"
          rules={[
            {
              required: true,
              message: t("common.placeholderSelect"),
            },
          ]}
        >
          <Select>
            {listAccountMapping.map((item, idx) => (
              <Select.Option
                key={_.get(item, "_id", idx)}
                value={_.get(item, "_id", idx)}
              >
                {_.get(item, "email", "")}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label={t("common.object")}
          name="object"
          rules={[
            {
              required: true,
              message: t("common.placeholderSelect"),
            },
          ]}
        >
          <Select
            disabled={!_.isEmpty(templateDetail)}
            options={listObjects}
            onChange={handleSelectObject}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <Note>{t("workflow.fields")}:</Note>

        {_.map(fields.selected_fields, (field, idx) => (
          <WrapField key={idx}>
            <Field>
              <Select
                value={field}
                options={listFields}
                placeholder={t("workflow.selectField")}
                onChange={onChangeFields("selected_fields", idx)}
              />
            </Field>

            <Field>
              <Input
                value={_.get(fields, `named_fields[${idx}]`)}
                onChange={onChangeFields("named_fields", idx)}
              />

              <Delete>
                <Tooltip title="Delete">
                  <img
                    src={DeleteIcon}
                    onClick={() => _onRemoveField(idx)}
                    alt="delete"
                  />
                </Tooltip>
              </Delete>
            </Field>
          </WrapField>
        ))}

        <AddFields onClick={_onAddField}>
          <span>+ {t("workflow.addField")}</span>
        </AddFields>

        <Form.Item
          label={t("TemplateFile.templateURL")}
          className="multiple-children"
        >
          <Form.Item name="template_url" noStyle>
            <Input
              disabled={!_.isEmpty(templateDetail)}
              style={{ width: "59%" }}
            />
          </Form.Item>

          <ButtonCustom
            type="primary"
            style={{ width: "19%" }}
            onClick={_onMakeCopy}
            loading={isLoading.copyFile}
            disabled={!_.isEmpty(templateDetail)}
          >
            {t("TemplateFile.makeCopy")}
          </ButtonCustom>
          <ButtonCustom
            type="primary"
            style={{ width: "19%" }}
            onClick={_onNew}
            loading={isLoading.newFile}
            disabled={!_.isEmpty(templateDetail)}
          >
            {t("TemplateFile.new")}
          </ButtonCustom>
        </Form.Item>

        <WrapUrl>
          <Form.Item
            label={t("TemplateFile.targetURL")}
            name="file_id"
            rules={[
              {
                required: true,
                message: t("common.placeholderInput"),
              },
            ]}
          >
            <Input
              onChange={(e) => $url(e.target.value)}
              disabled={!_.isEmpty(templateDetail)}
            />
          </Form.Item>
          {fileType && url && (
            <CopyOutlined
              onClick={() => {
                let text = `https://docs.google.com/${
                  fileType === "sheets" ? "spreadsheets" : "document"
                }/d/${url}/edit#`;
                navigator.clipboard.writeText(text);
                Notification("success", "Copy success");
              }}
            />
          )}
        </WrapUrl>

        <WrapButton>
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalConfig);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .multiple-children {
    .ant-form-item-control-input-content {
      display: flex;
      justify-content: space-around;
    }
  }
  .ant-btn-primary[disabled],
  .ant-btn-primary[disabled]:hover {
    border-color: #d9d9d9 !important;
    background: #f5f5f5 !important;
    span {
      color: rgba(0, 0, 0, 0.25);
    }
  }
`;

const Note = styled.div`
  margin-bottom: 24px;
  font-size: 16px;
`;

const WrapField = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Field = styled.div`
  width: 49%;
  display: flex;
  margin-bottom: 24px;

  .ant-select {
    width: 100%;
  }
`;

const AddFields = styled.div`
  margin-bottom: 24px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const ButtonCustom = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};

  span {
    color: #fff;
    /* font-size: 16px; */
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 8px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const WrapUrl = styled.div`
  width: 100%;
  position: relative;
  svg {
    position: absolute;
    right: 6px;
    top: 42px;
  }
`;
