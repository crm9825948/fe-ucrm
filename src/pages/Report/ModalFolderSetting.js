import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";

import { Notification } from "components/Notification/Noti";
import {
  createFolder,
  resetReportInfo,
  updateFolder,
} from "redux/slices/report";

function FolderSetting({
  showFolderSetting,
  onHideFolderSetting,
  isEdit,
  setIsEdit,
  getListFolders,
  listAllGroups,
  folderDetails,
  setFolderDetails,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoading, reportInfo } = useSelector((state) => state.reportReducer);

  const [listMembers, setListMembers] = useState([]);

  useEffect(() => {
    let tempGroup = [];
    if (listAllGroups.length > 0) {
      listAllGroups.map((item) => {
        return tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });
    }
    setListMembers(tempGroup);
  }, [listAllGroups]);

  const [form] = Form.useForm();
  const { TextArea } = Input;

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateFolder({
          report_id: folderDetails._id,
          description: values.description,
          name: values.folder_name,
          share_to_groups: values.share_group,
        })
      );
    } else {
      dispatch(
        createFolder({
          description: values.description,
          name: values.folder_name,
          share_to_groups: values.share_group,
        })
      );
    }
  };

  const _onCancel = () => {
    onHideFolderSetting();
    form.resetFields();
    setIsEdit(false);
    setFolderDetails({});
  };

  useEffect(() => {
    if (isEdit) {
      form.setFieldsValue({
        folder_name: folderDetails.name,
        description: folderDetails.description,
        share_group: folderDetails.share_to_groups,
      });
    }
  }, [form, isEdit, folderDetails]);

  useEffect(() => {
    if (isLoading === false && showFolderSetting) {
      if (reportInfo === "success") {
        onHideFolderSetting();
        form.resetFields();
        getListFolders();
        setIsEdit(false);
        setFolderDetails({});

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetReportInfo());
      }

      if (reportInfo !== null && reportInfo !== "success") {
        Notification("error", reportInfo);
        dispatch(resetReportInfo());
      }
    }
  }, [
    dispatch,
    form,
    getListFolders,
    reportInfo,
    isLoading,
    onHideFolderSetting,
    setIsEdit,
    setFolderDetails,
    isEdit,
    showFolderSetting,
  ]);

  return (
    <ModalCustom
      title={t("report.folderSetting")}
      visible={showFolderSetting}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("report.folderName")}
          name="folder_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>
        <Form.Item label={t("common.description")} name="description">
          <TextArea rows={6} placeholder={t("common.placeholderSelect")} />
        </Form.Item>

        <Form.Item label={t("report.shareToGroup")} name="share_group">
          <Select
            options={listMembers}
            placeholder={t("common.placeholderSelect")}
            mode="multiple"
            optionFilterProp="label"
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(FolderSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
