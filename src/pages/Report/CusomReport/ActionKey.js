import React, { forwardRef } from "react";
import styled from "styled-components";
import DragIcon from "../../../assets/icons/common/DrawIcon.svg";
import DeleteIcon from "../../../assets/icons/common/delete.png";
import EditIcon from "../../../assets/icons/common/edit.png";
import { Button, Checkbox } from "antd";
import { useDispatch } from "react-redux";
import { updateIsChange, updateListActionKey } from "redux/slices/customReport";
import { Notification } from "components/Notification/Noti";

const ActionKey = forwardRef(
  (
    {
      setDataDelete,
      setShowConfirmDelete,
      setOpen,
      setDetailKey,
      listActionKey,
      actionKey,
      index,
      dragHandleProps,
      snapshot,
      isRun,

      ...props
    },
    ref
  ) => {
    const dispatch = useDispatch();
    function numberWithCommas(x) {
      if (x === 0) {
        return "0";
      } else return x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    const handleDeleteKey = () => {
      let flag = false;
      listActionKey.forEach((key) => {
        if (
          key.value_type === "related" &&
          key.data_value.value.includes(actionKey.key_id)
        ) {
          flag = true;
        }
      });
      if (flag) {
        Notification("warning", "You can delete this key!");
      } else {
        setDataDelete(actionKey);
        setShowConfirmDelete(true);
      }
    };
    return (
      <Wrap
        ref={ref}
        {...props}
        className={snapshot.isDragging ? "hovering" : ""}
      >
        <img {...dragHandleProps} src={DragIcon} alt="" />
        <ItemKey>{actionKey.key_id}</ItemKey>
        <InfoKey>
          <p>{actionKey.key_name}</p>
          <span>{actionKey.description_action}</span>
        </InfoKey>
        <TypeKey>
          <span>Type</span>
          <p>{actionKey.value_type}</p>
        </TypeKey>
        <DetailKey>
          {actionKey.value_type === "static" ? (
            <p>{numberWithCommas(actionKey.data_value.value)}</p>
          ) : actionKey.value_type === "related" ? (
            <span>{actionKey.data_value.value}</span>
          ) : (
            <>
              <p>
                Object: <span>{actionKey.data_value.object_name}</span>
              </p>
              <p>
                Field: <span>{actionKey.data_value.field_name}</span>
              </p>
              <p>
                Aggregation: <span>{actionKey.data_value.aggregation}</span>
              </p>
            </>
          )}
        </DetailKey>
        <WrapAction>
          <Checkbox
            disabled={isRun}
            checked={actionKey.hidden}
            onChange={(e) => {
              let tem = [];
              listActionKey.forEach((item) => {
                if (item.key_id === actionKey.key_id) {
                  tem.push({ ...item, hidden: e.target.checked });
                } else {
                  tem.push(item);
                }
              });
              dispatch(updateListActionKey(tem));
              dispatch(updateIsChange(true));
            }}
            id={`ucrm_custom_report_hidden_key_${index}`}
          >
            Hidden
          </Checkbox>
          <WrapImg
            disabled={isRun}
            onClick={() => {
              setDetailKey(actionKey);
              setOpen(true);
            }}
            id={`ucrm_custom_report_edit_key_${index}`}
          >
            <img src={EditIcon} alt="edit" />
          </WrapImg>
          <WrapImg
            disabled={isRun}
            onClick={() => handleDeleteKey()}
            id={`ucrm_custom_report_delete_key_${index}`}
          >
            <img src={DeleteIcon} alt="delete" />
          </WrapImg>
        </WrapAction>
      </Wrap>
    );
  }
);

export default ActionKey;

const Wrap = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  padding: 8px 0;
  border-bottom: 1px solid #ececec;
  justify-content: space-between;
  align-items: center;
  font-size: 16px;
  color: #2c2c2c;
  p {
    font-size: 16px !important;
    margin-bottom: 0;
    line-height: 22px !important;
    font-family: var(--roboto-500) !important;
  }
`;

const ItemKey = styled.div`
  width: 60px;
  height: 60px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  background: #ffe4cb;
`;

const InfoKey = styled.div`
  width: 30%;
  span {
    font-size: 14px;
    color: #6b6b6b;
    word-break: break-all;
  }
`;
const TypeKey = styled.div`
  width: 10%;
  span {
    font-size: 14px;
  }
`;
const DetailKey = styled.div`
  width: 20%;
  span {
    font-family: var(--roboto-400);
    word-wrap: break-word;
  }
`;

const WrapAction = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .ant-checkbox-wrapper {
    font-size: 16px;
  }
`;

const WrapImg = styled(Button)`
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
  border: 1px solid #d9d9d9;
  margin-left: 8px;
  cursor: pointer;
  img {
    width: 16px;
    height: 16px;
  }
`;
