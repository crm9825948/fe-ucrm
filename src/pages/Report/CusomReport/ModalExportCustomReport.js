import React from "react";
import styled from "styled-components";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import { useDispatch } from "react-redux";
import { exportCustomReport } from "redux/slices/customReport";

function ModalExportCustomReport({ showModal, setShowModal, reportDetail }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const fileType = [
    {
      label: "xlsx",
      value: "xlsx",
    },
    {
      label: "csv",
      value: "csv",
    },
    {
      label: "pdf",
      value: "pdf",
    },
  ];

  const _onSubmit = (values) => {
    setShowModal(false);
    dispatch(
      exportCustomReport({
        data: {
          report_id: reportDetail._id,
          report_type: values.report_type,
        },
        name: reportDetail.name,
      })
    );
    form.resetFields();
  };

  const _onCancel = () => {
    form.resetFields();
    setShowModal(false);
  };

  return (
    <ModalCustom
      title={`Export report`}
      visible={showModal}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label="Select file type"
          name="report_type"
          rules={[{ required: true, message: "Please select file type" }]}
        >
          <Select options={fileType} placeholder="Please select" />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            Export
          </Button>
          <Button onClick={_onCancel}>Cancel</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalExportCustomReport;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
