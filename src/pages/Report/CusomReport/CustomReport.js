import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { changeTitlePage } from "redux/slices/authenticated";
import { Button, Checkbox, Tabs, Spin } from "antd";
import ModalKeys from "./ModalKey/ModalKeys";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import {
  createCustomReport,
  deleteActionKey,
  loadDetailCustomReport,
  loadDetailCustomReportSuccess,
  runReport,
  updateCustomReport,
  updateListActionKey,
  loadDataReport,
  loadDataReportSuccess,
  updateIsRun,
  updateIsChange,
  cancelReport,
} from "redux/slices/customReport";
import ActionKey from "./ActionKey";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import { useParams } from "react-router";
import EmptyDashboard from "assets/icons/common/nodata.png";
import WarningIcon from "assets/icons/common/warningIcon.svg";
import SuccesssIcon from "assets/icons/common/successIcon.svg";

import {
  StepForwardOutlined,
  ExclamationCircleOutlined,
  CloseCircleOutlined,
  SyncOutlined,
  LoadingOutlined,
} from "@ant-design/icons";
import Excel from "../../../assets/icons/report/Excel.svg";
import ModalExportCustomReport from "./ModalExportCustomReport";

const CustomReport = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { reportID } = useParams();
  const { reportDetail, listActionKey, result, isRun, isChange } = useSelector(
    (state) => state.customReportReducer
  );

  const [showReport, setShowReport] = useState(true);
  const [showKey, setShowKey] = useState(true);
  const [open, setOpen] = useState(false);
  const [detailKey, setDetailKey] = useState({});
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const [dataDelete, setDataDelete] = useState({});
  const [showModal, setShowModal] = useState(false);
  const [activeKey, setActiveKey] = useState("1");

  useEffect(() => {
    dispatch(changeTitlePage(t("customReport.customReport")));
  }, [t, dispatch]);

  useEffect(() => {
    dispatch(updateIsChange(false));
  }, [dispatch]);

  useEffect(() => {
    if (reportID) {
      dispatch(
        loadDetailCustomReport({
          api_version: "2",
          report_id: reportID,
        })
      );
      dispatch(
        loadDataReport({
          report_id: reportID,
        })
      );
    } else {
      dispatch(loadDetailCustomReportSuccess({}));
      dispatch(loadDataReportSuccess({}));
    }
  }, [reportID, dispatch]);

  useEffect(() => {
    if (Object.entries(reportDetail).length > 0) {
      setShowReport(reportDetail.show_report);
      setShowKey(reportDetail.show_key);
      dispatch(updateListActionKey(reportDetail.action_key));
      dispatch(updateIsRun(reportDetail.running_status));
    } else {
      setShowReport(true);
      setShowKey(true);
      dispatch(updateListActionKey([]));
      dispatch(updateIsRun(false));
    }
  }, [reportDetail, dispatch]);

  const dragEnded = (param) => {
    const { source, destination } = param;
    if (source == null || destination == null) return;
    let _arr = [...listActionKey];
    const _item = _arr.splice(source.index, 1)[0];
    _arr.splice(destination.index, 0, _item);
    dispatch(updateListActionKey(_arr));
    dispatch(updateIsChange(true));
  };
  const handleCustomReport = () => {
    dispatch(updateIsChange(false));
    let tem = listActionKey.map((key) => {
      return {
        ...key,
        data_value: {
          ...key.data_value,
          object_name: undefined,
          field_name: undefined,
        },
      };
    });
    let temp = {
      name: reportDetail.name,
      show_key: showKey,
      show_report: showReport,
      action_key: tem,
      description: reportDetail.description,
      report_type: "custom_report",
      folder_id: reportDetail.folder_id,
      main_object: "",
    };
    if (!reportID) {
      dispatch(createCustomReport(temp));
    } else {
      dispatch(
        updateCustomReport({
          data: temp,
          report_id: reportID,
        })
      );
    }
  };
  function numberWithCommas(x) {
    if (x === 0) {
      return "0";
    } else return x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  const renderResult = (list) =>
    list.map((item, index) => (
      <tr key={index}>
        <CustomTd>
          {item.value !== "$error" ? (
            <CustomColor>{item.key_id}</CustomColor>
          ) : (
            <img style={{ width: "38px" }} src={WarningIcon} alt="warning" />
          )}
        </CustomTd>
        <CustomTd>{item.key_name}</CustomTd>
        <td style={{ fontSize: "14px", color: "#6b6b6b" }}>
          {item.value !== "$error" ? (
            item.description_action
          ) : (
            <span>
              {t("customReport.warning")}
              <br />
              {t("customReport.please")}
              <span
                onClick={() => {
                  dispatch(
                    runReport({ report_id: reportID, key_id: item.key_id })
                  );
                }}
                style={{
                  color: "#d46b08",
                  cursor: "pointer",
                  textDecoration: "underline",
                }}
              >
                {t("customReport.tryAgain")}
              </span>
            </span>
          )}
        </td>
        <CustomTd style={{ fontSize: "22px" }}>
          {item.value !== "$error" && numberWithCommas(item.value)}
        </CustomTd>
      </tr>
    ));
  const checkTime = () => {
    let tem = false;
    if (Object.entries(result).length > 0) {
      if (
        new Date(result.modified_date).getTime() -
          new Date(result.last_running_time).getTime() >
        0
      ) {
        tem = true;
      }
    }
    return tem;
  };

  const item1 = [
    {
      key: "1",
      label: t("customReport.keyConfig"),
      children: "",
    },
  ];
  const item2 = [
    {
      key: "1",
      label: t("customReport.keyConfig"),
      children: "",
    },
    {
      key: "2",
      label: t("customReport.result"),
      children: "",
    },
  ];

  return (
    <Wrap>
      <ContentWrap>
        <Setting>
          <CustomFlex>
            <div style={{ width: "200px" }}>
              <Tabs
                items={reportID ? item2 : item1}
                activeKey={activeKey}
                onChange={(key) => setActiveKey(key)}
              />
            </div>
            <div style={{ display: "flex" }}>
              {activeKey === "2" && (
                <LastRun>
                  <SyncOutlined
                    style={{ marginLeft: "10px" }}
                    onClick={() =>
                      dispatch(
                        loadDataReport({
                          report_id: reportID,
                        })
                      )
                    }
                  />
                  {result.last_running_time && (
                    <span>
                      {t("customReport.lastRunning")}
                      {result.last_running_time}
                    </span>
                  )}
                </LastRun>
              )}
              {!isRun ? (
                <>
                  {isChange ? (
                    <ButtonSave
                      onClick={() => handleCustomReport()}
                      id="ucrm_custom_report_save"
                    >
                      {t("common.save")}
                    </ButtonSave>
                  ) : (
                    <ButtonSave
                      onClick={() => {
                        dispatch(runReport({ report_id: reportID }));
                      }}
                      id="ucrm_custom_report_run_report"
                    >
                      <StepForwardOutlined />
                      {t("customReport.runReport")}
                    </ButtonSave>
                  )}
                </>
              ) : (
                <ButtonCancel
                  onClick={() =>
                    dispatch(cancelReport({ report_id: reportID }))
                  }
                >
                  <CloseCircleOutlined />
                  {t("common.cancel")}
                </ButtonCancel>
              )}
            </div>
          </CustomFlex>
          {activeKey === "1" ? (
            <ConfigWrap>
              <p>{reportDetail && reportDetail.name}</p>
              <p>
                <span>{reportDetail && reportDetail.description}</span>
              </p>
              <InfoConfig>
                <div>
                  <Checkbox
                    disabled={isRun}
                    checked={showReport}
                    onChange={(value) => {
                      setShowReport(value.target.checked);
                      dispatch(updateIsChange(true));
                    }}
                    id="ucrm_custom_report_show_report"
                  >
                    {t("customReport.showReport")}
                  </Checkbox>
                  <Checkbox
                    disabled={isRun}
                    checked={showKey}
                    onChange={(value) => {
                      setShowKey(value.target.checked);
                      dispatch(updateIsChange(true));
                    }}
                    id="ucrm_custom_report_show_key"
                  >
                    {t("customReport.showKey")}
                  </Checkbox>
                </div>
              </InfoConfig>

              <KeyWrap>
                {listActionKey.length === 0 ? (
                  <NoData>
                    <img src={EmptyDashboard} alt="" />
                    <p>
                      {t("customReport.noData")}
                      <span> {t("customReport.key")}</span>
                    </p>
                  </NoData>
                ) : (
                  <DragDropContext onDragEnd={dragEnded}>
                    <Droppable droppableId="comments-wrapper">
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.droppableProps}
                        >
                          {listActionKey.map((actionKey, index) => {
                            return (
                              <Draggable
                                isDragDisabled={isRun}
                                draggableId={`comment-${actionKey.key_id}`}
                                index={index}
                                key={actionKey.key_id}
                              >
                                {(_provided, _snapshot) => (
                                  <ActionKey
                                    ref={_provided.innerRef}
                                    dragHandleProps={_provided.dragHandleProps}
                                    {..._provided.draggableProps}
                                    snapshot={_snapshot}
                                    actionKey={actionKey}
                                    index={index}
                                    setDetailKey={setDetailKey}
                                    setOpen={setOpen}
                                    listActionKey={listActionKey}
                                    setShowConfirmDelete={setShowConfirmDelete}
                                    setDataDelete={setDataDelete}
                                    isRun={isRun}
                                  />
                                )}
                              </Draggable>
                            );
                          })}
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                )}
                <ButtonAdd
                  disabled={isRun}
                  onClick={() => setOpen(true)}
                  id="ucrm_custom_report_add_key"
                >
                  + {t("customReport.addKey")}
                </ButtonAdd>
              </KeyWrap>
            </ConfigWrap>
          ) : (
            <>
              <WrapResult>
                <div>
                  <p>{result && result.report_name}</p>
                  <p>
                    <span>{result && result.description}</span>
                  </p>
                </div>
                {Object.entries(result).length > 0 && (
                  <ButtonExport
                    onClick={() => setShowModal(true)}
                    id="ucrm_custom_report_export_report"
                  >
                    <img src={Excel} alt="" />
                    Export
                  </ButtonExport>
                )}
              </WrapResult>
              {checkTime() && (
                <Noti>
                  <ExclamationCircleOutlined />
                  Current data may not be accurate!
                </Noti>
              )}
              <table>
                <thead>
                  <th>{t("customReport.key")}</th>
                  <th>{t("customReport.content")}</th>
                  <th>{t("customReport.description")}</th>
                  <th>{t("customReport.value")}</th>
                </thead>
                <tbody>{renderResult(result.data ? result.data : [])} </tbody>
              </table>
              {!result.data && (
                <NoData style={{ marginTop: "16px" }}>
                  <img src={EmptyDashboard} alt="" />
                  <p>
                    {t("customReport.no")}
                    <span> {t("customReport.result")}</span>
                  </p>
                </NoData>
              )}
            </>
          )}
        </Setting>
        <Result>
          <p>{reportDetail && reportDetail.name}</p>

          <ContentResult>
            <InfoResult>
              <ItemInfo>
                <span>{t("customReport.description")}:</span>
                <span> {reportDetail && reportDetail.description}</span>
              </ItemInfo>
              <ItemInfo>
                <span>{t("customReport.folderName")}</span>
                <span> {reportDetail && reportDetail.folder_name}</span>
              </ItemInfo>

              <ItemInfo>
                <span>{t("customReport.createdDate")}</span>
                <span> {reportDetail.created_date}</span>
              </ItemInfo>

              <ItemInfo>
                <span>{t("customReport.modifiedTime")}</span>
                <span> {reportDetail.modified_date}</span>
              </ItemInfo>
            </InfoResult>
          </ContentResult>

          <NotiWrap>
            {isRun ? (
              <>
                <Spin
                  indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />}
                />
                <WrapInfo>
                  <span>{t("customReport.runRunning")}</span>
                  <span>
                    {t("customReport.runRunning")}
                    <span
                      onClick={() =>
                        dispatch(cancelReport({ report_id: reportID }))
                      }
                      style={{
                        color: "#E84F3D",
                        cursor: "pointer",
                        textDecoration: "underline",
                      }}
                    >
                      {t("common.cancel")}
                    </span>
                  </span>
                </WrapInfo>
              </>
            ) : (
              <>
                <img
                  src={result.key_error ? WarningIcon : SuccesssIcon}
                  alt=""
                />
                <WrapInfo>
                  <span>
                    {result.key_error
                      ? t("customReport.runFailed")
                      : t("customReport.runSuccess")}
                  </span>
                  <span>
                    {result.key_error ? (
                      t("customReport.desFailed")
                    ) : (
                      <span
                        onClick={() => setActiveKey("2")}
                        style={{
                          color: "#1C9292",
                          textDecoration: "underline",
                          cursor: "pointer",
                        }}
                      >
                        {t("customReport.desSuccess")}
                      </span>
                    )}
                    {result.key_error && (
                      <span
                        onClick={() =>
                          dispatch(
                            runReport({
                              report_id: reportID,
                              key_id: result.key_error,
                            })
                          )
                        }
                        style={{
                          color: "#D46B08",
                          cursor: "pointer",
                          textDecoration: "underline",
                        }}
                      >
                        {t("customReport.tryAgain")}
                      </span>
                    )}
                  </span>
                </WrapInfo>
              </>
            )}
          </NotiWrap>
        </Result>
      </ContentWrap>
      <ModalKeys
        open={open}
        setOpen={setOpen}
        detailKey={detailKey}
        setDetailKey={setDetailKey}
      />
      <ModalConfirmDelete
        title={t("customReport.thisKey")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteActionKey}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={showConfirmDelete}
        setOpenConfirm={setShowConfirmDelete}
      />
      <ModalExportCustomReport
        showModal={showModal}
        setShowModal={setShowModal}
        reportDetail={reportDetail}
      />
    </Wrap>
  );
};

export default CustomReport;

const Wrap = styled.div`
  padding: 24px;
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-tabs-nav::before {
    border-bottom: none;
  }
  .ant-tabs-tab {
    padding: 0px 0px 8px 0px;
  }
  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn,
  .ant-tabs-tab:hover,
  .ant-tabs-tab-btn:focus {
    color: ${(props) => props.theme.main};
    text-shadow: none;
    font-size: 16px;
  }
  .ant-tabs-tab-btn {
    font-size: 16px;
  }
  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }
`;
const ContentWrap = styled.div`
  display: flex;
  justify-content: space-between;
`;
const Setting = styled.div`
  padding: 16px;
  flex: 1;
  margin-right: 16px;
  border-radius: 6px;
  border: 1px solid #ececec;
  background: #fff;
  font-size: 16px;
  table {
    width: 100%;
    border: 1px solid #ececec;
    border-radius: 6px;
    th {
      text-align: left;
      background: #fafafa;
      padding: 8px 16px;
      border-bottom: 1px solid #ececec;
      border-right: 1px solid #ececec;
    }
    td {
      padding: 8px 16px;
    }
    tr {
      border-bottom: 1px solid #f0f0f0;
      :last-child {
        border-bottom: none;
      }
    }
  }
`;
const Result = styled.div`
  width: 30%;
  height: fit-content;
  padding: 24px;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ececec;
  p {
    font-family: var(--roboto-500);
    font-size: 18px;
    width: 100%;
    border-bottom: 1px solid #ececec;
    padding-bottom: 14px;
  }
`;

const ConfigWrap = styled.div`
  border-top: 1px solid #ececec;
  font-size: 16px;
  padding-top: 16px;
  p {
    font-family: var(--roboto-500);
    margin-bottom: 0;
    span {
      color: #637381;
      font-family: var(--roboto-400);
    }
  }
`;
const InfoConfig = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  .ant-checkbox-wrapper {
    font-size: 16px;
  }
  margin: 12px 0;
`;

const KeyWrap = styled.div`
  width: 100%;
  .ant-btn[disabled],
  .ant-btn[disabled]:hover,
  .ant-btn[disabled]:focus,
  .ant-btn[disabled]:active {
    background: #fff;
  }
`;
const ButtonAdd = styled(Button)`
  font-size: 16px;
  color: ${(props) => props.theme.main};
  :hover,
  :focus,
  :active {
    color: ${(props) => props.theme.main};
  }
  border: none;
  cursor: pointer;
  box-shadow: none;
  margin-top: 12px;
`;

const CustomButton = styled(Button)`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border-radius: 6px;
  font-size: 16px;
  font-family: var(--roboto-400);
  color: #fff;
  border: none;
  :hover,
  :focus {
    color: #fff;
    border: none;
  }
`;

const ButtonSave = styled(CustomButton)`
  background: ${(props) => props.theme.main};
  :hover,
  :focus {
    background: ${(props) => props.theme.main};
  }
`;

const ButtonCancel = styled(CustomButton)`
  background: #f04b4b;
  :hover,
  :focus {
    background: #f04b4b;
  }
`;

const NoData = styled.div`
  width: 100%;
  text-align: center;
  img {
    width: 100px;
  }
  p {
    margin-top: 8px;
    font-size: 16px;
    font-family: var(--roboto-400);
    color: #000;
    span {
      color: ${(props) => props.theme.main};
    }
  }
`;

const ContentResult = styled.div`
  width: 100%;
  height: 100%;
`;
const InfoResult = styled.div`
  flex: 1;
  p {
    margin-bottom: 0;
    font-size: 18px;
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }
  span {
    font-size: 16px;
    color: #6b6b6b;
  }
`;
const ItemInfo = styled.div`
  margin-bottom: 16px;
  span {
    font-size: 16px;
    :first-child {
      font-family: var(--roboto-500);
      color: #252424;
    }
    :last-child {
      color: #2c2c2c;
    }
  }
`;

const ButtonExport = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 6px;
  height: 32px;
  cursor: pointer;
  background: #fff;
  border: 1px solid #d9d9d9;
  padding: 0 16px;
  img {
    margin-right: 8px;
  }
`;

const Noti = styled.div`
  font-size: 16px;
  color: #637381;
  margin-bottom: 8px;
  svg {
    color: #fa8c16;
    margin-right: 8px;
  }
`;

const CustomFlex = styled.div`
  width: 100%;
  justify-content: space-between;
  display: flex;
`;

const LastRun = styled.div`
  border-radius: 6px;
  background: #f9fafc;
  display: flex;
  align-items: center;
  padding: 0 12px;
  height: 40px;
  font-size: 16px;
  color: #637381;
  svg {
    margin-right: 12px;
  }
`;

const WrapResult = styled(CustomFlex)`
  font-size: 16px;
  border-top: 1px solid #ececec;
  padding-top: 16px;
  margin-bottom: 16px;
  p {
    font-family: var(--roboto-500);
    margin-bottom: 0;
    span {
      color: #637381;
      font-family: var(--roboto-400);
    }
  }
`;

const CustomTd = styled.td`
  font-family: var(--roboto-500);
`;

const CustomColor = styled.div`
  border-radius: 6px;
  background: #ffe4cb;
  padding: 8px;
  max-width: 64px;
`;

const NotiWrap = styled.div`
  height: 60px;
  border-radius: 6px;
  border: 1px solid #ececec;
  width: 100%;
  display: flex;

  align-items: center;
  padding: 12px;
  svg {
    color: ${(props) => props.theme.main};
  }
`;

const WrapInfo = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 12px;
  span {
    :first-child {
      font-size: 16px;
      font-family: var(--roboto-500);
    }
    :last-child {
      color: #637381;
      font-size: 14px;
    }
  }
`;
