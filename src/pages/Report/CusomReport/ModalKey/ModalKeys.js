import React, { useRef, useState } from "react";
import {
  Modal,
  Button,
  Form,
  Input,
  Select,
  InputNumber,
  Checkbox,
} from "antd";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import TimeRangeFilter from "components/TimeRangeFilter/TimeRangeFilter";
import { useEffect } from "react";
import { loadListObjectField } from "redux/slices/objects";
import _ from "lodash";
import { updateIsChange, updateListActionKey } from "redux/slices/customReport";
import Conditions from "components/Conditions/conditions";
import { Notification } from "components/Notification/Noti";

const { Option } = Select;
const initialTimeRange = {
  filter_field_id: "modify_time",
  field_type: "datetime-local",
  filter_type: "all-time",
  amount_of_time: null,
  start_time: null,
  end_time: null,
};
const initialMeasure = {
  aggregation: "",
  field_id: "",
  field_type: "",
  formula_type: "",
  type: "default",
};
const measuresOptions = [
  {
    label: "Count",
    value: "count",
  },
];

const measuresOptionsNumber = [
  {
    label: "Count",
    value: "count",
  },
  {
    label: "Sum",
    value: "sum",
  },
  {
    label: "Average",
    value: "average",
  },
  {
    label: "Min",
    value: "min",
  },
  {
    label: "Max",
    value: "max",
  },
];
const ModalKeys = ({ detailKey, setDetailKey, open, setOpen }) => {
  const { t } = useTranslation();
  const { listActionKey } = useSelector((state) => state.customReportReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const dispatch = useDispatch();

  const [form] = Form.useForm();
  const [type, setType] = useState("static");
  const [timeRangeFilter, $timeRangeFilter] = useState(initialTimeRange);
  const [objects, setObjects] = useState([]);
  const [field, setField] = useState([]);
  const [measure, setMeasure] = useState(initialMeasure);
  const [insertField, setInsertField] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const ref = useRef(null);

  useEffect(() => {
    if (listObjectField.length > 0) {
      let tempField = [];
      listObjectField.forEach((item) => {
        if (Object.keys(item)[0] === "main_object") {
          item.main_object.sections.forEach((section) => {
            section.fields.forEach((field) => {
              if (!_.get(field, "encrypted", false)) {
                tempField.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  formula_type: !field?.in_words && (field?.formula_type || ""),
                });
              }
            });
          });
        }
      });
      setField(tempField);
    }
  }, [listObjectField]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    if (Object.entries(detailKey).length > 0) {
      const {
        hidden,
        data_value,
        description_action,
        key_name,
        key_id,
        value_type,
      } = detailKey;
      form.setFieldsValue({
        key_name: key_name,
        key_id: key_id,
        value_type: value_type,
        hidden: hidden,
        description_action: description_action,
      });
      setType(value_type);

      if (value_type === "static" || value_type === "related") {
        form.setFieldsValue({ value: data_value.value });
      } else {
        if (open) {
          dispatch(
            loadListObjectField({
              api_version: "2",
              object_id: data_value.object_id,
              show_meta_fields: true,
            })
          );
        }
        $timeRangeFilter(data_value.time_range_filter);
        form.setFieldsValue({
          aggregation: data_value.aggregation,
          field_id: data_value.field_id,
          object_id: data_value.object_id,
          object_name: data_value.object_name,
          field_name: data_value.field_name,
        });
        setMeasure({
          aggregation: data_value.aggregation,
          field_id: data_value.field_id,
          field_type: data_value.field_type,
          formula_type: data_value.formula_type,
          type: "default",
        });
        let temCondition = JSON.parse(data_value.filter_condition);
        setAllCondition(temCondition.and_filter);
        setAnyCondition(temCondition.or_filter);
      }
    } else {
      form.setFieldsValue({ key_id: `key_${checkIndex(listActionKey)}` });
    }
  }, [form, detailKey, dispatch, listActionKey, open]);
  useEffect(() => {
    if (!open) {
      form.resetFields();
      setDetailKey({});
      setType("static");
      $timeRangeFilter(initialTimeRange);
      setField([]);
      setMeasure(initialMeasure);
      setAllCondition([]);
      setAnyCondition([]);
    }
    //eslint-disable-next-line
  }, [open]);

  const handleFinish = (values) => {
    let tem = [...listActionKey];

    let temData = {
      key_id: values.key_id,
      key_name: values.key_name,
      value_type: values.value_type,
      hidden: values.hidden,
      description_action: values.description_action,
    };
    if (values.value_type === "static" || values.value_type === "related") {
      temData = {
        ...temData,
        data_value: {
          value: values.value,
        },
      };
    } else {
      let temCondition = {
        and_filter: allCondition,
        or_filter: anyCondition,
      };
      temData = {
        ...temData,
        data_value: {
          filter_condition: JSON.stringify(temCondition),
          time_range_filter: timeRangeFilter,
          aggregation: values.aggregation,
          field_id: values.field_id,
          object_id: values.object_id,
          field_name: values.field_name,
          object_name: values.object_name,
          field_type: measure.field_type,
          formula_type: measure.formula_type,
          type: measure.type,
        },
      };
    }
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      for (let i = 0; i < operatorValueAnd.length; i++) {
        if (operatorValueAnd[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }

    if (operatorValueOr.length === anyCondition.length) {
      for (let i = 0; i < operatorValueOr.length; i++) {
        if (operatorValueOr[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }

    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "today" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-today" &&
        item === "" &&
        operatorValueAnd[idx] !== "yesterday" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-year" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        item === "" &&
        operatorValueOr[idx] !== "not-empty" &&
        item === "" &&
        operatorValueOr[idx] !== "mine" &&
        item === "" &&
        operatorValueOr[idx] !== "not-mine" &&
        item === "" &&
        operatorValueOr[idx] !== "today" &&
        item === "" &&
        operatorValueOr[idx] !== "not-today" &&
        item === "" &&
        operatorValueOr[idx] !== "yesterday" &&
        item === "" &&
        operatorValueOr[idx] !== "this-week" &&
        item === "" &&
        operatorValueOr[idx] !== "last-week" &&
        item === "" &&
        operatorValueOr[idx] !== "this-month" &&
        item === "" &&
        operatorValueOr[idx] !== "last-month" &&
        item === "" &&
        operatorValueOr[idx] !== "this-year" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    if (values.value_type === "rollup" && flag) {
      Notification("warning", "Please fullfill information!");
    } else {
      if (Object.entries(detailKey).length > 0) {
        let index = tem.findIndex((item) => item.key_id === values.key_id);
        if (index !== -1) {
          tem[index] = temData;
        }
      } else {
        tem.push(temData);
      }
      setOpen(false);
      dispatch(updateListActionKey(tem));
      dispatch(updateIsChange(true));
    }
  };

  const handleCancel = () => {
    setOpen(false);
  };
  const handleSelectField = (value, option) => {
    setMeasure({
      ...measure,
      field_id: option.value,
      field_type: option.type,
      formula_type: option.formula_type,
      aggregation: "",
    });

    form.setFieldsValue({
      aggregation: undefined,
      field_name: option.label,
    });
  };
  const handleSelectMeasure = (value) => {
    setMeasure({
      ...measure,
      aggregation: value,
    });
  };
  const handleChangeObject = (value, option) => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
        show_meta_fields: true,
      })
    );
    setAllCondition([]);
    setAnyCondition([]);
    $timeRangeFilter(initialTimeRange);
    setMeasure(initialMeasure);
    form.setFieldsValue({
      field_id: undefined,
      aggregation: undefined,
      object_name: option.label,
    });
  };
  const handleOptionInsert = (list, position) => {
    let tem = [...list];
    tem.sort(function (a, b) {
      var numA = parseInt(a.key_id.split("_")[1]);
      var numB = parseInt(b.key_id.split("_")[1]);
      return numA - numB;
    });
    return tem.map(
      (item, index) =>
        parseInt(item.key_id.split("_")[1]) <
          parseInt(position.split("_")[1]) && (
          <Option key={index} value={item.key_id}>
            {item.key_id}
          </Option>
        )
    );
  };
  const checkIndex = (list) => {
    let tem = 0;
    list.forEach((item) => {
      let number = parseInt(item.key_id.split("_")[1]);
      if (tem < number) {
        tem = number;
      }
    });
    return tem + 1;
  };
  return (
    <ModalCustom
      title={t("customReport.keyDetail")}
      visible={open}
      footer={null}
      width={800}
      onCancel={handleCancel}
    >
      <Form
        form={form}
        onFinish={handleFinish}
        layout="vertical"
        initialValues={{
          value_type: "static",
          hidden: false,
        }}
      >
        <Form.Item label={t("customReport.keyID")} name="key_id">
          <Input disabled />
        </Form.Item>
        <Form.Item
          label={t("customReport.keyName")}
          name="key_name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>
        <Form.Item label={t("common.description")} name="description_action">
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>
        <Form.Item
          label={t("customReport.type")}
          name="value_type"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            onChange={(value) => {
              setType(value);
              form.setFieldsValue({ value: undefined });
            }}
            disabled={Object.entries(detailKey).length > 0}
          >
            <Option value="static">Static</Option>
            <Option value="rollup">Rollup</Option>
            <Option value="related">Related</Option>
          </Select>
        </Form.Item>
        {type === "static" && (
          <Form.Item
            label={t("customReport.value")}
            name="value"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <InputNumber placeholder={t("common.placeholderInput")} />
          </Form.Item>
        )}
        {type === "rollup" && (
          <>
            <Form.Item
              label={t("customReport.object")}
              name="object_id"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                showSearch
                options={objects}
                placeholder={t("common.placeholderSelect")}
                optionFilterProp="label"
                onChange={(value, option) => handleChangeObject(value, option)}
              ></Select>
            </Form.Item>
            <Form.Item style={{ display: "none" }} name="object_name">
              <Input />
            </Form.Item>
            <TimeRangeFilter
              timeRangeFilter={timeRangeFilter}
              $timeRangeFilter={$timeRangeFilter}
              listObjectField={listObjectField}
            />
            <Conditions
              title={"AND condition"}
              decs={"(All conditions must be met)"}
              conditions={allCondition}
              setConditions={setAllCondition}
              ID={Object.entries(detailKey).length > 0 ? detailKey.key_id : ""}
              dataDetails={
                Object.entries(detailKey).length > 0 ? detailKey : {}
              }
              operatorValue={operatorValueAnd}
              setOperatorValue={setOperatorValueAnd}
              value={valueAnd}
              setValue={setValueAnd}
            />
            <Conditions
              title={"OR condition"}
              decs={"(Any conditions must be met)"}
              conditions={anyCondition}
              setConditions={setAnyCondition}
              ID={Object.entries(detailKey).length > 0 ? detailKey.key_id : ""}
              dataDetails={
                Object.entries(detailKey).length > 0 ? detailKey : {}
              }
              operatorValue={operatorValueOr}
              setOperatorValue={setOperatorValueOr}
              value={valueOr}
              setValue={setValueOr}
            />
            <Form.Item
              label="Field"
              name="field_id"
              rules={[
                {
                  required: true,
                  message: "Please select measure field",
                },
              ]}
            >
              <Select
                placeholder="Select field"
                options={field}
                onChange={handleSelectField}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
            <Form.Item style={{ display: "none" }} name="field_name">
              <Input />
            </Form.Item>
            <Form.Item
              label="Aggregation"
              name="aggregation"
              rules={[
                {
                  required: true,
                  message: "Please select aggregation",
                },
              ]}
            >
              <Select
                onChange={handleSelectMeasure}
                placeholder="Select measure"
                options={
                  measure.field_type === ""
                    ? []
                    : measure.field_type === "number" ||
                      measure.formula_type === "dateDiff" ||
                      measure.formula_type === "dateToParts" ||
                      measure.formula_type === "advanceExpression"
                    ? measuresOptionsNumber
                    : measuresOptions
                }
              />
            </Form.Item>
          </>
        )}
        {type === "related" && (
          <>
            <Select
              style={{ width: "100%", margin: "8px 0px 16px 0px" }}
              placeholder="Insert field"
              mode="multiple"
              onChange={(e) => {
                let tempString = form.getFieldValue("value") || "";
                form.setFieldsValue({
                  value: tempString + `${e}$`,
                });
                setInsertField([]);
                ref.current.focus();
              }}
              value={insertField}
              showSearch
              optionFilterProp="label"
            >
              {handleOptionInsert(
                listActionKey,
                detailKey.key_id
                  ? detailKey.key_id
                  : `key_${checkIndex(listActionKey)}`
              )}
            </Select>
            <Form.Item
              label="Value"
              name="value"
              rules={[
                { required: true, message: t("common.placeholderInput") },
              ]}
            >
              <Input ref={ref} placeholder={t("common.placeholderInput")} />
            </Form.Item>
          </>
        )}
        <Form.Item
          name="hidden"
          valuePropName="checked"
          style={{ marginBottom: "85px" }}
        >
          <Checkbox>{t("customReport.hidden")}</Checkbox>
        </Form.Item>
        <ButtonWrap>
          <CustomButton onClick={handleCancel}>
            {t("common.cancel")}
          </CustomButton>
          <CustomButton htmlType="submit">{t("common.save")}</CustomButton>
        </ButtonWrap>
      </Form>
    </ModalCustom>
  );
};

export default ModalKeys;

const ModalCustom = styled(Modal)`
  position: relative;
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-body {
    padding: 24px 24px 0 24px;
    max-height: 600px;
    overflow: auto;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    display: none;
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-input-number:hover,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  .ant-input-number-handler-wrap {
    display: none;
  }
  .ant-input-number {
    width: 100%;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-modal-footer {
    border: none;
  }
`;
const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  background: #fff;
  position: absolute;
  bottom: 0;
  left: 0;
  padding: 24px;
  border-radius: 10px;
`;
const CustomButton = styled(Button)`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-right: 16px;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  &:last-child {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
    margin-right: 0;
  }
`;
