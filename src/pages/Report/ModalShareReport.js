import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";

import Share from "assets/images/reports/share.webp";

import { Notification } from "components/Notification/Noti";
import {
  updateReport,
  resetReportInfo,
  changeState,
} from "redux/slices/report";

function ShareReport({
  showModalShare,
  onHideModalShare,
  listAllGroups,
  reportDetails,
  isEdit,
}) {
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { isLoading, updateInfo } = useSelector((state) => state.reportReducer);
  const [listMembers, setListMembers] = useState([]);

  const [sharedGroup, setSharedGroup] = useState([]);

  const _onSubmit = (values) => {
    let data = { ...reportDetails.report_info };
    let id = reportDetails.report_info._id;
    delete data._id;

    setSharedGroup(values.share_group);

    data = {
      ...data,
      share_to_groups: values.share_group,
    };

    dispatch(
      updateReport({
        data: data,
        report_id: id,
      })
    );
  };

  const _onCancel = () => {
    onHideModalShare();
    form.resetFields();
  };

  useEffect(() => {
    let tempGroup = [];
    if (listAllGroups.length > 0) {
      listAllGroups.map((item) => {
        return tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });
    }
    setListMembers(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    if (Object.keys(reportDetails).length > 0 && showModalShare) {
      form.setFieldsValue({
        share_group: reportDetails.report_info.share_to_groups,
      });
    }
  }, [form, reportDetails, showModalShare]);

  useEffect(() => {
    if (isLoading === false && showModalShare) {
      if (updateInfo === "success") {
        onHideModalShare();
        form.resetFields();
        dispatch(
          changeState({
            type: "share",
            shared: sharedGroup,
          })
        );

        Notification("success", "Share successfully!");
        dispatch(resetReportInfo());
      }

      if (updateInfo !== null && updateInfo !== "success") {
        Notification("error", updateInfo);
        dispatch(resetReportInfo());
      }
    }
  }, [
    dispatch,
    form,
    isLoading,
    onHideModalShare,
    sharedGroup,
    showModalShare,
    updateInfo,
  ]);

  return (
    <ModalCustom
      title={`${t("report.share")} ${reportDetails?.report_info?.name}`}
      visible={showModalShare}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      <CustomContent>
        <img alt="share" src={Share} />
        <Title>
          {t("report.share")} {t("report.report")}
        </Title>
        <Decs>{t("report.descShare")}</Decs>
      </CustomContent>
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item label={t("report.choiceGroup")} name="share_group">
          <Select
            options={listMembers}
            placeholder={t("common.placeholderSelect")}
            mode="multiple"
            optionFilterProp="label"
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ShareReport);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  img {
    width: 100px;
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #2c2c2c;
  text-align: center;
`;

const Decs = styled.span`
  font-size: 16px;
  text-align: center;
  color: #595959;
  margin-bottom: 24px;
`;
