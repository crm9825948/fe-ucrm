import React, { useEffect, useState, useCallback } from "react";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Empty from "antd/lib/empty";
import Button from "antd/lib/button";
import Menu from "antd/lib/menu";
import Pagination from "antd/lib/pagination";
import Tabs from "antd/lib/tabs";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";

import Plus from "assets/icons/common/plus.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";
import Empty_folder from "assets/images/reports/empty-folder.webp";
import Empty_report from "assets/images/reports/empty-report.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EditGreen from "assets/icons/common/EditGreen.svg";
import DeleteGreen from "assets/icons/common/DeleteGreen.svg";
import ShareGreen from "assets/icons/common/ShareGreen.svg";
import Clone from "assets/icons/report/Clone.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadFolders,
  deleteFolder,
  resetStatusDelete,
  loadReports,
  loadSharedReport,
  deleteReport,
  loadDetailsReport,
  loadDataNecessary,
} from "redux/slices/report";

import { Notification } from "components/Notification/Noti";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import FolderSetting from "./ModalFolderSetting";
import ReportSetting from "./ModalReportSetting";
import ShareReport from "./ModalShareReport";
import CloneReport from "./ModalClone";
import { changeTitlePage } from "redux/slices/authenticated";

const Report = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { TabPane } = Tabs;
  const { Column } = Table;
  const { Text } = Typography;

  const {
    isLoading,
    listFolders,
    totalRecords,
    statusDelete,
    listReports,
    reportDetails,
  } = useSelector((state) => state.reportReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { reportPermission } = useSelector((state) => state.userReducer);

  const [tab, setTab] = useState("my_folder");
  const [folder, setFolder] = useState({
    name: "",
    id: "",
  });

  const [myFolder, setMyFolder] = useState([]);
  const [sharedFolder, setSharedFolder] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  const [showFolderSetting, setShowFolderSetting] = useState(false);
  const [showReportSetting, setShowReportSetting] = useState(false);
  const [showModalShare, setShowModalShare] = useState(false);
  const [showModalClone, setShowModalClone] = useState(false);

  const [isEdit, setIsEdit] = useState(false);
  const [folderDetails, setFolderDetails] = useState({});

  const [dataDelete, setDataDelete] = useState({});
  const [typeDelete, setTypeDelete] = useState("");

  const [reports, setReports] = useState([]);

  const [idClone, setIDClone] = useState("");

  useEffect(() => {
    dispatch(changeTitlePage(t("common.reports")));
    //eslint-disable-next-line
  }, [t]);

  const _onShowFolderSetting = () => {
    setShowFolderSetting(true);
  };

  const _onHideFolderSetting = () => {
    setShowFolderSetting(false);
  };

  const _onShowReportSetting = () => {
    setShowReportSetting(true);
  };

  const _onHideReportSetting = () => {
    setShowReportSetting(false);
  };

  const _onEditReport = (id) => {
    setShowReportSetting(true);
    setIsEdit(true);

    dispatch(
      loadDetailsReport({
        report_id: id,
        api_version: "2",
      })
    );
  };

  const _onCloneReport = (id) => {
    setShowModalClone(true);
    setIDClone(id);
  };

  const _onHideModalClone = () => {
    setShowModalClone(false);
    setIDClone("");
  };

  const _onShowModalShare = (id) => {
    setShowModalShare(true);

    dispatch(
      loadDetailsReport({
        report_id: id,
        api_version: "2",
      })
    );
  };

  const _onHideModalShare = () => {
    setShowModalShare(false);
  };

  const onSelectTab = (e) => {
    setTab(e);

    switch (e) {
      case "my_folder":
        window.location.hash = "my_folders";
        myFolder.forEach((item, idx) => {
          if (idx === 0) {
            setFolder({
              name: item.name,
              id: item._id,
            });
          }
        });
        break;

      case "shared_folders":
        window.location.hash = "shared_folders";
        sharedFolder.forEach((item, idx) => {
          if (idx === 0) {
            setFolder({
              name: item.name,
              id: item._id,
            });
          }
        });
        break;

      case "shared_reports":
        window.location.hash = "shared_reports";
        dispatch(
          loadSharedReport({
            current_page: currentPage,
            record_per_page: recordPerPage,
          })
        );
        break;

      default:
        break;
    }
  };

  const _onSelectFolder = (e, tab) => {
    setTab(tab);
    const found = listFolders.filter((item) => item._id === e.key);
    setFolder({
      name: found[0].name,
      id: found[0]._id,
    });
  };

  const _onEditFolder = (e, data) => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    setIsEdit(true);
    setFolderDetails(data);
    setShowFolderSetting(true);
  };

  const _onDeleteFolder = (e, id) => {
    if (e.stopPropagation) {
      e.stopPropagation();
    }
    setTypeDelete("folder");
    dispatch(setShowModalConfirmDelete(true));
    let tempDataDelete = {
      report_id: id,
    };
    setDataDelete(tempDataDelete);
  };

  const _onDeleteReport = (id) => {
    setTypeDelete("report");
    dispatch(setShowModalConfirmDelete(true));
    let tempDataDelete = {
      report_id: id,
    };
    setDataDelete(tempDataDelete);
  };

  const checkReportPermission = (type, object) => {
    return _.get(reportPermission, `${object}.${type}`, false);
  };

  const getListFolders = useCallback(() => {
    dispatch(loadFolders());
  }, [dispatch]);

  const getListReport = useCallback(() => {
    dispatch(
      loadReports({
        folder_id: folder.id,
        record_per_page: recordPerPage,
        current_page: currentPage,
      })
    );
  }, [currentPage, dispatch, folder, recordPerPage]);

  useEffect(() => {
    getListFolders();
  }, [dispatch, getListFolders]);

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    if (folder.id !== "") {
      dispatch(
        loadReports({
          folder_id: folder.id,
          record_per_page: recordPerPage,
          current_page: currentPage,
        })
      );
    }
  }, [currentPage, dispatch, folder, recordPerPage]);

  useEffect(() => {
    let tempList = [];
    listReports.map((item) => {
      return tempList.push({
        key: item._id,
        report_name: item.name,
        main_object: item.main_object,
        main_object_name: item.main_object_name,
        report_type:
          item.report_type === "custom_report"
            ? "Custom report"
            : item.report_type === "row_summary_report"
            ? "Row summary report"
            : item.report_type,
        description: item.description,
        owner: item.owner,
      });
    });
    setReports(tempList);
  }, [listReports]);

  // useEffect(() => {
  //   let tempMyFolder = [];
  //   let tempSharedFolder = [];
  //   // eslint-disable-next-line array-callback-return
  //   listFolders.map((item, idx) => {
  //     if (idx === 0) {
  //       setFolder({
  //         name: item.name,
  //         id: item._id,
  //       });
  //     }

  //     if (item.readonly) {
  //       tempSharedFolder.push(item);
  //     } else {
  //       tempMyFolder.push(item);
  //     }
  //   });
  //   setMyFolder(tempMyFolder);
  //   setSharedFolder(tempSharedFolder);
  // }, [dispatch, listFolders]);

  useEffect(() => {
    if (statusDelete === "success") {
      Notification("success", "Delete successfully!");
      getListReport();
      dispatch(setShowModalConfirmDelete(false));
      if (typeDelete === "folder") {
        getListFolders();
      }

      dispatch(resetStatusDelete());
    }

    if (statusDelete !== "success" && statusDelete !== null) {
      Notification("error", statusDelete);

      dispatch(resetStatusDelete());
    }
  }, [dispatch, getListFolders, getListReport, statusDelete, typeDelete]);

  const showTotal = () => {
    return `${t("common.total")} ${totalRecords} ${t("common.items")}`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    let tempMyFolder = [];
    let tempSharedFolder = [];
    listFolders.forEach((item, idx) => {
      if (item.readonly) {
        tempSharedFolder.push(item);
      } else {
        tempMyFolder.push(item);
      }
    });

    setMyFolder(tempMyFolder);
    setSharedFolder(tempSharedFolder);

    if (window.location.hash === "#shared_folders") {
      setTab("shared_folders");
      if (tempSharedFolder.length > 0) {
        tempSharedFolder.forEach((item, idx) => {
          if (idx === 0) {
            setFolder({
              name: item.name,
              id: item._id,
            });
          }
        });
      }
    } else {
      listFolders.forEach((item, idx) => {
        if (idx === 0) {
          setFolder({
            name: item.name,
            id: item._id,
          });
        }
      });
    }
  }, [listFolders]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item>{t("report.report")}</Breadcrumb.Item>
        <BreadcrumbItem>
          {tab === "my_folder"
            ? "My folder"
            : tab === "shared_folders"
            ? "Shared folders"
            : "Shared reports"}
        </BreadcrumbItem>
      </Breadcrumb>

      <Tabs activeKey={tab} onChange={onSelectTab}>
        <TabPane tab="My folder" key="my_folder">
          {myFolder.length === 0 ? (
            <Empty
              image={Empty_folder}
              description={
                <p>
                  {t("object.noObject")} <span>Folder report</span>
                </p>
              }
            >
              <AddFolder onClick={_onShowFolderSetting}>
                <img src={Plus} alt="plus" />
                {t("report.newFolder")}
              </AddFolder>
            </Empty>
          ) : (
            <Content>
              <MenuCustom
                onClick={(e) => _onSelectFolder(e, "my_folder")}
                selectedKeys={folder.id}
              >
                <NewFolder onClick={_onShowFolderSetting}>
                  {/* <img src={PlusGreen} alt="plus" /> */}
                  <span>+ {t("report.newFolder")}</span>
                </NewFolder>

                {myFolder.map((item, idx) => {
                  return (
                    <Menu.Item key={item._id}>
                      <Text ellipsis={{ tooltip: item.name }}>{item.name}</Text>
                      <div className="action">
                        <Tooltip title={t("common.edit")}>
                          <img
                            onClick={(e) => _onEditFolder(e, item)}
                            src={Edit}
                            alt="edit"
                            id={`ucrm_report_folder_edit_${idx}`}
                          />
                        </Tooltip>
                        <Tooltip title={t("common.delete")}>
                          <img
                            onClick={(e) => _onDeleteFolder(e, item._id)}
                            src={Delete}
                            alt="delete"
                            id={`ucrm_report_folder_delete_${idx}`}
                          />
                        </Tooltip>
                      </div>
                    </Menu.Item>
                  );
                })}
              </MenuCustom>
              <ListReports>
                {listReports.length === 0 ? (
                  <Empty
                    image={Empty_report}
                    description={
                      <p>
                        {t("object.noObject")} <span>{t("report.report")}</span>
                      </p>
                    }
                  >
                    <AddFolder onClick={_onShowReportSetting}>
                      <img src={Plus} alt="plus" />
                      {t("report.newReport")}
                    </AddFolder>
                  </Empty>
                ) : (
                  <>
                    <Table
                      pagination={false}
                      dataSource={reports}
                      onRow={(record) => {
                        return {
                          onDoubleClick: () => {
                            if (record.report_type === "Custom report") {
                              let temp = {
                                report_name: record.report_name,
                                description: record.description,
                                folder: folder,
                              };
                              localStorage.setItem(
                                "custom_report_crm",
                                JSON.stringify(temp)
                              );
                              navigate(`/custom-report/${record.key}`);
                            } else {
                              navigate(
                                `/report-details/${record.main_object}/${record.key}`
                              );
                            }
                          },
                        };
                      }}
                    >
                      <Column
                        title={t("report.reportName")}
                        dataIndex="report_name"
                        key="report_name"
                        render={(text) => <span>{text}</span>}
                      />
                      <Column
                        title={t("object.object")}
                        dataIndex="main_object_name"
                        key="main_object_name"
                      />
                      <Column
                        title={t("report.reportType")}
                        dataIndex="report_type"
                        key="Report_type"
                      />
                      <Column
                        title={t("common.description")}
                        dataIndex="description"
                        key="description"
                        render={(text, record, idx) => (
                          <>
                            {record.report_type === "Custom report" ? (
                              <div className="action">
                                <Tooltip title={t("common.edit")}>
                                  <img
                                    id={`ucrm_report_edit_${idx}`}
                                    onClick={() => _onEditReport(record.key)}
                                    src={EditGreen}
                                    alt="edit"
                                  />
                                </Tooltip>
                                <Tooltip title={t("report.clone")}>
                                  <img
                                    id={`ucrm_report_duplicate_${idx}`}
                                    src={Clone}
                                    alt="clone"
                                    className={"disabled"}
                                  />
                                </Tooltip>
                                <Tooltip title={t("common.delete")}>
                                  <img
                                    id={`ucrm_report_delete_${idx}`}
                                    onClick={() => _onDeleteReport(record.key)}
                                    src={DeleteGreen}
                                    alt="delete"
                                  />
                                </Tooltip>
                                <Tooltip title={t("report.share")}>
                                  <img
                                    id={`ucrm_report_share_${idx}`}
                                    src={ShareGreen}
                                    alt="share"
                                    className={"disabled"}
                                  />
                                </Tooltip>
                              </div>
                            ) : (
                              <div className="action">
                                <Tooltip title={t("common.edit")}>
                                  <img
                                    id={`ucrm_report_edit_${idx}`}
                                    onClick={() =>
                                      !checkReportPermission(
                                        "Edit_Report",
                                        record.main_object
                                      )
                                        ? () => {}
                                        : _onEditReport(record.key)
                                    }
                                    src={EditGreen}
                                    alt="edit"
                                    className={
                                      !checkReportPermission(
                                        "Edit_Report",
                                        record.main_object
                                      ) && "disabled"
                                    }
                                  />
                                </Tooltip>

                                <Tooltip title={t("report.clone")}>
                                  <img
                                    id={`ucrm_report_duplicate_${idx}`}
                                    onClick={() =>
                                      !checkReportPermission(
                                        "Create_Report",
                                        record.main_object
                                      )
                                        ? () => {}
                                        : _onCloneReport(record.key)
                                    }
                                    src={Clone}
                                    alt="clone"
                                    className={
                                      !checkReportPermission(
                                        "Create_Report",
                                        record.main_object
                                      ) && "disabled"
                                    }
                                  />
                                </Tooltip>

                                <Tooltip title={t("common.delete")}>
                                  <img
                                    id={`ucrm_report_delete_${idx}`}
                                    onClick={() =>
                                      !checkReportPermission(
                                        "Delete_Report",
                                        record.main_object
                                      )
                                        ? () => {}
                                        : _onDeleteReport(record.key)
                                    }
                                    src={DeleteGreen}
                                    alt="delete"
                                    className={
                                      !checkReportPermission(
                                        "Delete_Report",
                                        record.main_object
                                      ) && "disabled"
                                    }
                                  />
                                </Tooltip>

                                <Tooltip title={t("report.share")}>
                                  <img
                                    id={`ucrm_report_share_${idx}`}
                                    onClick={() =>
                                      !checkReportPermission(
                                        "Edit_Report",
                                        record.main_object
                                      )
                                        ? () => {}
                                        : _onShowModalShare(record.key)
                                    }
                                    src={ShareGreen}
                                    alt="share"
                                    className={
                                      !checkReportPermission(
                                        "Edit_Report",
                                        record.main_object
                                      ) && "disabled"
                                    }
                                  />
                                </Tooltip>
                              </div>
                            )}
                            <span>{text ? text : "\xa0"}</span>
                          </>
                        )}
                      />
                    </Table>
                    <Pagination
                      showTotal={showTotal}
                      current={currentPage}
                      pageSize={recordPerPage}
                      total={totalRecords}
                      onChange={handleSelectPage}
                      showSizeChanger
                      onShowSizeChange={_onSizeChange}
                      showQuickJumper
                    />
                  </>
                )}
              </ListReports>
            </Content>
          )}
        </TabPane>
        <TabPane tab="Shared folders" key="shared_folders">
          {sharedFolder.length === 0 ? (
            <Empty
              image={Empty_folder}
              description={
                <p>
                  {t("object.noObject")} <span>Shared Folder report</span>
                </p>
              }
            ></Empty>
          ) : (
            <>
              <Content>
                <MenuCustom
                  onClick={(e) => _onSelectFolder(e, "shared_folders")}
                  selectedKeys={folder.id}
                >
                  <SubNoti>
                    <span>{t("report.folderShareForYou")}</span>
                  </SubNoti>
                  {sharedFolder.map((item) => {
                    return (
                      <Menu.Item key={item._id}>
                        <Text ellipsis={{ tooltip: item.name }}>
                          {item.name}
                        </Text>
                      </Menu.Item>
                    );
                  })}
                </MenuCustom>
                <ListReports>
                  {listReports.length === 0 ? (
                    <Empty
                      image={Empty_report}
                      description={
                        <p>
                          {t("object.noObject")}
                          <span>{t("report.report")}</span>
                        </p>
                      }
                    ></Empty>
                  ) : (
                    <>
                      <Table
                        pagination={false}
                        dataSource={reports}
                        onRow={(record) => {
                          return {
                            onDoubleClick: () => {
                              navigate(
                                `/report-details/${record.main_object}/${record.key}`
                              );
                            },
                          };
                        }}
                      >
                        <Column
                          title={t("report.reportName")}
                          dataIndex="report_name"
                          key="report_name"
                          render={(text) => <span>{text}</span>}
                        />
                        <Column
                          title={t("object.object")}
                          dataIndex="main_object_name"
                          key="main_object_name"
                        />
                        <Column
                          title={t("report.reportType")}
                          dataIndex="report_type"
                          key="Report_type"
                        />
                        <Column
                          title={t("sharing.owner")}
                          dataIndex="owner"
                          key="owner"
                        />
                        <Column
                          title={t("common.description")}
                          dataIndex="description"
                          key="description"
                          render={(text, record) => (
                            <>
                              <div className="action-clone">
                                <Tooltip title={t("report.clone")}>
                                  <img
                                    onClick={() =>
                                      !checkReportPermission(
                                        "Create_Report",
                                        record.main_object
                                      )
                                        ? () => {}
                                        : _onCloneReport(record.key)
                                    }
                                    src={Clone}
                                    alt="clone"
                                    className={
                                      !checkReportPermission(
                                        "Create_Report",
                                        record.main_object
                                      ) && "disabled"
                                    }
                                  />
                                </Tooltip>
                              </div>
                              <span>{text ? text : "\xa0"}</span>
                            </>
                          )}
                        />
                      </Table>
                      <Pagination
                        showTotal={showTotal}
                        current={currentPage}
                        pageSize={recordPerPage}
                        total={totalRecords}
                        onChange={handleSelectPage}
                        showSizeChanger
                        onShowSizeChange={_onSizeChange}
                        showQuickJumper
                      />
                    </>
                  )}
                </ListReports>
              </Content>
            </>
          )}
        </TabPane>
        <TabPane tab="Shared reports" key="shared_reports">
          <>
            {reports.length === 0 ? (
              <Empty
                image={Empty_report}
                description={
                  <p>
                    {t("object.noObject")} <span>report shared</span>
                  </p>
                }
              ></Empty>
            ) : (
              <ListReports fullWidth={tab === "shared_reports"}>
                <Table
                  pagination={false}
                  dataSource={reports}
                  onRow={(record) => {
                    return {
                      onDoubleClick: () => {
                        navigate(
                          `/report-details/${record.main_object}/${record.key}`
                        );
                      },
                    };
                  }}
                >
                  <Column
                    title={t("report.reportName")}
                    dataIndex="report_name"
                    key="report_name"
                    render={(text) => <span>{text}</span>}
                  />
                  <Column
                    title={t("object.object")}
                    dataIndex="main_object_name"
                    key="main_object_name"
                  />
                  <Column
                    title={t("report.reportType")}
                    dataIndex="report_type"
                    key="Report_type"
                  />
                  <Column
                    title={t("sharing.owner")}
                    dataIndex="owner"
                    key="owner"
                  />
                  <Column
                    title={t("common.description")}
                    dataIndex="description"
                    key="description"
                    width="250px"
                    render={(text, record) => (
                      <>
                        <div className="action-clone">
                          <Tooltip title={t("report.clone")}>
                            <img
                              onClick={() =>
                                !checkReportPermission(
                                  "Create_Report",
                                  record.main_object
                                )
                                  ? () => {}
                                  : _onCloneReport(record.key)
                              }
                              src={Clone}
                              alt="clone"
                              className={
                                !checkReportPermission(
                                  "Create_Report",
                                  record.main_object
                                ) && "disabled"
                              }
                            />
                          </Tooltip>
                        </div>
                        <span>{text ? text : "\xa0"}</span>
                      </>
                    )}
                  />
                </Table>
                <Pagination
                  showTotal={showTotal}
                  current={currentPage}
                  pageSize={recordPerPage}
                  total={totalRecords}
                  onChange={handleSelectPage}
                  showSizeChanger
                  onShowSizeChange={_onSizeChange}
                  showQuickJumper
                />
              </ListReports>
            )}
          </>
        </TabPane>

        {myFolder.length > 0 && tab === "my_folder" && (
          <TabPane
            tab={
              <AddReport onClick={_onShowReportSetting}>
                + {t("report.newReport")}
              </AddReport>
            }
            key="create_report"
            disabled
          />
        )}
      </Tabs>

      <FolderSetting
        showFolderSetting={showFolderSetting}
        onHideFolderSetting={_onHideFolderSetting}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        getListFolders={getListFolders}
        listAllGroups={listAllGroups}
        folderDetails={folderDetails}
        setFolderDetails={setFolderDetails}
      />

      <ReportSetting
        showReportSetting={showReportSetting}
        onHideReportSetting={_onHideReportSetting}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        getListReport={getListReport}
        listAllGroups={listAllGroups}
        folder={folder}
        objects={category}
        reportDetails={reportDetails}
        loadInfoReport={() => {}}
        loadReport={() => {}}
        loadPagi={() => {}}
        setColumnReport={() => {}}
        listFolders={listFolders}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={typeDelete === "folder" ? deleteFolder : deleteReport}
        dataDelete={dataDelete}
        isLoading={isLoading}
      />

      <ShareReport
        showModalShare={showModalShare}
        onHideModalShare={_onHideModalShare}
        listAllGroups={listAllGroups}
        reportDetails={reportDetails}
        isEdit={isEdit}
      />

      <CloneReport
        showModalClone={showModalClone}
        myFolder={myFolder}
        idClone={idClone}
        getListReport={getListReport}
        onHideModalClone={_onHideModalClone}
      />
    </Wrapper>
  );
};

export default withTranslation()(Report);

const Wrapper = styled.div`
  padding: 24px;

  p {
    font-size: 16px;
  }

  span {
    font-size: 16px;
  }

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }

  .ant-tabs-nav {
    margin-bottom: 0;
  }

  .ant-tabs-nav-wrap {
    background: #fff;
    border-radius: 10px 10px 0px 0px;
    margin-top: 16px;
    padding-left: 16px;
    height: 56px;
  }

  .ant-tabs-tab-btn {
    font-size: 16px;
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(props) => props.theme.main} !important;
  }

  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }

  .ant-tabs-tab:hover {
    color: ${(props) => props.theme.darker};
  }

  .ant-tabs-tab.ant-tabs-tab-disabled {
    cursor: default;

    .ant-btn {
      display: flex;
      align-items: center;
    }
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
`;

const MenuCustom = styled(Menu)`
  margin-top: 8px;
  width: 392px;
  box-shadow: inset 0px -1px 0px #f0f0f0;
  border-radius: 0px 0px 0px 10px;
  height: 100%;

  .ant-menu-item {
    height: 46px !important;
    line-height: 46px !important;
    border-bottom: 1px solid #f0f0f0;
    margin: 0 !important;
    transition: border-color 0.5s ease;
  }

  .ant-menu-item-selected {
    background: #f0f0f0 !important;
    color: #2c2c2c;
  }

  .ant-menu-item {
    :hover {
      color: #2c2c2c;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
      border: 1px solid #d9d9d9;
    }
  }

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  .ant-menu-title-content {
    display: flex;
    justify-content: space-between;
    align-items: center;

    img {
      width: 20px;
      margin-left: 16px;
    }

    :hover .action {
      visibility: visible;
      opacity: 1;
      width: 72px;

      img {
        :hover {
          background: #eeeeee;
        }
      }
    }
  }
`;

const NewFolder = styled.div`
  height: 56px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  color: ${(props) => props.theme.main};
  font-family: var(--roboto-500);
  border-bottom: 1px solid #f0f0f0;

  img {
    margin-right: 8px;
  }
`;

const SubNoti = styled(NewFolder)`
  font-family: var(--roboto-400-i);
  font-size: 14px;
  color: #6b6b6b;
  cursor: default;
`;

const Content = styled.div`
  display: flex;
`;

const ListReports = styled.div`
  width: ${(props) => (props.fullWidth ? "100%" : "calc(100% - 392px)")};
  margin-top: 8px;

  .action {
    display: none;
  }

  .disabled {
    cursor: not-allowed !important;
  }

  .action-clone {
    display: none;
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fff;
    border-right: 1px solid #ececec;
    width: calc(100% / 4);

    :last-child {
      border-right: none;
    }

    :before {
      display: none;
    }
  }

  .ant-table-row {
    :hover .action {
      display: block;
      background: #ffffff;
      box-shadow: 0px 0px 30px rgb(0 0 0 / 16%);
      border-radius: 8px;
      width: 146px;
      text-align: center;
      position: absolute;

      img {
        cursor: pointer;
        margin: 0 2px;

        :hover {
          background: #eeeeee;
        }
      }
    }

    :hover .action-clone {
      display: block;
      background: #ffffff;
      box-shadow: 0px 0px 30px rgb(0 0 0 / 16%);
      border-radius: 8px;
      width: 44px;
      text-align: center;
      position: absolute;

      img {
        cursor: pointer;
        margin: 0 5px;

        :hover {
          background: #eeeeee;
        }
      }
    }
  }

  .ant-table-tbody > tr.ant-table-row:hover > td {
    background: #ffffff;
    box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    background: #f5f5f5;
    border-bottom: 1px solid #ececec;

    :nth-child(3) {
      text-transform: capitalize;
    }

    span {
      :first-child {
        display: -webkit-box;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
      }

      :last-child {
        display: -webkit-box;
        text-overflow: ellipsis;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
        overflow: hidden;
      }
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px;
    display: flex;
    justify-content: flex-end;
  }
`;

const AddFolder = styled(Button)`
  display: flex;
  align-items: center;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  color: #fff;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
  border-radius: 2px;

  img {
    margin-right: 8px;
    margin-top: 2px;
  }

  &.ant-btn {
    height: 40px;
  }

  &.ant-btn:active {
    background: ${(props) => props.theme.main};
    border: none;
    color: #fff;
  }

  &.ant-btn:focus {
    background: ${(props) => props.theme.main};
    border: none;
    color: #fff;
  }

  &.ant-btn:hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }
`;

const AddReport = styled(Button)`
  &.ant-btn:active {
    background: #fff;
    border-color: #d9d9d9;
    color: #000;
  }

  &.ant-btn:focus {
    background: #fff;
    border-color: #d9d9d9;
    color: #000;
  }

  &.ant-btn:hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;
