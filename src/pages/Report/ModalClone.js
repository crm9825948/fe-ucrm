import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";

import { Notification } from "components/Notification/Noti";
import { cloneReport, resetReportInfo } from "redux/slices/report";

function ModalClone({
  showModalClone,
  myFolder,
  idClone,
  getListReport,
  onHideModalClone,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoading, cloneInfo } = useSelector((state) => state.reportReducer);

  const [form] = Form.useForm();

  const [listMyFolder, setListMyFolder] = useState([]);

  const _onSubmit = (values) => {
    dispatch(
      cloneReport({
        report_id: idClone,
        api_version: "2",
        folder_id: values.folder,
      })
    );
  };

  const _onCancel = () => {
    onHideModalClone();
    form.resetFields();
  };

  useEffect(() => {
    let tempMyFolder = [];
    myFolder.map((item) => {
      return tempMyFolder.push({
        label: item.name,
        value: item._id,
      });
    });
    setListMyFolder(tempMyFolder);
  }, [myFolder]);

  useEffect(() => {
    if (isLoading === false) {
      if (cloneInfo === "success" && showModalClone) {
        onHideModalClone();
        form.resetFields();

        if (window.location.hash === "#my_folders") {
          getListReport();
        }

        Notification("success", "Duplicate successfully!");
        dispatch(resetReportInfo());
      }

      if (cloneInfo !== null && cloneInfo !== "success") {
        Notification("error", cloneInfo);
        dispatch(resetReportInfo());
      }
    }
  }, [
    dispatch,
    form,
    showModalClone,
    getListReport,
    cloneInfo,
    isLoading,
    onHideModalClone,
  ]);

  return (
    <ModalCustom
      title={t("report.cloneReport")}
      visible={showModalClone}
      footer={null}
      width={400}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("report.folderName")}
          name="folder"
          rules={[{ required: true, message: "Please select folder" }]}
        >
          <Select
            options={listMyFolder}
            placeholder={t("common.placeholderSelect")}
            optionFilterProp="label"
            showSearch
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={isLoading}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalClone);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
