import { useCallback, useEffect } from "react";
import update from "immutability-helper";
import _ from "lodash";

import { CardPanel } from "./CardPanel";

const Container = ({
  column,
  setColumn,
  handleRemoveColumn,
  listFields,
  onChangeColumnRowSummary,
  reportDetails,
  handleChangeConditions,
  operatorValue,
  $operatorValue,
  valueConditions,
  $valueConditions,
  isActive,
  $isActive,
  newIndex,
  $newIndex,
}) => {
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = column[dragIndex];
      setColumn(
        update(column, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
      $newIndex({
        dragIndex: dragIndex,
        hoverIndex: hoverIndex,
      });
    },
    [$newIndex, column, setColumn]
  );

  useEffect(() => {
    if (newIndex.dragIndex !== "") {
      $operatorValue((prev) => {
        const newOperatorValue = [...prev];
        newOperatorValue[newIndex.dragIndex] = prev[newIndex.hoverIndex];
        newOperatorValue[newIndex.hoverIndex] = prev[newIndex.dragIndex];
        return newOperatorValue;
      });

      $valueConditions((prev) => {
        const newValueConditions = [...prev];
        newValueConditions[newIndex.dragIndex] = prev[newIndex.hoverIndex];
        newValueConditions[newIndex.hoverIndex] = prev[newIndex.dragIndex];
        return newValueConditions;
      });
    }
  }, [$operatorValue, $valueConditions, newIndex]);

  const renderCard = (card, index) => {
    return (
      <CardPanel
        key={index}
        index={index}
        id={card.field_ID}
        text={card.label}
        card={card}
        moveCard={moveCard}
        handleRemoveColumn={handleRemoveColumn}
        isActive={isActive}
        $isActive={$isActive}
        listFields={listFields}
        onChangeColumnRowSummary={onChangeColumnRowSummary}
        reportDetails={reportDetails}
        handleChangeConditions={handleChangeConditions}
        operatorValue={operatorValue}
        valueConditions={valueConditions}
      />
    );
  };
  return (
    <>
      <div>{_.map(column, (card, i) => renderCard(card, i))}</div>
    </>
  );
};

export default Container;
