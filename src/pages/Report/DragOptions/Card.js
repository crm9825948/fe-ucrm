import { useRef } from "react";
import { useDrag, useDrop } from "react-dnd";
import styled from "styled-components";

import Tag from "antd/lib/tag";

const style = {
  padding: "2px 4px 2px 8px",
  marginRight: "4px",
  marginBottom: "16px",
  background: "#F5F5F5",
  border: "1px solid #F0F0F0",
  borderRadius: "2px",
  cursor: "move",
  width: "fit-content",
  overflow: "hidden",
};

export const Card = ({ id, text, index, moveCard, handleRemoveColumn }) => {
  const ref = useRef(null);

  const [{ handlerId }, drop] = useDrop({
    accept: "card",
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveCard(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    type: "card",
    item: () => {
      return { id, index };
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const opacity = isDragging ? 0 : 1;

  drag(drop(ref));

  return (
    <CustomTag
      closable
      ref={ref}
      style={{ ...style, opacity }}
      data-handler-id={handlerId}
      onClose={(e) => {
        handleRemoveColumn(id);
      }}
    >
      <Number>
        <span>{index + 1}</span>
      </Number>
      {text}
    </CustomTag>
  );
};

const CustomTag = styled(Tag)`
  display: flex;
  align-items: center;
`;

const Number = styled.div`
  background: #ffd8bf;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  margin-right: 4px;

  span {
    font-size: 14px;
  }
`;
