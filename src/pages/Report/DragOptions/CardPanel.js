import { useRef } from "react";
import { useDrag, useDrop } from "react-dnd";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import _ from "lodash";

import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Conditions from "components/Conditions/conditions";

import DeleteIcon from "assets/icons/common/delete.svg";

const measuresOptions = [
  {
    label: "Count",
    value: "count",
  },
  {
    label: "Count Distinct",
    value: "count_distinct",
  },
  {
    label: "Count If",
    value: "count_if",
  },
];

const measuresOptionsDateTimeLocal = [
  {
    label: "Count",
    value: "count",
  },
  {
    label: "Count If",
    value: "count_if",
  },
];

const measuresOptionsNumber = [
  {
    label: "Sum If",
    value: "sum_if",
  },
  {
    label: "Sum",
    value: "sum",
  },
  {
    label: "Average",
    value: "average",
  },
  {
    label: "Min",
    value: "min",
  },
  {
    label: "Max",
    value: "max",
  },
];

export const CardPanel = ({
  id,
  text,
  index,
  moveCard,
  handleRemoveColumn,
  isActive,
  $isActive,
  listFields,
  card,
  onChangeColumnRowSummary,
  reportDetails,
  handleChangeConditions,
  operatorValue,
  valueConditions,
}) => {
  const { t } = useTranslation();

  const ref = useRef(null);

  const [{ handlerId }, drop] = useDrop({
    accept: "card",
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      if (dragIndex === hoverIndex) {
        return;
      }
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      const clientOffset = monitor.getClientOffset();
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      moveCard(dragIndex, hoverIndex);
      item.index = hoverIndex;
    },
  });

  const [{ isDragging }, drag] = useDrag({
    type: "card",
    item: () => {
      return { id, index };
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });

  const opacity = isDragging ? 0 : 1;

  drag(drop(ref));

  const _onActive = (index) => {
    $isActive((prev) => {
      if (prev === index) {
        return -1;
      } else {
        return index;
      }
    });
  };

  return (
    <div style={{ position: "relative" }}>
      <CustomPanel
        closable
        ref={ref}
        style={{ opacity }}
        data-handler-id={handlerId}
        isActive={isActive === index}
        height={
          (card.column_condition.and_filter.length +
            card.column_condition.or_filter.length) *
            48 +
          (_.get(card, "aggregations[0]") === "sum_if" ||
          _.get(card, "aggregations[0]") === "count_if"
            ? 294
            : 121)
        }
      >
        <div
          className="action-delete"
          onClick={(e) => {
            e.stopPropagation();
            handleRemoveColumn(id, index);
          }}
        >
          <img src={DeleteIcon} alt="" style={{ width: "20px" }} />
        </div>

        <div style={{ display: "flex" }} onClick={() => _onActive(index)}>
          <Number>
            <span>{index + 1}</span>
          </Number>
          <div>
            <span
              style={{
                fontSize: "16px",
                fontFamily: "var(--roboto-500)",
                color: "#2c2c2c",
              }}
            >
              {text}
            </span>

            <div style={{ color: "#919EAB" }}>
              <span>
                {measuresOptions
                  .concat(measuresOptionsNumber)
                  .find(
                    (measure) =>
                      measure.value === _.get(card, `aggregations[0]`, "")
                  )?.label || ""}
              </span>

              {_.get(card, `display_name`, "") && (
                <span>
                  {"  "} | {_.get(card, `display_name`, "")}
                </span>
              )}
            </div>
          </div>
        </div>

        <div
          style={{
            marginTop: "16px",
            display: isActive === index ? "block" : "none",
          }}
        >
          <div
            style={{ display: "flex", columnGap: "15px", marginBottom: "28px" }}
          >
            <Select
              style={{ flex: 1 }}
              placeholder="Select field"
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
              onChange={(value) =>
                onChangeColumnRowSummary("field", index, value)
              }
              value={_.get(card, "field_ID")}
            >
              {listFields.map((item) => (
                <>
                  {item.options.map((option) => (
                    <>
                      {!_.get(option, "encrypted", false) && (
                        <Select.Option key={option.value}>
                          {option.label}
                        </Select.Option>
                      )}
                    </>
                  ))}
                </>
              ))}
            </Select>
            <Select
              style={{ flex: 1 }}
              placeholder="Select measure"
              options={
                card.field_type === "number" ||
                card.formula_type === "dateDiff" ||
                card.formula_type === "dateToParts" ||
                card.formula_type === "advanceExpression"
                  ? measuresOptionsNumber
                  : card.field_type === "datetime-local"
                  ? measuresOptionsDateTimeLocal
                  : card.field_type === ""
                  ? []
                  : measuresOptions
              }
              onChange={(value) =>
                onChangeColumnRowSummary("aggregations", index, value)
              }
              value={_.get(card, "aggregations[0]")}
            />
            <Input
              placeholder="Input display name"
              style={{ flex: 1 }}
              onChange={(e) =>
                onChangeColumnRowSummary("display_name", index, e.target.value)
              }
              value={_.get(card, "display_name")}
            />
          </div>
          {(_.get(card, "aggregations[0]") === "sum_if" ||
            _.get(card, "aggregations[0]") === "count_if") && (
            <>
              <Conditions
                title={t("common.allCondition")}
                decs={`(${t("common.descAllCondition")})`}
                conditions={_.get(card, `column_condition.and_filter`, [])}
                setConditions={(value) =>
                  handleChangeConditions(value, index, "conditions", "and")
                }
                operatorValue={_.get(
                  operatorValue,
                  `${[index]}.operatorValueAnd.operator`,
                  []
                )}
                setOperatorValue={(value) =>
                  handleChangeConditions(value, index, "operator", "and")
                }
                value={_.get(valueConditions, `${[index]}.valueAnd.value`, [])}
                setValue={(value) =>
                  handleChangeConditions(value, index, "value", "and")
                }
                ID={
                  Object.keys(reportDetails).length > 0 &&
                  reportDetails.report_info._id
                    ? reportDetails.report_info._id
                    : ""
                }
                dataDetails={
                  Object.keys(reportDetails).length > 0 &&
                  (reportDetails.report_info || {})
                }
              />
              <Conditions
                title={t("common.anyCondition")}
                decs={`(${t("common.descAnyCondition")})`}
                conditions={_.get(card, `column_condition.or_filter`, [])}
                setConditions={(value) =>
                  handleChangeConditions(value, index, "conditions", "or")
                }
                operatorValue={_.get(
                  operatorValue,
                  `${[index]}.operatorValueOr.operator`,
                  []
                )}
                setOperatorValue={(value) =>
                  handleChangeConditions(value, index, "operator", "or")
                }
                value={_.get(valueConditions, `${[index]}.valueOr.value`, [])}
                setValue={(value) =>
                  handleChangeConditions(value, index, "value", "or")
                }
                ID={
                  Object.keys(reportDetails).length > 0 &&
                  reportDetails.report_info._id
                    ? reportDetails.report_info._id
                    : ""
                }
                dataDetails={
                  Object.keys(reportDetails).length > 0 &&
                  (reportDetails.report_info || {})
                }
              />
            </>
          )}
        </div>
      </CustomPanel>
    </div>
  );
};

const CustomPanel = styled.div`
  display: flex;
  flex-direction: column;
  cursor: move;
  width: 100%;
  height: ${({ isActive, height }) => (isActive ? `${height}px` : "66px")};
  padding: 10px;
  border-radius: 5px;
  border: 1px solid #d9d9d9;
  margin-bottom: 16px;
  overflow: hidden;
  transition: all 0.3s;

  .action-delete {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
    position: absolute;
    top: -12px;
    right: 12px;
    z-index: 2;
    background: #fff;
    cursor: pointer;
  }

  :hover {
    .action-delete {
      opacity: 1;
      visibility: visible;
    }
  }
`;

const Number = styled.div`
  width: 40px;
  height: 46px;
  border-radius: 5px;
  background: rgba(32, 162, 162, 0.1);
  margin-right: 10px;
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    color: ${(props) => props.theme.main};
    font-size: 18px;
    font-family: var(--roboto-700);
  }
`;
