import { useCallback } from "react";
import update from "immutability-helper";

import { Card } from "./Card";

const style = {
  display: "flex",
  flexWrap: "wrap",
};

const Container = ({ column, setColumn, handleRemoveColumn }) => {
  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = column[dragIndex];
      setColumn(
        update(column, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [column, setColumn]
  );

  const renderCard = (card, index) => {
    return (
      <Card
        key={card.field_ID}
        index={index}
        id={card.field_ID}
        text={card.label}
        moveCard={moveCard}
        handleRemoveColumn={handleRemoveColumn}
      />
    );
  };
  return (
    <>
      <div style={style}>{column.map((card, i) => renderCard(card, i))}</div>
    </>
  );
};

export default Container;
