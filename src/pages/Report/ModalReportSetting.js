import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import moment from "moment";
import { checkConditions } from "util/helper";

import Tag from "antd/lib/tag";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import DatePicker from "antd/lib/date-picker";
import Modal from "antd/lib/modal";
import Steps from "antd/lib/steps";
import Table from "antd/lib/table";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import Tooltip from "antd/lib/tooltip";
import { InfoCircleOutlined, WarningOutlined } from "@ant-design/icons";

import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";

import { Notification } from "components/Notification/Noti";
import Container from "pages/Report/DragOptions/Container";
import ContainerPanel from "pages/Report/DragOptions/ContainerPanel";
import Conditions from "components/Conditions/conditions";

import { loadListObjectField } from "redux/slices/objects";
import {
  resetReportInfo,
  createReport,
  updateReport,
  setCurrentPage,
} from "redux/slices/report";
import {
  loadSpecificReportSuccess,
  loadTotalRecordsSuccess,
} from "redux/slices/reportDetails";

import { optionsTimeRangeReport } from "util/staticData";
import { useNavigate } from "react-router";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";
import { BASE_URL_API } from "constants/constants";

function ReportSetting({
  isEdit,
  // report,
  folder,
  objects,
  setIsEdit,
  // setReport,
  getListReport,
  listAllGroups,
  reportDetails,
  showReportSetting,
  onHideReportSetting,
  loadInfoReport,
  loadReport,
  loadWidgets,
  loadCharts,
  setColumnReport,
  listFolders,
}) {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const [form] = Form.useForm();
  const { Step } = Steps;
  const { Column } = Table;
  const { Option } = Select;
  const { TextArea } = Input;
  const { CheckableTag } = Tag;

  const dispatch = useDispatch();
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { isLoading, createInfo, updateInfo } = useSelector(
    (state) => state.reportReducer
  );
  const { reportPermission } = useSelector((state) => state.userReducer);
  const [listFields, setListFields] = useState([]);
  const [listFieldsMain, setListFieldsMain] = useState([]);
  const [listMembers, setListMembers] = useState([]);
  const [listObjects, setListObjects] = useState([]);
  const [currentStep, setCurrentStep] = useState(0);
  const [selectedTags, setSelectedTags] = useState("Pagination");

  const [column, setColumn] = useState([]);
  const [columnSelected, setColumnSelected] = useState([]);
  const [columnCal, setColumnCal] = useState([]);
  const [columnCalSelected, $columnCalSelected] = useState([]);
  const [arithmetic, setArithmetic] = useState([]);

  const [valueSummary, setValueSummary] = useState([]);
  const [pivotRow, setPivotRow] = useState([]);
  const [pivotColumn, setPivotColumn] = useState([]);
  const [selectRow, setSelectRow] = useState([]);
  const [selectColumn, setSelectColumn] = useState([]);
  const [isShowPercentage, setIsShowPercentage] = useState("hidden");
  const [checkSummary, setCheckSummary] = useState({
    drop_rows_with_all_zeros: false,
    show_row_total: false,
    show_col_total: false,
  });

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [timeRangeFilter, $timeRangeFilter] = useState({
    filter_field_id: "modify_time",
    field_type: "datetime-local",
    filter_type: "all-time",
    amount_of_time: null,
    start_time: null,
    end_time: null,
  });

  const [folderCustom, setFolderCustom] = useState({
    name: "",
    id: "",
  });

  // const tagsPercentage = ["Show", "Hide"];
  const tagsData = ["Pagination", "Detail", "Summary", "Row Summary", "Custom"];
  const percentageOptions = [
    {
      label: "Hide",
      value: "hidden",
    },

    {
      label: "Show by row",
      value: "by-row",
    },

    {
      label: "Show by column",
      value: "by-column",
    },
  ];

  const measuresOptions = [
    {
      label: "Count",
      value: "count",
    },
  ];

  const measuresOptionsNumber = [
    {
      label: "Count",
      value: "count",
    },
    {
      label: "Sum",
      value: "sum",
    },
    {
      label: "Average",
      value: "average",
    },
    {
      label: "Min",
      value: "min",
    },
    {
      label: "Max",
      value: "max",
    },
  ];

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  //RowSummary
  const [isActive, $isActive] = useState(-1);
  const [newIndex, $newIndex] = useState({
    dragIndex: "",
    hoverIndex: "",
  });
  const [operatorValue, $operatorValue] = useState([]);
  const [valueConditions, $valueConditions] = useState([]);

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    //Check timerange
    let flagTimeRange = false;
    if (
      _.get(timeRangeFilter, "filter_type", "") === "custom" &&
      _.get(timeRangeFilter, "start_time") === null &&
      _.get(timeRangeFilter, "end_time") === null
    ) {
      flagTimeRange = true;
    }

    //Check column summary report
    let condition = -1;
    if (selectedTags === "Row Summary") {
      condition = column.findIndex(
        (item, idx) =>
          (_.get(item, "aggregations[0]") === "count_if" ||
            _.get(item, "aggregations[0]") === "sum_if") &&
          (checkConditions(
            _.get(item, "column_condition.and_filter", []),
            _.get(operatorValue, `${[idx]}.operatorValueAnd.operator`, []),
            _.get(valueConditions, `${[idx]}.valueAnd.value`, [])
          ) ||
            checkConditions(
              _.get(item, "column_condition.or_filter", []),
              _.get(operatorValue, `${[idx]}.operatorValueOr.operator`, []),
              _.get(valueConditions, `${[idx]}.valueOr.value`, [])
            ))
      );
    }

    switch (selectedTags) {
      case "Pagination":
        if (
          form.getFieldValue("fields") !== undefined &&
          form.getFieldValue("fields")?.length !== 0
        ) {
          // setCurrentStep(currentStep + 1);
        } else {
          Notification("warning", "Please full fields required!");
          return;
        }
        break;

      case "Detail":
        if (
          form.getFieldValue("fields") !== undefined &&
          form.getFieldValue("fields")?.length !== 0
        ) {
          // setCurrentStep(currentStep + 1);
        } else {
          Notification("warning", "Please full fields required!");
          return;
        }
        break;

      case "Summary":
        if (
          form.getFieldValue("fields_rows") !== undefined &&
          form.getFieldValue("fields_rows")?.length !== 0 &&
          valueSummary.find((field) => field.field_ID === "") === undefined &&
          valueSummary.find((field) => field.aggregations === undefined) ===
            undefined
        ) {
          // setCurrentStep(currentStep + 1);
        } else {
          Notification("warning", "Please full fields required!");
          return;
        }
        break;

      case "Row Summary":
        if (
          form.getFieldValue("fields_rows") !== undefined &&
          form.getFieldValue("fields_rows")?.length !== 0 &&
          !column.find(
            (item) =>
              !_.get(item, "field_ID") || _.isEmpty(_.get(item, "aggregations"))
          )
        ) {
          // setCurrentStep(currentStep + 1);
        } else {
          Notification("warning", "Please full fields required!");
          return;
        }
        break;

      default:
        break;
    }

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else if (flagTimeRange) {
      Notification("warning", "Please select start time or end time!");
    } else if (condition > -1) {
      Notification(
        "warning",
        `Please fullfill conditions at column ${condition + 1}!`
      );
      $isActive(condition);
    } else {
      let fields = [];
      if (selectedTags.toLowerCase() === "summary") {
        fields = pivotRow.concat(pivotColumn);
      } else if (selectedTags.toLowerCase() === "row summary") {
        pivotRow.concat(column).forEach((item, idx) => {
          fields.push({
            field_ID: item.field_ID,
            position: idx,
          });
        });
      } else {
        column.forEach((item, idx) => {
          fields.push({
            field_ID: item.field_ID,
            position: idx,
          });
        });
      }

      let resultArithmetic = [...arithmetic];
      if (selectedTags.toLowerCase() === "detail") {
        if (columnCalSelected.length > 0 && arithmetic.length === 0) {
          columnCalSelected.forEach((item) => {
            resultArithmetic.push({
              id_field: item.id_field,
              ops: [],
            });
          });
        } else if (columnCalSelected.length !== arithmetic.length) {
          const temp = columnCalSelected.filter((item1) =>
            arithmetic.some((item2) => item1.id_field !== item2.id_field)
          );

          if (temp.length > 0) {
            temp.forEach((item) => {
              resultArithmetic.push({
                id_field: item.id_field,
                ops: [],
              });
            });
          }
        }
      }

      let data = {
        filter_condition: { and_filter: allCondition, or_filter: anyCondition },
        description: form.getFieldValue("description"),
        folder_id: form.getFieldValue("folder_report"),
        main_object: form.getFieldValue("main_object"),
        name: form.getFieldValue("report_name"),
        share_to_groups: form.getFieldValue("share_group"),
        show_fields: fields,
        report_type:
          selectedTags === "Row Summary"
            ? "row_summary_report"
            : selectedTags.toLowerCase(),
        meta_data: [],
        decimal_places: 2,
        arithmetic_ops: resultArithmetic,
        time_range_filter: {
          ...timeRangeFilter,
        },
      };

      if (selectedTags.toLowerCase() === "summary") {
        data = {
          ...data,
          measures: valueSummary,
          view_in_percentage: isShowPercentage,
          pivot_columns: pivotColumn,
          pivot_rows: pivotRow,
          drop_rows_with_all_zeros: checkSummary.drop_rows_with_all_zeros,
          show_row_total: checkSummary.show_row_total,
          show_col_total: checkSummary.show_col_total,
        };
      }

      if (selectedTags.toLowerCase() === "row summary") {
        data = {
          ...data,
          measures: valueSummary,
          pivot_columns: column,
          pivot_rows: pivotRow,
          column_condition_operators: operatorValue,
          column_condition_values: valueConditions,
          view_in_percentage: isShowPercentage,
          drop_rows_with_all_zeros: checkSummary.drop_rows_with_all_zeros,
          show_col_total: checkSummary.show_col_total,
          show_row_total: checkSummary.show_row_total,
        };
      }

      if (isEdit) {
        dispatch(
          updateReport({
            data: data,
            report_id: reportDetails.report_info._id,
          })
        );
      } else {
        dispatch(
          createReport({
            ...data,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    onHideReportSetting();
    form.resetFields();
    setIsEdit(false);
    // setReport({});
    setCurrentStep(0);
    setColumn([]);
    setColumnSelected([]);
    setColumnCal([]);
    $columnCalSelected([]);
    setSelectedTags("Pagination");
    setIsShowPercentage("hidden");
    setValueSummary([]);
    setPivotRow([]);
    setPivotColumn([]);
    setSelectColumn([]);
    setArithmetic([]);
    setSelectRow([]);
    setAnyCondition([]);
    setAllCondition([]);
    $timeRangeFilter({
      filter_field_id: "modify_time",
      field_type: "datetime-local",
      filter_type: "all-time",
      amount_of_time: null,
      start_time: null,
      end_time: null,
    });
    $isActive(-1);
    $newIndex({
      dragIndex: "",
      hoverIndex: "",
    });
    $operatorValue([]);
    $valueConditions([]);
  };

  const handleChange = (tag, checked) => {
    if (checked && !isEdit) {
      const nextSelectedTags = tag;
      setSelectedTags(nextSelectedTags);

      setColumn([]);
      setColumnSelected([]);
      setColumnCal([]);
      $columnCalSelected([]);
      setValueSummary([]);
      setPivotRow([]);
      setPivotColumn([]);
      setSelectColumn([]);
      setArithmetic([]);
      setSelectRow([]);
      setAnyCondition([]);
      setAllCondition([]);
      $timeRangeFilter({
        filter_field_id: "modify_time",
        field_type: "datetime-local",
        filter_type: "all-time",
        amount_of_time: null,
        start_time: null,
        end_time: null,
      });
      form.setFieldsValue({
        fields: undefined,
        fields_rows: undefined,
        fields_column: undefined,
      });
    }
  };

  // const handleChangePercentage = (tag, checked) => {
  //   if (checked) {
  //     const isShow = tag;
  //     setIsShowPercentage(isShow);
  //   }
  // };

  const handleChangePercentage = (value) => {
    // if (checked) {
    //   const isShow = tag;
    //   setIsShowPercentage(isShow);
    // }
    setIsShowPercentage(value);
  };

  const _onNext = async () => {
    if (currentStep === 0) {
      if (selectedTags === "Custom") {
        if (
          form.getFieldValue("report_name") !== undefined &&
          form.getFieldValue("report_name") !== ""
        ) {
          const isTokenValid = await checkTokenExpiration();

          if (Object.entries(reportDetails).length > 0) {
            let tem = reportDetails.report_info.action_key.map((key) => {
              return {
                ...key,
                data_value: {
                  ...key.data_value,
                  object_name: undefined,
                  field_name: undefined,
                },
              };
            });
            axios
              .post(
                BASE_URL_API + "report/update",
                {
                  data: {
                    name: form.getFieldValue("report_name"),
                    description: form.getFieldValue("description"),
                    report_type: "custom_report",
                    folder_id: folderCustom.id,
                    main_object: "",
                    show_key: reportDetails.report_info.show_key,
                    show_report: reportDetails.report_info.show_report,
                    action_key: tem,
                  },
                  report_id: reportDetails.report_info._id,
                },
                {
                  headers: {
                    Authorization: isTokenValid,
                  },
                }
              )
              .then((res) => {
                navigate(`/custom-report/${reportDetails.report_info._id}`);
              })
              .catch((err) => Notification("error", err.response.data.error));
          } else {
            axios
              .post(
                BASE_URL_API + "report/create-report",
                {
                  name: form.getFieldValue("report_name"),
                  description: form.getFieldValue("description"),
                  report_type: "custom_report",
                  folder_id: folderCustom.id,
                  main_object: "",
                  show_key: true,
                  show_report: true,
                  action_key: [],
                },
                {
                  headers: {
                    Authorization: isTokenValid,
                  },
                }
              )
              .then((res) => {
                navigate(`/custom-report/${res.data.data.report_id}`);
              })
              .catch((err) => Notification("error", err.response.data.error));
          }
        } else {
          Notification("warning", "Please full fields required!");
        }
      } else {
        if (
          form.getFieldValue("main_object") !== undefined &&
          form.getFieldValue("report_name") !== undefined &&
          form.getFieldValue("report_name") !== ""
        ) {
          setCurrentStep(currentStep + 1);
        } else {
          Notification("warning", "Please full fields required!");
        }
      }
    }

    if (currentStep === 1) {
      setCurrentStep(currentStep + 1);
      // switch (selectedTags) {
      //   case "Pagination":
      //     if (
      //       form.getFieldValue("fields") !== undefined &&
      //       form.getFieldValue("fields")?.length !== 0
      //     ) {
      //       setCurrentStep(currentStep + 1);
      //     } else {
      //       Notification("warning", "Please full fields required!");
      //     }
      //     break;

      //   case "Detail":
      //     if (
      //       form.getFieldValue("fields") !== undefined &&
      //       form.getFieldValue("fields")?.length !== 0
      //     ) {
      //       setCurrentStep(currentStep + 1);
      //     } else {
      //       Notification("warning", "Please full fields required!");
      //     }
      //     break;

      //   case "Summary":
      //     if (
      //       form.getFieldValue("fields_rows") !== undefined &&
      //       form.getFieldValue("fields_rows")?.length !== 0 &&
      //       valueSummary.find((field) => field.field_ID === "") === undefined &&
      //       valueSummary.find((field) => field.aggregations === undefined) ===
      //         undefined
      //     ) {
      //       setCurrentStep(currentStep + 1);
      //     } else {
      //       Notification("warning", "Please full fields required!");
      //     }
      //     break;

      //   default:
      //     break;
      // }
    }
  };

  const _onPrev = () => {
    setCurrentStep(currentStep - 1);
  };

  const onLoadFields = useCallback(
    (value) => {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: value,
          show_meta_fields: true,
        })
      );

      if (!isEdit) {
        setColumn([]);
        setColumnSelected([]);
        setColumnCal([]);
        $columnCalSelected([]);
        setValueSummary([]);
        setPivotRow([]);
        setPivotColumn([]);
        setSelectColumn([]);
        setArithmetic([]);
        setSelectRow([]);
        setAnyCondition([]);
        setAllCondition([]);
        $timeRangeFilter({
          filter_field_id: "modify_time",
          field_type: "datetime-local",
          filter_type: "all-time",
          amount_of_time: null,
          start_time: null,
          end_time: null,
        });
        form.setFieldsValue({
          fields: undefined,
          fields_rows: undefined,
          fields_column: undefined,
        });
      }
    },
    [dispatch, form, isEdit]
  );

  useEffect(() => {
    if (Object.keys(objects).length > 0) {
      let tempObjects = [];
      Object.entries(objects).forEach(([key, val]) => {
        val.forEach((object) => {
          if (isEdit) {
            if (
              object.Status &&
              _.get(reportPermission, `${object._id}.Edit_Report`, false)
            ) {
              tempObjects.push({
                label: object.Name,
                value: object._id,
              });
            }
          } else {
            if (
              object.Status &&
              _.get(reportPermission, `${object._id}.Create_Report`, false)
            ) {
              tempObjects.push({
                label: object.Name,
                value: object._id,
              });
            }
          }
        });
      });

      setListObjects(tempObjects);
    }
  }, [objects, reportPermission, isEdit]);

  useEffect(() => {
    let tempOptions = [];
    let tempFieldsMain = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        let tempFields = [];
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempFields.push({
                label: field.related_name,
                value: field.full_field_id,
                type: field.type,
                encrypted: _.get(field, "encrypted", false),
                // formula_type: !field?.in_words && (field?.formula_type || ""),
                formula_type:
                  field.type !== "formula"
                    ? ""
                    : field?.formula_type === "dateDiff"
                    ? !field?.in_words
                      ? field?.formula_type
                      : ""
                    : field?.formula_type,
              });
            }
          });
        });
        if (tempFields.length > 0) {
          tempOptions.push({
            label: Object.values(item)[0].object_name,
            options: tempFields,
          });
        }

        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempFieldsMain.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  encrypted: _.get(field, "encrypted", false),
                  is_indexed: _.get(field, "is_indexed", false),
                  // formula_type: !field?.in_words && (field?.formula_type || ""),
                  formula_type:
                    field.type !== "formula"
                      ? ""
                      : field?.formula_type === "dateDiff"
                      ? !field?.in_words
                        ? field?.formula_type
                        : ""
                      : field?.formula_type,
                });
              }
            });
          });
        }
      }
    });
    setListFields(tempOptions);
    setListFieldsMain(tempFieldsMain);
  }, [listObjectField]);

  useEffect(() => {
    let tempGroup = [];
    if (listAllGroups.length > 0) {
      listAllGroups.map((item) => {
        return tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });
    }
    setListMembers(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    if (showReportSetting) {
      form.setFieldsValue({
        folder_report: folder.id,
      });
      setFolderCustom(folder);
    }
  }, [folder, form, showReportSetting]);

  useEffect(() => {
    if (isLoading === false && showReportSetting) {
      if (createInfo === "success" || updateInfo === "success") {
        onHideReportSetting();
        form.resetFields();
        getListReport();
        loadInfoReport();
        // loadReport(true);
        dispatch(setCurrentPage(1));
        loadReport(null, null);
        dispatch(loadTotalRecordsSuccess(null));
        setIsEdit(false);
        // setReport({});
        setCurrentStep(0);
        setColumn([]);
        setColumnSelected([]);
        setColumnCal([]);
        $columnCalSelected([]);
        setSelectedTags("Pagination");
        setValueSummary([]);
        setPivotRow([]);
        setPivotColumn([]);
        setIsShowPercentage("hidden");
        setSelectColumn([]);
        setArithmetic([]);
        setSelectRow([]);
        setAnyCondition([]);
        setAllCondition([]);
        $timeRangeFilter({
          filter_field_id: "modify_time",
          field_type: "datetime-local",
          filter_type: "all-time",
          amount_of_time: null,
          start_time: null,
          end_time: null,
        });
        $isActive(-1);
        $newIndex({
          dragIndex: "",
          hoverIndex: "",
        });
        $operatorValue([]);
        $valueConditions([]);

        dispatch(loadSpecificReportSuccess([]));
        setColumnReport([]);

        if (isEdit && loadWidgets && loadCharts) {
          loadWidgets();
          loadCharts();
        }

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetReportInfo());
      }

      if (createInfo !== null && createInfo !== "success") {
        Notification("error", createInfo);
        dispatch(resetReportInfo());
      }

      if (updateInfo !== null && updateInfo !== "success") {
        Notification("error", updateInfo);
        dispatch(resetReportInfo());
      }
    }
  }, [
    createInfo,
    dispatch,
    loadWidgets,
    loadCharts,
    form,
    getListReport,
    isEdit,
    isLoading,
    loadInfoReport,
    loadReport,
    onHideReportSetting,
    setIsEdit,
    showReportSetting,
    updateInfo,
    setColumnReport,
  ]);

  const onCheckCal = (checked, type, data) => {
    let tempArithmetic = [...arithmetic];

    const found = arithmetic.find((item) => item.id_field === data.id_field);
    const foundIndex = arithmetic.findIndex(
      (item) => item.id_field === data.id_field
    );

    if (checked) {
      if (arithmetic.length > 0) {
        if (found) {
          let tempOps = [...found.ops];
          tempOps.push(type);
          tempArithmetic[foundIndex] = {
            id_field: found.id_field,
            ops: tempOps,
          };
        } else {
          tempArithmetic.push({
            id_field: data.id_field,
            ops: [type],
          });
        }
      } else {
        tempArithmetic.push({
          id_field: data.id_field,
          ops: [type],
        });
      }
    } else {
      let tempOps = found.ops.filter((op) => op !== type);
      tempArithmetic[foundIndex] = {
        id_field: found.id_field,
        ops: tempOps,
      };
    }
    setArithmetic(tempArithmetic);
  };

  const onSelectFields = (value, list) => {
    let tempColumn = [];
    let tempSelectedColumn = [];
    list.forEach((item) => {
      tempColumn.push({
        field_ID: item.value,
        label: item.children,
      });
      tempSelectedColumn.push(item.value);
    });
    setColumn(tempColumn);
    setColumnSelected(tempSelectedColumn);

    if (selectedTags === "Detail") {
      let tempCal = [];
      list.forEach((item) => {
        let tempField = {};
        listFields.forEach((ele) => {
          ele.options.forEach((field) => {
            if (field.value === item.value) {
              tempField = {
                ...field,
              };
            }
          });
        });
        if (
          !tempField.value.includes(".") &&
          (tempField.type === "number" ||
            tempField.formula_type === "dateDiff" ||
            tempField.formula_type === "advanceExpression")
        ) {
          tempCal.push({
            column: tempField.label,
            id_field: tempField.value,
            sum: "",
            average: "",
            min: "",
            max: "",
          });
        }
      });
      setColumnCal(tempCal);
      $columnCalSelected(tempCal);

      let tempMetricsFields = [];

      tempCal.forEach((item) => {
        tempMetricsFields.push({
          label: item.column,
          value: item.id_field,
        });
      });

      form.setFieldsValue({
        metrics_fields: tempMetricsFields,
      });

      let listID = [];
      tempCal.forEach((item) => {
        listID.push(item.id_field);
      });

      let tempArithmetic = arithmetic.filter((item) =>
        listID.includes(item.id_field)
      );

      setArithmetic(tempArithmetic);
    }
  };

  const _onAddColumnRowSummary = () => {
    setColumn([
      ...column,
      {
        field_ID: undefined,
        label: "No data",
        field_type: "",
        formula_type: "",
        type: "default",
        display_name: "",
        aggregations: undefined,
        column_condition: {
          and_filter: [],
          or_filter: [],
        },
      },
    ]);
    $valueConditions([
      ...valueConditions,
      {
        valueAnd: { value: [] },
        valueOr: { value: [] },
      },
    ]);
    $operatorValue([
      ...operatorValue,
      {
        operatorValueAnd: { operator: [] },
        operatorValueOr: { operator: [] },
      },
    ]);
  };

  const onChangeColumnRowSummary = (type, index, data) => {
    let tempColumn = [...column];

    switch (type) {
      case "field":
        let tempField = {};
        listFields.forEach((ele) => {
          ele.options.forEach((field) => {
            if (field.value === data) {
              tempField = {
                ...field,
              };
            }
          });
        });
        tempColumn[index] = {
          ...tempColumn[index],
          field_ID: data,
          label: _.get(tempField, "label"),
          field_type: _.get(tempField, "type"),
          formula_type: _.get(tempField, "formula_type"),
          aggregations: undefined,
        };
        setColumn(tempColumn);
        break;
      case "aggregations":
        tempColumn[index] = {
          ...tempColumn[index],
          aggregations: [data],
        };
        setColumn(tempColumn);
        break;
      case "display_name":
        tempColumn[index] = {
          ...tempColumn[index],
          display_name: data,
        };
        setColumn(tempColumn);
        break;

      default:
        break;
    }
  };

  const handleChangeConditions = (value, index, type, typeCondition) => {
    let tempColumn = [...column];
    let tempOperatorValue = [...operatorValue];
    let tempValueConditions = [...valueConditions];

    console.log("ruhn :>> ", type);

    switch (type) {
      case "conditions":
        tempColumn[index] = {
          ...tempColumn[index],
          column_condition: {
            ...tempColumn[index].column_condition,
            [typeCondition === "and" ? "and_filter" : "or_filter"]: value,
          },
        };
        setColumn(tempColumn);
        break;

      case "operator":
        tempOperatorValue[index] = {
          ...tempOperatorValue[index],
          [typeCondition === "and" ? "operatorValueAnd" : "operatorValueOr"]: {
            operator: value,
          },
        };
        $operatorValue(tempOperatorValue);
        break;

      case "value":
        tempValueConditions[index] = {
          ...tempValueConditions[index],
          [typeCondition === "and" ? "valueAnd" : "valueOr"]: {
            value: value,
          },
        };
        $valueConditions(tempValueConditions);
        break;

      default:
        break;
    }
  };

  const onSelectMetricsFields = (value, list) => {
    let tempCal = [];

    list.forEach((item) => {
      tempCal.push({
        column: item.children,
        id_field: item.value,
        sum: "",
        average: "",
        min: "",
        max: "",
      });
    });

    let listID = [];
    tempCal.forEach((item) => {
      listID.push(item.id_field);
    });

    let tempArithmetic = arithmetic.filter((item) =>
      listID.includes(item.id_field)
    );

    setArithmetic(tempArithmetic);
    $columnCalSelected(tempCal);
  };

  const handleRemoveColumn = (removedTag, index) => {
    const newTags = column.filter((tag) => tag.field_ID !== removedTag);
    setColumn(newTags);

    let tempSelectedColumn = [];
    newTags.forEach((item) => {
      tempSelectedColumn.push(item.field_ID);
    });
    setColumnSelected(tempSelectedColumn);

    let tempFields = [];
    newTags.forEach((item) => {
      tempFields.push(item.field_ID);
    });

    form.setFieldsValue({
      fields: tempFields,
    });

    if (selectedTags === "Detail") {
      let tempCal = [...columnCal];

      columnCal.forEach((item, idx) => {
        if (item.id_field === removedTag) {
          tempCal.splice(idx, 1);
        }
      });

      let listID = [];
      tempCal.forEach((item) => {
        listID.push(item.id_field);
      });

      let tempArithmetic = arithmetic.filter((item) =>
        listID.includes(item.id_field)
      );
      setArithmetic(tempArithmetic);
      setColumnCal(tempCal);
      $columnCalSelected(tempCal);

      let tempMetricsFields = [];

      tempCal.forEach((item) => {
        tempMetricsFields.push({
          label: item.column,
          value: item.id_field,
        });
      });

      form.setFieldsValue({
        metrics_fields: tempMetricsFields,
      });
    }

    if (selectedTags === "Row Summary") {
      $operatorValue((prev) => {
        const newOperatorValue = [...prev];
        newOperatorValue.splice(index, 1);
        return newOperatorValue;
      });
      $valueConditions((prev) => {
        const newValueConditions = [...prev];
        newValueConditions.splice(index, 1);
        return newValueConditions;
      });
    }
  };

  const handleChangeField = (e, option, idx, type) => {
    let arr = [...valueSummary];

    switch (type) {
      case "field":
        let tempField = {};
        listFields.forEach((item) => {
          Object.values(item)[1].forEach((field) => {
            if (field.value === e) {
              tempField = field;
            }
          });
        });
        valueSummary.forEach((item, index) => {
          if (index === idx) {
            arr[index] = {
              ...item,
              field_ID: e,
              field_type: tempField.type,
              aggregations: undefined,
              formula_type: tempField.formula_type,
            };
          }
        });
        setValueSummary(arr);

        break;

      case "measure":
        valueSummary.forEach((item, index) => {
          if (index === idx) {
            arr[index] = {
              ...item,
              aggregations: [e],
            };
          }
        });
        setValueSummary(arr);

        break;

      case "display":
        valueSummary.forEach((item, index) => {
          if (index === idx) {
            arr[index] = {
              ...item,
              display_name: e,
            };
          }
        });
        setValueSummary(arr);

        break;

      default:
        break;
    }
  };

  const _onCheckSummary = (checked, type) => {
    let temp = { ...checkSummary };
    temp[type] = checked;
    setCheckSummary(temp);
  };

  useEffect(() => {
    valueSummary.forEach((item, idx) => {
      if (item.field_ID !== "") {
        form.setFieldsValue({
          [item.field_ID + idx]: item.field_ID,
        });
      } else {
        form.setFieldsValue({
          ["" + idx]: undefined,
        });
      }

      if (item.aggregations !== undefined) {
        form.setFieldsValue({
          ["measure_" + item.field_ID + idx]: item.aggregations[0],
        });
      }

      if (item.display_name !== undefined) {
        form.setFieldsValue({
          ["display_name_" + item.field_ID + idx]: item.display_name,
        });
      }
    });
  }, [form, valueSummary]);

  const onDeleteField = (idx) => {
    let temp = [...valueSummary];
    temp.splice(idx, 1);
    setValueSummary(temp);
  };

  const _onAddValueSummary = () => {
    const item = [
      ...valueSummary,
      {
        field_ID: "",
        field_type: "",
        type: "default",
        display_name: "",
        formula_type: "",
      },
    ];
    setValueSummary(item);
  };

  const onSelectRow = (value, list) => {
    setSelectRow(value);

    let tempRow = [];
    list.map((item) => {
      return tempRow.push({
        field_ID: item.value,
      });
    });

    listFields.forEach((item) => {
      Object.values(item)[1].forEach((field) => {
        tempRow.forEach((ele, idx) => {
          if (field.value === ele.field_ID)
            tempRow[idx] = { field_ID: ele.field_ID, type: field.type };
        });
      });
    });

    setPivotRow(tempRow);
  };

  const onSelectColumn = (value, list) => {
    setSelectColumn(value);

    let tempColumn = [];
    list.map((item) => {
      return tempColumn.push({
        field_ID: item.value,
      });
    });

    listFields.forEach((item) => {
      Object.values(item)[1].forEach((field) => {
        tempColumn.forEach((ele, idx) => {
          if (field.value === ele.field_ID)
            tempColumn[idx] = { field_ID: ele.field_ID, type: field.type };
        });
      });
    });

    setPivotColumn(tempColumn);
  };

  const handlerTimeRange = {
    filterField: (value, option) => {
      $timeRangeFilter({
        filter_field_id: _.get(option, "value", ""),
        field_type: _.get(option, "type", ""),
        filter_type: "all-time",
        amount_of_time: null,
        start_time: null,
        end_time: null,
      });
    },
    filterType: (value) => {
      switch (value) {
        case "last-n-days":
        case "last-n-weeks":
        case "last-n-months":
        case "last-n-years":
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: 3,
            start_time: null,
            end_time: null,
          });
          break;

        default:
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: null,
            start_time: null,
            end_time: null,
          });
          break;
      }
    },
    amountOfTime: (value) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        amount_of_time: value ? value : 1,
      });
    },
    startTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        start_time: valueString ? valueString : null,
      });
    },
    endTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        end_time: valueString ? valueString : null,
      });
    },
  };

  const disabledStartDate = (current) => {
    return (
      timeRangeFilter.end_time &&
      timeRangeFilter.end_time <
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  const disabledEndDate = (current) => {
    return (
      timeRangeFilter.start_time &&
      timeRangeFilter.start_time >
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  useEffect(() => {
    if (isEdit && Object.keys(reportDetails).length > 0 && showReportSetting) {
      if (reportDetails.report_info.report_type !== "custom_report") {
        onLoadFields(reportDetails.report_info.main_object);
        setAllCondition(reportDetails.report_info.filter_condition.and_filter);
        setAnyCondition(reportDetails.report_info.filter_condition.or_filter);
        $timeRangeFilter(
          _.get(reportDetails, "report_info.time_range_filter", {
            filter_field_id: "modify_time",
            field_type: "datetime-local",
            filter_type: "all-time",
            amount_of_time: null,
            start_time: null,
            end_time: null,
          })
        );

        form.setFieldsValue({
          report_name: reportDetails.report_info.name,
          description: reportDetails.report_info.description,
          main_object: reportDetails.report_info.main_object,
          share_group: reportDetails.report_info.share_to_groups,
        });

        let tempField = [];
        let tempColumn = [];
        let tempSelectedColumn = [];
        let tempRows = [];

        switch (reportDetails.report_info.report_type) {
          case "pagination":
            setSelectedTags("Pagination");

            reportDetails.report_info.show_fields.map((item) => {
              return tempField.push(item.field_ID);
            });

            form.setFieldsValue({
              fields: tempField,
            });

            reportDetails.data.forEach((item) => {
              tempColumn.push({
                label: item.name,
                field_ID: item.full_field_id,
              });
              tempSelectedColumn.push(item.full_field_id);
            });

            setColumn(tempColumn);
            setColumnSelected(tempSelectedColumn);

            break;

          case "detail":
            setSelectedTags("Detail");

            reportDetails.report_info.show_fields.map((item) => {
              return tempField.push(item.field_ID);
            });

            form.setFieldsValue({
              fields: tempField,
            });

            reportDetails.data.forEach((item) => {
              tempColumn.push({
                label: item.name,
                field_ID: item.full_field_id,
              });
              tempSelectedColumn.push(item.full_field_id);
            });

            setColumn(tempColumn);
            setColumnSelected(tempSelectedColumn);

            let tempCal = [];
            let tempArithmetic = [];
            let tempMetricsFields = [];

            reportDetails.report_info.arithmetic_ops.forEach((item) => {
              tempCal.push({
                column: item.name,
                id_field: item.id_field,
                sum: "",
                average: "",
                min: "",
                max: "",
              });
              tempArithmetic.push({
                id_field: item.id_field,
                ops: item.ops,
              });
            });

            tempCal.forEach((item) => {
              tempMetricsFields.push({
                label: item.column,
                value: item.id_field,
              });
            });

            form.setFieldsValue({
              metrics_fields: tempMetricsFields,
            });

            setArithmetic(tempArithmetic);
            $columnCalSelected(tempCal);
            setColumnCal(tempCal);

            break;

          case "summary":
            let tempColumns = [];

            setSelectedTags("Summary");
            setIsShowPercentage(reportDetails.report_info.view_in_percentage);

            reportDetails.report_info.pivot_rows.map((item) => {
              return tempRows.push(item.field_ID);
            });

            reportDetails.report_info.pivot_columns.map((item) => {
              return tempColumns.push(item.field_ID);
            });

            setPivotRow(reportDetails.report_info.pivot_rows);
            setPivotColumn(reportDetails.report_info.pivot_columns);
            setSelectRow(tempRows);
            setSelectColumn(tempColumns);
            setValueSummary(reportDetails.report_info.measures);

            setCheckSummary({
              drop_rows_with_all_zeros:
                reportDetails.report_info.drop_rows_with_all_zeros,
              show_row_total: reportDetails.report_info.show_row_total,
              show_col_total: reportDetails.report_info.show_col_total,
            });

            form.setFieldsValue({
              fields_rows: tempRows,
              fields_column: tempColumns,
              percentage: reportDetails.report_info.view_in_percentage,
              drop_rows_with_all_zeros:
                reportDetails.report_info.drop_rows_with_all_zeros,
              show_row_total: reportDetails.report_info.show_row_total,
              show_col_total: reportDetails.report_info.show_col_total,
            });

            break;

          case "row_summary_report":
            reportDetails.report_info.pivot_rows.map((item) => {
              return tempRows.push(item.field_ID);
            });

            setIsShowPercentage(reportDetails.report_info.view_in_percentage);
            setCheckSummary({
              drop_rows_with_all_zeros:
                reportDetails.report_info.drop_rows_with_all_zeros,
              show_row_total: reportDetails.report_info.show_row_total,
              show_col_total: reportDetails.report_info.show_col_total,
            });
            setSelectRow(tempRows);
            setSelectedTags("Row Summary");
            setColumn(reportDetails.report_info.pivot_columns);
            setPivotRow(reportDetails.report_info.pivot_rows);
            setPivotColumn(reportDetails.report_info.pivot_columns);
            $operatorValue(
              _.get(
                reportDetails,
                "report_info.column_condition_operators",
                []
              ) || []
            );
            $valueConditions(
              _.get(reportDetails, "report_info.column_condition_values", []) ||
                []
            );

            form.setFieldsValue({
              fields_rows: tempRows,
              percentage: reportDetails.report_info.view_in_percentage,
              drop_rows_with_all_zeros:
                reportDetails.report_info.drop_rows_with_all_zeros,
              show_row_total: reportDetails.report_info.show_row_total,
              show_col_total: reportDetails.report_info.show_col_total,
            });
            break;

          default:
            break;
        }
      } else {
        form.setFieldsValue({
          report_name: reportDetails.report_info.name,
          description: reportDetails.report_info.description,
          main_object: reportDetails.report_info.main_object,
        });
        setSelectedTags("Custom");
      }
    }
  }, [form, isEdit, onLoadFields, reportDetails, showReportSetting]);

  return (
    <ModalCustom
      title="Report setting"
      visible={showReportSetting}
      footer={null}
      width={900}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        labelCol={{ span: 7 }}
        wrapperCol={{ span: 17 }}
        colon={false}
        labelAlign="left"
      >
        <Steps current={currentStep}>
          <Step title="Information" />
          <Step title="Filter" />
          <Step title="Column" />
        </Steps>

        {currentStep === 0 ? (
          <>
            <Form.Item
              label="Report name"
              name="report_name"
              rules={[
                { required: true, message: "Please input name of report" },
              ]}
            >
              <Input placeholder="Input name of group" />
            </Form.Item>
            <Form.Item label="Description" name="description">
              <TextArea rows={3} placeholder="Input description of report" />
            </Form.Item>

            <Form.Item label="Folder report" name="folder_report">
              <Select
                onChange={(item, option) => {
                  setFolderCustom({
                    name: option.label,
                    id: option.value,
                  });
                }}
                placeholder="Folder report"
                options={_.chain(listFolders)
                  .filter((item) => item.readonly === false)
                  .map((item) => ({
                    label: item.name,
                    value: item._id,
                  }))
                  .value()}
              />
            </Form.Item>

            <Form.Item
              label="Report type"
              name="report_type"
              rules={[{ required: true, message: "Please select report type" }]}
            >
              {tagsData.map((tag, idx) => (
                <CheckableTag
                  id={`ucrm_report_type_${idx}`}
                  key={tag}
                  checked={selectedTags === tag}
                  onChange={(checked) => handleChange(tag, checked)}
                  style={{
                    cursor: isEdit ? "not-allowed" : "pointer",
                    background: isEdit && "#f5f5f5",
                  }}
                >
                  {tag}
                </CheckableTag>
              ))}
            </Form.Item>
            {selectedTags !== "Custom" && (
              <>
                <Form.Item
                  label="Main object"
                  name="main_object"
                  rules={[
                    { required: true, message: "Please select main object" },
                  ]}
                >
                  <Select
                    options={listObjects}
                    placeholder="Please select"
                    onChange={onLoadFields}
                    disabled={isEdit}
                    optionFilterProp="label"
                    showSearch
                  />
                </Form.Item>
                <Form.Item label="Share to group" name="share_group">
                  <Select
                    options={listMembers}
                    placeholder="Please select"
                    mode="multiple"
                    optionFilterProp="label"
                  />
                </Form.Item>
              </>
            )}
          </>
        ) : currentStep === 2 && selectedTags === "Pagination" ? (
          <>
            <Form.Item
              label="Select column"
              name="fields"
              rules={[{ required: true, message: "Please select column" }]}
            >
              <Select
                placeholder="Please select"
                mode="multiple"
                onChange={onSelectFields}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listFields.map((item) => (
                  <>
                    {item.options.map((option) => {
                      return (
                        <Option
                          disabled={
                            columnSelected.length > 24
                              ? columnSelected.includes(option.value)
                                ? false
                                : true
                              : false
                          }
                          key={option.value}
                        >
                          {option.label}
                        </Option>
                      );
                    })}
                  </>
                ))}
              </Select>
            </Form.Item>

            <SelectedColumn>
              <legend>
                Selected column <span>(Drag to change position)</span>
              </legend>

              <DndProvider backend={HTML5Backend}>
                <Container
                  column={column}
                  setColumn={setColumn}
                  handleRemoveColumn={handleRemoveColumn}
                />
              </DndProvider>
            </SelectedColumn>
          </>
        ) : currentStep === 2 && selectedTags === "Detail" ? (
          <>
            <Form.Item
              label="Select column"
              name="fields"
              rules={[{ required: true, message: "Please select column" }]}
            >
              <Select
                placeholder="Please select"
                mode="multiple"
                onChange={onSelectFields}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listFields.map((item) => (
                  <>
                    {item.options.map((option) => {
                      return (
                        <Option
                          disabled={
                            columnSelected.length > 24
                              ? columnSelected.includes(option.value)
                                ? false
                                : true
                              : false
                          }
                          key={option.value}
                        >
                          {option.label}
                        </Option>
                      );
                    })}
                  </>
                ))}
              </Select>
            </Form.Item>

            <SelectedColumn>
              <legend>
                Selected column <span>(Drag to change position)</span>
              </legend>

              <DndProvider backend={HTML5Backend}>
                <Container
                  column={column}
                  setColumn={setColumn}
                  handleRemoveColumn={handleRemoveColumn}
                />
              </DndProvider>
            </SelectedColumn>

            <Form.Item
              label="Metrics fields"
              name="metrics_fields"
              style={{ marginTop: 24 }}
            >
              <Select
                placeholder="Please select"
                mode="multiple"
                onChange={onSelectMetricsFields}
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {columnCal.map((item) => (
                  <Option key={item.id_field}>{item.column}</Option>
                ))}
              </Select>
            </Form.Item>

            <Calculator>
              <SubTitle>
                <span>Calculator</span>
                <hr />
              </SubTitle>

              <Table pagination={false} dataSource={columnCalSelected}>
                <Column title="Column" dataIndex="column" key="column" />
                <Column
                  title="Sum"
                  dataIndex="sum"
                  key="sum"
                  render={(text, record) => (
                    <Checkbox
                      checked={arithmetic[
                        arithmetic.findIndex(
                          (ele) => ele.id_field === record.id_field
                        )
                      ]?.ops.includes("sum")}
                      onChange={(e) =>
                        onCheckCal(e.target.checked, "sum", record)
                      }
                    />
                  )}
                />
                <Column
                  title="Avarage"
                  dataIndex="average"
                  key="average"
                  render={(text, record) => (
                    <Checkbox
                      checked={arithmetic[
                        arithmetic.findIndex(
                          (ele) => ele.id_field === record.id_field
                        )
                      ]?.ops.includes("average")}
                      onChange={(e) =>
                        onCheckCal(e.target.checked, "average", record)
                      }
                    />
                  )}
                />
                <Column
                  title="Minimum value"
                  dataIndex="min"
                  key="min"
                  render={(text, record) => (
                    <Checkbox
                      checked={arithmetic[
                        arithmetic.findIndex(
                          (ele) => ele.id_field === record.id_field
                        )
                      ]?.ops.includes("min")}
                      onChange={(e) =>
                        onCheckCal(e.target.checked, "min", record)
                      }
                    />
                  )}
                />
                <Column
                  title="Maximum value"
                  dataIndex="max"
                  key="max"
                  render={(text, record) => (
                    <Checkbox
                      checked={arithmetic[
                        arithmetic.findIndex(
                          (ele) => ele.id_field === record.id_field
                        )
                      ]?.ops.includes("max")}
                      onChange={(e) =>
                        onCheckCal(e.target.checked, "max", record)
                      }
                    />
                  )}
                />
              </Table>
            </Calculator>
          </>
        ) : currentStep === 2 && selectedTags === "Summary" ? (
          <>
            <FormRow>
              <FormCustom
                label={
                  <span>
                    <span>Select rows</span> <br /> <p>(Max: 5)</p>
                  </span>
                }
                name="fields_rows"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 8 }}
                rules={[{ required: true, message: "Please select row" }]}
              >
                <Select
                  placeholder="Please select"
                  mode="multiple"
                  onChange={onSelectRow}
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {listFields.map((item) => (
                    <>
                      {item.options.map((option) => (
                        <>
                          {!_.get(option, "encrypted", false) && (
                            <Option
                              disabled={
                                selectRow.length > 4
                                  ? selectRow.includes(option.value)
                                    ? false
                                    : true
                                  : selectColumn.includes(option.value)
                                  ? true
                                  : false
                              }
                              key={option.value}
                            >
                              {option.label}
                            </Option>
                          )}
                        </>
                      ))}
                    </>
                  ))}
                </Select>
              </FormCustom>

              <Form.Item
                label=""
                // labelCol={{ span: 16 }}
                // wrapperCol={{ span: 8 }}
                // labelAlign="right"
                name="show_row_total"
                valuePropName="checked"
                style={{ paddingBottom: "12px" }}
              >
                <Checkbox
                  style={{ fontSize: "16px" }}
                  onChange={(e) =>
                    _onCheckSummary(e.target.checked, "show_row_total")
                  }
                >
                  Show total
                </Checkbox>
              </Form.Item>
            </FormRow>

            <FormRow>
              <FormCustom
                label={
                  <span>
                    <span>Select column</span> <br /> <p>(Max: 1)</p>
                  </span>
                }
                name="fields_column"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 8 }}
              >
                <Select
                  placeholder="Please select"
                  mode="multiple"
                  onChange={(value, list) => {
                    let newValue = [];
                    let newList = [];

                    if (value.slice(-1)[0]) {
                      newValue.push(value.slice(-1)[0]);

                      form.setFieldsValue({
                        fields_column: value.slice(-1)[0],
                      });
                    } else {
                      form.setFieldsValue({
                        fields_column: undefined,
                      });
                    }

                    if (list.slice(-1)[0]) {
                      newList.push(list.slice(-1)[0]);
                    }

                    onSelectColumn(newValue, newList);
                  }}
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {listFields.map((item) => (
                    <>
                      {item.options.map((option) => (
                        <>
                          {!_.get(option, "encrypted", false) && (
                            <Option key={option.value}>{option.label}</Option>
                          )}
                        </>
                      ))}
                    </>
                  ))}
                </Select>
              </FormCustom>

              <Form.Item
                label=""
                // labelCol={{ span: 16 }}
                // wrapperCol={{ span: 8 }}
                // labelAlign="right"
                name="show_col_total"
                valuePropName="checked"
                style={{ paddingBottom: "12px" }}
              >
                <Checkbox
                  style={{ fontSize: "16px" }}
                  onChange={(e) =>
                    _onCheckSummary(e.target.checked, "show_col_total")
                  }
                >
                  Show total
                </Checkbox>
              </Form.Item>
            </FormRow>

            <Form.Item
              label=""
              // labelCol={{ span: 0 }}
              wrapperCol={{ span: 10 }}
              // labelAlign="right"
              name="drop_rows_with_all_zeros"
              valuePropName="checked"
            >
              <Checkbox
                style={{ fontSize: "16px" }}
                onChange={(e) =>
                  _onCheckSummary(e.target.checked, "drop_rows_with_all_zeros")
                }
              >
                Remove rows that contains all zeros
              </Checkbox>
            </Form.Item>

            <Form.Item
              label="Show percentage"
              name="percentage"
              labelCol={{ span: 5 }}
              wrapperCol={{ span: 11 }}
            >
              <Select
                defaultValue="hidden"
                placeholder="Please select"
                options={percentageOptions}
                onChange={handleChangePercentage}
              />
              {/* {tagsPercentage.map((tag) => (
                <CheckableTag
                  key={tag}
                  checked={isShowPercentage.indexOf(tag) > -1}
                  onChange={(checked) => handleChangePercentage(tag, checked)}
                >
                  {tag}
                </CheckableTag>
              ))} */}
            </Form.Item>

            <AddValue>
              <legend>
                Add value <span>(Max 5 field)</span>
              </legend>

              {valueSummary.length > 0 && (
                <>
                  {valueSummary.map((item, idx) => {
                    return (
                      <FormValueSummary>
                        <FormValue
                          labelCol={{ span: 6 }}
                          wrapperCol={{ span: 18 }}
                          label="Field"
                          name={item.field_ID + idx}
                          rules={[
                            { required: true, message: "Please select field" },
                          ]}
                        >
                          <Select
                            placeholder="Select field"
                            onChange={(e, option) => {
                              handleChangeField(e, option, idx, "field");
                            }}
                            showSearch
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                          >
                            {listFieldsMain
                              .filter(
                                (field) => field?.formula_type !== "dateToParts"
                              )
                              .map((item) => (
                                <>
                                  {!_.get(item, "encrypted", false) && (
                                    <Option
                                      disabled={
                                        valueSummary.find(
                                          (field) =>
                                            field.field_ID === item.value
                                        )
                                          ? true
                                          : false
                                      }
                                      key={item.value}
                                    >
                                      {item.label}
                                    </Option>
                                  )}
                                </>
                              ))}
                          </Select>
                        </FormValue>
                        <FormValue
                          label="Measure"
                          name={`measure_${item.field_ID}${idx}`}
                          rules={[
                            {
                              required: true,
                              message: "Please select measure",
                            },
                          ]}
                        >
                          <Select
                            placeholder="Select measure"
                            options={
                              item.field_type === "number" ||
                              item.formula_type === "dateDiff" ||
                              item.formula_type === "dateToParts" ||
                              item.formula_type === "advanceExpression"
                                ? measuresOptionsNumber
                                : item.field_type === ""
                                ? []
                                : measuresOptions
                            }
                            onChange={(e, option) => {
                              handleChangeField(e, option, idx, "measure");
                            }}
                          />
                        </FormValue>

                        <FormValue
                          label="Display name"
                          name={`display_name_${item.field_ID}${idx}`}
                        >
                          <Input
                            placeholder="Input display name"
                            onChange={(e) =>
                              handleChangeField(
                                e.target.value,
                                [],
                                idx,
                                "display"
                              )
                            }
                          />
                        </FormValue>

                        <Delete>
                          <Tooltip title="Delete">
                            <img
                              src={DeleteIcon}
                              onClick={() => onDeleteField(idx)}
                              alt="delete"
                            />
                          </Tooltip>
                        </Delete>
                      </FormValueSummary>
                    );
                  })}
                </>
              )}

              {valueSummary.length < 5 && (
                <AddNew onClick={() => _onAddValueSummary()}>
                  {/* <img src={PlusGreen} alt="plus" /> */}
                  <span>+ Add value</span>
                </AddNew>
              )}
            </AddValue>
          </>
        ) : currentStep === 2 && selectedTags === "Row Summary" ? (
          <>
            <FormRow>
              <FormCustom
                label={
                  <span>
                    <span>Select rows</span> <br /> <p>(Max: 5)</p>
                  </span>
                }
                name="fields_rows"
                labelCol={{ span: 6 }}
                wrapperCol={{ span: 8 }}
                rules={[{ required: true, message: "Please select row" }]}
              >
                <Select
                  placeholder="Please select"
                  mode="multiple"
                  onChange={onSelectRow}
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {listFields.map((item) => (
                    <>
                      {item.options.map((option) => (
                        <>
                          {!_.get(option, "encrypted", false) && (
                            <Option
                              disabled={
                                selectRow.length > 4
                                  ? selectRow.includes(option.value)
                                    ? false
                                    : true
                                  : selectColumn.includes(option.value)
                                  ? true
                                  : false
                              }
                              key={option.value}
                            >
                              {option.label}
                            </Option>
                          )}
                        </>
                      ))}
                    </>
                  ))}
                </Select>
              </FormCustom>

              <Form.Item
                label=""
                // labelCol={{ span: 16 }}
                // wrapperCol={{ span: 8 }}
                // labelAlign="right"
                name="show_row_total"
                valuePropName="checked"
                style={{ paddingBottom: "12px" }}
              >
                <Checkbox
                  style={{ fontSize: "16px" }}
                  onChange={(e) =>
                    _onCheckSummary(e.target.checked, "show_row_total")
                  }
                >
                  Show total
                </Checkbox>
              </Form.Item>
            </FormRow>

            <Form.Item
              label=""
              name="show_col_total"
              valuePropName="checked"
              style={{ marginBottom: "0" }}
            >
              <Checkbox
                style={{ fontSize: "16px" }}
                onChange={(e) =>
                  _onCheckSummary(e.target.checked, "show_col_total")
                }
              >
                Show column total
              </Checkbox>
            </Form.Item>

            <Form.Item
              label=""
              wrapperCol={{ span: 10 }}
              name="drop_rows_with_all_zeros"
              valuePropName="checked"
            >
              <Checkbox
                style={{ fontSize: "16px" }}
                onChange={(e) =>
                  _onCheckSummary(e.target.checked, "drop_rows_with_all_zeros")
                }
              >
                Remove rows that contains all zeros
              </Checkbox>
            </Form.Item>

            <Form.Item
              label="Show percentage"
              name="percentage"
              labelCol={{ span: 5 }}
              wrapperCol={{ span: 11 }}
            >
              <Select
                defaultValue="hidden"
                placeholder="Please select"
                options={percentageOptions}
                onChange={handleChangePercentage}
              />
            </Form.Item>

            <div
              style={{
                marginBottom: "16px",
                display: "flex",
                justifyContent: "space-between",
                fontSize: "16px",
              }}
            >
              <div>
                <span style={{ fontFamily: "var(--roboto-500)" }}>
                  {t("report.addColumn")}
                </span>

                <span> ({t("report.max10Fields")})</span>
              </div>

              <ButtonAddNew onClick={_onAddColumnRowSummary}>
                {t("report.addNew")}
              </ButtonAddNew>
            </div>
            <DndProvider backend={HTML5Backend}>
              <ContainerPanel
                column={column}
                setColumn={setColumn}
                handleRemoveColumn={handleRemoveColumn}
                listFields={listFields}
                onChangeColumnRowSummary={onChangeColumnRowSummary}
                reportDetails={reportDetails}
                handleChangeConditions={handleChangeConditions}
                operatorValue={operatorValue}
                $operatorValue={$operatorValue}
                valueConditions={valueConditions}
                $valueConditions={$valueConditions}
                isActive={isActive}
                $isActive={$isActive}
                newIndex={newIndex}
                $newIndex={$newIndex}
              />
            </DndProvider>
          </>
        ) : (
          ""
        )}

        <WrapConditions currentStep={currentStep}>
          <TimeRange>
            <legend>
              Time range
              <Tooltip title={t("report.desTimeRange")}>
                <InfoCircleOutlined
                  style={{ marginLeft: "8px", fontSize: "18px" }}
                />
              </Tooltip>
            </legend>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                width: "100%",
              }}
            >
              <div style={{ display: "flex" }}>
                <Select
                  value={_.get(
                    timeRangeFilter,
                    "filter_field_id",
                    "modify_time"
                  )}
                  onChange={handlerTimeRange.filterField}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {(
                    listFieldsMain.filter(
                      (item) =>
                        item?.type === "datetime-local" || item?.type === "date"
                    ) || []
                  ).map((item) => (
                    <Option
                      key={item.value}
                      style={{ fontWeight: item.is_indexed ? "700" : "400" }}
                      type={item.type}
                    >
                      {item.label}
                    </Option>
                  ))}
                </Select>
                <Select
                  options={optionsTimeRangeReport}
                  value={_.get(timeRangeFilter, "filter_type", "all-time")}
                  onChange={handlerTimeRange.filterType}
                />

                {(_.get(timeRangeFilter, "filter_type", "") === "last-n-days" ||
                  _.get(timeRangeFilter, "filter_type", "") ===
                    "last-n-weeks" ||
                  _.get(timeRangeFilter, "filter_type", "") ===
                    "last-n-months" ||
                  _.get(timeRangeFilter, "filter_type", "") ===
                    "last-n-years") && (
                  <InputNumber
                    value={_.get(timeRangeFilter, "amount_of_time", 3)}
                    min={1}
                    onChange={handlerTimeRange.amountOfTime}
                  />
                )}

                {_.get(timeRangeFilter, "filter_type", "") === "custom" && (
                  <>
                    <DatePicker
                      showTime={
                        _.get(timeRangeFilter, "field_type", "") ===
                        "datetime-local"
                      }
                      onChange={handlerTimeRange.startTime}
                      placeholder={t("knowledgeBase.startDate")}
                      disabledDate={disabledStartDate}
                      value={
                        _.get(timeRangeFilter, "start_time")
                          ? moment(_.get(timeRangeFilter, "start_time"))
                          : undefined
                      }
                    />
                    <DatePicker
                      showTime={
                        _.get(timeRangeFilter, "field_type", "") ===
                        "datetime-local"
                      }
                      onChange={handlerTimeRange.endTime}
                      placeholder={t("knowledgeBase.endDate")}
                      disabledDate={disabledEndDate}
                      value={
                        _.get(timeRangeFilter, "end_time")
                          ? moment(_.get(timeRangeFilter, "end_time"))
                          : undefined
                      }
                    />
                  </>
                )}
              </div>

              {listFieldsMain.find(
                (item) =>
                  _.get(item, "value") ===
                  _.get(timeRangeFilter, "filter_field_id")
              )?.is_indexed === false && (
                <p
                  style={{
                    marginBottom: 0,
                    marginTop: "8px",
                    color: "#708090",
                  }}
                >
                  <WarningOutlined
                    style={{ color: "#eb7029", fontSize: "18px" }}
                  />{" "}
                  {t("report.warningIndex")}
                </p>
              )}
            </div>
          </TimeRange>

          <Conditions
            title={"AND condition"}
            decs={"(All conditions must be met)"}
            conditions={allCondition}
            setConditions={setAllCondition}
            ID={
              Object.keys(reportDetails).length > 0 &&
              reportDetails.report_info._id
                ? reportDetails.report_info._id
                : ""
            }
            dataDetails={
              Object.keys(reportDetails).length > 0 &&
              (reportDetails.report_info || {})
            }
            operatorValue={operatorValueAnd}
            setOperatorValue={setOperatorValueAnd}
            value={valueAnd}
            setValue={setValueAnd}
          />
          <Conditions
            title={"OR condition"}
            decs={"(Any conditions must be met)"}
            conditions={anyCondition}
            setConditions={setAnyCondition}
            ID={
              Object.keys(reportDetails).length > 0 &&
              reportDetails.report_info._id
                ? reportDetails.report_info._id
                : ""
            }
            dataDetails={
              Object.keys(reportDetails).length > 0 &&
              (reportDetails.report_info || {})
            }
            operatorValue={operatorValueOr}
            setOperatorValue={setOperatorValueOr}
            value={valueOr}
            setValue={setValueOr}
          />
        </WrapConditions>

        <WrapButton label=" ">
          {currentStep > 0 && (
            <Back onClick={() => _onPrev()}>{t("common.back")}</Back>
          )}
          {currentStep === 2 ? (
            <Button
              type="primary"
              htmlType="submit"
              loading={isLoading}
              onClick={() => _onSubmit()}
            >
              {t("common.save")}
            </Button>
          ) : (
            <Button type="primary" onClick={() => _onNext()}>
              {t("common.next")}
            </Button>
          )}
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ReportSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-steps {
    margin-bottom: 24px;
  }

  .ant-steps-item-process .ant-steps-item-icon {
    background: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-tag-checkable-checked {
    background-color: #fff;
  }

  .ant-tag-checkable {
    border-color: #d9d9d9;
  }

  .ant-tag-checkable:active,
  .ant-tag-checkable-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-tag {
    height: 32px;
    width: 112px;
    padding: 4px 0;
    text-align: center;
    font-size: 16px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin: 0 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Back = styled(Button)`
  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;

const SelectedColumn = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }
  }
`;

const TimeRange = styled(SelectedColumn)`
  display: flex;
  margin-bottom: 16px;
  padding-bottom: 16px;

  legend {
    display: flex;
    align-items: center;
  }

  .ant-select {
    width: 28%;
    margin-right: 16px;
  }

  .ant-input-number {
    width: 28%;
  }
`;

const Calculator = styled.div`
  margin-top: 24px;
  margin-bottom: 8px;

  .ant-table-thead > tr > th {
    text-align: center;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;

    :first-child {
      width: 144px;
    }

    :nth-child(2) {
      width: 60px;
    }

    :nth-child(3) {
      width: 85px;
    }

    :nth-child(4) {
      width: 130px;
    }

    :last-child {
      width: 130px;
      border-right: none;
    }

    ::before {
      display: none;
    }
  }

  .ant-table-tbody > tr > td {
    text-align: center;

    :first-child {
      width: 144px;
      text-align: left;
    }

    :nth-child(2) {
      width: 60px;
    }

    :nth-child(3) {
      width: 85px;
    }

    :nth-child(4) {
      width: 130px;
    }

    :last-child {
      width: 130px;
    }
  }
`;

const SubTitle = styled.div`
  display: flex;
  align-items: center;

  span {
    font-size: 16px;
    font-family: var(--roboto-700);
    width: 90px;
  }

  hr {
    width: calc(100% - 90px);
    border-top: 1px solid #d9d9d9;
  }
`;

const AddValue = styled(SelectedColumn)``;

const FormCustom = styled(Form.Item)`
  width: 70%;
  padding-right: 16px;

  .ant-form-item-label > label {
    height: fit-content;

    ::before {
      padding-bottom: 20px;
    }
  }

  p {
    color: #ff9900;
    font-size: 14px;
    margin-bottom: 0;
  }
`;

const FormValueSummary = styled(Form.Item)`
  width: 100%;
  padding: 0 16px;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }

  .ant-col {
    max-width: 100% !important;
  }
`;

const FormRow = styled(Form.Item)`
  width: 100%;
  margin-bottom: 0;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }

  .ant-col {
    max-width: 100% !important;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const FormValue = styled(Form.Item)`
  margin-bottom: 0;
  width: 30%;
  margin-right: 16px;
`;

const AddNew = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const WrapConditions = styled.div`
  opacity: ${({ currentStep }) => (currentStep === 1 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 1 ? "100%" : "0")};
  width: ${({ currentStep }) => (currentStep === 1 ? "100%" : "0")};
  pointer-events: ${({ currentStep }) => (currentStep === 1 ? "auto" : "none")};
`;

const ButtonAddNew = styled.div`
  color: ${(props) => props.theme.main};
  cursor: pointer;
`;
