import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import Breadcrumb from "antd/lib/breadcrumb";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import TimePicker from "antd/lib/time-picker";
import DatePicker from "antd/lib/date-picker";
import Tooltip from "antd/lib/tooltip";

import {
  updateWorkflow,
  loadDataNecessaryEdit,
  detailsWorkflow,
  loadAllActions,
  setStatus,
  unMountDetailsWorkflows,
} from "redux/slices/workflows";
import { getListTemplate } from "redux/slices/templateFile";
import { getListAccountMapping } from "redux/slices/googleIntegration";
import Conditions from "components/Conditions/conditions";
import Actions from "./Actions/actions";
import { Notification } from "components/Notification/Noti";

import {
  triggerWorkflow,
  typeInteraction,
  frequencyWorkflow,
  week,
  days,
} from "util/staticData";

const { Option } = Select;

function EditWorkflow(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { recordID } = useParams();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { details, userFields, listActions, status, listPopup } = useSelector(
    (state) => state.workflowsReducer
  );

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const [listObjects, setListObjects] = useState([]);
  const [listMainFields, setListMainFields] = useState([]);

  const [trigger, setTrigger] = useState(0);
  const [frequency, setFrequency] = useState(0);

  const [numberDateYearly, setNumberDateYearly] = useState([""]);
  const [dateYearly, setDateYearly] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const [isEdit, setIsEdit] = useState(false);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "workflow" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSubmit = () => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    let flagForm = false;
    Object.values(form.getFieldsValue()).forEach((item, index) => {
      if (index !== 1) {
        if (Array.isArray(item)) {
          if (item.length === 0) {
            flagForm = true;
          }
        } else {
          if (!item) {
            flagForm = true;
          }
        }
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else if (
      // !form.getFieldValue("name") ||
      // (form.getFieldValue("trigger") === 5 &&
      //   !form.getFieldValue("check_field_changed"))
      flagForm
    ) {
      Notification("warning", "Please fullfill required!");
    } else {
      const dataForm = form.getFieldsValue();

      let data = {
        workflow_name: dataForm.name,
        description: dataForm.description,
        execution_condition: dataForm.trigger,
        object_id: dataForm.object,
        filter_condition: { and_filter: allCondition, or_filter: anyCondition },
      };

      if (dataForm.trigger === 4) {
        switch (dataForm.frequency) {
          case 1:
            data = {
              ...data,
              sch_annual_dates: [],
              sch_day_of_month: [],
              sch_day_of_week: [],
              sch_type_id: 1,
              schedule_time:
                "00:" + moment(dataForm.timeHourly).format("mm:ss"),
            };

            break;
          case 2:
            data = {
              ...data,
              sch_annual_dates: [],
              sch_day_of_month: [],
              sch_day_of_week: [],
              sch_type_id: 2,
              schedule_time: moment(dataForm.timeDaily).format("HH:mm:ss"),
            };
            break;
          case 3:
            data = {
              ...data,
              sch_annual_dates: [],
              sch_day_of_month: [],
              sch_day_of_week: dataForm.dateWeekly,
              sch_type_id: 3,
              schedule_time: moment(dataForm.timeWeekly).format("HH:mm:ss"),
            };
            break;
          case 4:
            data = {
              ...data,
              sch_annual_dates: [
                moment(dataForm.dateSpecific).format("YYYY-MM-DD"),
              ],
              sch_day_of_month: [],
              sch_day_of_week: [],
              sch_type_id: 4,
              schedule_time: moment(dataForm.timeSpecific).format("HH:mm:ss"),
            };
            break;
          case 5:
            data = {
              ...data,
              sch_annual_dates: [],
              sch_day_of_month: dataForm.dateMonthly,
              sch_day_of_week: [],
              sch_type_id: 5,
              schedule_time: moment(dataForm.timeMonthly).format("HH:mm:ss"),
            };

            break;
          case 6:
            data = {
              ...data,
              sch_annual_dates: dateYearly,
              sch_day_of_month: [],
              sch_day_of_week: [],
              sch_type_id: 6,
              schedule_time: moment(dataForm.timeYearly).format("HH:mm:ss"),
            };
            break;

          default:
            break;
        }
      } else if (dataForm.trigger === 5) {
        data = {
          ...data,
          check_field_changed: dataForm.check_field_changed,
          sch_annual_dates: [],
          sch_day_of_month: [],
          sch_day_of_week: [],
          sch_type_id: 0,
          schedule_time: "",
        };
      } else if (dataForm.trigger === 6) {
        data = {
          ...data,
          interaction_type: dataForm.interaction_type,
          sch_annual_dates: [],
          sch_day_of_month: [],
          sch_day_of_week: [],
          sch_type_id: 0,
          schedule_time: "",
        };
      } else if (dataForm.trigger === 7) {
        data = {
          ...data,
          url_popup: _.get(dataForm, "url_popup"),
          sch_annual_dates: [],
          sch_day_of_month: [],
          sch_day_of_week: [],
          sch_type_id: 0,
          schedule_time: "",
        };
      } else {
        data = {
          ...data,
          sch_annual_dates: [],
          sch_day_of_month: [],
          sch_day_of_week: [],
          sch_type_id: 0,
          schedule_time: "",
        };
      }

      dispatch(
        updateWorkflow({
          data: {
            ...data,
          },
          _id: details._id,
        })
      );
    }
  };

  const _onCancel = () => {
    form.resetFields();
    setTrigger(0);
    setFrequency(0);
    setDateYearly([]);
    setNumberDateYearly([""]);
    navigate("/workflows");
  };

  const handleSelectTrigger = (value) => {
    setTrigger(value);

    form.setFieldsValue({
      frequency: undefined,
    });
    setFrequency(0);
  };

  const handleSelectFrequency = (value) => {
    setFrequency(value);

    if (details.execution_condition === 4) {
      switch (details.sch_type_id) {
        case 1:
          form.setFieldsValue({
            timeDaily: undefined,
            dateWeekly: undefined,
            timeWeekly: undefined,
            dateSpecific: undefined,
            timeSpecific: undefined,
            dateMonthly: undefined,
            timeMonthly: undefined,
            timeYearly: undefined,
          });
          numberDateYearly.forEach((item, idx) => {
            form.setFieldsValue({
              ["dateYearly" + idx]: undefined,
            });
          });

          setDateYearly([]);
          setNumberDateYearly([""]);
          break;
        case 2:
          form.setFieldsValue({
            timeHourly: undefined,
            dateWeekly: undefined,
            timeWeekly: undefined,
            dateSpecific: undefined,
            timeSpecific: undefined,
            dateMonthly: undefined,
            timeMonthly: undefined,
            timeYearly: undefined,
          });
          numberDateYearly.forEach((item, idx) => {
            form.setFieldsValue({
              ["dateYearly" + idx]: undefined,
            });
          });

          setDateYearly([]);
          setNumberDateYearly([""]);
          break;
        case 3:
          form.setFieldsValue({
            timeHourly: undefined,
            timeDaily: undefined,
            dateSpecific: undefined,
            timeSpecific: undefined,
            dateMonthly: undefined,
            timeMonthly: undefined,
            timeYearly: undefined,
          });
          numberDateYearly.forEach((item, idx) => {
            form.setFieldsValue({
              ["dateYearly" + idx]: undefined,
            });
          });

          setDateYearly([]);
          setNumberDateYearly([""]);
          break;
        case 4:
          form.setFieldsValue({
            timeHourly: undefined,
            timeDaily: undefined,
            dateWeekly: undefined,
            timeWeekly: undefined,
            dateMonthly: undefined,
            timeMonthly: undefined,
            timeYearly: undefined,
          });
          numberDateYearly.forEach((item, idx) => {
            form.setFieldsValue({
              ["dateYearly" + idx]: undefined,
            });
          });

          setDateYearly([]);
          setNumberDateYearly([""]);
          break;
        case 5:
          form.setFieldsValue({
            timeHourly: undefined,
            timeDaily: undefined,
            dateWeekly: undefined,
            timeWeekly: undefined,
            dateSpecific: undefined,
            timeSpecific: undefined,
            timeYearly: undefined,
          });
          numberDateYearly.forEach((item, idx) => {
            form.setFieldsValue({
              ["dateYearly" + idx]: undefined,
            });
          });

          setDateYearly([]);
          setNumberDateYearly([""]);
          break;

        case 6:
          form.setFieldsValue({
            timeHourly: undefined,
            timeDaily: undefined,
            dateWeekly: undefined,
            timeWeekly: undefined,
            dateSpecific: undefined,
            timeSpecific: undefined,
            dateMonthly: undefined,
            timeMonthly: undefined,
          });
          break;

        default:
          break;
      }
    } else {
      form.setFieldsValue({
        timeHourly: undefined,
        timeDaily: undefined,
        dateWeekly: undefined,
        timeWeekly: undefined,
        dateSpecific: undefined,
        timeSpecific: undefined,
        dateMonthly: undefined,
        timeMonthly: undefined,
        timeYearly: undefined,
      });

      numberDateYearly.forEach((item, idx) => {
        form.setFieldsValue({
          ["dateYearly" + idx]: undefined,
        });
      });

      setDateYearly([]);
      setNumberDateYearly([""]);
    }
  };

  const _onAddDate = () => {
    let tempList = [...numberDateYearly];
    tempList.push("");
    setNumberDateYearly(tempList);
  };

  const _onDeleteDate = (idx) => {
    let tempList = [...numberDateYearly];
    tempList.splice(idx, 1);

    let tempValue = [...dateYearly];
    tempValue.splice(idx, 1);

    tempValue.forEach((item, index) => {
      form.setFieldsValue({
        ["dateYearly" + index]: moment(item),
      });
      if (index === tempValue.length - 1) {
        let tempField = index + 1;
        form.setFieldsValue({
          ["dateYearly" + tempField]: undefined,
        });
      }
    });

    setDateYearly(tempValue);
    setNumberDateYearly(tempList);
  };

  const handleSelectDateYearly = (idx, dateString) => {
    let tempList = [...dateYearly];
    tempList[idx] = dateString;
    setDateYearly(tempList);
  };

  const disabledDate = (current) => {
    return dateYearly.find(
      (date) => date === moment(current).format("YYYY-MM-DD")
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListMainFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (Object.keys(details).length > 0) {
      form.setFieldsValue({
        name: details.workflow_name,
        description: details.description,
        object: details.object_id,
        trigger: details.execution_condition,
      });

      if (details.execution_condition === 4) {
        form.setFieldsValue({
          frequency: details.sch_type_id,
        });

        switch (details.sch_type_id) {
          case 1:
            form.setFieldsValue({
              timeHourly: moment(
                details.schedule_time.substring(3, 8),
                "mm:ss"
              ),
            });
            break;
          case 2:
            form.setFieldsValue({
              timeDaily: moment(details.schedule_time, "HH:mm:ss"),
            });
            break;
          case 3:
            form.setFieldsValue({
              dateWeekly: details.sch_day_of_week,
              timeWeekly: moment(details.schedule_time, "HH:mm:ss"),
            });
            break;
          case 4:
            form.setFieldsValue({
              dateSpecific: moment(details.sch_annual_dates[0]),
              timeSpecific: moment(details.schedule_time, "HH:mm:ss"),
            });
            break;
          case 5:
            form.setFieldsValue({
              dateMonthly: details.sch_day_of_month,
              timeMonthly: moment(details.schedule_time, "HH:mm:ss"),
            });
            break;

          case 6:
            form.setFieldsValue({
              timeYearly: moment(details.schedule_time, "HH:mm:ss"),
            });

            let tempNumBerDateYearly = [];

            details.sch_annual_dates.forEach((item, idx) => {
              form.setFieldsValue({
                ["dateYearly" + idx]: moment(item),
              });
              tempNumBerDateYearly.push("");
            });

            setNumberDateYearly(tempNumBerDateYearly);
            setDateYearly(details.sch_annual_dates);
            break;

          default:
            break;
        }
      }

      if (details.execution_condition === 5) {
        form.setFieldsValue({
          check_field_changed: details.check_field_changed,
        });
      }
      if (details.execution_condition === 6) {
        form.setFieldsValue({
          interaction_type: details.interaction_type,
        });
      }
      if (details.execution_condition === 7) {
        form.setFieldsValue({
          url_popup: _.get(details, "url_popup", ""),
        });
      }

      setTrigger(details.execution_condition);
      setFrequency(details.sch_type_id);
      setAllCondition(details.filter_condition.and_filter);
      setAnyCondition(details.filter_condition.or_filter);
    }
  }, [details, form]);

  useEffect(() => {
    dispatch(
      detailsWorkflow({
        _id: recordID,
      })
    );
    dispatch(
      loadAllActions({
        workflow_id: recordID,
      })
    );
  }, [dispatch, recordID]);

  useEffect(() => {
    if (details.object_id !== undefined) {
      dispatch(
        loadDataNecessaryEdit({
          object_id: details.object_id,
        })
      );
    }
  }, [dispatch, details.object_id]);

  useEffect(() => {
    if (status === "Create action successfully!") {
      dispatch(
        loadAllActions({
          workflow_id: recordID,
        })
      );
      dispatch(setStatus(null));
    }

    if (status === "Update action successfully!") {
      setIsEdit(false);
      dispatch(setStatus(null));
    }
  }, [dispatch, status, recordID]);

  useEffect(() => {
    dispatch(getListTemplate());
    dispatch(getListAccountMapping());
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(unMountDetailsWorkflows());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <Breadcrumb.Item onClick={() => navigate("/workflows")}>
          {t("settings.workflow")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("workflow.editworkflow")}</BreadcrumbItem>
      </Breadcrumb>

      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <WrapInfo>
          <WrapBasic>
            <BasicInfo>
              <legend>{t("workflow.basicInfo")}</legend>
              <Form.Item
                label={t("workflow.nameWorkflow")}
                name="name"
                rules={[
                  {
                    required: true,
                    message: t("common.placeholderInput"),
                  },
                ]}
              >
                <Input
                  disabled={!checkRule("edit")}
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>
              <Form.Item label={t("common.description")} name="description">
                <Input
                  disabled={!checkRule("edit")}
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>
              <Form.Item
                label={t("object.object")}
                name="object"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listObjects}
                  disabled
                />
              </Form.Item>
            </BasicInfo>

            <Trigger>
              <legend>{t("workflow.workflowTrigger")}</legend>
              <Form.Item
                label={t("workflow.triggerOn")}
                name="trigger"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  disabled={!checkRule("edit")}
                  onChange={handleSelectTrigger}
                  placeholder={t("common.placeholderSelect")}
                >
                  {triggerWorkflow.map((item, index) => {
                    return (
                      <Option key={index} value={item.value}>
                        {t(item.label)}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>

              {trigger === 7 && (
                <Form.Item
                  label={t("workflow.urlPopup")}
                  name="url_popup"
                  rules={[
                    { required: true, message: t("common.placeholderSelect") },
                  ]}
                >
                  <Select
                    placeholder={t("common.placeholderSelect")}
                    options={_.map(listPopup, (obj) =>
                      _.mapValues(
                        _.mapKeys(obj, (value, key) => {
                          if (key === "name") {
                            return "label";
                          } else if (key === "ID") {
                            return "value";
                          }
                          return key;
                        })
                      )
                    )}
                    optionFilterProp="label"
                    showSearch
                  />
                </Form.Item>
              )}

              {trigger === 6 && (
                <Form.Item
                  label={t("highlightSetting.selectType")}
                  name="interaction_type"
                  rules={[
                    { required: true, message: t("common.placeholderSelect") },
                  ]}
                >
                  <Select
                    placeholder={t("common.placeholderSelect")}
                    options={typeInteraction}
                    optionFilterProp="label"
                    showSearch
                  />
                </Form.Item>
              )}

              {trigger === 5 && (
                <Form.Item
                  label={t("workflow.selectField")}
                  name="check_field_changed"
                  rules={[
                    { required: true, message: t("common.placeholderSelect") },
                  ]}
                >
                  <Select
                    disabled={!checkRule("edit")}
                    placeholder={t("common.placeholderSelect")}
                    options={listMainFields}
                    optionFilterProp="label"
                    showSearch
                  />
                </Form.Item>
              )}

              {trigger === 4 && (
                <Frequency>
                  <legend>{t("workflow.Frequency")}</legend>
                  <Form.Item
                    label={t("workflow.SelectFrequency")}
                    name="frequency"
                    rules={[
                      {
                        required: true,
                        message: t("common.placeholderSelect"),
                      },
                    ]}
                  >
                    <Select
                      disabled={!checkRule("edit")}
                      onChange={handleSelectFrequency}
                      placeholder={t("common.placeholderSelect")}
                    >
                      {frequencyWorkflow.map((item, index) => {
                        return (
                          <Option key={index} value={item.value}>
                            {t(item.label)}
                          </Option>
                        );
                      })}
                    </Select>
                  </Form.Item>

                  {frequency === 1 && (
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeHourly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker
                        disabled={!checkRule("edit")}
                        format="mm:ss"
                        placeholder="mm:ss"
                      />
                    </Form.Item>
                  )}

                  {frequency === 2 && (
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeDaily"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker
                        disabled={!checkRule("edit")}
                        placeholder="HH:mm:ss"
                      />
                    </Form.Item>
                  )}

                  {frequency === 3 && (
                    <>
                      <Form.Item
                        label={t("workflow.OnTheseDays")}
                        name="dateWeekly"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <Select
                          disabled={!checkRule("edit")}
                          mode="multiple"
                          placeholder={t("common.placeholderSelect")}
                          optionFilterProp="label"
                        >
                          {" "}
                          {week.map((item, index) => {
                            return (
                              <Option key={index} value={item.value}>
                                {t(item.label)}
                              </Option>
                            );
                          })}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        label={t("workflow.AtTime")}
                        name="timeWeekly"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <TimePicker
                          disabled={!checkRule("edit")}
                          placeholder="HH:mm:ss"
                        />
                      </Form.Item>
                    </>
                  )}

                  {frequency === 4 && (
                    <>
                      <Form.Item
                        label={t("workflow.ChooseDay")}
                        name="dateSpecific"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <DatePicker
                          disabled={!checkRule("edit")}
                          placeholder={t("common.placeholderSelect")}
                        />
                      </Form.Item>
                      <Form.Item
                        label={t("workflow.AtTime")}
                        name="timeSpecific"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <TimePicker
                          disabled={!checkRule("edit")}
                          placeholder="HH:mm:ss"
                        />
                      </Form.Item>
                    </>
                  )}
                  {frequency === 5 && (
                    <>
                      <Form.Item
                        label={t("workflow.ChooseDay")}
                        name="dateMonthly"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <Select
                          disabled={!checkRule("edit")}
                          options={days}
                          mode="multiple"
                          placeholder={t("common.placeholderSelect")}
                          optionFilterProp="label"
                        />
                      </Form.Item>
                      <Form.Item
                        label={t("workflow.AtTime")}
                        name="timeMonthly"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <TimePicker
                          disabled={!checkRule("edit")}
                          placeholder="HH:mm:ss"
                        />
                      </Form.Item>
                    </>
                  )}

                  {frequency === 6 && (
                    <>
                      {numberDateYearly.map((item, idx) => {
                        return (
                          <>
                            <Form.Item
                              label={
                                idx === numberDateYearly.length - 1 ? (
                                  <>
                                    <span>{t("workflow.ChooseDay")}</span>
                                    <AddDate
                                      onClick={() => {
                                        if (!checkRule("edit")) return;
                                        _onAddDate();
                                      }}
                                    >
                                      <Tooltip title={t("workflow.addDate")}>
                                        <span>+</span>
                                      </Tooltip>
                                    </AddDate>
                                    {numberDateYearly.length > 1 && (
                                      <DeleteDate
                                        onClick={() => {
                                          if (!checkRule("edit")) return;
                                          _onDeleteDate(idx);
                                        }}
                                      >
                                        <Tooltip
                                          title={t("workflow.deleteDate")}
                                        >
                                          <span>x</span>
                                        </Tooltip>
                                      </DeleteDate>
                                    )}
                                  </>
                                ) : (
                                  <>
                                    <span>{t("workflow.ChooseDay")}</span>
                                    {numberDateYearly.length > 1 && (
                                      <DeleteDate
                                        onClick={() => {
                                          if (!checkRule("edit")) return;
                                          _onDeleteDate(idx);
                                        }}
                                      >
                                        <Tooltip
                                          title={t("workflow.deleteDate")}
                                        >
                                          <span>x</span>
                                        </Tooltip>
                                      </DeleteDate>
                                    )}
                                  </>
                                )
                              }
                              name={`dateYearly${idx}`}
                              rules={[
                                {
                                  required: true,
                                  message: t("common.placeholderSelect"),
                                },
                              ]}
                            >
                              <DatePicker
                                placeholder={t("common.placeholderSelect")}
                                onChange={(value, dateString) =>
                                  handleSelectDateYearly(idx, dateString)
                                }
                                disabled={!checkRule("edit")}
                                disabledDate={disabledDate}
                              />
                            </Form.Item>
                          </>
                        );
                      })}
                      <Form.Item
                        label={t("workflow.AtTime")}
                        name="timeYearly"
                        rules={[
                          {
                            required: true,
                            message: t("common.placeholderSelect"),
                          },
                        ]}
                      >
                        <TimePicker
                          disabled={!checkRule("edit")}
                          placeholder="HH:mm:ss"
                        />
                      </Form.Item>
                    </>
                  )}
                </Frequency>
              )}
            </Trigger>
          </WrapBasic>

          <WrapCondition disabled={!checkRule("edit")}>
            <Wrap>
              <legend>{t("workflow.workflowConditions")}</legend>
              <Conditions
                title={t("common.allCondition")}
                decs={`(${t("common.descAllCondition")})`}
                conditions={allCondition}
                setConditions={setAllCondition}
                ID={
                  Object.keys(details).length > 0 && details._id
                    ? details._id
                    : ""
                }
                dataDetails={
                  Object.keys(details).length > 0 && details ? details : {}
                }
                operatorValue={operatorValueAnd}
                setOperatorValue={setOperatorValueAnd}
                value={valueAnd}
                setValue={setValueAnd}
              />
              <Conditions
                title={t("common.anyCondition")}
                decs={`(${t("common.descAnyCondition")})`}
                conditions={anyCondition}
                setConditions={setAnyCondition}
                ID={
                  Object.keys(details).length > 0 && details._id
                    ? details._id
                    : ""
                }
                dataDetails={
                  Object.keys(details).length > 0 && details ? details : {}
                }
                operatorValue={operatorValueOr}
                setOperatorValue={setOperatorValueOr}
                value={valueOr}
                setValue={setValueOr}
              />
            </Wrap>
          </WrapCondition>
        </WrapInfo>
      </Form>

      <Actions
        onSave={_onSubmit}
        onCancel={_onCancel}
        objectID={details.object_id}
        userFields={userFields}
        fields_related={fields_related}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        allActions={listActions}
        status={status}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listMainFields={listMainFields}
        details={details}
        trigger={trigger}
      />
    </Wrapper>
  );
}

export default withTranslation()(EditWorkflow);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-form-item-label > label {
    font-size: 16px;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapInfo = styled.div`
  background: #fff;
  margin-top: 16px;
  padding: 24px;
  display: flex;
`;

const WrapBasic = styled.div`
  width: 50%;
  margin-right: 16px;
  height: 335px;
  overflow-y: auto;
`;

const BasicInfo = styled.fieldset`
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 10px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const Trigger = styled(BasicInfo)`
  margin-bottom: 0;
`;

const Wrap = styled(BasicInfo)`
  margin-bottom: 0;
`;

const WrapCondition = styled(WrapBasic)`
  margin-right: 0;
  pointer-events: ${({ disabled }) => disabled && "none"};
`;

const Frequency = styled(BasicInfo)`
  .ant-picker {
    width: 100%;
  }
`;

const AddDate = styled.div`
  margin-left: 8px;
  width: 20px;
  height: 20px;
  background: #fff;
  border: 1px solid ${(props) => props.theme.main};
  color: ${(props) => props.theme.main};
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    padding-bottom: 4px;
  }

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
    cursor: pointer;
  }
`;

const DeleteDate = styled(AddDate)``;
