import React, { useState, useEffect } from "react";
import styled from "styled-components";
import isEmail from "validator/lib/isEmail";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
  loadFieldsObjectSuccess,
} from "redux/slices/workflows";
import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

function DeletePermission({
  visible,
  $visible,
  isEdit,
  setIsEdit,
  objectID,
  listAllGroups,
  listAllUser,
  loading,
  detailsAction,
  status,
  isDynamicButton,
  details,
  objects,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const [form] = Form.useForm();
  const { listFieldsMain } = useSelector((state) => state.workflowsReducer);
  const { listAccountMapping } = useSelector(
    (state) => state.googleIntegrationReducer
  );
  const [fieldsMain, setFieldsMain] = useState([]);
  const [listTo, setListTo] = useState([]);
  const [listCheckValid, setListCheckValid] = useState([]);

  const _onSubmit = (values) => {
    let data = {
      action_name: _.get(values, "action_name"),
      description: _.get(values, "description"),
      action_type: 18,
      object_id: objectID,
      oauth2_id: _.get(values, "oauth2_id"),
      field_url: _.get(values, "field_url"),
      deleted_users: _.get(values, "deleted_users"),
    };

    if (isEdit) {
      if (isDynamicButton) {
        dispatch(
          updateActionDynamicButton({
            ...data,
            button_id: recordID,
            _id: detailsAction._id,
          })
        );
      } else {
        dispatch(
          updateAction({
            ...data,
            workflow_id: recordID,
            _id: detailsAction._id,
          })
        );
      }
    } else {
      if (isDynamicButton) {
        dispatch(
          createActionDynamicButton({
            ...data,
            button_id: recordID,
          })
        );
      } else {
        dispatch(
          createAction({
            ...data,
            workflow_id: recordID,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    $visible(false);
    form.resetFields();
    setIsEdit(false);
    dispatch(loadFieldsObjectSuccess([]));
    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  useEffect(() => {
    if (visible && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        action_name: _.get(detailsAction, "action_name"),
        description: _.get(detailsAction, "description"),
        oauth2_id: _.get(detailsAction, "oauth2_id"),
        field_url: _.get(detailsAction, "field_url"),
        deleted_users: _.get(detailsAction, "deleted_users", []),
      });
    }
  }, [dispatch, visible, form, isEdit, detailsAction]);

  useEffect(() => {
    const tempListFieldMain = [];
    listFieldsMain.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                field.type === "file" &&
                !_.get(field, "encrypted", false)
              ) {
                tempListFieldMain.push({
                  label: field.related_name,
                  value: field.ID,
                });
              }
            });
          });
        }
      }
    });
    setFieldsMain(tempListFieldMain);
  }, [listFieldsMain]);

  useEffect(() => {
    let tempOptionsTo = [];
    let tempListCheckValid = [];

    let tempGroup = [];
    let tempUser = [];

    listAllGroups.forEach((item) => {
      tempGroup.push({
        label: item.name,
        value: item._id,
      });

      tempListCheckValid.push(item._id);
    });

    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item.Email,
        });

        tempListCheckValid.push(item.Email);
      }
    });

    if (tempUser.length > 0) {
      tempOptionsTo.push({
        label: "Users",
        options: tempUser,
      });
    }

    if (tempGroup.length > 0) {
      tempOptionsTo.push({
        label: "Groups",
        options: tempGroup,
      });
    }

    setListCheckValid(tempListCheckValid);
    setListTo(tempOptionsTo);
  }, [listAllGroups, listAllUser]);

  useEffect(() => {
    if (
      visible &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      $visible(false);
      form.resetFields();
      setIsEdit(false);
      dispatch(loadFieldsObjectSuccess([]));
      if (isDynamicButton) {
        dispatch(loadDetailsActionDynamicButtonSuccess({}));
      } else {
        dispatch(loadDetailsActionFail());
      }
    }
  }, [$visible, dispatch, form, isDynamicButton, setIsEdit, status, visible]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={visible}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action || status === "LoadingAction" ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="action_name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item
            label="Oauth2"
            name="oauth2_id"
            rules={[
              {
                required: true,
                message: t("common.placeholderSelect"),
              },
            ]}
          >
            <Select placeholder={t("common.placeholderSelect")}>
              {listAccountMapping.map((item, idx) => (
                <Select.Option
                  key={_.get(item, "_id", idx)}
                  value={_.get(item, "_id", idx)}
                >
                  {_.get(item, "email", "")}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>

          <Form.Item
            label={t("user.deleteUser")}
            name="deleted_users"
            rules={[
              { required: true, message: t("common.placeholderInputSelect") },
              () => ({
                validator(_, values) {
                  if (values) {
                    let tempNotIncludes = [];
                    values.forEach((item) => {
                      if (!listCheckValid.includes(item)) {
                        tempNotIncludes.push(item);
                      }
                    });

                    if (tempNotIncludes.length > 0) {
                      let invalid = tempNotIncludes.filter(
                        (item) => isEmail(item) === false
                      );

                      if (invalid.length > 0) {
                        return Promise.reject(
                          new Error(t("workflow.invalidEmail"))
                        );
                      }
                    }
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listTo}
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.fieldURL")}
            name="field_url"
            rules={[
              {
                required: true,
                message: t("common.placeholderSelect"),
              },
            ]}
          >
            <Select
              placeholder={t("common.placeholderSelect")}
              options={fieldsMain}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={isDynamicButton ? status === "Loading" : loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(DeletePermission);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
