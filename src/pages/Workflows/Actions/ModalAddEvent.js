import React, { useEffect, useState, useRef } from "react";
import styled from "styled-components";
import _ from "lodash";
import {
  Modal,
  Button,
  Dropdown,
  Radio,
  Select,
  InputNumber,
  Checkbox,
  Input,
} from "antd";

import Editor from "components/Editor/Editor2";

import moment from "moment";
import { BE_URL } from "constants/constants";
import { useSelector } from "redux/store";

import { useTranslation } from "react-i18next";

import PrivateIcon from "assets/icons/common/privateIcon.svg";
import PublicIcon from "assets/icons/common/publicIcon.svg";
import DefaultAvatarGroup from "assets/icons/common/avtGroupDefault.png";
import DefaultAvatarAgent from "assets/icons/common/avtAgentDefault.png";
import DeleteImg from "assets/icons/common/deleteImg.png";
import CloseIcon from "assets/icons/common/close-circle.png";
import { useParams } from "react-router";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
} from "redux/slices/workflows";

import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

import { generalInfo, metaData } from "util/staticData";
import { useDispatch } from "react-redux";

const { Option, OptGroup } = Select;

const listOptionUnit = [
  {
    label: "Minutes",
    value: "minutes",
  },
  {
    label: "Hours",
    value: "hours",
  },
  {
    label: "Days",
    value: "days",
  },
  {
    label: "Weeks",
    value: "weeks",
  },
  {
    label: "Months",
    value: "months",
  },
];

const listColor = [
  {
    backgroundColor: "#D2ECEC",
    borderColor: "#20A2A2",
  },
  {
    backgroundColor: "#D1E9FF",
    borderColor: "#1890FF",
  },
  {
    backgroundColor: "#DDF7D5",
    borderColor: "#54D62C",
  },
  {
    backgroundColor: " #FFF3CD",
    borderColor: "#FFC107",
  },
  {
    backgroundColor: "#FFDAD9",
    borderColor: "#FF4842",
  },

  {
    backgroundColor: "#E9DFFD",
    borderColor: "#9161F7",
  },
  {
    backgroundColor: "#FBDAEB",
    borderColor: "#EC4899",
  },
];

const ModalAddEvent = ({
  showModalAddEvent,
  setShowModalAddEvent,
  listAllUser,
  listAllGroups,
  isEdit,
  setIsEdit,
  objectID,
  isDynamicButton,
  details,
  detailsAction,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const editorJodit = useRef(null);

  const { recordID } = useParams();

  const dateFormatSecond = "YYYY-MM-DD HH:mm:ss";

  const { userDetail } = useSelector((state) => state.userReducer);

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [content, setContent] = useState({
    start_time: moment(new Date()).format(dateFormatSecond),
    end_time: moment(new Date()).format(dateFormatSecond),
    event_name: "",
    owner: userDetail._id,
    permission: "private",
    description: "",
    users_sharing: [],
    sharing_fields: ["_id", "owner", "start_time", "end_time", "color"],
    alerts: [
      {
        send_mail: false,
        quantity: 10,
        unit: "minutes",
      },
    ],
    guests: [],
    repeat_event: {},
    color: "#D2ECEC",
    groups_sharing: [],
  });

  const [listFields, setListFields] = useState([]);

  const [valueSearchGuest, setValueSearchGuest] = useState("");

  const [listIdGuest, setListIdGuest] = useState([]); // id guest

  const [lstGuest, setLstGuest] = useState([]); // list select guest

  const [listRender, setListRender] = useState([]); // listRenderGuest

  const [valueShareTo, setValueShareTo] = useState([]); // value group and agent

  const [valueSreachShare, setValueSearchShare] = useState("");

  const [lstShareTo, setLstShareTo] = useState([]); // list select share to

  const [agent, setAgent] = useState([]); //share agent

  const [group, setGroup] = useState([]); // share group

  const [listGuest, setListGuest] = useState([]);

  const [listUserActive, setListUserActive] = useState([]);

  const [disabled, setDisabled] = useState(false);

  const [listShareTo, setListShareTo] = useState([]); // group and agent

  const [optionsAppend, setOptionsAppend] = useState([]);

  const [selectedFields, setSelectedFields] = useState([]);
  const [valueField, setValueField] = useState([]);

  const [selectedGeneralFields, setSelectedGeneralFields] = useState([]);
  const [valueGeneralField, setValueGeneralField] = useState([]);

  const [schedulerType, setSchedulerType] = useState("default");

  const [listPermission, setListPermission] = useState([]);
  const [name, setName] = useState("");
  const [eventName, setEventName] = useState([]);
  const [startTrigger, setStartTrigger] = useState({
    hours: 0,
    minutes: 0,
    days: 0,
  });
  const [endTrigger, setEndTrigger] = useState({
    hours: 0,
    minutes: 10,
    days: 0,
  });
  useEffect(() => {
    if (showModalAddEvent && Object.keys(detailsAction).length > 0) {
      setName(detailsAction.action_name);
      setContent(detailsAction.calendar_event_template);
      setSchedulerType(detailsAction.scheduler_type);
      setStartTrigger(detailsAction.start_when_triggered);
      setEndTrigger(detailsAction.end_when_triggered);
      setValueField(detailsAction.selected_fields);
      setValueGeneralField(detailsAction.selected_general_fields);

      const shareDefault = ["_id", "owner", "start_time", "end_time", "color"];

      let tem = [];

      detailsAction.calendar_event_template.sharing_fields.forEach((field) => {
        if (!shareDefault.includes(field)) {
          tem.push(field);
        }
      });
      setListPermission(tem);
      const { users_sharing, groups_sharing, guests } =
        detailsAction.calendar_event_template;
      let arr = [];
      users_sharing.forEach((item) => {
        arr.push(item);
      });

      groups_sharing.forEach((item) => {
        arr.push(item);
      });

      setValueShareTo(arr);
      setListIdGuest(guests);

      let tempOptionsAppend = [];

      detailsAction.selected_fields.forEach((item) => {
        const temp = listFields.find((field) => field.value === item);
        if (temp) {
          tempOptionsAppend.push(temp);
        }
      });

      detailsAction.selected_general_fields.forEach((item) => {
        const temp = generalInfo.find((field) => field.value === item);
        if (temp) {
          tempOptionsAppend.push(temp);
        }
      });

      setOptionsAppend(tempOptionsAppend);
      setEventName(formatData(detailsAction.event_name_arr, [], "parse"));
    }
  }, [detailsAction, listFields, showModalAddEvent]);

  useEffect(() => {
    if (!showModalAddEvent) {
      setIsEdit(false);
      setValueField([]);
      setValueGeneralField([]);
      if (isDynamicButton) {
        dispatch(loadDetailsActionDynamicButtonSuccess({}));
      } else {
        dispatch(loadDetailsActionFail());
      }

      setContent({
        start_time: moment(new Date()).format(dateFormatSecond),
        end_time: moment(new Date()).format(dateFormatSecond),
        event_name: "",
        owner: userDetail._id,
        permission: "private",
        description: "",
        users_sharing: [],
        sharing_fields: ["_id", "owner", "start_time", "end_time", "color"],
        alerts: [
          {
            send_mail: false,
            quantity: 10,
            unit: "minutes",
          },
        ],
        guests: [],
        repeat_event: {},
        color: "#D2ECEC",
        groups_sharing: [],
      });
      setListPermission([]);
      setSchedulerType("default");
      setValueShareTo([]);
      setListIdGuest([]);
      setName("");
      setEventName([]);
    }
    //eslint-disable-next-line
  }, [showModalAddEvent]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            if (ele.section_name !== "Meta fields") {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false &&
                  !_.get(field, "encrypted", false)
                ) {
                  tempOptionsFields.push({
                    label: field.related_name,
                    value: field.full_field_id,
                    type: field.type,
                  });
                }
              });
            }
          });
        } else {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    const temp = listAllUser.filter((user) => user.Active);
    setListUserActive(temp);
  }, [listAllUser]);

  useEffect(() => {
    if (listUserActive) {
      setListGuest(listUserActive);
    }
  }, [listUserActive]);

  useEffect(() => {
    setContent((prev) => ({
      ...prev,
      owner: userDetail._id,
    }));
  }, [userDetail]);

  useEffect(() => {
    const temp = content.guests.map((guest) => {
      return guest.user_id;
    });
    setListIdGuest(temp);

    if (content.event_name.length === 0) {
      // setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [content]);

  useEffect(() => {
    const filterList = listGuest.filter(
      (item) => !listIdGuest.includes(item._id)
    );

    const filterRender = listGuest.filter((item) =>
      listIdGuest.includes(item._id)
    );

    let tempFilter = [...filterRender];

    filterRender.forEach((item, idx) => {
      content.guests.forEach((value) => {
        if (item._id === value.user_id) {
          tempFilter[idx] = {
            ...tempFilter[idx],
            stt: value.status,
          };
        }
      });
    });

    setLstGuest(filterList);
    setListRender(tempFilter);
  }, [listIdGuest, listGuest, content]);

  useEffect(() => {
    const filterShare = listShareTo.map((item) => {
      return {
        label: item.label,
        options: item.options.filter(
          (value) => !valueShareTo.includes(value._id)
        ),
      };
    });
    setLstShareTo(filterShare);
  }, [listShareTo, valueShareTo]);

  useEffect(() => {
    const listShare = [];
    if (listUserActive.length > 0) {
      listShare.push({
        label: "Agent",
        options: listUserActive,
      });
    }
    if (listAllGroups.length > 0) {
      listShare.push({
        label: "Group",
        options: listAllGroups,
      });
    }
    setListShareTo(listShare);
  }, [listAllGroups, listUserActive, userDetail]);

  useEffect(() => {
    const filterAgent = listUserActive.filter((user) =>
      valueShareTo.includes(user._id)
    );

    const filterGroup = listAllGroups.filter((group) =>
      valueShareTo.includes(group._id)
    );

    setAgent(filterAgent);
    setGroup(filterGroup);
  }, [valueShareTo, listAllGroups, listUserActive]);

  useEffect(() => {
    const shareUser = agent.map((item) => {
      return item._id;
    });

    const shareGroup = group.map((item) => {
      return item._id;
    });

    setContent((prev) => ({
      ...prev,
      users_sharing: shareUser,
      groups_sharing: shareGroup,
    }));
  }, [agent, group]);

  useEffect(() => {
    if (
      !name ||
      valueField.length === 0 ||
      valueGeneralField.length === 0 ||
      eventName.length === 0
    ) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [name, valueField, valueGeneralField, eventName]);

  useEffect(() => {
    if (schedulerType === "default") {
      let trigger = {
        hours: 0,
        minutes: 0,
        days: 0,
      };
      setStartTrigger(trigger);
      setEndTrigger(trigger);
    }
  }, [schedulerType]);

  const renderPopover = () => {
    return (
      <ContentPopover>
        <p
          onClick={() => {
            setContent((prev) => ({
              ...prev,
              permission: "private",
            }));
          }}
        >
          <img src={PrivateIcon} alt="Private" /> Private
        </p>
        <p
          onClick={() => {
            setContent((prev) => ({
              ...prev,
              permission: "public",
            }));
          }}
        >
          <img src={PublicIcon} alt="Public" /> Public
        </p>
      </ContentPopover>
    );
  };

  const handleOnchange = (key, value) => {
    setContent((prev) => ({
      ...prev,
      [key]: value,
    }));
  };

  const handleChangeGuest = (value) => {
    const temp = [];

    content.guests.forEach((item) => {
      if (value.includes(item.user_id)) {
        temp.push(item);
        value = value.filter((va) => va !== item.user_id);
      }
    });

    value.forEach((item) => {
      temp.push({
        user_id: item,
        status: "waiting",
      });
    });

    setContent((prev) => ({
      ...prev,
      guests: temp,
    }));
  };

  const handleDeleteShareTo = (id) => {
    const deleteShare = valueShareTo.filter((item) => item !== id);

    setValueShareTo(deleteShare);
  };

  const handleDeleteGuest = (guest) => {
    const deleteGuest = content.guests.filter((item) => item.user_id !== guest);
    setContent((prev) => ({
      ...prev,
      guests: deleteGuest,
    }));
  };

  const renderListGuest = (value, option) => {
    return (
      <ContentGuest>
        {value.map((guest) => {
          return (
            guest._id !== content.owner && (
              <div className="guestItem" key={guest._id}>
                {option === "user" ? (
                  <div className="guestImg">
                    {guest.avatar_config?.url !== "" &&
                    guest.avatar_config?.url ? (
                      <img
                        src={`${BE_URL}${guest.avatar_config?.url}`}
                        alt="Img"
                      />
                    ) : (
                      <img
                        src={
                          option === "agent" || option === "user"
                            ? DefaultAvatarAgent
                            : DefaultAvatarGroup
                        }
                        alt="img"
                      />
                    )}

                    <span>
                      <img
                        onClick={() => {
                          handleDeleteGuest(guest._id);
                        }}
                        src={CloseIcon}
                        alt="close"
                      />
                    </span>
                  </div>
                ) : (
                  <div className="guestImg">
                    {guest.avatar_config?.url !== "" &&
                    guest.avatar_config?.url ? (
                      <img
                        src={`${BE_URL}${guest.avatar_config?.url}`}
                        alt="Img"
                      />
                    ) : (
                      <img
                        src={
                          option === "agent" || option === "user"
                            ? DefaultAvatarAgent
                            : DefaultAvatarGroup
                        }
                        alt="img"
                      />
                    )}

                    <span>
                      <img
                        onClick={() => {
                          handleDeleteShareTo(guest._id);
                        }}
                        src={CloseIcon}
                        alt="close"
                      />
                    </span>
                  </div>
                )}
                <div className="guestName">
                  {option === "agent" || option === "user"
                    ? guest.First_Name
                    : guest.name}
                </div>
              </div>
            )
          );
        })}
      </ContentGuest>
    );
  };

  const handleSelectField = (values, options, type) => {
    if (type === "fields") {
      setValueField(values);
      setSelectedFields(options);
      setOptionsAppend(selectedGeneralFields.concat(options));
    } else {
      setValueGeneralField(values);
      setSelectedGeneralFields(options);
      setOptionsAppend(selectedFields.concat(options));
    }
  };

  const handleChangeListNoti = (key, value, idx) => {
    let arr = [...content.alerts];

    content.alerts.forEach((item, index) => {
      if (index === idx) {
        switch (key) {
          case "mail":
            arr[index] = {
              ...arr[index],
              send_mail: value,
            };
            break;
          case "quantity":
            arr[index] = {
              ...arr[index],
              quantity: value,
            };
            break;
          case "unit":
            arr[index] = {
              ...arr[index],
              unit: value,
            };
            break;
          default:
            break;
        }
      }
    });

    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const handleDeleteNoti = (idx) => {
    let arr = [...content.alerts];
    if (arr.length === 1) return;
    arr.splice(idx, 1);
    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const addNoti = () => {
    let arr = [...content.alerts];
    arr.push({ unit: "minutes", quantity: 10, send_mail: false });
    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const formatData = (data, fields, type) => {
    let result = [];

    if (type === "submit" && data) {
      data.forEach((item) => {
        if (fields.find((field) => field.value === item)) {
          result.push("$" + item);
        } else {
          result.push(item);
        }
      });
    }
    if (type === "parse") {
      data.forEach((item) => {
        if (item.split("$").length > 1) {
          result.push(item.split("$")[1]);
        } else {
          result.push(item);
        }
      });
    }
    return result;
  };

  const handleChangeTime = (option, key, value) => {
    if (option === "start") {
      setStartTrigger((prev) => ({
        ...prev,
        [key]: value,
      }));
    } else {
      setEndTrigger((prev) => ({
        ...prev,
        [key]: value,
      }));
    }
  };

  const handleEvent = () => {
    const shareDefault = ["_id", "owner", "start_time", "end_time", "color"];
    listPermission.forEach((field) => {
      shareDefault.push(field);
    });

    const nameArr = formatData(
      eventName,
      listFields.concat(metaData),
      "submit"
    );

    let nameEvent = "";
    nameArr.forEach((item) => {
      nameEvent += `${item} `;
    });

    const event = {
      ...content,
      event_name: nameEvent,
      sharing_fields: shareDefault,
      description: editorJodit.current.value,
    };

    let data = {
      action_name: name,
      action_type: 11,
      object_id: objectID,
      selected_fields: valueField,
      selected_general_fields: valueGeneralField,
      calendar_event_template: event,
      scheduler_type: schedulerType,
      start_when_triggered: startTrigger,
      end_when_triggered: endTrigger,
      event_name_arr: nameArr,
    };

    if (isEdit) {
      if (isDynamicButton) {
        dispatch(
          updateActionDynamicButton({
            data: {
              ...data,
              button_id: recordID,
            },
            action_type: 11,
            _id: detailsAction._id,
          })
        );
      } else {
        dispatch(
          updateAction({
            data: {
              ...data,
              workflow_id: recordID,
            },
            action_type: 11,
            _id: detailsAction._id,
          })
        );
      }
    } else {
      if (isDynamicButton) {
        dispatch(
          createActionDynamicButton({
            ...data,
            button_id: recordID,
          })
        );
      } else {
        dispatch(
          createAction({
            ...data,
            workflow_id: recordID,
          })
        );
      }
    }
  };

  return (
    <>
      <CustomModal
        title="Add event"
        visible={showModalAddEvent}
        onCancel={() => setShowModalAddEvent(false)}
        width={1184}
        footer={[
          <ButtonGroup key="button">
            <div className="btnGroup">
              <>
                <Button
                  key="save"
                  onClick={() => {
                    setShowModalAddEvent(false);
                    handleEvent();
                  }}
                  disabled={
                    disabled ||
                    details.status ||
                    (isEdit && detailsAction.status)
                  }
                >
                  Save
                </Button>
                <Button
                  key="save"
                  onClick={() => {
                    setShowModalAddEvent(false);
                    editorJodit.current.value = "";
                    // handleEvent();
                  }}
                >
                  Cancel
                </Button>
              </>
            </div>
          </ButtonGroup>,
        ]}
      >
        <ContentWrap>
          <CustomInput
            value={name}
            placeholder="Action name"
            onChange={(e) => {
              setName(e.target.value);
            }}
          />
          <div className="groupInput">
            <CustomTitle
              // value={"helloooo how are you"}
              value={eventName}
              placeholder="Add title"
              mode="tags"
              options={listFields.concat(metaData)}
              onChange={(value) => {
                setEventName(value);
              }}
            />
            <Dropdown placement="bottom" overlay={renderPopover()}>
              <div className="popoverContent">
                <Button>
                  <img
                    src={
                      content.permission === "private"
                        ? PrivateIcon
                        : PublicIcon
                    }
                    alt="icon"
                  />
                  <span> {content.permission}</span>
                </Button>
              </div>
            </Dropdown>
          </div>

          <Radio.Group
            onChange={(e) => handleOnchange("color", e.target.value)}
            value={content.color}
            className="radioGroup"
          >
            {listColor.map((item, index) => {
              return (
                <CustomRadio
                  key={index}
                  color={item.borderColor}
                  value={item.backgroundColor}
                  borderColor={item.borderColor}
                ></CustomRadio>
              );
            })}
          </Radio.Group>

          <SelectGuest>
            {!valueSearchGuest ? (
              <span>Add guest</span>
            ) : (
              <span className="addGuest">Add guest</span>
            )}
            <CustomSelect
              onSelect={() => setValueSearchGuest("")}
              mode="multiple"
              optionFilterProp="children"
              value={listIdGuest}
              onSearch={(value) => {
                setValueSearchGuest(value);
              }}
              onChange={(value) => {
                handleChangeGuest(value);
              }}
            >
              {lstGuest.map((item) => {
                return (
                  item._id !== userDetail._id && (
                    <Option key={item._id} value={item._id}>
                      {item.Full_Name}
                    </Option>
                  )
                );
              })}
            </CustomSelect>
          </SelectGuest>

          {renderListGuest(listRender, "user")}

          {content.permission === "private" && (
            <>
              <SelectGuest>
                {!valueSreachShare ? (
                  <span>Share to</span>
                ) : (
                  <span className="addGuest">Share to</span>
                )}
                <CustomSelect
                  onSelect={() => setValueSearchShare("")}
                  mode="multiple"
                  optionFilterProp="children"
                  value={valueShareTo}
                  onSearch={(value) => {
                    setValueSearchShare(value);
                  }}
                  onChange={(value) => {
                    setValueShareTo(value);
                  }}
                >
                  {lstShareTo.map((item, index) => {
                    return (
                      <OptGroup label={item.label} key={index}>
                        {item.options.map((option) => {
                          return (
                            option._id !== userDetail._id && (
                              <Option key={option._id} value={option._id}>
                                {item.label === "Agent"
                                  ? option.Full_Name
                                  : option.name}
                              </Option>
                            )
                          );
                        })}
                      </OptGroup>
                    );
                  })}
                </CustomSelect>
              </SelectGuest>

              {renderListGuest(agent, "agent")}
              {renderListGuest(group, "group")}
            </>
          )}

          {/* <label>Type</label>
          <CustomType
            value={schedulerType}
            onChange={(value) => {
              setSchedulerType(value);
            }}
          >
            <Option key={"default"} value={"default"}>
              Default
            </Option>
            <Option key={"on_triggered"} value={"on_triggered"}>
              Custom
            </Option>
          </CustomType> */}
          <DelayWrap>
            <Checkbox
              checked={schedulerType === "on_triggered"}
              onChange={(value) => {
                if (value.target.checked) {
                  setSchedulerType("on_triggered");
                } else {
                  setSchedulerType("default");
                }
              }}
            >
              Delay
            </Checkbox>
          </DelayWrap>

          {schedulerType === "on_triggered" && (
            <DatetimeWrap>
              <DateItem>
                <label>Delay start time</label>
                <TimeWrap>
                  <Temdetail>
                    <span>Day</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("start", "days", value);
                      }}
                      value={startTrigger.days}
                    />
                  </Temdetail>
                  <Temdetail>
                    <span>Hours</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("start", "hours", value);
                      }}
                      value={startTrigger.hours}
                    />
                  </Temdetail>
                  <Temdetail>
                    <span>Minutes</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("start", "minutes", value);
                      }}
                      value={startTrigger.minutes}
                    />
                  </Temdetail>
                </TimeWrap>
              </DateItem>
              <CustomLine></CustomLine>
              <DateItem>
                <label>Delay end time</label>

                <TimeWrap>
                  <Temdetail>
                    <span>Day</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("end", "days", value);
                      }}
                      value={endTrigger.days}
                    />
                  </Temdetail>
                  <Temdetail>
                    <span>Hours</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("end", "hours", value);
                      }}
                      value={endTrigger.hours}
                    />
                  </Temdetail>
                  <Temdetail>
                    <span>Minutes</span>
                    <InputNumber
                      min={0}
                      onChange={(value) => {
                        handleChangeTime("end", "minutes", value);
                      }}
                      value={endTrigger.minutes}
                    />
                  </Temdetail>
                </TimeWrap>
              </DateItem>
            </DatetimeWrap>
          )}
          <SharePermisson>
            <span>Share permission</span>

            <div className="selectExcept">
              <Checkbox.Group
                value={listPermission}
                onChange={(value) => {
                  setListPermission(value);
                }}
              >
                <Checkbox value="event_name">Title</Checkbox>
                <Checkbox value="description">Description</Checkbox>
                <Checkbox value="guests">Guest list</Checkbox>
              </Checkbox.Group>
            </div>
          </SharePermisson>
        </ContentWrap>
        <CustomLine></CustomLine>
        <ContentWrap>
          <label>Notices</label>
          <CustomNotiWrap>
            {content.alerts.map((noti, index) => {
              return (
                <CustomNotiGroup key={index}>
                  <CustomSelectNoti
                    value={noti.send_mail}
                    onChange={(value) =>
                      handleChangeListNoti("mail", value, index)
                    }
                  >
                    <Option value={false}>Notification</Option>
                    <Option value={true}>Email</Option>
                  </CustomSelectNoti>

                  <CustomInputNumber
                    onChange={(value) =>
                      handleChangeListNoti("quantity", value, index)
                    }
                    value={noti.quantity}
                  />

                  <CustomSelectNoti
                    onChange={(value) =>
                      handleChangeListNoti("unit", value, index)
                    }
                    value={noti.unit}
                  >
                    {listOptionUnit.map((unit, index) => {
                      return (
                        <Option key={index} value={unit.value}>
                          {unit.label}
                        </Option>
                      );
                    })}
                  </CustomSelectNoti>
                  {content.alerts.length > 1 && (
                    <img
                      onClick={() => handleDeleteNoti(index)}
                      src={DeleteImg}
                      alt="delete"
                    />
                  )}
                </CustomNotiGroup>
              );
            })}
            <CustomAddNoti>
              <span>+</span> <p onClick={addNoti}>Add Notification</p>
            </CustomAddNoti>
          </CustomNotiWrap>
          <label>
            <span>* </span> Field
          </label>
          <CustomType
            placeholder={t("workflow.selectField")}
            options={listFields}
            mode="multiple"
            value={valueField}
            onChange={(value, options) => {
              handleSelectField(value, options, "fields");
            }}
            optionFilterProp="label"
          />
          <label>
            <span>* </span>General field
          </label>
          <CustomType
            placeholder={t("workflow.selectField")}
            options={generalInfo}
            mode="multiple"
            value={valueGeneralField}
            onChange={(value, options) => {
              handleSelectField(value, options, "general_fields");
            }}
            optionFilterProp="label"
          />
          <label>Description</label>

          <Editor
            editorJodit={editorJodit}
            content={content.description}
            optionsAppend={optionsAppend}
            minHeightInput="200px"
          />
        </ContentWrap>
      </CustomModal>
    </>
  );
};

export default ModalAddEvent;

const CustomLine = styled.div`
  width: 1px;
  background: #ececec;
`;

const CustomModal = styled(Modal)`
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
  }

  .ant-modal-body {
    display: flex;
    justify-content: space-between;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
    line-height: normal;
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-footer {
    box-shadow: ${(props) =>
      props.myPostiton !== "share"
        ? "0px -2px 17px rgba(0, 0, 0, 0.16)"
        : "none"};
    border-top: none;
    padding: 18px 24px;

    display: ${(props) => (props.myPostiton !== "share" ? "block" : "none")};
  }

  label {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
    span {
      color: red;
    }
  }

  .radioGroup {
    width: 100%;
    margin-bottom: 10px;
  }

  .popoverContent {
    margin-top: 5px;

    button {
      border-radius: 2px;
      transition: all 0.5s;
      width: 98px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;

      span::first-letter {
        text-transform: capitalize;
      }

      img {
        width: 14px;
        height: 14px;
        margin-right: 6px;
      }

      &:hover,
      &:focus {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
        img {
          filter: brightness(200);
        }
      }
    }
  }

  .groupInput {
    display: flex;
  }

  .ant-picker-focused {
    box-shadow: none;
    border: none;
  }
`;

const ContentWrap = styled.div`
  width: 49%;
`;

const ButtonGroup = styled.div`
  display: flex;
  justify-content: end;

  button {
    border-radius: 2px;
    width: 114px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:disabled {
        color: rgba(0, 0, 0, 0.25) !important;
        border-color: #d9d9d9 !important;
        background: #f5f5f5 !important;
      }

      &:first-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;

const ContentPopover = styled.div`
  background-color: white;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  p {
    cursor: pointer;
    margin: 0;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #2c2c2c;
    display: flex;
    align-items: center;
    transition: all 0.5s;
    padding: 5px 20px;
    border-radius: 2px;

    img {
      width: 14px;
      margin-right: 6px;
    }
    :hover {
      background: #f5f5f5;
    }
  }
`;

const CustomRadio = styled(Radio)`
  .ant-radio-inner {
    background-color: ${(props) => props.color};
    border: 1.3px solid ${(props) => props.borderColor};
    width: 24px;
    height: 24px;
    transition: all 1s;
  }

  .ant-radio-wrapper {
    margin-right: 16px;
  }

  .ant-radio {
    width: 24px;
  }

  .ant-radio-inner::after {
    background-color: white;
  }

  .ant-radio-checked .ant-radio-inner {
    border: 1.3px solid ${(props) => props.borderColor};
  }
`;

const SelectGuest = styled.div`
  background: #ececec;
  border-radius: 5px;
  padding: 5px 16px;
  cursor: pointer;
  position: relative;
  margin-bottom: 12px;

  span {
    color: #6b6b6b;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
  }
  .addGuest {
    color: transparent;
  }
`;

const CustomSelect = styled(Select)`
  width: 100%;
  border-radius: 2px;
  position: absolute;
  top: 0;
  left: 0;

  .ant-select-selector {
    cursor: pointer !important;
    border: none !important;
    border-radius: 5px !important;
    background: transparent !important;
    box-shadow: none !important;
  }

  .ant-select-selection-item {
    display: none;
  }

  .ant-select-selection-overflow {
    padding: 0 5px !important;
  }
`;

const ContentGuest = styled.div`
  display: flex;
  flex-wrap: wrap;

  .guestItem {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-right: 14px;
    margin-bottom: 10px;

    .guestImg {
      position: relative;

      img {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        cursor: pointer;
        object-fit: cover;
      }
      span {
        transition: all 1s;
        display: none;
        img {
          width: unset;
          height: unset;
          position: absolute;
          top: -6px;
          right: -6px;
          cursor: pointer;
        }
      }
      &:hover {
        span {
          display: unset;
        }
      }
    }

    .guestName {
      font-family: var(--roboto-400);
      font-size: 12px;
      color: rgba(0, 0, 0, 0.45);
    }
  }
`;

const CustomTitle = styled(Select)`
  width: 100%;
  .ant-select-selector {
    font-size: 18px;
    margin-bottom: 16px;
    margin-right: 16px;
    border-top: none !important;
    border-left: none !important;
    border-right: none !important;

    &:placeholder-shown {
      text-overflow: ellipsis;
      color: #6b6b6b;
    }
  }
`;

const DatetimeWrap = styled.div`
  margin-top: 8px;
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const DateItem = styled.div`
  width: 48%;
`;

const CustomType = styled(Select)`
  width: 100%;
  margin: 8px 0;
`;

const TimeWrap = styled.div`
  width: 100%;
  justify-content: space-between;
  display: flex;
`;
const Temdetail = styled.div`
  display: flex;
  width: 30%;
  flex-direction: column;
  .ant-input-number {
    margin-top: 8px;
    width: 100%;
  }
  span {
    font-size: 16px;
  }
`;
const SharePermisson = styled.div`
  margin-top: 10px;
  span {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
  }

  .selectExcept {
    margin-top: 6px;
    .ant-checkbox-group {
      display: flex;
    }
    .ant-checkbox-wrapper {
      display: flex;
      margin-bottom: 4px;
      margin-right: 16px;

      span {
        font-family: var(--roboto-400);
        font-size: 16px;
        color: #2c2c2c;
      }
    }
    .ant-checkbox-wrapper + .ant-checkbox-wrapper {
      margin-left: 0px;
    }
    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
    .ant-checkbox:hover .ant-checkbox-inner,
    .ant-checkbox:focus .ant-checkbox-inner,
    .ant-checkbox-wrapper:hover .ant-checkbox-inner,
    .ant-checkbox-input:focus + .ant-checkbox-inner,
    .ant-checkbox-checked::after {
      border-color: ${(props) => props.theme.main} !important;
    }
  }

  margin-bottom: 10px;
`;

const CustomNotiWrap = styled.div`
  width: 100%;
  margin-top: 6px;
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input-number:hover {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const CustomNotiGroup = styled.div`
  width: 100%;
  margin-bottom: 8px;

  img:hover {
    cursor: pointer;
  }
`;

const CustomInputNumber = styled(InputNumber)`
  width: 90px;
  margin-right: 16px;
  border-radius: 2px;
`;

const CustomSelectNoti = styled(Select)`
  width: 135px;
  margin-right: 16px;
  border-radius: 2px;
`;

const CustomAddNoti = styled.div`
  display: flex;
  align-items: center;
  p {
    border: none;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: ${(props) => props.theme.darker};
    padding: 0;
    margin: 0;
    &:hover {
      cursor: pointer;
      text-decoration: underline;
    }
  }
  span {
    margin-right: 4px;
    border: none;
    padding: 0;
    font-family: var(--roboto-400);
    font-size: 20px;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomInput = styled(Input)`
  font-size: 18px;
  margin-bottom: 16px;
  margin-right: 16px;
  border-top: none;
  border-left: none;
  border-right: none;

  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none !important;
    outline: none !important;
  }

  &:hover {
    border-color: ${(props) => props.theme.main};
  }

  &:placeholder-shown {
    text-overflow: ellipsis;
    font-family: var(--roboto-400);
    color: #6b6b6b;
    font-size: 18px;
  }
`;

const DelayWrap = styled.div`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }

  label {
    span {
      font-family: var(--roboto-400);
      color: #2c2c2c;
    }
  }
`;
