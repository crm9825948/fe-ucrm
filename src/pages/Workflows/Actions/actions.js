import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Switch from "antd/lib/switch";
import Spin from "antd/lib/spin";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  loadDetailsAction,
  activeAction,
  deleteActionWorkflow,
} from "redux/slices/workflows";

import {
  activeActionDynamicButton,
  deleteActionDynamicButton,
  loadDetailsActionDynamicButton,
} from "redux/slices/dynamicButton";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalSendNoti from "./ModalSendNotification";
import ModalSendSMS from "./ModalSendSMS";
import ModalSendEmail from "./ModalSendEmail";
import ModalCreateRecord from "./ModalCreateRecord";
import ModalUpdateRecord from "./ModalUpdateRecord";
import ModalCallAPI from "./ModalCallAPI";
import ModalUpdateRelatedRecordTargetSource from "./ModalUpdateRelatedRecordTargetSource";
import ModalUpdateRelatedRecordSourceTarget from "./ModalUpdateRelatedRecordSourceTarget";
import ModalAddEvent from "./ModalAddEvent";
import CreateFile from "./CreateFile";
import UpdatePermission from "./UpdatePermission";
import DeletePermission from "./DeletePermission";

function Actions({
  onSave,
  onCancel,
  objectID,
  userFields,
  fields_related,
  listAllGroups,
  listAllUser,
  isDynamicButton,
  allActions,
  isEdit,
  setIsEdit,
  status,
  listMainFields,
  details,
  trigger,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { Column } = Table;
  const { Text } = Typography;
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { loading, detailsAction, linkingFieldEmail } = useSelector(
    (state) => state.workflowsReducer
  );

  const { detailsActionDynamicButton } = useSelector(
    (state) => state.dynamicButtonReducer
  );
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [actionTable, setActionTable] = useState([]);

  const [showModalNoti, setShowModalNoti] = useState(false);
  const [showModalSMS, setShowModalSMS] = useState(false);
  const [showModalEmail, setShowModalEmail] = useState(false);
  const [showModalCreate, setShowModalCreate] = useState(false);
  const [showModalUpdate, setShowModalUpdate] = useState(false);
  const [showModalCallAPI, setShowModalCallAPI] = useState(false);
  const [
    showModalUpdateRelatedTargetSource,
    $showModalUpdateRelatedTargetSource,
  ] = useState(false);
  const [showModalUpdateRelatedSourceTarget, $showUpdateRelatedSourceTarget] =
    useState(false);
  const [showModalAddEvent, setShowModalAddEvent] = useState(false);
  const [showCreateFile, $showCreateFile] = useState(false);
  const [showUpdatePermission, $showUpdatePermission] = useState(false);
  const [showDeletePermission, $showDeletePermission] = useState(false);

  const [disabled, setDisabled] = useState(true);

  const [dataDelete, setDataDelete] = useState({});

  const checkRule = (rule, domain) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === domain && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const optionsAction = [
    {
      label: t("workflow.sendEmail"),
      value: "send-email",
    },
    {
      label: t("workflow.createRecord"),
      value: "create-record",
    },
    {
      label: t("workflow.updateRecord"),
      value: "update-record",
    },
    {
      label: t("workflow.sendNoti"),
      value: "send-notification",
    },
    {
      label: t("workflow.sendSMS"),
      value: "send-sms",
    },
    {
      label: t("workflow.callAPI"),
      value: "call-api",
    },
    {
      label: t("workflow.updateRelatedSourceRecord"),
      value: "update-related-record-target-source",
    },
    {
      label: t("workflow.updateRelatedTargetRecord"),
      value: "update-related-record-source-target",
    },
    {
      label: t("workflow.addEvent"),
      value: "add-event",
    },
    // {
    //   label: t("workflow.createFile"),
    //   value: "create-file",
    // },
    // {
    //   label: t("workflow.updatePermission"),
    //   value: "update-permission",
    // },
    // {
    //   label: t("workflow.deletePermission"),
    //   value: "delete-permission",
    // },
  ];

  const handleSelectAction = (value) => {
    switch (value) {
      case "send-notification":
        setShowModalNoti(true);
        break;
      case "send-sms":
        setShowModalSMS(true);
        break;
      case "send-email":
        setShowModalEmail(true);
        break;
      case "create-record":
        setShowModalCreate(true);
        break;
      case "update-record":
        setShowModalUpdate(true);
        break;
      case "call-api":
        setShowModalCallAPI(true);
        break;
      case "update-related-record-target-source":
        $showModalUpdateRelatedTargetSource(true);
        break;
      case "add-event":
        setShowModalAddEvent(true);
        break;
      case "update-related-record-source-target":
        $showUpdateRelatedSourceTarget(true);
        break;
      case "create-file":
        $showCreateFile(true);
        break;
      case "update-permission":
        $showUpdatePermission(true);
        break;
      case "delete-permission":
        $showDeletePermission(true);
        break;
      default:
        break;
    }
  };

  const handleActive = (checked, id) => {
    if (isDynamicButton) {
      dispatch(
        activeActionDynamicButton({
          status: checked,
          _id: id,
        })
      );
    } else {
      dispatch(
        activeAction({
          status: checked,
          _id: id,
        })
      );
    }
  };

  const _onEditAction = (id, type) => {
    setIsEdit(true);
    if (isDynamicButton) {
      dispatch(
        loadDetailsActionDynamicButton({
          action_id: id,
        })
      );
    } else {
      dispatch(
        loadDetailsAction({
          _id: id,
        })
      );
    }
    switch (type) {
      case t("workflow.sendNoti"):
        setShowModalNoti(true);
        break;
      case t("workflow.sendSMS"):
        setShowModalSMS(true);
        break;
      case t("workflow.sendEmail"):
        setShowModalEmail(true);
        break;
      case t("workflow.createRecord"):
        setShowModalCreate(true);
        break;
      case t("workflow.updateRecord"):
        setShowModalUpdate(true);
        break;
      case t("workflow.callAPI"):
        setShowModalCallAPI(true);
        break;
      case t("workflow.updateRelatedSourceRecord"):
        $showModalUpdateRelatedTargetSource(true);
        break;
      case t("workflow.updateRelatedTargetRecord"):
        $showUpdateRelatedSourceTarget(true);
        break;
      case t("workflow.addEvent"):
        setShowModalAddEvent(true);
        break;
      case t("workflow.createFile"):
        $showCreateFile(true);
        break;
      case t("workflow.updatePermission"):
        $showUpdatePermission(true);
        break;
      case t("workflow.deletePermission"):
        $showDeletePermission(true);
        break;
      default:
        break;
    }
  };

  const _onDeleteAction = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  useEffect(() => {
    let tempList = [];
    allActions.map((item) => {
      return tempList.push({
        key: item._id,
        active: item.status,
        type:
          item.action_type === 1
            ? t("workflow.sendEmail")
            : item.action_type === 2
            ? t("workflow.createRecord")
            : item.action_type === 3
            ? t("workflow.updateRecord")
            : item.action_type === 7
            ? t("workflow.sendNoti")
            : item.action_type === 8
            ? t("workflow.sendSMS")
            : item.action_type === 9
            ? t("workflow.callAPI")
            : item.action_type === 10
            ? t("workflow.updateRelatedSourceRecord")
            : item.action_type === 11
            ? t("workflow.addEvent")
            : item.action_type === 12
            ? t("workflow.updateRelatedTargetRecord")
            : item.action_type === 16
            ? t("workflow.createFile")
            : item.action_type === 17
            ? t("workflow.updatePermission")
            : item.action_type === 18
            ? t("workflow.deletePermission")
            : "",
        name: item.action_name,
        action: "",
      });
    });
    setActionTable(tempList);
  }, [allActions, t]);

  useEffect(() => {
    if (actionTable.length === 0) return;

    setTimeout(() => {
      setDisabled(false);
    }, 3000);
  }, [actionTable]);

  return (
    <Wrapper>
      <WrapTitle>
        <Title>
          {isDynamicButton
            ? t("dynamicButton.dynamicActions")
            : t("workflow.workflowActions")}
        </Title>
        <Select
          disabled={
            !checkRule(
              "create",
              isDynamicButton ? "dynamic_button" : "workflow"
            )
          }
          placeholder={t("workflow.addToDo")}
          options={optionsAction}
          onSelect={handleSelectAction}
        />
      </WrapTitle>

      <WrapTable>
        <Table pagination={false} dataSource={actionTable} scroll={{ x: 1500 }}>
          <Column
            title={t("workflow.active")}
            dataIndex="active"
            key="active"
            width="196px"
            render={(text, record) => (
              <Switch
                disabled={
                  !checkRule(
                    "edit",
                    isDynamicButton ? "dynamic_button" : "workflow"
                  ) || details.status
                }
                checkedChildren={t("workflow.on")}
                unCheckedChildren={t("workflow.off")}
                checked={text}
                onChange={(checked) => handleActive(checked, record.key)}
              />
            )}
          />
          <Column
            title={t("workflow.typeOfWork")}
            dataIndex="type"
            key="type"
            width="660px"
            sorter={(a, b) => a.type.localeCompare(b.type)}
            render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
          />
          <Column
            title={t("workflow.nameWork")}
            dataIndex="name"
            key="name"
            width="512px"
            sorter={(a, b) => a.name.localeCompare(b.name)}
            render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
          />
          {(checkRule(
            "edit",
            isDynamicButton ? "dynamic_button" : "workflow"
          ) ||
            checkRule(
              "delete",
              isDynamicButton ? "dynamic_button" : "workflow"
            )) && (
            <Column
              title={t("common.action")}
              dataIndex="action"
              key="action"
              fixed="right"
              width="200px"
              render={(text, record) => (
                <WrapAction>
                  {disabled ? (
                    <Spin />
                  ) : (
                    <>
                      {checkRule(
                        "edit",
                        isDynamicButton ? "dynamic_button" : "workflow"
                      ) && (
                        <Tooltip title={t("common.edit")}>
                          <img
                            onClick={() =>
                              _onEditAction(record.key, record.type)
                            }
                            src={Edit}
                            alt="edit"
                          />
                        </Tooltip>
                      )}
                    </>
                  )}
                  {checkRule(
                    "delete",
                    isDynamicButton ? "dynamic_button" : "workflow"
                  ) && (
                    <Tooltip title={t("common.delete")}>
                      <img
                        onClick={() => _onDeleteAction(record.key)}
                        src={Delete}
                        alt="delete"
                      />
                    </Tooltip>
                  )}
                </WrapAction>
              )}
            />
          )}
        </Table>
      </WrapTable>

      <WrapButton>
        <Button
          disabled={
            details.status ||
            !checkRule("edit", isDynamicButton ? "dynamic_button" : "workflow")
          }
          type="primary"
          onClick={onSave}
          loading={loading.update}
        >
          {t("common.save")}
        </Button>
        <Button onClick={onCancel}>{t("common.cancel")}</Button>
      </WrapButton>

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={
          isDynamicButton ? deleteActionDynamicButton : deleteActionWorkflow
        }
        dataDelete={dataDelete}
        isLoading={loading.modalDelete || false}
      />

      <ModalSendNoti
        showModalNoti={showModalNoti}
        setShowModalNoti={setShowModalNoti}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        userFields={userFields}
        fields_related={fields_related}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />
      <ModalSendSMS
        showModalSMS={showModalSMS}
        setShowModalSMS={setShowModalSMS}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />
      <ModalSendEmail
        showModalEmail={showModalEmail}
        setShowModalEmail={setShowModalEmail}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        userFields={userFields}
        fields_related={fields_related}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        linkingFieldEmail={linkingFieldEmail}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        listMainFields={listMainFields}
        details={details}
      />
      <ModalAddEvent
        showModalAddEvent={showModalAddEvent}
        setShowModalAddEvent={setShowModalAddEvent}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        objectID={objectID}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />
      <ModalCreateRecord
        showModalCreate={showModalCreate}
        setShowModalCreate={setShowModalCreate}
        objectID={objectID}
        objects={category}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
        trigger={trigger}
      />

      <ModalUpdateRecord
        showModalUpdate={showModalUpdate}
        setShowModalUpdate={setShowModalUpdate}
        objectID={objectID}
        objects={category}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />

      <ModalUpdateRelatedRecordTargetSource
        showModalUpdate={showModalUpdateRelatedTargetSource}
        setShowModalUpdate={$showModalUpdateRelatedTargetSource}
        objectID={objectID}
        objects={category}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />

      <ModalUpdateRelatedRecordSourceTarget
        showModalUpdate={showModalUpdateRelatedSourceTarget}
        setShowModalUpdate={$showUpdateRelatedSourceTarget}
        objectID={objectID}
        objects={category}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />

      <ModalCallAPI
        showModalCallAPI={showModalCallAPI}
        setShowModalCallAPI={setShowModalCallAPI}
        objectID={objectID}
        objects={category}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
      />

      <CreateFile
        visible={showCreateFile}
        $visible={$showCreateFile}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
        objects={category}
      />
      <UpdatePermission
        visible={showUpdatePermission}
        $visible={$showUpdatePermission}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
        objects={category}
      />
      <DeletePermission
        visible={showDeletePermission}
        $visible={$showDeletePermission}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllGroups={listAllGroups}
        listAllUser={listAllUser}
        loading={loading}
        detailsAction={
          isDynamicButton ? detailsActionDynamicButton : detailsAction
        }
        status={status}
        isDynamicButton={isDynamicButton}
        details={details}
        objects={category}
      />
    </Wrapper>
  );
}

export default withTranslation()(Actions);

Actions.defaultProps = {
  isDynamicButton: false,
  allActions: [],
};

const Wrapper = styled.div`
  margin-top: 24px;
  background: #fff;
  padding: 24px;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapTitle = styled.div`
  display: flex;
  align-items: center;

  .ant-select {
    width: 256px;
    height: 40px;
  }

  .ant-select-selection-placeholder {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-select-selector {
    height: 100% !important;
    align-items: center;
  }

  .ant-select-selection-search-input {
    height: 100% !important;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  color: ${(props) => props.theme.main};
  margin-right: 10px;
`;

const WrapTable = styled.div`
  background: #fff;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }
`;
