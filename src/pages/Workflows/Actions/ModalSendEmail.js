import { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import isEmail from "validator/lib/isEmail";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";

import Editor from "components/Editor/Editor2";

import { metaData, generalInfo, metaFields } from "util/staticData";

import {
  getEmailTempateById,
  getEmailTempateByIdSuccess,
} from "redux/slices/emailTemplate";
import {
  createAction,
  updateAction,
  loadDetailsActionFail,
} from "redux/slices/workflows";
import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

function ModalSendEmail({
  showModalEmail,
  setShowModalEmail,
  isEdit,
  setIsEdit,
  objectID,
  loading,
  detailsAction,
  status,
  linkingFieldEmail,
  userFields,
  fields_related,
  listAllGroups,
  listAllUser,
  isDynamicButton,
  listMainFields,
  details,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { listEmailTemplate, editEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );
  const editorJodit = useRef(null);

  const [form] = Form.useForm();

  const [listTo, setListTo] = useState([]);
  const [listCheckFields, setListCheckFields] = useState([]);
  const [listCheckValid, setListCheckValid] = useState([]);

  const [listFields, setListFields] = useState([]);
  const [selectedFields, setSelectedFields] = useState([]);
  const [selectedGeneralFields, setSelectedGeneralFields] = useState([]);
  const [listTemplate, setListTemplate] = useState([]);

  const [body, setBody] = useState("");
  const [optionsAppend, setOptionsAppend] = useState([]);

  const formatData = (data, fields, type) => {
    let result = [];

    if (type === "submit" && data) {
      data.forEach((item) => {
        if (fields.find((field) => field.value === item)) {
          result.push("$" + item);
        } else {
          result.push(item);
        }
      });
    }
    if (type === "parse") {
      data.forEach((item) => {
        if (item.split("$").length > 1) {
          result.push(item.split("$")[1]);
        } else {
          result.push(item);
        }
      });
    }
    return result;
  };

  const _onSubmit = (values) => {
    let data = {
      action_name: values.name,
      action_type: 1,
      body: editorJodit.current.value,
      description: values.description,
      bcc: formatData(values.bcc, listCheckFields, "submit"),
      cc: formatData(values.cc, listCheckFields, "submit"),
      to: formatData(values.to, listCheckFields, "submit"),
      object_id: objectID,
      selected_fields: values.fields,
      selected_general_fields: values.general_fields,
      subject: formatData(
        values.subject,
        listFields.concat(metaData),
        "submit"
      ),
    };

    if (isEdit) {
      if (isDynamicButton) {
        dispatch(
          updateActionDynamicButton({
            data: {
              ...data,
              button_id: recordID,
            },
            action_type: 1,
            _id: detailsAction._id,
          })
        );
      } else {
        dispatch(
          updateAction({
            data: {
              ...data,
              workflow_id: recordID,
            },
            action_type: 1,
            _id: detailsAction._id,
          })
        );
      }
    } else {
      if (isDynamicButton) {
        dispatch(
          createActionDynamicButton({
            ...data,
            button_id: recordID,
          })
        );
      } else {
        dispatch(
          createAction({
            ...data,
            workflow_id: recordID,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    setShowModalEmail(false);
    form.resetFields();
    setIsEdit(false);
    setSelectedFields([]);
    setSelectedGeneralFields([]);
    setBody("");
    editorJodit.current.value = "";
    setOptionsAppend([]);
    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  const handleSelectField = (values, options, type) => {
    if (type === "fields") {
      setSelectedFields(options);
      setOptionsAppend(selectedGeneralFields.concat(options));
    } else {
      setSelectedGeneralFields(options);
      setOptionsAppend(selectedFields.concat(options));
    }
  };

  const handleSelectTemplate = (value) => {
    dispatch(
      getEmailTempateById({
        id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(editEmailTemplate).length > 0) {
      form.setFieldsValue({
        subject: formatData(editEmailTemplate.subject, [], "parse"),
        fields: editEmailTemplate.selected_fields,
        general_fields: editEmailTemplate.selected_general_fields,
      });
      const selected_fields = listFields.filter((field) =>
        editEmailTemplate.selected_fields.includes(field.value)
      );

      const selected_general_fields = generalInfo.filter((field) =>
        editEmailTemplate.selected_general_fields.includes(field.value)
      );

      setSelectedFields(selected_fields);
      setSelectedGeneralFields(selected_general_fields);
      setOptionsAppend(selected_fields.concat(selected_general_fields));
      setBody(editEmailTemplate.body);
      dispatch(getEmailTempateByIdSuccess({}));
    }
  }, [detailsAction, dispatch, editEmailTemplate, form, listFields]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            if (ele.section_name !== "Meta fields") {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false &&
                  !_.get(field, "encrypted", false)
                ) {
                  tempOptionsFields.push({
                    label: field.related_name,
                    value: field.full_field_id,
                    type: field.type,
                  });
                }
              });
            }
          });
        } else {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    let tempList = [];
    listEmailTemplate.forEach((item) => {
      if (item.object_id === objectID) {
        tempList.push({
          label: item.email_template_name,
          value: item._id,
        });
      }
    });
    setListTemplate(tempList);
  }, [listEmailTemplate, objectID]);

  useEffect(() => {
    let tempOptionsTo = [];
    let tempListCheckValid = [];

    let tempEmail = [];
    let tempGroup = [];
    let tempUserFields = [];
    let tempUser = [];
    let tempLinkingEmail = [];
    let tempTextFields = [];

    linkingFieldEmail.forEach((item) => {
      tempLinkingEmail.push({ label: item.name, value: item.ID });

      tempListCheckValid.push(item.ID);
    });

    fields_related.forEach((item) => {
      if (item.type === "email") {
        tempEmail.push({
          label: item.name,
          value: item.ID,
        });

        tempListCheckValid.push(item.ID);
      }
    });

    listAllGroups.forEach((item) => {
      tempGroup.push({
        label: item.name,
        value: item._id,
      });

      tempListCheckValid.push(item._id);
    });

    userFields.forEach((item) => {
      tempUserFields.push({
        label: item.name,
        value: item.ID,
      });
      tempUserFields.push({
        label: item.name + ".Report to",
        value: item.ID + ".Report_To",
      });
      tempListCheckValid.push(item.ID);
      tempListCheckValid.push(item.ID + ".Report_To");
    });

    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item.Email,
        });
        tempListCheckValid.push(item._id);
      }
    });

    metaFields.forEach((item) => {
      tempListCheckValid.push(item.value);
    });

    listMainFields.forEach((item) => {
      if (item.type === "text") {
        tempTextFields.push({
          label: item.label,
          value: item.value,
        });

        tempListCheckValid.push(item.value);
      }
    });

    if (tempLinkingEmail.length > 0) {
      tempOptionsTo.push({
        label: "Linking",
        options: tempLinkingEmail,
      });
    }

    if (tempUser.length > 0) {
      tempOptionsTo.push({
        label: "Users",
        options: tempUser,
      });
    }

    if (tempUserFields.length > 0) {
      tempOptionsTo.push({
        label: "User Fields",
        options: tempUserFields,
      });
    }

    if (tempEmail.length > 0) {
      tempOptionsTo.push({
        label: "Email",
        options: tempEmail,
      });
    }

    if (tempGroup.length > 0) {
      tempOptionsTo.push({
        label: "Groups",
        options: tempGroup,
      });
    }

    if (tempTextFields.length > 0) {
      tempOptionsTo.push({
        label: "Text Fields",
        options: tempTextFields,
      });
    }

    tempOptionsTo.push({
      label: "Meta fields",
      options: metaFields,
    });

    setListCheckValid(tempListCheckValid);

    setListCheckFields(
      tempUserFields
        .concat(tempEmail)
        .concat(metaFields)
        .concat(tempLinkingEmail)
        .concat(tempTextFields)
    );
    setListTo(tempOptionsTo);
  }, [
    fields_related,
    listAllGroups,
    listAllUser,
    userFields,
    linkingFieldEmail,
    listMainFields,
  ]);

  useEffect(() => {
    if (showModalEmail && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        name: detailsAction.action_name,
        description: detailsAction.description,
        subject: formatData(detailsAction.subject, [], "parse"),
        to: formatData(detailsAction.to, [], "parse"),
        cc: formatData(detailsAction.cc, [], "parse"),
        bcc: formatData(detailsAction.bcc, [], "parse"),
        fields: detailsAction.selected_fields,
        general_fields: detailsAction.selected_general_fields,
      });

      const selected_fields = listFields.filter((field) =>
        detailsAction.selected_fields.includes(field.value)
      );

      const selected_general_fields = generalInfo.filter((field) =>
        detailsAction.selected_general_fields.includes(field.value)
      );

      setSelectedFields(selected_fields);
      setSelectedGeneralFields(selected_general_fields);
      setOptionsAppend(selected_fields.concat(selected_general_fields));
      setBody(detailsAction.body);
    }
  }, [dispatch, showModalEmail, form, isEdit, detailsAction, listFields]);

  useEffect(() => {
    if (
      showModalEmail &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalEmail(false);
      setIsEdit(false);
      form.resetFields();
      setSelectedFields([]);
      setSelectedGeneralFields([]);
      setBody("");
      setOptionsAppend([]);
    }
  }, [form, setIsEdit, setShowModalEmail, showModalEmail, status]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalEmail}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action || status === "LoadingAction" ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item label={t("workflow.emailTemplate")} name="email_template">
            <Select
              placeholder={t("workflow.selectEmailTemplate")}
              options={listTemplate}
              onChange={handleSelectTemplate}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.subject")}
            name="subject"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listFields.concat(metaData)}
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.to")}
            name="to"
            rules={[
              { required: true, message: t("common.placeholderInputSelect") },
              () => ({
                validator(_, values) {
                  if (values) {
                    let tempNotIncludes = [];
                    values.forEach((item) => {
                      if (!listCheckValid.includes(item)) {
                        tempNotIncludes.push(item);
                      }
                    });

                    if (tempNotIncludes.length > 0) {
                      let invalid = tempNotIncludes.filter(
                        (item) => isEmail(item) === false
                      );

                      if (invalid.length > 0) {
                        return Promise.reject(
                          new Error(t("workflow.invalidEmail"))
                        );
                      }
                    }
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listTo}
            />
          </Form.Item>
          <Form.Item
            label="CC"
            name="cc"
            rules={[
              () => ({
                validator(_, values) {
                  if (values) {
                    let tempNotIncludes = [];
                    values.forEach((item) => {
                      if (!listCheckValid.includes(item)) {
                        tempNotIncludes.push(item);
                      }
                    });

                    if (tempNotIncludes.length > 0) {
                      let invalid = tempNotIncludes.filter(
                        (item) => isEmail(item) === false
                      );

                      if (invalid.length > 0) {
                        return Promise.reject(
                          new Error(t("workflow.invalidEmail"))
                        );
                      }
                    }
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listTo}
            />
          </Form.Item>
          <Form.Item
            label="BCC"
            name="bcc"
            rules={[
              () => ({
                validator(_, values) {
                  if (values) {
                    let tempNotIncludes = [];
                    values.forEach((item) => {
                      if (!listCheckValid.includes(item)) {
                        tempNotIncludes.push(item);
                      }
                    });

                    if (tempNotIncludes.length > 0) {
                      let invalid = tempNotIncludes.filter(
                        (item) => isEmail(item) === false
                      );

                      if (invalid.length > 0) {
                        return Promise.reject(
                          new Error(t("workflow.invalidEmail"))
                        );
                      }
                    }
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listTo}
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.fields")}
            name="fields"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("workflow.selectField")}
              options={listFields}
              mode="multiple"
              onChange={(value, options) =>
                handleSelectField(value, options, "fields")
              }
              optionFilterProp="label"
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.generalFields")}
            name="general_fields"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("workflow.selectField")}
              options={generalInfo}
              mode="multiple"
              onChange={(value, options) =>
                handleSelectField(value, options, "general_fields")
              }
              optionFilterProp="label"
            />
          </Form.Item>

          <Editor
            editorJodit={editorJodit}
            content={body}
            optionsAppend={optionsAppend}
            minHeightInput="200px"
          />

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={isDynamicButton ? status === "Loading" : loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalSendEmail);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
