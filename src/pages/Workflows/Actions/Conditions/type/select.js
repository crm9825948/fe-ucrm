import React, { useEffect } from "react";
import { Select } from "antd";

const { Option } = Select;

const SelectComp = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
  field,
  dataDetails,
}) => {
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }
    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return (
    <>
      <Select
        showSearch
        style={{ width: "100%" }}
        onChange={(e) => {
          let tmp = [...value];
          tmp[index] = e;
          setValue(tmp);

          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });
          tmpConditions[index].value = {
            [operatorValue]: e,
          };

          setConditions(tmpConditions);
        }}
        disabled={
          disabled ||
          operatorValue === "empty" ||
          operatorValue === "not-empty" ||
          operatorValue === undefined
        }
        value={value[index]}
        allowClear
        onClear={() => {
          let tmp = [...value];
          tmp[index] = null;
          setValue(tmp);

          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });
          tmpConditions[index].value = {
            [operatorValue]: null,
          };

          setConditions(tmpConditions);
        }}
      >
        {field?.option?.map((item, idx) => {
          return <Option value={item.value}>{item.label}</Option>;
        })}
      </Select>
    </>
  );
};

export default SelectComp;
