import { Input } from "antd";
import React, { useEffect } from "react";
import styled from "styled-components";

const Email = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
}) => {
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return (
    <Wrapper>
      {/* <Form name="" initialValues={{
          email: value[index]
      }}>
        <Form.Item
          name="email"
          label=""
          rules={[
            {
              type: "email",
              message: "Not valid E-mail!",
            },
          ]}
         
        > */}
      <Input
        disabled={
          disabled || operatorValue === "empty" || operatorValue === "not-empty"
        }
        type="email"
        value={value[index]}
        maxLength={1000}
        onChange={(e) => {
          let tmpConditions = [...conditions];
          // handle contain and not contain
          if (operatorValue === "$regex") {
            tmpConditions[index].value = {
              [operatorValue]: e.target.value,
              $options: "i",
            };
          } else if (operatorValue === "$not") {
            tmpConditions[index].value = {
              $not: {
                $regex: e.target.value,
                $options: "i",
              },
            };
          } else {
            tmpConditions[index].value = {
              [operatorValue]: e.target.value,
            };
          }

          let tmp = [...value];
          tmp[index] = e.target.value;
          setValue(tmp);

          setConditions(tmpConditions);
        }}
      />
      {/* </Form.Item> */}
      {/* </Form> */}
    </Wrapper>
  );
};

export default Email;

const Wrapper = styled.div`
  .ant-form-item {
    margin-bottom: 0px;
  }
`;
