import React, { useEffect } from "react";
import { DatePicker } from "antd";
import moment from "moment";
const { RangePicker } = DatePicker;

const Date = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
  // init,
}) => {
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  if (operatorValue === "$between") {
    return (
      <RangePicker
        value={
          value[index]
            ? [moment(value[index][0]), moment(value[index][1])]
            : null
        }
        onChange={(e, dateString) => {
          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });

          tmpConditions[index].value = {
            $gte: dateString[0],
            $lte: dateString[1],
          };

          let tmp = [...value];
          tmp[index] = dateString;
          setValue(tmp);
          setConditions(tmpConditions);
        }}
      />
    );
  } else
    return (
      <>
        <DatePicker
          disabled={
            disabled ||
            operatorValue === "empty" ||
            operatorValue === "not-empty"
          }
          value={value[index] ? moment(value[index]) : null}
          onChange={(e, dateString) => {
            let tmpConditions = [];
            /*eslint-disable-next-line*/
            conditions.map((item, idx) => {
              tmpConditions.push({ ...item });
            });

            if (operatorValue !== "$between") {
              tmpConditions[index].value = {
                [operatorValue]: dateString,
              };
              let tmp = [...value];
              tmp[index] = dateString;
              setValue(tmp);
              setConditions(tmpConditions);
            }
          }}
        />
      </>
    );
};

export default Date;
