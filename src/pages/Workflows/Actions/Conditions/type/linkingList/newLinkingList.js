import { SearchOutlined } from "@ant-design/icons";
import { Button, Drawer, Input, Pagination, Space, Table } from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import editImg from "assets/icons/common/icon-edit.png";
import React, { useEffect, useState } from "react";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import { loadDataLinking, loadPaginationLinking } from "redux/slices/objects";
import styled from "styled-components";

const LinkingList = (props) => {
  const {
    visible,
    setVisible,
    field,
    selectedRowKeys,
    setSelectedRowKeys,
    handleSelect,
    setSelectedRows,
    selectedRows,
    conditions,
    index,
  } = props;
  /*eslint-disable-next-line*/
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchList, setSearchList] = useState({});
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearcherColumn] = useState("");
  const dispatch = useDispatch();
  const [dateSource, setDataSource] = useState([]);
  const [column, setColumn] = useState([]);

  const { headerLinking, dataLinking, totalRecordLinking } = useSelector(
    (state) => state.objectsReducer
  );

  useEffect(() => {
    let newData = [];
    /* eslint-disable-next-line */
    dataLinking.map((item, index) => {
      let newItem = {};
      newItem["key"] = item._id;
      /* eslint-disable-next-line */
      Object.entries(item).forEach(([key, value], idx) => {
        if (typeof value === "object") {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      newData.push(newItem);
    });
    setDataSource(newData);
  }, [dataLinking]);

  useEffect(() => {
    let newColumn = [];
    /* eslint-disable-next-line */
    headerLinking.map((item, idx) => {
      let newItem = {
        title: item.name,
        dataIndex: item.ID,
        key: item.ID,
        width: "maxContent",
        editable: true,
        type: item.type,
        ...getColumnSearchProps(item.ID, idx, item.name, searchList, item.type),
      };
      newColumn.push(newItem);
    });
    newColumn.push({
      title: "Action",
      key: "action",
      fixed: "right",
      render: () => (
        <Space size="middle">
          <a href>
            <img alt="" src={editImg} style={{ width: "20px" }} />
          </a>
          <a className="ant-dropdown-link" href>
            <img alt="" src={deleteImg} style={{ width: "20px" }} />
          </a>
        </Space>
      ),
    });
    setColumn(newColumn);
    /* eslint-disable-next-line */
  }, [headerLinking, searchList]);

  const onCloseList = () => {
    setVisible(false);
    setSelectedRowKeys([]);
  };

  const getColumnSearchProps = (dataIndex, idx, name, searchList, type) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${name}`}
          value={selectedKeys[0]}
          onChange={(e) => {
            setSelectedKeys(e.target.value ? [e.target.value] : []);
          }}
          onPressEnter={() => {
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => {
              handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </CustomButtonCancel>
          <CustomButtonCancel
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });

              setSearchText(selectedKeys[0]);
              setSearcherColumn(dataIndex);
            }}
          >
            Filter
          </CustomButtonCancel>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text, ...props) => {
      /* eslint-disable-next-line */
      const expression =
        /*eslint-disable-next-line*/
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
      const regex = new RegExp(expression);
      if (text && typeof text === "object") {
        return text.map((ele, idx) => {
          if (ele !== null && ele.match(regex)) {
            let fileNew = ele.split("/");
            return (
              <a style={{ display: "block" }} href>
                {fileNew[fileNew.length - 1]}
              </a>
            );
          } else {
            return searchedColumn === dataIndex ? (
              <Highlighter
                highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text ? text.toString() : ""}
              />
            ) : (
              text
            );
          }
        });
      } else {
        return searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ""}
          />
        ) : (
          text
        );
      }
    },
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    let searchTemp = { ...searchList };
    searchTemp[dataIndex] = selectedKeys[0];
    setSearchList(searchTemp);
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchTemp).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    reloadData(searchData, currentPage, recordPerPage);
    setSearchText(selectedKeys[0]);
    setSearcherColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const reloadData = (searchData, currentPage, recordPerPage) => {
    dispatch(
      loadDataLinking({
        object_id: field.objectname,
        current_page: currentPage,
        record_per_page: recordPerPage,
        search_with: {
          meta: [],
          data: searchData,
        },
        filter: [],
      })
    );
    dispatch(
      loadPaginationLinking({
        object_id: field.objectname,
        current_page: currentPage,
        record_per_page: recordPerPage,
        search_with: {
          meta: [],
          data: searchData,
        },
        filter: [],
      })
    );
  };

  const onSelectChange = (selectedRowKeys, selectedRows) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRows(selectedRows);
  };

  return (
    <>
      <Drawer
        title="Linking list"
        placement="right"
        onClose={onCloseList}
        visible={visible}
        width={1000}
      >
        <Wrapper>
          <Table
            // bordered
            dataSource={dateSource}
            columns={column}
            rowClassName="editable-row"
            pagination={false}
            rowSelection={{
              selectedRowKeys: conditions[index].key
                ? [conditions[index].key]
                : selectedRowKeys,
              onChange: onSelectChange,
              type: "radio",
            }}
            scroll={{
              x: "max-content",
            }}
          />
          <div>
            <CustomPagination
              showQuickJumper
              current={currentPage}
              total={totalRecordLinking}
              showSizeChanger
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              // pageSize={recordPerPage}
              onChange={(e, pageSize) => {
                setCurrentPage(e);
                //loadData
                let searchData = [];
                /* eslint-disable-next-line */
                Object.entries(searchList).forEach(([key, value], index) => {
                  if (value) {
                    let newItem = {
                      id_field: key,
                      value: value,
                    };
                    searchData.push(newItem);
                  }
                });
                reloadData(searchData, e, pageSize);
              }}
            />
            <WrapperSave>
              <CustomButtonSave
                onClick={() => {
                  handleSelect(selectedRowKeys, selectedRows);
                }}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                onClick={() => {
                  setVisible(false);
                }}
              >
                Cancel
              </CustomButtonCancel>
            </WrapperSave>
          </div>
        </Wrapper>
      </Drawer>
    </>
  );
};

export default LinkingList;

const WrapperSave = styled.div`
  position: fixed;
  bottom: -8px;
  right: 826px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
`;

const CustomPagination = styled(Pagination)`
  position: fixed;
  bottom: -8px;
  right: 16px;
  width: 976px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
  .ant-pagination-item-active {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
  .ant-pagination-item:hover {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
`;

const Wrapper = styled.div`
  table {
    width: max-content;
    th {
      width: max-content;
      .ant-table-column-title {
        margin-right: 30px;
      }
    }
  }
  padding-bottom: 100px;
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  border-color: ${(props) => props.theme.main};
  margin-right: 16px;
  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;

  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
