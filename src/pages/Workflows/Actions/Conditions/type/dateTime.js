import { DatePicker } from "antd";
import moment from "moment";
import React, { useEffect } from "react";

const { RangePicker } = DatePicker;

const Date = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
  ID,
  init,
}) => {
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });

    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        // setValue(tmp);
        // setConditions(tmpConditions);
        break;
    }
    /*eslint-disable-next-line*/
  }, [operatorValue]);

  if (operatorValue === "$between") {
    return (
      <RangePicker
        showTime
        value={
          value[index]
            ? [moment(value[index][0]), moment(value[index][1])]
            : null
        }
        onChange={(e, dateString) => {
          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });

          tmpConditions[index].value = {
            $gte: dateString[0],
            $lte: dateString[1],
          };

          let tmp = [...value];
          tmp[index] = dateString;
          setValue(tmp);
          setConditions(tmpConditions);
        }}
      />
    );
  } else {
    return (
      <>
        <DatePicker
          showTime
          disabled={
            disabled ||
            operatorValue === "empty" ||
            operatorValue === "not-empty"
          }
          value={
            ID && value[index] && typeof value[index] !== "string"
              ? value[index]
              : value[index] && typeof value[index] !== "object"
              ? moment(value[index])
              : null
          }
          onChange={(e, dateString) => {
            // let tmpConditions = [...conditions];
            let tmpConditions = [];
            /*eslint-disable-next-line*/
            conditions.map((item, idx) => {
              tmpConditions.push({ ...item });
            });

            if (operatorValue !== "$between") {
              tmpConditions[index].value = {
                [operatorValue]: dateString,
              };
              let tmp = [...value];
              tmp[index] = dateString;
              setValue(tmp);
              setConditions(tmpConditions);
            }
          }}
        />
      </>
    );
  }
};

export default Date;
