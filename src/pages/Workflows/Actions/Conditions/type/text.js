import React, { useEffect } from "react";
import { Select } from "antd";

const Text = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
  dataDetails,
  fieldsMain,
}) => {
  useEffect(() => {
    // let tmpConditions = [...conditions];
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      case "today":
      case "yesterday":
      case "this-week":
      case "last-week":
      case "this-month":
      case "last-month":
      case "this-year":
        tmpConditions[index].value = {
          $eq: operatorValue,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return (
    <Select
      mode="tags"
      options={fieldsMain}
      disabled={
        disabled || operatorValue === "empty" || operatorValue === "not-empty"
      }
      value={value[index] ? value[index] : []}
      onChange={(e) => {
        if (e?.length > 1) {
          e.shift();
        }

        let tmpConditions = [];
        conditions.forEach((item) => {
          tmpConditions.push({ ...item });
        });

        if (operatorValue === "$regex") {
          tmpConditions[index].value = {
            [operatorValue]: e[0],
            $options: "i",
          };
        } else if (operatorValue === "$not") {
          tmpConditions[index].value = {
            $not: {
              $regex: e[0],
              $options: "i",
            },
          };
        } else {
          tmpConditions[index].value = {
            [operatorValue]: e[0],
          };
        }

        let tmp = [...value];
        tmp[index] = e[0];
        setValue(tmp);

        setConditions(tmpConditions);
      }}
    />

    // <Input
    //   disabled={
    //     disabled || operatorValue === "empty" || operatorValue === "not-empty"
    //   }
    //   value={value[index]}
    //   maxLength={1000}
    //   onChange={(e) => {
    //     let tmpConditions = [];
    //     /*eslint-disable-next-line*/
    //     conditions.map((item, idx) => {
    //       tmpConditions.push({ ...item });
    //     });

    //     // handle contain and not contain
    //     if (operatorValue === "$regex") {
    //       tmpConditions[index].value = {
    //         [operatorValue]: e.target.value,
    //         $options: "i",
    //       };
    //     } else if (operatorValue === "$not") {
    //       tmpConditions[index].value = {
    //         $not: {
    //           $regex: e.target.value,
    //           $options: "i",
    //         },
    //       };
    //     } else {
    //       tmpConditions[index].value = {
    //         [operatorValue]: e.target.value,
    //       };
    //     }

    //     let tmp = [...value];
    //     tmp[index] = e.target.value;
    //     setValue(tmp);

    //     setConditions(tmpConditions);
    //   }}
    // />
  );
};

export default Text;
