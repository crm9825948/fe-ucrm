import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Input } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  loadDataLinking,
  loadHeaderLinking,
  loadPaginationLinking,
} from "redux/slices/objects";
import LinkingList from "./linkingList/linkingList";

const Linking = ({
  field,
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
}) => {
  const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const dispatch = useDispatch();
  const [selectedRows, setSelectedRows] = useState([]);

  useEffect(() => {
    if (conditions[index].key) {
      setSelectedRowKeys([conditions[index].key]);
    }
  }, [conditions, index]);

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadHeaderLinking({
          object_id: field.objectname,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
    }
    /* eslint-disable-next-line */
  }, [dispatch, visible]);

  const handleSelect = (selectedRowKeys, selectedRows) => {
    setVisible(false);

    let tmpConditions = [...conditions];
    tmpConditions[index] = {
      ...tmpConditions[index],
      value: {
        ...tmpConditions[index].value,
        [operatorValue]: selectedRows[0][field.field],
      },
    };
    tmpConditions[index] = {
      ...tmpConditions[index],
      key: selectedRowKeys[0],
    };
    setConditions(tmpConditions);

    let tmp = [...value];
    tmp[index] = selectedRows[0][field.field];
    setValue(tmp);
  };

  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return (
    <>
      <Input
        disabled={
          disabled || operatorValue === "empty" || operatorValue === "not-empty"
        }
        addonAfter={
          <>
            {disabled ||
            operatorValue === "empty" ||
            operatorValue === "not-empty" ||
            operatorValue === "mine" ||
            operatorValue === "not-mine" ? (
              <SearchOutlined style={{ pointerEvents: "none" }} />
            ) : (
              <SearchOutlined
                onClick={() => {
                  setVisible(true);
                }}
              />
            )}
          </>
        }
        addonBefore={
          disabled ||
          operatorValue === "empty" ||
          operatorValue === "not-empty" ||
          operatorValue === "mine" ||
          operatorValue === "not-mine" ? (
            ""
          ) : value[index] ? (
            <ClearOutlined
              onClick={() => {
                let tmpConditions = [...conditions];

                let newTmpConditions = [];
                tmpConditions.forEach((item, idx) => {
                  newTmpConditions[idx] = { ...item };
                });

                newTmpConditions[index].value = {
                  [operatorValue]: "",
                };

                newTmpConditions[index].key = "";
                setConditions(newTmpConditions);

                let tmp = [...value];
                tmp[index] = null;
                setValue(tmp);
              }}
            />
          ) : (
            ""
          )
        }
        value={value[index]}
      />
      <LinkingList
        visible={visible}
        setVisible={setVisible}
        field={field}
        selectedRowKeys={selectedRowKeys}
        setSelectedRowKeys={setSelectedRowKeys}
        handleSelect={handleSelect}
        setSelectedRows={setSelectedRows}
        selectedRows={selectedRows}
        conditions={conditions}
        index={index}
      />
    </>
  );
};

export default Linking;
