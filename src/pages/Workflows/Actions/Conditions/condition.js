import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import ConditionItem from "./conditionItem";
import moment from "moment";

const Condition = ({
  title,
  decs,
  conditions,
  setConditions,
  ID,
  dataDetails,
  operatorValue,
  setOperatorValue,
  value,
  setValue,
  fieldsMain,
}) => {
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [fields, setFields] = useState({});
  // const [operatorValue, setOperatorValue] = useState([]);
  // const [value, setValue] = useState([]);
  const [init, setInit] = useState(false);
  useEffect(() => {
    if (conditions.length === 0) {
      setOperatorValue([]);
      setValue([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [conditions]);

  useEffect(() => {
    let operatorVal = [];
    let valueArray = [];
    if (ID) {
      /*eslint-disable-next-line*/
      conditions.map((item, idx) => {
        //length === 1
        if (Object.keys(item).length > 0)
          if (Object.keys(item && item.value).length === 1) {
            //Operator
            let keys = Object.keys(item.value);

            if (keys[0] !== "$eq" && keys[0] !== "$ne") {
              operatorVal[idx] = keys[0];
            } else if (keys[0] === "$eq" || keys[0] === "$ne") {
              if (item.value["$eq"] === null) {
                operatorVal[idx] = "empty";
              } else if (item.value["$ne"] === null) {
                operatorVal[idx] = "not-empty";
              } else if (item.value["$eq"] === "mine") {
                operatorVal[idx] = "mine";
              } else if (item.value["$ne"] === "mine") {
                operatorVal[idx] = "not-mine";
              } else {
                if (
                  item.value[keys[0]] === "today" ||
                  item.value[keys[0]] === "yesterday" ||
                  item.value[keys[0]] === "this-week" ||
                  item.value[keys[0]] === "last-week" ||
                  item.value[keys[0]] === "this-month" ||
                  item.value[keys[0]] === "last-month" ||
                  item.value[keys[0]] === "this-year"
                ) {
                  operatorVal[idx] = item.value[keys[0]];
                } else {
                  operatorVal[idx] = keys[0] === undefined ? "" : keys[0];
                }
              }
            }

            //Value
            if (item.type === "datetime" || item.type === "datetime-local") {
              if (
                item.value[keys[0]] === "today" ||
                item.value[keys[0]] === "yesterday" ||
                item.value[keys[0]] === "this-week" ||
                item.value[keys[0]] === "last-week" ||
                item.value[keys[0]] === "this-month" ||
                item.value[keys[0]] === "last-month" ||
                item.value[keys[0]] === "this-year"
              ) {
                valueArray[idx] = null;
              } else if (item.value[keys[0]]) {
                valueArray[idx] = moment(item.value[keys[0]]);
              } else {
                valueArray[idx] = null;
              }
            }
            if (item.type === "user") {
              if (keys[0] === "$not") {
                valueArray[idx] = item.value[keys[0]]["$regex"];
              } else {
                // if (
                //   operatorVal[idx] === "mine" ||
                //   operatorVal[idx] === "not-mine"
                // ) {
                //   valueArray[idx] = null;
                // } else {
                valueArray[idx] = item.value[keys[0]];
                // }
              }
            } else {
              if (keys[0] === "$not") {
                valueArray[idx] = item.value[keys[0]]["$regex"];
              } else {
                if (
                  operatorVal[idx] === "mine" ||
                  operatorVal[idx] === "not-mine"
                ) {
                  valueArray[idx] = null;
                } else {
                  valueArray[idx] = item.value[keys[0]];
                }
              }
            }
          } else if (Object.keys(item && item.value).length > 1) {
            let keys = Object.keys(item.value);
            if (keys[0] === "$gte" && keys[1] === "$lte") {
              operatorVal[idx] = "$between";
            } else {
              operatorVal[idx] = keys[0];
            }

            //value
            if (keys[0] === "$gte" && keys[1] === "$lte") {
              valueArray[idx] = [
                moment(item.value[keys[0]]),
                moment(item.value[keys[1]]),
              ];
            } else {
              valueArray[idx] = item.value[keys[0]];
            }
          } else {
          }
      });
      setValue(valueArray);
      setOperatorValue(operatorVal);
      setInit(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [ID, conditions, init]);

  useEffect(() => {
    if (listObjectField.length > 0) {
      let objectFields = {};
      listObjectField.forEach((object, idx) => {
        if (listObjectField.length - 1 === idx) {
          objectFields["main_object"] = [];
          object["main_object"].sections.forEach((section) => {
            section.fields.forEach((field) => {
              if (
                field.type === "text" ||
                field.type === "textarea" ||
                field.type === "email"
              ) {
                objectFields["main_object"].push(field);
              }
            });
          });
        }
        // else {
        //   Object.entries(object).forEach(([key, value]) => {
        //     objectFields[object[key].object_name] = [];
        //     return object[key].sections.forEach((section) => {
        //       return section.fields.forEach((field) => {
        //         if (
        //           field.hidden === false &&
        //           field.permission_hidden === false
        //         ) {
        //           if (
        //             field.type === "text" ||
        //             field.type === "textarea" ||
        //             field.type === "email"
        //           ) {
        //             objectFields[object[key].object_name].push(field);
        //           }
        //         } else if (field.permission_hidden === undefined) {
        //           if (
        //             field.type === "text" ||
        //             field.type === "textarea" ||
        //             field.type === "email"
        //           ) {
        //             objectFields[object[key].object_name].push(field);
        //           }
        //         }
        //       });
        //     });
        //   });
        // }
      });
      setFields(objectFields);
    } else {
      setFields({});
    }
  }, [listObjectField]);

  return (
    <Wrapper>
      <Title className="conditions__title">
        {title} <span>{decs}</span>
      </Title>

      {Object.keys(fields).length > 0 && (
        <>
          {conditions.map((condition, idx) => {
            return (
              <ConditionItem
                key={idx}
                condition={condition}
                conditions={conditions}
                setConditions={setConditions}
                fields={fields}
                index={idx}
                object_name={
                  listObjectField.length > 0 &&
                  listObjectField[listObjectField.length - 1]["main_object"]
                    .object_name
                }
                operatorValue={operatorValue}
                setOperatorValue={setOperatorValue}
                value={value}
                setValue={setValue}
                ID={ID}
                dataDetails={dataDetails}
                init={init}
                fieldsMain={fieldsMain}
              />
            );
          })}
        </>
      )}
      {conditions.length < 5 ? (
        <CustomButton
          onClick={() => {
            let tempConditions = [...conditions];
            tempConditions.push({});
            setConditions(tempConditions);
          }}
          type="button"
        >
          + Add condition
        </CustomButton>
      ) : (
        ""
      )}
    </Wrapper>
  );
};

export default Condition;

const Wrapper = styled.div`
  min-height: 50px;
  border: 1px solid #d9d9d9;
  box-sizing: border-box;
  border-radius: 5px;
  /* .ant-select:not(.ant-select-customize-input)  */
  .ant-select-selector {
    border: 1px solid #d9d9d9 !important;
    box-sizing: border-box;
    border-radius: 2px !important;
  }
`;

const Title = styled.div`
  font-style: normal;
  font-family: var(--roboto-700);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  display: flex;
  align-items: center;

  color: #2c2c2c;
  margin-top: -13px;
  background-color: #fff;
  width: fit-content;
  margin-left: 22px;
  padding-left: 8px;
  padding-right: 8px;
  span {
    font-family: var(--roboto-400);
    margin-left: 10px;
  }
`;

const CustomButton = styled.button`
  border: none;
  background-color: #fff;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  text-align: center;
  text-decoration-line: underline;

  /* Primary/6 */

  color: ${(props) => props.theme.main};
  margin-left: 30px;
  margin-top: 8px;
  margin-bottom: 16px;
  :hover {
    cursor: pointer;
    color: ${(props) => props.theme.darker}!important;
  }
`;
