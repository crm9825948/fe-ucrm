import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import moment from "moment";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";
import Tooltip from "antd/lib/tooltip";

import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";

import Other from "./fieldType/other";
import SelectType from "./fieldType/select";
import Number from "./fieldType/number";
import Email from "./fieldType/email";
import Date from "./fieldType/date";
import LinkingObject from "./fieldType/linkingobject";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
  loadFieldsObject,
  updateRequiredFieldsCreate,
  updateOptionalFieldsCreate,
  loadFieldsObjectFail,
  loadTargetPicklistFail,
  loadFieldsObjectSuccess,
} from "redux/slices/workflows";

import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

import { optionsCreateRecordCallValues } from "util/staticData";

import { Notification } from "components/Notification/Noti";

function ModalCreateRecord({
  showModalCreate,
  setShowModalCreate,
  isEdit,
  setIsEdit,
  objectID,
  loading,
  detailsAction,
  status,
  objects,
  isDynamicButton,
  listAllUser,
  details,
  trigger,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const {
    listFields,
    listFieldsMain,
    requiredFieldsCreate,
    optionalFieldsCreate,
  } = useSelector((state) => state.workflowsReducer);

  const [form] = Form.useForm();

  const [listObjects, setListObjects] = useState([]);
  const [fieldsMain, setFieldsMain] = useState([]);
  const [fieldsID, setFieldsID] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [optionalFieldsSelect, setOptionalFieldsSelect] = useState([]);
  const [requiredFieldsValue, setRequiredFieldsValue] = useState([]);
  const [formValue, setFormValue] = useState({});

  const _onSubmit = (values) => {
    const emptyField = optionalFieldsSelect.find(
      (item) => item.field_id === ""
    );

    if (emptyField) {
      Notification("warning", "Please fullfill or remove empty Field!");
    } else {
      const fieldsValues = requiredFieldsValue.concat(optionalFieldsSelect);
      let tempFieldValue = {};
      let interactionFields = {};

      fieldsValues.forEach((item) => {
        if (optionsCreateRecordCallValues.includes(item.value)) {
          interactionFields = {
            ...interactionFields,
            [item.field_id]: item.value,
          };
        } else {
          tempFieldValue = {
            ...tempFieldValue,
            [item.field_id]: item.value,
          };
        }
      });

      let data = {
        object_id: objectID,
        action_name: values.name,
        description: values.description,
        action_type: 2,
        object_create_record: values.object,
        fields_value: tempFieldValue,
        fields_values: fieldsValues,
      };

      if (isEdit) {
        if (isDynamicButton) {
          dispatch(
            updateActionDynamicButton({
              data: {
                ...data,
                button_id: recordID,
              },
              action_type: 2,
              _id: detailsAction._id,
            })
          );
        } else {
          dispatch(
            updateAction({
              data: {
                ...data,
                interaction_fields: interactionFields,
                workflow_id: recordID,
              },
              action_type: 2,
              _id: detailsAction._id,
            })
          );
        }
      } else {
        if (isDynamicButton) {
          dispatch(
            createActionDynamicButton({
              ...data,
              button_id: recordID,
            })
          );
        } else {
          dispatch(
            createAction({
              ...data,
              interaction_fields: interactionFields,
              workflow_id: recordID,
            })
          );
        }
      }
    }
  };

  const _onCancel = () => {
    setShowModalCreate(false);
    form.resetFields();
    setIsEdit(false);
    setOptionalFieldsSelect([]);
    setRequiredFieldsValue([]);
    setFormValue({});
    dispatch(updateRequiredFieldsCreate([]));
    dispatch(updateOptionalFieldsCreate([]));
    dispatch(loadFieldsObjectFail());
    dispatch(loadTargetPicklistFail());
    dispatch(loadFieldsObjectSuccess([]));

    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  const handleChangeFieldRequired = (
    field_id,
    value,
    idx,
    isDate,
    operator
  ) => {
    let temp = [...requiredFieldsValue];

    const foundExist = temp.find((item) => item.field_id === field_id);
    const foundIndex = temp.findIndex((item) => item.field_id === field_id);

    if (foundExist) {
      temp[foundIndex] = {
        field_id: field_id,
        value: value,
        required: true,
        isDate: isDate ? true : false,
        operator: operator ? operator : null,
      };
      setRequiredFieldsValue(temp);
    } else {
      setRequiredFieldsValue([
        ...requiredFieldsValue,
        {
          field_id: field_id,
          value: value,
          required: true,
          isDate: isDate ? true : false,
          operator: operator ? operator : null,
        },
      ]);
    }
  };

  const onDeleteField = (idx) => {
    let temp = [...optionalFieldsSelect];
    temp.splice(idx, 1);
    temp.forEach((item) => {
      form.setFieldsValue({
        [item.value]: undefined,
      });
    });

    setOptionalFieldsSelect(temp);
  };

  const _onAddField = () => {
    const item = [
      ...optionalFieldsSelect,
      { field_id: "", value: "", type: "", required: false, isDate: false },
    ];
    setOptionalFieldsSelect(item);
  };

  const handleChangeField = (
    e,
    option,
    idx,
    type,
    isDate,
    id_field_related_record,
    object_related,
    id_related_record,
    operator
  ) => {
    let arr = [...optionalFieldsSelect];

    let tempField = {};
    optionalFieldsCreate.forEach((item) => {
      if (item.value === e) {
        tempField = item;
      }
    });

    if (type === "field") {
      optionalFieldsSelect.forEach((item, index) => {
        if (index === idx) {
          arr[index] = {
            ...item,
            field_id: e,
            type: tempField.type,
            value: "",
          };
        }
      });
    } else {
      optionalFieldsSelect.forEach((item, index) => {
        if (index === idx) {
          arr[index] = {
            ...item,
            value: e,
            isDate: isDate ? true : false,
            operator: operator ? operator : null,
          };
        }
      });
    }

    setOptionalFieldsSelect(arr);
  };

  const handleSelectObject = useCallback(
    (value) => {
      setOptionalFieldsSelect([]);
      setRequiredFieldsValue([]);
      setFormValue({});
      dispatch(updateRequiredFieldsCreate([]));
      dispatch(updateOptionalFieldsCreate([]));
      dispatch(
        loadFieldsObject({
          api_version: "2",
          object_id: value,
        })
      );
    },
    [dispatch]
  );

  const handleFieldType = (field, optionalFields, required, idx) => {
    switch (field.type) {
      case "number":
        return (
          <Number
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      // case "datetime-local":
      //   return (
      //     <DateTime
      //       form={form}
      //       key={field.value}
      //       field={field}
      //       optionalFields={optionalFields}
      //       required={required}
      //     />
      //   );
      case "date":
      case "datetime-local":
        return (
          <Date
            form={form}
            key={field.value}
            field={field}
            required={required}
            idx={idx}
            listValues={optionalFieldsSelect}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      case "email":
        return (
          <Email
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      case "user":
      case "file":
      case "select":
      case "dynamic-field":
      case "lookup":
        return (
          <SelectType
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            handleChangeFieldRequired={handleChangeFieldRequired}
            idx={idx}
            handleChangeField={handleChangeField}
            isEdit={
              showModalCreate && isEdit && Object.keys(detailsAction).length > 0
            }
            listObjects={listObjects}
            objectID={objectID}
            updateRecord={false}
          />
        );
      case "linkingobject":
        return (
          <LinkingObject
            flag={""}
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            fieldsMain={fieldsMain}
            required={required}
            // objectID={objectID}
            objectCreate={form.getFieldValue("object")}
            form={form}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      default:
        return (
          <Other
            trigger={trigger}
            isUseFieldCall
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            fieldsMain={fieldsMain}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
            fieldsID={fieldsID}
          />
        );
    }
  };

  useEffect(() => {
    optionalFieldsSelect.forEach((item, idx) => {
      if (item.field_id !== "") {
        form.setFieldsValue({
          [item.field_id + idx]: item.field_id,
          [item.field_id]:
            item.value !== ""
              ? item.type === "number" || item.type === "email"
                ? [item.value]
                : item.type === "date" || item.type === "datetime-local"
                ? item.isDate
                  ? moment(item.value)
                  : item.value
                : item.value
              : undefined,
        });
      } else {
        form.setFieldsValue({
          [item.field_id + idx]: undefined,
        });
      }
    });
  }, [form, optionalFieldsSelect]);

  useEffect(() => {
    let tempListFieldMain = [];
    let tempListFieldID = [];

    listFieldsMain.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (
              field.hidden === false &&
              field.permission_hidden === false &&
              field.type !== "formula" &&
              !_.get(field, "encrypted", false)
            ) {
              if (field.type === "id") {
                if (Object.keys(item)[0] === "main_object") {
                  tempListFieldID.push({
                    label: field.related_name,
                    value:
                      Object.keys(item)[0] === "main_object"
                        ? "$" + field.ID
                        : "$" + Object.keys(item)[0] + "." + field.ID,
                    type: field.type,
                  });
                }
              } else {
                tempListFieldMain.push({
                  label: field.related_name,
                  value:
                    Object.keys(item)[0] === "main_object"
                      ? "$" + field.ID
                      : "$" + Object.keys(item)[0] + "." + field.ID,
                  type: field.type,
                });
              }
            }
          });
        });
      }
    });
    setFieldsMain(tempListFieldMain);
    setFieldsID(tempListFieldID);
  }, [listFieldsMain]);

  useEffect(() => {
    let tempRequiredFields = [];
    let tempOptionalFields = [];
    if (showModalCreate && listFields.length > 0 && fieldsMain.length > 0) {
      listFields.forEach((item) => {
        if (
          Object.values(item)[0] !== null &&
          (Object.values(item)[0].readable || Object.values(item)[0].writeable)
        ) {
          if (Object.keys(item)[0] === "main_object") {
            Object.values(item)[0].sections.forEach((ele) => {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false &&
                  field.type !== "id" &&
                  field.type !== "formula" &&
                  !_.get(field, "encrypted", false)
                ) {
                  if (field.required === true) {
                    if (
                      field.type === "select" ||
                      field.type === "dynamic-field"
                    ) {
                      tempRequiredFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: field.option,
                        required: field.required,
                        source: field.source,
                        target: field.target,
                      });
                    } else if (field.type === "linkingobject") {
                      if (field.key) {
                        tempRequiredFields.push({
                          label: field.name,
                          value: field.ID,
                          type: field.type,
                          options: [],
                          required: field.required,
                          related_name: field.related_name,
                          objectname: field.objectname,
                          field: field.field,
                        });
                      }
                    } else if (field.type === "lookup") {
                      tempRequiredFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: [],
                        required: field.required,
                        related_name: field.related_name,
                        objectname: field.objectname,
                        field: field.field,
                      });
                    } else {
                      tempRequiredFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: field.option || [],
                        required: field.required,
                      });
                    }
                  } else {
                    if (
                      field.type === "select" ||
                      field.type === "dynamic-field"
                    ) {
                      tempOptionalFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: field.option,
                        required: field.required,
                        source: field.source,
                        target: field.target,
                      });
                    } else if (field.type === "linkingobject") {
                      if (field.key) {
                        tempOptionalFields.push({
                          label: field.name,
                          value: field.ID,
                          type: field.type,
                          options: [],
                          required: field.required,
                          related_name: field.related_name,
                          objectname: field.objectname,
                          field: field.field,
                        });
                      }
                    } else if (field.type === "lookup") {
                      tempOptionalFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: [],
                        required: field.required,
                        related_name: field.related_name,
                        objectname: field.objectname,
                        field: field.field,
                      });
                    } else {
                      tempOptionalFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: field.option || [],
                        required: field.required,
                      });
                    }
                  }
                }
              });
            });
          }
        }
      });

      tempRequiredFields.push({
        label: "Assign to",
        value: "owner",
        type: "user",
        options: listUser,
      });

      const foundTargetObject = listObjects.find(
        (object) => object.value === form.getFieldValue("object")
      );

      // const foundMainObject = listObjects.find(
      //   (object) => object.value === objectID
      // );

      tempRequiredFields.forEach((requiredField, idx) => {
        switch (requiredField.type) {
          case "select":
            if (foundTargetObject) {
              let newOptions = [...requiredField.options];
              if (foundTargetObject.value === objectID) {
                newOptions.push({
                  label: foundTargetObject.label + "." + requiredField.label,
                  value:
                    "$" + foundTargetObject.value + "." + requiredField.value,
                });
              } else {
                if (!isDynamicButton) {
                  listFieldsMain.forEach((element) => {
                    if (
                      Object.values(element)[0].readable ||
                      Object.values(element)[0].writeable
                    ) {
                      if (Object.keys(element)[0] === "main_object") {
                        Object.values(element)[0].sections.forEach((main) => {
                          main.fields.forEach((fld) => {
                            if (
                              fld.hidden === false &&
                              fld.permission_hidden === false &&
                              !_.get(fld, "encrypted", false) &&
                              fld.type === "select"
                            ) {
                              newOptions.push({
                                label:
                                  Object.values(element)[0].object_name +
                                  "." +
                                  fld.name,
                                value: "$" + objectID + "." + fld.ID,
                              });
                            }
                          });
                        });
                      }
                    }
                  });
                }
              }
              tempRequiredFields[idx] = {
                ...requiredField,
                options: newOptions,
              };
            }
            break;
          case "dynamic-field":
            if (foundTargetObject && foundTargetObject.value === objectID) {
              let newOptions = [...requiredField.options];

              newOptions.push({
                label: foundTargetObject.label + "." + requiredField.label,
                value:
                  "$" + foundTargetObject.value + "." + requiredField.value,
              });

              tempRequiredFields[idx] = {
                ...requiredField,
                options: newOptions,
              };

              // if (
              //   (requiredField.source && requiredField.source !== null) ||
              //   (requiredField.target && requiredField.target.length > 0)
              // ) {
              //   //
              // } else {
              //   newOptions.push({
              //     label: foundTargetObject.label + "." + requiredField.label,
              //     value:
              //       "$" + foundTargetObject.value + "." + requiredField.value,
              //   });

              //   tempRequiredFields[idx] = {
              //     ...requiredField,
              //     options: newOptions,
              //   };
              // }
            }
            break;

          case "number":
            tempRequiredFields[idx] = {
              ...requiredField,
              options: fieldsMain.filter(
                (element) => element.type === "number"
              ),
            };
            break;

          case "email":
            tempRequiredFields[idx] = {
              ...requiredField,
              options: fieldsMain.filter((element) => element.type === "email"),
            };
            break;

          case "file":
            tempRequiredFields[idx] = {
              ...requiredField,
              options: fieldsMain.filter((element) => element.type === "file"),
            };
            break;

          case "date":
            tempRequiredFields[idx] = {
              ...requiredField,
              options: fieldsMain.filter((element) => element.type === "date"),
            };
            break;

          case "datetime-local":
            tempRequiredFields[idx] = {
              ...requiredField,
              options: fieldsMain.filter(
                (element) => element.type === "datetime-local"
              ),
            };
            break;

          case "user":
            let newOptions = fieldsMain
              .filter((element) => element.type === "user")
              .concat(listUser);

            listFieldsMain.forEach((element) => {
              if (
                Object.values(element)[0].readable ||
                Object.values(element)[0].writeable
              ) {
                if (Object.keys(element)[0] === "main_object") {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value:
                      "$" +
                      Object.values(element)[0].sections[0].object_id +
                      ".owner",
                  });
                } else {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value: "$" + Object.keys(element)[0] + ".owner",
                  });
                }
              }
            });

            newOptions = newOptions.filter(
              (value, index, self) =>
                index === self.findIndex((t) => t.value === value.value)
            );

            if (isDynamicButton) {
              newOptions.push({
                label: "Triggered User",
                value: "$triggered_user",
              });
            }

            tempRequiredFields[idx] = {
              ...requiredField,
              options: newOptions,
            };
            break;

          case "lookup":
            let optionsLookup = [];

            if (foundTargetObject) {
              optionsLookup.push({
                label: foundTargetObject.label + "." + requiredField.label,
                value:
                  "$" + foundTargetObject.value + "." + requiredField.value,
              });
            }

            // optionsLookup.push({
            //   label: "Select record",
            //   value: "select_record",
            // });

            tempRequiredFields[idx] = {
              ...requiredField,
              options: optionsLookup,
            };
            break;

          // case "linkingobject":
          //   tempRequiredFields[idx] = {
          //     ...requiredField,
          //     options: [
          //       {
          //         label: requiredField.related_name,
          //         value: "$" + objectID + "." + requiredField.value,
          //       },
          //     ],
          //   };
          // if (objectID === form.getFieldValue("object")) {
          //   tempRequiredFields[idx] = {
          //     ...requiredField,
          //     options: [
          //       {
          //         label: requiredField.related_name,
          //         value: "$" + objectID + "." + requiredField.value,
          //       },
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // } else {
          //   tempRequiredFields[idx] = {
          //     ...requiredField,
          //     options: [
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // }
          // break;

          default:
            break;
        }
      });

      tempOptionalFields.forEach((optionalField, idx) => {
        switch (optionalField.type) {
          case "select":
            if (foundTargetObject) {
              let newOptions = [...optionalField.options];
              if (foundTargetObject.value === objectID) {
                newOptions.push({
                  label: foundTargetObject.label + "." + optionalField.label,
                  value:
                    "$" + foundTargetObject.value + "." + optionalField.value,
                });
              } else {
                if (!isDynamicButton) {
                  listFieldsMain.forEach((element) => {
                    if (
                      Object.values(element)[0].readable ||
                      Object.values(element)[0].writeable
                    ) {
                      if (Object.keys(element)[0] === "main_object") {
                        Object.values(element)[0].sections.forEach((main) => {
                          main.fields.forEach((fld) => {
                            if (
                              fld.hidden === false &&
                              fld.permission_hidden === false &&
                              !_.get(fld, "encrypted", false) &&
                              fld.type === "select"
                            ) {
                              newOptions.push({
                                label:
                                  Object.values(element)[0].object_name +
                                  "." +
                                  fld.name,
                                value: "$" + objectID + "." + fld.ID,
                              });
                            }
                          });
                        });
                      }
                    }
                  });
                }
              }
              tempOptionalFields[idx] = {
                ...optionalField,
                options: newOptions,
              };
            }
            break;

          case "dynamic-field":
            if (foundTargetObject && foundTargetObject.value === objectID) {
              let newOptions = [...optionalField.options];

              newOptions.push({
                label: foundTargetObject.label + "." + optionalField.label,
                value:
                  "$" + foundTargetObject.value + "." + optionalField.value,
              });

              tempOptionalFields[idx] = {
                ...optionalField,
                options: newOptions,
              };
            }
            break;

          case "number":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter(
                (element) => element.type === "number"
              ),
            };
            break;

          case "email":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "email"),
            };
            break;

          case "file":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "file"),
            };
            break;

          case "date":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "date"),
            };
            break;

          case "datetime-local":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter(
                (element) => element.type === "datetime-local"
              ),
            };
            break;

          case "user":
            let newOptions = fieldsMain
              .filter((element) => element.type === "user")
              .concat(listUser);

            listFieldsMain.forEach((element) => {
              if (
                Object.values(element)[0].readable ||
                Object.values(element)[0].writeable
              ) {
                if (Object.keys(element)[0] === "main_object") {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value:
                      "$" +
                      Object.values(element)[0].sections[0].object_id +
                      ".owner",
                  });
                } else {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value: "$" + Object.keys(element)[0] + ".owner",
                  });
                }
              }
            });

            newOptions = newOptions.filter(
              (value, index, self) =>
                index === self.findIndex((t) => t.value === value.value)
            );

            if (isDynamicButton) {
              newOptions.push({
                label: "Triggered User",
                value: "$triggered_user",
              });
            }

            tempOptionalFields[idx] = {
              ...optionalField,
              options: newOptions,
            };
            break;

          case "lookup":
            let optionsLookup = [];

            if (foundTargetObject) {
              optionsLookup.push({
                label: foundTargetObject.label + "." + optionalField.label,
                value:
                  "$" + foundTargetObject.value + "." + optionalField.value,
              });
            }

            // optionsLookup.push({
            //   label: "Select record",
            //   value: "select_record",
            // });

            tempOptionalFields[idx] = {
              ...optionalField,
              options: optionsLookup,
            };
            break;

          // case "linkingobject":
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: optionalField.related_name,
          //         value: "$" + objectID + "." + optionalField.value,
          //       },
          //     ],
          //   };
          // if (objectID === form.getFieldValue("object")) {
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: optionalField.related_name,
          //         value: "$" + objectID + "." + optionalField.value,
          //       },
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // } else {
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // }
          // break;

          default:
            break;
        }
      });

      if (isEdit && showModalCreate && Object.keys(detailsAction).length > 0) {
        tempRequiredFields.forEach((item) => {
          let temp = detailsAction.field_value.find(
            (ele) => ele.ID === item.value
          );
          if (temp) {
            form.setFieldsValue({
              [temp.ID]:
                temp.type === "number" || temp.type === "email"
                  ? [temp.value]
                  : temp.value,
            });
          }
        });

        setFormValue(form.getFieldsValue());
      }

      listFields.forEach((item) => {
        if (Object.keys(item)[0] !== "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            const resultRequired = ele.fields.filter((item1) =>
              tempRequiredFields.some((item2) => item1.field_id === item2.field)
            );

            if (resultRequired.length > 0) {
              tempRequiredFields.forEach((item, idx) => {
                if (
                  item.field === resultRequired[0].field_id &&
                  item.type !== "lookup"
                ) {
                  tempRequiredFields[idx] = {
                    ...tempRequiredFields[idx],
                    options: [
                      ...tempRequiredFields[idx].options,
                      {
                        label: resultRequired[0].related_name,
                        value:
                          "$" + resultRequired[0].object_id + "." + item.field,
                      },
                      {
                        label:
                          foundTargetObject.label +
                          "." +
                          tempRequiredFields[idx].label,
                        value:
                          "$" +
                          foundTargetObject.value +
                          "." +
                          tempRequiredFields[idx].value,
                      },
                      {
                        label: "Get Record by Current Assign To",
                        value: "@OWNER",
                      },
                      {
                        label: "Get Record by Current Report To",
                        value: "@OWNER_REPORT_TO",
                      },
                      {
                        label: "Select record",
                        value: "select_record",
                      },
                    ],
                  };
                }
              });
            }

            const resultOptional = ele.fields.filter((item1) =>
              tempOptionalFields.some((item2) => item1.field_id === item2.field)
            );

            if (resultOptional.length > 0) {
              tempOptionalFields.forEach((item, idx) => {
                if (
                  item.field === resultOptional[0].field_id &&
                  item.type !== "lookup"
                ) {
                  tempOptionalFields[idx] = {
                    ...tempOptionalFields[idx],
                    options: [
                      ...tempOptionalFields[idx].options,
                      {
                        label: resultOptional[0].related_name,
                        value:
                          "$" + resultOptional[0].object_id + "." + item.field,
                      },
                      {
                        label:
                          foundTargetObject.label +
                          "." +
                          tempOptionalFields[idx].label,
                        value:
                          "$" +
                          foundTargetObject.value +
                          "." +
                          tempOptionalFields[idx].value,
                      },
                      {
                        label: "Get Record by Current Assign To",
                        value: "@OWNER",
                      },
                      {
                        label: "Get Record by Current Report To",
                        value: "@OWNER_REPORT_TO",
                      },
                      {
                        label: "Select record",
                        value: "select_record",
                      },
                    ],
                  };
                }
              });
            }
          });
        }
      });

      dispatch(updateRequiredFieldsCreate(tempRequiredFields));
      dispatch(updateOptionalFieldsCreate(tempOptionalFields));
    }
  }, [
    dispatch,
    form,
    listFields,
    listObjects,
    objectID,
    fieldsMain,
    listUser,
    listFieldsMain,
    detailsAction,
    isEdit,
    showModalCreate,
    isDynamicButton,
  ]);

  useEffect(() => {
    if (Object.keys(objects).length > 0) {
      let tempObjects = [];
      Object.entries(objects).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [objects]);

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    if (showModalCreate && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        name: detailsAction.action_name,
        description: detailsAction.description,
        object: detailsAction.object_create_record,
      });

      handleSelectObject(detailsAction.object_create_record);

      setRequiredFieldsValue(
        detailsAction.fields_values.filter((item) => item.required)
      );

      setOptionalFieldsSelect(
        detailsAction.fields_values.filter((item) => item.required === false)
      );
    }
  }, [
    dispatch,
    showModalCreate,
    form,
    isEdit,
    detailsAction,
    handleSelectObject,
  ]);

  useEffect(() => {
    if (
      showModalCreate &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalCreate(false);
      form.resetFields();
      setIsEdit(false);
      setOptionalFieldsSelect([]);
      setRequiredFieldsValue([]);
      setFormValue({});
      dispatch(updateRequiredFieldsCreate([]));
      dispatch(updateOptionalFieldsCreate([]));
      dispatch(loadFieldsObjectFail());
      dispatch(loadTargetPicklistFail());
      dispatch(loadFieldsObjectSuccess([]));

      if (isDynamicButton) {
        dispatch(loadDetailsActionDynamicButtonSuccess({}));
      } else {
        dispatch(loadDetailsActionFail());
      }
    }
  }, [
    dispatch,
    form,
    isDynamicButton,
    setIsEdit,
    setShowModalCreate,
    showModalCreate,
    status,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalCreate}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
          onValuesChange={(value, values) => {
            setFormValue(values);
          }}
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item
            label={t("workflow.selectObject")}
            name="object"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("workflow.selectObject")}
              disabled={isEdit}
              options={listObjects}
              onChange={handleSelectObject}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          {requiredFieldsCreate.length > 0 && (
            <>
              <Note>{t("workflow.requiredFields")}</Note>
              {requiredFieldsCreate.map((item, idx) => {
                return handleFieldType(item, requiredFieldsCreate, true, idx);
              })}
            </>
          )}

          {optionalFieldsCreate.length > 0 && (
            <Note>{t("workflow.optionalFields")}</Note>
          )}

          {optionalFieldsCreate.length > 0 &&
            optionalFieldsSelect.length > 0 && (
              <>
                {optionalFieldsSelect.map((item, idx) => {
                  return (
                    <FormValueOptional
                      key={idx}
                      labelCol={{ span: 8 }}
                      wrapperCol={{ span: 16 }}
                      // name={item.value + idx}
                      label={
                        <FormCustom
                          name={item.field_id + idx}
                          wrapperCol={{ span: 24 }}
                        >
                          <Select
                            placeholder={t("common.placeholderSelect")}
                            onChange={(e, option) => {
                              form.setFieldsValue({
                                [item.field_id]: undefined,
                              });

                              handleChangeField(e, option, idx, "field");
                            }}
                            showSearch
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                          >
                            {optionalFieldsCreate.map((ele) => {
                              return (
                                <Select.Option
                                  disabled={
                                    optionalFieldsSelect.find(
                                      (field) => field.field_id === ele.value
                                    )
                                      ? true
                                      : false
                                  }
                                  key={ele.value}
                                >
                                  {ele.label}
                                </Select.Option>
                              );
                            })}
                          </Select>
                        </FormCustom>
                      }
                    >
                      {item.field_id !== "" ? (
                        <>
                          {handleFieldType(
                            optionalFieldsCreate.find(
                              (ele) => ele.value === item.field_id
                            ),
                            optionalFieldsCreate,
                            false,
                            idx
                          )}
                        </>
                      ) : (
                        <Input disabled />
                      )}

                      <Delete>
                        <Tooltip title="Delete">
                          <img
                            src={DeleteIcon}
                            onClick={() => onDeleteField(idx)}
                            alt="delete"
                          />
                        </Tooltip>
                      </Delete>
                    </FormValueOptional>
                  );
                })}
              </>
            )}

          {optionalFieldsCreate.length > 0 && (
            <AddFields onClick={() => _onAddField()}>
              {/* <img src={PlusGreen} alt="plus" /> */}
              <span>+ {t("workflow.addField")}</span>
            </AddFields>
          )}

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalCreateRecord);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    width: 100%;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .requiredMark {
    .ant-form-item-label > label {
      ::before {
        display: none;
      }
    }
  }
`;

const Note = styled.div`
  margin-bottom: 16px;
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const FormValueOptional = styled(Form.Item)`
  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }
`;

const FormCustom = styled(Form.Item)`
  margin-bottom: 0;
  flex: 1;
  overflow: hidden;
`;
