import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useTranslation, withTranslation } from "react-i18next";

import Input from "antd/lib/input";
import SelectAntd from "antd/lib/select";
import Form from "antd/lib/form";
import DatePicker from "antd/lib/date-picker";
import InputNumber from "antd/lib/input-number";
import ClearOutlined from "@ant-design/icons/ClearOutlined";

const Date = ({
  form,
  field,
  listValues,
  required,
  handleChangeField,
  handleChangeFieldRequired,
  idx,
}) => {
  const { t } = useTranslation();

  const [type, setType] = useState("");

  const optionsDate = [
    {
      label: "Today",
      value: "today",
    },
    {
      label: "Select Day",
      value: "select",
    },
    {
      label: "Today + n days",
      value: "$AddDatesToCurrentDate",
    },
    {
      label: "Today - n days",
      value: "$SubtractDatesToCurrentDate",
    },
  ];

  const handleChange = (value) => {
    if (
      value === "select" ||
      value === "$AddDatesToCurrentDate" ||
      value === "$SubtractDatesToCurrentDate"
    ) {
      form.setFieldsValue({
        [field.value]: undefined,
      });
    }
    setType(value);
  };

  useEffect(() => {
    if (listValues.length > 0 && Object.keys(field).length > 0) {
      listValues.forEach((currentItem) => {
        if (field.value === currentItem.field_id && currentItem.isDate) {
          setType("select");
        }
      });
    }
  }, [listValues, field]);

  return (
    <Wrapper>
      <FormCustom
        required={required}
        label={required ? <Input defaultValue={field.label} disabled /> : ""}
        name={field.value}
        rules={[
          {
            required: true,
            message: `${t("common.placeholderInputSelect")} ${field.label}!`,
          },
        ]}
        className="requiredMark"
      >
        {type === "select" ? (
          <DatePicker
            showTime={field.type === "datetime-local"}
            onChange={(date, dateString) => {
              required
                ? handleChangeFieldRequired(field.value, dateString, idx)
                : handleChangeField(dateString, [], idx, "value", true);
            }}
          />
        ) : type === "$AddDatesToCurrentDate" ||
          type === "$SubtractDatesToCurrentDate" ? (
          <InputNumber
            min={type === "$AddDatesToCurrentDate" && 1}
            max={type === "$SubtractDatesToCurrentDate" && -1}
            placeholder={t("common.placeholderInput")}
            onChange={(e) => {
              required
                ? handleChangeFieldRequired(field.value, e, idx, false, type)
                : handleChangeField(
                    e,
                    [],
                    idx,
                    "value",
                    false,
                    null,
                    null,
                    null,
                    type
                  );
            }}
            addonBefore={
              <ClearOutlined
                onClick={() => {
                  form.setFieldsValue({
                    [field.value]: undefined,
                  });
                  setType("");
                }}
              />
            }
          />
        ) : (
          <SelectAntd
            placeholder={t("common.placeholderSelect")}
            options={field.options.concat(optionsDate)}
            onChange={(e) => {
              handleChange(e);

              if (
                e !== "select" &&
                e !== "$AddDatesToCurrentDate" &&
                e !== "$SubtractDatesToCurrentDate"
              ) {
                required
                  ? handleChangeFieldRequired(field.value, e, idx)
                  : handleChangeField(e, [], idx, "value");
              }
            }}
          />
        )}
      </FormCustom>

      {type === "select" && (
        <Clear
          onClick={() => {
            form.setFieldsValue({
              [field.value]: undefined,
            });
            setType("");
          }}
        >
          Clear
        </Clear>
      )}
    </Wrapper>
  );
};

export default withTranslation()(Date);

const Wrapper = styled.div`
  display: flex;
  position: relative;
  width: 100%;
  overflow: hidden;

  .ant-picker {
    width: 85%;
  }

  .ant-input-number-group-wrapper {
    width: 100%;
  }

  .requiredMark {
    flex: 1;
  }
`;

const Clear = styled.span`
  position: absolute;
  top: 5px;
  right: 0;
  cursor: pointer;
`;

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
`;
