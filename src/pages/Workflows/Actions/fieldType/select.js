import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Input from "antd/lib/input";
import SelectAntd from "antd/lib/select";
import Form from "antd/lib/form";

import { loadValuePicklist, loadTargetPicklist } from "redux/slices/workflows";

const Select = ({
  field,
  optionalFields,
  required,
  form,
  formValue,
  setFormValue,
  handleChangeFieldRequired,
  handleChangeField,
  idx,
  objectID,
  isEdit,
  listObjects,
  updateRecord,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { valuePicklist, targetPicklist } = useSelector(
    (state) => state.workflowsReducer
  );
  const { Option } = SelectAntd;

  const [newValuePicklist, setNewValuePicklist] = useState({});

  useEffect(() => {
    let newObj = { ...newValuePicklist, ...valuePicklist };
    Object.entries(valuePicklist).forEach(([key, val]) => {
      Object.entries(val).forEach(([keyVal, valVal]) => {
        if (keyVal === "related" && !valVal && field.value === key) {
          let foundTargetObject = undefined;
          if (updateRecord) {
            foundTargetObject = listObjects.find(
              (object) => object.value === objectID
            );
          } else {
            foundTargetObject = listObjects.find(
              (object) => object.value === form.getFieldValue("object")
            );
          }

          if (foundTargetObject && foundTargetObject.value === objectID) {
            newObj = {
              [key]: {
                ...val,
                [foundTargetObject.label + "." + field.label]:
                  "$" + foundTargetObject.value + "." + field.value,
              },
            };
          }
        }
      });
    });

    let result = {};
    let temp = {};
    Object.entries(newObj).forEach(([key, val]) => {
      Object.entries(val).forEach(([keyVal, valVal]) => {
        if (valVal) {
          temp[keyVal] = valVal;
        }
      });
      result[key] = { ...temp };
    });

    setNewValuePicklist(result);
    /* eslint-disable-next-line */
  }, [valuePicklist]);

  useEffect(() => {
    let value = form.getFieldsValue();
    /* eslint-disable-next-line */
    targetPicklist.map((item, idx) => {
      form.setFieldsValue({ value, [item]: undefined });
    });
    setFormValue(form.getFieldsValue());
    /* eslint-disable-next-line */
  }, [targetPicklist]);

  useEffect(() => {
    if (isEdit) {
      if (
        (field.source === null || field.source === undefined) &&
        field.target &&
        field.target.length > 0
      ) {
        field.target.forEach((item, idx) => {
          dispatch(
            loadValuePicklist({
              data: {
                Object_ID: form.getFieldValue("object") || objectID,
                Source_ID: field.value,
                Target_ID: item,
                Value: form.getFieldValue(field.value),
              },
              ID: item,
              listValue: { ...valuePicklist },
            })
          );
        });
      } else if (field.source && field.target && field.target.length > 0) {
        field.target.forEach((item, idx) => {
          dispatch(
            loadValuePicklist({
              data: {
                Object_ID: form.getFieldValue("object") || objectID,
                Source_ID: field.value,
                Target_ID: item,
                Value: form.getFieldValue(field.value),
              },
              ID: item,
              listValue: { ...valuePicklist },
            })
          );
        });
      }
      // else if (field.source && field.target && field.target.length === 0) {
      // }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [field, isEdit]);

  return (
    <>
      {(field.source === null || field.source === undefined) &&
      field.target &&
      field.target.length > 0 ? (
        <FormCustom
          required={required}
          label={required ? <Input defaultValue={field.label} disabled /> : ""}
          name={field.value}
          rules={[
            {
              required: true,
              message: `${t("common.placeholderSelect")} ${field.label}!`,
            },
          ]}
          className="requiredMark"
        >
          <SelectAntd
            placeholder={t("common.placeholderSelect")}
            onChange={(e, option) => {
              /* eslint-disable-next-line */
              field.target.map((item, idx) => {
                dispatch(
                  loadValuePicklist({
                    data: {
                      Object_ID: form.getFieldValue("object") || objectID,
                      Source_ID: field.value,
                      Target_ID: item,
                      Value: e,
                    },
                    ID: item,
                    listValue: valuePicklist,
                  })
                );
              });

              dispatch(
                loadTargetPicklist({
                  field_id: field.value,
                })
              );

              required
                ? handleChangeFieldRequired(field.value, e, idx)
                : handleChangeField(e, option, idx, "value");
            }}
            // onClear={() => {
            //   let tempFormValue = { ...formValue };
            //   tempFormValue[field.ID] = null;
            //   /* eslint-disable-next-line */
            //   field.target.map((item, idx) => {
            //     tempFormValue[item] = null;
            //   });

            //   setFormValue(tempFormValue);
            //   form.setFieldsValue(tempFormValue);

            //   dispatch(
            //     loadTarget({
            //       field_id: field.ID,
            //     })
            //   );
            // }}
          >
            {field.options.map((item, idx) => {
              return (
                <Option value={item.value} key={idx}>
                  {item.label}
                </Option>
              );
            })}
          </SelectAntd>
        </FormCustom>
      ) : field.source && field.target && field.target.length > 0 ? (
        <FormCustom
          required={required}
          label={required ? <Input defaultValue={field.label} disabled /> : ""}
          name={field.value}
          rules={[
            {
              required: true,
              message: `${t("common.placeholderSelect")} ${field.label}!`,
            },
          ]}
          className="requiredMark"
        >
          <SelectAntd
            disabled={formValue[field.source] ? false : true}
            placeholder={t("common.placeholderSelect")}
            onChange={(e, option) => {
              /* eslint-disable-next-line */
              field.target.map((item, idx) => {
                dispatch(
                  loadValuePicklist({
                    data: {
                      Object_ID: form.getFieldValue("object") || objectID,
                      Source_ID: field.value,
                      Target_ID: item,
                      Value: e,
                    },
                    ID: item,
                    listValue: valuePicklist,
                  })
                );
              });
              dispatch(
                loadTargetPicklist({
                  field_id: field.value,
                })
              );

              required
                ? handleChangeFieldRequired(field.value, e, idx)
                : handleChangeField(e, option, idx, "value");
            }}
          >
            {newValuePicklist && newValuePicklist[field.value]
              ? Object.entries(
                  newValuePicklist && newValuePicklist[field.value]
                ).map(([key, value], idx) => {
                  return (
                    <Option value={value === true ? key : value} key={idx}>
                      {key}
                    </Option>
                  );
                })
              : field.options.map((item, idx) => {
                  return (
                    <Option value={item.value} key={idx}>
                      {item.label}
                    </Option>
                  );
                })}
          </SelectAntd>
        </FormCustom>
      ) : field.source && field.target && field.target.length === 0 ? (
        <FormCustom
          required={required}
          label={required ? <Input defaultValue={field.label} disabled /> : ""}
          name={field.value}
          rules={[
            {
              required: true,
              message: `${t("common.placeholderSelect")} ${field.label}!`,
            },
          ]}
          className="requiredMark"
        >
          <SelectAntd
            placeholder={t("common.placeholderSelect")}
            disabled={formValue[field.source] ? false : true}
            onChange={(e, option) => {
              required
                ? handleChangeFieldRequired(field.value, e, idx)
                : handleChangeField(e, option, idx, "value");
            }}
          >
            {newValuePicklist && newValuePicklist[field.value]
              ? Object.entries(
                  newValuePicklist && newValuePicklist[field.value]
                ).map(([key, value], idx) => {
                  return (
                    <Option value={value === true ? key : value} key={idx}>
                      {key}
                    </Option>
                  );
                })
              : field.options.map((item, idx) => {
                  return (
                    <Option value={item.value} key={idx}>
                      {item.label}
                    </Option>
                  );
                })}
          </SelectAntd>
        </FormCustom>
      ) : (
        <FormCustom
          required={required}
          label={required ? <Input defaultValue={field.label} disabled /> : ""}
          name={field.value}
          rules={[
            {
              required: true,
              message: `${t("common.placeholderSelect")} ${field.label}!`,
            },
          ]}
          className="requiredMark"
        >
          <SelectAntd
            placeholder={t("common.placeholderSelect")}
            options={field.options}
            onChange={(e, option) => {
              required
                ? handleChangeFieldRequired(field.value, e, idx)
                : handleChangeField(e, option, idx, "value");
            }}
          />
        </FormCustom>
      )}
    </>
  );
};

export default withTranslation()(Select);

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
`;
