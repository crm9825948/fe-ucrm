import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import Axios from "axios";
import { useTranslation, withTranslation } from "react-i18next";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Form from "antd/lib/form";
import SearchOutlined from "@ant-design/icons/SearchOutlined";
import ClearOutlined from "@ant-design/icons/ClearOutlined";

import LinkingList from "pages/Objects/modal/fieldsType/linking/linkingList";

import {
  loadDataLinking,
  loadHeaderLinking,
  loadPaginationLinking,
} from "redux/slices/objects";
import { BASE_URL_API } from "constants/constants";

function LinkingObject({
  field,
  optionalFields,
  fieldsMain,
  required,
  // objectID,
  objectCreate,
  form,
  handleChangeField,
  handleChangeFieldRequired,
  idx,
  linkingList,
  setLinkingList,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { recordData } = useSelector((state) => state.objectsReducer);

  const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [linkingFieldValue, setLinkingFieldValue] = useState({});
  const [rowData, setRowData] = useState({});

  const handleSelectLinking = async (selectedRowKeys) => {
    const isTokenValid = await checkTokenExpiration();
    Axios.post(
      BASE_URL_API + "object/load-linking-field-value",
      {
        object_related: field.objectname,
        record_id: selectedRowKeys[0],
        object_create_record: objectCreate,
      },
      {
        headers: {
          Authorization: isTokenValid,
        },
      }
    )
      .then((res) => {
        setLinkingFieldValue(res.data.data);
      })
      .catch(() => {});

    // dispatch(
    //   loadLinkingFieldValue({
    //     object_related: field.objectname,
    //     record_id: selectedRowKeys[0],
    //     object_create_record: objectID,
    //   })
    // );

    setVisible(false);
  };

  const handleSelectField = (e) => {
    if (e === "select_record") {
      setVisible(true);
    }
  };

  const _onCancelModal = () => {
    form.setFieldsValue({
      [field.value]: undefined,
    });
  };

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadHeaderLinking({
          object_id: field.objectname,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: [],
        })
      );
    }
  }, [dispatch, field, visible]);

  useEffect(() => {
    if (Object.keys(linkingFieldValue).length > 0) {
      Object.keys(linkingFieldValue).forEach((item) => {
        if (item === field.value) {
          form.setFieldsValue({
            [field.value]: linkingFieldValue[field.value].value,
          });
          let arr = { ...linkingList };
          arr[field.value] = linkingFieldValue[field.value];
          setLinkingList(arr);
          if (required) {
            handleChangeFieldRequired(
              field.value,
              linkingFieldValue[field.value]?.value,
              idx
            );
          } else {
            handleChangeField(
              linkingFieldValue[field.value]?.value,
              [],
              idx,
              "value",
              false,
              linkingFieldValue[field.value]?.id_field_related_record,
              linkingFieldValue[field.value]?.object_related,
              linkingFieldValue[field.value]?.id_related_record
            );
          }
        }
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    field.value,
    form,
    // handleChangeField,
    // handleChangeFieldRequired,
    idx,
    linkingFieldValue,
    required,
  ]);

  return (
    <>
      <FormCustom
        required={required}
        label={required ? <Input defaultValue={field.label} disabled /> : ""}
        name={field.value}
        rules={[
          {
            required: true,
            message: `${t("common.placeholderSelect")} ${field.label}!`,
          },
        ]}
        className="requiredMark"
      >
        {field.options.length > 1 &&
        Object.keys(linkingFieldValue).length === 0 ? (
          <Select
            placeholder={t("common.placeholderSelect")}
            options={field.options}
            onSelect={(e) => {
              handleSelectField(e);
              if (e !== "select_record") {
                required
                  ? handleChangeFieldRequired(field.value, e, idx)
                  : handleChangeField(e, [], idx, "value");
              }
            }}
          />
        ) : (
          <Input
            placeholder={`${t("common.placeholderSelect")} ${field.label}!`}
            disabled
            addonBefore={
              linkingFieldValue &&
              linkingFieldValue[field.value] &&
              linkingFieldValue[field.value].value && (
                <ClearOutlined
                  onClick={() => {
                    setLinkingFieldValue({});
                    form.setFieldsValue({
                      [field.value]: undefined,
                    });
                  }}
                />
              )
            }
            addonAfter={
              <>
                <SearchOutlined
                  onClick={() => {
                    setVisible(true);
                  }}
                />
              </>
            }
          />
        )}
      </FormCustom>

      {visible && (
        <LinkingList
          setVisible={setVisible}
          visible={visible}
          field={field}
          selectedRowKeys={selectedRowKeys}
          setSelectedRowKeys={setSelectedRowKeys}
          handleSelect={handleSelectLinking}
          dataFilterLinking={{}}
          onCancelModal={_onCancelModal}
          recordData={recordData}
          rowData={rowData}
          setRowData={setRowData}
        />
      )}
    </>
  );
}

export default withTranslation()(LinkingObject);

LinkingObject.defaultProps = {
  linkingList: {},
  setLinkingList: () => {},
};

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
`;
