import { useEffect, useState } from "react";
import isEmail from "validator/lib/isEmail";
import styled from "styled-components/macro";
import { useTranslation, withTranslation } from "react-i18next";

import Input from "antd/lib/input";
import SelectAntd from "antd/lib/select";
import Form from "antd/lib/form";

const Email = ({
  field,
  optionalFields,
  required,
  handleChangeField,
  handleChangeFieldRequired,
  idx,
}) => {
  const { t } = useTranslation();

  const [listValid, setListValid] = useState([]);

  useEffect(() => {
    if (field.options.length > 0) {
      let tempValid = [];

      field.options.map((item) => {
        return tempValid.push(item.value);
      });
      setListValid(tempValid);
    }
  }, [field.options]);

  return (
    <FormCustom
      required={required}
      label={required ? <Input defaultValue={field.label} disabled /> : ""}
      name={field.value}
      rules={[
        {
          required: true,
          message: `${t("common.placeholderInputSelect")} ${field.label}!`,
        },
        () => ({
          validator(_, value) {
            if (value && value.length > 0) {
              if (!listValid.includes(value[0])) {
                if (isEmail(value[0]) === false) {
                  return Promise.reject(new Error(t("workflow.invalidEmail")));
                }
              }
            }
            return Promise.resolve();
          },
        }),
      ]}
      className="requiredMark"
    >
      <SelectAntd
        mode="tags"
        placeholder={t("common.placeholderInputSelect")}
        options={field.options}
        onChange={(value) => {
          if (value?.length > 1) {
            value.shift();
          }
          required
            ? handleChangeFieldRequired(field.value, value[0], idx)
            : handleChangeField(value[0], [], idx, "value");
        }}
      />
    </FormCustom>
  );
};

export default withTranslation()(Email);

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
`;
