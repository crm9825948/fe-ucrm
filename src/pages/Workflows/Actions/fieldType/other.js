import styled from "styled-components/macro";
import { useTranslation, withTranslation } from "react-i18next";

import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Form from "antd/lib/form";

import { optionsCreateRecordCall } from "util/staticData";

const Other = ({
  field,
  optionalFields,
  fieldsMain,
  required,
  handleChangeField,
  handleChangeFieldRequired,
  idx,
  trigger,
  isUseFieldCall,
  fieldsID,
}) => {
  const { t } = useTranslation();

  return (
    <FormCustom
      required={required}
      label={required ? <Input defaultValue={field.label} disabled /> : ""}
      name={field.value}
      rules={[
        {
          required: true,
          message: `${t("common.placeholderInputSelect")} ${field.label}!`,
        },
      ]}
      className="requiredMark"
    >
      <Select
        mode="tags"
        placeholder={t("common.placeholderInputSelect")}
        options={
          trigger === 6 && isUseFieldCall
            ? fieldsMain.concat(optionsCreateRecordCall).concat(fieldsID)
            : fieldsMain.concat(fieldsID)
        }
        onChange={(value, option) => {
          if (value?.length > 1) {
            value.shift();
          }

          required
            ? handleChangeFieldRequired(field.value, value[0], idx)
            : handleChangeField(value[0], option, idx, "value");
        }}
      />
    </FormCustom>
  );
};

export default withTranslation()(Other);

Other.defaultProps = {
  fieldsID: [],
};

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
`;
