import styled from "styled-components/macro";
import Input from "antd/lib/input";
import SelectAntd from "antd/lib/select";
import Form from "antd/lib/form";
import { useEffect, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import InputNumber from "antd/lib/input-number";
import ClearOutlined from "@ant-design/icons/ClearOutlined";
const optionsNumber = [
  {
    label: "Number + n",
    value: "$AddNumberToCurrentNumber",
  },
  {
    label: "Number - n",
    value: "$SubtractNumberToCurrentNumber",
  },
];
const Select = ({
  form,
  operator,
  field,
  optionalFields,
  required,
  handleChangeField,
  handleChangeFieldRequired,
  idx,
  isUpdate,
}) => {
  const { t } = useTranslation();

  const [listValid, setListValid] = useState([]);
  const [type, setType] = useState("");
  useEffect(() => {
    setType(operator);
  }, [operator]);
  useEffect(() => {
    if (field.options.length > 0) {
      let tempValid = [];

      field.options.map((item) => {
        return tempValid.push(item.value);
      });
      setListValid(tempValid);
    }
  }, [field.options]);

  return (
    <FormCustom
      required={required}
      label={required ? <Input defaultValue={field.label} disabled /> : ""}
      name={field.value}
      rules={[
        {
          required: true,
          message: `${t("common.placeholderInputSelect")} ${field.label}!`,
        },
        () => ({
          validator(_, value) {
            if (value && value.length > 0) {
              if (!listValid.includes(value[0])) {
                if (isNaN(Number(value[0]))) {
                  return Promise.reject(new Error(t("workflow.invalidNumber")));
                }
              }
            }
            return Promise.resolve();
          },
        }),
      ]}
      className="requiredMark"
    >
      {type === "$AddNumberToCurrentNumber" ||
      type === "$SubtractNumberToCurrentNumber" ? (
        <InputNumber
          min={type === "$AddNumberToCurrentNumber" && 1}
          max={type === "$SubtractNumberToCurrentNumber" && -1}
          placeholder={t("common.placeholderInput")}
          onChange={(e) => {
            required
              ? handleChangeFieldRequired(field.value, e, idx, false, type)
              : handleChangeField(
                  e,
                  [],
                  idx,
                  "value",
                  false,
                  null,
                  null,
                  null,
                  type
                );
          }}
          addonBefore={
            <ClearOutlined
              onClick={() => {
                form.setFieldsValue({
                  [field.value]: undefined,
                });
                setType("");
              }}
            />
          }
        />
      ) : (
        <SelectAntd
          mode="tags"
          options={
            isUpdate ? field.options.concat(optionsNumber) : field.options
          }
          placeholder={t("common.placeholderInputSelect")}
          onChange={(value, option) => {
            if (value?.length > 1) {
              value.shift();
            }
            if (
              value[0] === "$AddNumberToCurrentNumber" ||
              value[0] === "$SubtractNumberToCurrentNumber"
            ) {
              form.setFieldsValue({
                [field.value]: undefined,
              });
            } else {
              if (value?.length > 0) {
                required
                  ? handleChangeFieldRequired(
                      field.value,
                      value[0].includes("$") ? value[0] : parseInt(value[0]),
                      idx
                    )
                  : handleChangeField(
                      value[0].includes("$") ? value[0] : parseInt(value[0]),
                      option,
                      idx,
                      "value"
                    );
              }
            }
            setType(value[0]);
          }}
        />
      )}
    </FormCustom>
  );
};

export default withTranslation()(Select);

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: ${({ required }) => (required ? "24px" : "0")};
  .ant-input-number-group-wrapper {
    width: 100%;
  }
`;
