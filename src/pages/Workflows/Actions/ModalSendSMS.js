import { useState, useEffect, useRef } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";

import Editor from "components/Editor/Editor2";

import { generalInfo } from "util/staticData";

import {
  getSMSTemplateById,
  getSMSTemplateByIdSuccess,
} from "redux/slices/smsTemplate";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
} from "redux/slices/workflows";

import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

function ModalSendSMS({
  showModalSMS,
  setShowModalSMS,
  isEdit,
  setIsEdit,
  objectID,
  loading,
  detailsAction,
  status,
  isDynamicButton,
  details,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { listSMSTemplate, editSMSTemplate } = useSelector(
    (state) => state.smsTemplateReducer
  );

  const editorJodit = useRef(null);
  const [form] = Form.useForm();

  const [listFields, setListFields] = useState([]);
  const [selectedFields, setSelectedFields] = useState([]);
  const [selectedGeneralFields, setSelectedGeneralFields] = useState([]);
  const [listTemplate, setListTemplate] = useState([]);

  const [body, setBody] = useState("");
  const [optionsAppend, setOptionsAppend] = useState([]);

  const optionUnicode = [
    {
      label: "0",
      value: 0,
    },
    {
      label: "1",
      value: 1,
    },
  ];

  const _onSubmit = (values) => {
    let data = {
      action_name: values.name,
      action_type: 8,
      content: editorJodit.current.value,
      description: values.description,
      object_id: objectID,
      selected_fields: values.fields,
      selected_general_fields: values.general_fields,
      unicode: values.unicode,
    };

    if (isEdit) {
      if (isDynamicButton) {
        dispatch(
          updateActionDynamicButton({
            ...data,
            button_id: recordID,
            _id: detailsAction._id,
          })
        );
      } else {
        dispatch(
          updateAction({
            ...data,
            workflow_id: recordID,
            _id: detailsAction._id,
          })
        );
      }
    } else {
      if (isDynamicButton) {
        dispatch(
          createActionDynamicButton({
            ...data,
            button_id: recordID,
          })
        );
      } else {
        dispatch(
          createAction({
            ...data,
            workflow_id: recordID,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    setShowModalSMS(false);
    form.resetFields();
    setIsEdit(false);
    setSelectedFields([]);
    setSelectedGeneralFields([]);
    setBody("");
    editorJodit.current.value = "";
    setOptionsAppend([]);
    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  const handleSelectField = (values, options, type) => {
    if (type === "fields") {
      setSelectedFields(options);
      setOptionsAppend(selectedGeneralFields.concat(options));
    } else {
      setSelectedGeneralFields(options);
      setOptionsAppend(selectedFields.concat(options));
    }
  };

  const handleSelectTemplate = (value) => {
    dispatch(
      getSMSTemplateById({
        _id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(editSMSTemplate).length > 0) {
      form.setFieldsValue({
        fields: editSMSTemplate.selected_fields,
        general_fields: editSMSTemplate.selected_general_fields,
        unicode: editSMSTemplate.unicode,
      });

      const selected_fields = listFields.filter((field) =>
        editSMSTemplate.selected_fields.includes(field.value)
      );
      const selected_general_fields = generalInfo.filter((field) =>
        editSMSTemplate.selected_general_fields.includes(field.value)
      );
      setSelectedFields(selected_fields);
      setSelectedGeneralFields(selected_general_fields);
      setOptionsAppend(selected_fields.concat(selected_general_fields));

      setBody(editSMSTemplate.content);

      dispatch(getSMSTemplateByIdSuccess({}));
    }
  }, [dispatch, editSMSTemplate, form, listFields]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            if (ele.section_name !== "Meta fields") {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false &&
                  !_.get(field, "encrypted", false)
                ) {
                  tempOptionsFields.push({
                    label: field.related_name,
                    value: field.full_field_id,
                    type: field.type,
                  });
                }
              });
            }
          });
        } else {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (showModalSMS && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        name: detailsAction.action_name,
        description: detailsAction.description,
        unicode: detailsAction.unicode,
        fields: detailsAction.selected_fields,
        general_fields: detailsAction.selected_general_fields,
      });

      setSelectedFields(detailsAction.selected_fields);
      setSelectedGeneralFields(detailsAction.selected_general_fields);

      let tempOptionsAppend = [];

      detailsAction.selected_fields.forEach((item) => {
        const temp = listFields.find((field) => field.value === item);
        if (temp) {
          tempOptionsAppend.push(temp);
        }
      });

      detailsAction.selected_general_fields.forEach((item) => {
        const temp = generalInfo.find((field) => field.value === item);
        if (temp) {
          tempOptionsAppend.push(temp);
        }
      });

      setOptionsAppend(tempOptionsAppend);
      setBody(detailsAction.content);
    }
  }, [dispatch, showModalSMS, form, isEdit, detailsAction, listFields]);

  useEffect(() => {
    let tempList = [];
    listSMSTemplate.forEach((item) => {
      if (item.object_id === objectID) {
        tempList.push({
          label: item.sms_template_name,
          value: item._id,
        });
      }
    });
    setListTemplate(tempList);
  }, [listSMSTemplate, objectID]);

  useEffect(() => {
    if (
      showModalSMS &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalSMS(false);
      setIsEdit(false);
      form.resetFields();
      setSelectedFields([]);
      setSelectedGeneralFields([]);
      setBody("");
      setOptionsAppend([]);
    }
  }, [form, setIsEdit, setShowModalSMS, showModalSMS, status]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalSMS}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action || status === "LoadingAction" ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item label={t("workflow.smsTemplate")} name="sms_template">
            <Select
              placeholder={t("common.placeholderSelect")}
              options={listTemplate}
              onChange={handleSelectTemplate}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <Form.Item
            label="Unicode"
            name="unicode"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("common.placeholderSelect")}
              options={optionUnicode}
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.fields")}
            name="fields"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("common.placeholderSelect")}
              options={listFields}
              mode="multiple"
              onChange={(value, options) =>
                handleSelectField(value, options, "fields")
              }
              optionFilterProp="label"
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.generalFields")}
            name="general_fields"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("common.placeholderSelect")}
              options={generalInfo}
              mode="multiple"
              onChange={(value, options) =>
                handleSelectField(value, options, "general_fields")
              }
              optionFilterProp="label"
            />
          </Form.Item>

          <Editor
            editorJodit={editorJodit}
            content={body}
            showToolbar={false}
            optionsAppend={optionsAppend}
          />

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={isDynamicButton ? status === "Loading" : loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalSendSMS);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
