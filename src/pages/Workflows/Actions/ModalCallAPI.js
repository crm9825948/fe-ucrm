import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";
import Tooltip from "antd/lib/tooltip";

import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";

import { Notification } from "components/Notification/Noti";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
} from "redux/slices/workflows";

import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

function ModalCallAPI({
  showModalCallAPI,
  setShowModalCallAPI,
  isEdit,
  setIsEdit,
  objectID,
  loading,
  detailsAction,
  status,
  objects,
  isDynamicButton,
  details,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [form] = Form.useForm();

  const [listFields, setListFields] = useState([]);
  const [params, setParams] = useState([]);
  const [body, setBody] = useState([]);

  const [typeAuthen, setTypeAuthen] = useState("");

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    {
      label: "Token",
      value: "token",
    },
  ];

  const Method = [
    {
      label: "GET",
      value: "GET",
    },
    {
      label: "POST",
      value: "POST",
    },
    {
      label: "PUT",
      value: "PUT",
    },
    {
      label: "DELETE",
      value: "DELETE",
    },
  ];

  const _onSubmit = (values) => {
    if (
      params.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field params!");
    } else if (
      body.find(
        (item) =>
          item.key === undefined || item.key === "" || item.value === undefined
      )
    ) {
      Notification("warning", "Please full field body!");
    } else {
      let data = {
        object_id: objectID,
        action_name: values.name,
        description: values.description,
        action_type: 9,
        auth_type: values.authentication_type,
        method: values.method,
        api_url: values.api_url,
        params: params,
        body: body,
      };

      if (typeAuthen === "basic") {
        data = {
          ...data,
          username: values.user_name,
          password: values.password,
        };
      } else {
        data = {
          ...data,
          token: values.token,
        };
      }

      if (isEdit) {
        if (isDynamicButton) {
          dispatch(
            updateActionDynamicButton({
              ...data,
              button_id: recordID,
              _id: detailsAction._id,
            })
          );
        } else {
          dispatch(
            updateAction({
              ...data,
              workflow_id: recordID,
              _id: detailsAction._id,
            })
          );
        }
      } else {
        if (isDynamicButton) {
          dispatch(
            createActionDynamicButton({
              ...data,
              button_id: recordID,
            })
          );
        } else {
          dispatch(
            createAction({
              ...data,
              workflow_id: recordID,
            })
          );
        }
      }
    }
  };

  const _onCancel = () => {
    setShowModalCallAPI(false);
    form.resetFields();
    setIsEdit(false);
    setParams([]);
    setBody([]);
    setTypeAuthen("");

    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  const _onAddParam = () => {
    setParams([
      ...params,
      {
        key: undefined,
        value: undefined,
      },
    ]);
  };

  const _onDeleteParam = (idx) => {
    let temp = [...params];
    temp.splice(idx, 1);
    setParams(temp);
  };

  const handleChangeParam = (value, index, type) => {
    let temp = [...params];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }
    setParams(temp);
  };
  const _onAddBody = () => {
    setBody([
      ...body,
      {
        key: undefined,
        value: undefined,
      },
    ]);
  };

  const _onDeleteBody = (idx) => {
    let temp = [...body];
    temp.splice(idx, 1);
    setBody(temp);
  };

  const handleChangeBody = (value, index, type) => {
    let temp = [...body];
    switch (type) {
      case "key":
        temp[index] = {
          ...temp[index],
          key: value,
        };
        break;
      case "value":
        temp[index] = {
          ...temp[index],
          value: value,
        };
        break;
      default:
        break;
    }
    setBody(temp);
  };

  const handleSelectAuthenType = (value) => {
    setTypeAuthen(value);
  };

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: "$" + field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    if (isDynamicButton) {
      tempOptionsFields.push({
        label: "Triggered User",
        value: "$triggered_user",
        type: "user",
      });
    }

    setListFields(tempOptionsFields);
  }, [listObjectField, isDynamicButton]);

  useEffect(() => {
    if (showModalCallAPI && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        name: detailsAction.action_name,
        description: detailsAction.description,
        api_url: detailsAction.api_url,
        authentication_type: detailsAction.auth_type,
        method: detailsAction.method,
      });

      if (detailsAction.auth_type === "basic") {
        form.setFieldsValue({
          user_name: detailsAction.username,
          password: detailsAction.password,
        });
      } else {
        form.setFieldsValue({
          token: detailsAction.token,
        });
      }

      setTypeAuthen(detailsAction.auth_type);
      setParams(detailsAction.params);
      setBody(detailsAction.body);
    }
  }, [dispatch, showModalCallAPI, form, isEdit, detailsAction]);

  useEffect(() => {
    if (
      showModalCallAPI &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalCallAPI(false);
      form.resetFields();
      setIsEdit(false);
      setBody([]);
      setParams([]);
      setTypeAuthen("");

      if (isDynamicButton) {
        dispatch(loadDetailsActionDynamicButtonSuccess({}));
      } else {
        dispatch(loadDetailsActionFail());
      }
    }
  }, [
    dispatch,
    form,
    isDynamicButton,
    setIsEdit,
    setShowModalCallAPI,
    showModalCallAPI,
    status,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalCallAPI}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item
            label="API URL"
            name="api_url"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("common.placeholderInput")} />
          </Form.Item>

          <Form.Item
            label={t("workflow.authentication")}
            name="authentication_type"
            // rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("common.placeholderSelect")}
              options={AuthenticationType}
              onChange={handleSelectAuthenType}
            />
          </Form.Item>

          {typeAuthen === "basic" && (
            <WrapAuthen>
              <Form.Item
                label={t("workflow.userName")}
                name="user_name"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input
                  autocomplete="new-password"
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>

              <Form.Item
                label={t("workflow.password")}
                name="password"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input.Password
                  autocomplete="new-password"
                  placeholder={t("common.placeholderInput")}
                />
              </Form.Item>
            </WrapAuthen>
          )}

          {typeAuthen === "token" && (
            <WrapAuthen>
              <Form.Item
                label="Token"
                name="token"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
            </WrapAuthen>
          )}

          <Form.Item
            label={t("workflow.method")}
            name="method"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              options={Method}
              placeholder={t("common.placeholderSelect")}
            />
          </Form.Item>

          <WrapParams>
            <legend>Params</legend>

            {params.length > 0 && (
              <>
                {params.map((item, idx) => {
                  return (
                    <WrapValue>
                      <p>Key</p>
                      <Input
                        value={item.key}
                        placeholder={t("common.placeholderInput")}
                        onChange={(e) =>
                          handleChangeParam(e.target.value, idx, "key")
                        }
                      />
                      <p>Value</p>
                      <Select
                        value={item.value}
                        placeholder={t("common.placeholderInputSelect")}
                        options={listFields}
                        mode="tags"
                        onChange={(e) => {
                          if (e?.length > 1) {
                            e.shift();
                          }
                          handleChangeParam(e[0], idx, "value");
                        }}
                      />
                      <Delete>
                        <Tooltip title="Delete">
                          <img
                            src={DeleteIcon}
                            onClick={() => _onDeleteParam(idx)}
                            alt="delete"
                          />
                        </Tooltip>
                      </Delete>
                    </WrapValue>
                  );
                })}
              </>
            )}
            <AddFields onClick={() => _onAddParam()}>
              {/* <img src={PlusGreen} alt="plus" /> */}
              <span>+ {t("workflow.addParam")}</span>
            </AddFields>
          </WrapParams>

          <WrapBody>
            <legend>Body</legend>

            {body.length > 0 && (
              <>
                {body.map((item, idx) => {
                  return (
                    <WrapValue>
                      <p>Key</p>
                      <Input
                        value={item.key}
                        placeholder={t("common.placeholderInput")}
                        onChange={(e) =>
                          handleChangeBody(e.target.value, idx, "key")
                        }
                      />
                      <p>Value</p>
                      <Select
                        value={item.value}
                        placeholder={t("common.placeholderInputSelect")}
                        options={listFields}
                        mode="tags"
                        onChange={(e) => {
                          if (e?.length > 1) {
                            e.shift();
                          }
                          handleChangeBody(e[0], idx, "value");
                        }}
                      />
                      <Delete>
                        <Tooltip title="Delete">
                          <img
                            src={DeleteIcon}
                            onClick={() => _onDeleteBody(idx)}
                            alt="delete"
                          />
                        </Tooltip>
                      </Delete>
                    </WrapValue>
                  );
                })}
              </>
            )}
            <AddFields onClick={() => _onAddBody()}>
              {/* <img src={PlusGreen} alt="plus" /> */}
              <span>+ {t("workflow.addBody")}</span>
            </AddFields>
          </WrapBody>

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalCallAPI);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    width: 100%;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .requiredMark {
    .ant-form-item-label > label {
      ::before {
        display: none;
      }
    }
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const WrapAuthen = styled.div`
  background: #f2f4f5;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 24px;
  padding: 18px 16px 4px 16px;

  .ant-form-item {
    margin-bottom: 8px;
  }
`;

const WrapParams = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }
  }
`;

const WrapBody = styled(WrapParams)`
  margin-top: 24px;
`;

const WrapValue = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    margin-bottom: 0;
    margin-right: 16px;
  }

  .ant-input {
    width: 40%;
    margin-right: 8px;
  }

  .ant-select {
    width: 30%;
  }
`;
