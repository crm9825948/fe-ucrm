import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import moment from "moment";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";
import Tooltip from "antd/lib/tooltip";

import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";

import Other from "./fieldType/other";
import SelectType from "./fieldType/select";
import Number from "./fieldType/number";
import Email from "./fieldType/email";
import Date from "./fieldType/date";
import LinkingObject from "./fieldType/linkingobject";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
  // updateRequiredFieldsUpdate,
  updateOptionalFieldsUpdateRelated,
  loadTargetPicklistFail,
  loadFieldsObjectRelated,
  loadFieldsObjectRelatedFail,
  loadTargetObjects,
} from "redux/slices/workflows";

import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";
import { loadListObjectField } from "redux/slices/objects";

import { Notification } from "components/Notification/Noti";
import Conditions from "./Conditions/conditions";

function ModalUpdateRecord({
  showModalUpdate,
  setShowModalUpdate,
  isEdit,
  setIsEdit,
  objectID,
  loading,
  detailsAction,
  status,
  objects,
  isDynamicButton,
  listAllUser,
  details,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const {
    listFieldsMain,
    // requiredFieldsUpdate,
    optionalFieldsUpdateRelated,
    targetObjects,
    listFieldsRelated,
  } = useSelector((state) => state.workflowsReducer);

  const [form] = Form.useForm();

  const [listObjects, setListObjects] = useState([]);
  const [fieldsMain, setFieldsMain] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [optionalFieldsSelect, setOptionalFieldsSelect] = useState([]);
  const [requiredFieldsValue, setRequiredFieldsValue] = useState([]);
  const [formValue, setFormValue] = useState({});

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    const emptyField = optionalFieldsSelect.find(
      (item) => item.field_id === ""
    );

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else if (emptyField) {
      Notification("warning", "Please fullfill or remove empty Field!");
    } else {
      const fieldsValues = requiredFieldsValue.concat(optionalFieldsSelect);

      // let tempFieldValue = {};
      // fieldsValues.forEach((item) => {
      //   tempFieldValue = {
      //     ...tempFieldValue,
      //     [item.field_id]: item.value,
      //   };
      // });
      let data = {
        object_id: objectID,
        action_name: values.name,
        description: values.description,
        action_type: 12,
        target_object: values.object,
        filter_condition: JSON.stringify({
          and_filter: allCondition,
          or_filter: anyCondition,
        }),
        fields_mapping: fieldsValues,
      };

      if (isEdit) {
        if (isDynamicButton) {
          dispatch(
            updateActionDynamicButton({
              data: {
                ...data,
                button_id: recordID,
              },
              action_type: 12,
              _id: detailsAction._id,
            })
          );
        } else {
          dispatch(
            updateAction({
              data: {
                ...data,
                workflow_id: recordID,
              },
              action_type: 12,
              _id: detailsAction._id,
            })
          );
        }
      } else {
        if (isDynamicButton) {
          dispatch(
            createActionDynamicButton({
              ...data,
              button_id: recordID,
            })
          );
        } else {
          dispatch(
            createAction({
              ...data,
              workflow_id: recordID,
            })
          );
        }
      }
    }
  };

  const _onCancel = () => {
    setShowModalUpdate(false);
    form.resetFields();
    setIsEdit(false);
    setOptionalFieldsSelect([]);
    setRequiredFieldsValue([]);
    setFormValue({});

    setAllCondition([]);
    setAnyCondition([]);
    setOperatorValueAnd([]);
    setOperatorValueOr([]);
    setValueAnd([]);
    setValueOr([]);

    dispatch(loadTargetPicklistFail());
    dispatch(loadFieldsObjectRelatedFail());
    dispatch(updateOptionalFieldsUpdateRelated([]));
    dispatch(
      loadListObjectField({
        object_id: objectID,
        api_version: "2",
        show_meta_fields: true,
      })
    );

    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  const handleSelectObject = useCallback(
    (value) => {
      setOptionalFieldsSelect([]);
      setRequiredFieldsValue([]);
      setFormValue({});

      setAllCondition([]);
      setAnyCondition([]);
      setOperatorValueAnd([]);
      setOperatorValueOr([]);
      setValueAnd([]);
      setValueOr([]);

      //   dispatch(updateRequiredFieldsCreate([]));
      dispatch(updateOptionalFieldsUpdateRelated([]));
      dispatch(
        loadFieldsObjectRelated({
          api_version: "2",
          object_id: value,
        })
      );

      dispatch(
        loadListObjectField({
          object_id: value,
          api_version: "2",
          show_meta_fields: true,
        })
      );
    },
    [dispatch]
  );

  const onDeleteField = (idx) => {
    let temp = [...optionalFieldsSelect];
    temp.splice(idx, 1);
    temp.forEach((item) => {
      form.setFieldsValue({
        [item.value]: undefined,
      });
    });

    setOptionalFieldsSelect(temp);
  };

  const _onAddField = () => {
    const item = [
      ...optionalFieldsSelect,
      {
        field_id: "",
        value: "",
        id_field_related_record: null,
        object_related: null,
        id_related_record: null,
        operator: null,
        field_type: null,
        required: false,
        isDate: false,
      },
    ];

    setOptionalFieldsSelect(item);
  };

  const handleChangeFieldRequired = (field_id, value, idx) => {
    let temp = [...requiredFieldsValue];

    const foundExist = temp.find((item) => item.field_id === field_id);
    const foundIndex = temp.findIndex((item) => item.field_id === field_id);

    if (foundExist) {
      temp[foundIndex] = {
        field_id: field_id,
        value: value,
        required: true,
      };
      setRequiredFieldsValue(temp);
    } else {
      setRequiredFieldsValue([
        ...requiredFieldsValue,
        {
          field_id: field_id,
          value: value,
          required: true,
        },
      ]);
    }
  };

  const handleChangeField = (
    e,
    option,
    idx,
    type,
    isDate,
    id_field_related_record,
    object_related,
    id_related_record,
    operator
  ) => {
    let arr = [...optionalFieldsSelect];

    let tempField = {};
    optionalFieldsUpdateRelated.forEach((item) => {
      if (item.value === e) {
        tempField = item;
      }
    });

    if (type === "field") {
      optionalFieldsSelect.forEach((item, index) => {
        if (index === idx) {
          arr[index] = {
            ...item,
            field_id: e,
            field_type: tempField.type,
            value: "",
          };
        }
      });
    } else {
      optionalFieldsSelect.forEach((item, index) => {
        if (index === idx) {
          if (
            item.field_type === "linkingobject" ||
            item.field_type === "lookup"
          ) {
            arr[index] = {
              ...item,
              value: e,
              isDate: isDate ? true : false,
              id_field_related_record: id_field_related_record
                ? id_field_related_record
                : null,
              object_related: object_related ? object_related : null,
              id_related_record: id_related_record ? id_related_record : null,
            };
          } else {
            arr[index] = {
              ...item,
              value: e,
              isDate: isDate ? true : false,
              operator: operator ? operator : null,
            };
          }
        }
      });
    }

    setOptionalFieldsSelect(arr);
  };

  const handleFieldType = (field, optionalFields, required, idx) => {
    switch (field.type) {
      case "number":
        return (
          <Number
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      // case "datetime-local":
      //   return (
      //     <DateTime
      //       form={form}
      //       key={field.value}
      //       field={field}
      //       optionalFields={optionalFields}
      //       required={required}
      //     />
      //   );
      case "date":
      case "datetime-local":
        return (
          <Date
            form={form}
            key={field.value}
            field={field}
            required={required}
            idx={idx}
            listValues={optionalFieldsSelect}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      case "email":
        return (
          <Email
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      case "user":
      case "file":
      case "select":
      case "dynamic-field":
        return (
          <SelectType
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            required={required}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            handleChangeFieldRequired={handleChangeFieldRequired}
            idx={idx}
            handleChangeField={handleChangeField}
            objectID={objectID}
            isEdit={
              showModalUpdate && isEdit && Object.keys(detailsAction).length > 0
            }
            listObjects={listObjects}
            updateRecord={true}
          />
        );
      case "lookup":
      case "linkingobject":
        return (
          <LinkingObject
            flag={""}
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            fieldsMain={fieldsMain}
            required={required}
            objectCreate={form.getFieldValue("object")}
            form={form}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
      default:
        return (
          <Other
            key={field.value}
            field={field}
            optionalFields={optionalFields}
            fieldsMain={fieldsMain}
            required={required}
            idx={idx}
            handleChangeField={handleChangeField}
            handleChangeFieldRequired={handleChangeFieldRequired}
          />
        );
    }
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    if (optionalFieldsSelect.length > 0) {
      optionalFieldsSelect.forEach((item, idx) => {
        if (item.field_id !== "") {
          form.setFieldsValue({
            [item.field_id + idx]: item.field_id,
            [item.field_id]:
              item.value !== ""
                ? item.field_type === "number" || item.field_type === "email"
                  ? [item.value]
                  : item.field_type === "date" ||
                    item.field_type === "datetime-local"
                  ? item.isDate
                    ? moment(item.value)
                    : item.value
                  : item.value
                : undefined,
          });
        } else {
          form.setFieldsValue({
            [item.field_id + idx]: undefined,
          });
        }
      });
    }
  }, [form, optionalFieldsSelect]);

  //   useEffect(() => {
  //     let tempOptionalFields = [];
  //     let tempListFieldMain = [];

  //     listFieldsMain.forEach((item) => {
  //       if (Object.values(item)[0].readable || Object.values(item)[0].writeable) {
  //         Object.values(item)[0].sections.forEach((ele) => {
  //           ele.fields.forEach((field) => {
  //             if (
  //               field.hidden === false &&
  //               field.permission_hidden === false &&
  //               field.type !== "id" &&
  //               field.type !== "formula"
  //             ) {
  //               tempListFieldMain.push({
  //                 label: field.related_name,
  //                 value:
  //                   Object.keys(item)[0] === "main_object"
  //                     ? "$" + field.ID
  //                     : "$" + Object.keys(item)[0] + "." + field.ID,
  //                 type: field.type,
  //               });
  //             }
  //           });
  //         });
  //       }
  //     });
  //     setFieldsMain(tempListFieldMain);

  //     if (tempListFieldMain.length > 0) {
  //       listFieldsRelated.forEach((item) => {
  //         if (
  //           Object.values(item)[0].readable ||
  //           Object.values(item)[0].writeable
  //         ) {
  //           if (Object.keys(item)[0] === "main_object") {
  //             Object.values(item)[0].sections.forEach((ele) => {
  //               ele.fields.forEach((field) => {
  //                 if (
  //                   field.hidden === false &&
  //                   field.permission_hidden === false &&
  //                   field.type !== "id" &&
  //                   field.type !== "formula"
  //                 ) {
  //                   if (field.type === "linkingobject") {
  //                     if (field.key) {
  //                       tempOptionalFields.push({
  //                         label: field.name,
  //                         value: field.ID,
  //                         type: field.type,
  //                         options: [],
  //                         required: field.required,
  //                         related_name: field.related_name,
  //                         objectname: field.objectname,
  //                         field: field.field,
  //                       });
  //                     }
  //                   } else if (field.type === "lookup") {
  //                     tempOptionalFields.push({
  //                       label: field.name,
  //                       value: field.ID,
  //                       type: field.type,
  //                       options: [],
  //                       required: field.required,
  //                       related_name: field.related_name,
  //                       objectname: field.objectname,
  //                       field: field.field,
  //                     });
  //                   } else if (field.type === "select") {
  //                     tempOptionalFields.push({
  //                       label: field.name,
  //                       value: field.ID,
  //                       type: field.type,
  //                       options: field.option,
  //                       required: field.required,
  //                       source: field.source,
  //                       target: field.target,
  //                     });
  //                   } else {
  //                     tempOptionalFields.push({
  //                       label: field.name,
  //                       value: field.ID,
  //                       type: field.type,
  //                       options: field.option || [],
  //                       required: field.required,
  //                     });
  //                   }
  //                 }
  //               });
  //             });
  //           }
  //         }
  //       });

  //       tempOptionalFields.push({
  //         label: "Assign to",
  //         value: "owner",
  //         type: "user",
  //         options: listUser,
  //       });

  //       let tempObjects = [];
  //       Object.entries(objects).forEach(([key, val]) => {
  //         val.forEach((object) => {
  //           if (object.Status) {
  //             tempObjects.push({
  //               label: object.Name,
  //               value: object._id,
  //             });
  //           }
  //         });
  //       });

  //       const foundTargetObject = tempObjects.find(
  //         (object) => object.value === objectID
  //       );

  //       // tempRequiredFields.forEach((requiredField, idx) => {
  //       //   switch (requiredField.type) {
  //       //     case "select":
  //       //       if (foundTargetObject) {
  //       //         let newOptions = [...requiredField.options];

  //       //         newOptions.push({
  //       //           label: requiredField.label + "." + foundTargetObject.label,
  //       //           value:
  //       //             "$" + foundTargetObject.value + "." + requiredField.value,
  //       //         });

  //       //         tempRequiredFields[idx] = {
  //       //           ...requiredField,
  //       //           options: newOptions,
  //       //         };
  //       //       }
  //       //       break;
  //       //     default:
  //       //       break;
  //       //   }
  //       // });

  //       tempOptionalFields.forEach((optionalField, idx) => {
  //         switch (optionalField.type) {
  //           case "select":
  //           case "dynamic-field":
  //             if (foundTargetObject && foundTargetObject.value === objectID) {
  //               let newOptions = [...optionalField.options];

  //               newOptions.push({
  //                 label: foundTargetObject.label + "." + optionalField.label,
  //                 value:
  //                   "$" + foundTargetObject.value + "." + optionalField.value,
  //               });

  //               tempOptionalFields[idx] = {
  //                 ...optionalField,
  //                 options: newOptions,
  //               };

  //               // if (
  //               //   (optionalField.source && optionalField.source !== null) ||
  //               //   (optionalField.target && optionalField.target.length > 0)
  //               // ) {
  //               //   //
  //               // } else {
  //               //   newOptions.push({
  //               //     label: foundTargetObject.label + "." + optionalField.label,
  //               //     value:
  //               //       "$" + foundTargetObject.value + "." + optionalField.value,
  //               //   });

  //               //   tempOptionalFields[idx] = {
  //               //     ...optionalField,
  //               //     options: newOptions,
  //               //   };
  //               // }
  //             }
  //             break;

  //           case "number":
  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: tempListFieldMain.filter(
  //                 (element) => element.type === "number"
  //               ),
  //             };
  //             break;

  //           case "email":
  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: tempListFieldMain.filter(
  //                 (element) => element.type === "email"
  //               ),
  //             };
  //             break;

  //           case "file":
  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: tempListFieldMain.filter(
  //                 (element) => element.type === "file"
  //               ),
  //             };
  //             break;

  //           case "date":
  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: tempListFieldMain.filter(
  //                 (element) => element.type === "date"
  //               ),
  //             };
  //             break;

  //           case "datetime-local":
  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: tempListFieldMain.filter(
  //                 (element) => element.type === "datetime-local"
  //               ),
  //             };
  //             break;

  //           case "user":
  //             let newOptions = tempListFieldMain
  //               .filter((element) => element.type === "user")
  //               .concat(listUser);

  //             listFieldsMain.forEach((element) => {
  //               if (
  //                 Object.values(element)[0].readable ||
  //                 Object.values(element)[0].writeable
  //               ) {
  //                 if (Object.keys(element)[0] === "main_object") {
  //                   newOptions.push({
  //                     label: Object.values(element)[0].object_name + ".AssignTo",
  //                     value:
  //                       "$" +
  //                       Object.values(element)[0].sections[0].object_id +
  //                       ".owner",
  //                   });
  //                 } else {
  //                   newOptions.push({
  //                     label: Object.values(element)[0].object_name + ".AssignTo",
  //                     value: "$" + Object.keys(element)[0] + ".owner",
  //                   });
  //                 }
  //               }
  //             });

  //             newOptions = newOptions.filter(
  //               (value, index, self) =>
  //                 index === self.findIndex((t) => t.value === value.value)
  //             );

  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: newOptions,
  //             };
  //             break;

  //           case "lookup":
  //             let optionsLookup = [];

  //             if (foundTargetObject) {
  //               optionsLookup.push({
  //                 label: foundTargetObject.label + "." + optionalField.label,
  //                 value:
  //                   "$" + foundTargetObject.value + "." + optionalField.value,
  //               });
  //             }

  //             optionsLookup.push({
  //               label: "Select record",
  //               value: "select_record",
  //             });

  //             tempOptionalFields[idx] = {
  //               ...optionalField,
  //               options: optionsLookup,
  //             };
  //             break;

  //           // case "linkingobject":
  //           //   tempOptionalFields[idx] = {
  //           //     ...optionalField,
  //           //     options: [
  //           //       {
  //           //         label: optionalField.related_name,
  //           //         value: "$" + objectID + "." + optionalField.value,
  //           //       },
  //           //     ],
  //           //   };
  //           //   break;

  //           default:
  //             break;
  //         }
  //       });

  //       if (isEdit && showModalUpdate && Object.keys(detailsAction).length > 0) {
  //         tempOptionalFields.forEach((item) => {
  //           let temp = detailsAction.field_value.find(
  //             (ele) => ele.ID === item.value
  //           );
  //           if (temp) {
  //             form.setFieldsValue({
  //               [temp.ID]:
  //                 temp.type === "number" || temp.type === "email"
  //                   ? [temp.value]
  //                   : temp.value,
  //             });
  //           }
  //         });

  //         setFormValue(form.getFieldsValue());
  //       }

  //       listFieldsRelated.forEach((item) => {
  //         if (Object.keys(item)[0] !== "main_object") {
  //           Object.values(item)[0].sections.forEach((ele) => {
  //             const resultOptional = ele.fields.filter((item1) =>
  //               tempOptionalFields.some((item2) => item1.field_id === item2.field)
  //             );

  //             if (resultOptional.length > 0) {
  //               tempOptionalFields.forEach((item, idx) => {
  //                 if (
  //                   item.field === resultOptional[0].field_id &&
  //                   item.type !== "lookup"
  //                 ) {
  //                   tempOptionalFields[idx] = {
  //                     ...tempOptionalFields[idx],
  //                     options: [
  //                       ...tempOptionalFields[idx].options,
  //                       {
  //                         label: resultOptional[0].related_name,
  //                         value: "$" + objectID + "." + item.field,
  //                       },
  //                       {
  //                         label: "Select record",
  //                         value: "select_record",
  //                       },
  //                     ],
  //                   };
  //                 }
  //               });
  //             }
  //           });
  //         }
  //       });

  //       // dispatch(updateRequiredFieldsUpdate(tempRequiredFields));
  //       dispatch(updateOptionalFieldsUpdateRelated(tempOptionalFields));
  //     }
  //   }, [
  //     detailsAction,
  //     dispatch,
  //     form,
  //     isEdit,
  //     listFieldsMain,
  //     listFieldsRelated,
  //     listUser,
  //     objectID,
  //     objects,
  //     showModalUpdate,
  //   ]);

  useEffect(() => {
    let tempListFieldMain = [];

    listFieldsMain.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (
              field.hidden === false &&
              field.permission_hidden === false &&
              field.type !== "id" &&
              field.type !== "formula" &&
              !_.get(field, "encrypted", false)
            ) {
              tempListFieldMain.push({
                label: field.related_name,
                value:
                  Object.keys(item)[0] === "main_object"
                    ? "$" + field.ID
                    : "$" + Object.keys(item)[0] + "." + field.ID,
                type: field.type,
              });
            }
          });
        });
      }
    });
    setFieldsMain(tempListFieldMain);
  }, [listFieldsMain]);

  useEffect(() => {
    // let tempRequiredFields = [];
    let tempOptionalFields = [];
    if (
      showModalUpdate &&
      listFieldsRelated.length > 0 &&
      fieldsMain.length > 0
    ) {
      listFieldsRelated.forEach((item) => {
        if (
          Object.values(item)[0] !== null &&
          (Object.values(item)[0].readable || Object.values(item)[0].writeable)
        ) {
          if (Object.keys(item)[0] === "main_object") {
            Object.values(item)[0].sections.forEach((ele) => {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false &&
                  field.type !== "id" &&
                  field.type !== "formula" &&
                  !_.get(field, "encrypted", false)
                ) {
                  if (
                    field.type === "select" ||
                    field.type === "dynamic-field"
                  ) {
                    tempOptionalFields.push({
                      label: field.name,
                      value: field.ID,
                      type: field.type,
                      options: field.option,
                      required: field.required,
                      source: field.source,
                      target: field.target,
                    });
                  } else if (field.type === "linkingobject") {
                    if (field.key) {
                      tempOptionalFields.push({
                        label: field.name,
                        value: field.ID,
                        type: field.type,
                        options: [],
                        required: field.required,
                        related_name: field.related_name,
                        objectname: field.objectname,
                        field: field.field,
                      });
                    }
                  } else if (field.type === "lookup") {
                    tempOptionalFields.push({
                      label: field.name,
                      value: field.ID,
                      type: field.type,
                      options: [],
                      required: field.required,
                      related_name: field.related_name,
                      objectname: field.objectname,
                      field: field.field,
                    });
                  } else {
                    tempOptionalFields.push({
                      label: field.name,
                      value: field.ID,
                      type: field.type,
                      options: field.option || [],
                      required: field.required,
                    });
                  }
                }
              });
            });
          }
        }
      });

      //   tempRequiredFields.push({
      //     label: "Assign to",
      //     value: "owner",
      //     type: "user",
      //     options: listUser,
      //   });

      const foundTargetObject = listObjects.find(
        (object) => object.value === form.getFieldValue("object")
      );

      // const foundMainObject = listObjects.find(
      //   (object) => object.value === objectID
      // );

      //   tempRequiredFields.forEach((requiredField, idx) => {
      //     switch (requiredField.type) {
      //       case "select":
      //       case "dynamic-field":
      //         if (foundTargetObject && foundTargetObject.value === objectID) {
      //           let newOptions = [...requiredField.options];

      //           newOptions.push({
      //             label: foundTargetObject.label + "." + requiredField.label,
      //             value:
      //               "$" + foundTargetObject.value + "." + requiredField.value,
      //           });

      //           tempRequiredFields[idx] = {
      //             ...requiredField,
      //             options: newOptions,
      //           };

      //           // if (
      //           //   (requiredField.source && requiredField.source !== null) ||
      //           //   (requiredField.target && requiredField.target.length > 0)
      //           // ) {
      //           //   //
      //           // } else {
      //           //   newOptions.push({
      //           //     label: foundTargetObject.label + "." + requiredField.label,
      //           //     value:
      //           //       "$" + foundTargetObject.value + "." + requiredField.value,
      //           //   });

      //           //   tempRequiredFields[idx] = {
      //           //     ...requiredField,
      //           //     options: newOptions,
      //           //   };
      //           // }
      //         }
      //         break;

      //       case "number":
      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: fieldsMain.filter(
      //             (element) => element.type === "number"
      //           ),
      //         };
      //         break;

      //       case "email":
      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: fieldsMain.filter((element) => element.type === "email"),
      //         };
      //         break;

      //       case "file":
      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: fieldsMain.filter((element) => element.type === "file"),
      //         };
      //         break;

      //       case "date":
      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: fieldsMain.filter((element) => element.type === "date"),
      //         };
      //         break;

      //       case "datetime-local":
      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: fieldsMain.filter(
      //             (element) => element.type === "datetime-local"
      //           ),
      //         };
      //         break;

      //       case "user":
      //         let newOptions = fieldsMain
      //           .filter((element) => element.type === "user")
      //           .concat(listUser);

      //         listFieldsMain.forEach((element) => {
      //           if (
      //             Object.values(element)[0].readable ||
      //             Object.values(element)[0].writeable
      //           ) {
      //             if (Object.keys(element)[0] === "main_object") {
      //               newOptions.push({
      //                 label: Object.values(element)[0].object_name + ".AssignTo",
      //                 value:
      //                   "$" +
      //                   Object.values(element)[0].sections[0].object_id +
      //                   ".owner",
      //               });
      //             } else {
      //               newOptions.push({
      //                 label: Object.values(element)[0].object_name + ".AssignTo",
      //                 value: "$" + Object.keys(element)[0] + ".owner",
      //               });
      //             }
      //           }
      //         });

      //         newOptions = newOptions.filter(
      //           (value, index, self) =>
      //             index === self.findIndex((t) => t.value === value.value)
      //         );

      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: newOptions,
      //         };
      //         break;

      //       case "lookup":
      //         let optionsLookup = [];

      //         if (foundTargetObject) {
      //           optionsLookup.push({
      //             label: foundTargetObject.label + "." + requiredField.label,
      //             value:
      //               "$" + foundTargetObject.value + "." + requiredField.value,
      //           });
      //         }

      //         optionsLookup.push({
      //           label: "Select record",
      //           value: "select_record",
      //         });

      //         tempRequiredFields[idx] = {
      //           ...requiredField,
      //           options: optionsLookup,
      //         };
      //         break;

      //       // case "linkingobject":
      //       //   tempRequiredFields[idx] = {
      //       //     ...requiredField,
      //       //     options: [
      //       //       {
      //       //         label: requiredField.related_name,
      //       //         value: "$" + objectID + "." + requiredField.value,
      //       //       },
      //       //     ],
      //       //   };
      //       // if (objectID === form.getFieldValue("object")) {
      //       //   tempRequiredFields[idx] = {
      //       //     ...requiredField,
      //       //     options: [
      //       //       {
      //       //         label: requiredField.related_name,
      //       //         value: "$" + objectID + "." + requiredField.value,
      //       //       },
      //       //       {
      //       //         label: "Select record",
      //       //         value: "select_record",
      //       //       },
      //       //     ],
      //       //   };
      //       // } else {
      //       //   tempRequiredFields[idx] = {
      //       //     ...requiredField,
      //       //     options: [
      //       //       {
      //       //         label: "Select record",
      //       //         value: "select_record",
      //       //       },
      //       //     ],
      //       //   };
      //       // }
      //       // break;

      //       default:
      //         break;
      //     }
      //   });

      tempOptionalFields.forEach((optionalField, idx) => {
        switch (optionalField.type) {
          case "select":
            if (foundTargetObject) {
              let newOptions = [...optionalField.options];
              if (foundTargetObject.value === objectID) {
                newOptions.push({
                  label: foundTargetObject.label + "." + optionalField.label,
                  value:
                    "$" + foundTargetObject.value + "." + optionalField.value,
                });
              } else {
                if (!isDynamicButton) {
                  listFieldsMain.forEach((element) => {
                    if (
                      Object.values(element)[0].readable ||
                      Object.values(element)[0].writeable
                    ) {
                      if (Object.keys(element)[0] === "main_object") {
                        Object.values(element)[0].sections.forEach((main) => {
                          main.fields.forEach((fld) => {
                            if (
                              fld.hidden === false &&
                              fld.permission_hidden === false &&
                              !_.get(fld, "encrypted", false) &&
                              fld.type === "select"
                            ) {
                              newOptions.push({
                                label:
                                  Object.values(element)[0].object_name +
                                  "." +
                                  fld.name,
                                value: "$" + objectID + "." + fld.ID,
                              });
                            }
                          });
                        });
                      }
                    }
                  });
                }
              }
              tempOptionalFields[idx] = {
                ...optionalField,
                options: newOptions,
              };
            }
            break;

          case "dynamic-field":
            if (foundTargetObject && foundTargetObject.value === objectID) {
              let newOptions = [...optionalField.options];

              newOptions.push({
                label: foundTargetObject.label + "." + optionalField.label,
                value:
                  "$" + foundTargetObject.value + "." + optionalField.value,
              });

              tempOptionalFields[idx] = {
                ...optionalField,
                options: newOptions,
              };
            }
            break;

          case "number":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter(
                (element) => element.type === "number"
              ),
            };
            break;

          case "email":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "email"),
            };
            break;

          case "file":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "file"),
            };
            break;

          case "date":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter((element) => element.type === "date"),
            };
            break;

          case "datetime-local":
            tempOptionalFields[idx] = {
              ...optionalField,
              options: fieldsMain.filter(
                (element) => element.type === "datetime-local"
              ),
            };
            break;

          case "user":
            let newOptions = fieldsMain
              .filter((element) => element.type === "user")
              .concat(listUser);

            listFieldsMain.forEach((element) => {
              if (
                Object.values(element)[0].readable ||
                Object.values(element)[0].writeable
              ) {
                if (Object.keys(element)[0] === "main_object") {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value:
                      "$" +
                      Object.values(element)[0].sections[0].object_id +
                      ".owner",
                  });
                } else {
                  newOptions.push({
                    label: Object.values(element)[0].object_name + ".AssignTo",
                    value: "$" + Object.keys(element)[0] + ".owner",
                  });
                }
              }
            });

            newOptions = newOptions.filter(
              (value, index, self) =>
                index === self.findIndex((t) => t.value === value.value)
            );

            tempOptionalFields[idx] = {
              ...optionalField,
              options: newOptions,
            };
            break;

          case "lookup":
            let optionsLookup = [];

            if (foundTargetObject) {
              optionsLookup.push({
                label: foundTargetObject.label + "." + optionalField.label,
                value:
                  "$" + foundTargetObject.value + "." + optionalField.value,
              });
            }

            // optionsLookup.push({
            //   label: "Select record",
            //   value: "select_record",
            // });

            tempOptionalFields[idx] = {
              ...optionalField,
              options: optionsLookup,
            };
            break;

          // case "linkingobject":
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: optionalField.related_name,
          //         value: "$" + objectID + "." + optionalField.value,
          //       },
          //     ],
          //   };
          // if (objectID === form.getFieldValue("object")) {
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: optionalField.related_name,
          //         value: "$" + objectID + "." + optionalField.value,
          //       },
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // } else {
          //   tempOptionalFields[idx] = {
          //     ...optionalField,
          //     options: [
          //       {
          //         label: "Select record",
          //         value: "select_record",
          //       },
          //     ],
          //   };
          // }
          // break;

          default:
            break;
        }
      });

      // if (isEdit && showModalUpdate && Object.keys(detailsAction).length > 0) {
      //   tempOptionalFields.forEach((item) => {
      //     let temp = detailsAction.field_value.find(
      //       (ele) => ele.ID === item.value
      //     );
      //     if (temp) {
      //       form.setFieldsValue({
      //         [temp.ID]:
      //           temp.type === "number" || temp.type === "email"
      //             ? [temp.value]
      //             : temp.value,
      //       });
      //     }
      //   });

      //   setFormValue(form.getFieldsValue());
      // }

      listFieldsRelated.forEach((item) => {
        if (Object.keys(item)[0] !== "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            // const resultRequired = ele.fields.filter((item1) =>
            //   tempRequiredFields.some((item2) => item1.field_id === item2.field)
            // );

            // if (resultRequired.length > 0) {
            //   tempRequiredFields.forEach((item, idx) => {
            //     if (
            //       item.field === resultRequired[0].field_id &&
            //       item.type !== "lookup"
            //     ) {
            //       tempRequiredFields[idx] = {
            //         ...tempRequiredFields[idx],
            //         options: [
            //           ...tempRequiredFields[idx].options,
            //           {
            //             label: resultRequired[0].related_name,
            //             value:
            //               "$" + resultRequired[0].object_id + "." + item.field,
            //           },
            //           {
            //             label:
            //               foundTargetObject.label +
            //               "." +
            //               tempRequiredFields[idx].label,
            //             value:
            //               "$" +
            //               foundTargetObject.value +
            //               "." +
            //               tempRequiredFields[idx].value,
            //           },
            //           {
            //             label: "Select record",
            //             value: "select_record",
            //           },
            //         ],
            //       };
            //     }
            //   });
            // }

            const resultOptional = ele.fields.filter((item1) =>
              tempOptionalFields.some((item2) => item1.field_id === item2.field)
            );

            if (resultOptional.length > 0) {
              tempOptionalFields.forEach((item, idx) => {
                resultOptional.forEach((field) => {
                  if (item.field === field.field_id && item.type !== "lookup") {
                    tempOptionalFields[idx] = {
                      ...tempOptionalFields[idx],
                      options: [
                        ...tempOptionalFields[idx].options,
                        {
                          label: resultOptional[0].related_name,
                          value:
                            "$" +
                            resultOptional[0].object_id +
                            "." +
                            item.field,
                        },
                        {
                          label:
                            foundTargetObject.label +
                            "." +
                            tempOptionalFields[idx].label,
                          value:
                            "$" +
                            foundTargetObject.value +
                            "." +
                            tempOptionalFields[idx].value,
                        },
                        {
                          label: "Select record",
                          value: "select_record",
                        },
                      ],
                    };
                  }
                });
              });
            }
          });
        }
      });

      //   dispatch(updateRequiredFieldsCreate(tempRequiredFields));
      dispatch(updateOptionalFieldsUpdateRelated(tempOptionalFields));
    }
  }, [
    detailsAction,
    dispatch,
    fieldsMain,
    form,
    isEdit,
    listFieldsMain,
    listFieldsRelated,
    listObjects,
    listUser,
    objectID,
    showModalUpdate,
    isDynamicButton,
  ]);

  useEffect(() => {
    if (showModalUpdate && isEdit && Object.keys(detailsAction).length > 0) {
      form.setFieldsValue({
        name: detailsAction.action_name,
        description: detailsAction.description,
        object: detailsAction.target_object,
      });

      handleSelectObject(detailsAction.target_object);

      setRequiredFieldsValue(
        detailsAction.fields_mapping.filter((item) => item.required)
      );

      setOptionalFieldsSelect(
        detailsAction.fields_mapping.filter((item) => item.required === false)
      );

      const conditions = JSON.parse(detailsAction.filter_condition);
      setAllCondition(conditions.and_filter);
      setAnyCondition(conditions.or_filter);
      // setOptionalFieldsSelect(detailsAction.fields_values);
    }
  }, [
    dispatch,
    showModalUpdate,
    form,
    isEdit,
    detailsAction,
    handleSelectObject,
  ]);

  useEffect(() => {
    let tempObjects = [];
    targetObjects.forEach((item) => {
      tempObjects.push({
        label: item.object_name,
        value: item.object_id,
      });
    });
    setListObjects(tempObjects);
  }, [targetObjects]);

  useEffect(() => {
    if (
      showModalUpdate &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalUpdate(false);
      form.resetFields();
      setIsEdit(false);
      setOptionalFieldsSelect([]);
      setRequiredFieldsValue([]);
      setFormValue({});

      setAllCondition([]);
      setAnyCondition([]);
      setOperatorValueAnd([]);
      setOperatorValueOr([]);
      setValueAnd([]);
      setValueOr([]);

      dispatch(loadTargetPicklistFail());
      dispatch(loadFieldsObjectRelatedFail());
      dispatch(updateOptionalFieldsUpdateRelated([]));

      dispatch(
        loadListObjectField({
          object_id: objectID,
          api_version: "2",
          show_meta_fields: true,
        })
      );

      if (isDynamicButton) {
        dispatch(loadDetailsActionDynamicButtonSuccess({}));
      } else {
        dispatch(loadDetailsActionFail());
      }
    }
  }, [
    dispatch,
    form,
    isDynamicButton,
    objectID,
    setIsEdit,
    setShowModalUpdate,
    showModalUpdate,
    status,
  ]);

  useEffect(() => {
    if (objectID) {
      dispatch(
        loadTargetObjects({
          source_object: objectID,
        })
      );
    }
  }, [dispatch, objectID]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalUpdate}
      footer={null}
      width={700}
      onCancel={_onCancel}
    >
      {loading.action ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
          onValuesChange={(value, values) => {
            setFormValue(values);
          }}
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item label={t("common.description")} name="description">
            <Input placeholder={t("workflow.enterDescription")} />
          </Form.Item>

          <Form.Item
            label={t("workflow.selectTargetObject")}
            name="object"
            rules={[{ required: true, message: t("common.placeholderSelect") }]}
          >
            <Select
              placeholder={t("workflow.selectTargetObject")}
              disabled={isEdit}
              options={listObjects}
              onChange={handleSelectObject}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          {/* {requiredFieldsUpdate.length > 0 && (
            <>
              <Note>{t("workflow.requiredFields")}</Note>
              {requiredFieldsUpdate.map((item, idx) => {
                return handleFieldType(item, requiredFieldsUpdate, true, idx);
              })}
            </>
          )} */}

          {optionalFieldsUpdateRelated.length > 0 && (
            <Condition>
              <legend>{t("workflow.conditions")}</legend>
              <Conditions
                title={t("common.allCondition")}
                decs={`(${t("common.descAllCondition")})`}
                conditions={allCondition}
                setConditions={setAllCondition}
                operatorValue={operatorValueAnd}
                setOperatorValue={setOperatorValueAnd}
                value={valueAnd}
                setValue={setValueAnd}
                ID={
                  Object.keys(detailsAction).length > 0 && detailsAction._id
                    ? detailsAction._id
                    : ""
                }
                fieldsMain={fieldsMain}
              />
              <Conditions
                title={t("common.anyCondition")}
                decs={`(${t("common.descAnyCondition")})`}
                conditions={anyCondition}
                setConditions={setAnyCondition}
                operatorValue={operatorValueOr}
                setOperatorValue={setOperatorValueOr}
                value={valueOr}
                setValue={setValueOr}
                ID={
                  Object.keys(detailsAction).length > 0 && detailsAction._id
                    ? detailsAction._id
                    : ""
                }
                fieldsMain={fieldsMain}
              />
            </Condition>
          )}

          {optionalFieldsUpdateRelated.length > 0 && (
            <Note>{t("workflow.optionalFields")}</Note>
          )}

          {optionalFieldsUpdateRelated.length > 0 &&
            optionalFieldsSelect.length > 0 && (
              <>
                {optionalFieldsSelect.map((item, idx) => {
                  return (
                    <FormValueOptional
                      key={idx}
                      labelCol={{ span: 8 }}
                      wrapperCol={{ span: 16 }}
                      // name={item.value + idx}
                      label={
                        <FormCustom
                          name={item.field_id + idx}
                          wrapperCol={{ span: 24 }}
                        >
                          <Select
                            placeholder={t("common.placeholderSelect")}
                            onChange={(e, option) => {
                              form.setFieldsValue({
                                [item.field_id]: undefined,
                              });
                              handleChangeField(e, option, idx, "field");
                            }}
                            showSearch
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                          >
                            {optionalFieldsUpdateRelated.map((ele) => {
                              return (
                                <Select.Option
                                  disabled={
                                    optionalFieldsSelect.find(
                                      (field) => field.field_id === ele.value
                                    )
                                      ? true
                                      : false
                                  }
                                  key={ele.value}
                                >
                                  {ele.label}
                                </Select.Option>
                              );
                            })}
                          </Select>
                        </FormCustom>
                      }
                    >
                      {item.field_id !== "" ? (
                        <>
                          {handleFieldType(
                            optionalFieldsUpdateRelated.find(
                              (ele) => ele.value === item.field_id
                            ),
                            optionalFieldsUpdateRelated,
                            false,
                            idx
                          )}
                        </>
                      ) : (
                        <Input disabled />
                      )}

                      <Delete>
                        <Tooltip title="Delete">
                          <img
                            src={DeleteIcon}
                            onClick={() => onDeleteField(idx)}
                            alt="delete"
                          />
                        </Tooltip>
                      </Delete>
                    </FormValueOptional>
                  );
                })}
              </>
            )}

          {optionalFieldsUpdateRelated.length > 0 && (
            <AddFields onClick={() => _onAddField()}>
              {/* <img src={PlusGreen} alt="plus" /> */}
              <span>+ {t("workflow.addField")}</span>
            </AddFields>
          )}

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalUpdateRecord);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    width: 100%;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  .requiredMark {
    .ant-form-item-label > label {
      ::before {
        display: none;
      }
    }
  }
`;

const Note = styled.div`
  margin-bottom: 16px;
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 40px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const AddFields = styled.div`
  margin-bottom: 8px;
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 40px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const FormValueOptional = styled(Form.Item)`
  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }
`;

const FormCustom = styled(Form.Item)`
  margin-bottom: 0;
  flex: 1;
  overflow: hidden;
`;

const Condition = styled.fieldset`
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 14px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }

  .ant-select-selection-overflow-item-suffix {
    display: none;
  }
`;
