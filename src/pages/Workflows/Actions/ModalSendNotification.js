import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import isEmail from "validator/lib/isEmail";
import { useParams } from "react-router";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Spin from "antd/lib/spin";

import {
  createAction,
  updateAction,
  loadDetailsActionFail,
} from "redux/slices/workflows";
import {
  createActionDynamicButton,
  loadDetailsActionDynamicButtonSuccess,
  updateActionDynamicButton,
} from "redux/slices/dynamicButton";

import { metaData, metaFields } from "util/staticData";

function ModalSendNoti({
  showModalNoti,
  setShowModalNoti,
  isEdit,
  setIsEdit,
  objectID,
  userFields,
  fields_related,
  listAllGroups,
  listAllUser,
  loading,
  detailsAction,
  status,
  isDynamicButton,
  details,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { recordID } = useParams();

  const [form] = Form.useForm();

  const [listFields, setListFields] = useState([]);
  const [listTo, setListTo] = useState([]);

  const [listCheckFields, setListCheckFields] = useState([]);
  const [listCheckValid, setListCheckValid] = useState([]);

  const _onSubmit = (values) => {
    let tempContent = [];
    let tempTo = [];

    values.content.forEach((item) => {
      if (listFields.find((field) => field.value === item)) {
        tempContent.push("$" + item);
      } else {
        tempContent.push(item);
      }
    });

    values.to.forEach((item) => {
      if (listCheckFields.find((field) => field.value === item)) {
        tempTo.push("$" + item);
      } else {
        tempTo.push(item);
      }
    });

    let data = {
      action_name: values.name,
      action_type: 7,
      object_id: objectID,
      configs: {
        content: tempContent,
        to: tempTo,
      },
    };

    if (isEdit) {
      if (isDynamicButton) {
        dispatch(
          updateActionDynamicButton({
            ...data,
            button_id: recordID,
            _id: detailsAction._id,
          })
        );
      } else {
        dispatch(
          updateAction({
            ...data,
            workflow_id: recordID,
            _id: detailsAction._id,
          })
        );
      }
    } else {
      if (isDynamicButton) {
        dispatch(
          createActionDynamicButton({
            ...data,
            button_id: recordID,
          })
        );
      } else {
        dispatch(
          createAction({
            ...data,
            workflow_id: recordID,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    setShowModalNoti(false);
    form.resetFields();
    setIsEdit(false);
    if (isDynamicButton) {
      dispatch(loadDetailsActionDynamicButtonSuccess({}));
    } else {
      dispatch(loadDetailsActionFail());
    }
  };

  useEffect(() => {
    if (showModalNoti && isEdit && Object.keys(detailsAction).length > 0) {
      let tempContent = [];
      let tempTo = [];

      detailsAction.configs.content.forEach((item) => {
        if (item.split("$").length > 1) {
          tempContent.push(item.split("$")[1]);
        } else {
          tempContent.push(item);
        }
      });

      detailsAction.configs.to.forEach((item) => {
        if (item.split("$").length > 1) {
          tempTo.push(item.split("$")[1]);
        } else {
          tempTo.push(item);
        }
      });

      form.setFieldsValue({
        name: detailsAction.action_name,
        content: tempContent,
        to: tempTo,
      });
    }
  }, [dispatch, showModalNoti, form, isEdit, detailsAction]);

  useEffect(() => {
    let tempFields = [];
    fields_related.forEach((item) => {
      if (!_.get(item, "encrypted", false)) {
        tempFields.push({
          label: item.name,
          value: item.ID,
        });
      }
    });

    setListFields(tempFields.concat(metaData));
  }, [fields_related]);

  useEffect(() => {
    let tempOptionsTo = [];
    let tempListCheckValid = [];

    let tempEmail = [];
    let tempGroup = [];
    let tempUserFields = [];
    let tempUser = [];

    fields_related.forEach((item) => {
      if (item.type === "email" && !_.get(item, "encrypted", false)) {
        tempEmail.push({
          label: item.name,
          value: item.ID,
        });

        tempListCheckValid.push(item.ID);
      }
    });

    listAllGroups.forEach((item) => {
      tempGroup.push({
        label: item.name,
        value: item._id,
      });

      tempListCheckValid.push(item._id);
    });

    userFields.forEach((item) => {
      tempUserFields.push({
        label: item.name,
        value: item.ID,
      });
      tempUserFields.push({
        label: item.name + ".Report to",
        value: item.ID + ".Report_To",
      });
      tempListCheckValid.push(item.ID);
      tempListCheckValid.push(item.ID + ".Report_To");
    });

    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item.Email,
        });

        tempListCheckValid.push(item.Email);
      }
    });

    metaFields.forEach((item) => {
      tempListCheckValid.push(item.value);
    });

    if (tempUser.length > 0) {
      tempOptionsTo.push({
        label: "Users",
        options: tempUser,
      });
    }

    if (tempUserFields.length > 0) {
      tempOptionsTo.push({
        label: "User Fields",
        options: tempUserFields,
      });
    }

    if (tempEmail.length > 0) {
      tempOptionsTo.push({
        label: "Email",
        options: tempEmail,
      });
    }

    if (tempGroup.length > 0) {
      tempOptionsTo.push({
        label: "Groups",
        options: tempGroup,
      });
    }

    tempOptionsTo.push({
      label: "Meta fields",
      options: metaFields,
    });

    setListCheckValid(tempListCheckValid);

    setListCheckFields(tempUserFields.concat(tempEmail).concat(metaFields));
    setListTo(tempOptionsTo);
  }, [fields_related, listAllGroups, listAllUser, userFields]);

  useEffect(() => {
    if (
      showModalNoti &&
      (status === "Create action successfully!" ||
        status === "Update action successfully!")
    ) {
      setShowModalNoti(false);
      setIsEdit(false);
      form.resetFields();
    }
  }, [form, setIsEdit, setShowModalNoti, showModalNoti, status]);

  return (
    <ModalCustom
      title={isEdit ? t("workflow.editWork") : t("workflow.addWork")}
      visible={showModalNoti}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      {loading.action || status === "LoadingAction" ? (
        <Spin />
      ) : (
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label={t("workflow.actionName")}
            name="name"
            rules={[{ required: true, message: t("common.placeholderInput") }]}
          >
            <Input placeholder={t("workflow.enterAction")} />
          </Form.Item>

          <Form.Item
            label={t("workflow.content")}
            name="content"
            rules={[
              { required: true, message: t("common.placeholderInputSelect") },
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              options={listFields}
              mode="tags"
            />
          </Form.Item>

          <Form.Item
            label={t("workflow.to")}
            name="to"
            rules={[
              { required: true, message: t("common.placeholderInputSelect") },
              () => ({
                validator(_, values) {
                  if (values) {
                    let tempNotIncludes = [];
                    values.forEach((item) => {
                      if (!listCheckValid.includes(item)) {
                        tempNotIncludes.push(item);
                      }
                    });

                    if (tempNotIncludes.length > 0) {
                      let invalid = tempNotIncludes.filter(
                        (item) => isEmail(item) === false
                      );

                      if (invalid.length > 0) {
                        return Promise.reject(
                          new Error(t("workflow.invalidEmail"))
                        );
                      }
                    }
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Select
              placeholder={t("common.placeholderInputSelect")}
              mode="tags"
              options={listTo}
            />
          </Form.Item>

          <WrapButton label=" ">
            <Button
              disabled={details.status || (isEdit && detailsAction.status)}
              loading={isDynamicButton ? status === "Loading" : loading.modal}
              type="primary"
              htmlType="submit"
            >
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      )}
    </ModalCustom>
  );
}

export default withTranslation()(ModalSendNoti);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    color: rgba(0, 0, 0, 0.25);
    border-color: #d9d9d9;
    background: #f5f5f5;
    text-shadow: none;
    box-shadow: none;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
