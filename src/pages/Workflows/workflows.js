import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Switch from "antd/lib/switch";
import "./workflows.scss";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  loadDataNecessary,
  resetStatus,
  activeWorkflow,
  changeState,
  deleteWorkflow,
  unMountWorkflows,
} from "redux/slices/workflows";

import { Notification } from "components/Notification/Noti";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalAddWorkflow from "./ModalAddWorkflow";
import { changeTitlePage } from "redux/slices/authenticated";

function Workflows(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;
  const { Text } = Typography;

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.workflow")));
    //eslint-disable-next-line
  }, [t]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { loading, status, listWorkflows, IDNewWorkflow } = useSelector(
    (state) => state.workflowsReducer
  );

  const [workflows, setWorkflows] = useState([]);
  const [workflowChange, setWorkflowChange] = useState("");

  const [showModalAdd, setShowModalAdd] = useState(false);

  const [dataDelete, setDataDelete] = useState({});

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "workflow" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActive = (checked, id) => {
    setWorkflowChange(id);
    dispatch(
      activeWorkflow({
        status: checked,
        _id: id,
      })
    );
  };

  const _onDeleteWorkflow = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
    setWorkflowChange(id);
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    let tempList = [];
    listWorkflows.map((item) => {
      return tempList.push({
        key: item._id,
        active: item.status,
        name: item.workflow_name,
        description: item.description,
        object: item.object_name,
        trigger:
          item.execution_condition === 1
            ? t("workflow.trigger1")
            : item.execution_condition === 2
            ? t("workflow.trigger2")
            : item.execution_condition === 3
            ? t("workflow.trigger3")
            : item.execution_condition === 4
            ? t("workflow.trigger5")
            : item.execution_condition === 5
            ? t("workflow.trigger4")
            : item.execution_condition === 6
            ? t("workflow.triggerInteraction")
            : item.execution_condition === 7
            ? t("workflow.triggerWhenPopup")
            : "",
        create_date: item.created_date,
      });
    });

    setTimeout(() => {
      setWorkflows(tempList);
    }, 50);
  }, [dispatch, listWorkflows, t]);

  useEffect(() => {
    if (status !== null) {
      switch (status) {
        case "change status success":
          Notification("success", "Change status successfully!");
          dispatch(
            changeState({
              workflowChange: workflowChange,
              type: "change-status",
            })
          );
          dispatch(resetStatus());
          break;
        case "delete workflow success":
          dispatch(setShowModalConfirmDelete(false));
          Notification("success", "Delete successfully!");
          dispatch(resetStatus());
          dispatch(
            changeState({
              workflowChange: workflowChange,
              type: "delete-workflow",
            })
          );
          break;
        case "create workflow success":
          if (IDNewWorkflow !== null) {
            Notification("success", "Create successfully!");
            setShowModalAdd(false);
            dispatch(resetStatus());

            setTimeout(() => {
              navigate(`/edit-workflow/${IDNewWorkflow}`);
            }, 500);
          }
          break;

        default:
          Notification("error", status);
          dispatch(resetStatus());
          break;
      }
    }
  }, [dispatch, navigate, status, workflowChange, IDNewWorkflow]);

  useEffect(() => {
    return () => {
      dispatch(unMountWorkflows());
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("settings.workflow")}</BreadcrumbItem>
        </Breadcrumb>
        {listWorkflows.length > 0 && checkRule("create") && (
          <AddButton onClick={() => setShowModalAdd(true)}>
            + {t("workflow.addworkflow")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listWorkflows.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("settings.workflow")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={() => setShowModalAdd(true)}>
              + {t("workflow.addworkflow")}
            </AddButton>
          )}
        </Empty>
      ) : (
        <WrapWorkflows>
          <Table
            pagination={false}
            dataSource={workflows}
            scroll={{ x: "max-content" }}
          >
            <Column
              title={t("workflow.active")}
              dataIndex="active"
              key="active"
              width="120px"
              render={(text, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checkedChildren={t("workflow.on")}
                  unCheckedChildren={t("workflow.off")}
                  checked={text}
                  onChange={(checked) => handleActive(checked, record.key)}
                />
              )}
            />
            <Column
              title={t("workflow.nameWorkflow")}
              dataIndex="name"
              key="name"
              width="500px"
              sorter={(a, b) => a.name.localeCompare(b.name)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("common.description")}
              dataIndex="description"
              key="description"
              width="300px"
              sorter={(a, b) => a.description.localeCompare(b.description)}
              render={(text) => (
                <Text style={{ width: "300px" }} ellipsis={{ tooltip: text }}>
                  {text}
                </Text>
              )}
            />
            <Column
              title={t("object.object")}
              dataIndex="object"
              key="object"
              sorter={(a, b) => a.object.localeCompare(b.object)}
              width="200px"
            />
            <Column
              title={t("workflow.trigger")}
              dataIndex="trigger"
              key="trigger"
              sorter={(a, b) => a.trigger.localeCompare(b.trigger)}
              width="396px"
            />
            <Column
              title={t("common.createdDate")}
              dataIndex="create_date"
              key="create_date"
              width="200px"
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                fixed="right"
                width="150px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() =>
                            navigate(`/edit-workflow/${record.key}`)
                          }
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDeleteWorkflow(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
        </WrapWorkflows>
      )}

      <ModalAddWorkflow
        showModalAdd={showModalAdd}
        objects={category}
        setShowModalAdd={setShowModalAdd}
      />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteWorkflow}
        dataDelete={dataDelete}
        isLoading={loading.modalDelete}
      />
    </Wrapper>
  );
}

export default withTranslation()(Workflows);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const WrapWorkflows = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
