import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import TimePicker from "antd/lib/time-picker";
import DatePicker from "antd/lib/date-picker";
import Tooltip from "antd/lib/tooltip";
import Checkbox from "antd/lib/checkbox";
import Tag from "antd/lib/tag";

import Conditions from "components/Conditions/conditions";
import { Notification } from "components/Notification/Noti";

import {
  createWorkflow,
  loadFieldsCondition,
  getListPopup,
} from "redux/slices/workflows";
import { createDynamicButton } from "redux/slices/dynamicButton";
import { loadListObjectFieldSuccess } from "redux/slices/objects";

import {
  triggerWorkflow,
  typeInteraction,
  frequencyWorkflow,
  week,
  days,
} from "util/staticData";

const { Option } = Select;
function ModalAddWorkflow({
  showModalAdd,
  objects,
  isDynamicButton,
  listAllGroups,
  setShowModalAdd,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { loading, listPopup } = useSelector((state) => state.workflowsReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [listObjects, setListObjects] = useState([]);
  const [listGroups, setListGroups] = useState([]);
  const [listMainFields, setListMainFields] = useState([]);
  const [dynamicField, $dynamicField] = useState([]);

  const [trigger, setTrigger] = useState(0);
  const [frequency, setFrequency] = useState(0);

  const [numberDateYearly, setNumberDateYearly] = useState([""]);
  const [dateYearly, setDateYearly] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      if (isDynamicButton) {
        dispatch(
          createDynamicButton({
            object_id: values.object,
            name: values.name,
            description: values.description,
            accessed_by: values.access_by,
            dynamic_field: dynamicField,
            filter_condition: {
              and_filter: allCondition,
              or_filter: anyCondition,
            },
          })
        );
      } else {
        let data = {
          workflow_name: values.name,
          description: values.description,
          execution_condition: values.trigger,
          object_id: values.object,
          filter_condition: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
        };

        if (values.trigger === 4) {
          switch (values.frequency) {
            case 1:
              data = {
                ...data,
                sch_annual_dates: [],
                sch_day_of_month: [],
                sch_day_of_week: [],
                sch_type_id: 1,
                schedule_time:
                  "00:" + moment(values.timeHourly).format("mm:ss"),
              };

              break;
            case 2:
              data = {
                ...data,
                sch_annual_dates: [],
                sch_day_of_month: [],
                sch_day_of_week: [],
                sch_type_id: 2,
                schedule_time: moment(values.timeDaily).format("HH:mm:ss"),
              };
              break;
            case 3:
              data = {
                ...data,
                sch_annual_dates: [],
                sch_day_of_month: [],
                sch_day_of_week: values.dateWeekly,
                sch_type_id: 3,
                schedule_time: moment(values.timeWeekly).format("HH:mm:ss"),
              };
              break;
            case 4:
              data = {
                ...data,
                sch_annual_dates: [
                  moment(values.dateSpecific).format("YYYY-MM-DD"),
                ],
                sch_day_of_month: [],
                sch_day_of_week: [],
                sch_type_id: 4,
                schedule_time: moment(values.timeSpecific).format("HH:mm:ss"),
              };
              break;
            case 5:
              data = {
                ...data,
                sch_annual_dates: [],
                sch_day_of_month: values.dateMonthly,
                sch_day_of_week: [],
                sch_type_id: 5,
                schedule_time: moment(values.timeMonthly).format("HH:mm:ss"),
              };

              break;
            case 6:
              data = {
                ...data,
                sch_annual_dates: dateYearly,
                sch_day_of_month: [],
                sch_day_of_week: [],
                sch_type_id: 6,
                schedule_time: moment(values.timeYearly).format("HH:mm:ss"),
              };
              break;

            default:
              break;
          }
        } else if (values.trigger === 5) {
          data = {
            ...data,
            check_field_changed: values.check_field_changed,
            sch_annual_dates: [],
            sch_day_of_month: [],
            sch_day_of_week: [],
            sch_type_id: 0,
            schedule_time: "",
          };
        } else if (values.trigger === 6) {
          data = {
            ...data,
            interaction_type: values.interaction_type,
            sch_annual_dates: [],
            sch_day_of_month: [],
            sch_day_of_week: [],
            sch_type_id: 0,
            schedule_time: "",
          };
        } else if (values.trigger === 7) {
          data = {
            ...data,
            url_popup: _.get(values, "url_popup"),
            sch_annual_dates: [],
            sch_day_of_month: [],
            sch_day_of_week: [],
            sch_type_id: 0,
            schedule_time: "",
          };
        } else {
          data = {
            ...data,
            sch_annual_dates: [],
            sch_day_of_month: [],
            sch_day_of_week: [],
            sch_type_id: 0,
            schedule_time: "",
          };
        }
        dispatch(
          createWorkflow({
            ...data,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    if (isDynamicButton) {
      dispatch(setShowModalAdd(false));
    } else {
      setShowModalAdd(false);
    }
    form.resetFields();
    setTrigger(0);
    setFrequency(0);
    setDateYearly([]);
    setNumberDateYearly([""]);
    setAllCondition([]);
    setAnyCondition([]);
    dispatch(loadListObjectFieldSuccess([]));
  };

  const handleSelectTrigger = (value) => {
    setTrigger(value);

    form.setFieldsValue({
      frequency: undefined,
    });
    setFrequency(0);
  };

  const handleSelectFrequency = (value) => {
    setFrequency(value);

    form.setFieldsValue({
      timeHourly: undefined,
      timeDaily: undefined,
      dateWeekly: undefined,
      timeWeekly: undefined,
      dateSpecific: undefined,
      timeSpecific: undefined,
      dateMonthly: undefined,
      timeMonthly: undefined,
      timeYearly: undefined,
    });

    numberDateYearly.forEach((item, idx) => {
      form.setFieldsValue({
        ["dateYearly" + idx]: undefined,
      });
    });

    setDateYearly([]);
    setNumberDateYearly([""]);
  };

  const _onAddDate = () => {
    let tempList = [...numberDateYearly];
    tempList.push("");
    setNumberDateYearly(tempList);
  };

  const _onDeleteDate = (idx) => {
    let tempList = [...numberDateYearly];
    tempList.splice(idx, 1);

    let tempValue = [...dateYearly];
    tempValue.splice(idx, 1);

    tempValue.forEach((item, index) => {
      form.setFieldsValue({
        ["dateYearly" + index]: moment(item),
      });
      if (index === tempValue.length - 1) {
        let tempField = index + 1;
        form.setFieldsValue({
          ["dateYearly" + tempField]: undefined,
        });
      }
    });

    setDateYearly(tempValue);
    setNumberDateYearly(tempList);
  };

  const handleSelectDateYearly = (idx, dateString) => {
    let tempList = [...dateYearly];
    tempList[idx] = dateString;
    setDateYearly(tempList);
  };

  const disabledDate = (current) => {
    return dateYearly.find(
      (date) => date === moment(current).format("YYYY-MM-DD")
    );
  };

  const loadFields = (value) => {
    dispatch(
      loadFieldsCondition({
        object_id: value,
        api_version: "2",
        show_meta_fields: true,
      })
    );
    dispatch(
      getListPopup({
        object_id: value,
      })
    );
    $dynamicField([]);
  };

  const tagRender = (props) => {
    const { closable, onClose } = props;
    return (
      <Tag
        closable={closable}
        onClose={onClose}
        style={{
          marginRight: "4px",
          marginTop: "2px",
          marginBottom: "2px",
          height: "24px",
          background: "#f5f5f5",
          border: "1px solid #f0f0f0",
          borderRadius: "2px",
          fontSize: "14px",
          display: "flex",
          alignItems: "center",
        }}
      >
        {_.get(props, "label.props.children[0]", "")}
      </Tag>
    );
  };

  const onChangeDynamicField = (value) => {
    const newValues = value.map((item) => ({
      field_id: item,
      required: _.find(dynamicField, (field) => field.field_id === item)
        ? _.find(dynamicField, (field) => field.field_id === item).required
        : false,
    }));
    $dynamicField(newValues);
  };

  const onChangeRequired = (value) => {
    return (e) => {
      const index = dynamicField.findIndex((field) => field.field_id === value);
      if (index !== -1) {
        let newDynamicField = [...dynamicField];
        newDynamicField[index] = {
          ...newDynamicField[index],
          required: e.target.checked,
        };
        $dynamicField(newDynamicField);
      }
    };
  };

  useEffect(() => {
    if (Object.keys(objects).length > 0) {
      let tempObjects = [];
      Object.entries(objects).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [objects]);

  useEffect(() => {
    let tempGroup = [];

    listAllGroups.forEach((item) => {
      tempGroup.push({
        label: item.name,
        value: item._id,
      });
    });
    setListGroups(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  is_editor: _.get(field, "is_editor", false),
                });
              }
            });
          });
        }
      }
    });

    setListMainFields(tempOptionsFields);
  }, [listObjectField]);

  return (
    <ModalCustom
      title={
        isDynamicButton
          ? t("dynamicButton.addDynamic")
          : t("workflow.addworkflow")
      }
      visible={showModalAdd}
      footer={null}
      width={688}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 10 }}
        wrapperCol={{ span: 14 }}
        colon={false}
        labelAlign="left"
      >
        <BasicInfo>
          <legend>{t("workflow.basicInfo")}</legend>
          {isDynamicButton ? (
            <>
              <Form.Item
                label={t("dynamicButton.buttonName")}
                name="name"
                rules={[
                  {
                    required: true,
                    message: t("common.placeholderInput"),
                  },
                ]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
              <Form.Item label={t("common.description")} name="description">
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
              <Form.Item
                label={t("object.object")}
                name="object"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listObjects}
                  onChange={loadFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item
                label={t("dynamicButton.accessBy")}
                name="access_by"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listGroups}
                  mode="multiple"
                  optionFilterProp="label"
                />
              </Form.Item>

              <Form.Item
                label={t("dynamicButton.updateFields")}
                name="dynamic_field"
                valuePropName="option"
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  onChange={onChangeDynamicField}
                  options={listMainFields
                    ?.filter(
                      (item) =>
                        (item.type === "text" ||
                          item.type === "textarea" ||
                          item.type === "email" ||
                          item.type === "number" ||
                          item.type === "select" ||
                          item.type === "date" ||
                          item.type === "datetime-local") &&
                        item.value !== "created_date" &&
                        item.value !== "modify_time"
                    )
                    ?.map(
                      (item) =>
                        (item = {
                          label: (
                            <div
                              style={{
                                display: "flex",
                                justifyContent: "space-between",
                              }}
                            >
                              {item.label}
                              <Checkbox
                                disabled={item.is_editor}
                                checked={dynamicField.find(
                                  (field) =>
                                    field.field_id === item.value &&
                                    field.required
                                )}
                                onChange={onChangeRequired(item.value)}
                              >
                                {t("form.isRequired")}
                              </Checkbox>
                            </div>
                          ),
                          value: item.value,
                        })
                    )}
                  tagRender={tagRender}
                  mode="multiple"
                  value={dynamicField.map((item) => item.field_id)}
                />
              </Form.Item>
            </>
          ) : (
            <>
              <Form.Item
                label={t("workflow.nameWorkflow")}
                name="name"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input placeholder={t("workflow.enterWorkflow")} />
              </Form.Item>
              <Form.Item label={t("common.description")} name="description">
                <Input placeholder={t("workflow.writeDescription")} />
              </Form.Item>
              <Form.Item
                label={t("object.object")}
                name="object"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("workflow.selectObject")}
                  options={listObjects}
                  onChange={loadFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
            </>
          )}
        </BasicInfo>

        {!isDynamicButton && (
          <Trigger>
            <legend>{t("workflow.workflowTrigger")}</legend>
            <Form.Item
              label={t("workflow.startWhen")}
              name="trigger"
              rules={[
                { required: true, message: t("common.placeholderSelect") },
              ]}
            >
              <Select
                onChange={handleSelectTrigger}
                placeholder={t("workflow.selectWorkflow")}
              >
                {triggerWorkflow.map((item, index) => {
                  return (
                    <Option key={index} value={item.value}>
                      {t(item.label)}
                    </Option>
                  );
                })}
              </Select>
            </Form.Item>

            {trigger === 7 && (
              <Form.Item
                label={t("workflow.urlPopup")}
                name="url_popup"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={_.map(listPopup, (obj) =>
                    _.mapValues(
                      _.mapKeys(obj, (value, key) => {
                        if (key === "name") {
                          return "label";
                        } else if (key === "ID") {
                          return "value";
                        }
                        return key;
                      })
                    )
                  )}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
            )}

            {trigger === 6 && (
              <Form.Item
                label={t("highlightSetting.selectType")}
                name="interaction_type"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={typeInteraction}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
            )}

            {trigger === 5 && (
              <Form.Item
                label={t("workflow.selectField")}
                name="check_field_changed"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={listMainFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
            )}

            {trigger === 4 && (
              <Frequency>
                <legend>{t("workflow.Frequency")}</legend>
                <Form.Item
                  label={t("workflow.SelectFrequency")}
                  name="frequency"
                  rules={[
                    { required: true, message: t("common.placeholderSelect") },
                  ]}
                >
                  <Select
                    onChange={handleSelectFrequency}
                    placeholder={t("common.placeholderSelect")}
                  >
                    {frequencyWorkflow.map((item, index) => {
                      return (
                        <Option key={index} value={item.value}>
                          {t(item.label)}
                        </Option>
                      );
                    })}
                  </Select>
                </Form.Item>

                {frequency === 1 && (
                  <Form.Item
                    label={t("workflow.AtTime")}
                    name="timeHourly"
                    rules={[
                      {
                        required: true,
                        message: t("common.placeholderSelect"),
                      },
                    ]}
                  >
                    <TimePicker format="mm:ss" placeholder="mm:ss" />
                  </Form.Item>
                )}

                {frequency === 2 && (
                  <Form.Item
                    label={t("workflow.AtTime")}
                    name="timeDaily"
                    rules={[
                      {
                        required: true,
                        message: t("common.placeholderSelect"),
                      },
                    ]}
                  >
                    <TimePicker placeholder="HH:mm:ss" />
                  </Form.Item>
                )}

                {frequency === 3 && (
                  <>
                    <Form.Item
                      label={t("workflow.OnTheseDays")}
                      name="dateWeekly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <Select
                        mode="multiple"
                        placeholder={t("common.placeholderSelect")}
                        optionFilterProp="label"
                        showSearch
                      >
                        {" "}
                        {week.map((item, index) => {
                          return (
                            <Option key={index} value={item.value}>
                              {t(item.label)}
                            </Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeWeekly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker placeholder="HH:mm:ss" />
                    </Form.Item>
                  </>
                )}

                {frequency === 4 && (
                  <>
                    <Form.Item
                      label={t("workflow.ChooseDay")}
                      name="dateSpecific"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <DatePicker placeholder={t("common.placeholderSelect")} />
                    </Form.Item>
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeSpecific"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker placeholder="HH:mm:ss" />
                    </Form.Item>
                  </>
                )}
                {frequency === 5 && (
                  <>
                    <Form.Item
                      label={t("workflow.ChooseDay")}
                      name="dateMonthly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <Select
                        options={days}
                        mode="multiple"
                        placeholder={t("common.placeholderSelect")}
                        optionFilterProp="label"
                        showSearch
                      />
                    </Form.Item>
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeMonthly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker placeholder="HH:mm:ss" />
                    </Form.Item>
                  </>
                )}

                {frequency === 6 && (
                  <>
                    {numberDateYearly.map((item, idx) => {
                      return (
                        <>
                          <Form.Item
                            label={
                              idx === numberDateYearly.length - 1 ? (
                                <>
                                  <span>{t("workflow.ChooseDay")}</span>
                                  <AddDate onClick={() => _onAddDate()}>
                                    <Tooltip title={t("workflow.addDate")}>
                                      <span>+</span>
                                    </Tooltip>
                                  </AddDate>
                                  {numberDateYearly.length > 1 && (
                                    <DeleteDate
                                      onClick={() => _onDeleteDate(idx)}
                                    >
                                      <Tooltip title={t("workflow.deleteDate")}>
                                        <span>x</span>
                                      </Tooltip>
                                    </DeleteDate>
                                  )}
                                </>
                              ) : (
                                <>
                                  <span>{t("workflow.ChooseDay")}</span>
                                  {numberDateYearly.length > 1 && (
                                    <DeleteDate
                                      onClick={() => _onDeleteDate(idx)}
                                    >
                                      <Tooltip title={t("workflow.deleteDate")}>
                                        <span>x</span>
                                      </Tooltip>
                                    </DeleteDate>
                                  )}
                                </>
                              )
                            }
                            name={`dateYearly${idx}`}
                            rules={[
                              {
                                required: true,
                                message: t("common.placeholderSelect"),
                              },
                            ]}
                          >
                            <DatePicker
                              placeholder={t("common.placeholderSelect")}
                              onChange={(value, dateString) =>
                                handleSelectDateYearly(idx, dateString)
                              }
                              disabledDate={disabledDate}
                            />
                          </Form.Item>
                        </>
                      );
                    })}
                    <Form.Item
                      label={t("workflow.AtTime")}
                      name="timeYearly"
                      rules={[
                        {
                          required: true,
                          message: t("common.placeholderSelect"),
                        },
                      ]}
                    >
                      <TimePicker placeholder="HH:mm:ss" />
                    </Form.Item>
                  </>
                )}
              </Frequency>
            )}
          </Trigger>
        )}

        <Condition>
          <legend>
            {isDynamicButton
              ? t("dynamicButton.dynamicConditions")
              : t("workflow.workflowConditions")}
          </legend>
          <Conditions
            title={t("common.allCondition")}
            decs={`(${t("common.descAllCondition")})`}
            conditions={allCondition}
            setConditions={setAllCondition}
            operatorValue={operatorValueAnd}
            setOperatorValue={setOperatorValueAnd}
            value={valueAnd}
            setValue={setValueAnd}
          />
          <Conditions
            title={t("common.anyCondition")}
            decs={`(${t("common.descAnyCondition")})`}
            conditions={anyCondition}
            setConditions={setAnyCondition}
            operatorValue={operatorValueOr}
            setOperatorValue={setOperatorValueOr}
            value={valueOr}
            setValue={setValueOr}
          />
        </Condition>

        <WrapButton label=" ">
          <Button
            type="primary"
            htmlType="submit"
            loading={loading.modalCreate}
          >
            {t("common.saveNext")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalAddWorkflow);

ModalAddWorkflow.defaultProps = {
  showModalAdd: false,
  objects: {},
  isDynamicButton: false,
  listAllGroups: [],
  setShowModalAdd: () => {},
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const BasicInfo = styled.fieldset`
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 14px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const Trigger = styled(BasicInfo)``;

const Condition = styled(BasicInfo)``;

const Frequency = styled(BasicInfo)`
  .ant-picker {
    width: 100%;
  }
`;

const AddDate = styled.div`
  margin-left: 8px;
  width: 20px;
  height: 20px;
  background: #fff;
  border: 1px solid ${(props) => props.theme.main};
  color: ${(props) => props.theme.main};
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;

  span {
    padding-bottom: 4px;
  }

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
    cursor: pointer;
  }
`;

const DeleteDate = styled(AddDate)``;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
