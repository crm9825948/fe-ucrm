import { Form, Input, Select, Steps, Switch } from "antd";
import Breadcrumb from "antd/lib/breadcrumb";
import { useForm } from "antd/lib/form/Form";
import img from "assets/icons/common/confirm.png";
import iconEmpty from "assets/icons/interaction/empty.png";
import iconSetting from "assets/icons/interaction/setting.png";
import iconWarning from "assets/icons/interaction/warning.png";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  createInteractionConfig,
  updateInteractionConfig,
  deleteInteractionConfig,
  interactionConfig,
} from "redux/slices/consolidatedViewSettings";
import { loadListObjectField } from "redux/slices/objects";
import styled from "styled-components";
import _ from "lodash";

const keysToDelete = [
  "totalDuration_talkover",
  "maxDuration_talkover",
  "minDuration_talkover",
  "avgDuration_talkover",
  "totalEvents_talkover",
  "percentOfCall_talkover",
  "totalDuration_silence",
  "maxDuration_silence",
  "minDuration_silence",
  "avgDuration_silence",
  "totalEvents_silence",
  "percentOfCall_silence",
  "totalDuration_pause",
  "maxDuration_pause",
  "minDuration_pause",
  "avgDuration_pause",
  "totalEvents_pause",
  "percentOfCall_pause",
  "totalDuration_hold",
  "maxDuration_hold",
  "minDuration_hold",
  "avgDuration_hold",
  "totalEvents_hold",
  "percentOfCall_hold",
];

const { Step } = Steps;
const { Option, OptGroup } = Select;
const Interaction = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const [current, setCurrent] = useState(0);
  const [form] = useForm();
  const [allObjects, setAllObjects] = useState({});
  const [openForce, setOpenForce] = useState(false);
  const [openDelete, setOpenDelete] = useState(false);
  const [objectId, setObjectId] = useState("");
  const [formValue, setFormValue] = useState({});
  const [createMode, setCreateMode] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [data, $data] = useState({});

  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "interaction_setting" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    let objectList = {};
    Object.entries(category).map(([key, value], idx) => {
      return (
        <OptGroup label={key}>
          {/* eslint-disable-next-line*/}
          {value.map((object, index) => {
            if (object.Status && !object?.is_embed_iframe) {
              objectList[object._id] = { ...object };
            }
          })}
        </OptGroup>
      );
    });
    setAllObjects({ ...objectList });
  }, [category]);

  useEffect(() => {
    dispatch(interactionConfig());
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(interactionConfigState).length === 0) {
      setCreateMode(true);
      setEditMode(false);
      form.resetFields();
      setFormValue({});
      setObjectId("");
    } else {
      form.resetFields();
      dispatch(
        loadListObjectField({
          object_id: interactionConfigState.object_id,
          api_version: "2",
          show_meta_fields: true,
        })
      );
      setEditMode(true);
      setCreateMode(false);
      // setFormValue(interactionConfigState.field_mapping);
      form.setFieldsValue(interactionConfigState.field_mapping);
      const fieldMapping = interactionConfigState?.field_mapping;

      const temp = fieldMapping?.eventCalculations
        ? Object.keys(fieldMapping?.eventCalculations)?.reduce(
            (values, key) => {
              if (["talkover", "silence", "hold", "pause"].includes(key)) {
                return {
                  ...values,
                  [`totalDuration_${key}`]:
                    fieldMapping.eventCalculations[key]?.totalDuration || "",
                  [`maxDuration_${key}`]:
                    fieldMapping.eventCalculations[key]?.maxDuration || "",
                  [`minDuration_${key}`]:
                    fieldMapping.eventCalculations[key]?.minDuration || "",
                  [`avgDuration_${key}`]:
                    fieldMapping.eventCalculations[key]?.avgDuration || "",
                  [`totalEvents_${key}`]:
                    fieldMapping.eventCalculations[key]?.totalEvents || "",
                  [`percentOfCall_${key}`]:
                    fieldMapping.eventCalculations[key]?.percentOfCall || "",
                };
              }

              return values;
            },
            {}
          )
        : undefined;

      const formValues =
        temp === undefined
          ? {
              source_object_interaction:
                interactionConfigState?.source_object_interaction,
            }
          : {
              source_object_interaction:
                interactionConfigState?.source_object_interaction,
              ...temp,
            };

      setFormValue({
        ...interactionConfigState.field_mapping,
        ...formValues,
      });
      form.setFieldsValue(formValues);
      setObjectId(interactionConfigState.object_id);
    } // eslint-disable-next-line
  }, [interactionConfigState]);

  function checkUndefinedValues(obj) {
    let checkUndefined = false;
    Object.entries(obj).forEach(([key, value]) => {
      if (!keysToDelete.includes(key) && typeof value === "undefined") {
        checkUndefined = true;
      }
    });

    return checkUndefined;
  }

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>Interaction</BreadcrumbItem>
        </Breadcrumb>
      </WrapBreadcrumb>

      {createMode ? (
        <div className="wrapper-add">
          <div className="item-1">
            <img alt="" src={iconSetting} />
            {t("Interaction.interactionSetting")}
          </div>
          <div className="item-2">{t("Interaction.decs")}</div>
          <div className="item-3">
            <img alt="" src={iconEmpty} />
          </div>
          <div className="item-4">
            <span> {t("Interaction.pleaseEnable")}</span>{" "}
            <span>{t("Interaction.interaction")}</span>
          </div>
          {checkRule("create") ? (
            <button
              className="enable-btn"
              onClick={() => {
                setCreateMode(false);
              }}
            >
              {t("Interaction.enableNow")}
            </button>
          ) : (
            ""
          )}
        </div>
      ) : (
        <div className="wrapper-sync">
          <Switch
            disabled={!checkRule("delete")}
            checked={
              Object.keys(interactionConfigState).length === 0 ||
              editMode === true
            }
            onClick={() => {
              if (Object.keys(interactionConfigState).length > 0) {
                setOpenDelete(true);
              }
            }}
          />{" "}
          <span className="sync-1"> {t("Interaction.syncInteraction")}</span>
          <div className="sync-2">{t("Interaction.settingRelevant")}</div>
          <div className="sync-3">
            {t("Interaction.chooseObject")}
            <span>*</span>
          </div>
          <Select
            style={{ width: "385px" }}
            optionFilterProp="children"
            showSearch
            value={objectId}
            disabled={editMode}
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
            onChange={(e) => {
              dispatch(
                loadListObjectField({
                  object_id: e,
                  api_version: "2",
                  show_meta_fields: true,
                })
              );
              setObjectId(e);
              form.resetFields();
            }}
          >
            {Object.entries(category).map(([key, value], idx) => {
              return (
                <OptGroup label={key}>
                  {/* eslint-disable-next-line*/}
                  {value.map((object, index) => {
                    if (object.Status && !object?.is_embed_iframe)
                      return <Option value={object._id}>{object.Name}</Option>;
                  })}
                </OptGroup>
              );
            })}
          </Select>
          <div className="steps-wrapper">
            <Steps current={current}>
              <Step
                title={t("Interaction.mappingRule")}
                description={
                  current === 0
                    ? t("Interaction.inprogress")
                    : current === 1
                    ? t("Interaction.finish")
                    : ""
                }
              />
              <Step
                title={t("Interaction.relationship")}
                description={
                  current === 0
                    ? t("Interaction.waiting")
                    : current === 1
                    ? t("Interaction.inprogress")
                    : ""
                }
              />
            </Steps>
          </div>
          {current === 0 ? (
            <div className="form-wrapper">
              <Form
                layout="vertical"
                form={form}
                onValuesChange={(values, allValues) => {
                  setFormValue(allValues);
                }}
              >
                <div className="left">
                  <Form.Item
                    name="source_object_interaction"
                    label="Source Object Interaction"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="source_interaction_id"
                    label="Source interaction ID"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="type"
                    label="Type"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="incoming_type"
                    label="Incoming type"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="outgoing_type"
                    label="Outgoing type"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="start_time"
                    label="Start time"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          if (option.type === "datetime-local")
                            return (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={
                                  Object.values(formValue).includes(option.ID)
                                    ? true
                                    : false
                                }
                              >
                                {option.name}
                              </Option>
                            );
                          return null;
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="end_time"
                    label="Endtime"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          if (option.type === "datetime-local")
                            return (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={
                                  Object.values(formValue).includes(option.ID)
                                    ? true
                                    : false
                                }
                              >
                                {option.name}
                              </Option>
                            );
                          return null;
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="object_id"
                    label="Source Object ID"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="record_id"
                    label="Source Record ID"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="third_party_id"
                    label="Call ID"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="call_id"
                    label="Transfer call"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="call_status"
                    label="Call status"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="reply_to"
                    label="Reply to"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="cc_email"
                    label="CC Email"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <div
                    style={{
                      border: "1px solid #ECECEC",
                      marginBottom: "24px",
                    }}
                  >
                    <div
                      style={{
                        borderRadius: "2px",
                        background: "#E9F6F6",
                        padding: "8px 8px 8px 16px",
                        fontSize: "16px",
                        fontFamily: "var(--roboto-500)",
                        marginBottom: "16px",
                      }}
                    >
                      Talk over
                    </div>

                    <div
                      style={{
                        padding: "0 16px",
                      }}
                    >
                      <Form.Item
                        name="totalDuration_talkover"
                        label="Total duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="maxDuration_talkover"
                        label="Max duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="minDuration_talkover"
                        label="Min duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="avgDuration_talkover"
                        label="Average duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="totalEvents_talkover"
                        label="Total events"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="percentOfCall_talkover"
                        label="Percent of call"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </div>
                  </div>
                  <div
                    style={{
                      border: "1px solid #ECECEC",
                      marginBottom: "24px",
                    }}
                  >
                    <div
                      style={{
                        borderRadius: "2px",
                        background: "#E9F6F6",
                        padding: "8px 8px 8px 16px",
                        fontSize: "16px",
                        fontFamily: "var(--roboto-500)",
                        marginBottom: "16px",
                      }}
                    >
                      Pause
                    </div>

                    <div
                      style={{
                        padding: "0 16px",
                      }}
                    >
                      <Form.Item
                        name="totalDuration_pause"
                        label="Total duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="maxDuration_pause" label="Max duration">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="minDuration_pause" label="Min duration">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="avgDuration_pause"
                        label="Average duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="totalEvents_pause" label="Total events">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="percentOfCall_pause"
                        label="Percent of call"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </div>
                  </div>
                </div>
                <div className="right">
                  <Form.Item
                    name="type_call"
                    label="Type call"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="extension"
                    label="Extension"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="channel_type"
                    label="Channel type"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="username"
                    label="Username"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="tenant_id"
                    label="Tenant ID"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>{" "}
                  <Form.Item
                    name="content_file_url"
                    label="Content File URL"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          if (option.type === "file")
                            return (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={
                                  Object.values(formValue).includes(option.ID)
                                    ? true
                                    : false
                                }
                              >
                                {option.name}
                              </Option>
                            );
                          return null;
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="content_file_type"
                    label="Content File Type"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="content_file_name"
                    label="Content File Name"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="content_download_url"
                    label="Content Download Url"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {" "}
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          if (option.type === "file")
                            return (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={
                                  Object.values(formValue).includes(option.ID)
                                    ? true
                                    : false
                                }
                              >
                                {option.name}
                              </Option>
                            );
                          return null;
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="phone"
                    label="Phone number"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="from"
                    label="From"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="to"
                    label="To"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="bcc_email"
                    label="BCC Email"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <Form.Item
                    name="subject"
                    label="Subject"
                    rules={[
                      {
                        required: true,
                      },
                    ]}
                  >
                    <Select
                      optionFilterProp="children"
                      showSearch
                      filterOption={(inputValue, option) => {
                        if (option.children) {
                          return option.children
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        } else if (option.label) {
                          return option.label
                            .toLowerCase()
                            .indexOf(inputValue.toLowerCase()) >= 0
                            ? true
                            : false;
                        }
                      }}
                    >
                      {listObjectField[
                        listObjectField.length - 1
                      ]?.main_object?.sections.map((item, idx) => {
                        return item.fields.map((option, index) => {
                          return (
                            <Option
                              value={option.ID}
                              key={index}
                              disabled={
                                Object.values(formValue).includes(option.ID)
                                  ? true
                                  : false
                              }
                            >
                              {option.name}
                            </Option>
                          );
                        });
                      })}
                    </Select>
                  </Form.Item>
                  <div
                    style={{
                      border: "1px solid #ECECEC",
                      marginBottom: "24px",
                    }}
                  >
                    <div
                      style={{
                        borderRadius: "2px",
                        background: "#E9F6F6",
                        padding: "8px 8px 8px 16px",
                        fontSize: "16px",
                        fontFamily: "var(--roboto-500)",
                        marginBottom: "16px",
                      }}
                    >
                      Silence
                    </div>

                    <div
                      style={{
                        padding: "0 16px",
                      }}
                    >
                      <Form.Item
                        name="totalDuration_silence"
                        label="Total duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="maxDuration_silence"
                        label="Max duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="minDuration_silence"
                        label="Min duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="avgDuration_silence"
                        label="Average duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="totalEvents_silence"
                        label="Total events"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="percentOfCall_silence"
                        label="Percent of call"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </div>
                  </div>
                  <div
                    style={{
                      border: "1px solid #ECECEC",
                      marginBottom: "24px",
                    }}
                  >
                    <div
                      style={{
                        borderRadius: "2px",
                        background: "#E9F6F6",
                        padding: "8px 8px 8px 16px",
                        fontSize: "16px",
                        fontFamily: "var(--roboto-500)",
                        marginBottom: "16px",
                      }}
                    >
                      Hold
                    </div>

                    <div
                      style={{
                        padding: "0 16px",
                      }}
                    >
                      <Form.Item
                        name="totalDuration_hold"
                        label="Total duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="maxDuration_hold" label="Max duration">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="minDuration_hold" label="Min duration">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="avgDuration_hold"
                        label="Average duration"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item name="totalEvents_hold" label="Total events">
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      <Form.Item
                        name="percentOfCall_hold"
                        label="Percent of call"
                      >
                        <Select
                          optionFilterProp="children"
                          showSearch
                          filterOption={(inputValue, option) => {
                            if (option.children) {
                              return option.children
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            } else if (option.label) {
                              return option.label
                                .toLowerCase()
                                .indexOf(inputValue.toLowerCase()) >= 0
                                ? true
                                : false;
                            }
                          }}
                        >
                          {listObjectField[
                            listObjectField.length - 1
                          ]?.main_object?.sections
                            .flatMap((item) =>
                              item.fields.filter(
                                (option) => option.type === "number"
                              )
                            )
                            .map((option, index) => (
                              <Option
                                value={option.ID}
                                key={index}
                                disabled={Object.values(formValue).includes(
                                  option.ID
                                )}
                              >
                                {option.name}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                    </div>
                  </div>
                </div>
              </Form>
            </div>
          ) : (
            <div>
              {listObjectField[
                listObjectField.length - 1
              ]?.main_object?.sections.map((item, idx) => {
                return item.fields.map((option, index) => {
                  if (option.type === "linkingobject" && option.key === true) {
                    return (
                      <div className="title">
                        <div className="title-1">
                          {allObjects[option.objectname].Name}
                        </div>
                        <Input value={option.name} disabled />
                      </div>
                    );
                  }

                  return null;
                });
              })}
              <div className="warning">
                <img alt="" src={iconWarning} />
                {t("Interaction.decs1")}
              </div>
            </div>
          )}
          <div className="btn-wrapper">
            {editMode === false ? (
              <button
                className="btn-cancel"
                onClick={() => {
                  setCreateMode(true);
                  form.resetFields();
                }}
              >
                {t("Interaction.cancel")}
              </button>
            ) : (
              ""
            )}
            {current === 1 ? (
              <button
                className="btn-back"
                onClick={() => {
                  setCurrent(0);
                }}
              >
                {t("Interaction.back")}
              </button>
            ) : (
              ""
            )}
            {current === 0 ? (
              <button
                className="btn-next"
                onClick={() => {
                  let tempFieldMapping = { ...formValue };
                  let updatedObj = _.omit(tempFieldMapping, keysToDelete);
                  updatedObj = {
                    ...updatedObj,
                    eventCalculations: {
                      talkover: {
                        totalDuration: _.get(
                          formValue,
                          "totalDuration_talkover"
                        ),
                        maxDuration: _.get(formValue, "maxDuration_talkover"),
                        minDuration: _.get(formValue, "minDuration_talkover"),
                        avgDuration: _.get(formValue, "avgDuration_talkover"),
                        totalEvents: _.get(formValue, "totalEvents_talkover"),
                        percentOfCall: _.get(
                          formValue,
                          "percentOfCall_talkover"
                        ),
                      },
                      silence: {
                        totalDuration: _.get(
                          formValue,
                          "totalDuration_silence"
                        ),
                        maxDuration: _.get(formValue, "maxDuration_silence"),
                        minDuration: _.get(formValue, "minDuration_silence"),
                        avgDuration: _.get(formValue, "avgDuration_silence"),
                        totalEvents: _.get(formValue, "totalEvents_silence"),
                        percentOfCall: _.get(
                          formValue,
                          "percentOfCall_silence"
                        ),
                      },
                      pause: {
                        totalDuration: _.get(formValue, "totalDuration_pause"),
                        maxDuration: _.get(formValue, "maxDuration_pause"),
                        minDuration: _.get(formValue, "minDuration_pause"),
                        avgDuration: _.get(formValue, "avgDuration_pause"),
                        totalEvents: _.get(formValue, "totalEvents_pause"),
                        percentOfCall: _.get(formValue, "percentOfCall_pause"),
                      },
                      hold: {
                        totalDuration: _.get(formValue, "totalDuration_hold"),
                        maxDuration: _.get(formValue, "maxDuration_hold"),
                        minDuration: _.get(formValue, "minDuration_hold"),
                        avgDuration: _.get(formValue, "avgDuration_hold"),
                        totalEvents: _.get(formValue, "totalEvents_hold"),
                        percentOfCall: _.get(formValue, "percentOfCall_hold"),
                      },
                    },
                  };
                  delete updatedObj["talkover"];
                  delete updatedObj["silence"];
                  delete updatedObj["pause"];
                  delete updatedObj["hold"];

                  let payload = {
                    object_id: objectId,
                    source_object_interaction:
                      formValue.source_object_interaction,
                    field_mapping: {
                      ...updatedObj,
                      source_object_interaction: undefined,
                    },
                  };

                  if (editMode) {
                    payload = {
                      ...payload,
                      _id: _.get(interactionConfigState, "_id"),
                    };
                  }

                  if (checkUndefinedValues(form.getFieldsValue())) {
                    Notification("error", "Please fullfill requred!");
                  } else {
                    $data(payload);
                    setCurrent(1);
                  }
                }}
              >
                {t("Interaction.next")}
              </button>
            ) : (
              ""
            )}
            {current === 1 ? (
              <button
                className={checkRule("edit") ? "btn-next" : "disable btn-next"}
                onClick={() => {
                  setOpenForce(true);
                }}
                disabled={!checkRule("edit")}
              >
                {t("common.save")}
              </button>
            ) : (
              ""
            )}
          </div>
        </div>
      )}

      <ModalConfirm
        title={t("Interaction.confirm3")}
        decs={t("Interaction.confirm2")}
        open={openForce}
        setOpen={setOpenForce}
        method={editMode ? updateInteractionConfig : createInteractionConfig}
        data={data}
        setOpenModal={() => {}}
        img={img}
      />
      <ModalConfirm
        title={t("Interaction.confirm1")}
        decs={t("Interaction.confirm2")}
        open={openDelete}
        setOpen={setOpenDelete}
        method={deleteInteractionConfig}
        data={{}}
        setOpenModal={() => {}}
        img={img}
      />
    </Wrapper>
  );
};

export default withTranslation()(Interaction);

const Wrapper = styled.div`
  padding: 24px;
  background-color: #f9fafc;
  width: 100%;
  min-height: fit-content;
  height: fit-content;
  padding-bottom: 100px;
  svg {
    fill: ${(props) => props.theme.main}!important;
  }
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main}!important;
  }
  .ant-steps-item-title::after {
    background-color: ${(props) => props.theme.main}!important;
  }
  .ant-steps-item-finish {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main};
      color: ${(props) => props.theme.main}!important;
    }
  }

  button:focus-visible {
    outline: none;
  }
  .title {
    color: #2c2c2c;
    margin-bottom: 16px;
    input {
      margin-top: 8px;
    }
  }
  .title-1 {
    font-family: var(--roboto-400);
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */

    /* Character/Body text */

    color: #2c2c2c;
  }
  .warning {
    font-family: var(--roboto-400);
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    margin-top: 20px;
    /* or 150% */

    /* Character/Body text */

    color: #2c2c2c;
    margin-bottom: 48px;
    img {
      margin-right: 10px;
    }
  }
  .wrapper-add {
    margin-top: 80px;
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    .item-1 {
      font-family: var(--roboto-700);
      font-style: normal;
      font-size: 16px;
      line-height: 20px;
      color: #2c2c2c;
      margin-bottom: 10px;
      display: flex;
      justify-content: center;
      align-items: center;
      img {
        margin-right: 10px;
      }
    }
    .item-2 {
      font-family: var(--roboto-400);
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      text-align: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xam */

      color: #6b6b6b;
      margin-bottom: 24px;
    }
    .item-3 {
      img {
        width: 100px;
        height: 100px;
        margin-bottom: 8px;
      }
    }
    .item-4 {
      span:first-child {
        font-family: var(--roboto-400);
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
        line-height: 22px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
      span:last-child {
        font-family: var(--roboto-400);
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
        line-height: 22px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #20a2a2;
      }
      margin-bottom: 24px;
    }
    .enable-btn {
      padding: 5px 16px;
      font-family: var(--roboto-400);
      font-style: normal;
      font-weight: 400;
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 138% */

      text-align: center;

      /* Character/Primary(inverse) */

      color: #ffffff;
      background: #20a2a2;
      border-radius: 2px;
      border: none;
    }
  }
  .wrapper-sync {
    width: calc(100% - 306px);
    margin: auto;
    background: #ffffff;
    /* sh v3 */

    box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
    border-radius: 5px;
    height: fit-content;
    margin-top: 24px;
    padding: 24px 5%;
    .sync-1 {
      font-family: var(--roboto-700);
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 20px;
      /* identical to box height, or 125% */

      /* Character/Color text main */

      color: #2c2c2c;
      margin-left: 18px;
    }
    .sync-2 {
      font-family: var(--roboto-400);
      font-style: normal;
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      font-feature-settings: "tnum" on, "lnum" on;

      /* Sunset Orange / 6 */
      margin-top: 9px;
      color: #fa8c16;
      margin-bottom: 24px;
    }
    .sync-3 {
      font-family: var(--roboto-700);
      font-style: normal;
      font-weight: 700;
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 138% */

      /* Character/Body text */

      color: #2c2c2c;
      margin-bottom: 8px;
      span {
        color: red;
      }
    }
    .steps-wrapper {
      width: 70%;
      padding: 0 167px;

      margin: auto;
      margin-top: 48px;
    }
  }
  .steps-content {
    min-height: 200px;
    margin-top: 16px;
    padding-top: 80px;
    text-align: center;
    background-color: #fafafa;
    border: 1px dashed #e9e9e9;
    border-radius: 2px;
  }
  .ant-steps-item-process {
    .ant-steps-item-container {
      .ant-steps-item-icon {
        background: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.main};
      }
    }
  }
  .form-wrapper {
    margin-top: 24px;
    .ant-form {
      display: flex;
      .ant-form-item-required {
        font-family: var(--roboto-700);
        font-style: normal;
        font-weight: 700;
        font-size: 16px;
        line-height: 22px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
      .left {
        width: 50%;
        padding-right: 16px;
        border-right: 1px solid #ececec;
      }
      .right {
        width: 50%;
        padding-left: 16px;
      }
    }
  }
  .btn-wrapper {
    display: flex;
    justify-content: right;
  }
  .btn-cancel {
    font-family: var(--roboto-400);
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    text-align: center;

    /* Character/Body text */

    color: #2c2c2c;
    padding: 8px 40.5px;
    background: #ffffff;
    /* Neutral/5 */

    border: 1px solid #d9d9d9;
    /* drop-shadow / button-secondary */

    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    border-radius: 2px;
    margin-right: 32px;
  }
  .btn-back {
    font-family: var(--roboto-400);
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    text-align: center;

    /* Character/Body text */

    color: #2c2c2c;
    padding: 8px 40.5px;
    background: #ffffff;
    /* Neutral/5 */

    border: 1px solid #d9d9d9;
    /* drop-shadow / button-secondary */

    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    border-radius: 2px;
  }
  .btn-next {
    margin-left: 32px;
    font-family: var(--roboto-400);
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    text-align: center;

    /* Character / Primary(inverse) */

    color: #ffffff;
    background: ${(props) => props.theme.main};
    /* Primary/6 */

    border: 1px solid #20a2a2;
    /* drop-shadow / button-primary */

    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
    border-radius: 2px;
    padding: 8px 40.5px;
  }

  .disable.btn-next {
    background-color: #ccc;
    color: #fff;
    cursor: not-allowed;
    border: 1px solid #ccc;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
