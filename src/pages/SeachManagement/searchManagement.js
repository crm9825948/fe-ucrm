import { Breadcrumb, Input, Typography, Switch, Checkbox, Tooltip } from "antd";
import ModalConfirmState from "components/Modal/ModalConfirmStateIn";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import styled from "styled-components";
import CheckImg from "assets/icons/common/check.png";
import ImgConfirm from "assets/icons/common/confirm.png";
import {
  loadDataSetting,
  loadListObject,
  loadListObjectSuccess,
  toggleGlobalSearch,
  updateListField,
  updateLoadMoreObject,
  updateSearchManagement,
} from "redux/slices/searchManagement";
import InfiniteScroll from "react-infinite-scroll-component";
import Spin from "antd/lib/spin";
import NoDataImg from "../../assets/icons/common/nodata.png";
import AscendingImg from "../../assets/icons/common/ascending.png";
import DescendingImg from "../../assets/icons/common/descending.png";
import {
  CaretUpOutlined,
  CaretDownOutlined,
  EyeInvisibleOutlined,
} from "@ant-design/icons";
import { useRef } from "react";
import ModalConfirm from "components/Modal/ModalConfirm";

const { Search } = Input;
const { Text: TextComponent } = Typography;

const SearchManagement = () => {
  const { tenantID } = useParams();
  const { t } = useTranslation();
  let navigate = useNavigate();
  const dispatch = useDispatch();
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { listObject, loadMoreObject, allowGlobalSearch, listField } =
    useSelector((state) => state.searchManagementReducer);
  const ref = useRef(null);
  const [objects, setObjects] = useState([]);
  const [hasMore, setHasMore] = useState(false);
  const [page, setPage] = useState(1);
  const [itemActive, setItemActive] = useState("");
  const [sortObject, setSortObject] = useState(true);
  const [sortField, setSortField] = useState(true);
  const [textSearch, setTextSearch] = useState("");
  const [showConfirm, setShowConfirm] = useState(false);
  const [dataConfirm, setDataConfirm] = useState({});

  useEffect(() => {
    if (itemActive) {
      setSortField(true);
      setTextSearch("");
      dispatch(
        loadDataSetting({
          tenant_id: tenantID,
          object_id: itemActive._id,
          search_text: null,
          sort_by: true,
          current_page: 1,
          records_per_page: 500,
        })
      );
    }
    // eslint-disable-next-line
  }, [itemActive, dispatch]);

  useEffect(() => {
    if (objects.length > 0) {
      setItemActive(objects[0]);
    } else {
      setItemActive("");
    }
  }, [objects]);

  useEffect(() => {
    return () => {
      setObjects([]);
      dispatch(loadListObjectSuccess([]));
      setHasMore(false);
    };
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    // if (listObject.length > 0) {
    if (loadMoreObject) {
      const temp = objects.concat(listObject);
      setObjects(temp);
    } else {
      setObjects(listObject);
    }
    // }
    if (listObject.length === 100) {
      setHasMore(true);
    } else {
      setHasMore(false);
    }
    //eslint-disable-next-line
  }, [listObject]);

  useEffect(() => {
    if (tenantID) {
      dispatch(
        loadListObject({
          tenant_id: tenantID,
          search_text: null,
          sort_by: true,
          current_page: 1,
          records_per_page: 100,
        })
      );
    }
  }, [tenantID, dispatch]);

  const nonAccentVietnamese = (value) => {
    value = value.toLowerCase();
    value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    value = value.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    value = value.replace(/đ/g, "d");
    value = value.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
    value = value.replace(/\u02C6|\u0306|\u031B/g, "");

    return value;
  };

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  const renderListObject = (list) =>
    list.map((item, index) => (
      <ItemObject
        active={itemActive._id === item._id}
        disable={!item.active}
        onClick={() => {
          setItemActive(item);
        }}
        key={index}
        color={hexToRGB(defaultBrandName.theme_color)}
      >
        <TextComponent ellipsis={{ tooltip: item.Name }}>
          {item.Name}
        </TextComponent>
        {(!item.Status || !item.visible) && <EyeInvisibleOutlined />}
      </ItemObject>
    ));

  const checkType = (type) => {
    if (
      type === "id" ||
      type === "select" ||
      type === "email" ||
      type === "text" ||
      type === "linkingobject"
    ) {
      return true;
    } else {
      return false;
    }
  };
  const handleCheckDiasable = (type) => {
    if (!allowGlobalSearch) {
      return true;
    } else {
      return !checkType(type);
    }
  };
  const handleChangeListField = (id, value) => {
    let temp = [];
    listField.forEach((item, index) => {
      if (item._id === id) {
        temp.push({
          ...item,
          is_searchable: value,
        });
      } else {
        temp.push(item);
      }
    });
    dispatch(updateListField(temp));
  };
  const renderListField = (list) => {
    return list.map(
      (item, index) =>
        nonAccentVietnamese(item.name).includes(
          nonAccentVietnamese(textSearch)
        ) && (
          <Tooltip
            overlayInnerStyle={{ width: "400px" }}
            key={index}
            title={
              item.is_searchable &&
              !item.is_indexed &&
              "This field is searchable but not indexed. Please contact the administrator to index it in the database."
            }
          >
            <tr key={index}>
              <td>
                <TextComponent ellipsis={{ tooltip: item.name }}>
                  {item.name}
                </TextComponent>
              </td>
              <td>{item.type}</td>
              <td>{item.required && <img src={CheckImg} alt="check" />}</td>
              <td>{item.hidden && <img src={CheckImg} alt="check" />}</td>
              <td>
                {item.active === false && <img src={CheckImg} alt="check" />}
              </td>
              <td>
                <Tooltip
                  title={checkType(item.type) ? "" : t("searchManagement.noti")}
                >
                  <Checkbox
                    disabled={handleCheckDiasable(item.type)}
                    checked={item.is_searchable}
                    onChange={(e) => {
                      handleChangeListField(item._id, e.target.checked);
                    }}
                  />
                </Tooltip>
              </td>
              <td>{item.is_indexed && <img src={CheckImg} alt="check" />}</td>
            </tr>
          </Tooltip>
        )
    );
  };

  const handleLoadMore = (type) => {
    if (type === "object") {
      const temp = page + 1;
      dispatch(updateLoadMoreObject(true));
      dispatch(
        loadListObject({
          tenant_id: tenantID,
          search_text: null,
          sort_by: sortObject,
          current_page: temp,
          records_per_page: 100,
        })
      );
      setPage(temp);
    }
  };
  const handleSearch = (text, sort) => {
    setPage(1);
    dispatch(updateLoadMoreObject(false));
    dispatch(
      loadListObject({
        tenant_id: tenantID,
        search_text: text,
        sort_by: sort,
        current_page: 1,
        records_per_page: 100,
      })
    );
  };

  const handleSortField = (text, sort) => {
    setTextSearch(nonAccentVietnamese(text));
    dispatch(
      loadDataSetting({
        tenant_id: tenantID,
        object_id: itemActive._id,
        search_text: null,
        sort_by: sort,
        current_page: 1,
        records_per_page: 500,
      })
    );
  };

  const handleUpdateData = () => {
    let temp = [];
    listField.forEach((item) => {
      if (item.is_searchable) {
        temp.push({
          field_id: item.field_id,
          is_searchable: true,
        });
      }
    });
    setDataConfirm({
      update: {
        tenant_id: tenantID,
        object_id: itemActive._id,
        allow_global_search: true,
        searchable_fields: temp,
        force_change: true,
      },
    });
    dispatch(
      updateSearchManagement({
        update: {
          tenant_id: tenantID,
          object_id: itemActive._id,
          allow_global_search: true,
          searchable_fields: temp,
        },
      })
    );
  };
  return (
    <Wrap>
      <Breadcrumb>
        <CustomBreadCrumb onClick={() => navigate("/tenants")}>
          {t("searchManagement.tenants")}
        </CustomBreadCrumb>
        <BreadcrumbItem>
          {t("searchManagement.searchManagement")}
        </BreadcrumbItem>
      </Breadcrumb>
      <WrapContent>
        <Left>
          <SearchWrap>
            <CustomLabel>{t("common.object")}</CustomLabel>
            <WrapSearch>
              <CustomInputSearch
                ref={ref}
                allowClear
                placeholder={t("searchManagement.findObject")}
                onSearch={(e) => {
                  handleSearch(e, sortObject);
                }}
              />
              <WrapIcon>
                {sortObject ? (
                  <img
                    src={AscendingImg}
                    alt="Ascending"
                    onClick={() => {
                      setSortObject(false);
                      handleSearch(ref?.current.input.value, false);
                    }}
                  />
                ) : (
                  <img
                    src={DescendingImg}
                    alt="Descending"
                    onClick={() => {
                      setSortObject(true);
                      handleSearch(ref?.current.input.value, true);
                    }}
                  />
                )}
              </WrapIcon>
            </WrapSearch>
          </SearchWrap>
          <ObjectWrap id="loadObjectId">
            {objects.length > 0 ? (
              <InfiniteScroll
                scrollableTarget="loadObjectId"
                dataLength={objects.length}
                loader={<Spin />}
                hasMore={hasMore}
                next={() => handleLoadMore("object")}
              >
                {renderListObject(objects)}
              </InfiniteScroll>
            ) : (
              <NoData>
                <DataWrap>
                  <img src={NoDataImg} alt="nodata" />
                  <p>
                    No <span>Object</span>
                  </p>
                </DataWrap>
              </NoData>
            )}
          </ObjectWrap>
        </Left>
        {itemActive ? (
          <Right>
            {itemActive && !itemActive.active && <Shadow />}
            <TitleWrap>
              <Switch
                checkedChildren={t("knowledgeBase.on")}
                unCheckedChildren={t("knowledgeBase.off")}
                checked={allowGlobalSearch}
                onChange={(value) => {
                  if (value) {
                    dispatch(
                      toggleGlobalSearch({
                        data: {
                          tenant_id: tenantID,
                          object_id: itemActive._id,
                          force_change: value,
                        },
                        reload: {
                          tenant_id: tenantID,
                          object_id: itemActive._id,
                          search_text: null,
                          sort_by: sortField,
                          current_page: 1,
                          records_per_page: 500,
                        },
                      })
                    );
                  } else {
                    setShowConfirm(true);
                  }
                }}
              />
              <CustomLabel>{t("searchManagement.useGlobalSearch")}</CustomLabel>
              <Tooltip title={t("searchManagement.description")}>
                <CustomIcon>i</CustomIcon>
              </Tooltip>
            </TitleWrap>
            <CustomLabel>{t("searchManagement.searchField")}</CustomLabel>
            <CustomInputSearch
              allowClear
              placeholder={t("searchManagement.findField")}
              value={textSearch}
              onChange={(value) => setTextSearch(value.target.value)}
              // onSearch={(e) => {
              //   setTextSearch(nonAccentVietnamese(e));
              // }}
            />
            <TableWrap>
              <table>
                <thead className="table-header">
                  <tr>
                    <th>
                      <WrapIconSort>
                        {t("searchManagement.label")}
                        <Sort>
                          <CaretUpOutlined
                            onClick={() => {
                              if (sortField) {
                                setSortField(false);
                                handleSortField(textSearch, false);
                              }
                            }}
                            style={{
                              color:
                                sortField === false
                                  ? "#096DD9"
                                  : "rgba(0, 0, 0, 0.25)",
                            }}
                          />
                          <CaretDownOutlined
                            onClick={() => {
                              if (!sortField) {
                                setSortField(true);
                                handleSortField(textSearch, true);
                              }
                            }}
                            style={{
                              color:
                                sortField === true
                                  ? "#096DD9"
                                  : "rgba(0, 0, 0, 0.25)",
                            }}
                          />
                        </Sort>
                      </WrapIconSort>
                    </th>
                    <th>{t("searchManagement.type")}</th>
                    <th>{t("searchManagement.required")}</th>
                    <th>{t("searchManagement.hidden")}</th>
                    <th>{t("searchManagement.deleted")}</th>
                    <th>{t("searchManagement.seachable")}</th>
                    <th>{t("searchManagement.index")}</th>
                  </tr>
                </thead>
                <tbody className="table-body">
                  {renderListField(listField)}
                </tbody>
              </table>
            </TableWrap>
            <ButtonWrap>
              {allowGlobalSearch && (
                <CustomButton
                  onClick={() => {
                    handleUpdateData();
                  }}
                >
                  {t("common.save")}
                </CustomButton>
              )}
            </ButtonWrap>
          </Right>
        ) : (
          <Right>
            <NoData>
              <DataWrap>
                <img src={NoDataImg} alt="nodata" />
                <p>
                  No <span>Data</span>
                </p>
              </DataWrap>
            </NoData>
          </Right>
        )}
      </WrapContent>
      <ModalConfirmState
        title="Confirm"
        decs="Are you sure to change the search indexing structure?"
        method={toggleGlobalSearch}
        data={{
          data: {
            tenant_id: tenantID,
            object_id: itemActive._id,
            force_change: true,
          },
          reload: {
            tenant_id: tenantID,
            object_id: itemActive._id,
            search_text: null,
            sort_by: sortField,
            current_page: 1,
            records_per_page: 500,
          },
        }}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={setShowConfirm}
      />
      <ModalConfirm
        title="Confirm"
        decs="Are you sure to change the search indexing structure?"
        img={ImgConfirm}
        method={updateSearchManagement}
        data={dataConfirm}
      />
    </Wrap>
  );
};

const Wrap = styled.div`
  padding: 24px;
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
`;
const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: pointer;
`;
const WrapContent = styled.div`
  display: flex;
  margin-top: 16px;
  gap: 16px;
`;
const Left = styled.div`
  width: 286px;
  height: calc(100vh - 174px);
  background: #fff;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
`;
const Right = styled.div`
  position: relative;
  height: calc(100vh - 174px);
  background: #fff;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  flex: 1;
  padding: 16px;
`;
const Shadow = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  cursor: not-allowed;
  background: #766c6c12;
  z-index: 5;
`;
const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: #2c2c2c;
`;
const CustomLabel = styled.div`
  font-family: var(--roboto-500);
  line-height: 21px;
`;
const SearchWrap = styled.div`
  padding: 16px;
`;
const CustomInputSearch = styled(Search)`
  flex: 1;
  margin-top: 8px;
  max-width: 270px;
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 30px;
  }
  button {
    height: 40px;
  }
`;
const WrapIcon = styled.div`
  margin-top: 8px;

  margin-left: 8px;
  cursor: pointer;
`;
const WrapSearch = styled.div`
  display: flex;
  align-items: center;
`;
const ObjectWrap = styled.div`
  overflow: auto;
  max-height: calc(100% - 102px);
`;
const ItemObject = styled.div`
  width: 100%;
  padding: 8px 16px;
  cursor: pointer;
  transition: all 0.5s;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background: ${(props) => (props.active ? props.color : "unset")};
  span {
    color: ${(props) =>
      props.disable ? "#8c8c8c" : props.active ? props.theme.main : "#2c2c2c"};
  }

  :hover {
    background: ${(props) => props.color};
    span {
      color: ${(props) => (props.disable ? "#8c8c8c" : props.theme.main)};
    }
  }
`;
const TitleWrap = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
  .ant-switch {
    width: 54px;
    margin-right: 16px;
  }
`;
const CustomIcon = styled.div`
  margin-left: 6px;
  background: #bfbfbf;
  color: #fff;
  width: 20px;
  height: 20px;
  font-size: 14px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  cursor: pointer;
`;
const TableWrap = styled.div`
  width: 100%;
  margin-top: 16px;
  max-height: calc(100% - 181px);
  overflow: auto;
  &::-webkit-scrollbar {
    height: 8px !important;
  }
  .ant-checkbox-wrapper {
    display: flex;
    margin-bottom: 4px;

    span {
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;
    }
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  table {
    width: max-content;
    table-layout: auto;
    padding: 8px;
    width: 100%;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }

    .table-header th {
      text-align: left;
      padding: 8px;
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      overflow: hidden;
      background: #f0f0f0;
      min-width: 100px;
      white-space: nowrap;

      &:first-child {
        box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
        width: 30%;
      }
      &:last-child {
        border-right: 1px solid #ddd;
      }
    }

    .table-body td {
      border-bottom: 1px solid #ddd;
      padding: 8.5px 16px;
      height: 100%;
      max-width: 200px;
      :first-child {
        box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9,
          inset 0px 0px 0px #d9d9d9;
      }
      :last-child {
        border-right: 1px solid #ddd;
      }
    }
    tr {
      cursor: pointer;
      transition: all 0.5s;
      :hover {
        background: rgb(245, 245, 245);
      }
    }
  }
`;
const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-top: 16px;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  cursor: pointer;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  :last-child {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;
const NoData = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const DataWrap = styled.div`
  img {
    width: 80px;
  }
  p {
    margin-top: 8px;
    text-align: center;
    font-size: 16px;
    line-height: 22px;

    color: #2c2c2c;

    span {
      color: ${(props) => props.theme.main};
    }
  }
`;

const WrapIconSort = styled.div`
  display: flex;
  align-items: center;
`;
const Sort = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 6px;
  svg {
    width: 12px;
    height: 12px;
    cursor: pointer;
  }
`;

export default SearchManagement;
