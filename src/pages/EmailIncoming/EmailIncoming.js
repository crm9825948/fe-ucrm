import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
import styled from "styled-components";
import _ from "lodash";

import { Breadcrumb, Button, Switch, Table, Tag, Tooltip } from "antd";

import {
  changeStatus,
  checkConnection,
  deleteMailIncoming,
  getListEmailIncoming,
  unmountEmailIncoming,
  updateEditEmail,
  updateEditEmailSuccess,
  updateEditEmailFolder,
  setDefaultEmailIncoming,
  getEmailFolders,
  scanEmail,
} from "redux/slices/emailIncoming";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  getListAccountMapping,
  getListAccountMappingSuccess,
} from "redux/slices/googleIntegration";
import { getUserName } from "redux/slices/user";

import deleteIcon from "assets/icons/email/deleteIcon.svg";
import emptyEmail from "assets/icons/email/empty-email.svg";
import viewRuleIcon from "assets/icons/email/viewRuleIcon.svg";
import editIcon from "assets/icons/email/editIcon.svg";
import FolderScanIcon from "assets/icons/email/folder_scan.svg";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import ModalEmailIncoming from "./ModalEmailIncoming";
import ModalFolderScan from "./ModalFolderScan";
import { changeTitlePage } from "redux/slices/authenticated";

export const jwt = require("jsonwebtoken");
const EmailIncoming = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(changeTitlePage(t("emailIncoming.emailIncoming")));
    //eslint-disable-next-line
  }, [t]);

  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { listEmailIncoming } = useSelector(
    (state) => state.emailIncomingReducer
  );

  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const [showModalFolder, $showModalFolder] = useState(false);
  const [dataDelete, setDataDelete] = useState({});

  // const [searchText, setSearchText] = useState("");
  // const [searchedColumn, setSearchedColumn] = useState("");
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "inbox" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const getColumnSearchProps = (dataIndex) => ({
    // filterDropdown: ({
    //   setSelectedKeys,
    //   selectedKeys,
    //   confirm,
    //   clearFilters,
    // }) => (
    //   <div style={{ padding: 8 }}>
    //     <Input
    //       placeholder={`Search`}
    //       value={selectedKeys[0]}
    //       onChange={(e) =>
    //         setSelectedKeys(e.target.value ? [e.target.value] : [])
    //       }
    //       onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //       style={{ marginBottom: 8, display: "block" }}
    //     />
    //     <Space>
    //       <Button
    //         type="primary"
    //         onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //         icon={<SearchOutlined />}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.search")}
    //       </Button>
    //       <Button
    //         onClick={() => handleReset(clearFilters)}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.reset")}
    //       </Button>
    //       <Button
    //         type="link"
    //         size="small"
    //         onClick={() => {
    //           confirm({ closeDropdown: false });
    //           setSearchText(selectedKeys[0]);
    //           setSearchedColumn(dataIndex);
    //         }}
    //       >
    //         {t("emailIncoming.filterData")}
    //       </Button>
    //     </Space>
    //   </div>
    // ),
    // filterIcon: (filtered) => (
    //   <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    // ),
    // onFilter: (value, record) =>
    //   record[dataIndex]
    //     ? record[dataIndex]
    //         .toString()
    //         .toLowerCase()
    //         .includes(value.toLowerCase())
    //     : "",
    render: (text) =>
      //   searchedColumn === dataIndex ? (
      //     <Highlighter
      //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //       searchWords={[searchText]}
      //       autoEscape
      //       textToHighlight={text ? text.toString() : ''}
      //     />
      //   ) : (
      text,
    //   ),
  });

  const columns = [
    {
      title: "",
      key: "operation",
      render: (record) => (
        <>
          <CustomTag onClick={() => checkConnectionMail(record)}>
            Test
          </CustomTag>

          <CustomTag onClick={_onScanEmail(record.object_id)} isScan={true}>
            Scan
          </CustomTag>
        </>
      ),
    },
    {
      title: t("object.object"),
      dataIndex: "object_name",
      key: "object_name",
      ...getColumnSearchProps("object_name"),
    },
    {
      title: t("emailIncoming.scannerName"),
      dataIndex: "scanner_name",
      key: "scanner_name",
      ...getColumnSearchProps("scanner_name"),
    },
    {
      title: t("emailIncoming.serverName"),
      dataIndex: "server_name",
      key: "server_name",
      ...getColumnSearchProps("server_name"),
    },
    {
      title: t("emailIncoming.emailLinking"),
      dataIndex: "user_name",
      key: "user_name",
      ...getColumnSearchProps("user_name"),
    },
    {
      title: t("emailIncoming.SSLType"),
      dataIndex: "ssl_type",
      key: "ssl_type",
      ...getColumnSearchProps("ssl_type"),
    },
    {
      title: t("emailIncoming.timeZone"),
      dataIndex: "time_zone",
      key: "time_zone",
      ...getColumnSearchProps("time_zone"),
    },
    {
      title: t("emailIncoming.default"),
      dataIndex: "is_default",
      key: "is_default",
      render: (status, record) => (
        <>
          {
            <Switch
              disabled={record.is_default || !checkRule("edit")}
              onChange={() => dispatch(setDefaultEmailIncoming(record._id))}
              checked={record.is_default}
            />
          }
        </>
      ),
    },
    {
      title: t("common.status"),
      dataIndex: "status",
      key: "status",
      render: (status, record) => (
        <>
          {
            <Switch
              disabled={!checkRule("edit")}
              onChange={(e) => changeStatusItem(record, e)}
              checked={status}
            />
          }
        </>
      ),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "right",
      width: 150,
      render: (record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                onClick={() => editIncoming(record)}
                src={editIcon}
                alt="edit"
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                onClick={() => {
                  deleteIncoming(record);
                }}
                src={deleteIcon}
                alt="delete"
              />
            </Tooltip>
          )}
          <Tooltip title="View rule">
            <img
              alt="Rule"
              src={viewRuleIcon}
              onClick={() => settingRule(record)}
            />
          </Tooltip>
          {checkRule("edit") && (
            <Tooltip title="Folder scan">
              <img
                alt="folder"
                src={FolderScanIcon}
                onClick={() => {
                  dispatch(
                    updateEditEmailFolder({
                      _id: record._id,
                    })
                  );
                  dispatch(
                    getEmailFolders({
                      config_id: record._id,
                    })
                  );
                  $showModalFolder(true);
                }}
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  const changeStatusItem = (item, value) => {
    dispatch(
      changeStatus({
        item: item,
        payload: {
          data: {
            status: value,
          },
          _id: item._id,
        },
      })
    );
  };

  const checkConnectionMail = (record) => {
    dispatch(
      checkConnection({
        email_type: 1,
        setting_id: record._id,
      })
    );
  };

  const _onScanEmail = (object_id) => {
    return () => {
      dispatch(
        scanEmail({
          tenant_id: _.get(userDetail, "tenant_id", ""),
          object_id: object_id,
        })
      );
    };
  };

  const addIncoming = () => {
    dispatch(
      updateEditEmailSuccess({
        object_id: "",
        user_name: "",
        pass_word: "",
        server_name: "",
        port: "",
        scanner_name: "",
        ssl_type: "",
        ssl_method: "validate",
        look_for: "UNSEEN",
        after_scan: "Keep Unseen",
        status: false,
        time_zone: "+0007",
        folder_selected: "INBOX",
        protocol: "IMAP4",
        port_custom: "",
        is_use_username: false,
        update_fields_after_read_all: false,
      })
    );
  };
  const editIncoming = (record) => {
    dispatch(
      updateEditEmail({
        _id: record._id,
      })
    );
  };
  const deleteIncoming = (record) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: record._id,
    });
  };
  const settingRule = (record) => {
    navigate("/edit-incoming-rule/" + record._id + "/" + record.object_id);
  };
  // const handleSearch = (selectedKeys, confirm, dataIndex) => {
  //   confirm();
  //   setSearchText(selectedKeys[0]);
  //   setSearchedColumn(dataIndex);
  // };
  // const handleReset = (clearFilters) => {
  //   clearFilters();
  //   setSearchText("");
  // };

  useEffect(() => {
    // if (credential_id && credential_id.length > 0) {
    //   dispatch(
    //     getListAccountMapping({
    //       credential_id: credential_id,
    //     })
    //   );
    // } else {
    //   dispatch(getListAccountMappingSuccess([]));
    // }

    dispatch(
      getListAccountMapping({
        credential_id: "",
      })
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(getListEmailIncoming());
    dispatch(getListAccountMappingSuccess([]));
    dispatch(getUserName());
    // eslint-disable-next-line
  }, []);

  // unmount
  useEffect(
    () => () => {
      dispatch(unmountEmailIncoming());
    },
    // eslint-disable-next-line
    []
  );
  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("emailIncoming.emailIncoming")}</BreadcrumbItem>
        </Breadcrumb>
        {listEmailIncoming.length > 0 && checkRule("create") && (
          <AddButton onClick={addIncoming}>
            + {t("emailIncoming.addMailBox")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listEmailIncoming.length > 0 ? (
        <WrapTable>
          <Table
            dataSource={listEmailIncoming}
            columns={columns}
            pagination={{
              position: ["bottomRight"],
              showSizeChanger: true,
              defaultPageSize: 10,
              showQuickJumper: true,
            }}
          />
        </WrapTable>
      ) : (
        <Empty>
          <img src={emptyEmail} alt="empty" />
          <p>
            {t("object.noObject")}{" "}
            <span>{t("emailIncoming.emailIncoming")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={addIncoming}>
              + {t("emailIncoming.addMailBox")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalEmailIncoming />

      <ModalFolderScan
        showModalFolder={showModalFolder}
        $showModalFolder={$showModalFolder}
      />

      <ModalConfimDelete
        title={t("emailIncoming.deleteEmail")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteMailIncoming}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};

export default EmailIncoming;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-switch-disabled {
    opacity: 1;
  }
`;

const CustomTag = styled(Tag)`
  border: ${(props) =>
    props.isScan ? `1px solid ${props.theme.main}` : "1px solid #f0f0f0"};
  color: ${(props) => props.theme.main};
  padding: 0 16px;
  cursor: pointer;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
