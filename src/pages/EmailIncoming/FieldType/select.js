import React, { useState, useEffect } from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";

import Input from "antd/lib/input";
import Col from "antd/lib/col";
import SelectAntd from "antd/lib/select";
import Form from "antd/lib/form";

import {
  loadValuePicklist,
  loadTargetPicklist,
  loadTargetPicklistFail,
} from "redux/slices/emailIncoming";

const { Option } = SelectAntd;

const optionsEmail = [
  {
    label: "From",
    value: "from",
  },
  {
    label: "To",
    value: "to",
  },
  {
    label: "Cc",
    value: "cc",
  },
  {
    label: "Bcc",
    value: "bcc",
  },
];

function SelectType({
  field,
  value,
  type,
  index,
  required,
  objectID,
  handleChangeValue,
  editRuleIncoming,
  form,
  formValue,
  setFormValue,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { valuePicklist, targetPicklist } = useSelector(
    (state) => state.emailIncomingReducer
  );

  const [newValuePicklist, setNewValuePicklist] = useState({});

  useEffect(() => {
    // let newObj = { ...newValuePicklist, ...valuePicklist };

    // let result = {};
    // let temp = {};
    // Object.entries(newObj).forEach(([key, val]) => {
    //   Object.entries(val).forEach(([keyVal, valVal]) => {
    //     if (valVal) {
    //       temp[keyVal] = valVal;
    //     }
    //   });
    //   result[key] = { ...temp };
    // });

    // setNewValuePicklist(result);

    setNewValuePicklist((prev) => {
      let newObj = { ...prev, ...valuePicklist };

      let result = {};
      let temp = {};
      Object.entries(newObj).forEach(([key, val]) => {
        Object.entries(val).forEach(([keyVal, valVal]) => {
          if (valVal) {
            temp[keyVal] = valVal;
          }
        });
        result[key] = { ...temp };
      });
      return result;
    });
  }, [valuePicklist]);

  useEffect(() => {
    let value = form.getFieldsValue();

    if (targetPicklist.length > 0) {
      targetPicklist.forEach((item) => {
        form.setFieldsValue({
          value,
          [item]: undefined,
        });
      });

      dispatch(loadTargetPicklistFail());
    }

    setFormValue(form.getFieldsValue());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [targetPicklist]);

  useEffect(() => {
    if (editRuleIncoming) {
      const value =
        type === "reply"
          ? form.getFieldValue(`${field.full_field_id}_reply`)
          : form.getFieldValue(field.full_field_id);

      if (
        (field.source === null || field.source === undefined) &&
        field.target &&
        field.target.length > 0
      ) {
        field.target.forEach((item) => {
          dispatch(
            loadValuePicklist({
              data: {
                Object_ID: objectID,
                Source_ID: field.full_field_id,
                Target_ID: item,
                Value: value,
              },
              ID: item,
              listValue: { ...valuePicklist },
            })
          );
        });
      } else if (field.source && field.target && field.target.length > 0) {
        field.target.forEach((item) => {
          dispatch(
            loadValuePicklist({
              data: {
                Object_ID: objectID,
                Source_ID: field.full_field_id,
                Target_ID: item,
                Value: value,
              },
              ID: item,
              listValue: { ...valuePicklist },
            })
          );
        });
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [field, editRuleIncoming]);

  return (
    // <>
    //   {required && (
    //     <Col flex="50%">
    //       <Input
    //         style={{ width: "100%" }}
    //         defaultValue={_.get(field, "name", "")}
    //         disabled
    //       />
    //     </Col>
    //   )}

    //   <ColCustom required={required}>
    //     <Select
    //       placeholder={t("common.placeholderSelect")}
    //       style={{ width: "100%" }}
    //       options={
    //         _.isEqual(_.get(field, "type", ""), "email")
    //           ? optionsEmail
    //           : _.get(field, "option", [])
    //       }
    //       value={value}
    //       onChange={(e) =>
    //         handleChangeValue(type, _.get(field, "type", ""), e, index)
    //       }
    //     />
    //   </ColCustom>
    // </>

    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      {(field.source === null || field.source === undefined) &&
      field.target &&
      field.target.length > 0 ? (
        <ColCustom required={required}>
          <FormCustom
            wrapperCol={{ span: 24 }}
            label={""}
            name={
              type === "reply"
                ? `${field.full_field_id}_reply`
                : field.full_field_id
            }
            rules={[
              {
                required: true,
                message: `${t("common.placeholderSelect")} ${field.label}!`,
              },
            ]}
            className="requiredMark"
          >
            <SelectAntd
              placeholder={t("common.placeholderSelect")}
              onChange={(e, option) => {
                /* eslint-disable-next-line */
                field.target.map((item, idx) => {
                  dispatch(
                    loadValuePicklist({
                      data: {
                        Object_ID: objectID,
                        Source_ID: field.full_field_id,
                        Target_ID: item,
                        Value: e,
                      },
                      ID: item,
                      listValue: valuePicklist,
                    })
                  );
                });

                dispatch(
                  loadTargetPicklist({
                    field_id: field.full_field_id,
                  })
                );

                handleChangeValue(type, _.get(field, "type", ""), e, index);
              }}
            >
              {field.option.map((item, idx) => {
                return (
                  <Option value={item.value} key={idx}>
                    {item.label}
                  </Option>
                );
              })}
            </SelectAntd>
          </FormCustom>
        </ColCustom>
      ) : field.source && field.target && field.target.length > 0 ? (
        <ColCustom required={required}>
          <FormCustom
            wrapperCol={{ span: 24 }}
            label={""}
            name={
              type === "reply"
                ? `${field.full_field_id}_reply`
                : field.full_field_id
            }
            rules={[
              {
                required: true,
                message: `${t("common.placeholderSelect")} ${field.label}!`,
              },
            ]}
            className="requiredMark"
          >
            <SelectAntd
              disabled={
                formValue[
                  type === "reply" ? `${field.source}_reply` : field.source
                ]
                  ? false
                  : true
              }
              placeholder={t("common.placeholderSelect")}
              onChange={(e, option) => {
                /* eslint-disable-next-line */
                field.target.map((item, idx) => {
                  dispatch(
                    loadValuePicklist({
                      data: {
                        Object_ID: objectID,
                        Source_ID: field.full_field_id,
                        Target_ID: item,
                        Value: e,
                      },
                      ID: item,
                      listValue: valuePicklist,
                    })
                  );
                });

                dispatch(
                  loadTargetPicklist({
                    field_id: field.full_field_id,
                  })
                );

                handleChangeValue(type, _.get(field, "type", ""), e, index);
              }}
            >
              {newValuePicklist && newValuePicklist[field.full_field_id]
                ? Object.entries(
                    newValuePicklist && newValuePicklist[field.full_field_id]
                  ).map(([key, value], idx) => {
                    return (
                      <Option value={value === true ? key : value} key={idx}>
                        {key}
                      </Option>
                    );
                  })
                : field.option.map((item, idx) => {
                    return (
                      <Option value={item.value} key={idx}>
                        {item.label}
                      </Option>
                    );
                  })}
            </SelectAntd>
          </FormCustom>
        </ColCustom>
      ) : field.source && field.target && field.target.length === 0 ? (
        <ColCustom required={required}>
          <FormCustom
            wrapperCol={{ span: 24 }}
            label={""}
            name={
              type === "reply"
                ? `${field.full_field_id}_reply`
                : field.full_field_id
            }
            rules={[
              {
                required: true,
                message: `${t("common.placeholderSelect")} ${field.label}!`,
              },
            ]}
            className="requiredMark"
          >
            <SelectAntd
              placeholder={t("common.placeholderSelect")}
              disabled={
                formValue[
                  type === "reply" ? `${field.source}_reply` : field.source
                ]
                  ? false
                  : true
              }
              onChange={(e) => {
                handleChangeValue(type, _.get(field, "type", ""), e, index);
              }}
            >
              {newValuePicklist && newValuePicklist[field.full_field_id]
                ? Object.entries(
                    newValuePicklist && newValuePicklist[field.full_field_id]
                  ).map(([key, value], idx) => {
                    return (
                      <Option value={value === true ? key : value} key={idx}>
                        {key}
                      </Option>
                    );
                  })
                : field.option.map((item, idx) => {
                    return (
                      <Option value={item.value} key={idx}>
                        {item.label}
                      </Option>
                    );
                  })}
            </SelectAntd>
          </FormCustom>
        </ColCustom>
      ) : (
        <ColCustom required={required}>
          <FormCustom
            wrapperCol={{ span: 24 }}
            label={""}
            name={
              type === "reply"
                ? `${field.full_field_id}_reply`
                : field.full_field_id
            }
            rules={[
              {
                required: true,
                message: `${t("common.placeholderSelect")} ${field.label}!`,
              },
            ]}
            className="requiredMark"
          >
            <SelectAntd
              placeholder={t("common.placeholderSelect")}
              options={
                _.isEqual(_.get(field, "type", ""), "email")
                  ? optionsEmail
                  : _.get(field, "option", [])
              }
              onChange={(e) => {
                handleChangeValue(type, _.get(field, "type", ""), e, index);
              }}
            />
          </FormCustom>
        </ColCustom>
      )}
    </>
  );
}

export default withTranslation()(SelectType);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }
`;

const FormCustom = styled(Form.Item)`
  width: 100%;
  margin-bottom: 0 !important;
`;
