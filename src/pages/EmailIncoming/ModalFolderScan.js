import React from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";

import { changeField, updateMailBoxIncoming } from "redux/slices/emailIncoming";

function ModalChangePass({ showModalFolder, $showModalFolder }) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { editEmailIncoming, listEmailFolders } = useSelector(
    (state) => state.emailIncomingReducer
  );

  const [form] = Form.useForm();

  const _onSubmit = () => {
    dispatch(
      updateMailBoxIncoming({
        ...editEmailIncoming,
      })
    );
    $showModalFolder(false);
  };

  const _onCancel = () => {
    $showModalFolder(false);
  };

  return (
    <>
      <ModalCustom
        title="Folder scan"
        visible={showModalFolder}
        footer={null}
        width={600}
        onCancel={_onCancel}
      >
        <Form
          form={form}
          onFinish={_onSubmit}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          colon={false}
          labelAlign="left"
        >
          <Form.Item
            label="Folder Scanning"
            name="folder_selected"
            valuePropName={editEmailIncoming?.folder_selected}
            style={{ marginTop: 16 }}
          >
            <Select
              onChange={(e) => {
                dispatch(
                  changeField({
                    key: "folder_selected",
                    value: e,
                  })
                );
              }}
              value={editEmailIncoming?.folder_selected}
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {listEmailFolders &&
                listEmailFolders.map((item) => {
                  return <Select.Option value={item}>{item}</Select.Option>;
                })}
            </Select>
          </Form.Item>

          <WrapButton label=" ">
            <Button type="primary" htmlType="submit">
              {t("common.save")}
            </Button>
            <Button onClick={_onCancel}>{t("common.cancel")}</Button>
          </WrapButton>
        </Form>
      </ModalCustom>
    </>
  );
}

export default withTranslation()(ModalChangePass);

ModalChangePass.defaultProps = {
  isAdmin: true,
  root: false,
  email: "",
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
