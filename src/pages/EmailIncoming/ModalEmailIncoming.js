import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import InputNumber from "antd/lib/input-number";
import Switch from "antd/lib/switch";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Tooltip from "antd/lib/tooltip";

import {
  changeField,
  setShowDrawer,
  submitMailBox,
  updateEditEmailSuccess,
  updateMailBoxIncoming,
  updateMappingFields,
} from "redux/slices/emailIncoming";
import { loadListObjectField } from "redux/slices/objects";
import { getListGoogleIntegrationSetting } from "redux/slices/googleIntegration";
import { loadOutlookAccount } from "redux/slices/outlook";
import { Notification } from "components/Notification/Noti";

import Number from "pages/RuleIncoming/FieldType/number";
import Date from "pages/RuleIncoming/FieldType/date";
import SelectType from "./FieldType/select";
import File from "pages/RuleIncoming/FieldType/file";
import User from "pages/RuleIncoming/FieldType/user";
import Linking from "./FieldType/linkingobject";
import Other from "pages/RuleIncoming/FieldType/other";

import deleteIcon from "assets/icons/email/deleteIcon.svg";
import AvatarImg from "assets/images/header/avatar.png";

function ModalEmailIncoming({ listOptionObjectField }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();

  const { editEmailIncoming, showDrawer } = useSelector(
    (state) => state.emailIncomingReducer
  );

  const { allAccount } = useSelector((state) => state.outlookReducer);

  const {
    _id,
    object_id,
    user_name,
    pass_word,
    server_name,
    port,
    scanner_name,
    is_use_username,
    username_authentication,
    server_type,
    google_account_integration,
    ssl_type,
    port_custom,
    update_fields_after_read_all,
    mapping_fields_update,
  } = editEmailIncoming;
  const { listObject } = useSelector((state) => state.objectsManagementReducer);
  const { listAccountMapping } = useSelector(
    (state) => state.googleIntegrationReducer
  );

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [formValue, setFormValue] = useState({});
  const [optionalFields, $optionalFields] = useState([]);
  const [optionalFieldsSelected, $optionalFieldsSelected] = useState([]);

  const _onSubmit = () => {
    let checkEmptyMapping = false;

    optionalFieldsSelected.forEach((item) => {
      if (item.type === "linkingobject" || item.type === "lookup") {
        if (!item.value || !item.default_value) {
          checkEmptyMapping = true;
        }
      } else {
        if (!item.value) {
          checkEmptyMapping = true;
        }
      }
    });

    if (checkEmptyMapping) {
      Notification("warning", "Please input or delete empty mapping fields!");
    } else {
      const payload = {
        ...editEmailIncoming,
        mapping_fields_update: optionalFieldsSelected,
        ...(server_type === "owa" && { port: 0 }),
      };

      if (_id) {
        dispatch(updateMailBoxIncoming(payload));
      } else {
        dispatch(submitMailBox(payload));
      }
    }
  };

  const _onCancel = () => {
    dispatch(setShowDrawer(false));
    dispatch(updateMappingFields([]));
    form.resetFields();
    changeFieldEmail("object_id", "");
    $optionalFields([]);
    $optionalFieldsSelected([]);
    setFormValue({});
  };

  const changeFieldEmail = (key, e) => {
    dispatch(
      changeField({
        key: key,
        value: e,
      })
    );
  };

  const handleSelectObject = (value) => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
      })
    );
  };

  const _onAddOptionalField = () => {
    const item = [
      ...optionalFieldsSelected,
      { key_object: undefined, value: undefined, type: "" },
    ];
    $optionalFieldsSelected(item);
  };

  const _onDeleteFieldOptional = (index, key_object) => {
    return () => {
      let temp = [...optionalFieldsSelected];
      temp.splice(index, 1);
      $optionalFieldsSelected(temp);

      if (key_object) {
        form.setFieldsValue({
          [key_object]: undefined,
        });
      }
    };
  };

  const handleChangeField = (index) => {
    return (e) => {
      let tempOptionalFields = [...optionalFieldsSelected];
      tempOptionalFields[index] = {
        ...tempOptionalFields[index],
        key_object: e,
        type: optionalFields.find((item) => item.full_field_id === e)?.type,
        value:
          optionalFields.find((item) => item.full_field_id === e)?.type ===
          "file"
            ? "attach_file"
            : undefined,
      };

      delete tempOptionalFields[index].default_value;

      if (e) {
        form.setFieldsValue({
          [e]: undefined,
        });
      }

      $optionalFieldsSelected(tempOptionalFields);
    };
  };

  const handleChangeValue = (type, typeField, value, index, defaultValue) => {
    let tempOptional = [...optionalFieldsSelected];

    switch (typeField) {
      case "linkingobject":
      case "lookup":
        tempOptional[index] = {
          ...tempOptional[index],
          value: value,
          type: typeField,
          default_value: defaultValue,
        };
        $optionalFieldsSelected(tempOptional);

        break;
      case "file":
        break;

      //text, textarea, email, select, dynamic-field, user, number,date, datetime-local
      default:
        tempOptional[index] = {
          ...tempOptional[index],
          value: value,
          type: typeField,
        };
        $optionalFieldsSelected(tempOptional);
        break;
    }
  };

  const handleFieldType = (field, value, index, type, defaultValue) => {
    if (!field) return;

    switch (field.type) {
      case "number":
        return (
          <Number
            field={field}
            value={value}
            type={type}
            index={index}
            required={false}
            handleChangeValue={handleChangeValue}
          />
        );

      case "date":
      case "datetime-local":
        return (
          <Date
            field={field}
            value={value}
            type={type}
            index={index}
            required={false}
            handleChangeValue={handleChangeValue}
          />
        );

      case "linkingobject":
      case "lookup":
        return (
          <Linking
            field={field}
            value={value}
            defaultValue={defaultValue}
            type={type}
            index={index}
            required={false}
            handleChangeValue={handleChangeValue}
          />
        );

      case "file":
        return <File field={field} required={false} />;

      case "user":
        return (
          <User
            field={field}
            value={value}
            type={type}
            index={index}
            required={false}
            handleChangeValue={handleChangeValue}
          />
        );

      case "email":
      case "select":
      case "dynamic-field":
        return (
          <SelectType
            field={field}
            value={value}
            type={type}
            index={index}
            required={false}
            objectID={object_id}
            editRuleIncoming={_id}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            handleChangeValue={handleChangeValue}
          />
        );

      //text, textarea
      default:
        return (
          <Other
            field={field}
            value={value}
            type={type}
            index={index}
            required={false}
            handleChangeValue={handleChangeValue}
          />
        );
    }
  };

  useEffect(() => {
    let tempFullFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                field.type !== "id" &&
                field.type !== "formula" &&
                field.type !== "bridging-field"
              ) {
                if (
                  field.type !== "linkingobject" ||
                  (field.type === "linkingobject" && field.key)
                ) {
                  tempFullFields.push(field);
                }
              }
            });
          });
        }
      }
    });

    $optionalFields(tempFullFields);
  }, [listObjectField]);

  useEffect(() => {
    if (
      _id &&
      optionalFields.length > 0 &&
      mapping_fields_update?.length > 0 &&
      update_fields_after_read_all
    ) {
      let tempOptionalFields = [];

      mapping_fields_update?.forEach((field) => {
        tempOptionalFields.push(field);

        if (
          _.get(field, "type", "") === "select" ||
          _.get(field, "type", "") === "email" ||
          _.get(field, "type", "") === "dynamic-field"
        ) {
          form.setFieldsValue({
            [_.get(field, "key_object", "")]: _.get(field, "value", undefined),
          });
        }
      });

      $optionalFieldsSelected(mapping_fields_update);
    } else {
      form.resetFields();
      $optionalFieldsSelected([]);
    }
  }, [
    _id,
    form,
    mapping_fields_update,
    optionalFields,
    update_fields_after_read_all,
  ]);

  useEffect(() => {
    if (!showDrawer) {
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [showDrawer]);

  useEffect(() => {
    if (server_type && !_id) {
      dispatch(
        updateEditEmailSuccess({
          ...editEmailIncoming,
          object_id: "",
          user_name: "",
          pass_word: "",
          server_name: "",
          port: "",
          scanner_name: "",
          ssl_type: "",
          ssl_method: "validate",
          look_for: "UNSEEN",
          after_scan: "Keep Unseen",
          status: false,
          time_zone: "+0007",
          folder_selected: "INBOX",
          protocol: "IMAP4",
          port_custom: "",
          is_use_username: false,
          update_fields_after_read_all: false,
        })
      );
    }

    if (showDrawer && server_type === "google") {
      dispatch(getListGoogleIntegrationSetting());
      dispatch(
        updateEditEmailSuccess({
          ...editEmailIncoming,
          server_name: "smtp.gmail.com",
          port: 587,
        })
      );
    }
    if (showDrawer && server_type === "outlook") {
      dispatch(loadOutlookAccount());
      dispatch(
        updateEditEmailSuccess({
          ...editEmailIncoming,
          server_name: "outlook.office365.com",
          port: 587,
        })
      );
    }
    if (showDrawer && server_type === "owa") {
      dispatch(
        updateEditEmailSuccess({
          ...editEmailIncoming,
          is_use_username: true,
        })
      );
    }
    // eslint-disable-next-line
  }, [server_type, _id, dispatch]);

  return (
    <ModalCustom
      title={
        _id ? t("emailIncoming.updateMailBox") : t("emailIncoming.addMailBox")
      }
      open={showDrawer}
      footer={null}
      width={update_fields_after_read_all ? 800 : 600}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        colon={false}
        labelAlign="left"
        layout="vertical"
        initialValues={{
          object_id: object_id,
          scanner_name: scanner_name,
          user_name: user_name,
          pass_word: pass_word,
          ssl_type: ssl_type,
          port: port,
          port_custom: port_custom,
        }}
      >
        <Form.Item
          label={t("emailIncoming.serverType")}
          name="server_type"
          rules={[
            {
              validator: (rule, value = server_type, cb) => {
                server_type.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={server_type}
        >
          <Select
            onChange={(e) => changeFieldEmail("server_type", e)}
            value={server_type}
          >
            <Select.Option value={"custom"}>Custom</Select.Option>
            <Select.Option value={"google"}>Google</Select.Option>
            <Select.Option value={"outlook"}>Outlook</Select.Option>
            <Select.Option value={"owa"}>OWA</Select.Option>
          </Select>
        </Form.Item>
        {server_type === "custom" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => {
                  changeFieldEmail("object_id", e);
                  handleSelectObject(e);
                }}
                value={object_id}
                disabled={_id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listObject &&
                  listObject.map((item) => {
                    if (item.Status) {
                      return (
                        <Select.Option value={item._id} key={item._id}>
                          {item.Name}
                        </Select.Option>
                      );
                    } else {
                      return <></>;
                    }
                  })}
              </Select>
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.scannerName")}
              name="scanner_name"
              rules={[
                {
                  validator: (rule, value = scanner_name, cb) => {
                    scanner_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={scanner_name}
            >
              <Input
                value={scanner_name}
                onChange={(e) =>
                  changeFieldEmail("scanner_name", e.target.value)
                }
              />
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.emailLinking")}
              name="user_name"
              rules={[
                {
                  validator: (rule, value = user_name, cb) => {
                    user_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={user_name}
            >
              <Input
                autocomplete="new-password"
                value={user_name}
                onChange={(e) => changeFieldEmail("user_name", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("user.pass")}
              name="pass_word"
              valuePropName={pass_word}
            >
              <Input.Password
                autocomplete="new-password"
                value={pass_word}
                onChange={(e) => changeFieldEmail("pass_word", e.target.value)}
              />
            </Form.Item>
            <Form.Item name={"is_use_username"} valuePropName={is_use_username}>
              <Checkbox
                checked={is_use_username}
                onChange={(e) => {
                  changeFieldEmail("is_use_username", e.target.checked);
                  if (!e.target.checked) {
                    changeFieldEmail("username_authentication", "");
                  }
                }}
              >
                {t("emailIncoming.username")}
              </Checkbox>
            </Form.Item>
            {is_use_username ? (
              <Form.Item
                label={t("emailIncoming.userAuth")}
                name={"username_authentication"}
                valuePropName={username_authentication}
                rules={[
                  {
                    validator: (rule, value = username_authentication, cb) => {
                      username_authentication.length === 0
                        ? cb(t("common.requiredField"))
                        : cb();
                    },
                    required: true,
                  },
                ]}
              >
                <Input
                  value={username_authentication}
                  onChange={(e) =>
                    changeFieldEmail("username_authentication", e.target.value)
                  }
                ></Input>
              </Form.Item>
            ) : (
              ""
            )}
            <Form.Item
              label={t("emailIncoming.serverName")}
              name="server_name"
              rules={[
                {
                  validator: (rule, value = server_name, cb) => {
                    server_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={server_name}
            >
              <Input
                value={server_name}
                onChange={(e) =>
                  changeFieldEmail("server_name", e.target.value)
                }
              />
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.SSLType")}
              name="ssl_type"
              rules={[
                {
                  validator: (rule, value = ssl_type, cb) => {
                    ssl_type.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={ssl_type}
            >
              <Select
                value={ssl_type}
                onChange={(e) => changeFieldEmail("ssl_type", e)}
              >
                <Select.Option value={"SSL"}>SSL</Select.Option>
                <Select.Option value={"TLS"}>TLS</Select.Option>
                <Select.Option value={"none"}>None</Select.Option>
              </Select>
            </Form.Item>
            <Port
              label={t("emailIncoming.port")}
              name="port"
              rules={[
                {
                  validator: (rule, value = port, cb) => {
                    port.length === 0 ? cb(t("common.requiredField")) : cb();
                  },
                  required: true,
                },
                {
                  validator: (rule, value = port, cb) => {
                    port === "custom" && port_custom.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={port}
              isCustom={port === "custom"}
            >
              <Select
                value={port}
                onChange={(e) => changeFieldEmail("port", e)}
                style={{ flex: port === "custom" && 2 }}
              >
                <Select.Option value={"993"}>993</Select.Option>
                <Select.Option value={"153"}>153</Select.Option>
                <Select.Option value={"custom"}>Custom</Select.Option>
              </Select>
              {port === "custom" && (
                <InputNumber
                  style={{ flex: 1 }}
                  onChange={(e) => changeFieldEmail("port_custom", e)}
                  value={port_custom}
                ></InputNumber>
              )}
            </Port>
          </>
        )}
        {server_type === "google" && (
          <>
            {/* <Form.Item
                name={"credential_id"}
                label={t("emailIncoming.credentialId")}
                className={"credential_id"}
                valuePropName={credential_id}
                rules={[
                  {
                    validator: (rule, value = credential_id, cb) => {
                      credential_id.length === 0
                        ? cb(t("common.requiredField"))
                        : cb();
                    },
                    required: true,
                  },
                ]}
              >
                <Select
                  value={credential_id}
                  onChange={(e) => {
                    changeFieldEmail("credential_id", e);
                  }}
                >
                  {listGoogleIntegrationSetting.map((item) => {
                    return <Option value={item._id}>{item.config_name}</Option>;
                  })}
                </Select>
              </Form.Item> */}
            <></>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => {
                  changeFieldEmail("object_id", e);
                  handleSelectObject(e);
                }}
                value={object_id}
                disabled={_id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {
                  // eslint-disable-next-line
                  listObject &&
                    listObject.map((item) => {
                      if (item.Status) {
                        return (
                          <Select.Option value={item._id} key={item._id}>
                            {item.Name}
                          </Select.Option>
                        );
                      } else return "";
                    })
                }
              </Select>
            </Form.Item>
            <div style={{ backgroundColor: "#F2F4F5", padding: "1rem" }}>
              {listAccountMapping.map((item, idx) => {
                return (
                  <div
                    key={idx}
                    className="selected_google"
                    style={{
                      padding: "1rem",
                      backgroundColor:
                        item.email === google_account_integration
                          ? "#dbd8d8"
                          : "white",
                      marginBottom: "0.5rem",
                      borderRadius: "5px",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      changeFieldEmail(
                        "google_account_integration",
                        item.email
                      );
                    }}
                  >
                    <img
                      src={item.avatar_url}
                      alt=""
                      style={{
                        width: "2rem",
                        borderRadius: "50%",
                        marginRight: "1rem",
                      }}
                    />
                    {item.email}
                  </div>
                );
              })}
            </div>
          </>
        )}

        {server_type === "outlook" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => {
                  changeFieldEmail("object_id", e);
                  handleSelectObject(e);
                }}
                value={object_id}
                disabled={_id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {
                  // eslint-disable-next-line
                  listObject &&
                    listObject.map((item) => {
                      if (item.Status) {
                        return (
                          <Select.Option value={item._id} key={item._id}>
                            {item.Name}
                          </Select.Option>
                        );
                      } else return "";
                    })
                }
              </Select>
            </Form.Item>
            <div style={{ backgroundColor: "#F2F4F5", padding: "1rem" }}>
              {allAccount.map((item, idx) => {
                return (
                  <div
                    key={idx}
                    style={{
                      padding: "1rem",
                      backgroundColor:
                        item.email === google_account_integration
                          ? "#dbd8d8"
                          : "white",
                      marginBottom: "0.5rem",
                      borderRadius: "5px",
                      cursor: "pointer",
                    }}
                    onClick={() => {
                      changeFieldEmail(
                        "google_account_integration",
                        item.email
                      );
                    }}
                  >
                    <img
                      src={AvatarImg}
                      alt=""
                      style={{
                        width: "2rem",
                        borderRadius: "50%",
                        marginRight: "1rem",
                      }}
                    />
                    {item.email}
                  </div>
                );
              })}
            </div>
          </>
        )}
        {server_type === "owa" && (
          <>
            <Form.Item
              label={t("object.object")}
              name="object"
              rules={[
                {
                  validator: (rule, value = object_id, cb) => {
                    object_id.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={object_id}
            >
              <Select
                onChange={(e) => {
                  changeFieldEmail("object_id", e);
                  handleSelectObject(e);
                }}
                value={object_id}
                disabled={_id}
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
              >
                {listObject &&
                  listObject.map((item) => {
                    if (item.Status) {
                      return (
                        <Select.Option value={item._id} key={item._id}>
                          {item.Name}
                        </Select.Option>
                      );
                    } else {
                      return <></>;
                    }
                  })}
              </Select>
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.scannerName")}
              name="scanner_name"
              rules={[
                {
                  validator: (rule, value = scanner_name, cb) => {
                    scanner_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={scanner_name}
            >
              <Input
                value={scanner_name}
                onChange={(e) =>
                  changeFieldEmail("scanner_name", e.target.value)
                }
              />
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.emailLinking")}
              name="user_name"
              rules={[
                {
                  validator: (rule, value = user_name, cb) => {
                    user_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={user_name}
            >
              <Input
                autocomplete="new-password"
                value={user_name}
                onChange={(e) => changeFieldEmail("user_name", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("user.pass")}
              name="pass_word"
              valuePropName={pass_word}
            >
              <Input.Password
                autocomplete="new-password"
                value={pass_word}
                onChange={(e) => changeFieldEmail("pass_word", e.target.value)}
              />
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.userAuth")}
              name={"username_authentication"}
              valuePropName={username_authentication}
              rules={[
                {
                  validator: (rule, value = username_authentication, cb) => {
                    username_authentication.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
            >
              <Input
                value={username_authentication}
                onChange={(e) =>
                  changeFieldEmail("username_authentication", e.target.value)
                }
              ></Input>
            </Form.Item>
            <Form.Item
              label={t("emailIncoming.serverName")}
              name="server_name"
              rules={[
                {
                  validator: (rule, value = server_name, cb) => {
                    server_name.length === 0
                      ? cb(t("common.requiredField"))
                      : cb();
                  },
                  required: true,
                },
              ]}
              valuePropName={server_name}
            >
              <Input
                value={server_name}
                onChange={(e) =>
                  changeFieldEmail("server_name", e.target.value)
                }
              />
            </Form.Item>
          </>
        )}

        {server_type && object_id && (
          <>
            <FormCustom
              label={t("emailIncoming.allEmailAsRead")}
              name={"update_fields_after_read_all"}
              valuePropName={update_fields_after_read_all}
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              // valuePropName="checked"
            >
              <Switch
                checked={update_fields_after_read_all}
                onChange={(e) => {
                  changeFieldEmail("update_fields_after_read_all", e);
                }}
              />
            </FormCustom>
            {update_fields_after_read_all && (
              <WrapMapping>
                <OptionalFields>
                  {_.map(optionalFieldsSelected, (field, index) => (
                    <Row
                      key={index}
                      gutter={8}
                      align="middle"
                      style={{
                        marginBottom: 16,
                      }}
                    >
                      <Col flex="50%">
                        <Select
                          placeholder={t("common.placeholderSelect")}
                          style={{ width: "100%" }}
                          onChange={handleChangeField(index)}
                          value={_.get(field, "key_object", undefined)}
                        >
                          {_.map(optionalFields, (optionalField) => (
                            <Select.Option
                              key={_.get(optionalField, "full_field_id", "")}
                              value={_.get(optionalField, "full_field_id", "")}
                              disabled={
                                optionalFieldsSelected.find(
                                  (ele) =>
                                    _.get(ele, "key_object", "") ===
                                    _.get(optionalField, "full_field_id", "")
                                )
                                  ? true
                                  : false
                              }
                            >
                              {_.get(optionalField, "name", "")}
                            </Select.Option>
                          ))}
                        </Select>
                      </Col>

                      {_.get(field, "key_object", "") ? (
                        <>
                          {handleFieldType(
                            optionalFields.find(
                              (item) => item.full_field_id === field.key_object
                            ),
                            field.value,
                            index,
                            "nonRequired",
                            field?.default_value
                          )}
                        </>
                      ) : (
                        <ColCustom>
                          <Input disabled />
                        </ColCustom>
                      )}

                      <ColDelCustom>
                        <Tooltip title="Delete">
                          <Delete
                            onClick={_onDeleteFieldOptional(
                              index,
                              _.get(field, "key_object", "")
                            )}
                          >
                            <img src={deleteIcon} alt="delete" />
                          </Delete>
                        </Tooltip>
                      </ColDelCustom>
                    </Row>
                  ))}

                  <AddNew
                    style={{ marginBottom: 0 }}
                    onClick={_onAddOptionalField}
                  >
                    + {t("emailIncoming.addFieldMapping")}
                  </AddNew>
                </OptionalFields>
              </WrapMapping>
            )}
          </>
        )}

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalEmailIncoming);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .selected_google {
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapMapping = styled.div`
  padding: 16px;
  background: #f2f4f5;
  border: 1px solid #ececec;
  border-radius: 5px;
`;

const AddNew = styled.div`
  line-height: 22px;
  color: ${(props) => props.theme.main};
  margin-bottom: 8px;
  cursor: pointer;
  width: fit-content;
`;

const OptionalFields = styled.div``;

const ColCustom = styled(Col)`
  flex: 0 0 44%;

  @media screen and (max-width: 1499px) {
    flex: 0 0 41%;
  }
`;

const ColDelCustom = styled(Col)`
  display: flex;
  justify-content: flex-end;
  flex: 0 0 6%;

  @media screen and (max-width: 1499px) {
    flex: 0 0 9%;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 32px;
  height: 32px;

  :hover {
    cursor: pointer;
    background: #eeeeee;
  }

  img {
    width: 20px;
  }
`;

const FormCustom = styled(Form.Item)`
  margin-top: 24px;
  margin-bottom: 10px;

  .ant-form-item-label {
    padding: 0;
  }

  .ant-row {
    align-items: center;
    flex-direction: row !important;
  }
`;

const Port = styled(Form.Item)`
  .ant-form-item-control-input-content {
    display: ${({ isCustom }) => (isCustom ? "flex" : "unset")};
  }
`;
