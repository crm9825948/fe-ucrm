import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { Form, Modal, Input, Select, Radio, InputNumber, Button } from "antd";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { loadFieldsCondition } from "redux/slices/workflows";
import Conditions from "components/Conditions/conditions";
import {
  createAssignmentRule,
  getAssignmentRuleDetailSuccess,
  // runManualAssign,
  updateAssignmentRule,
} from "redux/slices/assignmentRule";
import { Notification } from "components/Notification/Noti";
import BusinessHours from "pages/SLASetting/ServiceType/BusinessHours";
import _ from "lodash";

const { TextArea } = Input;

function ModalAssignmentRule({
  showModalAdd,
  setShowModalAdd,
  objects,
  listGroups,
  objectID,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);

  const typeSLA = [
    {
      label: "24/7",
      value: "calendar",
    },
    {
      label: t("slaSetting.businessHours"),
      value: "business_hours",
    },
  ];

  const [form] = Form.useForm();

  const [dataBusinessHours, $dataBusinessHours] = useState({
    holidays: [],
    shifts: [],
    workingDays: [],
  });

  const [listObjects, setListObjects] = useState([]);
  const [groups, setGroups] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueOr, setValueOr] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [operatorValueAndCapacity, setOperatorValueAndCapacity] = useState([]);
  const [valueAndCapacity, setValueAndCapacity] = useState([]);
  const [allConditionCapacity, setAllConditionCapacity] = useState([]);
  const [operatorValueOrCapacity, setOperatorValueOrCapacity] = useState([]);
  const [valueOrCapacity, setValueOrCapacity] = useState([]);
  const [anyConditionCapacity, setAnyConditionCapacity] = useState([]);
  const [type, setType] = useState("calendar");

  const [objectName, setObjectName] = useState("");
  const { assignmentRuleDetail } = useSelector(
    (state) => state.assignmentRuleReducer
  );

  const handleChangeType = (value) => {
    setType(value.target.value);
  };

  useEffect(() => {
    if (!_.isEmpty(assignmentRuleDetail)) {
      let conditions = JSON.parse(assignmentRuleDetail.filter_condition);
      let capacity = JSON.parse(assignmentRuleDetail.assignment_max_condition);
      form.setFieldsValue({
        name: assignmentRuleDetail.name,
        description: assignmentRuleDetail.description,
        object: assignmentRuleDetail.object_id,
        rule_type: assignmentRuleDetail.rule_type,
        assignment_unit: assignmentRuleDetail.assignment_unit,
        assignment_max: assignmentRuleDetail.assignment_max,
        group_id: assignmentRuleDetail.assignment_group.group_id,
        assign_user: assignmentRuleDetail.assignment_group.assign_user,
        current_assignment_amount:
          assignmentRuleDetail.current_assignment_amount
            ? assignmentRuleDetail.current_assignment_amount
            : 0,
        stop_assign_at: assignmentRuleDetail.stop_assign_at
          ? assignmentRuleDetail.stop_assign_at
          : 0,
        working_day:
          _.get(assignmentRuleDetail, "type", "calendar") === "business_hours"
            ? _.get(assignmentRuleDetail, "working_days", []) || []
            : [],
        type: _.get(assignmentRuleDetail, "type", "calendar"),
      });
      setAllCondition(conditions.and_filter);
      setAnyCondition(conditions.or_filter);
      setAllConditionCapacity(capacity.and_filter);
      setAnyConditionCapacity(capacity.or_filter);
      $dataBusinessHours({
        holidays: _.get(assignmentRuleDetail, "holidays", []) || [],
        shifts: _.get(assignmentRuleDetail, "shift_work", []) || [],
        workingDays: _.get(assignmentRuleDetail, "working_days", []) || [],
      });
      setType(_.get(assignmentRuleDetail, "type", "calendar"));
    } else if (_.isEmpty(assignmentRuleDetail) && !_.isEmpty(allConfig)) {
      form.setFieldsValue({
        working_day: _.get(allConfig, "working_days", []) || [],
      });

      $dataBusinessHours({
        holidays: _.get(allConfig, "holidays", []) || [],
        shifts: _.get(allConfig, "shift_work", []) || [],
        workingDays: _.get(allConfig, "working_days", []) || [],
      });
    }
  }, [allConfig, assignmentRuleDetail, form]);

  useEffect(() => {
    if (Object.keys(objects).length > 0) {
      let tempObjects = [];
      Object.entries(objects).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [objects]);

  useEffect(() => {
    let name = "";
    listObjects.forEach((item) => {
      if (item.value === objectID) {
        name = item.label;
      }
    });
    setObjectName(name);
  }, [objectID, listObjects]);

  useEffect(() => {
    dispatch(
      loadFieldsCondition({
        object_id: objectID,
        api_version: "2",
        show_meta_fields: true,
      })
    );
  }, [objectID, dispatch]);

  useEffect(() => {
    let temp = [];
    listGroups.forEach((group) => {
      temp.push({
        label: group.name,
        value: group._id,
      });
    });
    setGroups(temp);
  }, [listGroups]);

  useEffect(() => {
    if (!showModalAdd) {
      form.resetFields();
      setAllCondition([]);
      setAnyCondition([]);
      setAllConditionCapacity([]);
      setAnyConditionCapacity([]);
      dispatch(getAssignmentRuleDetailSuccess({}));
      $dataBusinessHours({
        holidays: [],
        shifts: [],
        workingDays: [],
      });
      setType("calendar");
    }
    // eslint-disable-next-line
  }, [showModalAdd]);

  // const loadFields = (value) => {
  //   dispatch(
  //     loadFieldsCondition({
  //       object_id: value,
  //       api_version: "2",
  //       show_meta_fields: true,
  //     })
  //   );
  // };

  const _onSubmit = (values) => {
    if (
      dataBusinessHours.workingDays.length === 0 ||
      (dataBusinessHours.workingDays.length > 0 &&
        dataBusinessHours.shifts.length > 0 &&
        dataBusinessHours.shifts.every((shift) => shift.time.length !== 0))
    ) {
      let resultShifts = [];
      dataBusinessHours.shifts.forEach((shift) => {
        let applicable_weekdays = [
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
          "False",
        ];

        shift.day.forEach((item) => {
          applicable_weekdays[item] = "True";
        });
        resultShifts.push({
          from_time: {
            hour: parseInt(shift.time[0].slice(0, 2)),
            minute: parseInt(shift.time[0].slice(3, 5)),
          },
          to_time: {
            hour: parseInt(shift.time[1].slice(0, 2)),
            minute: parseInt(shift.time[1].slice(3, 5)),
          },
          applicable_weekdays: [...applicable_weekdays],
        });
      });

      let flag = false;
      if (operatorValueAnd.length === allCondition.length) {
        operatorValueAnd.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }
      if (operatorValueOr.length === anyCondition.length) {
        operatorValueOr.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }
      //value
      valueAnd.forEach((item, idx) => {
        if (
          item === "" &&
          operatorValueAnd[idx] !== "empty" &&
          operatorValueAnd[idx] !== "not-empty" &&
          operatorValueAnd[idx] !== "mine" &&
          operatorValueAnd[idx] !== "not-mine" &&
          operatorValueAnd[idx] !== "today" &&
          operatorValueAnd[idx] !== "not-today" &&
          operatorValueAnd[idx] !== "yesterday" &&
          operatorValueAnd[idx] !== "this-week" &&
          operatorValueAnd[idx] !== "last-week" &&
          operatorValueAnd[idx] !== "this-month" &&
          operatorValueAnd[idx] !== "last-month" &&
          operatorValueAnd[idx] !== "this-year" &&
          operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
          operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
          operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
          operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
        ) {
          flag = true;
        }
        if (item === undefined) {
          flag = true;
        }
      });
      valueOr.forEach((item, idx) => {
        if (
          item === "" &&
          operatorValueOr[idx] !== "empty" &&
          operatorValueOr[idx] !== "not-empty" &&
          operatorValueOr[idx] !== "mine" &&
          operatorValueOr[idx] !== "not-mine" &&
          operatorValueOr[idx] !== "today" &&
          operatorValueOr[idx] !== "not-today" &&
          operatorValueOr[idx] !== "yesterday" &&
          operatorValueOr[idx] !== "this-week" &&
          operatorValueOr[idx] !== "last-week" &&
          operatorValueOr[idx] !== "this-month" &&
          operatorValueOr[idx] !== "last-month" &&
          operatorValueOr[idx] !== "this-year" &&
          operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
          operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
          operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
          operatorValueOr[idx] !== "$ninWorkingDayUCRM"
        ) {
          flag = true;
        }
        if (item === undefined) {
          flag = true;
        }
      });

      if (operatorValueAndCapacity.length === allConditionCapacity.length) {
        operatorValueAndCapacity.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }

      if (operatorValueOrCapacity.length === anyConditionCapacity.length) {
        operatorValueOrCapacity.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }

      valueAndCapacity.forEach((item, idx) => {
        if (
          item === "" &&
          operatorValueAndCapacity[idx] !== "empty" &&
          operatorValueAndCapacity[idx] !== "not-empty" &&
          operatorValueAndCapacity[idx] !== "mine" &&
          operatorValueAndCapacity[idx] !== "not-mine" &&
          operatorValueAndCapacity[idx] !== "today" &&
          operatorValueAndCapacity[idx] !== "yesterday" &&
          operatorValueAndCapacity[idx] !== "this-week" &&
          operatorValueAndCapacity[idx] !== "last-week" &&
          operatorValueAndCapacity[idx] !== "this-month" &&
          operatorValueAndCapacity[idx] !== "last-month" &&
          operatorValueAndCapacity[idx] !== "this-year" &&
          operatorValueAndCapacity[idx] !== "$inWorkingTimeUCRM" &&
          operatorValueAndCapacity[idx] !== "$ninWorkingTimeUCRM" &&
          operatorValueAndCapacity[idx] !== "$inWorkingDayUCRM" &&
          operatorValueAndCapacity[idx] !== "$ninWorkingDayUCRM"
        ) {
          flag = true;
        }
        if (item === undefined) {
          flag = true;
        }
      });
      valueOrCapacity.forEach((item, idx) => {
        if (
          item === "" &&
          operatorValueOrCapacity[idx] !== "empty" &&
          operatorValueOrCapacity[idx] !== "not-empty" &&
          operatorValueOrCapacity[idx] !== "mine" &&
          operatorValueOrCapacity[idx] !== "not-mine" &&
          operatorValueOrCapacity[idx] !== "today" &&
          operatorValueOrCapacity[idx] !== "yesterday" &&
          operatorValueOrCapacity[idx] !== "this-week" &&
          operatorValueOrCapacity[idx] !== "last-week" &&
          operatorValueOrCapacity[idx] !== "this-month" &&
          operatorValueOrCapacity[idx] !== "last-month" &&
          operatorValueOrCapacity[idx] !== "this-year" &&
          operatorValueOrCapacity[idx] !== "$inWorkingTimeUCRM" &&
          operatorValueOrCapacity[idx] !== "$ninWorkingTimeUCRM" &&
          operatorValueOrCapacity[idx] !== "$inWorkingDayUCRM" &&
          operatorValueOrCapacity[idx] !== "$ninWorkingDayUCRM"
        ) {
          flag = true;
        }
        if (item === undefined) {
          flag = true;
        }
      });

      if (flag) {
        Notification("warning", "Please fullfill conditions!");
      } else {
        if (assignmentRuleDetail._id) {
          dispatch(
            updateAssignmentRule({
              data: {
                name: values.name,
                object_id: objectID,
                assignment_unit: values.assignment_unit,
                rule_type: values.rule_type,
                description: values.description,
                assignment_max: values.assignment_max,
                current_assignment_amount: values.current_assignment_amount
                  ? values.current_assignment_amount
                  : 0,
                stop_assign_at: values.stop_assign_at
                  ? values.stop_assign_at
                  : 0,
                assignment_group: {
                  group_id: values.group_id,
                  assign_user: values.assign_user,
                },
                filter_condition: JSON.stringify({
                  and_filter: allCondition,
                  or_filter: anyCondition,
                }),
                assignment_max_condition: JSON.stringify({
                  and_filter: allConditionCapacity,
                  or_filter: anyConditionCapacity,
                }),
                working_days:
                  type === "business_hours"
                    ? _.get(values, "working_day", [])
                    : [],
                holidays:
                  type === "business_hours"
                    ? _.get(dataBusinessHours, "holidays", [])
                    : [],
                shift_work: type === "business_hours" ? resultShifts : [],
                type: type,
              },
              id: assignmentRuleDetail._id,
              _id: objectID,
            })
          );
        } else {
          dispatch(
            createAssignmentRule({
              data: {
                name: values.name,
                object_id: objectID,
                assignment_unit: values.assignment_unit,
                rule_type: values.rule_type,
                description: values.description,
                assignment_max: values.assignment_max,
                current_assignment_amount: values.current_assignment_amount
                  ? values.current_assignment_amount
                  : 0,
                stop_assign_at: values.stop_assign_at
                  ? values.stop_assign_at
                  : 0,
                assignment_group: {
                  group_id: values.group_id,
                  assign_user: values.assign_user,
                },
                filter_condition: JSON.stringify({
                  and_filter: allCondition,
                  or_filter: anyCondition,
                }),
                assignment_max_condition: JSON.stringify({
                  and_filter: allConditionCapacity,
                  or_filter: anyConditionCapacity,
                }),
                working_days:
                  type === "business_hours"
                    ? _.get(values, "working_day", [])
                    : [],
                holidays:
                  type === "business_hours"
                    ? _.get(dataBusinessHours, "holidays", [])
                    : [],
                shift_work: type === "business_hours" ? resultShifts : [],
                type: type,
              },
              _id: objectID,
            })
          );
        }

        setShowModalAdd(false);
      }
    } else {
      Notification("warning", "Please full fields shift work!");
    }
  };

  const _onCancel = () => {
    setShowModalAdd(false);
    form.resetFields();
  };

  return (
    <ModalCustom
      title="Assignment rule"
      visible={showModalAdd}
      footer={null}
      width={1000}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 7 }}
        wrapperCol={{ span: 17 }}
        colon={false}
        labelAlign="left"
        initialValues={{
          rule_type: "random",
          assignment_unit: 1,
          assign_user: "online",
          current_assignment_amount: 0,
          stop_assign_at: 0,
        }}
      >
        <ContentWrap>
          <BasicInfo>
            <legend>{t("assignmentRule.basic")}</legend>
            <>
              <Form.Item
                label={t("assignmentRule.name")}
                name="name"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
              <Form.Item
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
                label={t("common.description")}
                name="description"
              >
                <TextArea placeholder={t("assignmentRule.type")} />
              </Form.Item>
              <Form.Item label={t("object.object")} name="object">
                <CustomSelect
                  disabled={true}
                  options={listObjects}
                  placeholder={objectName}
                />
              </Form.Item>

              <BasicInfo>
                <legend>{t("assignmentRule.filter")}</legend>
                <Conditions
                  title={t("common.allCondition")}
                  decs={`(${t("common.descAllCondition")})`}
                  conditions={allCondition}
                  setConditions={setAllCondition}
                  operatorValue={operatorValueAnd}
                  setOperatorValue={setOperatorValueAnd}
                  value={valueAnd}
                  setValue={setValueAnd}
                  ID={
                    Object.keys(assignmentRuleDetail).length > 0 &&
                    assignmentRuleDetail._id
                      ? assignmentRuleDetail._id
                      : ""
                  }
                  dataDetails={
                    Object.keys(assignmentRuleDetail).length > 0 &&
                    assignmentRuleDetail
                      ? assignmentRuleDetail
                      : {}
                  }
                />
                <Conditions
                  title={t("common.anyCondition")}
                  decs={`(${t("common.descAnyCondition")})`}
                  conditions={anyCondition}
                  setConditions={setAnyCondition}
                  operatorValue={operatorValueOr}
                  setOperatorValue={setOperatorValueOr}
                  value={valueOr}
                  setValue={setValueOr}
                  ID={
                    Object.keys(assignmentRuleDetail).length > 0 &&
                    assignmentRuleDetail._id
                      ? assignmentRuleDetail._id
                      : ""
                  }
                  dataDetails={
                    Object.keys(assignmentRuleDetail).length > 0 &&
                    assignmentRuleDetail
                      ? assignmentRuleDetail
                      : {}
                  }
                />
              </BasicInfo>
              <CustomSpace></CustomSpace>

              <Form.Item label="Rule type" name="rule_type">
                <Radio.Group>
                  <Radio value="random">Random</Radio>
                  <Radio value="round_robin">Round robin</Radio>
                </Radio.Group>
              </Form.Item>

              <Form.Item
                label={t("assignmentRule.batchAssignmentSize")}
                name="assignment_unit"
                rules={[
                  { required: true, message: t("common.placeholderInput") },
                ]}
              >
                <InputNumber min={1} style={{ width: "100%" }} />
              </Form.Item>

              <Form.Item
                label="Group ID"
                name="group_id"
                rules={[
                  { required: true, message: t("common.placeholderSelect") },
                ]}
              >
                <Select
                  placeholder={t("common.placeholderSelect")}
                  options={groups}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item label="Assign to user" name="assign_user">
                <Radio.Group>
                  <Radio value="online">Online</Radio>
                  <Radio value="offline">Offline</Radio>
                  <Radio value="both">Both</Radio>
                </Radio.Group>
              </Form.Item>
            </>
          </BasicInfo>
          <Wrap>
            <BasicInfo>
              <legend>{t("assignmentRule.workloadRules")}</legend>
              <InputWrap>
                {/* <Form.Item
                  label="Assignment Unit"
                  name="assignment_unit"
                  rules={[
                    { required: true, message: t("common.placeholderInput") },
                  ]}
                >
                  <InputNumber min={1} />
                </Form.Item> */}
                <Form.Item
                  label={t("assignmentRule.capacityLimit")}
                  name="assignment_max"
                >
                  <InputNumber min={1} />
                </Form.Item>
              </InputWrap>

              {/* <ItemWrap>
                <Form.Item
                  label="Current records assigned"
                  name="current_assignment_amount"
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    min={0}
                  />
                </Form.Item>
                <Form.Item
                  label="Maximum records assignment"
                  name="stop_assign_at"
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    min={0}
                  />
                </Form.Item>
              </ItemWrap> */}
              <Conditions
                title={t("common.allCondition")}
                decs={`(${t("common.descAllCondition")})`}
                conditions={allConditionCapacity}
                setConditions={setAllConditionCapacity}
                operatorValue={operatorValueAndCapacity}
                setOperatorValue={setOperatorValueAndCapacity}
                value={valueAndCapacity}
                setValue={setValueAndCapacity}
                ID={
                  Object.keys(assignmentRuleDetail).length > 0 &&
                  assignmentRuleDetail._id
                    ? assignmentRuleDetail._id
                    : ""
                }
                dataDetails={
                  Object.keys(assignmentRuleDetail).length > 0 &&
                  assignmentRuleDetail
                    ? assignmentRuleDetail
                    : {}
                }
              />
              <Conditions
                title={t("common.anyCondition")}
                decs={`(${t("common.descAnyCondition")})`}
                conditions={anyConditionCapacity}
                setConditions={setAnyConditionCapacity}
                operatorValue={operatorValueOrCapacity}
                setOperatorValue={setOperatorValueOrCapacity}
                value={valueOrCapacity}
                setValue={setValueOrCapacity}
                ID={
                  Object.keys(assignmentRuleDetail).length > 0 &&
                  assignmentRuleDetail._id
                    ? assignmentRuleDetail._id
                    : ""
                }
                dataDetails={
                  Object.keys(assignmentRuleDetail).length > 0 &&
                  assignmentRuleDetail
                    ? assignmentRuleDetail
                    : {}
                }
              />
            </BasicInfo>
          </Wrap>

          <Wrap>
            <BasicInfo>
              <legend>Working time</legend>

              <Form.Item label={t("slaSetting.chooseType")} name="type">
                <Radio.Group
                  optionType="button"
                  options={typeSLA}
                  defaultValue="calendar"
                  onChange={handleChangeType}
                />
              </Form.Item>

              {type === "business_hours" && (
                <BusinessHours
                  form={form}
                  dataBusinessHours={dataBusinessHours}
                  $dataBusinessHours={$dataBusinessHours}
                  required={false}
                />
              )}
            </BasicInfo>
          </Wrap>

          <ButtonWrap>
            {/* {Object.keys(assignmentRuleDetail).length > 0 && (
              <>
                <CustomButton
                  onClick={() => {
                    dispatch(
                      runManualAssign({
                        id: assignmentRuleDetail._id,
                      })
                    );
                    setShowModalAdd(false);
                  }}
                >
                  Run muanual
                </CustomButton>
              </>
            )} */}
            <CustomButton onClick={() => setShowModalAdd(false)}>
              {t("common.cancel")}
            </CustomButton>
            <CustomButton htmlType="submit">{t("common.save")}</CustomButton>
          </ButtonWrap>
        </ContentWrap>
      </Form>
    </ModalCustom>
  );
}

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper {
    width: 148px;
    text-align: center;

    :hover {
      color: ${(props) => props.theme.main};
    }
  }
`;

// const ItemWrap = styled.div`
//   width: 100%;
//   margin-top: -10px;
//   margin-bottom: 10px;
//   display: flex;
//   justify-content: space-between;
//   .ant-form-item {
//     width: 50% !important;
//     .ant-col-7 {
//       max-width: 60%;
//       flex: auto;
//     }
//   }
// `;

const ContentWrap = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const BasicInfo = styled.fieldset`
  width: 100%;
  padding: 24px 24px 0 24px;
  border: 1px solid #ececec;
  border-radius: 5px;

  /* height: fit-content; */
  /* max-height: 600px; */
  /* overflow: auto; */
  textarea.ant-input {
    height: 150px;
  }

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 14px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const CustomSelect = styled(Select)`
  .ant-select-arrow {
    display: none;
  }
`;

const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
`;
const CustomButton = styled(Button)`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-right: 16px;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  &:last-child {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
    margin-right: 0;
  }
`;

const Wrap = styled.div`
  width: 100%;
  margin-top: 16px;
  /* max-height: 600px; */
  /* overflow: auto; */
`;

const CustomSpace = styled.div`
  margin-top: 24px;
`;

const InputWrap = styled.div`
  label {
    font-size: 16px;
    line-height: 22px;
    white-space: pre-line;
  }
  .ant-input-number {
    width: 100%;
  }
`;

export default ModalAssignmentRule;
