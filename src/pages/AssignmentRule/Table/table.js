import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import { Tooltip, Typography } from "antd";
import _ from "lodash";

import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deleteAssignmentRule,
  getAssignmentRuleDetail,
  updatePriorityRule,
} from "redux/slices/assignmentRule";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { useSelector, useDispatch } from "react-redux";

const { Text: TextComponent } = Typography;

const Table = ({
  listRecord,
  listObjects,
  objectID,
  listAllGroups,
  setShowModalAdd,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const [idRecord, setIdRecord] = useState("");

  const [listUpdate, setListUpdate] = useState([]);
  const [objectName, setObjectName] = useState("");

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "assignment_rules" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    let temp = [...listRecord];
    temp.sort((a, b) => {
      return a.priority - b.priority;
    });

    setListUpdate(temp);
  }, [listRecord]);

  useEffect(() => {
    let name = "";
    listObjects.forEach((item) => {
      if (item.value === objectID) {
        name = item.label;
      }
    });
    setObjectName(name);
  }, [objectID, listObjects]);

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const handleName = (id) => {
    let name = "";
    listAllGroups.forEach((item) => {
      if (item._id === id) {
        name = item.name;
      }
    });
    return name;
  };

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const newList = reorder(
      listUpdate,
      result.source.index,
      result.destination.index
    );

    let temp = [];
    newList.forEach((item, index) => {
      temp.push({
        ...item,
        priority: index,
      });
    });
    let temUpdate = [];
    temp.forEach((item) => {
      temUpdate.push({
        priority: item.priority,
        _id: item._id,
      });
    });

    dispatch(
      updatePriorityRule({
        data: {
          list_priority: temUpdate,
        },
        id: objectID,
        _id: objectID,
      })
    );
    setListUpdate(temp);
  };

  return (
    <TableWrap>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <CustomWrap {...provided.droppableProps} ref={provided.innerRef}>
              <table>
                <thead className="table-header">
                  <tr>
                    <th>Object ID</th>
                    <th>Name</th>
                    <th>Group ID</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody className="table-body">
                  {listUpdate.map((item, index) => (
                    <Draggable
                      key={item._id}
                      draggableId={item._id}
                      index={index}
                      isDragDisabled={!checkRule("edit")}
                    >
                      {(provided, snapshot) => (
                        <tr
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <td>
                            <TextComponent ellipsis={{ tooltip: objectName }}>
                              {objectName}
                            </TextComponent>
                          </td>
                          <td>
                            <TextComponent ellipsis={{ tooltip: item.name }}>
                              {item.name}
                            </TextComponent>
                          </td>
                          <td>
                            <TextComponent
                              ellipsis={{
                                tooltip: handleName(
                                  item.assignment_group.group_id
                                ),
                              }}
                            >
                              {handleName(item.assignment_group.group_id)}
                            </TextComponent>
                          </td>
                          <td>
                            <WrapAction>
                              {checkRule("edit") && (
                                <Tooltip title={t("common.edit")}>
                                  <img
                                    onClick={() => {
                                      dispatch(
                                        getAssignmentRuleDetail({
                                          id: item._id,
                                        })
                                      );
                                      setShowModalAdd(true);
                                    }}
                                    src={Edit}
                                    alt="edit"
                                  />
                                </Tooltip>
                              )}

                              {checkRule("delete") && (
                                <Tooltip title={t("common.delete")}>
                                  <img
                                    onClick={() => {
                                      setIdRecord(item._id);
                                      dispatch(setShowModalConfirmDelete(true));
                                    }}
                                    src={Delete}
                                    alt="delete"
                                  />
                                </Tooltip>
                              )}
                            </WrapAction>
                          </td>
                        </tr>
                      )}
                    </Draggable>
                  ))}
                </tbody>
              </table>
              {provided.placeholder}
              {/* {listUpdate.map((item, index) => (
              <Draggable key={item._id} draggableId={item._id} index={index}>
                {(provided, snapshot) => (
                  <div
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                  >
                    {item.name}
                  </div>
                )}
              </Draggable>
            ))} */}
            </CustomWrap>
          )}
        </Droppable>
      </DragDropContext>
      <ModalConfimDelete
        title={"record"}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác. Dữ liệu con cũng sẽ bị xóa theo record."
        methodDelete={deleteAssignmentRule}
        dataDelete={{ id: idRecord, _id: objectID }}
        isLoading={showLoadingScreen}
      />
    </TableWrap>
  );
};

const TableWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  &::-webkit-scrollbar {
    height: 8px !important;
  }

  table {
    width: max-content;
    table-layout: auto;
    padding: 8px;
    background: #fff;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }

    .table-header th {
      width: calc((100vw - 310px) / 3);

      text-align: left;
      padding: 8px;
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      overflow: hidden;
      background: #f0f0f0;
      min-width: 100px;
      white-space: nowrap;

      &:first-child {
        box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:last-child {
        resize: unset;
        width: 150px;
        max-width: 150px;
        border-right: 1px solid #ddd;
      }
    }

    .table-body td {
      width: calc((100vw - 310px) / 3);
      min-width: calc((100vw - 310px) / 3);

      background: #fff;
      border-bottom: 1px solid #ddd;
      padding: 8.5px 16px;
      height: 100%;
      max-width: 200px;
      :first-child {
        box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9,
          inset 0px 0px 0px #d9d9d9;
      }
      :last-child {
        width: 150px;
        min-width: 150px;

        border-right: 1px solid #ddd;
      }
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    width: 20px;
    :first-child {
      margin-right: 10px;
    }
  }
`;
const CustomWrap = styled.div`
  background-color: #fff;
  padding: 16px;
  width: 100%;
`;

export default Table;
