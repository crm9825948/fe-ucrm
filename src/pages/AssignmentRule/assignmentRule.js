import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import Breadcrumb from "antd/lib/breadcrumb";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import Button from "antd/lib/button";
import EmptyFolder from "assets/images/sharing/EmptyFolder.png";
import _ from "lodash";
import {
  getListAssignmentRule,
  getListAssignmentRuleSuccess,
} from "redux/slices/assignmentRule";
import ModalAssignmentRule from "./ModalAssignmentRule";
import { loadAllGroups } from "redux/slices/group";

import { Select } from "antd";

import { loadConfig } from "redux/slices/datetimeSetting";
import Table from "./Table/table";
import { changeTitlePage } from "redux/slices/authenticated";

const AssignmentRule = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { listAssignmentRule } = useSelector(
    (state) => state.assignmentRuleReducer
  );

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const [showModalAdd, setShowModalAdd] = useState(false);
  const [listObjects, setListObjects] = useState([]);
  const [objectID, setObjectID] = useState("");
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(changeTitlePage("Assignment rule"));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "assignment_rules" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };
  useEffect(() => {
    if (objectID === "") dispatch(getListAssignmentRuleSuccess({}));
  }, [objectID, dispatch]);

  useEffect(() => {
    dispatch(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
    dispatch(loadConfig());
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  const loadListAssignment = (value) => {
    setObjectID(value);
    dispatch(
      getListAssignmentRule({
        _id: value,
      })
    );
  };

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <BreadcrumNavigate onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </BreadcrumNavigate>
          <BreadcrumbItem>Assignment rule</BreadcrumbItem>
        </Breadcrumb>

        {listAssignmentRule.length > 0 && checkRule("create") && (
          <AddButton onClick={() => setShowModalAdd(true)}>
            + Add assignment rule
          </AddButton>
        )}
      </WrapBreadcrumb>

      <ChooseObject>
        <CustomText>{t("assignmentRule.chooseObject")}</CustomText>
        <CustomSelect
          size="large"
          showSearch
          options={listObjects}
          placeholder={t("common.placeholderSelect")}
          optionFilterProp="label"
          onChange={loadListAssignment}
        />
      </ChooseObject>

      {listAssignmentRule.length === 0 && objectID !== "" ? (
        <Empty>
          <img src={EmptyFolder} alt="empty" />
          <p>
            No <span> Assignment Rule</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={() => setShowModalAdd(true)}>
              + Add assignment rule
            </AddButton>
          )}
        </Empty>
      ) : (
        ""
      )}

      {listAssignmentRule.length > 0 && objectID !== "" && (
        <WrapTable>
          <Table
            objectID={objectID}
            listObjects={listObjects}
            listRecord={listAssignmentRule}
            listAllGroups={listAllGroups}
            setShowModalAdd={setShowModalAdd}
          />
        </WrapTable>
      )}

      <ModalAssignmentRule
        setShowModalAdd={setShowModalAdd}
        showModalAdd={showModalAdd}
        objects={category}
        listGroups={listAllGroups}
        objectID={objectID}
      />
    </Wrapper>
  );
};
export default AssignmentRule;

const WrapTable = styled.div`
  width: 100%;
  /* background: #fff;
  padding: 8px; */
  margin-top: 24px;
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: #2c2c2c;
  cursor: default;
`;

const BreadcrumNavigate = styled(Breadcrumb.Item)`
  cursor: pointer;
  font-size: 16px;
`;

const Wrapper = styled.div`
  padding: 16px 24px;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  color: #fff !important;
  font-size: 16px;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
    span {
      color: ${(props) => props.theme.main};
    }
  }

  img {
    margin-bottom: 8px;
  }

  .ant-btn:hover {
    border-color: ${(props) => props.theme.darker};
  }
`;

const ChooseObject = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CustomText = styled.span`
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  margin-right: 20px;
`;

const CustomSelect = styled(Select)`
  width: 400px;
`;

export const ContentWrap = styled.div`
  span {
    color: #2c2c2c;
    font-size: 16px;
  }
  img {
    cursor: pointer;
    :last-child {
      margin-left: 20px;
    }
  }
`;
