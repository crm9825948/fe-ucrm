import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useParams } from "react-router";
import axios from "axios";
import styled from "styled-components/macro";
import { Button, Form } from "antd";
import { BASE_URL_API, SITE_KEY } from "constants/constants";
import { useDispatch, useSelector } from "react-redux";
import { submitForm } from "redux/slices/formSetting";
import ModalResult from "./ModalResult";
import IconSuccess from "assets/icons/common/formSuccess.svg";
import IconFailed from "assets/icons/common/formFaileds.svg";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import FormContent from "./FormContent/FormContent";
import ReCAPTCHA from "react-google-recaptcha";
import moment from "moment";

const FormLink = () => {
  const dispatch = useDispatch();
  const { tenantID, configID, defaultParams } = useParams();
  const [config, setConfig] = useState({});
  const [defaultValue, setDefaultValue] = useState({});
  const { dataReturn } = useSelector((state) => state.formSettingReducer);
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  //eslint-disable-next-line
  const [change, setChange] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [listFileSubmit, setListFileSubmit] = useState([]);
  const [page, setPage] = useState(0);
  const [history, setHistory] = useState(["Default"]);
  const [dataSubmit, setDataSubmit] = useState({});
  const [conditionPagination, setConditionPagination] = useState("");
  const [formPoint, setFormPoint] = useState(0);

  useEffect(() => {
    if (Object.entries(dataReturn).length > 0) {
      setVisible(true);
    } else {
      setVisible(false);
    }
    if (Object.entries(config).length > 0) {
      if (!config.active || !config.status) {
        setVisible(true);
      }
    }
  }, [dataReturn, config]);

  useEffect(() => {
    if (Object.entries(config).length === 0 && isLoading) {
      setVisible(true);
    }
  }, [config, isLoading]);

  useEffect(() => {
    if (defaultParams) {
      let temObject = {};
      defaultParams.split("&").forEach((item) => {
        if (item.split("=").length === 2) {
          temObject = {
            ...temObject,
            [item.split("=")[0]]: item.split("=")[1],
          };
        }
      });
      setDefaultValue(temObject);
    }
  }, [defaultParams]);

  useEffect(() => {
    let data = {
      tenant_id: tenantID,
      _id: configID,
      device: window.innerWidth > 768 ? "desktop" : "mobile",
      outer_domain: window.location.origin,
    };
    if (tenantID && configID && Object.entries(config).length === 0) {
      axios
        .post(`${BASE_URL_API}internet-form/config/display`, {
          data: data,
        })
        .then((res) => {
          let temSection = ["Default"];
          if (res.data.data.sections && res.data.data.sections.length > 0) {
            temSection = res.data.data.sections;
          }
          let tem = {
            ...res.data.data,
            sections: temSection,
          };
          res.data.data.list_field.forEach((field) => {
            if (field.default_value !== undefined) {
              if (
                field.field_type === "date" ||
                field.field_type === "datetime-local"
              ) {
                form.setFieldsValue({
                  [field.id]: moment(field.default_value),
                });
              } else {
                form.setFieldsValue({ [field.id]: field.default_value });
              }
            }
          });
          setConfig(tem);
          setHistory([tem.sections[0]]);
          setIsLoading(true);
        })
        .catch((err) => {
          setIsLoading(true);
        });
    }
    //eslint-disable-next-line
  }, [tenantID, configID]);
  const [form] = Form.useForm();

  const handleCloseModal = () => {
    form.resetFields();
    setDataSubmit({});
    setPage(0);
    setHistory([config.sections[0]]);
    config.list_field?.forEach((field) => {
      if (field.default_value !== undefined) {
        if (
          field.field_type === "date" ||
          field.field_type === "datetime-local"
        ) {
          form.setFieldsValue({
            [field.id]: moment(field.default_value),
          });
        } else {
          form.setFieldsValue({ [field.id]: field.default_value });
        }
      }
    });
  };

  const handleFinish = (values) => {
    let temp = { ...values, ...defaultValue };
    let fields_data = [];
    if (config.field_total) {
      fields_data.push({ id_field: config.field_total, value: formPoint });
    }
    if (config.field_grading) {
      fields_data.push({
        id_field: config.field_grading,
        value: handleGrading(),
      });
    }

    const formDataSave = new FormData();
    Object.entries(temp).forEach(([key, value]) => {
      let tem = value;
      if (config.list_field && config.list_field.length > 0) {
        config.list_field.forEach((field) => {
          if (
            key === field.id &&
            (history.includes(field.section) || !field.section)
          ) {
            if (field.field_type !== "file") {
              if (
                field.field_type === "date" ||
                field.field_type === "datetime-local"
              ) {
                if (value) {
                  tem = moment(value).format(
                    field.field_type === "date"
                      ? "YYYY-MM-DD"
                      : "YYYY-MM-DD HH:mm:ss"
                  );
                }
              }
              if (field.field_type === "number") {
                if (value) {
                  tem = value;
                } else {
                  tem = 0;
                }
              }
              if (tem === 0 || tem) {
                fields_data.push({ id_field: key, value: tem });
              }
            } else {
              if (value) {
                value.forEach((item) => {
                  formDataSave.append(field.id, item.originFileObj);
                });
              }
            }
          }
        });
      }
    });

    formDataSave.append("form_id", configID);
    formDataSave.append("tenant_id", tenantID);
    formDataSave.append("fields_data", JSON.stringify(fields_data));
    dispatch(submitForm(formDataSave));
    setVisible(true);
    setIsSubmitting(false);
    handleCloseModal();
  };

  const renderTitle = (data) => {
    if (data.message === "success") {
      if (typeof data.data === "string" || typeof data.data === "number") {
        return `${t("form.result")}: ${data.data}`;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  const checkModalResutl = () => {
    if (Object.entries(config).length === 0) {
      return true;
    } else {
      if (!config.active || !config.status) {
        return true;
      } else {
        if (!_.isEmpty(dataReturn)) {
          return true;
        } else {
          return false;
        }
      }
    }
  };

  const handleResetField = (field) => {
    let tem = "";
    if (config.list_field) {
      config.list_field.forEach((item) => {
        if (item.triggering_field === field) {
          form.setFieldsValue({ [item.id]: undefined });
          dataSubmit[item.id] = undefined;
          tem = item.id;
        } else {
          tem = "";
        }
        if (tem) {
          handleResetField(tem);
        }
      });
    }
  };

  const handleGrading = () => {
    let tem = config.default_grading_value;
    config.grading_mapping.every((mapping) => {
      if (formPoint >= mapping.min_val && formPoint <= mapping.max_max) {
        tem = mapping.grading_value;
        return false;
      }
      return true;
    });
    return tem;
  };
  //eslint-disable-next-line
  const countPoint = () => {
    let point = 0;
    let tem = { ...dataSubmit, ...form.getFieldsValue(), ...defaultValue };
    if (config.scoring_option !== "no_scoring") {
      config.list_field?.forEach((field) => {
        if (field.point_options?.length > 0) {
          Object.entries(tem).forEach((item) => {
            if (
              item[0] === field.id &&
              (history.includes(field.section) || !field.section)
            ) {
              field.point_options?.forEach((option) => {
                if (option.option === item[1]) {
                  point = point + option.point;
                }
              });
              return;
            }
          });
        }
      });
    }
    if (config.min_point === 0 || config.min_point) {
      if (point < config.min_point) {
        point = config.min_point;
      }
    }
    if (config.max_point === 0 || config.max_point) {
      if (point > config.max_point) {
        point = config.max_point;
      }
    }
    setFormPoint(point);
  };

  useEffect(() => {
    countPoint();
  }, [countPoint]);

  const checkConditionPagination = useCallback(() => {
    let flag = "";
    let tem = { ...defaultValue, ...form.getFieldsValue() };
    Object.entries(tem).forEach(([key, value]) => {
      config.list_field?.forEach((field) => {
        if (
          field.conditional_pagination &&
          field.pagination_option &&
          field.id === key &&
          field.section === config.sections[page]
        ) {
          field.pagination_option.every((option) => {
            if (option.value.toString() === value?.toString()) {
              flag = option.option;
              return false;
            } else {
              return true;
            }
          });
        }
      });
    });
    setConditionPagination(flag);
    //eslint-disable-next-line
  }, [defaultValue, config, page, form, visible]);

  useEffect(() => {
    checkConditionPagination();
  }, [checkConditionPagination]);

  const onChangeHandle = useCallback(
    (searchText) => {
      setChange(searchText);
      countPoint();
      checkConditionPagination();
    },
    [countPoint, checkConditionPagination]
  );

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onChangeHandle, 50),
    [onChangeHandle]
  );

  function onChange(value) {
    if (value) {
      setIsSubmitting(true);
    } else {
      setIsSubmitting(false);
    }
  }

  const handleOldData = (listData = [], ordereds = ["Default"]) => {
    let tem = [];
    listData.forEach((item) => {
      tem.push({
        ...item,
        section: item.section ? item.section : ordereds[0],
      });
    });
    return tem;
  };

  const checkIndexPage = (name, ordereds = ["Default"]) => {
    let index = ordereds.findIndex((item) => item === name);
    return index;
  };
  const handleNext = (values) => {
    let newData = { ...dataSubmit, ...values };
    if (
      page === config.sections.length - 1 ||
      conditionPagination === "submit_form_crm"
    ) {
      handleFinish(newData);
    } else {
      setDataSubmit(newData);
      if (
        conditionPagination &&
        checkIndexPage(conditionPagination, config.sections) > page
      ) {
        let tem = [...history, conditionPagination];
        setHistory(tem);
        setPage(checkIndexPage(conditionPagination, config.sections));
      } else {
        setPage((prev) => {
          let tem = [...history, config.sections[prev + 1]];
          setHistory(tem);
          return prev + 1;
        });
      }
    }
  };
  const handleBack = () => {
    let tem = [...history];
    tem.pop();
    setHistory(tem);
    setPage(checkIndexPage(tem[tem.length - 1], config.sections));
    setDataSubmit((prev) => ({
      ...prev,
      ...form.getFieldsValue(),
    }));
  };

  return (
    <Wrap>
      {Object.entries(config).length > 0 &&
        Object.entries(dataReturn).length === 0 &&
        config.status &&
        config.active && (
          <WrapForm dark_screen={config.dark_screen}>
            <WrapDisplay
              device={window.innerWidth > 768 ? "desktop" : "mobile"}
              className={config.position_display}
              config={config}
            >
              <FromWrap>
                <IntroWrap>
                  {config.form_name && <Name>{config.form_name}</Name>}
                  {/* <Name>{config.sections[page]}</Name> */}
                  {config.form_description && (
                    <Description>{config.form_description}</Description>
                  )}
                </IntroWrap>
                <Form
                  // onFinish={handleFinish}
                  onFinish={handleNext}
                  form={form}
                  labelCol={{ span: 24 }}
                  wrapperCol={{ span: 24 }}
                  onValuesChange={(value, values) => {
                    debouncedSearchHandler(value);
                    handleResetField(Object.keys(value)[0]);
                  }}
                >
                  <FormContent
                    // fields={config.list_field}
                    fields={handleOldData(config.list_field, config.sections)}
                    form={form}
                    defaultValue={defaultValue}
                    listFileSubmit={listFileSubmit}
                    setListFileSubmit={setListFileSubmit}
                    listOrdered={config.sections}
                    page={page}
                  />
                </Form>
                {config.scoring_option !== "no_scoring" && (
                  <Result>
                    <span>Tổng điểm : {formPoint}</span>
                  </Result>
                )}
                {config.check_grading && (
                  <Result>
                    <span>Xếp hạng : {handleGrading()}</span>
                  </Result>
                )}
                <ReCAPTCHA
                  sitekey={SITE_KEY}
                  onChange={onChange}
                  style={{
                    marginBottom: "12px",
                    display:
                      page === config.sections.length - 1 ||
                      conditionPagination === "submit_form_crm"
                        ? "block"
                        : "none",
                  }}
                />
                <ButtonWrap>
                  {page === config.sections.length - 1 ||
                  conditionPagination === "submit_form_crm" ? (
                    <CustomButton
                      onClick={() => form.submit()}
                      disabled={!isSubmitting}
                    >
                      {config.form_save}
                    </CustomButton>
                  ) : (
                    <CustomButton onClick={() => form.submit()}>
                      {t("form.next")}
                    </CustomButton>
                  )}

                  {page !== 0 && (
                    <CustomButton onClick={handleBack}>
                      {t("form.back")}
                    </CustomButton>
                  )}
                  <CustomButton onClick={() => handleCloseModal()}>
                    {config.form_cancel}
                  </CustomButton>
                </ButtonWrap>
              </FromWrap>
            </WrapDisplay>
          </WrapForm>
        )}

      {checkModalResutl() && (
        <ModalResult
          image={dataReturn.message === "success" ? IconSuccess : IconFailed}
          title={
            dataReturn.message === "success"
              ? t("form.titleSuccess")
              : t("form.titleFailed")
          }
          visible={visible}
          contentButton={
            dataReturn.message === "success" && t("form.submitAnother")
          }
          description={renderTitle(dataReturn)}
          setVisible={setVisible}
        />
      )}
    </Wrap>
  );
};

export default FormLink;

const Wrap = styled.div`
  /* width: 100%;
  height: 100%; */
  position: relative;
  .top_left {
    top: 20px;
    left: 20px;
  }
  .top_center {
    top: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .top_right {
    top: 20px;
    right: 20px;
  }
  .center_left {
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .center_center {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center_right {
    right: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .bottom_left {
    left: 20px;
    bottom: 20px;
  }
  .bottom_center {
    bottom: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .bottom_right {
    right: 20px;
    bottom: 20px;
  }
`;
const WrapForm = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background: ${(props) =>
    props.dark_screen ? "rgba(0, 0, 0, 0.45)" : "transparent"};
  line-height: 22px;
`;

const WrapDisplay = styled.div`
  padding: ${(props) =>
    props.device === "desktop"
      ? `${props.config.desktop_top}px ${props.config.desktop_right}px ${props.config.desktop_bottom}px ${props.config.desktop_left}px`
      : `${props.config.mobile_top}px ${props.config.mobile_right}px ${props.config.mobile_bottom}px ${props.config.mobile_left}px`};
  width: ${(props) =>
    props.device === "desktop"
      ? props.config.desktop_width
      : props.config.mobile_width}px;
  height: ${(props) =>
    props.device === "desktop"
      ? props.config.desktop_height
      : props.config.mobile_height}px;

  label {
    font-size: ${(props) =>
      props.device === "desktop"
        ? `${props.config.font_desktop}px`
        : `${props.config.font_mobile}px`} !important;
    color: ${(props) => props.config.font_color};
    font-style: ${(props) =>
      props.config.font_style === "italic" ? "italic" : "normal"} !important;
    font-family: ${(props) =>
      props.config.font_style === "bold"
        ? "var(--roboto-500)"
        : "var(--roboto-400)"};
    position: relative;
    white-space: pre-line;
    height: unset;
    /* ::before {
      position: absolute;
      right: -12px;
      top: 5px;
    } */
  }
  background: #fff;
  background-image: ${(props) =>
    props.config.background_url ? `url(${props.config.background_url})` : ""};
  max-width: 90%;
  max-height: 90%;
  position: absolute;
  border-radius: ${(props) => (props.config.rounded ? "5px" : "0px")};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: ${(props) => props.config.background_position};
  overflow: auto;
  z-index: 100;
`;

const FromWrap = styled.div`
  max-width: 100%;
  max-height: 100%;

  overflow: auto;
  background: #fff;
  padding: 16px;
  .ant-col-24.ant-form-item-label {
    padding: 2px;
  }
  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #fff;
    border-radius: 20px;
  }

  ::-webkit-scrollbar-thumb {
    background: #d8d6d6;
    border-radius: 20px;
  }
  .ant-form-item {
    margin-bottom: 12px !important;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-input-number:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-picker-focused,
  .ant-picker:hover,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle,
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after,
  .ant-radio-input:focus + .ant-radio-inner {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none !important;
  }
  .ant-slider-track,
  .ant-slider:hover .ant-slider-track,
  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const IntroWrap = styled.div``;
const Name = styled.div`
  font-size: 18px;
  font-weight: 500;
  text-align: center;
  margin-bottom: 8px;
`;

const Description = styled.div`
  text-align: center;
  margin-bottom: 8px;
  font-size: 16px;
`;

const ButtonWrap = styled.div`
  width: 100%;
`;
const CustomButton = styled(Button)`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  transition: all 0.5s;
  border-radius: 5px;
  font-size: 16px;
  margin: 12px 0;
  :not([disabled]):hover,
  :not([disabled]):focus {
    color: #fff;
    border-color: #d9d9d9;
    background: #00ab55;
  }
  :first-child {
    :not([disabled]) {
      background: #00ab55;
      color: #fff;
    }
  }
`;

const Result = styled.div`
  margin-bottom: 10px;
`;
