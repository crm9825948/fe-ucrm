import React, { useState, useEffect } from "react";
import { useParams } from "react-router";

import styled from "styled-components/macro";
import DesignForm from "./DesignForm";
import { useDispatch, useSelector } from "react-redux";
import {
  getFieldObject,
  getFormSetting,
  getListRelatedObject,
  unMountFormSetting,
  updateListFieldOject,
} from "redux/slices/formSetting";

const FormDetail = () => {
  const dispatch = useDispatch();

  const { objectId, configId } = useParams();
  const { listField, listObjectRelated } = useSelector(
    (state) => state.formSettingReducer
  );

  const [fields, setFields] = useState([]);
  const [allField, setAllField] = useState([]);

  useEffect(() => {
    return () => dispatch(unMountFormSetting());
  }, [dispatch]);

  useEffect(() => {
    if (configId) {
      dispatch(getFormSetting({ config_id: configId }));
    }
  }, [dispatch, configId]);

  useEffect(() => {
    let temp = [];
    let all = [];
    const typeAccept = [
      "text",
      "textarea",
      "number",
      "date",
      "email",
      "select",
      "file",
      "datetime-local",
    ];
    if (
      listField.length > 0 &&
      listField[listField.length - 1]["main_object"] &&
      listField[listField.length - 1]["main_object"]["sections"]
    ) {
      listField[listField.length - 1]["main_object"]["sections"].forEach(
        (section) => {
          section.fields.forEach((field) => {
            if (
              !field.hidden &&
              typeAccept.includes(field.type) &&
              field.ID !== "fld_qm_form_attach_file_01" &&
              field.ID !== "fld_qm_form_name_01" &&
              field.ID !== "fld_qm_form_id_01" &&
              field.ID !== "created_date" &&
              field.ID !== "modify_time"
            ) {
              temp.push(field);
            }
            if (!field.hidden) {
              all.push(field);
            }
          });
        }
      );
    }
    if (temp.length > 0) {
      setFields(temp);
      dispatch(updateListFieldOject(temp));
    }
    if (all.length > 0) {
      setAllField(all);
    }
  }, [dispatch, listField]);

  useEffect(() => {
    if (objectId) {
      dispatch(
        getFieldObject({
          data: {
            object_id: objectId,
            api_version: "2",
            show_meta_fields: true,
          },
        })
      );
      dispatch(getListRelatedObject({ object_id: objectId }));
    }
  }, [objectId, dispatch]);

  return (
    <Wrap>
      <DesignForm
        listObjectRelated={listObjectRelated}
        fields={fields}
        objectId={objectId}
        allField={allField}
      />
    </Wrap>
  );
};

export default FormDetail;

const Wrap = styled.div`
  padding: 24px 24px 0 24px;
`;
