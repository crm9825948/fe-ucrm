import React, { useCallback, useEffect, useMemo, useState } from "react";
import styled from "styled-components/macro";
import axios from "axios";

import { Form } from "antd";
import { BASE_URL_API } from "constants/constants";
import { useDispatch, useSelector } from "react-redux";
import { submitForm } from "redux/slices/formSetting";
import { useTranslation } from "react-i18next";
import ModalResult from "../ModalResult";
import IconSuccess from "assets/icons/common/formSuccess.svg";
import IconFailed from "assets/icons/common/formFaileds.svg";
import _ from "lodash";
import FormContent from "../FormContent/FormContent";

const FormView = ({ active, dataMess, setActive, device }) => {
  const [config, setConfig] = useState({});
  const dispatch = useDispatch();
  const { dataReturn } = useSelector((state) => state.formSettingReducer);
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation();
  const [isLoading, setIsLoading] = useState(false);
  //eslint-disable-next-line
  const [change, setChange] = useState("");
  useEffect(() => {
    if (Object.entries(dataReturn).length > 0) {
      setVisible(true);
    } else {
      setVisible(false);
    }
    if (Object.entries(config).length > 0) {
      if (!config.active || !config.status) {
        setVisible(true);
      }
    }
  }, [dataReturn, config]);
  useEffect(() => {
    if (Object.entries(config).length === 0 && isLoading) {
      setVisible(true);
    }
  }, [config, isLoading]);

  useEffect(() => {
    if (Object.entries(config).length === 0) {
      axios
        .post(`${BASE_URL_API}internet-form/config/display`, {
          data: dataMess,
        })
        .then((res) => {
          setConfig(res.data.data);
          setIsLoading(true);
        })
        .catch((err) => {
          console.log("err", err);
          setIsLoading(true);
        });
    }
    //eslint-disable-next-line
  }, [dataMess]);

  useEffect(() => {
    if (Object.entries(config).length > 0) {
      window.parent.postMessage(
        {
          messageType: "send_data_from_modal",
          value: {
            is_access: config.is_access,
            is_click: config.is_click,
            is_not_interact: config.is_not_interact,
            is_scroll: config.is_scroll,
            time_access: config.time_access,
            time_not_interact: config.time_not_interact,
            id_click: config.id_click,
            id_config: config._id,
            scroll_option: config.scroll_option,
            time_display: config.time_display,
            apply_for: config.apply_for,
            is_apply_all: config.is_apply_all,
            list_website: config.list_website,
            display_frequency: config.display_frequency,
            status: config.status,
            active: config.active,
          },
        },
        dataMess.outer_domain
      );
    }
    //eslint-disable-next-line
  }, [config]);
  const [form] = Form.useForm();

  const handleCloseModal = () => {
    let tem = active.filter((item) => item !== dataMess._id);
    setActive(tem);
    form.resetFields();
    setVisible(false);
  };

  const handleFinish = (values) => {
    let fields_data = [];
    let temp = { ...values };
    if (config.field_total) {
      fields_data.push({ id_field: config.field_total, value: countPoint() });
    }
    if (config.field_grading) {
      fields_data.push({
        id_field: config.field_grading,
        value: handleGrading(),
      });
    }
    const formDataSave = new FormData();
    Object.entries(temp).forEach(([key, value]) => {
      // if (value !== undefined) {
      let tem = value;
      if (config.list_field && config.list_field.length > 0) {
        config.list_field.forEach((field) => {
          if (key === field.id) {
            if (field.field_type !== "file") {
              if (field.field_type === "date") {
                let date = new Date(value);
                tem = date.toISOString().substring(0, 10);
              }
              if (field.field_type === "number") {
                if (value) {
                  tem = value;
                } else {
                  tem = 0;
                }
              }
              if (tem === 0 || tem) {
                fields_data.push({ id_field: key, value: tem });
              }
            } else {
              if (value) {
                value.forEach((item) => {
                  formDataSave.append(field.id, item.originFileObj);
                });
              }
            }
          }
        });
      }
      // }
    });

    // let data = {
    //   form_id: dataMess._id,
    //   tenant_id: dataMess.tenant_id,
    //   fields_data: fields_data,
    // };
    formDataSave.append("form_id", dataMess._id);
    formDataSave.append("tenant_id", dataMess.tenant_id);
    formDataSave.append("fields_data", JSON.stringify(fields_data));
    dispatch(submitForm(formDataSave));
    // setVisible(true);
    // handleCloseModal();
  };

  const renderTitle = (data) => {
    if (data.message === "success") {
      if (typeof data.data === "string" || typeof data.data === "number") {
        return `${t("form.result")}: ${data.data}`;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  const handleResetField = (field) => {
    let tem = "";
    if (config.list_field) {
      config.list_field.forEach((item) => {
        if (item.triggering_field === field) {
          form.setFieldsValue({ [item.id]: undefined });
          tem = item.id;
        } else {
          tem = "";
        }
        if (tem) {
          handleResetField(tem);
        }
      });
    }
  };
  const handleGrading = () => {
    let tem = config.default_grading_value;
    config.grading_mapping.every((mapping) => {
      if (countPoint() >= mapping.min_val && countPoint() <= mapping.max_max) {
        tem = mapping.grading_value;
        return false;
      }
      return true;
    });
    return tem;
  };

  const countPoint = useCallback(() => {
    let point = 0;

    if (config.scoring_option !== "no_scoring") {
      config.list_field.forEach((field) => {
        // if (field.point_options?.length > 0) {
        Object.entries(form.getFieldsValue()).forEach((item) => {
          if (item[0] === field.id) {
            field.point_options?.forEach((option) => {
              if (option.option.toString() === item[1]?.toString()) {
                point = point + option.point;
              }
            });
          }
        });
        // }
      });
    }
    if (config.min_point === 0 || config.min_point) {
      if (point < config.min_point) {
        point = config.min_point;
      }
    }

    if (config.max_point === 0 || config.max_point) {
      if (point > config.max_point) {
        point = config.max_point;
      }
    }
    return point;
  }, [form, config]);

  const onSearchHandle = useCallback((searchText) => {
    setChange(searchText);
  }, []);

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 50),
    [onSearchHandle]
  );
  return (
    <>
      {Object.entries(config).length > 0 &&
        Object.entries(dataReturn).length === 0 &&
        config.status &&
        config.active && (
          <WrapForm
            isActive={active.includes(config._id)}
            dark_screen={config.dark_screen}
          >
            <Wrap
              className={config.position_display}
              config={config}
              device={device}
            >
              <FromWrap>
                <IntroWrap>
                  {config.form_name && <Name>{config.form_name}</Name>}
                  {config.form_description && (
                    <Description>{config.form_description}</Description>
                  )}
                </IntroWrap>
                <Form
                  onFinish={handleFinish}
                  form={form}
                  labelCol={{ span: 24 }}
                  wrapperCol={{ span: 24 }}
                  onValuesChange={(value, values) => {
                    debouncedSearchHandler(value);
                    handleResetField(Object.keys(value)[0]);
                  }}
                >
                  {/* {renderForm(config.list_field)} */}
                  <FormContent
                    fields={config.list_field}
                    form={form}
                    defaultValue={{}}
                  />
                </Form>
                {config.scoring_option !== "no_scoring" && (
                  <Result>
                    <span>Tổng điểm : {countPoint()}</span>
                  </Result>
                )}
                {config.check_grading && (
                  <Result>
                    <span>Xếp hạng : {handleGrading()}</span>
                  </Result>
                )}
                <ButtonWrap>
                  <CustomButton onClick={() => form.submit()}>
                    {config.form_save}
                  </CustomButton>
                  <CustomButton onClick={() => handleCloseModal()}>
                    {config.form_cancel}
                  </CustomButton>
                </ButtonWrap>
              </FromWrap>
            </Wrap>
          </WrapForm>
        )}
      {(!_.isEmpty(dataReturn) || Object.entries(config).length === 0) && (
        <ModalResult
          image={dataReturn.message === "success" ? IconSuccess : IconFailed}
          title={
            dataReturn.message === "success"
              ? t("form.titleSuccess")
              : t("form.titleFailed")
          }
          visible={visible}
          contentButton={
            dataReturn.message === "success" && t("form.submitAnother")
          }
          description={renderTitle(dataReturn)}
          setVisible={setVisible}
          handleCloseModal={handleCloseModal}
        />
      )}
    </>
  );
};

export default React.memo(FormView);

const WrapForm = styled.div`
  display: ${(props) => (props.isActive ? "block" : "none")};
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background: ${(props) =>
    props.dark_screen ? "rgba(0, 0, 0, 0.45)" : "transparent"};
  line-height: 22px;
`;

const Wrap = styled.div`
  padding: ${(props) =>
    props.device === "desktop"
      ? `${props.config.desktop_top}px ${props.config.desktop_right}px ${props.config.desktop_bottom}px ${props.config.desktop_left}px`
      : `${props.config.mobile_top}px ${props.config.mobile_right}px ${props.config.mobile_bottom}px ${props.config.mobile_left}px`};
  width: ${(props) =>
    props.device === "desktop"
      ? props.config.desktop_width
      : props.config.mobile_width}px;
  height: ${(props) =>
    props.device === "desktop"
      ? props.config.desktop_height
      : props.config.mobile_height}px;

  label {
    font-size: ${(props) =>
      props.device === "desktop"
        ? `${props.config.font_desktop}px`
        : `${props.config.font_mobile}px`} !important;
    color: ${(props) => props.config.font_color};
    font-style: ${(props) =>
      props.config.font_style === "italic" ? "italic" : "normal"} !important;
    font-weight: ${(props) =>
      props.config.font_style === "bold" ? "500" : 400};
    position: relative;
    white-space: pre-line;
    height: unset;
    /* ::before {
      position: absolute;
      right: -12px;
      top: 5px;
    } */
  }
  background: #fff;
  background-image: ${(props) =>
    props.config.background_url ? `url(${props.config.background_url})` : ""};
  max-width: 90%;
  max-height: 90%;
  position: absolute;
  border-radius: ${(props) => (props.config.rounded ? "5px" : "0px")};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: ${(props) => props.config.background_position};
  overflow: auto;
  z-index: 100;
`;

const FromWrap = styled.div`
  max-width: 100%;
  max-height: 100%;

  overflow: auto;
  background: #fff;
  padding: 16px;
  .ant-col-24.ant-form-item-label {
    padding: 2px;
  }
  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #fff;
    border-radius: 20px;
  }

  ::-webkit-scrollbar-thumb {
    background: #d8d6d6;
    border-radius: 20px;
  }
  .ant-form-item {
    margin-bottom: 12px !important;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-input-number:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-picker-focused,
  .ant-picker:hover,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle,
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after,
  .ant-radio-input:focus + .ant-radio-inner {
    border-color: #00ab55 !important;
    box-shadow: none !important;
  }
  .ant-slider-track,
  .ant-slider:hover .ant-slider-track,
  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const IntroWrap = styled.div``;
const Name = styled.div`
  font-size: 18px;
  font-weight: 500;
  text-align: center;
  margin-bottom: 8px;
`;

const Description = styled.div`
  text-align: center;
  margin-bottom: 8px;
  font-size: 16px;
`;

const ButtonWrap = styled.div`
  width: 100%;
`;
const CustomButton = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 5px;
  font-size: 16px;
  cursor: pointer;
  background: #fff;
  :hover {
    background: #079e52 !important;
    color: #fff;
    border: #079e52;
  }

  :nth-child(1) {
    background: #00ab55;
    color: #fff;
    border: #00ab55;
    margin-bottom: 12px;
  }
`;

const Result = styled.div`
  margin-bottom: 10px;
`;
