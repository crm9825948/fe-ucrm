import React, { useState, useEffect } from "react";
// import axios from "axios";
import styled from "styled-components/macro";
import FormView from "./FormView";
// import { BASE_URL_API } from "constants/constants";

const FormEmbed = () => {
  const [dataTranfer, setDataTransfer] = useState({});
  // const [publicKey, setPublicKey] = useState("");
  const [active, setActive] = useState([]);
  useEffect(() => {
    if (active.length > 0) {
      document.body.style.backgroundColor = "transparent";
    }
  }, [active]);

  const handleDataTransfer = (e) => {
    if (e.data.messageType === "send_app_info_crm") {
      setDataTransfer(e.data.value);
    }
  };
  window.addEventListener("message", handleDataTransfer, false);

  // useEffect(() => {
  //   axios
  //     .get(`${BASE_URL_API}internet-form/config/get-public-key`)
  //     .then((res) => {
  //       setPublicKey(res.data.data);
  //     })
  //     .catch((err) => {
  //       console.log("err", err);
  //     });
  // }, []);

  useEffect(() => {
    const changeState = (e) => {
      if (e.data.messageType === "send_active_modal") {
        console.log("receive", e.data._id);
        if (!active.includes(e.data._id)) {
          setActive((prev) => {
            let tem = prev.filter(
              (item, index) => prev.indexOf(item) === index
            );
            tem.push(e.data._id);
            return tem;
          });
        }
      }
    };

    window.addEventListener("message", changeState, false);

    return () => {
      window.removeEventListener("message", () => setActive([]), false);
    };
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    if (active.length === 0) {
      window.parent.postMessage(
        {
          messageType: "close_iframe",
        },
        dataTranfer.outer_domain
      );
    }
  }, [active, dataTranfer]);

  const renderListModal = (data) => {
    return data.listForm.map((config, index) => (
      <FormView
        key={index}
        // publicKey={publicKey}
        dataMess={{
          ...config,
          device: data.device,
          outer_domain: data.outer_domain,
        }}
        active={active}
        setActive={setActive}
        device={data.device}
      />
    ));
  };
  return (
    <Wrap>
      {Object.entries(dataTranfer).length > 0 && renderListModal(dataTranfer)}
    </Wrap>
  );
};

export default FormEmbed;
const Wrap = styled.div`
  /* width: 100%;
  height: 100%; */
  position: relative;
  .top_left {
    top: 20px;
    left: 20px;
  }
  .top_center {
    top: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .top_right {
    top: 20px;
    right: 20px;
  }
  .center_left {
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .center_center {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center_right {
    right: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .bottom_left {
    left: 20px;
    bottom: 20px;
  }
  .bottom_center {
    bottom: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .bottom_right {
    right: 20px;
    bottom: 20px;
  }
`;
