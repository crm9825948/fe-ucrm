import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import Breadcrumb from "antd/lib/breadcrumb";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { Select, Typography, Switch, Tooltip, Table, Pagination } from "antd";
import EmptyIcon from "assets/images/sharing/EmptyObject.webp";
import {
  deleteFormSetting,
  getListFieldConfigSuccess,
  getListFormSetting,
  toggleFormSetting,
  unMountFormSetting,
} from "redux/slices/formSetting";
import editImg from "assets/icons/common/edit.svg";
import deleteImg from "assets/icons/common/delete.svg";
import shareImg from "assets/icons/common/share.png";
import { setShowModalConfirmDelete } from "redux/slices/global";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { FE_URL } from "constants/constants";
import _ from "lodash";
import ModalShareForm from "./ModalShare";

const { Text: TextComponent } = Typography;

const FormSetting = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listFormSetting, listFieldConfig, totalRecord } = useSelector(
    (state) => state.formSettingReducer
  );
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const [objects, setOjects] = useState([]);
  const [objectID, setObjectID] = useState("");
  const [configId, setConfigId] = useState("");
  const [idShare, setIdShare] = useState("");

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [listOrdered, setListOrdered] = useState(["Default"]);
  const [page, setPage] = useState(0);

  const dispatch = useDispatch();

  useEffect(() => {
    return () => dispatch(unMountFormSetting());
  }, [dispatch]);

  useEffect(() => {
    if (objectID) {
      dispatch(
        getListFormSetting({
          id: objectID,
          data: {
            search_data: {},
            page: page,
            limit: 10,
          },
        })
      );
    }
  }, [page, objectID, dispatch]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setOjects(tempObjects);
    }
  }, [category]);

  const handleNameObject = (id) => {
    let name = "";
    objects.forEach((object) => {
      if (object.value === id) {
        name = object.label;
      }
    });
    return name;
  };

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "internet_form" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };
  //eslint-disable-next-line
  const renderTable = (list) => {
    return (
      <>
        <table>
          <thead className="table-header">
            <tr>
              <th>{t("form.object")}</th>
              <th>{t("form.name")}</th>
              <th>{t("form.websiteInteraction")}</th>
              <th>{t("form.linkInteraction")}</th>
              <th>{t("form.status")}</th>
              <th>{t("form.createdDate")}</th>
              <th>{t("form.modifyTime")}</th>
              <th>{t("knowledgeBase.action")}</th>
            </tr>
          </thead>

          <tbody className="table-body">
            {list.map(
              (setting, index) => (
                // (!objectID || objectID === setting.object_id) && (
                <tr key={index}>
                  <td>
                    <TextComponent
                      ellipsis={{
                        tooltip: handleNameObject(setting.object_id),
                      }}
                    >
                      {handleNameObject(setting.object_id)}
                    </TextComponent>
                  </td>
                  <td>
                    <TextComponent ellipsis={{ tooltip: setting.form_name }}>
                      {setting.form_name}
                    </TextComponent>
                  </td>
                  <td>
                    <TextComponent
                      ellipsis={{
                        tooltip: `<div class="internet_form_crm" tenant_id="${userDetail.tenant_id}" config_id="${setting._id}"></div>
                       
                    `,
                      }}
                    >
                      {setting.form_type !== "QM_FORM" &&
                        ` <div class="internet_form_crm" tenant_id="${userDetail.tenant_id}" config_id="${setting._id}"></div>
                       `}
                    </TextComponent>
                  </td>
                  <td>
                    <TextComponent
                      ellipsis={{
                        tooltip: `${FE_URL}/internet-form/${userDetail.tenant_id}/${setting._id}`,
                      }}
                    >
                      {setting.form_type !== "QM_FORM" &&
                        `${FE_URL}/internet-form/${userDetail.tenant_id}/${setting._id}`}
                    </TextComponent>
                  </td>
                  <td>
                    <Switch
                      disabled={!checkRule("edit")}
                      checkedChildren={t("knowledgeBase.on")}
                      unCheckedChildren={t("knowledgeBase.off")}
                      checked={setting.status}
                      onChange={(checked) => {
                        dispatch(
                          toggleFormSetting({
                            _id: setting._id,
                            status: checked,
                            object_id: objectID,
                          })
                        );
                      }}
                    />
                  </td>
                  <td>{setting.created_date}</td>
                  <td>{setting.modified_date}</td>
                  <td>
                    {checkRule("edit") && (
                      <>
                        {setting.form_type !== "QM_FORM" && (
                          <Tooltip title={t("form.share")}>
                            <img
                              src={shareImg}
                              alt="share"
                              onClick={() => {
                                setIsModalVisible(true);
                                setIdShare(setting._id);
                                if (setting.sections?.length > 0) {
                                  setListOrdered(setting.sections);
                                }

                                dispatch(
                                  getListFieldConfigSuccess(setting.list_field)
                                );
                              }}
                            />
                          </Tooltip>
                        )}
                        <Tooltip title={t("common.edit")}>
                          <img
                            src={editImg}
                            alt="Edit"
                            onClick={() => {
                              navigate(
                                `/form-setting/${setting.object_id}/${setting._id}/detail`
                              );
                            }}
                          />
                        </Tooltip>
                      </>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          src={deleteImg}
                          alt="Delete"
                          onClick={() => {
                            setConfigId(setting._id);
                            dispatch(setShowModalConfirmDelete(true));
                          }}
                        />
                      </Tooltip>
                    )}
                  </td>
                </tr>
              )
              // )
            )}
          </tbody>
        </table>
      </>
    );
  };

  const columns = [
    {
      title: "Object",
      dataIndex: "object_id",
      index: "object_id",
      ellipsis: true,
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: handleNameObject(setting.object_id),
          }}
        >
          {handleNameObject(setting.object_id)}
        </TextComponent>
      ),
    },
    {
      title: "Name",
      dataIndex: "form_name",
      index: "form_name",
      ellipsis: true,
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: setting.form_name,
          }}
        >
          {setting.form_name}
        </TextComponent>
      ),
    },
    {
      title: "Website interaction",
      dataIndex: "website_interaction",
      index: "website_interaction",
      ellipsis: true,
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: `<div class="internet_form_crm" tenant_id="${userDetail.tenant_id}" config_id="${setting._id}"></div>
         
      `,
          }}
        >
          {setting.form_type !== "QM_FORM" &&
            ` <div class="internet_form_crm" tenant_id="${userDetail.tenant_id}" config_id="${setting._id}"></div>
         `}
        </TextComponent>
      ),
    },
    {
      title: "Link interaction",
      dataIndex: "link_interaction",
      index: "link_interaction",
      ellipsis: true,
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: `${FE_URL}/internet-form/${userDetail.tenant_id}/${setting._id}`,
          }}
        >
          {setting.form_type !== "QM_FORM" &&
            `${FE_URL}/internet-form/${userDetail.tenant_id}/${setting._id}`}
        </TextComponent>
      ),
    },
    {
      title: "Status",
      dataIndex: "object_id",
      index: "object_id",
      width: 100,
      render: (value, setting) => (
        <Switch
          disabled={!checkRule("edit")}
          checkedChildren={t("knowledgeBase.on")}
          unCheckedChildren={t("knowledgeBase.off")}
          checked={setting.status}
          onChange={(checked) => {
            dispatch(
              toggleFormSetting({
                data: {
                  _id: setting._id,
                  status: checked,
                },
                dataReload: {
                  id: objectID,
                  data: {
                    search_data: {},
                    page: page,
                    limit: 10,
                  },
                },
              })
            );
          }}
        />
      ),
    },
    {
      title: "Created date",
      dataIndex: "created_date",
      index: "created_date",
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: setting.created_date,
          }}
        >
          {setting.created_date}
        </TextComponent>
      ),
    },
    {
      title: "Modifytime",
      dataIndex: "modified_date",
      index: "modified_date",
      render: (value, setting) => (
        <TextComponent
          ellipsis={{
            tooltip: setting.modified_date,
          }}
        >
          {setting.modified_date}
        </TextComponent>
      ),
    },
    {
      title: "Action",
      dataIndex: "action",
      index: "action",
      width: 120,
      render: (value, setting) => (
        <>
          {checkRule("edit") && (
            <>
              {setting.form_type !== "QM_FORM" && (
                <Tooltip title={t("form.share")}>
                  <img
                    src={shareImg}
                    alt="share"
                    onClick={() => {
                      setIsModalVisible(true);
                      setIdShare(setting._id);
                      if (setting.sections?.length > 0) {
                        setListOrdered(setting.sections);
                      }

                      dispatch(getListFieldConfigSuccess(setting.list_field));
                    }}
                  />
                </Tooltip>
              )}
              <Tooltip title={t("common.edit")}>
                <img
                  src={editImg}
                  alt="Edit"
                  onClick={() => {
                    navigate(
                      `/form-setting/${setting.object_id}/${setting._id}/detail`
                    );
                  }}
                />
              </Tooltip>
            </>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                src={deleteImg}
                alt="Delete"
                onClick={() => {
                  setConfigId(setting._id);
                  dispatch(setShowModalConfirmDelete(true));
                }}
              />
            </Tooltip>
          )}
        </>
      ),
    },
  ];

  return (
    <Wrap>
      <WrapBreadcrumb>
        <Breadcrumb>
          <BreadcrumNavigate onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </BreadcrumNavigate>
          <BreadcrumbItem>{t("form.formSetting")}</BreadcrumbItem>
        </Breadcrumb>
        {objectID && checkRule("create") && (
          <AddButton
            onClick={() => navigate(`/form-setting/${objectID}/detail`)}
          >
            + {t("form.add")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      <Content>
        <ChooseObject>
          <CustomText>{t("form.chooseObject")}</CustomText>
          <CustomSelect
            size="large"
            showSearch
            options={objects}
            placeholder={t("common.placeholderSelect")}
            optionFilterProp="label"
            onChange={(value) => {
              setObjectID(value);
              setPage(0);
            }}
          />
        </ChooseObject>
      </Content>
      {listFormSetting.length > 0 ? (
        // <TableWrap>{renderTable(listFormSetting)}</TableWrap>
        <TableWrap>
          <Table
            columns={columns}
            dataSource={listFormSetting}
            pagination={false}
          ></Table>
          <CustomPagination
            showSizeChanger={false}
            current={page + 1}
            total={totalRecord}
            showTotal={(total, range) =>
              `${range[0]}-${range[1]} of ${total} records`
            }
            onChange={(e, pageSize) => {
              setPage(e - 1);
            }}
          />
        </TableWrap>
      ) : (
        <NoData>
          <img src={EmptyIcon} alt="No data" />

          {!objectID ? (
            <p>
              {t("form.pleaseChoose")} <span>{t("form.object")}</span>
            </p>
          ) : (
            <p>
              No <span>data</span>
            </p>
          )}
        </NoData>
      )}
      <ModalConfimDelete
        title={t("knowledgeBase.titleDelete")}
        decs={t("knowledgeBase.desDelete")}
        methodDelete={deleteFormSetting}
        dataDelete={{
          data: { config_id: configId, object_id: objectID },
          dataReload: {
            id: objectID,
            data: {
              search_data: {},
              page: page,
              limit: 10,
            },
          },
        }}
        isLoading={showLoadingScreen}
      />
      <ModalShareForm
        listFieldConfig={listFieldConfig}
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        idShare={idShare}
        setIdShare={setIdShare}
        tenantId={userDetail.tenant_id}
        listOrdered={listOrdered}
        setListOrdered={setListOrdered}
      />
    </Wrap>
  );
};

export default FormSetting;

const Wrap = styled.div`
  padding: 24px;
  color: #2c2c2c;
  font-size: 16px;
  line-height: 22px;
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  cursor: default;
`;

const BreadcrumNavigate = styled(Breadcrumb.Item)`
  cursor: pointer;
`;

const AddButton = styled.div`
  color: #fff;
  font-family: var(--roboto-500);
  background: ${(props) => props.theme.main};
  padding: 8px 16px;
  cursor: pointer;
  border-radius: 2px;
  border: 1px solid ${(props) => props.theme.darker};
  transition: all 0.5s;
  :hover {
    background: ${(props) => props.theme.darker};
  }
`;
const Content = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
const ChooseObject = styled.div``;

const CustomText = styled.div`
  font-family: var(--roboto-500);
  margin-bottom: 8px;
`;

const CustomSelect = styled(Select)`
  width: 400px;
`;

const NoData = styled.div`
  width: 100%;
  justify-content: center;
  align-items: center;
  display: flex;
  margin-top: 24px;
  flex-direction: column;
  p {
    margin-top: 8px;

    span {
      color: ${(props) => props.theme.main};
    }
  }
`;

const TableWrap = styled.div`
  width: 100%;
  background: #fff;
  margin-top: 20px;
  border-radius: 5px;
  padding: 16px;
  .ant-typography {
    color: #2c2c2c;
    font-size: 16px;
  }
  td {
    padding: 8px 16px !important;
  }
  table {
    width: 100%;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;

    .ant-switch {
      width: 54px;
    }
    .ant-switch-checked {
      background-color: ${(props) => props.theme.main};
    }

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      font-size: 16px;
      padding: 10px 16px;
      text-align: left;
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;
      background: #f0f0f0;
      &:first-child {
        border-left: none;
        box-shadow: inset -1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:nth-child(2) {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }
      &:last-child {
        border-right: none;
        resize: none;
        width: 122px;
        max-width: 122px;
      }
    }

    .table-body td {
      border-bottom: 1px solid #ddd;
      padding: 6px 16px;
      max-width: 200px;
      &:last-child {
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
        img {
          width: 18px;
          margin-right: 8px;
          &:hover {
            cursor: pointer;
          }
        }
      }
    }

    tr {
      :hover {
        background: #f5f5f5;
      }
    }

    td {
      img {
        width: 18px;
        margin-right: 8px;
        &:hover {
          cursor: pointer;
        }
      }
    }
  }
  position: relative;
`;

const CustomPagination = styled(Pagination)`
  margin-top: 16px;
  text-align: end;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover,
  .ant-pagination-prev .ant-pagination-item-link:hover,
  .ant-pagination-next .ant-pagination-item-link:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;
