import React, { useState } from "react";
import { Form, Steps } from "antd";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import DrawerComponent from "./Drawer";
import DesignData from "./DesignData";
import DesignUI from "./DesignUI";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import Breadcrumb from "antd/lib/breadcrumb";
import { useEffect } from "react";
import {
  deleteFieldMapping,
  updateColumns,
  updateFieldMapping,
  updateListOrdered,
} from "redux/slices/formSetting";
import { loadAllUser } from "redux/slices/user";
import reorder, { reorderQuoteMap } from "./FormBoard/FormReorder";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";

const { Step } = Steps;

const week = [
  {
    label: "common.monday",
    value: 1,
    listTime: [],
  },
  {
    label: "common.tuesday",
    value: 2,
    listTime: [],
  },
  {
    label: "common.wednesday",
    value: 3,
    listTime: [],
  },
  {
    label: "common.thurday",
    value: 4,
    listTime: [],
  },
  {
    label: "common.friday",
    value: 5,
    listTime: [],
  },
  {
    label: "common.saturday",
    value: 6,
    listTime: [],
  },
  {
    label: "common.sunday",
    value: 0,
    listTime: [],
  },
];

const DesignForm = ({ fields, objectId, listObjectRelated, allField }) => {
  const { t } = useTranslation();
  const [formUI] = Form.useForm();
  const [formData] = Form.useForm();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    listFieldMapping,
    infoField,
    dataSetting,
    fieldsObjectRelated,
    listOrdered,
    columns,
    dataDelete,
  } = useSelector((state) => state.formSettingReducer);

  const [step, setStep] = useState(0);
  const [background, setBackground] = useState("");
  const [positionImage, setPositionImage] = useState("0% 0%");
  const [position, setPosition] = useState("center_center");
  const [fontColor, setFontColor] = useState("#2C2C2C");

  const [time, setTime] = useState(week);
  const [frequency, setFrequency] = useState("only_session");
  const [all, setAll] = useState(false);
  const [apply, setApply] = useState([]);
  const [listWebsite, setListWebsite] = useState([]);
  const [optionsGrading, setOptionsGrading] = useState([]);
  const [listMappingGrading, setListMappingGrading] = useState([
    {
      grading_value: undefined,
      min_val: undefined,
      max_max: undefined,
    },
  ]);
  useEffect(() => {
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (Object.entries(dataSetting).length > 0 && fields.length > 0) {
      fields.forEach((field) => {
        if (field.ID === dataSetting.field_grading) {
          if (field.option) {
            setOptionsGrading(field.option);
          } else {
            setOptionsGrading([]);
          }
        }
      });
    }
  }, [dataSetting, fields]);
  useEffect(() => {
    if (Object.entries(dataSetting).length > 0) {
      let {
        background_url,
        background_position,
        position_display,
        font_color,
        list_field,
        form_description,
        form_name,
        form_save,
        form_cancel,
        desktop_top,
        desktop_bottom,
        desktop_left,
        desktop_right,
        desktop_width,
        desktop_height,
        mobile_top,
        mobile_bottom,
        mobile_left,
        mobile_right,
        mobile_width,
        dark_screen,
        mobile_height,
        rounded,
        font_style,
        font_mobile,
        font_desktop,
        time_display,
        display_frequency,
        is_apply_all,
        apply_for,
        list_website,
        is_access,
        is_not_interact,
        is_scroll,
        is_click,
        time_access,
        time_not_interact,
        scroll_option,
        id_click,
        field_return,
        owner_id,
        scoring_option,
        min_point,
        max_point,
        field_total,
        check_grading,
        field_grading,
        default_grading_value,
        grading_mapping,
        form_type,
        sections,
        priority,
        // field_ip,
      } = dataSetting;
      //config UI
      setBackground(background_url);
      setPositionImage(background_position);
      setPosition(position_display);
      setFontColor(font_color);
      let temFields = [];
      list_field.forEach((item) => {
        let temSection = "Default";
        if (item.section) {
          temSection = item.section;
        } else {
          if (sections && sections.length > 0) {
            temSection = sections[0];
          }
        }
        temFields.push({
          ...item,
          section: temSection,
          pagination_option: item.pagination_option
            ? item.pagination_option
            : [],
        });
      });
      dispatch(updateFieldMapping(temFields));
      formUI.setFieldsValue({
        form_description: form_description,
        form_name: form_name,
        form_save: form_save,
        form_cancel: form_cancel,
        desktop_top: desktop_top,
        desktop_bottom: desktop_bottom,
        desktop_left: desktop_left,
        desktop_right: desktop_right,
        desktop_width: desktop_width,
        desktop_height: desktop_height,
        mobile_top: mobile_top,
        mobile_bottom: mobile_bottom,
        mobile_left: mobile_left,
        mobile_right: mobile_right,
        mobile_width: mobile_width,
        mobile_height: mobile_height,
        display: "center",
        dark_screen: dark_screen,
        rounded: rounded,
        font_style: font_style,
        font_mobile: font_mobile,
        font_desktop: font_desktop,
        field_return: field_return,
        owner_id: owner_id,
        scoring_option: scoring_option ? scoring_option : "no_scoring",
        min_point: min_point,
        max_point: max_point,
        field_total: field_total,
        check_grading: check_grading,
        field_grading: field_grading,
        default_grading_value: default_grading_value,
        form_type: form_type ? form_type : "Internet Form",
        priority: priority,
        // field_ip: field_ip,
      });
      //config data
      setTime(time_display);
      setFrequency(display_frequency);
      setAll(is_apply_all);
      setApply(apply_for);
      setListWebsite(list_website);
      if (grading_mapping) {
        setListMappingGrading(grading_mapping);
      }

      formData.setFieldsValue({
        is_access: is_access,
        is_not_interact: is_not_interact,
        is_scroll: is_scroll,
        is_click: is_click,
        scroll_option: scroll_option,
        id_click: id_click,
      });
      const list = [10, 20, 30, 40];

      if (time_access) {
        if (list.includes(time_access)) {
          formData.setFieldsValue({ time_access: time_access });
        } else {
          formData.setFieldsValue({
            time_access: "custom",
            custom_access: time_access,
          });
        }
      }
      if (time_not_interact) {
        if (list.includes(time_not_interact)) {
          formData.setFieldsValue({ time_not_interact: time_not_interact });
        } else {
          formData.setFieldsValue({
            time_not_interact: "custom",
            custom_interact: time_not_interact,
          });
        }
      }

      dispatch(
        updateColumns(
          handleColumns(
            sections?.length > 0 ? sections : ["Default"],
            temFields
          )
        )
      );
      dispatch(
        updateListOrdered(sections?.length > 0 ? sections : ["Default"])
      );
    }

    //eslint-disable-next-line
  }, [dataSetting]);
  const renderDescription = (step, position) => {
    let description = "";
    if (step === position) {
      description = t("common.inprogress");
    } else if (step > position) {
      description = t("common.finished");
    } else {
      description = t("common.waiting");
    }
    return description;
  };
  const handleColumns = (ordereds, listField) => {
    let tem = {};
    ordereds.forEach((item) => {
      let list = [];
      listField.forEach((field) => {
        if (field.section === item) {
          list.push(field);
        }
      });
      tem[item] = list;
    });
    return tem;
  };
  const onDragEnd = (result, listField, change) => {
    const valueColumns = handleColumns(listOrdered, listField);
    if (result.combine) {
      if (result.type === "COLUMN") {
        const shallow = [...listOrdered];
        shallow.splice(result.source.index, 1);
        dispatch(updateListOrdered(shallow));
        return;
      }
      const column = valueColumns[result.source.droppableId];
      const withQuoteRemoved = [...column];
      withQuoteRemoved.splice(result.source.index, 1);

      const orderedColumns = {
        ...valueColumns,
        [result.source.droppableId]: withQuoteRemoved,
      };
      dispatch(updateColumns(orderedColumns));
      return;
    }
    if (!result.destination) {
      return;
    }
    const source = result.source;
    const destination = result.destination;
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index &&
      !change
    ) {
      return;
    }
    if (result.type === "COLUMN") {
      const reorderedorder = reorder(
        listOrdered,
        source.index,
        destination.index
      );
      dispatch(updateListOrdered(reorderedorder));
      return;
    }
    const data = reorderQuoteMap({
      quoteMap: valueColumns,
      source,
      destination,
    });
    let tem = [];
    Object.entries(data.quoteMap).forEach((item) => {
      let newItem = [];
      item[1].forEach((value) => {
        newItem.push({
          ...value,
          section: item[0],
        });
      });
      tem = tem.concat(newItem);
    });
    const newList = [];
    tem.forEach((item, index) => {
      newList.push({
        ...item,
        position: index,
      });
    });
    dispatch(updateColumns(handleColumns(listOrdered, newList)));
    dispatch(updateFieldMapping(newList));
  };
  return (
    <Wrap>
      <StepWrap step={step}>
        <WrapBreadcrumb>
          <Breadcrumb>
            <BreadcrumNavigate onClick={() => navigate("/settings")}>
              {t("settings.settings")}
            </BreadcrumNavigate>
            <BreadcrumNavigate onClick={() => navigate("/form-setting")}>
              {t("form.formSetting")}
            </BreadcrumNavigate>
            <BreadcrumbItem>{t("form.detail")}</BreadcrumbItem>
          </Breadcrumb>
        </WrapBreadcrumb>
        <Content>
          <Steps current={step}>
            <Step
              title={t("form.design")}
              description={renderDescription(step, 0)}
            />

            <Step
              title={t("form.setting")}
              description={renderDescription(step, 1)}
            />
          </Steps>
        </Content>
      </StepWrap>
      <WrapContent>
        <DesignUI
          form={formUI}
          fields={fields}
          objectId={objectId}
          listFieldMapping={listFieldMapping}
          step={step}
          setStep={setStep}
          background={background}
          setBackground={setBackground}
          positionImage={positionImage}
          setPositionImage={setPositionImage}
          position={position}
          setPosition={setPosition}
          fontColor={fontColor}
          setFontColor={setFontColor}
          allField={allField}
          optionsGrading={optionsGrading}
          setOptionsGrading={setOptionsGrading}
          listMappingGrading={listMappingGrading}
          setListMappingGrading={setListMappingGrading}
          listOrdered={listOrdered}
          columns={columns}
          onDragEnd={onDragEnd}
        />
        <DesignData
          form={formData}
          step={step}
          setStep={setStep}
          dataUI={formUI.getFieldsValue()}
          listFieldMapping={listFieldMapping}
          background={background}
          positionImage={positionImage}
          position={position}
          fontColor={fontColor}
          time={time}
          setTime={setTime}
          frequency={frequency}
          setFrequency={setFrequency}
          all={all}
          setAll={setAll}
          apply={apply}
          setApply={setApply}
          listWebsite={listWebsite}
          setListWebsite={setListWebsite}
          listMappingGrading={listMappingGrading}
          listOrdered={listOrdered}
        />
      </WrapContent>

      <DrawerComponent
        formUI={formUI}
        listFieldObject={fields}
        listFieldMapping={listFieldMapping}
        infoField={infoField}
        objectId={objectId}
        listObjectRelated={listObjectRelated}
        fieldsObjectRelated={fieldsObjectRelated}
        listOrdered={listOrdered}
        onDragEnd={onDragEnd}
        columns={columns}
      />
      <ModalConfimDelete
        title={t("form.thisField")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteFieldMapping}
        dataDelete={dataDelete}
      />
    </Wrap>
  );
};

export default DesignForm;

const Wrap = styled.div`
  background: #fff;
  padding: 16px;
  .ant-radio-button-wrapper {
    border: none !important;
    padding: 0 6px;
  }
  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper:not(:first-child)::before {
    display: none;
  }

  .ant-radio-button-wrapper-disabled {
    background-color: transparent !important;
  }
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-input:focus + .ant-radio-inner,
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):focus-within {
    box-shadow: none;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapContent = styled.div`
  width: 100%;
  display: flex;
  color: #2c2c2c;
  font-size: 16px;
  line-height: 22px;
  label {
    font-size: 16px;
    line-height: 22px;
    font-family: var(--roboto-500);
    height: unset;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
`;

const StepWrap = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 16px;
  .ant-steps {
    width: 400px;
    margin-right: ${(props) => (props.step === 1 ? "200px" : "0px")};
  }
  .ant-steps-item-active {
    .ant-steps-item-icon {
      background: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }

    .ant-steps-item-title {
      line-height: 22px;
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-item-wait
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    line-height: 22px;
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }
  .ant-steps-item-finish {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main};
    }
    svg {
      color: ${(props) => props.theme.main};
    }
    .ant-steps-item-title {
      line-height: 22px;
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
    .ant-steps-item-title::after {
      line-height: 22px;
      background-color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-icon {
    top: -1.5px;
  }
  .ant-steps-item-description {
    font-size: 16px;
  }
  .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description,
  .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description {
    color: #2c2c2c;
  }
`;
const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 40px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  cursor: default;
`;

const BreadcrumNavigate = styled(Breadcrumb.Item)`
  cursor: pointer;
`;

const Content = styled.div`
  display: flex;
  justify-content: center;
  flex: 1;
`;
