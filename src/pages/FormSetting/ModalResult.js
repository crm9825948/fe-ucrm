import Modal from "antd/lib/modal";
import { useDispatch } from "react-redux";
import { updateDataReturn } from "redux/slices/formSetting";
import styled from "styled-components";

const ModalResult = ({
  image,
  description,
  title,
  contentButton,
  visible,
  setVisible,
  handleCloseModal,
}) => {
  const dispatch = useDispatch();
  return (
    <CustomModal
      footer={false}
      open={visible}
      closable={false}
      onCancel={() => {
        setVisible(false);
        dispatch(updateDataReturn({}));
        handleCloseModal();
      }}
    >
      <ContentWrap>
        <WrapCenter>
          <img src={image} alt="" />
        </WrapCenter>
        <Title>{title}</Title>

        <Description>{description}</Description>
        <WrapCenter
          onClick={() => {
            setVisible(false);
            dispatch(updateDataReturn({}));
            handleCloseModal();
          }}
        >
          {contentButton && <CustomButtom>{contentButton}</CustomButtom>}
        </WrapCenter>
      </ContentWrap>
    </CustomModal>
  );
};

export default ModalResult;
ModalResult.defaultProps = {
  handleCloseModal: () => {},
};
const CustomModal = styled(Modal)`
  width: 295px !important;
  .ant-modal-content {
    padding: 0px;
  }
  @media screen and (min-width: 768px) {
    width: 575px !important;
  }
  @media screen and (min-width: 1280px) {
    width: 847px !important;
  }
`;

const ContentWrap = styled.div`
  width: 100%;
  text-align: center;
  font-size: 16px;
  line-height: 24px;
  padding: 24px;
  @media screen and (min-width: 1280px) {
    padding: 36px 160px;
  }
  img {
    width: 132px;
    @media screen and (min-width: 768px) {
      width: 236px;
    }
  }
`;
const WrapCenter = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;
const Title = styled.div`
  font-weight: 500;

  font-size: 20px;
  line-height: 23px;
  text-align: center;
  color: #2c2c2c;
  margin: 36px 0 24px 0;
  @media screen and (min-width: 768px) {
    font-size: 36px;
    line-height: 42px;
  }
`;
const Description = styled.div`
  color: #6b6b6b;
  font-size: 14px;
  @media screen and (min-width: 768px) {
    font-size: 16px;
  }
`;

const CustomButtom = styled.div`
  cursor: pointer;
  margin-top: 24px;
  background: #498bf3;
  border-radius: 4px;
  padding: 10px;
  font-weight: 500;
  color: #fff;
`;
