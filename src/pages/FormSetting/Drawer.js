import React, {
  useEffect,
  useState,
  useCallback,
  useRef,
  useMemo,
} from "react";
import {
  Checkbox,
  Drawer,
  Form,
  Input,
  Radio,
  Select,
  InputNumber,
  Typography,
  DatePicker,
} from "antd";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import {
  getFieldObjectRelated,
  updateInfoField,
  updateOpenDrawer,
} from "redux/slices/formSetting";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, BE_URL } from "constants/constants";
import Upload from "antd/lib/upload";
import { UploadOutlined } from "@ant-design/icons";
import Editor from "components/Editor/Editor2";
import moment from "moment";
import HiddenSetting from "./HiddenSetting";
import _ from "lodash";

const { TextArea } = Input;
const { Option } = Select;
const { Text: TextComponent } = Typography;
const listTypeAccept = [
  ".jpg",
  ".jpeg",
  ".png",
  ".docx",
  ".pdf",
  ".xlsx",
  ".csv",
];
const DrawerComponent = ({
  formUI,
  listFieldObject,
  listFieldMapping,
  infoField,
  objectId,
  listObjectRelated,
  fieldsObjectRelated,
  listOrdered,
  onDragEnd,
  columns,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { openDrawer } = useSelector((state) => state.formSettingReducer);
  const [listOption, setListOption] = useState([]);
  const [fieldType, setFieldType] = useState("");
  const [withImg, setWithImg] = useState(false);
  const [optionWithImg, setOptionWithImg] = useState([]);
  const [optionWithGrading, setOptionWithGrading] = useState([]);
  const [isLinking, setIsLinking] = useState(false);
  const [fieldsRelated, setFieldRelated] = useState([]);
  const [objectRelated, setObjectRelated] = useState("");
  const [displayField, setDisplayField] = useState("input");
  //eslint-disable-next-line
  const [change, setChange] = useState("");
  const [valueDynamic, setValueDynamic] = useState([]);
  const editorJodit = useRef(null);
  const [optionWithPagination, setOptionWithPagination] = useState([]);
  const [exampleHidden, setExampleHidden] = useState("");
  const [optionLimit, setOptionLimit] = useState({
    min: "value",
    max: "value",
  });

  useEffect(() => {
    let temp = [];
    const typeAccept = ["text", "textarea", "number", "email"];
    if (
      fieldsObjectRelated.length > 0 &&
      fieldsObjectRelated[fieldsObjectRelated.length - 1]["main_object"] &&
      fieldsObjectRelated[fieldsObjectRelated.length - 1]["main_object"][
        "sections"
      ]
    ) {
      fieldsObjectRelated[fieldsObjectRelated.length - 1]["main_object"][
        "sections"
      ].forEach((section) => {
        section.fields.forEach((field) => {
          if (!field.hidden && typeAccept.includes(field.type)) {
            temp.push(field);
          }
        });
      });
    }
    if (temp.length > 0) {
      setFieldRelated(temp);
    }
  }, [fieldsObjectRelated]);

  useEffect(() => {
    if (objectRelated) {
      dispatch(
        getFieldObjectRelated({
          data: {
            object_id: objectRelated,
            api_version: "2",
            show_meta_fields: false,
          },
        })
      );
    }
  }, [objectRelated, dispatch]);

  useEffect(() => {
    if (Object.entries(infoField).length > 0) {
      form.setFieldsValue({
        name: infoField.name,
        id: infoField.id,
        required: infoField.required,
        with_img: infoField.with_img,
        search_related_data: infoField.search_related_data
          ? infoField.search_related_data
          : false,
        source_query_object: infoField.source_query_object
          ? infoField.source_query_object
          : undefined,
        source_query_field: infoField.source_query_field
          ? infoField.source_query_field
          : undefined,
        type_display: infoField.type_display,
        max_number: infoField.max_number,
        is_regex: infoField.is_regex ? infoField.is_regex : false,
        is_conditional_display: infoField.is_conditional_display,
        triggering_field: infoField.triggering_field,
        triggering_answer: infoField.triggering_answer,
        is_phone_number: infoField.is_phone_number,
        number_of_attachments: infoField.number_of_attachments,
        total_attachments_size_limit: infoField.total_attachments_size_limit,
        allow_file_extensions: infoField.allow_file_extensions
          ? infoField.allow_file_extensions
          : [],
        only_number: infoField.only_number,
        is_uppercase: infoField.is_uppercase,
        is_lowercase: infoField.is_lowercase,
        section: infoField.section,
        conditional_pagination: infoField.conditional_pagination,
        default_value:
          infoField.default_value &&
          (infoField.field_type === "date" ||
            infoField.field_type === "datetime-local")
            ? moment(infoField.default_value)
            : infoField.default_value,
        is_saving_ip: infoField.is_saving_ip,
        is_hidden: infoField.is_hidden,
        input_hidden_length: infoField.input_hidden_length,
        input_hidden_left: infoField.input_hidden_left,
        input_hidden_right: infoField.input_hidden_right,
        input_hidden_start: infoField.input_hidden_start,
        input_hidden_end: infoField.input_hidden_end,
        is_search: infoField.is_search,
        input_min_value:
          infoField.input_min_value === "current"
            ? "current"
            : infoField.input_min_value &&
              (infoField.field_type === "date" ||
                infoField.field_type === "datetime-local")
            ? moment(infoField.input_min_value)
            : infoField.input_min_value,
        input_max_value:
          infoField.input_max_value === "current"
            ? "current"
            : infoField.input_max_value &&
              (infoField.field_type === "date" ||
                infoField.field_type === "datetime-local")
            ? moment(infoField.input_max_value)
            : infoField.input_max_value,
      });
      setListOption(infoField.field_options);
      setWithImg(infoField.with_img ? infoField.with_img : false);
      setOptionWithImg(
        infoField.option_with_img ? infoField.option_with_img : []
      );
      setOptionWithGrading(
        infoField.point_options ? infoField.point_options : []
      );
      setOptionWithPagination(infoField.pagination_option);
      setObjectRelated(infoField.source_query_object);
      setIsLinking(infoField.search_related_data);
      setDisplayField(infoField.type_display);
      setFieldType(infoField.field_type);
      handleValueDynamic(infoField.triggering_field, listFieldMapping);
      if (infoField.is_hidden) {
        handleExample(infoField);
      }
      if (infoField.field_type === "number") {
        setOptionLimit({
          min:
            typeof infoField.input_min_value === "string" ? "field" : "value",
          max:
            typeof infoField.input_max_value === "string" ? "field" : "value",
        });
      } else {
        setOptionLimit({
          min: infoField.input_min_value === "current" ? "field" : "value",
          max: infoField.input_max_value === "current" ? "field" : "value",
        });
      }
    }
    //eslint-disable-next-line
  }, [form, infoField]);

  useEffect(() => {
    if (!!!openDrawer) {
      dispatch(updateInfoField({}));
      form.resetFields();
      setWithImg(false);
      setOptionWithImg([]);
      setOptionWithGrading([]);
      setOptionWithPagination([]);
      setListOption([]);
      setIsLinking(false);
      setFieldRelated([]);
      setObjectRelated("");
      setDisplayField("input");
      setFieldType("");
      setValueDynamic([]);
      setExampleHidden("");
      setOptionLimit({
        min: "value",
        max: "value",
      });
    }
  }, [openDrawer, dispatch, form]);

  const handleFieldType = (fields, id) => {
    let type = "";
    fields.forEach((item) => {
      if (item.ID === id) {
        type = item.type;
      }
    });
    return type;
  };

  const handleStep = (fields, id) => {
    let step = 1;
    fields.forEach((item) => {
      if (item.ID === id) {
        if (item.step) {
          step = item.step;
        }
      }
    });
    return step;
  };

  const handleMaxLength = (fields, id) => {
    let max_length = 10000;
    fields.forEach((item) => {
      if (item.ID === id) {
        if (item.max_length) {
          max_length = item.max_length;
        }
      }
    });
    return max_length;
  };

  const handleFieldOption = (fields, id) => {
    let option = [];
    fields.forEach((item) => {
      if (item.ID === id) {
        if (item.option) {
          item.option.forEach((value) => {
            option.push(value.value);
          });
        }
        if (item.options_number) {
          item.options_number.forEach((value) => {
            option.push(value);
          });
        }
      }
    });
    return option;
  };

  const handleDisabled = (id) => {
    let flag = false;
    listFieldMapping.forEach((field) => {
      if (field.id === id) {
        flag = true;
      }
    });
    return flag;
  };

  const handleRequired = (fields, id) => {
    let flag = false;
    fields.forEach((item) => {
      if (item.ID === id) {
        flag = item.required;
      }
    });
    if (flag) {
      form.setFieldsValue({ required: true });
    }
    return flag;
  };

  const renderListOption = (list) => {
    return list.map((item, index) => {
      return (
        <Option
          disabled={
            handleDisabled(item.ID) ||
            formUI.getFieldValue("field_total") === item.ID ||
            formUI.getFieldValue("field_grading") === item.ID
            // ||
            // formUI.getFieldValue("field_ip") === item.ID
          }
          value={item.ID}
          key={index}
        >
          {item.name}
        </Option>
      );
    });
  };

  const handleOptionWithImg = (value, url) => {
    let temp = [...optionWithImg];
    temp.forEach((item, index) => {
      if (item.value === value) {
        temp[index] = {
          ...temp[index],
          url: url,
        };
      }
    });
    setOptionWithImg(temp);
  };

  const renderOptionWithImg = (list) =>
    list.map((item, index) => (
      <WrapOption key={index}>
        <ItemOption>{item.value}</ItemOption>
        <Upload
          accept=".jpg, .png, .gif, .jfif, .jpeg"
          action={BASE_URL_API + "upload-file"}
          method={"post"}
          headers={{
            Authorization: localStorage.getItem("setting_accessToken"),
          }}
          data={{
            obj: objectId,
          }}
          beforeUpload={(file) => {
            let regex = new RegExp('[!@#$%^&*(),?":{}|<>]');
            const isLt5M = file.size / 1024 / 1024 < 5;
            if (!isLt5M) {
              Notification("error", "File must smaller than 5MB!");
              return isLt5M || Upload.LIST_IGNORE;
            }
            if (file.name.match(regex)) {
              Notification("error", "Tên file không được chứa kí tự đặc biệt");
              return Upload.LIST_IGNORE;
            } else if (
              file.type.indexOf("image") === -1 ||
              file.type.indexOf("svg") > -1
            ) {
              Notification("error", "Vui lòng chọn file định dạng hình ảnh");
              return Upload.LIST_IGNORE;
            } else {
              return true;
            }
          }}
          onChange={(info) => {
            const status = info.file.status;
            if (status === "done") {
              handleOptionWithImg(
                item.value,
                `${BE_URL}${info.file.response.data[0]}`
              );
            } else if (status === "error") {
              Notification("error", "Error!");
            }
          }}
          showUploadList={false}
          progress={{
            strokeColor: {
              "0%": "#108ee9",
              "100%": "#87d068",
            },
            strokeWidth: 3,
            format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
          }}
        >
          <UploadImg>
            {item.url ? (
              <img src={item.url} alt="" />
            ) : (
              <span>
                <UploadOutlined />
              </span>
            )}
          </UploadImg>
        </Upload>
      </WrapOption>
    ));

  const checkLenghtSection = (name) => {
    let tem = 0;
    if (columns[name]) {
      tem = columns[name].length;
    }
    return tem;
  };

  const checkName = () => {
    return infoField.section
      ? infoField.section
      : listOrdered[listOrdered.length - 1];
  };

  const checkIndex = () => {
    let tem = columns[checkName()].length;
    columns[checkName()].forEach((item, index) => {
      if (item.id === infoField.id) {
        tem = index;
      }
    });
    return tem;
  };
  const handleDataDate = (value) => {
    let tem = value === null ? undefined : value;
    if (tem && typeof tem === "object") {
      tem = moment(value).format(
        fieldType === "date" ? "YYYY-MM-DD" : "YYYY-MM-DD HH:mm:ss"
      );
    }
    return tem;
  };
  const handleFinish = (values) => {
    let list = [...listFieldMapping];
    if (Object.entries(infoField).length > 0) {
      list.forEach((field, index) => {
        if (field.position === infoField.position) {
          list[index] = {
            position: field.position,
            id: values.id,
            name: values.name,
            required: values.required,
            is_regex: values.is_regex ? values.is_regex : false,
            field_type: handleFieldType(listFieldObject, values.id),
            field_options: handleFieldOption(listFieldObject, values.id),
            step: handleStep(listFieldObject, values.id),
            with_img: values.with_img ? values.with_img : false,
            option_with_img: optionWithImg,
            search_related_data: values.search_related_data,
            source_query_object: values.source_query_object,
            source_query_field: values.source_query_field,
            type_display: values.type_display,
            max_number: values.max_number,
            is_conditional_display: values.is_conditional_display,
            triggering_field: values.triggering_field,
            triggering_answer: values.triggering_answer,
            is_phone_number: values.is_phone_number,
            point_options: optionWithGrading,
            number_of_attachments: values.number_of_attachments,
            total_attachments_size_limit: values.total_attachments_size_limit,
            allow_file_extensions: values.allow_file_extensions,
            is_uploadable: true,
            only_number: values.only_number,
            is_uppercase: values.is_uppercase,
            is_lowercase: values.is_lowercase,
            section: infoField.section,
            conditional_pagination:
              infoField.section === values.section
                ? values.conditional_pagination
                : false,
            pagination_option: optionWithPagination,
            default_value: handleDataDate(values.default_value),
            is_saving_ip: values.is_saving_ip,
            is_hidden: values.is_hidden,
            input_hidden_length: values.input_hidden_length,
            input_hidden_left: values.input_hidden_left
              ? values.input_hidden_left
              : undefined,
            input_hidden_right: values.input_hidden_right
              ? values.input_hidden_right
              : undefined,
            input_hidden_start:
              values.input_hidden_start === 0 || values.input_hidden_start
                ? values.input_hidden_start
                : undefined,
            input_hidden_end:
              values.input_hidden_end === 0 || values.input_hidden_end
                ? values.input_hidden_end
                : undefined,
            input_max_length: handleMaxLength(listFieldObject, values.id),
            is_search: values.is_search,
            input_min_value: handleDataDate(values.input_min_value),
            input_max_value: handleDataDate(values.input_max_value),
          };
        }
      });
    } else {
      list.push({
        position: list.length,
        name: values.name,
        id: values.id,
        required: values.required,
        is_regex: values.is_regex ? values.is_regex : false,
        field_type: handleFieldType(listFieldObject, values.id),
        field_options: handleFieldOption(listFieldObject, values.id),
        step: handleStep(listFieldObject, values.id),
        with_img: values.with_img ? values.with_img : false,
        option_with_img: optionWithImg,
        search_related_data: values.search_related_data,
        source_query_object: values.source_query_object,
        source_query_field: values.source_query_field,
        type_display: values.type_display,
        max_number: values.max_number,
        is_conditional_display: values.is_conditional_display,
        triggering_field: values.triggering_field,
        triggering_answer: values.triggering_answer,
        is_phone_number: values.is_phone_number,
        point_options: optionWithGrading,
        number_of_attachments: values.number_of_attachments,
        total_attachments_size_limit: values.total_attachments_size_limit,
        allow_file_extensions: values.allow_file_extensions,
        is_uploadable: true,
        only_number: values.only_number,
        is_uppercase: values.is_uppercase,
        is_lowercase: values.is_lowercase,
        section: listOrdered[listOrdered.length - 1],
        conditional_pagination: values.conditional_pagination,
        pagination_option: optionWithPagination,
        default_value: handleDataDate(values.default_value),
        is_saving_ip: values.is_saving_ip,
        is_hidden: values.is_hidden,
        input_hidden_length: values.input_hidden_length,
        input_hidden_left: values.input_hidden_left
          ? values.input_hidden_left
          : undefined,
        input_hidden_right: values.input_hidden_right
          ? values.input_hidden_right
          : undefined,
        input_hidden_start:
          values.input_hidden_start === 0 || values.input_hidden_start
            ? values.input_hidden_start
            : undefined,
        input_hidden_end:
          values.input_hidden_end === 0 || values.input_hidden_end
            ? values.input_hidden_end
            : undefined,
        input_max_length: handleMaxLength(listFieldObject, values.id),
        is_search: values.is_search,
        input_min_value: handleDataDate(values.input_min_value),
        input_max_value: handleDataDate(values.input_max_value),
      });
    }
    let flag = false;

    listFieldMapping.forEach((field) => {
      if (field.name === values.name && field.position !== infoField.position) {
        flag = true;
      }
    });
    let flagOptionWithImg = false;
    if (withImg && listOption.length > 0 && displayField === "radio") {
      optionWithImg.forEach((option) => {
        if (!option.url) {
          flagOptionWithImg = true;
        }
      });
    }
    const regex = /^x*$/;
    let flagValue = false;
    if (
      (values.input_min_value || values.input_min_value === 0) &&
      (values.input_max_value || values.input_max_value === 0)
    ) {
      if (fieldType === "date" || fieldType === "datetime-local") {
        if (
          values.input_min_value === "current" &&
          values.input_max_value === "current"
        ) {
          flagValue = true;
        } else {
          flagValue =
            new Date(
              moment(values.input_min_value).startOf("day")
            ).getTime() ===
              new Date(
                moment(values.input_max_value).startOf("day")
              ).getTime() ||
            new Date(moment(values.input_min_value).startOf("day")).getTime() >
              new Date(moment(values.input_max_value).startOf("day")).getTime();
        }
      }
      if (
        fieldType === "number" &&
        typeof values.input_min_value === "number" &&
        typeof values.input_max_value === "number"
      ) {
        flagValue =
          values.input_min_value > values.input_max_value ||
          values.input_min_value === values.input_max_value;
      }

      if (
        fieldType === "number" &&
        typeof values.input_min_value === "string" &&
        typeof values.input_max_value === "string"
      ) {
        flagValue = values.input_min_value === values.input_max_value;
      }
    }

    if (flag) {
      Notification("warning", "Label đã tồn tại");
    } else if (flagOptionWithImg) {
      Notification("warning", "Vui lòng chọn hình ảnh cho option");
    } else if (values.is_hidden && regex.test(exampleHidden)) {
      Notification("warning", "It is not possible to hide all characters!");
    } else if (values.is_hidden && !exampleHidden.includes("x")) {
      Notification("warning", "Please hide at least one character!");
    } else if (flagValue) {
      Notification("warning", "Min value must be less than max value");
    } else {
      // dispatch(updateFieldMapping(list));
      let result = {
        draggableId: values.id,
        type: "QUOTE",
        source: {
          index: checkIndex(),
          droppableId: checkName(),
        },
        reason: "DROP",
        mode: "FLUID",
        destination: {
          droppableId: values.section,
          index:
            Object.entries(infoField).length > 0
              ? checkIndex()
              : checkLenghtSection(values.section),
        },
        combine: null,
      };
      onDragEnd(result, list, true);
      dispatch(updateOpenDrawer(""));
    }
  };

  const handleCancel = () => {
    dispatch(updateOpenDrawer(""));
  };

  const handleChangeFieldMapping = (value) => {
    let tem = handleFieldOption(listFieldObject, value);
    setListOption(tem);
    let option = [];
    let optionPoint = [];
    tem.forEach((item) => {
      option.push({ value: item, url: "" });
      optionPoint.push({ option: item, point: 0 });
    });
    setOptionWithImg(option);
    setOptionWithGrading(optionPoint);
    let temp = handleFieldType(listFieldObject, value);
    setFieldType(temp);
    form.setFieldsValue({
      default_value: undefined,
      is_hidden: false,
      input_hidden_length: 16,
      input_hidden_left: undefined,
      input_hidden_right: undefined,
      input_hidden_start: undefined,
      input_hidden_end: undefined,
      input_min_value: undefined,
      input_max_value: undefined,
    });
    setOptionLimit({
      min: "value",
      max: "value",
    });
    if (tem.length > 0) {
      form.setFieldsValue({ type_display: "select" });
    } else {
      if (temp === "number") {
        form.setFieldsValue({ type_display: "input", max_number: 5 });
      }
    }
  };

  const handleOptionDynamic = (position, listField) => {
    let tem = [];
    listField.forEach((field) => {
      if (field.position < position && field.field_options.length > 0) {
        tem.push({ label: field.name, value: field.id });
      }
    });
    return tem;
  };

  const handleOptionLimitNumber = (position, listField) => {
    let tem = [];
    listField.forEach((field) => {
      if (field.position < position && field.field_type === "number") {
        tem.push({ label: field.name, value: field.id });
      }
    });
    return tem;
  };

  const handleValueDynamic = (field, list) => {
    let tem = [];
    list.forEach((item) => {
      if (item.id === field) {
        tem = item.field_options;
      }
    });
    setValueDynamic(tem);
  };

  const handleOptionGrading = (option, value) => {
    let temp = [...optionWithGrading];
    temp.forEach((item, index) => {
      if (item.option === option) {
        temp[index] = {
          ...temp[index],
          point: value,
        };
      }
    });
    setOptionWithGrading(temp);
  };

  const renderOptionGrading = (list) => {
    return list.map((item, index) => (
      <OptionGrading key={index}>
        <TextComponent ellipsis={{ tooltip: item.option }}>
          {item.option}
        </TextComponent>
        <InputNumber
          value={item.point}
          placeholder={t("common.placeholderInput")}
          onChange={(value) =>
            handleOptionGrading(item.option, value ? value : 0)
          }
        />
      </OptionGrading>
    ));
  };

  const handleFinishDescription = (values) => {
    let temId = Math.random().toString();
    let list = [...listFieldMapping];
    if (Object.entries(infoField).length > 0) {
      list.forEach((field, index) => {
        if (field.position === infoField.position) {
          list[index] = {
            id: field.id,
            position: field.position,
            name: values.name,
            description: editorJodit.current.value
              ? editorJodit.current.value
              : "",
            field_options: [],
            point_options: [],
            option_with_img: [],
            section: infoField.section,
          };
        }
      });
    } else {
      list.push({
        id: temId,
        position: list.length,
        name: values.name,
        description: editorJodit.current.value ? editorJodit.current.value : "",
        field_options: [],
        point_options: [],
        option_with_img: [],
        section: listOrdered[listOrdered.length - 1],
      });
    }

    let result = {
      draggableId: infoField.id ? infoField.id : temId,
      type: "QUOTE",
      source: {
        index: checkIndex(),
        droppableId: checkName(),
      },
      reason: "DROP",
      mode: "FLUID",
      destination: {
        droppableId: values.section,
        index:
          Object.entries(infoField).length > 0
            ? checkIndex()
            : checkLenghtSection(values.section),
      },
      combine: null,
    };
    onDragEnd(result, list, true);
    dispatch(updateOpenDrawer(""));
  };

  const handleAppend = useCallback(() => {
    let tem = [];
    listFieldMapping.forEach((field) => {
      if (field.field_type && field.field_type !== "file") {
        tem.push({
          label:
            field.name.length > 36
              ? `${field.name.slice(0, 36)}...`
              : field.name,
          value: field.id,
        });
      }
    });
    return tem;
  }, [listFieldMapping]);

  const handleChangeOptionPagination = (option, value) => {
    let temp = [...optionWithPagination];
    temp.forEach((item, index) => {
      if (item.value === option) {
        temp[index] = {
          ...temp[index],
          option: value,
        };
      }
    });
    setOptionWithPagination(temp);
  };

  const renderOptionPagination = (list = []) => {
    let temIndex = listOrdered.findIndex(
      (item) => item === form.getFieldValue("section")
    );
    return list.map((item, index) => (
      <OptionGrading key={index}>
        <TextComponent ellipsis={{ tooltip: item.value }}>
          {item.value}
        </TextComponent>
        <Select
          value={item.option}
          placeholder={t("common.placeholderSelect")}
          onChange={(value) => handleChangeOptionPagination(item.value, value)}
        >
          {listOrdered.map(
            (item, index) =>
              index > temIndex && (
                <Option key={index} value={item}>
                  {item}
                </Option>
              )
          )}
          <Option value="submit_form_crm">Submit form</Option>
        </Select>
      </OptionGrading>
    ));
  };

  const checkDisabled = (name) => {
    let flag = false;
    columns[name]?.forEach((item) => {
      if (item.conditional_pagination) {
        flag = true;
      }
    });
    return flag;
  };

  const handleOptionPagination = (value) => {
    let tem = handleFieldOption(listFieldObject, value);
    let index = listOrdered.findIndex(
      (item) => item === form.getFieldValue("section")
    );
    let optionPagination = [];
    tem.forEach((item) => {
      optionPagination.push({ value: item, option: listOrdered[index + 1] });
    });
    setOptionWithPagination(optionPagination);
  };

  const renderDefaultValue = (type, options) => {
    switch (type) {
      case "text":
      case "email":
        return (
          <Input
            placeholder={t("common.placeholderInput")}
            maxLength={handleMaxLength(
              listFieldObject,
              form.getFieldValue("id")
            )}
          />
        );
      case "textarea":
        return (
          <TextArea
            placeholder={t("common.placeholderInput")}
            rows={3}
            maxLength={handleMaxLength(
              listFieldObject,
              form.getFieldValue("id")
            )}
          />
        );
      case "number":
        if (options.length > 0) {
          return (
            <Select
              showSearch
              allowClear
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
              placeholder={t("common.placeholderSelect")}
            >
              {options.map((option, index) => (
                <Option value={option} key={index}>
                  {option}
                </Option>
              ))}
            </Select>
          );
        } else {
          return <InputNumber placeholder={t("common.placeholderInput")} />;
        }
      case "date":
        return <DatePicker />;
      case "datetime-local":
        return <DatePicker showTime />;
      case "select":
        return (
          <Select
            showSearch
            allowClear
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
            placeholder={t("common.placeholderSelect")}
          >
            {options.map((option, index) => (
              <Option value={option} key={index}>
                {option}
              </Option>
            ))}
          </Select>
        );
      default:
        break;
    }
  };

  const checkHidden = (length, index, left, right, start, end) => {
    let flag = false;
    if (
      index <= left ||
      index > length - right ||
      ((start === 0 || start) &&
        (end === 0 || end) &&
        index >= start &&
        index <= end)
    ) {
      flag = true;
    }
    return flag;
  };

  const handleExample = useCallback((values) => {
    const {
      input_hidden_length = 16,
      input_hidden_left = 0,
      input_hidden_right = 0,
      input_hidden_start,
      input_hidden_end,
    } = values;
    let temExample = "";

    if (input_hidden_length) {
      for (let i = 1; i < input_hidden_length + 1; i++) {
        if (
          checkHidden(
            input_hidden_length,
            i,
            input_hidden_left,
            input_hidden_right,
            input_hidden_start,
            input_hidden_end
          )
        ) {
          temExample += "x";
        } else {
          temExample += `${i}`;
        }
      }
    }
    setExampleHidden(temExample);
  }, []);
  const debouncedChangeHandler = useMemo(
    () => _.debounce(handleExample, 500),
    [handleExample]
  );
  return (
    <Drawer
      title={
        Object.entries(infoField).length > 0 && openDrawer === "field"
          ? t("form.editFieldMapping")
          : Object.entries(infoField).length > 0 && openDrawer === "description"
          ? "Edit description"
          : openDrawer === "field"
          ? t("form.addFieldMapping")
          : "Add description"
      }
      placement="right"
      onClose={() => dispatch(updateOpenDrawer(""))}
      visible={!!openDrawer}
      // visible={true}
      width={400}
      footer={
        <ButtonGroup>
          <Button onClick={() => handleCancel()}>{t("common.cancel")}</Button>
          <Button onClick={() => form.submit()}>{t("common.save")}</Button>
        </ButtonGroup>
      }
    >
      <Wrap>
        {openDrawer === "field" ? (
          <Form
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{
              required: false,
              number_of_attachments: 5,
              allow_file_extensions: [
                ".jpg",
                ".jpeg",
                ".png",
                ".docx",
                ".pdf",
                ".xlsx",
              ],
              total_attachments_size_limit: 25,
              input_hidden_length: 16,
            }}
            onFinish={handleFinish}
            onValuesChange={(value, values) => {
              setChange(value);

              debouncedChangeHandler(values);
            }}
          >
            <Form.Item
              label={t("form.label")}
              name="name"
              rules={[
                { required: true, message: t("form.required") },
                () => ({
                  validator: (_, value) => {
                    let temp = false;
                    if (value && value.trim().length === 0) {
                      temp = true;
                    }
                    if (temp) {
                      return Promise.reject(
                        new Error(t("knowledgeBase.required"))
                      );
                    } else {
                      return Promise.resolve();
                    }
                  },
                }),
              ]}
            >
              <Input placeholder={t("form.placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("form.fieldMapping")}
              name="id"
              rules={[{ required: true, message: t("form.required") }]}
            >
              <Select
                allowClear
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                placeholder={t("common.placeholderSelect")}
                onChange={(value) => {
                  handleChangeFieldMapping(value);
                  handleOptionPagination(value);
                }}
              >
                {renderListOption(listFieldObject)}
              </Select>
            </Form.Item>
            {fieldType &&
              fieldType !== "file" &&
              !form.getFieldValue("is_hidden") && (
                <Form.Item label={t("form.defaultValue")} name="default_value">
                  {renderDefaultValue(fieldType, listOption)}
                </Form.Item>
              )}
            <Form.Item
              label={t("form.section")}
              name="section"
              rules={[{ required: true, message: t("form.required") }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                placeholder={t("common.placeholderSelect")}
                onChange={(value) => {
                  form.setFieldsValue({
                    conditional_pagination: false,
                  });
                }}
              >
                {listOrdered.map((item, index) => (
                  <Option value={item} key={index}>
                    {item}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item name="required" valuePropName="checked">
              <Checkbox
                disabled={
                  // form.getFieldValue("is_conditional_display") ||
                  handleRequired(listFieldObject, form.getFieldValue("id"))
                }
              >
                {t("form.isRequired")}
              </Checkbox>
            </Form.Item>
            {fieldType === "text" && (
              <>
                <Form.Item name="is_phone_number" valuePropName="checked">
                  <Checkbox disabled={form.getFieldValue("is_hidden")}>
                    {t("form.isPhoneNumber")}
                  </Checkbox>
                </Form.Item>
                <Form.Item name="only_number" valuePropName="checked">
                  <Checkbox disabled={form.getFieldValue("is_hidden")}>
                    {t("form.onlyNumber")}
                  </Checkbox>
                </Form.Item>
                <Form.Item name="is_uppercase" valuePropName="checked">
                  <Checkbox
                    disabled={
                      form.getFieldValue("is_lowercase") ||
                      form.getFieldValue("is_hidden")
                    }
                  >
                    {t("form.isUppercase")}
                  </Checkbox>
                </Form.Item>
                <Form.Item name="is_lowercase" valuePropName="checked">
                  <Checkbox
                    disabled={
                      form.getFieldValue("is_uppercase") ||
                      form.getFieldValue("is_hidden")
                    }
                  >
                    {t("form.isLowercase")}
                  </Checkbox>
                </Form.Item>
                <Form.Item name="is_saving_ip" valuePropName="checked">
                  <Checkbox>{t("form.requestIP")}</Checkbox>
                </Form.Item>
                <Form.Item name="is_hidden" valuePropName="checked">
                  <Checkbox
                    disabled={
                      form.getFieldValue("default_value") ||
                      form.getFieldValue("is_phone_number") ||
                      form.getFieldValue("only_number") ||
                      form.getFieldValue("is_uppercase") ||
                      form.getFieldValue("is_lowercase")
                    }
                    onChange={() => {
                      form.setFieldsValue({
                        input_hidden_length: 16,
                        input_hidden_left: undefined,
                        input_hidden_right: undefined,
                        input_hidden_start: undefined,
                        input_hidden_end: undefined,
                      });
                      handleExample(form.getFieldsValue());
                    }}
                  >
                    {t("form.isHidden")}
                  </Checkbox>
                </Form.Item>
                {form.getFieldValue("is_hidden") && (
                  <HiddenSetting exampleHidden={exampleHidden} />
                )}
              </>
            )}
            <Form.Item name="search_related_data" valuePropName="checked">
              <Checkbox
                onChange={(e) => {
                  setIsLinking(e.target.checked);
                }}
              >
                Search related data
              </Checkbox>
            </Form.Item>
            <Form.Item name="is_regex" valuePropName="checked">
              <Checkbox>Is regex</Checkbox>
            </Form.Item>
            {isLinking && (
              <WrapLinking>
                <Form.Item
                  name="source_query_object"
                  rules={[{ required: true, message: t("form.required") }]}
                  label="Object source"
                >
                  <Select
                    showSearch
                    optionFilterProp="children"
                    placeholder={t("common.placeholderSelect")}
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                    onChange={(value) => {
                      setObjectRelated(value);
                      form.setFieldsValue({ source_query_field: undefined });
                    }}
                  >
                    {listObjectRelated.map((object, index) => (
                      <Option key={index} value={object._id}>
                        {object.Name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
                <Form.Item
                  label="Field of object source"
                  name="source_query_field"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Select
                    showSearch
                    optionFilterProp="children"
                    placeholder={t("common.placeholderSelect")}
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {fieldsRelated.map((item, index) => (
                      <Option value={item.ID} key={index}>
                        {item.name}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </WrapLinking>
            )}
            {listOption.length > 0 && (
              <>
                <WrapLinking>
                  <Form.Item
                    label="Type display"
                    name="type_display"
                    rules={[{ required: true, message: t("form.required") }]}
                  >
                    <Radio.Group
                      onChange={(value) => setDisplayField(value.target.value)}
                    >
                      <Radio value="select">Select</Radio>
                      <Radio value="radio">Radio</Radio>
                    </Radio.Group>
                  </Form.Item>
                  {displayField === "radio" && (
                    <>
                      <Form.Item name="with_img" valuePropName="checked">
                        <Checkbox
                          onChange={(e) => {
                            setWithImg(e.target.checked);
                          }}
                        >
                          With image
                        </Checkbox>
                      </Form.Item>
                      <OptionImg>
                        {withImg && renderOptionWithImg(optionWithImg)}
                      </OptionImg>
                    </>
                  )}
                </WrapLinking>
                <Form.Item name="is_search" valuePropName="checked">
                  <Checkbox>{t("form.allowSearch")}</Checkbox>
                </Form.Item>
              </>
            )}
            {fieldType === "number" && listOption.length === 0 && (
              <WrapLinking>
                <Form.Item
                  style={{ marginBottom: "0px" }}
                  label="Type display"
                  name="type_display"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Radio.Group
                    onChange={(value) => {
                      setDisplayField(value.target.value);
                      form.setFieldsValue({
                        input_min_value: undefined,
                        input_max_value: undefined,
                        max_number: 5,
                      });
                      setOptionLimit({
                        min: "value",
                        max: "value",
                      });
                    }}
                  >
                    <Radio value="input">Input</Radio>
                    <Radio value="rating">Rating</Radio>
                    <Radio value="slide">Slide</Radio>
                  </Radio.Group>
                </Form.Item>
                {(displayField === "rating" || displayField === "slide") && (
                  <Form.Item
                    label="Max"
                    name="max_number"
                    rules={[{ required: true, message: t("form.required") }]}
                  >
                    <InputNumber
                      min={1}
                      max={1000}
                      placeholder={t("common.placeholderInput")}
                      parser={(value) => parseInt(value)}
                    />
                  </Form.Item>
                )}
                {displayField === "input" && (
                  <>
                    <WrapMinMax>
                      <CustomText>Min</CustomText>
                      <Select
                        style={{ width: "30%" }}
                        value={optionLimit.min}
                        onChange={(value) => {
                          form.setFieldsValue({ input_min_value: undefined });
                          setOptionLimit((prev) => ({
                            ...prev,
                            min: value,
                          }));
                        }}
                      >
                        <Option value="value">Value</Option>
                        <Option value="field">Field</Option>
                      </Select>

                      <Form.Item name="input_min_value">
                        {optionLimit.min === "value" ? (
                          <InputNumber
                            placeholder={t("common.placeholderInput")}
                          />
                        ) : (
                          <Select
                            allowClear
                            showSearch
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                            placeholder={t("common.placeholderSelect")}
                            options={handleOptionLimitNumber(
                              Object.entries(infoField).length > 0
                                ? infoField.position
                                : listFieldMapping.length,
                              listFieldMapping
                            )}
                          ></Select>
                        )}
                      </Form.Item>
                    </WrapMinMax>
                    <WrapMinMax>
                      <CustomText>Max</CustomText>
                      <Select
                        style={{ width: "30%" }}
                        value={optionLimit.max}
                        onChange={(value) => {
                          form.setFieldsValue({ input_max_value: undefined });
                          setOptionLimit((prev) => ({
                            ...prev,
                            max: value,
                          }));
                        }}
                      >
                        <Option value="value">Value</Option>
                        <Option value="field">Field</Option>
                      </Select>

                      <Form.Item name="input_max_value">
                        {optionLimit.max === "value" ? (
                          <InputNumber
                            placeholder={t("common.placeholderInput")}
                          />
                        ) : (
                          <Select
                            allowClear
                            showSearch
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                            placeholder={t("common.placeholderSelect")}
                            options={handleOptionLimitNumber(
                              Object.entries(infoField).length > 0
                                ? infoField.position
                                : listFieldMapping.length,
                              listFieldMapping
                            )}
                          ></Select>
                        )}
                      </Form.Item>
                    </WrapMinMax>
                  </>
                )}
              </WrapLinking>
            )}
            <Form.Item name="is_conditional_display" valuePropName="checked">
              <Checkbox
              // disabled={form.getFieldValue("required")}
              >
                {t("form.isConditional")}
              </Checkbox>
            </Form.Item>
            {form.getFieldValue("is_conditional_display") && (
              <>
                <Form.Item
                  label={t("form.triggerField")}
                  name="triggering_field"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                    placeholder={t("common.placeholderSelect")}
                    onChange={(value) => {
                      handleValueDynamic(value, listFieldMapping);
                      form.setFieldsValue({ triggering_answer: undefined });
                    }}
                    options={handleOptionDynamic(
                      Object.entries(infoField).length > 0
                        ? infoField.position
                        : listFieldMapping.length,
                      listFieldMapping
                    )}
                  ></Select>
                </Form.Item>
                <Form.Item
                  label={t("form.triggerAnswer")}
                  name="triggering_answer"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                    placeholder={t("common.placeholderSelect")}
                  >
                    {valueDynamic.map((item, index) => (
                      <Option key={index} value={item}>
                        {item}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </>
            )}
            {(fieldType === "date" || fieldType === "datetime-local") && (
              <>
                <WrapMinMax>
                  <CustomText>Min</CustomText>
                  <Select
                    style={{ width: "30%" }}
                    value={optionLimit.min}
                    onChange={(value) => {
                      form.setFieldsValue({
                        input_min_value:
                          value === "value" ? undefined : "current",
                      });
                      setOptionLimit((prev) => ({
                        ...prev,
                        min: value,
                      }));
                    }}
                  >
                    <Option value="value">Value</Option>
                    <Option value="field">Current</Option>
                  </Select>

                  <Form.Item name="input_min_value">
                    {optionLimit.min === "value" && (
                      <DatePicker disabled={optionLimit.min === "field"} />
                    )}
                  </Form.Item>
                </WrapMinMax>
                <WrapMinMax>
                  <CustomText>Max</CustomText>
                  <Select
                    style={{ width: "30%" }}
                    value={optionLimit.max}
                    onChange={(value) => {
                      form.setFieldsValue({
                        input_max_value:
                          value === "value" ? undefined : "current",
                      });
                      setOptionLimit((prev) => ({
                        ...prev,
                        max: value,
                      }));
                    }}
                  >
                    <Option value="value">Value</Option>
                    <Option value="field">Current</Option>
                  </Select>

                  <Form.Item name="input_max_value">
                    {optionLimit.max === "value" && (
                      <DatePicker disabled={optionLimit.max === "field"} />
                    )}
                  </Form.Item>
                </WrapMinMax>
              </>
            )}
            {formUI.getFieldValue("scoring_option") !== "no_scoring" &&
              optionWithGrading.length > 0 && (
                <WrapOptionGrading>
                  <OptionGrading>
                    <label>{t("form.choices")}</label>
                    <label>{t("form.point")}</label>
                  </OptionGrading>
                  {renderOptionGrading(optionWithGrading)}
                </WrapOptionGrading>
              )}

            {fieldType === "file" && (
              <>
                <Form.Item
                  label={t("form.numberAttachement")}
                  name="number_of_attachments"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    min={1}
                    max={10}
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                  />
                </Form.Item>
                <Form.Item
                  label={t("form.maxCapacity")}
                  name="total_attachments_size_limit"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    min={1}
                    max={25}
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                  />
                </Form.Item>
                <Form.Item
                  label={t("form.allowFile")}
                  name="allow_file_extensions"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Select allowClear showSearch mode="multiple">
                    {listTypeAccept.map((item, index) => (
                      <Option key={index} value={item}>
                        {item}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </>
            )}

            {listOption.length > 0 &&
              form.getFieldValue("section") !==
                listOrdered[listOrdered.length - 1] && (
                <WrapOptionGrading>
                  <Form.Item
                    name="conditional_pagination"
                    valuePropName="checked"
                  >
                    <Checkbox
                      onChange={(e) => {
                        handleOptionPagination(form.getFieldValue("id"));
                      }}
                      disabled={
                        !infoField.conditional_pagination &&
                        checkDisabled(form.getFieldValue("section"))
                      }
                    >
                      {t("form.conditionPagination")}
                    </Checkbox>
                  </Form.Item>
                  {form.getFieldValue("conditional_pagination") && (
                    <>
                      <OptionGrading>
                        <label>{t("form.option")}</label>
                        <label>{t("form.condition")}</label>
                      </OptionGrading>
                      {renderOptionPagination(optionWithPagination)}
                    </>
                  )}
                </WrapOptionGrading>
              )}
          </Form>
        ) : (
          <Form
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            onFinish={handleFinishDescription}
          >
            <Form.Item
              label={t("form.label")}
              name="name"
              rules={[{ required: true, message: t("form.required") }]}
            >
              <Input placeholder={t("form.placeholder")} />
            </Form.Item>
            <Form.Item
              label={t("form.section")}
              name={"section"}
              rules={[{ required: true, message: t("form.required") }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  if (option.children) {
                    return option.children
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.label) {
                    return option.label
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }}
                placeholder={t("common.placeholderSelect")}
              >
                {listOrdered.map((item, index) => (
                  <Option value={item} key={index}>
                    {item}
                  </Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item label={t("common.description")} name="description">
              <Editor
                content={infoField.description}
                readonly={false}
                objectID={objectId}
                showAppend={true}
                editorJodit={editorJodit}
                minHeight={"340px"}
                optionsAppend={handleAppend()}
              />
            </Form.Item>
          </Form>
        )}
      </Wrap>
    </Drawer>
  );
};

export default DrawerComponent;

const Wrap = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  .ant-form-item-label
    > label.ant-form-item-required:not(
      .ant-form-item-required-mark-optional
    )::before {
    display: none;
  }
  label {
    font-size: 16px;
    line-height: 22px;
    font-family: var(--roboto-500);
    height: unset;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-picker-focused,
  .ant-picker:hover,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-radio:hover .ant-radio-inner,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-input-number:hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-checkbox-disabled + span {
    color: #2c2c2c;
  }
`;

const ButtonGroup = styled.div`
  display: flex;
  justify-content: end;
`;
const Button = styled.div`
  margin-left: 16px;
  width: 114px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
  transition: all 0.5s;
  border: 1px solid ${(props) => props.theme.darker};
  color: #fff;
  background: ${(props) => props.theme.main};
  cursor: pointer;
  :first-child {
    border-color: #d9d9d9;
    background: #fff;
    color: #2c2c2c;
  }
  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
    border-color: ${(props) => props.theme.darker};
  }
`;

const OptionImg = styled.div`
  width: 100%;
  margin-top: 8px;
`;

const WrapOption = styled.div`
  display: flex;
  width: 100%;
  gap: 8px;
  height: 32px;
  margin-bottom: 8px;
`;

const ItemOption = styled.div`
  border: 1px solid #d9d9d9;
  flex: 1;
  background: #f5f5f5;
  color: rgba(0, 0, 0, 0.25);
  cursor: not-allowed;
  padding: 4px 0 0 8px;
  text-overflow: ellipsis;
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
`;

const UploadImg = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  overflow: hidden;
  cursor: pointer;
  object-fit: fill;
  width: 60px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: rgba(0, 0, 0, 0.45);
  background: #fff;
  img {
    width: 100%;
    height: 100%;
    object-fit: fill;
  }
`;

const WrapLinking = styled.div`
  background: #f2f4f5;
  border-radius: 4px;
  padding: 12px;
  margin-bottom: 8px;
`;

const WrapOptionGrading = styled.div``;

const OptionGrading = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 12px;
  align-items: center;
  .ant-input-number,
  .ant-typography,
  label,
  .ant-select {
    width: 50%;
  }
`;

const WrapMinMax = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 8px;
  flex-wrap: wrap;
  .ant-form-item {
    margin-bottom: 0 !important;
    width: 60%;
  }
`;

const CustomText = styled.div`
  width: 100%;
  font-size: 16px;
  font-family: var(--roboto-500);
  line-height: 26px;
  margin-bottom: 8px;
`;
