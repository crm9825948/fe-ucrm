import React, { useEffect } from "react";
import styled from "styled-components";
import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import { Input } from "antd";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import {
  updateColumns,
  updateFieldMapping,
  updateListOrdered,
  updateModalSection,
  updateSectionName,
} from "redux/slices/formSetting";
import { Notification } from "components/Notification/Noti";

const ModalSection = ({ listFieldMapping, listOrdered }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { modalSection, sectionName } = useSelector(
    (state) => state.formSettingReducer
  );

  useEffect(() => {
    if (sectionName) {
      form.setFieldsValue({ section_name: sectionName });
    }
  }, [sectionName, form]);
  const _onCancel = () => {
    dispatch(updateModalSection(false));
    dispatch(updateSectionName(""));
    form.resetFields();
  };
  const handleColumns = (ordereds, listField) => {
    let tem = {};
    ordereds.forEach((item) => {
      let list = [];
      listField.forEach((field) => {
        if (field.section === item) {
          list.push(field);
        }
      });
      tem[item] = list;
    });
    return tem;
  };
  const _onSubmit = (values) => {
    if (sectionName) {
      let index = listOrdered.findIndex((item) => item === sectionName);
      let temOrdered = [...listOrdered];
      if (
        listOrdered.includes(values.section_name.trim()) &&
        values.section_name.trim() !== sectionName
      ) {
        Notification("warning", "Section already exists!");
      } else {
        temOrdered[index] = values.section_name.trim();
        let tempMapping = [];
        listFieldMapping.forEach((item) => {
          let temPagination = [];
          item.pagination_option?.forEach((option) => {
            temPagination.push({
              value: option.value,
              option:
                option.option === sectionName
                  ? values.section_name.trim()
                  : option.option,
            });
          });
          let newItem = { ...item, pagination_option: temPagination };
          if (item.section === sectionName) {
            tempMapping.push({
              ...newItem,
              section: values.section_name.trim(),
            });
          } else {
            tempMapping.push(newItem);
          }
        });
        dispatch(updateListOrdered(temOrdered));
        dispatch(updateFieldMapping(tempMapping));
        dispatch(updateColumns(handleColumns(temOrdered, tempMapping)));
        Notification("success", "Update section success!");

        _onCancel();
      }
    } else {
      if (listOrdered.includes(values.section_name.trim())) {
        Notification("warning", "Section already exists!");
      } else {
        let tem = [...listOrdered, values.section_name.trim()];
        dispatch(updateListOrdered(tem));
        Notification("success", "Add section success!");
        dispatch(updateColumns(handleColumns(tem, listFieldMapping)));
        _onCancel();
      }
    }
  };
  return (
    <ModalCustom
      title={sectionName ? "Update section" : "Add section"}
      visible={modalSection}
      footer={null}
      width={600}
      onCancel={_onCancel}
    >
      <Form form={form} onFinish={_onSubmit} colon={false} layout="vertical">
        <Form.Item
          label="Section name"
          name="section_name"
          rules={[
            { required: true, message: t("form.required") },
            () => ({
              validator: (_, value) => {
                let temp = false;
                if (value && value.trim().length === 0) {
                  temp = true;
                }
                if (temp) {
                  return Promise.reject(new Error(t("knowledgeBase.required")));
                } else {
                  return Promise.resolve();
                }
              },
            }),
          ]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>
        <WrapButton>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
          <Button onClick={_onCancel}>Cancel</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
};

export default ModalSection;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-500);
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
