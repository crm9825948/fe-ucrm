import { Form, Checkbox, Input, DatePicker, Radio, Space } from "antd";
import React from "react";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import OclockImg from "../../assets/icons/common/Oclock.png";
import DeleteImg from "../../assets/icons/common/deleteRelated.png";
import OptionActive from "./OptionActive";
import CloseImg from "../../assets/icons/common/close-circle.png";
import moment from "moment";
import { Notification } from "components/Notification/Noti";
import { useDispatch } from "react-redux";
import { createFormsetting, updateFormSetting } from "redux/slices/formSetting";
import { useNavigate, useParams } from "react-router";

const { RangePicker } = DatePicker;
const listOptionTime = [
  {
    label: "10s",
    value: 10,
  },
  {
    label: "20s",
    value: 20,
  },
  {
    label: "30s",
    value: 30,
  },
  {
    label: "40s",
    value: 40,
  },
  {
    label: "form.custom",
    value: "custom",
  },
];

const listOptionScroll = [
  { label: "form.aLittle", value: "little" },
  { label: "form.overHalf", value: "half" },
  { label: "form.endPage", value: "end" },
];
const DesignData = ({
  step,
  setStep,
  form,
  dataUI,
  listFieldMapping,
  background,
  positionImage,
  position,
  fontColor,
  time,
  setTime,
  frequency,
  setFrequency,
  all,
  setAll,
  apply,
  setApply,
  listWebsite,
  setListWebsite,
  listMappingGrading,
  listOrdered,
}) => {
  const { t } = useTranslation();
  //eslint-disable-next-line
  const [render, setRender] = useState("");
  const [flagInput, setFlagInput] = useState(false);
  const [flag, setFlag] = useState();
  const { objectId, configId } = useParams();

  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleAddTime = (day, value) => {
    let temp = [...time];

    temp.forEach((item, index) => {
      if (item.value === day) {
        let flag = false;
        item.listTime.forEach((times) => {
          if (
            value[0] === times[0] ||
            (value[0] > times[0] && value[0] < times[1]) ||
            (value[1] > times[0] && value[1] < times[1]) ||
            (value[0] < times[0] && value[1] > times[1]) ||
            (value[1] === times[1] && value[0] < times[0])
          ) {
            flag = true;
          }
        });
        if (flag) {
          Notification("warning", "Schedule overlap time!!!");
        } else {
          // item.listTime.push(value);
          temp[index] = {
            ...temp[index],
            listTime: [...temp[index].listTime, value],
          };
        }
      }
    });

    setTime(temp);
  };

  const handleDeleteTime = (day, idx) => {
    let temp = [...time];
    temp.forEach((item, id) => {
      if (item.value === day) {
        let tem = item.listTime.filter((_, index) => index !== idx);
        // temp[id].listTime = tem;
        temp[id] = {
          ...temp[id],
          listTime: tem,
        };
      }
    });
    setTime(temp);
  };

  const handleValueChange = (value) => {
    switch (!Object.values(value)[0] && Object.keys(value)[0]) {
      case "is_access":
        form.setFieldsValue({
          time_access: 10,
          custom_access: 60,
        });
        break;
      case "is_not_interact":
        form.setFieldsValue({
          time_not_interact: 10,
          custom_interact: 60,
        });
        break;
      case "is_scroll":
        form.setFieldsValue({
          scroll_option: "little",
        });
        break;
      default:
        break;
    }
  };

  const isValidUrl = (string) => {
    try {
      new URL(string);
      return true;
    } catch (err) {
      return false;
    }
  };

  const handleAddWebsite = (e) => {
    if (e.target.value && isValidUrl(e.target.value)) {
      let temp = [...listWebsite, new URL(e.target.value).origin];
      setListWebsite(temp);
      setFlagInput(false);
    }
  };

  const handleDeleteWebsite = (idx) => {
    let temp = listWebsite.filter((item, index) => index !== idx);
    setListWebsite(temp);
  };

  const renderListWebsite = (list) =>
    list.map((item, index) => (
      <ItemWebsite key={index}>
        <span>{new URL(item).origin}</span>
        <img
          src={DeleteImg}
          onClick={() => handleDeleteWebsite(index)}
          alt="delete"
        />
      </ItemWebsite>
    ));

  const renderListTime = (list, flag) =>
    list.map((item, index) => (
      <CustomItemTime key={index}>
        <CustomLabel>{t(item.label)}</CustomLabel>
        <WrapTime>
          {item.listTime.map((itemTime, index) => (
            <ItemTime key={index}>
              <RangePicker
                disabled
                format={"HH:mm"}
                picker="time"
                value={[
                  moment(itemTime[0], "HH:mm"),
                  moment(itemTime[1], "HH:mm"),
                ]}
              />
              <img
                src={CloseImg}
                alt="Remove time"
                onClick={() => handleDeleteTime(item.value, index)}
              />
            </ItemTime>
          ))}
          <ItemTime>
            {flag === item.value ? (
              <RangePicker
                format={"HH:mm"}
                picker="time"
                onChange={(value, values) => {
                  handleAddTime(item.value, values);
                  setFlag("");
                }}
              />
            ) : (
              <CustomIconAdd onClick={() => setFlag(item.value)}>
                +
              </CustomIconAdd>
            )}
          </ItemTime>
        </WrapTime>
      </CustomItemTime>
    ));

  const handleFinish = (value) => {
    let data = {
      ...dataUI,
      list_field: listFieldMapping,
      font_desktop: dataUI.font_desktop.toString(),
      font_mobile: dataUI.font_mobile.toString(),
      object_id: objectId,
      background_position: positionImage,
      background_url: background,
      position_display: position,
      font_color: fontColor,
      list_website: listWebsite,
      is_apply_all: all,
      apply_for: apply,
      time_display: time,
      display_frequency: frequency,
      is_access: value.is_access,
      is_click: value.is_click,
      is_not_interact: value.is_not_interact,
      is_scroll: value.is_scroll,
      time_access: value.custom_access
        ? value.custom_access
        : value.time_access,
      time_not_interact: value.custom_interact
        ? value.custom_interact
        : value.time_not_interact,
      id_click: value.id_click,
      scroll_option: value.scroll_option,
      grading_mapping: dataUI.check_grading ? listMappingGrading : undefined,
      sections: listOrdered,
    };
    if (configId) {
      dispatch(updateFormSetting({ config_id: configId, data: data }));
    } else {
      dispatch(createFormsetting(data));
    }
  };
  return (
    <Wrap step={step}>
      <Left>
        <Description>
          <img src={OclockImg} alt="Oclock" />
          <Detail>
            <span>{t("form.time")}</span>
            <span>{t("form.descriptionTime")}</span>
          </Detail>
        </Description>
        <Form
          form={form}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          onValuesChange={(value, values) => {
            setRender(value);
            handleValueChange(value);
          }}
          initialValues={{
            is_access: false,
            is_not_interact: false,
            is_click: false,
            is_scroll: false,
            time_access: 10,
            custom_access: 60,
            time_not_interact: 10,
            custom_interact: 60,
            scroll_option: "little",
          }}
          onFinish={handleFinish}
        >
          <OptionActive
            form={form.getFieldsValue()}
            listOption={listOptionTime}
            name="form.userAccess"
            activeName="is_access"
            optionName="time_access"
            customOption="custom_access"
          />
          <OptionActive
            form={form.getFieldsValue()}
            listOption={listOptionTime}
            name="form.userNotInteract"
            activeName="is_not_interact"
            optionName="time_not_interact"
            customOption="custom_interact"
          />
          <OptionActive
            form={form.getFieldsValue()}
            listOption={listOptionScroll}
            name="form.userScroll"
            activeName="is_scroll"
            optionName="scroll_option"
          />
          <OptionActive
            form={form.getFieldsValue()}
            listOption={[]}
            name="form.userClick"
            activeName="is_click"
            optionName="id_click"
          />
        </Form>
        <ButtonWrap>
          <CustomButton onClick={() => navigate("/form-setting")}>
            {t("common.cancel")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 0 ? "none" : "flex" }}
            onClick={() => setStep(step - 1)}
          >
            {t("common.back")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 1 ? "none" : "flex" }}
            onClick={() => setStep(step + 1)}
          >
            {t("common.next")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 1 ? "flex" : "none" }}
            onClick={() => {
              form.submit();
              setTimeout(() => {
                navigate("/form-setting");
              }, 1000);
            }}
          >
            {t("common.save")}
          </CustomButton>
        </ButtonWrap>
      </Left>
      <Right>
        <Website>
          <span>{t("form.website")}</span>
          <ButtonAdd onClick={() => setFlagInput(true)}>
            + {t("form.addWebsite")}
          </ButtonAdd>
          <Checkbox
            checked={all}
            onChange={(value) => setAll(value.target.checked)}
          >
            {t("form.applyAll")}
          </Checkbox>
        </Website>
        <WrapList>{renderListWebsite(listWebsite)}</WrapList>
        {flagInput && (
          <Input
            style={{ marginLeft: "16px", width: "98%" }}
            onPressEnter={handleAddWebsite}
            onBlur={handleAddWebsite}
            placeholder={t("form.pleaseInput")}
          />
        )}
        <AppliedWrap>
          <CustomLabel>{t("form.appliedFor")}</CustomLabel>

          <Checkbox.Group value={apply} onChange={(value) => setApply(value)}>
            <Checkbox value="desktop">{t("form.desktop")}</Checkbox>
            <Checkbox value="mobile">{t("form.mobile")}</Checkbox>
          </Checkbox.Group>
        </AppliedWrap>
        <TimeWrap>
          <CustomLabel>{t("form.activeTime")}</CustomLabel>
          {renderListTime(time, flag)}
        </TimeWrap>
        <Frequency>
          <CustomLabel>{t("form.displayFrequency")}</CustomLabel>
          <CustomText>{t("form.adjustTheLevel")}</CustomText>
          <OptionFrequency>
            <Radio.Group
              value={frequency}
              onChange={(e) => setFrequency(e.target.value)}
            >
              <Space direction="vertical">
                {/* <Radio value={"only_user"}>{t("form.perUser")}</Radio> */}
                <Radio value={"only_session"}>{t("form.perSession")}</Radio>
                <Radio value={"only_everyhour"}>{t("form.perHour")}</Radio>
                <Radio value={"only_everyday"}>{t("form.perDay")}</Radio>
                <Radio value={"most_show"}>{t("form.assPossible")}</Radio>
              </Space>
            </Radio.Group>
          </OptionFrequency>
        </Frequency>
      </Right>
    </Wrap>
  );
};
export default DesignData;

const Wrap = styled.div`
  width: 100%;
  background: #fff;
  color: #2c2c2c;
  line-height: 22px;
  font-size: 16px;
  display: ${(props) => (props.step === 1 ? "flex" : "none")};
  .ant-checkbox-wrapper {
    display: flex;
    span {
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;
    }
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
`;

const Left = styled.div`
  flex: 1;
  .ant-form-item {
    margin-bottom: 0px !important;
  }
`;
const Right = styled.div`
  width: 590px;
  margin-left: 24px;
`;
const Description = styled.div`
  display: flex;
  padding: 16px 24px;
  background: #f0f0f0;
  img {
    object-fit: contain;
    margin-right: 8px;
  }

  span {
    :first-child {
      font-family: var(--roboto-500);
      color: #2c2c2c;
    }
    color: #6b6b6b;
  }
`;
const Detail = styled.div`
  display: flex;
  flex-direction: column;
`;
const Website = styled.div`
  display: flex;
  align-items: center;
  span {
    :first-child {
      font-family: var(--roboto-500);
    }
  }
`;
const ButtonAdd = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  transition: all 0.5s;
  cursor: pointer;
  :hover {
    background: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
  padding: 2px 16px;
  margin: 0 16px;
`;
const WrapList = styled.div`
  margin: 8px 0px 8px 16px;
`;
const ItemWebsite = styled.div`
  transition: all 0.5s;
  border-bottom: 0.5px solid #ececec;
  padding: 4px 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  img {
    visibility: hidden;
    opacity: 0;
    transition: all 0.5s;
  }
  cursor: pointer;
  :hover {
    background: #ececec;
    img {
      display: block;
      visibility: visible;
      opacity: 1;
    }
  }
`;

const AppliedWrap = styled.div`
  padding: 16px 0;
  /* border-top: 1px solid #ececec; */
  border-bottom: 1px solid #ececec;
  display: flex;
  align-items: center;
  .ant-checkbox-group {
    display: flex;
  }
`;
const TimeWrap = styled.div`
  padding: 16px 0;
  border-bottom: 1px solid #ececec;
`;
const CustomLabel = styled.span`
  font-family: var(--roboto-500);
  width: 100px;
`;

const CustomItemTime = styled.div`
  display: flex;
  margin-top: 12px;
  margin-left: 16px;
`;
const WrapTime = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  gap: 12px;
`;
const ItemTime = styled.div`
  position: relative;
  width: calc((100% - 24px) / 3);
  .ant-picker-focused,
  .ant-picker:hover {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }

  img {
    position: absolute;
    top: -8px;
    right: -8px;
    cursor: pointer;
    width: 16px;
    visibility: hidden;
    opacity: 0;
    transition: all 0.5s;
  }

  :hover {
    img {
      visibility: visible;
      opacity: 1;
    }
  }
`;

const CustomIconAdd = styled.div`
  border: 1px solid #d9d9d9;
  cursor: pointer;
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
`;
const ButtonWrap = styled.div`
  margin-top: 16px;
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  cursor: pointer;
  background: #fff;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  :nth-child(4) {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;

const Frequency = styled.div`
  margin-top: 16px;
`;
const CustomText = styled.div`
  color: rgba(0, 0, 0, 0.45);
`;

const OptionFrequency = styled.div`
  padding: 8px 0px 0px 16px;
  label {
    font-family: var(--roboto-400) !important;
  }
`;
