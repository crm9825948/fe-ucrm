import React from "react";
import {
  Form,
  Input,
  InputNumber,
  DatePicker,
  Select,
  Rate,
  Slider,
  Radio,
  Upload,
  Button,
} from "antd";
import styled from "styled-components/macro";
import { UploadOutlined } from "@ant-design/icons";
import { Notification } from "components/Notification/Noti";
import _ from "lodash";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import InputMask from "react-input-mask";
import moment from "moment";

const { TextArea } = Input;
const { Option } = Select;
const FormContent = ({ fields, form, defaultValue, listOrdered, page }) => {
  const handleFieldName = (id, list) => {
    let name = id;
    list.forEach((item) => {
      if (item.id === id) {
        name = item.name;
      }
    });
    return name;
  };
  const handleTypeDisplayNumber = (type, step, max, field) => {
    if (type === "rating") {
      return <Rate allowHalf count={max} />;
    } else if (type === "slide") {
      return <Slider max={max} step={step ? step : 1} />;
    } else {
      return (
        <InputNumber
          step={step ? step : 1}
          placeholder={"Please input"}
          min={
            typeof field.input_min_value === "number"
              ? field.input_min_value
              : undefined
          }
          max={
            typeof field.input_max_value === "number"
              ? field.input_max_value
              : undefined
          }
          formatter={(value) =>
            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          }
        />
      );
    }
  };
  const handleTypeDisplaySelect = (
    type,
    opsitons,
    withImg,
    optionImg,
    field
  ) => {
    if (type === "select") {
      return (
        <Select showSearch={field.is_search} placeholder={"Please select"}>
          {opsitons.map((option, index) => (
            <Option key={index} value={option}>
              {option}
            </Option>
          ))}
        </Select>
      );
    } else {
      return (
        <RadioGroup withImg={withImg}>
          {opsitons.map((option, index) =>
            withImg ? (
              <CustomRadio key={index} value={option}>
                <WrapImg>
                  <img src={mapValueImg(option, optionImg)} alt="" />{" "}
                  <span>{option}</span>
                </WrapImg>
              </CustomRadio>
            ) : (
              <Radio key={index} value={option}>
                {option}
              </Radio>
            )
          )}
        </RadioGroup>
      );
    }
  };
  const mapValueImg = (value, list) => {
    let src = "";
    list.forEach((item) => {
      if (item.value === value) {
        src = item.url;
      }
    });
    return src;
  };
  const handleTypeAccept = (list) => {
    let tem = "";
    if (list) {
      tem = list.join(",");
    }
    return tem;
  };
  const normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e?.fileList;
  };
  const handleReplaceDescription = (field) => {
    let tem = _.get(field, "description", "");
    Object.entries(defaultValue).forEach((item) => {
      tem = tem.replaceAll(`$${item[0]}`, item[1]);
    });
    fields.forEach((item) => {
      if (item.field_type && window.location.href.includes("internet-form")) {
        tem = tem.replaceAll(`$${item.id}`, "");
      }
    });
    return tem;
  };
  const checkHidden = (length = 16, index, left = 0, right = 0, start, end) => {
    let flag = false;
    if (
      index <= left ||
      index > length - right ||
      ((start === 0 || start) &&
        (end === 0 || end) &&
        index >= start &&
        index <= end)
    ) {
      flag = true;
    }
    return flag;
  };
  const handleMaskText = (field) => {
    const {
      input_hidden_length = 16,
      input_hidden_left = 0,
      input_hidden_right = 0,
      input_hidden_start,
      input_hidden_end,
    } = field;
    let temMask = "";
    for (let i = 1; i < input_hidden_length + 1; i++) {
      if (
        checkHidden(
          input_hidden_length,
          i,
          input_hidden_left,
          input_hidden_right,
          input_hidden_start,
          input_hidden_end
        )
      ) {
        temMask += "x";
      } else {
        temMask += `*`;
      }
    }
    return temMask;
  };
  const handleMaskInput = (field) => {
    let temMask = handleMaskText(field);
    return (
      <InputMask
        className="ant-input"
        placeholder={temMask.replaceAll("*", "_")}
        mask={temMask}
      ></InputMask>
    );
  };
  const renderForm = (listField) => {
    return listField.map((field, index) => {
      if (field.section === listOrdered[page]) {
        if (
          Object.keys(defaultValue).includes(field.id) ||
          (field.is_conditional_display &&
            form.getFieldValue(field.triggering_field) !==
              field.triggering_answer &&
            defaultValue[field.triggering_field] !==
              field.triggering_answer?.toString())
        ) {
          return <></>;
        } else {
          if (!field.field_type) {
            return (
              <WrapParse>
                {parse(handleReplaceDescription(field), optionsParse)}
              </WrapParse>
            );
          } else if (field.field_type === "number") {
            return (
              <Form.Item
                key={index}
                label={field.name}
                name={field.id}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                  () => ({
                    validator: (_, value) => {
                      if (
                        (typeof field.input_min_value === "string" ||
                          typeof field.input_max_value === "string") &&
                        field.type_display === "input"
                      ) {
                        if (
                          typeof field.input_min_value === "string" &&
                          (value === 0 || value) &&
                          (form.getFieldValue(field.input_min_value) === 0 ||
                            form.getFieldValue(field.input_min_value)) &&
                          value < form.getFieldValue(field.input_min_value)
                        ) {
                          return Promise.reject(
                            new Error(
                              `Value must be greater than field ${handleFieldName(
                                field.input_min_value,
                                fields
                              )}!`
                            )
                          );
                        } else if (
                          typeof field.input_max_value === "string" &&
                          (value === 0 || value) &&
                          (form.getFieldValue(field.input_max_value) === 0 ||
                            form.getFieldValue(field.input_max_value)) &&
                          value > form.getFieldValue(field.input_max_value)
                        ) {
                          return Promise.reject(
                            new Error(
                              `Value must be less than field ${handleFieldName(
                                field.input_max_value,
                                fields
                              )}!`
                            )
                          );
                        } else {
                          return Promise.resolve();
                        }
                      } else {
                        return Promise.resolve();
                      }
                    },
                  }),
                ]}
              >
                {field.field_options.length === 0
                  ? handleTypeDisplayNumber(
                      field.type_display,
                      field.step,
                      field.max_number ? field.max_number : 5,
                      field
                    )
                  : handleTypeDisplaySelect(
                      field.type_display,
                      field.field_options,
                      field.with_img,
                      field.option_with_img,
                      field
                    )}
              </Form.Item>
            );
          } else if (
            field.field_type === "date" ||
            field.field_type === "datetime-local"
          ) {
            const disabledDate = (current) => {
              return (
                (field.input_min_value &&
                  current <
                    moment(
                      field.input_min_value === "current"
                        ? undefined
                        : field.input_min_value
                    ).startOf("day")) ||
                (field.input_max_value &&
                  current >
                    moment(
                      field.input_max_value === "current"
                        ? undefined
                        : field.input_max_value
                    ).endOf("day"))
              );
            };
            let flagShowNow =
              (field.input_min_value &&
                new Date().getTime() <
                  new Date(
                    moment(field.input_min_value).startOf("day")
                  ).getTime()) ||
              (field.input_max_value &&
                new Date().getTime() >
                  new Date(
                    moment(field.input_max_value).startOf("day")
                  ).getTime());

            return (
              <Form.Item
                key={index}
                label={field.name}
                name={field.id}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                ]}
              >
                <DatePicker
                  showNow={!!!flagShowNow}
                  disabledDate={disabledDate}
                  showTime={field.field_type === "datetime-local"}
                />
              </Form.Item>
            );
          } else if (field.field_type === "textarea") {
            return (
              <Form.Item
                key={index}
                label={field.name}
                name={field.id}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                ]}
              >
                <TextArea
                  maxLength={
                    field.input_max_length ? field.input_max_length : 10000
                  }
                  rows={2}
                  placeholder={"Please input"}
                />
              </Form.Item>
            );
          } else if (field.field_type === "select") {
            return (
              <Form.Item
                key={index}
                label={field.name}
                name={field.id}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                ]}
              >
                {handleTypeDisplaySelect(
                  field.type_display,
                  field.field_options,
                  field.with_img,
                  field.option_with_img,
                  field
                )}
              </Form.Item>
            );
          } else if (field.field_type === "email") {
            return (
              <Form.Item
                key={index}
                label={field.name}
                name={field.id}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                  {
                    type: "email",
                    message: "The input is not valid E-mail!",
                  },
                ]}
              >
                <Input placeholder={"Please input"} />
              </Form.Item>
            );
          } else if (field.field_type === "file") {
            return (
              <Form.Item
                valuePropName="fileList"
                key={index}
                label={field.name}
                name={field.id}
                getValueFromEvent={normFile}
                rules={[
                  {
                    required: field.required,
                    message: "This field is required!",
                  },
                ]}
              >
                <Upload
                  accept={handleTypeAccept(field.allow_file_extensions)}
                  maxCount={field.number_of_attachments}
                  multiple={true}
                  beforeUpload={(file, fileList) => {
                    let regex = new RegExp('[!@#$%^&*(),?":{}|<>]');

                    let temCapacity = file.size;
                    let temNumber = 0;
                    if (form.getFieldValue(field.id)) {
                      form.getFieldValue(field.id).forEach((item) => {
                        temCapacity += item.size;
                      });
                      temNumber = form.getFieldValue(field.id).length;
                    }

                    const capacity =
                      temCapacity / 1024 / 1024 <
                      field.total_attachments_size_limit;
                    const fileExtension = file.name
                      .split(".")
                      .pop()
                      .toLowerCase();
                    if (temNumber === field.number_of_attachments) {
                      Notification(
                        "error",
                        `Maximum number of files is ${field.number_of_attachments}.!`
                      );
                      return Upload.LIST_IGNORE;
                    }
                    if (
                      !field.allow_file_extensions.includes(`.${fileExtension}`)
                    ) {
                      Notification(
                        "error",
                        `Invalid file type. Only ${field.allow_file_extensions} files are allowed.!`
                      );
                      return Upload.LIST_IGNORE;
                    }
                    if (!capacity) {
                      Notification(
                        "error",
                        `File must smaller than ${field.total_attachments_size_limit}MB!`
                      );
                      return Upload.LIST_IGNORE;
                    }

                    if (file.name.match(regex)) {
                      Notification(
                        "error",
                        "Tên file không được chứa kí tự đặc biệt"
                      );
                      return Upload.LIST_IGNORE;
                    }

                    return false;
                  }}
                >
                  <Button icon={<UploadOutlined />}>Select File</Button>
                </Upload>
              </Form.Item>
            );
          } else {
            if (field.is_saving_ip) {
              return <></>;
            } else {
              return (
                <Form.Item
                  key={index}
                  label={field.name}
                  name={field.id}
                  rules={[
                    {
                      required: field.required,
                      message: "This field is required!",
                    },
                    () => ({
                      validator: (_, value) => {
                        const regex =
                          /^(\+?84|0)(2[0-9]|3[2-9]|5[689]|7[0|6-9]|8[1-9]|9[0-4|6-9])(\d{7}|\d{8})$/;
                        const regexOnlyNumber = /^[0-9]+$/;
                        if (
                          !regex.test(value) &&
                          field.is_phone_number &&
                          value
                        ) {
                          return Promise.reject(
                            new Error("The input is not valid phone number!")
                          );
                        } else if (
                          !regexOnlyNumber.test(value) &&
                          field.only_number &&
                          value
                        ) {
                          return Promise.reject(
                            new Error("The input only accept number!")
                          );
                        } else if (
                          field.is_hidden &&
                          value &&
                          value.includes("_")
                        ) {
                          let temMask = handleMaskText(field).replaceAll(
                            "*",
                            "_"
                          );
                          if (temMask === value) {
                            return Promise.resolve();
                          } else {
                            return Promise.reject(new Error("Invalid format!"));
                          }
                        } else {
                          return Promise.resolve();
                        }
                      },
                    }),
                  ]}
                >
                  {field.is_hidden ? (
                    handleMaskInput(field)
                  ) : (
                    <Input
                      maxLength={
                        field.input_max_length ? field.input_max_length : 10000
                      }
                      placeholder={"Please input"}
                      onInput={(e) => {
                        if (field.is_uppercase) {
                          return (e.target.value =
                            e.target.value.toUpperCase());
                        } else if (field.is_lowercase) {
                          return (e.target.value =
                            e.target.value.toLowerCase());
                        } else {
                          return e.target.value;
                        }
                      }}
                    />
                  )}
                </Form.Item>
              );
            }
          }
        }
      } else {
        return <></>;
      }
    });
  };
  return <ContentWrap>{renderForm(fields)}</ContentWrap>;
};

export default FormContent;
FormContent.defaultProps = {
  listOrdered: ["Default"],
  page: 0,
  defaultValue: {},
};
const ContentWrap = styled.div`
  .ant-btn:hover,
  .ant-btn:active,
  .ant-btn:focus {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  label {
    white-space: pre-line;
    margin-left: 6px !important;
    ::before {
      position: absolute;
      bottom: 12px;
      left: -8px;
    }
  }
`;
const RadioGroup = styled(Radio.Group)`
  display: flex;
  flex-wrap: wrap;
  label {
    width: ${(props) => (props.withImg ? "135px" : "100%")};
    font-family: var(--roboto-400) !important;
    margin-right: 0;
    line-height: ${(props) => (props.withImg ? "10px" : "22px")} !important;
  }
`;

const CustomRadio = styled(Radio)`
  display: flex;
  flex-direction: column-reverse;
  justify-content: center;
  align-items: center;
  .ant-radio-wrapper {
    margin-right: 0;
  }
  img {
    width: 36px;
    height: 36px;
    border-radius: 50%;
    overflow: hidden;
    object-fit: cover;
    margin-bottom: 4px;
  }
`;
const WrapImg = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  span {
    font-size: 14px;
    line-height: 18px;
    text-align: center;
    text-overflow: ellipsis;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
`;

const WrapParse = styled.div`
  margin-bottom: 16px;
  max-width: 100%;
  overflow: auto;
  p {
    margin-bottom: 0 !important;
  }
`;
