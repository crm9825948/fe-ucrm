import React from "react";
import styled from "styled-components";
import { colors } from "@atlaskit/theme";
import { Draggable } from "react-beautiful-dnd";
import FormList from "./FormStyle/FormList";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import { useTranslation } from "react-i18next";
import { Tooltip, Typography } from "antd";
import { useDispatch } from "react-redux";
import {
  updateModalSection,
  updateSectionName,
} from "redux/slices/formSetting";
const { Text: TextComponent } = Typography;

const FormColumn = (props) => {
  const title = props.title;
  const quotes = props.quotes;
  const index = props.index;
  const { setDataDelete, setShowConfirmDelete } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  return (
    <Draggable draggableId={title} index={index} isDragDisabled={true}>
      {(provided, snapshot) => (
        <Container ref={provided.innerRef} {...provided.draggableProps}>
          <Header isDragging={snapshot.isDragging}>
            <FormTitle
              isDragging={snapshot.isDragging}
              {...provided.dragHandleProps}
              aria-label={`${title} quote list`}
            >
              <Title>
                <TextComponent ellipsis={{ tooltip: title }}>
                  {title}
                </TextComponent>
              </Title>
              <WrapAction>
                <Tooltip
                  onClick={() => {
                    dispatch(updateSectionName(title));
                    dispatch(updateModalSection(true));
                  }}
                  title={t("common.edit")}
                >
                  <img src={Edit} alt="edit" />
                </Tooltip>

                <Tooltip title={t("common.delete")}>
                  <img
                    onClick={() => {
                      setDataDelete({ name: title });
                      setShowConfirmDelete(true);
                    }}
                    src={Delete}
                    alt="delete"
                  />
                </Tooltip>
              </WrapAction>
            </FormTitle>
          </Header>
          <Wrap>
            <WrapItem>
              <Content>Content</Content>
              <Content>Field mapping</Content>
              <Action>Action</Action>
            </WrapItem>
          </Wrap>
          <FormList
            listId={title}
            listType="QUOTE"
            quotes={quotes}
            internalScroll={props.isScrollable}
            isCombineEnabled={Boolean(props.isCombineEnabled)}
          />
        </Container>
      )}
    </Draggable>
  );
};

export default FormColumn;

const Container = styled.div`
  margin-bottom: 8px;
  display: flex;
  flex-direction: column;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-top-left-radius: 2px;
  border-top-right-radius: 2px;
  background-color: ${({ isDragging }) =>
    isDragging ? colors.G50 : colors.N30};
  transition: background-color 0.2s ease;
  &:hover {
    background-color: ${colors.G50};
  }
`;
const WrapAction = styled.div`
  width: 50px;
  img {
    cursor: pointer;
    width: 20px;
    :first-child {
      margin-right: 10px;
    }
  }
`;

const FormTitle = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 8px 12px;
  span {
    font-size: 16px;
    font-family: var(--roboto-500);
  }
`;
const Wrap = styled.div`
  width: 100%;
  background: #ebecf0;
  padding: 0 8px;
`;

const WrapItem = styled.div`
  width: 100%;
  display: flex;
  background: #fff;
  padding: 8px;
`;
const Content = styled.div`
  width: 40%;
  font-family: var(--roboto-500);
`;
const Action = styled.div`
  width: 20%;
  display: flex;
  justify-content: end;
  font-family: var(--roboto-500);
`;
const Title = styled.div`
  width: calc(100% - 50px);
`;
