import React from "react";
import styled from "styled-components";

import PropTypes from "prop-types";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import FormColumn from "./FormColumn";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import { handleDeleteSection } from "redux/slices/formSetting";
import { useTranslation } from "react-i18next";
import { useState } from "react";
const FormBoard = ({
  isCombineEnabled,
  initial,
  containerHeight,
  withScrollableColumns,
  listOrdered,
  columns,
  onDragEnd,
}) => {
  const { t } = useTranslation();
  const [dataDelete, setDataDelete] = useState({});
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  return (
    <>
      <DragDropContext onDragEnd={(result) => onDragEnd(result, initial)}>
        <Droppable
          droppableId="board"
          type="COLUMN"
          direction="vertical"
          ignoreContainerClipping={Boolean(containerHeight)}
          isCombineEnabled={isCombineEnabled}
        >
          {(provided) => (
            <Container ref={provided.innerRef} {...provided.droppableProps}>
              {listOrdered.map((key, index) => (
                <FormColumn
                  key={key}
                  index={index}
                  title={key}
                  quotes={columns[key] ? columns[key] : []}
                  isScrollable={withScrollableColumns}
                  isCombineEnabled={isCombineEnabled}
                  setDataDelete={setDataDelete}
                  setShowConfirmDelete={setShowConfirmDelete}
                />
              ))}
              {provided.placeholder}
            </Container>
          )}
        </Droppable>
      </DragDropContext>
      <ModalConfirmDelete
        title={t("this section!")}
        decs={t("common.descriptionDelete")}
        methodDelete={handleDeleteSection}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={showConfirmDelete}
        setOpenConfirm={setShowConfirmDelete}
      />
    </>
  );
};

FormBoard.defaultProps = {
  isCombineEnabled: false,
};

FormBoard.propTypes = {
  isCombineEnabled: PropTypes.bool,
};

export default FormBoard;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 16px;
`;
