import React from "react";
import styled from "styled-components";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import { Tooltip, Typography } from "antd";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import {
  updateDataDelete,
  updateInfoField,
  updateOpenDrawer,
} from "redux/slices/formSetting";
import { setShowModalConfirmDelete } from "redux/slices/global";

const getBackgroundColor = (isDragging, isGroupedOver, authorColors) => {
  return "#FFFFFF";
};
const { Text: TextComponent } = Typography;
function QuoteItem(props) {
  const { quote, isDragging, isGroupedOver, provided, isClone, index } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { listFieldObject } = useSelector((state) => state.formSettingReducer);

  const handleFieldName = (id) => {
    let name = "";
    listFieldObject.forEach((field) => {
      if (field.ID === id) {
        name = field.name;
      }
    });
    return name;
  };

  return (
    <Container
      isDragging={isDragging}
      isGroupedOver={isGroupedOver}
      isClone={isClone}
      ref={provided.innerRef}
      {...provided.draggableProps}
      {...provided.dragHandleProps}
      data-is-dragging={isDragging}
      data-testid={quote.id}
      data-index={index}
    >
      <WrapItem>
        <Content>
          <TextComponent ellipsis={{ tooltip: quote.name }}>
            {quote.name}
          </TextComponent>
        </Content>
        <Content>
          <TextComponent ellipsis={{ tooltip: handleFieldName(quote.id) }}>
            {quote.description ? quote.description : handleFieldName(quote.id)}
          </TextComponent>
        </Content>
        <WrapAction>
          <Tooltip
            onClick={() => {
              dispatch(updateInfoField(quote));
              dispatch(
                updateOpenDrawer(quote.field_type ? "field" : "description")
              );
            }}
            title={t("common.edit")}
          >
            <img src={Edit} alt="edit" />
          </Tooltip>

          <Tooltip title={t("common.delete")}>
            <img
              onClick={() => {
                dispatch(updateDataDelete(quote));
                dispatch(setShowModalConfirmDelete(true));
              }}
              src={Delete}
              alt="delete"
            />
          </Tooltip>
        </WrapAction>
      </WrapItem>
    </Container>
  );
}

export default React.memo(QuoteItem);

const Container = styled.div`
  border-radius: 2px;
  border: 2px solid transparent;
  background-color: ${(props) =>
    getBackgroundColor(props.isDragging, props.isGroupedOver, props.colors)};
  box-shadow: ${({ isDragging }) =>
    isDragging ? `2px 2px 1px #EBECF0` : "none"};
  box-sizing: border-box;
  padding: 8px 0px 8px 8px;

  border-bottom: 1px solid #e9e9e9;
  display: flex;
  justify-content: center;
  align-items: color-interpolation-filters;
  &:hover,
  &:active {
    text-decoration: none;
  }
  &:focus {
    outline: none;
    box-shadow: none;
  }
`;

const WrapItem = styled.div`
  width: 100%;
  flex-wrap: wrap;
  justify-content: space-between;
  display: flex;
`;
const Content = styled.div`
  width: 40%;
`;
const WrapAction = styled.div`
  width: 20%;
  display: flex;
  justify-content: flex-end;
  img {
    cursor: pointer;
    width: 20px;
    :first-child {
      margin-right: 10px;
    }
  }
`;
