import React, { useState, useEffect, useCallback, useMemo } from "react";
import styled from "styled-components";
import Draggable from "react-draggable";
import _ from "lodash";
import {
  Checkbox,
  Col,
  Form,
  Input,
  Radio,
  Row,
  InputNumber,
  Dropdown,
  Select,
  Popover,
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import { BASE_URL_API, BE_URL, FE_URL } from "constants/constants";
import Upload from "antd/lib/upload";
import {
  EyeOutlined,
  MinusOutlined,
  PlusOutlined,
  CameraOutlined,
  DownOutlined,
} from "@ant-design/icons";
import FormView from "./FormView";
import Menu from "antd/lib/menu";
import { Notification } from "components/Notification/Noti";
import { useNavigate, useParams } from "react-router";
import Grading from "./Grading";
import FormBoard from "./FormBoard/FormBoard";
import ModalSection from "./ModalSection";
import { updateModalSection, updateOpenDrawer } from "redux/slices/formSetting";

const { TextArea } = Input;
const { Option } = Select;

const listPosition = [
  ["top_left", "top_center", "top_right"],
  ["center_left", "center_center", "center_right"],
  ["bottom_left", "bottom_center", "bottom_right"],
];
const colors = [
  "#2C2C2C",
  "#D81B60",
  "#8E24AA",
  "#5E35B1",
  "#3949AB",
  "#1E88E5",
  "#039BE5",
  "#00ACC1",
  "#00897B",
  "#43A047",
  "#7CB342",
  "#C0CA33",
  "#FDD835",
  "#FFB300",
  "#FB8C00",
  "#F4511E",
  "#6D4C41",
  "#757575",
  "#fff",
  "#000",
];

const DesignUI = ({
  fields,
  allField,
  objectId,
  listFieldMapping,
  step,
  setStep,
  form,
  background,
  setBackground,
  positionImage,
  setPositionImage,
  position,
  setPosition,
  fontColor,
  setFontColor,
  optionsGrading,
  setOptionsGrading,
  listMappingGrading,
  setListMappingGrading,
  listOrdered,
  columns,
  onDragEnd,
}) => {
  const { t } = useTranslation();
  const { configId } = useParams();

  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const navigate = useNavigate();
  const dispatch = useDispatch();
  // const [form] = Form.useForm();
  const [display, setDisplay] = useState("center");
  const [optionDisplay, setOptionDisplay] = useState("desktop");
  const [zoom, setZoom] = useState(100);
  const [render, setRender] = useState("");
  const [frameSize, setframeSize] = useState({ width: 0, height: 0 });
  const [modalSize, setmodalSize] = useState({ width: 0, height: 0 });
  const [positionDraggable, setPositionDraggable] = useState({ x: 0, y: 0 });
  const [openDropdown, setOpenDropDown] = useState(false);
  const [visible, setVisible] = useState(false);

  function getImageDimensions(path, callback) {
    let img = new Image();
    img.onload = function () {
      callback({
        width: img.width,
        height: img.height,
      });
    };
    img.src = path;
  }

  function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {
    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return {
      width: Math.round(srcWidth * ratio),
      height: Math.round(srcHeight * ratio),
    };
  }

  useEffect(() => {
    if (background) {
      getImageDimensions(background, function (data) {
        setframeSize(
          calculateAspectRatioFit(data?.width, data?.height, 200, 200)
        );
      });
    }
  }, [background, render]);

  useEffect(() => {
    if (optionDisplay === "desktop") {
      if (
        form.getFieldValue("desktop_width") &&
        form.getFieldValue("desktop_height") &&
        frameSize
      ) {
        setmodalSize(
          calculateAspectRatioFit(
            form.getFieldValue("desktop_width"),
            form.getFieldValue("desktop_height"),
            frameSize?.width,
            frameSize?.height
          )
        );
      }
    } else {
      if (
        form.getFieldValue("mobile_width") &&
        form.getFieldValue("mobile_height") &&
        frameSize
      ) {
        setmodalSize(
          calculateAspectRatioFit(
            form.getFieldValue("mobile_width"),
            form.getFieldValue("mobile_height"),
            frameSize?.width,
            frameSize?.height
          )
        );
      }
    }
  }, [frameSize, form, render, optionDisplay]);

  useEffect(() => {
    let tem = positionImage.split("%");
    if (modalSize && frameSize) {
      setPositionDraggable({
        x: (parseFloat(tem[0]) / 100) * (frameSize?.width - modalSize?.width),
        y: (parseFloat(tem[1]) / 100) * (frameSize?.height - modalSize?.height),
      });
    }
  }, [positionImage, frameSize, modalSize]);
  const handleChangeDisplay = (value) => {
    if (optionDisplay === "desktop") {
      switch (value) {
        case "center":
          form.setFieldsValue({
            desktop_top: 20,
            desktop_bottom: 20,
            desktop_left: 20,
            desktop_right: 20,
          });
          break;
        case "left":
          form.setFieldsValue({
            desktop_top: 20,
            desktop_bottom: 20,
            desktop_left: 20,
            desktop_right: parseInt(form.getFieldValue("desktop_width")) / 2,
          });
          break;
        case "right":
          form.setFieldsValue({
            desktop_top: 20,
            desktop_bottom: 20,
            desktop_left: parseInt(form.getFieldValue("desktop_width")) / 2,
            desktop_right: 20,
          });
          break;
        case "bottom":
          form.setFieldsValue({
            desktop_top: parseInt(form.getFieldValue("desktop_height")) / 2,
            desktop_bottom: 20,
            desktop_left: 20,
            desktop_right: 20,
          });
          break;
        case "top":
          form.setFieldsValue({
            desktop_top: 20,
            desktop_bottom: parseInt(form.getFieldValue("desktop_height")) / 2,
            desktop_left: 20,
            desktop_right: 20,
          });
          break;
        default:
          break;
      }
    } else {
      switch (value) {
        case "center":
          form.setFieldsValue({
            mobile_top: 10,
            mobile_bottom: 10,
            mobile_left: 10,
            mobile_right: 10,
          });
          break;
        case "left":
          form.setFieldsValue({
            mobile_top: 10,
            mobile_bottom: 10,
            mobile_left: 10,
            mobile_right: parseInt(form.getFieldValue("mobile_width")) / 2,
          });
          break;
        case "right":
          form.setFieldsValue({
            mobile_top: 10,
            mobile_bottom: 10,
            mobile_left: parseInt(form.getFieldValue("mobile_width")) / 2,
            mobile_right: 10,
          });
          break;
        case "bottom":
          form.setFieldsValue({
            mobile_top: parseInt(form.getFieldValue("mobile_height")) / 2,
            mobile_bottom: 10,
            mobile_left: 10,
            mobile_right: 10,
          });
          break;
        case "top":
          form.setFieldsValue({
            mobile_top: 10,
            mobile_bottom: parseInt(form.getFieldValue("mobile_height")) / 2,
            mobile_left: 10,
            mobile_right: 10,
          });
          break;
        default:
          break;
      }
    }
  };
  const renderPosition = (list) => {
    return list.map((item, index) => (
      <ItemWrap key={index} isCenter={index === 1 ? true : false}>
        {item.map((value, idx) => {
          return (
            <Item
              onClick={() => setPosition(value)}
              isActive={position === value}
              key={idx}
              height={index === 1 ? 88 : 44}
            ></Item>
          );
        })}
      </ItemWrap>
    ));
  };
  const handleZoom = (action) => {
    let temp = zoom;
    if (action === "minus") {
      if (temp > 50) {
        setZoom(temp - 1);
      }
    } else {
      if (temp < 100) {
        setZoom(temp + 1);
      }
    }
  };
  const onSearchHandle = useCallback((searchText) => {
    setRender(searchText);
  }, []);

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 500),
    [onSearchHandle]
  );

  const renderColor = () => {
    return (
      <CustomMenu>
        <Row>
          {colors.map((color) => {
            return (
              <Col key={color}>
                <CustomColor
                  onClick={() => {
                    setFontColor(color);
                    setVisible(false);
                  }}
                  color={color}
                />
              </Col>
            );
          })}
        </Row>
      </CustomMenu>
    );
  };
  const renderListOption = (list) => {
    return list.map((item, index) => {
      return (
        <Option value={item.ID} key={index}>
          {item.name}
        </Option>
      );
    });
  };

  const handleFinish = (values) => {
    setStep(step + 1);
  };
  const findPosition = (id, list) => {
    let tem = 0;
    list.forEach((item) => {
      if (item.id === id) {
        tem = item.position;
      }
    });
    return tem;
  };

  const checkDynamic = () => {
    let text = "";
    let flag = true;
    listFieldMapping.forEach((field) => {
      if (
        (findPosition(field.id, listFieldMapping) <
          findPosition(field.triggering_field, listFieldMapping) ||
          findPosition(field.id, listFieldMapping) <
            findPosition(field.input_min_value, listFieldMapping) ||
          findPosition(field.id, listFieldMapping) <
            findPosition(field.input_max_value, listFieldMapping)) &&
        flag
      ) {
        text = `Field ${field.name} is positioned before its trigger.`;
        flag = false;
      }
    });
    return text;
  };

  const checkMappingGrading = () => {
    let flag = false;
    listMappingGrading.forEach((mapping) => {
      if (
        !mapping.grading_value ||
        (mapping.min_val !== 0 && !mapping.min_val) ||
        (!mapping.max_max && mapping.max_max !== 0)
      ) {
        flag = true;
        return;
      }
    });
    return flag;
  };
  const checkSection = () => {
    let flag = false;
    Object.values(columns).forEach((item) => {
      if (item.length === 0) {
        flag = true;
      }
    });
    return flag;
  };
  const menuView = (
    <div>
      <CustomItem
        onClick={() => {
          setOpenDropDown(false);
          dispatch(updateModalSection(true));
        }}
      >
        Add section
      </CustomItem>
      <CustomItem
        onClick={() => {
          setOpenDropDown(false);
          dispatch(updateOpenDrawer("field"));
        }}
      >
        Add field
      </CustomItem>
      <CustomItem
        onClick={() => {
          setOpenDropDown(false);
          dispatch(updateOpenDrawer("description"));
        }}
      >
        Add description
      </CustomItem>
    </div>
  );
  return (
    <WrapContent step={step}>
      <SettingWrap>
        <Form
          form={form}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          initialValues={{
            form_save: "Submit",
            form_cancel: "Cancel",
            desktop_top: 20,
            desktop_bottom: 20,
            desktop_left: 20,
            desktop_right: 20,
            desktop_width: 800,
            desktop_height: 600,
            mobile_top: 10,
            mobile_bottom: 10,
            mobile_left: 10,
            mobile_right: 10,
            mobile_width: 300,
            mobile_height: 300,
            display: "center",
            dark_screen: true,
            rounded: true,
            font_style: "bold",
            font_mobile: 12,
            font_desktop: 16,
            scoring_option: "no_scoring",
            form_type: "Internet Form",
            check_grading: false,
          }}
          onValuesChange={(value, values) => {
            debouncedSearchHandler(value);
          }}
          onFinish={handleFinish}
        >
          <Form.Item
            label="Name"
            name="form_name"
            rules={[
              {
                required: form.getFieldValue("form_type") === "QM_FORM",
                message: t("form.required"),
              },
            ]}
          >
            <Input placeholder={t("common.placeholderInput")} />
          </Form.Item>
          <Form.Item label="Description" name="form_description">
            <TextArea rows={2} placeholder={t("common.placeholderInput")} />
          </Form.Item>
          <Form.Item label="Field return" name="field_return">
            <Select placeholder={t("common.placeholderSelect")}>
              {renderListOption(allField)}
            </Select>
          </Form.Item>
          <Form.Item label="Priority" name="priority">
            <InputNumber
              placeholder={t("common.placeholderInput")}
              parser={(value) => parseInt(value)}
              min={0}
            />
          </Form.Item>
          <Form.Item label="Owner id" name="owner_id">
            <Select placeholder={t("common.placeholderSelect")}>
              {listAllUser.map((user, index) => (
                <Option value={user._id} key={index}>
                  {user.Full_Name}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Form type" name="form_type">
            <Select
              placeholder={t("common.placeholderSelect")}
              disabled={configId || objectId !== "obj_crm_qm_interaction_00001"}
            >
              <Option value={"Internet Form"}>Internet Form</Option>
              <Option value={"QM_FORM"}>QM Form</Option>
            </Select>
          </Form.Item>
          <Grading
            listFieldMapping={listFieldMapping}
            form={form}
            fields={fields}
            optionsGrading={optionsGrading}
            setOptionsGrading={setOptionsGrading}
            listMappingGrading={listMappingGrading}
            setListMappingGrading={setListMappingGrading}
          />
          <FieldWrap>
            <span>{t("form.content")}</span>
            <Popover
              overlayClassName="custom_popover"
              visible={openDropdown}
              content={menuView}
              trigger={"click"}
              onVisibleChange={() => setOpenDropDown(!openDropdown)}
              placement="bottom"
            >
              <ButtonAdd>
                Add section <DownOutlined />
              </ButtonAdd>
            </Popover>
          </FieldWrap>

          {listOrdered.length > 0 && (
            <>
              <FormBoard
                initial={listFieldMapping}
                listOrdered={listOrdered}
                columns={columns}
                onDragEnd={onDragEnd}
              />
            </>
          )}
          <Row gutter={[16, 0]}>
            <Col span={12}>
              <Form.Item
                label={t("form.mainLabel")}
                name="form_save"
                rules={[{ required: true, message: t("form.required") }]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label={t("form.subLabel")}
                name="form_cancel"
                rules={[{ required: true, message: t("form.required") }]}
              >
                <Input placeholder={t("common.placeholderInput")} />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label={t("form.displayConfig")} value="display">
            <Radio.Group
              onChange={(e) => {
                setDisplay(e.target.value);
                handleChangeDisplay(e.target.value);
              }}
            >
              <DisplayItem value="top">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_10511_328817)">
                    <path
                      d="M24 22C24 23.1046 23.1046 24 22 24L2 24C0.89543 24 7.8281e-08 23.1046 1.74846e-07 22L1.9233e-06 2C2.01987e-06 0.895428 0.895434 -2.01987e-06 2 -1.9233e-06L22 -1.74846e-07C23.1046 -7.8281e-08 24 0.895432 24 2L24 22Z"
                      fill={
                        display === "top"
                          ? defaultBrandName.theme_color
                          : "#d9d9d9"
                      }
                    />
                    <path
                      d="M24 12L0 12L8.74228e-07 2C9.70792e-07 0.895428 0.895433 -2.01987e-06 2 -1.9233e-06L22 -1.74846e-07C23.1046 -7.8281e-08 24 0.895431 24 2L24 12Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="23.5"
                    y="23.5"
                    width="23"
                    height="23"
                    rx="1.5"
                    transform="rotate(-180 23.5 23.5)"
                    stroke={
                      display === "top"
                        ? defaultBrandName.theme_color
                        : "#d9d9d9"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_10511_328817">
                      <rect
                        x="24"
                        y="24"
                        width="24"
                        height="24"
                        rx="2"
                        transform="rotate(-180 24 24)"
                        fill="white"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </DisplayItem>
              <DisplayItem value="left">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_10255_337081)">
                    <path
                      d="M0 2C0 0.89543 0.895431 0 2 0H22C23.1046 0 24 0.895431 24 2V22C24 23.1046 23.1046 24 22 24H2C0.89543 24 0 23.1046 0 22V2Z"
                      fill={
                        display === "left"
                          ? defaultBrandName.theme_color
                          : "#d9d9d9"
                      }
                    />
                    <path
                      d="M0 2C0 0.89543 0.895431 0 2 0H12V24H2C0.895431 24 0 23.1046 0 22V2Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="0.5"
                    y="0.5"
                    width="23"
                    height="23"
                    rx="1.5"
                    stroke={
                      display === "left"
                        ? defaultBrandName.theme_color
                        : "#d9d9d9"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_10255_337081">
                      <rect width="24" height="24" rx="2" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </DisplayItem>
              <DisplayItem value="center">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_10255_337086)">
                    <path
                      d="M0 2C0 0.89543 0.895431 0 2 0H22C23.1046 0 24 0.895431 24 2V22C24 23.1046 23.1046 24 22 24H2C0.89543 24 0 23.1046 0 22V2Z"
                      fill={
                        display === "center"
                          ? defaultBrandName.theme_color
                          : "#d9d9d9"
                      }
                    />
                  </g>
                  <rect
                    x="0.5"
                    y="0.5"
                    width="23"
                    height="23"
                    rx="1.5"
                    stroke={
                      display === "center"
                        ? defaultBrandName.theme_color
                        : "#d9d9d9"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_10255_337">
                      <rect width="24" height="24" rx="2" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </DisplayItem>
              <DisplayItem value="right">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_10255_337078)">
                    <path
                      d="M0 2C0 0.89543 0.895431 0 2 0H22C23.1046 0 24 0.895431 24 2V22C24 23.1046 23.1046 24 22 24H2C0.89543 24 0 23.1046 0 22V2Z"
                      fill={
                        display === "right"
                          ? defaultBrandName.theme_color
                          : "#d9d9d9"
                      }
                    />
                    <path
                      d="M12 1H21.1667C22.1792 1 23 1.82081 23 2.83333V21.1667C23 22.1792 22.1792 23 21.1667 23H12V1Z"
                      fill="white"
                    />
                  </g>
                  <defs>
                    <clipPath id="clip0_10255_337078">
                      <rect width="24" height="24" rx="2" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </DisplayItem>
              <DisplayItem value="bottom">
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g clipPath="url(#clip0_10255_337085)">
                    <path
                      d="M0 2C0 0.89543 0.895431 0 2 0H22C23.1046 0 24 0.895431 24 2V22C24 23.1046 23.1046 24 22 24H2C0.89543 24 0 23.1046 0 22V2Z"
                      fill={
                        display === "bottom"
                          ? defaultBrandName.theme_color
                          : "#d9d9d9"
                      }
                    />
                    <path
                      d="M0 12H24V22C24 23.1046 23.1046 24 22 24H2C0.89543 24 0 23.1046 0 22V12Z"
                      fill="white"
                    />
                  </g>
                  <rect
                    x="0.5"
                    y="0.5"
                    width="23"
                    height="23"
                    rx="1.5"
                    stroke={
                      display === "bottom"
                        ? defaultBrandName.theme_color
                        : "#d9d9d9"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_10255_337085">
                      <rect width="24" height="24" rx="2" fill="white" />
                    </clipPath>
                  </defs>
                </svg>
              </DisplayItem>
            </Radio.Group>
          </Form.Item>
          <BackGroundImage>
            <label>{t("form.image")}</label>
            {background && (
              <ImageWrap>
                <Imagee frameSize={frameSize}>
                  {background && <img src={background} alt="" />}

                  <Draggable
                    position={positionDraggable}
                    bounds="parent"
                    onDrag={(e, d) => {
                      // let temp = { x: d.x, y: d.y };
                      // setPositionDraggable(temp);
                      const bgPosition = `${
                        (d.x / (frameSize?.width - modalSize?.width)) * 100 || 0
                      }% ${
                        (d.y / (frameSize?.height - modalSize?.height)) * 100 ||
                        0
                      }%`;
                      setPositionImage(bgPosition);
                    }}
                    onDragStop={(e, d) => {
                      const bgPosition = `${
                        (d.x / (frameSize?.width - modalSize?.width)) * 100 || 0
                      }% ${
                        (d.y / (frameSize?.height - modalSize?.height)) * 100 ||
                        0
                      }%`;
                      setPositionImage(bgPosition);
                    }}
                  >
                    <MiniImage
                      modalSize={modalSize}
                      positionImage={positionImage}
                    >
                      <img draggable="false" src={background} alt="" />
                      <WrapSize option={optionDisplay === "desktop"}>
                        <span>
                          {form.getFieldValue("desktop_width")}x
                          {form.getFieldValue("desktop_height")}
                        </span>
                        <span>
                          {form.getFieldValue("mobile_width")}x
                          {form.getFieldValue("mobile_height")}
                        </span>
                      </WrapSize>
                    </MiniImage>
                  </Draggable>
                  <DarkTheme />
                </Imagee>
              </ImageWrap>
            )}
          </BackGroundImage>
          <SettingDisplay>
            <Row gutter={[12, 0]}>
              <Col span={2}>
                <IconWrap
                  onClick={() => setOptionDisplay("desktop")}
                  isActive={optionDisplay === "desktop" ? true : false}
                >
                  <svg
                    width="24"
                    height="20"
                    viewBox="0 0 24 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M23.1429 0.0390625H0.857143C0.383036 0.0390625 0 0.422098 0 0.896205V14.1819C0 14.656 0.383036 15.0391 0.857143 15.0391H11.0357V18.0391H6.42857C6.19286 18.0391 6 18.2319 6 18.4676V19.7533C6 19.8712 6.09643 19.9676 6.21429 19.9676H17.7857C17.9036 19.9676 18 19.8712 18 19.7533V18.4676C18 18.2319 17.8071 18.0391 17.5714 18.0391H12.9643V15.0391H23.1429C23.617 15.0391 24 14.656 24 14.1819V0.896205C24 0.422098 23.617 0.0390625 23.1429 0.0390625ZM22.0714 13.1105H1.92857V1.96763H22.0714V13.1105Z"
                      fill={optionDisplay === "desktop" ? "#fff" : "#6B6B6B"}
                    />
                  </svg>
                </IconWrap>
              </Col>
              <Col span={10}>
                <Form.Item
                  label="Width"
                  name="desktop_width"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                    min={200}
                  />
                </Form.Item>
              </Col>
              <Col span={10}>
                <Form.Item
                  label="Height"
                  name="desktop_height"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                    min={200}
                  />
                </Form.Item>
              </Col>
              <Col span={2}>
                <IconWrapExtend>
                  <span>px</span>
                </IconWrapExtend>
              </Col>
              <Col span={2}></Col>
              <Col span={20}>
                <SpacingWrap>
                  <Row gutter={[0, 0]}>
                    <Col span={5}>
                      <span>Spacing</span>
                    </Col>
                    <Col span={14}>
                      <Form.Item
                        name="desktop_top"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}></Col>
                    <Col span={5}>
                      <Form.Item
                        name="desktop_left"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={14}>
                      <Center />
                    </Col>
                    <Col span={5}>
                      <Form.Item
                        name="desktop_right"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <span></span>
                    </Col>
                    <Col span={14}>
                      <Form.Item
                        name="desktop_bottom"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}></Col>
                  </Row>
                </SpacingWrap>
              </Col>
              <Col span={2}></Col>
            </Row>
            <Row gutter={[12, 0]}>
              <Col span={2}>
                <IconWrap
                  onClick={() => setOptionDisplay("mobile")}
                  isActive={optionDisplay === "mobile" ? true : false}
                >
                  <svg
                    width="16"
                    height="24"
                    viewBox="0 0 16 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M14.2137 -0.0585938H1.78509C0.839551 -0.0585938 0.0708008 0.710156 0.0708008 1.65569V22.2271C0.0708008 23.1727 0.839551 23.9414 1.78509 23.9414H14.2137C15.1592 23.9414 15.9279 23.1727 15.9279 22.2271V1.65569C15.9279 0.710156 15.1592 -0.0585938 14.2137 -0.0585938ZM13.9994 22.0128H1.99937V1.86998H13.9994V22.0128ZM6.92794 19.2807C6.92794 19.5649 7.04083 19.8374 7.24176 20.0383C7.44269 20.2392 7.71521 20.3521 7.99937 20.3521C8.28353 20.3521 8.55606 20.2392 8.75699 20.0383C8.95792 19.8374 9.0708 19.5649 9.0708 19.2807C9.0708 18.9965 8.95792 18.724 8.75699 18.5231C8.55606 18.3221 8.28353 18.2093 7.99937 18.2093C7.71521 18.2093 7.44269 18.3221 7.24176 18.5231C7.04083 18.724 6.92794 18.9965 6.92794 19.2807Z"
                      fill={optionDisplay === "mobile" ? "#fff" : "#6B6B6B"}
                    />
                  </svg>
                </IconWrap>
              </Col>
              <Col span={10}>
                <Form.Item
                  label="Width"
                  name="mobile_width"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                    min={200}
                  />
                </Form.Item>
              </Col>
              <Col span={10}>
                <Form.Item
                  label="Height"
                  name="mobile_height"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <InputNumber
                    placeholder={t("common.placeholderInput")}
                    parser={(value) => parseInt(value)}
                    min={200}
                  />
                </Form.Item>
              </Col>
              <Col span={2}>
                <IconWrapExtend>
                  <span>px</span>
                </IconWrapExtend>
              </Col>
              <Col span={2}></Col>
              <Col span={20}>
                <SpacingWrap>
                  <Row gutter={[0, 0]}>
                    <Col span={5}>
                      <span>Spacing</span>
                    </Col>
                    <Col span={14}>
                      <Form.Item
                        name="mobile_top"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}></Col>
                    <Col span={5}>
                      <Form.Item
                        name="mobile_left"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={14}>
                      <Center />
                    </Col>
                    <Col span={5}>
                      <Form.Item
                        name="mobile_right"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <span></span>
                    </Col>
                    <Col span={14}>
                      <Form.Item
                        name="mobile_bottom"
                        rules={[
                          { required: true, message: t("form.required") },
                        ]}
                      >
                        <InputNumber
                          parser={(value) => parseInt(value)}
                          min={0}
                          placeholder={t("common.placeholderInput")}
                        />
                      </Form.Item>
                    </Col>
                    <Col span={5}></Col>
                  </Row>
                </SpacingWrap>
              </Col>
              <Col span={2}></Col>
            </Row>
            <CheckboxWwrap>
              <Form.Item name="dark_screen" valuePropName="checked">
                <Checkbox>{t("form.darkScreen")}</Checkbox>
              </Form.Item>
              <Form.Item name="rounded" valuePropName="checked">
                <Checkbox>{t("form.rounded")}</Checkbox>
              </Form.Item>
            </CheckboxWwrap>
          </SettingDisplay>

          <DisplayWrap>
            <label>{t("form.displayPosition")}</label>
            <DisplayPosition>{renderPosition(listPosition)}</DisplayPosition>
          </DisplayWrap>

          <DisplayWrap>
            <label>{t("form.define")}</label>
            <SettingDisplay>
              <label>{t("form.fontSize")}</label>
              <FontWrap>
                <Font>
                  <span>{t("form.onDesktop")}</span>
                  <Form.Item
                    name="font_desktop"
                    rules={[{ required: true, message: t("form.required") }]}
                  >
                    <InputNumber
                      min={8}
                      parser={(value) => parseInt(value)}
                      placeholder={t("common.placeholderInput")}
                    />
                  </Form.Item>
                </Font>
                <Font>
                  <span>{t("form.onMobile")}</span>
                  <Form.Item
                    name="font_mobile"
                    rules={[{ required: true, message: t("form.required") }]}
                  >
                    <InputNumber
                      min={8}
                      parser={(value) => parseInt(value)}
                      placeholder={t("common.placeholderInput")}
                    />
                  </Form.Item>
                </Font>
              </FontWrap>
              <StyleWrap>
                <Style>
                  <label>{t("form.fontStyle")}</label>
                  <Form.Item name="font_style">
                    <Select>
                      <Option value="normal">{t("form.normal")}</Option>
                      <Option value="italic">{t("form.italic")}</Option>
                      <Option value="bold">{t("form.bold")}</Option>
                    </Select>
                  </Form.Item>
                </Style>
                <Style>
                  <label>{t("form.fontColor")}</label>
                  <Value>
                    <Dropdown
                      open={visible}
                      placement="bottom"
                      overlay={renderColor()}
                      trigger="click"
                    >
                      <WrapColor onClick={() => setVisible(true)}>
                        <Color color={fontColor} />
                      </WrapColor>
                    </Dropdown>
                  </Value>
                </Style>
              </StyleWrap>
            </SettingDisplay>
          </DisplayWrap>
        </Form>
      </SettingWrap>
      <ViewWrap>
        <HeaderWrap>
          <ButtonOption>
            <Button
              isActive={optionDisplay === "desktop"}
              onClick={() => setOptionDisplay("desktop")}
            >
              <svg
                width="24"
                height="20"
                viewBox="0 0 24 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M23.1429 0.0390625H0.857143C0.383036 0.0390625 0 0.422098 0 0.896205V14.1819C0 14.656 0.383036 15.0391 0.857143 15.0391H11.0357V18.0391H6.42857C6.19286 18.0391 6 18.2319 6 18.4676V19.7533C6 19.8712 6.09643 19.9676 6.21429 19.9676H17.7857C17.9036 19.9676 18 19.8712 18 19.7533V18.4676C18 18.2319 17.8071 18.0391 17.5714 18.0391H12.9643V15.0391H23.1429C23.617 15.0391 24 14.656 24 14.1819V0.896205C24 0.422098 23.617 0.0390625 23.1429 0.0390625ZM22.0714 13.1105H1.92857V1.96763H22.0714V13.1105Z"
                  fill={optionDisplay === "desktop" ? "#fff" : "#6B6B6B"}
                />
              </svg>
              <span>{t("form.desktop")}</span>
            </Button>
            <Button
              isActive={optionDisplay === "mobile"}
              onClick={() => setOptionDisplay("mobile")}
            >
              <svg
                width="16"
                height="24"
                viewBox="0 0 16 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M14.2137 -0.0585938H1.78509C0.839551 -0.0585938 0.0708008 0.710156 0.0708008 1.65569V22.2271C0.0708008 23.1727 0.839551 23.9414 1.78509 23.9414H14.2137C15.1592 23.9414 15.9279 23.1727 15.9279 22.2271V1.65569C15.9279 0.710156 15.1592 -0.0585938 14.2137 -0.0585938ZM13.9994 22.0128H1.99937V1.86998H13.9994V22.0128ZM6.92794 19.2807C6.92794 19.5649 7.04083 19.8374 7.24176 20.0383C7.44269 20.2392 7.71521 20.3521 7.99937 20.3521C8.28353 20.3521 8.55606 20.2392 8.75699 20.0383C8.95792 19.8374 9.0708 19.5649 9.0708 19.2807C9.0708 18.9965 8.95792 18.724 8.75699 18.5231C8.55606 18.3221 8.28353 18.2093 7.99937 18.2093C7.71521 18.2093 7.44269 18.3221 7.24176 18.5231C7.04083 18.724 6.92794 18.9965 6.92794 19.2807Z"
                  fill={optionDisplay === "mobile" ? "#fff" : "#6B6B6B"}
                />
              </svg>
              <span>{t("form.mobile")}</span>
            </Button>
          </ButtonOption>
          <ButtonAction>
            <Upload
              accept=".jpg, .png, .gif, .jfif, .jpeg"
              action={BASE_URL_API + "upload-file"}
              method={"post"}
              headers={{
                Authorization: localStorage.getItem("setting_accessToken"),
              }}
              data={{
                obj: objectId,
              }}
              onChange={(info) => {
                const status = info.file.status;
                if (status === "done") {
                  setBackground(`${BE_URL}${info.file.response.data[0]}`);
                  // setPositionDraggable({ x: 0, y: 0 });
                } else if (status === "error") {
                  Notification("error", "Upload file error!");
                }
              }}
              showUploadList={false}
              progress={{
                strokeColor: {
                  "0%": "#108ee9",
                  "100%": "#87d068",
                },
                strokeWidth: 3,
                format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
              }}
            >
              <ButtonBackground>
                <CameraOutlined />
                <span>{t("form.setBackground")}</span>
              </ButtonBackground>
            </Upload>
            <ButtonZoom>
              <InputNumber
                placeholder={t("common.placeholderInput")}
                min={50}
                max={100}
                value={zoom}
                formatter={(value) => `${value}%`}
                onChange={(value) => {
                  setZoom(value);
                }}
              />
              <MinusOutlined onClick={() => handleZoom("minus")} />
              <PlusOutlined onClick={() => handleZoom("plus")} />
              <ButtonReset onClick={() => setZoom(100)}>
                {t("form.reset")}
              </ButtonReset>
            </ButtonZoom>
            <ButtonPreview
              onClick={() => {
                let temp = {
                  optionDisplay: optionDisplay,
                  background: background,
                  position: position,
                  objectStyle: form.getFieldsValue(),
                  positionImage: positionImage,
                  listFieldMapping: listFieldMapping,
                  fontColor: fontColor,
                  listMappingGrading: listMappingGrading,
                  listOrdered: listOrdered,
                };
                localStorage.setItem("formSetting", JSON.stringify(temp));
                // setTimeout(() => {
                window.open(`${FE_URL}/preview`);
                // }, 500);
              }}
            >
              <EyeOutlined />
              <span>{t("form.preview")}</span>
            </ButtonPreview>
          </ButtonAction>
        </HeaderWrap>
        <BackgroundWrap
          dark_screen={form.getFieldValue("dark_screen")}
          scale={zoom / 100}
        >
          <FormView
            optionDisplay={optionDisplay}
            background={background}
            position={position}
            objectStyle={form.getFieldsValue()}
            positionImage={positionImage}
            listFieldMapping={listFieldMapping}
            fontColor={fontColor}
            listMappingGrading={listMappingGrading}
            listOrdered={listOrdered}
          />
        </BackgroundWrap>
        <ButtonWrap>
          <CustomButton onClick={() => navigate("/form-setting")}>
            {t("common.cancel")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 0 ? "none" : "flex" }}
            onClick={() => setStep(step - 1)}
          >
            {t("common.back")}
          </CustomButton>
          <CustomButton
            style={{ display: step === 1 ? "none" : "flex" }}
            onClick={() => {
              if (checkDynamic()) {
                Notification("error", checkDynamic());
              } else if (
                checkMappingGrading() &&
                form.getFieldValue("check_grading")
              ) {
                Notification("warning", "Please fullfill mapping");
              } else if (checkSection()) {
                Notification("warning", "Section is empty");
              } else if (listOrdered.length === 0) {
                Notification("warning", "You need add section");
              } else {
                form.submit();
              }
            }}
          >
            {t("common.next")}
          </CustomButton>
        </ButtonWrap>
      </ViewWrap>
      <ModalSection
        listFieldMapping={listFieldMapping}
        listOrdered={listOrdered}
      />
    </WrapContent>
  );
};

export default DesignUI;
const WrapContent = styled.div`
  width: 100%;
  display: ${(props) => (props.step === 0 ? "flex" : "none")};
  background: #fff;
  color: #2c2c2c;
  font-size: 16px;
  line-height: 22px;
  label {
    font-size: 16px;
    line-height: 22px;
    font-family: var(--roboto-500);
    height: unset;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-input-number:hover,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  .ant-input-number-handler-wrap {
    display: none;
  }
  .ant-input-number {
    width: 100%;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const SettingWrap = styled.div`
  width: 380px;
  padding-right: 16px;
  overflow-y: auto;
  overflow-x: hidden;
  height: calc(100vh - 200px);
  label {
    ::before {
      display: none !important;
    }
  }
`;

const ViewWrap = styled.div`
  flex: 1;
  background: #f0f0f0;
  border-radius: 5px;
  padding: 16px;
`;

const DisplayItem = styled(Radio.Button)``;

const SettingDisplay = styled.div`
  background: #f0f0f0;
  border-radius: 5px;
  padding: 16px;
  margin-top: 8px;
`;

const SpacingWrap = styled.div`
  background: #fff;
  padding: 8px;
  border-radius: 5px;
  margin-bottom: 16px;
  .ant-form-item {
    margin-bottom: 0px !important;
    input {
      border: none;
      text-align: center;
      padding: 4px 6px;
    }
  }
  span {
    font-size: 12px;
    line-height: 14px;
    color: #6b6b6b;
  }
  .ant-input-number {
    border: none;
  }
`;

const FieldWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
  span {
    font-size: 16px;
    line-height: 22px;
    font-family: var(--roboto-500);
    flex: 1;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
  }
  margin-bottom: 16px;
`;

const Center = styled.div`
  background: #f0f0f0;
  width: 100%;
  height: 100%;
`;

const IconWrap = styled.div`
  cursor: pointer;
  position: absolute;
  bottom: 16px;
  width: 34px;
  right: -2px;
  height: 34px;
  background: ${(props) => (props.isActive ? props.theme.main : "#fff")};
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
`;

const IconWrapExtend = styled(IconWrap)`
  background: transparent;
  cursor: unset;
`;
const DisplayWrap = styled.div`
  width: 100%;
  margin-top: 16px;
`;

const DisplayPosition = styled.div`
  background: #f5f5f5;
  border-radius: 5px;
  width: 214px;
  padding: 8px;
  margin-top: 8px;
`;

const ItemWrap = styled.div`
  width: 100%;
  display: flex;
  gap: 8px;
  margin: ${(props) => (props.isCenter ? "8px 0" : "0px")};
`;

const Item = styled.div`
  padding: 4px;
  background: ${(props) => (props.isActive ? "#595959" : "#d9d9d9")};
  border-radius: 5px;
  flex-grow: 2;
  text-align: center;
  height: ${(props) => (props.height ? `${props.height}px` : "44px")};
  cursor: pointer;
  :nth-child(2) {
    flex-grow: 4;
  }
`;

const HeaderWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const ButtonOption = styled.div`
  display: flex;
`;

const Button = styled.div`
  background: ${(props) => (props.isActive ? props.theme.main : "#fff")};
  cursor: pointer;
  width: 136px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  transition: all 0.5s;
  border-radius: 5px 0 0 5px;
  :last-child {
    border-radius: 0px 5px 5px 0px;
  }
  svg {
    margin-right: 8px;
  }
  span {
    font-size: 16px;
    line-height: 24px;
    color: ${(props) => (props.isActive ? "#fff" : "#000")};
  }
`;

const ButtonAction = styled.div`
  display: flex;
`;

const ButtonPreview = styled.div`
  background: #fff;
  width: 117px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${(props) => props.theme.main};
  cursor: pointer;
  border-radius: 5px;
  margin-left: 16px;
  svg {
    margin-right: 8px;
    color: ${(props) => props.theme.main};
  }
  span {
    font-size: 16px;
    color: ${(props) => props.theme.main};
  }
`;

const ButtonZoom = styled.div`
  border-radius: 5px;
  background: #fff;
  display: flex;
  align-items: center;
  padding-right: 16px;
  input {
    text-align: center;
  }
  .ant-input-number {
    border: none;
    width: 70px;
  }
  .ant-input-number:focus,
  .ant-input-number-focused {
    box-shadow: none;
  }
  .ant-input-number-handler-wrap {
    display: none;
  }
  span {
    svg {
      margin: 0 12px 0 4px;
      cursor: pointer;
    }
  }
`;

const ButtonReset = styled.div`
  cursor: pointer;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  line-height: 22px;
  color: #2c2c2c;
  padding: 2px 16px;
`;

const BackgroundWrap = styled.div`
  scale: ${(props) => props.scale};
  width: 100%;
  background: ${(props) => (props.dark_screen ? "#434343" : "#f3f8ff")};
  border-radius: 5px;
  margin: 12px 0px;
  height: calc(100% - 100px);
  position: relative;
  .top_left {
    top: 20px;
    left: 20px;
  }
  .top_center {
    top: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .top_right {
    top: 20px;
    right: 20px;
  }
  .center_left {
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .center_center {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center_right {
    right: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .bottom_left {
    left: 20px;
    bottom: 20px;
  }
  .bottom_center {
    bottom: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .bottom_right {
    right: 20px;
    bottom: 20px;
  }
`;

const ButtonBackground = styled.div`
  background: #fff;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 8px 16px;
  margin-right: 16px;
  cursor: pointer;
  span {
    color: #2c2c2c;
  }
  svg {
    margin-right: 8px;
  }
`;

const CheckboxWwrap = styled.div`
  .ant-form-item {
    margin-bottom: 0px !important;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const ImageWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

const Imagee = styled.div`
  position: relative;
  width: ${(props) => props.frameSize.width}px;
  height: ${(props) => props.frameSize.height}px;
  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`;

const BackGroundImage = styled.div`
  width: 100%;
  margin-bottom: 16px;
`;

const DarkTheme = styled.div`
  background: rgba(0, 0, 0, 0.67);
  inset: 0;
  position: absolute;
`;

const MiniImage = styled.div`
  width: ${(props) => props.modalSize.width}px;
  height: ${(props) => props.modalSize.height}px;
  position: absolute;
  top: 0;
  z-index: 2;
  cursor: move;
  img {
    object-fit: cover;
    width: 100%;
    height: 100%;
    object-position: ${(props) => props.positionImage};
  }
`;

const WrapSize = styled.div`
  background: rgba(0, 0, 0, 0.67);
  font-size: 12px;
  color: #fff;
  position: absolute;
  top: 0;
  left: 0;
  z-index: 3;
  padding: 0 4px;
  cursor: move;
  word-break: break-all;
  span {
    :first-child {
      display: ${(props) => (props.option ? "block" : "none")};
    }
    :last-child {
      display: ${(props) => (props.option ? "none" : "block")};
    }
  }
`;
const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  cursor: pointer;
  background: #fff;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }

  :nth-child(3) {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;

const FontWrap = styled.div`
  width: 100%;
  background: #fff;
  padding: 16px;
  display: flex;
  justify-content: space-between;
  border-radius: 2px;
  padding-bottom: 0px;
`;
const Font = styled.div`
  width: 48%;
  span {
    font-size: 16px;
  }
`;

const StyleWrap = styled.div`
  display: flex;
  margin-top: 16px;
`;

const CustomMenu = styled(Menu)`
  padding: 8px;
  width: 258px;
`;

const CustomColor = styled.div`
  height: 24px;
  width: 24px;
  background-color: ${(props) => props.color};
  cursor: pointer;
  &:hover {
    border: 2px solid white;
  }
`;

const Value = styled.div`
  width: 60%;
`;
const WrapColor = styled.div`
  padding: 4px;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  width: 72px;
`;

const Color = styled.div`
  height: 22px;
  background-color: ${(props) => props.color};
  cursor: pointer;

  &:hover {
    border: 2px solid white;
  }
`;

const Style = styled.div`
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  :first-child {
    margin-right: 10px;
    flex: 1;
  }
`;

const CustomItem = styled.div`
  cursor: pointer;
  :hover {
    background: #f5f5f5;
  }
  padding: 4px 10px;
  font-size: 15px;
  transition: all 0.5s;
`;

const ButtonAdd = styled.div`
  cursor: pointer;
  background: ${(props) => props.theme.main};
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 10px;
  border-radius: 2px;
  svg {
    margin-left: 4px;
    font-size: 14px;
  }
`;
