import React, { useCallback, useMemo } from "react";
import { Checkbox, Form, Input, InputNumber, Select } from "antd";
import { useTranslation } from "react-i18next";
import styled from "styled-components";
import Delete from "assets/icons/common/delete.svg";
import Compare from "assets/icons/common/Compare.png";
import _ from "lodash";
const { Option } = Select;
const Grading = ({
  listFieldMapping,
  form,
  fields,
  optionsGrading,
  setOptionsGrading,
  listMappingGrading,
  setListMappingGrading,
}) => {
  const { t } = useTranslation();

  const handleOptionGrading = (id) => {
    fields.forEach((field) => {
      if (field.ID === form.getFieldValue("field_grading")) {
        if (field.option) {
          setOptionsGrading(field.option);
        } else {
          setOptionsGrading([]);
        }
      }
    });
  };
  const handleDisabled = (list, id) => {
    let flag = false;
    list.forEach((item) => {
      if (item.id === id) {
        flag = true;
      }
    });
    return flag;
  };

  const renderOptionTotal = (list) =>
    list.map(
      (item, index) =>
        item.type === "number" &&
        item.options_number?.length === 0 && (
          <Option
            disabled={handleDisabled(listFieldMapping, item.ID)}
            value={item.ID}
            key={index}
          >
            {item.name}
          </Option>
        )
    );

  const renderOptionGrading = (list) =>
    list.map(
      (item, index) =>
        (item.type === "text" || item.type === "select") && (
          <Option
            disabled={handleDisabled(listFieldMapping, item.ID)}
            value={item.ID}
            key={index}
          >
            {item.name}
          </Option>
        )
    );

  // const renderOptionIP = (list) =>
  //   list.map(
  //     (item, index) =>
  //       item.type === "text" && (
  //         <Option
  //           disabled={
  //             handleDisabled(listFieldMapping, item.ID) ||
  //             item.ID === form.getFieldValue("field_grading")
  //           }
  //           value={item.ID}
  //           key={index}
  //         >
  //           {item.name}
  //         </Option>
  //       )
  //   );

  const handleChangeMapping = useCallback(
    (key, value, idx) => {
      let tem = [...listMappingGrading];
      tem[idx] = { ...tem[idx], [key]: value };

      setListMappingGrading(tem);
    },
    //eslint-disable-next-line
    [listMappingGrading]
  );

  const addMappingGrading = () => {
    let tem = [...listMappingGrading];
    tem.push({
      grading_value: undefined,
      min_val: undefined,
      max_max: undefined,
    });
    setListMappingGrading(tem);
  };

  const debouncedMappingHandler = useMemo(
    () => _.debounce(handleChangeMapping, 50),
    [handleChangeMapping]
  );
  const handleDeleteMapping = (idx) => {
    let tem = listMappingGrading.filter((item, index) => index !== idx);
    setListMappingGrading(tem);
  };
  const renderMappingGrading = (list) =>
    list.map((item, index) => (
      <BlackBox key={index}>
        <GradingWrap isDelete={listMappingGrading.length > 1}>
          {optionsGrading.length > 0 ? (
            <Select
              value={item.grading_value}
              placeholder={t("common.placeholderSelect")}
              onChange={(value) =>
                handleChangeMapping("grading_value", value, index)
              }
            >
              {optionsGrading.map((option, index) => (
                <Option key={index} value={option.value}>
                  {option.label}
                </Option>
              ))}
            </Select>
          ) : (
            <Input
              value={item.grading_value}
              onChange={(e) =>
                handleChangeMapping("grading_value", e.target.value, index)
              }
              placeholder={t("common.placeholderInput")}
            />
          )}
          <RangeGrading>
            <InputNumber
              max={item.max_max ? item.max_max - 1 : undefined}
              onChange={(value) =>
                debouncedMappingHandler("min_val", value, index)
              }
              value={item.min_val}
              placeholder={t("common.placeholderInput")}
            />
            <img src={Compare} alt="" />
            <span> Grade</span> <img src={Compare} alt="" />
            <InputNumber
              min={item.min_val === 0 || item.min_val ? item.min_val + 1 : 1}
              onChange={(value) =>
                debouncedMappingHandler("max_max", value, index)
              }
              value={item.max_max}
              placeholder={t("common.placeholderInput")}
            />
          </RangeGrading>
        </GradingWrap>
        {listMappingGrading.length > 1 && (
          <img
            src={Delete}
            alt="delete"
            onClick={() => handleDeleteMapping(index)}
          />
        )}
      </BlackBox>
    ));
  const handleResetGrading = () => {
    setOptionsGrading([]);
    form.setFieldsValue({
      field_grading: undefined,
      default_grading_value: undefined,
    });
    setListMappingGrading([
      {
        grading_value: undefined,
        min_val: undefined,
        max_max: undefined,
      },
    ]);
  };
  return (
    <div>
      {/* <Form.Item label="Field IP" name="field_ip">
        <Select allowClear placeholder={t("common.placeholderSelect")}>
          {renderOptionIP(fields)}
        </Select>
      </Form.Item> */}
      <Form.Item label="Scoring" name="scoring_option">
        <Select
          placeholder={t("common.placeholderSelect")}
          onChange={() => {
            form.setFieldsValue({ check_grading: false });
            handleResetGrading();
          }}
        >
          <Option value="no_scoring">No scoring</Option>
          <Option value="at_the_end">Scoring at the end</Option>
        </Select>
      </Form.Item>

      {form.getFieldValue("scoring_option") !== "no_scoring" && (
        <ScoringWrap>
          <WrapPoint>
            <Form.Item
              name="min_point"
              label="Min point"
              rules={[
                { required: true, message: t("form.required") },
                () => ({
                  validator: (_, value) => {
                    if (
                      (value || value === 0) &&
                      value >= form.getFieldValue("max_point")
                    ) {
                      return Promise.reject(
                        new Error("The min point must lower max point!")
                      );
                    } else {
                      return Promise.resolve();
                    }
                  },
                }),
              ]}
            >
              <InputNumber
                parser={(value) => parseInt(value)}
                placeholder={t("common.placeholderInput")}
              />
            </Form.Item>
            <Form.Item
              name="max_point"
              label="Max point"
              rules={[
                { required: true, message: t("form.required") },
                () => ({
                  validator: (_, value) => {
                    if (
                      (value || value === 0) &&
                      value <= form.getFieldValue("min_point")
                    ) {
                      return Promise.reject(
                        new Error("The max point must higher min point!")
                      );
                    } else {
                      return Promise.resolve();
                    }
                  },
                }),
              ]}
            >
              <InputNumber
                parser={(value) => parseInt(value)}
                placeholder={t("common.placeholderInput")}
              />
            </Form.Item>
          </WrapPoint>
          <Form.Item
            label="Total score return field"
            name="field_total"
            rules={[{ required: true, message: t("form.required") }]}
          >
            <Select placeholder={t("common.placeholderSelect")} allowClear>
              {renderOptionTotal(fields)}
            </Select>
          </Form.Item>
          <Form.Item name="check_grading" valuePropName="checked">
            <Checkbox onChange={() => handleResetGrading()}>
              Check grading
            </Checkbox>
          </Form.Item>
          {form.getFieldValue("check_grading") && (
            <WrapGrading>
              <WhiteBox style={{ marginBottom: "16px", paddingBottom: "1px" }}>
                <Form.Item
                  label="Field grading"
                  name="field_grading"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  <Select
                    onChange={(value) => {
                      handleOptionGrading(value);
                      form.setFieldsValue({ default_grading_value: undefined });
                      setListMappingGrading([
                        {
                          value: undefined,
                          min: undefined,
                          max: undefined,
                        },
                      ]);
                    }}
                    placeholder={t("common.placeholderSelect")}
                  >
                    {renderOptionGrading(fields)}
                  </Select>
                </Form.Item>
              </WhiteBox>

              <WhiteBox>
                <Form.Item
                  label="Default value"
                  name="default_grading_value"
                  rules={[{ required: true, message: t("form.required") }]}
                >
                  {optionsGrading.length > 0 ? (
                    <Select placeholder={t("common.placeholderSelect")}>
                      {optionsGrading.map((option, index) => (
                        <Option key={index} value={option.value}>
                          {option.label}
                        </Option>
                      ))}
                    </Select>
                  ) : (
                    <Input placeholder={t("common.placeholderInput")} />
                  )}
                </Form.Item>
                <label>Mapping value grading</label>
                {renderMappingGrading(listMappingGrading)}
                <ButtonAdd onClick={() => addMappingGrading()}>
                  + Add range
                </ButtonAdd>
              </WhiteBox>
            </WrapGrading>
          )}
        </ScoringWrap>
      )}
    </div>
  );
};

export default Grading;
const ScoringWrap = styled.div``;

const WrapGrading = styled.div`
  width: 100%;
  background: #f0f0f0;
  border-radius: 5px;
  padding: 16px;
  margin-bottom: 16px;
`;

const WhiteBox = styled.div`
  background: #fff;
  padding: 16px;
`;

const BlackBox = styled.div`
  background: #f0f0f0;
  border-radius: 2px;
  padding: 16px;
  margin-top: 16px;
  display: flex;
  align-items: start;
  .ant-select {
    width: 100%;
  }
  img {
    margin-left: 12px;
    margin-top: 6px;
    width: 22px;
    cursor: pointer;
  }
`;

const WrapPoint = styled.div`
  display: flex;
  justify-content: space-between;

  .ant-form-item {
    width: 48%;
  }
`;

const GradingWrap = styled.div`
  width: ${(props) => (props.isDelete ? "calc(100% - 32px)" : "100%")};
`;

const RangeGrading = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;
  span {
    font-size: 16px;
    color: rgba(0, 0, 0, 0.85);
  }
  img {
    margin: 0 6px;
  }
`;

const ButtonAdd = styled.div`
  color: ${(props) => props.theme.main};
  font-size: 16px;
  cursor: pointer;
  margin-top: 8px;
  width: fit-content;
`;
