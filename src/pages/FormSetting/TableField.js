import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { Tooltip, Typography } from "antd";
import { useTranslation } from "react-i18next";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import { useDispatch } from "react-redux";
import {
  deleteFieldMapping,
  updateFieldMapping,
  updateInfoField,
} from "redux/slices/formSetting";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";

const { Text: TextComponent } = Typography;

const TableField = ({ listUpdate, fields, setOpen }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const [dataDelete, setDataDelete] = useState({});

  useEffect(() => {
    if (!showConfirmDelete) {
      setDataDelete({});
    }
  }, [showConfirmDelete]);

  const handleFieldName = (id) => {
    let name = "";
    fields.forEach((field) => {
      if (field.ID === id) {
        name = field.name;
      }
    });
    return name;
  };

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }

    const newList = reorder(
      listUpdate,
      result.source.index,
      result.destination.index
    );

    let temp = [];
    newList.forEach((item, index) => {
      temp.push({
        ...item,
        position: index,
      });
    });
    dispatch(updateFieldMapping(temp));
  };

  return (
    <TableWrap>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <CustomWrap {...provided.droppableProps} ref={provided.innerRef}>
              <Table>
                <RowWrap>
                  <Col isHeader>
                    <TextComponent>{t("form.label")}</TextComponent>
                  </Col>
                  <Col isHeader>
                    <TextComponent>{t("form.fieldMapping")}</TextComponent>
                  </Col>
                  <Col isHeader>
                    <TextComponent>{t("common.action")}</TextComponent>
                  </Col>
                </RowWrap>
                {listUpdate.map((item, index) => (
                  <Draggable key={item.id} draggableId={item.id} index={index}>
                    {(provided, snapshot) => (
                      <RowWrap
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <Col>
                          <TextComponent ellipsis={{ tooltip: item.name }}>
                            {item.name}
                          </TextComponent>
                        </Col>
                        <Col>
                          <TextComponent
                            ellipsis={{
                              tooltip: handleFieldName(item.id),
                            }}
                          >
                            {item.description
                              ? item.description
                              : handleFieldName(item.id)}
                          </TextComponent>
                        </Col>
                        <Col>
                          <WrapAction>
                            <Tooltip
                              onClick={() => {
                                dispatch(updateInfoField(item));
                                setOpen(
                                  item.field_type ? "field" : "description"
                                );
                              }}
                              title={t("common.edit")}
                            >
                              <img src={Edit} alt="edit" />
                            </Tooltip>

                            <Tooltip title={t("common.delete")}>
                              <img
                                onClick={() => {
                                  setDataDelete(item);
                                  setShowConfirmDelete(true);
                                }}
                                src={Delete}
                                alt="delete"
                              />
                            </Tooltip>
                          </WrapAction>
                        </Col>
                      </RowWrap>
                    )}
                  </Draggable>
                ))}
              </Table>
              {provided.placeholder}
            </CustomWrap>
          )}
        </Droppable>
      </DragDropContext>
      <ModalConfirmDelete
        title={t("form.thisField")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteFieldMapping}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={showConfirmDelete}
        setOpenConfirm={setShowConfirmDelete}
      />
    </TableWrap>
  );
};

export default TableField;
const TableWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  margin-bottom: 16px;
  &::-webkit-scrollbar {
    height: 8px !important;
  }
`;

const CustomWrap = styled.div`
  background-color: #fff;
  width: 100%;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    width: 20px;
    :first-child {
      margin-right: 10px;
    }
  }
`;

const Table = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
`;

const RowWrap = styled.div`
  display: flex;
  width: 100%;
  flex-wrap: wrap;
  border: 1px solid #d9d9d9;
  margin-top: -1px;
`;

const Col = styled.div`
  width: calc((100% - 80px) / 2);
  border-right: 1px solid #d9d9d9;
  padding: 4px 8px;
  font-size: ${(props) => (props.isHeader ? "16px" : "14px")};
  font-family: ${(props) =>
    props.isHeader ? "var(--roboto-500)" : "var(--roboto-400)"};
  background: ${(props) => (props.isHeader ? "#fafafa" : "unset")};
  line-height: 22px;
  :last-child {
    width: 80px;
    border: none;
  }
`;
