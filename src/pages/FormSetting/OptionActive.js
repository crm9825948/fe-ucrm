import { Form, Input, Radio, Space, Switch, InputNumber } from "antd";
import React from "react";
import { useTranslation } from "react-i18next";
import styled from "styled-components";

const OptionActive = ({
  form,
  listOption,
  name,
  activeName,
  optionName,
  customOption,
}) => {
  const { t } = useTranslation();

  const renderOption = (list) => {
    return list.map((option, index) => (
      <Radio key={index} value={option.value}>
        <CustomText>{t(option.label)}</CustomText>
      </Radio>
    ));
  };
  return (
    <Wrap>
      <SwitchWrap>
        <Form.Item name={activeName} valuePropName="checked">
          <Switch
            checkedChildren={t("knowledgeBase.on")}
            unCheckedChildren={t("knowledgeBase.off")}
          />
        </Form.Item>
        <CustomLabel>{t(name)}</CustomLabel>
      </SwitchWrap>

      {form[activeName] && (
        <>
          {activeName === "is_click" ? (
            <>
              <Form.Item
                name={optionName}
                rules={[
                  { required: true, message: t("knowledgeBase.required") },
                ]}
              >
                <Input placeholder="Css selector : #id1,#id2" />
              </Form.Item>
            </>
          ) : (
            <Form.Item name={optionName}>
              <Radio.Group>
                <Space direction="vertical">{renderOption(listOption)}</Space>
              </Radio.Group>
            </Form.Item>
          )}
        </>
      )}
      {form[optionName] === "custom" && form[activeName] && (
        <Form.Item
          name={customOption}
          rules={[{ required: true, message: t("knowledgeBase.required") }]}
        >
          <InputNumber
            placeholder={t("common.placeholderInput")}
            parser={(value) => parseInt(value)}
            min={1}
          />
        </Form.Item>
      )}
    </Wrap>
  );
};
const Wrap = styled.div`
  width: 100%;
  color: #2c2c2c;
  line-height: 22px;
  font-size: 16px;
  padding: 16px 32px;
  border: 1px solid #f4f4f4;
  border-radius: 2px;
  margin-top: 16px;
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
  .ant-switch {
    width: 54px;
  }
  .ant-space {
    gap: 4px !important;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-input-number:hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  input,
  .ant-input-number {
    width: 160px;
  }
  .ant-input-number,
  .ant-input {
    margin-top: 8px;
  }
  .ant-input-number-handler-wrap {
    display: none;
  }
  .ant-form-item {
    margin-bottom: 0;
  }
`;
const CustomLabel = styled.span`
  margin-left: 16px;
  font-size: 16px;
  font-family: var(--roboto-500);
`;
const SwitchWrap = styled.div`
  display: flex;
  align-items: center;
`;
const CustomText = styled.div`
  font-size: 16px;
`;

export default OptionActive;
