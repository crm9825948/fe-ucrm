import React from "react";
import styled from "styled-components";
import { Form, InputNumber } from "antd";
import { useTranslation } from "react-i18next";

const HiddenSetting = ({ exampleHidden }) => {
  const { t } = useTranslation();

  return (
    <Wrap>
      <Form.Item
        label={t("form.hiddenLength")}
        name="input_hidden_length"
        rules={[{ required: true, message: t("form.required") }]}
      >
        <InputNumber
          min={3}
          max={20}
          parser={(value) => parseInt(value)}
          placeholder={t("common.placeholderInput")}
        />
      </Form.Item>
      <WrapHidden>
        <Form.Item name="input_hidden_left" label={t("form.hiddenLeft")}>
          <InputNumber
            min={0}
            parser={(value) => parseInt(value)}
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>
        <Form.Item name="input_hidden_right" label={t("form.hiddenRight")}>
          <InputNumber
            min={0}
            parser={(value) => parseInt(value)}
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>
        <Form.Item name="input_hidden_start" label={t("form.hiddenStart")}>
          <InputNumber
            min={0}
            parser={(value) => parseInt(value)}
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>
        <Form.Item name="input_hidden_end" label={t("form.hiddenEnd")}>
          <InputNumber
            min={0}
            parser={(value) => parseInt(value)}
            placeholder={t("common.placeholderInput")}
          />
        </Form.Item>
      </WrapHidden>
      <p>
        <span>Example </span>: {exampleHidden}
      </p>
    </Wrap>
  );
};

export default HiddenSetting;

const Wrap = styled.div`
  width: 100%;
  p {
    span {
      font-family: var(--roboto-500);
    }
  }
`;

const WrapHidden = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  .ant-form-item {
    width: 48%;
  }
`;
