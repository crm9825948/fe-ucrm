import React, { useCallback, useMemo, useState, useEffect } from "react";
import styled from "styled-components/macro";
import { Button, Form } from "antd";
import FormContent from "./FormContent/FormContent";
import _ from "lodash";
import { t } from "i18next";
import moment from "moment";

const FormView = ({
  objectStyle,
  position,
  background,
  optionDisplay,
  positionImage,
  listFieldMapping,
  fontColor,
  listMappingGrading,
  listOrdered,
}) => {
  const [form] = Form.useForm();
  //eslint-disable-next-line
  const [change, setChange] = useState("");
  const [page, setPage] = useState(0);
  const [history, setHistory] = useState([listOrdered[0]]);
  const [dataSubmit, setDataSubmit] = useState({});
  const [formPoint, setFormPoint] = useState(0);
  const [conditionPagination, setConditionPagination] = useState("");

  useEffect(() => {
    listFieldMapping.forEach((field) => {
      if (field.default_value !== undefined) {
        if (
          field.field_type === "date" ||
          field.field_type === "datetime-local"
        ) {
          form.setFieldsValue({ [field.id]: moment(field.default_value) });
        } else {
          form.setFieldsValue({ [field.id]: field.default_value });
        }
      }
    });
  }, [listFieldMapping, form]);

  useEffect(() => {
    setPage(0);
    setHistory([listOrdered[0]]);
  }, [listOrdered]);

  const handleResetField = (field) => {
    let tem = "";
    listFieldMapping.forEach((item) => {
      if (item.triggering_field === field) {
        form.setFieldsValue({ [item.id]: undefined });
        dataSubmit[item.id] = undefined;
        tem = item.id;
      } else {
        tem = "";
      }
      if (tem) {
        handleResetField(tem);
      }
    });
  };

  const handleGrading = () => {
    let tem = objectStyle.default_grading_value;
    listMappingGrading.every((mapping) => {
      if (formPoint >= mapping.min_val && formPoint <= mapping.max_max) {
        tem = mapping.grading_value;
        return false;
      }
      return true;
    });
    return tem;
  };

  //eslint-disable-next-line
  const countPoint = () => {
    let point = 0;
    let tem = { ...dataSubmit, ...form.getFieldsValue() };
    if (objectStyle.scoring_option !== "no_scoring") {
      listFieldMapping.forEach((field) => {
        if (field.point_options?.length > 0) {
          Object.entries(tem).forEach((item) => {
            if (
              item[0] === field.id &&
              (history.includes(field.section) || !field.section)
            ) {
              field.point_options?.forEach((option) => {
                if (option.option === item[1]) {
                  point = point + option.point;
                }
              });
              return;
            }
          });
        }
      });
    }
    if (objectStyle.min_point === 0 || objectStyle.min_point) {
      if (point < objectStyle.min_point) {
        point = objectStyle.min_point;
      }
    }
    if (objectStyle.max_point === 0 || objectStyle.max_point) {
      if (point > objectStyle.max_point) {
        point = objectStyle.max_point;
      }
    }
    setFormPoint(point);
  };

  useEffect(() => {
    countPoint();
  }, [countPoint]);

  const checkConditionPagination = useCallback(() => {
    let flag = "";
    Object.entries(form.getFieldsValue()).forEach(([key, value]) => {
      listFieldMapping.forEach((field) => {
        if (
          field.conditional_pagination &&
          field.pagination_option &&
          field.id === key &&
          field.section === listOrdered[page]
        ) {
          field.pagination_option.every((option) => {
            if (option.value.toString() === value?.toString()) {
              flag = option.option;
              return false;
            } else {
              return true;
            }
          });
        }
      });
    });
    setConditionPagination(flag);
  }, [listFieldMapping, listOrdered, page, form]);

  useEffect(() => {
    checkConditionPagination();
  }, [checkConditionPagination]);

  const onChangeHandle = useCallback(
    (searchText) => {
      setChange(searchText);
      countPoint();
      checkConditionPagination();
    },
    [countPoint, checkConditionPagination]
  );

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onChangeHandle, 50),
    [onChangeHandle]
  );

  const checkIndexPage = (name, ordereds = ["Default"]) => {
    let index = ordereds.findIndex((item) => item === name);
    return index;
  };

  const handleNext = (values) => {
    if (
      page === listOrdered.length - 1 ||
      conditionPagination === "submit_form_crm"
    ) {
    } else {
      let newData = { ...dataSubmit, ...values };
      setDataSubmit(newData);
      if (
        conditionPagination &&
        checkIndexPage(conditionPagination, listOrdered) > page
      ) {
        let tem = [...history, conditionPagination];
        setHistory(tem);
        setPage(checkIndexPage(conditionPagination, listOrdered));
      } else {
        setPage((prev) => {
          let tem = [...history, listOrdered[prev + 1]];
          setHistory(tem);
          return prev + 1;
        });
      }
    }
  };

  const handleBack = () => {
    let tem = [...history];
    tem.pop();
    setHistory(tem);
    setPage(checkIndexPage(tem[tem.length - 1], listOrdered));
    setDataSubmit((prev) => ({
      ...prev,
      ...form.getFieldsValue(),
    }));
  };

  return (
    <Wrap
      className={position}
      objectStyle={objectStyle}
      background={background ? background.replaceAll(" ", "%20") : ""}
      optionDisplay={optionDisplay}
      positionImage={positionImage}
      fontColor={fontColor}
    >
      <FromWrap>
        <IntroWrap>
          {objectStyle.form_name && <Name>{objectStyle.form_name}</Name>}
          {/* <Name>{listOrdered[page]}</Name> */}
          {objectStyle.form_description && (
            <Description>{objectStyle.form_description}</Description>
          )}
        </IntroWrap>
        <Form
          onFinish={handleNext}
          form={form}
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          onValuesChange={(value, values) => {
            debouncedSearchHandler(value);
            handleResetField(Object.keys(value)[0]);
          }}
        >
          <FormContent
            fields={listFieldMapping}
            form={form}
            defaultValue={{}}
            listOrdered={listOrdered}
            page={page}
          />
        </Form>
        {objectStyle.scoring_option !== "no_scoring" && (
          <Result>
            <span>Tổng điểm : {formPoint}</span>
          </Result>
        )}
        {objectStyle.check_grading && (
          <Result>
            <span>Xếp hạng : {handleGrading()}</span>
          </Result>
        )}
        <ButtonWrap>
          {page === listOrdered.length - 1 ||
          conditionPagination === "submit_form_crm" ? (
            <CustomButton onClick={() => form.submit()}>
              {objectStyle.form_save}
            </CustomButton>
          ) : (
            <CustomButton onClick={() => form.submit()}>
              {t("form.next")}
            </CustomButton>
          )}

          {page !== 0 && (
            <CustomButton onClick={handleBack}>{t("form.back")}</CustomButton>
          )}
          <CustomButton>{objectStyle.form_cancel}</CustomButton>
        </ButtonWrap>
      </FromWrap>
    </Wrap>
  );
};

export default React.memo(FormView);
FormView.defaultProps = {
  listOrdered: ["Default"],
};

const Wrap = styled.div`
  padding: ${(props) =>
    props.optionDisplay === "desktop"
      ? `${props.objectStyle.desktop_top}px ${props.objectStyle.desktop_right}px ${props.objectStyle.desktop_bottom}px ${props.objectStyle.desktop_left}px`
      : `${props.objectStyle.mobile_top}px ${props.objectStyle.mobile_right}px ${props.objectStyle.mobile_bottom}px ${props.objectStyle.mobile_left}px`};
  width: ${(props) =>
    props.optionDisplay === "desktop"
      ? props.objectStyle.desktop_width
      : props.objectStyle.mobile_width}px;
  height: ${(props) =>
    props.optionDisplay === "desktop"
      ? props.objectStyle.desktop_height
      : props.objectStyle.mobile_height}px;

  label {
    font-size: ${(props) =>
      props.optionDisplay === "desktop"
        ? `${props.objectStyle.font_desktop}px`
        : `${props.objectStyle.font_mobile}px`} !important;
    color: ${(props) => props.fontColor};
    font-family: ${(props) =>
      props.objectStyle.font_style === "normal"
        ? "var(--roboto-400)"
        : props.objectStyle.font_style === "italic"
        ? "var(--roboto-400-i)"
        : "var(--roboto-500)"} !important;
    position: relative;
    white-space: pre-line;
    height: unset;
    /* ::before {
      position: absolute;
      right: -12px;
      top: 0px;
    } */
  }
  background: #fff;
  background-image: ${(props) =>
    props.background ? `url(${props.background})` : ""};
  max-width: 90%;
  max-height: 90%;
  position: absolute;
  border-radius: ${(props) => (props.objectStyle.rounded ? "5px" : "0px")};
  background-size: cover;
  background-repeat: no-repeat;
  background-position: ${(props) => props.positionImage};
  overflow: auto;
  z-index: 100;
`;

const FromWrap = styled.div`
  max-width: 100%;
  max-height: 100%;

  overflow: auto;
  background: #fff;
  padding: 16px;
  .ant-col-24.ant-form-item-label {
    padding: 2px;
  }
  ::-webkit-scrollbar {
    width: 8px;
  }

  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px #fff;
    border-radius: 20px;
  }

  ::-webkit-scrollbar-thumb {
    background: #d8d6d6;
    border-radius: 20px;
  }
  .ant-form-item {
    margin-bottom: 12px !important;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-input-number:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-picker-focused,
  .ant-picker:hover,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none !important;
  }
  .ant-slider-track,
  .ant-slider:hover .ant-slider-track {
    background-color: ${(props) => props.theme.main};
  }
`;

const IntroWrap = styled.div``;
const Name = styled.div`
  font-size: 18px;
  font-family: var(--roboto-500);
  text-align: center;
  margin-bottom: 8px;
`;

const Description = styled.div`
  text-align: center;
  margin-bottom: 8px;
`;

const ButtonWrap = styled.div`
  width: 100%;
`;
const CustomButton = styled(Button)`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  transition: all 0.5s;
  border-radius: 5px;
  font-size: 16px;
  margin: 12px 0;

  :not([disabled]):hover,
  :not([disabled]):focus {
    color: #fff;
    border-color: #d9d9d9;
    background: #00ab55;
  }
  :first-child {
    :not([disabled]) {
      background: #00ab55;
      color: #fff;
    }
  }
`;

const Result = styled.div`
  margin-bottom: 10px;
`;
