import React from "react";
import styled from "styled-components";
import BackgroundImg from "../../assets/icons/dashboard/background.png";
import FormView from "./FormView";

const PreviewPage = () => {
  const data = JSON.parse(localStorage.getItem("formSetting"));
  const {
    objectStyle,
    position,
    background,
    optionDisplay,
    positionImage,
    listFieldMapping,
    fontColor,
    listMappingGrading,
    listOrdered,
  } = data;
  return (
    <Wrap>
      <img
        style={{ width: "100vw", height: "100vh" }}
        src={BackgroundImg}
        alt="background"
      />
      {objectStyle.dark_screen && <DarkTheme />}
      {Object.entries(data).length > 0 && (
        <FormView
          optionDisplay={optionDisplay}
          background={background}
          position={position}
          objectStyle={objectStyle}
          positionImage={positionImage}
          listFieldMapping={listFieldMapping}
          fontColor={fontColor}
          listOrdered={listOrdered}
          listMappingGrading={listMappingGrading}
        />
      )}
    </Wrap>
  );
};

export default PreviewPage;

const Wrap = styled.div`
  position: relative;
  /* img {
    width: 100vw;
    height: 100vh;
  } */
  .top_left {
    top: 20px;
    left: 20px;
  }
  .top_center {
    top: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .top_right {
    top: 20px;
    right: 20px;
  }
  .center_left {
    left: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .center_center {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
  .center_right {
    right: 20px;
    top: 50%;
    transform: translateY(-50%);
  }
  .bottom_left {
    left: 20px;
    bottom: 20px;
  }
  .bottom_center {
    bottom: 20px;
    left: 50%;
    transform: translateX(-50%);
  }
  .bottom_right {
    right: 20px;
    bottom: 20px;
  }
`;
const DarkTheme = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.45);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 5;
`;
