import { Form, Modal, Input, InputNumber, Button } from "antd";
import { FE_URL } from "constants/constants";
import React, { useCallback, useMemo } from "react";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { getListFieldConfigSuccess } from "redux/slices/formSetting";
import styled from "styled-components";
import _ from "lodash";
import {
  CopyOutlined,
  FileImageOutlined,
  FilePdfOutlined,
  UploadOutlined,
} from "@ant-design/icons";
import { Notification } from "components/Notification/Noti";
import QRCode from "qrcode.react";
import { jsPDF } from "jspdf";
import FormContent from "./FormContent/FormContent";
import moment from "moment";

const ModalShareForm = ({
  isModalVisible,
  setIsModalVisible,
  listFieldConfig,
  idShare,
  tenantId,
  setIdShare,
  listOrdered,
  setListOrdered,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [url, setUrl] = useState("");
  const [valueInput, setValueInput] = useState("");
  const [embedUrl, setEmbedUrl] = useState("");
  const [linkQR, setLinkQR] = useState("");
  const [params, setParams] = useState("");
  const [form] = Form.useForm();
  const [internal, setInternal] = useState("");
  const [external, setExternal] = useState("");
  const [colorInternal, setColorInternal] = useState("#000");
  const [colorExternal, setColorExternal] = useState("#000");
  const [sizeInternal, setSizeInternal] = useState(532);
  const [sizeExternal, setSizeExternal] = useState(532);
  const [fieldAccept, setfieldAccept] = useState([]);
  const [name, setName] = useState("QR");
  const [page, setPage] = useState(0);
  const [history, setHistory] = useState([listOrdered[0]]);
  const [dataSubmit, setDataSubmit] = useState({});
  const [conditionPagination, setConditionPagination] = useState("");
  useEffect(() => {
    setPage(0);
    setHistory([listOrdered[0]]);
  }, [listOrdered]);
  useEffect(() => {
    let tem = [];
    listFieldConfig.forEach((item) => {
      if (item.field_type !== "file" && !item.is_hidden) {
        tem.push({
          ...item,
          section: item.section ? item.section : "Default",
        });
      }
    });
    setfieldAccept(tem);
  }, [listFieldConfig, form]);
  useEffect(() => {
    let tem = `${valueInput}/internet-form/${tenantId}/${idShare}/${params}`;
    setEmbedUrl(tem);
    let temQR = `${valueInput}/qr-form/${tenantId}/${idShare}/${params}`;
    setLinkQR(temQR);
  }, [valueInput, params, idShare, tenantId]);

  useEffect(() => {
    if (!isModalVisible) {
      dispatch(getListFieldConfigSuccess([]));
      setIdShare("");
      setEmbedUrl("");
      setParams("");
      setValueInput("");
      setInternal("");
      setExternal("");
      setColorInternal("#000");
      setColorExternal("#000");
      setSizeInternal(532);
      setSizeExternal(532);
      setName("QR");
      setLinkQR("");
      setListOrdered(["Default"]);
      setPage(0);
      setHistory([listOrdered[0]]);
      setDataSubmit({});
      form.resetFields();
      setConditionPagination("");
    }
    //eslint-disable-next-line
  }, [isModalVisible, dispatch]);

  useEffect(() => {
    if (idShare && tenantId) {
      setUrl(`${FE_URL}/internet-form/${tenantId}/${idShare}`);
    }
  }, [idShare, tenantId]);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };

  //eslint-disable-next-line
  const checkConditionPagination = () => {
    let flag = "";
    Object.entries(form.getFieldsValue()).forEach(([key, value]) => {
      fieldAccept.forEach((field) => {
        if (
          field.conditional_pagination &&
          field.pagination_option &&
          field.id === key &&
          field.section === listOrdered[page]
        ) {
          field.pagination_option.every((option) => {
            if (option.value.toString() === value?.toString()) {
              flag = option.option;
              return false;
            } else {
              return true;
            }
          });
        }
      });
    });

    setConditionPagination(flag);
  };
  useEffect(() => {
    checkConditionPagination();
  }, [checkConditionPagination]);
  const onHandleChange = useCallback(
    (searchText, values, tenant, id, temHistory) => {
      let temp = `${FE_URL}/internet-form/${tenant}/${id}/`;
      let temParams = ``;
      let temName = ``;
      let temValue = { ...dataSubmit, ...form.getFieldsValue() };
      Object.entries(temValue).forEach((value) => {
        fieldAccept.forEach((item) => {
          if (value[0] === item.id && temHistory.includes(item.section)) {
            if (
              value[1] !== undefined &&
              value[1] !== null &&
              value[1] !== ""
            ) {
              if (typeof value[1] === "object") {
                temp += `&${value[0]}=${moment(value[1]).format(
                  item.field_type === "date"
                    ? "YYYY-MM-DD"
                    : "YYYY-MM-DD HH:mm:ss"
                )}`;
                temParams += `&${value[0]}=${moment(value[1]).format(
                  item.field_type === "date"
                    ? "YYYY-MM-DD"
                    : "YYYY-MM-DD HH:mm:ss"
                )}`;
                temName += `${moment(value[1]).format(
                  item.field_type === "date"
                    ? "YYYY-MM-DD"
                    : "YYYY-MM-DD HH:mm:ss"
                )} - `;
              } else {
                temp += `&${value[0]}=${value[1]}`;
                temParams += `&${value[0]}=${value[1]}`;
                temName += `${value[1].toString().trim()} - `;
              }
            }
          }
        });
      });
      console.log("temp", temp);
      console.log("temParams", temParams);
      setUrl(temp);
      setParams(temParams);
      setDataSubmit(temValue);
      if (temName) {
        let lastIndex = temName.lastIndexOf("-");
        if (lastIndex !== -1) {
          temName =
            temName.substring(0, lastIndex) + temName.substring(lastIndex + 1);
        }
        setName(temName);
      } else {
        setName("QR");
      }
      checkConditionPagination();
    },
    //eslint-disable-next-line
    [fieldAccept, page, dataSubmit, listOrdered, checkConditionPagination]
  );

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onHandleChange, 100),
    [onHandleChange]
  );

  const generatePDF = (option) => {
    let pdf = new jsPDF({
      orientation: "landscape",
      unit: "mm",
      format: [80, 80],
    });
    let op = option === "external" ? "external" : "qrcode";
    let base64Image = document.getElementById(op).toDataURL();
    pdf.addImage(base64Image, "png", 0, 0, 80, 80);
    pdf.save(`${name}.pdf`);
  };
  const generatePNG = (option) => {
    let op = option === "external" ? "external" : "qrcode";

    const dataURL = document.getElementById(op).toDataURL("image/png");
    const link = document.createElement("a");
    link.href = dataURL;
    link.download = `${name}.png`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleResetField = (field) => {
    let tem = "";
    listFieldConfig.forEach((item) => {
      if (item.triggering_field === field) {
        form.setFieldsValue({ [item.id]: undefined });
        tem = item.id;
        dataSubmit[item.id] = undefined;
      } else {
        tem = "";
      }
      if (tem) {
        handleResetField(tem);
      }
    });
  };

  const onColorHandle = useCallback((color, option) => {
    if (option === "internal") {
      setColorInternal(color);
    } else {
      setColorExternal(color);
    }
  }, []);

  const debouncedColorHandler = useMemo(
    () => _.debounce(onColorHandle, 100),
    [onColorHandle]
  );

  const checkIndexPage = (name, ordereds = ["Default"]) => {
    let index = ordereds.findIndex((item) => item === name);
    return index;
  };

  const handleNext = (values) => {
    if (
      conditionPagination &&
      checkIndexPage(conditionPagination, listOrdered) > page
    ) {
      let tem = [...history, conditionPagination];
      setHistory(tem);
      setPage(checkIndexPage(conditionPagination, listOrdered));
    } else {
      setPage((prev) => {
        let tem = [...history, listOrdered[prev + 1]];
        setHistory(tem);
        return prev + 1;
      });
    }
  };

  const handleBack = () => {
    let tem = [...history];
    tem.pop();
    setHistory(tem);
    debouncedSearchHandler({}, form.getFieldsValue(), tenantId, idShare, tem);
    Object.entries(form.getFieldsValue()).forEach(([key, value]) => {
      form.setFieldsValue({ [key]: undefined });
    });
    setPage(checkIndexPage(tem[tem.length - 1], listOrdered));
  };
  return (
    <>
      <CustomModal
        width={1200}
        title={t("form.share")}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Wrap>
          <Left>
            <p>{listOrdered[page]}</p>
            <Form
              labelCol={{ span: 24 }}
              wrapperCol={{ span: 24 }}
              name="basic"
              form={form}
              onFinish={handleNext}
              onValuesChange={(value, values) => {
                debouncedSearchHandler(
                  value,
                  values,
                  tenantId,
                  idShare,
                  history
                );
                handleResetField(Object.keys(value)[0]);
              }}
            >
              {/* {renderForm(listFieldConfig)} */}
              <FormContent
                fields={fieldAccept}
                form={form}
                defaultValue={{}}
                page={page}
                listOrdered={listOrdered}
              />
            </Form>
            <ButtonWrap>
              {page !== listOrdered.length - 1 &&
                conditionPagination !== "submit_form_crm" && (
                  <CustomButton onClick={() => form.submit()}>
                    {t("form.next")}
                  </CustomButton>
                )}
              {page !== 0 && (
                <CustomButton onClick={handleBack}>
                  {t("form.back")}
                </CustomButton>
              )}
            </ButtonWrap>
          </Left>
          <Right>
            <WrapContent>
              <Content>
                <CustomText>Internal URL</CustomText>
                <URLWrap>
                  <Input disabled value={url} />
                  <ButtonCopty
                    onClick={() => {
                      navigator.clipboard.writeText(url);
                      Notification("success", "Copy body success");
                    }}
                  >
                    <CopyOutlined />
                  </ButtonCopty>
                </URLWrap>
                <QRWrap>
                  <QRCode
                    color
                    id="qrcode"
                    value={url.replaceAll(" ", "%20")}
                    size={sizeInternal}
                    fgColor={colorInternal}
                    imageSettings={{
                      src: internal,
                      width: sizeInternal / 6,
                      height: sizeInternal / 6,
                      excavate: true,
                    }}
                  />
                  <ButtonGroup>
                    <ButtonWrap>
                      <CustomButtonModal>
                        <UploadOutlined />
                        Upload logo
                        <input
                          type="file"
                          accept="image/*, image/apng,image/bmp,image/gif,image/jpeg,image/pjpeg,image/png,image/svg+xml,image/tiff,image/webp,image/x-icon, image/vnd.microsoft.icon"
                          onChange={(e) => {
                            if (e.target.files[0]) {
                              setInternal(
                                URL.createObjectURL(e.target.files[0])
                              );
                            }
                          }}
                        ></input>
                      </CustomButtonModal>
                      <CustomButtonModal onClick={() => generatePDF()}>
                        <FilePdfOutlined />
                        PDF
                      </CustomButtonModal>
                      <CustomButtonModal onClick={() => generatePNG()}>
                        <FileImageOutlined />
                        PNG
                      </CustomButtonModal>
                    </ButtonWrap>
                    <input
                      // style={{ display: "none" }}
                      value={colorInternal}
                      type="color"
                      onChange={(e) =>
                        debouncedColorHandler(e.target.value, "internal")
                      }
                    ></input>

                    <InputNumber
                      placeholder="Kích thước"
                      min={500}
                      max={4000}
                      value={sizeInternal}
                      onChange={(value) => {
                        if (value) {
                          setSizeInternal(value);
                        } else {
                          setSizeInternal(532);
                        }
                      }}
                    />
                  </ButtonGroup>
                </QRWrap>
              </Content>
              <Content>
                <CustomText>External URL</CustomText>
                <URLWrap>
                  <Input
                    value={valueInput}
                    onChange={(value) => setValueInput(value.target.value)}
                  />
                  <ButtonCopty
                    onClick={() => {
                      navigator.clipboard.writeText(embedUrl);
                      Notification("success", "Copy body success");
                    }}
                  >
                    <CopyOutlined />
                  </ButtonCopty>
                </URLWrap>
                {embedUrl && (
                  <QRWrap>
                    <QRCode
                      id="external"
                      value={embedUrl.replaceAll(" ", "%20")}
                      size={sizeExternal}
                      fgColor={colorExternal}
                      imageSettings={{
                        src: external,
                        width: sizeExternal / 6,
                        height: sizeExternal / 6,
                        excavate: true,
                      }}
                    />
                    <ButtonGroup>
                      <ButtonWrap>
                        <CustomButtonModal>
                          <UploadOutlined />
                          Upload logo
                          <input
                            type="file"
                            accept="image/*, image/apng,image/bmp,image/gif,image/jpeg,image/pjpeg,image/png,image/svg+xml,image/tiff,image/webp,image/x-icon, image/vnd.microsoft.icon"
                            onChange={(e) => {
                              if (e.target.files[0]) {
                                setExternal(
                                  URL.createObjectURL(e.target.files[0])
                                );
                              }
                            }}
                          ></input>
                        </CustomButtonModal>
                        <CustomButtonModal
                          onClick={() => generatePDF("external")}
                        >
                          <FilePdfOutlined />
                          PDF
                        </CustomButtonModal>
                        <CustomButtonModal
                          onClick={() => generatePNG("external")}
                        >
                          <FileImageOutlined />
                          PNG
                        </CustomButtonModal>
                      </ButtonWrap>
                      <input
                        // style={{ display: "none" }}
                        value={colorExternal}
                        type="color"
                        onChange={(e) =>
                          debouncedColorHandler(e.target.value, "external")
                        }
                      ></input>
                      <InputNumber
                        placeholder="Kích thước"
                        min={500}
                        max={4000}
                        value={sizeExternal}
                        onChange={(value) => {
                          if (value) {
                            setSizeExternal(value);
                          } else {
                            setSizeExternal(532);
                          }
                        }}
                      />
                    </ButtonGroup>
                  </QRWrap>
                )}
              </Content>
              <Content style={{ width: "100%", marginTop: "12px" }}>
                <CustomText>Link QR</CustomText>
                <URLWrap>
                  <Input disabled value={linkQR} />
                  <ButtonCopty
                    onClick={() => {
                      navigator.clipboard.writeText(linkQR);
                      Notification("success", "Copy body success");
                    }}
                  >
                    <CopyOutlined />
                  </ButtonCopty>
                </URLWrap>
              </Content>
            </WrapContent>
            <CustomFooter>
              <Close onClick={() => handleCancel()}>Close</Close>
            </CustomFooter>
          </Right>
        </Wrap>
      </CustomModal>
    </>
  );
};

export default ModalShareForm;

const CustomModal = styled(Modal)`
  .ant-modal-footer {
    display: none;
  }
  .ant-modal-body {
    padding: 24px 24px 18px 24px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    padding: 6px 24px;
    background: #f2f4f5;
  }
  .ant-modal-title {
    font-family: var(--roboto-700);
    font-size: 16px;
  }
  .ant-modal-close-x {
    line-height: 36px;
    font-size: 16px;
    color: #000000;
  }
  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
  .ant-form-item-label {
    display: flex;
  }
`;

const Wrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

const Left = styled.div`
  width: 49%;
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  padding: 16px;
  max-height: 625px;
  overflow: auto;
  .ant-form-item-label
    > label.ant-form-item-required:not(
      .ant-form-item-required-mark-optional
    )::before {
    display: none;
  }
  label {
    white-space: pre-line;
    font-size: 16px;
    line-height: 22px;
    font-family: var(--roboto-500);
    height: unset;
  }
  .ant-form-item {
    margin-bottom: 16px;
  }
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-input-number-focused,
  .ant-picker-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after,
  .ant-input-number:hover,
  .ant-picker:hover {
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-slider-track,
  .ant-slider:hover .ant-slider-track {
    background-color: ${(props) => props.theme.main};
  }
`;

const Right = styled.div`
  width: 49%;
  font-size: 16px;
  line-height: 22px;
`;
const CustomText = styled.div`
  font-family: var(--roboto-500);
`;

const URLWrap = styled.div`
  margin: 8px 0;
  display: flex;
`;

const ButtonCopty = styled.div`
  border: 1px solid #d9d9d9;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 36px;
  cursor: pointer;
`;

const QRWrap = styled.div`
  canvas {
    max-width: 270px;
    max-height: 270px;
    min-width: 270px;
    min-height: 270px;
  }
`;

const ButtonGroup = styled.div`
  flex: 1;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  input {
    height: 40px;
    width: 100%;
  }
  .ant-input-number,
  .ant-input-number-focused {
    margin-top: 8px;
    width: 100%;
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
    input {
      width: 100%;
      height: 32px;
    }
  }
`;
const ButtonWrap = styled.div`
  width: 100%;
`;
const CustomButtonModal = styled.div`
  position: relative;
  width: 100%;
  cursor: pointer;
  border: 1px solid ${(props) => props.theme.main};
  border-radius: 2px;
  color: ${(props) => props.theme.main};
  margin-bottom: 8px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  svg {
    color: ${(props) => props.theme.main};
    margin-right: 8px;
  }
  input {
    opacity: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: absolute;
    top: 0;
    left: 0;
    cursor: pointer;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 32px;
`;
const Close = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  align-self: start;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
`;

const WrapContent = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  flex-wrap: wrap;
`;
const Content = styled.div`
  width: 48%;
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none !important;
  }
`;
const CustomButton = styled(Button)`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  transition: all 0.5s;
  border-radius: 5px;
  font-size: 16px;

  :not([disabled]):hover,
  :not([disabled]):focus {
    color: #fff;
    border-color: #d9d9d9;
    background: #00ab55;
  }
  :nth-child(1) {
    :not([disabled]) {
      background: #00ab55;
      color: #fff;
    }
    margin-bottom: 12px;
  }
`;
