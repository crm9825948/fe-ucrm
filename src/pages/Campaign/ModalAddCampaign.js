import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";

import Modal from "antd/lib/modal";
import Button from "antd/lib/button";

import emailIcon from "assets/icons/campaign/campaignEmail.svg";
import emailLogo from "assets/icons/campaign/campaignEmailLogo.png";
import smsIcon from "assets/icons/campaign/campaignSMS.svg";
import smsLogo from "assets/icons/campaign/campaignSMSLogo.png";
// import lcmIcon from "assets/icons/campaign/campaignLCM.svg";
// import lcmLogo from "assets/icons/campaign/campaignLCMLogo.png";
import teleMarketingIcon from "assets/icons/campaign/campaignTeleMarketing.svg";
import teleMarketingLogo from "assets/icons/campaign/campaignTeleMarketingLogo.png";
import { FE_URL } from "constants/constants";

function ModalAddCampaign({
  showModalCampaign,
  $showModalCampaign,
  isRedirect,
  typeCampaign,
}) {
  const navigate = useNavigate();

  const [typeSelected, $typeSelected] = useState({
    icon: emailIcon,
    logo: emailLogo,
    title: "Email",
    des: "Run Email Campaign from multiple CRM Objects (eg. Contact, Lead)",
    des1: "Email Campaign",
    des2: "Email Campaign have to connect with another email marketing service.",
    path: "/setting-campaign/create/Email",
  });

  const campaignTypes = [
    {
      icon: emailIcon,
      logo: emailLogo,
      title: "Email",
      des: "Run Email Campaign from multiple CRM Objects (eg. Contact, Lead)",
      des1: "Email Campaign",
      des2: "Email Campaign have to connect with another email marketing service.",
      path: "/setting-campaign/create/Email",
    },
    {
      icon: smsIcon,
      logo: smsLogo,
      title: "SMS",
      des: "Run SMS Campaign from multiple CRM Objects (eg. Contact, Lead)",
      des1: "SMS Campaign",
      des2: "User must setup SMS Outogoing Config for all related Objects Source with a Telecom Provider to run SMS Campaign.",
      path: "/setting-campaign/create/SMS",
    },
    // {
    //   icon: lcmIcon,
    //   logo: lcmLogo,
    //   title: "LCM",
    //   des: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    //   des1: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    //   des2: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    // },
    {
      icon: teleMarketingIcon,
      logo: teleMarketingLogo,
      title: "Telemarketing",
      des: "Run Telemarketing Campaign from multiple CRM Objects (eg. Contact, Lead)",
      des1: "Telemarketing Campaign",
      des2: "Extract contact information from multiple CRM Objects (eg. Contact, Lead) then generate tasks for Agents.",
      path: "/setting-campaign/create/Telemarketing",
    },
  ];

  const _onSubmit = () => {
    if (isRedirect) {
      window.open(`${FE_URL}${typeSelected.path}`, "_self");
    } else {
      navigate(typeSelected.path);
    }
  };

  const handleActive = (type) => {
    switch (type) {
      case "Email":
        $typeSelected({
          icon: emailIcon,
          logo: emailLogo,
          title: "Email",
          des: "Run Email Campaign from multiple CRM Objects (eg. Contact, Lead)",
          des1: "Email Campaign",
          des2: "Email Campaign have to connect with another email marketing service.",
          path: "/setting-campaign/create/Email",
        });
        break;

      case "SMS":
        $typeSelected({
          icon: smsIcon,
          logo: smsLogo,
          title: "SMS",
          des: "Run SMS Campaign from multiple CRM Objects (eg. Contact, Lead)",
          des1: "SMS Campaign",
          des2: "User must setup SMS Outogoing Config for all related Objects Source with a Telecom Provider to run SMS Campaign.",
          path: "/setting-campaign/create/SMS",
        });
        break;

      case "Telemarketing":
        $typeSelected({
          icon: teleMarketingIcon,
          logo: teleMarketingLogo,
          title: "Telemarketing",
          des: "Run Telemarketing Campaign from multiple CRM Objects (eg. Contact, Lead)",
          des1: "Telemarketing Campaign",
          des2: "Extract contact information from multiple CRM Objects (eg. Contact, Lead) then generate tasks for Agents.",
          path: "/setting-campaign/create/Telemarketing",
        });
        break;

      default:
        break;
    }
  };

  const _onCancel = () => {
    $showModalCampaign(false);

    if (typeCampaign) {
      handleActive(typeCampaign);
    } else {
      $typeSelected({
        icon: emailIcon,
        logo: emailLogo,
        title: "Email",
        des: "Run Email Campaign from multiple CRM Objects (eg. Contact, Lead)",
        des1: "Email Campaign",
        des2: "Email Campaign have to connect with another email marketing service.",
        path: "/setting-campaign/create/Email",
      });
    }
  };

  useEffect(() => {
    if (typeCampaign) {
      handleActive(typeCampaign);
    }
  }, [typeCampaign]);

  return (
    <>
      <ModalCustom
        title="Select campaign type"
        visible={showModalCampaign}
        footer={null}
        width={960}
        onCancel={_onCancel}
      >
        <Wrapper>
          <Left>
            {campaignTypes.map((item) => {
              return (
                <Wrap
                  onClick={() => $typeSelected(item)}
                  typeSelected={typeSelected.title === item.title}
                  key={item.title}
                >
                  <img src={item.icon} alt="icon" />

                  <WrapContent>
                    <Title>{item.title}</Title>
                    <Des>{item.des}</Des>
                  </WrapContent>
                </Wrap>
              );
            })}
          </Left>

          <Right>
            <Des2>{typeSelected.des1}</Des2>
            <Des3>{typeSelected.des2}</Des3>

            <ButtonSubmit type="primary" onClick={_onSubmit}>
              Create now
            </ButtonSubmit>

            <img src={typeSelected.logo} alt="logo" />
          </Right>
        </Wrapper>
      </ModalCustom>
    </>
  );
}

export default ModalAddCampaign;

ModalAddCampaign.defaultProps = {
  isRedirect: false,
  typeCampaign: "",
};

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const Wrapper = styled.div`
  display: flex;
`;

const Left = styled.div`
  padding-right: 16px;
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;
  padding: 16px;
  width: 379px;
  height: 109px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 8px;
  cursor: pointer;
  position: relative;
  background: ${({ typeSelected }) => (typeSelected ? "#f0f0f0" : "#fff")};

  ::before {
    content: "";
    width: ${({ typeSelected }) => (typeSelected ? "3px" : "0")};
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: ${(props) => props.theme.main};
    border-radius: 50px 0px 0 50px;
    border: ${(props) =>
      props.typeSelected ? `1px solid ${props.theme.main}` : "none"};
  }

  :hover {
    ::before {
      content: "";
      width: 3px;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      background: ${(props) => props.theme.main};
      border-radius: 50px 0px 0 50px;
      border: 1px solid ${(props) => props.theme.main};
    }
  }

  img {
    margin-right: 12px;
    margin-top: 8px;
    align-self: flex-start;
  }
`;

const WrapContent = styled.div``;

const Title = styled.div`
  color: ${(props) => props.theme.main};
  font-size: 18px;
  font-family: var(--roboto-500);
  margin-bottom: 3px;
`;

const Des = styled.div`
  font-size: 16px;
  color: #2c2c2c;
`;

const Right = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background: #f0f0f0;
  border-radius: 5px;
  padding: 16px;
`;

const Des2 = styled.div`
  font-size: 22px;
  font-family: var(--roboto-500);
  margin-bottom: 16px;
  text-align: center;
`;

const Des3 = styled.div`
  font-size: 16px;
  margin-bottom: 24px;
  text-align: center;
`;

const ButtonSubmit = styled(Button)`
  margin-bottom: 24px;

  font-size: 16px;
  height: unset;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  color: #fff;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;
