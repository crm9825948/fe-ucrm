import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";

import Button from "antd/lib/button";

import EmptyCampaign from "assets/images/campaign/empty.png";
import ImgConfirm from "assets/icons/common/confirm.png";

import PieChart from "components/Charts/pieChart";
import DoughnutChart from "components/Charts/doughnutChart";
import ModalConfirm from "components/Modal/ModalConfirm";

import { generateTask, runCampaign, stopCampaign } from "redux/slices/campaign";
import { setShowModalConfirm } from "redux/slices/global";

function SummaryCampaign({
  taskRules,
  recordData,
  listFieldsCampaign,
  userAssignTo,
  listObjects,
  taskCampaigns,
}) {
  const dispatch = useDispatch();
  const { objectID, recordID } = useParams();

  const [dataGenerate, setDataGenerate] = useState({});
  const [userAssign, setUserAssign] = useState("");
  const [dataCharts, setDataCharts] = useState({
    runningStatus: {
      name: "Running Status",
      datasets: [
        {
          data: [],
        },
      ],
      labels: ["Runned", "Not Runned"],
      percentage: "",
    },
    tasksStatus: {
      name: "Tasks Status",
      datasets: [
        {
          data: [],
        },
      ],
      labels: ["Total Succees", "Total Fail"],
    },
  });

  const { statusGenerateTask } = useSelector((state) => state.campaignReducer);

  const _onGenerateTask = () => {
    if (taskCampaigns.data.length === 0) {
      dispatch(
        generateTask({
          campaign_id: taskRules.campaign_id,
        })
      );
    } else {
      setDataGenerate({ campaign_id: taskRules.campaign_id });
      dispatch(setShowModalConfirm(true));
    }
  };

  const _onRunCampaign = () => {
    if (recordData.fld_campaign_running_status_01.value === "running") {
      dispatch(
        stopCampaign({
          id: {
            campaign_id: taskRules.campaign_id,
          },
          data_record: {
            object_id: objectID,
            id: recordID,
          },
        })
      );
    } else {
      dispatch(
        runCampaign({
          id: {
            campaign_id: taskRules.campaign_id,
          },
          data_record: {
            object_id: objectID,
            id: recordID,
          },
        })
      );
    }
  };

  useEffect(() => {
    if (Object.keys(recordData).length > 0) {
      if (
        recordData.fld_campaign_total_task_01.value &&
        (recordData.fld_campaign_total_successful_task_01.value ||
          recordData.fld_campaign_total_failure_task_01.value)
      ) {
        setDataCharts((prev) => {
          let tempData = { ...prev };

          const totalTask = recordData.fld_campaign_total_task_01.value
            ? recordData.fld_campaign_total_task_01.value
            : 0;

          const totalSuccessful = recordData
            .fld_campaign_total_successful_task_01.value
            ? recordData.fld_campaign_total_successful_task_01.value
            : 0;
          const totalFailure = recordData.fld_campaign_total_failure_task_01
            .value
            ? recordData.fld_campaign_total_failure_task_01.value
            : 0;

          tempData.runningStatus = {
            ...tempData.runningStatus,
            datasets: [
              {
                data: [
                  totalSuccessful + totalFailure,
                  totalTask - (totalSuccessful + totalFailure),
                ],
              },
            ],
            percentage: (((totalSuccessful + totalFailure) / totalTask) * 100)
              .toFixed(2)
              .toString(),
          };
          tempData.tasksStatus = {
            ...tempData.tasksStatus,
            datasets: [
              {
                data: [totalSuccessful, totalFailure],
              },
            ],
          };
          return tempData;
        });
      }
    }
  }, [recordData]);

  useEffect(() => {
    if (Object.keys(recordData).length > 0 && userAssignTo.length > 0) {
      const tempUser = userAssignTo.find(
        (user) => user._id === recordData.owner
      );
      const nameUser =
        tempUser.Middle_Name === ""
          ? tempUser.Last_Name + " " + tempUser.First_Name
          : tempUser.Last_Name +
            " " +
            tempUser.Middle_Name +
            " " +
            tempUser.First_Name;
      setUserAssign(nameUser);
    }
  }, [recordData, userAssignTo]);

  return (
    <Wrapper>
      <Infor>
        <WrapInfor>
          <Left>
            <WrapField>
              <Label>Assign to:</Label>
              <Value>{userAssign}</Value>
            </WrapField>

            {listFieldsCampaign.map((item, idx) => {
              return (
                <WrapField key={idx}>
                  <Label>{item.label}:</Label>
                  <Value>{recordData[item.value]?.value || ""}</Value>
                </WrapField>
              );
            })}
          </Left>

          <Right>
            <WrapField>
              <Label>Rule:</Label>
              <Value>
                {Object.keys(taskRules).length > 0 &&
                  taskRules.mapping_rules.length > 0 &&
                  taskRules.mapping_rules.map((rule, idx) => {
                    return (
                      <div key={idx}>
                        <span>
                          •{" "}
                          {listObjects.length > 0 &&
                            listObjects.find(
                              (object) => object.value === rule.object_id
                            ).label}
                          : {rule.description}
                        </span>
                      </div>
                    );
                  })}
              </Value>
            </WrapField>

            <WrapField>
              <Label>Schedule:</Label>
              <Value>
                {Object.keys(taskRules).length > 0 &&
                  taskRules.schedule_configs.length > 0 &&
                  taskRules.schedule_configs.map((schedule, idx) => {
                    return (
                      <span key={idx}>
                        • From {schedule.time[0]} to {schedule.time[1]}
                      </span>
                    );
                  })}
              </Value>
            </WrapField>
          </Right>
        </WrapInfor>

        <GenerateTask>
          {taskCampaigns.data.length === 0 ? (
            <Empty>
              <img src={EmptyCampaign} alt="empty" />
              <p>
                Hiện chưa phân chia <span>Công việc</span>
              </p>
              <ButtonGenerateTask
                disabled={statusGenerateTask === "Generate Task..."}
                onClick={_onGenerateTask}
              >
                {statusGenerateTask}
              </ButtonGenerateTask>
            </Empty>
          ) : (
            <>
              <ButtonGenerateTask
                disabled={statusGenerateTask === "Generate Task..."}
                onClick={_onGenerateTask}
              >
                {statusGenerateTask}
              </ButtonGenerateTask>
              <ButtonRun onClick={_onRunCampaign}>
                {Object.keys(recordData).length > 0
                  ? recordData.fld_campaign_running_status_01.value ===
                    "running"
                    ? "Pause"
                    : "Run manually"
                  : ""}
              </ButtonRun>
            </>
          )}
        </GenerateTask>
      </Infor>

      {dataCharts.tasksStatus.datasets[0].data.length > 0 && (
        <WrapChart>
          <Status>
            <WrapTotal>
              <TotalTask>
                <ValueTask>
                  {Object.keys(recordData).length > 0 &&
                    recordData.fld_campaign_total_task_01.value}
                </ValueTask>
                <Title>Total Task</Title>
              </TotalTask>
            </WrapTotal>
            <Details>
              <TotalTask className="task">
                <ValueTask>
                  {Object.keys(recordData).length > 0 &&
                    recordData.fld_campaign_total_successful_task_01.value}
                </ValueTask>
                <Title>Success</Title>
              </TotalTask>
              <TotalTask>
                <ValueTask>
                  {Object.keys(recordData).length > 0 &&
                    recordData.fld_campaign_total_failure_task_01.value}
                </ValueTask>
                <Title>Fail</Title>
              </TotalTask>
            </Details>
          </Status>
          {dataCharts.runningStatus.percentage !== "" && (
            <Chart>
              <DoughnutChart
                data={dataCharts.runningStatus}
                isShowAction={false}
              />
            </Chart>
          )}
          <Chart>
            <PieChart data={dataCharts.tasksStatus} isShowAction={false} />
          </Chart>
        </WrapChart>
      )}

      <ModalConfirm
        title="Generate Task Confirmation"
        decs="Some Campaign Task may be overwritten by the process. Are you sure you want to proceed?"
        method={generateTask}
        data={dataGenerate}
        img={ImgConfirm}
      />
    </Wrapper>
  );
}

export default SummaryCampaign;

SummaryCampaign.defaultProps = {
  taskRules: {},
  recordData: {},
};

const Wrapper = styled.div`
  margin-top: 16px;
`;

const Infor = styled.div`
  background: #fff;
  padding: 24px;
`;

const WrapInfor = styled.div`
  display: flex;
  justify-content: center;
`;

const Left = styled.div`
  border-right: 1px solid #ececec;
  padding-right: 12.5rem;
`;

const Right = styled.div`
  padding-left: 3rem;
`;

const WrapField = styled.div`
  display: flex;
  margin-bottom: 12px;
`;

const Label = styled.div`
  font-family: var(--roboto-700);
  font-size: 16px;
  width: 120px;
  margin-right: 10px;
`;

const Value = styled.div`
  font-family: var(--roboto-400);
  font-size: 16px;
  text-transform: capitalize;
  display: flex;
  flex-direction: column;
`;

const GenerateTask = styled.div`
  display: flex;
  justify-content: center;
  margin-left: 9rem;
`;

const Empty = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #2c2c2c;

  img {
    width: fit-content;
  }

  p {
    margin-bottom: 0;
  }

  span {
    color: ${(props) => props.theme.main};
    font-family: var(--roboto-700);
  }
`;

const ButtonGenerateTask = styled(Button)`
  width: 134px;
  font-size: 16px;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  span {
    color: #fff;
    font-family: var(--roboto-400);
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
  }

  :active {
    background-color: ${(props) => props.theme.main};
  }

  :focus {
    background-color: ${(props) => props.theme.main};
  }

  :disabled {
    span {
      color: rgba(0, 0, 0, 0.25) !important;
    }

    :hover {
      background: #fff !important;
    }
  }
`;

const ButtonRun = styled(ButtonGenerateTask)`
  margin-left: 16px;
`;

const WrapChart = styled.div`
  display: flex;
  margin-top: 16px;
  background: #fff;
  padding: 16px;
`;

const Status = styled.div`
  border: 1px solid #ececec;
  border-radius: 5px;
  flex: 1;
  padding: 16px;
  margin-right: 16px;
  /* display: flex; */
  /* flex-direction: column; */
`;

const WrapTotal = styled.div`
  height: 50%;
  margin-bottom: 8px;
`;

const TotalTask = styled.div`
  margin-bottom: 8px;
  background: #f6fbff;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  flex: 1;
  height: 100%;

  &.task {
    margin-right: 8px;
  }
`;

const Title = styled.div`
  color: #6b6b6b;
  font-family: var(--roboto-400);
  font-size: 24px;
`;

const ValueTask = styled.div`
  color: #252424;
  font-family: var(--roboto-500);
  font-size: 60px;
  margin-bottom: 16px;
`;

const Details = styled.div`
  display: flex;
  height: 50%;
`;

const Chart = styled.div`
  flex: 2;
  /* width: 594px; */
  /* height: 510px; */
  padding: 20px 20px 28px 20px;
  background: #fff;
  margin-right: 8px;
  border: 1px solid #ececec;
  border-radius: 5px;

  :last-child {
    margin-right: 0;
    margin-left: 8px;
  }
`;
