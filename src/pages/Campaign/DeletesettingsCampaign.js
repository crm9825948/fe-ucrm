import { useEffect } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Switch from "antd/lib/switch";

import EmailOutgoing from "assets/icons/campaign/emailOutgoing.svg";
import SMSOutgoing from "assets/icons/campaign/smsOutgoing.svg";
import EmailTemplate from "assets/icons/campaign/emailTemplate.svg";
import SMSTemplate from "assets/icons/campaign/smsTemplate.svg";

import { FE_URL } from "constants/constants";

import {
  updateActivateCampaign,
  activateCampaign,
  deactivateCampaign,
} from "redux/slices/campaign";

function SettingsCampaign(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { activateStatus } = useSelector((state) => state.campaignReducer);
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const directLink = [
    {
      icon: EmailOutgoing,
      title: t("campaignSetting.configEmailOutgoing"),
      desc: t("campaignSetting.descEmailOutgoing"),
      link: "email-outgoing",
    },
    {
      icon: SMSOutgoing,
      title: t("campaignSetting.configSMSOutgoing"),
      desc: t("campaignSetting.descSMSOutgoing"),
      link: "sms-outgoing",
    },
    {
      icon: EmailTemplate,
      title: t("campaignSetting.createEmailTemplate"),
      desc: t("campaignSetting.descCreateEmail"),
      link: "email-template",
    },
    {
      icon: SMSTemplate,
      title: t("campaignSetting.createSMSTemplate"),
      desc: t("campaignSetting.descCreateSMS"),
      link: "sms-template",
    },
  ];

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "campaign" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActivate = () => {
    if (activateStatus) {
      dispatch(deactivateCampaign());
    } else {
      dispatch(activateCampaign());
    }
  };

  useEffect(() => {
    Object.entries(objectCategory).forEach(([key, value], idx) => {
      value.forEach((object, index) => {
        if (object._id === "obj_crm_campaign_00001") {
          dispatch(updateActivateCampaign(true));
        }
      });
    });
  }, [dispatch, objectCategory]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.campaignSetting")}</BreadcrumbItem>
      </Breadcrumb>

      <WrapSettings>
        <Settings>
          <Activate>
            <Switch
              checked={activateStatus}
              onChange={handleActivate}
              disabled={!checkRule("edit")}
            />
            <span>{t("campaignSetting.enableCampaign")}</span>
          </Activate>
          <Note>{t("campaignSetting.noteDisable")}</Note>
          {directLink.map((item, idx) => {
            return (
              <DirectLink
                key={idx}
                isDisabled={!activateStatus}
                onClick={() =>
                  activateStatus && window.open(`${FE_URL}/${item.link}`)
                }
              >
                <img src={item.icon} alt="idx" />
                <Text isDisabled={!activateStatus}>
                  <p>{item.title}</p>
                  <span>{item.desc}</span>
                </Text>
              </DirectLink>
            );
          })}
        </Settings>
      </WrapSettings>
    </Wrapper>
  );
}

export default withTranslation()(SettingsCampaign);

const Wrapper = styled.div`
  padding: 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapSettings = styled.div`
  background: #fff;
  padding: 55px 0;
  margin-top: 16px;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Settings = styled.div`
  padding: 32px 48px;
  border: 1px solid #ececec;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  display: flex;
  flex-direction: column;
`;

const Activate = styled.div`
  display: flex;
  align-items: center;

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  span {
    margin-left: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-700);
    font-size: 16px;
  }
`;

const Note = styled.div`
  margin-top: 8px;
  color: #ffa940;
  font-family: var(--roboto-400);
`;

const DirectLink = styled.div`
  display: flex;
  align-items: center;
  padding: 16px;
  border: ${({ isDisabled }) =>
    isDisabled ? "1px solid #8C8C8C" : "1px solid #f0f0f0"};

  border-radius: 5px;
  margin-top: 24px;

  :hover {
    cursor: ${({ isDisabled }) => (isDisabled ? "normal" : "pointer")};
    background: ${({ isDisabled }) => (isDisabled ? "unset" : "#f5f5f5")};
  }

  img {
    opacity: ${({ isDisabled }) => (isDisabled ? "0.5" : "1")};
  }
`;

const Text = styled.div`
  margin-left: 16px;

  p {
    margin-bottom: 0;
    font-family: var(--roboto-700);
    font-size: 16px;
    color: ${({ isDisabled }) => (isDisabled ? "#8C8C8C" : "#252424")};
    margin-bottom: 8px;
  }

  span {
    font-family: var(--roboto-400);
    font-size: 16px;
    color: ${({ isDisabled }) => (isDisabled ? "#8C8C8C" : "#252424")};
  }
`;
