import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch } from "react-redux";
import { useParams } from "react-router";

import Table from "antd/lib/table";
import Typography from "antd/lib/typography";
import Pagination from "antd/lib/pagination";
import Tooltip from "antd/lib/tooltip";
import "./campaign.scss";

import { loadTaskCampaign } from "redux/slices/campaign";

function TaskListCampaign({ listFieldsCampaignTask, taskCampaigns }) {
  const dispatch = useDispatch();
  const { recordID } = useParams();

  const { Column } = Table;
  const { Text } = Typography;

  const [dataTable, setDataTable] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  const showTotal = () => {
    return `Total ${taskCampaigns.pagination.total_record} items`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    dispatch(
      loadTaskCampaign({
        campaign_id: recordID,
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [currentPage, dispatch, recordPerPage, recordID]);

  useEffect(() => {
    let tempList = [];

    taskCampaigns.data.forEach((item, idx) => {
      tempList.push({
        key: item._id,
      });
      let tempFields = [];
      Object.entries(item).forEach(([key, val]) => {
        if (val.value) {
          tempFields.push({
            [key]: val.value,
          });
        }
      });
      tempFields.forEach((field) => {
        tempList[idx] = {
          ...tempList[idx],
          ...field,
        };
      });
    });

    setDataTable(tempList);
  }, [taskCampaigns]);

  return (
    <Wrapper>
      <Table
        pagination={false}
        dataSource={dataTable}
        scroll={{ x: "max-content" }}
      >
        {listFieldsCampaignTask.map((field) => {
          return (
            <Column
              title={field.label}
              dataIndex={field.value}
              key={field.value}
              render={(text) =>
                field.value === "fld_ct_content_string_01" ? (
                  <Tooltip
                    placement="right"
                    overlayClassName="customTooltip"
                    title={
                      <>
                        <Title>{text.split("-----")[0]}</Title>
                        <span
                          dangerouslySetInnerHTML={{
                            __html: text.split("-----")[1],
                          }}
                        ></span>
                      </>
                    }
                  >
                    {text.split("-----")[0]}
                  </Tooltip>
                ) : (
                  <Text
                    ellipsis={{ tooltip: text }}
                    style={{ maxWidth: "350px" }}
                  >
                    {text}
                  </Text>
                )
              }
            />
          );
        })}
      </Table>

      <Pagination
        showTotal={showTotal}
        current={currentPage}
        pageSize={recordPerPage}
        total={taskCampaigns.pagination.total_record}
        onChange={handleSelectPage}
        showSizeChanger
        onShowSizeChange={_onSizeChange}
        showQuickJumper
      />
    </Wrapper>
  );
}

export default TaskListCampaign;

const Wrapper = styled.div`
  margin-top: 16px;
  padding: 16px 24px;
  background: #fff;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 7px 16px;
    border: 1px solid #ededed;

    :before {
      display: none;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 13.5px 16px;
    border: 1px solid #ededed;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const Title = styled.div`
  font-family: var(--roboto-700);
  font-size: 20px;
  color: #252424;
  margin-bottom: 16px;
`;
