import styled from "styled-components/macro";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router";

import Tabs from "antd/lib/tabs";
import Breadcrumb from "antd/lib/breadcrumb";

import {
  loadSetingsCampaign,
  loadRulesSuccess,
  loadFieldsCampaign,
  loadFieldsCampaignTask,
} from "redux/slices/campaign";
import { loadUserAssignTo } from "redux/slices/objects";

import SummaryCampaign from "./summaryCampaign";
import RulesCampaign from "./ruleCampaign";
import TaskListCampaign from "./taskListCampaign";

function EditCampaign(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { TabPane } = Tabs;
  const { objectID, recordID } = useParams();

  const { recordData } = useSelector((state) => state.objectsReducer);
  const { taskRules } = useSelector((state) => state.campaignReducer);
  const { listFieldsCampaign, listFieldsCampaignTask, taskCampaigns } =
    useSelector((state) => state.campaignReducer);
  const { userAssignTo } = useSelector((state) => state.objectsReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [tab, setTab] = useState("summary");

  const [listObjects, setListObjects] = useState([]);

  const handleSelectTab = (e) => {
    setTab(e);
  };

  useEffect(() => {
    dispatch(
      loadSetingsCampaign({
        task: {
          campaign_id: recordID,
        },
        data_record: {
          object_id: objectID,
          id: recordID,
        },
      })
    );
    dispatch(
      loadFieldsCampaign({
        api_version: "2",
        object_id: "obj_crm_campaign_00001",
      })
    );
    dispatch(
      loadFieldsCampaignTask({
        api_version: "2",
        object_id: "obj_crm_campaign_task_00001",
      })
    );

    dispatch(loadUserAssignTo());
  }, [dispatch, objectID, recordID]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      let tempListObjects = tempObjects.filter(
        (object) =>
          object.value !== "obj_crm_campaign_task_00001" &&
          object.value !== "obj_crm_campaign_00001"
      );
      setListObjects(tempListObjects);
    }
  }, [category]);

  useEffect(() => {
    return () => {
      dispatch(loadRulesSuccess({}));
    };
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item
          onClick={() =>
            navigate("/objects/obj_crm_campaign_00001/default-view")
          }
        >
          Objects
        </Breadcrumb.Item>
        <Breadcrumb.Item
          onClick={() =>
            navigate("/objects/obj_crm_campaign_00001/default-view")
          }
        >
          Campaign list
        </Breadcrumb.Item>
        <BreadcrumbItem>
          {Object.keys(recordData).length > 0 &&
            recordData.fld_campaign_name_01.value}
        </BreadcrumbItem>
      </Breadcrumb>

      <Tabs activeKey={tab} onChange={handleSelectTab}>
        <TabPane tab="Summary" key="summary">
          <SummaryCampaign
            taskRules={taskRules}
            recordData={recordData}
            listFieldsCampaign={listFieldsCampaign}
            userAssignTo={userAssignTo}
            listObjects={listObjects}
            taskCampaigns={taskCampaigns}
          />
        </TabPane>
        <TabPane tab="Setting" key="setting">
          <RulesCampaign taskRules={taskRules} recordData={recordData} />
        </TabPane>
        <TabPane tab="Task list" key="task_list">
          <TaskListCampaign
            listFieldsCampaignTask={listFieldsCampaignTask}
            taskCampaigns={taskCampaigns}
          />
        </TabPane>
      </Tabs>
    </Wrapper>
  );
}

export default EditCampaign;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-tabs-nav {
    margin-bottom: 0;
  }

  .ant-tabs-nav-wrap {
    background: #fff;
    border-radius: 5px;
    margin-top: 16px;
    padding-left: 16px;
    height: 46px;
  }

  .ant-tabs-tab-btn {
    font-size: 16px;
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(props) => props.theme.main} !important;
  }

  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }

  .ant-tabs-tab:hover {
    color: ${(props) => props.theme.main};
  }

  .ant-tabs-tab.ant-tabs-tab-disabled {
    cursor: default;

    .ant-btn {
      display: flex;
      align-items: center;
    }
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
