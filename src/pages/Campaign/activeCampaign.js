import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Switch from "antd/lib/switch";
import Spin from "antd/lib/spin";
import Button from "antd/lib/button";
import { RightOutlined } from "@ant-design/icons";

import SMSConfigInactive from "assets/icons/campaign/SMSConfigInactive.svg";
import SMSConfigActive from "assets/icons/campaign/SMSConfigActive.svg";
import SMSTemplateInactive from "assets/icons/campaign/SMSTemplateInactive.svg";
import SMSTemplateActive from "assets/icons/campaign/SMSTemplateActive.svg";
import MailChimpInactive from "assets/icons/campaign/MailChimpInactive.svg";
import MailChimpActive from "assets/icons/campaign/MailChimpActive.svg";

import { FE_URL } from "constants/constants";

import {
  activateCampaign,
  deactivateCampaign,
  checkAvailable,
  getInfoMailChimp,
  connectMailchimp,
  disconnectMailchimp,
} from "redux/slices/campaign";
import { useEffect, useState } from "react";
import { changeTitlePage } from "redux/slices/authenticated";

function SettingsCampaign(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  useEffect(() => {
    dispatch(changeTitlePage(t("settings.campaignSetting")));
    //eslint-disable-next-line
  }, [t]);

  const { activateStatus, isLoading, infoMailChimp } = useSelector(
    (state) => state.campaignReducer
  );
  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const [mailchimp, $mailchimp] = useState([]);

  const directLink = [
    {
      iconInactive: SMSConfigInactive,
      iconActive: SMSConfigActive,
      title: t("campaignSetting.configSMSOutgoing"),
      desc: t("campaignSetting.descSMSOutgoing"),
      link: "sms-outgoing",
    },
    {
      iconInactive: SMSTemplateInactive,
      iconActive: SMSTemplateActive,
      title: t("campaignSetting.createSMSTemplate"),
      desc: t("campaignSetting.descCreateSMS"),
      link: "sms-template",
    },
  ];

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "campaign" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActivate = () => {
    if (activateStatus) {
      dispatch(deactivateCampaign());
    } else {
      dispatch(activateCampaign());
    }
  };

  const _onConnectMailchimp = () => {
    dispatch(connectMailchimp());
  };

  const _onDisconnectMailchimp = (e) => {
    e.stopPropagation();
    dispatch(disconnectMailchimp());
  };

  useEffect(() => {
    dispatch(checkAvailable());
    dispatch(getInfoMailChimp());
  }, [dispatch]);

  useEffect(() => {
    if (_.isEmpty(infoMailChimp)) {
      $mailchimp([
        {
          iconInactive: MailChimpInactive,
          iconActive: MailChimpActive,
          title: "Mailchimp",
          desc: (
            <>
              <span>Connect to Mailchimp &nbsp;</span>
              <RightOutlined style={{ fontSize: 13 }} />
            </>
          ),
        },
      ]);
    } else {
    }
  }, [infoMailChimp]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.campaignSetting")}</BreadcrumbItem>
      </Breadcrumb>

      <WrapSettings>
        <Settings>
          <Activate>
            <Switch
              checked={activateStatus}
              onChange={handleActivate}
              disabled={!checkRule("edit")}
            />
            <span>{t("campaignSetting.enableCampaign")}</span>

            {isLoading.active && (
              <div style={{ display: "flex", alignItems: "center" }}>
                <Spin />
                <p style={{ marginBottom: 0, marginLeft: 8 }}>
                  {activateStatus ? "Deactivating..." : "Activating..."}
                </p>
              </div>
            )}
          </Activate>
          <Note>{t("campaignSetting.noteDisable")}</Note>

          {mailchimp.map((item, idx) => {
            return (
              <DirectLink
                key={idx}
                isDisabled={!activateStatus}
                onClick={() =>
                  activateStatus &&
                  _.isEmpty(infoMailChimp) &&
                  _onConnectMailchimp()
                }
              >
                <img
                  src={
                    activateStatus && !_.isEmpty(infoMailChimp)
                      ? item.iconActive
                      : item.iconInactive
                  }
                  alt="idx"
                />
                <Text isDisabled={!activateStatus}>
                  <p>{item.title}</p>

                  {_.isEmpty(infoMailChimp) ? (
                    <span style={{ display: "flex", alignItems: "center" }}>
                      {item.desc}
                    </span>
                  ) : (
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <span style={{ display: "flex", alignItems: "center" }}>
                        {infoMailChimp.email}
                      </span>
                      <DisconnectButton onClick={_onDisconnectMailchimp}>
                        Disconnect
                      </DisconnectButton>
                    </div>
                  )}
                </Text>
              </DirectLink>
            );
          })}

          {directLink.map((item, idx) => {
            return (
              <DirectLink
                key={idx}
                isDisabled={!activateStatus}
                onClick={() =>
                  activateStatus && window.open(`${FE_URL}/${item.link}`)
                }
              >
                <img
                  src={activateStatus ? item.iconActive : item.iconInactive}
                  alt="idx"
                />
                <Text isDisabled={!activateStatus}>
                  <p>{item.title}</p>
                  <span>{item.desc}</span>
                </Text>
              </DirectLink>
            );
          })}
        </Settings>
      </WrapSettings>
    </Wrapper>
  );
}

export default withTranslation()(SettingsCampaign);

const Wrapper = styled.div`
  padding: 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapSettings = styled.div`
  background: #fff;
  padding: 55px 0;
  margin-top: 16px;
  display: flex;
  justify-content: center;
  align-items: center;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const Settings = styled.div`
  padding: 32px 48px;
  border: 1px solid #ececec;
  box-shadow: 0px 4px 10px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  display: flex;
  flex-direction: column;
`;

const Activate = styled.div`
  display: flex;
  align-items: center;

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  span {
    margin-left: 16px;
    color: #2c2c2c;
    font-family: var(--roboto-700);
    font-size: 16px;
  }
`;

const Note = styled.div`
  margin-top: 8px;
  color: #ffa940;
  font-family: var(--roboto-400);
`;

const DirectLink = styled.div`
  display: flex;
  align-items: center;
  padding: 16px;
  border: ${({ isDisabled }) =>
    isDisabled ? "1px solid #8C8C8C" : "1px solid #f0f0f0"};

  border-radius: 5px;
  margin-top: 24px;

  :hover {
    cursor: ${({ isDisabled }) => (isDisabled ? "normal" : "pointer")};
    background: ${({ isDisabled }) => (isDisabled ? "unset" : "#f5f5f5")};
  }

  img {
    opacity: ${({ isDisabled }) => (isDisabled ? "0.5" : "1")};
  }
`;

const Text = styled.div`
  margin-left: 16px;

  p {
    margin-bottom: 0;
    font-family: var(--roboto-700);
    font-size: 16px;
    color: ${({ isDisabled }) => (isDisabled ? "#8C8C8C" : "#252424")};
    margin-bottom: 8px;
  }

  span {
    font-family: var(--roboto-400);
    font-size: 16px;
    color: ${({ isDisabled }) => (isDisabled ? "#8C8C8C" : "#252424")};
  }
`;

const DisconnectButton = styled(Button)`
  margin-left: 8px;

  &.ant-btn {
    font-size: 16px;

    :hover {
      background: ${(props) => props.theme.darker}!important;

      span {
        color: #fff !important;
      }
    }
  }

  &.ant-btn:active {
    background: #ebeef0;
    border: 1px solid #d9d9d9;
    span {
      color: #252424;
    }
  }

  &.ant-btn:focus {
    background: #ebeef0;
    border: 1px solid #d9d9d9;
    span {
      color: #252424;
    }
  }
`;
