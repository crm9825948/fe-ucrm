import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Button from "antd/lib/button";
import Progress from "antd/lib/progress";
import Typography from "antd/lib/typography";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Switch from "antd/lib/switch";
import Checkbox from "antd/lib/checkbox";
import DatePicker from "antd/lib/date-picker";
import locale from "antd/es/date-picker/locale/en_US";

import CalendarIcon from "assets/icons/common/calendarIcon.svg";

import ModalAddCampaign from "./ModalAddCampaign";
const { Paragraph } = Typography;

function ListviewCampaign({ listCampaign }) {
  const [showModalCampaign, $showModalCampaign] = useState(false);
  const [isActive, $isActive] = useState(0);
  const [activeCampaign, $activeCampaign] = useState({});

  const formattedNumber = (value) => {
    return value.toFixed(3).replace(/[.,]000$/, "");
  };

  const _onSelectCampaign = (index, campaign) => {
    $isActive(index);
    $activeCampaign(campaign);
  };

  useEffect(() => {
    if (listCampaign.length > 0) {
      $activeCampaign(listCampaign[0]);
    }
  }, [listCampaign]);

  return (
    <Wrapper>
      <Left>
        <Filter>
          <div style={{ display: "flex" }}>
            <Input placeholder="Please input" />
            <AddCampaign onClick={() => $showModalCampaign(true)}>
              + Add campaign
            </AddCampaign>
          </div>

          <div
            style={{
              padding: "16px",
              border: "1px solid #D9D9D9",
              borderRadius: "5px",
              marginTop: "8px",
            }}
          >
            <WrapFilter>
              <p>Status</p>
              <Select />
            </WrapFilter>
            <WrapFilter>
              <p>Type</p>
              <Select />
            </WrapFilter>
          </div>
        </Filter>

        <List>
          {_.map(listCampaign, (campaign, index) => {
            return (
              <Campaign
                isActive={isActive === index}
                onClick={() => {
                  _onSelectCampaign(index, campaign);
                }}
              >
                <NameCampaign
                  isActive={isActive === index}
                  ellipsis={{ tooltip: campaign.name, rows: 2 }}
                >
                  {campaign.name}
                </NameCampaign>

                <TypeCampaign>
                  Campaign type: <span>{campaign.type}</span>
                </TypeCampaign>

                <TimeCampaign>
                  <div>
                    Start:&nbsp; <span>{campaign.start_date}</span> End:&nbsp;
                    <span>{campaign.end_date}</span>
                  </div>
                  <StatusCampaign
                    color={campaign.status === "In Queue" ? "#2f54eb" : "#fff"}
                  >
                    {campaign.status}
                  </StatusCampaign>
                </TimeCampaign>
              </Campaign>
            );
          })}
        </List>
      </Left>

      {Object.keys(activeCampaign).length > 0 && (
        <Right>
          <Top>
            <TopLeft>
              <div
                style={{
                  display: "flex",
                  marginBottom: "16px",
                }}
              >
                <NumberStatus color={"#016eff"}>
                  <p>{_.get(activeCampaign, "total_members", 0)}</p>
                  <span>Total member</span>
                </NumberStatus>
                <NumberStatus color={"#52c41a"}>
                  <p>{_.get(activeCampaign, "total_success", 0)}</p>
                  <span>Success</span>
                </NumberStatus>
                <NumberStatus color={"#6b6b6b"}>
                  <p>
                    {_.get(activeCampaign, "total_sent", 0) -
                      _.get(activeCampaign, "total_success", 0)}
                  </p>
                  <span>Fail</span>
                </NumberStatus>
              </div>

              <div
                style={{
                  display: "flex",
                }}
              >
                <RunningStatus>
                  <TitleRunning>Running Status</TitleRunning>

                  <Progress
                    width={230}
                    percent={
                      (_.get(activeCampaign, "total_sent", 0) /
                        _.get(activeCampaign, "total_members", 0)) *
                      100
                    }
                    strokeColor={{
                      "0%": "#016eff",
                      "100%": "#016eff",
                    }}
                    format={(percent) => (
                      <div
                        style={{
                          display: "flex",
                          justifyContent: "center",
                          alignItems: "center",
                        }}
                      >
                        <Percent>
                          {_.get(activeCampaign, "total_sent", 0)}/
                        </Percent>
                        <Percent>
                          {_.get(activeCampaign, "total_members", 0)}
                        </Percent>
                      </div>
                    )}
                    type="circle"
                    style={{
                      border: "20px solid #F6FBFF",
                      borderRadius: "50%",
                    }}
                  />

                  <WrapLabel>
                    <Label color={"#016eff"}>Đã chạy</Label>
                    <Label color={"#e2eefe"}>Chưa chạy</Label>
                  </WrapLabel>
                </RunningStatus>

                <TaskStatus>
                  <TitleRunning>Tasks Status</TitleRunning>

                  <Progress
                    width={230}
                    percent={formattedNumber(
                      (Math.abs(_.get(activeCampaign, "total_success", 0)) /
                        _.get(activeCampaign, "total_sent", 0)) *
                        100
                    )}
                    strokeColor={{
                      "0%": "#4dd865",
                      "100%": "#4dd865",
                    }}
                    type="circle"
                    style={{
                      border: "20px solid #F6FBFF",
                      borderRadius: "50%",
                    }}
                  />

                  <WrapLabel>
                    <Label color={"#4dd865"}>Success</Label>
                    <Label color={"#e2eefe"}>Fail</Label>
                  </WrapLabel>
                </TaskStatus>
              </div>
            </TopLeft>

            <TopRight>
              <div
                style={{
                  border: "1px solid rgba(89, 106, 106, 0.25)",
                  borderRadius: "5px",
                  display: "flex",
                  marginBottom: "16px",
                }}
              >
                <CampaignStatus
                  active={activeCampaign.status === "Draft"}
                  border={activeCampaign.status !== "In Queue"}
                >
                  <span>Draft</span>
                </CampaignStatus>
                <CampaignStatus
                  active={activeCampaign.status === "In Queue"}
                  border={activeCampaign.status !== "Running"}
                >
                  <span>In queue</span>
                </CampaignStatus>
                <CampaignStatus
                  active={activeCampaign.status === "Running"}
                  border={activeCampaign.status !== "Closed"}
                >
                  <span>Running</span>
                </CampaignStatus>
                <CampaignStatus active={activeCampaign.status === "Closed"}>
                  <span>Closed</span>
                </CampaignStatus>
              </div>

              <CampaignName>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    marginBottom: "8px",
                  }}
                >
                  <p>Campaign name</p>
                  <Switch />
                </div>

                <Input value={activeCampaign.name} />
              </CampaignName>

              <Checkbox style={{ marginTop: "16px" }}>
                Close Campaign when all tasks are finished
              </Checkbox>

              <CustomRangePicker>
                <img src={CalendarIcon} alt="Calendar icon" />
                <WrapDatePicker>
                  <CustomDatePicker
                    allowClear={false}
                    locale={locale}
                    suffixIcon=""
                    // onChange={(value, valueString) =>
                    //   handleChangeData("start_date", valueString)
                    // }
                    // value={
                    //   campaignTaskRules.start_date &&
                    //   moment(campaignTaskRules.start_date, "DD-MM-YYYY")
                    // }
                    format={"DD-MM-YYYY"}
                  />
                  <span>-</span>
                  <CustomDatePicker
                    allowClear={false}
                    locale={locale}
                    suffixIcon=""
                    // onChange={(value, valueString) =>
                    //   handleChangeData("end_date", valueString)
                    // }
                    // value={
                    //   campaignTaskRules.end_date &&
                    //   moment(campaignTaskRules.end_date, "DD-MM-YYYY")
                    // }
                    format={"DD-MM-YYYY"}
                  />
                </WrapDatePicker>
              </CustomRangePicker>
            </TopRight>
          </Top>

          <Center></Center>

          <Bottom></Bottom>
        </Right>
      )}

      <ModalAddCampaign
        showModalCampaign={showModalCampaign}
        $showModalCampaign={$showModalCampaign}
      />
    </Wrapper>
  );
}

export default ListviewCampaign;

const Wrapper = styled.div`
  background: #fff;
  padding: 16px;
  margin-top: 16px;
  display: flex;
`;

const Left = styled.div`
  padding-right: 16px;
`;

const Filter = styled.div``;

const WrapFilter = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 8px;

  p {
    margin-bottom: 0;
    font-size: 16px;
    font-family: var(--roboto-700);
    margin-right: 16px;
    width: 18%;
  }

  .ant-select {
    width: 82%;
  }
`;

const AddCampaign = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;
  margin-left: 8px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const List = styled.div``;

const Campaign = styled.div`
  width: 359px;
  height: 127px;
  background: ${({ isActive }) => (isActive ? "#f0f0f0" : "#fff")};
  border: 1px solid #ececec;
  border-radius: 5px;
  padding: 16px;
  position: relative;
  margin-top: 8px;

  ::before {
    content: "";
    width: ${({ isActive }) => (isActive ? "3px" : "0")};
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: ${(props) => props.theme.main};
    border-radius: 50px 0px 0 50px;
    border: ${(props) =>
      props.isActive ? `1px solid ${props.theme.main}` : "none"};
  }

  :hover {
    cursor: pointer;
    ::before {
      content: "";
      width: 3px;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      background: ${(props) => props.theme.main};
      border-radius: 50px 0px 0 50px;
      border: 1px solid ${(props) => props.theme.main};
    }
  }
`;

const NameCampaign = styled(Paragraph)`
  color: ${(props) => (props.isActive ? props.theme.main : "#2c2c2c")};
  font-size: 18px;
  font-family: var(--roboto-500);
  margin-bottom: 0 !important;
`;

const TypeCampaign = styled.div`
  span {
    color: #6b6b6b;
  }
`;

const TimeCampaign = styled.div`
  display: flex;
  justify-content: space-between;

  span {
    color: #6b6b6b;
  }
`;

const StatusCampaign = styled.div`
  border-radius: 5px;
  padding: 1px 8px;
  background: ${({ color }) => (color ? color : "#fff")};
  color: #fff;
`;

const Right = styled.div`
  background: #f0f0f0;
  border-radius: 5px;
  padding: 16px;
`;

const Top = styled.div`
  display: flex;
`;

const TopLeft = styled.div``;

const RunningStatus = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #fff;
  border-radius: 5px;
  width: 323px;
  height: 400px;

  .ant-progress-inner {
    background: #f6fbff;
  }

  .ant-progress-circle-trail {
    stroke: #e2eefe;
  }
`;

const TaskStatus = styled(RunningStatus)`
  margin-left: 16px;
`;

const Percent = styled.div`
  font-family: var(--roboto-700);
  font-size: 24px;
  line-height: 30px;
  color: #252424;

  :last-child {
    line-height: 22px;
    font-size: 16px;
    font-family: var(--roboto-600);
    margin-top: 12px;
  }
`;

const TitleRunning = styled.div`
  margin: 24px 0 24px 32px;
  font-family: var(--roboto-500);
  font-size: 16px;
  line-height: 22px;
  align-self: flex-start;
`;

const WrapLabel = styled.div`
  margin-top: 27px;
  display: flex;
`;

const Label = styled.div`
  position: relative;
  margin-right: 48px;

  ::before {
    content: "";
    width: 16px;
    height: 16px;
    position: absolute;
    top: 2px;
    left: -25px;
    background: ${({ color }) => (color ? color : "#000")};
    border: 1px solid ${({ color }) => (color ? color : "#000")};
    border-radius: 4px;
  }

  :last-child {
    margin-right: 0;
  }
`;

const NumberStatus = styled.div`
  background: #fff;
  border-radius: 5px;
  width: 210px;
  height: 110px;
  padding: 16px 16px 16px 24px;
  margin-right: 16px;
  color: ${({ color }) => (color ? color : "#000")};
  display: flex;
  align-items: center;
  flex-direction: column;

  p {
    margin-bottom: 8px;
    font-size: 40px;
    font-family: var(--roboto-500);
    line-height: 48px;
  }

  span {
    font-family: var(--roboto-500);
    color: #6b6b6b;
    font-size: 16px;
    line-height: 22px;
  }

  :last-child {
    margin-right: 0;
  }
`;

const TopRight = styled.div`
  background: #fff;
  padding: 16px;
  margin-left: 16px;
`;

const CampaignStatus = styled.div`
  width: 160px;
  height: 48px;
  background: ${(props) => (props.active ? props.theme.main : "#f6fbff")};
  position: relative;
  color: ${(props) => (props.active ? "#fff" : "#8c8c8c")};
  font-size: 16px;
  font-family: var(--roboto-500);
  line-height: 22px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-right: ${({ border }) =>
    border && "1px solid rgba(89, 106, 106, 0.25)"};

  span {
    margin-left: ${(props) => (props.active ? "20px" : "0")};
  }

  :after {
    content: "";
    position: absolute;
    left: 0;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: 24px solid #f6fbff;
    border-top: 24px solid transparent;
    border-bottom: 24px solid transparent;
  }

  :before {
    content: "";
    position: absolute;
    right: -24px;
    bottom: 0;
    width: 0;
    height: 0;
    border-left: ${(props) =>
      props.active ? `24px solid ${props.theme.main}` : "none"};
    border-top: 24px solid transparent;
    border-bottom: 24px solid transparent;
    z-index: 2;
  }

  :first-child {
    :after {
      display: none;
    }
  }

  :last-child {
    border-right: none;
    :before {
      display: none;
    }
  }
`;

const CampaignName = styled.div`
  p {
    font-size: 16px;
    font-family: var(--roboto-500);
    line-height: 22px;
    margin-bottom: 0;
  }
`;

const CustomRangePicker = styled.div`
  display: flex;
  height: 20px;
  margin-top: 16px;

  img {
    margin-right: 8px;
  }

  .datePicker {
    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
      font-size: 16px;
      margin-right: 10px;
    }
  }
`;

const WrapDatePicker = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    font-size: 16px;
    margin-right: 16px;
  }
`;

const CustomDatePicker = styled(DatePicker)`
  margin-top: 4px;
  padding: 0;
  border: none;

  &:hover {
    border: none;
  }

  .ant-picker-input > input:focus {
    border: none;
  }

  .ant-picker-input > input {
    text-transform: capitalize;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #000000;
    width: fit-content;
    cursor: pointer;

    transition: all 0.5s;

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Center = styled.div``;

const Bottom = styled.div``;
