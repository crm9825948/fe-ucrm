import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Select from "antd/lib/select";

import { getTemplateCampaignTasks } from "redux/slices/campaign";
import RefreshIcon from "assets/icons/campaign/refresh.svg";
import Info from "assets/icons/campaign/info.svg";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import { FE_URL } from "constants/constants";

function ManualTasks({
  currentStep,
  type,
  campaignTaskRules,
  handleChangeData,
  selectedTemplateCampaignTasks,
  $selectedTemplateCampaignTasks,
}) {
  const dispatch = useDispatch();

  const { templateSMSCampaignTasks } = useSelector(
    (state) => state.campaignReducer
  );
  const [templates, $templates] = useState([]);

  const _onOpenNewTemplate = () => {
    window.open(`${FE_URL}/sms-template`);
  };

  const _onRefeshTemplate = () => {
    dispatch(getTemplateCampaignTasks());
  };

  const handleSelectTemplate = (value) => {
    $selectedTemplateCampaignTasks(
      templates.find((item) => item.value === value)?.body
    );
    handleChangeData("campaign_task_template_id", value);
  };

  useEffect(() => {
    dispatch(getTemplateCampaignTasks());
  }, [dispatch]);

  useEffect(() => {
    let tempTemplates = [];

    templateSMSCampaignTasks.forEach((item) => {
      tempTemplates.push({
        label: item.sms_template_name,
        value: item._id,
        body: item.content,
      });
    });
    $templates(tempTemplates);
  }, [templateSMSCampaignTasks]);

  return (
    <Wrapper currentStep={currentStep} type={type}>
      <Note>
        <img src={Info} alt="Info" />
        This template will be used when User create Member or Task manually.
      </Note>

      <WrapItem style={{ marginBottom: 8 }}>
        <TitleItem>Default template</TitleItem>
        <RefreshButton onClick={_onRefeshTemplate}>
          <img src={RefreshIcon} alt="RefreshIcon" />
        </RefreshButton>
      </WrapItem>

      <WrapItem>
        <Select
          style={{ width: "50%" }}
          placeholder="Please select"
          options={templates}
          value={campaignTaskRules.campaign_task_template_id}
          onChange={(e) => handleSelectTemplate(e)}
        />
        <NewTemplateButton onClick={_onOpenNewTemplate}>+</NewTemplateButton>
      </WrapItem>
      {selectedTemplateCampaignTasks && (
        <PreviewTemplate>
          {parse(selectedTemplateCampaignTasks, optionsParse)}
        </PreviewTemplate>
      )}
    </Wrapper>
  );
}

export default ManualTasks;

const Wrapper = styled.div`
  opacity: ${({ currentStep, type }) =>
    currentStep === 2 && type === "SMS" ? "1" : "0"};
  height: ${({ currentStep, type }) =>
    currentStep === 2 && type === "SMS" ? "100%" : "0"};
  pointer-events: ${({ currentStep, type }) =>
    currentStep === 2 && type === "SMS" ? "auto" : "none"};
  padding: ${({ currentStep, type }) =>
    currentStep === 2 && type === "SMS" ? "0 24px 24px 24px" : "0"};
`;

const Note = styled.div`
  padding: 5px 10px;
  background: #e8f5ff;
  border-radius: 7px;
  font-size: 16px;
  line-height: 19px;
  margin: 0 auto;
  width: fit-content;
  margin-bottom: 37px;

  img {
    margin-top: 4px;
  }
`;

const WrapItem = styled.div`
  display: flex;
  align-items: ${({ vertical }) => (vertical ? "flex-start" : "center")};
  flex-direction: ${({ vertical }) => (vertical ? "column" : "row")};

  &.item {
    margin-bottom: 10px;
  }

  .required {
    ::before {
      display: inline-block;
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }
`;

const TitleItem = styled.p`
  margin-bottom: 0;
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  position: relative;

  /* &.required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 8px;
      left: -8px;
    }
  } */
`;

const NewTemplateButton = styled.div`
  width: 32px;
  height: 32px;
  padding: 8px 16px;
  color: #fff;
  background: ${(props) => props.theme.main};
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;
  cursor: pointer;

  :hover {
    background: ${(props) => props.theme.darker};
  }
`;

const RefreshButton = styled.div`
  width: 32px;
  height: 32px;
  padding: 8px 16px;
  background: #fff;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;
  cursor: pointer;
`;

const PreviewTemplate = styled.div`
  margin-top: 16px;
  max-height: 400px;
  overflow: auto;
  padding: 10px;
  border: 1px solid #ececec;
  box-shadow: inset 0px 0px 26px rgba(138, 138, 138, 0.25);
  border-radius: 8px;
  background: #fff;
`;
