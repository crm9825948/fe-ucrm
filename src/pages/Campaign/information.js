import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import moment from "moment";
import { useTranslation, withTranslation } from "react-i18next";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";
import Select from "antd/lib/select";
import DatePicker from "antd/lib/date-picker";
import TimePicker from "antd/lib/time-picker";
import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import Checkbox from "antd/lib/checkbox";
import { PaperClipOutlined } from "@ant-design/icons";
import Typography from "antd/lib/typography";
import Spin from "antd/lib/spin";
import Switch from "antd/lib/switch";

import Delete from "assets/icons/common/delete.svg";
import emailIcon from "assets/icons/campaign/campaignEmail.svg";
import smsIcon from "assets/icons/campaign/campaignSMS.svg";
import CalendarIcon from "assets/icons/common/calendarIcon.svg";
import locale from "antd/es/date-picker/locale/en_US";
import defaultAvatar from "assets/images/header/avatar.png";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import { getContentCampaignMailchimp } from "redux/slices/campaign";

import ModalAddCampaign from "./ModalAddCampaign";
import { BE_URL, FE_URL } from "constants/constants";
import { week } from "util/staticData";

const { Text } = Typography;

function Information({
  currentStep,
  type,
  typeCampaign,
  selectedObjects,
  $selectedObjects,
  campaignTaskRules,
  handleChangeData,
  recordID,
  mailchimpSelected,
  $mailchimpSelected,
}) {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    listObjects,
    listCampaignMailChimp,
    contentCampaignMailchimp,
    isLoading,
  } = useSelector((state) => state.campaignReducer);
  const { userAssignTo } = useSelector((state) => state.objectsReducer);

  const [showModalCampaign, $showModalCampaign] = useState(false);
  const [modalShift, $modalShift] = useState(false);
  const [modalCampaignList, $modalCampaignList] = useState(false);
  const [dataShift, $dataShift] = useState({});

  const handleSelectObjects = (values) => {
    let temp = [];
    _.map(values, (item) => {
      const object = listObjects.find((object) => object.object_id === item);
      if (object) {
        temp.push(object);
      }
    });
    handleChangeData("object", temp);
    $selectedObjects(temp);
  };

  const _onOpenModalShift = (idx) => {
    $dataShift(idx);
    $modalShift(true);
  };

  const _onSelectCampaignTemplate = (id) => {
    $mailchimpSelected(id);
    dispatch(
      getContentCampaignMailchimp({
        mailchimp_campaign_id: id,
      })
    );
  };

  const _onSelectCampaignMailchimp = () => {
    $modalCampaignList(false);
    handleChangeData(
      "mailchimp_campaign",
      listCampaignMailChimp.find((item) => item.id === mailchimpSelected)
    );
  };

  const disabledStartDate = (current) => {
    return (
      campaignTaskRules.end_date &&
      campaignTaskRules.end_date <
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  const disabledEndDate = (current) => {
    return (
      (campaignTaskRules.start_date &&
        campaignTaskRules.start_date >
          moment(current).endOf("day").format("YYYY-MM-DD")) ||
      moment(current).endOf("day").format("YYYY-MM-DD") <
        moment().startOf("day").format("YYYY-MM-DD")
    );
  };

  const renderSchedule = (value) => {
    if (_.isEqual(value, [0, 1, 2, 3, 4, 5, 6])) {
      return <span>All week&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3, 4])) {
      return <span>Weekdays&nbsp;</span>;
    } else if (_.isEqual(value, [5, 6])) {
      return <span>Weekends&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1])) {
      return <span>Mon-Tue&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2])) {
      return <span>Mon-Wed&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3])) {
      return <span>Mon-Thu&nbsp;</span>;
    } else if (_.isEqual(value, [0, 1, 2, 3, 4, 5])) {
      return <span>Mon-Sat&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2])) {
      return <span>Tue-Wed&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3])) {
      return <span>Tue-Thu&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4])) {
      return <span>Tue-Fri&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4, 5])) {
      return <span>Tue-Sat&nbsp;</span>;
    } else if (_.isEqual(value, [1, 2, 3, 4, 5, 6])) {
      return <span>Tue-Sun&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3])) {
      return <span>Wed-Thu&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4])) {
      return <span>Wed-Fri&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4, 5])) {
      return <span>Wed-Sat&nbsp;</span>;
    } else if (_.isEqual(value, [2, 3, 4, 5, 6])) {
      return <span>Wed-Sun&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4])) {
      return <span>Thu-Fri&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4, 5])) {
      return <span>Thu-Sat&nbsp;</span>;
    } else if (_.isEqual(value, [3, 4, 5, 6])) {
      return <span>Thu-Sun&nbsp;</span>;
    } else if (_.isEqual(value, [4, 5])) {
      return <span>Fri-Sat&nbsp;</span>;
    } else if (_.isEqual(value, [4, 5, 6])) {
      return <span>Fri-Sun&nbsp;</span>;
    } else {
      let result = [];
      value.forEach((item, idx) => {
        switch (item) {
          case 0:
            if (idx === value.length - 1) {
              result.push("Mon");
            } else {
              result.push("Mon, ");
            }
            break;
          case 1:
            if (idx === value.length - 1) {
              result.push("Tue");
            } else {
              result.push("Tue, ");
            }

            break;
          case 2:
            if (idx === value.length - 1) {
              result.push("Wed");
            } else {
              result.push("Wed, ");
            }

            break;
          case 3:
            if (idx === value.length - 1) {
              result.push("Thu");
            } else {
              result.push("Thu, ");
            }

            break;
          case 4:
            if (idx === value.length - 1) {
              result.push("Fri");
            } else {
              result.push("Fri, ");
            }

            break;
          case 5:
            if (idx === value.length - 1) {
              result.push("Sat");
            } else {
              result.push("Sat, ");
            }

            break;
          case 6:
            if (idx === value.length - 1) {
              result.push("Sun");
            } else {
              result.push("Sun, ");
            }

            break;

          default:
            break;
        }
      });
      return <span>{result}&nbsp;</span>;
    }
  };

  useEffect(() => {
    if (
      !_.isEmpty(listCampaignMailChimp) &&
      campaignTaskRules.mailchimp_campaign_id === ""
    ) {
      $mailchimpSelected(_.get(listCampaignMailChimp[0], "id", ""));
      dispatch(
        getContentCampaignMailchimp({
          mailchimp_campaign_id: _.get(listCampaignMailChimp[0], "id", ""),
        })
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, listCampaignMailChimp, campaignTaskRules]);

  return (
    <Wrapper currentStep={currentStep}>
      {!recordID && (
        <WrapType>
          <Type>
            <img src={type === "Email" ? emailIcon : smsIcon} alt="icon" />
            <span>Create an {type} campaign</span>
            <p onClick={() => $showModalCampaign(true)}>Change type</p>
          </Type>

          {type === "Email" ? (
            <DesType>
              Send Email for Campaign Member from multiple sources (E.g. Lead,
              Contact). Email Campaign have to connect with another Email
              marketing service.
            </DesType>
          ) : type === "SMS" ? (
            <DesType>
              Send SMS for Campaign Member from multiple sources (E.g. Lead,
              Contact). The source object must have SMS outgoing config and SMS
              Template.
            </DesType>
          ) : type === "Telemarketing" ? (
            <DesType>
              Extract contact information from multiple CRM Objects (eg.
              Contact, Lead) then generate tasks for Agents. Agents must finish
              the tasks by themselves.
            </DesType>
          ) : (
            ""
          )}
        </WrapType>
      )}

      <WrapSetting>
        <Left>
          <div className="required">
            <Input
              value={campaignTaskRules.name}
              placeholder="Campaign name"
              onChange={(e) => handleChangeData("name", e.target.value)}
            />
          </div>
          <AssignTo>
            <p>Assign to</p>
            <Select
              placeholder="Please select"
              value={campaignTaskRules.owner}
              onChange={(value) => handleChangeData("owner", value)}
            >
              {userAssignTo &&
                userAssignTo.map((user) => {
                  return (
                    <Select.Option value={user._id} key={user._id}>
                      <WrapAssignTo>
                        <img
                          style={{
                            width: 38,
                            height: 38,
                            borderRadius: "50%",
                            marginRight: 4,
                          }}
                          src={
                            user?.avatar_config?.url
                              ? BE_URL + user.avatar_config.url
                              : defaultAvatar
                          }
                          alt="avatar"
                        />
                        <div
                          style={{
                            display: "flex",
                            flexDirection: "column",
                          }}
                        >
                          <span style={{ lineHeight: "14px" }}>
                            {user.Last_Name} {_.get(user, "Middle_Name", "")}
                            {user.First_Name}
                          </span>
                          <span
                            style={{
                              fontSize: 12,
                              color: "rgba(0, 0, 0, 0.45)",
                              lineHeight: "20px",
                            }}
                          >
                            ({_.get(user, "Email", "")})
                          </span>
                        </div>
                      </WrapAssignTo>
                    </Select.Option>
                  );
                })}
            </Select>
          </AssignTo>

          <Checkbox
            style={{
              fontSize: "16px",
              lineHeight: "22px",
              color: "#2C2C2C",
              marginBottom: "16px",
            }}
            onChange={(e) => handleChangeData("is_one_time", e.target.checked)}
            checked={campaignTaskRules.is_one_time}
          >
            Close Campaign when all tasks are finished
          </Checkbox>

          <div></div>

          <Checkbox
            style={{
              fontSize: "16px",
              lineHeight: "22px",
              color: "#2C2C2C",
              marginBottom: "16px",
            }}
            onChange={(e) =>
              handleChangeData("checking_workflow_sla", e.target.checked)
            }
            checked={campaignTaskRules.checking_workflow_sla}
          >
            Apply workflow and SLA for campaign tasks
          </Checkbox>

          {type === "Email" && (
            <>
              {_.get(campaignTaskRules, "mailchimp_campaign_id", "") ? (
                <MailChimpCampaignSelected
                  onClick={() => {
                    $modalCampaignList(true);

                    if (_.get(campaignTaskRules, "mailchimp_campaign_id", "")) {
                      $mailchimpSelected(
                        _.get(campaignTaskRules, "mailchimp_campaign_id", "")
                      );

                      dispatch(
                        getContentCampaignMailchimp({
                          mailchimp_campaign_id: _.get(
                            campaignTaskRules,
                            "mailchimp_campaign_id",
                            ""
                          ),
                        })
                      );
                    }
                  }}
                >
                  <PaperClipOutlined /> &nbsp;{" "}
                  {_.get(campaignTaskRules, "mailchimp_campaign_name", "")}
                </MailChimpCampaignSelected>
              ) : (
                <MailChimpCampaign
                  onClick={() => $modalCampaignList(true)}
                  className="required"
                >
                  <PaperClipOutlined /> &nbsp; Link to Mailchimp Campaign
                </MailChimpCampaign>
              )}
            </>
          )}

          <CustomRangePicker>
            <img src={CalendarIcon} alt="Calendar icon" />
            <WrapDatePicker>
              <CustomDatePicker
                locale={locale}
                suffixIcon=""
                disabledDate={disabledStartDate}
                onChange={(value, valueString) =>
                  handleChangeData("start_date", valueString)
                }
                value={
                  campaignTaskRules.start_date &&
                  moment(campaignTaskRules.start_date, "YYYY-MM-DD")
                }
                format={"YYYY-MM-DD"}
              />
              <span>-</span>
              <CustomDatePicker
                locale={locale}
                suffixIcon=""
                disabledDate={disabledEndDate}
                onChange={(value, valueString) =>
                  handleChangeData("end_date", valueString)
                }
                value={
                  campaignTaskRules.end_date &&
                  moment(campaignTaskRules.end_date, "YYYY-MM-DD")
                }
                format={"YYYY-MM-DD"}
              />
            </WrapDatePicker>
          </CustomRangePicker>

          <Schedule>
            <p>Schedule</p>

            {_.map(campaignTaskRules.schedule_configs, (item, index) => {
              return (
                <Shift key={index}>
                  <Button onClick={() => _onOpenModalShift(index)}>
                    {campaignTaskRules.schedule_configs[index]?.day && (
                      <>
                        {renderSchedule(
                          campaignTaskRules.schedule_configs[index]?.day
                        )}
                      </>
                    )}
                  </Button>
                  <TimePicker.RangePicker
                    onChange={(value, valueString) => {
                      handleChangeData(
                        "schedule_configs",
                        valueString,
                        "addTime",
                        index
                      );
                    }}
                    value={
                      campaignTaskRules.schedule_configs.length > 0 &&
                      campaignTaskRules.schedule_configs[index]?.time?.length >
                        0 && [
                        moment(
                          campaignTaskRules.schedule_configs[index]?.time[0],
                          "HH:mm"
                        ),
                        moment(
                          campaignTaskRules.schedule_configs[index]?.time[1],
                          "HH:mm"
                        ),
                      ]
                    }
                    allowClear={false}
                    suffixIcon=""
                    format="HH:mm"
                  />
                  <div className="action">
                    <Tooltip title="Delete">
                      <img
                        onClick={() =>
                          handleChangeData("schedule_configs", index, "delete")
                        }
                        src={Delete}
                        alt="delete"
                      />
                    </Tooltip>
                  </div>
                </Shift>
              );
            })}

            {campaignTaskRules.schedule_configs.length < 5 && (
              <AddNew
                onClick={() => handleChangeData("schedule_configs", "", "add")}
              >
                <span>+ Add new</span>
              </AddNew>
            )}
          </Schedule>
        </Left>

        <Right>
          {recordID && (
            <div style={{ display: "flex" }}>
              <Switch
                checked={
                  _.get(
                    campaignTaskRules,
                    "fld_campaign_running_status_01",
                    ""
                  ) === "Running"
                }
                onChange={(e) =>
                  handleChangeData("fld_campaign_running_status_01", e)
                }
              />
              <p style={{ margin: "0 0 0 16px" }}>Active</p>
            </div>
          )}

          <Objects>
            <p className="required">Objects</p>
            <Select
              mode="multiple"
              onChange={handleSelectObjects}
              placeholder="Please select"
              value={_.map(selectedObjects, (item) => item.object_id)}
            >
              {_.map(listObjects, (item) => (
                <>
                  {item.object_id !== "obj_crm_campaign_00001" &&
                    item.object_id !== "obj_crm_campaign_member_00001" &&
                    item.object_id !== "obj_crm_campaign_task_00001" && (
                      <Select.Option
                        key={item.object_id}
                        value={item.object_id}
                      >
                        {item.object_name}
                      </Select.Option>
                    )}
                </>
              ))}
            </Select>

            {_.map(selectedObjects, (item, index) => {
              return (
                <ObjectItem key={index}>
                  {item.object_name} &nbsp;
                  {!item.have_outgoing_config &&
                    type !== "Telemarketing" &&
                    type !== "Email" && (
                      <span
                        style={{ color: "red", cursor: "pointer" }}
                        onClick={() =>
                          window.open(`
                          ${FE_URL}${
                            typeCampaign === "Email"
                              ? "/email-outgoing"
                              : "/sms-outgoing"
                          }`)
                        }
                      >
                        (This object has no outgoing configuration!)
                      </span>
                    )}
                </ObjectItem>
              );
            })}
          </Objects>
        </Right>
      </WrapSetting>

      <ModalAddCampaign
        showModalCampaign={showModalCampaign}
        $showModalCampaign={$showModalCampaign}
        isRedirect={true}
        typeCampaign={typeCampaign}
      />

      <ModalCustom
        title="Link to Mailchimp Campaign"
        visible={modalCampaignList}
        footer={null}
        width={1130}
        onCancel={() => $modalCampaignList(false)}
      >
        {!_.isEmpty(listCampaignMailChimp) ? (
          <div style={{ display: "flex" }}>
            <LeftListMailchimp>
              {_.map(listCampaignMailChimp, (item) => (
                <Wrap
                  onClick={() => _onSelectCampaignTemplate(item.id)}
                  typeSelected={mailchimpSelected === item.id}
                  key={item.id}
                >
                  <WrapContent>
                    <Title ellipsis={{ tooltip: item.settings.title }}>
                      {item.settings.title}
                    </Title>
                    <Des ellipsis={{ tooltip: item.settings.subject_line }}>
                      Subject: <span>{item.settings.subject_line}</span>
                    </Des>
                    <Des>
                      Create date:
                      <span>
                        {moment(item.create_time).format("DD/MM/YYYY h:m A")}
                      </span>
                    </Des>
                  </WrapContent>
                </Wrap>
              ))}
            </LeftListMailchimp>

            {isLoading.contentCampaignMailchimp ? (
              <div
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                  width: "600px",
                }}
              >
                <Spin />
              </div>
            ) : (
              <>
                {_.isEmpty(
                  _.get(contentCampaignMailchimp, "archive_html", "")
                ) ? (
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                      width: "600px",
                    }}
                  >
                    <Empty>
                      <img src={EmptyObject} alt="empty" />
                      <p>
                        <span>No content</span>
                      </p>
                    </Empty>
                  </div>
                ) : (
                  <RightListMailchimp>
                    {parse(
                      _.get(contentCampaignMailchimp, "archive_html", ""),
                      optionsParse
                    )}
                  </RightListMailchimp>
                )}
              </>
            )}
          </div>
        ) : (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <p>
              <span>There is no campaign in your MailChimp Account</span>
            </p>
          </Empty>
        )}

        <WrapButton>
          {!_.isEmpty(listCampaignMailChimp) && (
            <Button type="primary" onClick={() => _onSelectCampaignMailchimp()}>
              Link to this campaign
            </Button>
          )}
          <Button onClick={() => $modalCampaignList(false)}>Cancel</Button>
        </WrapButton>
      </ModalCustom>

      <ModalCustom
        title="Shift work"
        visible={modalShift}
        width={400}
        footer={null}
        onCancel={() => $modalShift(false)}
      >
        <Item>
          <p>Giờ làm</p>
          <TimePicker.RangePicker
            onChange={(value, valueString) =>
              handleChangeData(
                "schedule_configs",
                valueString,
                "addTime",
                dataShift
              )
            }
            value={
              campaignTaskRules.schedule_configs.length > 0 &&
              campaignTaskRules.schedule_configs[dataShift]?.time?.length >
                0 && [
                moment(
                  campaignTaskRules.schedule_configs[dataShift]?.time[0],
                  "HH:mm"
                ),
                moment(
                  campaignTaskRules.schedule_configs[dataShift]?.time[1],
                  "HH:mm"
                ),
              ]
            }
            allowClear={false}
            suffixIcon=""
            format="HH:mm"
          />
        </Item>

        <Apply>
          <legend>Áp dụng</legend>
          <Checkbox.Group
            value={
              campaignTaskRules.schedule_configs.length > 0 &&
              campaignTaskRules.schedule_configs[dataShift]?.day
            }
            onChange={(value) =>
              handleChangeData("schedule_configs", value, "addDay", dataShift)
            }
          >
            {week.map((item) => {
              return (
                <Checkbox key={item.value} value={item.value}>
                  {t(item.label)}
                </Checkbox>
              );
            })}
          </Checkbox.Group>
        </Apply>

        <WrapButton>
          <Button type="primary" onClick={() => $modalShift(false)}>
            Ok
          </Button>
        </WrapButton>
      </ModalCustom>
    </Wrapper>
  );
}

export default withTranslation()(Information);

const Wrapper = styled.div`
  opacity: ${({ currentStep }) => (currentStep === 0 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 0 ? "100%" : "0")};
  pointer-events: ${({ currentStep }) => (currentStep === 0 ? "auto" : "none")};
  padding: ${({ currentStep }) =>
    currentStep === 0 ? "0 24px 24px 24px" : "0"};
  display: flex;
  flex-direction: column;
  justify-content: center;

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .required {
    ::before {
      display: inline-block;
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }
`;

const WrapType = styled.div`
  margin-bottom: 32px;
`;

const Type = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    width: 24px;
    height: 24px;
  }
  span {
    font-size: 16px;
    font-family: var(--roboto-700);
    margin-left: 8px;
    margin-right: 16px;
  }

  p {
    margin-bottom: 0;
    margin-top: 0;
    color: ${(props) => props.theme.main};

    :hover {
      text-decoration: underline;
      cursor: pointer;
    }
  }
`;

const DesType = styled.div`
  font-size: 16px;
  width: 443px;
  text-align: center;
  margin: 24px auto 0 auto;
`;

const WrapSetting = styled.div`
  display: flex;
`;

const Left = styled.div`
  flex: 1;

  .ant-input {
    width: 69%;
    margin-bottom: 16px;
    border: none;
    box-shadow: none;
    border-bottom: 1px solid #d9d9d9;
    border-radius: 0;
  }
`;

const Right = styled.div`
  flex: 1;
  padding-left: 16px;
  border-left: 1px solid rgba(0, 0, 0, 0.06);
`;

const Objects = styled.div`
  p {
    margin-bottom: 8px;
    font-size: 16px;
    font-family: var(--roboto-500);
  }

  .ant-select {
    width: 100%;
  }
`;

const AssignTo = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 16px;

  img {
    width: 38px;
    height: 38px;
    border-radius: 50%;
  }

  p {
    font-size: 16px;
    font-family: var(--roboto-500);
    margin-bottom: 8px;
  }

  .ant-select {
    width: 69%;
  }

  .ant-select-arrow {
    display: none;
  }

  .ant-select:not(.ant-select-customize-input) .ant-select-selector {
    border: none;
    height: 43px;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none;
  }
`;

const WrapAssignTo = styled.div`
  display: flex;
  align-items: center;
  padding: 0px 6px;

  &:hover {
    background: #e1e5f1;
    border-radius: 5px;
  }
`;

const CustomRangePicker = styled.div`
  display: flex;
  height: 20px;
  margin-bottom: 16px;

  img {
    margin-right: 8px;
  }

  .datePicker {
    display: flex;
    justify-content: space-between;
    align-items: center;

    span {
      font-size: 16px;
      margin-right: 10px;
    }
  }
`;

const WrapDatePicker = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  span {
    font-size: 16px;
    margin-right: 8px;
  }
`;

const CustomDatePicker = styled(DatePicker)`
  margin-top: 4px;
  padding: 0;
  border: none;

  &:hover {
    border: none;
  }

  .ant-picker-input > input:focus {
    border: none;
  }

  .ant-picker-input > input {
    text-transform: capitalize;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #000000;
    width: 120;
    cursor: pointer;

    transition: all 0.5s;

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Schedule = styled.div`
  font-size: 16px;

  p {
    margin-bottom: 8px;
    font-family: var(--roboto-500);
  }
`;

const Shift = styled.div`
  display: flex;
  position: relative;
  margin-bottom: 8px;

  .action {
    position: absolute;
    top: 1px;
    right: 17%;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    img {
      cursor: pointer;
    }
  }

  :hover .action {
    visibility: visible;
    opacity: 1;

    img {
      :hover {
        background: #eeeeee;
      }
    }
  }

  .ant-picker {
    width: 56%;
  }

  .ant-btn {
    background: #f2f8ff;
    border-right: none;
    padding: 4px 10px;
  }
`;

const AddNew = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;
  margin-top: 13px;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const ObjectItem = styled.div`
  background: #f5f5f5;
  padding: 12px 16px;
  border: 1px solid #f0f0f0;
  border-radius: 2px;
  margin: 8px 0;
`;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-picker {
    width: 100%;
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0 !important;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled.div`
  margin-bottom: 0;
  margin-top: 16px;
  display: flex;
  justify-content: flex-end;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const Apply = styled.fieldset`
  background: #fff;
  border-radius: 10px;
  height: fit-content;
  border: 1px solid #ececec;
  padding: 0 16px 16px 16px;

  .ant-checkbox-group {
    display: flex;
    flex-direction: column;
  }

  .ant-checkbox {
    :hover {
      .ant-checkbox-inner {
        border-color: ${(props) => props.theme.main}!important;
      }
    }
  }

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const Item = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  p {
    margin-bottom: 0;
    width: fit-content;
  }

  .ant-picker {
    width: 80%;
  }
`;

const MailChimpCampaign = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: ${(props) => props.theme.main};
  cursor: pointer;
  margin-bottom: 16px;
`;

const MailChimpCampaignSelected = styled(MailChimpCampaign)`
  color: #2c2c2c;
  font-family: var(--roboto-700);
`;

const LeftListMailchimp = styled.div`
  max-height: 632px;
  overflow: auto;
  padding-right: 12px;
`;

const RightListMailchimp = styled.div`
  max-height: 632px;
  overflow: auto;
  padding-left: 12px;
  padding-right: 12px;
  border: 1px solid #f0f0f0;
  padding: 16px;
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;
  padding: 16px;
  width: 425px;
  height: 109px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 8px;
  cursor: pointer;
  position: relative;
  background: ${({ typeSelected }) => (typeSelected ? "#f0f0f0" : "#fff")};

  ::before {
    content: "";
    width: ${({ typeSelected }) => (typeSelected ? "3px" : "0")};
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    background: ${(props) => props.theme.main};
    border-radius: 50px 0px 0 50px;
    border: ${(props) =>
      props.typeSelected ? `1px solid ${props.theme.main}` : "none"};
  }

  :hover {
    ::before {
      content: "";
      width: 3px;
      height: 100%;
      position: absolute;
      top: 0;
      left: 0;
      background: ${(props) => props.theme.main};
      border-radius: 50px 0px 0 50px;
      border: 1px solid ${(props) => props.theme.main};
    }
  }
`;

const WrapContent = styled.div``;

const Title = styled(Text)`
  color: #2c2c2c;
  font-size: 18px;
  line-height: 26px;
  font-family: var(--roboto-500);
  margin-bottom: 6px;
  width: 390px;
`;

const Des = styled(Text)`
  font-size: 14px;
  color: #252424;
  line-height: 20px;
  width: 390px;

  span {
    color: #6b6b6b;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
