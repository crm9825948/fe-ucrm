import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import isEmail from "validator/lib/isEmail";
import moment from "moment";
import { useNavigate, useLocation } from "react-router-dom";

import Button from "antd/lib/button";
import Collapse from "antd/lib/collapse";
import Input from "antd/lib/input";
import Tooltip from "antd/lib/tooltip";
import Select from "antd/lib/select";
import Form from "antd/lib/form";
import Checkbox from "antd/lib/checkbox";
import InputNumber from "antd/lib/input-number";
import Breadcrumb from "antd/lib/breadcrumb";
import TimePicker from "antd/lib/time-picker";
import DatePicker from "antd/lib/date-picker";
import Typography from "antd/lib/typography";
import Modal from "antd/lib/modal";

// import PlusGreen from "assets/icons/common/plus-green.svg";
import MinusSquare from "assets/icons/SLA/MinusSquare.svg";
import PlusSquare from "assets/icons/SLA/PlusSquare.svg";
import Delete from "assets/icons/common/delete.svg";

import { loadListObjectField } from "redux/slices/objects";
import { createCampaign, updateCampaign } from "redux/slices/campaign";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { getListSMSTemplate } from "redux/slices/smsTemplate";
import { loadUserAssignTo } from "redux/slices/objects";
import { loadObject } from "redux/slices/objectsManagement";

import {
  loadFieldsCampaign,
  loadFieldsCampaignTask,
} from "redux/slices/campaign";

import Conditions from "components/Conditions/conditions";
import { week } from "util/staticData";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import { t } from "i18next";

function SettingCampaign({ taskRules, recordData }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [form] = Form.useForm();
  const { Panel } = Collapse;
  const { Text } = Typography;
  const { pathname } = useLocation();

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { userDetail } = useSelector((state) => state.userReducer);
  const { userAssignTo } = useSelector((state) => state.objectsReducer);
  const { listFieldsCampaign, listFieldsCampaignTask } = useSelector(
    (state) => state.campaignReducer
  );

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { listEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );
  const { listSMSTemplate } = useSelector((state) => state.smsTemplateReducer);

  const [ruleSelect, setRuleSelect] = useState("");
  const [selectedObject, setSelectedObject] = useState([]);
  const [listFields, setListFields] = useState([]);
  const [shifts, setShifts] = useState([]);
  const [listTemplateEmail, setListTemplateEmail] = useState([]);
  const [listTemplateSMS, setListTemplateSMS] = useState([]);
  const [modalShift, setModalShift] = useState(false);
  const [dataShift, setDataShift] = useState({});
  const [listObjects, setListObjects] = useState([]);
  const [fieldsCampaign, setFieldsCampaign] = useState([]);
  const [fieldsCampaignTask, setFieldsCampaignTask] = useState([]);

  useEffect(() => {
    const tempFields = listFieldsCampaign.filter(
      (item) =>
        item.value !== "fld_campaign_total_task_01" &&
        item.value !== "fld_campaign_total_successful_task_01" &&
        item.value !== "fld_campaign_total_failure_task_01"
    );
    setFieldsCampaign(tempFields);
  }, [listFieldsCampaign]);

  useEffect(() => {
    const tempFields = listFieldsCampaignTask.filter(
      (item) =>
        item.value !== "fld_ct_task_id_01" &&
        item.value !== "fld_ct_type_01" &&
        item.value !== "fld_ct_content_string_01" &&
        item.value !== "fld_ct_send_to_01" &&
        item.value !== "fld_ct_campaign_name_01" &&
        item.value !== "fld_ct_status_01" &&
        item.value !== "fld_ct_status_code_01" &&
        item.value !== "fld_ct_error_code_01" &&
        item.value !== "fld_ct_sent_at_01"
    );
    setFieldsCampaignTask(tempFields);
  }, [listFieldsCampaignTask]);

  const [campaignTaskRules, setCampaignTaskRules] = useState({
    additional_task_fields: [],
    additional_result_fields: [],
    mapping_rules: [],
  });

  const [campaignInfo, setCampaignInfo] = useState([]);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onSubmit = (values) => {
    let checkRequired = false;
    let checkDone = false;
    const indexType = campaignInfo.findIndex(
      (item) => item.id_field === "fld_campaign_type_01"
    );

    let resultShifts = [];
    shifts.forEach((shift, idx) => {
      let applicable_weekdays = [
        "False",
        "False",
        "False",
        "False",
        "False",
        "False",
        "False",
      ];

      shift.day.forEach((item) => {
        applicable_weekdays[item] = "True";
      });
      resultShifts.push({
        from_time: {
          hour: parseInt(shift.time[0].slice(0, 2)),
          minute: parseInt(shift.time[0].slice(3, 5)),
        },
        to_time: {
          hour: parseInt(shift.time[1].slice(0, 2)),
          minute: parseInt(shift.time[1].slice(3, 5)),
        },
        applicable_weekdays: [...applicable_weekdays],
      });
    });

    if (
      (campaignInfo[indexType].value !== "Telemarketing" &&
        campaignTaskRules.mapping_rules.find(
          (item) => item.template_id === undefined
        )) ||
      campaignTaskRules.mapping_rules.find(
        (item) => Object.keys(item.task_mapping_rules).length === 0
      )
    ) {
      Notification("warning", "Please full field required!");
      checkDone = true;
    }

    if (!checkDone) {
      campaignTaskRules.mapping_rules.forEach((item) => {
        Object.entries(item.task_mapping_rules).forEach(([key, value]) => {
          if (value === "") {
            checkRequired = true;
          }
        });
      });

      if (checkRequired) {
        Notification("warning", "Please full field required!");
      }
    }
    if (!checkDone && !checkRequired) {
      //Check conditions

      let flag = false;
      campaignTaskRules.mapping_rules.forEach((ele, index) => {
        //operator
        if (
          operatorValueAnd[index] &&
          operatorValueAnd[index].operator &&
          operatorValueAnd[index].operator.length ===
            ele.filter_condition.and_filter.length
        ) {
          operatorValueAnd[index].operator.forEach((item) => {
            if (item === undefined) {
              flag = true;
            }
          });
        } else {
          flag = true;
        }
        if (
          operatorValueOr[index] &&
          operatorValueOr[index].operator &&
          operatorValueOr[index].operator.length ===
            ele.filter_condition.or_filter.length
        ) {
          operatorValueOr[index].operator.forEach((item) => {
            if (item === undefined) {
              flag = true;
            }
          });
        } else {
          flag = true;
        }
        //value
        valueAnd[index] &&
          valueAnd[index].value &&
          valueAnd[index].value.forEach((item, idx) => {
            if (
              item === "" &&
              operatorValueAnd[index].operator[idx] !== "empty" &&
              operatorValueAnd[index].operator[idx] !== "not-empty" &&
              operatorValueAnd[index].operator[idx] !== "mine" &&
              operatorValueAnd[index].operator[idx] !== "not-mine" &&
              operatorValueAnd[index].operator[idx] !== "today" &&
              operatorValueAnd[index].operator[idx] !== "yesterday" &&
              operatorValueAnd[index].operator[idx] !== "this-week" &&
              operatorValueAnd[index].operator[idx] !== "last-week" &&
              operatorValueAnd[index].operator[idx] !== "this-month" &&
              operatorValueAnd[index].operator[idx] !== "last-month" &&
              operatorValueAnd[index].operator[idx] !== "this-year"
            ) {
              flag = true;
            }
            if (item === undefined) {
              flag = true;
            }
          });
        valueOr[index] &&
          valueOr[index].value &&
          valueOr[index].value.forEach((item, idx) => {
            if (
              item === "" &&
              operatorValueOr[index].operator[idx] !== "empty" &&
              operatorValueOr[index].operator[idx] !== "not-empty" &&
              operatorValueOr[index].operator[idx] !== "mine" &&
              operatorValueOr[index].operator[idx] !== "not-mine" &&
              operatorValueOr[index].operator[idx] !== "today" &&
              operatorValueOr[index].operator[idx] !== "yesterday" &&
              operatorValueOr[index].operator[idx] !== "this-week" &&
              operatorValueOr[index].operator[idx] !== "last-week" &&
              operatorValueOr[index].operator[idx] !== "this-month" &&
              operatorValueOr[index].operator[idx] !== "last-month" &&
              operatorValueOr[index].operator[idx] !== "this-year"
            ) {
              flag = true;
            }
            if (item === undefined) {
              flag = true;
            }
          });
      });

      if (flag) {
        Notification("warning", "Please fullfill conditions!");
      } else {
        if (Object.keys(taskRules).length > 0) {
          dispatch(
            updateCampaign({
              rules: {
                campaign_task_rule: {
                  ...campaignTaskRules,
                  schedule_configs: [...resultShifts],
                  campaign_id: taskRules.campaign_id,
                },
              },
              record: {
                data: [
                  ...campaignInfo,
                  {
                    id_field: "fld_campaign_total_task_01",
                    id_field_related_record: null,
                    id_related_record: null,
                    object_related: null,
                    value: null,
                  },
                  {
                    id_field: "fld_campaign_total_successful_task_01",
                    id_field_related_record: null,
                    id_related_record: null,
                    object_related: null,
                    value: null,
                  },
                  {
                    id_field: "fld_campaign_total_failure_task_01",
                    id_field_related_record: null,
                    id_related_record: null,
                    object_related: null,
                    value: null,
                  },
                ],
                owner_id: values.owner,
                id: recordData._id,
                object_id: "obj_crm_campaign_00001",
              },
            })
          );
        } else {
          dispatch(
            createCampaign({
              owner: values.owner,
              campaign_info: [
                ...campaignInfo,
                {
                  id_field: "fld_campaign_total_task_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: null,
                },
                {
                  id_field: "fld_campaign_total_successful_task_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: null,
                },
                {
                  id_field: "fld_campaign_total_failure_task_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: null,
                },
              ],
              campaign_task_rule: {
                ...campaignTaskRules,
                schedule_configs: [...resultShifts],
              },
            })
          );
        }
      }
    }
  };

  const handleSelectObject = (value, values) => {
    setSelectedObject(values);
    let tempMappingRules = [...campaignTaskRules.mapping_rules];

    if (campaignTaskRules.mapping_rules.length === 0) {
      tempMappingRules = [
        {
          object_id: value[0],
          description: "",
          filter_condition: {
            and_filter: [],
            or_filter: [],
          },
          task_mapping_rules: {},
          template_id: undefined,
          result_mapping_rules: {},
        },
      ];
    } else {
      if (value.length === 0) {
        tempMappingRules = [];
      } else {
        let existObject = [];
        campaignTaskRules.mapping_rules.forEach((item) => {
          return existObject.push(item.object_id);
        });

        let newObject = "";
        let removedObject = null;

        if (existObject.length > value.length) {
          removedObject = existObject.findIndex(
            (object) => !value.includes(object)
          );
          if (removedObject !== null) {
            tempMappingRules.splice(removedObject, 1);
          }
        } else {
          newObject = value.find((object) => !existObject.includes(object));
          tempMappingRules = [
            ...campaignTaskRules.mapping_rules,
            {
              object_id: newObject,
              description: "",
              filter_condition: {
                and_filter: [],
                or_filter: [],
              },
              task_mapping_rules: {},
              template_id: undefined,
              result_mapping_rules: {},
            },
          ];
        }
      }
    }
    let tempRule = {
      ...campaignTaskRules,
      mapping_rules: [...tempMappingRules],
    };

    setCampaignTaskRules(tempRule);
  };

  const handleSelectRule = (value) => {
    if (value) {
      setRuleSelect(value);

      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: value,
        })
      );

      // let tempListEmail = [];
      // listEmailTemplate.forEach((item) => {
      //   if (item.object_id === value) {
      //     tempListEmail.push({
      //       label: item.email_template_name,
      //       value: item._id,
      //     });
      //   }
      // });
      // setListTemplateEmail(tempListEmail);

      // let tempListSMS = [];
      // listSMSTemplate.forEach((item) => {
      //   if (item.object_id === value) {
      //     tempListSMS.push({
      //       label: item.sms_template_name,
      //       value: item._id,
      //     });
      //   }
      // });
      // setListTemplateSMS(tempListSMS);
    }
  };

  const _onAddShift = () => {
    setShifts([...shifts, { day: [], time: [] }]);
  };

  const _onDeleteShift = (idx) => {
    let tempShifts = [...shifts];
    tempShifts.splice(idx, 1);
    setShifts(tempShifts);
  };

  const handleChangeConditions = (value, idx, type) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    switch (type) {
      case "and":
        tempMappingRules[idx] = {
          ...tempMappingRules[idx],
          filter_condition: {
            ...tempMappingRules[idx].filter_condition,
            and_filter: value,
          },
        };
        tempRules = {
          ...tempRules,
          mapping_rules: tempMappingRules,
        };
        setCampaignTaskRules(tempRules);
        break;
      case "or":
        tempMappingRules[idx] = {
          ...tempMappingRules[idx],
          filter_condition: {
            ...tempMappingRules[idx].filter_condition,
            or_filter: value,
          },
        };
        tempRules = {
          ...tempRules,
          mapping_rules: tempMappingRules,
        };
        setCampaignTaskRules(tempRules);
        break;

      default:
        break;
    }
  };

  const handleChangeTempOperator = (value, idx, type) => {
    switch (type) {
      case "operatorAnd":
        let tempOperatorValueAnd = [...operatorValueAnd];
        tempOperatorValueAnd[idx] = {
          operator: [],
        };

        tempOperatorValueAnd[idx].operator = value;
        setOperatorValueAnd(tempOperatorValueAnd);
        break;
      case "operatorOr":
        let tempOperatorValueOr = [...operatorValueOr];
        tempOperatorValueOr[idx] = {
          operator: [],
        };

        tempOperatorValueOr[idx].operator = value;
        setOperatorValueOr(tempOperatorValueOr);
        break;
      default:
        break;
    }
  };

  const handleChangeTempValue = (value, idx, type) => {
    switch (type) {
      case "valueAnd":
        let tempValueAnd = [...valueAnd];
        tempValueAnd[idx] = {
          value: "",
        };

        tempValueAnd[idx].value = value;
        setValueAnd(tempValueAnd);

        break;
      case "valueOr":
        let tempValueOr = [...valueOr];
        tempValueOr[idx] = {
          value: "",
        };

        tempValueOr[idx].value = value;
        setValueOr(tempValueOr);
        break;
      default:
        break;
    }
  };

  const hanldeSelectField = (e, options, indexRule, type) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];

    let existFields = [];
    options.map((item) => {
      return existFields.push(item.value);
    });

    if (type === "task") {
      if (
        options.length >
        Object.keys(tempMappingRules[indexRule].task_mapping_rules).length
      ) {
        if (options.length === 1) {
          options.forEach((item) => {
            tempMappingRules[indexRule] = {
              ...tempMappingRules[indexRule],
              task_mapping_rules: {
                ...tempMappingRules[indexRule].task_mapping_rules,
                [item.value]: "",
              },
            };
          });
        } else {
          let tempTaskFields = [];

          Object.keys(tempMappingRules[indexRule].task_mapping_rules).forEach(
            (key) => {
              tempTaskFields.push(key);
            }
          );

          let newField = existFields.filter(
            (item) => !tempTaskFields.includes(item)
          );
          tempMappingRules[indexRule] = {
            ...tempMappingRules[indexRule],
            task_mapping_rules: {
              ...tempMappingRules[indexRule].task_mapping_rules,
              [newField]: "",
            },
          };
        }
      } else {
        Object.keys(tempMappingRules[indexRule].task_mapping_rules).forEach(
          (key) => {
            if (!existFields.includes(key)) {
              let temp = { ...tempMappingRules[indexRule].task_mapping_rules };
              delete temp[key];

              tempMappingRules[indexRule] = {
                ...tempMappingRules[indexRule],
                task_mapping_rules: {
                  ...temp,
                },
              };
            }
          }
        );
      }
    } else {
      if (
        options.length >
        Object.keys(tempMappingRules[indexRule].result_mapping_rules).length
      ) {
        if (options.length === 1) {
          options.forEach((item) => {
            tempMappingRules[indexRule] = {
              ...tempMappingRules[indexRule],
              result_mapping_rules: {
                ...tempMappingRules[indexRule].result_mapping_rules,
                [item.value]: "",
              },
            };
          });
        } else {
          let tempTaskFields = [];

          Object.keys(tempMappingRules[indexRule].result_mapping_rules).forEach(
            (key) => {
              tempTaskFields.push(key);
            }
          );

          let newField = existFields.filter(
            (item) => !tempTaskFields.includes(item)
          );
          tempMappingRules[indexRule] = {
            ...tempMappingRules[indexRule],
            result_mapping_rules: {
              ...tempMappingRules[indexRule].result_mapping_rules,
              [newField]: "",
            },
          };
        }
      } else {
        Object.keys(tempMappingRules[indexRule].result_mapping_rules).forEach(
          (key) => {
            if (!existFields.includes(key)) {
              let temp = {
                ...tempMappingRules[indexRule].result_mapping_rules,
              };
              delete temp[key];

              tempMappingRules[indexRule] = {
                ...tempMappingRules[indexRule],
                result_mapping_rules: {
                  ...temp,
                },
              };
            }
          }
        );
      }
    }

    tempRules = {
      ...tempRules,
      mapping_rules: tempMappingRules,
    };
    setCampaignTaskRules(tempRules);
  };

  const hanldeSelectValueField = (value, field, indexRule, type) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = campaignTaskRules.mapping_rules;

    if (type === "task") {
      tempMappingRules[indexRule] = {
        ...tempMappingRules[indexRule],
        task_mapping_rules: {
          ...tempMappingRules[indexRule].task_mapping_rules,
          [field]: value,
        },
      };
    } else {
      tempMappingRules[indexRule] = {
        ...tempMappingRules[indexRule],
        result_mapping_rules: {
          ...tempMappingRules[indexRule].result_mapping_rules,
          [field]: value,
        },
      };
    }

    tempRules = {
      ...tempRules,
      mapping_rules: tempMappingRules,
    };
    setCampaignTaskRules(tempRules);
  };

  const handleChangeValueInfo = (value, index) => {
    let tempCampaignInfo = [...campaignInfo];

    tempCampaignInfo[index] = {
      ...tempCampaignInfo[index],
      value: value,
    };
    setCampaignInfo(tempCampaignInfo);
  };

  const handleFieldType = (field, index) => {
    switch (field.type) {
      case "select":
        return (
          <Select
            placeholder="Please select"
            options={field.option}
            onChange={(e) => handleChangeValueInfo(e, index)}
          />
        );
      case "date":
      case "datetime-local":
        return (
          <DatePicker
            showTime={field.type === "datetime-local"}
            onChange={(date, dateString) =>
              handleChangeValueInfo(dateString, index)
            }
          />
        );
      case "email":
        return (
          <Input
            placeholder="Please input"
            onChange={(e) => handleChangeValueInfo(e.target.value, index)}
          />
        );
      case "number":
        return (
          <InputNumber
            placeholder="Please input"
            onChange={(e) => handleChangeValueInfo(e, index)}
          />
        );
      default:
        return (
          <Input
            placeholder="Please input"
            onChange={(e) => handleChangeValueInfo(e.target.value, index)}
          />
        );
    }
  };

  const hanldeChangeDescription = (value, indexRule) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    tempMappingRules[indexRule] = {
      ...tempMappingRules[indexRule],
      description: value,
    };
    tempRules = {
      ...tempRules,
      mapping_rules: tempMappingRules,
    };
    setCampaignTaskRules(tempRules);
  };

  const handleSelectTemplate = (value, indexRule) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    tempMappingRules[indexRule] = {
      ...tempMappingRules[indexRule],
      template_id: value,
    };
    tempRules = {
      ...tempRules,
      mapping_rules: tempMappingRules,
    };
    setCampaignTaskRules(tempRules);
  };

  const handleSelectShift = (value, idx, type) => {
    let temp = [...shifts];

    if (type === "time") {
      temp[idx] = {
        ...temp[idx],
        time: value,
      };
    } else {
      temp[idx] = {
        ...temp[idx],
        day: value,
      };
    }
    setShifts(temp);
  };

  const _onOpenModalShift = (idx) => {
    setDataShift(idx);
    setModalShift(true);
  };

  const _onCloseModal = () => {
    setModalShift(false);
  };

  const _onOpenNewTemplate = () => {
    if (
      campaignInfo[
        campaignInfo.findIndex(
          (item) => item.id_field === "fld_campaign_type_01"
        )
      ]?.value === "Email"
    ) {
      window.open(`${FE_URL}/email-template`);
    } else if (
      campaignInfo[
        campaignInfo.findIndex(
          (item) => item.id_field === "fld_campaign_type_01"
        )
      ]?.value === "SMS"
    ) {
      window.open(`${FE_URL}/sms-template`);
    }
  };

  const _onRefeshTemplate = () => {
    dispatch(getListEmailTemplate());
    dispatch(getListSMSTemplate());
  };

  useEffect(() => {
    let tempFields = [];
    if (fieldsCampaign.length > 0) {
      fieldsCampaign.map((field) => {
        return tempFields.push({
          id_field: field.value,
          id_field_related_record: null,
          id_related_record: null,
          object_related: null,
          value: null,
        });
      });
    }

    if (Object.keys(recordData).length > 0) {
      Object.entries(recordData).forEach(([key, val]) => {
        let index = tempFields.findIndex((field) => field.id_field === key);
        if (index !== -1) {
          tempFields[index] = {
            ...tempFields[index],
            value: val.value,
          };

          if (val.value) {
            form.setFieldsValue({
              [key]:
                fieldsCampaign.find((item) => item.value === key).type ===
                "date"
                  ? moment(val.value)
                  : fieldsCampaign.find((item) => item.value === key).type ===
                    "datetime-local"
                  ? moment(val.value)
                  : val.value,
            });
          }
        }
      });
    }
    setCampaignInfo(tempFields);
  }, [form, fieldsCampaign, recordData]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      let tempListObjects = tempObjects.filter(
        (object) =>
          object.value !== "obj_crm_campaign_task_00001" &&
          object.value !== "obj_crm_campaign_00001"
      );
      setListObjects(tempListObjects);
    }
  }, [category]);

  useEffect(() => {
    dispatch(getListEmailTemplate());
    dispatch(getListSMSTemplate());

    if (pathname.split("/").includes("add-new-campaign")) {
      dispatch(loadObject());
      dispatch(loadUserAssignTo());
      dispatch(
        loadFieldsCampaign({
          api_version: "2",
          object_id: "obj_crm_campaign_00001",
        })
      );
      dispatch(
        loadFieldsCampaignTask({
          api_version: "2",
          object_id: "obj_crm_campaign_task_00001",
        })
      );
    }
  }, [dispatch, pathname]);

  useEffect(() => {
    let tempOptionsFields = [];

    listObjectField.forEach((item) => {
      if (Object.values(item)[0].readable || Object.values(item)[0].writeable) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });
    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (shifts.length > 0) {
      shifts.forEach((item, idx) => {
        if (item.time.length > 0) {
          form.setFieldsValue({
            ["shift" + idx]: [
              moment(item.time[0], "HH:mm"),
              moment(item.time[1], "HH:mm"),
            ],
          });
        } else {
          form.setFieldsValue({
            ["shift" + idx]: [undefined, undefined],
          });
        }
      });
    }
  }, [shifts, form]);

  useEffect(() => {
    if (Object.keys(taskRules).length > 0) {
      setCampaignTaskRules({
        additional_task_fields: [],
        additional_result_fields: [],
        mapping_rules: [...taskRules.mapping_rules],
      });

      let listObject = [];

      let tempOperatorValueAnd = [];
      let tempOperatorValueOr = [];

      taskRules.mapping_rules.forEach((rule, idx) => {
        let tempObject = listObjects.filter(
          (object) => object.value === rule.object_id
        );
        if (tempObject) {
          listObject.push(...tempObject);
        }

        tempOperatorValueAnd[idx] = {
          operator: [],
        };

        tempOperatorValueOr[idx] = {
          operator: [],
        };

        rule.filter_condition.and_filter.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueAnd[idx].operator = [
                ...tempOperatorValueAnd[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        rule.filter_condition.or_filter.forEach((item) => {
          Object.entries(item).forEach(([key, val]) => {
            if (key === "value") {
              tempOperatorValueOr[idx].operator = [
                ...tempOperatorValueOr[idx].operator,
                Object.keys(val)[0],
              ];
            }
          });
        });

        setOperatorValueAnd(tempOperatorValueAnd);
        setOperatorValueOr(tempOperatorValueOr);
      });

      let tempListValue = [];
      listObject.forEach((item) => {
        tempListValue.push(item.value);
      });

      form.setFieldsValue({
        objects: tempListValue,
      });

      if (Object.keys(recordData).length > 0) {
        form.setFieldsValue({
          owner: recordData.owner,
        });
      }

      setSelectedObject(listObject);
      setShifts(taskRules.schedule_configs);
    }
  }, [taskRules, listObjects, recordData, form]);

  useEffect(() => {
    if (Object.keys(taskRules).length === 0) {
      form.setFieldsValue({
        owner: userDetail._id,
      });
    }
  }, [form, taskRules, userDetail._id]);

  useEffect(() => {
    if (ruleSelect !== "") {
      let tempListEmail = [];
      listEmailTemplate.forEach((item) => {
        if (item.object_id === ruleSelect) {
          tempListEmail.push({
            label: item.email_template_name,
            value: item._id,
          });
        }
      });
      setListTemplateEmail(tempListEmail);

      let tempListSMS = [];
      listSMSTemplate.forEach((item) => {
        if (item.object_id === ruleSelect) {
          tempListSMS.push({
            label: item.sms_template_name,
            value: item._id,
          });
        }
      });
      setListTemplateSMS(tempListSMS);
    }
  }, [listEmailTemplate, listSMSTemplate, ruleSelect]);

  return (
    <Wrapper isEdit={Object.keys(taskRules).length > 0}>
      {Object.keys(taskRules).length === 0 && (
        <Breadcrumb>
          <Breadcrumb.Item>Objects</Breadcrumb.Item>
          <Breadcrumb.Item
            onClick={() =>
              navigate("/objects/obj_crm_campaign_00001/default-view")
            }
          >
            Campaign list
          </Breadcrumb.Item>
          <BreadcrumbItem>Add new</BreadcrumbItem>
        </Breadcrumb>
      )}
      <Form
        form={form}
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 20 }}
        labelAlign="left"
        onFinish={_onSubmit}
      >
        <Content>
          <Left>
            <Form.Item
              label="Assign To"
              name="owner"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              rules={[{ required: true, message: "Please select" }]}
            >
              <Select placeholder="Please select">
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Select.Option value={user._id} key={user._id}>
                        {user.First_Name} {user.Middle_Name} {user.Last_Name}
                      </Select.Option>
                    );
                  })}
              </Select>
            </Form.Item>

            {fieldsCampaign.map((item, idx) => {
              return (
                <Form.Item
                  label={
                    <Text ellipsis={{ tooltip: item.label }}>{item.label}</Text>
                  }
                  name={item.value}
                  labelCol={{ span: 7 }}
                  wrapperCol={{ span: 16, offset: 1 }}
                  rules={[
                    {
                      required: item.required,
                      message: "Please full field required!",
                    },
                    () => ({
                      validator(_, value) {
                        if (value && item.type === "email") {
                          if (isEmail(value) === false) {
                            return Promise.reject(new Error("Invalid Email"));
                          }
                        }
                        return Promise.resolve();
                      },
                    }),
                  ]}
                  key={idx}
                >
                  {handleFieldType(item, idx)}
                </Form.Item>
              );
            })}

            <Schedule>
              <TitleItem type="schedule">Schedule: </TitleItem>
              <WrapShifts>
                {shifts.length > 0 &&
                  shifts.map((item, index) => {
                    return (
                      <Shift key={index}>
                        <Form.Item
                          label={
                            <Button onClick={() => _onOpenModalShift(index)}>
                              Time {index + 1}
                            </Button>
                          }
                          name={"shift" + index}
                          labelCol={{ span: 4 }}
                          wrapperCol={{ span: 20 }}
                        >
                          <TimePicker.RangePicker
                            onChange={(time, timeString) =>
                              handleSelectShift(timeString, index, "time")
                            }
                            allowClear={false}
                            suffixIcon=""
                            format="HH:mm"
                          />
                        </Form.Item>
                        <div className="action">
                          <Tooltip title="Delete">
                            <img
                              onClick={() => _onDeleteShift(index)}
                              src={Delete}
                              alt="delete"
                            />
                          </Tooltip>
                        </div>
                      </Shift>
                    );
                  })}
                {shifts.length < 5 && (
                  <AddNew onClick={() => _onAddShift()}>
                    {/* <img src={PlusGreen} alt="plus" /> */}
                    <span>+ Add new</span>
                  </AddNew>
                )}
              </WrapShifts>
            </Schedule>
          </Left>

          <Right>
            <Form.Item label="Objects" name="objects">
              <Select
                placeholder="Please select"
                mode="multiple"
                options={listObjects}
                onChange={handleSelectObject}
              />
            </Form.Item>

            {campaignTaskRules.mapping_rules.length > 0 && (
              <Collapse
                accordion
                expandIcon={({ isActive }) =>
                  isActive ? (
                    <img src={MinusSquare} alt="minus" />
                  ) : (
                    <img src={PlusSquare} alt="plus" />
                  )
                }
                onChange={handleSelectRule}
              >
                {campaignTaskRules.mapping_rules.map((rule, idx) => {
                  return (
                    <Panel
                      header={
                        selectedObject.find(
                          (object) => object.value === rule.object_id
                        )?.label
                      }
                      key={rule.object_id}
                    >
                      <WrapItem>
                        <TitleItem>Description</TitleItem>
                        <Input
                          value={rule.description}
                          placeholder="Please input"
                          onChange={(e) =>
                            hanldeChangeDescription(e.target.value, idx)
                          }
                        />
                      </WrapItem>

                      <Wrap>
                        <WrapConditions>
                          <legend>Filter conditions</legend>
                          <Conditions
                            title={"AND condition"}
                            decs={"(All conditions must be met - Max 10)"}
                            conditions={rule.filter_condition.and_filter}
                            setConditions={(value) =>
                              handleChangeConditions(value, idx, "and")
                            }
                            ID={
                              Object.keys(taskRules).length > 0 && taskRules._id
                                ? taskRules._id
                                : ""
                            }
                            dataDetails={
                              Object.keys(taskRules).length > 0 &&
                              rule.filter_condition.and_conditions?.length > 0
                                ? rule.filter_condition.and_conditions
                                : {}
                            }
                            operatorValue={operatorValueAnd[idx]?.operator}
                            setOperatorValue={(value) =>
                              handleChangeTempOperator(
                                value,
                                idx,
                                "operatorAnd"
                              )
                            }
                            value={valueAnd[idx]?.value}
                            setValue={(value) =>
                              handleChangeTempValue(value, idx, "valueAnd")
                            }
                          />
                          <Conditions
                            title={"OR condition"}
                            decs={"(Any conditions must be met - Max 10)"}
                            conditions={rule.filter_condition.or_filter}
                            setConditions={(value) =>
                              handleChangeConditions(value, idx, "or")
                            }
                            ID={
                              Object.keys(taskRules).length > 0 && taskRules._id
                                ? taskRules._id
                                : ""
                            }
                            dataDetails={
                              Object.keys(taskRules)?.length > 0 &&
                              rule.filter_condition.or_conditions?.length > 0
                                ? rule.filter_condition.or_conditions
                                : {}
                            }
                            operatorValue={operatorValueOr[idx]?.operator}
                            setOperatorValue={(value) =>
                              handleChangeTempOperator(value, idx, "operatorOr")
                            }
                            value={valueOr[idx]?.value}
                            setValue={(value) =>
                              handleChangeTempValue(value, idx, "valueOr")
                            }
                          />
                        </WrapConditions>

                        <WrapField>
                          <TitleItem className="required">Send to</TitleItem>
                          <Select
                            placeholder="Please select"
                            options={listFields}
                            value={
                              Object.values(rule.task_mapping_rules)[
                                Object.keys(rule.task_mapping_rules).findIndex(
                                  (item) => item === "fld_ct_send_to_01"
                                )
                              ] !== ""
                                ? Object.values(rule.task_mapping_rules)[
                                    Object.keys(
                                      rule.task_mapping_rules
                                    ).findIndex(
                                      (item) => item === "fld_ct_send_to_01"
                                    )
                                  ]
                                : undefined
                            }
                            onChange={(e) =>
                              hanldeSelectValueField(
                                e,
                                "fld_ct_send_to_01",
                                idx,
                                "task"
                              )
                            }
                          />
                        </WrapField>

                        {fieldsCampaignTask.length > 0 && (
                          <WrapTaskFields>
                            <legend>Select additional fields</legend>
                            <WrapItem className="item">
                              <TitleItem className="required">
                                Select fields
                              </TitleItem>
                              <Select
                                placeholder="Please select"
                                options={fieldsCampaignTask}
                                mode="multiple"
                                value={Object.keys(rule.task_mapping_rules)}
                                onChange={(e, option) =>
                                  hanldeSelectField(e, option, idx, "task")
                                }
                              />
                            </WrapItem>

                            {Object.keys(rule.task_mapping_rules).length > 0 &&
                              fieldsCampaignTask
                                .filter((item) =>
                                  Object.keys(rule.task_mapping_rules).includes(
                                    item.value
                                  )
                                )
                                .map((field) => {
                                  return (
                                    <WrapField key={field.value}>
                                      <TitleItem>{field.label}</TitleItem>
                                      <Select
                                        placeholder="Please select"
                                        options={listFields}
                                        value={
                                          Object.values(
                                            rule.task_mapping_rules
                                          )[
                                            Object.keys(
                                              rule.task_mapping_rules
                                            ).findIndex(
                                              (item) => item === field.value
                                            )
                                          ] !== ""
                                            ? Object.values(
                                                rule.task_mapping_rules
                                              )[
                                                Object.keys(
                                                  rule.task_mapping_rules
                                                ).findIndex(
                                                  (item) => item === field.value
                                                )
                                              ]
                                            : undefined
                                        }
                                        onChange={(e) =>
                                          hanldeSelectValueField(
                                            e,
                                            field.value,
                                            idx,
                                            "task"
                                          )
                                        }
                                      />
                                    </WrapField>
                                  );
                                })}
                          </WrapTaskFields>
                        )}

                        {/* <WrapResultFields>
                          <legend>Mapping result field</legend>
                          <WrapItem className="item">
                            <TitleItem>Select fields</TitleItem>
                            <Select
                              placeholder="Please select"
                              options={fieldsCampaignTask}
                              mode="multiple"
                              onChange={(e, option) =>
                                hanldeSelectField(e, option, idx, "result")
                              }
                            />
                          </WrapItem>

                          {Object.keys(rule.result_mapping_rules).length > 0 &&
                            fieldsCampaignTask
                              .filter((item) =>
                                Object.keys(rule.result_mapping_rules).includes(
                                  item.value
                                )
                              )
                              .map((field) => {
                                return (
                                  <WrapField key={field.value}>
                                    <TitleItem>{field.label}</TitleItem>
                                    <Select
                                      placeholder="Please select"
                                      options={listFields}
                                      onChange={(e) =>
                                        hanldeSelectValueField(
                                          e,
                                          field.value,
                                          idx,
                                          "result"
                                        )
                                      }
                                    />
                                  </WrapField>
                                );
                              })}
                        </WrapResultFields> */}

                        {campaignInfo[
                          campaignInfo.findIndex(
                            (item) => item.id_field === "fld_campaign_type_01"
                          )
                        ]?.value !== "Telemarketing" && (
                          <WrapItem>
                            <TitleItem className="required">Template</TitleItem>
                            <Select
                              placeholder="Please select type campaign and then select template"
                              options={
                                campaignInfo[
                                  campaignInfo.findIndex(
                                    (item) =>
                                      item.id_field === "fld_campaign_type_01"
                                  )
                                ]?.value === "Email"
                                  ? listTemplateEmail
                                  : campaignInfo[
                                      campaignInfo.findIndex(
                                        (item) =>
                                          item.id_field ===
                                          "fld_campaign_type_01"
                                      )
                                    ]?.value === "SMS"
                                  ? listTemplateSMS
                                  : []
                              }
                              value={rule.template_id}
                              onChange={(e) => handleSelectTemplate(e, idx)}
                            />
                            <NewTemplateButton onClick={_onOpenNewTemplate}>
                              + New template
                            </NewTemplateButton>
                            <RefreshButton onClick={_onRefeshTemplate}>
                              Refresh
                            </RefreshButton>
                          </WrapItem>
                        )}
                      </Wrap>
                    </Panel>
                  );
                })}
              </Collapse>
            )}
          </Right>

          <ModalCustom
            title="Shift work"
            visible={modalShift}
            width={400}
            footer={null}
            onCancel={_onCloseModal}
          >
            <Form.Item
              label="Giờ làm"
              name={"shift" + dataShift}
              labelCol={{ span: 4 }}
              wrapperCol={{ span: 20 }}
            >
              <TimePicker.RangePicker
                onChange={(time, timeString) =>
                  handleSelectShift(timeString, dataShift, "time")
                }
                allowClear={false}
                suffixIcon=""
                format="HH:mm"
              />
            </Form.Item>
            <Apply>
              <legend>Áp dụng</legend>
              <Checkbox.Group
                value={shifts.length > 0 && shifts[dataShift]?.day}
                onChange={(e) => handleSelectShift(e, dataShift, "day")}
              >
                {week.map((item, index) => {
                  return (
                    <Checkbox key={index} value={item.value}>
                      {t(item.label)}
                    </Checkbox>
                  );
                })}
              </Checkbox.Group>
            </Apply>

            <WrapButton label=" " colon={false}>
              <Button type="primary" onClick={_onCloseModal}>
                Ok
              </Button>
            </WrapButton>
          </ModalCustom>
        </Content>

        <WrapButtonSave
          label=" "
          colon={false}
          labelCol={{ span: 0 }}
          wrapperCol={{ span: 24 }}
        >
          <Button
            onClick={() =>
              navigate("/objects/obj_crm_campaign_00001/default-view")
            }
          >
            Cancel
          </Button>
          <Button type="primary" htmlType="submit">
            Save
          </Button>
        </WrapButtonSave>
      </Form>
    </Wrapper>
  );
}

export default SettingCampaign;

SettingCampaign.defaultProps = {
  taskRules: {},
  recordData: {},
};

const Wrapper = styled.div`
  padding: ${({ isEdit }) => (isEdit ? "0" : "24px")};

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-form {
    margin-top: 16px;
  }

  .ant-form-item-label > label {
    font-size: 16px;
    font-family: var(--roboto-700);
    color: #2c2c2c;
  }

  .ant-collapse-header {
    background: #e3e3e3;
    padding: 2px 16px !important;
    border: 1px solid #ececec;
    align-items: center !important;
    font-size: 16px;
    color: #252424;
    font-family: var(--roboto-500);

    :hover {
      border: 1px solid ${(props) => props.theme.main};

      .action {
        visibility: visible;
        opacity: 1;

        img {
          :hover {
            background: #eeeeee;
          }
        }
      }

      .action-edit {
        visibility: visible;
        opacity: 1;
      }
    }
  }

  .ant-collapse-item {
    margin-bottom: 8px;
    border-bottom: none;
  }

  .ant-collapse {
    border: none;
    background: #fff;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
`;

const Content = styled.div`
  display: flex;
`;

const Left = styled.div`
  flex: 1;
  margin-right: 16px;
  padding: 24px 16px;
  background: #fff;
  height: fit-content;

  .ant-form-item {
    margin-bottom: 10px;
  }

  .ant-picker {
    width: 100%;
  }

  .ant-input-number {
    width: 100%;
  }
`;

const Right = styled.div`
  flex: 2;
  padding: 24px;
  background: #fff;
  height: fit-content;
`;

const WrapItem = styled.div`
  display: flex;
  align-items: center;

  .ant-select {
    flex: 1;
  }

  &.item {
    margin-bottom: 10px;
  }
`;

const WrapField = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;

  .ant-select {
    flex: 1;
  }
`;

const TitleItem = styled.p`
  margin-bottom: 0;
  width: ${({ type }) => (type === "schedule" ? "33.33%" : "20%")};
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  position: relative;

  &.required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 8px;
      left: -8px;
    }
  }
`;

const Wrap = styled.div`
  padding: 16px;
  border: 1px solid #ececec;
  margin-top: 24px;
`;

const WrapConditions = styled.fieldset`
  padding: 8px 24px 16px 24px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }

  .conditions__title {
    font-size: 14px;

    span {
      color: #ffa940;
    }
  }
`;

const WrapTaskFields = styled(WrapConditions)`
  .ant-select {
    flex: 1;
  }
`;

// const WrapResultFields = styled(WrapConditions)`
//   .ant-select {
//     flex: 1;
//   }
// `;

const Schedule = styled.div`
  display: flex;
  flex: 1;
`;

const WrapShifts = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
`;

const Shift = styled.div`
  display: flex;
  position: relative;

  .action {
    position: absolute;
    top: 4px;
    right: 15px;
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    img {
      cursor: pointer;
    }
  }

  :hover .action {
    visibility: visible;
    opacity: 1;

    img {
      :hover {
        background: #eeeeee;
      }
    }
  }

  .ant-picker {
    width: 100%;
  }

  .ant-form-item {
    width: 100%;
  }

  .ant-btn {
    background: #f2f8ff;
    width: 100%;
    border-right: none;
    padding: 4px 10px;
  }

  .ant-form-item-label > label.ant-form-item-no-colon::after {
    display: none;
  }
`;

const AddNew = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-picker {
    width: 100%;
  }
`;

const Apply = styled.fieldset`
  background: #fff;
  border-radius: 10px;
  height: fit-content;
  border: 1px solid #ececec;
  padding: 0 16px 16px 16px;

  .ant-checkbox-group {
    display: flex;
    flex-direction: column;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox {
    :hover {
      .ant-checkbox-inner {
        border-color: ${(props) => props.theme.main}!important;
      }
    }
  }

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;
  margin-top: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const WrapButtonSave = styled(WrapButton)`
  padding: 24px 16px;
  background: #ffffff;

  .ant-btn {
    width: 130px;
  }

  .ant-btn-primary {
    margin-left: 16px;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: center;
  }
`;

const NewTemplateButton = styled(Button)`
  margin: 0 16px;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;

    span {
      color: #fff !important;
    }
  }

  :active {
    background: #fff;
    border-color: #d9d9d9;

    span {
      color: #000;
    }
  }

  :focus {
    background: #fff;
    border-color: #d9d9d9;

    span {
      color: #000;
    }
  }
`;

const RefreshButton = styled(NewTemplateButton)`
  margin: 0;
`;
