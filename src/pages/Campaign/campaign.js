import styled from "styled-components";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Button from "antd/lib/button";
import Table from "antd/lib/table";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";

import { loadCampaign } from "redux/slices/campaign";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { Notification } from "components/Notification/Noti";
import ModalAddCampaign from "./ModalAddCampaign";
import ListviewCampaign from "./listviewCampaign";

function Campaign(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;

  const { listCampaign } = useSelector((state) => state.campaignReducer);

  const [showModalCampaign, $showModalCampaign] = useState(false);

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  useEffect(() => {
    dispatch(
      loadCampaign({
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [currentPage, dispatch, recordPerPage]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>Campaign</BreadcrumbItem>
      </Breadcrumb>

      {listCampaign.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>Campaign</span>
          </p>
          <AddButton onClick={() => $showModalCampaign(true)}>
            + Add Campaign
          </AddButton>
        </Empty>
      ) : (
        <ListviewCampaign listCampaign={listCampaign} />
      )}

      <ModalAddCampaign
        showModalCampaign={showModalCampaign}
        $showModalCampaign={$showModalCampaign}
      />
    </Wrapper>
  );
}

export default withTranslation()(Campaign);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
    border-radius: 50%;
  }
`;
