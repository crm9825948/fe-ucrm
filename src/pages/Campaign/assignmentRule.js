import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Select from "antd/lib/select";
import Table from "pages/AssignmentRule/Table/table";

import { getListAssignmentRule } from "redux/slices/assignmentRule";
import ModalAssignmentRule from "pages/AssignmentRule/ModalAssignmentRule";

function AssignmentRule({ currentStep, type }) {
  const dispatch = useDispatch();

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listAllGroups } = useSelector((state) => state.groupReducer);
  const { listAssignmentRule } = useSelector(
    (state) => state.assignmentRuleReducer
  );

  const [showModalAssignmentRule, $showModalAssignmentRule] = useState(false);
  const [objectAssignmentRule, $objectAssignmentRule] = useState(
    "obj_crm_campaign_task_00001"
  );
  const [listObjects, $listObjects] = useState([]);

  const loadListAssignment = (value) => {
    $objectAssignmentRule(value);
    dispatch(
      getListAssignmentRule({
        _id: value,
      })
    );
  };

  useEffect(() => {
    dispatch(
      getListAssignmentRule({
        _id: "obj_crm_campaign_task_00001",
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  return (
    <Wrapper currentStep={currentStep} type={type}>
      <WrapItem>
        <TitleItem style={{ marginRight: 8 }}>Object</TitleItem>
        <Select
          style={{ width: "30%" }}
          placeholder="Please select"
          options={[
            {
              label: "Campaign Task",
              value: "obj_crm_campaign_task_00001",
            },
            {
              label: "Campaign Member",
              value: "obj_crm_campaign_member_00001",
            },
          ]}
          value={objectAssignmentRule}
          onChange={(e) => loadListAssignment(e)}
        />
        <NewTemplateButton onClick={() => $showModalAssignmentRule(true)}>
          + Add assignment rule
        </NewTemplateButton>
      </WrapItem>

      {listAssignmentRule.length > 0 && (
        <WrapTable>
          <Table
            objectID={objectAssignmentRule}
            listObjects={listObjects}
            listRecord={listAssignmentRule}
            listAllGroups={listAllGroups}
            setShowModalAdd={$showModalAssignmentRule}
          />
        </WrapTable>
      )}

      <ModalAssignmentRule
        showModalAdd={showModalAssignmentRule}
        setShowModalAdd={$showModalAssignmentRule}
        objects={category}
        listGroups={listAllGroups}
        objectID={objectAssignmentRule}
      />
    </Wrapper>
  );
}

export default AssignmentRule;

const Wrapper = styled.div`
  opacity: ${({ currentStep, type }) =>
    currentStep === 2 && type === "Telemarketing" ? "1" : "0"};
  height: ${({ currentStep, type }) =>
    currentStep === 2 && type === "Telemarketing" ? "100%" : "0"};
  pointer-events: ${({ currentStep, type }) =>
    currentStep === 2 && type === "Telemarketing" ? "auto" : "none"};
  padding: ${({ currentStep, type }) =>
    currentStep === 2 && type === "Telemarketing" ? "0 24px 24px 24px" : "0"};
`;

const WrapItem = styled.div`
  display: flex;
  padding: 0 16px 0 16px;
  align-items: ${({ vertical }) => (vertical ? "flex-start" : "center")};
  flex-direction: ${({ vertical }) => (vertical ? "column" : "row")};

  &.item {
    margin-bottom: 10px;
  }

  .required {
    ::before {
      display: inline-block;
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }
`;

const TitleItem = styled.p`
  margin-bottom: 0;
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  position: relative;

  /* &.required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 8px;
      left: -8px;
    }
  } */
`;

const NewTemplateButton = styled.div`
  width: fit-content;
  padding: 8px 16px;
  height: 32px;
  color: #fff;
  background: ${(props) => props.theme.main};
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;
  cursor: pointer;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
  border-radius: 2px;

  :hover {
    background: ${(props) => props.theme.darker};
  }
`;

const WrapTable = styled.div`
  width: 100%;

  .table-header th {
    width: 100% !important;
  }

  .table-body td {
    width: 100% !important;
    min-width: 250px !important;
  }
`;
