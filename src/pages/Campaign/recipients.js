import styled from "styled-components/macro";
import { useState, useEffect } from "react";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import Tabs from "antd/lib/tabs";
import Select from "antd/lib/select";

import RefreshIcon from "assets/icons/campaign/refresh.svg";

import {
  getListEmailTemplate,
  getEmailTempateByIdSuccess,
} from "redux/slices/emailTemplate";
import {
  getListSMSTemplate,
  getSMSTemplateByIdSuccess,
} from "redux/slices/smsTemplate";
import { loadListObjectField } from "redux/slices/objects";

import Conditions from "components/Conditions/conditions";
import { FE_URL } from "constants/constants";

function Recipients({
  currentStep,
  type,
  typeCampaign,
  selectedObjects,
  campaignTaskRules,
  $campaignTaskRules,
  ruleSelected,
  $ruleSelected,
  handleChangeData,
  operatorValueAnd,
  setOperatorValueAnd,
  operatorValueOr,
  setOperatorValueOr,
  valueAnd,
  setValueAnd,
  valueOr,
  setValueOr,
  templates,
  $templates,
  recordID,
  listFieldsCampaignMember,
}) {
  const { TabPane } = Tabs;
  const dispatch = useDispatch();

  const { listEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );
  const { listSMSTemplate } = useSelector((state) => state.smsTemplateReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  // const { listFieldsCampaign, listFieldsCampaignTask } = useSelector(
  //   (state) => state.campaignReducer
  // );

  // const [dataRule, $dataRule] = useState({
  //   object_id: "",
  //   description: "",
  //   filter_condition: {
  //     and_filter: [],
  //     or_filter: [],
  //   },
  //   member_mapping_rules: {
  //     fld_cm_send_to_01: "",
  //   },
  //   template_id: "",
  // });

  const [listTemplateEmail, setListTemplateEmail] = useState([]);
  const [listTemplateSMS, setListTemplateSMS] = useState([]);
  const [listFields, $listFields] = useState([]);
  // const [fieldsCampaignTask, $fieldsCampaignTask] = useState([]);

  const handleChangeTab = (value) => {
    $ruleSelected(value);
    dispatch(getEmailTempateByIdSuccess({}));
    dispatch(getSMSTemplateByIdSuccess({}));
    // $dataRule(
    //   campaignTaskRules.mapping_rules.find((item) => item.object_id === value)
    // );
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
      })
    );
  };

  const handleChangeTempOperator = (value, idx, type) => {
    switch (type) {
      case "operatorAnd":
        let tempOperatorValueAnd = [...operatorValueAnd];
        tempOperatorValueAnd[idx] = {
          operator: [],
        };

        tempOperatorValueAnd[idx].operator = value;
        setOperatorValueAnd(tempOperatorValueAnd);
        break;
      case "operatorOr":
        let tempOperatorValueOr = [...operatorValueOr];
        tempOperatorValueOr[idx] = {
          operator: [],
        };

        tempOperatorValueOr[idx].operator = value;
        setOperatorValueOr(tempOperatorValueOr);
        break;
      default:
        break;
    }
  };

  const handleChangeTempValue = (value, idx, type) => {
    switch (type) {
      case "valueAnd":
        let tempValueAnd = [...valueAnd];
        tempValueAnd[idx] = {
          value: "",
        };

        tempValueAnd[idx].value = value;
        setValueAnd(tempValueAnd);

        break;
      case "valueOr":
        let tempValueOr = [...valueOr];
        tempValueOr[idx] = {
          value: "",
        };

        tempValueOr[idx].value = value;
        setValueOr(tempValueOr);
        break;
      default:
        break;
    }
  };

  const handleChangeConditions = (value, idx, type) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    switch (type) {
      case "and":
        tempMappingRules[idx] = {
          ...tempMappingRules[idx],
          filter_condition: {
            ...tempMappingRules[idx].filter_condition,
            and_filter: value,
          },
        };
        tempRules = {
          ...tempRules,
          mapping_rules: tempMappingRules,
        };
        $campaignTaskRules(tempRules);
        break;
      case "or":
        tempMappingRules[idx] = {
          ...tempMappingRules[idx],
          filter_condition: {
            ...tempMappingRules[idx].filter_condition,
            or_filter: value,
          },
        };
        tempRules = {
          ...tempRules,
          mapping_rules: tempMappingRules,
        };
        $campaignTaskRules(tempRules);
        break;

      default:
        break;
    }
  };

  const handleSelectTemplate = (value, indexRule) => {
    let tempRules = { ...campaignTaskRules };
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    tempMappingRules[indexRule] = {
      ...tempMappingRules[indexRule],
      template_id: value,
    };
    tempRules = {
      ...tempRules,
      mapping_rules: tempMappingRules,
    };
    $campaignTaskRules(tempRules);

    switch (typeCampaign) {
      case "Email":
        const findTemplate = listEmailTemplate.find(
          (item) => item._id === value
        );

        if (findTemplate) {
          $templates((prev) => {
            let temp = [...prev];
            const findIndex = prev.findIndex(
              (item) => item.object_id === findTemplate.object_id
            );
            if (findIndex !== -1) {
              temp[findIndex] = {
                ...temp[findIndex],
                body: findTemplate.body,
              };
            } else {
              temp.push({
                object_id: findTemplate.object_id,
                body: findTemplate.body,
              });
            }
            return temp;
          });
        }
        break;

      case "SMS":
        const findTemplateSMS = listSMSTemplate.find(
          (item) => item._id === value
        );

        if (findTemplateSMS) {
          $templates((prev) => {
            let temp = [...prev];
            const findIndex = prev.findIndex(
              (item) => item.object_id === findTemplateSMS.object_id
            );
            if (findIndex !== -1) {
              temp[findIndex] = {
                ...temp[findIndex],
                body: findTemplateSMS.content,
              };
            } else {
              temp.push({
                object_id: findTemplateSMS.object_id,
                body: findTemplateSMS.content,
              });
            }
            return temp;
          });
        }

        break;

      default:
        break;
    }
  };

  const _onOpenNewTemplate = () => {
    if (typeCampaign === "Email") {
      window.open(`${FE_URL}/email-template`);
    } else if (typeCampaign === "SMS") {
      window.open(`${FE_URL}/sms-template`);
    }
  };

  const _onRefeshTemplate = () => {
    switch (typeCampaign) {
      case "Email":
        dispatch(getListEmailTemplate());
        break;

      case "SMS":
        dispatch(getListSMSTemplate());
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    if (ruleSelected !== "") {
      switch (typeCampaign) {
        case "Email":
          let tempListEmail = [];
          listEmailTemplate.forEach((item) => {
            if (item.object_id === ruleSelected) {
              tempListEmail.push({
                label: item.email_template_name,
                value: item._id,
                object_id: item.object_id,
                body: item.body,
              });
            }
          });
          setListTemplateEmail(tempListEmail);
          break;

        case "SMS":
          let tempListSMS = [];
          listSMSTemplate.forEach((item) => {
            if (item.object_id === ruleSelected) {
              tempListSMS.push({
                label: item.sms_template_name,
                value: item._id,
                object_id: item.object_id,
                body: item.content,
              });
            }
          });
          setListTemplateSMS(tempListSMS);
          break;

        default:
          break;
      }
    }
  }, [ruleSelected, listEmailTemplate, listSMSTemplate, typeCampaign]);

  // useEffect(() => {
  //   if (ruleSelected) {
  //     switch (typeCampaign) {
  //       case "email":
  //       case "Email":
  //         if (listTemplateEmail.length > 0) {
  //           const findTemplate = listTemplateEmail.find(
  //             (item) => item.object_id === ruleSelected
  //           );

  //           if (findTemplate) {
  //             $templates((prev) => {
  //               let temp = [
  //                 ...prev,
  //                 {
  //                   object_id: ruleSelected,
  //                   body: findTemplate.body,
  //                 },
  //               ];
  //               return temp;
  //             });
  //           }
  //         }
  //         break;

  //       case "sms":
  //       case "SMS":
  //         if (listTemplateSMS.length > 0) {
  //           const findTemplate = listTemplateSMS.find(
  //             (item) => item.object_id === ruleSelected
  //           );

  //           if (findTemplate) {
  //             $templates((prev) => {
  //               let temp = [
  //                 ...prev,
  //                 {
  //                   object_id: ruleSelected,
  //                   body: findTemplate.content,
  //                 },
  //               ];
  //               return temp;
  //             });
  //           }
  //         }
  //         break;

  //       default:
  //         break;
  //     }
  //   }
  // }, [
  //   $templates,
  //   listTemplateEmail,
  //   listTemplateSMS,
  //   ruleSelected,
  //   typeCampaign,
  // ]);

  useEffect(() => {
    let tempOptionsFields = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (field.hidden === false && field.permission_hidden === false) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });
    $listFields(tempOptionsFields);
  }, [listObjectField]);

  // useEffect(() => {
  //   const tempFields = listFieldsCampaignTask.filter(
  //     (item) =>
  //       item.value !== "fld_ct_task_id_01" &&
  //       item.value !== "fld_ct_type_01" &&
  //       item.value !== "fld_ct_content_string_01" &&
  //       item.value !== "fld_ct_send_to_01" &&
  //       item.value !== "fld_ct_campaign_name_01" &&
  //       item.value !== "fld_ct_status_01" &&
  //       item.value !== "fld_ct_status_code_01" &&
  //       item.value !== "fld_ct_error_code_01" &&
  //       item.value !== "fld_ct_sent_at_01"
  //   );
  //   $fieldsCampaignTask(tempFields);
  // }, [listFieldsCampaignTask]);

  return (
    <Wrapper currentStep={currentStep}>
      <Tabs onChange={handleChangeTab}>
        {_.map(selectedObjects, (item, idx) => {
          return (
            <>
              {campaignTaskRules.mapping_rules.length > 0 && (
                <TabPane tab={item.object_name} key={item.object_id}>
                  <Wrap>
                    <Left>
                      <WrapConditions>
                        <p style={{ marginBottom: 16 }}>Filter conditions</p>
                        <Conditions
                          title={"AND condition"}
                          decs={"(All conditions must be met - Max 10)"}
                          conditions={
                            campaignTaskRules.mapping_rules[idx]
                              ?.filter_condition?.and_filter
                          }
                          setConditions={(value) =>
                            handleChangeConditions(value, idx, "and")
                          }
                          ID={recordID ? recordID : ""}
                          dataDetails={
                            campaignTaskRules.mapping_rules[idx]
                              ?.filter_condition?.and_conditions?.length > 0
                              ? campaignTaskRules.mapping_rules[idx]
                                  ?.filter_condition?.and_conditions
                              : {}
                          }
                          operatorValue={operatorValueAnd[idx]?.operator}
                          setOperatorValue={(value) =>
                            handleChangeTempOperator(value, idx, "operatorAnd")
                          }
                          value={valueAnd[idx]?.value}
                          setValue={(value) =>
                            handleChangeTempValue(value, idx, "valueAnd")
                          }
                        />
                        <Conditions
                          title={"OR condition"}
                          decs={"(Any conditions must be met - Max 10)"}
                          conditions={
                            campaignTaskRules.mapping_rules[idx]
                              ?.filter_condition?.or_filter
                          }
                          setConditions={(value) =>
                            handleChangeConditions(value, idx, "or")
                          }
                          ID={recordID ? recordID : ""}
                          dataDetails={
                            campaignTaskRules.mapping_rules[idx]
                              ?.filter_condition?.or_conditions?.length > 0
                              ? campaignTaskRules.mapping_rules[idx]
                                  ?.filter_condition?.or_conditions
                              : {}
                          }
                          operatorValue={operatorValueOr[idx]?.operator}
                          setOperatorValue={(value) =>
                            handleChangeTempOperator(value, idx, "operatorOr")
                          }
                          value={valueOr[idx]?.value}
                          setValue={(value) =>
                            handleChangeTempValue(value, idx, "valueOr")
                          }
                        />
                      </WrapConditions>

                      {type === "Telemarketing" ? (
                        <></>
                      ) : (
                        <>
                          <WrapItem style={{ marginBottom: 8 }}>
                            <TitleItem className="required">Template</TitleItem>
                            <RefreshButton onClick={_onRefeshTemplate}>
                              <img src={RefreshIcon} alt="RefreshIcon" />
                            </RefreshButton>
                          </WrapItem>

                          {type === "Email" && currentStep === 1 ? (
                            <PreviewTemplate>
                              {parse(
                                campaignTaskRules.mailchimp_campaign_content
                                  .archive_html,
                                optionsParse
                              )}
                            </PreviewTemplate>
                          ) : (
                            <>
                              <WrapItem>
                                <Select
                                  style={{ width: "50%" }}
                                  placeholder="Please select"
                                  options={
                                    typeCampaign === "Email"
                                      ? listTemplateEmail
                                      : typeCampaign === "SMS"
                                      ? listTemplateSMS
                                      : []
                                  }
                                  value={
                                    campaignTaskRules.mapping_rules[idx]
                                      .template_id
                                  }
                                  onChange={(e) => handleSelectTemplate(e, idx)}
                                />
                                <NewTemplateButton onClick={_onOpenNewTemplate}>
                                  +
                                </NewTemplateButton>
                              </WrapItem>

                              {templates.find(
                                (item) => item.object_id === ruleSelected
                              ) && (
                                <PreviewTemplate>
                                  {parse(
                                    templates.find(
                                      (item) => item.object_id === ruleSelected
                                    )?.body,
                                    optionsParse
                                  )}
                                </PreviewTemplate>
                              )}
                            </>
                          )}
                        </>
                      )}
                    </Left>

                    <Right>
                      <WrapItem vertical={true} style={{ marginBottom: 8 }}>
                        <TitleItem
                          style={{ marginBottom: 8 }}
                          className="required"
                        >
                          First name
                        </TitleItem>
                        <Select
                          style={{ width: "100%" }}
                          options={listFields}
                          placeholder="Please select"
                          onChange={(value) =>
                            handleChangeData(
                              "member_mapping_rules",
                              value,
                              "fld_cm_member_first_name_01",
                              idx
                            )
                          }
                          value={
                            campaignTaskRules.mapping_rules[idx]
                              .member_mapping_rules.fld_cm_member_first_name_01
                          }
                        />
                      </WrapItem>
                      <WrapItem vertical={true} style={{ marginBottom: 8 }}>
                        <TitleItem style={{ marginBottom: 8 }}>
                          Last name
                        </TitleItem>
                        <Select
                          style={{ width: "100%" }}
                          options={listFields}
                          placeholder="Please select"
                          allowClear
                          onChange={(value) =>
                            handleChangeData(
                              "member_mapping_rules",
                              value,
                              "fld_cm_member_last_name_01",
                              idx
                            )
                          }
                          value={
                            campaignTaskRules.mapping_rules[idx]
                              .member_mapping_rules.fld_cm_member_last_name_01
                          }
                        />
                      </WrapItem>
                      <WrapItem vertical={true} style={{ marginBottom: 8 }}>
                        <TitleItem
                          style={{ marginBottom: 8 }}
                          className="required"
                        >
                          {type === "Telemarketing" ? "Call to" : "Send to"}
                        </TitleItem>
                        <Select
                          style={{ width: "100%" }}
                          options={
                            type === "Email"
                              ? listFields.filter(
                                  (item) => item.type === "email"
                                )
                              : listFields.filter(
                                  (item) =>
                                    item.type === "text" ||
                                    item.type === "number"
                                )
                          }
                          placeholder="Please select"
                          onChange={(value) =>
                            handleChangeData(
                              "member_mapping_rules",
                              value,
                              "fld_cm_send_to_01",
                              idx
                            )
                          }
                          value={
                            campaignTaskRules.mapping_rules[idx]
                              .member_mapping_rules.fld_cm_send_to_01
                          }
                        />
                      </WrapItem>

                      {listFieldsCampaignMember.length > 0 && (
                        <>
                          <WrapItem vertical={true} style={{ marginBottom: 8 }}>
                            <TitleItem style={{ marginBottom: 8 }}>
                              Member additional fields
                            </TitleItem>
                            <Select
                              style={{ width: "100%" }}
                              mode="multiple"
                              placeholder="Please select"
                              options={listFieldsCampaignMember}
                              onChange={(value) => {
                                handleChangeData(
                                  "member_mapping_rules",
                                  "",
                                  value,
                                  idx
                                );
                              }}
                            />
                          </WrapItem>

                          {Object.keys(
                            campaignTaskRules.mapping_rules[idx]
                              .member_mapping_rules
                          ).length > 0 &&
                            Object.entries(
                              campaignTaskRules.mapping_rules[idx]
                                .member_mapping_rules
                            )
                              .filter(([key, val]) => !key.includes("_01"))
                              .map(([key, val]) => {
                                return (
                                  <WrapField key={key}>
                                    <TitleItem>
                                      {
                                        listFieldsCampaignMember.find(
                                          (item) => item.value === key
                                        )?.label
                                      }
                                    </TitleItem>
                                    <Select
                                      placeholder="Please select"
                                      options={listFields}
                                      onChange={(value) =>
                                        handleChangeData(
                                          "member_mapping_rules",
                                          value,
                                          key,
                                          idx
                                        )
                                      }
                                    />
                                  </WrapField>
                                );
                              })}
                        </>
                      )}
                    </Right>
                  </Wrap>
                </TabPane>
              )}
            </>
          );
        })}
      </Tabs>
    </Wrapper>
  );
}

export default Recipients;

const Wrapper = styled.div`
  opacity: ${({ currentStep }) => (currentStep === 1 ? "1" : "0")};
  height: ${({ currentStep }) => (currentStep === 1 ? "100%" : "0")};
  pointer-events: ${({ currentStep }) => (currentStep === 1 ? "auto" : "none")};
  padding: ${({ currentStep }) =>
    currentStep === 1 ? "0 16px 16px 16px" : "0"};

  .ant-tabs-nav {
    margin-bottom: 0;
  }

  .ant-tabs-nav-wrap {
    background: #fff;
    border-radius: 10px 10px 0 0;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(props) => props.theme.main} !important;
  }

  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }

  .ant-tabs-tab-btn:hover {
    color: ${(props) => props.theme.darker};
    background: #e6f7ff;
  }
`;

const Wrap = styled.div`
  margin-top: 16px;
  display: flex;
`;

const Left = styled.div`
  flex: 2;
  padding-right: 16px;
`;

const Right = styled.div`
  flex: 1;
  border-left: 1px solid rgba(0, 0, 0, 0.06);
  padding-left: 16px;
`;

const WrapConditions = styled.div`
  padding: 8px 0 16px 0;
  border-radius: 5px;
  margin-bottom: 24px;

  p {
    font-size: 16px;
    font-family: var(--roboto-500);
  }

  .conditions__title {
    font-size: 14px;

    span {
      color: #ffa940;
    }
  }
`;

const WrapItem = styled.div`
  display: flex;
  align-items: ${({ vertical }) => (vertical ? "flex-start" : "center")};
  flex-direction: ${({ vertical }) => (vertical ? "column" : "row")};

  &.item {
    margin-bottom: 10px;
  }

  .required {
    ::before {
      display: inline-block;
      margin-right: 4px;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
    }
  }
`;

const TitleItem = styled.p`
  margin-bottom: 0;
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  position: relative;

  /* &.required {
    ::after {
      position: absolute;
      color: #ff4d4f;
      font-size: 14px;
      line-height: 1;
      content: "*";
      top: 8px;
      left: -8px;
    }
  } */
`;

const NewTemplateButton = styled.div`
  width: 32px;
  height: 32px;
  padding: 8px 16px;
  color: #fff;
  background: ${(props) => props.theme.main};
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;
  cursor: pointer;

  :hover {
    background: ${(props) => props.theme.darker};
  }
`;

const RefreshButton = styled.div`
  width: 32px;
  height: 32px;
  padding: 8px 16px;
  background: #fff;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-left: 16px;
  cursor: pointer;
`;

const PreviewTemplate = styled.div`
  margin-top: 16px;
  max-height: 400px;
  overflow: auto;
  padding: 10px;
  border: 1px solid #ececec;
  box-shadow: inset 0px 0px 26px rgba(138, 138, 138, 0.25);
  border-radius: 8px;
  background: #fff;
`;

const WrapField = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 10px;

  p {
    width: 30%;
  }

  .ant-select {
    width: 70%;
  }
`;
