import { useState, useRef, useEffect } from "react";
import styled from "styled-components/macro";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Steps from "antd/lib/steps";
import Button from "antd/lib/button";

import {
  loadFieldsCampaignMember,
  loadObjects,
  validateObjects,
  validateObjectsResult,
  createCampaign,
  loadDetailsCampaign,
  updateCampaign,
  unMountCampaignSetting,
  getListCampaignMailChimp,
  getContentCampaignMailchimp,
  // loadFieldsCampaign,
  // loadFieldsCampaignTask,
} from "redux/slices/campaign";
import { getListEmailTemplate } from "redux/slices/emailTemplate";
import { getListSMSTemplate } from "redux/slices/smsTemplate";
import { loadUserAssignTo } from "redux/slices/objects";
import { loadListObjectField } from "redux/slices/objects";
import { loadAllGroups } from "redux/slices/group";

import Information from "./information";
import Recipients from "./recipients";
import AssignmentRule from "./assignmentRule";
import ManualTasks from "./manualTasks";
import { Notification } from "components/Notification/Noti";

function SettingCampaign(props) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { Step } = Steps;
  const { action, type, recordID } = useParams();

  const { userDetail } = useSelector((state) => state.userReducer);
  const { validateObject, detailsCampaign, contentCampaignMailchimp } =
    useSelector((state) => state.campaignReducer);
  const { listObjects, listFieldsCampaignMember, templateSMSCampaignTasks } =
    useSelector((state) => state.campaignReducer);
  const { listEmailTemplate } = useSelector(
    (state) => state.emailTemplateReducer
  );
  const { listSMSTemplate } = useSelector((state) => state.smsTemplateReducer);

  const pageRef = useRef(null);
  const [currentStep, $currentStep] = useState(0);
  const [selectedObjects, $selectedObjects] = useState([]);
  const [ruleSelected, $ruleSelected] = useState("");
  const [typeCampaign, $typeCampaign] = useState("");
  const [templates, $templates] = useState([]);
  const [mailchimpSelected, $mailchimpSelected] = useState("");
  const [selectedTemplateCampaignTasks, $selectedTemplateCampaignTasks] =
    useState("");

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const [campaignTaskRules, $campaignTaskRules] = useState({
    name: "",
    end_date: undefined,
    start_date: undefined,
    type: "",
    is_one_time: false,
    checking_workflow_sla: false,
    additional_result_fields: [],
    additional_task_fields: [],
    mapping_rules: [],
    schedule_configs: [],
    owner: undefined,
    fld_campaign_running_status_01: "Running",
    mailchimp_campaign_id: "",
    mailchimp_campaign_name: "",
    mailchimp_audience_id: "",
    mailchimp_campaign_content: {
      archive_html: "",
    },
    campaign_task_template_id: undefined,
  });

  const _onSubmit = () => {
    let resultShifts = [];
    campaignTaskRules.schedule_configs.forEach((shift) => {
      let applicable_weekdays = [
        "False",
        "False",
        "False",
        "False",
        "False",
        "False",
        "False",
      ];

      shift.day.forEach((item) => {
        applicable_weekdays[item] = "True";
      });
      resultShifts.push({
        from_time: {
          hour: parseInt(shift.time[0].slice(0, 2)),
          minute: parseInt(shift.time[0].slice(3, 5)),
        },
        to_time: {
          hour: parseInt(shift.time[1].slice(0, 2)),
          minute: parseInt(shift.time[1].slice(3, 5)),
        },
        applicable_weekdays: [...applicable_weekdays],
      });
    });

    let flag = false;
    campaignTaskRules.mapping_rules.forEach((ele, index) => {
      //operator
      if (
        operatorValueAnd[index] &&
        operatorValueAnd[index].operator &&
        operatorValueAnd[index].operator.length ===
          ele.filter_condition.and_filter.length
      ) {
        operatorValueAnd[index].operator.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }
      if (
        operatorValueOr[index] &&
        operatorValueOr[index].operator &&
        operatorValueOr[index].operator.length ===
          ele.filter_condition.or_filter.length
      ) {
        operatorValueOr[index].operator.forEach((item) => {
          if (item === undefined) {
            flag = true;
          }
        });
      } else {
        flag = true;
      }
      //value
      valueAnd[index] &&
        valueAnd[index].value &&
        valueAnd[index].value.forEach((item, idx) => {
          if (
            item === "" &&
            operatorValueAnd[index].operator[idx] !== "empty" &&
            operatorValueAnd[index].operator[idx] !== "not-empty" &&
            operatorValueAnd[index].operator[idx] !== "mine" &&
            operatorValueAnd[index].operator[idx] !== "not-mine" &&
            operatorValueAnd[index].operator[idx] !== "today" &&
            operatorValueAnd[index].operator[idx] !== "not-today" &&
            operatorValueAnd[index].operator[idx] !== "yesterday" &&
            operatorValueAnd[index].operator[idx] !== "this-week" &&
            operatorValueAnd[index].operator[idx] !== "last-week" &&
            operatorValueAnd[index].operator[idx] !== "this-month" &&
            operatorValueAnd[index].operator[idx] !== "last-month" &&
            operatorValueAnd[index].operator[idx] !== "this-year"
          ) {
            flag = true;
          }
          if (item === undefined) {
            flag = true;
          }
        });
      valueOr[index] &&
        valueOr[index].value &&
        valueOr[index].value.forEach((item, idx) => {
          if (
            item === "" &&
            operatorValueOr[index].operator[idx] !== "empty" &&
            operatorValueOr[index].operator[idx] !== "not-empty" &&
            operatorValueOr[index].operator[idx] !== "mine" &&
            operatorValueOr[index].operator[idx] !== "not-mine" &&
            operatorValueOr[index].operator[idx] !== "today" &&
            operatorValueOr[index].operator[idx] !== "not-today" &&
            operatorValueOr[index].operator[idx] !== "yesterday" &&
            operatorValueOr[index].operator[idx] !== "this-week" &&
            operatorValueOr[index].operator[idx] !== "last-week" &&
            operatorValueOr[index].operator[idx] !== "this-month" &&
            operatorValueOr[index].operator[idx] !== "last-month" &&
            operatorValueOr[index].operator[idx] !== "this-year"
          ) {
            flag = true;
          }
          if (item === undefined) {
            flag = true;
          }
        });
    });

    let checkRequired = false;
    campaignTaskRules.mapping_rules.forEach((ele) => {
      Object.entries(ele.member_mapping_rules).find(([key, val]) => {
        if (
          (key === "fld_cm_member_first_name_01" ||
            key === "fld_cm_send_to_01") &&
          !val
        ) {
          checkRequired = true;
        }
        return checkRequired;
      });

      if (
        ((!recordID && type !== "Telemarketing") ||
          (recordID && campaignTaskRules.type !== "Telemarketing")) &&
        ((!recordID && type !== "Email") ||
          (recordID && campaignTaskRules.type !== "Email")) &&
        !ele.template_id
      ) {
        checkRequired = true;
      }
    });

    let checkRequiredDate = false;
    if (
      (campaignTaskRules.start_date && !campaignTaskRules.end_date) ||
      (!campaignTaskRules.start_date && campaignTaskRules.end_date)
    ) {
      checkRequiredDate = true;
    }

    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else if (checkRequired) {
      Notification("warning", "Please fullfill required!");
    } else if (checkRequiredDate) {
      Notification(
        "warning",
        "Please fullfill or remove start date and end date!"
      );
    } else {
      if (recordID) {
        dispatch(
          updateCampaign({
            campaign_info: {
              data: [
                {
                  id_field: "fld_campaign_name_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: campaignTaskRules.name,
                },
                {
                  id_field: "fld_campaign_type_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: campaignTaskRules.type,
                },
                {
                  id_field: "fld_campaign_status_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: _.get(
                    detailsCampaign.campaignInfo.fld_campaign_status_01,
                    "value",
                    "Draft"
                  ),
                },
                {
                  id_field: "fld_campaign_start_date_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: campaignTaskRules.start_date,
                },
                {
                  id_field: "fld_campaign_end_date_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: campaignTaskRules.end_date,
                },
                {
                  id_field: "fld_campaign_running_status_01",
                  id_field_related_record: null,
                  id_related_record: null,
                  object_related: null,
                  value: campaignTaskRules.fld_campaign_running_status_01,
                },
              ],
              owner_id: campaignTaskRules.owner,
              object_id: "obj_crm_campaign_00001",
              id: _.get(detailsCampaign.campaignInfo, "_id", ""),
            },
            campaign_config: {
              is_one_time: campaignTaskRules.is_one_time,
              checking_workflow_sla: campaignTaskRules.checking_workflow_sla,
              mapping_rules: campaignTaskRules.mapping_rules,
              mailchimp_campaign_id: campaignTaskRules.mailchimp_campaign_id,
              mailchimp_campaign_name:
                campaignTaskRules.mailchimp_campaign_name,
              mailchimp_audience_id: campaignTaskRules.mailchimp_audience_id,
              schedule_configs: resultShifts,
              campaign_id: recordID,
              campaign_task_template_id:
                campaignTaskRules.campaign_task_template_id,
            },
          })
        );
      } else {
        dispatch(
          createCampaign({
            campaign_info: [
              {
                id_field: "fld_campaign_name_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: campaignTaskRules.name,
              },
              {
                id_field: "fld_campaign_type_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: type,
              },
              {
                id_field: "fld_campaign_status_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: "Draft",
              },
              {
                id_field: "fld_campaign_start_date_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: campaignTaskRules.start_date,
              },
              {
                id_field: "fld_campaign_end_date_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: campaignTaskRules.end_date,
              },
              {
                id_field: "fld_campaign_running_status_01",
                id_field_related_record: null,
                id_related_record: null,
                object_related: null,
                value: campaignTaskRules.fld_campaign_running_status_01,
              },
            ],
            campaign_config: {
              is_one_time: campaignTaskRules.is_one_time,
              checking_workflow_sla: campaignTaskRules.checking_workflow_sla,
              mapping_rules: campaignTaskRules.mapping_rules,
              mailchimp_campaign_id: campaignTaskRules.mailchimp_campaign_id,
              mailchimp_campaign_name:
                campaignTaskRules.mailchimp_campaign_name,
              mailchimp_audience_id: campaignTaskRules.mailchimp_audience_id,
              schedule_configs: resultShifts,
              campaign_task_template_id:
                campaignTaskRules.campaign_task_template_id,
            },
            owner: campaignTaskRules.owner,
          })
        );
      }
    }
  };

  const _onPrev = () => {
    $currentStep(currentStep - 1);
    pageRef.current?.scrollIntoView();
  };

  const _onNext = () => {
    switch (currentStep) {
      case 0:
        if (typeCampaign === "Email") {
          let tempObjects = [];

          _.map(selectedObjects, (item) => {
            tempObjects.push(item.object_id);
          });

          if (
            tempObjects.length === 0 ||
            campaignTaskRules.name === "" ||
            campaignTaskRules.mailchimp_campaign_id === ""
          ) {
            Notification("warning", "Please fullfill required!");
          } else {
            $currentStep(currentStep + 1);
            pageRef.current?.scrollIntoView();

            dispatch(
              loadListObjectField({
                api_version: "2",
                object_id: campaignTaskRules.mapping_rules[0].object_id,
              })
            );

            $ruleSelected(selectedObjects[0].object_id);
          }
        } else {
          if (validateObject) {
            $currentStep(currentStep + 1);
            pageRef.current?.scrollIntoView();

            dispatch(
              loadListObjectField({
                api_version: "2",
                object_id: campaignTaskRules.mapping_rules[0].object_id,
              })
            );

            $ruleSelected(selectedObjects[0].object_id);
          } else {
            let tempObjects = [];

            _.map(selectedObjects, (item) => {
              tempObjects.push(item.object_id);
            });

            if (tempObjects.length === 0 || campaignTaskRules.name === "") {
              Notification("warning", "Please fullfill required!");
            } else {
              dispatch(
                validateObjects({
                  campaign_type: typeCampaign,
                  campaign_object_ids: tempObjects,
                })
              );
            }
          }
        }

        break;
      case 1:
        let checkRequired = false;
        campaignTaskRules.mapping_rules.forEach((ele) => {
          Object.entries(ele.member_mapping_rules).find(([key, val]) => {
            if (
              (key === "fld_cm_member_first_name_01" ||
                key === "fld_cm_send_to_01") &&
              !val
            ) {
              checkRequired = true;
            }
            return checkRequired;
          });
        });

        if (checkRequired) {
          Notification("warning", "Please fullfill required!");
        } else {
          $currentStep(currentStep + 1);
        }
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    if (validateObject) {
      $currentStep((prev) => {
        let tempStep = prev + 1;
        return tempStep;
      });
      pageRef.current?.scrollIntoView();

      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: selectedObjects[0].object_id,
        })
      );

      $ruleSelected(selectedObjects[0].object_id);
    }
  }, [selectedObjects, dispatch, validateObject, typeCampaign]);

  const handleChangeData = (type, value, typeAction, index) => {
    let tempMappingRules = [...campaignTaskRules.mapping_rules];
    let tempShifts = [...campaignTaskRules.schedule_configs];

    switch (type) {
      case "name":
      case "owner":
      case "is_one_time":
      case "checking_workflow_sla":
      case "campaign_task_template_id":
        $campaignTaskRules({
          ...campaignTaskRules,
          [type]: value,
        });
        break;

      case "start_date":
      case "end_date":
        if (value) {
          $campaignTaskRules({
            ...campaignTaskRules,
            [type]: value,
          });
        } else {
          $campaignTaskRules({
            ...campaignTaskRules,
            [type]: undefined,
          });
        }
        break;

      case "schedule_configs":
        if (typeAction === "add") {
          $campaignTaskRules({
            ...campaignTaskRules,
            schedule_configs: [
              ...campaignTaskRules.schedule_configs,
              { day: [0, 1, 2, 3, 4], time: [] },
            ],
          });
        } else if (typeAction === "delete") {
          tempShifts.splice(value, 1);
          $campaignTaskRules({
            ...campaignTaskRules,
            schedule_configs: tempShifts,
          });
        } else if (typeAction === "addTime") {
          tempShifts[index] = {
            ...tempShifts[index],
            time: value,
          };

          $campaignTaskRules({
            ...campaignTaskRules,
            schedule_configs: tempShifts,
          });
        } else if (typeAction === "addDay") {
          tempShifts[index] = {
            ...tempShifts[index],
            day: value,
          };

          $campaignTaskRules({
            ...campaignTaskRules,
            schedule_configs: tempShifts,
          });
        }

        break;

      case "object":
        dispatch(validateObjectsResult(false));

        _.map(value, (item) => {
          const existObject = tempMappingRules.find(
            (rule) => rule.object_id === item.object_id
          );
          if (!existObject) {
            tempMappingRules.push({
              object_id: item.object_id,
              description: "",
              filter_condition: {
                and_filter: [],
                or_filter: [],
              },
              member_mapping_rules: {
                fld_cm_send_to_01: undefined,
                fld_cm_member_first_name_01: undefined,
                fld_cm_member_last_name_01: undefined,
              },
              template_id: undefined,
            });
          }
        });

        _.map(tempMappingRules, (item, idx) => {
          const existObject = value.find(
            (object) => object?.object_id === item?.object_id
          );

          if (!existObject) {
            tempMappingRules.splice(idx, 1);
          }
        });

        $campaignTaskRules({
          ...campaignTaskRules,
          mapping_rules: tempMappingRules,
        });
        break;

      case "member_mapping_rules":
        if (typeof typeAction === "string") {
          tempMappingRules[index] = {
            ...tempMappingRules[index],
            member_mapping_rules: {
              ...tempMappingRules[index].member_mapping_rules,
              [typeAction]: value,
            },
          };
        } else {
          Object.entries(tempMappingRules[index].member_mapping_rules).forEach(
            ([key, val]) => {
              if (
                key !== "fld_cm_send_to_01" &&
                key !== "fld_cm_member_first_name_01" &&
                key !== "fld_cm_member_last_name_01"
              ) {
                if (!typeAction.includes(key)) {
                  delete tempMappingRules[index].member_mapping_rules[key];
                }
              }
            }
          );

          typeAction.forEach((item) => {
            tempMappingRules[index] = {
              ...tempMappingRules[index],
              member_mapping_rules: {
                ...tempMappingRules[index].member_mapping_rules,
                [item]: value,
              },
            };
          });
        }

        $campaignTaskRules({
          ...campaignTaskRules,
          mapping_rules: tempMappingRules,
        });

        break;

      case "mailchimp_campaign":
        $campaignTaskRules({
          ...campaignTaskRules,
          mailchimp_campaign_id: _.get(value, "id", ""),
          mailchimp_campaign_name: _.get(value.settings, "title", ""),
          mailchimp_audience_id: _.get(value.recipients, "list_id", ""),
          mailchimp_campaign_content: {
            archive_html: _.get(contentCampaignMailchimp, "archive_html", ""),
          },
        });
        break;

      case "fld_campaign_running_status_01":
        $campaignTaskRules({
          ...campaignTaskRules,
          fld_campaign_running_status_01: value ? "Running" : "Stop",
        });
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    dispatch(loadUserAssignTo());
    dispatch(
      loadFieldsCampaignMember({
        api_version: "2",
        object_id: "obj_crm_campaign_member_00001",
        show_meta_fields: false,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (type) {
      dispatch(
        loadObjects({
          campaign_type: type,
        })
      );
      // dispatch(
      //   loadFieldsCampaign({
      //     api_version: "2",
      //     object_id: "obj_crm_campaign_00001",
      //   })
      // );
      // dispatch(
      //   loadFieldsCampaignTask({
      //     api_version: "2",
      //     object_id: "obj_crm_campaign_task_00001",
      //   })
      // );

      switch (type) {
        case "Email":
          dispatch(getListEmailTemplate());
          dispatch(
            getListCampaignMailChimp({
              current_page: 1,
              record_per_page: 1000,
            })
          );
          break;

        case "SMS":
          dispatch(getListSMSTemplate());
          break;

        default:
          break;
      }
    }
  }, [dispatch, type]);

  useEffect(() => {
    if (typeCampaign === "Telemarketing") {
      dispatch(
        loadAllGroups({
          CurrentPage: 1,
          RecordPerPage: 1000,
        })
      );
    }
  }, [dispatch, typeCampaign]);

  useEffect(() => {
    if (
      recordID &&
      !_.isEmpty(detailsCampaign.campaignInfo) &&
      !_.isEmpty(listObjects)
    ) {
      $campaignTaskRules({
        name: _.get(
          detailsCampaign.campaignInfo.fld_campaign_name_01,
          "value",
          ""
        ),
        end_date: _.get(
          detailsCampaign.campaignInfo.fld_campaign_end_date_01,
          "value",
          undefined
        ),
        start_date: _.get(
          detailsCampaign.campaignInfo.fld_campaign_start_date_01,
          "value",
          undefined
        ),
        type: _.get(
          detailsCampaign.campaignInfo.fld_campaign_type_01,
          "value",
          ""
        ),
        is_one_time: _.get(
          detailsCampaign.campaignConfig,
          "is_one_time",
          false
        ),
        checking_workflow_sla: _.get(
          detailsCampaign.campaignConfig,
          "checking_workflow_sla",
          false
        ),
        campaign_task_template_id: _.get(
          detailsCampaign.campaignConfig,
          "campaign_task_template_id",
          undefined
        ),
        additional_result_fields: [],
        additional_task_fields: [],
        mapping_rules: _.get(
          detailsCampaign.campaignConfig,
          "mapping_rules",
          []
        ),
        schedule_configs: _.get(
          detailsCampaign.campaignConfig,
          "schedule_configs",
          []
        ),
        owner: _.get(detailsCampaign.campaignInfo, "owner", undefined),
        mailchimp_campaign_id: _.get(
          detailsCampaign.campaignConfig,
          "mailchimp_campaign_id",
          ""
        ),
        mailchimp_campaign_name: _.get(
          detailsCampaign.campaignConfig,
          "mailchimp_campaign_name",
          ""
        ),
        mailchimp_audience_id: _.get(
          detailsCampaign.campaignConfig,
          "mailchimp_audience_id",
          ""
        ),
        mailchimp_campaign_content: {
          archive_html: _.get(
            detailsCampaign.campaignConfig,
            "mailchimp_campaign_content.archive_html",
            ""
          ),
        },
        fld_campaign_running_status_01: _.get(
          detailsCampaign.campaignInfo.fld_campaign_running_status_01,
          "value",
          "Running"
        ),
      });

      $mailchimpSelected(
        _.get(detailsCampaign.campaignConfig, "mailchimp_campaign_id", "")
      );

      if (
        _.get(
          detailsCampaign.campaignInfo.fld_campaign_type_01,
          "value",
          ""
        ) === "Email"
      ) {
        dispatch(
          getContentCampaignMailchimp({
            mailchimp_campaign_id: _.get(
              detailsCampaign.campaignConfig,
              "mailchimp_campaign_id",
              ""
            ),
          })
        );
      }

      $typeCampaign(
        _.get(detailsCampaign.campaignInfo.fld_campaign_type_01, "value", "")
      );

      let tempObjects = [];
      _.map(
        _.get(detailsCampaign.campaignConfig, "recipients_objects", []),
        (item) => {
          const object = listObjects.find(
            (object) => object.object_id === item
          );
          if (object) {
            tempObjects.push(object);
          }
        }
      );
      $selectedObjects(tempObjects);

      let tempOperatorValueAnd = [];
      let tempOperatorValueOr = [];

      _.map(
        _.get(detailsCampaign.campaignConfig, "mapping_rules", []),
        (item, idx) => {
          tempOperatorValueAnd[idx] = {
            operator: [],
          };

          tempOperatorValueOr[idx] = {
            operator: [],
          };

          item.filter_condition.and_filter.forEach((condition) => {
            Object.entries(condition).forEach(([key, val]) => {
              if (key === "value") {
                tempOperatorValueAnd[idx].operator = [
                  ...tempOperatorValueAnd[idx].operator,
                  Object.keys(val)[0],
                ];
              }
            });
          });

          item.filter_condition.or_filter.forEach((condition) => {
            Object.entries(condition).forEach(([key, val]) => {
              if (key === "value") {
                tempOperatorValueOr[idx].operator = [
                  ...tempOperatorValueOr[idx].operator,
                  Object.keys(val)[0],
                ];
              }
            });
          });
        }
      );

      setOperatorValueAnd(tempOperatorValueAnd);
      setOperatorValueOr(tempOperatorValueOr);
    }
  }, [detailsCampaign, dispatch, listObjects, recordID]);

  useEffect(() => {
    if (recordID && !_.isEmpty(detailsCampaign.campaignConfig)) {
      $selectedTemplateCampaignTasks(
        templateSMSCampaignTasks.find(
          (item) =>
            item._id ===
            _.get(
              detailsCampaign.campaignConfig,
              "campaign_task_template_id",
              undefined
            )
        )?.content
      );
    }
  }, [detailsCampaign, recordID, templateSMSCampaignTasks]);

  useEffect(() => {
    if (
      recordID &&
      !_.isEmpty(detailsCampaign.campaignInfo) &&
      !_.isEmpty(listObjects)
    ) {
      _.map(
        _.get(detailsCampaign.campaignConfig, "mapping_rules", []),
        (item, idx) => {
          if (
            detailsCampaign.campaignInfo?.fld_campaign_type_01?.value ===
              "Email" &&
            !_.isEmpty(listEmailTemplate)
          ) {
            const findTemplate = listEmailTemplate.find(
              (template) => template._id === item.template_id
            );
            if (findTemplate) {
              $templates((prev) => {
                let temp = [...prev];
                const findIndex = prev.findIndex(
                  (index) => index.object_id === findTemplate.object_id
                );
                if (findIndex !== -1) {
                  temp[findIndex] = {
                    ...temp[findIndex],
                    body: findTemplate.body,
                  };
                } else {
                  temp.push({
                    object_id: findTemplate.object_id,
                    body: findTemplate.body,
                  });
                }
                return temp;
              });
            }
          } else if (
            detailsCampaign.campaignInfo?.fld_campaign_type_01?.value ===
              "SMS" &&
            !_.isEmpty(listSMSTemplate)
          ) {
            const findTemplate = listSMSTemplate.find(
              (template) => template._id === item.template_id
            );
            if (findTemplate) {
              $templates((prev) => {
                let temp = [...prev];
                const findIndex = prev.findIndex(
                  (index) => index.object_id === findTemplate.object_id
                );
                if (findIndex !== -1) {
                  temp[findIndex] = {
                    ...temp[findIndex],
                    body: findTemplate.content,
                  };
                } else {
                  temp.push({
                    object_id: findTemplate.object_id,
                    body: findTemplate.content,
                  });
                }
                return temp;
              });
            }
          }
        }
      );
    }
  }, [
    detailsCampaign,
    listEmailTemplate,
    listObjects,
    listSMSTemplate,
    recordID,
  ]);

  useEffect(() => {
    if (recordID) {
      dispatch(
        loadDetailsCampaign({
          id: recordID,
          object_id: "obj_crm_campaign_00001",
        })
      );
    }
  }, [dispatch, recordID]);

  useEffect(() => {
    if (userDetail && type) {
      $campaignTaskRules((prev) => {
        let temp = {
          ...prev,
          owner: userDetail._id,
        };
        return temp;
      });
    }
  }, [userDetail, type]);

  useEffect(() => {
    if (type) {
      $typeCampaign(type);
    }
  }, [type]);

  useEffect(() => {
    return () => {
      dispatch(unMountCampaignSetting());
    };
  }, [dispatch]);

  return (
    <Wrapper ref={pageRef}>
      <Breadcrumb>
        <Breadcrumb.Item
          onClick={() =>
            navigate("/objects/obj_crm_campaign_00001/default-view")
          }
        >
          Campaign
        </Breadcrumb.Item>
        <BreadcrumbItem>
          {action === "create" ? "Add new campaign" : "Edit campaign"}
        </BreadcrumbItem>
      </Breadcrumb>

      <WrapSetting>
        <Steps current={currentStep}>
          <Step
            title="Information"
            description={currentStep === 0 ? "In Progress" : "Finished"}
          />
          <Step
            title="Recipients"
            description={
              currentStep === 0
                ? "Waiting"
                : currentStep === 1
                ? "In Progress"
                : "Finished"
            }
          />
          {typeCampaign === "Telemarketing" && (
            <Step
              title="Assignment Rule"
              description={
                currentStep < 2
                  ? "Waiting"
                  : currentStep === 2
                  ? "In Progress"
                  : "Finished"
              }
            />
          )}
          {typeCampaign === "SMS" && (
            <Step
              title="Manual Tasks"
              description={
                currentStep < 2
                  ? "Waiting"
                  : currentStep === 2
                  ? "In Progress"
                  : "Finished"
              }
            />
          )}
        </Steps>

        <Information
          currentStep={currentStep}
          type={typeCampaign}
          recordID={recordID}
          typeCampaign={typeCampaign}
          selectedObjects={selectedObjects}
          $selectedObjects={$selectedObjects}
          mailchimpSelected={mailchimpSelected}
          $mailchimpSelected={$mailchimpSelected}
          campaignTaskRules={campaignTaskRules}
          $campaignTaskRules={$campaignTaskRules}
          handleChangeData={handleChangeData}
        />

        <Recipients
          currentStep={currentStep}
          type={typeCampaign}
          typeCampaign={typeCampaign}
          selectedObjects={selectedObjects}
          campaignTaskRules={campaignTaskRules}
          $campaignTaskRules={$campaignTaskRules}
          handleChangeData={handleChangeData}
          ruleSelected={ruleSelected}
          $ruleSelected={$ruleSelected}
          operatorValueAnd={operatorValueAnd}
          setOperatorValueAnd={setOperatorValueAnd}
          operatorValueOr={operatorValueOr}
          setOperatorValueOr={setOperatorValueOr}
          valueAnd={valueAnd}
          setValueAnd={setValueAnd}
          valueOr={valueOr}
          setValueOr={setValueOr}
          templates={templates}
          $templates={$templates}
          recordID={recordID}
          listFieldsCampaignMember={listFieldsCampaignMember}
        />

        <AssignmentRule currentStep={currentStep} type={typeCampaign} />
        <ManualTasks
          currentStep={currentStep}
          type={typeCampaign}
          campaignTaskRules={campaignTaskRules}
          handleChangeData={handleChangeData}
          selectedTemplateCampaignTasks={selectedTemplateCampaignTasks}
          $selectedTemplateCampaignTasks={$selectedTemplateCampaignTasks}
        />

        <WrapButton>
          {currentStep === 0 && (
            <Back
              onClick={() =>
                navigate("/objects/obj_crm_campaign_00001/default-view")
              }
            >
              {t("common.cancel")}
            </Back>
          )}
          {currentStep > 0 && (
            <Back onClick={() => _onPrev()}>{t("common.back")}</Back>
          )}
          {(typeCampaign === "Telemarketing" || typeCampaign === "SMS") &&
          currentStep === 2 ? (
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => _onSubmit()}
            >
              {t("common.save")}
            </Button>
          ) : typeCampaign !== "Telemarketing" &&
            typeCampaign !== "SMS" &&
            currentStep === 1 ? (
            <Button
              type="primary"
              htmlType="submit"
              onClick={() => _onSubmit()}
            >
              {t("common.save")}
            </Button>
          ) : (
            <Button type="primary" onClick={() => _onNext()}>
              {t("common.next")}
            </Button>
          )}
        </WrapButton>
      </WrapSetting>
    </Wrapper>
  );
}

export default withTranslation()(SettingCampaign);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-steps {
    margin-bottom: 24px;
  }

  .ant-steps-item-process .ant-steps-item-icon {
    background: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-steps-item-title::after {
    background-color: ${(props) => props.theme.main}!important;
  }

  .ant-steps-item-finish .ant-steps-item-icon {
    border-color: ${(props) => props.theme.main};

    > .ant-steps-icon {
      color: ${(props) => props.theme.main};
    }
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapSetting = styled.div`
  background: #fff;
  border-radius: 5px;
  margin-top: 20px;

  padding: 2.5rem 2rem;

  @media screen and (min-width: 1600px) {
    padding: 2.5rem 15rem;
  }
`;

const WrapButton = styled.div`
  margin-top: 30px;
  display: flex;
  justify-content: center;

  .ant-btn {
    font-size: 16px;
    height: unset;
    width: 8.125rem;

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    margin: 0 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn-primary[disabled] {
    background: #f5f5f5;
    border: 1px solid #d9d9d9;
    color: rgba(0, 0, 0, 0.25);

    :hover {
      background: #f5f5f5 !important;
      border: 1px solid #d9d9d9 !important;
      color: rgba(0, 0, 0, 0.25) !important;
    }
  }
`;

const Back = styled(Button)`
  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;
