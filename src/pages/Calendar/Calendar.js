import React, { useEffect, useState, useRef } from "react";
import _ from "lodash";
import { useDispatch, useSelector } from "redux/store";
import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";
import timeGrigPlugin from "@fullcalendar/timegrid";
import listPlugin from "@fullcalendar/list";
import interactionPlugin from "@fullcalendar/interaction";
import esLocale from "@fullcalendar/core/locales/en-au";
import viLocale from "@fullcalendar/core/locales/vi";
import "antd/dist/antd.css";
import {
  Collapse,
  Modal,
  Button,
  Input,
  DatePicker,
  Radio,
  Select,
  Checkbox,
  Dropdown,
  Tooltip,
} from "antd";

import moment from "moment";
import locale from "antd/es/date-picker/locale/vi_VN";
import styled from "styled-components";
import { useNavigate } from "react-router";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";

import Editor from "components/Editor/Editor2";

import {
  getListEvent,
  getListEventFilter,
  updateEvent,
  createEvent,
  updateEventDetail,
  deleteEvent,
  updateStatus,
  setEventId,
} from "redux/slices/calendar";

import { loadAllUser } from "redux/slices/user";
import { loadAllGroups } from "redux/slices/group";

import PrivateIcon from "assets/icons/common/privateIcon.svg";
import PublicIcon from "assets/icons/common/publicIcon.svg";
import CalendarIcon from "assets/icons/common/calendarIcon.svg";
import DefaultAvatarGroup from "assets/icons/common/avtGroupDefault.png";
import DefaultAvatarAgent from "assets/icons/common/avtAgentDefault.png";
import CloseIcon from "assets/icons/common/close-circle.png";
import AcceptIcon from "assets/icons/calendar/accept.png";
import RejectIcon from "assets/icons/calendar/reject.png";
import WaitingIcon from "assets/icons/calendar/waiting.png";
import DescriptIcon from "assets/icons/calendar/descript.png";

import { BE_URL } from "constants/constants";

import Calendar from "react-calendar";

import "react-calendar/dist/Calendar.css";
import { useTranslation } from "react-i18next";
import { changeTitlePage } from "redux/slices/authenticated";

const { Option, OptGroup } = Select;
const { Panel } = Collapse;

export default function MyCalendar() {
  const lang = localStorage.getItem("i18nextLng");
  require("moment/locale/vi");

  const dateEvent = "DD/MM HH:mm";
  const dateFormatVi = "dddd, DD [tháng] MM, HH:mm";
  const dateFormatEndDate = "YYYY-MM-DD";
  const dateFormatSecond = "YYYY-MM-DD HH:mm:ss";

  const calendarRef = React.createRef();

  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const editorJodit = useRef(null);

  const { userDetail, listAllUser } = useSelector((state) => state.userReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const { listEvent, listEventFilter } = useSelector(
    (state) => state.calendarReducer
  );

  const [color] = useState("#D2ECEC");

  const [lstEvent, setLstEvent] = useState([]);

  const [isModalVisible, setIsModalVisible] = useState(false); // modal event

  const [openModal, setOpenModal] = useState(false); // modal repeat

  const [disable, setDisabled] = useState(false);

  const [isErr, setIsErr] = useState(false);

  const [activeKey, setActiveKey] = useState([]);

  // const [listUser, setListUser] = useState([]);

  const [listGuest, setListGuest] = useState([]);

  const [listUserActive, setListUserActive] = useState([]);

  const [lstGuest, setLstGuest] = useState([]); // list select guest

  const [listRender, setListRender] = useState([]); // listRenderGuest

  const [optionView, setOptionView] = useState("dayGridMonth");

  const [miniCalendar, setMiniCalendar] = useState(new Date());

  const [optionRepeat, setOptionRepeat] = useState(""); // Never day month week year

  const [endDate, setEndate] = useState(new Date()); // Day end repeat

  const [lstDate, setLstDate] = useState([]); // 0 1 2 3 4 5 6

  const [valueSearchGuest, setValueSearchGuest] = useState("");

  const [valueSreachShare, setValueSearchShare] = useState("");

  const [listShareTo, setListShareTo] = useState([]); // group and agent

  const [lstShareTo, setLstShareTo] = useState([]); // list select share to

  const [valueShareTo, setValueShareTo] = useState([]); // value group and agent

  const [agent, setAgent] = useState([]); //share agent
  const [group, setGroup] = useState([]); // share group

  const [listIdGuest, setListIdGuest] = useState([]); // id guest

  const [idEvent, setIdEvent] = useState(""); // update and delete

  const [position, setPosition] = useState("owner");

  const [status, setStatus] = useState("waiting");

  const [eventType, setEventType] = useState(["my_event"]);

  const [url, setUrl] = useState(
    `?start_range=${moment(miniCalendar)
      .startOf("month")
      .format(dateFormatSecond)}&end_range=${moment(miniCalendar)
      .endOf("month")
      .format(dateFormatSecond)}`
  );

  const [mainUrl, setMainUrl] = useState("?event_type=my_event");

  const [lstEventFilter, setLstEventFilter] = useState([]);

  const [content, setContent] = useState({
    start_time: moment(new Date()).format(dateFormatSecond),
    end_time: moment(new Date()).format(dateFormatSecond),
    event_name: "",
    owner: userDetail._id,
    permission: "private",
    description: "",
    users_sharing: [],
    sharing_fields: ["_id", "owner", "start_time", "end_time", "color"],
    alerts: [
      {
        send_mail: false,
        quantity: 10,
        unit: "minutes",
      },
    ],
    guests: [],
    repeat_event: {},
    color: color,
    groups_sharing: [],
  });

  useEffect(() => {
    dispatch(changeTitlePage(t("common.calendar")));
    //eslint-disable-next-line
  }, [t]);

  const listColor = [
    {
      backgroundColor: "#D2ECEC",
      borderColor: "#20A2A2",
    },
    {
      backgroundColor: "#D1E9FF",
      borderColor: "#1890FF",
    },
    {
      backgroundColor: "#DDF7D5",
      borderColor: "#54D62C",
    },
    {
      backgroundColor: " #FFF3CD",
      borderColor: "#FFC107",
    },
    {
      backgroundColor: "#FFDAD9",
      borderColor: "#FF4842",
    },

    {
      backgroundColor: "#E9DFFD",
      borderColor: "#9161F7",
    },
    {
      backgroundColor: "#FBDAEB",
      borderColor: "#EC4899",
    },
  ];

  const listView = [
    {
      label: t("calendar.day"),
      value: "timeGridDay",
    },
    {
      label: t("calendar.week"),
      value: "timeGridWeek",
    },
    {
      label: t("calendar.workWeek"),
      value: "timeGridForweek",
    },
    {
      label: t("calendar.month"),
      value: "dayGridMonth",
    },
    {
      label: t("calendar.year"),
      value: "listYear",
    },
  ];

  const viewToolbar = {
    left: "",
    center: "prev,title,next",
    right: "today,dayGridMonth,listYear",
  };

  const views = {
    timeGridDay: { buttonText: "Day" },
    timeGridWeek: { buttonText: "Week" },
    dayGridMonth: { buttonText: "Month", dayHeaderFormat: { weekday: "long" } },
    listYear: { buttonText: "Year" },

    timeGridForweek: {
      buttonText: "Work",
      type: "timeGridWeek",
      hiddenDays: [0, 6],
    },
  };

  const listOptionRepeat = [
    {
      label: t("calendar.notRepeat"),
      value: "",
    },
    {
      label: t("calendar.daily"),
      value: "days",
    },
    {
      label: t("calendar.weekly"),
      value: "weeks",
    },
    {
      label: t("calendar.monthly"),
      value: "months",
    },
    {
      label: t("calendar.annual"),
      value: "years",
    },
  ];

  const listDate = [
    {
      label: t("common.monday"),
      value: 1,
    },
    {
      label: t("common.tuesday"),
      value: 2,
    },
    {
      label: t("common.wednesday"),
      value: 3,
    },
    {
      label: t("common.thurday"),
      value: 4,
    },
    {
      label: t("common.friday"),
      value: 5,
    },
    {
      label: t("common.saturday"),
      value: 6,
    },
    {
      label: t("common.sunday"),
      value: 0,
    },
  ];

  const listStatus = [
    {
      label: t("calendar.going"),
      value: "accept",
    },
    {
      label: t("calendar.notGoing"),
      value: "reject",
    },
    {
      label: t("calendar.later"),
      value: "waiting",
    },
  ];

  useEffect(() => {
    if (isModalVisible === false) {
      setActiveKey([]);
    }
  }, [isModalVisible]);

  useEffect(() => {
    const calendarApi = calendarRef.current.getApi();

    calendarApi.changeView(optionView);
  }, [calendarRef, optionView]);

  useEffect(() => {
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(getListEvent({ url: mainUrl }));
  }, [dispatch, mainUrl]);

  useEffect(() => {
    const temp = listAllUser.filter((user) => user.Active);
    setListUserActive(temp);
  }, [listAllUser]);

  useEffect(() => {
    if (listUserActive) {
      setListGuest(listUserActive);
    }
  }, [listUserActive]);

  useEffect(() => {
    setContent((prev) => ({
      ...prev,
      owner: userDetail._id,
    }));
  }, [userDetail]);

  useEffect(() => {
    dispatch(
      getListEventFilter({
        urlFilter: url,
      })
    );
  }, [dispatch, url]);

  useEffect(() => {
    if (listEventFilter.data) {
      const changeKey = {
        start_time: "start",
        end_time: "end",
        event_name: "title",
        color: "backgroundColor",
      };

      const formatData = (keysMap, object) =>
        Object.keys(object).reduce(
          (acc, key) => ({
            ...acc,
            ...{ [keysMap[key] || key]: object[key] },
          }),
          {}
        );

      const result = listEventFilter.data.map((item) => {
        return formatData(changeKey, item);
      });

      result.sort((c, d) => {
        return new Date(c.start).getTime() - new Date(d.start).getTime();
      });

      setLstEventFilter(result);
    }
  }, [listEventFilter]);

  useEffect(() => {
    let tem = `?start_range=${moment(miniCalendar)
      .startOf("month")
      .format(dateFormatSecond)}&end_range=${moment(miniCalendar)
      .endOf("month")
      .format(dateFormatSecond)}`;

    // if (eventType.length === 0) {
    //   setLstEventFilter([]);
    //   return;
    // }
    // if (eventType.length === 1) {
    //   tem = tem + `&event_type=${eventType[0]}`;
    // }
    setUrl(tem);
  }, [miniCalendar]);

  useEffect(() => {
    let tem = "";

    if (eventType.length === 0) {
      tem = "?color=texts";
    } else if (eventType.length === 1) {
      tem = tem + `?event_type=${eventType[0]}`;
    }

    setMainUrl(tem);
  }, [eventType]);

  useEffect(() => {
    if (listEvent.data) {
      const changeKey = {
        start_time: "start",
        end_time: "end",
        event_name: "title",
        color: "backgroundColor",
      };

      const formatData = (keysMap, object) =>
        Object.keys(object).reduce(
          (acc, key) => ({
            ...acc,
            ...{ [keysMap[key] || key]: object[key] },
          }),
          {}
        );

      const result = listEvent.data.map((item) => {
        return formatData(changeKey, item);
      });

      setLstEvent(result);
    }
  }, [dispatch, listEvent]);

  useEffect(() => {
    const temp = content.guests.map((guest) => {
      return guest.user_id;
    });
    setListIdGuest(temp);

    if (
      content.event_name.length === 0 ||
      content.start_time === content.end_time ||
      isNaN(new Date(content.start_time).valueOf()) ||
      isNaN(new Date(content.end_time).valueOf())
    ) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }

    const start = content.start_time;
    const end = content.end_time;
    if (
      new Date(start).getTime() > new Date(end).getTime() ||
      new Date(start).getTime() === new Date(end).getTime()
    ) {
      // setContent((prev) => ({
      //   ...prev,
      //   start_time: end,
      //   end_time: start,
      // }));
      setIsErr(true);
      setDisabled(true);
    } else {
      setIsErr(false);
    }
  }, [content]);

  useEffect(() => {
    const filterList = listGuest.filter(
      (item) => !listIdGuest.includes(item._id)
    );

    const filterRender = listGuest.filter((item) =>
      listIdGuest.includes(item._id)
    );

    let tempFilter = [...filterRender];

    filterRender.forEach((item, idx) => {
      content.guests.forEach((value) => {
        if (item._id === value.user_id) {
          tempFilter[idx] = {
            ...tempFilter[idx],
            stt: value.status,
          };
        }
      });
    });

    setLstGuest(filterList);
    setListRender(tempFilter);
  }, [listIdGuest, listGuest, content]);

  useEffect(() => {
    const filterShare = listShareTo.map((item) => {
      return {
        label: item.label,
        options: item.options.filter(
          (value) => !valueShareTo.includes(value._id)
        ),
      };
    });
    setLstShareTo(filterShare);
  }, [listShareTo, valueShareTo]);

  useEffect(() => {
    const listShare = [];
    if (listUserActive.length > 0) {
      listShare.push({
        label: "Agent",
        options: listUserActive,
      });
    }
    if (listAllGroups.length > 0) {
      listShare.push({
        label: "Group",
        options: listAllGroups,
      });
    }
    setListShareTo(listShare);
  }, [listAllGroups, listUserActive, userDetail]);

  useEffect(() => {
    const filterAgent = listUserActive.filter((user) =>
      valueShareTo.includes(user._id)
    );

    const filterGroup = listAllGroups.filter((group) =>
      valueShareTo.includes(group._id)
    );

    setAgent(filterAgent);
    setGroup(filterGroup);
  }, [valueShareTo, listAllGroups, listUserActive]);

  useEffect(() => {
    const shareUser = agent.map((item) => {
      return item._id;
    });

    const shareGroup = group.map((item) => {
      return item._id;
    });

    setContent((prev) => ({
      ...prev,
      users_sharing: shareUser,
      groups_sharing: shareGroup,
    }));
  }, [agent, group]);

  useEffect(() => {
    const { stop_before, exclude_day_of_week } = content.repeat_event;
    if (!openModal) {
      if (stop_before) {
        setEndate(new Date(stop_before));
      } else {
        setEndate(new Date());
      }
      if (exclude_day_of_week) {
        setLstDate(exclude_day_of_week);
      } else {
        setLstDate([]);
      }
    }
  }, [openModal, content.repeat_event]);

  useEffect(() => {
    const { owner, guests } = content;
    let isGuest = false;

    guests.forEach((item) => {
      if (item.user_id === userDetail._id) {
        isGuest = true;
        return;
      }
    });

    if (userDetail._id === owner) {
      setPosition("owner");
    } else if (isGuest === true) {
      setPosition("guest");
    } else {
      setPosition("share");
    }
  }, [userDetail, content]);

  const onChangePermission = (value) => {
    setContent((prev) => ({
      ...prev,
      permission: value,
    }));
  };

  const handleChangeGuest = (value) => {
    const temp = [];

    // value.forEach((v, idx) => {
    //   content.guests.forEach((item) => {
    //     if (v === item.user_id) {
    //       temp.push(item);
    //       value.splice(idx, 1);
    //     }
    //   });
    // });

    content.guests.forEach((item) => {
      if (value.includes(item.user_id)) {
        temp.push(item);
        value = value.filter((va) => va !== item.user_id);
      }
    });

    value.forEach((item) => {
      temp.push({
        user_id: item,
        status: "waiting",
      });
    });

    setContent((prev) => ({
      ...prev,
      guests: temp,
    }));
  };

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleEvent = () => {
    setIsModalVisible(false);
    if (idEvent === "") {
      dispatch(
        createEvent({
          data: {
            ...content,
            description: _.get(editorJodit, "current.value", ""),
          },
          url: mainUrl,
          urlFilter: url,
          isCall: true,
        })
      );
    } else {
      dispatch(
        updateEvent({
          data: {
            ...content,
            description: _.get(editorJodit, "current.value", ""),
          },
          id: idEvent,
          url: mainUrl,
          urlFilter: url,
          isCall: true,
        })
      );
    }
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const handleOnchange = (key, value) => {
    setContent((prev) => ({
      ...prev,
      [key]: value,
    }));
  };

  useEffect(() => {
    if (!openModal) {
      if (content.repeat_event.stop_before) {
        setEndate(new Date(content.repeat_event.stop_before));
      } else {
        setEndate(new Date());
      }
    }
  }, [openModal, content.repeat_event.stop_before]);

  const updateEventStatus = (value) => {
    dispatch(
      updateStatus({
        status: value,
        event_id: idEvent,
        guest_id: userDetail._id,
        url: mainUrl,
        urlFilter: url,
        isCall: true,
      })
    );
  };

  const handleOnclick = (data, option) => {
    const {
      owner,
      permission,
      description,
      users_sharing,
      sharing_fields,
      alerts,
      guests,
      repeat_event,
      groups_sharing,
      _id,
    } = option === "right" ? data.extendedProps : data;

    let pos = "owner";
    let isG = false;

    guests?.forEach((item) => {
      if (item.user_id === userDetail._id) {
        isG = true;
        setStatus(item.status);
        return;
      }
    });

    if (userDetail._id === owner) {
      pos = "owner";
    } else if (isG === true) {
      pos = "guest";
    } else {
      pos = "share";
    }

    if (pos === "owner" || pos === "guest") {
      let arr = [];

      setIdEvent(_id);

      users_sharing.forEach((item) => {
        arr.push(item);
      });

      groups_sharing.forEach((item) => {
        arr.push(item);
      });

      if (repeat_event.stop_before) {
        setEndate(new Date(repeat_event.stop_before));
      } else {
        setOptionRepeat("");
      }

      if (repeat_event.exclude_day_of_week) {
        setLstDate(repeat_event.exclude_day_of_week);
      }

      setContent((prev) => ({
        ...prev,
        start_time: moment(data.start).format(dateFormatSecond),
        end_time: moment(data.end).format(dateFormatSecond),
        event_name: data.title,
        owner: owner,
        permission: permission,
        description: description,
        users_sharing: users_sharing,
        sharing_fields: sharing_fields,
        alerts: alerts,
        guests: guests,
        repeat_event: repeat_event,
        groups_sharing: groups_sharing,
        color: data.backgroundColor,
      }));
      setValueShareTo(arr);
    } else {
      setIdEvent(_id);
      setContent((prev) => ({
        ...prev,
        start_time: moment(data.start).format(dateFormatSecond),
        end_time: moment(data.end).format(dateFormatSecond),
        event_name: data.title || "",
        owner: owner,
        permission: permission || "private",
        description: description || "",
        users_sharing: users_sharing || [],
        sharing_fields: sharing_fields || [
          "_id",
          "owner",
          "start_time",
          "end_time",
          "color",
        ],
        alerts: alerts || [
          {
            send_mail: false,
            quantity: 10,
            unit: "minutes",
          },
        ],
        guests: guests || [],
        repeat_event: repeat_event || {},
        groups_sharing: groups_sharing || [],
        color: data.backgroundColor,
      }));
      setOptionRepeat("");
      setAgent([]);
      setGroup([]);
    }

    showModal();
  };

  const handleOnSelect = (data) => {
    setContent((prev) => ({
      ...prev,
      start_time: moment(data.start).format(dateFormatSecond),
      end_time: moment(data.end).format(dateFormatSecond),
      event_name: "",
      owner: userDetail._id,
      permission: "private",
      description: "",
      users_sharing: [],
      sharing_fields: ["_id", "owner", "start_time", "end_time", "color"],
      alerts: [
        {
          send_mail: false,
          quantity: 10,
          unit: "minutes",
        },
      ],
      guests: [],
      repeat_event: {},
      color: color,
      groups_sharing: [],
    }));
    setMiniCalendar(data.start);
    setIdEvent("");
    setOptionRepeat("");
    setAgent([]);
    setGroup([]);
    setValueShareTo([]);

    showModal();
  };

  const handleChangeTime = (key, value) => {
    if (value) {
      setContent((prev) => ({
        ...prev,
        [key]: moment(value).format(dateFormatSecond),
      }));
    } else {
      setContent((prev) => ({
        ...prev,
        [key]: moment(new Date()).format(dateFormatSecond),
      }));
    }
  };

  const renderPopover = () => {
    return (
      <ContentPopover>
        <p
          onClick={() => {
            onChangePermission("private");
          }}
        >
          <img src={PrivateIcon} alt="Private" /> {t("calendar.private")}
        </p>
        <p
          onClick={() => {
            onChangePermission("public");
          }}
        >
          <img src={PublicIcon} alt="Public" /> {t("calendar.public")}
        </p>
      </ContentPopover>
    );
  };

  const onChangeRepeat = (value) => {
    setOptionRepeat(value);

    if (!content.repeat_event.stop_before) {
      setEndate(new Date());
    }

    if (value !== "") {
      setOpenModal(true);
      setIsModalVisible(false);
    } else {
      setContent((prev) => ({
        ...prev,
        repeat_event: {},
      }));
    }
  };

  const renderOwner = () => {
    const owner = listUserActive.filter((user) => user._id === content.owner);
    if (owner[0])
      return (
        <ContentGuest>
          <div className="guestItem" key={owner[0]._id}>
            <div className="guestImg">
              {owner[0].avatar_config?.url !== "" &&
              owner[0].avatar_config?.url ? (
                <img
                  src={`${BE_URL}${owner[0].avatar_config?.url}`}
                  alt="Img"
                />
              ) : (
                <img src={DefaultAvatarAgent} alt="img" />
              )}
            </div>
            <div className="guestName">
              {owner[0]._id === userDetail._id ? "Tôi" : owner[0].First_Name}
            </div>
          </div>
        </ContentGuest>
      );
  };

  // const handleChangeEditor = (value) => {
  //   setContent((prev) => ({
  //     ...prev,
  //     description: value,
  //   }));
  // };

  const renderHeaderDescript = (title) => {
    return (
      <CustomHeader>
        <img src={DescriptIcon} alt="" /> {title}
      </CustomHeader>
    );
  };

  const renderTitle = (pos) => {
    let title = "";
    if (idEvent !== "") {
      if (pos === "owner") {
        title = t("calendar.updateEvent");
      } else {
        title = t("calendar.detailEvent");
      }
    } else {
      title = t("calendar.addEvent");
    }

    return title;
  };

  const renderModal = (pos) => {
    return (
      <CustomModal
        myPostiton={pos}
        title={renderTitle(pos)}
        visible={isModalVisible}
        onOk={handleEvent}
        onCancel={handleCancel}
        footer={[
          <ButtonGroup key="button">
            {pos === "owner" && (
              <div>
                <Button
                  key="more"
                  onClick={() => {
                    dispatch(updateEventDetail(content));
                    dispatch(setEventId(idEvent));
                    localStorage.removeItem("id_event");
                    navigate("/calendar-detail");
                  }}
                >
                  {t("calendar.more")}
                </Button>
              </div>
            )}
            {pos === "owner" && (
              <div className="btnGroup">
                <>
                  {idEvent !== "" && (
                    <Button
                      key="delete"
                      onClick={() => {
                        setIsModalVisible(false);
                        dispatch(setShowModalConfirmDelete(true));
                      }}
                    >
                      {t("common.delete")}
                    </Button>
                  )}
                  <Button
                    key="save"
                    onClick={() => {
                      setIsModalVisible(false);
                      handleEvent();
                    }}
                    disabled={disable}
                  >
                    {t("common.save")}
                  </Button>
                </>
              </div>
            )}
            {pos === "guest" && (
              <ConfirmWrap>
                <SelectStatus
                  value={status}
                  placeholder="Please confirm"
                  onChange={(value) => {
                    setStatus(value);
                    setIsModalVisible(false);
                    updateEventStatus(value);
                  }}
                >
                  {listStatus.map((stt, index) => {
                    return (
                      <Option key={index} value={stt.value}>
                        {stt.label}
                      </Option>
                    );
                  })}
                </SelectStatus>

                <QuestConfirm> {t("calendar.question")}</QuestConfirm>
              </ConfirmWrap>

              // <ConFirmStatus>
              //   <ConfirmGroup
              //     value={status}
              //     onChange={(value) => {
              //       setStatus(value.target.value);
              //       setIsModalVisible(false);
              //       updateEventStatus(value.target.value);
              //     }}
              //   >
              //     {listStatus.map((stt, index) => {
              //       return (
              //         <ItemConfirm
              //           isCheck={status === stt.value ? true : false}
              //           key={index}
              //           value={stt.value}
              //         >
              //           {stt.label}
              //         </ItemConfirm>
              //       );
              //     })}
              //   </ConfirmGroup>

              //   <span>Do you join this event?</span>
              // </ConFirmStatus>
            )}
          </ButtonGroup>,
        ]}
      >
        {pos === "owner" && (
          <>
            <div className="groupInput">
              <CustomInput
                value={content.event_name}
                placeholder={t("calendar.addTitle")}
                onChange={(e) => {
                  handleOnchange("event_name", e.target.value);
                }}
              />

              <Dropdown placement="bottom" overlay={renderPopover()}>
                <div className="popoverContent">
                  <Button>
                    <img
                      src={
                        content.permission === "private"
                          ? PrivateIcon
                          : PublicIcon
                      }
                      alt="icon"
                    />
                    <span>
                      {content.permission === "private"
                        ? t("calendar.private")
                        : t("calendar.public")}
                    </span>
                  </Button>
                </div>
              </Dropdown>
            </div>

            <Radio.Group
              onChange={(e) => handleOnchange("color", e.target.value)}
              value={content.color}
              className="radioGroup"
            >
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </Radio.Group>

            <CustomRangePicker>
              <img src={CalendarIcon} alt="Calendar icon" />
              <div className="datePicker">
                {/* <label>Start</label> */}
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("start_time", value);
                  }}
                  value={moment(content.start_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
                <span>-</span>
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("end_time", value);
                  }}
                  value={moment(content.end_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
              </div>
            </CustomRangePicker>

            {isErr && <ErrorStatus>{t("calendar.err")}</ErrorStatus>}

            <SelectRepeat
              disabled={idEvent !== "" ? true : false}
              bordered={false}
              showArrow={false}
              value={
                Object.keys(content.repeat_event).length !== 0
                  ? content.repeat_event.unit_interval
                  : optionRepeat
              }
              onChange={(value) => onChangeRepeat(value)}
              onSelect={(value) => {
                if (value !== "") {
                  setOpenModal(true);
                  setIsModalVisible(false);
                }
              }}
            >
              {listOptionRepeat.map((option, index) => {
                return (
                  <Option key={index} value={option.value}>
                    {option.label}
                  </Option>
                );
              })}
            </SelectRepeat>

            <SelectGuest>
              {!valueSearchGuest ? (
                <span>{t("calendar.add")}</span>
              ) : (
                <span className="addGuest">{t("calendar.add")}</span>
              )}
              <CustomSelect
                onSelect={() => setValueSearchGuest("")}
                mode="multiple"
                optionFilterProp="children"
                value={listIdGuest}
                onSearch={(value) => {
                  setValueSearchGuest(value);
                }}
                onChange={(value) => {
                  handleChangeGuest(value);
                }}
              >
                {lstGuest.map((item) => {
                  return (
                    item._id !== userDetail._id && (
                      <Option key={item._id} value={item._id}>
                        {/* {renderItemSelect(item)} */}
                        {item.Full_Name}
                      </Option>
                    )
                  );
                })}
              </CustomSelect>
            </SelectGuest>

            {renderListGuest(listRender, "user")}

            {content.permission === "private" && (
              <>
                <SelectGuest>
                  {!valueSreachShare ? (
                    <span>{t("calendar.share")}</span>
                  ) : (
                    <span className="addGuest">{t("calendar.share")}</span>
                  )}
                  <CustomSelect
                    onSelect={() => setValueSearchShare("")}
                    mode="multiple"
                    optionFilterProp="children"
                    value={valueShareTo}
                    onSearch={(value) => {
                      setValueSearchShare(value);
                    }}
                    onChange={(value) => {
                      setValueShareTo(value);
                    }}
                  >
                    {lstShareTo.map((item, index) => {
                      return (
                        <OptGroup label={item.label} key={index}>
                          {item.options.map((option) => {
                            return (
                              option._id !== userDetail._id && (
                                <Option key={option._id} value={option._id}>
                                  {item.label === "Agent"
                                    ? option.Full_Name
                                    : option.name}
                                </Option>
                              )
                            );
                          })}
                        </OptGroup>
                      );
                    })}
                  </CustomSelect>
                </SelectGuest>

                {renderListGuest(agent, "agent")}
                {renderListGuest(group, "group")}
              </>
            )}

            <CustomCollapse
              activeKey={activeKey}
              onChange={(key) => setActiveKey(key)}
            >
              <CustomPanel
                key={1}
                header={renderHeaderDescript(t("calendar.addDescription"))}
              >
                <Editor
                  editorJodit={editorJodit}
                  content={content.description}
                  showAppend={false}
                  minHeightInput="200px"
                />
              </CustomPanel>
            </CustomCollapse>

            <label>{t("calendar.owner")}</label>
            {renderOwner()}
          </>
        )}

        {pos === "guest" && (
          <ContentShare>
            {content.event_name !== "" && (
              <ShareTitle>
                <span>{content.event_name}</span>
                <div className="buttonPermission">
                  <Button>
                    <img
                      src={
                        content.permission === "private"
                          ? PrivateIcon
                          : PublicIcon
                      }
                      alt="icon"
                    />
                    <span>
                      {" "}
                      {content.permission === "private"
                        ? t("calendar.private")
                        : t("calendar.public")}
                    </span>
                  </Button>
                </div>
              </ShareTitle>
            )}
            <Radio.Group value={content.color} className="radioGroup">
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </Radio.Group>
            <SharePickerWrap>
              <img src={CalendarIcon} alt="Calendar icon" />
              <SharePickerContent>
                <SharePicker>
                  {moment(content.start_time).format(dateFormatVi)}
                </SharePicker>
                <span>-</span>
                <SharePicker>
                  {moment(content.end_time).format(dateFormatVi)}
                </SharePicker>
              </SharePickerContent>
            </SharePickerWrap>
            <label>{t("calendar.guest")}</label>{" "}
            {renderListGuest(listRender, "user")}
            {(agent.length > 0 || group.length > 0) && (
              <label>{t("calendar.share")}</label>
            )}
            {renderListGuest(agent, "agent")}
            {renderListGuest(group, "group")}
            {content.description !== "" && (
              <CustomCollapse
                activeKey={activeKey}
                onChange={(key) => setActiveKey(key)}
              >
                <CustomPanel
                  key={1}
                  header={renderHeaderDescript("Click to see description")}
                >
                  <Editor
                    editorJodit={editorJodit}
                    content={content.description}
                    showAppend={false}
                    minHeightInput="200px"
                  />
                  <Editor
                    editorJodit={editorJodit}
                    content={content.description}
                    showAppend={false}
                    minHeightInput="200px"
                    readonly={true}
                  />
                </CustomPanel>
              </CustomCollapse>
            )}
            <label>{t("calendar.owner")}</label>
            {renderOwner()}
          </ContentShare>
        )}

        {pos === "share" && (
          <ContentShare>
            {content.event_name !== "" && (
              <ShareTitle>
                <span>{content.event_name}</span>
              </ShareTitle>
            )}

            <Radio.Group value={content.color} className="radioGroup">
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </Radio.Group>

            <SharePickerWrap>
              <img src={CalendarIcon} alt="Calendar icon" />
              <SharePickerContent>
                <SharePicker>
                  {moment(content.start_time).format(dateFormatVi)}
                </SharePicker>
                <span>-</span>
                <SharePicker>
                  {moment(content.end_time).format(dateFormatVi)}
                </SharePicker>
              </SharePickerContent>
            </SharePickerWrap>

            {content.guests.length > 1 && (
              <>
                <label>{t("calendar.guest")}</label>{" "}
                {renderListGuest(listRender, "user")}
              </>
            )}
            {content.description !== "" && (
              <CustomCollapse
                activeKey={activeKey}
                onChange={(key) => setActiveKey(key)}
              >
                <CustomPanel
                  key={1}
                  header={renderHeaderDescript("Click to see description")}
                >
                  <Editor
                    editorJodit={editorJodit}
                    content={content.description}
                    showAppend={false}
                    minHeightInput="200px"
                    readonly={true}
                  />
                </CustomPanel>
              </CustomCollapse>
            )}

            <label>{t("calendar.owner")}</label>
            {renderOwner()}
          </ContentShare>
        )}
      </CustomModal>
    );
  };

  const handleCreateEvent = () => {
    const timeDelay = new Date().getTime() + 30 * 60 * 1000; // 30 phut

    setContent((prev) => ({
      ...prev,
      event_name: "",
      description: "",
      color: color,
      guests: [],
      start_time: moment(new Date()).format(dateFormatSecond),
      end_time: moment(timeDelay).format(dateFormatSecond),
      alerts: [
        {
          send_mail: false,
          quantity: 10,
          unit: "minutes",
        },
      ],
    }));

    showModal();
  };

  const onChangeDate = (value) => {
    setMiniCalendar(value);
    const calendarApi = calendarRef.current.getApi();
    calendarApi.gotoDate(value);
  };

  const goToDay = () => {
    setMiniCalendar(new Date());
    const calendarApi = calendarRef.current.getApi();
    calendarApi.today();
  };

  const handleCloseModal = () => {
    setOpenModal(false);
    setIsModalVisible(true);
  };

  const handleChangeDate = (value) => {
    setEndate(value._d);
  };

  const handleChangeRepeat = () => {
    setContent((prev) => ({
      ...prev,
      repeat_event: {
        unit_interval: optionRepeat,
        stop_before: moment(endDate).endOf("day").format(dateFormatSecond),
        exclude_day_of_week: lstDate,
      },
    }));
    handleCloseModal();
  };

  const renderModalRepeat = () => {
    return (
      <ModalRepeat
        onCancel={handleCloseModal}
        footer={[
          <Button className="btnSave" key="save" onClick={handleChangeRepeat}>
            {t("common.save")}
          </Button>,
          <Button
            key="cancel"
            onClick={() => {
              handleCloseModal();
            }}
          >
            {t("common.cancel")}
          </Button>,
        ]}
        visible={openModal}
        title={t("calendar.custom")}
      >
        <ContentRepeat>
          <div className="endDate">
            <div>{t("calendar.endDate")}</div>
            <DatePicker
              value={moment(endDate, dateFormatEndDate)}
              onChange={(value) => handleChangeDate(value)}
              format={dateFormatEndDate}
              allowClear={false}
            />
          </div>
          <div className="exceptDate">
            <p>{t("calendar.except")}</p>
            <div className="selectExcept">
              <Checkbox.Group
                onChange={(value) => {
                  setLstDate(value);
                }}
                value={lstDate}
              >
                {listDate.map((date, index) => {
                  return (
                    <Checkbox key={index} value={date.value}>
                      {date.label}
                    </Checkbox>
                  );
                })}
              </Checkbox.Group>
            </div>
          </div>
        </ContentRepeat>
      </ModalRepeat>
    );
  };

  const handleDeleteGuest = (guest) => {
    const deleteGuest = content.guests.filter((item) => item.user_id !== guest);
    setContent((prev) => ({
      ...prev,
      guests: deleteGuest,
    }));
  };

  const handleDeleteShareTo = (id) => {
    const deleteShare = valueShareTo.filter((item) => item !== id);

    setValueShareTo(deleteShare);
  };

  const renderListGuest = (value, option) => {
    return (
      <ContentGuest>
        {value.map((guest) => {
          let statusImg = WaitingIcon;
          let stt = "Maybe";
          if (guest.stt === "accept") {
            statusImg = AcceptIcon;
            stt = t("calendar.go");
          } else if (guest.stt === "reject") {
            statusImg = RejectIcon;
            stt = t("calendar.notGo");
          } else {
            statusImg = WaitingIcon;
            stt = t("calendar.waitting");
          }
          return (
            guest._id !== content.owner && (
              <div className="guestItem" key={guest._id}>
                {option === "user" ? (
                  <Tooltip placement="top" title={stt}>
                    <div className="guestImg">
                      {guest.avatar_config?.url !== "" &&
                      guest.avatar_config?.url ? (
                        <img
                          src={`${BE_URL}${guest.avatar_config?.url}`}
                          alt="Img"
                        />
                      ) : (
                        <img
                          src={
                            option === "agent" || option === "user"
                              ? DefaultAvatarAgent
                              : DefaultAvatarGroup
                          }
                          alt="img"
                        />
                      )}

                      <Status>
                        <img src={statusImg} alt="icon" />
                      </Status>

                      <span>
                        {position === "owner" && (
                          <img
                            onClick={() => {
                              handleDeleteGuest(guest._id);
                            }}
                            src={CloseIcon}
                            alt="close"
                          />
                        )}
                      </span>
                    </div>
                  </Tooltip>
                ) : (
                  <div className="guestImg">
                    {guest.avatar_config?.url !== "" &&
                    guest.avatar_config?.url ? (
                      <img
                        src={`${BE_URL}${guest.avatar_config?.url}`}
                        alt="Img"
                      />
                    ) : (
                      <img
                        src={
                          option === "agent" || option === "user"
                            ? DefaultAvatarAgent
                            : DefaultAvatarGroup
                        }
                        alt="img"
                      />
                    )}

                    <span>
                      {position === "owner" && (
                        <img
                          onClick={() => {
                            handleDeleteShareTo(guest._id);
                          }}
                          src={CloseIcon}
                          alt="close"
                        />
                      )}
                    </span>
                  </div>
                )}
                <div className="guestName">
                  {option === "agent" || option === "user"
                    ? guest.First_Name
                    : guest.name}
                </div>
              </div>
            )
          );
        })}
      </ContentGuest>
    );
  };

  // const renderItemSelect = (item) => {
  //   return (
  //     <ItemGuest>
  //       <span>
  //         {item.avatar_config?.url !== "" && item.avatar_config?.url ? (
  //           <img src={`${BE_URL}${item.avatar_config?.url}`} alt="Img" />
  //         ) : (
  //           <img src={DefaultAvatarAgent} alt="img" />
  //         )}
  //       </span>
  //       <span>{item.Full_Name}</span>
  //     </ItemGuest>
  //   );
  // };

  const colorEvent = (color) => {
    let tempColor = "#20A2A2";
    let temp = [...listColor];

    temp.forEach((item) => {
      if (item.backgroundColor === color) {
        tempColor = item.borderColor;
        return;
      }
    });

    return tempColor;
  };

  const renderListEvent = (list) => {
    return (
      <ListEventWrap>
        <FillterEvent>
          <div className="selectExcept">
            <Checkbox.Group
              value={eventType}
              onChange={(value) => {
                setEventType(value);
              }}
            >
              <Checkbox value="my_event">{t("calendar.myCalendar")}</Checkbox>
              <Checkbox value="shared_event">
                {t("calendar.shareCalendar")}
              </Checkbox>
            </Checkbox.Group>
          </div>
        </FillterEvent>
        <ListWrap>
          {list.map((item) => {
            return (
              <ItemEvent
                onClick={() => handleOnclick(item, "left")}
                key={item._id}
              >
                <ItemColor color={colorEvent(item.backgroundColor)}></ItemColor>
                <ItemInfo>
                  <div className="eventTime">
                    {moment(item.start).format(dateEvent)}
                    <span>-</span>
                    {moment(item.end).format(dateEvent)}
                  </div>
                  <div className="eventName">{item.title}</div>
                </ItemInfo>
              </ItemEvent>
            );
          })}
        </ListWrap>
      </ListEventWrap>
    );
  };
  return (
    <CalendarWrap>
      <CalendarLeft>
        <ItemLeft>
          <Minicalendar
            language={lang ? (lang === "en" ? "en-EN" : "vi") : "en-EN"}
            locale={lang ? (lang === "en" ? "en-EN" : "vi") : "en-EN"}
            // locale="vi"
            onChange={(value) => onChangeDate(value)}
            value={miniCalendar}
          />
          {renderListEvent(lstEventFilter)}
        </ItemLeft>
      </CalendarLeft>

      <CalendarRight>
        <FullCalendar
          locale={lang ? (lang === "en" ? esLocale : viLocale) : esLocale}
          ref={calendarRef}
          titleFormat={{ month: "long", year: "numeric", day: "numeric" }}
          eventDisplay="block"
          // editable={false}
          plugins={[
            dayGridPlugin,
            timeGrigPlugin,
            listPlugin,
            interactionPlugin,
          ]}
          // dateClick={(info) => {
          //   handleOnSelect(info);
          // }}
          events={lstEvent}
          headerToolbar={viewToolbar}
          // eventStartEditable={true}
          // eventDurationEditable={true}
          eventMaxStack={4}
          dayMaxEventRows={4}
          selectable={true}
          initialView={"timeGridDay"}
          views={views}
          stickyHeaderDates={false}
          firstDay={1}
          displayEventEnd={true}
          showNonCurrentDates={false}
          slotEventOverlap={true}
          allDaySlot={false}
          nowIndicator={true}
          scrollTimeReset={true}
          scrollTime={"08:00:00"}
          select={(info) => {
            handleOnSelect(info);
          }}
          eventClick={(info) => {
            handleOnclick(info.event, "right");
          }}
          // eventChange={(info) => {
          //   handleChangeEvent(info.event);
          // }}
        />
      </CalendarRight>

      {renderModal(position)}
      {renderModalRepeat()}

      <ModalConfimDelete
        title={t("calendar.thisEvent")}
        decs={t("calendar.infoDelete")}
        methodDelete={deleteEvent}
        dataDelete={{ id: idEvent, url: mainUrl, urlFilter: url, isCall: true }}
        isLoading={showLoadingScreen}
      />

      <CustomButtonCreate onClick={handleCreateEvent}>
        <span>+ </span>
        {t("common.create")}
      </CustomButtonCreate>

      <CustomSelectView
        value={optionView}
        onChange={(value) => {
          setOptionView(value);
        }}
      >
        {listView.map((item, index) => {
          return (
            <Option key={index} value={item.value}>
              {item.label}
            </Option>
          );
        })}
      </CustomSelectView>

      <ButtonToday onClick={goToDay}>{t("calendar.today")}</ButtonToday>
    </CalendarWrap>
  );
}

export const CalendarWrap = styled.div`
  display: flex;
  width: 100%;
  background-color: white;
  position: relative;

  .fc-today-button::first-letter {
    text-transform: capitalize;
  }

  .fc-daygrid-day.fc-day-today,
  .fc-timegrid-col.fc-day-today {
    background: #eff6ff;
  }

  .fc-daygrid-day-top {
    justify-content: center;
    a {
      /* font-family: var(--roboto-500); */
      font-size: 14px !important;
      line-height: 17px;
      color: #2c2c2c !important;
    }
  }

  .fc-scrollgrid-sync-inner {
    a {
      color: #2c2c2c;
      font-size: 16px;
      line-height: 22px;
    }
  }

  .fc-event-time {
    font-family: var(--roboto-400);
    font-size: 14px;
    color: #6b6b6b;
  }

  .fc-event-title {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
    line-height: 20px;
  }

  .fc-event-main-frame {
    .fc-event-title-container {
      flex: 1;
    }
  }

  .fc-daygrid-event {
    .fc-event-title {
      /* width: 100px; */
      text-overflow: ellipsis;
    }
    .fc-event-time {
      width: fit-content;
    }
  }

  .fc-toolbar-title {
    font-family: var(--roboto-500);
    font-size: 20px;
    color: #000000;
    line-height: 20px;
  }

  .fc-toolbar-chunk:nth-child(2) {
    div {
      display: flex;
      align-items: center;
      .fc-prev-button,
      .fc-next-button {
        background-color: transparent;
        color: #6b6b6b;
        border: none;
        font-size: 16px;
      }
      .fc-button-primary:focus {
        box-shadow: none;
      }
    }
  }

  .fc-toolbar.fc-header-toolbar {
    padding: 16px 0;
    box-shadow: 0px 1px 0px #dddddd;
    margin: 0px;
    .fc-toolbar-chunk {
      :nth-child(2) {
        margin-left: -340px;
      }
    }
  }

  .fc-list-event-dot {
    border-radius: 0px;
    height: 16px;
    border-width: 2px;
  }

  .fc-list-event td {
    border: none;
  }

  .fc-list-event-time {
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #6b6b6b;
  }

  .fc-list-event-title a {
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #2c2c2c;
    line-height: 20px;
  }

  .fc-list-day-text,
  .fc-list-day-side-text {
    font-family: var(--roboto-700);
    font-size: 16px;
    color: #000000;
  }

  .fc-list-day-cushion {
    background: #f4f6f8;
    border-radius: 5px;
  }

  .fc-list-day th {
    border: none;
  }

  .fc-scroller-liquid {
    margin-top: 24px;
    margin-right: 24px;
    padding-bottom: 24px;
  }

  .fc-theme-standard .fc-list {
    border: none;
  }

  .fc-button-group {
    .fc-button-primary {
      display: none;
      border: none;
      padding: 11px;
      box-shadow: none;
      margin: 0 8px;
      width: 70px;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 16px;
      color: #2c2c2c;
      border-radius: 2px !important;
      transition: all 0.5s;
      border: 1px solid #d9d9d9;
      background: #ffffff;

      &:hover,
      &:focus {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
        box-shadow: none !important;
      }
    }
    .fc-button-primary:disabled {
      cursor: not-allowed;
      &:hover,
      &:focus {
        background-color: #ffffff;
        border-color: #d9d9d9;
        color: black;
        box-shadow: none;
      }
    }
  }

  .fc-button-primary:not(:disabled).fc-button-active {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #ffffff;
    box-shadow: none;
  }

  .fc-col-header {
    background: #f5f5f5;
  }

  .fc-scrollgrid-sync-inner {
    padding: 10px 0;
  }

  .ant-popover-placement-bottom {
    padding-top: 0;
  }
`;

export const CalendarRight = styled.div`
  /* width: 84%; */
  flex: 1;
  .fc-daygrid-event-harness a,
  .fc-timegrid-event-harness a {
    border-color: transparent;
  }
  .fc-scrollgrid-liquid {
    padding-right: 24px;
  }
`;

export const CalendarLeft = styled.div`
  border-top: 1px solid #dddddd;
  margin-top: 70px;
  display: flex;
  justify-content: center;
  width: 290px;
`;

export const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
    line-height: normal;
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-footer {
    box-shadow: ${(props) =>
      props.myPostiton !== "share"
        ? "0px -2px 17px rgba(0, 0, 0, 0.16)"
        : "none"};
    border-top: none;
    padding: 18px 24px;

    display: ${(props) => (props.myPostiton !== "share" ? "block" : "none")};
  }

  label {
    font-family: var(--roboto-500);
    font-size: 14px;
    color: #2c2c2c;
  }

  .radioGroup {
    width: 100%;
    margin-bottom: 10px;
  }

  .popoverContent {
    margin-top: 5px;

    button {
      border-radius: 2px;
      transition: all 0.5s;
      width: 110px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;

      span::first-letter {
        text-transform: capitalize;
      }

      img {
        width: 14px;
        height: 14px;
        margin-right: 6px;
      }

      &:hover,
      &:focus {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
        img {
          filter: brightness(200);
        }
      }
    }
  }

  .groupInput {
    display: flex;
  }

  .ant-picker-focused {
    box-shadow: none;
    border: none;
  }
`;

export const CustomInput = styled(Input)`
  font-size: 18px;
  margin-bottom: 16px;
  margin-right: 16px;
  border-top: none;
  border-left: none;
  border-right: none;

  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none !important;
    outline: none !important;
  }

  &:hover {
    border-color: ${(props) => props.theme.main};
  }

  &:placeholder-shown {
    text-overflow: ellipsis;
    font-family: var(--roboto-400);
    color: #6b6b6b;
    font-size: 18px;
  }
`;

export const CustomRadio = styled(Radio)`
  .ant-radio-inner {
    background-color: ${(props) => props.color};
    border: 1.3px solid ${(props) => props.borderColor};
    width: 24px;
    height: 24px;
    transition: all 1s;
  }

  .ant-radio-wrapper {
    margin-right: 16px;
  }

  .ant-radio {
    width: 24px;
  }

  .ant-radio-inner::after {
    background-color: white;
  }

  .ant-radio-checked .ant-radio-inner {
    border: 1.3px solid ${(props) => props.borderColor};
  }
`;

export const CustomRangePicker = styled.div`
  display: flex;
  height: 20px;
  margin-bottom: 6px;

  img {
    margin-right: 8px;
  }

  .datePicker {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    span {
      font-family: var(--roboto-400);
      font-size: 16px;

      color: #000000;
    }
  }
`;

const ErrorStatus = styled.div`
  color: #f13642;
  font-size: 12px;
`;

export const CustomDatePicker = styled(DatePicker)`
  margin-top: 4px;

  padding: 0;

  border: none;

  &:hover {
    border: none;
  }

  .ant-picker-input > input:focus {
    border: none;
  }

  .ant-picker-input > input {
    text-transform: capitalize;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #000000;
    width: 210px;
    cursor: pointer;
    transition: all 0.5s;

    &:hover {
      text-decoration: underline;
    }
  }
`;

export const CustomSelect = styled(Select)`
  width: 100%;
  border-radius: 2px;
  position: absolute;
  top: 0;
  left: 0;

  .ant-select-selector {
    cursor: pointer !important;
    border: none !important;
    border-radius: 5px !important;
    background: transparent !important;
    box-shadow: none !important;
  }

  .ant-select-selection-item {
    display: none;
  }

  .ant-select-selection-overflow {
    padding: 0 5px !important;
  }
`;

export const SelectGuest = styled.div`
  background: #ececec;
  border-radius: 5px;
  padding: 5px 16px;
  cursor: pointer;
  position: relative;
  margin-bottom: 12px;

  span {
    color: #6b6b6b;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
  }
  .addGuest {
    color: transparent;
  }
`;

export const ButtonGroup = styled.div`
  display: flex;
  justify-content: space-between;

  button {
    border-radius: 2px;
    width: 120px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:disabled {
        color: rgba(0, 0, 0, 0.25) !important;
        border-color: #d9d9d9 !important;
        background: #f5f5f5 !important;
      }

      &:last-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;

export const CustomButtonCreate = styled(Button)`
  background-color: ${(props) => props.theme.main};
  border-color: ${(props) => props.theme.main};
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 24px;
  position: absolute;
  top: 16px;
  left: 24px;
  color: #ffffff;
  transition: all 0.5s;
  height: 40px;

  &:hover,
  &:focus {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #ffffff;
  }

  span {
    margin-right: 10px;
  }
`;

export const CustomSelectView = styled(Select)`
  position: absolute;
  top: 16px;
  right: 8px;
  border-radius: 2px;
  z-index: 2;
  width: 147px;
  height: 40px;

  .ant-select-selector {
    height: 40px !important;
    align-items: center;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #2c2c2c;
  }
`;

export const ContentPopover = styled.div`
  background-color: white;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  p {
    cursor: pointer;
    margin: 0;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #2c2c2c;
    display: flex;
    align-items: center;
    transition: all 0.5s;
    padding: 5px 20px;
    border-radius: 2px;

    img {
      width: 14px;
      margin-right: 6px;
    }
    :hover {
      background: #f5f5f5;
    }
  }
`;

export const Minicalendar = styled(Calendar)`
  border: none !important;
  width: 290px !important;
  padding: 16px;

  abbr[title],
  abbr[data-original-title] {
    text-decoration: none;
    cursor: pointer;
    font-family: var(--roboto-500);
    font-size: 10px;
    line-height: 16px;
    color: #71717a;
  }

  .react-calendar__tile {
    font-family: var(--roboto-500);
    font-size: 11px;
    color: #000000;
    border-radius: 50%;
    transition: all 0.5s;
  }

  .react-calendar__month-view__days__day--neighboringMonth {
    color: #757575;
  }

  .react-calendar__tile--active {
    background: rgba(32, 162, 162, 0.3);
  }

  .react-calendar__tile--active:enabled:focus,
  .react-calendar__tile--active:enabled:hover {
    background: rgba(32, 162, 162, 0.3);
  }

  .react-calendar__tile--now,
  .react-calendar__tile--now:enabled:hover,
  .react-calendar__tile--now:enabled:focus {
    background: ${(props) => props.theme.darker};
    color: #ffffff;
  }

  .react-calendar__navigation__prev2-button,
  .react-calendar__navigation__next2-button {
    display: none;
  }
  .react-calendar__navigation__prev-button,
  .react-calendar__navigation__next-button {
    font-family: var(--roboto-500);
    font-size: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .react-calendar__navigation__label {
    font-family: var(--roboto-500);
    font-size: ${(props) => (props.language === "vi" ? "17px" : "20px")};
    margin-top: 6px;
    color: ${(props) => props.theme.darker};
    ::first-letter {
      text-transform: capitalize;
    }
  }

  .react-calendar__navigation button:enabled:hover,
  .react-calendar__navigation button:enabled:focus {
    background-color: transparent;
  }
`;

export const ButtonToday = styled(Button)`
  position: absolute;
  height: 40px;
  top: 16px;
  right: 162px;
  border-radius: 2px;
  font-family: var(--roboto-400);
  font-size: 16px;
  transition: all 0.5s;
  color: #2c2c2c;
  background: #eeeeee;
  border-color: #eeeeee;
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #ffffff;
  }

  &:focus {
    color: #ffffff;
    border-color: #d9d9d9;
    background-color: ${(props) => props.theme.darker};
  }
`;

export const SelectRepeat = styled(Select)`
  display: block;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #6b6b6b;
  transition: all 0.5s;
  margin-bottom: 10px;
  width: 50%;

  &:hover {
    text-decoration: underline;
  }

  &:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: transparent !important;
  }

  .ant-select-selector {
    padding: 0 !important;
  }
`;

export const ModalRepeat = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
    line-height: normal;
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-footer {
    border-top: none;
    padding: 18px 24px;

    button {
      border-radius: 2px;
      width: 80px;
      transition: all 0.5s;

      &:hover {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
      }
    }

    .btnSave {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
      color: #ffffff;
    }
  }
`;

export const ContentRepeat = styled.div`
  width: 100%;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #2c2c2c;

  .endDate {
    display: flex;
    align-items: center;
    margin-bottom: 24px;

    .ant-picker-focused,
    .ant-picker:hover {
      box-shadow: none;
      border-color: ${(props) => props.theme.darker} !important;
    }

    div {
      margin-right: 16px;
    }
  }

  .exceptDate {
    p {
      margin-bottom: 16px;
    }
    .selectExcept {
      .ant-checkbox-wrapper {
        display: flex;
        margin-bottom: 4px;

        span {
          font-family: var(--roboto-400);
          font-size: 16px;
          color: #2c2c2c;
        }
      }

      .ant-checkbox-wrapper + .ant-checkbox-wrapper {
        margin-left: 0px;
      }

      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.main};
      }

      .ant-checkbox:hover .ant-checkbox-inner,
      .ant-checkbox:focus .ant-checkbox-inner,
      .ant-checkbox-wrapper:hover .ant-checkbox-inner,
      .ant-checkbox-input:focus + .ant-checkbox-inner,
      .ant-checkbox-checked::after {
        border-color: ${(props) => props.theme.main} !important;
      }
    }
  }
`;

export const ContentGuest = styled.div`
  display: flex;
  flex-wrap: wrap;

  .guestItem {
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-right: 14px;
    margin-bottom: 10px;

    .guestImg {
      position: relative;

      img {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        cursor: pointer;
        object-fit: cover;
      }
      span {
        transition: all 1s;
        display: none;
        img {
          width: unset;
          height: unset;
          position: absolute;
          top: -6px;
          right: -6px;
          cursor: pointer;
        }
      }
      &:hover {
        span {
          display: unset;
        }
      }
    }

    .guestName {
      font-family: var(--roboto-400);
      font-size: 12px;
      color: rgba(0, 0, 0, 0.45);
    }
  }
`;

export const Status = styled.div`
  img {
    border-radius: 50%;
    position: absolute;
    left: 10px;
    top: 14px;
  }
`;

export const ItemGuest = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  transition: all 0.5s;
  img {
    width: 32px;
    height: 32px;
    border-radius: 50%;
    margin-right: 10px;
  }

  span {
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #000000;
  }
`;

const ConfirmWrap = styled.div`
  display: flex;
  flex-direction: row-reverse;
  width: 100%;
  align-items: center;

  .ant-select:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: ${(props) => props.theme.darker} !important;
  }
`;

const QuestConfirm = styled.span`
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
`;

export const SelectStatus = styled(Select)`
  margin-left: 24px;
  width: 160px;
  .ant-select-selection-item {
    display: flex;
    font-size: 14px;
    color: #2c2c2c;
  }
`;

// const ConFirmStatus = styled.div`
//   width: 100%;
//   display: flex;
//   flex-direction: row-reverse;

//   span {
//     font-family: var(--roboto-400);
//     font-size: 16px;
//     line-height: 22px;
//     color: #2c2c2c;
//   }
// `;

// const ConfirmGroup = styled(Radio.Group)`
//   display: flex;
//   margin-left: 24px;

//   .ant-radio {
//     display: none;
//   }
// `;

// const ItemConfirm = styled(Radio)`
//   span {
//     font-family: var(--roboto-400);
//     font-size: 18px;
//     line-height: 22px;
//     color: ${(props) => (props.isCheck ? props.theme.main : "#6b6b6b")};
//   }

//   &:nth-child(2) {
//     span {
//       border-left: solid 1px #d9d9d9;
//       border-right: solid 1px #d9d9d9;
//       padding: 0 10px;
//     }
//   }
// `;

const ListWrap = styled.div`
  max-height: calc(100vh - 150px);
  @media screen and (max-width: 1500px) {
    max-height: calc(100vh - 300px);
  }
  overflow: auto;
`;

export const ListEventWrap = styled.div`
  padding: 0 16px;
`;

export const FillterEvent = styled.div`
  margin-bottom: 12px;
  .selectExcept {
    .ant-checkbox-group {
      display: flex;
    }
    .ant-checkbox-wrapper {
      display: flex;
      margin-bottom: 4px;

      margin-right: 16px;
      &:last-child {
        margin-right: 0;
      }

      span {
        font-family: var(--roboto-400);
        font-size: 16px;
        color: #2c2c2c;
      }
    }

    .ant-checkbox + span {
      padding-right: 0px;
      padding-left: 4px;
    }

    .ant-checkbox-wrapper + .ant-checkbox-wrapper {
      margin-left: 0px;
    }

    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }

    .ant-checkbox:hover .ant-checkbox-inner,
    .ant-checkbox:focus .ant-checkbox-inner,
    .ant-checkbox-wrapper:hover .ant-checkbox-inner,
    .ant-checkbox-input:focus + .ant-checkbox-inner,
    .ant-checkbox-checked::after {
      border-color: ${(props) => props.theme.main}!important;
    }
  }
`;

export const ItemEvent = styled.div`
  display: flex;
  margin-bottom: 8px;

  cursor: pointer;
`;

export const ItemColor = styled.div`
  width: 14px;
  height: 14px;
  border-radius: 50%;
  background: ${(props) => props.color};
  margin-right: 8px;
`;

export const ItemInfo = styled.div`
  .eventTime {
    font-family: var(--roboto-500);
    font-size: 14px;
    line-height: 16px;
    color: #8c8c8c;
    span {
      margin: 0 4px;
    }
  }
  .eventName {
    font-family: var(--roboto-500);
    font-size: 14px;
    line-height: 16px;
    color: #2c2c2c;

    -webkit-line-clamp: 1;
    width: 210px;
    text-overflow: ellipsis;
    word-break: break-all;
    -webkit-box-orient: vertical;
    overflow: hidden;
    white-space: nowrap;
  }
`;

export const ItemLeft = styled.div`
  width: 100%;
`;

const ContentShare = styled.div``;

const ShareTitle = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 6px;
  span {
    font-size: 18px;
    font-family: var(--roboto-500);
    line-height: 30px;
  }

  .buttonPermission {
    button {
      border-radius: 2px;
      transition: all 0.5s;
      width: 110px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;

      span::first-letter {
        text-transform: capitalize;
      }

      span {
        font-size: 16px;
        font-family: var(--roboto-400);
        color: #2c2c2c;
      }

      img {
        width: 14px;
        height: 14px;
        margin-right: 6px;
      }
      &:hover {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};

        img {
          filter: brightness(200);
        }

        span {
          color: #ffffff;
        }
      }
    }
  }
`;

const SharePickerWrap = styled.div`
  display: flex;

  margin-bottom: 6px;

  img {
    margin-right: 8px;
    margin-bottom: 2px;
  }
`;

const SharePickerContent = styled.div`
  display: flex;

  span {
    &:nth-child(2) {
      margin: 0 16px;
    }
  }
`;

const SharePicker = styled.span`
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  color: #000000;
  &::first-letter {
    text-transform: capitalize;
  }
`;

const CustomHeader = styled.span`
  img {
    width: 17px;
    margin-bottom: 2px;
  }
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  color: #6b6b6b;

  &:hover {
    text-decoration: underline;
  }
`;
const CustomCollapse = styled(Collapse)`
  border: none;
  background-color: #ffffff;

  margin-bottom: 6px;
  .ant-collapse-item {
    border: none;

    .ant-collapse-header {
      padding: 0;
      .ant-collapse-arrow {
        display: none;
      }
    }
  }
  .ant-collapse-content {
    border: none;
    .ant-collapse-content-box {
      margin-top: 16px;
      padding: 0;
    }
  }
  .se-wrapper-inner {
    max-height: 200px !important;
  }
`;
const CustomPanel = styled(Panel)``;
