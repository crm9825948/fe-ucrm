import { Breadcrumb } from "antd";
import React, { useEffect, useState, useRef } from "react";
import { useNavigate } from "react-router";
import {
  Input,
  DatePicker,
  Radio,
  Select,
  Checkbox,
  Button,
  Modal,
  Dropdown,
} from "antd";
import "antd/dist/antd.css";
import { useSelector, useDispatch } from "redux/store";
import moment from "moment";
import styled from "styled-components";
import { loadAllUser } from "redux/slices/user";
import { loadAllGroups } from "redux/slices/group";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  getListEvent,
  updateEvent,
  createEvent,
  deleteEvent,
  updateStatus,
  updateEventDetail,
} from "redux/slices/calendar";

import locale from "antd/es/date-picker/locale/vi_VN";
import { BE_URL } from "constants/constants";

import PrivateIcon from "assets/icons/common/privateIcon.svg";
import PublicIcon from "assets/icons/common/publicIcon.svg";
import DeleteImg from "assets/icons/common/deleteImg.png";
// import ExitImg from "assets/icons/common/exitImg.png";
import CalendarIcon from "assets/icons/common/calendarIcon.svg";
import DefaultAvatarGroup from "assets/icons/common/avtGroupDefault.png";
import DefaultAvatarAgent from "assets/icons/common/avtAgentDefault.png";

import Editor from "components/Editor/Editor2";

import { InputNumber } from "antd";
import { useTranslation } from "react-i18next";

const { Option, OptGroup } = Select;

function CalendarDetail() {
  require("moment/locale/vi");

  const dateFormatSecond = "YYYY-MM-DD HH:mm:ss";
  const dateFormatVi = "dddd, DD [tháng] MM, HH:mm";
  const dateFormatEndDate = "YYYY-MM-DD";

  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const editorJodit = useRef(null);

  const { userDetail, listAllUser } = useSelector((state) => state.userReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const { showLoadingScreen } = useSelector((state) => state.globalReducer);

  const { eventDetail, eventID, listEvent } = useSelector(
    (state) => state.calendarReducer
  );
  const [idEvent, setIdEvent] = useState("");

  const [listShareTo, setListShareTo] = useState([]);

  const [agent, setAgent] = useState([]); //share agent
  const [group, setGroup] = useState([]);

  const [lstShareTo, setLstShareTo] = useState([]);

  // const [listUser, setListUser] = useState([]);

  const [color] = useState("#D2ECEC");

  const [optionRepeat, setOptionRepeat] = useState("");

  const [endDate, setEndate] = useState(new Date());

  const [openModal, setOpenModal] = useState(false);

  const [lstDate, setLstDate] = useState([]);

  const [valueSearch, setValueSearch] = useState("");

  const [valueSearchShare, setValueSearchShare] = useState("");

  const [listIdGuest, setListIdGuest] = useState([]);

  const [position, setPosition] = useState("owner");

  const [valueShareTo, setValueShareTo] = useState([]);

  const [listGuest, setListGuest] = useState([]);

  const [listUserActive, setListUserActive] = useState([]);

  const [disable, setDisabled] = useState(false);

  const [isErr, setIsErr] = useState(false);

  const [lstGuest, setLstGuest] = useState([]);

  const [listPermission, setListPermission] = useState([]);

  const [listRender, setListRender] = useState([]);

  const [status, setStatus] = useState("waiting");

  const [nav, setNav] = useState(false);

  const [content, setContent] = useState({
    start_time: moment(new Date()).format(dateFormatSecond),
    end_time: moment(new Date()).format(dateFormatSecond),
    event_name: "",
    owner: userDetail._id,
    permission: "private",
    description: "",
    users_sharing: [],
    sharing_fields: ["_id", "owner", "start_time", "end_time", "color"],
    alerts: [
      {
        send_mail: false,
        quantity: 10,
        unit: "minutes",
      },
    ],
    guests: [],
    repeat_event: {},
    color: color,
    groups_sharing: [],
  });

  const listStatus = [
    {
      label: t("calendar.going"),
      value: "accept",
    },
    {
      label: t("calendar.notGoing"),
      value: "reject",
    },
    {
      label: t("calendar.later"),
      value: "waiting",
    },
  ];

  const listColor = [
    {
      backgroundColor: "#D2ECEC",
      borderColor: "#20A2A2",
    },
    {
      backgroundColor: "#D1E9FF",
      borderColor: "#1890FF",
    },
    {
      backgroundColor: "#DDF7D5",
      borderColor: "#54D62C",
    },
    {
      backgroundColor: " #FFF3CD",
      borderColor: "#FFC107",
    },
    {
      backgroundColor: "#FFDAD9",
      borderColor: "#FF4842",
    },

    {
      backgroundColor: "#E9DFFD",
      borderColor: "#9161F7",
    },
    {
      backgroundColor: "#FBDAEB",
      borderColor: "#EC4899",
    },
  ];

  const listOptionUnit = [
    {
      label: t("calendar.minutes"),
      value: "minutes",
    },
    {
      label: t("calendar.hours"),
      value: "hours",
    },
    {
      label: t("calendar.days"),
      value: "days",
    },
    {
      label: t("calendar.weeks"),
      value: "weeks",
    },
    {
      label: t("calendar.months"),
      value: "months",
    },
  ];

  const listOptionRepeat = [
    {
      label: t("calendar.notRepeat"),
      value: "",
    },
    {
      label: t("calendar.daily"),
      value: "days",
    },
    {
      label: t("calendar.weekly"),
      value: "weeks",
    },
    {
      label: t("calendar.monthly"),
      value: "months",
    },
    {
      label: t("calendar.annual"),
      value: "years",
    },
  ];

  const listDate = [
    {
      label: t("common.monday"),
      value: 1,
    },
    {
      label: t("common.tuesday"),
      value: 2,
    },
    {
      label: t("common.wednesday"),
      value: 3,
    },
    {
      label: t("common.thurday"),
      value: 4,
    },
    {
      label: t("common.friday"),
      value: 5,
    },
    {
      label: t("common.saturday"),
      value: 6,
    },
    {
      label: t("common.sunday"),
      value: 0,
    },
  ];

  useEffect(() => {
    if (window)
      window.onbeforeunload = () => {
        localStorage.removeItem("id_event");
      };
  }, []);

  useEffect(() => {
    if (localStorage.getItem("id_event")) {
      setIdEvent(localStorage.getItem("id_event"));
    }
  }, []);

  useEffect(() => {
    if (listEvent.data && idEvent !== "" && localStorage.getItem("id_event")) {
      let tem = true;

      listEvent.data.forEach((item) => {
        if (item._id === idEvent && item.permission) {
          tem = false;
          dispatch(
            updateEventDetail({
              start_time: item.start_time,
              end_time: item.end_time,
              event_name: item.event_name,
              owner: item.owner,
              permission: item.permission,
              description: item.description,
              users_sharing: item.users_sharing,
              sharing_fields: item.sharing_fields,
              alerts: item.alerts,
              guests: item.guests,
              repeat_event: item.repeat_event,
              color: item.color,
              groups_sharing: item.groups_sharing,
            })
          );
        }
      });
      setNav(tem);
    }
  }, [listEvent, idEvent, dispatch]);

  useEffect(() => {
    if (idEvent !== "" && nav) {
      navigate("/404");
    }
  }, [nav, idEvent, navigate]);

  useEffect(() => {
    if (Object.keys(eventDetail).length === 0 && idEvent === "")
      setContent((prev) => ({
        ...prev,
        owner: userDetail._id,
      }));
  }, [userDetail, eventDetail, idEvent]);

  useEffect(() => {
    if (!localStorage.getItem("id_event")) setIdEvent(eventID);
  }, [eventID]);

  useEffect(() => {
    const filterList = listGuest.filter(
      (item) => !listIdGuest.includes(item._id)
    );

    const filterRender = listGuest.filter((item) =>
      listIdGuest.includes(item._id)
    );

    let tempFilter = [...filterRender];

    filterRender.forEach((item, idx) => {
      content.guests.forEach((value) => {
        if (item._id === value.user_id) {
          tempFilter[idx] = {
            ...tempFilter[idx],
            stt: value.status,
          };
        }
      });
    });

    setLstGuest(filterList);
    setListRender(tempFilter);
  }, [listIdGuest, listGuest, content]);

  useEffect(() => {
    const temp = listAllUser.filter((user) => user.Active);
    setListUserActive(temp);
  }, [listAllUser]);

  useEffect(() => {
    if (listUserActive) {
      setListGuest(listUserActive);
    }
  }, [listUserActive]);

  useEffect(() => {
    dispatch(getListEvent({ url: "" }));
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(eventDetail).length !== 0) {
      const { users_sharing, groups_sharing, guests } = eventDetail;

      guests?.forEach((item) => {
        if (item.user_id === userDetail._id) {
          setStatus(item.status);
          return;
        }
      });
      let arr = [];
      users_sharing.forEach((item) => {
        arr.push(item);
      });

      groups_sharing.forEach((item) => {
        arr.push(item);
      });

      setValueShareTo(arr);

      setContent(eventDetail);

      const shareDefault = ["_id", "owner", "start_time", "end_time", "color"];
      let tem = [];
      eventDetail.sharing_fields.forEach((field) => {
        if (!shareDefault.includes(field)) {
          tem.push(field);
        }
      });
      setListPermission(tem);
    }
  }, [eventDetail, userDetail]);

  useEffect(() => {
    const temp = content.guests.map((guest) => {
      return guest.user_id;
    });
    setListIdGuest(temp);

    if (
      content.event_name.length === 0 ||
      content.start_time === content.end_time ||
      isNaN(new Date(content.start_time).valueOf()) ||
      isNaN(new Date(content.end_time).valueOf())
    ) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }

    const start = content.start_time;
    const end = content.end_time;
    if (
      new Date(start).getTime() > new Date(end).getTime() ||
      new Date(start).getTime() === new Date(end).getTime()
    ) {
      // setContent((prev) => ({
      //   ...prev,
      //   start_time: end,
      //   end_time: start,
      // }));
      setDisabled(true);
      setIsErr(true);
    } else {
      setIsErr(false);
    }
  }, [content]);

  useEffect(() => {
    const { owner, guests } = content;
    let isGuest = false;

    guests.forEach((item) => {
      if (item.user_id === userDetail._id) {
        isGuest = true;
        return;
      }
    });

    if (userDetail._id === owner) {
      setPosition("owner");
    } else if (isGuest === true) {
      setPosition("guest");
    } else {
      setPosition("share");
    }
  }, [userDetail, content]);

  useEffect(() => {
    const { stop_before, exclude_day_of_week } = content.repeat_event;
    if (!openModal) {
      if (stop_before) {
        setEndate(new Date(stop_before));
      } else {
        setEndate(new Date());
      }
      if (exclude_day_of_week) {
        setLstDate(exclude_day_of_week);
      } else {
        setLstDate([]);
      }
    }
  }, [openModal, content.repeat_event]);

  useEffect(() => {
    const filterAgent = listUserActive.filter((user) =>
      valueShareTo.includes(user._id)
    );

    const filterGroup = listAllGroups.filter((group) =>
      valueShareTo.includes(group._id)
    );

    setAgent(filterAgent);
    setGroup(filterGroup);
  }, [valueShareTo, listAllGroups, listUserActive]);

  useEffect(() => {
    const shareUser = agent.map((item) => {
      return item._id;
    });

    const shareGroup = group.map((item) => {
      return item._id;
    });

    setContent((prev) => ({
      ...prev,
      users_sharing: shareUser,
      groups_sharing: shareGroup,
    }));
  }, [agent, group]);

  useEffect(() => {
    const filterShare = listShareTo.map((item) => {
      return {
        label: item.label,
        options: item.options.filter(
          (value) => !valueShareTo.includes(value._id)
        ),
      };
    });
    setLstShareTo(filterShare);
  }, [listShareTo, valueShareTo]);

  useEffect(() => {
    const listShare = [];

    if (listUserActive.length > 0) {
      listShare.push({
        label: "Agent",
        options: listUserActive,
      });
    }
    if (listAllGroups.length > 0) {
      listShare.push({
        label: "Group",
        options: listAllGroups,
      });
    }
    setListShareTo(listShare);
  }, [listAllGroups, listUserActive, userDetail]);

  const onChangePermission = (value) => {
    setContent((prev) => ({
      ...prev,
      permission: value,
    }));
  };

  const renderPopover = () => {
    return (
      <ContentPopover>
        <p
          onClick={() => {
            onChangePermission("private");
          }}
        >
          <img src={PrivateIcon} alt="Private" /> {t("calendar.private")}
        </p>
        <p
          onClick={() => {
            onChangePermission("public");
          }}
        >
          <img src={PublicIcon} alt="Public" /> {t("calendar.public")}
        </p>
      </ContentPopover>
    );
  };

  const updateEventStatus = (value) => {
    dispatch(
      updateStatus({
        status: value,
        event_id: idEvent,
        guest_id: userDetail._id,
        url: "",
      })
    );
    setTimeout(() => {
      navigate("/calendar");
    }, 1000);
  };

  const handleDeleteGuest = (guest) => {
    const deleteGuest = content.guests.filter((item) => item.user_id !== guest);
    setContent((prev) => ({
      ...prev,
      guests: deleteGuest,
    }));
  };

  const handleDeleteShareTo = (id) => {
    const deleteShare = valueShareTo.filter((item) => item !== id);

    setValueShareTo(deleteShare);
  };

  const handleChangeGuest = (value) => {
    const temp = [];

    // value.forEach((v, idx) => {
    //   content.guests.forEach((item) => {
    //     if (v === item.user_id) {
    //       temp.push(item);
    //       value.splice(idx, 1);
    //     }
    //   });
    // });

    content.guests.forEach((item) => {
      if (value.includes(item.user_id)) {
        temp.push(item);
        value = value.filter((va) => va !== item.user_id);
      }
    });

    value.forEach((item) => {
      temp.push({
        user_id: item,
        status: "waiting",
      });
    });

    setContent((prev) => ({
      ...prev,
      guests: temp,
    }));
  };

  const handleOnchange = (key, value) => {
    setContent((prev) => ({
      ...prev,
      [key]: value,
    }));
  };

  const handleChangeTime = (key, value) => {
    if (value) {
      setContent((prev) => ({
        ...prev,
        [key]: moment(value).format(dateFormatSecond),
      }));
    } else {
      setContent((prev) => ({
        ...prev,
        [key]: moment(new Date()).format(dateFormatSecond),
      }));
    }
  };

  const addNoti = () => {
    let arr = [...content.alerts];
    arr.push({ unit: "minutes", quantity: 10, send_mail: false });
    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const handleChangeListNoti = (key, value, idx) => {
    let arr = [...content.alerts];

    content.alerts.forEach((item, index) => {
      if (index === idx) {
        switch (key) {
          case "mail":
            arr[index] = {
              ...arr[index],
              send_mail: value,
            };
            break;
          case "quantity":
            arr[index] = {
              ...arr[index],
              quantity: value,
            };
            break;
          case "unit":
            arr[index] = {
              ...arr[index],
              unit: value,
            };
            break;
          default:
            break;
        }
      }
    });

    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const handleDeleteNoti = (idx) => {
    let arr = [...content.alerts];
    if (arr.length === 1) return;
    arr.splice(idx, 1);
    setContent((prev) => ({
      ...prev,
      alerts: arr,
    }));
  };

  const onChangeRepeat = (value) => {
    setOptionRepeat(value);

    if (!content.repeat_event.stop_before) {
      setEndate(new Date());
    }

    // setLstDate([]);
    if (value !== "") {
      setOpenModal(true);
    } else {
      setContent((prev) => ({
        ...prev,
        repeat_event: {},
      }));
    }
  };

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const handleChangeRepeat = () => {
    setContent((prev) => ({
      ...prev,
      repeat_event: {
        unit_interval: optionRepeat,
        stop_before: moment(endDate).endOf("day").format(dateFormatSecond),
        exclude_day_of_week: lstDate,
      },
    }));
    handleCloseModal();
  };

  const handleChangeDate = (value) => {
    setEndate(value._d);
  };

  const renderListGuest = (value, option) => {
    return (
      <ContentGuest>
        {value.map((guest) => {
          return (
            guest._id !== content.owner && (
              <div className="guestItem" key={guest._id}>
                <div className="guestInfo">
                  {guest.avatar_config?.url !== "" &&
                  guest.avatar_config?.url ? (
                    <img
                      src={`${BE_URL}${guest.avatar_config?.url}`}
                      alt="Img"
                    />
                  ) : (
                    <img
                      src={
                        option === "agent" || option === "user"
                          ? DefaultAvatarAgent
                          : DefaultAvatarGroup
                      }
                      alt="img"
                    />
                  )}
                  <div className="infoDetail">
                    <span>
                      {option === "agent" || option === "user"
                        ? guest.Full_Name
                        : guest.name}
                    </span>
                    {option === "user" && <span>{guest.stt}</span>}
                  </div>
                </div>

                <div className="closeImg">
                  {position === "owner" && guest._id !== userDetail._id && (
                    <img
                      onClick={() => {
                        if (option === "user") {
                          handleDeleteGuest(guest._id);
                        } else {
                          handleDeleteShareTo(guest._id);
                        }
                      }}
                      src={DeleteImg}
                      alt="close"
                    />
                  )}
                </div>
              </div>
            )
          );
        })}
      </ContentGuest>
    );
  };

  const renderModalRepeat = () => {
    return (
      <ModalRepeat
        onCancel={handleCloseModal}
        footer={[
          <Button className="btnSave" key="save" onClick={handleChangeRepeat}>
            {t("common.save")}
          </Button>,
          <Button
            key="cancel"
            onClick={() => {
              handleCloseModal();
            }}
          >
            {t("common.cancel")}
          </Button>,
        ]}
        visible={openModal}
        title={t("calendar.custom")}
      >
        <ContentRepeat>
          <div className="endDate">
            <div>{t("calendar.endDate")}</div>
            <DatePicker
              value={moment(endDate, dateFormatEndDate)}
              onChange={(value) => handleChangeDate(value)}
              format={dateFormatEndDate}
              allowClear={false}
            />
          </div>
          <div className="exceptDate">
            <p>{t("calendar.except")}</p>
            <div className="selectExcept">
              <Checkbox.Group
                onChange={(value) => {
                  setLstDate(value);
                }}
                value={lstDate}
              >
                {listDate.map((date, index) => {
                  return (
                    <Checkbox key={index} value={date.value}>
                      {date.label}
                    </Checkbox>
                  );
                })}
              </Checkbox.Group>
            </div>
          </div>
        </ContentRepeat>
      </ModalRepeat>
    );
  };

  const handleEvent = () => {
    const shareDefault = ["_id", "owner", "start_time", "end_time", "color"];
    listPermission.forEach((field) => {
      shareDefault.push(field);
    });

    let temp = { ...content, description: editorJodit.current.value };

    temp.sharing_fields = shareDefault;

    if (idEvent === "") {
      dispatch(
        createEvent({
          data: temp,
          url: "",
        })
      );
    } else {
      dispatch(
        updateEvent({
          data: temp,
          id: idEvent,
          url: "",
        })
      );
    }
    setTimeout(() => {
      navigate("/calendar");
    }, 1000);
  };

  const renderOwner = () => {
    const owner = listUserActive.filter((user) => user._id === content.owner);

    if (owner[0])
      return (
        <ContentGuest>
          <Guest>
            <p>
              <span>{t("calendar.owner")}</span>
            </p>

            <div className="guestContent">
              <div className="guestItem">
                <div className="guestInfo">
                  {owner[0].avatar_config?.url !== "" &&
                  owner[0].avatar_config?.url ? (
                    <img
                      src={`${BE_URL}${owner[0].avatar_config?.url}`}
                      alt="Img"
                    />
                  ) : (
                    <img src={DefaultAvatarAgent} alt="img" />
                  )}
                  <span>
                    {owner[0]._id === userDetail._id
                      ? "Tôi"
                      : owner[0].Full_Name}
                  </span>
                </div>
              </div>
            </div>
          </Guest>
        </ContentGuest>
      );
  };

  const renderLeftContent = (pos) => {
    return (
      <>
        {pos === "owner" && (
          <Content>
            <div className="groupTitle">
              {/* <span
                onClick={() => {
                  navigate("/calendar");
                }}
              >
                <img src={ExitImg} alt="Thoat" />
              </span> */}
              <CustomInput
                value={content.event_name}
                placeholder={t("calendar.addTitle")}
                onChange={(e) => {
                  handleOnchange("event_name", e.target.value);
                }}
              />

              <Dropdown placement="bottom" overlay={renderPopover()}>
                <div className="popoverContent">
                  <Button>
                    <img
                      src={
                        content.permission === "private"
                          ? PrivateIcon
                          : PublicIcon
                      }
                      alt="icon"
                    />
                    <span>
                      {" "}
                      {content.permission === "private"
                        ? t("calendar.private")
                        : t("calendar.public")}
                    </span>
                  </Button>
                </div>
              </Dropdown>
            </div>

            <div className="myTitle">
              <p>
                <span>{t("calendar.detailEvent")}</span>
              </p>
            </div>

            <RadioGroup
              onChange={(e) => handleOnchange("color", e.target.value)}
              value={content.color}
              className="radioGroup"
            >
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </RadioGroup>

            <CustomRangePicker>
              <img src={CalendarIcon} alt="Calendar icon" />
              <div className="datePicker">
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("start_time", value);
                  }}
                  value={moment(content.start_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
                <span>-</span>
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("end_time", value);
                  }}
                  value={moment(content.end_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
              </div>
            </CustomRangePicker>

            {isErr && <ErrorStatus>{t("calendar.err")}</ErrorStatus>}

            <SelectRepeat
              disabled={idEvent !== "" ? true : false}
              bordered={false}
              showArrow={false}
              value={
                Object.keys(content.repeat_event).length !== 0
                  ? content.repeat_event.unit_interval
                  : optionRepeat
              }
              onChange={(value) => onChangeRepeat(value)}
              onSelect={(value) => {
                if (value !== "") {
                  setOpenModal(true);
                }
              }}
            >
              {listOptionRepeat.map((option, index) => {
                return (
                  <Option key={index} value={option.value}>
                    {option.label}
                  </Option>
                );
              })}
            </SelectRepeat>

            <SharePermisson>
              <span>{t("calendar.permission")}</span>

              <div className="selectExcept">
                <Checkbox.Group
                  value={listPermission}
                  onChange={(value) => {
                    setListPermission(value);
                  }}
                >
                  <Checkbox value="event_name">{t("calendar.title")}</Checkbox>
                  <Checkbox value="description">
                    {t("calendar.description")}
                  </Checkbox>
                  <Checkbox value="guests">{t("calendar.guestList")}</Checkbox>
                </Checkbox.Group>
              </div>
            </SharePermisson>
            <label>{t("calendar.notices")}</label>
            <CustomNotiWrap>
              {content.alerts.map((noti, index) => {
                return (
                  <CustomNotiGroup key={index}>
                    <CustomSelectNoti
                      value={noti.send_mail}
                      onChange={(value) =>
                        handleChangeListNoti("mail", value, index)
                      }
                    >
                      <Option value={false}>{t("calendar.noti")}</Option>
                      <Option value={true}>{t("calendar.email")}</Option>
                    </CustomSelectNoti>

                    <CustomInputNumber
                      min={1}
                      onChange={(value) =>
                        handleChangeListNoti("quantity", value, index)
                      }
                      value={noti.quantity}
                    />

                    <CustomSelectNoti
                      onChange={(value) =>
                        handleChangeListNoti("unit", value, index)
                      }
                      value={noti.unit}
                    >
                      {listOptionUnit.map((unit, index) => {
                        return (
                          <Option key={index} value={unit.value}>
                            {unit.label}
                          </Option>
                        );
                      })}
                    </CustomSelectNoti>
                    {content.alerts.length > 1 && (
                      <img
                        onClick={() => handleDeleteNoti(index)}
                        src={DeleteImg}
                        alt="delete"
                      />
                    )}
                  </CustomNotiGroup>
                );
              })}
              <CustomAddNoti>
                <span>+</span> <p onClick={addNoti}>{t("calendar.addNoti")}</p>
              </CustomAddNoti>
            </CustomNotiWrap>

            <Editor
              editorJodit={editorJodit}
              content={content.description}
              showAppend={false}
              minHeightInput="200px"
            />
          </Content>
        )}

        {pos === "guest" && (
          <Content>
            <div className="groupTitle">
              <CustomInput
                value={content.event_name}
                placeholder={t("calendar.addTitle")}
              />

              <div className="popoverContent">
                <Button>
                  <img
                    src={
                      content.permission === "private"
                        ? PrivateIcon
                        : PublicIcon
                    }
                    alt="icon"
                  />
                  <span>
                    {" "}
                    {content.permission === "private"
                      ? t("calendar.private")
                      : t("calendar.public")}
                  </span>
                </Button>
              </div>
            </div>

            <div className="myTitle">
              <p>
                <span>{t("calendar.detailEvent")}</span>
              </p>
            </div>

            <RadioGroup value={content.color} className="radioGroup">
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </RadioGroup>

            <SharePickerWrap>
              <img src={CalendarIcon} alt="Calendar icon" />
              <SharePickerContent>
                <SharePicker>
                  {moment(content.start_time).format(dateFormatVi)}
                </SharePicker>
                <span>-</span>
                <SharePicker>
                  {moment(content.end_time).format(dateFormatVi)}
                </SharePicker>
              </SharePickerContent>
            </SharePickerWrap>

            <Editor
              editorJodit={editorJodit}
              content={content.description}
              showAppend={false}
              minHeightInput="200px"
            />
          </Content>
        )}

        {pos === "share" && (
          <Content>
            <div className="groupTitle">
              {/* <span
                onClick={() => {
                  navigate("/calendar");
                }}
              >
                <img src={ExitImg} alt="Thoat" />
              </span> */}
              {content.event_name !== "" && (
                <CustomInput
                  value={content.event_name}
                  placeholder={t("calendar.addTitle")}
                  onChange={(e) => {
                    handleOnchange("event_name", e.target.value);
                  }}
                />
              )}
            </div>

            <div className="myTitle">
              {" "}
              <p>
                <span>{t("calendar.detailEvent")}</span>
              </p>
            </div>

            <RadioGroup
              onChange={(e) => handleOnchange("color", e.target.value)}
              value={content.color}
              className="radioGroup"
            >
              {listColor.map((item, index) => {
                return (
                  <CustomRadio
                    key={index}
                    color={item.borderColor}
                    value={item.backgroundColor}
                    borderColor={item.borderColor}
                  ></CustomRadio>
                );
              })}
            </RadioGroup>

            <CustomRangePicker>
              <img src={CalendarIcon} alt="Calendar icon" />
              <div className="datePicker">
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("start_time", value);
                  }}
                  value={moment(content.start_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
                <span>-</span>
                <CustomDatePicker
                  allowClear={false}
                  locale={locale}
                  suffixIcon=""
                  onChange={(value) => {
                    handleChangeTime("end_time", value);
                  }}
                  value={moment(content.end_time, dateFormatSecond)}
                  showTime={{
                    format: "HH:mm",
                  }}
                  format={dateFormatVi}
                />
              </div>
            </CustomRangePicker>

            {content.description !== "" && (
              <div style={{ marginTop: "12px" }}>
                <Editor
                  editorJodit={editorJodit}
                  content={content.description}
                  showAppend={false}
                  minHeightInput="200px"
                />
              </div>
            )}
          </Content>
        )}
      </>
    );
  };

  const renderRightContent = (pos) => {
    return (
      <ContentRight>
        {(pos === "owner" || pos === "guest") && (
          <>
            {pos === "owner" && (
              <ButtonGroup>
                <Button
                  onClick={() => {
                    handleEvent();
                  }}
                  disabled={disable}
                >
                  {t("common.save")}
                </Button>
                {idEvent !== "" && (
                  <Button
                    onClick={() => {
                      dispatch(setShowModalConfirmDelete(true));
                    }}
                  >
                    {t("common.delete")}
                  </Button>
                )}
                <Button onClick={() => navigate("/calendar")}>
                  {" "}
                  {t("common.cancel")}
                </Button>
              </ButtonGroup>
            )}

            {pos === "guest" && (
              <SelectStatus
                value={status}
                placeholder="Please confirm"
                onChange={(value) => {
                  setStatus(value);
                  updateEventStatus(value);
                }}
              >
                {listStatus.map((stt, index) => {
                  return (
                    <Option key={index} value={stt.value}>
                      {stt.label}
                    </Option>
                  );
                })}
              </SelectStatus>
            )}
            <ContentUser>
              <Guest>
                <p>
                  <span> {t("calendar.guest")}</span>
                </p>

                <div className="guestContent">
                  {pos === "owner" && (
                    <SelectGuest>
                      {!valueSearch ? (
                        <span>{t("calendar.add")}</span>
                      ) : (
                        <span className="addGuest">{t("calendar.add")}</span>
                      )}

                      <CustomSelectUser
                        onSelect={() => setValueSearch("")}
                        mode="multiple"
                        optionFilterProp="children"
                        value={listIdGuest}
                        onSearch={(value) => {
                          setValueSearch(value);
                        }}
                        onChange={(value) => {
                          handleChangeGuest(value);
                        }}
                      >
                        {lstGuest.map((item) => {
                          return (
                            item._id !== userDetail._id && (
                              <Option key={item._id} value={item._id}>
                                {/* {renderItemSelect(item)} */}
                                {item.Full_Name}
                              </Option>
                            )
                          );
                        })}
                      </CustomSelectUser>
                    </SelectGuest>
                  )}
                  <ListWrap> {renderListGuest(listRender, "user")}</ListWrap>
                </div>
              </Guest>
              <Share>
                {content.permission === "private" && (
                  <>
                    <p>
                      <span>{t("calendar.shareUser")}</span>
                    </p>

                    <div className="guestContent">
                      {pos === "owner" && (
                        <SelectGuest>
                          {!valueSearchShare ? (
                            <span>{t("calendar.share")}</span>
                          ) : (
                            <span className="addGuest">
                              {t("calendar.share")}
                            </span>
                          )}
                          <CustomSelectUser
                            onSelect={() => setValueSearchShare("")}
                            mode="multiple"
                            optionFilterProp="children"
                            value={valueShareTo}
                            onSearch={(value) => {
                              setValueSearchShare(value);
                            }}
                            onChange={(value) => {
                              setValueShareTo(value);
                            }}
                          >
                            {lstShareTo.map((item, index) => {
                              return (
                                <OptGroup label={item.label} key={index}>
                                  {item.options.map((option) => {
                                    return (
                                      option._id !== userDetail._id && (
                                        <Option
                                          key={option._id}
                                          value={option._id}
                                        >
                                          {item.label === "Agent"
                                            ? option.Full_Name
                                            : option.name}
                                        </Option>
                                      )
                                    );
                                  })}
                                </OptGroup>
                              );
                            })}
                          </CustomSelectUser>
                        </SelectGuest>
                      )}
                    </div>

                    <ListShareTo>
                      {renderListGuest(agent, "agent")}
                      {renderListGuest(group, "group")}
                    </ListShareTo>
                  </>
                )}
              </Share>
            </ContentUser>
          </>
        )}

        {pos === "share" && (
          <>
            {content.guests.length !== 0 && (
              <Guest>
                <p>
                  <span>{t("calendar.guest")}</span>
                </p>

                <div className="guestContent">
                  <ListWrap> {renderListGuest(listRender, "user")}</ListWrap>
                </div>
              </Guest>
            )}
          </>
        )}

        {renderOwner()}
      </ContentRight>
    );
  };

  return (
    <CustomWrap>
      <Breadcrumb>
        <CustomItem
          onClick={() => {
            navigate("/calendar");
          }}
        >
          {t("calendar.myCalendar")}
        </CustomItem>
        <BreadcrumbItem>
          {idEvent === "" ? t("calendar.addEvent") : t("calendar.detailEvent")}
        </BreadcrumbItem>
      </Breadcrumb>

      <ContentWrap>
        {renderLeftContent(position)}
        {renderRightContent(position)}
      </ContentWrap>
      {renderModalRepeat()}
      <ModalConfimDelete
        title={t("calendar.thisEvent")}
        decs={t("calendar.infoDelete")}
        methodDelete={deleteEvent}
        dataDelete={{ id: idEvent, url: "", isBack: true }}
        isLoading={showLoadingScreen}
      />
    </CustomWrap>
  );
}

export default CalendarDetail;

export const CustomWrap = styled.div`
  padding-left: 24px;
  padding-top: 20px;
  background-color: #ffffff;
  overflow: auto;

  min-height: 100%;

  label {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
  }

  .radioGroup {
    width: 100%;
    margin-top: 4px;
    margin-bottom: 12px;
  }

  .sun-editor {
    height: 383px;
  }

  .ant-picker-focused {
    box-shadow: none;
  }
`;

export const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #2c2c2c;
  cursor: context-menu;
`;

export const CustomItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-400);
  font-size: 16px;
  color: rgba(0, 0, 0, 0.45);
  cursor: pointer;
`;

export const ContentWrap = styled.div`
  display: flex;
  margin-top: 19px;
`;

export const Content = styled.div`
  width: 50%;
  .myTitle {
    p {
      span {
        font-family: var(--roboto-500);
        font-size: 16px;

        color: ${(props) => props.theme.darker};
        border-bottom: 2px solid ${(props) => props.theme.darker};
        padding-bottom: 13px;
      }

      border-bottom: 1px solid rgba(0, 0, 0, 0.06);
      padding-bottom: 10px;
    }
  }

  .groupTitle {
    display: flex;

    img {
      margin-bottom: 16px;
      margin-right: 10px;
      margin: 7px 10px 16px 0px;
      cursor: pointer;
    }

    button {
      border-radius: 2px;
      transition: all 0.5s;
      width: 110px;
      display: flex;
      justify-content: center;
      align-items: center;
      font-family: var(--roboto-400);
      font-size: 16px;
      color: #2c2c2c;
      height: 37px;
      margin-left: 10px;
      span::first-letter {
        text-transform: capitalize;
      }

      img {
        width: 14px;
        height: 14px;
        margin: 0px 6px 0px 0px;
      }

      &:hover,
      &:focus {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
        img {
          filter: brightness(200);
        }
      }
    }
  }
`;

export const ContentRight = styled.div`
  flex: 1;
  margin-left: 16px;
`;

export const ContentUser = styled.div`
  display: flex;
  width: 100%;
`;

export const Guest = styled.div`
  .guestContent {
    padding-right: 16px;
  }
  width: 50%;
  p {
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: ${(props) => props.theme.darker};
      border-bottom: 2px solid ${(props) => props.theme.darker};
      padding-bottom: 12px;
    }

    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
    padding-bottom: 10px;
  }
`;
export const Share = styled.div`
  .guestContent {
    padding-right: 16px;
  }
  width: 50%;
  p {
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: ${(props) => props.theme.darker};
      border-bottom: 2px solid ${(props) => props.theme.darker};
      padding-bottom: 12px;
    }

    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
    padding-bottom: 10px;
  }
`;

export const CustomInput = styled(Input)`
  font-size: 18px;
  margin-bottom: 15px;

  border-top: none;
  border-left: none;
  border-right: none;

  &:focus {
    border-color: ${(props) => props.theme.darker};
    box-shadow: none !important;
    outline: none !important;
  }

  &:hover {
    border-color: ${(props) => props.theme.darker};
  }

  &:placeholder-shown {
    text-overflow: ellipsis;
    font-family: var(--roboto-400);
    color: #6b6b6b;
    font-size: 18px;
  }
`;

export const CustomInputDisabled = styled(Input)`
  margin-top: 4px;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #2c2c2c !important;
  margin-bottom: 16px;
`;

const RadioGroup = styled(Radio.Group)`
  display: flex;
`;

export const CustomRadio = styled(Radio)`
  .ant-radio-inner {
    background-color: ${(props) => props.color};
    border: 1.3px solid ${(props) => props.borderColor};
    width: 24px;
    height: 24px;
    transition: all 1s;
  }

  .ant-radio {
    width: 24px;
  }

  .ant-radio-inner::after {
    background-color: white;
  }

  .ant-radio-checked .ant-radio-inner {
    border: 1.3px solid ${(props) => props.borderColor};
  }
`;

export const CustomRangePicker = styled.div`
  display: flex;
  height: 20px;
  margin-bottom: 2px;

  img {
    margin-right: 8px;
  }

  .datePicker {
    display: flex;
    justify-content: space-between;
    align-items: center;
    span {
      font-family: var(--roboto-400);
      font-size: 16px;
      margin-right: 10px;
      color: #000000;
    }
  }
`;

const ErrorStatus = styled.div`
  color: #f13642;
  font-size: 12px;
`;

export const CustomDatePicker = styled(DatePicker)`
  margin-top: 4px;

  padding: 0;

  border: none;

  &:hover {
    border: none;
  }

  .ant-picker-input > input:focus {
    border: none;
  }

  .ant-picker-input > input {
    text-transform: capitalize;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #000000;
    width: 210px;
    cursor: pointer;

    transition: all 0.5s;

    &:hover {
      text-decoration: underline;
    }
  }
`;

export const CustomSelect = styled(Select)`
  width: 100%;
  margin: 6px 0 16px 0;
  border-radius: 2px;
`;

export const CustomNotiWrap = styled.div`
  width: 100%;
  margin: 6px 0 16px 0;
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input-number:hover {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

export const CustomNotiGroup = styled.div`
  width: 100%;
  margin-bottom: 8px;

  img:hover {
    cursor: pointer;
  }
`;

export const CustomInputNumber = styled(InputNumber)`
  width: 90px;
  margin-right: 16px;
  border-radius: 2px;
`;

export const CustomSelectNoti = styled(Select)`
  width: 135px;
  margin-right: 16px;
  border-radius: 2px;
`;

export const CustomAddNoti = styled.div`
  display: flex;
  align-items: center;
  p {
    border: none;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: ${(props) => props.theme.darker};
    padding: 0;
    margin: 0;
    &:hover {
      cursor: pointer;
      text-decoration: underline;
    }
  }
  span {
    margin-right: 4px;
    border: none;
    padding: 0;
    font-family: var(--roboto-400);
    font-size: 20px;
    color: ${(props) => props.theme.darker};
  }
`;

export const SelectRepeat = styled(Select)`
  display: block;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #6b6b6b;
  transition: all 0.5s;
  margin-bottom: 6px;
  width: 50%;

  &:hover {
    text-decoration: underline;
  }

  &:not(.ant-select-disabled):hover .ant-select-selector {
    border-color: transparent !important;
  }

  .ant-select-selector {
    padding: 0 !important;
  }
`;

export const ModalRepeat = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
    line-height: normal;
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-footer {
    border-top: none;
    padding: 18px 24px;

    button {
      border-radius: 2px;
      width: 80px;
      transition: all 0.5s;

      &:hover {
        background-color: ${(props) => props.theme.darker};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;
      }
    }

    .btnSave {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
      color: #ffffff;
    }
  }
`;

export const ContentRepeat = styled.div`
  width: 100%;
  font-family: var(--roboto-400);
  font-size: 16px;
  color: #2c2c2c;

  .endDate {
    display: flex;
    align-items: center;
    margin-bottom: 24px;

    .ant-picker-focused,
    .ant-picker:hover {
      box-shadow: none;
      border-color: ${(props) => props.theme.darker} !important;
    }

    div {
      margin-right: 16px;
    }
  }

  .exceptDate {
    p {
      margin-bottom: 16px;
    }
    .selectExcept {
      .ant-checkbox-wrapper {
        display: flex;
        margin-bottom: 4px;

        span {
          font-family: var(--roboto-400);
          font-size: 16px;
          color: #2c2c2c;
        }
      }

      .ant-checkbox-wrapper + .ant-checkbox-wrapper {
        margin-left: 0px;
      }

      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.main};
      }

      .ant-checkbox:hover .ant-checkbox-inner,
      .ant-checkbox:focus .ant-checkbox-inner,
      .ant-checkbox-wrapper:hover .ant-checkbox-inner,
      .ant-checkbox-input:focus + .ant-checkbox-inner,
      .ant-checkbox-checked::after {
        border-color: ${(props) => props.theme.main} !important;
      }
    }
  }
`;

export const ButtonGroup = styled.div`
  display: flex;

  margin-bottom: 16px;

  button {
    border-radius: 2px;
    width: 78px;
    height: 37px;
    transition: all 0.5s;
    &:disabled {
      color: rgba(0, 0, 0, 0.25) !important;
      border-color: #d9d9d9 !important;
      background: #f5f5f5 !important;
    }

    &:first-child {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.darker};
      color: #ffffff;
    }

    &:nth-child(2),
    &:nth-child(3) {
      margin-left: 16px;
    }

    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};
      color: #ffffff;
    }
  }
`;

export const ContentGuest = styled.div`
  /* max-height: 240px;
  overflow: auto; */
  .guestItem {
    width: 100%;
    align-items: center;
    display: flex;
    justify-content: space-between;
    padding: 10px;

    transition: all 1s;
    cursor: context-menu;

    &:hover {
      background: #f5f5f5;
      border-radius: 2px;

      .closeImg {
        opacity: 1;
      }
    }

    .guestInfo {
      display: flex;
      align-items: center;
      img {
        width: 40px;
        height: 40px;
        border-radius: 50%;
        margin-right: 10px;
        object-fit: cover;
      }
      .infoDetail {
        display: flex;
        flex-direction: column;
        span {
          font-family: var(--roboto-400);
          font-size: 16px;
          line-height: 20px;
          color: #000000;
          margin-bottom: 3px;

          &:nth-child(2) {
            font-size: 12px;
            color: #6b6b6b;
            line-height: 12px;

            &::first-letter {
              text-transform: capitalize;
            }
          }
        }
      }
    }

    .closeImg {
      opacity: 0;
      cursor: pointer;

      transition: all 0.5s;
    }
  }
`;

export const SelectGuest = styled.div`
  border-radius: 2px;
  padding: 5px 16px;
  cursor: pointer;
  position: relative;
  margin-bottom: 12px;
  border: 1px solid #d9d9d9;
  span {
    color: #6b6b6b;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
  }
  .addGuest {
    color: transparent;
  }
`;

export const CustomSelectUser = styled(Select)`
  width: 100%;
  border-radius: 2px;
  position: absolute;
  top: 0;
  left: 0;

  .ant-select-selector {
    cursor: pointer !important;
    border: none !important;
    border-radius: 5px !important;
    background: transparent !important;
    box-shadow: none !important;
  }

  .ant-select-selection-item {
    display: none;
  }

  .ant-select-selection-overflow {
    padding: 0 5px !important;
  }
`;

export const SharePermisson = styled.div`
  span {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #2c2c2c;
  }

  .selectExcept {
    margin-top: 6px;
    .ant-checkbox-group {
      display: flex;
    }
    .ant-checkbox-wrapper {
      display: flex;
      margin-bottom: 4px;
      margin-right: 16px;

      span {
        font-family: var(--roboto-400);
        font-size: 16px;
        color: #2c2c2c;
      }
    }
    .ant-checkbox-wrapper + .ant-checkbox-wrapper {
      margin-left: 0px;
    }
    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
    .ant-checkbox:hover .ant-checkbox-inner,
    .ant-checkbox:focus .ant-checkbox-inner,
    .ant-checkbox-wrapper:hover .ant-checkbox-inner,
    .ant-checkbox-input:focus + .ant-checkbox-inner,
    .ant-checkbox-checked::after {
      border-color: ${(props) => props.theme.main} !important;
    }
  }

  margin-bottom: 10px;
`;

export const ContentPopover = styled.div`
  background-color: white;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  p {
    cursor: pointer;
    margin: 0;
    font-family: var(--roboto-400);
    font-size: 16px;
    color: #2c2c2c;
    display: flex;
    align-items: center;
    transition: all 0.5s;
    padding: 5px 20px;
    border-radius: 2px;

    img {
      width: 14px;
      margin-right: 6px;
    }
    :hover {
      background: #f5f5f5;
    }
  }
`;

export const SelectStatus = styled(Select)`
  width: 160px;
  margin-bottom: 16px;
  .ant-select-selection-item {
    display: flex;
    font-size: 14px;
    color: #2c2c2c;
  }

  .ant-select-selector {
    height: 37px !important;
    align-items: center;
  }
`;

const ListShareTo = styled.div`
  margin-right: 16px;
  max-height: 480px;
  overflow: auto;
`;

const SharePickerWrap = styled.div`
  display: flex;

  margin-bottom: 16px;

  img {
    margin-right: 8px;
    margin-bottom: 2px;
  }
`;

const SharePickerContent = styled.div`
  display: flex;

  span {
    &:nth-child(2) {
      margin: 0 16px;
    }
  }
`;

const SharePicker = styled.span`
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  color: #000000;
  &::first-letter {
    text-transform: capitalize;
  }
`;

const ListWrap = styled.div`
  max-height: 480px;
  overflow: auto;
`;
