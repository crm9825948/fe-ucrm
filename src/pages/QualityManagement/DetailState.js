import { memo } from "react";
import { Collapse } from "antd";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import IconHold from "assets/icons/qualityManagement/State_Hold.png";
import IconTalkover from "assets/icons/qualityManagement/State_Talkover.png";
import IconSlience from "assets/icons/qualityManagement/State_Silence.png";
import IconPause from "assets/icons/qualityManagement/State_Pause.png";
import Icon from "assets/icons/qualityManagement/icon_percent.png";
import IconEvent from "assets/icons/qualityManagement/color-swatch.png";

const { Panel } = Collapse;

const IconDetailState = {
  talkover: IconTalkover,
  silence: IconSlience,
  pause: IconPause,
  hold: IconHold,
};

const DetailState = ({ data, listMainFields }) => {
  const { t } = useTranslation();

  const convertMsToMinutes = (ms) => {
    if (ms) {
      let seconds = Math.floor(ms / 1000);
      let minutes = Math.floor(seconds / 60);
      let remainingSeconds = seconds % 60;

      let minutesString = minutes < 10 ? "0" + minutes : minutes;
      let secondsString =
        remainingSeconds < 10 ? "0" + remainingSeconds : remainingSeconds;

      return minutesString + ":" + secondsString;
    } else {
      return "-- --";
    }
  };

  return (
    <WrapperState>
      <CustomCollapse defaultActiveKey={"1"}>
        <Panel header={t("QualityManagement.stateDetail")} key="1">
          <WrapperItems>
            {_.map(data, (item, index) => (
              <WrapperItemLog key={index}>
                <WrapperIcon>
                  <IconState src={IconDetailState[item[0]] || ""} alt="" />
                  <Text>{item[0]}</Text>
                </WrapperIcon>
                <WrapperDuration>
                  <Duration>Duration</Duration>
                  <WrapperTime>
                    <WrapperMax>
                      <Title>Max</Title>
                      <Value>
                        {convertMsToMinutes(_.get(item[1], "maxDuration", "0"))}
                      </Value>
                    </WrapperMax>
                    <WrapperMin>
                      <Title>Min</Title>
                      <Value>
                        {convertMsToMinutes(_.get(item[1], "minDuration", "0"))}
                      </Value>
                    </WrapperMin>
                    <WrapperAve>
                      <Title>Average</Title>
                      <Value>
                        {convertMsToMinutes(_.get(item[1], "avgDuration", "0"))}
                      </Value>
                    </WrapperAve>
                  </WrapperTime>
                  <Total>
                    <Title>Total</Title>
                    <Value>
                      {convertMsToMinutes(_.get(item[1], "totalDuration", "0"))}
                    </Value>
                  </Total>
                </WrapperDuration>
                <WrapperPercent>
                  <WrapperInfoPercent>
                    <IconPercent src={Icon} alt="" />
                    <Percent>
                      <Title>Percent of call</Title>
                      <Value>
                        {_.get(item[1], "percentOfCall", "--") || "--"}%
                      </Value>
                    </Percent>
                  </WrapperInfoPercent>
                  <WrapperTotalEvent>
                    <IconPercent src={IconEvent} alt="" />
                    <Percent>
                      <Title>Total event</Title>
                      <Value>
                        {_.get(item[1], "totalEvents", "--") || "--"}
                      </Value>
                    </Percent>
                  </WrapperTotalEvent>
                </WrapperPercent>
              </WrapperItemLog>
            ))}
          </WrapperItems>
        </Panel>
      </CustomCollapse>
    </WrapperState>
  );
};

export default withTranslation()(memo(DetailState));

const WrapperState = styled.div`
  padding: 0.5rem 0;
  border-bottom: 1px solid #ececec;
`;

const WrapperItems = styled.div`
  display: flex;
  flex-direction: column;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-between;
  }
`;

const WrapperItemLog = styled.div`
  padding: 0.5rem 1rem;
  background: #ffffff;

  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 0.75rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  gap: 0.5rem;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 49%;
    gap: 1rem;
    padding: 1rem;
  }
`;

const WrapperIcon = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;

  width: 105px;
`;

const Text = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;

  color: #2c2c2c;
`;

const IconState = styled.img``;

const WrapperDuration = styled.div`
  display: flex;
  align-items: center;
  gap: 2px;
  flex-direction: row;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    flex-direction: column;
    flex-wrap: wrap;
    width: 100%;
  }
`;

const Duration = styled.div`
  width: 74px;
  min-height: 60px;
  background: #e2eefe;
  border-radius: 8px 0px 0px 8px;
  display: flex;
  align-items: center;
  justify-content: center;

  font-weight: 500;
  font-size: 14px;
  line-height: 16px;

  color: #2c2c2c;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    display: none;
  }
`;

const WrapperTime = styled.div`
  min-height: 60px;

  background: #f4fefb;
  border-radius: 0px;
  display: flex;
  flex-direction: row;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 100%;
    border-radius: 8px;
  }
`;

const WrapperMax = styled.div`
  padding: 12px 1rem;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 33.33%;
  }
`;

const WrapperMin = styled.div`
  padding: 12px 1rem;
  position: relative;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 33.33%;
  }

  &::after {
    content: "";
    width: 1px;
    background-color: #dadada;
    height: 50%;
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }

  &::before {
    content: "";
    width: 1px;
    background-color: #dadada;
    height: 50%;
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);
  }
`;

const WrapperAve = styled.div`
  padding: 12px 1rem;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 33.33%;
  }
`;

const Title = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;

  color: #6b6b6b;
  margin-bottom: 0.25rem;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    margin: 0;
  }
`;
const Value = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 16px;

  color: #2c2c2c;
`;

const Total = styled.div`
  width: 74px;
  min-height: 60px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  background: #e2eefe;
  border-radius: 0px 8px 8px 0px;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 100%;
    border-radius: 8px;
    flex-direction: row;
    gap: 0.25rem;
    height: 40px;
    min-height: unset;
  }
`;

const WrapperPercent = styled.div`
  display: flex;
  align-items: center;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 100%;
  }
`;

const WrapperInfoPercent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  gap: 8px;
  position: relative;
  padding-right: 0.5rem;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    flex-direction: column;
    width: 50%;
  }

  &::before {
    content: "";
    width: 1px;
    background-color: #dadada;
    height: 100%;
    position: absolute;
    top: 50%;
    right: 0;
    transform: translateY(-50%);

    @media screen and (max-width: 1710px) and (max-height: 960px) {
      height: 50%;
    }
  }
`;

const WrapperTotalEvent = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 8px;
  padding-left: 0.75rem;
  flex-direction: row;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    flex-direction: column;
    width: 50%;
  }
`;

const Percent = styled.div`
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    display: flex;
    flex-direction: column-reverse;
    align-items: center;
  }
`;

const IconPercent = styled.img``;

const CustomCollapse = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border: none;
    .ant-collapse-header {
      display: flex;
      flex-direction: row-reverse;
      background-color: #ffffff;
      padding: 0.5rem 0;

      .ant-collapse-header-text {
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;

        color: #2c2c2c;
      }
    }

    .ant-collapse-content {
      border: none;

      .ant-collapse-content-box {
        padding: 0;
      }
    }
  }
`;
