import _ from "lodash";
import styled from "styled-components";
import { Spin } from "antd";
import { memo, useEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import LogScoring from "./LogScoring";
import DetailState from "./DetailState";
import InfoDetail from "./InfoDetail";
import Record from "./Record/Record";
import Comments from "./Comment/Comments";
import ConversationLayout from "./ConversationLayout";
import ChatIC from "./ChatIC";

const ContentQM = ({
  dataInteraction,
  listLogScoring,
  dataDetailState,
  Emails,
  typeSelected,
  urlRecord,
  chat,
  listMainFields,
  listMainFieldsQM,
  listChatIC,
  idInteraction,
  setCurrentPageChat,
}) => {
  const { t } = useTranslation();
  const elementRef = useRef(null);
  const [idQM, setIdQM] = useState("");

  const { loadingLogScoring, loadingInteractionDetail } = useSelector(
    (state) => state.qualityManagementReducer
  );

  const scrollToTopFunc = () => {
    if (elementRef.current) {
      elementRef.current.scrollTop = 0;
    }
  };

  useEffect(() => {
    if (!loadingInteractionDetail) {
      scrollToTopFunc();
    }
  }, [loadingInteractionDetail]);

  useEffect(() => {
    if (listLogScoring) setIdQM(_.get(listLogScoring, "[0]._id", ""));
  }, [listLogScoring]);

  return (
    <WrapperContent ref={elementRef}>
      <CustomSpin spinning={loadingLogScoring || loadingInteractionDetail}>
        <WrapperBG>
          {dataInteraction && (
            <InfoDetail
              title={t("QualityManagement.interactionDetail")}
              data={dataInteraction}
              listMainFields={listMainFields}
              isIC={
                typeSelected &&
                _.defaults(typeSelected, "").toLowerCase() === "ic"
              }
            />
          )}
          {listChatIC && _.size(listChatIC) > 0 && (
            <ChatIC
              data={listChatIC}
              idInteraction={idInteraction}
              setCurrentPageChat={setCurrentPageChat}
            />
          )}
          {typeSelected &&
            _.defaults(typeSelected, "").toLowerCase() === "sms" && (
              <ConversationLayout
                data={chat}
                type={0}
                dataInteraction={dataInteraction}
              />
            )}
          {typeSelected &&
            _.defaults(typeSelected, "").toLowerCase() === "email" && (
              <ConversationLayout
                data={Emails}
                type={1}
                dataInteraction={dataInteraction}
              />
            )}
          {_.get(urlRecord, "link_record")?.length > 0 &&
            typeSelected &&
            _.defaults(typeSelected, "").toLowerCase() === "call" && (
              <Record urlRecord={urlRecord} />
            )}
          {dataDetailState?.length > 0 &&
            typeSelected &&
            _.defaults(typeSelected, "").toLowerCase() === "call" && (
              <DetailState
                data={dataDetailState}
                listMainFields={listMainFields}
              />
            )}
          {listLogScoring && _.defaults(listLogScoring, []).length > 0 && (
            <LogScoring
              listLogScoring={listLogScoring}
              setIdQM={setIdQM}
              idQM={idQM}
              listMainFieldsQM={listMainFieldsQM}
            />
          )}
          {idQM && (
            <Comments
              objectId={"obj_crm_qm_interaction_00001"}
              recordID={idQM}
            />
          )}
        </WrapperBG>
      </CustomSpin>
    </WrapperContent>
  );
};

export default withTranslation()(memo(ContentQM));

const WrapperContent = styled.div`
  flex: 6.5;
  padding: 0;
  max-height: 100%;
  overflow-y: auto;
  margin-top: 0.75rem;
  background: #ffffff;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const WrapperBG = styled.div`
  padding: 0.5rem 1rem 1.5rem 1rem;
  height: 100%;
  position: relative;
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;
