import _ from "lodash";
import moment from "moment";
import parse from "html-react-parser";
import styled from "styled-components";
import { Collapse, Spin, Tooltip } from "antd";
import { memo, useEffect, useRef } from "react";
import { withTranslation } from "react-i18next";

import { optionsParse } from "util/staticData";
import AvatarImg from "assets/images/header/avatar.png";
import InfiniteScroll from "react-infinite-scroll-component";
import { useSelector } from "react-redux";
import ReactPlayer from "react-player";
import AttachFileIcon from "assets/icons/common/attach-file.svg";

const { Panel } = Collapse;

const ChatIC = ({ data, idInteraction, setCurrentPageChat }) => {
  const elementRef = useRef(null);
  const { loadedAllChatIC } = useSelector(
    (state) => state.qualityManagementReducer
  );

  const scrollToBottomFunc = () => {
    if (elementRef.current) {
      elementRef.current.scrollTop = elementRef.current.scrollHeight;
    }
  };

  const handlLoadMore = () => {
    setCurrentPageChat((pre) => pre + 1);
  };

  useEffect(() => {
    if (idInteraction) scrollToBottomFunc();
  }, [idInteraction]);

  const isValidEmail = (email) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  return (
    <Wrapper>
      <CustomCollapse defaultActiveKey={"1"}>
        <Panel header={"Chats"} key="1">
          <WrapperBG
            ref={elementRef}
            id="chats"
            style={{
              display: "flex",
              flexDirection: "column-reverse",
            }}
          >
            <CustomInfiniteScroll
              style={{ display: "flex", flexDirection: "column-reverse" }}
              dataLength={(data && data?.length) || 0}
              scrollableTarget="chats"
              loader={
                <div
                  style={{
                    display: "flex",
                    justifyContent: "center",
                    marginBottom: "1rem",
                    marginTop: "2px",
                  }}
                >
                  <CustomSpin />
                </div>
              }
              hasMore={!loadedAllChatIC}
              next={() => {
                handlLoadMore();
              }}
              inverse={true}
            >
              {data &&
                data.map((item, index) => {
                  return _.get(item, "sender_type") === "activity_log" ? (
                    <div style={{ marginBottom: "12px" }} key={index}>
                      <WrapperRowLog>
                        <Tooltip
                          placement="bottom"
                          title={moment(_.get(item, "sent_time", "")).format(
                            "YYYY-MM-DD HH:mm:ss"
                          )}
                        >
                          <WrapperLog>
                            {_.get(item, "content_text", "")}
                          </WrapperLog>
                        </Tooltip>
                      </WrapperRowLog>
                    </div>
                  ) : (
                    <WrapperRow
                      isMe={
                        _.get(item, "sender_type") === "agent" ||
                        _.get(item, "sender_type") === "bot"
                      }
                      key={index}
                    >
                      <Tooltip
                        placement="bottom"
                        title={
                          _.get(item, "sender_type") === "agent" ||
                          _.get(item, "sender_type") === "bot"
                            ? _.get(item, "create_by", "")
                            : ""
                        }
                      >
                        <Avatar src={AvatarImg} alt="" />
                      </Tooltip>
                      <WrapperMess
                        className={
                          _.get(item, "sender_type") === "agent" ||
                          _.get(item, "sender_type") === "bot"
                            ? "isMe"
                            : ""
                        }
                        isMe={
                          _.get(item, "sender_type") === "agent" ||
                          _.get(item, "sender_type") === "bot"
                        }
                      >
                        {/* <WrapperReply
                          isMe={_.get(item, "sender_type") === "agent" || _.get(item, "sender_type") === "bot"}
                        >
                          <TextReply
                            isMe={_.get(item, "sender_type") === "agent" || _.get(item, "sender_type") === "bot"}
                          >
                            Hello zin chaofoskn
                          </TextReply>
                          <Name>- Nguyễn Minh Triết -</Name>
                        </WrapperReply> */}
                        <WrapperContent
                          isMe={
                            _.get(item, "sender_type") === "agent" ||
                            _.get(item, "sender_type") === "bot"
                          }
                        >
                          {_.get(item, "sender_type") === "agent" ? (
                            <CustomName>
                              {isValidEmail(_.get(item, "create_by", ""))
                                ? _.get(item, "create_by", "")
                                : "Fanpage"}
                            </CustomName>
                          ) : _.get(item, "sender_type") === "bot" ? (
                            <CustomName>Auto message</CustomName>
                          ) : (
                            ""
                          )}

                          {_.get(item, "content_text") &&
                            !_.get(item, "content") &&
                            parse(
                              _.get(item, "content_text", ""),
                              optionsParse
                            )}

                          {_.get(item, "content") && (
                            <WrapEmail>
                              {_.get(item, "subject") && (
                                <SubjectEmail>
                                  {_.get(item, "subject")}
                                </SubjectEmail>
                              )}

                              {_.get(item, "content") && (
                                <div>
                                  {parse(
                                    _.get(item, "content", ""),
                                    optionsParse
                                  )}
                                </div>
                              )}

                              {_.get(item, "attachments") &&
                                _.get(item, "attachments", []).map((file) => (
                                  <WrapFileEmail>
                                    <img
                                      src={AttachFileIcon}
                                      alt="AttachFile"
                                    />
                                    <span
                                      onClick={() =>
                                        window.open(
                                          _.get(file, "file_upload_url", "")
                                        )
                                      }
                                    >
                                      {_.get(file, "file_name", "")}
                                    </span>
                                  </WrapFileEmail>
                                ))}

                              {_.get(item, "from") && (
                                <From>
                                  <span>From:&nbsp;</span>
                                  <p>{_.get(item, "from")}</p>
                                </From>
                              )}
                              {_.get(item, "to") && (
                                <To>
                                  <span>To:&nbsp;</span>
                                  <p>{_.get(item, "to")}</p>
                                </To>
                              )}
                            </WrapEmail>
                          )}

                          {_.get(item, "content_file_url") &&
                            _.get(item, "interaction_type") === "image" && (
                              <CustomImgChat
                                src={_.get(item, "content_file_url", "")}
                                alt="imageic"
                                onClick={() =>
                                  window.open(
                                    _.get(item, "content_file_url", "")
                                  )
                                }
                              />
                            )}

                          {_.get(item, "content_file_url") &&
                            _.get(item, "interaction_type") === "file" && (
                              <CustomFile
                                onClick={() =>
                                  window.open(
                                    _.get(item, "content_file_url", "")
                                  )
                                }
                              >
                                {_.get(item, "content_file_name", "File name")}
                              </CustomFile>
                            )}

                          {_.get(item, "content_file_url") &&
                            _.get(item, "interaction_type") === "link" && (
                              <CustomFile
                                onClick={() =>
                                  window.open(
                                    _.get(item, "content_file_url", "")
                                  )
                                }
                              >
                                {_.get(item, "content_file_name", "Link")}
                              </CustomFile>
                            )}

                          {_.get(item, "content_file_url") &&
                            _.get(item, "interaction_type") === "audio" && (
                              <audio
                                controls
                                src={_.get(item, "content_file_url", "")}
                              />
                            )}

                          {_.get(item, "content_file_url") &&
                            _.get(item, "interaction_type") === "video" && (
                              <ReactPlayer
                                url={_.get(item, "content_file_url", "")}
                                controls
                                style={{ maxWidth: "250px" }}
                                height="auto"
                              />
                            )}

                          {_.get(item, "location_lat") &&
                            _.get(item, "location_long") &&
                            _.get(item, "interaction_type") === "location" && (
                              <iframe
                                title="location"
                                src={`https://maps.google.com/maps?hl=en&q=${_.get(
                                  item,
                                  "location_lat",
                                  ""
                                )},${_.get(
                                  item,
                                  "location_long",
                                  ""
                                )}&t=&z=14&ie=UTF8&iwloc=B&output=embed`}
                              />
                            )}
                        </WrapperContent>
                        {/* <ByName>
                          <span>By:</span> {_.get(item, "sender_type")}
                        </ByName> */}
                      </WrapperMess>
                      <Time
                        isMe={
                          _.get(item, "sender_type") === "agent" ||
                          _.get(item, "sender_type") === "bot"
                        }
                      >
                        {moment(_.get(item, "sent_time", "")).format(
                          "YYYY-MM-DD HH:mm:ss"
                        )}
                      </Time>
                    </WrapperRow>
                  );
                })}
            </CustomInfiniteScroll>
          </WrapperBG>
        </Panel>
      </CustomCollapse>
    </Wrapper>
  );
};

export default withTranslation()(memo(ChatIC));

const CustomName = styled.p`
  color: ${(props) => props.theme.main};
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 4px;
`;

const CustomFile = styled.div`
  cursor: pointer;
  margin: 4px 0;
  text-decoration: underline;

  &:hover {
    color: ${(props) => props.theme.main};
  }
`;

const CustomImgChat = styled.img`
  width: 100%;
  height: auto;
  object-fit: contain;
  cursor: pointer;
  margin: 4px 0;
`;

const CustomInfiniteScroll = styled(InfiniteScroll)`
  &::-webkit-scrollbar {
    display: none;
  }
`;

const Wrapper = styled.div`
  padding: 0.5rem 0;
  border-bottom: 1px solid #ececec;

  width: 100%;
`;

const WrapperBG = styled.div`
  padding: 1rem;
  width: 100%;
  background-color: #f4fafa;
  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 1rem;
  max-height: 500px;
  overflow-y: auto;
`;

const WrapperRow = styled.div`
  display: flex;
  align-items: flex-end;
  gap: 15px;
  padding-bottom: 40px;
  position: relative;
  overflow-x: auto;

  ${(props) =>
    props.isMe ? "flex-direction: row-reverse;" : " flex-direction: row;"}
`;

const WrapperRowLog = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: relative;

  &::before {
    content: "";
    height: 0.5px;
    width: 85%;
    background-color: #e8e8e8;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;

const WrapperLog = styled.div`
  background-color: #e8e8e8;
  padding: 4px 12px;
  border-radius: 10px;
  color: #5c5757;
  font-size: 13px;
  z-index: 10;
`;

const Time = styled.p`
  margin: 0;
  content: "text";
  position: absolute;
  bottom: 16px;
  ${(props) => (props.isMe ? "right: 55px;" : "left: 55px;")}

  font-weight: 400;
  font-size: 12px;
  line-height: 20px;
  color: #8c8c8c;
`;

const Avatar = styled.img`
  width: 40px;
  height: 40px;
  object-fit: cover;
  border-radius: 50%;
`;

const WrapperMess = styled.div`
  padding: 0.5rem 1rem;
  border-radius: 20px 20px 20px 0px;
  max-width: 85%;

  ${(props) =>
    props.isMe ? "background-color: #D3E1F4;" : "background-color: #ffffff;"}
  ${(props) => (props.isMe ? "border: none;" : "border: 1px solid #ececec;")}

  &.isMe {
    border-radius: 20px 20px 0px 20px;
  }
`;

const WrapperContent = styled.div`
  font-weight: 400;
  line-height: 20px;
  color: #464545;
  font-size: 16px;
  margin-bottom: 5px;

  overflow-x: auto;
`;

// const WrapperReply = styled.div`
//   border-radius: 20px;
//   padding: 1rem;
//   margin-bottom: 0.5rem;
//   margin-top: 0.2rem;

//   ${(props) =>
//     props.isMe
//       ? "background-color: #adc7ed;"
//       : props.isMe
//       ? "background-color: #D3E1F4;"
//       : "background-color: #F0F2F5;"}
// `;

// const TextReply = styled.p`
//   margin: 0;
//   padding-left: 0.75rem;
//   font-weight: 400;
//   line-height: 20px;
//   color: #464545;
//   position: relative;

//   &::after {
//     content: "";
//     width: 4px;
//     height: 100%;
//     position: absolute;
//     top: 0;
//     left: 0;
//     border-radius: 4px;

//     ${(props) =>
//       props.isMe ? "background-color: #f0f4f4;" : "background-color: #CDCDCD;"}
//   }
// `;

// const Name = styled.p`
//   margin: 0;
//   margin-top: 10px;
//   font-weight: 400;
//   font-size: 14px;
//   line-height: 20px;

//   color: #252424;
// `;

// const ByName = styled.p`
//   margin: 0;
//   font-weight: 400;
//   font-size: 14px;
//   line-height: 20px;

//   color: #252424;

//   span {
//     color: #5c5757;
//   }
// `;

const CustomCollapse = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border: none;
    .ant-collapse-header {
      display: flex;
      flex-direction: row-reverse;
      background-color: #ffffff;
      padding: 0.5rem 0;

      .ant-collapse-header-text {
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;

        color: #2c2c2c;
      }
    }

    .ant-collapse-content {
      border: none;

      .ant-collapse-content-box {
        padding: 0;
      }
    }
  }
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const SubjectEmail = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: #252424;
  font-family: var(--roboto-500);
  padding-bottom: 14px;
  margin-bottom: 14px;
  border-bottom: 1px solid #e4e8eb;
`;

const WrapFile = styled.div`
  img {
    margin-right: 10px;
  }
`;

const WrapFileEmail = styled(WrapFile)`
  cursor: pointer;
  display: flex;
  align-items: center;
  margin: 8px 0;

  span {
    color: ${(props) => props.theme.main};
  }
`;

const From = styled.div`
  display: flex;
  align-items: center;
  margin-top: 8px;

  span {
    line-height: 20px;
    color: #8c8c8c;
  }

  p {
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    color: #252424;
    margin-bottom: 0;
  }
`;

const WrapEmail = styled.div`
  width: fit-content;
  max-width: 590px;
  background-color: "#fff";

  padding: 24px 16px;

  p {
    margin-bottom: 0;
  }
`;

const To = styled(From)``;
