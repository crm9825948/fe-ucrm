import "./index.scss";
import { Skeleton, Spin, Tooltip, Typography } from "antd";
import styled from "styled-components";
import React, { useEffect, useState, memo, useCallback, useRef } from "react";
import AllIcon from "assets/icons/qualityManagement/icon-all.png";
import EmailInIcon from "assets/icons/qualityManagement/Emailin-icon.png";
import EmailOutIcon from "assets/icons/qualityManagement/Emailout-icon.png";
import MessInIcon from "assets/icons/qualityManagement/Messin-icon.png";
import MessOutIcon from "assets/icons/qualityManagement/Messout-icon.png";
import CallInIcon from "assets/icons/qualityManagement/Callin-icon.png";
import CallOutIcon from "assets/icons/qualityManagement/Callout-icon.png";
import VBBIIcon from "assets/icons/qualityManagement/VBBIIn.png";
import CountTotal from "assets/icons/qualityManagement/ReloadCountRecords.png";
import VBBIIconOut from "assets/icons/qualityManagement/VBBIOut.png";
import EditQuickFilter from "assets/icons/qualityManagement/EditQuickFilter.png";
import ICIcon from "assets/icons/qualityManagement/MailIC-icon.png";
import EmptyIcon from "assets/icons/qualityManagement/IconUndefined.png";

import Viber from "assets/icons/qualityManagement/viber.svg";
import Webchat from "assets/icons/qualityManagement/chat.svg";
import Zalo from "assets/icons/qualityManagement/zalo.svg";
import EmailIC from "assets/icons/qualityManagement/mail.svg";
import Facebook from "assets/icons/qualityManagement/Facebook.svg";
import FacebookMessage from "assets/icons/qualityManagement/Mess.svg";
import FacebookComment from "assets/icons/timeline/facebook_comment.svg";
import Instagram from "assets/icons/qualityManagement/Intagram.svg";
import InstagramMessage from "assets/icons/qualityManagement/Intagram_mess.svg";
import InstagramComment from "assets/icons/timeline/instagram_comment.svg";
import Telegram from "assets/icons/qualityManagement/Telegram.svg";
import ThirdPartyChannel from "assets/icons/qualityManagement/3rd.svg";
import Whatsapp from "assets/icons/qualityManagement/Whatsapp.svg";

import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import Popover from "antd/lib/popover";
import {
  CloseOutlined,
  CheckOutlined,
  RightOutlined,
  LeftOutlined,
} from "@ant-design/icons";

import {
  UpdateListOptionsQuickFilter,
  getListDataFilter,
  getTotalRecord,
  resetTotalRecord,
  setPayloadFilter,
} from "redux/slices/QualityManagement";
import { COLOR_INTERACTION_TYPE } from "util/staticData";
import { useParams } from "react-router";
import SelectMultipleUser from "../../components/SelectMultipleUser/SelectMultipleUser";
import _ from "lodash";
import { Notification } from "components/Notification/Noti";

const ICONS = {
  "Call In": CallInIcon,
  "Call Out": CallOutIcon,
  "Email In": EmailInIcon,
  "Email Out": EmailOutIcon,
  "SMS In": MessInIcon,
  "SMS Out": MessOutIcon,
  "VBEE In": VBBIIcon,
  "VBEE Out": VBBIIconOut,
  "IC Viber": Viber,
  "IC Webchat": Webchat,
  "IC Zalo": Zalo,
  "IC Email": EmailIC,
  "IC Facebook": Facebook,
  "IC Facebook Message": FacebookMessage,
  "IC Facebook Comment": FacebookComment,
  "IC Instagram": Instagram,
  "IC Instagram Message": InstagramMessage,
  "IC Instagram Comment": InstagramComment,
  "IC Telegram": Telegram,
  "IC ThirdParty Channel": ThirdPartyChannel,
  "IC Whatsapp": Whatsapp,
  IC: ICIcon,
  Manual: EmptyIcon,
};

const COLORS = {
  "Call In": COLOR_INTERACTION_TYPE.CALLIN,
  "Call Out": COLOR_INTERACTION_TYPE.CALLOUT,
  "Email In": COLOR_INTERACTION_TYPE.EMAILIN,
  "Email Out": COLOR_INTERACTION_TYPE.EMAILOUT,
  "SMS In": COLOR_INTERACTION_TYPE.MESSIN,
  "SMS Out": COLOR_INTERACTION_TYPE.MESSOUT,
  "VBEE In": COLOR_INTERACTION_TYPE.VBEEIN,
  "VBEE Out": COLOR_INTERACTION_TYPE.VBEEOUT,
  "IC Viber": COLOR_INTERACTION_TYPE.IC_VIDBER,
  "IC Webchat": COLOR_INTERACTION_TYPE.IC_WEBCHAT,
  "IC Zalo": COLOR_INTERACTION_TYPE.IC_ZALO,
  "IC Email": COLOR_INTERACTION_TYPE.IC_EMAIL,
  "IC Facebook": COLOR_INTERACTION_TYPE.IC_FACEBOOK,
  "IC Facebook Message": COLOR_INTERACTION_TYPE.IC_FACEBOOKMESSAGE,
  "IC Facebook Comment": COLOR_INTERACTION_TYPE.IC_FACEBOOKCOMMENT,
  "IC Instagram": COLOR_INTERACTION_TYPE.IC_INSTAGRAM,
  "IC Instagram Message": COLOR_INTERACTION_TYPE.IC_INSTAGRAMMESSAGE,
  "IC Instagram Comment": COLOR_INTERACTION_TYPE.IC_INSTAGRAMCOMMENT,
  "IC Telegram": COLOR_INTERACTION_TYPE.IC_TELEGRAM,
  "IC ThirdParty Channel": COLOR_INTERACTION_TYPE.IC_THIRDPARTYCHANNEL,
  "IC Whatsapp": COLOR_INTERACTION_TYPE.IC_WHATSAPP,
  IC: COLOR_INTERACTION_TYPE.IC,
  Manual: "#EAEBED",
};

const { Text } = Typography;

const QuickFilter = ({
  listOption,
  timeRangeFilter,
  LoadingGetData,
  collapse,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const taskbarRef = useRef(null);
  const totalRef = useRef(null);
  const { customViewId } = useParams();
  const [open, setOpen] = useState(false);
  const [listOwner, setListOwner] = useState([]);
  const [optionActive, setOptionActive] = useState(["ALL"]);
  const [optionFilter, setOptionFilter] = useState([]);
  const [optionFilterAnd, setOptionFilterAnd] = useState([]);
  const [listOptionCurr, setListOptionCurr] = useState([]);
  const [listOptionMore, setListOptionMore] = useState([]);
  const [showScrollButtonLeft, setShowScrollButtonLeft] = useState(false);
  const [showScrollButtonRight, setShowScrollButtonRight] = useState(false);
  const [widthBtnTotal, setWidthBtnTotal] = useState(0);

  const {
    listUser,
    totalRecord,
    loadingTotalRecord,
    listAllOptionsQuickFilter,
  } = useSelector((state) => state.qualityManagementReducer);
  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const handleSelectOption = (value, dataFilter) => {
    if (value === "ALL") {
      setOptionActive([value]);
      setOptionFilter([]);
      setOptionFilterAnd([]);
      return;
    }

    if (optionActive.includes(value)) {
      setOptionActive((pre) => pre.filter((item) => item !== value));
      if (value === "Manual") {
        setOptionFilterAnd([]);
      } else {
        setOptionFilter((pre) =>
          pre.filter((item) => {
            if (item.id_field !== dataFilter.id_field) {
              return true;
            } else {
              if (item.value.$eq !== dataFilter.value.$eq) {
                return true;
              } else {
                return false;
              }
            }
          })
        );
      }
    } else {
      setOptionActive((pre) => [
        ...pre.filter((item) => item !== "ALL"),
        value,
      ]);
      if (value === "Manual") {
        setOptionFilterAnd(dataFilter);
      } else {
        setOptionFilter((pre) => [
          ...pre.filter((item) => item !== "ALL"),
          dataFilter,
        ]);
      }
    }
  };

  const generatePayload = useCallback(() => {
    if (
      timeRangeFilter?.filter_type === "custom" &&
      timeRangeFilter?.start_time === null &&
      timeRangeFilter?.end_time === null
    ) {
      return "";
    } else {
      const payload =
        customViewId === "default-view"
          ? {
              object_id: interactionConfigState?.object_id || null,
              quick_filter: {
                time_range_filter: timeRangeFilter,
              },
            }
          : {
              id: customViewId,
              object_id: interactionConfigState?.object_id || null,
              quick_filter: {
                time_range_filter: timeRangeFilter,
              },
            };

      if (listOwner && _.size(listOwner) > 0) {
        payload.quick_filter.owner = listOwner;
      }

      if (optionFilter && _.size(optionFilter) > 0) {
        payload.quick_filter.or_filter = optionFilter;
      }

      if (optionFilterAnd && _.size(optionFilterAnd) > 0)
        payload.quick_filter.and_filter = optionFilterAnd;

      return payload;
    }
  }, [
    interactionConfigState?.object_id,
    listOwner,
    optionFilter,
    timeRangeFilter,
    customViewId,
    optionFilterAnd,
  ]);

  useEffect(() => {
    if (optionActive?.length === 0) setOptionActive(["ALL"]);
  }, [optionActive]);

  useEffect(() => {
    if (interactionConfigState?.object_id) {
      const payload = generatePayload();
      if (payload !== "") {
        dispatch(setPayloadFilter(payload));
        dispatch(getListDataFilter(payload));
      } else {
        Notification("warning", t("QualityManagement.warningCustom"));
      }
    }
    //eslint-disable-next-line
  }, [generatePayload, interactionConfigState?.object_id, dispatch]);

  useEffect(() => {
    dispatch(resetTotalRecord());
  }, [optionFilter, dispatch, timeRangeFilter, listOwner, optionFilterAnd]);

  const handleLoadTotal = () => {
    const payload = generatePayload();
    if (payload !== "") {
      dispatch(getTotalRecord(payload));
    } else {
      Notification("warning", t("QualityManagement.warningCustom"));
    }
  };

  const Loading = useCallback(() => {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
          gap: "1rem",
          flexDirection: "row",
        }}
      >
        {[...Array(10).keys()].map((_, index) => (
          <Skeleton.Button
            key={index}
            style={{ width: "80px" }}
            active={true}
            size={"default"}
            shape={"default"}
            block={"square"}
          />
        ))}
      </div>
    );
  }, []);

  useEffect(() => {
    if (
      listOptionCurr &&
      listAllOptionsQuickFilter &&
      _.size(listAllOptionsQuickFilter) >= 0 &&
      _.size(listOptionCurr) >= 0
    ) {
      setListOptionMore(
        listAllOptionsQuickFilter.filter(
          (item) => !listOptionCurr.includes(item)
        )
      );
    }
  }, [listAllOptionsQuickFilter, listOptionCurr]);

  useEffect(() => {
    if (listOption && _.size(listOption) > 0) {
      setListOptionCurr(
        listOption.map((item) => _.get(item, "label_name", ""))
      );
    }
  }, [listOption]);

  const handleClickAdd = (value) => {
    setListOptionMore((pre) => pre.filter((item) => item !== value));
    setListOptionCurr([...listOptionCurr, value]);
  };

  const handleClickRemove = (value) => {
    setListOptionCurr((pre) => pre.filter((item) => item !== value));
    setListOptionMore([...listOptionMore, value]);
  };

  const handleCancel = () => {
    if (
      listOptionCurr &&
      listAllOptionsQuickFilter &&
      _.size(listAllOptionsQuickFilter) >= 0 &&
      _.size(listOptionCurr) >= 0
    ) {
      setListOptionMore(
        listAllOptionsQuickFilter.filter(
          (item) => !listOptionCurr.includes(item)
        )
      );
    }

    if (listOption && _.size(listOption) >= 0) {
      setListOptionCurr(
        listOption.map((item) => _.get(item, "label_name", ""))
      );
    }
    setOpen(false);
  };

  const handleUpdate = () => {
    dispatch(
      UpdateListOptionsQuickFilter({
        qm_quick_filter_list: listOptionCurr,
      })
    );

    setOpen(false);
    if (_.size(optionFilter) > 0) {
      setOptionActive(["ALL"]);
      setOptionFilter([]);
    }
  };

  const handleScrollRight = () => {
    if (taskbarRef.current) {
      taskbarRef.current.scrollLeft += 200;
    }
  };

  const handleScrollLeft = () => {
    if (taskbarRef.current) {
      taskbarRef.current.scrollLeft -= 200;
    }
  };

  useEffect(() => {
    if (listOptionCurr) {
      const handleScroll = () => {
        const currentRef = taskbarRef.current;
        if (currentRef) {
          const { scrollWidth, clientWidth, scrollLeft } = currentRef;
          setShowScrollButtonLeft(scrollLeft > 0);
          setShowScrollButtonRight(
            scrollWidth > clientWidth &&
              scrollLeft < scrollWidth - clientWidth - 2
          );
        }
      };

      const currentRef = taskbarRef.current;
      if (currentRef) {
        currentRef.addEventListener("scroll", handleScroll);
        handleScroll();
      }

      return () => {
        if (currentRef) {
          currentRef.removeEventListener("scroll", handleScroll);
        }
      };
    }
  }, [listOptionCurr, taskbarRef]);

  useEffect(() => {
    setWidthBtnTotal(_.get(totalRef, "current.offsetWidth") || 0);
  }, [totalRef, totalRecord]);

  return (
    <Wrapper>
      {LoadingGetData ? (
        <Loading />
      ) : (
        <WrapperList ref={taskbarRef}>
          {showScrollButtonLeft && (
            <ButtonScroll
              onClick={handleScrollLeft}
              style={{
                left: `${widthBtnTotal + 30}px`,
              }}
            >
              <LeftOutlined />
            </ButtonScroll>
          )}
          <WrapperIconEdit
            className={
              open
                ? collapse
                  ? "collapse open"
                  : "open"
                : collapse
                ? "collapse"
                : ""
            }
            onClick={() => {
              if (!open) setOpen(true);
            }}
          >
            <Popover
              content={
                <WrapperLisMore>
                  {listOptionMore?.map((item, index) => (
                    <WrapperOptionMcore
                      key={index}
                      onClick={() => handleClickAdd(item)}
                    >
                      <CustomIcon
                        src={ICONS[item]}
                        alt="icon-filter"
                        style={{ marginRight: "3px" }}
                      />
                      <Text
                        ellipsis={{
                          tooltip: "",
                        }}
                        style={{ maxWidth: 200 }}
                      >
                        {item}
                      </Text>
                    </WrapperOptionMcore>
                  ))}
                </WrapperLisMore>
              }
              trigger="click"
              open={open}
              // onOpenChange={handleOpenChange}
              placement="topRight"
              overlayClassName="popoverNotiQuickfilter"
            >
              {open ? (
                <>
                  <CheckOutlined
                    onClick={handleUpdate}
                    style={{
                      color: "#2cbc63",
                      fontSize: "16px",
                    }}
                  />
                  <CloseOutlined
                    onClick={handleCancel}
                    style={{
                      color: "red",
                      fontSize: "16px",
                      marginLeft: "8px",
                    }}
                  />
                </>
              ) : (
                <Tooltip
                  placement="right"
                  title={"Custom options quick filter"}
                  arrow={true}
                  mouseEnterDelay={0.75}
                >
                  <CustomIconEdit src={EditQuickFilter} alt="icon-filter" />
                </Tooltip>
              )}
            </Popover>
          </WrapperIconEdit>
          <div
            style={{
              backgroundColor: "white",
              position: "sticky",
              left: 0,
            }}
          >
            <TotalRecord ref={totalRef}>
              <CustomSpin spinning={loadingTotalRecord}>
                <img
                  style={{ opacity: loadingTotalRecord ? 0 : 1 }}
                  src={CountTotal}
                  alt="icon"
                  onClick={handleLoadTotal}
                />
              </CustomSpin>
              {totalRecord !== null && (
                <p>
                  {`${totalRecord || 0} ${
                    Number(totalRecord) > 1
                      ? t("common.records")
                      : t("QualityManagement.record")
                  }`}{" "}
                </p>
              )}
            </TotalRecord>
          </div>
          {listOption && _.size(listOption) >= 0 && (
            <WrapperOption
              key={"ALL"}
              onClick={() => handleSelectOption("ALL")}
              active={optionActive.includes("ALL")}
              bgColor={COLOR_INTERACTION_TYPE.ALL}
            >
              <CustomIcon src={AllIcon} alt="icon-filter" />
              <CustomText>ALL</CustomText>
            </WrapperOption>
          )}
          {open
            ? listOptionCurr?.map((item) => (
                <WrapperOptionCustom key={item}>
                  <CustomIcon src={ICONS[item]} alt="icon-filter" />
                  <Text
                    ellipsis={{
                      tooltip: item,
                    }}
                    style={{ maxWidth: 100 }}
                  >
                    {item}
                  </Text>
                  <WrapperClose>
                    <CustomCloseOutlined
                      style={{
                        fontSize: "16px",
                      }}
                      onClick={() => handleClickRemove(item)}
                    />
                  </WrapperClose>
                </WrapperOptionCustom>
              ))
            : listOption?.map((item) => (
                <WrapperOption
                  key={item?.label_name}
                  onClick={() =>
                    handleSelectOption(item?.label_name, item?.label_filter)
                  }
                  active={optionActive.includes(item?.label_name)}
                  bgColor={COLORS[item?.label_name]}
                >
                  <CustomIcon src={ICONS[item?.label_name]} alt="icon-filter" />
                  <Text
                    ellipsis={{
                      tooltip: item?.label_name,
                    }}
                    style={{ maxWidth: 100 }}
                  >
                    {item?.label_name}
                  </Text>
                </WrapperOption>
              ))}
        </WrapperList>
      )}
      <div
        style={{
          display: "flex",
          alignItems: "center",
          gap: "12px",
          position: "relative",
        }}
      >
        <SelectMultipleUser
          listUser={listUser}
          showUserSelect={
            listUser && listUser.length > 5
              ? 5
              : listUser
              ? Number(listUser.length) - 1
              : 1
          }
          listId={listOwner}
          setListId={setListOwner}
        />
        {showScrollButtonRight && (
          <ButtonScroll onClick={handleScrollRight}>
            <RightOutlined />
          </ButtonScroll>
        )}
      </div>
    </Wrapper>
  );
};

export default withTranslation()(memo(QuickFilter));

const ButtonScroll = styled.button`
  width: 35px;
  height: 35px;
  border: none;
  border-radius: 50%;
  position: absolute;
  top: 50%;
  left: -35px;
  z-index: 10;
  transform: translateY(-50%);
  transition: all ease-in 0.1s;
  cursor: pointer;
  box-shadow: rgba(0, 0, 0, 0.2) 0px 12px 28px 0px,
    rgba(0, 0, 0, 0.1) 0px 2px 4px 0px,
    rgba(255, 255, 255, 0.05) 0px 0px 0px 1px inset;

  &:hover {
    border: 2px solid ${(props) => props.theme.main};
    transition: all ease-in 0.1s;
  }
`;

const WrapperClose = styled.div`
  height: 16px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    background-color: #ccc;
  }
`;

const CustomCloseOutlined = styled(CloseOutlined)`
  &:hover {
    color: red;
  }
`;

const Wrapper = styled.div`
  background-color: #ffffff;
  padding: 0.65rem 1.5rem;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
  flex-wrap: nowrap;
  gap: 8px;
  max-height: 61px;
  position: relative;
`;

const WrapperLisMore = styled.div`
  display: flex;
  flex-direction: column;
  height: 400px;
  overflow-y: auto;
  overflow-x: hidden;
`;

const WrapperList = styled.div`
  background-color: #ffffff;
  display: flex;
  align-items: center;
  flex-direction: row;
  flex-wrap: nowrap;
  overflow-x: auto;
  scroll-behavior: smooth;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const WrapperOption = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: nowrap;

  padding: 9px 12px;
  margin-right: 8px;
  user-select: none;
  cursor: pointer;
  border-radius: 34px;
  transition: all ease-in-out 0.1s;
  ${({ active, bgColor }) => active && `background-color: ${bgColor};`}
  ${({ active, bgColor }) =>
    active && `&:hover { background-color: ${bgColor} !important;}`}
  &:hover {
    background-color: #eeeeee;
    transition: all ease-in 0.1s;
  }
`;

const WrapperOptionCustom = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: row;
  flex-wrap: nowrap;
  gap: 2px;

  padding: 9px 12px;
  margin-right: 12px;
  user-select: none;
  cursor: pointer;
  border-radius: 34px;
  transition: all ease-in-out 0.1s;

  background-color: #eeeeee;
  width: 110px;
`;

const WrapperOptionMcore = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  flex-direction: row;
  flex-wrap: nowrap;
  gap: 0.5rem;

  padding: 9px 12px;
  user-select: none;
  cursor: pointer;
  transition: all ease-in-out 0.1s;

  &:hover {
    background-color: #eeeeee;
    transition: all ease-in 0.1s;
  }
`;

const CustomIcon = styled.img`
  width: 20px;
  height: auto;
  object-fit: contain;
  cursor: pointer;
  margin-right: 5px;
`;

const WrapperIconEdit = styled.div`
  background-color: #fff;
  position: absolute;
  top: -50px;
  left: -25px;
  padding: 8px 14px;
  border-radius: 0 20px 20px 0;
  box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
  transition: all ease-in 0.1s;

  &.collapse {
    padding: 8px 10px;

    top: 10px;
    transition: all ease-in 0.1s;
  }

  &.open {
    padding: 8px 24px;
    transition: all ease-in 0.1s;
  }
  z-index: 100;
`;

const CustomIconEdit = styled.img`
  width: 20px;
  height: auto;
  object-fit: contain;
  cursor: pointer;
  margin-right: 5px;

  color: #fff;
`;

const CustomText = styled.p`
  font-weight: 400;
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  margin: 0;
  white-space: nowrap;
`;

const TotalRecord = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  justify-content: center;
  margin-right: 8px;

  border-radius: 40px;
  padding: 9px 16px;
  background-color: #eeeeee;
  /* min-width: 100px; */

  p {
    margin: 0;
    margin-left: 8px;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: 22px;
    color: ${(props) => props.theme.main};
    white-space: nowrap;
  }

  img {
    width: 20px;
    height: 20px;
    cursor: pointer;
  }
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;
