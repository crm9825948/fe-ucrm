import _ from "lodash";
import { memo, useCallback, useState } from "react";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { Collapse, Modal, Typography, Image, Button, Tooltip } from "antd";
import { useSelector } from "react-redux";
import AvatarImg from "assets/images/header/avatar.png";
import { BE_URL } from "constants/constants";
import { DownloadOutlined } from "@ant-design/icons";
const { Panel } = Collapse;
const { Text } = Typography;

const ColorScore = {
  GOOD: {
    bg: "#e7f8f0",
    color: "#0bb865",
  },
  AVERAGE: {
    bg: "#FBF0E6",
    color: "#D46B08",
  },
  BAD: {
    bg: "#CF13221A",
    color: "#CF1322",
  },
};

const LogScoring = ({ listLogScoring, setIdQM, idQM, listMainFieldsQM }) => {
  const { t } = useTranslation();
  const { listUser } = useSelector((state) => state.qualityManagementReducer);
  const [urlPreview, setUrlPreview] = useState("");
  const [fileName, setFileName] = useState("");
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isLoadingDownload, setIsLoadingDownload] = useState(false);

  const getName = useCallback(
    (id) => {
      return _.get(
        listUser?.filter((item) => item._id === id)[0],
        "Full_name",
        ""
      );
    },
    [listUser]
  );

  const getAvatar = useCallback(
    (id) => {
      let temp = listUser?.filter((item) => item._id === id);

      return _.get(temp[0], "avatar_config.url", "") !== ""
        ? BE_URL + _.get(temp[0], "avatar_config.url", "")
        : AvatarImg;
    },
    [listUser]
  );

  const checkColorScore = (min, max, score) => {
    let percent = (Number(score) - Number(min)) / (Number(max) - Number(min));

    if (percent < 0.5) return ColorScore.BAD;
    if (percent < 0.75) return ColorScore.AVERAGE;
    return ColorScore.GOOD;
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const downloadImage = (url, filename) => {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "blob";

    xhr.onload = function () {
      if (xhr.status === 200) {
        var blob = xhr.response;
        var link = document.createElement("a");
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;
        link.click();
      }
    };

    xhr.send();
  };

  return (
    <WrapperLogScoring>
      <CustomCollapse defaultActiveKey={"1"}>
        <Panel header={t("QualityManagement.logScoring")} key="1">
          <WrapperBody>
            {_.map(listLogScoring, (item, index) => {
              let grading = _.get(item, "field_grading", "");
              let scoring = _.get(item, "field_total", "");
              let max = _.get(item, "max_point", 0);
              let min = _.get(item, "min_point", 0);
              let ColorCheck = checkColorScore(
                min,
                max,
                _.get(item, `${scoring}.value`, 0)
              );

              return (
                <Tooltip
                  placement="bottom"
                  title={
                    listMainFieldsQM &&
                    listMainFieldsQM.includes("fld_qm_form_attach_file_01")
                      ? "Double click to view details"
                      : ""
                  }
                  arrow={true}
                  mouseEnterDelay={0.5}
                >
                  <WrapperItemLog
                    className={idQM === _.get(item, "_id", "") ? "active" : ""}
                    key={index}
                    onClick={() => {
                      setIdQM(_.get(item, "_id", ""));
                    }}
                    onDoubleClick={() => {
                      if (
                        listMainFieldsQM &&
                        listMainFieldsQM.includes("fld_qm_form_attach_file_01")
                      ) {
                        setUrlPreview(
                          _.get(item, "fld_qm_form_attach_file_01.value", "")
                        );
                        setFileName(_.get(item, "fld_qm_id_01.value", ""));
                        setIsModalOpen(true);
                      }
                    }}
                  >
                    <WrapperInfo>
                      <Avatar
                        src={getAvatar(_.get(item, "created_by", ""))}
                        alt=""
                      />
                      <WrapperName>
                        <Name>{getName(_.get(item, "created_by", ""))}</Name>
                        <TextCustom>
                          QM ID: {_.get(item, "fld_qm_id_01.value", "")}
                        </TextCustom>
                      </WrapperName>
                    </WrapperInfo>
                    <WrapperScore
                      BGColor={_.get(ColorCheck, "bg", "")}
                      Color={_.get(ColorCheck, "color", "")}
                    >
                      <Score>
                        {_.get(item, `${scoring}.value`, "")} / {max}
                      </Score>
                      <TextScore>
                        {_.get(item, `${grading}.value`, "")}
                      </TextScore>
                    </WrapperScore>
                    <WrapperDes>
                      <WrapperDate>
                        <TextCustom>
                          {t("QualityManagement.scoringDate")}
                        </TextCustom>
                        <Name>{_.get(item, "created_date", "")}</Name>
                      </WrapperDate>
                      <WrapperID>
                        <TextCustom>
                          {t("QualityManagement.formName")}
                        </TextCustom>
                        <NameId>
                          <TextAntd
                            ellipsis={{
                              tooltip: _.get(
                                item,
                                "fld_qm_form_name_01.value",
                                ""
                              ),
                            }}
                            style={{ maxWidth: 100 }}
                          >
                            {_.get(item, "fld_qm_form_name_01.value", "")}
                          </TextAntd>
                        </NameId>
                        <NameSmall>
                          <TextAntd
                            ellipsis={{
                              tooltip: _.get(
                                item,
                                "fld_qm_form_name_01.value",
                                ""
                              ),
                            }}
                            style={{ maxWidth: 300 }}
                          >
                            {_.get(item, "fld_qm_form_name_01.value", "")}
                          </TextAntd>
                        </NameSmall>
                      </WrapperID>
                    </WrapperDes>
                  </WrapperItemLog>
                </Tooltip>
              );
            })}
            <CustomModal
              open={isModalOpen}
              onOk={handleOk}
              onCancel={handleCancel}
              width={650}
              footer={null}
            >
              <DownloadImg
                type="primary"
                loading={isLoadingDownload}
                onClick={() => {
                  setIsLoadingDownload(true);
                  setTimeout(() => {
                    setIsLoadingDownload(false);
                  }, 2000);
                  downloadImage(urlPreview, `${fileName}.png`);
                }}
              >
                {!isLoadingDownload && <DownloadOutlined />}
                Download
              </DownloadImg>
              <Image
                width={600}
                src={urlPreview || ""}
                style={{ objectFit: "contain", border: "1px solid #ddd" }}
                preview={false}
              />
            </CustomModal>
          </WrapperBody>
        </Panel>
      </CustomCollapse>
    </WrapperLogScoring>
  );
};

export default withTranslation()(memo(LogScoring));

const TextAntd = styled(Text)``;

const CustomModal = styled(Modal)`
  .ant-modal-body {
    padding-top: 15px;
  }

  .ant-modal-header {
    background: #f2f4f5;
    padding: 6px 24px;
    .ant-modal-title {
      font-weight: 600;
    }
  }

  .ant-modal-footer {
    padding: 1.5rem;
  }
`;

const WrapperLogScoring = styled.div`
  padding: 0.5rem 0;
  border-bottom: 1px solid #ececec;
`;

const WrapperBody = styled.div`
  margin-bottom: 0.5rem;
  max-height: 300px;
  overflow-y: auto;
`;

const CustomCollapse = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border: none;
    .ant-collapse-header {
      display: flex;
      flex-direction: row-reverse;
      background-color: #ffffff;
      padding: 0.5rem 0;

      .ant-collapse-header-text {
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;

        color: #2c2c2c;
      }
    }

    .ant-collapse-content {
      border: none;

      .ant-collapse-content-box {
        padding: 0;
      }
    }
  }
`;

const WrapperItemLog = styled.div`
  padding: 0.5rem 1rem;
  background: #ffffff;
  cursor: pointer;

  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 0.75rem;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  flex-wrap: wrap;
  gap: 0.5rem;

  &:hover {
    background-color: #f2f4f5;
  }

  &.active {
    background-color: #f2f8ff;
    border: 1px solid #8bbdf7;
  }
`;

const WrapperInfo = styled.div`
  display: flex;
  flex-direction: row;
  gap: 0.75rem;
  min-width: 30%;
`;

const Avatar = styled.img`
  width: 50px;
  height: 50px;
  object-fit: cover;
  border-radius: 50%;
  border: 1px solid #ececec;
`;

const WrapperName = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
  justify-content: center;
`;

const Name = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  letter-spacing: 0.01em;
  color: #2c2c2c;
`;

const NameId = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  letter-spacing: 0.01em;
  color: #2c2c2c;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    display: none;
  }
`;

const NameSmall = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 130%;
  letter-spacing: 0.01em;
  color: #2c2c2c;
  display: none;

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    display: unset;
  }
`;

const TextCustom = styled.p`
  display: flex;
  align-items: center;
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 130%;

  letter-spacing: 0.01em;
  color: #6b6b6b;
`;

const WrapperDes = styled.div`
  display: flex;
  align-items: center;
  gap: 2rem;

  width: 300px;
  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 100%;
  }
`;

const WrapperScore = styled.div`
  padding: 8px 14px;
  background-color: ${(props) => props.BGColor};

  color: ${(props) => props.Color};

  border-radius: 8px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 0.25rem;

  min-width: 95px;
  min-height: 62px;
`;

const Score = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 18px;
  line-height: 130%;
  letter-spacing: 0.01em;
`;

const TextScore = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 130%;
  letter-spacing: 0.01em;
`;

const WrapperDate = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
  justify-content: center;
`;

const WrapperID = styled.div`
  display: flex;
  flex-direction: column;
  gap: 0.25rem;
  justify-content: center;
`;

const DownloadImg = styled(Button)`
  display: inline-flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  margin-bottom: 10px;
  background-color: ${(props) => props.theme.main};
  padding: 3px 8px;
  border-radius: 5px;
  color: #fff;
  border: ${(props) => props.theme.main};

  :hover {
    background: ${(props) => props.theme.main} !important;
    color: #fff;
    border: ${(props) => props.theme.main};
  }

  :nth-child(1) {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;
