import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { memo, useCallback, useEffect } from "react";
import { withTranslation } from "react-i18next";
import { Collapse } from "antd";
import IconType from "assets/icons/qualityManagement/Type.png";
import IconIncomingType from "assets/icons/qualityManagement/IncommingType.png";
import IconOutgoingType from "assets/icons/qualityManagement/OutgoingType.png";
import IconTime from "assets/icons/qualityManagement/Time.png";
import IconObjectID from "assets/icons/qualityManagement/ObjectID.png";
import IconChannelType from "assets/icons/qualityManagement/Channel.png";
import IconExtension from "assets/icons/qualityManagement/Extension.png";
import IconContact from "assets/icons/qualityManagement/TenantID.png";
import IconTicket from "assets/icons/qualityManagement/ContentFileType.png";
import IconInteraction from "assets/icons/qualityManagement/SourceInteraction.png";
import IconOwner from "assets/icons/qualityManagement/iConOwner.png";
import IconUserName from "assets/icons/qualityManagement/iConUserName.png";
import IconTypeCall from "assets/icons/qualityManagement/IconTypeCall.png";
import IconCallStatus from "assets/icons/qualityManagement/IconCallStatus.png";
import IconNumberPhone from "assets/icons/qualityManagement/IconNumberPhone.png";

import IconSubject from "assets/icons/qualityManagement/subject.png";
import IconFrom from "assets/icons/qualityManagement/from.png";
import IconTo from "assets/icons/qualityManagement/to.png";
import IconCC from "assets/icons/qualityManagement/CC.png";
import IconBCC from "assets/icons/qualityManagement/BBC.png";
import {
  setFieldInteractionID,
  setFieldsourceInteractionID,
  setFieldsourceObjectID,
} from "redux/slices/QualityManagement";

const { Panel } = Collapse;

const TYPE_INTERACTION = {
  0: "Call",
  1: "Email",
  2: "SMS",
  3: "VBEE",
  4: "IC",
};

const InfoDetail = ({ title, data, listMainFields, isIC = false }) => {
  const dispatch = useDispatch();
  const { listUser } = useSelector((state) => state.qualityManagementReducer);

  const getName = useCallback(
    (id) => {
      return _.get(
        listUser.filter((item) => item._id === id)[0],
        "Full_name",
        ""
      );
    },
    [listUser]
  );

  const covertToTitleCase = (str) => {
    return str.replace(/\w\S*/g, function (word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
  };

  const checkTypeCall = (value) => {
    if (value === "1") {
      return "Finesse";
    }
    if (value === "0") {
      return "Connect247";
    }
    return "";
  };

  const checkShow = (value, field = "") => {
    if (
      value !== undefined &&
      value !== null &&
      listMainFields.includes(field)
    ) {
      return true;
    }
    return false;
  };

  useEffect(() => {
    if (data && data?.data_mapping?.fld_interactionid_) {
      dispatch(setFieldInteractionID(data?.data_mapping?.fld_interactionid_));
    }
    if (data && data?.data_mapping?.fld_sourceinteractionid_) {
      dispatch(
        setFieldsourceInteractionID(
          data?.data_mapping?.fld_sourceinteractionid_
        )
      );
    }
    if (data && data?.data_mapping?.fld_sourceobjectid_) {
      dispatch(setFieldsourceObjectID(data?.data_mapping?.fld_sourceobjectid_));
    }
  }, [data, dispatch]);

  return (
    <WrapperInfoDetail>
      <CustomCollapse defaultActiveKey={"1"}>
        <Panel header={title} key="1">
          <WrapperDetail>
            <WrapperColLeft>
              {checkShow(
                _.get(data, `data_interaction.created_by`),
                "created_by"
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconOwner} alt="" />
                    <Title>Assign to</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {getName(_.get(data, `data_interaction.created_by`, ""))}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_interactionid_}.value`
                ),
                `${data?.data_mapping?.fld_interactionid_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconInteraction} alt="" />
                    <Title>Interaction ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_interactionid_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {/* {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_objectid_}.value`,
                  undefined
                )
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconObject} alt="" />
                    <Title>Object ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_objectid_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )} */}

              {/* {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_sourceinteractionid_}.value`,
                  undefined
                )
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconSourceInteraction} alt="" />
                    <Title>Source Interaction ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_sourceinteractionid_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )} */}

              {/* <WrapperRow>
                <WrapperTitle>
                  <ImgIcon src={IconSourceObject} alt="" />
                  <Title>Source Object Interaction</Title>
                </WrapperTitle>
                <WrapperValue>
                  <Value>Interaction Center QM</Value>
                </WrapperValue>
              </WrapperRow> */}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_sourceobjectid_}.value`
                ),
                `${data?.data_mapping?.fld_sourceobjectid_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconObjectID} alt="" />
                    <Title>Source Object ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_sourceobjectid_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {/* {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_sourceobjectid_}.value`,
                  undefined
                )
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconRecordID} alt="" />
                    <Title>Source Record ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>94aw8r4a1s5g1sr9y</Value>
                  </WrapperValue>
                </WrapperRow>
              )} */}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_ticketid_}.value`
                ),
                `${data?.data_mapping?.fld_ticketid_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconTicket} alt="" />
                    <Title>Ticket ID</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_ticketid_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_contact}.value`
                ),
                `${data?.data_mapping?.fld_contact}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconContact} alt="" />
                    <Title>Contact</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_contact}.value`
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {!isIC &&
                checkShow(
                  _.get(data, `data_interaction.created_date`),
                  "created_date"
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconTime} alt="" />
                      <Title>Created date</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(data, `data_interaction.created_date`, "")}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}

              {!isIC &&
                checkShow(
                  _.get(
                    data,
                    `data_interaction.${data?.data_mapping?.fld_type_}.value`
                  ),
                  `${data?.data_mapping?.fld_type_}`
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconType} alt="" />
                      <Title>Type</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                          ""
                        ) === "CALL"
                          ? covertToTitleCase(
                              _.get(
                                data,
                                `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                                ""
                              )
                            )
                          : _.get(
                              data,
                              `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                              ""
                            )}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}
              {!isIC &&
                checkShow(
                  _.get(
                    data,
                    `data_interaction.${data?.data_mapping?.fld_channeltype_}.value`
                  ),
                  `${data?.data_mapping?.fld_channeltype_}`
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconChannelType} alt="" />
                      <Title>Channel type</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_channeltype_}.value`,
                          ""
                        )}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}
            </WrapperColLeft>
            <WrapperColRight>
              {isIC &&
                checkShow(
                  _.get(data, `data_interaction.created_date`),
                  "created_date"
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconTime} alt="" />
                      <Title>Created date</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(data, `data_interaction.created_date`, "")}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}

              {isIC &&
                checkShow(
                  _.get(
                    data,
                    `data_interaction.${data?.data_mapping?.fld_type_}.value`
                  ),
                  `${data?.data_mapping?.fld_type_}`
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconType} alt="" />
                      <Title>Type</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                          ""
                        ) === "CALL"
                          ? covertToTitleCase(
                              _.get(
                                data,
                                `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                                ""
                              )
                            )
                          : _.get(
                              data,
                              `data_interaction.${data?.data_mapping?.fld_type_}.value`,
                              ""
                            )}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}
              {isIC &&
                checkShow(
                  _.get(
                    data,
                    `data_interaction.${data?.data_mapping?.fld_channeltype_}.value`
                  ),
                  `${data?.data_mapping?.fld_channeltype_}`
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconChannelType} alt="" />
                      <Title>Channel type</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_channeltype_}.value`,
                          ""
                        )}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}
              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_username_}.value`
                ),
                `${data?.data_mapping?.fld_username_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconUserName} alt="" />
                    <Title>User name</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_username_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}
              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_incomingtype}.value`
                ),
                `${data?.data_mapping?.fld_incomingtype}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconIncomingType} alt="" />
                    <Title>Incomming type</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {
                        TYPE_INTERACTION[
                          _.get(
                            data,
                            `data_interaction.${data?.data_mapping?.fld_incomingtype}.value`,
                            ""
                          )
                        ]
                      }{" "}
                      {TYPE_INTERACTION[
                        _.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_incomingtype}.value`,
                          ""
                        )
                      ] !== "IC" && "In"}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_outgoingtype_}.value`
                ),
                `${data?.data_mapping?.fld_outgoingtype_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconOutgoingType} alt="" />
                    <Title>Outgoing type</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {
                        TYPE_INTERACTION[
                          _.get(
                            data,
                            `data_interaction.${data?.data_mapping?.fld_outgoingtype_}.value`
                          )
                        ]
                      }{" "}
                      {TYPE_INTERACTION[
                        _.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_outgoingtype_}.value`
                        )
                      ] !== "IC" && "Out"}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}
              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_extension}.value`
                ),
                `${data?.data_mapping?.fld_extension}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconExtension} alt="" />
                    <Title>Extension</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_extension}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}
              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_extensiontransfer_}.value`
                ),
                `${data?.data_mapping?.fld_extensiontransfer_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconExtension} alt="" />
                    <Title>Extension Transfer</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_extensiontransfer_}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_typecall_}.value`
                ),
                `${data?.data_mapping?.fld_typecall_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconTypeCall} alt="" />
                    <Title>Type call</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {checkTypeCall(
                        _.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_typecall_}.value`
                        )
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_callstatus}.value`
                ),
                `${data?.data_mapping?.fld_callstatus}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconCallStatus} alt="" />
                    <Title>Call Status</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_callstatus}.value`
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_phone_}.value`
                ),
                `${data?.data_mapping?.fld_phone_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconNumberPhone} alt="" />
                    <Title>Phone</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_phone_}.value`
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_starttime}.value`
                ),
                `${data?.data_mapping?.fld_starttime}`
              ) &&
                checkShow(
                  _.get(
                    data,
                    `data_interaction.${data?.data_mapping?.fld_endtime_}.value`
                  ),
                  `${data?.data_mapping?.fld_endtime_}`
                ) && (
                  <WrapperRow>
                    <WrapperTitle>
                      <ImgIcon src={IconTime} alt="" />
                      <Title>Start time - End time</Title>
                    </WrapperTitle>
                    <WrapperValue>
                      <Value>
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_starttime}.value`,
                          ""
                        )}{" "}
                        -{"  "}
                        {_.get(
                          data,
                          `data_interaction.${data?.data_mapping?.fld_endtime_}.value`,
                          ""
                        )}
                      </Value>
                    </WrapperValue>
                  </WrapperRow>
                )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_subject}.value`
                ),
                `${data?.data_mapping?.fld_subject}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconSubject} alt="" />
                    <Title>Subject</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_subject}.value`,
                        ""
                      )}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_from_}.value`
                ),
                `${data?.data_mapping?.fld_from_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconFrom} alt="" />
                    <Title>From</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_from_}.value`,
                        ""
                      ).replace(/,/g, ", ")}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_to_}.value`
                ),
                `${data?.data_mapping?.fld_to_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconTo} alt="" />
                    <Title>To</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_to_}.value`,
                        ""
                      ).replace(/,/g, ", ")}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_ccemail_}.value`
                ),
                `${data?.data_mapping?.fld_ccemail_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconCC} alt="" />
                    <Title>CC</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_ccemail_}.value`,
                        ""
                      ).replace(/,/g, ", ")}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}

              {checkShow(
                _.get(
                  data,
                  `data_interaction.${data?.data_mapping?.fld_bccemail_}.value`
                ),
                `${data?.data_mapping?.fld_bccemail_}`
              ) && (
                <WrapperRow>
                  <WrapperTitle>
                    <ImgIcon src={IconBCC} alt="" />
                    <Title>BCC</Title>
                  </WrapperTitle>
                  <WrapperValue>
                    <Value>
                      {_.get(
                        data,
                        `data_interaction.${data?.data_mapping?.fld_bccemail_}.value`,
                        ""
                      ).replace(/,/g, ", ")}
                    </Value>
                  </WrapperValue>
                </WrapperRow>
              )}
            </WrapperColRight>
          </WrapperDetail>
        </Panel>
      </CustomCollapse>
    </WrapperInfoDetail>
  );
};

export default withTranslation()(memo(InfoDetail));

const WrapperInfoDetail = styled.div`
  padding: 0.5rem 0;
  border-bottom: 1px solid #ececec;
`;

const WrapperDetail = styled.div`
  padding: 1rem;
  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 0.5rem;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
`;

const WrapperRow = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  justify-content: center;
  gap: 0.5rem;
`;

const WrapperColLeft = styled.div`
  flex: 1;
  padding-right: 1rem;
  border-right: 1px solid #ececec;
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
`;

const WrapperColRight = styled.div`
  flex: 1;
  padding-left: 1rem;
  display: flex;
  flex-direction: column;
  gap: 1.5rem;
`;

const WrapperTitle = styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  align-items: center;
  gap: 0.5rem;

  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  width: 190px;

  color: #6b6b6b;
`;

const WrapperValue = styled.div`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #2c2c2c;
  min-width: 115px;
`;

const ImgIcon = styled.img`
  max-width: 22px;
`;

const Title = styled.p`
  margin: 0;
`;

const Value = styled.p`
  margin: 0;
  word-wrap: break-all;
`;

const CustomCollapse = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border: none;
    .ant-collapse-header {
      display: flex;
      flex-direction: row-reverse;
      background-color: #ffffff;
      padding: 0.5rem 0;

      .ant-collapse-header-text {
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;

        color: #2c2c2c;
      }
    }

    .ant-collapse-content {
      border: none;

      .ant-collapse-content-box {
        padding: 0;
      }
    }
  }
`;
