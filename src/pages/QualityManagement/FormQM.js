import { memo, useCallback, useEffect, useMemo, useState } from "react";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { setLoadingSubmitForm, submitForm } from "redux/slices/formSetting";
import { Form, Button, Empty, Typography, Tabs, Spin, Select } from "antd";
import FormContent from "pages/FormSetting/FormContent/FormContent";
import _ from "lodash";
import { setShowModalConfirm } from "redux/slices/global";
import EmptyObject from "assets/icons/qualityManagement/IconEmpty.png";
import BG from "assets/images/qualityManagement/NewBG.png";
import ModalConfim from "components/Modal/ModalConfirm";
import { toBlob } from "html-to-image";
import { getAllFormQM } from "redux/slices/QualityManagement";
import moment from "moment";

const { Text } = Typography;

const ColorScore = {
  GOOD: "#0bb865",
  AVERAGE: "#D46B08",
  BAD: "#d63e4a",
};

const FormQM = ({
  idInteraction,
  detailSettings,
  keyFormSelected,
  dataInteraction,
  create,
  collapse,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [listFormQM, setListFormQM] = useState([]);
  const [formActive, setFormActive] = useState({});
  const [form] = Form.useForm();

  //eslint-disable-next-line
  const [change, setChange] = useState("");
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { isLoadingSubmit } = useSelector((state) => state.formSettingReducer);
  const { loadingLogScoring, loadingInteractionDetail, listFormQMSettings } =
    useSelector((state) => state.qualityManagementReducer);
  const [page, setPage] = useState(0);
  const [history, setHistory] = useState(["Default"]);
  const [dataSubmit, setDataSubmit] = useState({});
  const [conditionPagination, setConditionPagination] = useState("");
  const [formPoint, setFormPoint] = useState(0);

  const convertKeyFormSeleted = (str) => {
    str = str.toLowerCase();
    str = str.replace(/\s+/g, "_");

    return str;
  };

  const checkColorScore = (min, max, score) => {
    let percent = (Number(score) - Number(min)) / (Number(max) - Number(min));

    if (percent < 0.5) return ColorScore.BAD;
    if (percent < 0.75) return ColorScore.AVERAGE;
    return ColorScore.GOOD;
  };

  const checkRule = useCallback(
    (rule, key) => {
      if (
        userRuleGlobal.find(
          (item) =>
            _.get(item, "domain", "") === key && item.actions.includes(rule)
        )
      )
        return true;
      else return false;
    },
    [userRuleGlobal]
  );

  useEffect(() => {
    setFormActive({});
  }, [keyFormSelected]);

  useEffect(() => {
    if (detailSettings && keyFormSelected && listFormQMSettings) {
      let tem = [];
      listFormQMSettings.forEach((form) => {
        if (
          form?.object_id === "obj_crm_qm_interaction_00001" &&
          form?.form_type === "QM_FORM" &&
          form?.status === true
        ) {
          tem.push(form);
        }
      });
      let datalistForm = tem?.filter((item) =>
        detailSettings[convertKeyFormSeleted(keyFormSelected)]?.includes(
          item?._id
        )
      );
      if (datalistForm?.length > 0) {
        let temSections = ["Default"];
        if (datalistForm[0].sections && datalistForm[0].sections?.length > 0) {
          temSections = datalistForm[0].sections;
        }
        let temForm = {
          ...datalistForm[0],
          sections: temSections,
        };
        setHistory([temSections[0]]);
        setFormActive(temForm);
        temForm.list_field?.forEach((field) => {
          if (field.default_value !== undefined) {
            if (
              field.field_type === "date" ||
              field.field_type === "datetime-local"
            ) {
              form.setFieldsValue({
                [field.id]: moment(field.default_value),
              });
            } else {
              form.setFieldsValue({ [field.id]: field.default_value });
            }
          }
        });
      }
      setListFormQM(datalistForm);
    } else {
      setListFormQM([]);
    }
  }, [listFormQMSettings, detailSettings, keyFormSelected, form]);

  useEffect(() => {
    if (userDetail) {
      dispatch(
        getAllFormQM({
          tenant_id: userDetail.tenant_id,
        })
      );
    }
  }, [dispatch, userDetail]);

  const handleChangeForm = (id) => {
    listFormQM.forEach((form) => {
      if (form._id === id) {
        let temSections = ["Default"];
        if (form.sections && form.sections?.length > 0) {
          temSections = form.sections;
        }
        let tem = {
          ...form,
          sections: temSections,
        };
        setHistory([temSections[0]]);
        setFormActive(tem);
      }
    });
    // form.resetFields();
    setDataSubmit({});
    setPage(0);
  };

  const convertToBlob = (callback) => {
    toBlob(document.getElementById("formSubmit"))
      .then(function (blob) {
        callback(blob);
      })
      .catch(function (error) {
        console.error("Error converting HTML to PNG:", error);
      });
  };

  const handleFinish = (values) => {
    dispatch(setLoadingSubmitForm(true));

    convertToBlob((blob) => {
      const file = new File([blob], "screenshot.png", {
        type: "image/png",
      });

      let fields_data = [
        {
          id_field: "fld_qm_linking_interaction_01",
          value: idInteraction,
        },
        {
          id_field: "fld_qm_form_id_01",
          value: formActive?._id || "",
        },
        {
          id_field: "fld_qm_form_name_01",
          value: formActive?.form_name || "",
        },
      ];
      let temp = { ...values };
      if (formActive.field_total) {
        fields_data.push({
          id_field: formActive.field_total,
          value: formPoint,
        });
      }
      if (formActive.field_grading) {
        fields_data.push({
          id_field: formActive.field_grading,
          value: handleGrading(),
        });
      }
      const formDataSave = new FormData();
      Object.entries(temp).forEach(([key, value]) => {
        if (value !== undefined) {
          let tem = value;
          if (formActive.list_field && formActive.list_field.length > 0) {
            formActive.list_field.forEach((field) => {
              if (
                key === field.id &&
                (history.includes(field.section) || !field.section)
              ) {
                if (field.field_type !== "file") {
                  if (
                    field.field_type === "date" ||
                    field.field_type === "datetime-local"
                  ) {
                    tem = moment(value).format(
                      field.field_type === "date"
                        ? "YYYY-MM-DD"
                        : "YYYY-MM-DD HH:mm:ss"
                    );
                  }
                  fields_data.push({ id_field: key, value: tem });
                } else {
                  value.forEach((item) => {
                    formDataSave.append(field.id, item.originFileObj);
                  });
                }
              }
            });
          }
        }
      });
      formDataSave.append("form_id", formActive._id);
      formDataSave.append("tenant_id", userDetail.tenant_id);
      formDataSave.append("fields_data", JSON.stringify(fields_data));
      formDataSave.append("create_qm_user_id", _.get(userDetail, "_id", ""));
      formDataSave.append("fld_qm_form_attach_file_01", file);
      dispatch(submitForm(formDataSave));
      handleCancel();
    });
  };
  const handleResetField = (field) => {
    let tem = "";
    if (formActive.list_field) {
      formActive.list_field.forEach((item) => {
        if (item.triggering_field === field) {
          form.setFieldsValue({ [item.id]: undefined });
          dataSubmit[item.id] = undefined;
          tem = item.id;
        } else {
          tem = "";
        }
        if (tem) {
          handleResetField(tem);
        }
      });
    }
  };
  const handleGrading = () => {
    let tem = formActive.default_grading_value;
    formActive.grading_mapping.every((mapping) => {
      if (formPoint >= mapping.min_val && formPoint <= mapping.max_max) {
        tem = mapping.grading_value;
        return false;
      }
      return true;
    });
    return tem;
  };
  //eslint-disable-next-line
  const countPoint = () => {
    let point = 0;
    let tem = { ...dataSubmit, ...form.getFieldsValue() };
    if (formActive.scoring_option !== "no_scoring") {
      formActive.list_field?.forEach((field) => {
        // if (field.point_options?.length > 0) {
        Object.entries(tem).forEach((item) => {
          if (
            item[0] === field.id &&
            (history.includes(field.section) || !field.section)
          ) {
            field.point_options?.forEach((option) => {
              if (option.option.toString() === item[1]?.toString()) {
                point = point + option.point;
              }
            });
          }
        });
        // }
      });
    }
    if (formActive.min_point === 0 || formActive.min_point) {
      if (point < formActive.min_point) {
        point = formActive.min_point;
      }
    }

    if (formActive.max_point === 0 || formActive.max_point) {
      if (point > formActive.max_point) {
        point = formActive.max_point;
      }
    }
    setFormPoint(point);
  };

  useEffect(() => {
    countPoint();
  }, [countPoint]);

  const checkConditionPagination = useCallback(() => {
    let flag = "";
    Object.entries(form.getFieldsValue()).forEach(([key, value]) => {
      formActive.list_field?.forEach((field) => {
        if (
          field.conditional_pagination &&
          field.pagination_option &&
          field.id === key &&
          field.section === formActive.sections[page]
        ) {
          field.pagination_option.every((option) => {
            if (option.value.toString() === value?.toString()) {
              flag = option.option;
              return false;
            } else {
              return true;
            }
          });
        }
      });
    });
    setConditionPagination(flag);
    //eslint-disable-next-line
  }, [form, formActive, page, isLoadingSubmit, idInteraction]);

  useEffect(() => {
    checkConditionPagination();
  }, [checkConditionPagination]);

  const onChangeHandle = useCallback(
    (searchText) => {
      setChange(searchText);
      countPoint();
      checkConditionPagination();
    },
    [countPoint, checkConditionPagination]
  );

  const checkIndexPage = (name, ordereds = ["Default"]) => {
    let index = ordereds.findIndex((item) => item === name);
    return index;
  };

  const handleNext = (values) => {
    let newData = { ...dataSubmit, ...values };
    if (
      page === formActive.sections.length - 1 ||
      conditionPagination === "submit_form_crm"
    ) {
      handleFinish(newData);
    } else {
      setDataSubmit(newData);
      if (
        conditionPagination &&
        checkIndexPage(conditionPagination, formActive.sections) > page
      ) {
        let tem = [...history, conditionPagination];
        setHistory(tem);
        setPage(checkIndexPage(conditionPagination, formActive.sections));
      } else {
        setPage((prev) => {
          let tem = [...history, formActive.sections[prev + 1]];
          setHistory(tem);
          return prev + 1;
        });
      }
    }
  };

  const handleBack = () => {
    let tem = [...history];
    tem.pop();
    setHistory(tem);
    setPage(checkIndexPage(tem[tem.length - 1], formActive.sections));
    setDataSubmit((prev) => ({
      ...prev,
      ...form.getFieldsValue(),
    }));
  };

  const handleCancel = () => {
    console.log("cancel");
    form.resetFields();
    setDataSubmit({});
    setPage(0);
    if (Object.entries(formActive).length > 0) {
      setHistory([formActive.sections[0]]);
      formActive.list_field?.forEach((field) => {
        if (field.default_value !== undefined) {
          if (
            field.field_type === "date" ||
            field.field_type === "datetime-local"
          ) {
            form.setFieldsValue({
              [field.id]: moment(field.default_value),
            });
          } else {
            form.setFieldsValue({ [field.id]: field.default_value });
          }
        }
      });
    }
  };

  const handleOldData = (listData = [], ordereds = ["Default"]) => {
    let tem = [];
    listData.forEach((item) => {
      tem.push({
        ...item,
        section: item.section ? item.section : ordereds[0],
      });
    });
    return tem;
  };

  const checkShow = (value) => {
    if (value !== undefined && value !== null) {
      return true;
    }
    return false;
  };

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onChangeHandle, 50),
    [onChangeHandle]
  );

  useEffect(() => {
    if (idInteraction) {
      handleCancel();
    }
    //eslint-disable-next-line
  }, [idInteraction, form, formActive]);

  const RenderInfo = (value) => {
    switch (value.toLowerCase()) {
      case "email in":
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_from_}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "8px" }}>
                From:
              </div>
              <div className="number">
                <Text
                  style={{ maxWidth: 250, color: "#fff" }}
                  ellipsis={{
                    tooltip: _.get(
                      dataInteraction,
                      `data_interaction.${dataInteraction?.data_mapping?.fld_from_}.value`,
                      ""
                    ).replace(/,/g, ", "),
                  }}
                >
                  {_.get(
                    dataInteraction,
                    `data_interaction.${dataInteraction?.data_mapping?.fld_from_}.value`,
                    ""
                  )}
                </Text>
              </div>
            </Result>
          );
        }
        return "";
      case "email out":
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_to_}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "8px" }}>
                To:
              </div>
              <div className="number">
                <Text
                  style={{ maxWidth: 250, color: "#fff" }}
                  ellipsis={{
                    tooltip: _.get(
                      dataInteraction,
                      `data_interaction.${dataInteraction?.data_mapping?.fld_to_}.value`,
                      ""
                    ).replace(/,/g, ", "),
                  }}
                >
                  {_.get(
                    dataInteraction,
                    `data_interaction.${dataInteraction?.data_mapping?.fld_to_}.value`,
                    ""
                  )}
                </Text>
              </div>
            </Result>
          );
        }
        return "";
      case "call in":
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_phone_}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "12px" }}>
                {t("QualityManagement.numberPhone")}:{" "}
              </div>
              <div className="number">
                {_.get(
                  dataInteraction,
                  `data_interaction.${dataInteraction?.data_mapping?.fld_phone_}.value`,
                  ""
                )}
              </div>
            </Result>
          );
        }
        return "";
      case "call out":
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_phone_}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "12px" }}>
                {t("QualityManagement.numberPhone")}:{" "}
              </div>
              <div className="number">
                {_.get(
                  dataInteraction,
                  `data_interaction.${dataInteraction?.data_mapping?.fld_phone_}.value`,
                  ""
                )}
              </div>
            </Result>
          );
        }
        return "";
      case "sms out":
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_to_}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "12px" }}>
                {t("QualityManagement.numberPhone")}:{" "}
              </div>
              <div className="number">
                {_.get(
                  dataInteraction,
                  `data_interaction.${dataInteraction?.data_mapping?.fld_to_}.value`,
                  ""
                )}
              </div>
            </Result>
          );
        }
        return "";
      default:
        if (
          checkShow(
            _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_contact}.value`,
              undefined
            )
          )
        ) {
          return (
            <Result>
              <div className="tilte" style={{ marginBottom: "12px" }}>
                Contact ID:
              </div>
              <div className="number">
                {_.get(
                  dataInteraction,
                  `data_interaction.${dataInteraction?.data_mapping?.fld_contact}.value`,
                  ""
                )}
              </div>
            </Result>
          );
        }
        return "";
    }
  };

  return (
    <WrapperForm className={collapse ? "collapse" : ""}>
      <CustomSpin spinning={loadingLogScoring || loadingInteractionDetail}>
        {listFormQM.length === 0 ? (
          <>
            <ModalConfim
              img={EmptyObject}
              title={
                "Are you sure you want to go to Quality Management Settings?"
              }
              decs={
                "You will be redirected to the Quality Management Settings page."
              }
              type="Redirect"
              url="/quality-management-settings"
            />
            <EmptyCustom>
              <Empty
                description={
                  checkRule("edit", "quality_management") ? false : "No data"
                }
              />
              {checkRule("edit", "quality_management") && (
                <>
                  <p>
                    Please configure the form in the{" "}
                    <span>Quality Management Settings</span>
                  </p>
                  <ButtonCustom
                    onClick={() => dispatch(setShowModalConfirm(true))}
                  >
                    Go to Quality Management Settings
                  </ButtonCustom>
                </>
              )}
            </EmptyCustom>
          </>
        ) : listFormQM?.length > 4 ? (
          <div
            style={{ backgroundColor: "#fff", padding: "0 0 10px 0" }}
            id="formSubmit"
          >
            <FromWrap
              style={{
                padding: "10px 16px 0 16px",
                position: "sticky",
                top: "0px",
                zIndex: 10,
              }}
            >
              <CustomSelect
                showSearch
                placeholder="SelectForm.."
                value={_.get(formActive, "_id", "")}
                onChange={(value) => {
                  handleChangeForm(value);
                }}
                filterOption={(input, option) =>
                  (option?.label ?? "")
                    .toLowerCase()
                    .includes(input.toLowerCase())
                }
                options={listFormQM?.map((item) => {
                  return {
                    label: _.get(item, "form_name", "Form"),
                    value: _.get(item, "_id", ""),
                  };
                })}
              />
            </FromWrap>
            {Object.entries(formActive).length > 0 && (
              <Content>
                <FromWrap>
                  <IntroWrap>
                    {/* {formActive.form_name && (
                      <Name>{formActive.form_name}</Name>
                    )} */}
                    <Name>{formActive.sections[page]}</Name>
                    {formActive.form_description && (
                      <Description>{formActive.form_description}</Description>
                    )}
                  </IntroWrap>
                  <Form
                    // onFinish={handleFinish}
                    onFinish={handleNext}
                    form={form}
                    labelCol={{ span: 24 }}
                    wrapperCol={{ span: 24 }}
                    onValuesChange={(value, values) => {
                      debouncedSearchHandler(value);
                      handleResetField(Object.keys(value)[0]);
                    }}
                  >
                    <FormContent
                      // fields={formActive.list_field}
                      fields={handleOldData(
                        formActive.list_field,
                        formActive.sections
                      )}
                      form={form}
                      defaultValue={{}}
                      listOrdered={formActive.sections}
                      page={page}
                    />
                  </Form>
                  <WrapperResult style={{ backgroundImage: `url(${BG})` }}>
                    {/* <img src={BG} alt="" /> */}
                    {formActive.scoring_option !== "no_scoring" && (
                      <Result>
                        <div className="tilte">
                          {t("QualityManagement.totalScore")}:
                        </div>
                        <div className="result">
                          {formPoint}
                          {formActive.check_grading && (
                            <div
                              className="ratings"
                              style={{
                                backgroundColor: checkColorScore(
                                  _.get(formActive, "min_point", 0),
                                  _.get(formActive, "max_point", 0),
                                  formPoint
                                ),
                              }}
                            >
                              {handleGrading()}
                            </div>
                          )}
                        </div>
                      </Result>
                    )}
                    {RenderInfo(keyFormSelected)}
                  </WrapperResult>
                </FromWrap>
                {create ? (
                  <ButtonWrap>
                    {(page === formActive.sections.length - 1 ||
                      conditionPagination === "submit_form_crm") && (
                      <CustomButton
                        onClick={() => form.submit()}
                        loading={isLoadingSubmit}
                      >
                        {formActive.form_save}
                      </CustomButton>
                    )}
                    {/* <CustomButton
                      onClick={() => {
                        form.submit();
                      }}
                      loading={isLoadingSubmit}
                    >
                      {formActive.form_save}
                    </CustomButton> */}
                    {page !== formActive.sections.length - 1 &&
                      conditionPagination !== "submit_form_crm" && (
                        <CustomButton onClick={() => form.submit()}>
                          {t("form.next")}
                        </CustomButton>
                      )}
                    {page !== 0 && (
                      <CustomButton onClick={handleBack}>
                        {t("form.back")}
                      </CustomButton>
                    )}
                    <CustomButton onClick={() => handleCancel()}>
                      {formActive.form_cancel}
                    </CustomButton>
                  </ButtonWrap>
                ) : (
                  <ButtonWrap>
                    <Button
                      disabled
                      style={{ width: "100%", borderRadius: "10px" }}
                      size="large"
                    >
                      You don't have the authority to grade.
                    </Button>
                  </ButtonWrap>
                )}
              </Content>
            )}
          </div>
        ) : (
          <CustomTabs
            id="formSubmit"
            type="card"
            activeKey={_.get(formActive, "_id", "")}
            tabPosition={"top"}
            onChange={(value) => {
              handleChangeForm(value);
            }}
            items={listFormQM?.map((item) => {
              return {
                label: (
                  <Text
                    ellipsis={{
                      tooltip: _.get(item, "form_name", "Form"),
                    }}
                    style={{ maxWidth: 200 }}
                  >
                    {_.get(item, "form_name", "Form")}
                  </Text>
                ),
                key: item._id,
                children: Object.entries(formActive).length > 0 && (
                  <Content>
                    <FromWrap>
                      <IntroWrap>
                        {/* {formActive.form_name && (
                          <Name>{formActive.form_name}</Name>
                        )} */}
                        <Name>{formActive.sections[page]}</Name>
                        {formActive.form_description && (
                          <Description>
                            {formActive.form_description}
                          </Description>
                        )}
                      </IntroWrap>
                      <Form
                        // onFinish={handleFinish}
                        onFinish={handleNext}
                        form={form}
                        labelCol={{ span: 24 }}
                        wrapperCol={{ span: 24 }}
                        onValuesChange={(value, values) => {
                          debouncedSearchHandler(value);
                          handleResetField(Object.keys(value)[0]);
                        }}
                      >
                        <FormContent
                          // fields={formActive.list_field}
                          fields={handleOldData(
                            formActive.list_field,
                            formActive.sections
                          )}
                          form={form}
                          defaultValue={{}}
                          listOrdered={formActive.sections}
                          page={page}
                        />
                      </Form>
                      <WrapperResult style={{ backgroundImage: `url(${BG})` }}>
                        {formActive.scoring_option !== "no_scoring" && (
                          <Result>
                            <div className="tilte">
                              {t("QualityManagement.totalScore")}:
                            </div>
                            <div className="result">
                              {formPoint}
                              {formActive.check_grading && (
                                <div
                                  className="ratings"
                                  style={{
                                    backgroundColor: checkColorScore(
                                      _.get(formActive, "min_point", 0),
                                      _.get(formActive, "max_point", 0),
                                      formPoint
                                    ),
                                  }}
                                >
                                  {handleGrading()}
                                </div>
                              )}
                            </div>
                          </Result>
                        )}
                        {RenderInfo(keyFormSelected)}
                      </WrapperResult>
                    </FromWrap>
                    {create ? (
                      <ButtonWrap>
                        {(page === formActive.sections.length - 1 ||
                          conditionPagination === "submit_form_crm") && (
                          <CustomButton
                            onClick={() => form.submit()}
                            loading={isLoadingSubmit}
                          >
                            {formActive.form_save}
                          </CustomButton>
                        )}
                        {/* <CustomButton
                          onClick={() => {
                            form.submit();
                          }}
                          loading={isLoadingSubmit}
                        >
                          {formActive.form_save}
                        </CustomButton> */}
                        {page !== formActive.sections.length - 1 &&
                          conditionPagination !== "submit_form_crm" && (
                            <CustomButton onClick={() => form.submit()}>
                              {t("form.next")}
                            </CustomButton>
                          )}
                        {page !== 0 && (
                          <CustomButton onClick={handleBack}>
                            {t("form.back")}
                          </CustomButton>
                        )}
                        <CustomButton onClick={() => handleCancel()}>
                          {formActive.form_cancel}
                        </CustomButton>
                      </ButtonWrap>
                    ) : (
                      <ButtonWrap>
                        <Button
                          disabled
                          style={{ width: "100%", borderRadius: "10px" }}
                          size="large"
                        >
                          You don't have the authority to grade.
                        </Button>
                      </ButtonWrap>
                    )}
                  </Content>
                ),
              };
            })}
          />
        )}
      </CustomSpin>
    </WrapperForm>
  );
};

export default withTranslation()(memo(FormQM));

const WrapperForm = styled.div`
  flex: 5;
  padding: 0;
  border-left: 1px solid #ececec;
  margin-top: 0.75rem;
  background: #ffffff;
  margin-right: 0.75rem;
  max-height: calc(100vh - 80px - 5rem - 55px) !important;
  overflow-y: auto;
  overflow-x: hidden;
  transition: all ease-in-out 0.3s;

  &.collapse {
    max-height: calc(100vh - 80px - 5rem - 5px) !important;
    transition: all ease-in-out 0.3s;
  }

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 350px;
  }

  &::-webkit-scrollbar {
    display: none;
  }
`;
const Content = styled.div``;

const CustomTabs = styled(Tabs)`
  background-color: #fff;
  .ant-tabs-tab-active {
    border-top: 1px solid ${(props) => props.theme.main} !important;

    .ant-typography {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-tabs-nav {
    background-color: #fff;
    position: sticky;
    top: 0px;
    z-index: 10;
    padding-top: 10px;
    margin-bottom: 0;
  }
`;

const FromWrap = styled.div`
  max-width: 100%;
  max-height: calc(100% - 24px);
  padding: 16px 16px 12px 16px;
  border-radius: 5px;

  /* &.submit {
    width: 800px;
    padding-bottom: 2rem;
    max-width: unset;
  } */

  overflow: hidden;
  background: #fff;
  position: relative;

  .ant-input {
    border-radius: 10px;
  }

  .ant-select {
    .ant-select-selector {
      border-radius: 10px;
    }
  }

  .ant-btn {
    border-radius: 10px;
  }

  .ant-form-item {
    margin-bottom: 12px !important;
  }
  .ant-input-number,
  .ant-picker {
    width: 100%;
  }
  .ant-input-number:hover,
  .ant-input-number:focus,
  .ant-input-number-focused,
  .ant-picker-focused,
  .ant-picker:hover,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-input:focus,
  .ant-input-focused,
  .ant-input:hover,
  .ant-slider-handle,
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after,
  .ant-radio-input:focus + .ant-radio-inner {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none !important;
  }
  .ant-slider-track,
  .ant-slider:hover .ant-slider-track,
  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
  .ant-form-item-label {
    label {
      font-size: 16px;
      font-family: var(--roboto-500);
      height: unset;
    }
  }
`;

const IntroWrap = styled.div``;
const Name = styled.div`
  font-size: 18px;
  font-weight: 600;
  text-align: center;
  margin-bottom: 8px;
`;

const Description = styled.div`
  text-align: center;
  margin-bottom: 8px;
  font-size: 16px;
`;

const ButtonWrap = styled.div`
  width: 100%;
  padding: 0 1rem 10px 1rem;
`;
const CustomButton = styled(Button)`
  width: 100%;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 5px;
  font-size: 16px;
  cursor: pointer;
  background: #fff;
  margin: 12px 0;
  :hover {
    background: ${(props) => props.theme.main} !important;
    color: #fff;
    border: ${(props) => props.theme.main};
  }

  :nth-child(1) {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;

const Result = styled.div`
  flex: 1;

  .tilte {
    font-size: 16px;
    font-weight: 500;
    line-height: 18.75px;
    color: #fff;
    margin-bottom: 8px;
  }

  .result {
    font-size: 20px;
    font-weight: 500;
    line-height: 18.75px;
    color: #fff;

    display: flex;
    gap: 10px;
    align-items: center;

    .ratings {
      line-height: 18.75px;
      font-size: 18px !important;
      color: #fff;
      padding: 3px 8px;
      border-radius: 4px;
    }
  }

  .number {
    line-height: 18.75px;
    font-size: 20px;
    color: #fff;
  }
`;

const WrapperResult = styled.div`
  width: 100%;
  height: 90px;
  margin-top: 1rem;
  padding: 1rem 1.5rem;
  display: flex;
  flex-direction: row;
  align-items: center;

  border-radius: 8px;
  background-repeat: no-repeat;

  background-color: ${(props) => props.theme.main};
  /* img {
    width: 100%;
    object-fit: cover;
    margin-bottom: 16px;
  } */
  /* background-image: url("assets/icons/qualityManagement/BGScore.png"); */
`;

const EmptyCustom = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
  font-size: 16px;
  padding: 0 1rem;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
    text-align: center;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const ButtonCustom = styled(Button)`
  margin-left: 1.5rem;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 32px;
  color: #fff !important;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }

  span {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const CustomSelect = styled(Select)`
  width: 100%;
`;
