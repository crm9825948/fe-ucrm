import _ from "lodash";
import styled from "styled-components";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Breadcrumb, Tooltip } from "antd";
import { useTranslation, withTranslation } from "react-i18next";
import {
  ArrowUpOutlined,
  ArrowDownOutlined,
  RightOutlined,
  LeftOutlined,
} from "@ant-design/icons";
import FormQM from "./FormQM";
import SidebarQM from "./SidebarQM";
import ContentQM from "./ContentQM";
import QuickFilter from "./QuickFilter";
import CustomView from "pages/Objects/customView/customView";
import EmptyQM from "assets/icons/qualityManagement/Empty_QM.png";
import TimeRangeQuickFilter from "components/TimeRangeFilter/TimeRangeQuickFilter";

import {
  getDetailSettings,
  getListAllOptionsQuickFilter,
  getListOptionsQuickFilter,
  getListUser,
  loadListObjectFieldQM,
  resetListDataFilter,
  resetTotalRecord,
} from "redux/slices/QualityManagement";
import { loadListObjectField } from "redux/slices/objects";
import { changeTitlePage } from "redux/slices/authenticated";

const QualityManagement = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { customViewId } = useParams();
  const [updateDefault, setUpdateDefault] = useState(false);
  const [create, setCreate] = useState(false);
  const [firstLoad, setFirstLoad] = useState(true);
  const [objectFields, setObjectFields] = useState();
  const [valueSelected, setValueSelected] = useState();
  const [typeSelected, setTypeSelected] = useState("");
  const [currentPageChat, setCurrentPageChat] = useState(1);
  const [dataInteraction, setDataInteraction] = useState();
  const [keyFormSelected, setKeyFormSelected] = useState("");
  const [openCustomView, setOpenCustomView] = useState(false);
  const [collapseSidebar, setCollapseSidebar] = useState(false);
  const [listFieldsMain, setListFieldsMain] = useState([]);
  const [listMainFields, setListMainFields] = useState([]);
  const [listMainFieldsQM, setListMainFieldsQM] = useState([]);
  const [collapse, setCollapse] = useState(
    localStorage.getItem("collapseQM") === "true" ||
      localStorage.getItem("collapseQM") === null
  );

  const { userDetail } = useSelector((state) => state.userReducer);
  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { listObjectField, customView } = useSelector(
    (state) => state.objectsReducer
  );
  const { checkPermissionQMView } = useSelector(
    (state) => state.authenticatedReducer
  );
  const [timeRangeFilter, $timeRangeFilter] = useState({
    filter_field_id: "modify_time",
    field_type: "datetime-local",
    filter_type: "all-time",
    amount_of_time: null,
    start_time: null,
    end_time: null,
  });
  const {
    listOptionsQuickFilter,
    listDataFilter,
    loadingListOptions,
    listLogScoring,
    detailSettings,
    listDetailState,
    interactionDetail,
    Emails,
    urlRecord,
    listObjectFieldQM,
    chat,
    listChatIC,
  } = useSelector((state) => state.qualityManagementReducer);

  const handleCustomViewName = (id) => {
    if (id === "default-view") {
      return "Default";
    }
    let name =
      customView &&
      customView?.custom_views &&
      customView?.custom_views?.find((item) => item?._id === id);
    return name && name?.view_name;
  };

  useEffect(() => {
    if (customView && _.get(customView, "custom_views") && firstLoad) {
      _.get(customView, "custom_views", []).forEach((item) => {
        if (_.get(item, "is_default")) {
          navigate(`/quality-management/${_.get(item, "_id")}`);
          setFirstLoad(false);
        }
      });
    }
  }, [customView, navigate, firstLoad]);

  useEffect(() => {
    if (customView && _.get(customView, "custom_views") && updateDefault) {
      if (
        _.get(customView, "custom_views", []).every((item) => {
          return !_.get(item, "is_default");
        })
      ) {
        navigate(`/quality-management/default-view`);
        setUpdateDefault(false);
      }
    }
  }, [customView, navigate, updateDefault]);

  useEffect(() => {
    if (_.get(interactionConfigState, "object_id", undefined) !== undefined) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: _.get(interactionConfigState, "object_id", "undefined"),
          show_meta_fields: true,
        })
      );
    }
  }, [dispatch, interactionConfigState]);

  useEffect(() => {
    dispatch(changeTitlePage(t("QualityManagement.QualityManagement")));
  }, [dispatch, t]);

  useEffect(() => {
    dispatch(getListOptionsQuickFilter());
    dispatch(getListUser());
    dispatch(
      loadListObjectFieldQM({
        api_version: "2",
        object_id: "obj_crm_qm_interaction_00001",
        show_meta_fields: true,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (interactionConfigState) {
      setObjectFields({
        idIn: _.get(
          interactionConfigState,
          "field_mapping.incoming_type",
          undefined
        ),
        idOut: _.get(
          interactionConfigState,
          "field_mapping.outgoing_type",
          undefined
        ),
        idType: _.get(interactionConfigState, "field_mapping.type", undefined),
        channelType: _.get(
          interactionConfigState,
          "field_mapping.channel_type",
          undefined
        ),
      });
    }
  }, [interactionConfigState]);

  useEffect(() => {
    if (_.get(interactionDetail, "data_mapping", [])?.length > 0) {
      setDataInteraction({
        ...interactionDetail,
        data_mapping: _.reduce(
          _.get(interactionDetail, "data_mapping", []),
          function (result, item) {
            for (var key in item) {
              result[key] = item[key];
            }
            return result;
          },
          {}
        ),
      });
    }
  }, [interactionDetail]);

  useEffect(() => {
    if (valueSelected) {
      if (_.get(objectFields, "idType"))
        setTypeSelected(valueSelected[objectFields?.idType]?.value || "");
    }
  }, [valueSelected, objectFields]);

  useEffect(() => {
    dispatch(
      getDetailSettings({
        tenant_id: userDetail?.tenant_id,
      })
    );
  }, [dispatch, userDetail?.tenant_id]);

  useEffect(() => {
    dispatch(resetTotalRecord());
    dispatch(resetListDataFilter());
    dispatch(getListAllOptionsQuickFilter());
  }, [dispatch]);

  useEffect(() => {
    /* eslint-disable-next-line */
    listObjectFieldQM?.map((item) => {
      if (Object.keys(item)[0] === "main_object") {
        setCreate(item[Object.keys(item)[0]]?.create_permission);
      }
    });
  }, [listObjectFieldQM]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  is_editor: _.get(field, "is_editor", false),
                });
              }
            });
          });
        }
      }
    });

    setListMainFields(
      tempOptionsFields.map((item) => _.get(item, "value", ""))
    );
  }, [listObjectField]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectFieldQM.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                  is_editor: _.get(field, "is_editor", false),
                });
              }
            });
          });
        }
      }
    });

    setListMainFieldsQM(
      tempOptionsFields.map((item) => _.get(item, "value", ""))
    );
  }, [listObjectFieldQM]);

  return checkPermissionQMView ? (
    <Wrapper>
      {interactionConfigState?.object_id && (
        <CustomView
          visible={openCustomView}
          setVisible={setOpenCustomView}
          objectId={_.get(interactionConfigState, "object_id") || ""}
          reload={() => {
            setUpdateDefault(true);
          }}
          mode="QM View"
        />
      )}
      {Object.keys(interactionConfigState)?.length > 0 &&
        detailSettings?.active && (
          <div style={{ position: "relative" }}>
            <WrapBreadcrumb>
              <Breadcrumb></Breadcrumb>
              <WrapperAction>
                <CustomDisplayView>
                  Hiển thị:
                  <span
                    onClick={() => setOpenCustomView(true)}
                    style={{ cursor: "pointer" }}
                  >
                    {handleCustomViewName(customViewId)}
                  </span>
                </CustomDisplayView>
                <WrapperTimeRange>
                  Time Range:
                  <TimeRangeQuickFilter
                    timeRangeFilter={timeRangeFilter}
                    $timeRangeFilter={$timeRangeFilter}
                    listObjectField={listObjectField}
                    listFieldsMain={listFieldsMain}
                    setListFieldsMain={setListFieldsMain}
                  />
                </WrapperTimeRange>
              </WrapperAction>
            </WrapBreadcrumb>
            <WrapperContent className={collapse ? "collapse" : ""}>
              <CustomFull
                onClick={() => {
                  localStorage.setItem("collapseQM", !collapse);
                  setCollapse((pre) => !pre);
                }}
                className={collapse ? "collapse" : ""}
              >
                {collapse ? <ArrowDownOutlined /> : <ArrowUpOutlined />}
              </CustomFull>

              {checkPermissionQMView && listDataFilter?.length > 0 && (
                <Tooltip
                  placement="right"
                  title={"Collapse"}
                  arrow={true}
                  mouseEnterDelay={0.5}
                >
                  <CustomSideBar
                    onClick={() => setCollapseSidebar((pre) => !pre)}
                    className={collapseSidebar ? "collapse" : ""}
                  >
                    {collapseSidebar ? (
                      <RightOutlined
                        style={{ fontSize: "12px", fontWeight: 600 }}
                      />
                    ) : (
                      <LeftOutlined
                        style={{ fontSize: "12px", fontWeight: 600 }}
                      />
                    )}
                  </CustomSideBar>
                </Tooltip>
              )}

              <QuickFilter
                listOption={listOptionsQuickFilter}
                timeRangeFilter={timeRangeFilter}
                LoadingGetData={loadingListOptions}
                collapse={collapse}
              />

              {listDataFilter?.length > 0 ? (
                <WrapperBody className={collapse ? "collapse" : ""}>
                  <SidebarQM
                    collapse={collapse}
                    typeSelected={typeSelected}
                    valueSelected={valueSelected}
                    listDataFilter={listDataFilter}
                    collapseSidebar={collapseSidebar}
                    currentPageChat={currentPageChat}
                    dataInteraction={dataInteraction}
                    timeRangeFilter={timeRangeFilter}
                    setValueSelected={setValueSelected}
                    setKeyFormSelected={setKeyFormSelected}
                    setCurrentPageChat={setCurrentPageChat}
                    idIn={_.get(objectFields, "idIn", "")}
                    idOut={_.get(objectFields, "idOut", "")}
                    idType={_.get(objectFields, "idType", "")}
                    channelType={_.get(objectFields, "channelType", "")}
                    listFieldsMain={
                      listFieldsMain?.filter(
                        (item) =>
                          item?.type ===
                          ("datetime-local" || item?.type === "date")
                      ) || []
                    }
                  />

                  <ContentQM
                    chat={chat}
                    Emails={Emails}
                    urlRecord={urlRecord}
                    listChatIC={listChatIC}
                    typeSelected={typeSelected}
                    listLogScoring={listLogScoring}
                    listMainFields={listMainFields}
                    dataInteraction={dataInteraction}
                    listMainFieldsQM={listMainFieldsQM}
                    setCurrentPageChat={setCurrentPageChat}
                    idInteraction={_.get(valueSelected, "_id", "")}
                    dataDetailState={Object.entries(
                      _.get(listDetailState, "eventCalculations", [])
                    )}
                  />

                  <FormQM
                    create={create}
                    collapse={collapse}
                    detailSettings={detailSettings}
                    keyFormSelected={keyFormSelected}
                    dataInteraction={dataInteraction}
                    idInteraction={_.get(valueSelected, "_id", "")}
                  />
                </WrapperBody>
              ) : (
                <Empty>
                  <img src={EmptyQM} alt="empty" />
                  <p>No Interaction</p>
                </Empty>
              )}
            </WrapperContent>
          </div>
        )}
    </Wrapper>
  ) : (
    <Empty>
      <img src={EmptyQM} alt="empty" />
      <p>No permission</p>
    </Empty>
  );
};

export default withTranslation()(QualityManagement);

const Wrapper = styled.div`
  padding: 1rem;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const CustomFull = styled.div`
  opacity: 0.7;
  position: absolute;
  top: -22px;
  left: 50%;
  transform: translateX(-50%);
  width: 40px;
  height: 22px;
  background-image: linear-gradient(#ccc, #fff);
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px 5px 0 0;
  cursor: pointer;
  transition: all ease-in-out 0.3s;

  &.collapse {
    border-radius: 0 0 5px 5px;
    transition: all ease-in-out 0.3s;
    top: -20px;
    background-image: linear-gradient(#fff, #ccc);
  }
`;

const CustomSideBar = styled.div`
  opacity: 0.7;
  position: absolute;
  top: 50%;
  left: 340px;
  transform: translateY(-50%);
  width: 22px;
  height: 22px;
  border-radius: 50%;
  background-color: #fff;
  border: 1px solid #ccc;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: all ease-in-out 0.3s;
  z-index: 100;

  &:hover {
    transition: all ease-in-out 0.3s;
    opacity: 1;
    background-color: ${(props) => props.theme.main};
    border: none;

    span {
      color: #fff;
      transition: all ease-in-out 0.3s;
    }
  }

  &.collapse {
    transition: all ease-in-out 0.3s;
    left: 94px;
  }

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    left: 310px;
  }
`;

const WrapperContent = styled.div`
  width: 100%;
  position: absolute;
  top: 50px;
  left: 0;
  transition: all ease-in-out 0.3s;

  &.collapse {
    top: 0;
    transition: all ease-in-out 0.3s;
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 1rem;
`;

const WrapperAction = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 0.5rem;
`;

const WrapperBody = styled.div`
  width: 100%;
  height: calc(100vh - 80px - 4rem - 65px);
  background-color: #ebeef0;
  display: flex;
  transition: all ease-in-out 0.3s;

  &.collapse {
    height: calc(100vh - 80px - 4rem - 15px);
    transition: all ease-in-out 0.3s;
  }
`;

const WrapperTimeRange = styled.div`
  font-size: 16px;
  line-height: 22px;
  display: flex;
  align-items: center;
  gap: 0.5rem;
`;

const CustomDisplayView = styled.div`
  font-size: 16px;
  line-height: 22px;
  display: flex;
  align-items: center;
  margin-right: 1rem;

  span {
    margin-left: 6px;
    text-decoration: underline;
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
