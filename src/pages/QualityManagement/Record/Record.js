import styled from "styled-components";
import { withTranslation } from "react-i18next";
import { memo } from "react";
import IconRecord from "assets/icons/qualityManagement/voice-square.png";
import IconPlay from "assets/icons/qualityManagement/play-record.png";
import { Button } from "antd";
import _ from "lodash";

const Record = ({ urlRecord }) => {
  const convertSecondsToMinutesAndSeconds = (seconds) => {
    let minutes = Math.floor(seconds / 60);
    let remainingSeconds = seconds % 60;
    if (minutes > 0)
      return minutes + " minutes " + remainingSeconds + " seconds";
    return remainingSeconds + " seconds";
  };

  return (
    <Wrapper>
      <a
        href={_.get(urlRecord, "link_record")}
        target="_blank"
        rel="noreferrer"
      >
        <WrapperRecord>
          <WrapperLeft>
            <CustomIMG src={IconRecord} alt="" />
            <WrapperContent>
              <Title>Voice message interaction</Title>
              <WrapperText>
                <span className="customText">
                  {convertSecondsToMinutesAndSeconds(
                    _.get(urlRecord, "total_duration", 0)
                  )}
                </span>
              </WrapperText>
            </WrapperContent>
          </WrapperLeft>
          <WrapperRight>
            <CustomBtn>
              <ImgPlay src={IconPlay} alt="" />
            </CustomBtn>
          </WrapperRight>
        </WrapperRecord>
      </a>
    </Wrapper>
  );
};

export default withTranslation()(memo(Record));

const Wrapper = styled.div`
  padding: 1rem 0;
  border-bottom: 1px solid #ececec;
`;

const WrapperRecord = styled.div`
  padding: 0.75rem;
  width: 100%;
  height: auto;
  cursor: pointer;

  background: rgba(32, 162, 162, 0.05);
  border-radius: 12px;
  transition: all ease-in-out 0.2s;

  display: flex;
  align-items: center;
  justify-content: space-between;

  &:hover {
    background: #f1f5f5;
    box-shadow: 8px 5px 8px rgba(0, 0, 0, 0.07);
    transition: all ease-in-out 0.2s;
  }
`;

const WrapperLeft = styled.div`
  display: flex;
  gap: 10px;
`;
const WrapperRight = styled.div``;

const CustomIMG = styled.img`
  object-fit: contain;
`;

const WrapperContent = styled.div``;

const WrapperText = styled.div`
  font-size: 14px;

  .customText {
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;

    color: #6b6b6b;
  }
`;

const Title = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  margin-bottom: 4px;
`;

const CustomBtn = styled(Button)`
  width: 40px;
  height: 40px;

  background: #ffffff;
  border: 1px solid #f4f4f4;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 50%;

  display: flex;
  align-items: center;
  justify-content: center;
`;

const ImgPlay = styled.img``;
