import styled from "styled-components";
import { memo, useState } from "react";
import { Button, Modal, Slider } from "antd";
import { withTranslation } from "react-i18next";
import IconMic from "assets/icons/qualityManagement/Icon_Modal.png";
import IconRePlay from "assets/icons/qualityManagement/Icon_Replay.png";
import IconPlay from "assets/icons/qualityManagement/play-record.png";
import IconDownload from "assets/icons/qualityManagement/Icon_Download.png";
import AvatarIMG from "assets/images/defaultBrand.png";
import IconPhone from "assets/icons/qualityManagement/Icon_Phone.png";

const ModalRecord = ({ isModalOpen, setIsModalOpen }) => {
  const [isPlay, setIsPlay] = useState(false);

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <WrapperModalRecord>
      <CustomModal
        width={600}
        title="Record"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={[
          <ButtonClose key="back" onClick={handleCancel}>
            Close
          </ButtonClose>,
        ]}
      >
        <WrapperMic className={isPlay ? "play" : ""}>
          <CustomImgMic src={IconMic} alt="" />
        </WrapperMic>
        <WrapperSlider>
          <Min>00:00</Min>
          <CustomSlider
            tooltip={{ formatter: null }}
            defaultValue={30}
            min={0}
            max={100}
          />
          <Max>05:00</Max>
        </WrapperSlider>
        <WrapperControl>
          <WrapperIcon>
            <CustomRePlay src={IconRePlay} alt="" />
            <CustomButtonPlay onClick={() => setIsPlay((pre) => !pre)}>
              <CustomPlay src={IconPlay} alt="" />
            </CustomButtonPlay>
            <CustomDownload src={IconDownload} alt="" />
          </WrapperIcon>
        </WrapperControl>
        <WrapperInfo>
          <Avatar src={AvatarIMG} alt="" />
          <WrapperData>
            <Name>Nguyen Minh Triet</Name>
            <WrapperPhone>
              <Phone src={IconPhone} alt="" />
              <NumberPhone>0978 258 369</NumberPhone>
            </WrapperPhone>
          </WrapperData>
        </WrapperInfo>
      </CustomModal>
    </WrapperModalRecord>
  );
};

export default withTranslation()(memo(ModalRecord));

const CustomSlider = styled(Slider)`
  &:hover {
    .ant-slider-track {
      background-color: ${(props) => props.theme.darker};
    }

    .ant-slider-handle {
      border: solid 3px ${(props) => props.theme.darker};
    }
  }

  .ant-slider-track {
    background-color: ${(props) => props.theme.darker};
  }

  .ant-slider-handle {
    border: solid 3px ${(props) => props.theme.darker};
    position: relative;
    z-index: 2;

    &::before {
      content: "00:00";
      position: absolute;
      bottom: -30px;
      left: -22.5px;
      background-color: #ffffff;
      padding: 0 10px;
      z-index: 2;
    }
  }

  margin-top: 0;
  padding-bottom: 2rem;
`;

const WrapperModalRecord = styled.div``;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px 10px 0px 0px;
    .ant-modal-close {
      .ant-modal-close-x {
        width: 45px;
        height: 35px;
        line-height: 35px;
        font-weight: bold;
      }
    }
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-header {
    background: #f2f4f5;
    padding: 6px 24px;
    border-radius: 10px 10px 0px 0px;
    .ant-modal-title {
      font-weight: 600;
    }
  }

  .ant-modal-footer {
    padding: 1.5rem;
  }
`;

const ButtonClose = styled(Button)`
  padding: 5px 16px;
  width: 121px;
  height: 32px;

  background: #ffffff;

  border: 1px solid #d9d9d9;

  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;

  &:hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const WrapperMic = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  position: relative;
  transition: all ease-in-out 0.3s;

  &.play::before {
    transition: all ease-in-out 0.3s;
    content: "";
    background-color: rgba(236, 244, 253, 0.35);
    width: 231.64px;
    height: 231.64px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 5;
    border-radius: 50%;
    animation: mymove 0.7s infinite;
  }

  @keyframes mymove {
    from {
      width: 50px;
      height: 50px;
    }
    to {
      width: 200px;
      height: 200px;
    }
  }
`;

const CustomImgMic = styled.img`
  width: 231.64px;
  height: 231.64px;
  object-fit: contain;
`;

const WrapperSlider = styled.div`
  padding: 0 1.5rem;
  position: relative;
`;

const Min = styled.span`
  position: absolute;
  left: 1rem;
  bottom: -5px;
`;

const Max = styled.span`
  position: absolute;
  right: 1rem;
  bottom: -5px;
`;

const WrapperControl = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-top: 0.5rem;
  padding-bottom: 1.5rem;
`;

const WrapperIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 40px;
`;

const CustomRePlay = styled.img`
  cursor: pointer;
`;

const CustomButtonPlay = styled(Button)`
  cursor: pointer;
  object-fit: contain;
  width: 52px;
  height: 52px;
  border-radius: 50%;
  background: #ffffff;
  border: 1px solid #f4f4f4;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  display: flex;
  align-items: center;
  justify-content: center;
`;

const CustomPlay = styled.img``;

const CustomDownload = styled.img`
  cursor: pointer;
`;

const WrapperInfo = styled.div`
  padding: 0 1.5rem 1rem 1.5rem;
  display: flex;
  gap: 12px;
`;

const Avatar = styled.img`
  width: 48px;
  height: 48px;
  border-radius: 50%;
  object-fit: cover;

  border: 1px solid #ececec;
`;

const WrapperData = styled.div``;

const Name = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #2c2c2c;
  margin-bottom: 4px;
`;
const WrapperPhone = styled.div`
  display: flex;
  gap: 6px;
  align-items: center;
`;

const Phone = styled.img`
  height: 14.48px;
  width: 14.48px;
`;

const NumberPhone = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 16px;
  line-height: 19px;

  color: #6b6b6b;
`;
