import _ from "lodash";
import { memo, useCallback } from "react";
import parse from "html-react-parser";
import styled from "styled-components";
import { withTranslation } from "react-i18next";
import { Collapse, Tooltip } from "antd";
import { optionsParse } from "util/staticData";
import AvatarImg from "assets/images/header/avatar.png";
import { useSelector } from "react-redux";
import { BE_URL } from "constants/constants";

const { Panel } = Collapse;

const ConversationLayout = ({ data, type, dataInteraction }) => {
  const { listUser } = useSelector((state) => state.qualityManagementReducer);

  const checkShow = (value) => {
    if (value !== undefined && value !== null) {
      return true;
    }
    return false;
  };

  const getAvatar = useCallback(
    (id) => {
      let temp = listUser?.filter((item) => _.get(item, "_id") === id);

      return _.get(temp, "[0].avatar_config.url", "") !== ""
        ? BE_URL + _.get(temp, "[0].avatar_config.url", "")
        : AvatarImg;
    },
    [listUser]
  );

  const getName = useCallback(
    (id) => {
      return _.get(
        listUser?.filter((item) => item._id === id)[0],
        "Full_name",
        ""
      );
    },
    [listUser]
  );

  return (
    <Wrapper>
      <CustomCollapse defaultActiveKey={"1"}>
        <Panel header={type === 0 ? "SMS" : "Email"} key="1">
          <WrapperBG>
            {type === 0 ? (
              <>
                <WrapperRow isMe={true}>
                  <Tooltip
                    placement="top"
                    title={getName(_.get(data, "created_by", ""))}
                    arrow={true}
                  >
                    <Avatar
                      src={getAvatar(_.get(data, "created_by", ""))}
                      alt=""
                    />
                  </Tooltip>
                  <WrapperMess className={"isMe"} type={type} isMe={true}>
                    <Sender style={{ marginTop: "0px" }}>
                      From: <span>{_.get(data, "from", "")}</span>
                    </Sender>
                    <Sender style={{ marginTop: "0px", paddingBottom: "4px" }}>
                      To: <span>{_.get(data, "to", "")}</span>
                    </Sender>
                    <WrapperContent type={type} isMe={true} className="SMS">
                      {_.get(data, "text", "")}
                    </WrapperContent>
                  </WrapperMess>
                  <Time isMe={true}> {_.get(data, "created_date", "")}</Time>
                </WrapperRow>
              </>
            ) : (
              <WrapperRow>
                <WrapperEmail>
                  <TitleEmail>
                    {_.get(data, "subject", "")}
                    <TimeEmail isMe={true}>
                      {_.get(data, "created_date", "")}
                    </TimeEmail>
                  </TitleEmail>
                  <div
                    style={{
                      borderBottom: "1px solid #e4e8eb",
                      paddingBottom: "4px",
                    }}
                  >
                    <Sender style={{ marginTop: "5px" }}>
                      From: <span>{_.get(data, "from", "")}</span>
                    </Sender>
                    <Sender style={{ marginTop: "0px" }}>
                      To:{" "}
                      <span>
                        {_.isArray(_.get(data, "to", ""))
                          ? _.get(data, "to", "").join(", ")
                          : _.get(data, "to", "").replace(/,/g, ", ")}
                      </span>
                    </Sender>
                    {checkShow(
                      _.get(
                        dataInteraction,
                        `data_interaction.${dataInteraction?.data_mapping?.fld_ccemail_}.value`,
                        undefined
                      )
                    ) && (
                      <Sender style={{ marginTop: "0px" }}>
                        CC:{" "}
                        <span>
                          {_.get(
                            dataInteraction,
                            `data_interaction.${dataInteraction?.data_mapping?.fld_ccemail_}.value`,
                            ""
                          ).replace(/,/g, ", ")}
                        </span>
                      </Sender>
                    )}
                    {checkShow(
                      _.get(
                        dataInteraction,
                        `data_interaction.${dataInteraction?.data_mapping?.fld_bccemail_}.value`,
                        undefined
                      )
                    ) && (
                      <Sender style={{ marginTop: "0px" }}>
                        BCC:{" "}
                        <span>
                          {_.get(
                            dataInteraction,
                            `data_interaction.${dataInteraction?.data_mapping?.fld_bccemail_}.value`,
                            ""
                          ).replace(/,/g, ", ")}
                        </span>
                      </Sender>
                    )}
                  </div>

                  <WrapperContent type={type} isMe={true}>
                    {parse(
                      _.replace(
                        _.get(data, "body", ""),
                        `ref="noopener noreferrer"`,
                        ""
                      ),
                      optionsParse
                    )}
                  </WrapperContent>
                </WrapperEmail>
              </WrapperRow>
            )}
          </WrapperBG>
        </Panel>
      </CustomCollapse>
    </Wrapper>
  );
};

export default withTranslation()(memo(ConversationLayout));

const Wrapper = styled.div`
  padding: 0.5rem 0;
  border-bottom: 1px solid #ececec;

  width: 100%;
`;

const WrapperBG = styled.div`
  padding: 1rem;
  width: 100%;
  background-color: #f4fafa;
  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 1rem;
`;

const WrapperRow = styled.div`
  display: flex;
  align-items: flex-end;
  gap: 15px;
  position: relative;
  overflow-x: auto;

  ${(props) =>
    props.isMe ? "flex-direction: row-reverse;" : "flex-direction: row;"}

  ${(props) => props.isMe && "padding-bottom: 20px;"}
`;

const Time = styled.p`
  margin: 0;
  content: "text";
  position: absolute;
  bottom: 0px;
  ${(props) => (props.isMe ? "right: 55px;" : "left: 55px;")}

  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: #8c8c8c;
`;

const TimeEmail = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: #8c8c8c;
  margin-top: 4px;
`;

const Avatar = styled.img`
  width: 40px;
  height: 40px;
  object-fit: cover;
  border-radius: 50%;
`;

const WrapperMess = styled.div`
  padding: 1rem;

  ${(props) =>
    props.type === "chat" && props.isMe
      ? "background-color: #D3E1F4;"
      : "background-color: #ffffff;"}

  ${(props) =>
    props.type === "chat" && props.isMe
      ? "border: none;"
      : "border: 1px solid #ececec;"}

  border-radius: 20px 20px 20px 0px;
  /* max-width: 640px; */
  /* width: 95%; */

  &.isMe {
    border-radius: 20px 20px 0px 20px;
  }
`;

const WrapperEmail = styled.div`
  padding: 1rem;
  width: 100%;
  overflow: auto;

  ${(props) =>
    props.type === "chat" && props.isMe
      ? "background-color: #D3E1F4;"
      : "background-color: #ffffff;"}

  ${(props) =>
    props.type === "chat" && props.isMe
      ? "border: none;"
      : "border: 1px solid #ececec;"}
`;

const WrapperContent = styled.div`
  font-weight: 400;
  line-height: 25px;

  &.SMS {
    padding-top: 4px;
    border-top: 1px solid #e4e8eb;
  }

  ${(props) =>
    props.type === "chat" ? "font-size: 17px;" : "font-size: 14px;"}
  ${(props) => props.type === "email" && "margin-top: 12px;"}

  color: #464545;
  word-break: break-all;
`;

const TitleEmail = styled.div`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #252424;
  padding-bottom: 4px;
  border-bottom: 1px solid #e4e8eb;
`;

const Sender = styled.p`
  margin: 0;
  margin-top: 20px;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;
  color: #8c8c8c;

  span {
    font-weight: 500;
    font-size: 14px;
    line-height: 22px;
    color: #252424;
    word-wrap: break-all;
  }
`;

const CustomCollapse = styled(Collapse)`
  border: none;
  .ant-collapse-item {
    border: none;
    .ant-collapse-header {
      display: flex;
      flex-direction: row-reverse;
      background-color: #ffffff;
      padding: 0.5rem 0;

      .ant-collapse-header-text {
        font-style: normal;
        font-weight: 500;
        font-size: 18px;
        line-height: 26px;

        color: #2c2c2c;
      }
    }

    .ant-collapse-content {
      border: none;

      .ant-collapse-content-box {
        padding: 0;
      }
    }
  }
`;
