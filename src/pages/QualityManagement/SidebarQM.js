import _ from "lodash";
import { memo, useEffect, useRef, useState } from "react";
import styled from "styled-components";
import { withTranslation } from "react-i18next";
import InfiniteScroll from "react-infinite-scroll-component";

import EmailInListIcon from "assets/icons/qualityManagement/MailIn_List.png";
import EmailOutListIcon from "assets/icons/qualityManagement/MailOut_List.png";
import MessInListIcon from "assets/icons/qualityManagement/MessIn_List.png";
import MessOutListIcon from "assets/icons/qualityManagement/MessOut_List.png";
import CallInListIcon from "assets/icons/qualityManagement/PhoneIn_List.png";
import CallOutListIcon from "assets/icons/qualityManagement/PhoneOut_List.png";
import VBBIListIconIn from "assets/icons/qualityManagement/VBBIIn.png";
import VBBIListIconOut from "assets/icons/qualityManagement/VBBIOut.png";
import EmptyIcon from "assets/icons/qualityManagement/IconUndefined.png";

import Viber from "assets/icons/qualityManagement/viber.svg";
import Webchat from "assets/icons/qualityManagement/chat.svg";
import Zalo from "assets/icons/qualityManagement/zalo.svg";
import EmailIC from "assets/icons/qualityManagement/mail.svg";
import Facebook from "assets/icons/qualityManagement/Facebook.svg";
import FacebookMessage from "assets/icons/qualityManagement/Mess.svg";
import FacebookComment from "assets/icons/timeline/facebook_comment.svg";
import Instagram from "assets/icons/qualityManagement/Intagram.svg";
import InstagramMessage from "assets/icons/qualityManagement/Intagram_mess.svg";
import InstagramComment from "assets/icons/timeline/instagram_comment.svg";
import Telegram from "assets/icons/qualityManagement/Telegram.svg";
import ThirdPartyChannel from "assets/icons/qualityManagement/3rd.svg";
import Whatsapp from "assets/icons/qualityManagement/Whatsapp.svg";

import { useDispatch, useSelector } from "react-redux";
import {
  getInteractionDetail,
  getListDetailState,
  getEmails,
  getListScoring,
  getMoreListDataFilter,
  setTypeSelected,
  getURLRecord,
  getChat,
  getListChatIC,
  setListChatIC,
  setLoadedAllChatIC,
} from "redux/slices/QualityManagement";
import { COLOR_INTERACTION_TYPE, COLOR_LIST_FILTER } from "util/staticData";
import { Spin, Tooltip } from "antd";

const TYPE_INTERACTION = {
  0: "Call",
  1: "Email",
  2: "SMS",
  3: "VBEE",
  4: "IC",
};

const SidebarQM = ({
  listDataFilter,
  idType,
  idIn,
  idOut,
  channelType,
  setValueSelected,
  setKeyFormSelected,
  timeRangeFilter,
  typeSelected,
  dataInteraction,
  collapse,
  listFieldsMain,
  collapseSidebar,
  valueSelected,
  currentPageChat,
  setCurrentPageChat,
}) => {
  const dispatch = useDispatch();
  const [isSelected, setIsSelected] = useState();
  const [typeTime, setTypeTime] = useState({});
  const elementRef = useRef(null);
  const { isSubmit } = useSelector((state) => state.formSettingReducer);
  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const [hiddenData, setHiddenData] = useState(false);

  const scrollToTopFunc = () => {
    if (elementRef?.current) {
      elementRef.current.scrollTop = 0;
    }
  };

  const {
    payloadFilter,
    loadingMore,
    loadedAllData,
    scrollToTop,
    loadingLogScoring,
    loadingInteractionDetail,
    fieldInteractionID,
    fieldsourceInteractionID,
    fieldsourceObjectID,
  } = useSelector((state) => state.qualityManagementReducer);

  const covertToTitleCase = (str) => {
    return str.replace(/\w\S*/g, function (word) {
      return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
    });
  };

  const checkID = (value) => {
    if (value !== undefined && value !== null) {
      return true;
    }
    return false;
  };

  const formatString = (str) => {
    if (str) {
      const words = str?.split("_");
      const formattedWords = words?.map(
        (word) => word?.charAt(0)?.toUpperCase() + word?.slice(1)
      );
      return formattedWords?.join(" ");
    }

    return "";
  };

  const checkTitle = (data) => {
    if (data[idIn]?.value) {
      return data[idType]?.value?.toLowerCase() !== "ic"
        ? data[idType]?.value?.toLowerCase() === "call"
          ? `${covertToTitleCase(data[idType]?.value || "")} In`
          : `${data[idType]?.value} In`
        : `${data[idType]?.value} ${formatString(data[channelType]?.value)}`;
    }

    if (data[idOut]?.value) {
      return data[idType]?.value?.toLowerCase() !== "ic"
        ? data[idType]?.value?.toLowerCase() === "call"
          ? `${covertToTitleCase(data[idType]?.value || "")} Out`
          : `${data[idType]?.value} Out`
        : `${data[idType]?.value} ${formatString(data[channelType]?.value)}`;
    }
    return "Manual";
  };

  const checkIcon = (data) => {
    if (data[idIn]?.value !== undefined) {
      const type = `${TYPE_INTERACTION[data[idIn]?.value]} In`;

      if (type === "Call In") return CallInListIcon;
      if (type === "Email In") return EmailInListIcon;
      if (type === "SMS In") return MessInListIcon;
      if (type === "VBEE In") return VBBIListIconIn;
    }

    if (data[idOut]?.value !== undefined) {
      const type = `${TYPE_INTERACTION[data[idOut]?.value]} Out`;

      if (type === "Call Out") return CallOutListIcon;
      if (type === "Email Out") return EmailOutListIcon;
      if (type === "SMS Out") return MessOutListIcon;
      if (type === "VBEE Out") return VBBIListIconOut;
    }

    if (data[idType]?.value?.toLowerCase() === "ic") {
      if (formatString(data[channelType]?.value) === "Viber") return Viber;
      if (formatString(data[channelType]?.value) === "Webchat") return Webchat;
      if (formatString(data[channelType]?.value) === "Zalo") return Zalo;
      if (formatString(data[channelType]?.value) === "Email") return EmailIC;
      if (formatString(data[channelType]?.value) === "Facebook")
        return Facebook;
      if (formatString(data[channelType]?.value) === "Facebook Message")
        return FacebookMessage;
      if (formatString(data[channelType]?.value) === "Facebook Comment")
        return FacebookComment;
      if (formatString(data[channelType]?.value) === "Instagram")
        return Instagram;
      if (formatString(data[channelType]?.value) === "Instagram Message")
        return InstagramMessage;
      if (formatString(data[channelType]?.value) === "Instagram Comment")
        return InstagramComment;
      if (formatString(data[channelType]?.value) === "Telegram")
        return Telegram;
      if (formatString(data[channelType]?.value) === "Third Party Channel")
        return ThirdPartyChannel;
      if (formatString(data[channelType]?.value) === "Whatsapp")
        return Whatsapp;
    }

    return EmptyIcon;
  };

  const checkColor = (data) => {
    if (data[idIn]?.value !== undefined) {
      const type = `${TYPE_INTERACTION[data[idIn]?.value]} In`;

      if (type === "Call In") return COLOR_LIST_FILTER.CALLIN;
      if (type === "Email In") return COLOR_LIST_FILTER.EMAILIN;
      if (type === "SMS In") return COLOR_LIST_FILTER.MESSIN;
      if (type === "VBEE In") return COLOR_LIST_FILTER.VBBI;
    }

    if (data[idOut]?.value !== undefined) {
      const type = `${TYPE_INTERACTION[data[idOut]?.value]} Out`;

      if (type === "Call Out") return COLOR_LIST_FILTER.CALLOUT;
      if (type === "Email Out") return COLOR_LIST_FILTER.EMAILOUT;
      if (type === "SMS Out") return COLOR_LIST_FILTER.MESSOUT;
      if (type === "VBEE Out") return COLOR_LIST_FILTER.VBBI;
    }

    if (data[idType]?.value?.toLowerCase() === "ic") {
      if (formatString(data[channelType]?.value) === "Viber")
        return COLOR_INTERACTION_TYPE.IC_VIDBER;
      if (formatString(data[channelType]?.value) === "Webchat")
        return COLOR_INTERACTION_TYPE.IC_WEBCHAT;
      if (formatString(data[channelType]?.value) === "Zalo")
        return COLOR_INTERACTION_TYPE.IC_ZALO;
      if (formatString(data[channelType]?.value) === "Email")
        return COLOR_INTERACTION_TYPE.IC_EMAIL;
      if (formatString(data[channelType]?.value) === "Facebook")
        return COLOR_INTERACTION_TYPE.IC_FACEBOOK;
      if (formatString(data[channelType]?.value) === "Facebook Message")
        return COLOR_INTERACTION_TYPE.IC_FACEBOOKMESSAGE;
      if (formatString(data[channelType]?.value) === "Facebook Comment")
        return COLOR_INTERACTION_TYPE.IC_FACEBOOKCOMMENT;
      if (formatString(data[channelType]?.value) === "Instagram")
        return COLOR_INTERACTION_TYPE.IC_INSTAGRAM;
      if (formatString(data[channelType]?.value) === "Instagram Message")
        return COLOR_INTERACTION_TYPE.IC_INSTAGRAMMESSAGE;
      if (formatString(data[channelType]?.value) === "Instagram Comment")
        return COLOR_INTERACTION_TYPE.IC_INSTAGRAMCOMMENT;
      if (formatString(data[channelType]?.value) === "Telegram")
        return COLOR_INTERACTION_TYPE.IC_TELEGRAM;
      if (formatString(data[channelType]?.value) === "Third Party Channel")
        return COLOR_INTERACTION_TYPE.IC_THIRDPARTYCHANNEL;
      if (formatString(data[channelType]?.value) === "Whatsapp")
        return COLOR_INTERACTION_TYPE.IC_WHATSAPP;
    }

    return "#eeeeee";
  };

  const setDefaultValue = () => {
    if (listDataFilter?.length > 0) {
      setKeyFormSelected(checkTitle(listDataFilter[0]));
      setValueSelected(listDataFilter[0]);
      setIsSelected(listDataFilter[0]?._id);
      dispatch(setTypeSelected(checkTitle(listDataFilter[0])));
      dispatch(setListChatIC([]));
      setCurrentPageChat(1);
      dispatch(setLoadedAllChatIC(false));
    }
  };

  useEffect(() => {
    if (scrollToTop) {
      scrollToTopFunc();
      setDefaultValue();
    }
    //eslint-disable-next-line
  }, [scrollToTop]);

  useEffect(() => {
    if (isSelected) {
      dispatch(
        getListScoring({
          interaction_id: isSelected,
        })
      );
    }
  }, [isSelected, dispatch, isSubmit]);

  useEffect(() => {
    if (isSelected) {
      if (typeSelected?.toLowerCase() === "call") {
        dispatch(
          getListDetailState({
            interaction_id: isSelected,
            object_id_interaction: _.get(
              interactionConfigState,
              "object_id",
              ""
            ),
          })
        );
      }
    }
  }, [isSelected, dispatch, interactionConfigState, typeSelected]);

  useEffect(() => {
    if (isSelected) {
      if (
        typeSelected?.toLowerCase() === "ic" &&
        fieldsourceInteractionID &&
        fieldsourceObjectID
      ) {
        if (
          _.get(valueSelected, `${fieldsourceObjectID}.value`) &&
          _.get(valueSelected, `${fieldsourceInteractionID}.value`, "")
        )
          dispatch(
            getListChatIC({
              object_id: _.get(
                valueSelected,
                `${fieldsourceObjectID}.value`,
                ""
              ),
              source_interaction_id: _.get(
                valueSelected,
                `${fieldsourceInteractionID}.value`,
                ""
              ),
              current_page: currentPageChat,
              limit: 20,
            })
          );
      }
    }
  }, [
    valueSelected,
    isSelected,
    dispatch,
    typeSelected,
    fieldsourceInteractionID,
    fieldsourceObjectID,
    currentPageChat,
  ]);

  useEffect(() => {
    if (isSelected) {
      dispatch(
        getInteractionDetail({
          interaction_id: isSelected,
        })
      );
    }
  }, [isSelected, dispatch]);

  useEffect(() => {
    if (
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
          undefined
        )
      ) &&
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
          undefined
        )
      )
    )
      if (
        typeSelected?.toLowerCase() === "email" &&
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_type_}.value`,
          ""
        )?.toLowerCase() === "email"
      ) {
        dispatch(
          getEmails({
            source_interaction_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
              ""
            ),
            source_object_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
              ""
            ),
          })
        );
      }
  }, [dispatch, dataInteraction, typeSelected]);

  useEffect(() => {
    if (
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
          undefined
        )
      ) &&
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
          undefined
        )
      )
    )
      if (
        typeSelected?.toLowerCase() === "sms" &&
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_type_}.value`,
          ""
        )?.toLowerCase() === "sms"
      ) {
        dispatch(
          getChat({
            source_interaction_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
              ""
            ),
            source_object_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
              ""
            ),
          })
        );
      }
  }, [dispatch, dataInteraction, typeSelected]);

  useEffect(() => {
    if (
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
          undefined
        )
      ) &&
      checkID(
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
          undefined
        )
      )
    )
      if (
        typeSelected?.toLowerCase() === "call" &&
        _.get(
          dataInteraction,
          `data_interaction.${dataInteraction?.data_mapping?.fld_type_}.value`,
          ""
        )?.toLowerCase() === "call"
      ) {
        dispatch(
          getURLRecord({
            source_interaction_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceinteractionid_}.value`,
              ""
            ),
            source_object_id: _.get(
              dataInteraction,
              `data_interaction.${dataInteraction?.data_mapping?.fld_sourceobjectid_}.value`,
              ""
            ),
          })
        );
      }
  }, [dispatch, dataInteraction, typeSelected]);

  useEffect(() => {
    if (listFieldsMain?.length > 0) {
      let temp = listFieldsMain?.find(
        (item) =>
          _.get(item, "value", "") ===
          _.get(timeRangeFilter, "filter_field_id", "")
      );
      setTypeTime(temp);
    }
  }, [listFieldsMain, timeRangeFilter]);

  useEffect(() => {
    if (!collapseSidebar) {
      setTimeout(() => {
        setHiddenData(collapseSidebar);
      }, 200);
    } else {
      setHiddenData(collapseSidebar);
    }
  }, [collapseSidebar]);

  return (
    <WrapperSidebar
      id="sidebar"
      ref={elementRef}
      className={
        collapseSidebar
          ? loadingInteractionDetail || loadingLogScoring
            ? collapse
              ? "collapse loading collapSibar"
              : "loading collapSibar"
            : collapse
            ? "collapse collapSibar"
            : "collapSibar"
          : loadingInteractionDetail || loadingLogScoring
          ? collapse
            ? "collapse loading"
            : "loading"
          : collapse
          ? "collapse"
          : ""
      }
    >
      <CustomInfiniteScroll
        dataLength={listDataFilter?.length || 0}
        scrollableTarget="sidebar"
        loader={
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              marginTop: "1rem",
            }}
          >
            <CustomSpin />
          </div>
        }
        hasMore={loadingMore && !loadedAllData}
        next={() => {
          setTimeout(() => {
            dispatch(
              getMoreListDataFilter({
                ...payloadFilter,
                last_record_id: _.get(listDataFilter.at(-1), "_id", ""),
              })
            );
          }, 500);
        }}
      >
        {_.map(listDataFilter, (item, index) => (
          <Tooltip
            placement="right"
            title={
              !hiddenData ? (
                ""
              ) : (
                <div>
                  {fieldInteractionID && (
                    <Title style={{ color: "#fff" }}>
                      ID: {_.get(item, `${fieldInteractionID}.value`)}
                    </Title>
                  )}
                  <WrapperText style={{ whiteSpace: "nowrap" }}>
                    <span className="customText" style={{ color: "#fff" }}>
                      {_.get(typeTime, "label", "")}:{" "}
                    </span>
                    <span>
                      {typeof _.get(
                        item,
                        `${_.get(typeTime, "value", "")}`,
                        ""
                      ) === "string"
                        ? _.get(item, `${_.get(typeTime, "value", "")}`, "")
                        : _.get(
                            _.get(item, `${_.get(typeTime, "value", "")}`, []),
                            "value",
                            ""
                          )}
                    </span>
                  </WrapperText>
                  <WrapperText style={{ whiteSpace: "nowrap" }}>
                    <span className="customText" style={{ color: "#fff" }}>
                      Assign to:{" "}
                    </span>
                    <span>{item?.owner}</span>
                  </WrapperText>
                </div>
              )
            }
            arrow={true}
            key={index}
          >
            <WrapperItem
              className={item?._id === isSelected ? "selected" : ""}
              onClick={() => {
                if (isSelected !== _.get(item, "_id", "")) {
                  setKeyFormSelected(checkTitle(item));
                  setValueSelected(item);
                  setIsSelected(item?._id);
                  dispatch(setTypeSelected(checkTitle(item)));
                  dispatch(setListChatIC([]));
                  setCurrentPageChat(1);
                  dispatch(setLoadedAllChatIC(false));
                }
              }}
            >
              <WrapperIcon>
                <CustomIcon bgColor={checkColor(item)}>
                  <IMGIcon src={checkIcon(item)} alt="" />
                </CustomIcon>
              </WrapperIcon>
              {!hiddenData && (
                <WrapperContent>
                  {fieldInteractionID && (
                    <Title>
                      ID: {_.get(item, `${fieldInteractionID}.value`)}
                    </Title>
                  )}
                  <WrapperText>
                    <span className="customText">
                      {_.get(typeTime, "label", "")}:{" "}
                    </span>
                    <span>
                      {typeof _.get(
                        item,
                        `${_.get(typeTime, "value", "")}`,
                        ""
                      ) === "string"
                        ? _.get(item, `${_.get(typeTime, "value", "")}`, "")
                        : _.get(
                            _.get(item, `${_.get(typeTime, "value", "")}`, []),
                            "value",
                            ""
                          )}
                    </span>
                  </WrapperText>
                  <WrapperText>
                    <span className="customText">Assign to: </span>
                    <span>{item?.owner}</span>
                  </WrapperText>
                </WrapperContent>
              )}
            </WrapperItem>
          </Tooltip>
        ))}
      </CustomInfiniteScroll>
    </WrapperSidebar>
  );
};

export default withTranslation()(memo(SidebarQM));

const CustomInfiniteScroll = styled(InfiniteScroll)`
  &::-webkit-scrollbar {
    display: none;
  }
`;

const WrapperSidebar = styled.div`
  /* flex: 4; */
  width: 350px;
  margin-top: 0.75rem;
  padding: 0 0 0.75rem 0.75rem;
  max-height: calc(100vh - 80px - 5rem - 55px) !important;
  overflow-y: auto;
  transition: all ease-in-out 0.3s;
  position: relative;

  &::-webkit-scrollbar {
    display: none;
  }

  @media screen and (max-width: 1710px) and (max-height: 960px) {
    width: 320px;
    &.collapSibar {
      width: 103px;
    }
  }

  &.collapse {
    max-height: calc(100vh - 80px - 4rem - 5px) !important;
    transition: all ease-in-out 0.3s;
  }

  &.loading {
    pointer-events: none;
    user-select: none;
  }

  &.collapSibar {
    width: 103px;
  }
`;

const WrapperItem = styled.div`
  background: #ffffff;
  border-radius: 8px;
  padding: 0.75rem 1rem;
  margin-bottom: 12px;
  display: flex;
  cursor: pointer;
  gap: 10px;
  width: calc(100% - 0.8rem);

  &.selected {
    width: 100%;
    border-radius: 0;
    box-shadow: 8px 5px 8px rgba(0, 0, 0, 0.1);
    border-left: 6px solid ${(props) => props.theme.main};
  }
`;

const WrapperIcon = styled.div``;

const WrapperContent = styled.div``;

const CustomIcon = styled.div`
  padding: 13px;
  border-radius: 8px;
  background-color: ${(props) => props.bgColor} !important;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const IMGIcon = styled.img`
  width: 20px;
  object-fit: contain;
`;

const WrapperText = styled.div`
  font-size: 14px;

  .customText {
    font-weight: 400;
    line-height: 20px;
    color: #6b6b6b;
  }
`;

const Title = styled.p`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  color: #2c2c2c;
  margin-bottom: 0;
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;
