import React, { useEffect, useRef, useState } from "react";
import moment from "moment";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import Comment from "antd/lib/comment";
import Tooltip from "antd/lib/tooltip";
import Select from "antd/lib/select";

import EditIcon from "assets/icons/comment/edit.svg";
import AvatarImg from "assets/images/header/avatar.png";
import DeleteIcon from "assets/icons/comment/delete.svg";
import NoComment from "assets/images/comment/emptyComment.png";
import IconLike from "assets/icons/consolidatedViewSettings/iconLike.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  loadComment,
  postComment,
  editComment,
  deleteComment,
  replyComment,
  loadReplyComment,
  updateShowReplyComment,
  likeComment,
  updateCurrentPageReply,
  updateShowMore,
  updateSort,
} from "redux/slices/comment";
import { loadAllUser } from "redux/slices/user";

import { BE_URL } from "constants/constants";

import NewComment from "./NewComment";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalZoom from "components/Modal/ModalZoom";
import { Spin } from "antd";

function Comments({ objectId, recordID, isArticle }) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const messagesEndRef = useRef(null);

  const { userDetail } = useSelector((state) => state.userReducer);
  const { listAllUser } = useSelector((state) => state.userReducer);

  const {
    isLoading,
    listComment,
    showReplyComment,
    currentPageReply,
    showMore,
    recordID: recordIDRedux,
  } = useSelector((state) => state.commentReducer);

  const [listUser, setListUser] = useState([]);
  const [isShowEdit, setIsShowEdit] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [dataDelete, setDataDelete] = useState({});
  const [repUser, setRepUser] = useState({
    name: "",
    id: "",
    idComment: "",
  });
  const [filter, $filter] = useState(-1);

  const [isZoom, $isZoom] = useState(false);
  const [selectedImage, $selectedImage] = useState("");
  const [dimensions, $dimensions] = useState(0);

  const scrollToBottom = () => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollTop = messagesEndRef.current.scrollHeight;
    }
  };

  const _onViewMoreParent = () => {
    setCurrentPage(currentPage + 1);
  };

  const _onReply = (index, name, by, idComment) => {
    let temp = [...showReplyComment];
    temp[index] = true;
    dispatch(updateShowReplyComment(temp));

    setRepUser({
      name: name,
      id: by,
      idComment: idComment,
    });
  };

  const _onLike = (id) => {
    dispatch(
      likeComment({
        comment_id: id,
        type: "like",
      })
    );
  };

  const _onViewMore = (index, id) => {
    dispatch(
      loadReplyComment({
        comment_id: id,
        record_per_page: 5,
        current_page: currentPageReply[id] ? currentPageReply[id] : 1,
      })
    );

    let tempPage = { ...currentPageReply };
    tempPage[id] = tempPage[id] ? tempPage[id] + 1 : 2;
    dispatch(updateCurrentPageReply(tempPage));

    let temp = [...showReplyComment];
    temp[index] = true;
    dispatch(updateShowReplyComment(temp));
  };

  const _onShowMore = (id) => {
    let tempShowMore = { ...showMore };
    tempShowMore[id] = !tempShowMore[id];
    dispatch(updateShowMore(tempShowMore));
  };

  const _onComment = (value, type, idComment, file, mentions) => {
    if (type === "reply") {
      dispatch(
        replyComment({
          comment_id: idComment,
          content: value,
          reply_to: mentions !== null ? mentions : [],
          object_id: objectId,
          record_id: recordID,
          link:
            file.length > 0 && file[0].response.data[0]
              ? file[0].response.data[0]
              : null,
        })
      );
    } else {
      dispatch(
        postComment({
          object_id: objectId,
          record_id: recordID,
          content: value,
          reply_to: mentions !== null ? mentions : [],
          link:
            file.length > 0 && file[0].response.data[0]
              ? file[0].response.data[0]
              : null,
        })
      );
      setTimeout(() => {
        scrollToBottom();
      }, 1000);
    }
  };

  const _onEditComment = (id, link) => {
    setIsShowEdit(id);
  };

  const _onUpdateComment = (value, type, id, file, mentions) => {
    dispatch(
      editComment({
        id: id,
        reply_to: mentions !== null ? mentions : [],
        content: value,
        link:
          file.length > 0 && file[0].response.data[0]
            ? file[0].response.data[0]
            : null,
      })
    );
    setIsShowEdit("");
  };

  const _onDeleteComment = (id, index, isParent) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: id });

    if (isParent) {
      let temp = [...showReplyComment];
      temp[index] = false;
      dispatch(updateShowReplyComment(temp));
    }
  };

  function replaceWithBr(content) {
    return content.replace(/\n/g, "<br />");
  }

  function replaceURLs(message) {
    if (!message) return;

    var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
    return message.replace(urlRegex, function (url) {
      var hyperlink = url;
      if (!hyperlink.match("^https?://")) {
        hyperlink = "http://" + hyperlink;
      }
      return (
        '<a href="' +
        hyperlink +
        '" target="_blank" rel="noopener noreferrer">' +
        url +
        "</a>"
      );
    });
  }
  const ListComment = ({
    children,
    name,
    avatar,
    content,
    link,
    time,
    timeEdit,
    like,
    id,
    idParent,
    totalReply,
    index,
    by,
    isParent,
    isLike,
  }) => (
    <WrapComment isOwner={userDetail._id === by} haveReply={totalReply > 0}>
      <Comment
        actions={[
          <WrapAction>
            <Action>
              <Like isLike={isLike} onClick={() => _onLike(id)}>
                {t("common.like")}
              </Like>
              <Reply onClick={() => _onReply(index, name, by, idParent)}>
                {t("common.reply")}
              </Reply>
              {isShowEdit === id && (
                <Edit onClick={() => setIsShowEdit("")}>
                  {t("common.cancel")}
                </Edit>
              )}
              <Time>
                {timeEdit
                  ? `Edited ${moment(timeEdit).fromNow()}`
                  : moment(time).fromNow()}
              </Time>
            </Action>
            {like > 0 && (
              <NumberOfLike>
                <img src={IconLike} alt="like" />
                <span>{like}</span>
              </NumberOfLike>
            )}
          </WrapAction>,
          totalReply > 0 && isParent && !showReplyComment[index] && (
            <LoadMore onClick={() => _onViewMore(index, id)}>
              {t("comments.showMore")}
            </LoadMore>
          ),
        ]}
        author={
          <WrapName>
            <Name>{name}</Name>
            {userDetail._id === by && (
              <WrapEdit className="action">
                <Tooltip title="Edit">
                  <img
                    src={EditIcon}
                    alt="edit"
                    onClick={() => _onEditComment(id, link)}
                  />
                </Tooltip>
                <Tooltip title="Delete">
                  <img
                    src={DeleteIcon}
                    alt="delete"
                    onClick={() => _onDeleteComment(id, index, isParent)}
                  />
                </Tooltip>
              </WrapEdit>
            )}
          </WrapName>
        }
        avatar={
          <WrapAvatar>
            <img src={avatar ? BE_URL + avatar : AvatarImg} alt="avatar" />
          </WrapAvatar>
        }
        content={
          <>
            {isShowEdit === id ? (
              <>
                <NewComment
                  type="edit"
                  onComment={_onUpdateComment}
                  idComment={id}
                  objectId={objectId}
                  contentEdit={content}
                  link={link}
                  listUser={listUser}
                />
              </>
            ) : (
              <Content showMore={showMore[id]} overflow={content.length > 500}>
                {link && (
                  <img
                    onClick={() => {
                      $isZoom(true);
                      $selectedImage(BE_URL + link);
                    }}
                    src={BE_URL + link}
                    alt={link}
                  />
                )}
                {content && (
                  <span>
                    {parse(
                      replaceURLs(
                        replaceWithBr(
                          content
                            .replace(/{[^}]*}/g, "")
                            .replace(/[[]+/g, "<b>")
                            .replace(/[\]]+/g, "</b>")
                        )
                      ),
                      optionsParse
                    )}
                  </span>
                )}

                {content.length > 500 && (
                  <p onClick={() => _onShowMore(id)}>
                    {showMore[id] ? "Show less" : "Show More"}
                  </p>
                )}
              </Content>
            )}
          </>
        }
      >
        {children}
      </Comment>
    </WrapComment>
  );

  useEffect(() => {
    dispatch(
      loadComment({
        current_page: currentPage,
        record_per_page: 10,
        created_date: filter,
        record_id: recordID,
        object_id: objectId,
      })
    );
  }, [currentPage, filter, dispatch, recordID, objectId]);

  useEffect(() => {
    if (recordIDRedux !== recordID) {
      setCurrentPage(1);
    }
  }, [recordIDRedux, recordID]);

  useEffect(() => {
    dispatch(
      loadAllUser({
        current_page: 1,
        record_per_page: 10000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          display: item.Middle_Name
            ? item.Last_Name + " " + item.Middle_Name + " " + item.First_Name
            : item.Last_Name + " " + item.First_Name,
          id: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  return (
    <Wrapper isArticle={isArticle}>
      <CustomSpin spinning={isLoading}>
        <Title isArticle={isArticle}>{t("comments.comment")}</Title>
        {listComment.comment.length > 0 ? (
          <>
            <div
              ref={messagesEndRef}
              style={{
                height: "350px",
                overflowY: "auto",
                overflowX: "hidden",
              }}
            >
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  marginBottom: "8px",
                }}
              >
                <Select
                  value={filter}
                  options={[
                    {
                      label: t("comments.newestComment"),
                      value: -1,
                    },
                    {
                      label: t("comments.oldestComment"),
                      value: 1,
                    },
                  ]}
                  style={{ width: "193px" }}
                  onChange={(e) => {
                    dispatch(updateSort(true));
                    setCurrentPage(1);
                    $filter(e);
                  }}
                />
              </div>
              {listComment.comment.map((item, index) => {
                return (
                  <ListComment
                    name={
                      item.userDetails.Middle_Name
                        ? item.userDetails.Last_Name +
                          " " +
                          item.userDetails.Middle_Name +
                          " " +
                          item.userDetails.First_Name
                        : item.userDetails.Last_Name +
                          " " +
                          item.userDetails.First_Name
                    }
                    avatar={item?.userDetails?.avatar_config?.url || ""}
                    content={item.content}
                    link={item.link}
                    time={item.created_date}
                    timeEdit={item.modify_time}
                    like={item.likes}
                    id={item._id}
                    idParent={item._id}
                    totalReply={item.reply_count}
                    index={index}
                    by={item.userDetails._id}
                    isParent={true}
                    isLike={item.interacted}
                  >
                    {showReplyComment[index] && (
                      <>
                        {!!item.reply &&
                          item.reply.map((ele, idx) => {
                            return (
                              <>
                                <ListComment
                                  name={
                                    ele.userDetails.Middle_Name
                                      ? ele.userDetails.Last_Name +
                                        " " +
                                        ele.userDetails.Middle_Name +
                                        " " +
                                        ele.userDetails.First_Name
                                      : ele.userDetails.Last_Name +
                                        " " +
                                        ele.userDetails.First_Name
                                  }
                                  avatar={
                                    ele?.userDetails?.avatar_config?.url || ""
                                  }
                                  content={ele.content}
                                  link={ele.link}
                                  time={ele.created_date}
                                  timeEdit={ele.modify_time}
                                  like={ele.likes}
                                  id={ele._id}
                                  idParent={item._id}
                                  totalReply={ele.reply_count}
                                  index={index}
                                  by={ele.userDetails._id}
                                  isParent={false}
                                  isLike={ele.interacted}
                                />
                                {idx === item.reply.length - 1 && (
                                  <>
                                    {!!item.reply &&
                                      item.reply.length < item.reply_count && (
                                        <LoadMore
                                          onClick={() =>
                                            _onViewMore(index, item._id)
                                          }
                                        >
                                          {t("comments.showMore")}
                                        </LoadMore>
                                      )}

                                    <NewComment
                                      type="reply"
                                      onComment={_onComment}
                                      idComment={item._id}
                                      objectId={objectId}
                                      listUser={listUser}
                                      repUser={repUser}
                                      setRepUser={setRepUser}
                                    />
                                  </>
                                )}
                              </>
                            );
                          })}

                        {(!item.reply || item.reply.length === 0) && (
                          <NewComment
                            type="reply"
                            onComment={_onComment}
                            idComment={item._id}
                            objectId={objectId}
                            listUser={listUser}
                            repUser={repUser}
                            setRepUser={setRepUser}
                          />
                        )}
                      </>
                    )}
                  </ListComment>
                );
              })}
              {listComment.comment.length < listComment.total && (
                <LoadMore type={"parent"} onClick={() => _onViewMoreParent()}>
                  {t("comments.showMore")}
                </LoadMore>
              )}
            </div>
            <NewComment
              type="post"
              onComment={_onComment}
              objectId={objectId}
              sticky={true}
              listUser={listUser}
            />

            {/* <div ref={messagesEndRef} /> */}
          </>
        ) : (
          <>
            <div
              style={{
                height: "345px",
                overflowY: "auto",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <WrapNoComment>
                <img src={NoComment} alt="noComment" />
                <span> {t("comments.noComment")}</span>
              </WrapNoComment>
            </div>
            <NewComment
              type="post"
              onComment={_onComment}
              objectId={objectId}
              listUser={listUser}
            />
          </>
        )}
      </CustomSpin>
      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteComment}
        dataDelete={dataDelete}
        isLoading={false}
      />
      <ModalZoom
        isZoom={isZoom}
        $isZoom={$isZoom}
        selectedImage={selectedImage}
        $selectedImage={$selectedImage}
        dimensions={dimensions}
        $dimensions={$dimensions}
      />
    </Wrapper>
  );
}

export default withTranslation()(Comments);

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  background: #fff;

  .avatar {
    border-radius: 50% !important;
    width: 50px !important;
    height: 50px !important;
    border: 1.04008px solid #ececec;
  }

  .input {
    width: 100%;
  }

  .ant-comment-content {
    padding: 16px;
    background: #f2f8ff;
    border-radius: 10px;
  }

  .ant-comment-avatar {
    margin-right: 8px;
  }

  .ant-comment-nested {
    margin-left: 68px;
  }

  .ant-comment-actions {
    position: absolute;
    margin-top: 20px;
    width: 98%;

    li {
      width: 100%;
    }
  }

  .ant-comment-inner {
    padding-top: 0;
    padding-bottom: 32px;

    :hover {
      .action {
        visibility: visible;
        opacity: 1;
      }
    }
  }

  .ant-comment-content-author-name {
    width: 100%;
  }

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  .ant-comment-content-author-name > *:hover {
    color: #000;
  }

  .ant-input {
    margin-left: 8px;
    height: 50px;
    background: #f2f4f5;
    border-radius: 10px;
  }
`;

const WrapAvatar = styled.div`
  width: 50px;
  height: 50px;

  img {
    width: 50px;
    height: 50px;
    border: 1.04008px solid #ececec;
    object-fit: cover;
    border-radius: 50%;
  }
`;

const Title = styled.div`
  font-family: ${(props) =>
    props.isArticle ? "var(--roboto-500)" : "var(--roboto-400)"};
  box-shadow: inset 0px -1px 0px #f0f0f0;
  position: sticky;
  top: 0;
  z-index: ${(props) => (props.isArticle ? "2" : "5")};
  background: #fff;
  height: 36px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 24px 0;
  margin-bottom: 16px;
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 26px;

  color: #2c2c2c;
`;

const WrapComment = styled.div`
  margin-bottom: 16px;

  .ant-comment-inner {
    margin-bottom: ${({ haveReply }) => (haveReply ? "32px" : "24px")};
  }

  .ant-comment-content {
    background: ${({ isOwner }) => (isOwner ? "#f2f4f5" : "#f2f8ff")};
  }
`;

const Name = styled.div`
  font-family: var(--roboto-500);
  color: #000;
  font-size: 16px;
`;

const Content = styled.div`
  font-size: 16px;
  color: #000;
  display: flex;
  flex-direction: column;

  p {
    color: #016eff;
    margin-bottom: 0;
    cursor: pointer;
  }

  img {
    width: fit-content;
    max-width: 100%;
    margin-bottom: 8px;
  }

  span {
    display: ${({ overflow, showMore }) =>
      overflow && !showMore ? "-webkit-box" : "inline-block"};
    -webkit-line-clamp: 4;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

const WrapAction = styled.div`
  display: flex;
  justify-content: space-between;
`;

const Action = styled.div`
  display: flex;
`;

const Like = styled.div`
  margin-right: 16px;
  font-family: var(--roboto-500);
  font-size: 16px;
  color: ${({ isLike }) => (isLike ? "#1890ff" : "#6b6b6b")};
  cursor: pointer;

  :hover {
    text-decoration: underline;
  }
`;

const Reply = styled(Like)``;

const Edit = styled(Like)`
  color: #1890ff;
`;

const Time = styled.div`
  font-family: var(--roboto-300);
  font-size: 16px;
  color: #8c8c8c;
`;

const NumberOfLike = styled.div`
  box-shadow: 0px 0.5px 3px rgba(0, 0, 0, 0.2);
  border-radius: 30px;
  width: 38px;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    margin-right: 3px;
  }
`;

const LoadMore = styled.div`
  z-index: 2;
  font-size: 16px;
  color: #016eff;
  width: fit-content;
  margin-bottom: ${({ type }) => (type ? "24px" : "0")};
  margin-left: ${({ type }) => (type ? "70px" : "0")};
  margin-top: ${({ type }) => (type ? "24px" : "0")};

  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

const WrapNoComment = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  img {
    width: fit-content;
  }
`;

const WrapName = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WrapEdit = styled.div`
  img {
    margin-right: 8px;
    cursor: pointer;

    :hover {
      background: #fff;
    }
  }
`;

const CustomSpin = styled(Spin)`
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;
