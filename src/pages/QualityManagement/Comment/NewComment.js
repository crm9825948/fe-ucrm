import styled from "styled-components/macro";
import { useEffect, useState, useCallback } from "react";
import { useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import { MentionsInput, Mention } from "react-mentions";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Upload from "antd/lib/upload";
import Modal from "antd/lib/modal";

import AvatarImg from "assets/images/header/avatar.png";
import UploadImage from "assets/icons/comment/upload.svg";

import { BASE_URL_API, BE_URL } from "constants/constants";
import { Notification } from "components/Notification/Noti";

function NewComment({
  onComment,
  type,
  idComment,
  objectId,
  sticky,
  contentEdit,
  link,
  listUser,
  repUser,
  setRepUser,
}) {
  const { userDetail } = useSelector((state) => state.userReducer);

  const { t } = useTranslation();

  const [valueNew, setValueNew] = useState("");
  const [valueReply, setValueReply] = useState("");
  const [valueEdit, setValueEdit] = useState("");
  const [mentions, setMentions] = useState(null);

  const [previewVisible, setPreviewVisible] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");

  const [file, setFile] = useState([]);

  const props = {
    name: "file",
    action: BASE_URL_API + "upload-file",
    headers: {
      Authorization: localStorage.getItem("setting_accessToken"),
    },
    data: {
      obj: objectId,
    },
    // onChange(info) {
    //   const { fileList: newFileList } = info;

    //   /* eslint-disable-next-line */
    //   newFileList.map((file, idx) => {
    //     if (file.url === undefined) {
    //       file.url =
    //         BASE_URL_API + file &&
    //         file.response &&
    //         file.response.data &&
    //         file.response.data[0];
    //       file.url = BE_URL + file.url;
    //     }
    //   });
    //   setFile(newFileList);
    // },
  };

  const handleCancel = () => setPreviewVisible(false);

  const handlePreview = async (file) => {
    setPreviewImage(BE_URL + file.response.data[0]);
    setPreviewVisible(true);
    setPreviewTitle(file.name);
  };

  const handleChange = ({ fileList: newFileList }) => {
    setFile(newFileList);
  };

  useEffect(() => {
    if (
      type === "reply" &&
      userDetail._id !== repUser.id &&
      repUser.idComment === idComment
    ) {
      setValueReply((prev) => {
        // eslint-disable-next-line no-useless-concat
        let temp = prev + " " + `[${repUser.name}]{${repUser.id}}` + " ";
        return temp;
      });
    }
  }, [idComment, repUser, type, userDetail]);

  useEffect(() => {
    if (type === "edit") {
      setValueEdit(contentEdit);

      if (link) {
        setFile([
          {
            name: "image",
            response: {
              data: [link],
            },
          },
        ]);
      }
    }
  }, [contentEdit, link, type]);

  const checkBeforeSubmit = (value, type, id, file, mention) => {
    if (
      value !== "" ||
      (file.length > 0 && file[0].response.data[0]) ||
      mention !== null
    ) {
      onComment(value, type, id, file, mention);
      setValueNew("");
      setValueReply("");
      setValueEdit("");
      setFile([]);
      setRepUser({
        name: "",
        id: "",
        idComment: "",
      });
    } else {
      Notification("warning", "Please input your comment!");
    }
  };

  const _onSubmit = (e) => {
    if (e.key === "Enter" && !e.shiftKey) {
      e.preventDefault();
      if (type === "post") {
        checkBeforeSubmit(valueNew, "post", idComment, file, mentions);
      } else if (type === "reply") {
        checkBeforeSubmit(valueReply, "reply", idComment, file, mentions);
      } else {
        checkBeforeSubmit(valueEdit, "edit", idComment, file, mentions);
      }
    }
  };

  const defaultStyle = {
    control: {
      backgroundColor: "#F2F4F5",
      fontSize: 14,
      padding: 14,
      maxHeight: 300,
      overflow: "auto",
      borderRadius: "10px",
    },
    // highlighter: {
    //   overflow: "hidden"
    // },
    input: {
      borderRadius: "10px",
      padding: 14,
      overflow: "auto",
    },
    // "&multiLine": {
    // control: {
    //   fontFamily: "monospace",
    //   border: "1px solid silver"
    // },
    // highlighter: {
    //   padding: 9
    // },
    // input: {
    //   padding: 9,
    //   minHeight: 3,
    //   outline: 0,
    //   border: 0
    // }
    // },
    suggestions: {
      bottom: 0,
      top: "unset",
      left: 40,
      zIndex: 10,
      list: {
        backgroundColor: "white",
        border: "1px solid rgba(0,0,0,0.15)",
        borderRadius: 10,
        fontSize: 14,
        overflow: "auto",
        zIndex: 10,
        maxHeight: 300,
      },
      item: {
        padding: "5px 15px",
        borderBottom: "1px solid rgba(0,0,0,0.15)",
        "&focused": {
          backgroundColor: "#cee4e5",
        },
      },
    },
  };

  const nonAccentVietnamese = (value) => {
    value = value.toLowerCase();
    value = value.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    value = value.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    value = value.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    value = value.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    value = value.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    value = value.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    value = value.replace(/đ/g, "d");
    value = value.replace(/\u0300|\u0301|\u0303|\u0309|\u0323/g, "");
    value = value.replace(/\u02C6|\u0306|\u031B/g, "");

    return value;
  };

  const handlePasteFileFromClipboard = useCallback(
    async (event) => {
      const clipboardData = Array.from(event.clipboardData.items);
      let files = [];
      clipboardData.forEach((item) => {
        if (item?.kind === "file") {
          const file = item.getAsFile();
          files.push(file);
        }
      });

      if (files.length > 0) {
        const formData = new FormData();
        formData.append("file", files[0]);
        formData.append("obj", objectId);
        const token = await checkTokenExpiration();

        try {
          const res = await axios.post(BASE_URL_API + "upload-file", formData, {
            headers: {
              Authorization: `JWT ${token}`,
            },
          });
          const response = [
            {
              response: {
                data: [res.data.data[0]],
              },
              name: files[0].name,
              thumbUrl: BE_URL + res.data.data[0],
            },
          ];
          setFile(response);
        } catch (error) {
          console.error(error);
        }
      }
    },
    [objectId]
  );

  useEffect(() => {
    window.addEventListener("paste", handlePasteFileFromClipboard);
    return () => {
      window.removeEventListener("paste", handlePasteFileFromClipboard);
    };
  }, [handlePasteFileFromClipboard]);

  return (
    <WrapNewComment sticky={sticky}>
      {type !== "edit" && (
        <WrapAvatar>
          <img
            src={
              userDetail?.avatar_config?.url
                ? BE_URL + userDetail?.avatar_config?.url
                : AvatarImg
            }
            alt="avatar"
          />
        </WrapAvatar>
      )}

      <WrapUpload sticky={sticky} type={type} file={file.length > 0}>
        <Upload
          {...props}
          accept=".jpg, .png, .gif, .jfif, .jpeg"
          maxCount={1}
          listType="picture"
          fileList={file}
          onPreview={handlePreview}
          onChange={handleChange}
        >
          {file && file.length > 0 ? null : (
            <img style={{ cursor: "pointer" }} src={UploadImage} alt="upload" />
          )}
        </Upload>
      </WrapUpload>

      <WrapInput>
        <MentionsInput
          style={defaultStyle}
          value={
            type === "post"
              ? valueNew
              : type === "reply"
              ? valueReply
              : valueEdit
          }
          onChange={(e) => {
            type === "post"
              ? setValueNew(e.target.value)
              : type === "reply"
              ? setValueReply(e.target.value)
              : setValueEdit(e.target.value);

            const regex = /[^{}]+(?=})/g;
            // const regex2 = /(?<=\[)(.*?)(?=\])/g;
            const mentions = e.target.value.match(regex);
            // const mentions2 = e.target.value.match(regex2);
            setMentions(mentions);
          }}
          placeholder={t("common.placeholderInput")}
          onKeyDown={_onSubmit}
          allowSpaceInQuery
        >
          <Mention
            trigger="@"
            data={(search) => {
              if (search) {
                return listUser.filter((user) =>
                  nonAccentVietnamese(user.display).includes(
                    nonAccentVietnamese(search)
                  )
                );
              } else {
                return listUser;
              }
            }}
            style={{ backgroundColor: "#1877f273" }}
            appendSpaceOnAdd
            markup="[__display__]{__id__}"
            renderSuggestion={(entry) => <span>{entry.display}</span>}
          />
        </MentionsInput>
      </WrapInput>

      {/* <Input.TextArea
        autoSize={{ minRows: 2 }}
        value={
          type === "post" ? valueNew : type === "reply" ? valueReply : valueEdit
        }
        placeholder={t("common.placeholderInput")}
        onChange={(e) =>
          type === "post"
            ? setValueNew(e.target.value)
            : type === "reply"
            ? setValueReply(e.target.value)
            : setValueEdit(e.target.value)
        }
        onKeyDown={_onSubmit}
      /> */}

      <Modal
        open={previewVisible}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img
          alt="example"
          style={{
            width: "100%",
          }}
          src={previewImage}
        />
      </Modal>
    </WrapNewComment>
  );
}

export default withTranslation()(NewComment);

NewComment.defaultProps = {
  sticky: false,
  repUser: {},
  setRepUser: () => {},
};

const WrapNewComment = styled.div`
  z-index: 3;
  display: flex;
  align-items: center;
  margin: ${({ sticky }) => (sticky ? "16x 0 0 0" : "16px 0 0 0")};
  padding: 0;
  background: ${({ sticky }) => (sticky ? "#fff" : "unset")};
  width: 100%;
  position: relative;

  .ant-upload-list-item {
    height: 45px;
    margin-top: 0;
  }
`;

const WrapInput = styled.div`
  width: 100%;
  margin-left: 8px;
`;

const WrapUpload = styled.div`
  position: absolute;
  width: ${({ file }) => (file ? "25%" : "fit-content")};
  top: ${({ file }) => (file ? "3px" : "13px")};
  right: 15px;
  z-index: 2;
`;

const WrapAvatar = styled.div`
  width: 50px;
  height: 50px;

  img {
    width: 50px;
    height: 50px;
    border: 1.04008px solid #ececec;
    object-fit: cover;
    border-radius: 50%;
  }
`;
