import { DatePicker, Form, Switch } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import moment from "moment";

const DateType = (props) => {
  const { field, form } = props;

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);
  const [format, setFormat] = useState("");
  const [today, setToday] = useState(false);
  const [value, setValue] = useState(null);
  // const dispatch = useDispatch();

  // useEffect(() => {
  //   dispatch(loadConfig());
  // }, [dispatch]);
  const { recordData } = useSelector((state) => state.objectsReducer);
  useEffect(() => {
    let formatTemp = optionsDate.find(
      (ele) => ele.value === allConfig.tenant_date_format
    );
    setFormat(formatTemp && formatTemp.label);
    // eslint-disable-next-line
  }, [allConfig]);

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "YYYY MM DD",
      value: "%Y %m %d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "MM DD YYYY",
      value: "%m %d %Y",
    },

    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
    {
      label: "DD MM YYYY",
      value: "%d %m %Y",
    },
  ];

  useEffect(() => {
    if (Object.keys(recordData).length === 0) {
      setToday(false);
    }
    if (recordData[field.ID]?.value === "today") {
      setToday(true);
    } else {
      form.setFieldValue(
        field.ID,
        recordData[field.ID]?.value ? moment(recordData[field.ID]?.value) : null
      );
      setValue(
        recordData[field.ID]?.value ? moment(recordData[field.ID]?.value) : null
      );
      setToday(false);
    }
  }, [recordData, field, form]);

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      {today || form.getFieldValue(field.ID) === "today" ? (
        <>
          <Switch
            onChange={() => {
              setToday(!today);
              setValue(null);
              form.setFieldValue(field.ID, null);
            }}
            checked={today ? true : false}
            unCheckedChildren="OFF"
            checkedChildren="ON"
          />{" "}
          Autofill today
        </>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Switch
            onChange={() => {
              setToday(!today);
              form.setFieldValue(field.ID, "today");
            }}
            checked={today}
            unCheckedChildren="OFF"
            checkedChildren="ON"
            style={{ marginRight: "8px" }}
          />
          <DatePicker
            format={format}
            placeholder={field.placeholder}
            style={{ width: "100%" }}
            value={value}
            onChange={(e) => {
              form.setFieldValue(field.ID, e);
              setValue(e);
            }}
            disabled={
              field.readonly ||
              (field.permission_read && field.permission_write === false)
            }
          />
        </div>
      )}
    </Form.Item>
  );
};

export default DateType;
