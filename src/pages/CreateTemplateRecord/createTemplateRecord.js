import { StarOutlined } from "@ant-design/icons";
import {
  Breadcrumb,
  Button,
  Col,
  Form,
  Input,
  Row,
  Select,
  Switch,
  Typography,
} from "antd";
import img from "assets/icons/common/confirm.png";
import deleteImg from "assets/icons/common/delete-icon.png";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API, BE_URL } from "constants/constants";
import moment from "moment";
import DynamicField from "pages/Objects/modal/fieldsType/dynamicField";
import Email from "pages/Objects/modal/fieldsType/email";
import File from "pages/Objects/modal/fieldsType/file";
import FormulaField from "pages/Objects/modal/fieldsType/formulaField";
import IDComp from "pages/Objects/modal/fieldsType/ID";
import LinkingObject from "pages/Objects/modal/fieldsType/linkingObject";
import Lookup from "pages/Objects/modal/fieldsType/lookup";
import Number from "pages/Objects/modal/fieldsType/number";
import SelectType from "pages/Objects/modal/fieldsType/select";
import Text from "pages/Objects/modal/fieldsType/text";
import Textarea from "pages/Objects/modal/fieldsType/textarea";
import User from "pages/Objects/modal/fieldsType/user";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import {
  createTemplate,
  deleteTemplate,
  loadFormCreate,
  // loadFormCreate,
  loadListObjectField,
  loadRecordDataSuccess,
  loadTemplate,
  makeDefaultTemplate,
  setHiddenArray,
  setLinkingFieldValue,
  setShowModal,
  updateStatusTemplate,
  updateTemplate,
} from "redux/slices/objects";
import styled from "styled-components";
import Date from "./date";
import Datetime from "./dateTime";

const { Text: TextComponent } = Typography;
const { Option, OptGroup } = Select;

const CreateTemplateRecord = () => {
  const { t } = useTranslation();
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [open, setOpen] = useState(false);
  const {
    listObjectField,
    // recordData,
    // fields,
    hiddenArray,
    linkingFieldValue,
    // linkingFieldValue,
    template,
    hiddenDynamic,
    fields,
    isLoadingDeleteTemplate,
  } = useSelector((state) => state.objectsReducer);
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [objectId, setObjectId] = useState("");
  const [formValue, setFormValue] = useState({});
  const [dataFilterLinking, setDataFilterLinking] = useState({});
  // const [recordID, setRecordID] = useState("");
  const [templateName, setTemplateName] = useState("");
  const [IdTemplate, setIdTemplate] = useState("");
  const [dataTemplate, setDataTemplate] = useState({});
  const [allFields, setAllFields] = useState({});

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "create_record_from_template" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    if (Object.entries(dataTemplate).length > 0 && IdTemplate) {
      setDataFilterLinking(linkingFieldValue);
    }
  }, [linkingFieldValue, dataTemplate, IdTemplate]);

  useEffect(() => {
    if (isLoadingDeleteTemplate === false) {
      setIdTemplate("");
      setDataTemplate({});
      setTemplateName("");
      form.resetFields();
      dispatch(setLinkingFieldValue({}));
      dispatch(loadRecordDataSuccess({}));
    }
    /*eslint-disable-next-line*/
  }, [isLoadingDeleteTemplate, form]);

  useEffect(() => {
    let newObj = {};
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        /*eslint-disable-next-line*/
        return section.fields.map((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });
      });

    setAllFields(newObj);
  }, [listObjectField]);

  useEffect(() => {
    if (IdTemplate === "") {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((sections, idx) => {
          /*eslint-disable-next-line*/
          sections.fields.map((field, index) => {
            if (field.type === "dynamic-field" && field.default_value) {
              form.setFieldsValue({
                [field.ID]: field.default_value,
              });
              /*eslint-disable-next-line*/
              field.list_items[field.default_value].map((item, idx) => {
                hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
              });
              dispatch(setHiddenArray(hiddenTmp));
            } else if (field.type === "select" && field.default_value) {
              form.setFieldsValue({
                [field.ID]: field.default_value,
              });
            }
          });
        });
      setFormValue("");
    }
    /*eslint-disable-next-line*/
  }, [IdTemplate, listObjectField]);

  useEffect(() => {
    if (Object.entries(dataTemplate).length > 0 && IdTemplate) {
      form.setFieldsValue({
        assignTo: dataTemplate.owner,
      });
      setFormValue(form.getFieldsValue());
      // setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (dataTemplate[field.ID]?.value) {
              /*eslint-disable-next-line*/
              field.list_items[dataTemplate[field.ID].value].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      dispatch(setHiddenArray(tmpHidden));
    }
    /* eslint-disable-next-line */
  }, [dataTemplate, IdTemplate, fields]);

  const edit = (dataTemplate) => {
    dispatch(loadRecordDataSuccess(dataTemplate));
    let record = {};
    Object.entries(dataTemplate).forEach(([key, value]) => {
      if (value !== null && typeof value === "object") {
        record[key] = value.value;
      } else {
        record[key] = value;
      }
    });

    let newObj = {};
    /* eslint-disable-next-line */
    Object.entries(allFields).forEach(([key, value], index) => {
      if (value.type === "linkingobject") {
        newObj[key] = {
          ...dataTemplate[key],
        };
      }
    });
    dispatch(setLinkingFieldValue(newObj));

    form.resetFields();
    let newRecord = { ...record };
    /* eslint-disable-next-line */
    Object.entries(record).forEach(([key, value], index) => {
      if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "datetime-local"
      ) {
        if (record[key]) {
          if (record[key] === "today") {
            newRecord[key] = "today";
          } else {
            newRecord[key] = record[key] ? moment(record[key]) : null;
          }
        } else {
          newRecord[key] = null;
        }
      } else if (
        allFields &&
        allFields[key] &&
        allFields[key].type === "date"
      ) {
        if (record[key]) {
          if (record[key] === "today") {
            newRecord[key] = "today";
          } else {
            newRecord[key] = record[key] ? moment(record[key]) : null;
          }
        } else {
          newRecord[key] = null;
        }
      }
    });
    form.setFieldsValue({ ...newRecord, ...newObj });
    // setEditingKey(record.key);

    dispatch(setShowModal(true));
  };

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "id":
        return <IDComp field={field} recordData={dataTemplate} recordID={""} />;
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            listObjectField={listObjectField}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} form={form} />;
      case "date":
        return <Date field={field} form={form} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={IdTemplate}
            setRecordID={setIdTemplate}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={true}
            recordID={IdTemplate}
            setRecordID={setIdTemplate}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={IdTemplate}
            setRecordID={setIdTemplate}
            open={true}
          />
        );
      case "dynamic-field":
        return <DynamicField field={field} form={form} recordID={""} />;
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={dataTemplate}
          />
        );
      case "formula":
        return <FormulaField field={field} />;
      case "lookup":
        return <Lookup field={field} form={form} recordData={dataTemplate} />;

      default:
        break;
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      ...form.getFieldsValue(),
      ...linkingFieldValue,
    });
  }, [linkingFieldValue, form]);

  const onFinish = (values) => {
    if (templateName.trim()) {
      let fieldsObject = {};
      /* eslint-disable-next-line */
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"] &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((section, idx) => {
          /* eslint-disable-next-line */
          section.fields.map((item, index) => {
            fieldsObject[item.ID] = { ...item };
          });
        });
      let listValue = [];
      // eslint-disable-next-line
      Object.entries(values).forEach(([key, value], idx) => {
        if (
          key !== "assignTo" &&
          key !== "share_to" &&
          key !== "action_type" &&
          key !== "subject" &&
          key !== "permission"
        ) {
          if (fieldsObject[key].type === "file" && value) {
            const result = [];
            /* eslint-disable-next-line */
            if (value.fileList) {
              // eslint-disable-next-line
              value.fileList.map((item, idx) => {
                if (item.url === undefined) {
                  item.url =
                    BASE_URL_API + item &&
                    item.response &&
                    item.response.data &&
                    item.response.data[0];
                  item.url = BE_URL + item.url;
                }
                result.push(item.url);
              });
            } else {
              if (typeof value === "string") {
                let listFile = value.split(",");
                listFile.forEach((item, idx) => {
                  result.push(item);
                  return null;
                });
              } else {
                // eslint-disable-next-line
                value.map((item, idx) => {
                  if (item.url) {
                    result.push(item.url);
                  } else {
                    result.push(item);
                  }
                });
              }
            }
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: result.length === 0 ? null : result,
            };

            listValue.push(newItem);
          } else if (fieldsObject[key].type === "date" && value !== "today") {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value ? moment(value).format("YYYY-MM-DD") : null,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "date" && value === "today") {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: "today",
            };
            listValue.push(newItem);
          } else if (
            fieldsObject[key].type === "datetime-local" &&
            value !== "today"
          ) {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
            };
            listValue.push(newItem);
          } else if (
            fieldsObject[key].type === "datetime-local" &&
            value === "today"
          ) {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: "today",
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "linkingobject") {
            let newItem = {
              id_field: key,
              id_field_related_record:
                (value && value.id_field_related_record) || null,
              id_related_record: (value && value.id_related_record) || null,
              object_related: (value && value.object_related) || null,
              value: (value && value.value) || null,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "lookup") {
            let newItem = {
              id_field: key,
              id_field_related_record:
                (value && value.id_field_related_record) || null,
              id_related_record: (value && value.id_related_record) || null,
              object_related: (value && value.object_related) || null,
              value: (value && value.value) || null,
            };
            listValue.push(newItem);
          } else {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value?.toString() ? value : null,
            };
            listValue.push(newItem);
          }
        }
      });
      if (IdTemplate) {
        dispatch(
          updateTemplate({
            id_template: IdTemplate,
            record_values: listValue,
            object_id: objectId,
            name_template: templateName.trim(),
          })
        );
      } else {
        dispatch(
          createTemplate({
            record_values: listValue,
            object_id: objectId,
            name_template: templateName.trim(),
          })
        );
        // setEditingKey("");
        dispatch(setShowModal(false));
        setIdTemplate("");
        form.resetFields();
        setTemplateName("");
        dispatch(loadRecordDataSuccess({}));
      }

      setIdTemplate("");
      setDataTemplate({});
      setTemplateName("");
      form.resetFields();
      dispatch(setLinkingFieldValue({}));
      dispatch(loadRecordDataSuccess({}));
      dispatch(setHiddenArray(hiddenDynamic));
    } else {
      Notification("error", "Template name can not be empty!");
    }
  };

  const handleClear = () => {
    form.resetFields();
    setTemplateName("");
    dispatch(setLinkingFieldValue({}));
    dispatch(loadRecordDataSuccess({}));
  };

  return (
    <>
      <Wrapper>
        <Breadcrumb>
          <CustomBreadCrumb>Object</CustomBreadCrumb>
          <BreadcrumbItem> Object template </BreadcrumbItem>
        </Breadcrumb>

        <div className="wrapper-content">
          <div className="left">
            <div className="choose-object">
              {t("templateRecord.chooseObject")}
            </div>
            <Select
              style={{ width: "100%", marginBottom: "10px" }}
              size="large"
              placeholder={t("objectLayoutField.selectObject")}
              onChange={(e) => {
                setObjectId(e);

                dispatch(
                  loadFormCreate({
                    object_id: e,
                  })
                );
                dispatch(
                  loadListObjectField({
                    api_version: "2",
                    object_id: e,
                    show_meta_fields: true,
                  })
                );
                dispatch(
                  loadTemplate({
                    object_id: e,
                  })
                );

                setIdTemplate("");
                setDataTemplate({});
                setTemplateName("");
                form.resetFields();
                dispatch(setLinkingFieldValue({}));
                dispatch(loadRecordDataSuccess({}));
              }}
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(category).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {/* eslint-disable-next-line*/}
                    {value.map((object, index) => {
                      if (object.Status && !object?.is_embed_iframe)
                        return (
                          <Option value={object._id}>{object.Name}</Option>
                        );
                    })}
                  </OptGroup>
                );
              })}
            </Select>
            {objectId && checkRule("create") ? (
              <CustomButtonAdd
                size="large"
                onClick={() => {
                  setIdTemplate("");
                  setDataTemplate({});
                  setTemplateName("");
                  form.resetFields();
                  dispatch(setLinkingFieldValue({}));
                  dispatch(loadRecordDataSuccess({}));
                  dispatch(setHiddenArray(hiddenDynamic));
                }}
              >
                + {t("templateRecord.addTemplate")}
              </CustomButtonAdd>
            ) : (
              ""
            )}
            <div className="left-container">
              {template.map((item, idx) => {
                return (
                  <div
                    className={`template-item ${
                      item._id === IdTemplate ? "active" : ""
                    }`}
                    onClick={() => {
                      setIdTemplate(item._id);
                      setDataTemplate(item.record_values);
                      setTemplateName(item.name_template);
                      edit(item.record_values);
                    }}
                  >
                    <div className="name-comp">
                      <div className={`template-name `}>
                        <TextComponent
                          ellipsis={{ tooltip: item.name_template }}
                        >
                          {item.name_template}
                        </TextComponent>
                      </div>
                      <div>
                        {item.is_default ? (
                          ""
                        ) : checkRule("delete") ? (
                          <img
                            alt=""
                            src={deleteImg}
                            onClick={() => {
                              setIdTemplate(item._id);
                              setOpen(true);
                            }}
                          />
                        ) : (
                          ""
                        )}
                      </div>
                    </div>
                    <div className="action-template">
                      <span className="span-inside">
                        <Switch
                          checkedChildren="ON"
                          unCheckedChildren="OFF"
                          checked={item.status}
                          style={{ marginRight: "10px" }}
                          disabled={!checkRule("edit")}
                          onChange={() => {
                            dispatch(
                              updateStatusTemplate({
                                id_template: item._id,
                                status: !item.status,
                                object_id: objectId,
                              })
                            );
                          }}
                        />
                        {t("templateRecord.active")}
                      </span>
                      <span className="span-inside">
                        {" "}
                        <StarOutlined
                          style={{
                            color: `${item.is_default ? "#FADB14" : ""}`,
                            marginRight: "8px",
                          }}
                          onClick={() => {
                            if (checkRule("edit")) {
                              dispatch(
                                makeDefaultTemplate({
                                  id_template: item._id,
                                  is_default: !item.is_default,
                                  object_id: objectId,
                                })
                              );
                            }
                          }}
                        />
                        {t("templateRecord.default")}
                      </span>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="right">
            {objectId ? (
              <>
                <div className="template-name">
                  <span style={{ color: "red" }}>*</span>
                  {t("templateRecord.templateName")}
                </div>
                <div className="info">
                  <span className="input-name">
                    <Input
                      style={{ width: "100%" }}
                      value={templateName}
                      onChange={(e) => setTemplateName(e.target.value)}
                    />
                  </span>
                  {/* <span className="input-name">
                    <Switch checkedChildren="ON" unCheckedChildren="OFF" />{" "}
                    Active
                  </span> */}
                </div>
              </>
            ) : (
              ""
            )}

            <Form
              name="basic"
              form={form}
              labelCol={{ span: 24 }}
              wrapperCol={{ span: 24 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              noValidate={true}
              autoComplete="off"
              onValuesChange={(value, values) => {
                setFormValue(values);
                // setValueModal(values);
              }}
            >
              {listObjectField.length > 0 &&
                listObjectField[listObjectField.length - 1]["main_object"][
                  "sections"
                  /*eslint-disable-next-line*/
                ].map((section, idx) => {
                  if (section.section_name !== "Meta fields")
                    return (
                      <WrapperSection key={idx}>
                        <CustomTitleSection>
                          {section.section_name}
                        </CustomTitleSection>
                        <div>
                          <Row gutter={[70, 24]}>
                            {/* eslint-disable-next-line */}
                            {section.fields.map((field, index) => {
                              if (
                                field.hidden === false &&
                                field.permission_hidden === false &&
                                hiddenArray.findIndex(
                                  (ele) => ele === field._id
                                ) < 0
                              ) {
                                if (
                                  Object.entries(dataTemplate).length === 0 &&
                                  field.type === "id"
                                ) {
                                } else {
                                  if (
                                    field.type !== "file" &&
                                    field.type !== "id" &&
                                    field.type !== "formula"
                                  ) {
                                    return (
                                      <CustomCol span={12} key={field._id}>
                                        <div style={{ width: "100%" }}>
                                          {handleFieldType(field, true)}
                                        </div>
                                      </CustomCol>
                                    );
                                  }
                                }
                              }
                            })}
                          </Row>
                        </div>
                      </WrapperSection>
                    );
                })}

              {objectId && (checkRule("create") || checkRule("edit")) ? (
                <div className="action-button">
                  <CustomButtonCancel
                    size="large"
                    onClick={() => {
                      handleClear();

                      setIdTemplate("");
                      setDataTemplate({});
                      setTemplateName("");
                      form.resetFields();
                      dispatch(setLinkingFieldValue({}));
                      dispatch(loadRecordDataSuccess({}));
                      dispatch(setHiddenArray(hiddenDynamic));
                    }}
                  >
                    {t("common.cancel")}
                  </CustomButtonCancel>
                  {/* <CustomButtonCancel
                    size="large"
                    onClick={() => {
                      handleClear();
                      setIdTemplate("");
                      setDataTemplate({});
                      setTemplateName("");
                      form.resetFields();
                      dispatch(setLinkingFieldValue({}));
                      dispatch(loadRecordDataSuccess({}));
                      dispatch(setHiddenArray(hiddenDynamic));
                    }}
                  >
                    Clear
                  </CustomButtonCancel> */}
                  <CustomButtonSave size="large" htmlType="submit">
                    {t("common.save")}
                  </CustomButtonSave>
                </div>
              ) : (
                ""
              )}
            </Form>
          </div>
        </div>

        <ModalConfirm
          title={"Bạn có chắc chắn xóa bản mẫu này?"}
          decs={"Do you want to delete this record with the exist setting?"}
          open={open}
          setOpen={setOpen}
          method={deleteTemplate}
          data={{
            id_template: IdTemplate,
            object_id: objectId,
          }}
          setOpenModal={() => {}}
          img={img}
        />
      </Wrapper>
    </>
  );
};

const Wrapper = styled.div`
  padding: 24px;
  .custom-label {
    font-size: 16px;
    display: inherit;
    margin-bottom: 6px;
    font-family: var(--roboto-700);
  }
  .left-container {
    height: calc(100% - 140px);
    overflow: auto;
  }
  .wrapper-content {
    .name-comp {
      display: flex;
      justify-content: space-between;
      img {
        width: 20px;
      }
    }
    .ant-switch-checked {
      background-color: ${(props) => props.theme.main};
    }
    margin-top: 16px;
    width: 100%;
    height: 80vh;
    display: flex;
    justify-content: space-between;
    .left {
      width: 30%;
      height: 100%;
      background-color: white;
      padding: 16px;
      .active {
        .template-name {
          color: ${(props) => props.theme.main} !important;
        }
        background: rgba(32, 162, 162, 0.15) !important;
      }
      .template-item {
        padding: 12px 16px;
        gap: 10px;

        height: 80px;

        background: #ffffff;
        /* Vien xam - Stroke Gray */

        border: 1px solid #ececec;
        border-radius: 5px;
        margin-bottom: 8px;

        :hover {
          background: rgba(32, 162, 162, 0.15);
          cursor: pointer;
          transition: 1s;
        }
        .template-name {
          font-weight: bold;
          font-size: 16px;
          line-height: 22px;
          margin-bottom: 10px;
          /* identical to box height, or 138% */

          /* Character/Body text */
          width: 90%;
          color: #2c2c2c;
        }

        .action-template {
          display: flex;
          justify-content: space-between;
          align-items: center;
          .span-inside {
            font-weight: 400;
            font-size: 16px;
            line-height: 22px;
            /* identical to box height, or 138% */

            display: flex;
            align-items: center;

            /* Character/Body text */

            color: #2c2c2c;
          }
        }
      }
      .choose-object {
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 8px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
    }
    .right {
      width: 69%;
      height: 100%;
      padding: 16px;
      padding-bottom: 0;
      overflow: auto;
      background-color: white;
      position: relative;
      .ant-form-item-label {
        padding: 0 !important;
      }
      label {
        font-style: normal;
        font-weight: 700;
        font-size: 16px;
        line-height: 22px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
      .ant-form-item {
        margin-bottom: 0 !important;
      }
      .ant-row {
        row-gap: 0 !important;
      }
      .ant-col {
        margin-bottom: 4px;
      }
      .info {
        display: flex;
        align-items: center;
        justify-content: space-between;
        width: 100%;
        margin-bottom: 16px;
        .input-name {
          width: 48%;
        }
      }
      .template-name {
        font-style: normal;
        font-weight: bold;
        font-size: 16px;
        line-height: 22px;
        margin-bottom: 8px;
        /* identical to box height, or 138% */

        /* Character/Body text */

        color: #2c2c2c;
      }
    }
  }
  .action-button {
    width: 100%;
    display: flex;
    justify-content: flex-end;
    position: sticky;
    bottom: 0px;
    right: 0;
    padding: 16px;
    background-color: #fff;
  }
`;
const CustomButtonAdd = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  margin-top: 16px;
  margin-bottom: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: default;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: #2c2c2c;
  cursor: default;
`;
const CustomCol = styled(Col)`
  display: flex;
`;

const WrapperSection = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 15px;
  padding: 24px;
  position: relative;
`;

const CustomTitleSection = styled.div`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 20px;
  /* identical to box height, or 143% */

  display: flex;
  align-items: center;
  color: ${(props) => props.theme.main};

  margin-bottom: 24px;
  position: absolute;
  top: -11px;
  left: 24px;
  background-color: #fff;
  padding: 0 10px;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-left: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;
  margin-left: 16px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

export default CreateTemplateRecord;
