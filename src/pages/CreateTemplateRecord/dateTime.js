import { DatePicker, Form, Switch } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import moment from "moment";
// import { loadConfig } from "redux/slices/datetimeSetting";

const Datetime = (props) => {
  const { field, form } = props;

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);
  const [format, setFormat] = useState("");
  // const dispatch = useDispatch();
  const [today, setToday] = useState(false);
  const { recordData } = useSelector((state) => state.objectsReducer);
  const [value, setValue] = useState(null);

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "YYYY MM DD",
      value: "%Y %m %d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "MM DD YYYY",
      value: "%m %d %Y",
    },

    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
    {
      label: "DD MM YYYY",
      value: "%d %m %Y",
    },
  ];

  const optionsTime = [
    {
      label: "HH:mm:ss",
      value: "%H:%M:%S",
    },
    {
      label: "HH:mm",
      value: "%H:%M",
    },
  ];

  // useEffect(() => {
  //   dispatch(loadConfig());
  // }, [dispatch]);

  useEffect(() => {
    if (Object.keys(recordData).length === 0) {
      setToday(false);
    }
    if (recordData[field.ID]?.value === "today") {
      setToday(true);
    } else {
      form.setFieldValue(
        field.ID,
        recordData[field.ID]?.value ? moment(recordData[field.ID]?.value) : null
      );
      setValue(
        recordData[field.ID]?.value ? moment(recordData[field.ID]?.value) : null
      );
      setToday(false);
    }
  }, [recordData, field, form]);

  useEffect(() => {
    if (Object.keys(allConfig).length > 0) {
      let date = allConfig.tenant_datetime_format.split(" ")[0];
      let time = allConfig.tenant_datetime_format.split(" ")[1];
      let finalDate = optionsDate.find((ele) => ele.value === date).label;
      let finalTime = optionsTime.find((ele) => ele.value === time).label;
      setFormat(finalDate + " " + finalTime);
    }

    // eslint-disable-next-line
  }, [allConfig]);
  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      {today || form.getFieldValue(field.ID) === "today" ? (
        <>
          <Switch
            onChange={() => {
              setToday(!today);
              setValue(null);
              form.setFieldValue(field.ID, null);
            }}
            checked={today ? true : false}
            unCheckedChildren="OFF"
            checkedChildren="ON"
          />{" "}
          Autofill today
        </>
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
          }}
        >
          <Switch
            onChange={() => {
              setToday(!today);
              form.setFieldValue(field.ID, "today");
            }}
            checked={today}
            unCheckedChildren="OFF"
            checkedChildren="ON"
            style={{ marginRight: "8px" }}
          />
          <DatePicker
            format={format}
            placeholder={field.placeholder}
            style={{ width: "100%" }}
            value={value}
            onChange={(e) => {
              form.setFieldValue(field.ID, e);
              setValue(e);
            }}
            showTime
            disabled={
              field.readonly ||
              (field.permission_read && field.permission_write === false)
            }
          />
        </div>
      )}
    </Form.Item>
  );
};

export default Datetime;
