import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { CopyOutlined } from "@ant-design/icons";
import { Notification } from "components/Notification/Noti";
import { BASE_URL_API } from "constants/constants";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Switch from "antd/lib/switch";
import Radio from "antd/lib/radio";
import Tooltip from "antd/lib/tooltip";

import {
  updateConfigCTI,
  resetAllCTI,
  loadFieldsObjectToCall,
  generateTokenC247,
} from "redux/slices/consolidatedViewSettings";
import { loadListObjectField } from "redux/slices/objects";
import { useTranslation } from "react-i18next";
import ModalConfirm from "components/Modal/ModalConfirm";
import ImgConfirm from "assets/icons/common/confirm.png";
import { setShowModalConfirm } from "redux/slices/global";
import DeleteIcon from "assets/icons/common/delete.svg";
// import { loadFieldsCreate } from "redux/slices/consolidatedViewSettings";
// import FieldCreate from "pages/ConsolidatedViewSettings/CoreSetting/FieldCreate";
// import { loadFields } from "redux/slices/report";
const optionsPopup = [
  {
    label: "Default",
    value: "default",
  },
  {
    label: "Search",
    value: "search",
  },
];

function AdvancedConfig({ listObjects, selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { t } = useTranslation();
  const [typeAuthen, setTypeAuthen] = useState("");
  const [typeCall, $typeCall] = useState("");
  const [useThirdParty, setUseThirdParty] = useState(false);
  const [listFields, setListFields] = useState([]);
  const [objectsToCall, $objectsToCall] = useState([]);

  // //Searchcore
  // const [isSearchCore, $isSearchCore] = useState(false);
  // const [typeAuthenSearch, $typeAuthenSearch] = useState("");
  // const [fieldsCreate, setFieldsCreate] = useState([]);
  // const [requiredFieldsCreate, setRequiredFieldsCreate] = useState([]);
  // const [notRequiredFields, setNotRequiredFields] = useState([]);
  // const [mappingFields, setMappingFields] = useState({});
  // const [listUser, setListUser] = useState([]);
  // const [relatedFields, setRelatedFields] = useState([]);

  // const { configCTI, fieldsObjectToCall, fieldsObject } = useSelector(
  //   (state) => state.consolidatedViewSettingsReducer
  // );
  const { configCTI, fieldsObjectToCall, tokenC247 } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  // const { listAllUser } = useSelector((state) => state.userReducer);
  // const { listFieldsObject } = useSelector((state) => state.reportReducer);

  const { userDetail } = useSelector((state) => state.userReducer);

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    // {
    //   label: 'Token',
    //   value: 'token'
    // }
  ];

  // const AuthenticationTypeSearch = [
  //   {
  //     label: "Basic",
  //     value: "BASIC_AUTH",
  //   },
  //   {
  //     label: "Token",
  //     value: "token",
  //   },
  // ];

  const Method = [
    {
      label: "GET",
      value: "GET",
    },
    // {
    //   label: 'POST',
    //   value: 'POST'
    // }
  ];

  // const MethodSearch = [
  //   {
  //     label: "GET",
  //     value: "GET",
  //   },
  //   {
  //     label: "POST",
  //     value: "POST",
  //   },
  // ];

  const _onSubmit = (values) => {
    // let tempFields = requiredFieldsCreate.concat(notRequiredFields);
    // let tempKeyFields = [];

    // tempFields.forEach((item) => {
    //   tempKeyFields.push(item.value);
    // });

    // let tempMapping = Object.keys(mappingFields);
    // let resultMappingCreate = { ...mappingFields };

    // tempMapping.forEach((item) => {
    //   if (tempKeyFields.includes(item) === false) {
    //     delete resultMappingCreate[item];
    //   }
    // });

    dispatch(
      updateConfigCTI({
        tenant_id: userDetail.tenant_id,
        tenant_cti: values.company_ID,
        object_id: selectedObject,
        type: values.type,
        ip_address: values.ip,
        onlyCallOutWhenReady: values.call_out,
        field_id: values.id_fields,
        check_3rd: useThirdParty,
        auth_type: values.authentication_type,
        username: values.user_name,
        password: _.get(values, "password", undefined),
        api_url: values.api_url,
        method: values.method,
        params: values.params,
        popup_call: values.popup_call,
        object_to_call: objectsToCall,
        // search_core: isSearchCore,

        // search_core_config: {
        //   object_create_record: _.get(
        //     values,
        //     "object_create_record_search",
        //     undefined
        //   ),
        //   api_url: _.get(values, "api_url_search", undefined),
        //   key_object: _.get(values, "key_object_search", undefined),
        //   user_filter: _.get(values, "user_filter_search", undefined),
        //   authentication_type: _.get(
        //     values,
        //     "authentication_type_search",
        //     undefined
        //   ),
        //   user_name: _.get(values, "user_name_search", undefined),
        //   password: _.get(values, "password_search", undefined),
        //   token: _.get(values, "token_search", undefined),
        //   method: _.get(values, "method_search", undefined),
        //   mapping_create: isSearchCore ? resultMappingCreate : undefined,
        // },
      })
    );
  };

  const onSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const onSelectTypeCall = (e) => {
    $typeCall(e);
  };

  // const onSelectAuthenTypeSearch = (e) => {
  //   $typeAuthenSearch(e);
  // };

  // const onUseSearchCore = (checked) => {
  //   $isSearchCore(checked);
  // };

  // const handleSelectObjectCreateRecord = (value, edit) => {
  //   dispatch(
  //     loadFieldsCreate({
  //       object_id: value,
  //     })
  //   );

  //   if (!edit) {
  //     setNotRequiredFields([]);
  //     form.setFieldsValue({
  //       notRequiredFieldCreate: [],
  //     });
  //   }
  // };

  // const onChangeValue = (label, value, type) => {
  //   let arr = { ...mappingFields };
  //   arr[label] = value;
  //   setMappingFields(arr);
  // };

  // const onSelectField = (e, option) => {
  //   setNotRequiredFields(option);
  // };

  const typeCalls = [
    {
      label: "JABBER",
      value: "Jabber",
    },
    {
      label: "C247 - Standard",
      value: "C247Standard",
    },
    {
      label: "C247 - Cloud",
      value: "C247Cloud",
    },
    {
      label: "FINESSE",
      value: "Finesse",
    },
  ];

  const onUseThirdParty = (checked) => {
    setUseThirdParty(checked);
  };

  const _onAddObjectToCall = () => {
    form.setFieldsValue({
      ["objectToCall_object_" + objectsToCall.length]: undefined,
      ["objectToCall_fields_" + objectsToCall.length]: undefined,
    });

    $objectsToCall([
      ...objectsToCall,
      {
        object_id: undefined,
        field_id: undefined,
      },
    ]);
  };

  const _onDeleteObjectToCall = (idx) => {
    let temp = [...objectsToCall];
    temp.splice(idx, 1);

    temp.forEach((item, index) => {
      form.setFieldsValue({
        ["objectToCall_object_" + index]: item.object_id,
        ["objectToCall_fields_" + index]: item.field_id,
      });
    });

    $objectsToCall(temp);
  };

  const handleChangeObjectToCall = (value, type, index) => {
    let temp = [...objectsToCall];

    temp[index] = {
      ...temp[index],
      [type]: value,
    };
    $objectsToCall(temp);

    if (type === "object_id") {
      dispatch(
        loadFieldsObjectToCall({
          api_version: "2",
          object_id: value,
        })
      );
    }
  };

  const _onGenerateToken = () => {
    dispatch(generateTokenC247({}));
  };

  useEffect(() => {
    if (configCTI.length > 0) {
      form.setFieldsValue({
        company_ID: configCTI[0].tenant_cti,
        type: configCTI[0].type,
        ip: configCTI[0].ip_address,
        call_out: configCTI[0].onlyCallOutWhenReady,
        id_fields: configCTI[0].field_id,
        third_party: configCTI[0].check_3rd,
        popup_call: configCTI[0]?.popup_call,
      });
      $typeCall(configCTI[0].type);
      setUseThirdParty(configCTI[0].check_3rd);

      if (configCTI[0].check_3rd) {
        form.setFieldsValue({
          authentication_type: configCTI[0].third_party.auth_type,
          user_name: configCTI[0].third_party.username,
          // password: configCTI[0].third_party.password,
          api_url: configCTI[0].third_party.api_url,
          method: configCTI[0].third_party.method,
          params: configCTI[0].third_party.params,
        });
        setTypeAuthen(configCTI[0].third_party.auth_type);
      }

      if (configCTI[0]?.object_to_call?.length > 0) {
        configCTI[0].object_to_call.forEach((item, index) => {
          dispatch(
            loadFieldsObjectToCall({
              api_version: "2",
              object_id: item.object_id,
            })
          );

          form.setFieldsValue({
            ["objectToCall_object_" + index]: item.object_id,
            ["objectToCall_fields_" + index]: item.field_id,
          });
        });

        $objectsToCall(_.get(configCTI, "[0].object_to_call", []));
      }

      // if (_.get(configCTI[0], "search_core", false)) {
      //   let tempMappingCreate = {};
      //   let tempNotRequiredFields = [];
      //   let tempSelectFieldsCreate = [];

      //   _.get(configCTI[0], "search_core_config.mapping_create", []).forEach(
      //     (item) => {
      //       tempMappingCreate[item.value] = item.selected_value;
      //       form.setFieldsValue({
      //         [item.value]: item.selected_value,
      //       });

      //       if (item.required === false) {
      //         tempSelectFieldsCreate.push(item);
      //         tempNotRequiredFields.push(item.value);
      //       }
      //     }
      //   );
      //   setNotRequiredFields(tempSelectFieldsCreate);
      //   setMappingFields(tempMappingCreate);

      //   form.setFieldsValue({
      //     user_filter_search: _.get(
      //       configCTI[0],
      //       "search_core_config.user_filter"
      //     ),
      //     object_create_record_search: _.get(
      //       configCTI[0],
      //       "search_core_config.object_create_record"
      //     ),
      //     api_url_search: _.get(configCTI[0], "search_core_config.api_url"),
      //     key_object_search: _.get(
      //       configCTI[0],
      //       "search_core_config.key_object"
      //     ),
      //     authentication_type_search: _.get(
      //       configCTI[0],
      //       "search_core_config.authentication_type"
      //     ),
      //     user_name_search: _.get(configCTI[0], "search_core_config.user_name"),
      //     method_search: _.get(configCTI[0], "search_core_config.method"),
      //     notRequiredFieldCreate: tempNotRequiredFields,
      //   });
      //   $typeAuthenSearch(
      //     _.get(configCTI[0], "search_core_config.authentication_type")
      //   );
      //   handleSelectObjectCreateRecord(
      //     _.get(configCTI[0], "search_core_config.object_create_record"),
      //     true
      //   );
      // }

      // $isSearchCore(_.get(configCTI[0], "search_core", false));
    } else {
      form.resetFields();
      setUseThirdParty(false);
      $objectsToCall([]);
      $typeCall("");
      // $isSearchCore(false);
      // $typeAuthenSearch("");
      // setFieldsCreate([]);
      // setRequiredFieldsCreate([]);
      // setNotRequiredFields([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [configCTI, form, dispatch]);

  // useEffect(() => {
  //   if (fieldsObject !== null) {
  //     let tempFieldsCreate = [];
  //     let listRequiredFieldsCreate = [];
  //     fieldsObject.forEach((item) => {
  //       if (item.type !== "id") {
  //         if (item.required) {
  //           listRequiredFieldsCreate.push({
  //             label: item.name,
  //             value: item.ID,
  //             type: item.type,
  //             option: item.option || [],
  //             required: item.required,
  //           });
  //         } else {
  //           tempFieldsCreate.push({
  //             label: item.name,
  //             value: item.ID,
  //             type: item.type,
  //             option: item.option || [],
  //           });
  //         }
  //       }
  //     });
  //     setFieldsCreate(tempFieldsCreate);
  //     setRequiredFieldsCreate(listRequiredFieldsCreate);
  //   }
  // }, [fieldsObject]);

  // useEffect(() => {
  //   let tempUser = [];
  //   listAllUser.forEach((item) => {
  //     if (item.Active) {
  //       tempUser.push({
  //         label:
  //           item.Middle_Name === ""
  //             ? item.Last_Name + " " + item.First_Name
  //             : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
  //         value: item._id,
  //       });
  //     }
  //   });
  //   setListUser(tempUser);
  // }, [listAllUser]);

  // useEffect(() => {
  //   let tempFields = [];
  //   listFieldsObject.forEach((item) => {
  //     if (
  //       Object.values(item)[0] !== null &&
  //       (Object.values(item)[0].readable || Object.values(item)[0].writeable)
  //     ) {
  //       Object.values(item)[0].sections.forEach((ele) => {
  //         ele.fields.forEach((field) => {
  //           if (field.hidden === false && field.permission_hidden === false) {
  //             tempFields.push({
  //               label: field.related_name,
  //               value: field.full_field_id,
  //               type: field.type,
  //             });
  //           }
  //         });
  //       });
  //     }
  //   });
  //   setRelatedFields(tempFields);
  // }, [listFieldsObject]);

  // useEffect(() => {
  //   if (selectedObject) {
  //     dispatch(
  //       loadFields({
  //         api_version: "2",
  //         object_id: selectedObject,
  //       })
  //     );
  //   }
  // }, [dispatch, selectedObject]);

  useEffect(() => {
    if (selectedObject) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: selectedObject,
        })
      );
    }
  }, [dispatch, selectedObject]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  return (
    <Form
      form={form}
      onFinish={_onSubmit}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      colon={false}
      labelAlign="left"
    >
      <Wrapper>
        <BasicInfo>
          <Form.Item label="BE UCRM Link" colon={false}>
            <Input value={BASE_URL_API} disabled />

            <div
              style={{
                position: "absolute",
                top: "6px",
                right: "6px",
                cursor: "pointer",
                background: "#fff",
              }}
              onClick={() => {
                navigator.clipboard.writeText(BASE_URL_API);
                Notification("success", "Copy success!");
              }}
            >
              <CopyOutlined />
            </div>
          </Form.Item>

          <Form.Item
            label={t("CTISetting.company")}
            name="company_ID"
            rules={[
              {
                required: true,
                message: t("CTISetting.messCompany"),
              },
            ]}
          >
            <Input
              disabled={_.isEqual(_.get(configCTI, "[0].type"), "C247Cloud")}
            />
          </Form.Item>

          <Form.Item
            label={t("CTISetting.type")}
            name="type"
            rules={[{ required: true, message: t("CTISetting.messType") }]}
          >
            <Select
              placeholder={t("CTISetting.placeholderType")}
              options={typeCalls}
              disabled={_.isEqual(_.get(configCTI, "[0].type"), "C247Cloud")}
              onChange={(e) => onSelectTypeCall(e)}
            />
          </Form.Item>

          {typeCall === "C247Cloud" && (
            <>
              <Form.Item
                label={<Button onClick={_onGenerateToken}>Get token</Button>}
                colon={false}
              >
                <Input
                  value={tokenC247}
                  disabled
                  style={{ paddingRight: "30px" }}
                />

                <div
                  style={{
                    position: "absolute",
                    top: "6px",
                    right: "6px",
                    cursor: "pointer",
                    background: "#fff",
                  }}
                  onClick={() => {
                    navigator.clipboard.writeText(tokenC247);
                    Notification("success", "Copy token success!");
                  }}
                >
                  <CopyOutlined />
                </div>
              </Form.Item>
            </>
          )}

          <Form.Item
            label={t("CTISetting.link")}
            name="ip"
            rules={[{ required: true, message: t("CTISetting.messLinkApi") }]}
          >
            <Input
              disabled={_.isEqual(_.get(configCTI, "[0].type"), "C247Cloud")}
            />
          </Form.Item>

          <Form.Item
            label={t("CTISetting.call")}
            name="call_out"
            valuePropName="checked"
          >
            <Switch defaultChecked={true} />
          </Form.Item>

          <Form.Item label="Popup new contact" name="popup_call">
            <Select
              placeholder={t("common.placeholderSelect")}
              options={optionsPopup}
              defaultValue="default"
            />
          </Form.Item>

          <Form.Item label={t("CTISetting.addFields")} name="id_fields">
            <Select
              mode="multiple"
              placeholder={t("CTISetting.placeholderAddfields")}
              allowClear
              options={listFields}
              optionFilterProp="label"
              disabled={_.isEqual(_.get(configCTI, "[0].type"), "C247Cloud")}
            />
          </Form.Item>

          <AddNew onClick={() => _onAddObjectToCall()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ Add object click to call</span>
          </AddNew>

          {objectsToCall.length > 0 &&
            objectsToCall.map((objectToCall, idx) => {
              return (
                <FormObjectToCall key={idx}>
                  <Form.Item
                    label="Object"
                    name={`objectToCall_object_${idx}`}
                    rules={[{ required: true, message: "Please select!" }]}
                    style={{ width: "45%" }}
                  >
                    <Select
                      placeholder="Please select"
                      onChange={(e) =>
                        handleChangeObjectToCall(e, "object_id", idx)
                      }
                    >
                      {listObjects
                        ?.filter((item) => item?.value !== selectedObject)
                        ?.map((option) => {
                          return (
                            <Select.Option
                              disabled={
                                objectsToCall.find(
                                  (item) => item?.object_id === option?.value
                                )
                                  ? true
                                  : false
                              }
                              key={option.value}
                            >
                              {option.label}
                            </Select.Option>
                          );
                        })}
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Fields"
                    name={`objectToCall_fields_${idx}`}
                    rules={[
                      {
                        required: true,
                        message: "Please select!",
                      },
                    ]}
                    style={{ width: "45%" }}
                  >
                    <Select
                      options={
                        fieldsObjectToCall.find(
                          (item) => item?.object_id === objectToCall?.object_id
                        )?.fields
                      }
                      mode="multiple"
                      placeholder="Please select"
                      onChange={(e) =>
                        handleChangeObjectToCall(e, "field_id", idx)
                      }
                    />
                  </Form.Item>

                  <Delete>
                    <Tooltip title="Delete">
                      <img
                        src={DeleteIcon}
                        onClick={() => _onDeleteObjectToCall(idx)}
                        alt="delete"
                      />
                    </Tooltip>
                  </Delete>
                </FormObjectToCall>
              );
            })}
        </BasicInfo>
        <Right>
          <ThirdParty useThirdParty={useThirdParty}>
            {useThirdParty ? (
              <>
                <legend>
                  <Form.Item
                    labelCol={{ span: 18 }}
                    wrapperCol={{ span: 4 }}
                    label={t("CTISetting.use")}
                    name="third_party"
                  >
                    <Switch
                      checked={useThirdParty}
                      onChange={onUseThirdParty}
                    />
                  </Form.Item>
                </legend>
                <WrapThirdParty>
                  <Form.Item
                    label={t("CTISetting.authen")}
                    name="authentication_type"
                    rules={[
                      {
                        required: true,
                        message: t("CTISetting.mess"),
                      },
                    ]}
                  >
                    <Select
                      placeholder={t("CTISetting.placeholder")}
                      options={AuthenticationType}
                      onChange={(e) => onSelectAuthenType(e)}
                    ></Select>
                  </Form.Item>

                  {typeAuthen === "basic" && (
                    <>
                      <Form.Item
                        label={t("CTISetting.user")}
                        name="user_name"
                        rules={[
                          {
                            required: true,
                            message: t("CTISetting.messUserName"),
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item
                        label={t("CTISetting.password")}
                        name="password"
                      >
                        <Input.Password />
                      </Form.Item>
                    </>
                  )}

                  <Form.Item
                    label="API URL"
                    name="api_url"
                    rules={[
                      { required: true, message: t("CTISetting.messAPIURL") },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label={t("CTISetting.method")}
                    name="method"
                    rules={[
                      { required: true, message: t("CTISetting.messMethod") },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={Method}
                    ></Radio.Group>
                  </Form.Item>

                  <Form.Item
                    label="Params"
                    name="params"
                    rules={[
                      { required: true, message: "Please input Params!" },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                </WrapThirdParty>
              </>
            ) : (
              <Form.Item
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 4 }}
                label={t("CTISetting.use")}
                name="third_party"
              >
                <Switch checked={useThirdParty} onChange={onUseThirdParty} />
              </Form.Item>
            )}
          </ThirdParty>

          {/* <ThirdParty useThirdParty={isSearchCore}>
            {isSearchCore ? (
              <>
                <legend>
                  <Form.Item
                    labelCol={{ span: 18 }}
                    wrapperCol={{ span: 4 }}
                    label="Search core config"
                    name="search_core"
                  >
                    <Switch checked={isSearchCore} onChange={onUseSearchCore} />
                  </Form.Item>
                </legend>
                <WrapThirdParty>
                  <Form.Item
                    label="Object create record"
                    name="object_create_record_search"
                    rules={[
                      {
                        required: true,
                        message: "Please select Object create record!",
                      },
                    ]}
                  >
                    <Select
                      options={listObjects}
                      onChange={handleSelectObjectCreateRecord}
                    ></Select>
                  </Form.Item>

                  <Form.Item
                    label="Authentication type"
                    name="authentication_type_search"
                    rules={[
                      {
                        required: true,
                        message: "Please select authentication type!",
                      },
                    ]}
                  >
                    <Select
                      options={AuthenticationTypeSearch}
                      onChange={(e) => onSelectAuthenTypeSearch(e)}
                    ></Select>
                  </Form.Item>

                  {typeAuthenSearch === "BASIC_AUTH" && (
                    <>
                      <Form.Item label="User name" name="user_name_search">
                        <Input />
                      </Form.Item>

                      <Form.Item label="Password" name="password_search">
                        <Input.Password />
                      </Form.Item>
                    </>
                  )}

                  {typeAuthenSearch === "token" && (
                    <Form.Item label="Token" name="token_search">
                      <Input />
                    </Form.Item>
                  )}

                  <Form.Item
                    label="API URL"
                    name="api_url_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input API URL!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Key of object"
                    name="key_object_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input Key of object!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="User filter"
                    name="user_filter_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input User filter!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Method"
                    name="method_search"
                    rules={[
                      { required: true, message: "Please select method!" },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={MethodSearch}
                    ></Radio.Group>
                  </Form.Item>

                  {requiredFieldsCreate.length > 0 && (
                    <>
                      {requiredFieldsCreate.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </>
                  )}

                  <Form.Item
                    label="Mapping fields create"
                    name="notRequiredFieldCreate"
                    style={{ marginTop: "8px" }}
                  >
                    <Select
                      mode="multiple"
                      placeholder={t("coreSetting.placeholderAddfields")}
                      optionFilterProp="label"
                      options={fieldsCreate}
                      onChange={(e, option) => onSelectField(e, option)}
                    />
                  </Form.Item>

                  {notRequiredFields.length > 0 && (
                    <WrapFieldUpdate>
                      {notRequiredFields.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </WrapFieldUpdate>
                  )}
                </WrapThirdParty>
              </>
            ) : (
              <Form.Item
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 4 }}
                label="Search core config"
                name="search_core"
              >
                <Switch checked={isSearchCore} onChange={onUseSearchCore} />
              </Form.Item>
            )}
          </ThirdParty> */}
        </Right>
      </Wrapper>

      <WrapButton>
        <Button
          disabled={
            (Object.keys(configCTI)?.length > 0 && !checkRule("edit")) ||
            (Object.keys(configCTI)?.length === 0 && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {configCTI && Object.keys(configCTI)?.length > 0
            ? t("common.update")
            : t("common.save")}
        </Button>

        <Button
          style={{ marginLeft: "16px" }}
          disabled={!checkRule("delete")}
          onClick={() => dispatch(setShowModalConfirm(true))}
        >
          {t("common.reset")}
        </Button>
      </WrapButton>

      <ModalConfirm
        title={t("common.confirmReset")}
        decs={t("common.afterReset")}
        method={resetAllCTI}
        data={{}}
        img={ImgConfirm}
      />
    </Form>
  );
}

export default AdvancedConfig;

const Wrapper = styled.div`
  display: flex;
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;

  .ant-form-item {
    margin-bottom: 8px;

    :last-child {
      margin-bottom: 0;
    }
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const BasicInfo = styled.div`
  flex: 1;
  height: fit-content;
  margin-right: 16px;
`;

const ThirdParty = styled.fieldset`
  /* .ant-form-item-label > label {
    font-family: var(--roboto-700);
  } */
  flex: 1;
  background: #fff;
  border-radius: 10px;
  height: fit-content;
  border: ${({ useThirdParty }) =>
    useThirdParty ? "1px solid #d9d9d9" : "none"};

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
    /* padding-right: 24px; */
  }
`;

const WrapButton = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 83px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn {
    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const WrapThirdParty = styled.div`
  padding: 16px;
`;

const AddNew = styled.div`
  margin-bottom: 8px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const FormObjectToCall = styled(Form.Item)`
  width: 100%;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
  }

  .ant-col {
    max-width: 100% !important;
  }

  .ant-form-item {
    margin-bottom: 0;
  }

  .ant-form-item-row {
    flex-direction: column;
  }
`;

const Right = styled.div`
  flex: 1;
`;

// const WrapField = styled.div`
//   display: flex;
//   flex-direction: column;
//   border: 1px solid #d9d9d9;
//   border-radius: 5px;
//   padding: 8px;
//   max-height: 90px;
//   overflow-y: auto;
//   margin-bottom: 8px;

//   .ant-form-item {
//     :last-child {
//       margin-bottom: 0;
//     }
//   }
// `;

// const WrapFieldUpdate = styled(WrapField)`
//   max-height: unset;
//   .ant-form-item {
//     :last-child {
//       margin-bottom: 8px;
//     }
//   }
// `;
