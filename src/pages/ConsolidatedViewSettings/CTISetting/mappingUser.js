import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Empty from "antd/lib/empty";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Space from "antd/lib/space";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EmptyMapping from "assets/images/consolidatedViewSetting/empty_user_mapping.png";

import {
  createUserMappingCTI,
  resetStatus,
  deleteUserMappingCTI,
  loadDetailsUserMappingCTI,
  updateUserMappingCTI,
  loadUsersMappingCTI,
} from "redux/slices/consolidatedViewSettings";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { useTranslation } from "react-i18next";

function MappingUser({ selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { Column } = Table;
  const { t } = useTranslation();
  const {
    userMappingCTI,
    statusUserMappingCTI,
    loading,
    detailsUserMappingCTI,
  } = useSelector((state) => state.consolidatedViewSettingsReducer);

  const [dataTable, setDataTable] = useState([]);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const [listUser, setListUser] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [isEdit, setIsEdit] = useState(false);

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateUserMappingCTI({
          _id: detailsUserMappingCTI._id,
          new_user_id: values.user_name,
          new_agent: values.agent_name,
          new_extension: values.extension,
          new_accessKey: _.get(values, "access_key", undefined),
        })
      );
    } else {
      dispatch(
        createUserMappingCTI({
          user_id: values.user_name,
          object_id: selectedObject,
          username: values.agent_name,
          extension: values.extension,
          accessKey: values.access_key,
        })
      );
    }
  };

  const _onDelete = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  const _onEdit = (id) => {
    setIsEdit(true);
    dispatch(
      loadDetailsUserMappingCTI({
        user_id: id,
      })
    );
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    let tempList = [];
    userMappingCTI.map((item) => {
      return tempList.push({
        key: item._id,
        user_id: item.user_id,
        user_name: item.user_name,
        agent_name: item.username,
        extension: item.extension,
        // access_key: item.accessKey,
        created_by: item.create_by_name,
        created_date: item.created_date,
        action: "",
      });
    });
    setDataTable(tempList);
  }, [userMappingCTI]);

  useEffect(() => {
    if (statusUserMappingCTI === "success") {
      form.resetFields();
      setIsEdit(false);
      dispatch(resetStatus());
      dispatch(
        loadUsersMappingCTI({
          object_id: selectedObject,
        })
      );
    }
  }, [statusUserMappingCTI, form, dispatch, selectedObject]);

  useEffect(() => {
    if (isEdit && detailsUserMappingCTI !== null) {
      form.setFieldsValue({
        user_name: detailsUserMappingCTI.user_id,
        agent_name: detailsUserMappingCTI.username,
        extension: detailsUserMappingCTI.extension,
        // access_key: detailsUserMappingCTI.accessKey,
      });
    }
  }, [isEdit, detailsUserMappingCTI, form]);

  useEffect(() => {
    form.resetFields();
    setIsEdit(false);
  }, [selectedObject, form]);

  return (
    <>
      <FormCustom
        form={form}
        onFinish={_onSubmit}
        colon={false}
        labelAlign="left"
        labelCol={{ lg: { span: 12 }, xl: { span: 10 }, xxl: { span: 8 } }}
        wrapperCol={{ lg: { span: 12 }, xl: { span: 14 }, xxl: { span: 16 } }}
      >
        <Space size={40}>
          <Form.Item
            label={t("CTISetting.user")}
            name="user_name"
            rules={[
              {
                required: true,
                message: "Please select User name!",
              },
            ]}
          >
            <Select
              placeholder={t("CTISetting.select")}
              options={listUser}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>
          <Form.Item
            label="Agent name"
            name="agent_name"
            rules={[{ required: true, message: "Please input Agent name!" }]}
          >
            <Input />
          </Form.Item>
        </Space>
        <Space size={40}>
          <Form.Item
            label="Extension"
            name="extension"
            rules={[{ required: true, message: "Please input Extension!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label={t("CTISetting.access")} name="access_key">
            <Input.Password autocomplete="new-password" />
          </Form.Item>
        </Space>

        <Button
          disabled={
            (isEdit && !checkRule("edit")) || (!isEdit && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {isEdit ? t("common.update") : t("common.save")}
        </Button>
      </FormCustom>

      {userMappingCTI?.length > 0 ? (
        <UserMapping>
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title="User name"
              dataIndex="user_name"
              key="user_name"
              width="15%"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Agent name"
              dataIndex="agent_name"
              key="agent_name"
              width="15%"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Extension"
              dataIndex="extension"
              key="extension"
              width="15%"
            />
            {/* <Column
              title="Access key"
              dataIndex="access_key"
              key="access_key"
              width="15%"
              render={(text) => <div className="column">{text}</div>}
            /> */}
            <Column
              title="Create by"
              dataIndex="created_by"
              key="created_by"
              width="15%"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Create time"
              dataIndex="created_date"
              key="created_date"
              width="15%"
              render={(text) => <div className="column">{text}</div>}
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title="Action"
                dataIndex="action"
                key="action"
                width="10%"
                fixed="right"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title="Edit">
                        <img
                          onClick={() => _onEdit(record.user_id)}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title="Delete">
                        <img
                          onClick={() => _onDelete(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
        </UserMapping>
      ) : (
        <WrapEmpty>
          <Empty
            image={EmptyMapping}
            description={
              <p>
                {t("CTISetting.there")} <span> {t("CTISetting.map")} </span>
              </p>
            }
          ></Empty>
        </WrapEmpty>
      )}

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteUserMappingCTI}
        dataDelete={dataDelete}
        isLoading={loading.userMappingIC}
      />
    </>
  );
}

export default MappingUser;

const FormCustom = styled(Form)`
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;
  display: flex;
  flex-direction: column;

  .ant-space-item {
    flex: 1;
    margin-bottom: 16px;
  }

  .ant-form-item {
    flex: 1;
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    width: 139px;
    align-self: center;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const UserMapping = styled.div`
  background: #fff;
  border-radius: 10px;
  margin-top: 16px;

  .column {
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  .ant-table-wrapper {
    flex: 1;
    padding: 16px;
  }
`;

const WrapEmpty = styled.div`
  background: #fff;
  border-radius: 10px;
  min-height: 218px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;
    width: 18px;

    :hover {
      background: #eeeeee;
    }
  }
`;
