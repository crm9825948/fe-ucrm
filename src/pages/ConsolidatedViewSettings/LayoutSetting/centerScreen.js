import { Button, Tabs } from "antd";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import close from "assets/images/consolidatedView/close.png";
import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import _ from "lodash";
import { default as React, useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch, useSelector } from "react-redux";
import {
  loadLayouts,
  updateLayout,
} from "redux/slices/consolidatedViewSettings";
import { setShowLoadingScreen } from "redux/slices/global";
import styled from "styled-components";
import DrawerComponent from "./drawerComponent";
import DrawerComponentTop from "./drawerComponentTop";
import DrawerComponentLeft from "./drawerComponentLeft";
import TabComponent from "./tabComponent";
import { Notification } from "components/Notification/Noti";

const ReactGridLayout = WidthProvider(RGL);

const { TabPane } = Tabs;

const CenterScreen = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  checkRule,
  onLayoutChange = function () {},
  ...props
}) => {
  const [visible, setVisible] = useState(false);
  const [visibleTop, setVisibleTop] = useState(false);
  const [visibleLeft, setVisibleLeft] = useState(false);
  const [visibleRight, setVisibleRight] = useState(false);
  // const [visibleTab, setVisibleTab] = useState(false);
  const { selectedObject, tabKey } = props;
  let dispatch = useDispatch();
  const [items, setItems] = useState([]);
  const [itemsTop, setItemsTop] = useState([]);
  const [itemsLeft, setItemsLeft] = useState([]);
  const [itemsRight, setItemsRight] = useState([]);
  const { layouts, loadingComponents, isLoadingUpdate, isLoadingLoadLayout } =
    useSelector((state) => state.consolidatedViewSettingsReducer);

  const handleUpdate = (items) => {
    let result = [];
    /*eslint-disable-next-line*/
    items.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: selectedObject,
        placement: "middle",
        components: result,
        fix_top_component: itemsTop,
        fix_left_component: itemsLeft,
        fix_right_component: itemsRight,
      })
    );
  };

  const handleUpdateTop = (itemsTop) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsTop.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: selectedObject,
        placement: "middle",
        components: items,
        fix_top_component: result,
        fix_left_component: itemsLeft,
        fix_right_component: itemsRight,
      })
    );
  };

  const handleUpdateLeft = (itemsLeft) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsLeft.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: selectedObject,
        placement: "middle",
        components: items,
        fix_top_component: itemsTop,
        fix_left_component: result,
        fix_right_component: itemsRight,
      })
    );
  };

  const handleUpdateRight = (itemsRight) => {
    let result = [];
    /*eslint-disable-next-line*/
    itemsRight.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: selectedObject,
        placement: "middle",
        components: items,
        fix_top_component: itemsTop,
        fix_left_component: itemsLeft,
        fix_right_component: result,
      })
    );
  };

  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = [...layouts.components];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItems(tmpArr);
    } else if (layouts.length === 0) {
      setItems([]);
    }
  }, [layouts]);

  //top
  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_top_component ? [...layouts.fix_top_component] : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsTop(tmpArr);
    } else if (layouts.length === 0) {
      setItemsTop([]);
    }
  }, [layouts]);

  //left
  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_left_component
        ? [...layouts.fix_left_component]
        : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsLeft(tmpArr);
    } else if (layouts.length === 0) {
      setItemsLeft([]);
    }
  }, [layouts]);

  //right
  useEffect(() => {
    if (layouts.placement === "middle") {
      let tmp = layouts.fix_right_component
        ? [...layouts.fix_right_component]
        : [];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItemsRight(tmpArr);
    } else if (layouts.length === 0) {
      setItemsRight([]);
    }
  }, [layouts]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
  }, [loadingComponents, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingUpdate));
  }, [isLoadingUpdate, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingLoadLayout));
  }, [isLoadingLoadLayout, dispatch]);

  useEffect(() => {
    if (tabKey === "2") {
      dispatch(
        loadLayouts({
          object_id: selectedObject,
          placement: "middle",
        })
      );
      setReload(0);
    }
  }, [selectedObject, dispatch, tabKey]);

  const [key, setKey] = useState(0);

  function callback(key) {
    setKey(key);
  }

  const createElement = (el, position) => {
    if (JSON.parse(el.i).type === "tab") {
      let data = JSON.parse(el.i);
      return (
        <CustomItem
          key={el.i}
          data-grid={el}
          style={{
            overflow: "hidden",
            boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
            borderRadius: "10px",
            height: "100%",
            position: "relative",
          }}
        >
          {checkRule("delete") ? (
            <img
              alt=""
              src={close}
              style={{
                width: "24px",
                position: "absolute",
                right: "10px",
                top: "10px",
                cursor: "pointer",
                zIndex: 11,
              }}
              onClick={() => {
                let flag = false;

                JSON.parse(el.i).tab_name.map((item, idx) => {
                  if (JSON.parse(el.i)?.tab_component?.item?.length > 0) {
                    flag = true;
                  }
                  return null;
                });

                /* eslint-disable-next-line */
                // if (JSON.parse(el.i).tab_component)
                //   Object.entries(JSON.parse(el.i).tab_component).forEach(
                //     /* eslint-disable-next-line */
                //     ([key, value], index) => {
                //       if (value.length > 0) {
                //         flag = true;
                //       }
                //     }
                //   );
                if (flag === false) {
                  switch (position) {
                    case "top":
                      onRemoveItemTop(el.i);
                      break;
                    case "left":
                      onRemoveItemLeft(el.i);
                      break;
                    case "right":
                      onRemoveItemRight(el.i);
                      break;
                    case "normal":
                      onRemoveItem(el.i);
                      break;
                    default:
                      break;
                  }
                } else {
                  Notification("error", "Can not delete this component!");
                }
              }}
              className="close-png"
            />
          ) : (
            ""
          )}
          <Tabs defaultActiveKey={key} onChange={callback}>
            {data.tab_name.map((item, idx) => {
              return (
                <TabPane tab={item} key={idx}>
                  <TabComponent
                    data={JSON.parse(el.i)}
                    index={idx}
                    name={item}
                    itemParent={items}
                    setItemParent={setItems}
                    selectedObject={selectedObject}
                    checkRule={checkRule}
                  />
                </TabPane>
              );
            })}
          </Tabs>
        </CustomItem>
      );
    } else
      return (
        <div
          key={el.i}
          data-grid={el}
          style={{
            overflow: "hidden",
            boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
            borderRadius: "10px",
            height: "100%",
          }}
        >
          <div
            style={{
              height: "40px",
              background: "#D9D9D9",
              display: "flex",
              alignItems: "center",
              fontStyle: "normal",
              fontWeight: "500",
              fontSize: "18px",
              lineHeight: "26px",
              color: "#252424",
              paddingLeft: "15px",
              width: "100%",
              justifyContent: "space-between",
              paddingRight: "15px",
            }}
          >
            {JSON.parse(el.i).name}{" "}
            {checkRule("delete") ? (
              <img
                alt=""
                src={close}
                style={{ width: "24px" }}
                onClick={() => {
                  switch (position) {
                    case "top":
                      onRemoveItemTop(el.i);
                      break;
                    case "left":
                      onRemoveItemLeft(el.i);
                      break;
                    case "right":
                      onRemoveItemRight(el.i);
                      break;
                    case "normal":
                      onRemoveItem(el.i);
                      break;
                    default:
                      break;
                  }
                }}
                className="close-png"
              />
            ) : (
              ""
            )}
          </div>
          <div style={{ padding: "0", height: "100%" }}>
            <div
              style={{
                height: "100%",
                background: "#c4c4c4",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <img alt="" src={Triangle} style={{ width: "50%" }} />
            </div>
          </div>
        </div>
      );
  };

  const onAddItem = (newItem) => {
    if (checkRule("create")) {
      setItems(items.concat(newItem));
      handleUpdate(items.concat(newItem));
    }
  };

  const onAddItemTop = (newItem) => {
    if (checkRule("create")) {
      // setItems(items.concat(newItem));
      setItemsTop(itemsTop.concat(newItem));
      handleUpdateTop(itemsTop.concat(newItem));
    }
  };

  const onAddItemLeft = (newItem) => {
    if (checkRule("create")) {
      // setItems(items.concat(newItem));
      setItemsLeft(itemsLeft.concat(newItem));
      handleUpdateLeft(itemsLeft.concat(newItem));
    }
  };

  const onAddItemRight = (newItem) => {
    if (checkRule("create")) {
      // setItems(items.concat(newItem));
      setItemsRight(itemsRight.concat(newItem));
      handleUpdateRight(itemsRight.concat(newItem));
    }
  };

  const [reload, setReload] = useState(0);

  const onLayoutChangeNewCenter = (layout) => {
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  const [reloadTop, setReloadTop] = useState(0);
  const onLayoutChangeNewCenterTop = (layout) => {
    if (reloadTop === 0) {
    } else if (reloadTop === 1) {
      onLayoutChange(layout);
      setItemsTop(layout);
      handleUpdateTop(layout);
    } else if (reloadTop >= 2 && reloadTop % 2 === 0) {
      onLayoutChange(layout);
      setItemsTop(layout);
      handleUpdateTop(layout);
    }
    setReloadTop(reloadTop + 1);
  };

  const [reloadLeft, setReloadLeft] = useState(0);
  const onLayoutChangeNewCenterLeft = (layout) => {
    if (reloadLeft === 0) {
    } else if (reloadLeft === 1) {
      onLayoutChange(layout);
      setItemsLeft(layout);
      handleUpdateLeft(layout);
    } else if (reloadLeft >= 2 && reloadLeft % 2 === 0) {
      onLayoutChange(layout);
      setItemsLeft(layout);
      handleUpdateLeft(layout);
    }
    setReloadLeft(reloadTop + 1);
  };

  const [reloadRight, setReloadRight] = useState(0);
  const onLayoutChangeNewCenterRight = (layout) => {
    if (reloadRight === 0) {
    } else if (reloadRight === 1) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdateRight(layout);
    } else if (reloadRight >= 2 && reloadRight % 2 === 0) {
      onLayoutChange(layout);
      setItemsRight(layout);
      handleUpdateRight(layout);
    }
    setReloadRight(reloadTop + 1);
  };

  const onRemoveItem = (i) => {
    if (checkRule("delete")) {
      if (reload < 2) {
        handleUpdate(_.reject(items, { i: i }));
        setItems(_.reject(items, { i: i }));
      } else if (reload >= 2) {
        setItems(_.reject(items, { i: i }));
      }
    }
  };

  const onRemoveItemTop = (i) => {
    if (checkRule("delete")) {
      // if (reloadTop < 2) {
      //   handleUpdateTop(_.reject(itemsTop, { i: i }));
      //   setItemsTop(_.reject(itemsTop, { i: i }));
      // } else if (reloadTop >= 2) {
      //   setItemsTop(_.reject(itemsTop, { i: i }));
      // }
      handleUpdateTop(_.reject(itemsTop, { i: i }));
      setItemsTop(_.reject(itemsTop, { i: i }));
    }
  };
  const onRemoveItemLeft = (i) => {
    if (checkRule("delete")) {
      // if (reloadLeft < 2) {
      //   handleUpdateLeft(_.reject(itemsLeft, { i: i }));
      //   setItemsLeft(_.reject(itemsLeft, { i: i }));
      // } else if (reload >= 2) {
      //   setItemsLeft(_.reject(itemsLeft, { i: i }));
      // }
      handleUpdateLeft(_.reject(itemsLeft, { i: i }));
      setItemsLeft(_.reject(itemsLeft, { i: i }));
    }
  };
  const onRemoveItemRight = (i) => {
    if (checkRule("delete")) {
      // if (reloadRight < 2) {
      //   handleUpdateRight(_.reject(itemsRight, { i: i }));
      //   setItemsRight(_.reject(itemsRight, { i: i }));
      // } else if (reloadRight >= 2) {
      //   setItemsRight(_.reject(itemsRight, { i: i }));
      // }
      handleUpdateRight(_.reject(itemsRight, { i: i }));
      setItemsRight(_.reject(itemsRight, { i: i }));
    }
  };

  return (
    <>
      <Wrapper>
        {checkRule("create") ? (
          <Button
            className="fixed-btn"
            size="large"
            onClick={() => {
              setVisible(true);
            }}
          >
            <img alt="" src={plusIcon} />
          </Button>
        ) : (
          ""
        )}

        {items.length === 0 ? (
          <div className="no-data-container">
            <img alt="" src={emptyImg} className="empty-layout-img" />
            <div className="no-data-decs">No data. Select component to add</div>
            {checkRule("create") ? (
              <CustomButtonAddRecord
                size="large"
                onClick={() => {
                  setVisible(true);
                }}
              >
                <img alt="" src={plusIcon} />
                Add new
              </CustomButtonAddRecord>
            ) : (
              ""
            )}
          </div>
        ) : (
          <div style={{ height: "1300px", overflow: "auto" }}>
            <div className="top-fixed">
              {itemsTop.length === 0 ? (
                <div
                  style={{
                    width: "100%",
                    minHeight: "200px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  <CustomButtonAddRecord
                    size="large"
                    onClick={() => {
                      setVisibleTop(true);
                    }}
                  >
                    <img alt="" src={plusIcon} />
                    Add new
                  </CustomButtonAddRecord>
                </div>
              ) : (
                <ReactGridLayout
                  // isBounded={true}
                  onLayoutChange={onLayoutChangeNewCenterTop}
                  {...props}
                  isDraggable={checkRule("edit")}
                  isResizable={checkRule("edit")}
                  rowHeight={10}
                  // className="layout"
                  // maxRows={9}
                >
                  {_.map(itemsTop, (el) => createElement(el, "top"))}
                </ReactGridLayout>
              )}
            </div>
            <div className="combine-layout">
              <div className="left-fixed">
                {itemsLeft.length === 0 ? (
                  <div
                    style={{
                      width: "100%",
                      minHeight: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <CustomButtonAddRecord
                      size="large"
                      onClick={() => {
                        setVisibleLeft(true);
                      }}
                    >
                      <img alt="" src={plusIcon} />
                      Add new
                    </CustomButtonAddRecord>
                  </div>
                ) : (
                  <>
                    {" "}
                    <ReactGridLayout
                      // isBounded={true}
                      onLayoutChange={onLayoutChangeNewCenterLeft}
                      {...props}
                      isDraggable={checkRule("edit")}
                      isResizable={checkRule("edit")}
                      rowHeight={10}
                      cols={1}
                    >
                      {_.map(itemsLeft, (el) => createElement(el, "left"))}
                    </ReactGridLayout>{" "}
                    <div
                      style={{
                        width: "100%",
                        minHeight: "200px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <CustomButtonAddRecord
                        size="large"
                        onClick={() => {
                          setVisibleLeft(true);
                        }}
                      >
                        <img alt="" src={plusIcon} />
                        Add new
                      </CustomButtonAddRecord>
                    </div>
                  </>
                )}
              </div>
              <div className="middle-fixed">
                <ReactGridLayout
                  onLayoutChange={onLayoutChangeNewCenter}
                  {...props}
                  isDraggable={checkRule("edit")}
                  isResizable={checkRule("edit")}
                  rowHeight={10}
                >
                  {_.map(items, (el) => createElement(el, "normal"))}
                </ReactGridLayout>
              </div>
              <div className="right-fixed">
                {itemsRight.length === 0 ? (
                  <div
                    style={{
                      width: "100%",
                      minHeight: "200px",
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <CustomButtonAddRecord
                      size="large"
                      onClick={() => {
                        setVisibleRight(true);
                      }}
                    >
                      <img alt="" src={plusIcon} />
                      Add new
                    </CustomButtonAddRecord>
                  </div>
                ) : (
                  <>
                    {" "}
                    <ReactGridLayout
                      // isBounded={true}
                      onLayoutChange={onLayoutChangeNewCenterRight}
                      {...props}
                      isDraggable={checkRule("edit")}
                      isResizable={checkRule("edit")}
                      rowHeight={10}
                      cols={1}
                    >
                      {_.map(itemsRight, (el) => createElement(el, "right"))}
                    </ReactGridLayout>{" "}
                    <div
                      style={{
                        width: "100%",
                        minHeight: "200px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <CustomButtonAddRecord
                        size="large"
                        onClick={() => {
                          setVisibleRight(true);
                        }}
                      >
                        <img alt="" src={plusIcon} />
                        Add new
                      </CustomButtonAddRecord>
                    </div>
                  </>
                )}
              </div>
            </div>
          </div>
        )}

        {checkRule("create") ? (
          <>
            <DrawerComponent
              visible={visible}
              setVisible={setVisible}
              onAddItem={onAddItem}
              items={items}
              selectedObject={selectedObject}
            />
            <DrawerComponentTop
              visible={visibleTop}
              setVisible={setVisibleTop}
              onAddItem={onAddItemTop}
              items={itemsTop}
              selectedObject={selectedObject}
            />
            <DrawerComponentLeft
              visible={visibleLeft}
              setVisible={setVisibleLeft}
              onAddItem={onAddItemLeft}
              items={itemsLeft}
              selectedObject={selectedObject}
            />
            <DrawerComponentLeft
              visible={visibleRight}
              setVisible={setVisibleRight}
              onAddItem={onAddItemRight}
              items={itemsRight}
              selectedObject={selectedObject}
            />
          </>
        ) : (
          ""
        )}
      </Wrapper>
    </>
  );
};

export default CenterScreen;

const Wrapper = styled.div`
  position: relative;
  height: 100%;
  .top-fixed {
    border: 2px dashed ${(props) => props.theme.main};
    border-radius: 10px;
    min-height: 200px;
    height: fit-content;
    width: 100%;
  }
  .combine-layout {
    display: flex;
    width: 100%;
    .left-fixed {
      width: 15%;
      border: 2px dashed ${(props) => props.theme.main};
      border-radius: 10px;
      min-height: 100vh;
      height: fit-content;
      margin-top: 10px;
    }
    .middle-fixed {
      width: 70%;
      height: 100vh;
      overflow: hidden;
      overflow-y: scroll;
    }
    .right-fixed {
      width: 15%;
      border: 2px dashed ${(props) => props.theme.main};
      border-radius: 10px;
      min-height: 100vh;
      height: fit-content;
      margin-top: 10px;
    }
  }
  .fixed-btn {
    position: fixed;
    right: -11px;
    background-color: ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    z-index: 10;
    display: flex;
    justify-content: center;
    align-items: center;
    border-top-left-radius: 60px;
    border-bottom-left-radius: 60px;
    top: 290px;
    img {
      width: 15px;
      margin-right: 8px;
    }
    &:hover {
      background-color: ${(props) => props.theme.darker} !important;
      border-color: ${(props) => props.theme.darker};
      color: #fff;
      /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
    }
  }
  .no-data-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    z-index: 10;
    width: 100%;
    margin-top: 136px;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      margin-bottom: 16px;
    }
  }

  .no-data-container-tab {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    z-index: 10;
    width: 100%;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      margin-bottom: 16px;
    }
  }

  .components {
    padding: 10px;
    border: 1px solid #d9d9d9;
    box-sizing: border-box;
    border-radius: 5px;
    .active {
      border: 1px solid ${(props) => props.theme.main};
    }
  }
  .components-title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;
    margin-top: -25px;
    background-color: white;
    /* Character/Color text main */
    width: fit-content;
    padding-left: 10px;
    padding-right: 10px;
    color: #2c2c2c;
    margin-bottom: 10px;
  }
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
  .react-grid-layout {
    background-color: white;
  }
  .react-grid-item:not(.react-grid-placeholder) {
    background: #fff;
    border: none;
  }
  .close-png {
    :hover {
      cursor: pointer;
    }
  }
`;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomItem = styled.div`
  .ant-tabs > .ant-tabs-nav,
  .ant-tabs > div > .ant-tabs-nav {
    justify-content: unset;
  }
  .ant-tabs > div > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
  }
  .ant-tabs > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: flex-start !important;
    width: 100%;
  }
  .ant-tabs-top > .ant-tabs-nav::before {
    /* display: none; */
    display: block;
  }
  .ant-tabs-tab {
    position: relative;
    display: inline-flex;
    align-items: center;
    padding: 12px 0;
    font-size: 14px;
    background: 0 0;
    border: 0;
    outline: none;
    cursor: pointer;
  }
  .ant-tabs-nav-list {
    position: relative;
    display: flex;
    transition: transform 0.3s;
    border: 0 !important;
    padding: 4px 8px !important;
  }
  .ant-tabs-ink-bar {
    display: unset !important;
    background: ${(props) => props.theme.main} !important;
  }
  .ant-tabs-tab-active {
    background: #fff !important;
    .ant-tabs-tab-btn {
      color: ${(props) => props.theme.main} !important;
    }
  }
`;
