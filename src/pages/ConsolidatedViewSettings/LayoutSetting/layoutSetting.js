import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { loadComponents } from "redux/slices/consolidatedViewSettings";
import styled from "styled-components";
import CenterScreen from "./centerScreen";
import LeftScreen from "./leftScreen";
import RightScreen from "./rightScreen";

const { TabPane } = Tabs;

const LayoutSetting = ({ ...props }) => {
  const { selectedObject, checkRule } = props;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  // const { components, isLoadingDelete } = useSelector(
  //   (state) => state.consolidatedViewSettingsReducer
  // );

  useEffect(() => {
    dispatch(
      loadComponents({
        object_id: selectedObject,
      })
    );
  }, [selectedObject, dispatch]);

  function callback(key) {
    setTabKey(key);
  }
  const [tabKey, setTabKey] = useState("1");
  return (
    <Wrapper>
      <Tabs
        defaultActiveKey={tabKey}
        onChange={callback}
        style={{ marginTop: "23px" }}
        className="tabs-main"
      >
        <TabPane tab={t("layoutSetting.left")} key="1">
          {tabKey === "1" ? (
            <LeftScreen
              selectedObject={selectedObject}
              tabKey={tabKey}
              checkRule={checkRule}
            />
          ) : (
            ""
          )}
        </TabPane>
        <TabPane tab={t("layoutSetting.center")} key="2">
          {tabKey === "2" ? (
            <CenterScreen
              selectedObject={selectedObject}
              tabKey={tabKey}
              checkRule={checkRule}
            />
          ) : (
            ""
          )}
        </TabPane>
        <TabPane tab={t("layoutSetting.right")} key="3">
          {tabKey === "3" ? (
            <RightScreen
              selectedObject={selectedObject}
              tabKey={tabKey}
              checkRule={checkRule}
            />
          ) : (
            ""
          )}
        </TabPane>
      </Tabs>
    </Wrapper>
  );
};

export default LayoutSetting;

const Wrapper = styled.div`
  width: 100%;
  padding: 16px;
  padding-top: 0;
  background-color: white;
  height: 1366px;
  overflow: auto;
  .ant-tabs > .ant-tabs-nav,
  .ant-tabs > div > .ant-tabs-nav {
    justify-content: center;
  }
  .ant-tabs > div > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: center;
    width: 100%;
  }
  .ant-tabs > .ant-tabs-nav .ant-tabs-nav-wrap {
    flex: unset;
    display: flex;
    justify-content: center;
    width: 100%;
  }
  .ant-tabs-top > .ant-tabs-nav::before {
    display: none;
  }
  .ant-tabs-tab {
    padding: 8px 19.5px;
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    display: flex;
    align-items: center;

    /* Character/Color text main */

    color: #2c2c2c;
  }
  .ant-tabs-nav-list {
    border: 1.5px solid #597ef7;
    margin-bottom: 10px;
    box-sizing: border-box;
    border-radius: 5px;
    padding: 8px;
  }
  .ant-tabs-ink-bar {
    display: none;
  }
  .ant-tabs-tab-active {
    background: #597ef7;
    border-radius: 5px;
    .ant-tabs-tab-btn {
      color: white;
    }
  }
`;
