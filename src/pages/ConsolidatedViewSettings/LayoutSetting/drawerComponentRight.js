import { Button, Drawer } from "antd";
import detailsImg from "assets/images/consolidatedView/detail.png";
import KnowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";
import LogsImg from "assets/images/consolidatedView/Log.png";
import SLAImg from "assets/images/consolidatedView/SLA.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import TimelineVerticalImg from "assets/images/consolidatedView/timeline-vertical.png";
import TimelineHorizontalImg from "assets/images/consolidatedView/timeline-horizontal.png";
import ICChatWidget from "assets/images/consolidatedView/ICChatWidget.svg";
import Vector from "assets/images/consolidatedView/Vector.png";
import { Notification } from "components/Notification/Noti";
import React, { useEffect } from "react";
import styled from "styled-components";
import Comment from "assets/images/consolidatedView/comment.png";
import General from "assets/images/consolidatedView/General configuration.png";
import Template from "assets/images/consolidatedView/template configuration.png";
import { getListSMSOutgoing } from "redux/slices/smsOutgoing";
import { useDispatch, useSelector } from "redux/store";
import { getListObject } from "redux/slices/objectsManagement";

const DrawerComponent = (props) => {
  const { visible, setVisible, onAddItem, items, selectedObject } = props;
  const { listSMSOutgoing } = useSelector((state) => state.smsOutgoingReducer);

  const dispatch = useDispatch();
  const onClose = () => {
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  useEffect(() => {
    dispatch(getListSMSOutgoing());
    dispatch(getListObject());
    // eslint-disable-next-line
  }, []);

  const { components } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const handleTypeImg = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return detailsImg;
      case "table":
      case "TABLE":
        return tableImg;
      case "tags":
        return tagsImg;
      case "tab":
        return tabsImg;
      case "knowledge":
        return KnowledgeBaseImg;
      default:
        break;
    }
  };

  const handleTypeTitle = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return "Details";
      case "table":
      case "TABLE":
        return "Table";
      case "tags":
        return "Tags";
      case "tab":
        return "Tab";
      case "knowledge":
        return "Knowledge base enhancement";
      default:
        break;
    }
  };

  return (
    <>
      <CustomDrawer
        title="Select component to layout"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={665}
        footer={
          <CustomFooter>
            {/* <CustomButtonSave
              size="large"
              htmlType="submit"
              //   isLoading={isLoadingCreate}
            >
              Save
            </CustomButtonSave> */}
            <CustomButtonCancel
              size="large"
              onClick={() => {
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        }
      >
        <Wrapper>
          {components.map((item, idx) => {
            let activate = items.find(
              (ele) => JSON.parse(ele.i)._id === item._id
            )
              ? "active"
              : "";
            if (item.type !== "tab")
              return (
                <div
                  className={`component-item droppable-element ${activate}`}
                  onClick={() => {
                    if (activate) {
                      Notification("error", "Component has already added!");
                    } else if (item.type === "tab") {
                      let newItem = {
                        i: JSON.stringify(item),
                        x: (items.length * 2) % 12,
                        y: Infinity, // puts it at the bottom
                        w: 1,
                        h: 2,
                        id: item._id,
                        name: item.name,
                      };
                      onAddItem(newItem);
                    } else {
                      let newItem = {
                        i: JSON.stringify(item),
                        x: (items.length * 2) % 12,
                        y: Infinity, // puts it at the bottom
                        w: 1,
                        h: 2,
                        id: item._id,
                        name: item.name,
                      };
                      onAddItem(newItem);
                    }
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <img
                      alt=""
                      src={handleTypeImg(item.type || item.component_type)}
                    />
                    <div className="info">
                      <div className="title">{item.name}</div>
                      <div className="decs">
                        {handleTypeTitle(item.type || item.component_type)}
                        {item.type === "knowledge"
                          ? ""
                          : item.component_type
                          ? ""
                          : "• Related object: "}
                        {item.related_object_name}
                      </div>
                    </div>
                  </div>
                  {activate ? (
                    <div style={{ width: "22px" }}>
                      <img
                        alt=""
                        src={Vector}
                        className="vector-img"
                        style={{ width: "100%" }}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              );
            return null;
          })}
          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "sla"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "sla",
                    type: "sla",
                    name: "SLA Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={SLAImg} />
              <div className="info">
                <div className="title">SLA</div>
                <div className="decs">Service-level agreement</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "vertical-timeline",
                    type: "vertical-timeline",
                    name: "Vertical Timeline Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineVerticalImg} />
              <div className="info">
                <div className="title">Vertical Timeline</div>
                <div className="decs">Vertical Timeline component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "horizontal-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "horizontal-timeline"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "horizontal-timeline",
                    type: "horizontal-timeline",
                    name: "Horizontal Timeline Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineHorizontalImg} />
              <div className="info">
                <div className="title">Horizontal Vertical Timeline</div>
                <div className="decs">Horizontal Timeline component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "horizontal-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "vertical-timeline-new"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "vertical-timeline-new"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "vertical-timeline-new",
                    type: "vertical-timeline-new",
                    name: "Vertical Timeline Component New",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineVerticalImg} />
              <div className="info">
                <div className="title">Vertical Timeline New</div>
                <div className="decs">Vertical Timeline component new</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "vertical-timeline-new"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "horizontal-timeline-new"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "horizontal-timeline-new"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "horizontal-timeline-new",
                    type: "horizontal-timeline-new",
                    name: "Horizontal Timeline Component New",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineHorizontalImg} />
              <div className="info">
                <div className="title">Horizontal Vertical Timeline New</div>
                <div className="decs">Horizontal Timeline component new</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "horizontal-timeline-new"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "ic-chat-widget"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "ic-chat-widget"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "ic-chat-widget",
                    type: "ic-chat-widget",
                    name: "IC Chat Widget Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={ICChatWidget} />
              <div className="info">
                <div className="title">IC Chat Widget</div>
                <div className="decs">IC Chat Widget component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "ic-chat-widget"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "logs"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "logs",
                    type: "logs",
                    name: "Logs Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={LogsImg} />
              <div className="info">
                <div className="title">Logs</div>
                <div className="decs">Logs component</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "knowledgeBase",
                    type: "knowledgeBase",
                    name: "knowledgeBase Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={KnowledgeBaseImg} />
              <div className="info">
                <div className="title">Knowledge Base</div>
                <div className="decs">Knowledge base component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "comment") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "comment"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "comment",
                    type: "comment",
                    name: "Comment Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 1,
                  h: 2,
                };
                onAddItem(newItem);
              }

              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={Comment} />
              <div className="info">
                <div className="title">Comment</div>
                <div className="decs">Comment</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "comment") >=
            0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>
          {selectedObject === "obj_crm_campaign_00001" ? (
            <>
              {" "}
              <div
                className={`component-item droppable-element ${
                  items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "general-configuration"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "general-configuration"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "general-configuration",
                        type: "general-configuration",
                        name: "General Configuration",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }
                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={General} />
                  <div className="info">
                    <div className="title">General configuration</div>
                    <div className="decs">General configuration</div>
                  </div>
                </div>
                {items.findIndex(
                  (ele) => JSON.parse(ele.i)._id === "general-configuration"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
              <div
                className={`component-item droppable-element ${
                  items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "template-configuration"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "template-configuration"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "template-configuration",
                        type: "template-configuration",
                        name: "Template configuration",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }
                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={Template} />
                  <div className="info">
                    <div className="title">Template configuration</div>
                    <div className="decs">Template configuration</div>
                  </div>
                </div>
                {items.findIndex(
                  (ele) => JSON.parse(ele.i)._id === "template-configuration"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </>
          ) : (
            ""
          )}

          {listSMSOutgoing.map((item) => {
            if (item.object_id === selectedObject) {
              return (
                <div
                  className={`component-item droppable-element ${
                    items.findIndex(
                      (ele) => JSON.parse(ele.i)._id === "sms-component"
                    ) >= 0
                      ? "active"
                      : ""
                  }`}
                  draggable={true}
                  unselectable="on"
                  onClick={() => {
                    let activate = items.find(
                      (ele) => JSON.parse(ele.i)._id === "sms-component"
                    )
                      ? "active"
                      : "";
                    if (activate) {
                      Notification("error", "Component has already added!");
                    } else {
                      let newItem = {
                        i: JSON.stringify({
                          _id: "sms-component",
                          type: "sms-component",
                          name: "SMS Component",
                        }),
                        x: (items.length * 2) % 12,
                        y: Infinity, // puts it at the bottom
                        w: 1,
                        h: 2,
                      };
                      onAddItem(newItem);
                    }
                    //   setItems(items.concat(newItem));
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <img alt="" src={General} />
                    <div className="info">
                      <div className="title">SMS Component</div>
                      <div className="decs">SMS Component</div>
                    </div>
                  </div>
                  {items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "sms-component"
                  ) >= 0 ? (
                    <div style={{ width: "22px" }}>
                      <img
                        alt=""
                        src={Vector}
                        className="vector-img"
                        style={{ width: "100%" }}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              );
            }
            return null;
          })}
        </Wrapper>
      </CustomDrawer>
    </>
  );
};

export default DrawerComponent;

const CustomDrawer = styled(Drawer)`
  .ant-drawer-body {
    padding-bottom: 0;
  }
  .active {
    border: 1px solid ${(props) => props.theme.main} !important;
  }
`;

const Wrapper = styled.div`
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  /* margin-top: 34px; */
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
  /* margin-bottom: 10px; */
`;

// const CustomButtonSave = styled(Button)`
//   background-color: ${props => props.theme.main};
//   color: #fff;
//   margin-right: 16px;
//   border-radius: 4px;

//   :hover {
//     background: #1c9292;
//     color: #fff;
//   }
// `;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: #1c9292;
    color: #fff;
  }
`;
