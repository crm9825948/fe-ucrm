import { Col, Row } from "antd";
import close from "assets/images/consolidatedView/close.png";
import detailsImg from "assets/images/consolidatedView/detail.png";
import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import KnowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";
import LogsImg from "assets/images/consolidatedView/Log.png";
import SLAImg from "assets/images/consolidatedView/SLA.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import TimelineVerticalImg from "assets/images/consolidatedView/timeline-vertical.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import Vector from "assets/images/consolidatedView/Vector.png";
import Comment from "assets/images/consolidatedView/comment.png";

import { Notification } from "components/Notification/Noti";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch, useSelector } from "react-redux";
import {
  loadLayouts,
  updateLayout,
} from "redux/slices/consolidatedViewSettings";
import { setShowLoadingScreen } from "redux/slices/global";
import styled from "styled-components";

// const ResponsiveReactGridLayout = WidthProvider(Responsive);
const ReactGridLayout = WidthProvider(RGL);

const LeftScreen = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  onLayoutChange = function () {},
  checkRule,
  ...props
}) => {
  const { selectedObject, tabKey } = props;
  const handleTypeImg = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return detailsImg;
      case "table":
      case "TABLE":
        return tableImg;
      case "tags":
        return tagsImg;
      case "tab":
        return tabsImg;
      case "knowledge":
        return KnowledgeBaseImg;
      default:
        break;
    }
  };

  const handleTypeTitle = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return "Details";
      case "table":
      case "TABLE":
        return "Table";
      case "tags":
        return "Tags";
      case "tab":
        return "Tab";
      case "knowledge":
        return "Knowledge base enhancement";
      default:
        break;
    }
  };

  const {
    components,
    layouts,
    loadingComponents,
    isLoadingUpdate,
    isLoadingLoadLayout,
  } = useSelector((state) => state.consolidatedViewSettingsReducer);

  const [items, setItems] = useState([]);

  let dispatch = useDispatch();

  const handleUpdate = (items) => {
    let result = [];
    /*eslint-disable-next-line*/
    items.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });
    dispatch(
      updateLayout({
        object_id: selectedObject,
        placement: "left",
        components: result,
      })
    );
  };

  useEffect(() => {
    if (layouts.placement === "left") {
      let tmp = [...layouts.components];
      let tmpArr = [];
      /*eslint-disable-next-line*/
      tmp.forEach((item, idx) => {
        if (item.y === null) {
          let newItem = {
            ...item,
            y: Infinity,
          };
          tmpArr.push(newItem);
        } else {
          tmpArr.push(item);
        }
      });
      setItems(tmpArr);
    } else if (layouts.length === 0) {
      setItems([]);
    }
  }, [layouts]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
  }, [loadingComponents, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingUpdate));
  }, [isLoadingUpdate, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoadingLoadLayout));
  }, [isLoadingLoadLayout, dispatch]);

  useEffect(() => {
    if (tabKey === "1")
      dispatch(
        loadLayouts({
          object_id: selectedObject,
          placement: "left",
        })
      );
  }, [selectedObject, dispatch, tabKey]);

  const createElement = (el) => {
    return (
      <CustomDiv
        key={el.i}
        data-grid={el}
        style={{
          overflow: "hidden",
          boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
          borderRadius: "10px",
          height: "100%",
        }}
      >
        <div
          style={{
            height: "40px",
            background: "#D9D9D9",
            display: "flex",
            alignItems: "center",
            fontStyle: "normal",
            fontWeight: "500",
            fontSize: "18px",
            lineHeight: "26px",
            color: "#252424",
            paddingLeft: "15px",
            width: "100%",
            justifyContent: "space-between",
            paddingRight: "15px",
          }}
        >
          {JSON.parse(el.i).name}{" "}
          <img
            alt=""
            src={close}
            style={{ width: "24px" }}
            onClick={() => {
              onRemoveItem(el.i);
            }}
            className="close-png"
          />
        </div>
        <div style={{ padding: "0", height: "100%" }}>
          <div
            style={{
              height: "100%",
              background: "#c4c4c4",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img alt="" src={Triangle} style={{ width: "50%" }} />
          </div>
        </div>
      </CustomDiv>
    );
  };

  const onAddItem = (newItem) => {
    if (checkRule("create")) {
      setItems(items.concat(newItem));
      handleUpdate(items.concat(newItem));
    }
  };
  const [reload, setReload] = useState(0);

  const onLayoutChangeNew = (layout) => {
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  const onRemoveItem = (i) => {
    if (checkRule("delete")) {
      if (reload < 2) {
        handleUpdate(_.reject(items, { i: i }));
        setItems(_.reject(items, { i: i }));
      } else if (reload >= 2) {
        setItems(_.reject(items, { i: i }));
      }
    }
  };

  return (
    <Wrapper>
      <Row gutter={16}>
        <Col span={12}>
          <div className="components">
            <div className="components-title">Components</div>
            <div style={{ height: "900px", overflow: "auto" }}>
              {/*eslint-disable-next-line*/}
              {components.map((item, idx) => {
                let activate = items.find(
                  (ele) => JSON.parse(ele.i)._id === item._id
                )
                  ? "active"
                  : "";
                if (
                  item.type !== "tab" &&
                  item.type !== "table" &&
                  item.type !== "tags"
                  //  &&
                  // item.type !== "knowledge"
                )
                  return (
                    <div
                      className={`component-item droppable-element ${activate}`}
                      draggable={true}
                      unselectable="on"
                      onClick={() => {
                        if (activate) {
                          Notification("error", "Component has already added!");
                        } else {
                          let newItem = {
                            i: JSON.stringify(item),
                            x: (items.length * 2) % 12,
                            y: Infinity, // puts it at the bottom
                            w: 1,
                            h: 2,
                            id: item._id,
                            name: item.name,
                          };
                          onAddItem(newItem);
                        }
                      }}
                    >
                      <div style={{ display: "flex" }}>
                        {" "}
                        <img
                          alt=""
                          src={handleTypeImg(item.type || item.component_type)}
                        />
                        <div className="info">
                          <div className="title">{item.name}</div>
                          <div className="decs">
                            {handleTypeTitle(item.type || item.component_type)}
                            {item.type === "knowledge"
                              ? ""
                              : item.component_type
                              ? ""
                              : "• Related object: "}
                            {item.related_object_name}
                          </div>
                        </div>
                      </div>
                      {activate ? (
                        <div style={{ width: "22px" }}>
                          <img
                            alt=""
                            src={Vector}
                            className="vector-img"
                            style={{ width: "100%" }}
                          />
                        </div>
                      ) : (
                        ""
                      )}
                    </div>
                  );
              })}

              <div
                className={`component-item droppable-element ${
                  items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "sla"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "sla",
                        type: "sla",
                        name: "SLA Component",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }

                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={SLAImg} />
                  <div className="info">
                    <div className="title">SLA</div>
                    <div className="decs">Service-level agreement</div>
                  </div>
                </div>
                {items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >=
                0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>

              <div
                className={`component-item droppable-element ${
                  items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "vertical-timeline",
                        type: "vertical-timeline",
                        name: "Vertical Timeline Component",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }

                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={TimelineVerticalImg} />
                  <div className="info">
                    <div className="title">Vertical Timeline</div>
                    <div className="decs">Vertical Timeline component</div>
                  </div>
                </div>
                {items.findIndex(
                  (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>

              <div
                className={`component-item droppable-element ${
                  items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >=
                  0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "logs"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "logs",
                        type: "logs",
                        name: "Logs Component",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }

                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={LogsImg} />
                  <div className="info">
                    <div className="title">Logs</div>
                    <div className="decs">Logs component</div>
                  </div>
                </div>
                {items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >=
                0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>

              <div
                className={`component-item droppable-element ${
                  items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "knowledgeBase",
                        type: "knowledgeBase",
                        name: "knowledgeBase Component",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }

                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={KnowledgeBaseImg} />
                  <div className="info">
                    <div className="title">Knowledge Base</div>
                    <div className="decs">Knowledge base component</div>
                  </div>
                </div>
                {items.findIndex(
                  (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>

              <div
                className={`component-item droppable-element ${
                  items.findIndex(
                    (ele) => JSON.parse(ele.i)._id === "comment"
                  ) >= 0
                    ? "active"
                    : ""
                }`}
                draggable={true}
                unselectable="on"
                onClick={() => {
                  let activate = items.find(
                    (ele) => JSON.parse(ele.i)._id === "comment"
                  )
                    ? "active"
                    : "";
                  if (activate) {
                    Notification("error", "Component has already added!");
                  } else {
                    let newItem = {
                      i: JSON.stringify({
                        _id: "comment",
                        type: "comment",
                        name: "Comment Component",
                      }),
                      x: (items.length * 2) % 12,
                      y: Infinity, // puts it at the bottom
                      w: 1,
                      h: 2,
                    };
                    onAddItem(newItem);
                  }

                  //   setItems(items.concat(newItem));
                }}
              >
                <div style={{ display: "flex" }}>
                  {" "}
                  <img alt="" src={Comment} />
                  <div className="info">
                    <div className="title">Comment</div>
                    <div className="decs">Comment</div>
                  </div>
                </div>
                {items.findIndex(
                  (ele) => JSON.parse(ele.i)._id === "comment"
                ) >= 0 ? (
                  <div style={{ width: "22px" }}>
                    <img
                      alt=""
                      src={Vector}
                      className="vector-img"
                      style={{ width: "100%" }}
                    />
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
          </div>
        </Col>
        <Col span={12}>
          <div>
            <div className="components">
              <div className="components-title">Selected component</div>

              {items.length === 0 ? (
                <div className="no-data-container">
                  <img alt="" src={emptyImg} className="empty-layout-img" />
                  <div className="no-data-decs">
                    No data. Select component to add
                  </div>
                </div>
              ) : (
                ""
              )}

              <div style={{ height: "900px", overflow: "auto" }}>
                <ReactGridLayout
                  onLayoutChange={onLayoutChangeNew}
                  {...props}
                  isDraggable={checkRule("edit")}
                  isResizable={checkRule("edit")}
                  rowHeight={10}
                  cols={1}
                >
                  {_.map(items, (el) => createElement(el))}
                </ReactGridLayout>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default LeftScreen;

const Wrapper = styled.div`
  .no-data-container {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    position: absolute;
    top: 223px;
    left: 50%;
    transform: translate(-50%, -50%);
    z-index: 10;
    width: 100%;
    .empty-layout-img {
      width: 100px;
      margin-bottom: 8px;
    }
    .no-data-decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      display: flex;
      justify-content: center;
      width: fit-content;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
    }
  }
  .components {
    padding: 10px;
    border: 1px solid #d9d9d9;
    box-sizing: border-box;
    border-radius: 5px;
    .active {
      border: 1px solid ${(props) => props.theme.main};
    }
  }
  .components-title {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    display: flex;
    align-items: center;
    margin-top: -25px;
    background-color: white;
    /* Character/Color text main */
    width: fit-content;
    padding-left: 10px;
    padding-right: 10px;
    color: #2c2c2c;
    margin-bottom: 10px;
  }
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
  .react-grid-layout {
    background-color: white;
  }
  .react-grid-item:not(.react-grid-placeholder) {
    background: #fff;
    border: none;
  }
  .close-png {
    :hover {
      cursor: pointer;
    }
  }
`;

const CustomDiv = styled.div`
  width: calc(100% - 24px) !important;
`;
