import { Button, Drawer } from "antd";
import detailsImg from "assets/images/consolidatedView/detail.png";
// import KnowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";
import LogsImg from "assets/images/consolidatedView/Log.png";
import SLAImg from "assets/images/consolidatedView/SLA.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import TimelineVerticalImg from "assets/images/consolidatedView/timeline-vertical.png";
import TimelineHorizontalImg from "assets/images/consolidatedView/timeline-horizontal.png";
import Vector from "assets/images/consolidatedView/Vector.png";
import { Notification } from "components/Notification/Noti";
import React from "react";
import { useSelector } from "react-redux";
import styled from "styled-components";
import OtherImg from "assets/images/consolidatedView/Email.png";
import Comment from "assets/images/consolidatedView/comment.png";

const DrawerComponent = (props) => {
  const { visible, setVisible, onAddItem, items } = props;

  const handleTypeImg = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return detailsImg;
      case "table":
      case "TABLE":
        return tableImg;
      case "tags":
        return tagsImg;
      case "tab":
        return tabsImg;
      default:
        break;
    }
  };

  const handleTypeTitle = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return "Details";
      case "table":
      case "TABLE":
        return "Table";
      case "tags":
        return "Tags";
      case "tab":
        return "Tab";
      default:
        break;
    }
  };

  const onClose = () => {
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  const {
    components,
    // isLoadingDelete,
    // layouts,
    // loadingComponents,
    // isLoadingUpdate,
    // isLoadingLoadLayout,
  } = useSelector((state) => state.consolidatedViewSettingsReducer);

  return (
    <>
      <CustomDrawer
        title="Select component to tab"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={665}
        footer={
          <CustomFooter>
            {/* <CustomButtonSave
              size="large"
              htmlType="submit"
              //   isLoading={isLoadingCreate}
            >
              Save
            </CustomButtonSave> */}
            <CustomButtonCancel
              size="large"
              onClick={() => {
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        }
      >
        <Wrapper>
          {/*eslint-disable-next-line*/}
          {components.map((item, idx) => {
            let activate = items.find(
              (ele) => JSON.parse(ele.i)._id === item._id
            )
              ? "active"
              : "";
            if (item.type !== "tab")
              return (
                <div
                  className={`component-item droppable-element ${activate}`}
                  onClick={() => {
                    if (activate) {
                      Notification("error", "Component has already added!");
                    } else if (item.type === "tab") {
                      let newItem = {
                        i: JSON.stringify(item),
                        x: (items.length * 2) % 12,
                        y: Infinity, // puts it at the bottom
                        w: 12,
                        h: 2,
                        id: item._id,
                        name: item.name,
                      };
                      onAddItem(newItem);
                    } else {
                      let newItem = {
                        i: JSON.stringify(item),
                        x: (items.length * 2) % 12,
                        y: Infinity, // puts it at the bottom
                        w: 12,
                        h: 2,
                        id: item._id,
                        name: item.name,
                      };
                      onAddItem(newItem);
                    }
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <img
                      alt=""
                      src={handleTypeImg(item.type || item.component_type)}
                    />
                    <div className="info">
                      <div className="title">{item.name}</div>
                      <div className="decs">
                        {handleTypeTitle(item.type || item.component_type)}
                        {item.component_type ? "" : "• Related object: "}
                        {item.related_object_name}
                      </div>
                    </div>
                  </div>
                  {activate ? (
                    <div style={{ width: "22px" }}>
                      <img
                        alt=""
                        src={Vector}
                        className="vector-img"
                        style={{ width: "100%" }}
                      />
                    </div>
                  ) : (
                    ""
                  )}
                </div>
              );
          })}
          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "sla",
                  type: "sla",
                  name: "SLA Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={SLAImg} />
              <div className="info">
                <div className="title">SLA</div>
                <div className="decs">Service-level agreement</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "sla") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "vertical-timeline",
                  type: "vertical-timeline",
                  name: "Vertical Timeline Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineVerticalImg} />
              <div className="info">
                <div className="title">Vertical Timeline</div>
                <div className="decs">Vertical Timeline component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "vertical-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "horizontal-timeline"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "horizontal-timeline",
                  type: "horizontal-timeline",
                  name: "Horizontal Timeline Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={TimelineHorizontalImg} />
              <div className="info">
                <div className="title">Horizontal Vertical Timeline</div>
                <div className="decs">Horizontal Timeline component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "horizontal-timeline"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "email") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "email",
                  type: "email",
                  name: "Email Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={OtherImg} />
              <div className="info">
                <div className="title">Email</div>
                <div className="decs">Email component</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "email") >=
            0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "logs",
                  type: "logs",
                  name: "Logs Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={LogsImg} />
              <div className="info">
                <div className="title">Logs</div>
                <div className="decs">Logs component</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "logs") >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>
          <div
            className={`component-item droppable-element ${
              items.findIndex((ele) => JSON.parse(ele.i)._id === "comment") >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let activate = items.find(
                (ele) => JSON.parse(ele.i)._id === "comment"
              )
                ? "active"
                : "";
              if (activate) {
                Notification("error", "Component has already added!");
              } else {
                let newItem = {
                  i: JSON.stringify({
                    _id: "comment",
                    type: "comment",
                    name: "Comment Component",
                  }),
                  x: (items.length * 2) % 12,
                  y: Infinity, // puts it at the bottom
                  w: 12,
                  h: 2,
                };
                onAddItem(newItem);
              }

              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={Comment} />
              <div className="info">
                <div className="title">Comment</div>
                <div className="decs">Comment</div>
              </div>
            </div>
            {items.findIndex((ele) => JSON.parse(ele.i)._id === "comment") >=
            0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div>

          {/* <div
            className={`component-item droppable-element ${
              items.findIndex(
                (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
              ) >= 0
                ? "active"
                : ""
            }`}
            draggable={true}
            unselectable="on"
            onClick={() => {
              let newItem = {
                i: JSON.stringify({
                  _id: "knowledgeBase",
                  type: "knowledgeBase",
                  name: "knowledgeBase Component",
                }),
                x: (items.length * 2) % 12,
                y: Infinity, // puts it at the bottom
                w: 12,
                h: 2,
              };
              onAddItem(newItem);
              //   setItems(items.concat(newItem));
            }}
          >
            <div style={{ display: "flex" }}>
              {" "}
              <img alt="" src={KnowledgeBaseImg} />
              <div className="info">
                <div className="title">Knowledge Base</div>
                <div className="decs">Knowledge base component</div>
              </div>
            </div>
            {items.findIndex(
              (ele) => JSON.parse(ele.i)._id === "knowledgeBase"
            ) >= 0 ? (
              <div style={{ width: "22px" }}>
                <img
                  alt=""
                  src={Vector}
                  className="vector-img"
                  style={{ width: "100%" }}
                />
              </div>
            ) : (
              ""
            )}
          </div> */}
        </Wrapper>
      </CustomDrawer>
    </>
  );
};

export default DrawerComponent;

const CustomDrawer = styled(Drawer)`
  .ant-drawer-body {
    padding-bottom: 0;
  }
  .active {
    border: 1px solid ${(props) => props.theme.main} !important;
  }
`;

const Wrapper = styled.div`
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    align-items: center;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  /* margin-top: 34px; */
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
  /* margin-bottom: 10px; */
`;

// const CustomButtonSave = styled(Button)`
//   background-color: ${props => props.theme.main};
//   color: #fff;
//   margin-right: 16px;
//   border-radius: 4px;

//   :hover {
//     background: #1c9292;
//     color: #fff;
//   }
// `;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: #1c9292;
    color: #fff;
  }
`;
