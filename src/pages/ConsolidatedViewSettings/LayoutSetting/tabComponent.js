import { Button } from "antd";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import close from "assets/images/consolidatedView/close.png";
import emptyImg from "assets/images/consolidatedView/empty layout setting.png";
import Triangle from "assets/images/consolidatedView/triangle.png";
import _ from "lodash";
import React, { useEffect, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch } from "react-redux";
import { updateTab } from "redux/slices/consolidatedViewSettings";
import styled from "styled-components";
import DrawerComponentToTab from "./drawerAddComponentToTab";
// import { Notification } from "components/Notification/Noti";

const ReactGridLayout = WidthProvider(RGL);

const TabComponent = ({
  className = "layout",
  cols = { lg: 12, md: 12, sm: 12, xs: 12, xxs: 12 },
  rowHeight = 100,
  onLayoutChange = function () {},
  checkRule,
  ...props
}) => {
  const [visibleTab, setVisibleTab] = useState(false);
  const [items, setItems] = useState([]);
  const { data, name, itemParent, setItemParent } = props;
  let dispatch = useDispatch();

  const handleUpdate = (items) => {
    let result = [];
    /*eslint-disable-next-line*/
    items.map((item, idx) => {
      let newItem = {
        x: item.x,
        y: item.y,
        w: item.w,
        h: item.h,
        i: item.i,
      };
      result.push(newItem);
    });

    let dataTemp = { ...data };

    dataTemp.tab_component = { ...(data.tab_component || {}), [name]: result };

    let tmpItems = [...itemParent];
    let newItems = [];
    /*eslint-disable-next-line*/
    tmpItems.forEach((ele, i) => {
      let newEle = { ...ele };
      if (data._id === JSON.parse(ele.i)._id) {
        newEle = { ...ele, i: JSON.stringify(dataTemp) };
      }
      newItems.push(newEle);
    });

    setItemParent(newItems);

    dispatch(
      updateTab({
        ...dataTemp,
        id: data._id,
      })
    );
  };

  useEffect(() => {
    let tmp = [
      ...((data && data.tab_component && data.tab_component[name]) || []),
    ];
    let tmpArr = [];
    /*eslint-disable-next-line*/
    tmp.forEach((item, idx) => {
      if (item.y === null) {
        let newItem = {
          ...item,
          y: Infinity,
        };
        tmpArr.push(newItem);
      } else {
        tmpArr.push(item);
      }
    });

    setItems(tmpArr);
    /*eslint-disable-next-line*/
  }, [data]);

  const createElement = (el) => {
    return (
      <div
        key={el.i}
        data-grid={el}
        style={{
          overflow: "hidden",
          boxShadow: "0px 0px 16px rgba(0, 0, 0, 0.16)",
          borderRadius: "10px",
          height: "100%",
        }}
      >
        <div
          style={{
            height: "40px",
            background: "#D9D9D9",
            display: "flex",
            alignItems: "center",
            fontStyle: "normal",
            fontWeight: "500",
            fontSize: "18px",
            lineHeight: "26px",
            color: "#252424",
            paddingLeft: "15px",
            width: "100%",
            justifyContent: "space-between",
            paddingRight: "15px",
          }}
        >
          {JSON.parse(el.i).name}
          {checkRule("delete") ? (
            <img
              alt=""
              src={close}
              style={{ width: "24px" }}
              onClick={() => {
                onRemoveItem(el.i);
              }}
              className="close-png"
            />
          ) : (
            ""
          )}
        </div>
        <div style={{ padding: "0", height: "100%" }}>
          <div
            style={{
              height: "100%",
              background: "#c4c4c4",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <img alt="" src={Triangle} style={{ width: "50%" }} />
          </div>
        </div>
      </div>
    );
  };

  const onAddItem = (newItem) => {
    setItems(items.concat(newItem));
    handleUpdate(items.concat(newItem));
  };
  const [reload, setReload] = useState(0);

  const onLayoutChangeNew = (layout) => {
    if (reload === 0) {
    } else if (reload === 1) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    } else if (reload >= 2) {
      onLayoutChange(layout);
      setItems(layout);
      handleUpdate(layout);
    }
    setReload(reload + 1);
  };

  const onRemoveItem = (i) => {
    if (reload < 2) {
      handleUpdate(_.reject(items, { i: i }));
      setItems(_.reject(items, { i: i }));
    } else if (reload >= 2) {
      setItems(_.reject(items, { i: i }));
    }
  };

  return (
    <div>
      {items.length === 0 ? (
        <div className="no-data-container-tab">
          <img alt="" src={emptyImg} className="empty-layout-img" />
          <div className="no-data-decs">No data. Select component to add</div>
          {checkRule("create") ? (
            <CustomButtonAddRecord
              size="large"
              onClick={() => {
                setVisibleTab(true);
              }}
            >
              <img alt="" src={plusIcon} />
              Add new
            </CustomButtonAddRecord>
          ) : (
            ""
          )}
        </div>
      ) : (
        <div style={{ height: "1300px", overflow: "auto" }}>
          <ReactGridLayout
            onLayoutChange={onLayoutChangeNew}
            {...props}
            isDraggable={checkRule("edit")}
            isResizable={checkRule("edit")}
          >
            {_.map(items, (el) => createElement(el))}
          </ReactGridLayout>
        </div>
      )}
      <DrawerComponentToTab
        visible={visibleTab}
        setVisible={setVisibleTab}
        onAddItem={onAddItem}
        items={[]}
      />
    </div>
  );
};

export default TabComponent;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
