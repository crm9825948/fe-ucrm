import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";

import Breadcrumb from "antd/lib/breadcrumb";
import Menu from "antd/lib/menu";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Tooltip from "antd/lib/tooltip";
// import Button from "antd/lib/button";

import Collapse from "assets/icons/consolidatedViewSettings/Collapse.svg";
import UnCollapse from "assets/icons/consolidatedViewSettings/UnCollapse.svg";
import InfoCircle from "assets/icons/consolidatedViewSettings/info-circle.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import detailImg from "assets/images/consolidatedViewSetting/info Component setting.png";

import {
  loadDataNecessary,
  // resetAllConfigCallCenter,
  // resetAllIC,
  // resetAllCTI,
} from "redux/slices/consolidatedViewSettings";

import ExposeAPI from "./ExposeAPI/exposeAPI";
import PopupSetting from "./PopupSetting/popupSetting";
import CoreSetting from "./CoreSetting/coreSetting";
import ExposeAPICreate from "./ExposeAPICreate/exposeAPICreate";
import ExposeAPIUpdate from "./ExposeAPIUpdate/exposeAPIUpdate";
import ICIntegration from "./ICIntegration/ICIntegration";
import ComponentSettings from "./ComponentSettings/componentSettings";
import LayoutSetting from "./LayoutSetting/layoutSetting";
import VoiceBotSetting from "./VoiceBotSetting/voiceBotSetting";
import CTISetting from "./CTISetting/CTISetting";
import CallCenter from "./CallCenter/callCenter";
import VoiceBiometric from "./VoiceBiometric/voiceBiometric";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import { changeTitlePage } from "redux/slices/authenticated";

function ConsolidatedViewSettings(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.consolidatedViewSetting")));
    //eslint-disable-next-line
  }, [t]);

  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [collapsed, setCollapsed] = useState(false);
  const [delay, setDelay] = useState(true);
  const [listObjects, setListObjects] = useState([]);

  const [activeTab, setActiveTab] = useState("");
  const [selectedObject, setSelectedObject] = useState(undefined);

  const listSetting = ["Layout setting", "Component setting"];

  // const listIntegration = [
  //   "CTI setting",
  //   "Exposed API - Check",
  //   "Exposed API - Create",
  //   "Exposed API - Update",
  //   "External app integration",
  //   "IC integration",
  //   "URL Popup setting",
  //   "Voice biometric",
  //   "Voice bot setting",
  //   "Finesse integration",
  // ];

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "consolidate_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onCollapse = () => {
    setCollapsed(true);
    setTimeout(() => {
      setDelay(false);
    }, 300);
  };

  const onHoverMenu = () => {
    if (collapsed) {
      setCollapsed(false);
      setDelay(true);
    }
  };

  const handleSelectComponent = (item) => {
    setActiveTab(item.key);
  };

  const [objectName, setObjectName] = useState("");
  const handleSelectObject = (value, label) => {
    setObjectName(label.label);
    setSelectedObject(value);
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("consolidatedView.setting")}
        </Breadcrumb.Item>
        <BreadcrumbItem>Consolidated view setting</BreadcrumbItem>
      </Breadcrumb>

      <Content>
        <WrapMenu collapsed={collapsed}>
          {collapsed && delay === false ? (
            <Wrap onMouseOver={() => onHoverMenu()}>
              <img src={UnCollapse} alt="collapse" />
            </Wrap>
          ) : (
            <Menu
              onSelect={handleSelectComponent}
              inlineCollapsed={collapsed}
              mode="inline"
            >
              <Menu.Item key="object" disabled>
                <p> {t("consolidatedView.object")}</p>
                <Select
                  value={selectedObject}
                  options={listObjects}
                  placeholder={t("common.placeholderSelect")}
                  onChange={handleSelectObject}
                  optionFilterProp="label"
                  showSearch
                />
                <ImgCollapse
                  onClick={() => _onCollapse()}
                  src={Collapse}
                  alt="collapse"
                />
                <hr />
              </Menu.Item>

              {listSetting.map((item, idx) => {
                return (
                  <Menu.Item key={item}>
                    <div>
                      <Radio checked={activeTab === item} />
                      {item}
                      {idx === 1 && (
                        <CustomTooltip
                          color={"transparent"}
                          overlayInnerStyle={{ boxShadow: "unset" }}
                          title={
                            <img
                              alt=""
                              src={detailImg}
                              style={{
                                width: "1000px",
                                border: "1px solid #F2F4F5",
                                borderRadius: "10px",
                              }}
                            />
                          }
                        >
                          <img src={InfoCircle} alt="info" />
                        </CustomTooltip>
                      )}
                    </div>
                  </Menu.Item>
                );
              })}

              {/* <Menu.Item key="type" disabled>
                <p>Integration</p>
              </Menu.Item> */}

              {/* {listIntegration.map((item, idx) => {
                return (
                  <Menu.Item key={item}>
                    <div>
                      <Radio checked={activeTab === item} />
                      {item}
                    </div>

                    {(item === "Finesse integration" ||
                      item === "IC integration" ||
                      item === "CTI setting") && (
                      <ButtonReset
                        onClick={(e) => {
                          e.stopPropagation();

                          if (item === "Finesse integration") {
                            dispatch(resetAllConfigCallCenter());
                          } else if (item === "IC integration") {
                            dispatch(resetAllIC());
                          } else if (item === "CTI setting") {
                            dispatch(resetAllCTI());
                          }
                        }}
                        className="action"
                      >
                        Reset
                      </ButtonReset>
                    )}
                  </Menu.Item>
                );
              })} */}
            </Menu>
          )}
        </WrapMenu>

        {selectedObject === undefined || activeTab === "" ? (
          <Empty collapsed={collapsed}>
            <img src={EmptyObject} alt="empty" />
            <span>Vui lòng chọn dữ liệu để hiển thị</span>
          </Empty>
        ) : (
          <>
            {activeTab === "Component setting" && (
              <ComponentSettings
                selectedObject={selectedObject}
                objectName={objectName}
                checkRule={checkRule}
              />
            )}
            {activeTab === "Layout setting" && (
              <LayoutSetting
                selectedObject={selectedObject}
                checkRule={checkRule}
              />
            )}
            {activeTab === "Exposed API - Check" && (
              <ExposeAPI
                collapsed={collapsed}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "URL Popup setting" && (
              <PopupSetting selectedObject={selectedObject} />
            )}
            {activeTab === "CTI setting" && (
              <CTISetting
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "Voice bot setting" && (
              <VoiceBotSetting
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "External app integration" && (
              <CoreSetting
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "Exposed API - Create" && (
              <ExposeAPICreate
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "Exposed API - Update" && (
              <ExposeAPIUpdate
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "IC integration" && (
              <ICIntegration
                listObjects={listObjects}
                selectedObject={selectedObject}
              />
            )}
            {activeTab === "Finesse integration" && (
              <CallCenter
                listObjects={listObjects}
                selectedObject={selectedObject}
                collapsed={collapsed}
              />
            )}
            {activeTab === "Voice biometric" && (
              <VoiceBiometric selectedObject={selectedObject} />
            )}
          </>
        )}
      </Content>
    </Wrapper>
  );
}

export default ConsolidatedViewSettings;

const Wrapper = styled.div`
  .ant-breadcrumb {
    padding: 16px 24px;
  }
  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-menu {
    /* padding: 20px 0 24px 0; */
    padding: 8px 0 24px 0;
    border-radius: 0px 10px 10px 0px;
  }

  .ant-menu-item {
    padding: 0 24px;

    p {
      font-family: var(--roboto-500);
      font-size: 16px;
      color: #2c2c2c;
      margin-bottom: 0;
    }

    :first-child {
      height: 95px;
      cursor: default;

      img {
        position: absolute;
        top: -7px;
        right: 8px;
        cursor: pointer;
      }

      hr {
        margin-top: 20px;
        color: #d9d9d9;
        width: 100%;
      }

      .ant-menu-title-content {
        display: flex;
        flex-direction: column;
      }
    }

    :nth-child(2) {
      cursor: default;
      margin: 0;
    }

    :nth-child(3) {
      margin: 0;
      img {
        margin-left: 8px;
      }
    }
  }

  .ant-menu.ant-menu-inline-collapsed {
    width: 100%;
  }

  .ant-menu-item-selected {
    color: #2c2c2c;
    background-color: rgba(32, 162, 162, 0.2);
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapMenu = styled.div`
  margin-right: 16px;
  width: ${({ collapsed }) => (collapsed ? "32px" : "288px")};
  height: ${({ collapsed }) => (collapsed ? "190px" : "100%")};
  background: #fff;
  transition: all 0.5s ease-in-out;
  border-radius: 0px 10px 10px 0px;

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  .ant-menu-item {
    padding: 0 20px;

    :nth-child(5),
    :nth-child(10),
    :last-child {
      :hover .action {
        visibility: visible;
        opacity: 1;
      }

      .ant-menu-title-content {
        display: flex;
        align-items: center;
        justify-content: space-between;
      }
    }
  }
`;

const CustomTooltip = styled(Tooltip)`
  .ant-tooltip-inner {
    box-shadow: unset !important;
  }
`;

const Wrap = styled.div`
  width: 100%;
  height: 100%;
  min-height: 190px;

  display: flex;

  img {
    width: 100%;
  }
`;

const ImgCollapse = styled.img`
  width: 35px;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  width: ${({ collapsed }) => (collapsed ? "100%" : "calc(100% - 288px)")};

  span {
    font-size: 16px;
    color: ${(props) => props.theme.main};
  }
`;

const Content = styled.div`
  display: flex;
`;

// const ButtonReset = styled(Button)`
//   padding: 0 15px;
//   height: 24px;

//   span {
//     font-size: 16px;
//   }

//   :hover {
//     background: ${(props) => props.theme.darker}!important;
//     color: #fff !important;
//   }

//   :active {
//     background: #fff;
//     color: #000;
//     border-color: #d9d9d9;
//   }

//   :focus {
//     background: #fff;
//     color: #000;
//     border-color: #d9d9d9;
//   }
// `;
