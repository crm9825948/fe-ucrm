import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Empty from "antd/lib/empty";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Space from "antd/lib/space";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EmptyMapping from "assets/images/consolidatedViewSetting/empty_user_mapping.png";

import {
  createUserMappingCallCenter,
  resetStatus,
  deleteUserMappingCallCenter,
  updateUserMappingCallCenter,
  loadUsersMappingCallCenter,
  setDetailsUserMappingCallCenter,
} from "redux/slices/consolidatedViewSettings";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

function MappingUser({ selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { Column } = Table;

  const {
    userMappingCallCenter,
    statusUserMappingCallCenter,
    loading,
    detailsUserMappingCallCenter,
  } = useSelector((state) => state.consolidatedViewSettingsReducer);

  const [dataTable, setDataTable] = useState([]);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const [listUser, setListUser] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [isEdit, setIsEdit] = useState(false);

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateUserMappingCallCenter({
          _id: detailsUserMappingCallCenter.key,
          agent_id: values.agent_name,
          user_id: values.user_name,
          extension: values.extension,
          access_key: _.get(values, "access_key", undefined),
          user_calabrio: values.user_calabrio,
        })
      );
    } else {
      dispatch(
        createUserMappingCallCenter({
          agent_id: values.agent_name,
          user_id: values.user_name,
          extension: values.extension,
          access_key: _.get(values, "access_key", undefined),
          user_calabrio: values.user_calabrio,
        })
      );
    }
  };

  const _onDelete = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  const _onEdit = (details) => {
    setIsEdit(true);
    dispatch(setDetailsUserMappingCallCenter(details));
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    let tempList = [];
    if (userMappingCallCenter.length > 0) {
      userMappingCallCenter.map((item) => {
        return tempList.push({
          key: item._id,
          user_id: item.user_id,
          user_name: item.user_name,
          agent_name: item.agent_id,
          extension: item.extension,
          // access_key: item.access_key,
          user_calabrio: item.user_calabrio,
          created_by: item.created_by_name,
          created_date: item.created_date,
          action: "",
        });
      });
      setDataTable(tempList);
    }
  }, [userMappingCallCenter]);

  useEffect(() => {
    if (statusUserMappingCallCenter === "success") {
      form.resetFields();
      setIsEdit(false);
      dispatch(resetStatus());
      dispatch(loadUsersMappingCallCenter());
    }
  }, [statusUserMappingCallCenter, form, dispatch, selectedObject]);

  useEffect(() => {
    if (isEdit && detailsUserMappingCallCenter !== null) {
      form.setFieldsValue({
        user_name: detailsUserMappingCallCenter.user_id,
        agent_name: detailsUserMappingCallCenter.agent_name,
        extension: detailsUserMappingCallCenter.extension,
        // access_key: detailsUserMappingCallCenter.access_key,
        user_calabrio: detailsUserMappingCallCenter.user_calabrio,
      });
    }
  }, [isEdit, detailsUserMappingCallCenter, form]);

  useEffect(() => {
    form.resetFields();
    setIsEdit(false);
  }, [selectedObject, form]);

  return (
    <>
      <FormCustom
        form={form}
        onFinish={_onSubmit}
        colon={false}
        labelAlign="left"
        labelCol={{ lg: { span: 12 }, xl: { span: 10 }, xxl: { span: 8 } }}
        wrapperCol={{ lg: { span: 12 }, xl: { span: 14 }, xxl: { span: 16 } }}
      >
        <Space size={40}>
          <Form.Item
            label="User name"
            name="user_name"
            rules={[
              {
                required: true,
                message: "Please select User name!",
              },
            ]}
          >
            <Select
              placeholder="Select User name"
              options={listUser}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>
          <Form.Item
            label="Agent name"
            name="agent_name"
            rules={[{ required: true, message: "Please input Agent name!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Extension" name="extension">
            <Input />
          </Form.Item>
        </Space>
        <Space size={40}>
          <Form.Item label="Access key" name="access_key">
            <Input.Password autocomplete="new-password" />
          </Form.Item>
          <Form.Item label="User calabrio" name="user_calabrio">
            <Input />
          </Form.Item>
          <div></div>
        </Space>

        <Button
          disabled={
            (isEdit && !checkRule("edit")) || (!isEdit && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {isEdit ? "Update" : "Save"}
        </Button>
      </FormCustom>

      {userMappingCallCenter.length > 0 ? (
        <UserMapping>
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title="User name"
              dataIndex="user_name"
              key="user_name"
              width="300px"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Agent name"
              dataIndex="agent_name"
              key="agent_name"
              width="200px"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Extension"
              dataIndex="extension"
              key="extension"
              width="200px"
            />
            {/* <Column
              title="Access key"
              dataIndex="access_key"
              key="access_key"
              width="200px"
              render={(text) => <div className="column">{text}</div>}
            /> */}
            <Column
              title="User calabrio"
              dataIndex="user_calabrio"
              key="user_calabrio"
              width="200px"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Create by"
              dataIndex="created_by"
              key="created_by"
              width="200px"
              render={(text) => <div className="column">{text}</div>}
            />
            <Column
              title="Create time"
              dataIndex="created_date"
              key="created_date"
              width="200px"
              render={(text) => <div className="column">{text}</div>}
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title="Action"
                dataIndex="action"
                key="action"
                fixed="right"
                width="100px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title="Edit">
                        <img
                          onClick={() => _onEdit(record)}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title="Delete">
                        <img
                          onClick={() => _onDelete(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
        </UserMapping>
      ) : (
        <WrapEmpty>
          <Empty
            image={EmptyMapping}
            description={
              <p>
                There's no any <span>Mapping user</span>
              </p>
            }
          ></Empty>
        </WrapEmpty>
      )}

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteUserMappingCallCenter}
        dataDelete={dataDelete}
        isLoading={loading.userMappingIC}
      />
    </>
  );
}

export default MappingUser;

const FormCustom = styled(Form)`
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;
  display: flex;
  flex-direction: column;

  .ant-space-item {
    flex: 1;
  }

  .ant-form-item {
    flex: 1;
    margin-bottom: 16px;
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    width: 139px;
    align-self: center;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const UserMapping = styled.div`
  background: #fff;
  border-radius: 10px;
  margin-top: 16px;

  .column {
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }

  .ant-table-wrapper {
    flex: 1;
    padding: 16px;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapEmpty = styled.div`
  background: #fff;
  border-radius: 10px;
  min-height: 218px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;
    width: 18px;

    :hover {
      background: #eeeeee;
    }
  }
`;
