import styled from "styled-components";

import Form from "antd/lib/form";
import Input from "antd/lib/input";

function FieldCreate({ item, relatedFields, listUser, onChangeValue }) {
  return (
    <Wrapper>
      <Form.Item
        label={item.label}
        name={item.value}
        rules={[
          { required: item.required, message: `Please input ${item.label}!` },
        ]}
      >
        <Input
          placeholder={`Input ${item.label}`}
          onChange={(e) => onChangeValue(item.value, e.target.value)}
        />
      </Form.Item>
    </Wrapper>
  );
}

export default FieldCreate;

const Wrapper = styled.div``;
