import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Switch from "antd/lib/switch";
import Radio from "antd/lib/radio";
import Tooltip from "antd/lib/tooltip";
import InputNumber from "antd/lib/input-number";
import { useTranslation } from "react-i18next";

import {
  updateConfigCallCenter,
  resetAllConfigCallCenter,
  loadFieldsObjectToCall,
  testConnectionCalabrio,
  updateCallScoring,
} from "redux/slices/consolidatedViewSettings";
import { loadListObjectField } from "redux/slices/objects";
import ModalConfirm from "components/Modal/ModalConfirm";
import ImgConfirm from "assets/icons/common/confirm.png";
import { setShowModalConfirm } from "redux/slices/global";
import { loadFieldsCreate } from "redux/slices/consolidatedViewSettings";
import FieldCreate from "./FieldCreate";
import { loadFields } from "redux/slices/report";

// import PlusGreen from "assets/icons/common/plus-green.svg";
import DeleteIcon from "assets/icons/common/delete.svg";

const optionsPopup = [
  {
    label: "Default",
    value: "default",
  },
  {
    label: "Search",
    value: "search",
  },
];

function AdvancedConfig({ listObjects, selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [typeAuthen, setTypeAuthen] = useState("");
  const [useThirdParty, setUseThirdParty] = useState(false);
  const [listFields, setListFields] = useState([]);
  const [listHotline, setListHotline] = useState([]);
  const [objectsToCall, $objectsToCall] = useState([]);

  //Calabrio
  const [useCalabrio, $useCalabrio] = useState(false);

  //Searchcore
  const [isSearchCore, $isSearchCore] = useState(false);
  const [typeAuthenSearch, $typeAuthenSearch] = useState("");
  const [fieldsCreate, setFieldsCreate] = useState([]);
  const [requiredFieldsCreate, setRequiredFieldsCreate] = useState([]);
  const [notRequiredFields, setNotRequiredFields] = useState([]);
  const [mappingFields, setMappingFields] = useState({});
  const [listUser, setListUser] = useState([]);
  const [relatedFields, setRelatedFields] = useState([]);

  const { configCallCenter, fieldsObjectToCall, fieldsObject } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { listAllUser } = useSelector((state) => state.userReducer);
  const { listFieldsObject } = useSelector((state) => state.reportReducer);

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    // {
    //   label: 'Token',
    //   value: 'token'
    // }
  ];

  const AuthenticationTypeSearch = [
    {
      label: "Basic",
      value: "BASIC_AUTH",
    },
    {
      label: "Token",
      value: "token",
    },
  ];

  const Method = [
    {
      label: "GET",
      value: "GET",
    },
    // {
    //   label: 'POST',
    //   value: 'POST'
    // }
  ];

  const MethodSearch = [
    {
      label: "GET",
      value: "GET",
    },
    {
      label: "POST",
      value: "POST",
    },
  ];

  const _onSubmit = (values) => {
    let tempListFinesseURL = [];

    if (values.finesse_url_2) {
      tempListFinesseURL.push(values.finesse_url_1);
      tempListFinesseURL.push(values.finesse_url_2);
    } else {
      tempListFinesseURL.push(values.finesse_url_1);
    }

    let tempFields = requiredFieldsCreate.concat(notRequiredFields);
    let tempKeyFields = [];

    tempFields.forEach((item) => {
      tempKeyFields.push(item.value);
    });

    let tempMapping = Object.keys(mappingFields);
    let resultMappingCreate = { ...mappingFields };

    tempMapping.forEach((item) => {
      if (tempKeyFields.includes(item) === false) {
        delete resultMappingCreate[item];
      }
    });

    dispatch(
      updateConfigCallCenter({
        agent_admin: values.agent_admin,
        access_key_admin: _.get(values, "access_key_admin", undefined),
        default_hotline: values.default_hotline,
        identify_phone_prefix: values.identify_phone_prefix,
        list_finesse_url: tempListFinesseURL,
        object_id: values.object_id,
        check_3rd: useThirdParty,
        disable_calabrio_integration: !useCalabrio,
        search_core: isSearchCore,
        phone_fields_query: values.phone_fields_query,

        search_core_config: {
          object_create_record: _.get(
            values,
            "object_create_record_search",
            undefined
          ),
          api_url: _.get(values, "api_url_search", undefined),
          key_object: _.get(values, "key_object_search", undefined),
          user_filter: _.get(values, "user_filter_search", undefined),
          authentication_type: _.get(
            values,
            "authentication_type_search",
            undefined
          ),
          user_name: _.get(values, "user_name_search", undefined),
          password: _.get(values, "password_search", undefined),
          token: _.get(values, "token_search", undefined),
          method: _.get(values, "method_search", undefined),
          mapping_create: isSearchCore ? resultMappingCreate : undefined,
        },

        thirdparty_config: {
          auth_type: _.get(values, "authentication_type", undefined),
          username: _.get(values, "user_name", undefined),
          password: _.get(values, "password", undefined),
          api_url: _.get(values, "api_url", undefined),
          method: _.get(values, "method", undefined),
          params: _.get(values, "params", undefined),
          timeout: _.get(values, "timeout", undefined),
        },

        recording_call_config: {
          api_url_get_session: _.get(values, "api_url_get_session", undefined),
          api_url_check_ha: _.get(values, "api_url_check_ha", undefined),
          metadata_key: _.get(values, "metadata_key", undefined),
          user_admin_calabrio: _.get(values, "user_admin_calabrio", undefined),
          password_admin_calabrio: _.get(
            values,
            "password_admin_calabrio",
            undefined
          ),
          host_recording: _.get(values, "host_recording", undefined),
        },

        onlyCallOutWhenReady: values.call_out,
        allow_direct_signal: values.allow_direct_signal,
        allow_dynamic_extension: values.allow_dynamic_extension,
        custom_hotlines: listHotline,
        popup_call: values.popup_call,
        search_type: values.search_type,
        object_to_call: objectsToCall,
      })
    );
  };

  const onSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const onSelectAuthenTypeSearch = (e) => {
    $typeAuthenSearch(e);
  };

  const onUseThirdParty = (checked) => {
    setUseThirdParty(checked);
  };

  const onUseCalabrio = (checked) => {
    $useCalabrio(checked);
  };

  const onUseSearchCore = (checked) => {
    $isSearchCore(checked);
  };

  const handleSelectObject = (value) => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
      })
    );

    form.setFieldsValue({
      phone_fields_query: undefined,
    });
  };

  const handleSelectObjectCreateRecord = (value, edit) => {
    dispatch(
      loadFieldsCreate({
        object_id: value,
      })
    );

    if (!edit) {
      setNotRequiredFields([]);
      form.setFieldsValue({
        notRequiredFieldCreate: [],
      });
    }
  };

  const _onAddCustomHotline = () => {
    form.setFieldsValue({
      ["hotline_name_" + listHotline.length]: undefined,
      ["hotline_prefix_" + listHotline.length]: undefined,
      ["hotline_hotline_" + listHotline.length]: undefined,
    });

    setListHotline([
      ...listHotline,
      {
        name: "",
        prefix: null,
        hotline: "",
      },
    ]);
  };

  const _onDeleteHotline = (idx) => {
    let temp = [...listHotline];
    temp.splice(idx, 1);

    temp.forEach((item, index) => {
      form.setFieldsValue({
        ["hotline_name_" + index]: item.name,
        ["hotline_prefix_" + index]: item.prefix,
        ["hotline_hotline_" + index]: item.hotline,
      });
    });

    setListHotline(temp);
  };

  const handleChangeCustomHotline = (value, type, index) => {
    let tempHotlines = [...listHotline];

    tempHotlines[index] = {
      ...tempHotlines[index],
      [type]: value,
    };

    setListHotline(tempHotlines);
  };

  const _onAddObjectToCall = () => {
    form.setFieldsValue({
      ["objectToCall_object_" + objectsToCall.length]: undefined,
      ["objectToCall_fields_" + objectsToCall.length]: undefined,
    });

    $objectsToCall([
      ...objectsToCall,
      {
        object_id: undefined,
        field_id: undefined,
      },
    ]);
  };

  const _onDeleteObjectToCall = (idx) => {
    let temp = [...objectsToCall];
    temp.splice(idx, 1);

    temp.forEach((item, index) => {
      form.setFieldsValue({
        ["objectToCall_object_" + index]: item.object_id,
        ["objectToCall_fields_" + index]: item.field_id,
      });
    });

    $objectsToCall(temp);
  };

  const handleChangeObjectToCall = (value, type, index) => {
    let temp = [...objectsToCall];

    temp[index] = {
      ...temp[index],
      [type]: value,
    };
    $objectsToCall(temp);

    if (type === "object_id") {
      dispatch(
        loadFieldsObjectToCall({
          api_version: "2",
          object_id: value,
        })
      );
    }
  };

  const onChangeValue = (label, value, type) => {
    let arr = { ...mappingFields };
    arr[label] = value;
    setMappingFields(arr);
  };

  const onSelectField = (e, option) => {
    setNotRequiredFields(option);
  };

  const _onTestConnectionCalabrio = () => {
    dispatch(testConnectionCalabrio());
  };

  const _onUpdateCallScoring = () => {
    dispatch(updateCallScoring());
  };

  useEffect(() => {
    if (Object.keys(configCallCenter).length > 0) {
      form.setFieldsValue({
        agent_admin: configCallCenter.agent_admin,
        // access_key_admin: configCallCenter.access_key_admin,
        default_hotline: configCallCenter.default_hotline,
        identify_phone_prefix: _.get(configCallCenter, "identify_phone_prefix"),
        finesse_url_1: configCallCenter.list_finesse_url[0],
        finesse_url_2: configCallCenter?.list_finesse_url[1],
        object_id: configCallCenter.object_id,
        check_3rd: configCallCenter.check_3rd,
        phone_fields_query: configCallCenter.phone_fields_query,
        call_out: configCallCenter.onlyCallOutWhenReady,
        allow_direct_signal: _.get(
          configCallCenter,
          "allow_direct_signal",
          false
        ),
        allow_dynamic_extension: _.get(
          configCallCenter,
          "allow_dynamic_extension",
          false
        ),
        popup_call: configCallCenter?.popup_call,
        search_type: configCallCenter?.search_type,
      });

      setUseThirdParty(configCallCenter.check_3rd);
      $useCalabrio(
        !_.get(configCallCenter, "disable_calabrio_integration", false)
      );
      $isSearchCore(_.get(configCallCenter, "search_core", false));

      if (!_.get(configCallCenter, "disable_calabrio_integration", false)) {
        form.setFieldsValue({
          api_url_check_ha:
            configCallCenter.recording_call_config.api_url_check_ha,
          api_url_get_session:
            configCallCenter.recording_call_config.api_url_get_session,
          metadata_key: configCallCenter.recording_call_config.metadata_key,
          user_admin_calabrio:
            configCallCenter.recording_call_config.user_admin_calabrio,
          host_recording: configCallCenter.recording_call_config.host_recording,
        });
      }

      if (configCallCenter.check_3rd) {
        form.setFieldsValue({
          authentication_type: configCallCenter.thirdparty_config.auth_type,
          user_name: configCallCenter.thirdparty_config.username,
          // password: configCallCenter.thirdparty_config.password,
          api_url: configCallCenter.thirdparty_config.api_url,
          method: configCallCenter.thirdparty_config.method,
          params: configCallCenter.thirdparty_config.params,
          timeout: configCallCenter.thirdparty_config.timeout,
        });
        setTypeAuthen(configCallCenter.thirdparty_config.auth_type);
      }

      if (_.get(configCallCenter, "search_core", false)) {
        let tempMappingCreate = {};
        let tempNotRequiredFields = [];
        let tempSelectFieldsCreate = [];

        _.get(
          configCallCenter,
          "search_core_config.mapping_create",
          []
        ).forEach((item) => {
          tempMappingCreate[item.value] = item.selected_value;
          form.setFieldsValue({
            [item.value]: item.selected_value,
          });

          if (item.required === false) {
            tempSelectFieldsCreate.push(item);
            tempNotRequiredFields.push(item.value);
          }
        });
        setNotRequiredFields(tempSelectFieldsCreate);
        setMappingFields(tempMappingCreate);

        form.setFieldsValue({
          user_filter_search: _.get(
            configCallCenter,
            "search_core_config.user_filter"
          ),
          object_create_record_search: _.get(
            configCallCenter,
            "search_core_config.object_create_record"
          ),
          api_url_search: _.get(configCallCenter, "search_core_config.api_url"),
          key_object_search: _.get(
            configCallCenter,
            "search_core_config.key_object"
          ),
          authentication_type_search: _.get(
            configCallCenter,
            "search_core_config.authentication_type"
          ),
          user_name_search: _.get(
            configCallCenter,
            "search_core_config.user_name"
          ),
          method_search: _.get(configCallCenter, "search_core_config.method"),
          notRequiredFieldCreate: tempNotRequiredFields,
        });
        $typeAuthenSearch(
          _.get(configCallCenter, "search_core_config.authentication_type")
        );
        handleSelectObjectCreateRecord(
          _.get(configCallCenter, "search_core_config.object_create_record"),
          true
        );
      }

      if (configCallCenter.custom_hotlines.length > 0) {
        configCallCenter.custom_hotlines.map((hotline, idx) => {
          return form.setFieldsValue({
            ["hotline_name_" + idx]: hotline.name,
            ["hotline_prefix_" + idx]: hotline.prefix,
            ["hotline_hotline_" + idx]: hotline.hotline,
          });
        });

        setListHotline(configCallCenter.custom_hotlines);
      }

      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: configCallCenter.object_id,
        })
      );

      if (configCallCenter?.object_to_call?.length > 0) {
        configCallCenter.object_to_call.forEach((item, index) => {
          dispatch(
            loadFieldsObjectToCall({
              api_version: "2",
              object_id: item.object_id,
            })
          );

          form.setFieldsValue({
            ["objectToCall_object_" + index]: item.object_id,
            ["objectToCall_fields_" + index]: item.field_id,
          });
        });

        $objectsToCall(_.get(configCallCenter, "object_to_call", []));
      }
    } else {
      form.resetFields();
      setUseThirdParty(false);
      $useCalabrio(false);
      setTypeAuthen("");
      $isSearchCore(false);
      $typeAuthenSearch("");
      setFieldsCreate([]);
      setRequiredFieldsCreate([]);
      setNotRequiredFields([]);
      setListHotline([]);
      $objectsToCall([]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [configCallCenter, form, dispatch]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    if (fieldsObject !== null) {
      let tempFieldsCreate = [];
      let listRequiredFieldsCreate = [];
      fieldsObject.forEach((item) => {
        if (item.type !== "id" && !_.get(item, "encrypted", false)) {
          if (item.required) {
            listRequiredFieldsCreate.push({
              label: item.name,
              value: item.ID,
              type: item.type,
              option: item.option || [],
              required: item.required,
            });
          } else {
            tempFieldsCreate.push({
              label: item.name,
              value: item.ID,
              type: item.type,
              option: item.option || [],
            });
          }
        }
      });
      setFieldsCreate(tempFieldsCreate);
      setRequiredFieldsCreate(listRequiredFieldsCreate);
    }
  }, [fieldsObject]);

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    let tempFields = [];
    listFieldsObject.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempFields.push({
                label: field.related_name,
                value: field.full_field_id,
                type: field.type,
              });
            }
          });
        });
      }
    });
    setRelatedFields(tempFields);
  }, [listFieldsObject]);

  useEffect(() => {
    if (selectedObject) {
      dispatch(
        loadFields({
          api_version: "2",
          object_id: selectedObject,
        })
      );
    }
  }, [dispatch, selectedObject]);

  return (
    <Form
      form={form}
      onFinish={_onSubmit}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      colon={false}
      labelAlign="left"
    >
      <Wrapper>
        <BasicInfo>
          <Form.Item
            label="Agent admin"
            name="agent_admin"
            rules={[
              {
                required: true,
                message: "Please select input Agent admin!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="Access key admin" name="access_key_admin">
            <Input.Password autocomplete="new-password" />
          </Form.Item>

          <Form.Item
            label="Default_hotline"
            name="default_hotline"
            rules={[
              {
                required: true,
                message: "Please select input Default hotline!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item label="Identify prefix" name="identify_phone_prefix">
            <Input />
          </Form.Item>

          <Form.Item
            label="Object"
            name="object_id"
            rules={[{ required: true, message: "Please select Object!" }]}
          >
            <Select
              placeholder="Select type"
              options={listObjects}
              onChange={handleSelectObject}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <Form.Item
            label="Finesse url"
            name="finesse_url_1"
            rules={[
              { required: true, message: "Please input link Finesse Url 1!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item label="Finesse url 2" name="finesse_url_2">
            <Input />
          </Form.Item>

          <Form.Item
            label="Call out when ready"
            name="call_out"
            valuePropName="checked"
          >
            <Switch defaultChecked={true} />
          </Form.Item>

          <Form.Item
            label="Allow direct signal"
            name="allow_direct_signal"
            valuePropName="checked"
          >
            <Switch />
          </Form.Item>

          <Form.Item
            label="Allow dynamic Extension"
            name="allow_dynamic_extension"
            valuePropName="checked"
          >
            <Switch />
          </Form.Item>

          <Form.Item label="Popup new contact" name="popup_call">
            <Select
              placeholder={t("common.placeholderSelect")}
              options={optionsPopup}
              defaultValue="default"
            />
          </Form.Item>

          <Form.Item label="Search type" name="search_type">
            <Select
              placeholder={t("common.placeholderSelect")}
              options={[
                { label: "UCRM", value: "UCRM" },
                { label: "CORE", value: "CORE" },
              ]}
              defaultValue="UCRM"
            />
          </Form.Item>

          <Form.Item
            label="Add fields"
            name="phone_fields_query"
            rules={[{ required: true, message: "Please select fields!" }]}
          >
            <Select
              mode="multiple"
              placeholder="Select fields"
              allowClear
              options={listFields}
              optionFilterProp="label"
            />
          </Form.Item>

          <AddNew onClick={() => _onAddCustomHotline()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ Add custom hotline</span>
          </AddNew>

          {listHotline.length > 0 &&
            listHotline.map((hotline, idx) => {
              return (
                <FormHotlines key={idx}>
                  <Form.Item
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 18 }}
                    label="Name"
                    name={`hotline_name_${idx}`}
                    rules={[
                      { required: true, message: "Please input name hotline!" },
                    ]}
                  >
                    <Input
                      placeholder="Please input"
                      onChange={(e) =>
                        handleChangeCustomHotline(e.target.value, "name", idx)
                      }
                    />
                  </Form.Item>
                  <Form.Item
                    label="Prefix"
                    name={`hotline_prefix_${idx}`}
                    rules={[
                      {
                        required: true,
                        message: "Please input prefix!",
                      },
                    ]}
                  >
                    <InputNumber
                      placeholder="Please input"
                      onChange={(e) =>
                        handleChangeCustomHotline(e, "prefix", idx)
                      }
                    />
                  </Form.Item>

                  <Form.Item
                    label="Hotline"
                    name={`hotline_hotline_${idx}`}
                    rules={[
                      {
                        required: true,
                        message: "Please input hotline!",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Please input"
                      onChange={(e) =>
                        handleChangeCustomHotline(
                          e.target.value,
                          "hotline",
                          idx
                        )
                      }
                    />
                  </Form.Item>

                  <Delete>
                    <Tooltip title="Delete">
                      <img
                        src={DeleteIcon}
                        onClick={() => _onDeleteHotline(idx)}
                        alt="delete"
                      />
                    </Tooltip>
                  </Delete>
                </FormHotlines>
              );
            })}

          <AddNew onClick={() => _onAddObjectToCall()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ Add object click to call</span>
          </AddNew>

          {objectsToCall.length > 0 &&
            objectsToCall.map((objectToCall, idx) => {
              return (
                <FormObjectToCall key={idx}>
                  <Form.Item
                    label="Object"
                    name={`objectToCall_object_${idx}`}
                    rules={[{ required: true, message: "Please select!" }]}
                    style={{ width: "45%" }}
                  >
                    <Select
                      placeholder="Please select"
                      onChange={(e) =>
                        handleChangeObjectToCall(e, "object_id", idx)
                      }
                    >
                      {listObjects
                        ?.filter((item) => item?.value !== selectedObject)
                        ?.map((option) => {
                          return (
                            <Select.Option
                              disabled={
                                objectsToCall.find(
                                  (item) => item?.object_id === option?.value
                                )
                                  ? true
                                  : false
                              }
                              key={option.value}
                            >
                              {option.label}
                            </Select.Option>
                          );
                        })}
                    </Select>
                  </Form.Item>

                  <Form.Item
                    label="Fields"
                    name={`objectToCall_fields_${idx}`}
                    rules={[
                      {
                        required: true,
                        message: "Please select!",
                      },
                    ]}
                    style={{ width: "45%" }}
                  >
                    <Select
                      options={
                        fieldsObjectToCall.find(
                          (item) => item?.object_id === objectToCall?.object_id
                        )?.fields
                      }
                      mode="multiple"
                      placeholder="Please select"
                      onChange={(e) =>
                        handleChangeObjectToCall(e, "field_id", idx)
                      }
                    />
                  </Form.Item>

                  <Delete>
                    <Tooltip title="Delete">
                      <img
                        src={DeleteIcon}
                        onClick={() => _onDeleteObjectToCall(idx)}
                        alt="delete"
                      />
                    </Tooltip>
                  </Delete>
                </FormObjectToCall>
              );
            })}
        </BasicInfo>

        <Right>
          <ThirdParty useThirdParty={useCalabrio}>
            {useCalabrio ? (
              <>
                <legend>
                  <Form.Item
                    labelCol={{ span: 18 }}
                    wrapperCol={{ span: 4 }}
                    label="Use calabrio"
                    name="use_calabrio"
                  >
                    <Switch checked={useCalabrio} onChange={onUseCalabrio} />
                  </Form.Item>
                </legend>
                <WrapThirdParty>
                  <Form.Item
                    label="API url get session"
                    name="api_url_get_session"
                  >
                    <Input placeholder="Please input"></Input>
                  </Form.Item>

                  <Form.Item label="API url check HA" name="api_url_check_ha">
                    <Input placeholder="Please input" />
                  </Form.Item>

                  <Form.Item label="Metadata key" name="metadata_key">
                    <Input placeholder="Please input" />
                  </Form.Item>
                  <Form.Item
                    label="User admin calabrio"
                    name="user_admin_calabrio"
                  >
                    <Input placeholder="Please input" />
                  </Form.Item>
                  <Form.Item
                    label="Password admin calabrio"
                    name="password_admin_calabrio"
                  >
                    <Input.Password placeholder="Please input" />
                  </Form.Item>
                  <Form.Item label="Host recording" name="host_recording">
                    <Input placeholder="Please input" />
                  </Form.Item>

                  {!_.isEmpty(
                    _.get(configCallCenter, "recording_call_config", {})
                  ) && (
                    <WrapButton calabrio>
                      <Button
                        type="primary"
                        style={{ marginRight: "8px" }}
                        onClick={_onTestConnectionCalabrio}
                      >
                        Test connection
                      </Button>
                      <Button type="primary" onClick={_onUpdateCallScoring}>
                        Update call scoring
                      </Button>
                    </WrapButton>
                  )}
                </WrapThirdParty>
              </>
            ) : (
              <Form.Item
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 4 }}
                label="Use calabrio"
                name="use_calabrio"
              >
                <Switch checked={useCalabrio} onChange={onUseCalabrio} />
              </Form.Item>
            )}
          </ThirdParty>

          <ThirdParty useThirdParty={useThirdParty}>
            {useThirdParty ? (
              <>
                <legend>
                  <Form.Item
                    labelCol={{ span: 18 }}
                    wrapperCol={{ span: 4 }}
                    label="Use third party"
                    name="third_party"
                  >
                    <Switch
                      checked={useThirdParty}
                      onChange={onUseThirdParty}
                    />
                  </Form.Item>
                </legend>
                <WrapThirdParty>
                  <Form.Item
                    label="Authentication type"
                    name="authentication_type"
                    rules={[
                      {
                        required: true,
                        message: "Please select authentication type!",
                      },
                    ]}
                  >
                    <Select
                      placeholder="Select authentication type"
                      allowClear
                      options={AuthenticationType}
                      onChange={(e) => onSelectAuthenType(e)}
                    ></Select>
                  </Form.Item>

                  {typeAuthen === "basic" && (
                    <>
                      <Form.Item
                        label="User name"
                        name="user_name"
                        rules={[
                          {
                            required: true,
                            message: "Please input user name!",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item label="Password" name="password">
                        <Input.Password />
                      </Form.Item>
                    </>
                  )}

                  <Form.Item
                    label="API URL"
                    name="api_url"
                    rules={[
                      { required: true, message: "Please input API URL!" },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Method"
                    name="method"
                    rules={[
                      { required: true, message: "Please select method!" },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={Method}
                    ></Radio.Group>
                  </Form.Item>

                  <Form.Item
                    label="Params"
                    name="params"
                    rules={[
                      { required: true, message: "Please input Params!" },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    label="Time out"
                    name="timeout"
                    rules={[
                      { required: true, message: "Please input Time out!" },
                    ]}
                  >
                    <InputNumber min={0} />
                  </Form.Item>
                </WrapThirdParty>
              </>
            ) : (
              <Form.Item
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 4 }}
                label="Use third party"
                name="third_party"
              >
                <Switch checked={useThirdParty} onChange={onUseThirdParty} />
              </Form.Item>
            )}
          </ThirdParty>

          <ThirdParty useThirdParty={isSearchCore}>
            {isSearchCore ? (
              <>
                <legend>
                  <Form.Item
                    labelCol={{ span: 18 }}
                    wrapperCol={{ span: 4 }}
                    label="Search core config"
                    name="search_core"
                  >
                    <Switch checked={isSearchCore} onChange={onUseSearchCore} />
                  </Form.Item>
                </legend>
                <WrapThirdParty>
                  <Form.Item
                    label="Object create record"
                    name="object_create_record_search"
                    rules={[
                      {
                        required: true,
                        message: "Please select Object create record!",
                      },
                    ]}
                  >
                    <Select
                      options={listObjects}
                      onChange={handleSelectObjectCreateRecord}
                    ></Select>
                  </Form.Item>

                  <Form.Item
                    label="Authentication type"
                    name="authentication_type_search"
                    rules={[
                      {
                        required: true,
                        message: "Please select authentication type!",
                      },
                    ]}
                  >
                    <Select
                      options={AuthenticationTypeSearch}
                      onChange={(e) => onSelectAuthenTypeSearch(e)}
                    ></Select>
                  </Form.Item>

                  {typeAuthenSearch === "BASIC_AUTH" && (
                    <>
                      <Form.Item label="User name" name="user_name_search">
                        <Input />
                      </Form.Item>

                      <Form.Item label="Password" name="password_search">
                        <Input.Password />
                      </Form.Item>
                    </>
                  )}

                  {typeAuthenSearch === "token" && (
                    <Form.Item label="Token" name="token_search">
                      <Input />
                    </Form.Item>
                  )}

                  <Form.Item
                    label="API URL"
                    name="api_url_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input API URL!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Key of object"
                    name="key_object_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input Key of object!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="User filter"
                    name="user_filter_search"
                    rules={[
                      {
                        required: true,
                        message: "Please input User filter!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="Method"
                    name="method_search"
                    rules={[
                      { required: true, message: "Please select method!" },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={MethodSearch}
                    ></Radio.Group>
                  </Form.Item>

                  {requiredFieldsCreate.length > 0 && (
                    <>
                      {requiredFieldsCreate.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </>
                  )}

                  <Form.Item
                    label="Mapping fields create"
                    name="notRequiredFieldCreate"
                    style={{ marginTop: "8px" }}
                  >
                    <Select
                      mode="multiple"
                      placeholder={t("coreSetting.placeholderAddfields")}
                      optionFilterProp="label"
                      options={fieldsCreate}
                      onChange={(e, option) => onSelectField(e, option)}
                    />
                  </Form.Item>

                  {notRequiredFields.length > 0 && (
                    <WrapFieldUpdate>
                      {notRequiredFields.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </WrapFieldUpdate>
                  )}
                </WrapThirdParty>
              </>
            ) : (
              <Form.Item
                labelCol={{ span: 4 }}
                wrapperCol={{ span: 4 }}
                label="Search core config"
                name="search_core"
              >
                <Switch checked={isSearchCore} onChange={onUseSearchCore} />
              </Form.Item>
            )}
          </ThirdParty>
        </Right>
      </Wrapper>

      <WrapButton>
        <Button
          disabled={
            (Object.keys(configCallCenter).length > 0 && !checkRule("edit")) ||
            (Object.keys(configCallCenter).length === 0 && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {Object.keys(configCallCenter).length > 0
            ? t("common.update")
            : t("common.save")}
        </Button>

        <Button
          style={{ marginLeft: "16px" }}
          disabled={!checkRule("delete")}
          onClick={() => dispatch(setShowModalConfirm(true))}
        >
          {t("common.reset")}
        </Button>
      </WrapButton>

      <ModalConfirm
        title={t("common.confirmReset")}
        decs={t("common.afterReset")}
        method={resetAllConfigCallCenter}
        data={{}}
        img={ImgConfirm}
      />
    </Form>
  );
}

export default AdvancedConfig;

const Wrapper = styled.div`
  display: flex;
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;

  .ant-form-item {
    margin-bottom: 8px;

    :last-child {
      margin-bottom: 0;
    }
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const BasicInfo = styled.div`
  flex: 1;
  height: fit-content;
  margin-right: 16px;
`;

const Right = styled.div`
  flex: 1;
`;

const ThirdParty = styled.fieldset`
  background: #fff;
  border-radius: 10px;
  height: fit-content;
  border: ${({ useThirdParty }) =>
    useThirdParty ? "1px solid #ececec" : "none"};

  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }

  .ant-input-number {
    width: 100%;
  }
`;

const WrapButton = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: ${({ calabrio }) => (calabrio ? "fit-content" : "83px")};
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    display: flex;
    justify-content: center;
    align-items: center;

    .ant-btn {
      :hover {
        background: ${(props) => props.theme.darker};
        color: #fff;
      }
    }
  }
`;

const WrapThirdParty = styled.div`
  padding: 16px;
`;

const AddNew = styled.div`
  margin-bottom: 8px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const FormHotlines = styled(Form.Item)`
  width: 100%;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
  }

  .ant-col {
    max-width: 100% !important;
  }

  .ant-form-item {
    margin-bottom: 0;
  }
`;

const FormObjectToCall = styled(Form.Item)`
  width: 100%;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
    justify-content: space-around;
    width: 100%;
  }

  .ant-col {
    max-width: 100% !important;
  }

  .ant-form-item {
    margin-bottom: 0;
  }

  .ant-form-item-row {
    flex-direction: column;
  }
`;

const WrapField = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 8px;
  max-height: 90px;
  overflow-y: auto;
  margin-bottom: 8px;

  .ant-form-item {
    :last-child {
      margin-bottom: 0;
    }
  }
`;

const WrapFieldUpdate = styled(WrapField)`
  max-height: unset;
  .ant-form-item {
    :last-child {
      margin-bottom: 8px;
    }
  }
`;
