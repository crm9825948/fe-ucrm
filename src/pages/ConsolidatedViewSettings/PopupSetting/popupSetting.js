import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Table from "antd/lib/table";
import Typography from "antd/lib/typography";
import Tooltip from "antd/lib/tooltip";
import Button from "antd/lib/button";
import Switch from "antd/lib/switch";
import RouterPrompt from "components/Modal/ModalRouterPrompt";
import { Notification } from "components/Notification/Noti";

import Copy from "assets/icons/common/copy.svg";

import {
  resetStatus,
  loadAllPopupSetting,
  updatePopup,
} from "redux/slices/consolidatedViewSettings";
import { FE_URL } from "constants/constants";
import SelectObject from "components/ExternalService/selectObject";

function PopupSetting() {
  const dispatch = useDispatch();
  const { Column } = Table;
  const { Text } = Typography;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { allPopupSetting, statusUpdatePopup } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const [dataTable, setDataTable] = useState([]);
  const [dataPopup, setDataPopup] = useState([]);
  const [change, setChange] = useState(false);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "url_popup_setting" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleActive = (value, key) => {
    setChange(true);

    let tempPopup = [];
    if (dataPopup !== null) {
      tempPopup = [...dataPopup];
    }

    let indexExist = tempPopup.findIndex((item) => item === key);

    if (indexExist !== -1) {
      tempPopup.splice(indexExist, 1);
    } else {
      tempPopup.push(key);
    }

    setDataPopup(tempPopup);
  };

  const _onSave = () => {
    dispatch(
      updatePopup({
        fields: dataPopup,
        object_id: selectedObject,
      })
    );
  };

  const _onCopy = (URL) => {
    navigator.clipboard.writeText(URL);
    Notification("success", "Copy successfully!");
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);
    dispatch(
      loadAllPopupSetting({
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    setDataPopup(allPopupSetting);
  }, [allPopupSetting]);

  useEffect(() => {
    let tempList = [];

    fields_related.map((item, idx) => {
      return tempList.push({
        key: item.ID,
        name: item.name,
        active: false,
        URL: `${FE_URL}/popup/${selectedObject}/${item.ID}/{input params}`,
        URLCopy: `${FE_URL}/popup/${selectedObject}/${item.ID}/`,
        copy: "",
      });
    });

    if (dataPopup !== null) {
      dataPopup.forEach((item) => {
        const fieldIndex = tempList.findIndex((field) => field.key === item);
        if (fieldIndex !== -1) {
          tempList[fieldIndex] = {
            ...tempList[fieldIndex],
            active: true,
          };
        }
      });
    }

    setDataTable(tempList);
  }, [dataPopup, fields_related, selectedObject]);

  useEffect(() => {
    if (statusUpdatePopup === "success") {
      setChange(false);
      dispatch(resetStatus());
    }
  }, [statusUpdatePopup, dispatch]);

  return (
    <Wrapper>
      <RouterPrompt isBlocking={change} />

      <SelectObject
        nameBreadcrumb="URL popup setting"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <Wrap>
          {change && <SaveButton onClick={_onSave}>Save</SaveButton>}
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title="Name"
              dataIndex="name"
              key="name"
              width="205px"
              render={(name) => (
                <Text ellipsis={{ tooltip: name }}>{name}</Text>
              )}
            />
            <Column
              title="Active"
              dataIndex="active"
              key="active"
              width="50px"
              render={(value, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checked={value}
                  checkedChildren="ON"
                  unCheckedChildren="OFF"
                  onChange={(e) => handleActive(e, record.key)}
                />
              )}
            />
            <Column
              title="URL"
              dataIndex="URL"
              key="URL"
              width="882px"
              render={(text) => (
                <Text style={{ width: "882px" }} ellipsis={{ tooltip: text }}>
                  {text}
                </Text>
              )}
            />
            <Column
              title="Copy URL"
              dataIndex="copy"
              key="copy"
              width="107px"
              fixed="right"
              render={(text, record) => (
                <WrapAction>
                  <Tooltip title="Copy URL">
                    <img
                      onClick={() => _onCopy(record.URLCopy)}
                      src={Copy}
                      alt="copy"
                    />
                  </Tooltip>
                </WrapAction>
              )}
            />
          </Table>
        </Wrap>
      )}
    </Wrapper>
  );
}

export default PopupSetting;

const Wrapper = styled.div`
  padding: 16px 24px;
  border-radius: 10px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    .ant-select {
      width: 100%;
    }

    .ant-checkbox-wrapper {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .ant-tooltip-disabled-compatible-wrapper {
      display: flex !important;
      justify-content: center;
      align-items: center;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const SaveButton = styled(Button)`
  border: 1px solid ${(props) => props.theme.main};
  margin: 16px 0 16px 32px;

  span {
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }

  &.ant-btn:active {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    span {
      color: ${(props) => props.theme.main};
    }
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    span {
      color: ${(props) => props.theme.main};
    }
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border: 1px solid ${(props) => props.theme.darker} !important;

    span {
      color: #fff;
    }
  }
`;

const WrapAction = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const Wrap = styled.div`
  background: #fff;
`;
