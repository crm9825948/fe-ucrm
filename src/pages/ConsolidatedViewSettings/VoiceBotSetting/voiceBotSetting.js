import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Button from "antd/lib/button";

import { loadUserDetail } from "redux/slices/user";
import { loadListObjectField } from "redux/slices/objects";
import {
  createVoiceBotSetting,
  updateVoiceBotSetting,
  getDetailsVoiceBot,
} from "redux/slices/consolidatedViewSettings";
import SelectObject from "components/ExternalService/selectObject";

function VoiceBotSetting() {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const jwt = require("jsonwebtoken");

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { detailsVoiceBot } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const [listFields, setListFields] = useState([]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "voice_bot" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSubmit = (values) => {
    let data = {
      tenant_id: userDetail.tenant_id,
      tenant_vbee: values.tenant,
      object_id: selectedObject,
      object_vbee: values.object,
      field_ids: values.fields,
    };
    if (Object.keys(detailsVoiceBot)?.length > 0) {
      dispatch(
        updateVoiceBotSetting({
          ...data,
        })
      );
    } else {
      dispatch(
        createVoiceBotSetting({
          ...data,
        })
      );
    }
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);

    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
      })
    );

    form.resetFields();
    dispatch(
      getDetailsVoiceBot({
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempOptionsFields = [];
    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  useEffect(() => {
    let userDetail = jwt.decode(localStorage.getItem("setting_accessToken"));
    dispatch(
      loadUserDetail({
        _id: userDetail.crm_claims
          ? JSON.parse(userDetail.crm_claims).ID
          : userDetail.ID,
      })
    );
  }, [dispatch, jwt]);

  useEffect(() => {
    if (detailsVoiceBot && Object.keys(detailsVoiceBot)?.length) {
      form.setFieldsValue({
        tenant: detailsVoiceBot.tenant_vbee,
        object: detailsVoiceBot.object_vbee,
        fields: detailsVoiceBot.field_ids,
      });
    }
  }, [detailsVoiceBot, form]);

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="Voice bot setting"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <Wrap>
          <Form
            form={form}
            onFinish={_onSubmit}
            labelCol={{ span: 7 }}
            wrapperCol={{ span: 17 }}
            colon={false}
            labelAlign="right"
          >
            <Form.Item
              label={
                <TenantName>
                  {userDetail?.tenant_name}
                  <span>Tenant</span>
                </TenantName>
              }
              name="tenant"
            >
              <Input placeholder="Please input" />
            </Form.Item>
            <Form.Item
              label={
                <ObjectName>
                  {
                    listObjects.find(
                      (object) => object.value === selectedObject
                    ).label
                  }
                  <span>Object</span>
                </ObjectName>
              }
              name="object"
            >
              <Input placeholder="Please input" />
            </Form.Item>
            <Form.Item label="Fields" name="fields">
              <Select
                mode="multiple"
                placeholder="Please select"
                options={listFields}
                optionFilterProp="label"
              />
            </Form.Item>

            <WrapButton>
              <Button
                disabled={
                  (detailsVoiceBot &&
                    Object.keys(detailsVoiceBot)?.length > 0 &&
                    !checkRule("edit")) ||
                  (detailsVoiceBot &&
                    Object.keys(detailsVoiceBot)?.length === 0 &&
                    !checkRule("create"))
                }
                type="primary"
                htmlType="submit"
              >
                {detailsVoiceBot && Object.keys(detailsVoiceBot)?.length > 0 ? (
                  <span>Update</span>
                ) : (
                  <span>Save</span>
                )}
              </Button>
            </WrapButton>
          </Form>
        </Wrap>
      )}
    </Wrapper>
  );
}

export default VoiceBotSetting;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-form {
    margin: 0 auto;
    /* width: 70%; */
    width: 700px;
  }

  .ant-form-item-label > label {
    height: 100%;
    font-size: 16px;
    color: #2c2c2c;
    float: left;
    text-align: left;
  }
`;

const Wrap = styled.div`
  height: fit-content;
  background: #fff;
  border-radius: 10px;
  padding: 40px;
`;

const TenantName = styled.div`
  display: flex;
  flex-direction: column;
  font-family: var(--roboto-700);
  span {
    font-size: 14px;
    font-family: var(--roboto-400);
    color: #8c8c8c;
  }
`;

const ObjectName = styled(TenantName)``;

const WrapButton = styled.div`
  margin: 16px 0;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 160px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;
