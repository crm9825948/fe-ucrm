import {
  Button,
  Form,
  Input,
  InputNumber,
  Modal,
  Radio,
  Select,
  Switch,
} from "antd";
import Conditions from "components/Conditions/conditions";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  createComponent,
  updateComponent,
} from "redux/slices/consolidatedViewSettings";
import { setShowLoadingScreen } from "redux/slices/global";
import { loadListObjectField } from "redux/slices/objects";
import styled from "styled-components";
import Columns from "./dragAndDropColumn/columns";
import { Notification } from "components/Notification/Noti";
import { checkTokenExpiration } from "contexts/TokenCheck";
import DeleteImg from "assets/icons/common/delete.png";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import TimeRangeFilter from "components/TimeRangeFilter/TimeRangeFilter";
import _ from "lodash";

// import { getDefaultBrandName } from "redux/slices/brandName";

const { Option, OptGroup } = Select;

const ModalComponent = ({
  isModalVisible,
  setIsModalVisible,
  selectedObject,
  dataItem,
  componentID,
  setComponentID,
  setDataItem,
  objectName,
  initData,
  components,
}) => {
  const { listObjectField, targetObjects } = useSelector(
    (state) => state.objectsReducer
  );
  const { isLoadingCreate, loadingComponents } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [form] = Form.useForm();

  const dispatch = useDispatch();

  const [type, setType] = useState("tags");
  const [numberTab, setNumberTab] = useState([]);
  const [objectKnowledge, setObjectKnowledge] = useState([]);
  const [fields, setFields] = useState([]);
  const [listObjectArticle, setListObjectArticle] = useState([]);
  const [haveSetting, setHaveSetting] = useState(true);
  const [showFields, setShowFields] = useState([]);
  const [numberCols, setNumberCols] = useState();
  const [timeRangeFilter, $timeRangeFilter] = useState({
    filter_field_id: "modify_time",
    field_type: "datetime-local",
    filter_type: "all-time",
    amount_of_time: null,
    start_time: null,
    end_time: null,
  });

  useEffect(() => {
    if (components) {
      let flag = true;
      components.forEach((component) => {
        if (component.type === "knowledge" && !componentID) {
          flag = false;
        }
      });
      setHaveSetting(flag);
    }
  }, [components, componentID]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "object/objects-fields-permission",
          {
            api_version: "2",
            object_id: selectedObject,
            show_meta_fields: false,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          if (res.data.data.length > 0) {
            setFields(res.data.data[res.data.data.length - 1].main_object);
          }
        })
        .catch((error) => Notification("error", error.response.data.error));
    };
    checkToken();
  }, [selectedObject, objectKnowledge]);
  useEffect(() => {
    let temp = [];
    let listArticle = [];
    listKnowledgeSetting.forEach((setting) => {
      listArticle.push(setting.article_setting.obj_article);
      setting.object_registered.forEach((object) => {
        temp.push(object);
      });
    });
    setObjectKnowledge(temp);
    let objectsArticle = [];
    if (Object.keys(category).length > 0) {
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (listArticle.includes(object._id)) {
            objectsArticle.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
    }
    setListObjectArticle(objectsArticle);
  }, [listKnowledgeSetting, category]);

  const handleOk = () => {
    setIsModalVisible(false);
  };

  function confirm() {
    Modal.confirm({
      title: "Confirm",
      content: "Something has been changed in layout, please reload this page!",
      okText: "Yes",
      onOk: () => {
        initData();
        window.location.reload();
      },
      wrapClassName: "modal-confirm-button",
    });
  }

  useEffect(() => {
    form.resetFields();
    setColumns([]);
    form.setFieldsValue({
      type: "tags",
    });
    setType("tags");
    /*eslint-disable-next-line*/
  }, []);

  useEffect(() => {
    if (Object.keys(dataItem).length > 0 && componentID && isModalVisible) {
      $timeRangeFilter(
        _.get(dataItem, "time_range_filter", {
          filter_field_id: "modify_time",
          field_type: "datetime-local",
          filter_type: "all-time",
          amount_of_time: null,
          start_time: null,
          end_time: null,
        })
      );
      if (dataItem.type !== "tab" && dataItem.type !== "knowledge") {
        dispatch(
          loadListObjectField({
            api_version: "2",
            object_id:
              (dataItem &&
                dataItem.related_object_details &&
                dataItem.related_object_details._id) ||
              (dataItem && dataItem.related_object),
            show_meta_fields: true,
          })
        );

        setType(dataItem.type);
        let columnTemp = [];
        /*eslint-disable-next-line*/
        dataItem &&
          dataItem.show_fields &&
          // eslint-disable-next-line
          dataItem.show_fields.map((item, idx) => {
            if (item.ID) {
              columnTemp.push(item.ID);
            } else if (item.field_ID) {
              columnTemp.push(item.field_ID);
            }
          });
        setColumns(dataItem.show_fields);
        if (dataItem.type === "table" || dataItem.type === "tags") {
          form.setFieldsValue({
            with_details: dataItem.with_details,
          });
        }
        if (dataItem.type === "details") {
          form.setFieldsValue({
            view: dataItem.view ? dataItem.view : "default",
          });
        }
        if (dataItem.type === "multi_detail") {
          form.setFieldsValue({
            number_of_cols: dataItem.show_fields.length,
          });
          setNumberCols(dataItem.number_of_cols);
          setShowFields(dataItem.show_fields);
        }
        form.setFieldsValue({
          name: dataItem.name,
          type: dataItem.type,
          related_object:
            (dataItem &&
              dataItem.related_object_details &&
              dataItem.related_object_details._id) ||
            (dataItem && dataItem.related_object),
          column: columnTemp,
        });
        if (typeof dataItem.filter_condition === "object") {
          setAllCondition(dataItem.filter_condition.and_filter);
          setAnyCondition(dataItem.filter_condition.or_filter);
        } else {
          let dataItemTemp = JSON.parse(dataItem.filter_condition);
          setAllCondition(dataItemTemp.and_filter);
          setAnyCondition(dataItemTemp.or_filter);
        }
      } else if (dataItem.type === "tab") {
        let numberTabTmp = [];
        form.setFieldsValue({
          name: dataItem.name,
          numberOfTab: dataItem.tab_name.length,
          type: "tab",
        });
        /*eslint-disable-next-line*/
        dataItem.tab_name.map((item, idx) => {
          numberTabTmp.push(`name${idx + 1}`);
          form.setFieldsValue({
            [`name${idx + 1}`]: item,
          });
          // form.setFieldsValue({
          //   [`${item}${idx}`]: item,
          // });
        });
        setNumberTab(numberTabTmp);
        // setNumberTab(dataItem.tab_name);
        setType(dataItem.type);
      } else if (dataItem.type === "knowledge") {
        setType(dataItem.type);
        form.setFieldsValue({
          search_fields: dataItem.search_fields,
          name: dataItem.name,
          type: "knowledge",
          obj_article: dataItem.obj_article,
        });
      }
    } else {
      form.resetFields();
      form.setFieldsValue({
        type: "tags",
      });
      setType("tags");
      setColumns([]);
      $timeRangeFilter({
        filter_field_id: "modify_time",
        field_type: "datetime-local",
        filter_type: "all-time",
        amount_of_time: null,
        start_time: null,
        end_time: null,
      });
    }

    /*eslint-disable-next-line*/
  }, [dataItem, componentID, isModalVisible]);

  const [allFields, setAllFields] = useState([]);

  useEffect(() => {
    if (listObjectField.length > 0)
      setAllFields(listObjectField[listObjectField.length - 1].main_object);
  }, [listObjectField]);

  const { objects_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const handleCancel = () => {
    setIsModalVisible(false);
    setComponentID("");
    setDataItem({});
    setColumns([]);
    form.resetFields();
    form.setFieldsValue({
      type: "tags",
    });
    setType("tags");
  };

  const [columns, setColumns] = useState([]);

  //Condition
  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const onFinish = (values) => {
    let dataColumns = [];
    /*eslint-disable-next-line*/
    columns.forEach((field, idx) => {
      dataColumns.push({
        field_ID: field.ID ? field.ID : field.field_ID,
      });
    });

    let data = {
      name: values.name,
      filter_condition: {
        and_filter: allCondition,
        or_filter: anyCondition,
      },
      related_object: values.related_object,
      show_fields: dataColumns,
      object_id: selectedObject,
      type: values.type,
    };

    if (type === "tab") {
      let tabsName = [];
      /*eslint-disable-next-line*/
      numberTab.forEach((item, idx) => {
        tabsName.push(values[item]);
      });

      data = {
        name: values.name,
        tab_name: tabsName,
        object_id: selectedObject,
        type: values.type,
      };
    }
    if (type === "table" || type === "tags") {
      data = {
        ...data,
        with_details: values.with_details,
        time_range_filter: {
          ...timeRangeFilter,
        },
      };
    }
    if (type === "details") {
      data = { ...data, view: values.view ? values.view : "default" };
    }
    if (type === "knowledge") {
      data = {
        name: values.name,
        type: values.type,
        search_fields: values.search_fields,
        object_id: selectedObject,
        obj_article: values.obj_article,
      };
    }

    if (type === "multi_detail") {
      data = {
        type: values.type,
        number_of_cols: numberCols,
        show_fields: showFields,
        name: values.name,
        filter_condition: {
          and_filter: allCondition,
          or_filter: anyCondition,
        },
        related_object: values.related_object,
        object_id: selectedObject,
      };
    }
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      for (let i = 0; i < operatorValueAnd.length; i++) {
        if (operatorValueAnd[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      for (let i = 0; i < operatorValueOr.length; i++) {
        if (operatorValueOr[i] === undefined) {
          flag = true;
        }
      }
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-empty" &&
        item === "" &&
        operatorValueAnd[idx] !== "mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-mine" &&
        item === "" &&
        operatorValueAnd[idx] !== "today" &&
        item === "" &&
        operatorValueAnd[idx] !== "not-today" &&
        item === "" &&
        operatorValueAnd[idx] !== "yesterday" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-week" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "last-month" &&
        item === "" &&
        operatorValueAnd[idx] !== "this-year" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        item === "" &&
        operatorValueOr[idx] !== "not-empty" &&
        item === "" &&
        operatorValueOr[idx] !== "mine" &&
        item === "" &&
        operatorValueOr[idx] !== "not-mine" &&
        item === "" &&
        operatorValueOr[idx] !== "today" &&
        item === "" &&
        operatorValueOr[idx] !== "not-today" &&
        item === "" &&
        operatorValueOr[idx] !== "yesterday" &&
        item === "" &&
        operatorValueOr[idx] !== "this-week" &&
        item === "" &&
        operatorValueOr[idx] !== "last-week" &&
        item === "" &&
        operatorValueOr[idx] !== "this-month" &&
        item === "" &&
        operatorValueOr[idx] !== "last-month" &&
        item === "" &&
        operatorValueOr[idx] !== "this-year" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        item === "" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });

    if (flag) {
      Notification("warning", "Please fullfill information!");
    } else if (
      _.get(timeRangeFilter, "filter_type", "") === "custom" &&
      _.get(timeRangeFilter, "start_time") === null &&
      _.get(timeRangeFilter, "end_time") === null &&
      (type === "table" || type === "tags")
    ) {
      Notification("warning", "Please select start time or end time!");
    } else {
      if (Object.keys(dataItem).length > 0 && componentID) {
        data = { ...data, id: componentID };
        dispatch(updateComponent(data));
        if (window.location.pathname.includes("consolidated-view/")) {
          confirm();
        }
      } else {
        dispatch(createComponent(data));
      }

      form.resetFields();
      handleCancel();
    }
  };

  useEffect(() => {
    setIsModalVisible(isLoadingCreate);
    /*eslint-disable-next-line*/
  }, [isLoadingCreate, dispatch]);

  // useEffect(() => {
  //   setIsModalVisible(isLoadingUpdate);
  //   /*eslint-disable-next-line*/
  // }, [isLoadingUpdate, dispatch]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
  }, [loadingComponents, dispatch]);

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const deleteNumberTab = (value) => {
    let temp = [...numberTab];
    temp = temp.filter((item) => item !== value);
    form.setFieldsValue({
      ...form.getFieldsValue(),
      numberOfTab: temp.length,
      type: "tab",
      [value]: undefined,
    });
    setNumberTab(temp);
  };

  const deleteNumberCols = (value, idx) => {
    let temp = [...showFields];

    temp.splice(idx, 1);
    setShowFields(temp);
    setNumberCols(temp.length);
    form.setFieldValue("number_of_cols", temp.length);
  };

  return (
    <>
      <CustomModal
        title={`${
          Object.keys(dataItem).length > 0 && componentID
            ? "Edit component"
            : '"Add component"'
        }`}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        width={800}
        footer={false}
      >
        <Form
          name="basic"
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          initialValues={{}}
          onFinish={onFinish}
          onFinishFailed={() => {}}
          autoComplete="off"
          form={form}
        >
          <Form.Item
            label="Name"
            name="name"
            rules={[
              { required: true, message: "Please input component name!" },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="type"
            label="Type"
            rules={[{ required: true, message: "Please pick type!" }]}
          >
            <Radio.Group
              value={type}
              onChange={(e) => {
                setType(e.target.value);
                form.resetFields();
                form.setFieldsValue({
                  type: e.target.value,
                  related_object: "",
                  column: [],
                });
                if (e.target.value === "details") {
                  form.setFieldsValue({
                    view: "default",
                  });
                }
                setAllCondition([]);
                setAnyCondition([]);
                setColumns([]);
              }}
              disabled={
                Object.keys(dataItem).length > 0 && componentID ? true : false
              }
            >
              <CustomItem value="tags">
                <svg
                  width="123"
                  height="123"
                  viewBox="0 0 123 123"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    fill="white"
                    stroke={
                      type === "tags" ? defaultBrandName.theme_color : "#BFBFBF"
                    }
                  />
                  <g clip-path="url(#clip0_5965_195108)">
                    <rect
                      x="42"
                      y="62.998"
                      width="39"
                      height="8"
                      rx="0.5"
                      fill={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                      stroke={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="42"
                      y="28"
                      width="39"
                      height="8"
                      rx="0.5"
                      fill={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                      stroke={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="42.1875"
                      y="39.666"
                      width="39"
                      height="8"
                      rx="0.5"
                      fill={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                      stroke={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="42.1875"
                      y="51.332"
                      width="39"
                      height="8"
                      rx="0.5"
                      fill={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                      stroke={
                        type === "tags"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                  </g>
                  <path
                    d="M51.1914 82.5469V92.5H49.8926V82.5469H51.1914ZM54.3906 82.5469V83.627H46.7002V82.5469H54.3906ZM59.2578 91.2354V87.4277C59.2578 87.1361 59.1986 86.8831 59.0801 86.6689C58.9661 86.4502 58.793 86.2816 58.5605 86.1631C58.3281 86.0446 58.041 85.9854 57.6992 85.9854C57.3802 85.9854 57.0999 86.04 56.8584 86.1494C56.6214 86.2588 56.4346 86.4023 56.2979 86.5801C56.1657 86.7578 56.0996 86.9492 56.0996 87.1543H54.835C54.835 86.89 54.9033 86.6279 55.04 86.3682C55.1768 86.1084 55.3727 85.8737 55.6279 85.6641C55.8877 85.4499 56.1976 85.2812 56.5576 85.1582C56.9222 85.0306 57.3278 84.9668 57.7744 84.9668C58.3122 84.9668 58.7861 85.0579 59.1963 85.2402C59.611 85.4225 59.9346 85.6982 60.167 86.0674C60.404 86.432 60.5225 86.89 60.5225 87.4414V90.8867C60.5225 91.1328 60.543 91.3949 60.584 91.6729C60.6296 91.9508 60.6956 92.1901 60.7822 92.3906V92.5H59.4629C59.3991 92.3542 59.349 92.1605 59.3125 91.9189C59.276 91.6729 59.2578 91.445 59.2578 91.2354ZM59.4766 88.0156L59.4902 88.9043H58.2119C57.8519 88.9043 57.5306 88.9339 57.248 88.9932C56.9655 89.0479 56.7285 89.1322 56.5371 89.2461C56.3457 89.36 56.1999 89.5036 56.0996 89.6768C55.9993 89.8454 55.9492 90.0436 55.9492 90.2715C55.9492 90.5039 56.0016 90.7158 56.1064 90.9072C56.2113 91.0986 56.3685 91.2513 56.5781 91.3652C56.7923 91.4746 57.0544 91.5293 57.3643 91.5293C57.7516 91.5293 58.0934 91.4473 58.3896 91.2832C58.6859 91.1191 58.9206 90.9186 59.0938 90.6816C59.2715 90.4447 59.3672 90.2145 59.3809 89.9912L59.9209 90.5996C59.889 90.791 59.8024 91.0029 59.6611 91.2354C59.5199 91.4678 59.3307 91.6911 59.0938 91.9053C58.8613 92.1149 58.5833 92.2904 58.2598 92.4316C57.9408 92.5684 57.5807 92.6367 57.1797 92.6367C56.6784 92.6367 56.2386 92.5387 55.8604 92.3428C55.4867 92.1468 55.195 91.8848 54.9854 91.5566C54.7803 91.224 54.6777 90.8525 54.6777 90.4424C54.6777 90.0459 54.7552 89.6973 54.9102 89.3965C55.0651 89.0911 55.2884 88.8382 55.5801 88.6377C55.8717 88.4326 56.2227 88.2777 56.6328 88.1729C57.043 88.068 57.501 88.0156 58.0068 88.0156H59.4766ZM67.3037 85.1035H68.4521V92.3428C68.4521 92.9945 68.32 93.5505 68.0557 94.0107C67.7913 94.471 67.4222 94.8197 66.9482 95.0566C66.4788 95.2982 65.9365 95.4189 65.3213 95.4189C65.0661 95.4189 64.7653 95.3779 64.4189 95.2959C64.0771 95.2184 63.7399 95.084 63.4072 94.8926C63.0791 94.7057 62.8034 94.4528 62.5801 94.1338L63.2432 93.3818C63.5531 93.7555 63.8766 94.0153 64.2139 94.1611C64.5557 94.307 64.8929 94.3799 65.2256 94.3799C65.6266 94.3799 65.973 94.3047 66.2646 94.1543C66.5563 94.0039 66.7819 93.7806 66.9414 93.4844C67.1055 93.1927 67.1875 92.8327 67.1875 92.4043V86.7305L67.3037 85.1035ZM62.2109 88.8838V88.7402C62.2109 88.1751 62.277 87.6624 62.4092 87.2021C62.5459 86.7373 62.7396 86.3385 62.9902 86.0059C63.2454 85.6732 63.5531 85.418 63.9131 85.2402C64.2731 85.0579 64.6787 84.9668 65.1299 84.9668C65.5947 84.9668 66.0003 85.0488 66.3467 85.2129C66.6976 85.3724 66.9938 85.6071 67.2354 85.917C67.4814 86.2223 67.6751 86.5915 67.8164 87.0244C67.9577 87.4574 68.0557 87.9473 68.1104 88.4941V89.123C68.0602 89.6654 67.9622 90.153 67.8164 90.5859C67.6751 91.0189 67.4814 91.388 67.2354 91.6934C66.9938 91.9987 66.6976 92.2334 66.3467 92.3975C65.9958 92.557 65.5856 92.6367 65.1162 92.6367C64.6742 92.6367 64.2731 92.5433 63.9131 92.3564C63.5576 92.1696 63.2523 91.9076 62.9971 91.5703C62.7419 91.2331 62.5459 90.8366 62.4092 90.3809C62.277 89.9206 62.2109 89.4215 62.2109 88.8838ZM63.4756 88.7402V88.8838C63.4756 89.2529 63.512 89.5993 63.585 89.9229C63.6624 90.2464 63.7786 90.5312 63.9336 90.7773C64.0931 91.0234 64.2959 91.2171 64.542 91.3584C64.7881 91.4951 65.082 91.5635 65.4238 91.5635C65.8431 91.5635 66.1895 91.4746 66.4629 91.2969C66.7363 91.1191 66.9528 90.8844 67.1123 90.5928C67.2764 90.3011 67.404 89.9844 67.4951 89.6426V87.9951C67.445 87.7445 67.3675 87.5029 67.2627 87.2705C67.1624 87.0335 67.0303 86.8239 66.8662 86.6416C66.7067 86.4548 66.5085 86.3066 66.2715 86.1973C66.0345 86.0879 65.7565 86.0332 65.4375 86.0332C65.0911 86.0332 64.7926 86.1061 64.542 86.252C64.2959 86.3932 64.0931 86.5892 63.9336 86.8398C63.7786 87.0859 63.6624 87.373 63.585 87.7012C63.512 88.0247 63.4756 88.3711 63.4756 88.7402ZM74.6797 90.5381C74.6797 90.3558 74.6387 90.1872 74.5566 90.0322C74.4792 89.8727 74.3174 89.7292 74.0713 89.6016C73.8298 89.4694 73.4652 89.3555 72.9775 89.2598C72.5674 89.1732 72.196 89.0706 71.8633 88.9521C71.5352 88.8337 71.2549 88.6901 71.0225 88.5215C70.7946 88.3529 70.6191 88.1546 70.4961 87.9268C70.373 87.6989 70.3115 87.4323 70.3115 87.127C70.3115 86.8353 70.3753 86.5596 70.5029 86.2998C70.6351 86.04 70.8197 85.8099 71.0566 85.6094C71.2982 85.4089 71.5876 85.2516 71.9248 85.1377C72.262 85.0238 72.638 84.9668 73.0527 84.9668C73.6452 84.9668 74.151 85.0716 74.5703 85.2812C74.9896 85.4909 75.3109 85.7712 75.5342 86.1221C75.7575 86.4684 75.8691 86.8535 75.8691 87.2773H74.6045C74.6045 87.0723 74.543 86.874 74.4199 86.6826C74.3014 86.4867 74.126 86.3249 73.8936 86.1973C73.6657 86.0697 73.3854 86.0059 73.0527 86.0059C72.7018 86.0059 72.417 86.0605 72.1982 86.1699C71.984 86.2747 71.8268 86.4092 71.7266 86.5732C71.6309 86.7373 71.583 86.9105 71.583 87.0928C71.583 87.2295 71.6058 87.3525 71.6514 87.4619C71.7015 87.5667 71.7881 87.6647 71.9111 87.7559C72.0342 87.8424 72.2074 87.9245 72.4307 88.002C72.654 88.0794 72.9388 88.1569 73.2852 88.2344C73.8913 88.3711 74.3903 88.5352 74.7822 88.7266C75.1742 88.918 75.4658 89.1527 75.6572 89.4307C75.8486 89.7087 75.9443 90.0459 75.9443 90.4424C75.9443 90.766 75.876 91.0622 75.7393 91.3311C75.6071 91.5999 75.4134 91.8324 75.1582 92.0283C74.9076 92.2197 74.6068 92.3701 74.2559 92.4795C73.9095 92.5843 73.5199 92.6367 73.0869 92.6367C72.4352 92.6367 71.8838 92.5205 71.4326 92.2881C70.9814 92.0557 70.6396 91.7549 70.4072 91.3857C70.1748 91.0166 70.0586 90.627 70.0586 90.2168H71.3301C71.3483 90.5632 71.4486 90.8389 71.6309 91.0439C71.8132 91.2445 72.0365 91.388 72.3008 91.4746C72.5651 91.5566 72.8271 91.5977 73.0869 91.5977C73.4333 91.5977 73.7227 91.5521 73.9551 91.4609C74.1921 91.3698 74.3721 91.2445 74.4951 91.085C74.6182 90.9255 74.6797 90.7432 74.6797 90.5381Z"
                    fill={
                      type === "tags" ? defaultBrandName.theme_color : "#BFBFBF"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_5965_195108">
                      <rect
                        width="40"
                        height="44"
                        fill="white"
                        transform="translate(41.5 27.5)"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </CustomItem>
              <CustomItem value="table">
                <svg
                  width="123"
                  height="123"
                  viewBox="0 0 123 123"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    fill="white"
                    stroke={
                      type === "table"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <g clip-path="url(#clip0_5965_195110)">
                    <rect
                      x="42"
                      y="41.1973"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="42"
                      y="51.0996"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <path
                      d="M42 61.0039H55V71.0385H43.5C42.6716 71.0385 42 70.367 42 69.5385V61.0039Z"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="55"
                      y="41.1973"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="55"
                      y="51.0996"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="55"
                      y="61.0039"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="68"
                      y="41.1973"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="68"
                      y="51.0996"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <rect
                      x="68"
                      y="61.0039"
                      width="13"
                      height="10.0346"
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <path
                      d="M42 30.5C42 29.1193 43.1193 28 44.5 28H78.5C79.8807 28 81 29.1193 81 30.5V41H42V30.5Z"
                      fill={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                      stroke={
                        type === "table"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                  </g>
                  <path
                    d="M49.3936 82.5469V92.5H48.0947V82.5469H49.3936ZM52.5928 82.5469V83.627H44.9023V82.5469H52.5928ZM57.46 91.2354V87.4277C57.46 87.1361 57.4007 86.8831 57.2822 86.6689C57.1683 86.4502 56.9951 86.2816 56.7627 86.1631C56.5303 86.0446 56.2432 85.9854 55.9014 85.9854C55.5824 85.9854 55.3021 86.04 55.0605 86.1494C54.8236 86.2588 54.6367 86.4023 54.5 86.5801C54.3678 86.7578 54.3018 86.9492 54.3018 87.1543H53.0371C53.0371 86.89 53.1055 86.6279 53.2422 86.3682C53.3789 86.1084 53.5749 85.8737 53.8301 85.6641C54.0898 85.4499 54.3997 85.2812 54.7598 85.1582C55.1243 85.0306 55.5299 84.9668 55.9766 84.9668C56.5143 84.9668 56.9883 85.0579 57.3984 85.2402C57.8132 85.4225 58.1367 85.6982 58.3691 86.0674C58.6061 86.432 58.7246 86.89 58.7246 87.4414V90.8867C58.7246 91.1328 58.7451 91.3949 58.7861 91.6729C58.8317 91.9508 58.8978 92.1901 58.9844 92.3906V92.5H57.665C57.6012 92.3542 57.5511 92.1605 57.5146 91.9189C57.4782 91.6729 57.46 91.445 57.46 91.2354ZM57.6787 88.0156L57.6924 88.9043H56.4141C56.054 88.9043 55.7327 88.9339 55.4502 88.9932C55.1676 89.0479 54.9307 89.1322 54.7393 89.2461C54.5479 89.36 54.402 89.5036 54.3018 89.6768C54.2015 89.8454 54.1514 90.0436 54.1514 90.2715C54.1514 90.5039 54.2038 90.7158 54.3086 90.9072C54.4134 91.0986 54.5706 91.2513 54.7803 91.3652C54.9945 91.4746 55.2565 91.5293 55.5664 91.5293C55.9538 91.5293 56.2956 91.4473 56.5918 91.2832C56.888 91.1191 57.1227 90.9186 57.2959 90.6816C57.4736 90.4447 57.5693 90.2145 57.583 89.9912L58.123 90.5996C58.0911 90.791 58.0046 91.0029 57.8633 91.2354C57.722 91.4678 57.5329 91.6911 57.2959 91.9053C57.0635 92.1149 56.7855 92.2904 56.4619 92.4316C56.1429 92.5684 55.7829 92.6367 55.3818 92.6367C54.8805 92.6367 54.4408 92.5387 54.0625 92.3428C53.6888 92.1468 53.3971 91.8848 53.1875 91.5566C52.9824 91.224 52.8799 90.8525 52.8799 90.4424C52.8799 90.0459 52.9574 89.6973 53.1123 89.3965C53.2673 89.0911 53.4906 88.8382 53.7822 88.6377C54.0739 88.4326 54.4248 88.2777 54.835 88.1729C55.2451 88.068 55.7031 88.0156 56.209 88.0156H57.6787ZM60.707 82H61.9785V91.0645L61.8691 92.5H60.707V82ZM66.9756 88.7402V88.8838C66.9756 89.4215 66.9118 89.9206 66.7842 90.3809C66.6566 90.8366 66.4697 91.2331 66.2236 91.5703C65.9775 91.9076 65.6768 92.1696 65.3213 92.3564C64.9658 92.5433 64.5579 92.6367 64.0977 92.6367C63.6283 92.6367 63.2158 92.557 62.8604 92.3975C62.5094 92.2334 62.2132 91.9987 61.9717 91.6934C61.7301 91.388 61.5365 91.0189 61.3906 90.5859C61.2493 90.153 61.1514 89.6654 61.0967 89.123V88.4941C61.1514 87.9473 61.2493 87.4574 61.3906 87.0244C61.5365 86.5915 61.7301 86.2223 61.9717 85.917C62.2132 85.6071 62.5094 85.3724 62.8604 85.2129C63.2113 85.0488 63.6191 84.9668 64.084 84.9668C64.5488 84.9668 64.9613 85.0579 65.3213 85.2402C65.6813 85.418 65.9821 85.6732 66.2236 86.0059C66.4697 86.3385 66.6566 86.7373 66.7842 87.2021C66.9118 87.6624 66.9756 88.1751 66.9756 88.7402ZM65.7041 88.8838V88.7402C65.7041 88.3711 65.6699 88.0247 65.6016 87.7012C65.5332 87.373 65.4238 87.0859 65.2734 86.8398C65.123 86.5892 64.9248 86.3932 64.6787 86.252C64.4326 86.1061 64.1296 86.0332 63.7695 86.0332C63.4505 86.0332 63.1725 86.0879 62.9355 86.1973C62.7031 86.3066 62.5049 86.4548 62.3408 86.6416C62.1768 86.8239 62.0423 87.0335 61.9375 87.2705C61.8372 87.5029 61.762 87.7445 61.7119 87.9951V89.6426C61.7848 89.9616 61.9033 90.2692 62.0674 90.5654C62.236 90.8571 62.4593 91.0964 62.7373 91.2832C63.0199 91.4701 63.3685 91.5635 63.7832 91.5635C64.125 91.5635 64.4167 91.4951 64.6582 91.3584C64.9043 91.2171 65.1025 91.0234 65.2529 90.7773C65.4079 90.5312 65.5218 90.2464 65.5947 89.9229C65.6676 89.5993 65.7041 89.2529 65.7041 88.8838ZM69.9492 82V92.5H68.6777V82H69.9492ZM75.0488 92.6367C74.5339 92.6367 74.0667 92.5501 73.6475 92.377C73.2327 92.1992 72.875 91.9508 72.5742 91.6318C72.278 91.3128 72.0501 90.9346 71.8906 90.4971C71.7311 90.0596 71.6514 89.5811 71.6514 89.0615V88.7744C71.6514 88.1729 71.7402 87.6374 71.918 87.168C72.0957 86.694 72.3372 86.293 72.6426 85.9648C72.9479 85.6367 73.2943 85.3883 73.6816 85.2197C74.069 85.0511 74.4701 84.9668 74.8848 84.9668C75.4134 84.9668 75.8691 85.0579 76.252 85.2402C76.6393 85.4225 76.9561 85.6777 77.2021 86.0059C77.4482 86.3294 77.6305 86.7122 77.749 87.1543C77.8675 87.5918 77.9268 88.0703 77.9268 88.5898V89.1572H72.4033V88.125H76.6621V88.0293C76.6439 87.7012 76.5755 87.3822 76.457 87.0723C76.3431 86.7624 76.1608 86.5072 75.9102 86.3066C75.6595 86.1061 75.3177 86.0059 74.8848 86.0059C74.5977 86.0059 74.3333 86.0674 74.0918 86.1904C73.8503 86.3089 73.6429 86.4867 73.4697 86.7236C73.2965 86.9606 73.1621 87.25 73.0664 87.5918C72.9707 87.9336 72.9229 88.3278 72.9229 88.7744V89.0615C72.9229 89.4124 72.9707 89.7428 73.0664 90.0527C73.1667 90.3581 73.3102 90.627 73.4971 90.8594C73.6885 91.0918 73.9186 91.2741 74.1875 91.4062C74.4609 91.5384 74.7708 91.6045 75.1172 91.6045C75.5638 91.6045 75.9421 91.5133 76.252 91.3311C76.5618 91.1488 76.833 90.9049 77.0654 90.5996L77.8311 91.208C77.6715 91.4495 77.4688 91.6797 77.2227 91.8984C76.9766 92.1172 76.6735 92.2949 76.3135 92.4316C75.958 92.5684 75.5365 92.6367 75.0488 92.6367Z"
                    fill={
                      type === "table"
                        ? defaultBrandName.theme_color
                        : "#8C8C8C"
                    }
                  />
                  <defs>
                    <clipPath id="clip0_5965_195110">
                      <rect
                        x="41.5"
                        y="27.5"
                        width="40"
                        height="44"
                        rx="1"
                        fill="white"
                      />
                    </clipPath>
                  </defs>
                </svg>
              </CustomItem>
              <CustomItem value="details">
                <svg
                  width="123"
                  height="123"
                  viewBox="0 0 123 123"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    fill="white"
                    stroke={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <path
                    d="M42 29.5C42 28.6716 42.6716 28 43.5 28H79.5C80.3284 28 81 28.6716 81 29.5V41H42V29.5Z"
                    fill={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#BFBFBF"
                    }
                    stroke={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#BFBFBF"
                    }
                  />
                  <path
                    d="M41.5 66.5C41.5 65.9477 41.9477 65.5 42.5 65.5H80.5C81.0523 65.5 81.5 65.9477 81.5 66.5V69.5C81.5 70.6046 80.6046 71.5 79.5 71.5H43.5C42.3954 71.5 41.5 70.6046 41.5 69.5V66.5Z"
                    fill={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="41.5"
                    y="55.5215"
                    width="40"
                    height="6"
                    fill={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="41.5"
                    y="45.543"
                    width="40"
                    height="6"
                    fill={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <path
                    d="M47.541 92.5H45.4629L45.4766 91.4268H47.541C48.252 91.4268 48.8444 91.2786 49.3184 90.9824C49.7923 90.6816 50.1478 90.2624 50.3848 89.7246C50.6263 89.1823 50.7471 88.5488 50.7471 87.8242V87.2158C50.7471 86.6462 50.6787 86.1403 50.542 85.6982C50.4053 85.2516 50.2048 84.8757 49.9404 84.5703C49.6761 84.2604 49.3525 84.0257 48.9697 83.8662C48.5915 83.7067 48.1562 83.627 47.6641 83.627H45.4219V82.5469H47.6641C48.3158 82.5469 48.9105 82.6562 49.4482 82.875C49.986 83.0892 50.4486 83.4014 50.8359 83.8115C51.2279 84.2171 51.5286 84.7093 51.7383 85.2881C51.9479 85.8623 52.0527 86.5094 52.0527 87.2295V87.8242C52.0527 88.5443 51.9479 89.1937 51.7383 89.7725C51.5286 90.3467 51.2256 90.8366 50.8291 91.2422C50.4372 91.6478 49.9632 91.96 49.4072 92.1787C48.8558 92.3929 48.2337 92.5 47.541 92.5ZM46.167 82.5469V92.5H44.8477V82.5469H46.167ZM56.9131 92.6367C56.3981 92.6367 55.931 92.5501 55.5117 92.377C55.097 92.1992 54.7393 91.9508 54.4385 91.6318C54.1423 91.3128 53.9144 90.9346 53.7549 90.4971C53.5954 90.0596 53.5156 89.5811 53.5156 89.0615V88.7744C53.5156 88.1729 53.6045 87.6374 53.7822 87.168C53.96 86.694 54.2015 86.293 54.5068 85.9648C54.8122 85.6367 55.1585 85.3883 55.5459 85.2197C55.9333 85.0511 56.3343 84.9668 56.749 84.9668C57.2777 84.9668 57.7334 85.0579 58.1162 85.2402C58.5036 85.4225 58.8203 85.6777 59.0664 86.0059C59.3125 86.3294 59.4948 86.7122 59.6133 87.1543C59.7318 87.5918 59.791 88.0703 59.791 88.5898V89.1572H54.2676V88.125H58.5264V88.0293C58.5081 87.7012 58.4398 87.3822 58.3213 87.0723C58.2074 86.7624 58.0251 86.5072 57.7744 86.3066C57.5238 86.1061 57.182 86.0059 56.749 86.0059C56.4619 86.0059 56.1976 86.0674 55.9561 86.1904C55.7145 86.3089 55.5072 86.4867 55.334 86.7236C55.1608 86.9606 55.0264 87.25 54.9307 87.5918C54.835 87.9336 54.7871 88.3278 54.7871 88.7744V89.0615C54.7871 89.4124 54.835 89.7428 54.9307 90.0527C55.0309 90.3581 55.1745 90.627 55.3613 90.8594C55.5527 91.0918 55.7829 91.2741 56.0518 91.4062C56.3252 91.5384 56.6351 91.6045 56.9814 91.6045C57.4281 91.6045 57.8063 91.5133 58.1162 91.3311C58.4261 91.1488 58.6973 90.9049 58.9297 90.5996L59.6953 91.208C59.5358 91.4495 59.333 91.6797 59.0869 91.8984C58.8408 92.1172 58.5378 92.2949 58.1777 92.4316C57.8223 92.5684 57.4007 92.6367 56.9131 92.6367ZM64.3643 85.1035V86.0742H60.3652V85.1035H64.3643ZM61.7188 83.3057H62.9834V90.668C62.9834 90.9186 63.0221 91.1077 63.0996 91.2354C63.1771 91.363 63.2773 91.4473 63.4004 91.4883C63.5234 91.5293 63.6556 91.5498 63.7969 91.5498C63.9017 91.5498 64.0111 91.5407 64.125 91.5225C64.2435 91.4997 64.3324 91.4814 64.3916 91.4678L64.3984 92.5C64.2982 92.5319 64.166 92.5615 64.002 92.5889C63.8424 92.6208 63.6488 92.6367 63.4209 92.6367C63.111 92.6367 62.8262 92.5752 62.5664 92.4521C62.3066 92.3291 62.0993 92.124 61.9443 91.8369C61.7939 91.5452 61.7188 91.1533 61.7188 90.6611V83.3057ZM70.209 91.2354V87.4277C70.209 87.1361 70.1497 86.8831 70.0312 86.6689C69.9173 86.4502 69.7441 86.2816 69.5117 86.1631C69.2793 86.0446 68.9922 85.9854 68.6504 85.9854C68.3314 85.9854 68.0511 86.04 67.8096 86.1494C67.5726 86.2588 67.3857 86.4023 67.249 86.5801C67.1169 86.7578 67.0508 86.9492 67.0508 87.1543H65.7861C65.7861 86.89 65.8545 86.6279 65.9912 86.3682C66.1279 86.1084 66.3239 85.8737 66.5791 85.6641C66.8389 85.4499 67.1488 85.2812 67.5088 85.1582C67.8734 85.0306 68.279 84.9668 68.7256 84.9668C69.2633 84.9668 69.7373 85.0579 70.1475 85.2402C70.5622 85.4225 70.8857 85.6982 71.1182 86.0674C71.3551 86.432 71.4736 86.89 71.4736 87.4414V90.8867C71.4736 91.1328 71.4941 91.3949 71.5352 91.6729C71.5807 91.9508 71.6468 92.1901 71.7334 92.3906V92.5H70.4141C70.3503 92.3542 70.3001 92.1605 70.2637 91.9189C70.2272 91.6729 70.209 91.445 70.209 91.2354ZM70.4277 88.0156L70.4414 88.9043H69.1631C68.8031 88.9043 68.4818 88.9339 68.1992 88.9932C67.9167 89.0479 67.6797 89.1322 67.4883 89.2461C67.2969 89.36 67.151 89.5036 67.0508 89.6768C66.9505 89.8454 66.9004 90.0436 66.9004 90.2715C66.9004 90.5039 66.9528 90.7158 67.0576 90.9072C67.1624 91.0986 67.3197 91.2513 67.5293 91.3652C67.7435 91.4746 68.0055 91.5293 68.3154 91.5293C68.7028 91.5293 69.0446 91.4473 69.3408 91.2832C69.637 91.1191 69.8717 90.9186 70.0449 90.6816C70.2227 90.4447 70.3184 90.2145 70.332 89.9912L70.8721 90.5996C70.8402 90.791 70.7536 91.0029 70.6123 91.2354C70.471 91.4678 70.2819 91.6911 70.0449 91.9053C69.8125 92.1149 69.5345 92.2904 69.2109 92.4316C68.8919 92.5684 68.5319 92.6367 68.1309 92.6367C67.6296 92.6367 67.1898 92.5387 66.8115 92.3428C66.4378 92.1468 66.1462 91.8848 65.9365 91.5566C65.7314 91.224 65.6289 90.8525 65.6289 90.4424C65.6289 90.0459 65.7064 89.6973 65.8613 89.3965C66.0163 89.0911 66.2396 88.8382 66.5312 88.6377C66.8229 88.4326 67.1738 88.2777 67.584 88.1729C67.9941 88.068 68.4521 88.0156 68.958 88.0156H70.4277ZM74.8369 85.1035V92.5H73.5654V85.1035H74.8369ZM73.4697 83.1416C73.4697 82.9365 73.5312 82.7633 73.6543 82.6221C73.7819 82.4808 73.9688 82.4102 74.2148 82.4102C74.4564 82.4102 74.641 82.4808 74.7686 82.6221C74.9007 82.7633 74.9668 82.9365 74.9668 83.1416C74.9668 83.3376 74.9007 83.5062 74.7686 83.6475C74.641 83.7842 74.4564 83.8525 74.2148 83.8525C73.9688 83.8525 73.7819 83.7842 73.6543 83.6475C73.5312 83.5062 73.4697 83.3376 73.4697 83.1416ZM78.2412 82V92.5H76.9697V82H78.2412Z"
                    fill={
                      type === "details"
                        ? defaultBrandName.theme_color
                        : "#8C8C8C"
                    }
                  />
                </svg>
              </CustomItem>
              <CustomItem value="tab">
                <svg
                  width="123"
                  height="123"
                  viewBox="0 0 123 123"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    fill="white"
                    stroke={
                      type === "tab" ? defaultBrandName.theme_color : "#D9D9D9"
                    }
                  />
                  <path
                    d="M46.3143 27.6207C44.8584 28.0593 43.9661 29.3843 43.9661 31.113C43.9661 32.6182 43.6216 33.5732 42.8233 34.3559C42.1894 34.9753 41.9073 35.4485 41.6804 36.3173L41.5 37.0054V53.2204V69.4354L41.7037 69.9086C41.962 70.5105 42.4004 70.9923 42.9482 71.2762L43.3788 71.5H61.5H79.6212L80.0596 71.2762C80.6155 70.9923 81.038 70.5276 81.2963 69.9171L81.5 69.4354V53.6505V37.8657L81.2963 37.3924C80.8657 36.3945 80.1298 35.8786 78.9714 35.758C78.2045 35.6804 77.8678 35.5517 77.4841 35.1987C76.8498 34.6138 76.6151 33.7364 76.529 31.6288C76.4506 29.7279 76.2865 29.2031 75.5429 28.4374C74.6975 27.5771 74.3608 27.5 71.3082 27.5H68.8657L69.2494 27.8956C69.6878 28.3513 70.0555 29.0914 70.1886 29.7795L70.2747 30.2527H71.9812C74.0947 30.2527 73.9616 30.158 74.0947 31.7149C74.2433 33.5041 74.4939 34.5021 75.0029 35.3278L75.269 35.758H73.171C70.8461 35.758 70.5253 35.7064 70.0086 35.2417C69.3351 34.6223 69.1004 33.7795 69.0143 31.6288C68.9359 29.7279 68.7718 29.2031 68.0282 28.4374C67.1829 27.5771 66.8461 27.5 63.7935 27.5H61.3514L61.7351 27.8956C62.1735 28.3513 62.5412 29.0914 62.6743 29.7795L62.7604 30.2527H64.4669C66.5882 30.2527 66.4551 30.158 66.5804 31.7149C66.7212 33.4696 66.9796 34.5021 67.4886 35.3278L67.7547 35.758H65.6567C63.2927 35.758 63.0033 35.7064 62.4629 35.1987C61.8208 34.5967 61.5861 33.7364 61.5 31.6288C61.4608 30.6137 61.3984 29.9943 61.3122 29.7364C61.0461 28.9277 60.2788 28.0674 59.4882 27.7148C59.1282 27.5516 58.8935 27.5426 52.8894 27.5256C48.0988 27.517 46.5882 27.5345 46.3143 27.6207ZM77.7269 38.6309C78.1575 38.8031 78.5331 39.1556 78.7759 39.6114L78.9951 40.0155L79.0184 53.4783C79.042 68.6437 79.089 67.4565 78.4392 68.1535C77.7894 68.8505 79.269 68.7899 61.5 68.7899C43.6918 68.7899 45.2024 68.8501 44.5608 68.1364C43.9188 67.4309 43.9657 68.6007 43.9657 53.6505C43.9657 38.7089 43.9188 39.8441 44.5527 39.1561C45.171 38.468 43.6914 38.5281 61.5 38.5196C74.6114 38.5192 77.4845 38.5367 77.7269 38.6309Z"
                    fill={
                      type === "tab" ? defaultBrandName.theme_color : "#BFBFBF"
                    }
                  />
                  <path
                    d="M51.1914 82.5469V92.5H49.8926V82.5469H51.1914ZM54.3906 82.5469V83.627H46.7002V82.5469H54.3906ZM59.2578 91.2354V87.4277C59.2578 87.1361 59.1986 86.8831 59.0801 86.6689C58.9661 86.4502 58.793 86.2816 58.5605 86.1631C58.3281 86.0446 58.041 85.9854 57.6992 85.9854C57.3802 85.9854 57.0999 86.04 56.8584 86.1494C56.6214 86.2588 56.4346 86.4023 56.2979 86.5801C56.1657 86.7578 56.0996 86.9492 56.0996 87.1543H54.835C54.835 86.89 54.9033 86.6279 55.04 86.3682C55.1768 86.1084 55.3727 85.8737 55.6279 85.6641C55.8877 85.4499 56.1976 85.2812 56.5576 85.1582C56.9222 85.0306 57.3278 84.9668 57.7744 84.9668C58.3122 84.9668 58.7861 85.0579 59.1963 85.2402C59.611 85.4225 59.9346 85.6982 60.167 86.0674C60.404 86.432 60.5225 86.89 60.5225 87.4414V90.8867C60.5225 91.1328 60.543 91.3949 60.584 91.6729C60.6296 91.9508 60.6956 92.1901 60.7822 92.3906V92.5H59.4629C59.3991 92.3542 59.349 92.1605 59.3125 91.9189C59.276 91.6729 59.2578 91.445 59.2578 91.2354ZM59.4766 88.0156L59.4902 88.9043H58.2119C57.8519 88.9043 57.5306 88.9339 57.248 88.9932C56.9655 89.0479 56.7285 89.1322 56.5371 89.2461C56.3457 89.36 56.1999 89.5036 56.0996 89.6768C55.9993 89.8454 55.9492 90.0436 55.9492 90.2715C55.9492 90.5039 56.0016 90.7158 56.1064 90.9072C56.2113 91.0986 56.3685 91.2513 56.5781 91.3652C56.7923 91.4746 57.0544 91.5293 57.3643 91.5293C57.7516 91.5293 58.0934 91.4473 58.3896 91.2832C58.6859 91.1191 58.9206 90.9186 59.0938 90.6816C59.2715 90.4447 59.3672 90.2145 59.3809 89.9912L59.9209 90.5996C59.889 90.791 59.8024 91.0029 59.6611 91.2354C59.5199 91.4678 59.3307 91.6911 59.0938 91.9053C58.8613 92.1149 58.5833 92.2904 58.2598 92.4316C57.9408 92.5684 57.5807 92.6367 57.1797 92.6367C56.6784 92.6367 56.2386 92.5387 55.8604 92.3428C55.4867 92.1468 55.195 91.8848 54.9854 91.5566C54.7803 91.224 54.6777 90.8525 54.6777 90.4424C54.6777 90.0459 54.7552 89.6973 54.9102 89.3965C55.0651 89.0911 55.2884 88.8382 55.5801 88.6377C55.8717 88.4326 56.2227 88.2777 56.6328 88.1729C57.043 88.068 57.501 88.0156 58.0068 88.0156H59.4766ZM62.5049 82H63.7764V91.0645L63.667 92.5H62.5049V82ZM68.7734 88.7402V88.8838C68.7734 89.4215 68.7096 89.9206 68.582 90.3809C68.4544 90.8366 68.2676 91.2331 68.0215 91.5703C67.7754 91.9076 67.4746 92.1696 67.1191 92.3564C66.7637 92.5433 66.3558 92.6367 65.8955 92.6367C65.4261 92.6367 65.0137 92.557 64.6582 92.3975C64.3073 92.2334 64.0111 91.9987 63.7695 91.6934C63.528 91.388 63.3343 91.0189 63.1885 90.5859C63.0472 90.153 62.9492 89.6654 62.8945 89.123V88.4941C62.9492 87.9473 63.0472 87.4574 63.1885 87.0244C63.3343 86.5915 63.528 86.2223 63.7695 85.917C64.0111 85.6071 64.3073 85.3724 64.6582 85.2129C65.0091 85.0488 65.417 84.9668 65.8818 84.9668C66.3467 84.9668 66.7591 85.0579 67.1191 85.2402C67.4792 85.418 67.7799 85.6732 68.0215 86.0059C68.2676 86.3385 68.4544 86.7373 68.582 87.2021C68.7096 87.6624 68.7734 88.1751 68.7734 88.7402ZM67.502 88.8838V88.7402C67.502 88.3711 67.4678 88.0247 67.3994 87.7012C67.3311 87.373 67.2217 87.0859 67.0713 86.8398C66.9209 86.5892 66.7227 86.3932 66.4766 86.252C66.2305 86.1061 65.9274 86.0332 65.5674 86.0332C65.2484 86.0332 64.9704 86.0879 64.7334 86.1973C64.501 86.3066 64.3027 86.4548 64.1387 86.6416C63.9746 86.8239 63.8402 87.0335 63.7354 87.2705C63.6351 87.5029 63.5599 87.7445 63.5098 87.9951V89.6426C63.5827 89.9616 63.7012 90.2692 63.8652 90.5654C64.0339 90.8571 64.2572 91.0964 64.5352 91.2832C64.8177 91.4701 65.1663 91.5635 65.5811 91.5635C65.9229 91.5635 66.2145 91.4951 66.4561 91.3584C66.7021 91.2171 66.9004 91.0234 67.0508 90.7773C67.2057 90.5312 67.3197 90.2464 67.3926 89.9229C67.4655 89.5993 67.502 89.2529 67.502 88.8838ZM74.6797 90.5381C74.6797 90.3558 74.6387 90.1872 74.5566 90.0322C74.4792 89.8727 74.3174 89.7292 74.0713 89.6016C73.8298 89.4694 73.4652 89.3555 72.9775 89.2598C72.5674 89.1732 72.196 89.0706 71.8633 88.9521C71.5352 88.8337 71.2549 88.6901 71.0225 88.5215C70.7946 88.3529 70.6191 88.1546 70.4961 87.9268C70.373 87.6989 70.3115 87.4323 70.3115 87.127C70.3115 86.8353 70.3753 86.5596 70.5029 86.2998C70.6351 86.04 70.8197 85.8099 71.0566 85.6094C71.2982 85.4089 71.5876 85.2516 71.9248 85.1377C72.262 85.0238 72.638 84.9668 73.0527 84.9668C73.6452 84.9668 74.151 85.0716 74.5703 85.2812C74.9896 85.4909 75.3109 85.7712 75.5342 86.1221C75.7575 86.4684 75.8691 86.8535 75.8691 87.2773H74.6045C74.6045 87.0723 74.543 86.874 74.4199 86.6826C74.3014 86.4867 74.126 86.3249 73.8936 86.1973C73.6657 86.0697 73.3854 86.0059 73.0527 86.0059C72.7018 86.0059 72.417 86.0605 72.1982 86.1699C71.984 86.2747 71.8268 86.4092 71.7266 86.5732C71.6309 86.7373 71.583 86.9105 71.583 87.0928C71.583 87.2295 71.6058 87.3525 71.6514 87.4619C71.7015 87.5667 71.7881 87.6647 71.9111 87.7559C72.0342 87.8424 72.2074 87.9245 72.4307 88.002C72.654 88.0794 72.9388 88.1569 73.2852 88.2344C73.8913 88.3711 74.3903 88.5352 74.7822 88.7266C75.1742 88.918 75.4658 89.1527 75.6572 89.4307C75.8486 89.7087 75.9443 90.0459 75.9443 90.4424C75.9443 90.766 75.876 91.0622 75.7393 91.3311C75.6071 91.5999 75.4134 91.8324 75.1582 92.0283C74.9076 92.2197 74.6068 92.3701 74.2559 92.4795C73.9095 92.5843 73.5199 92.6367 73.0869 92.6367C72.4352 92.6367 71.8838 92.5205 71.4326 92.2881C70.9814 92.0557 70.6396 91.7549 70.4072 91.3857C70.1748 91.0166 70.0586 90.627 70.0586 90.2168H71.3301C71.3483 90.5632 71.4486 90.8389 71.6309 91.0439C71.8132 91.2445 72.0365 91.388 72.3008 91.4746C72.5651 91.5566 72.8271 91.5977 73.0869 91.5977C73.4333 91.5977 73.7227 91.5521 73.9551 91.4609C74.1921 91.3698 74.3721 91.2445 74.4951 91.085C74.6182 90.9255 74.6797 90.7432 74.6797 90.5381Z"
                    fill={
                      type === "tab" ? defaultBrandName.theme_color : "#8C8C8C"
                    }
                  />
                </svg>
              </CustomItem>

              {/* <CustomItem value="multi_detail" style={{ marginTop: "-24px" }}>
                <img alt="" src={MultiDetail} />
              </CustomItem> */}

              <CustomItem style={{ marginTop: "24px" }} value="multi_detail">
                <svg
                  width="123"
                  height="123"
                  viewBox="0 0 123 123"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    fill="white"
                  />
                  <path
                    d="M43.5 28H79.5C80.3284 28 81 28.6716 81 29.5V41H42V29.5C42 28.6716 42.6716 28 43.5 28Z"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                    stroke={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="63"
                    y="66"
                    width="18"
                    height="6"
                    rx="1"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="63"
                    y="56"
                    width="18"
                    height="6"
                    rx="1"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="41"
                    y="46"
                    width="18"
                    height="26"
                    rx="1"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <rect
                    x="63"
                    y="46"
                    width="18"
                    height="6"
                    rx="1"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                  <path
                    d="M28.5029 82.5469H29.7812L33.042 90.6611L36.2959 82.5469H37.5811L33.5342 92.5H32.5361L28.5029 82.5469ZM28.0859 82.5469H29.2139L29.3984 88.6172V92.5H28.0859V82.5469ZM36.8633 82.5469H37.9912V92.5H36.6787V88.6172L36.8633 82.5469ZM44.6426 90.791V85.1035H45.9141V92.5H44.7041L44.6426 90.791ZM44.8818 89.2324L45.4082 89.2188C45.4082 89.7109 45.3558 90.1667 45.251 90.5859C45.1507 91.0007 44.9867 91.3607 44.7588 91.666C44.5309 91.9714 44.2324 92.2106 43.8633 92.3838C43.4941 92.5524 43.0452 92.6367 42.5166 92.6367C42.1566 92.6367 41.8262 92.5843 41.5254 92.4795C41.2292 92.3747 40.974 92.2129 40.7598 91.9941C40.5456 91.7754 40.3792 91.4906 40.2607 91.1396C40.1468 90.7887 40.0898 90.3672 40.0898 89.875V85.1035H41.3545V89.8887C41.3545 90.2214 41.391 90.4971 41.4639 90.7158C41.5413 90.93 41.6439 91.1009 41.7715 91.2285C41.9036 91.3516 42.0495 91.4382 42.209 91.4883C42.373 91.5384 42.5417 91.5635 42.7148 91.5635C43.2526 91.5635 43.6787 91.4609 43.9932 91.2559C44.3076 91.0462 44.5332 90.766 44.6699 90.415C44.8112 90.0596 44.8818 89.6654 44.8818 89.2324ZM49.2158 82V92.5H47.9443V82H49.2158ZM54.3428 85.1035V86.0742H50.3438V85.1035H54.3428ZM51.6973 83.3057H52.9619V90.668C52.9619 90.9186 53.0007 91.1077 53.0781 91.2354C53.1556 91.363 53.2559 91.4473 53.3789 91.4883C53.502 91.5293 53.6341 91.5498 53.7754 91.5498C53.8802 91.5498 53.9896 91.5407 54.1035 91.5225C54.222 91.4997 54.3109 91.4814 54.3701 91.4678L54.377 92.5C54.2767 92.5319 54.1445 92.5615 53.9805 92.5889C53.821 92.6208 53.6273 92.6367 53.3994 92.6367C53.0895 92.6367 52.8047 92.5752 52.5449 92.4521C52.2852 92.3291 52.0778 92.124 51.9229 91.8369C51.7725 91.5452 51.6973 91.1533 51.6973 90.6611V83.3057ZM57.2002 85.1035V92.5H55.9287V85.1035H57.2002ZM55.833 83.1416C55.833 82.9365 55.8945 82.7633 56.0176 82.6221C56.1452 82.4808 56.332 82.4102 56.5781 82.4102C56.8197 82.4102 57.0042 82.4808 57.1318 82.6221C57.264 82.7633 57.3301 82.9365 57.3301 83.1416C57.3301 83.3376 57.264 83.5062 57.1318 83.6475C57.0042 83.7842 56.8197 83.8525 56.5781 83.8525C56.332 83.8525 56.1452 83.7842 56.0176 83.6475C55.8945 83.5062 55.833 83.3376 55.833 83.1416ZM67.3652 91.0645V82H68.6367V92.5H67.4746L67.3652 91.0645ZM62.3887 88.8838V88.7402C62.3887 88.1751 62.457 87.6624 62.5938 87.2021C62.735 86.7373 62.9333 86.3385 63.1885 86.0059C63.4482 85.6732 63.7559 85.418 64.1113 85.2402C64.4714 85.0579 64.8724 84.9668 65.3145 84.9668C65.7793 84.9668 66.1849 85.0488 66.5312 85.2129C66.8822 85.3724 67.1784 85.6071 67.4199 85.917C67.666 86.2223 67.8597 86.5915 68.001 87.0244C68.1423 87.4574 68.2402 87.9473 68.2949 88.4941V89.123C68.2448 89.6654 68.1468 90.153 68.001 90.5859C67.8597 91.0189 67.666 91.388 67.4199 91.6934C67.1784 91.9987 66.8822 92.2334 66.5312 92.3975C66.1803 92.557 65.7702 92.6367 65.3008 92.6367C64.8678 92.6367 64.4714 92.5433 64.1113 92.3564C63.7559 92.1696 63.4482 91.9076 63.1885 91.5703C62.9333 91.2331 62.735 90.8366 62.5938 90.3809C62.457 89.9206 62.3887 89.4215 62.3887 88.8838ZM63.6602 88.7402V88.8838C63.6602 89.2529 63.6966 89.5993 63.7695 89.9229C63.847 90.2464 63.9655 90.5312 64.125 90.7773C64.2845 91.0234 64.4873 91.2171 64.7334 91.3584C64.9795 91.4951 65.2734 91.5635 65.6152 91.5635C66.0345 91.5635 66.3786 91.4746 66.6475 91.2969C66.9209 91.1191 67.1396 90.8844 67.3037 90.5928C67.4678 90.3011 67.5954 89.9844 67.6865 89.6426V87.9951C67.6318 87.7445 67.5521 87.5029 67.4473 87.2705C67.347 87.0335 67.2148 86.8239 67.0508 86.6416C66.8913 86.4548 66.693 86.3066 66.4561 86.1973C66.2236 86.0879 65.9479 86.0332 65.6289 86.0332C65.2826 86.0332 64.984 86.1061 64.7334 86.252C64.4873 86.3932 64.2845 86.5892 64.125 86.8398C63.9655 87.0859 63.847 87.373 63.7695 87.7012C63.6966 88.0247 63.6602 88.3711 63.6602 88.7402ZM73.6748 92.6367C73.1598 92.6367 72.6927 92.5501 72.2734 92.377C71.8587 92.1992 71.501 91.9508 71.2002 91.6318C70.904 91.3128 70.6761 90.9346 70.5166 90.4971C70.3571 90.0596 70.2773 89.5811 70.2773 89.0615V88.7744C70.2773 88.1729 70.3662 87.6374 70.5439 87.168C70.7217 86.694 70.9632 86.293 71.2686 85.9648C71.5739 85.6367 71.9202 85.3883 72.3076 85.2197C72.695 85.0511 73.096 84.9668 73.5107 84.9668C74.0394 84.9668 74.4951 85.0579 74.8779 85.2402C75.2653 85.4225 75.582 85.6777 75.8281 86.0059C76.0742 86.3294 76.2565 86.7122 76.375 87.1543C76.4935 87.5918 76.5527 88.0703 76.5527 88.5898V89.1572H71.0293V88.125H75.2881V88.0293C75.2699 87.7012 75.2015 87.3822 75.083 87.0723C74.9691 86.7624 74.7868 86.5072 74.5361 86.3066C74.2855 86.1061 73.9437 86.0059 73.5107 86.0059C73.2236 86.0059 72.9593 86.0674 72.7178 86.1904C72.4762 86.3089 72.2689 86.4867 72.0957 86.7236C71.9225 86.9606 71.7881 87.25 71.6924 87.5918C71.5967 87.9336 71.5488 88.3278 71.5488 88.7744V89.0615C71.5488 89.4124 71.5967 89.7428 71.6924 90.0527C71.7926 90.3581 71.9362 90.627 72.123 90.8594C72.3145 91.0918 72.5446 91.2741 72.8135 91.4062C73.0869 91.5384 73.3968 91.6045 73.7432 91.6045C74.1898 91.6045 74.568 91.5133 74.8779 91.3311C75.1878 91.1488 75.459 90.9049 75.6914 90.5996L76.457 91.208C76.2975 91.4495 76.0947 91.6797 75.8486 91.8984C75.6025 92.1172 75.2995 92.2949 74.9395 92.4316C74.584 92.5684 74.1624 92.6367 73.6748 92.6367ZM81.126 85.1035V86.0742H77.127V85.1035H81.126ZM78.4805 83.3057H79.7451V90.668C79.7451 90.9186 79.7839 91.1077 79.8613 91.2354C79.9388 91.363 80.0391 91.4473 80.1621 91.4883C80.2852 91.5293 80.4173 91.5498 80.5586 91.5498C80.6634 91.5498 80.7728 91.5407 80.8867 91.5225C81.0052 91.4997 81.0941 91.4814 81.1533 91.4678L81.1602 92.5C81.0599 92.5319 80.9277 92.5615 80.7637 92.5889C80.6042 92.6208 80.4105 92.6367 80.1826 92.6367C79.8727 92.6367 79.5879 92.5752 79.3281 92.4521C79.0684 92.3291 78.861 92.124 78.7061 91.8369C78.5557 91.5452 78.4805 91.1533 78.4805 90.6611V83.3057ZM86.9707 91.2354V87.4277C86.9707 87.1361 86.9115 86.8831 86.793 86.6689C86.679 86.4502 86.5059 86.2816 86.2734 86.1631C86.041 86.0446 85.7539 85.9854 85.4121 85.9854C85.0931 85.9854 84.8128 86.04 84.5713 86.1494C84.3343 86.2588 84.1475 86.4023 84.0107 86.5801C83.8786 86.7578 83.8125 86.9492 83.8125 87.1543H82.5479C82.5479 86.89 82.6162 86.6279 82.7529 86.3682C82.8896 86.1084 83.0856 85.8737 83.3408 85.6641C83.6006 85.4499 83.9105 85.2812 84.2705 85.1582C84.6351 85.0306 85.0407 84.9668 85.4873 84.9668C86.0251 84.9668 86.499 85.0579 86.9092 85.2402C87.3239 85.4225 87.6475 85.6982 87.8799 86.0674C88.1169 86.432 88.2354 86.89 88.2354 87.4414V90.8867C88.2354 91.1328 88.2559 91.3949 88.2969 91.6729C88.3424 91.9508 88.4085 92.1901 88.4951 92.3906V92.5H87.1758C87.112 92.3542 87.0618 92.1605 87.0254 91.9189C86.9889 91.6729 86.9707 91.445 86.9707 91.2354ZM87.1895 88.0156L87.2031 88.9043H85.9248C85.5648 88.9043 85.2435 88.9339 84.9609 88.9932C84.6784 89.0479 84.4414 89.1322 84.25 89.2461C84.0586 89.36 83.9128 89.5036 83.8125 89.6768C83.7122 89.8454 83.6621 90.0436 83.6621 90.2715C83.6621 90.5039 83.7145 90.7158 83.8193 90.9072C83.9242 91.0986 84.0814 91.2513 84.291 91.3652C84.5052 91.4746 84.7673 91.5293 85.0771 91.5293C85.4645 91.5293 85.8063 91.4473 86.1025 91.2832C86.3988 91.1191 86.6335 90.9186 86.8066 90.6816C86.9844 90.4447 87.0801 90.2145 87.0938 89.9912L87.6338 90.5996C87.6019 90.791 87.5153 91.0029 87.374 91.2354C87.2327 91.4678 87.0436 91.6911 86.8066 91.9053C86.5742 92.1149 86.2962 92.2904 85.9727 92.4316C85.6536 92.5684 85.2936 92.6367 84.8926 92.6367C84.3913 92.6367 83.9515 92.5387 83.5732 92.3428C83.1995 92.1468 82.9079 91.8848 82.6982 91.5566C82.4932 91.224 82.3906 90.8525 82.3906 90.4424C82.3906 90.0459 82.4681 89.6973 82.623 89.3965C82.778 89.0911 83.0013 88.8382 83.293 88.6377C83.5846 88.4326 83.9355 88.2777 84.3457 88.1729C84.7559 88.068 85.2139 88.0156 85.7197 88.0156H87.1895ZM91.5986 85.1035V92.5H90.3271V85.1035H91.5986ZM90.2314 83.1416C90.2314 82.9365 90.293 82.7633 90.416 82.6221C90.5436 82.4808 90.7305 82.4102 90.9766 82.4102C91.2181 82.4102 91.4027 82.4808 91.5303 82.6221C91.6624 82.7633 91.7285 82.9365 91.7285 83.1416C91.7285 83.3376 91.6624 83.5062 91.5303 83.6475C91.4027 83.7842 91.2181 83.8525 90.9766 83.8525C90.7305 83.8525 90.5436 83.7842 90.416 83.6475C90.293 83.5062 90.2314 83.3376 90.2314 83.1416ZM95.0029 82V92.5H93.7314V82H95.0029Z"
                    fill={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#8c8c8c"
                    }
                  />
                  <rect
                    x="0.5"
                    y="0.5"
                    width="122"
                    height="122"
                    rx="3.5"
                    stroke={
                      type === "multi_detail"
                        ? defaultBrandName.theme_color
                        : "#D9D9D9"
                    }
                  />
                </svg>
              </CustomItem>

              {!objectKnowledge.includes(selectedObject) && haveSetting && (
                <CustomItem style={{ marginTop: "24px" }} value="knowledge">
                  <svg
                    width="123"
                    height="123"
                    viewBox="0 0 123 123"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <rect
                      x="0.5"
                      y="0.5"
                      width="122"
                      height="122"
                      rx="3.5"
                      fill="white"
                      stroke={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#D9D9D9"
                      }
                    />
                    <path
                      d="M60.6014 64.623C59.5058 64.0158 58.5035 63.3841 57.3893 62.954C54.8578 61.9727 52.2284 61.5228 49.521 61.489C49.0408 61.4832 48.5618 61.496 48.0828 61.489C47.2133 61.4703 46.6515 60.8992 46.6492 60.0228C46.6492 58.0414 46.6492 56.0717 46.6492 54.0962C46.6492 48.1685 46.6678 42.2408 46.6329 36.313C46.6259 35.1638 47.2156 34.5077 48.4615 34.5007C51.8788 34.4797 55.1981 34.9436 58.3963 36.1767C59.0419 36.4334 59.6733 36.7244 60.2879 37.0485C60.3927 37.0898 60.481 37.1644 60.5391 37.2609C60.5972 37.3574 60.622 37.4703 60.6095 37.5823C60.5967 39.5636 60.6026 41.5345 60.6026 43.51V64.623H60.6014Z"
                      fill={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#D9D9D9"
                      }
                    />
                    <path
                      d="M63.3916 64.6281V54.7482C63.3916 49.0675 63.3896 43.3869 63.3857 37.7062C63.3857 37.3706 63.4568 37.1759 63.7855 37.0069C66.0477 35.8228 68.4697 35.1666 70.9755 34.7739C72.5897 34.5233 74.2144 34.493 75.8426 34.5128C76.761 34.5244 77.3438 35.0955 77.3438 36.0081C77.3438 43.9988 77.3438 51.9895 77.3438 59.9801C77.3438 60.9265 76.7797 61.4953 75.8146 61.4789C73.5559 61.4451 71.3181 61.5955 69.12 62.1549C67.5827 62.543 66.0979 63.0629 64.7237 63.8682C64.3018 64.12 63.8741 64.3554 63.3916 64.6281Z"
                      fill={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#D9D9D9"
                      }
                    />
                    <path
                      d="M57.7063 65.2174H43.6317C42.5315 65.2174 42 64.6941 42 63.5951C42 55.6906 42 47.7862 42 39.8818C42 38.7478 42.507 38.2501 43.655 38.2501C44.0163 38.2501 44.4988 38.1335 44.704 38.313C44.9487 38.5286 44.7821 39.024 44.7821 39.3969C44.7879 46.2256 44.8042 53.0543 44.7739 59.8841C44.7669 61.6498 45.8765 62.9319 47.282 63.264C47.9813 63.4307 48.7005 63.3619 49.4102 63.3585C51.5815 63.3445 53.7408 63.6802 55.8053 64.3526C56.476 64.5514 57.1158 64.8425 57.7063 65.2174Z"
                      fill={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <path
                      d="M66.2852 65.2173C68.1989 64.1928 70.2479 63.7115 72.3644 63.4691C73.4786 63.3409 74.5975 63.37 75.7152 63.3653C77.8388 63.3572 79.1966 62.0029 79.1977 59.8747C79.1977 52.828 79.1946 45.7814 79.1884 38.7348C79.1884 38.3316 79.2921 38.1999 79.6884 38.2418C80.0043 38.2756 80.3283 38.2418 80.6464 38.2535C80.9988 38.2588 81.3353 38.4009 81.585 38.6497C81.8346 38.8984 81.9778 39.2344 81.9844 39.5868C81.9914 39.7173 81.9844 39.8479 81.9844 39.9784C81.9844 47.8106 81.9681 55.6416 81.9996 63.4725C82.0054 64.7138 81.2443 65.2418 80.2595 65.2336C75.7409 65.1975 71.2222 65.2185 66.7036 65.2185L66.2852 65.2173Z"
                      fill={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#BFBFBF"
                      }
                    />
                    <path
                      d="M13.2256 82.5469V92.5H11.9062V82.5469H13.2256ZM19.2344 82.5469L15.0986 87.1885L12.7744 89.6016L12.5557 88.1934L14.3057 86.2656L17.6484 82.5469H19.2344ZM17.9629 92.5L14.2783 87.6465L15.0645 86.6006L19.5352 92.5H17.9629ZM21.6064 86.6826V92.5H20.3418V85.1035H21.5381L21.6064 86.6826ZM21.3057 88.5215L20.7793 88.501C20.7839 87.9951 20.859 87.528 21.0049 87.0996C21.1507 86.6667 21.3558 86.2907 21.6201 85.9717C21.8844 85.6527 22.1989 85.4066 22.5635 85.2334C22.9326 85.0557 23.3405 84.9668 23.7871 84.9668C24.1517 84.9668 24.4798 85.0169 24.7715 85.1172C25.0632 85.2129 25.3115 85.3678 25.5166 85.582C25.7262 85.7962 25.8857 86.0742 25.9951 86.416C26.1045 86.7533 26.1592 87.1657 26.1592 87.6533V92.5H24.8877V87.6396C24.8877 87.2523 24.8307 86.9424 24.7168 86.71C24.6029 86.473 24.4365 86.3021 24.2178 86.1973C23.999 86.0879 23.7301 86.0332 23.4111 86.0332C23.0967 86.0332 22.8096 86.0993 22.5498 86.2314C22.2946 86.3636 22.0736 86.5459 21.8867 86.7783C21.7044 87.0107 21.5609 87.2773 21.4561 87.5781C21.3558 87.8743 21.3057 88.1888 21.3057 88.5215ZM27.7451 88.8838V88.7266C27.7451 88.1934 27.8226 87.6989 27.9775 87.2432C28.1325 86.7829 28.3558 86.3841 28.6475 86.0469C28.9391 85.7051 29.2923 85.4408 29.707 85.2539C30.1217 85.0625 30.5866 84.9668 31.1016 84.9668C31.6211 84.9668 32.0882 85.0625 32.5029 85.2539C32.9222 85.4408 33.2777 85.7051 33.5693 86.0469C33.8656 86.3841 34.0911 86.7829 34.2461 87.2432C34.401 87.6989 34.4785 88.1934 34.4785 88.7266V88.8838C34.4785 89.417 34.401 89.9115 34.2461 90.3672C34.0911 90.8229 33.8656 91.2217 33.5693 91.5635C33.2777 91.9007 32.9245 92.165 32.5098 92.3564C32.0996 92.5433 31.6348 92.6367 31.1152 92.6367C30.5957 92.6367 30.1286 92.5433 29.7139 92.3564C29.2992 92.165 28.9437 91.9007 28.6475 91.5635C28.3558 91.2217 28.1325 90.8229 27.9775 90.3672C27.8226 89.9115 27.7451 89.417 27.7451 88.8838ZM29.0098 88.7266V88.8838C29.0098 89.2529 29.0531 89.6016 29.1396 89.9297C29.2262 90.2533 29.3561 90.5404 29.5293 90.791C29.707 91.0417 29.9281 91.2399 30.1924 91.3857C30.4567 91.527 30.7643 91.5977 31.1152 91.5977C31.4616 91.5977 31.7646 91.527 32.0244 91.3857C32.2887 91.2399 32.5075 91.0417 32.6807 90.791C32.8538 90.5404 32.9837 90.2533 33.0703 89.9297C33.1615 89.6016 33.207 89.2529 33.207 88.8838V88.7266C33.207 88.362 33.1615 88.0179 33.0703 87.6943C32.9837 87.3662 32.8516 87.0768 32.6738 86.8262C32.5007 86.571 32.2819 86.3704 32.0176 86.2246C31.7578 86.0788 31.4525 86.0059 31.1016 86.0059C30.7552 86.0059 30.4499 86.0788 30.1855 86.2246C29.9258 86.3704 29.707 86.571 29.5293 86.8262C29.3561 87.0768 29.2262 87.3662 29.1396 87.6943C29.0531 88.0179 29.0098 88.362 29.0098 88.7266ZM37.9375 91.1875L39.8379 85.1035H40.6719L40.5078 86.3135L38.5732 92.5H37.7598L37.9375 91.1875ZM36.6592 85.1035L38.2793 91.2559L38.3955 92.5H37.541L35.3945 85.1035H36.6592ZM42.4902 91.208L44.0352 85.1035H45.293L43.1465 92.5H42.2988L42.4902 91.208ZM40.8564 85.1035L42.7158 91.085L42.9277 92.5H42.1211L40.1318 86.2998L39.9678 85.1035H40.8564ZM47.9658 82V92.5H46.6943V82H47.9658ZM53.0654 92.6367C52.5505 92.6367 52.0833 92.5501 51.6641 92.377C51.2493 92.1992 50.8916 91.9508 50.5908 91.6318C50.2946 91.3128 50.0667 90.9346 49.9072 90.4971C49.7477 90.0596 49.668 89.5811 49.668 89.0615V88.7744C49.668 88.1729 49.7568 87.6374 49.9346 87.168C50.1123 86.694 50.3538 86.293 50.6592 85.9648C50.9645 85.6367 51.3109 85.3883 51.6982 85.2197C52.0856 85.0511 52.4867 84.9668 52.9014 84.9668C53.43 84.9668 53.8857 85.0579 54.2686 85.2402C54.6559 85.4225 54.9727 85.6777 55.2188 86.0059C55.4648 86.3294 55.6471 86.7122 55.7656 87.1543C55.8841 87.5918 55.9434 88.0703 55.9434 88.5898V89.1572H50.4199V88.125H54.6787V88.0293C54.6605 87.7012 54.5921 87.3822 54.4736 87.0723C54.3597 86.7624 54.1774 86.5072 53.9268 86.3066C53.6761 86.1061 53.3343 86.0059 52.9014 86.0059C52.6143 86.0059 52.3499 86.0674 52.1084 86.1904C51.8669 86.3089 51.6595 86.4867 51.4863 86.7236C51.3132 86.9606 51.1787 87.25 51.083 87.5918C50.9873 87.9336 50.9395 88.3278 50.9395 88.7744V89.0615C50.9395 89.4124 50.9873 89.7428 51.083 90.0527C51.1833 90.3581 51.3268 90.627 51.5137 90.8594C51.7051 91.0918 51.9352 91.2741 52.2041 91.4062C52.4775 91.5384 52.7874 91.6045 53.1338 91.6045C53.5804 91.6045 53.9587 91.5133 54.2686 91.3311C54.5785 91.1488 54.8496 90.9049 55.082 90.5996L55.8477 91.208C55.6882 91.4495 55.4854 91.6797 55.2393 91.8984C54.9932 92.1172 54.6901 92.2949 54.3301 92.4316C53.9746 92.5684 53.5531 92.6367 53.0654 92.6367ZM62.082 91.0645V82H63.3535V92.5H62.1914L62.082 91.0645ZM57.1055 88.8838V88.7402C57.1055 88.1751 57.1738 87.6624 57.3105 87.2021C57.4518 86.7373 57.6501 86.3385 57.9053 86.0059C58.165 85.6732 58.4727 85.418 58.8281 85.2402C59.1882 85.0579 59.5892 84.9668 60.0312 84.9668C60.4961 84.9668 60.9017 85.0488 61.248 85.2129C61.599 85.3724 61.8952 85.6071 62.1367 85.917C62.3828 86.2223 62.5765 86.5915 62.7178 87.0244C62.859 87.4574 62.957 87.9473 63.0117 88.4941V89.123C62.9616 89.6654 62.8636 90.153 62.7178 90.5859C62.5765 91.0189 62.3828 91.388 62.1367 91.6934C61.8952 91.9987 61.599 92.2334 61.248 92.3975C60.8971 92.557 60.487 92.6367 60.0176 92.6367C59.5846 92.6367 59.1882 92.5433 58.8281 92.3564C58.4727 92.1696 58.165 91.9076 57.9053 91.5703C57.6501 91.2331 57.4518 90.8366 57.3105 90.3809C57.1738 89.9206 57.1055 89.4215 57.1055 88.8838ZM58.377 88.7402V88.8838C58.377 89.2529 58.4134 89.5993 58.4863 89.9229C58.5638 90.2464 58.6823 90.5312 58.8418 90.7773C59.0013 91.0234 59.2041 91.2171 59.4502 91.3584C59.6963 91.4951 59.9902 91.5635 60.332 91.5635C60.7513 91.5635 61.0954 91.4746 61.3643 91.2969C61.6377 91.1191 61.8564 90.8844 62.0205 90.5928C62.1846 90.3011 62.3122 89.9844 62.4033 89.6426V87.9951C62.3486 87.7445 62.2689 87.5029 62.1641 87.2705C62.0638 87.0335 61.9316 86.8239 61.7676 86.6416C61.6081 86.4548 61.4098 86.3066 61.1729 86.1973C60.9404 86.0879 60.6647 86.0332 60.3457 86.0332C59.9993 86.0332 59.7008 86.1061 59.4502 86.252C59.2041 86.3932 59.0013 86.5892 58.8418 86.8398C58.6823 87.0859 58.5638 87.373 58.4863 87.7012C58.4134 88.0247 58.377 88.3711 58.377 88.7402ZM70.1143 85.1035H71.2627V92.3428C71.2627 92.9945 71.1305 93.5505 70.8662 94.0107C70.6019 94.471 70.2327 94.8197 69.7588 95.0566C69.2894 95.2982 68.7471 95.4189 68.1318 95.4189C67.8766 95.4189 67.5758 95.3779 67.2295 95.2959C66.8877 95.2184 66.5505 95.084 66.2178 94.8926C65.8896 94.7057 65.6139 94.4528 65.3906 94.1338L66.0537 93.3818C66.3636 93.7555 66.6872 94.0153 67.0244 94.1611C67.3662 94.307 67.7035 94.3799 68.0361 94.3799C68.4372 94.3799 68.7835 94.3047 69.0752 94.1543C69.3669 94.0039 69.5924 93.7806 69.752 93.4844C69.916 93.1927 69.998 92.8327 69.998 92.4043V86.7305L70.1143 85.1035ZM65.0215 88.8838V88.7402C65.0215 88.1751 65.0876 87.6624 65.2197 87.2021C65.3564 86.7373 65.5501 86.3385 65.8008 86.0059C66.056 85.6732 66.3636 85.418 66.7236 85.2402C67.0837 85.0579 67.4893 84.9668 67.9404 84.9668C68.4053 84.9668 68.8109 85.0488 69.1572 85.2129C69.5081 85.3724 69.8044 85.6071 70.0459 85.917C70.292 86.2223 70.4857 86.5915 70.627 87.0244C70.7682 87.4574 70.8662 87.9473 70.9209 88.4941V89.123C70.8708 89.6654 70.7728 90.153 70.627 90.5859C70.4857 91.0189 70.292 91.388 70.0459 91.6934C69.8044 91.9987 69.5081 92.2334 69.1572 92.3975C68.8063 92.557 68.3962 92.6367 67.9268 92.6367C67.4847 92.6367 67.0837 92.5433 66.7236 92.3564C66.3682 92.1696 66.0628 91.9076 65.8076 91.5703C65.5524 91.2331 65.3564 90.8366 65.2197 90.3809C65.0876 89.9206 65.0215 89.4215 65.0215 88.8838ZM66.2861 88.7402V88.8838C66.2861 89.2529 66.3226 89.5993 66.3955 89.9229C66.473 90.2464 66.5892 90.5312 66.7441 90.7773C66.9036 91.0234 67.1064 91.2171 67.3525 91.3584C67.5986 91.4951 67.8926 91.5635 68.2344 91.5635C68.6536 91.5635 69 91.4746 69.2734 91.2969C69.5469 91.1191 69.7633 90.8844 69.9229 90.5928C70.0869 90.3011 70.2145 89.9844 70.3057 89.6426V87.9951C70.2555 87.7445 70.1781 87.5029 70.0732 87.2705C69.973 87.0335 69.8408 86.8239 69.6768 86.6416C69.5173 86.4548 69.319 86.3066 69.082 86.1973C68.8451 86.0879 68.5671 86.0332 68.248 86.0332C67.9017 86.0332 67.6032 86.1061 67.3525 86.252C67.1064 86.3932 66.9036 86.5892 66.7441 86.8398C66.5892 87.0859 66.473 87.373 66.3955 87.7012C66.3226 88.0247 66.2861 88.3711 66.2861 88.7402ZM76.2529 92.6367C75.738 92.6367 75.2708 92.5501 74.8516 92.377C74.4368 92.1992 74.0791 91.9508 73.7783 91.6318C73.4821 91.3128 73.2542 90.9346 73.0947 90.4971C72.9352 90.0596 72.8555 89.5811 72.8555 89.0615V88.7744C72.8555 88.1729 72.9443 87.6374 73.1221 87.168C73.2998 86.694 73.5413 86.293 73.8467 85.9648C74.152 85.6367 74.4984 85.3883 74.8857 85.2197C75.2731 85.0511 75.6742 84.9668 76.0889 84.9668C76.6175 84.9668 77.0732 85.0579 77.4561 85.2402C77.8434 85.4225 78.1602 85.6777 78.4062 86.0059C78.6523 86.3294 78.8346 86.7122 78.9531 87.1543C79.0716 87.5918 79.1309 88.0703 79.1309 88.5898V89.1572H73.6074V88.125H77.8662V88.0293C77.848 87.7012 77.7796 87.3822 77.6611 87.0723C77.5472 86.7624 77.3649 86.5072 77.1143 86.3066C76.8636 86.1061 76.5218 86.0059 76.0889 86.0059C75.8018 86.0059 75.5374 86.0674 75.2959 86.1904C75.0544 86.3089 74.847 86.4867 74.6738 86.7236C74.5007 86.9606 74.3662 87.25 74.2705 87.5918C74.1748 87.9336 74.127 88.3278 74.127 88.7744V89.0615C74.127 89.4124 74.1748 89.7428 74.2705 90.0527C74.3708 90.3581 74.5143 90.627 74.7012 90.8594C74.8926 91.0918 75.1227 91.2741 75.3916 91.4062C75.665 91.5384 75.9749 91.6045 76.3213 91.6045C76.7679 91.6045 77.1462 91.5133 77.4561 91.3311C77.766 91.1488 78.0371 90.9049 78.2695 90.5996L79.0352 91.208C78.8757 91.4495 78.6729 91.6797 78.4268 91.8984C78.1807 92.1172 77.8776 92.2949 77.5176 92.4316C77.1621 92.5684 76.7406 92.6367 76.2529 92.6367ZM84.0732 82H85.3447V91.0645L85.2354 92.5H84.0732V82ZM90.3418 88.7402V88.8838C90.3418 89.4215 90.278 89.9206 90.1504 90.3809C90.0228 90.8366 89.8359 91.2331 89.5898 91.5703C89.3438 91.9076 89.043 92.1696 88.6875 92.3564C88.332 92.5433 87.9242 92.6367 87.4639 92.6367C86.9945 92.6367 86.582 92.557 86.2266 92.3975C85.8757 92.2334 85.5794 91.9987 85.3379 91.6934C85.0964 91.388 84.9027 91.0189 84.7568 90.5859C84.6156 90.153 84.5176 89.6654 84.4629 89.123V88.4941C84.5176 87.9473 84.6156 87.4574 84.7568 87.0244C84.9027 86.5915 85.0964 86.2223 85.3379 85.917C85.5794 85.6071 85.8757 85.3724 86.2266 85.2129C86.5775 85.0488 86.9854 84.9668 87.4502 84.9668C87.915 84.9668 88.3275 85.0579 88.6875 85.2402C89.0475 85.418 89.3483 85.6732 89.5898 86.0059C89.8359 86.3385 90.0228 86.7373 90.1504 87.2021C90.278 87.6624 90.3418 88.1751 90.3418 88.7402ZM89.0703 88.8838V88.7402C89.0703 88.3711 89.0361 88.0247 88.9678 87.7012C88.8994 87.373 88.79 87.0859 88.6396 86.8398C88.4893 86.5892 88.291 86.3932 88.0449 86.252C87.7988 86.1061 87.4958 86.0332 87.1357 86.0332C86.8167 86.0332 86.5387 86.0879 86.3018 86.1973C86.0693 86.3066 85.8711 86.4548 85.707 86.6416C85.543 86.8239 85.4085 87.0335 85.3037 87.2705C85.2035 87.5029 85.1283 87.7445 85.0781 87.9951V89.6426C85.151 89.9616 85.2695 90.2692 85.4336 90.5654C85.6022 90.8571 85.8255 91.0964 86.1035 91.2832C86.3861 91.4701 86.7347 91.5635 87.1494 91.5635C87.4912 91.5635 87.7829 91.4951 88.0244 91.3584C88.2705 91.2171 88.4688 91.0234 88.6191 90.7773C88.7741 90.5312 88.888 90.2464 88.9609 89.9229C89.0339 89.5993 89.0703 89.2529 89.0703 88.8838ZM96.3027 91.2354V87.4277C96.3027 87.1361 96.2435 86.8831 96.125 86.6689C96.0111 86.4502 95.8379 86.2816 95.6055 86.1631C95.373 86.0446 95.0859 85.9854 94.7441 85.9854C94.4251 85.9854 94.1449 86.04 93.9033 86.1494C93.6663 86.2588 93.4795 86.4023 93.3428 86.5801C93.2106 86.7578 93.1445 86.9492 93.1445 87.1543H91.8799C91.8799 86.89 91.9482 86.6279 92.085 86.3682C92.2217 86.1084 92.4176 85.8737 92.6729 85.6641C92.9326 85.4499 93.2425 85.2812 93.6025 85.1582C93.9671 85.0306 94.3727 84.9668 94.8193 84.9668C95.3571 84.9668 95.8311 85.0579 96.2412 85.2402C96.6559 85.4225 96.9795 85.6982 97.2119 86.0674C97.4489 86.432 97.5674 86.89 97.5674 87.4414V90.8867C97.5674 91.1328 97.5879 91.3949 97.6289 91.6729C97.6745 91.9508 97.7406 92.1901 97.8271 92.3906V92.5H96.5078C96.444 92.3542 96.3939 92.1605 96.3574 91.9189C96.321 91.6729 96.3027 91.445 96.3027 91.2354ZM96.5215 88.0156L96.5352 88.9043H95.2568C94.8968 88.9043 94.5755 88.9339 94.293 88.9932C94.0104 89.0479 93.7734 89.1322 93.582 89.2461C93.3906 89.36 93.2448 89.5036 93.1445 89.6768C93.0443 89.8454 92.9941 90.0436 92.9941 90.2715C92.9941 90.5039 93.0465 90.7158 93.1514 90.9072C93.2562 91.0986 93.4134 91.2513 93.623 91.3652C93.8372 91.4746 94.0993 91.5293 94.4092 91.5293C94.7965 91.5293 95.1383 91.4473 95.4346 91.2832C95.7308 91.1191 95.9655 90.9186 96.1387 90.6816C96.3164 90.4447 96.4121 90.2145 96.4258 89.9912L96.9658 90.5996C96.9339 90.791 96.8473 91.0029 96.7061 91.2354C96.5648 91.4678 96.3757 91.6911 96.1387 91.9053C95.9062 92.1149 95.6283 92.2904 95.3047 92.4316C94.9857 92.5684 94.6257 92.6367 94.2246 92.6367C93.7233 92.6367 93.2835 92.5387 92.9053 92.3428C92.5316 92.1468 92.2399 91.8848 92.0303 91.5566C91.8252 91.224 91.7227 90.8525 91.7227 90.4424C91.7227 90.0459 91.8001 89.6973 91.9551 89.3965C92.11 89.0911 92.3333 88.8382 92.625 88.6377C92.9167 88.4326 93.2676 88.2777 93.6777 88.1729C94.0879 88.068 94.5459 88.0156 95.0518 88.0156H96.5215ZM103.863 90.5381C103.863 90.3558 103.822 90.1872 103.74 90.0322C103.663 89.8727 103.501 89.7292 103.255 89.6016C103.013 89.4694 102.649 89.3555 102.161 89.2598C101.751 89.1732 101.38 89.0706 101.047 88.9521C100.719 88.8337 100.438 88.6901 100.206 88.5215C99.9782 88.3529 99.8027 88.1546 99.6797 87.9268C99.5566 87.6989 99.4951 87.4323 99.4951 87.127C99.4951 86.8353 99.5589 86.5596 99.6865 86.2998C99.8187 86.04 100.003 85.8099 100.24 85.6094C100.482 85.4089 100.771 85.2516 101.108 85.1377C101.446 85.0238 101.822 84.9668 102.236 84.9668C102.829 84.9668 103.335 85.0716 103.754 85.2812C104.173 85.4909 104.494 85.7712 104.718 86.1221C104.941 86.4684 105.053 86.8535 105.053 87.2773H103.788C103.788 87.0723 103.727 86.874 103.604 86.6826C103.485 86.4867 103.31 86.3249 103.077 86.1973C102.849 86.0697 102.569 86.0059 102.236 86.0059C101.885 86.0059 101.601 86.0605 101.382 86.1699C101.168 86.2747 101.01 86.4092 100.91 86.5732C100.814 86.7373 100.767 86.9105 100.767 87.0928C100.767 87.2295 100.789 87.3525 100.835 87.4619C100.885 87.5667 100.972 87.6647 101.095 87.7559C101.218 87.8424 101.391 87.9245 101.614 88.002C101.838 88.0794 102.122 88.1569 102.469 88.2344C103.075 88.3711 103.574 88.5352 103.966 88.7266C104.358 88.918 104.649 89.1527 104.841 89.4307C105.032 89.7087 105.128 90.0459 105.128 90.4424C105.128 90.766 105.06 91.0622 104.923 91.3311C104.791 91.5999 104.597 91.8324 104.342 92.0283C104.091 92.2197 103.79 92.3701 103.439 92.4795C103.093 92.5843 102.703 92.6367 102.271 92.6367C101.619 92.6367 101.067 92.5205 100.616 92.2881C100.165 92.0557 99.8232 91.7549 99.5908 91.3857C99.3584 91.0166 99.2422 90.627 99.2422 90.2168H100.514C100.532 90.5632 100.632 90.8389 100.814 91.0439C100.997 91.2445 101.22 91.388 101.484 91.4746C101.749 91.5566 102.011 91.5977 102.271 91.5977C102.617 91.5977 102.906 91.5521 103.139 91.4609C103.376 91.3698 103.556 91.2445 103.679 91.085C103.802 90.9255 103.863 90.7432 103.863 90.5381ZM109.858 92.6367C109.343 92.6367 108.876 92.5501 108.457 92.377C108.042 92.1992 107.685 91.9508 107.384 91.6318C107.088 91.3128 106.86 90.9346 106.7 90.4971C106.541 90.0596 106.461 89.5811 106.461 89.0615V88.7744C106.461 88.1729 106.55 87.6374 106.728 87.168C106.905 86.694 107.147 86.293 107.452 85.9648C107.757 85.6367 108.104 85.3883 108.491 85.2197C108.879 85.0511 109.28 84.9668 109.694 84.9668C110.223 84.9668 110.679 85.0579 111.062 85.2402C111.449 85.4225 111.766 85.6777 112.012 86.0059C112.258 86.3294 112.44 86.7122 112.559 87.1543C112.677 87.5918 112.736 88.0703 112.736 88.5898V89.1572H107.213V88.125H111.472V88.0293C111.453 87.7012 111.385 87.3822 111.267 87.0723C111.153 86.7624 110.97 86.5072 110.72 86.3066C110.469 86.1061 110.127 86.0059 109.694 86.0059C109.407 86.0059 109.143 86.0674 108.901 86.1904C108.66 86.3089 108.452 86.4867 108.279 86.7236C108.106 86.9606 107.972 87.25 107.876 87.5918C107.78 87.9336 107.732 88.3278 107.732 88.7744V89.0615C107.732 89.4124 107.78 89.7428 107.876 90.0527C107.976 90.3581 108.12 90.627 108.307 90.8594C108.498 91.0918 108.728 91.2741 108.997 91.4062C109.271 91.5384 109.58 91.6045 109.927 91.6045C110.373 91.6045 110.752 91.5133 111.062 91.3311C111.371 91.1488 111.643 90.9049 111.875 90.5996L112.641 91.208C112.481 91.4495 112.278 91.6797 112.032 91.8984C111.786 92.1172 111.483 92.2949 111.123 92.4316C110.768 92.5684 110.346 92.6367 109.858 92.6367Z"
                      fill={
                        type === "knowledge"
                          ? defaultBrandName.theme_color
                          : "#8C8C8C"
                      }
                    />
                  </svg>
                </CustomItem>
              )}
            </Radio.Group>
          </Form.Item>
          {type === "details" && type !== "knowledge" && (
            <Form.Item
              name="view"
              label="View"
              rules={[{ required: true, message: "Please choose!" }]}
            >
              <Radio.Group defaultValue="default">
                <Radio value="default">Default</Radio>
                <Radio value="table">Table</Radio>
              </Radio.Group>
            </Form.Item>
          )}
          {type !== "tab" ? (
            type !== "knowledge" &&
            type !== "multi_detail" && (
              <>
                <Form.Item
                  name="related_object"
                  label="Related object"
                  rules={[
                    {
                      required: true,
                      message: "Please select related object!",
                    },
                  ]}
                >
                  <Select
                    disabled={
                      Object.keys(dataItem).length > 0 && componentID
                        ? true
                        : false
                    }
                    onChange={(e) => {
                      dispatch(
                        loadListObjectField({
                          api_version: "2",
                          object_id: e,
                          show_meta_fields: true,
                        })
                      );
                      form.setFieldsValue({
                        column: [],
                      });
                      setAllCondition([]);
                      setAnyCondition([]);
                      setColumns([]);
                      $timeRangeFilter({
                        filter_field_id: "modify_time",
                        field_type: "datetime-local",
                        filter_type: "all-time",
                        amount_of_time: null,
                        start_time: null,
                        end_time: null,
                      });
                    }}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {type === "details" || type === "multi_detail"
                      ? objects_related.map((item, idx) => {
                          return <Option value={item._id}>{item.Name}</Option>;
                        })
                      : type === "table" || type === "tags"
                      ? targetObjects.map((item, idx) => {
                          return <Option value={item._id}>{item.Name}</Option>;
                        })
                      : ""}
                    {(type === "details" || type === "multi_detail") &&
                    objects_related.findIndex(
                      (ele) => ele._id === selectedObject
                    ) === -1 ? (
                      <Option value={selectedObject}>{objectName}</Option>
                    ) : (
                      ""
                    )}
                  </Select>
                </Form.Item>

                <Form.Item
                  name="column"
                  label="Show fields"
                  rules={[
                    { required: true, message: "Please select show fields!" },
                  ]}
                >
                  <Select
                    mode="multiple"
                    onChange={(e, values) => {
                      let newArr = [];
                      /*eslint-disable-next-line*/
                      values.map((item, idx) => {
                        newArr.push(item.field);
                      });
                      setColumns(newArr);
                    }}
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {allFields &&
                      allFields.sections &&
                      allFields.sections.map((item, idx) => {
                        return (
                          <OptGroup label={item.section_name}>
                            {item.fields.map((field, index) => {
                              if (field.hidden === false)
                                return (
                                  <Option value={field.ID} field={field}>
                                    {field.name}
                                  </Option>
                                );
                              else return null;
                            })}
                          </OptGroup>
                        );
                      })}
                  </Select>
                </Form.Item>

                <Columns
                  columns={columns}
                  setColumns={setColumns}
                  form={form}
                />
              </>
            )
          ) : (
            <>
              <Form.Item
                label="Number of tabs"
                name="numberOfTab"
                rules={[
                  {
                    required: true,
                    message: "Please input number of tab!",
                  },
                  {
                    type: "number",
                    max: 10,
                    min: 1,
                  },
                ]}
              >
                <InputNumber
                  disabled={Object.keys(dataItem).length > 0 && componentID}
                  onChange={(e) => {
                    let tmp = [];
                    for (let i = 1; i <= e; i++) {
                      tmp.push(`name${i}`);
                    }

                    const tempName = form.getFieldValue("name");
                    form.resetFields();
                    form.setFieldsValue({
                      // ...form.getFieldsValue(),
                      name: tempName,
                      numberOfTab: e,
                      type: "tab",
                    });
                    setNumberTab(tmp);
                  }}
                  min={1}
                  style={{ width: "100%" }}
                  max={10}
                />
              </Form.Item>

              {numberTab.length === 0 && (
                <CustomAdd
                  onClick={() => {
                    let temp = [...numberTab];
                    temp.push(`name${numberTab.length}`);
                    setNumberTab(temp);
                    form.setFieldsValue({
                      numberOfTab: temp.length,
                    });
                  }}
                >
                  <span>+ Add tab</span>
                </CustomAdd>
              )}
              <div
                style={{
                  display: `${
                    form.getFieldValue("numberOfTab") > 0 ? "" : "none"
                  }`,
                }}
                className="tab-details"
              >
                <div className="tab-details-title">Tabs detail</div>
                <div style={{ marginTop: "10px" }}>
                  {numberTab.map((item, idx) => {
                    return (
                      <ItemWrap key={idx}>
                        <Form.Item
                          label={`Name of tab ${idx + 1}`}
                          name={item}
                          rules={[
                            {
                              required: true,
                              message: "Please input name of tab!",
                            },
                            () => ({
                              validator: (_, value) => {
                                let temp = false;
                                if (value) {
                                  numberTab.forEach((v) => {
                                    if (
                                      form.getFieldValue(v) === value &&
                                      v !== item
                                    ) {
                                      temp = true;
                                    }
                                  });
                                }
                                if (temp === true) {
                                  return Promise.reject(
                                    new Error(
                                      "Tên tab không được trùng. Vui lòng nhập lại!"
                                    )
                                  );
                                } else {
                                  return Promise.resolve();
                                }
                              },
                            }),
                          ]}
                        >
                          <Input
                          // value={form.getFieldValue(`${item}`)}
                          />
                        </Form.Item>
                        <img
                          src={DeleteImg}
                          alt="delete"
                          onClick={() => {
                            deleteNumberTab(item);
                          }}
                        />
                      </ItemWrap>
                    );
                  })}
                </div>
                {Object.keys(dataItem).length > 0 && componentID && (
                  <CustomAdd
                    onClick={() => {
                      let temp = [...numberTab];
                      temp.push(`name${Math.random()}`);
                      setNumberTab(temp);
                      form.setFieldsValue({
                        numberOfTab: temp.length,
                      });
                    }}
                  >
                    <span>+ Add tab</span>
                  </CustomAdd>
                )}
              </div>
            </>
          )}

          {type === "multi_detail" ? (
            <>
              <Form.Item
                name="related_object"
                label="Related object"
                rules={[
                  {
                    required: true,
                    message: "Please select related object!",
                  },
                ]}
              >
                <Select
                  disabled={
                    Object.keys(dataItem).length > 0 && componentID
                      ? true
                      : false
                  }
                  onChange={(e) => {
                    dispatch(
                      loadListObjectField({
                        api_version: "2",
                        object_id: e,
                        show_meta_fields: true,
                      })
                    );
                    form.setFieldsValue({
                      column: [],
                    });
                    setAllCondition([]);
                    setAnyCondition([]);
                    setColumns([]);
                  }}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {type === "details" || type === "multi_detail"
                    ? objects_related.map((item, idx) => {
                        return <Option value={item._id}>{item.Name}</Option>;
                      })
                    : type === "table" || type === "tags"
                    ? targetObjects.map((item, idx) => {
                        return <Option value={item._id}>{item.Name}</Option>;
                      })
                    : ""}
                  {(type === "details" || type === "multi_detail") &&
                  objects_related.findIndex(
                    (ele) => ele._id === selectedObject
                  ) === -1 ? (
                    <Option value={selectedObject}>{objectName}</Option>
                  ) : (
                    ""
                  )}
                </Select>
              </Form.Item>
              <Form.Item
                label="Number of columns"
                name="number_of_cols"
                rules={[
                  {
                    required: true,
                    message: "Please input number of columns!",
                  },
                  {
                    type: "number",
                    max: 5,
                    min: 1,
                  },
                ]}
              >
                <InputNumber
                  // disabled={Object.keys(dataItem).length > 0 && componentID}
                  onChange={(e) => {
                    if (e <= 5) {
                      setNumberCols(e);
                      let arr = [];
                      for (let i = 0; i < e; i++) {
                        arr.push([]);
                      }
                      setShowFields(arr);
                    }
                  }}
                  min={1}
                  style={{ width: "100%" }}
                  max={5}
                  value={showFields.length}
                />
              </Form.Item>

              {numberCols === 0 && (
                <CustomAdd
                  onClick={() => {
                    setNumberCols(1);
                    setShowFields([[]]);
                    form.setFieldValue("number_of_cols", 1);
                  }}
                >
                  <span>+ Add column</span>
                </CustomAdd>
              )}
              <div
                style={{
                  display: `${numberCols > 0 ? "" : "none"}`,
                }}
                className="tab-details"
              >
                <div className="tab-details-title">Columns detail</div>
                <div style={{ marginTop: "10px" }}>
                  {showFields.map((item, idx) => {
                    return (
                      <ItemWrap key={idx}>
                        <Form.Item
                          label={`Column ${idx + 1}`}
                          // name={item}
                          rules={[
                            {
                              required: true,
                              message: "Please choose fields!",
                            },
                          ]}
                        >
                          <Select
                            mode="multiple"
                            onChange={(e) => {
                              let newArr = [...showFields];

                              newArr[idx] = [...e];
                              setShowFields(newArr);
                            }}
                            showSearch
                            value={item}
                            optionFilterProp="children"
                            filterOption={(inputValue, option) => {
                              if (option.children) {
                                return option.children
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              } else if (option.label) {
                                return option.label
                                  .toLowerCase()
                                  .indexOf(inputValue.toLowerCase()) >= 0
                                  ? true
                                  : false;
                              }
                            }}
                          >
                            {allFields &&
                              allFields.sections &&
                              allFields.sections.map((item, idx) => {
                                return (
                                  <OptGroup label={item.section_name}>
                                    {item.fields.map((field, index) => {
                                      if (field.hidden === false)
                                        return (
                                          <Option
                                            value={field.ID}
                                            field={field}
                                          >
                                            {field.name}
                                          </Option>
                                        );
                                      else return null;
                                    })}
                                  </OptGroup>
                                );
                              })}
                          </Select>
                        </Form.Item>
                        <img
                          src={DeleteImg}
                          alt="delete"
                          onClick={() => {
                            deleteNumberCols(item, idx);
                          }}
                        />
                      </ItemWrap>
                    );
                  })}
                </div>
                {Object.keys(dataItem).length > 0 &&
                  componentID &&
                  showFields.length < 5 && (
                    <CustomAdd
                      onClick={() => {
                        setNumberCols(numberCols + 1);
                        let arr = [...showFields];
                        arr.push([]);
                        setShowFields(arr);
                        form.setFieldValue("number_of_cols", numberCols + 1);
                      }}
                    >
                      <span>+ Add column</span>
                    </CustomAdd>
                  )}
              </div>
            </>
          ) : (
            ""
          )}
          {(type === "table" || type === "tags") && (
            <TimeRangeFilter
              timeRangeFilter={timeRangeFilter}
              $timeRangeFilter={$timeRangeFilter}
              listObjectField={listObjectField}
            />
          )}
          {type !== "details" &&
          type !== "tab" &&
          type !== "knowledge" &&
          type !== "multi_detail" &&
          form.getFieldValue("related_object") ? (
            <>
              <Form.Item
                label="With details"
                name="with_details"
                valuePropName="checked"
              >
                <Switch checkedChildren="Yes" unCheckedChildren="No" />
              </Form.Item>
              <Conditions
                title={"AND condition"}
                decs={"(All conditions must be met)"}
                conditions={allCondition}
                setConditions={setAllCondition}
                ID={componentID}
                dataDetails={dataItem}
                operatorValue={operatorValueAnd}
                setOperatorValue={setOperatorValueAnd}
                value={valueAnd}
                setValue={setValueAnd}
              />
              <Conditions
                title={"OR condition"}
                decs={"(Any conditions must be met)"}
                conditions={anyCondition}
                setConditions={setAnyCondition}
                ID={componentID}
                dataDetails={dataItem}
                operatorValue={operatorValueOr}
                setOperatorValue={setOperatorValueOr}
                value={valueOr}
                setValue={setValueOr}
              />
            </>
          ) : (
            ""
          )}
          {type === "knowledge" && (
            <>
              <Form.Item
                label="Object article"
                name="obj_article"
                rules={[
                  { required: true, message: "Please select object articel!" },
                ]}
              >
                <Select
                  onChange={(e, values) => {}}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {listObjectArticle.map((article, index) => {
                    return (
                      <Option key={index} value={article.value}>
                        {article.label}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                label="Search fields"
                name="search_fields"
                rules={[
                  { required: true, message: "Please input search field!" },
                ]}
              >
                <Select
                  mode="multiple"
                  onChange={(e, values) => {}}
                  showSearch
                  optionFilterProp="children"
                  filterOption={(inputValue, option) => {
                    if (option.children) {
                      return option.children
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    } else if (option.label) {
                      return option.label
                        .toLowerCase()
                        .indexOf(inputValue.toLowerCase()) >= 0
                        ? true
                        : false;
                    }
                  }}
                >
                  {fields &&
                    fields.sections &&
                    fields.sections.map((item, idx) => {
                      return (
                        <OptGroup label={item.section_name}>
                          {item.fields.map((field, index) => {
                            if (field.hidden === false)
                              return (
                                <Option value={field.ID} field={field}>
                                  {field.name}
                                </Option>
                              );
                            else return null;
                          })}
                        </OptGroup>
                      );
                    })}
                </Select>
              </Form.Item>
            </>
          )}
          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              isLoading={isLoadingCreate}
            >
              Save
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

export default ModalComponent;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
  position: sticky;
  bottom: 0;
  background-color: white;
  padding-top: 10px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .content {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    /* or 21px */

    display: flex;
    align-items: center;
    text-align: center;

    /* Neutral / 8 */

    color: #595959;
    padding: 0 130px 0 130px;
  }
  .tab-details {
    border: 1px solid #d9d9d9;
    border-radius: 5px;
    padding: 16px;
    padding-bottom: 0;
    .tab-details-title {
      font-style: normal;
      font-family: var(--roboto-700);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;

      /* Character/Color text main */
      margin-top: -28px;
      color: #2c2c2c;
      background-color: #fff;
      width: fit-content;
      padding-left: 10px;
      padding-right: 10px;
    }
  }
  .ant-radio-button-wrapper {
    border: none !important;
  }
  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-button-wrapper:not(:first-child)::before {
    display: none;
  }

  .ant-radio-button-wrapper-disabled {
    background-color: transparent !important;
  }
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-input:focus + .ant-radio-inner {
    box-shadow: none;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;
const CustomItem = styled(Radio.Button)``;

const ItemWrap = styled.div`
  width: 100%;
  display: flex;

  .ant-form-item {
    width: 100%;
  }
  img {
    width: 20px;
    height: 20px;
    margin-left: 8px;
    margin-top: 6px;
    cursor: pointer;
  }
`;

const CustomAdd = styled.div`
  margin-bottom: 10px;
  margin-top: -10px;

  span {
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;
