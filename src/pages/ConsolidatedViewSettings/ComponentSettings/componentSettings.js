import { Button } from "antd";
import deleteImg from "assets/icons/common/delete.svg";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import detailsImg from "assets/images/consolidatedView/detail.png";
import noComponentImg from "assets/images/consolidatedView/noComponent.png";
import tableImg from "assets/images/consolidatedView/table.png";
import tabsImg from "assets/images/consolidatedView/tabs.png";
import tagsImg from "assets/images/consolidatedView/tags.png";
import knowledgeBaseImg from "assets/images/consolidatedView/KnowledgeBase.png";

import ModalDelete from "components/Modal/ModalConfirmDelete";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteComponent,
  loadComponents,
} from "redux/slices/consolidatedViewSettings";
import { loadRelatedObject } from "redux/slices/fieldsManagement";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import { loadSourceObjects, loadTargetObjects } from "redux/slices/objects";
import styled from "styled-components";
import ModalComponent from "./modalComponent";

const ComponentSettings = ({ selectedObject, objectName, checkRule }) => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [componentID, setComponentID] = useState("");
  const [dataItem, setDataItem] = useState({});
  const [componentName, setComponentName] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(
      loadRelatedObject({
        object_id: selectedObject,
      })
    );
    dispatch(
      loadSourceObjects({
        object_id: selectedObject,
      })
    );
    dispatch(
      loadTargetObjects({
        object_id: selectedObject,
      })
    );
    dispatch(
      loadComponents({
        object_id: selectedObject,
      })
    );
    /*eslint-disable-next-line*/
  }, [selectedObject]);

  const { loadingComponents } = useSelector((state) => state.objectsReducer);

  const { components, isLoadingDelete } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  useEffect(() => {
    dispatch(setShowModalConfirmDelete(isLoadingDelete));

    dispatch(setShowLoadingScreen(isLoadingDelete));
    /*eslint-disable-next-line*/
  }, [isLoadingDelete]);
  const handleTypeImg = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
      case "multi_detail":
        return detailsImg;
      case "table":
      case "TABLE":
        return tableImg;
      case "tags":
        return tagsImg;
      case "tab":
        return tabsImg;
      case "knowledge":
        return knowledgeBaseImg;
      default:
        break;
    }
  };

  const handleTypeTitle = (type) => {
    switch (type) {
      case "details":
      case "DETAILS":
        return "Details";
      case "table":
      case "TABLE":
        return "Table";
      case "tags":
        return "Tags";
      case "tab":
        return "Tab";
      case "knowledge":
        return "Knowledge base enhancement";
      default:
        break;
    }
  };

  useEffect(() => {
    dispatch(setShowLoadingScreen(loadingComponents));
    /*eslint-disable-next-line*/
  }, [loadingComponents]);

  return (
    <Wrapper>
      {components.length === 0 ? (
        <div className="no-data">
          <img alt="" src={noComponentImg} className="noDataImg" />
          <div className="title">
            There's no any <span>component setting</span>
          </div>
          {checkRule("create") ? (
            <CustomButton size="large" onClick={() => setIsModalVisible(true)}>
              <img alt="" src={plusIcon} />
              Add new
            </CustomButton>
          ) : (
            ""
          )}
        </div>
      ) : (
        <div>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {checkRule("create") ? (
              <CustomButton
                size="large"
                onClick={() => setIsModalVisible(true)}
              >
                <img alt="" src={plusIcon} />
                Add new
              </CustomButton>
            ) : (
              ""
            )}
          </div>
          <div style={{ height: "600px", overflow: "auto" }}>
            {components.map((item, idx) => {
              return (
                <div
                  className="component-item"
                  onDoubleClick={() => {
                    if (checkRule("edit")) {
                      if (item.component_type) {
                        //
                      } else {
                        setComponentID(item._id);
                        setDataItem(item);
                        setIsModalVisible(true);
                      }
                    }
                  }}
                >
                  <div style={{ display: "flex" }}>
                    {" "}
                    <img
                      alt=""
                      src={handleTypeImg(item.type || item.component_type)}
                    />
                    <div className="info">
                      <div className="title">{item.name}</div>
                      <div className="decs">
                        {handleTypeTitle(item.type || item.component_type)}
                        {item.type === "knowledge"
                          ? ""
                          : item.component_type
                          ? ""
                          : "• Related object: "}
                        {item.related_object_name}
                      </div>
                    </div>
                  </div>
                  {checkRule("delete") &&
                  !item.component_type &&
                  item._id !== "campaign_task_0000000001" &&
                  item._id !== "campaign_member_00000001" ? (
                    <img
                      alt=""
                      src={deleteImg}
                      className="delete-img"
                      onClick={() => {
                        setComponentID(item._id);
                        setComponentName(item.name);
                        dispatch(setShowModalConfirmDelete(true));
                      }}
                    />
                  ) : (
                    ""
                  )}
                </div>
              );
            })}
          </div>
        </div>
      )}

      <ModalComponent
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        selectedObject={selectedObject}
        dataItem={dataItem}
        componentID={componentID}
        setComponentID={setComponentID}
        setDataItem={setDataItem}
        objectName={objectName}
        components={components}
      />

      <ModalDelete
        title={`${componentName} này`}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deleteComponent}
        dataDelete={{
          id: componentID,
          object_id: selectedObject,
        }}
        setShowModalDelete={() => {}}
      />
    </Wrapper>
  );
};

export default ComponentSettings;

const Wrapper = styled.div`
  width: 100%;
  padding-right: 24px;
  padding-left: 8px;
  .no-data {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    .noDataImg {
      width: 100px;
    }
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      span {
        color: ${(props) => props.theme.darker};
      }
    }
  }
  .component-item {
    width: 100%;
    background: #ffffff;
    /* vien xam */

    border: 1px solid #ececec;
    box-sizing: border-box;
    border-radius: 10px;
    display: flex;
    margin-bottom: 16px;
    justify-content: space-between;
    img {
      width: 40px;
      margin-right: 16px;
    }
    padding: 16px;
    .title {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* or 137% */

      display: flex;
      align-items: center;

      /* Character/Color text main */

      color: #2c2c2c;
    }
    .decs {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      display: flex;
      align-items: center;
      font-feature-settings: "tnum" on, "lnum" on;

      /* text xám */

      color: #6b6b6b;
    }

    :hover {
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
      .delete-img {
        visibility: visible;
        opacity: 1;
      }
      cursor: pointer;
      transition: all 0.4s ease-in-out;
    }
    .delete-img {
      width: 22px;
      margin-right: 0;
      visibility: hidden;
      opacity: 0;
      transition: visibility 0s, opacity 0.5s linear;
    }
  }
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.darker};
  color: #fff !important;
  font-size: 16px;
  margin-top: 8px;
  margin-bottom: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker}!important;
    color: #fff !important;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
  &:active {
    background-color: ${(props) => props.theme.darker};
    border: 1px solid ${(props) => props.theme.darker};
    span {
      color: ${(props) => props.theme.white};
    }
  }

  &:focus {
    background-color: ${(props) => props.theme.darker};
    border: 1px solid ${(props) => props.theme.darker};
    span {
      color: ${(props) => props.theme.white};
    }
  }
`;
