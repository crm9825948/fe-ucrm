import Example from "./example";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

const Columns = (props) => {
  const { columns, setColumns, form } = props;
  return (
    <DndProvider backend={HTML5Backend}>
      <Example cards={columns} setCards={setColumns} form={form} />
    </DndProvider>
  );
};

export default Columns;
