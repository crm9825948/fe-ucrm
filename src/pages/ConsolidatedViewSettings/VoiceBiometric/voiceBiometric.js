import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import styled from "styled-components/macro";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import Button from "antd/lib/button";
import Radio from "antd/lib/radio";
import Tooltip from "antd/lib/tooltip";

import DeleteIcon from "assets/icons/users/delete-user.svg";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import { loadListObjectField } from "redux/slices/objects";
import {
  getConfigVoiceBiometric,
  updateConfigVoiceBiometric,
  deleteConfigVoiceBiometric,
} from "redux/slices/consolidatedViewSettings";

import { setShowModalConfirmDelete } from "redux/slices/global";
import SelectObject from "components/ExternalService/selectObject";

// import setting from "pages/setting";
// import { openSuccess, openError } from "components/Notification";

function VoiceBiometric() {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { configVoiceBiometric } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const [typeAuthen, setTypeAuthen] = useState("");
  const [payload, setPayload] = useState([]);
  const [listFields, setListFields] = useState([]);
  const [dataDelete, setDataDelete] = useState({});

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    // {
    //   label: 'Token',
    //   value: 'token'
    // }
  ];

  const Method = [
    // {
    //   label: 'GET',
    //   value: 'GET'
    // }
    {
      label: "POST",
      value: "POST",
    },
  ];

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "voice_biometric" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSubmit = (values) => {
    dispatch(
      updateConfigVoiceBiometric({
        customer_name_field: values.customer_name_field,
        username: values.username,
        password: _.get(values, "password", undefined),
        last_choice_field: values.last_choice_field,
        object_id: selectedObject,
        cif_field: values.cif_field,
        method: "POST",
        auth_type: typeAuthen,
        api_url: values.api_url,
        payload: payload,
      })
    );
  };

  const _onDeleteSetting = () => {
    setDataDelete({
      object_id: selectedObject,
    });
    dispatch(setShowModalConfirmDelete(true));
  };

  useEffect(() => {
    if (Object.keys(configVoiceBiometric).length > 0) {
      form.setFieldsValue({
        customer_name_field: configVoiceBiometric.customer_name_field,
        username: configVoiceBiometric.username,
        // password: configVoiceBiometric.password,
        last_choice_field: configVoiceBiometric.last_choice_field,
        cif_field: configVoiceBiometric.cif_field,
        method: configVoiceBiometric.method,
        auth_type: configVoiceBiometric.auth_type,
        api_url: configVoiceBiometric.api_url,
        webhook_url: configVoiceBiometric.webhook_url,
      });
      setTypeAuthen(configVoiceBiometric.auth_type);
      setPayload(configVoiceBiometric.payload);
    } else {
      form.setFieldsValue({
        customer_name_field: undefined,
        username: undefined,
        password: undefined,
        last_choice_field: undefined,
        cif_field: undefined,
        auth_type: undefined,
        api_url: undefined,
        webhook_url: undefined,
      });
      setTypeAuthen("");
      setPayload([]);
    }
  }, [configVoiceBiometric, form]);

  const handleSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const _onAddPayload = () => {
    setPayload([
      ...payload,
      {
        field_id: undefined,
        key: undefined,
        format_date: false,
      },
    ]);
  };

  const handleChangePayload = (value, type, index) => {
    let tempPayload = [...payload];
    switch (type) {
      case "field":
        tempPayload[index] = {
          ...tempPayload[index],
          field_id: value,
        };
        break;
      case "key":
        tempPayload[index] = {
          ...tempPayload[index],
          key: value,
        };
        break;
      case "format":
        tempPayload[index] = {
          ...tempPayload[index],
          format_date: value,
        };
        break;

      default:
        break;
    }
    setPayload(tempPayload);
  };

  const _onDeletePayload = (idx) => {
    let temp = [...payload];
    temp.splice(idx, 1);
    setPayload(temp);
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);

    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
      })
    );

    dispatch(
      getConfigVoiceBiometric({
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempOptionsFields = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                !_.get(field, "encrypted", false)
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  type: field.type,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField]);

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="Voice biometric"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <Wrap>
          <Form
            form={form}
            onFinish={_onSubmit}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            labelAlign="left"
          >
            <BasicInfo>
              <legend>General</legend>

              {Object.keys(configVoiceBiometric).length > 0 && (
                <Form.Item label="Webhook url" name="webhook_url">
                  <Input disabled />
                </Form.Item>
              )}
              <Form.Item
                label="Customer name field"
                name="customer_name_field"
                rules={[
                  {
                    required: true,
                    message: "Please select!",
                  },
                ]}
              >
                <Select
                  placeholder="Select Customer name field"
                  options={listFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item
                label="Authentication type"
                name="auth_type"
                rules={[
                  {
                    required: true,
                    message: "Please select!",
                  },
                ]}
              >
                <Select
                  placeholder="Select authentication type"
                  options={AuthenticationType}
                  onChange={(e) => handleSelectAuthenType(e)}
                ></Select>
              </Form.Item>
              {typeAuthen === "basic" && (
                <>
                  <Form.Item
                    label="User name"
                    name="username"
                    rules={[{ required: true, message: "Please input!" }]}
                  >
                    <Input placeholder="Please input" />
                  </Form.Item>

                  <Form.Item label="Password" name="password">
                    <Input.Password
                      placeholder="Please input"
                      autocomplete="new-password"
                    />
                  </Form.Item>
                </>
              )}
              <Form.Item
                label="Last choice field"
                name="last_choice_field"
                rules={[{ required: true, message: "Please select!" }]}
              >
                <Select
                  placeholder="Select Last choice field"
                  options={listFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item
                label="Cif field"
                name="cif_field"
                rules={[{ required: true, message: "Please select!" }]}
              >
                <Select
                  placeholder="Select Last choice field"
                  options={listFields}
                  optionFilterProp="label"
                  showSearch
                />
              </Form.Item>
              <Form.Item
                label="API URL"
                name="api_url"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <Input placeholder="Please input" />
              </Form.Item>
              <Form.Item label="Method" name="method">
                <Radio.Group
                  defaultValue="POST"
                  optionType="button"
                  options={Method}
                ></Radio.Group>
              </Form.Item>
            </BasicInfo>

            <Payload>
              <legend>Payload</legend>

              {payload.length > 0 &&
                payload.map((item, idx) => {
                  return (
                    <FormPayload key={idx} wrapperCol={{ span: 24 }}>
                      <Form.Item
                        label="Field ID"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        rules={[{ required: true, message: "Please input!" }]}
                      >
                        <Select
                          value={item.field_id}
                          placeholder="Please select"
                          options={listFields}
                          onChange={(e) => handleChangePayload(e, "field", idx)}
                          optionFilterProp="label"
                          showSearch
                        />
                      </Form.Item>
                      <Form.Item
                        label="Key"
                        labelCol={{ span: 8 }}
                        wrapperCol={{ span: 16 }}
                        rules={[
                          {
                            required: true,
                            message: "Please input prefix!",
                          },
                        ]}
                      >
                        <Input
                          value={item.key}
                          placeholder="Please input"
                          onChange={(e) =>
                            handleChangePayload(e.target.value, "key", idx)
                          }
                        />
                      </Form.Item>

                      <Form.Item
                        label="Format Date"
                        labelCol={{ span: 16 }}
                        wrapperCol={{ span: 8 }}
                        labelAlign="right"
                      >
                        <Checkbox
                          checked={item.format_date}
                          onChange={(e) =>
                            handleChangePayload(e.target.checked, "format", idx)
                          }
                        />
                      </Form.Item>

                      <Delete>
                        <Tooltip title="Delete">
                          <img
                            src={DeleteIcon}
                            onClick={() => _onDeletePayload(idx)}
                            alt="delete"
                          />
                        </Tooltip>
                      </Delete>
                    </FormPayload>
                  );
                })}
              <AddNew onClick={() => _onAddPayload()}>
                <span>+ Add payload</span>
              </AddNew>
            </Payload>

            <WrapButton>
              <Form.Item>
                <Button
                  disabled={
                    (Object.keys(configVoiceBiometric).length > 0 &&
                      !checkRule("edit")) ||
                    (Object.keys(configVoiceBiometric).length === 0 &&
                      !checkRule("create"))
                  }
                  type="primary"
                  htmlType="submit"
                >
                  {Object.keys(configVoiceBiometric).length > 0
                    ? t("common.update")
                    : t("common.save")}
                </Button>
              </Form.Item>
              <Form.Item>
                <Button
                  disabled={!checkRule("delete")}
                  onClick={_onDeleteSetting}
                >
                  {t("common.delete")}
                </Button>
              </Form.Item>
            </WrapButton>
          </Form>
        </Wrap>
      )}

      <ModalConfirmDelete
        title={""}
        decs={"After deleting data will not be undone"}
        methodDelete={deleteConfigVoiceBiometric}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default VoiceBiometric;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-form {
    margin: 0 auto;
    width: 80%;

    @media screen and (min-width: 1600px) {
      width: 50%;
    }
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper {
    width: 73px;
    text-align: center;

    :hover {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const Wrap = styled.div`
  padding: 24px;
  background: #fff;
  border-radius: 10px;
  display: flex;
  height: fit-content;
`;

const BasicInfo = styled.fieldset`
  padding: 24px 16px 0 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-500);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }

  .ant-form-item {
    margin-bottom: 8px;
  }
`;

const Payload = styled(BasicInfo)``;

const AddNew = styled.div`
  margin-bottom: 8px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;

const FormPayload = styled(Form.Item)`
  width: 100%;

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
  }

  > .ant-col {
    max-width: 100% !important;
  }

  .ant-form-item {
    margin-bottom: 0;
    width: 30%;
    flex-wrap: nowrap;
  }

  .ant-form-item-label > label {
    margin-bottom: 0;
  }
`;

const Delete = styled.div`
  background: #ffffff;

  img {
    width: 22px;
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 24px;

  .ant-form-item {
    margin-bottom: 0;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;
    width: 160px;

    :hover {
      background: ${(props) => props.theme.darker}!important;

      span {
        color: #fff !important;
      }
    }

    :active {
      background: #fff;
      border: 1px solid #d9d9d9;

      span {
        color: #2c2c2c;
      }
    }

    :focus {
      background: #fff;
      border: 1px solid #d9d9d9;

      span {
        color: #2c2c2c;
      }
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};

    span {
      color: #fff;
    }

    :active {
      background: ${(props) => props.theme.main};
      border: 1px solid ${(props) => props.theme.main};

      span {
        color: #fff;
      }
    }

    :focus {
      background: ${(props) => props.theme.main};
      border: 1px solid ${(props) => props.theme.main};

      span {
        color: #fff;
      }
    }
  }
`;
