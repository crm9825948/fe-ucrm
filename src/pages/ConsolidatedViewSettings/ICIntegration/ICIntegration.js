import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Tabs from "antd/lib/tabs";

import { loadDataICIntegration } from "redux/slices/consolidatedViewSettings";

import MappingUser from "./mappingUser";
import AdvancedConfig from "./advancedConfig";
import MappingExposeAPI from "./mappingExposeAPI";
import SelectObject from "components/ExternalService/selectObject";

function ICIntegration() {
  const dispatch = useDispatch();
  const { TabPane } = Tabs;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "ic_integration" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);

    dispatch(
      loadDataICIntegration({
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="IC integration"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />
      {selectedObject && (
        <Tabs>
          <TabPane tab="Mapping user" key="mapping-user">
            <MappingUser
              selectedObject={selectedObject}
              checkRule={checkRule}
            />
          </TabPane>
          <TabPane tab="Advanced config" key="advanced-config">
            <AdvancedConfig
              listObjects={listObjects}
              selectedObject={selectedObject}
              checkRule={checkRule}
            />
          </TabPane>
          <TabPane tab="Mapping Expose API" key="mapping-expose">
            <MappingExposeAPI
              selectedObject={selectedObject}
              checkRule={checkRule}
            />
          </TabPane>
        </Tabs>
      )}
    </Wrapper>
  );
}

export default ICIntegration;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-tabs-nav {
    margin-bottom: 0;
  }

  .ant-tabs-nav-wrap {
    background: #fff;
    padding-left: 16px;
    padding-top: 16px;
    border-radius: 10px 10px 0 0;
    border-bottom: 1px solid rgba(0, 0, 0, 0.06);
  }

  .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
    color: ${(props) => props.theme.main} !important;
  }

  .ant-tabs-ink-bar {
    background: ${(props) => props.theme.main};
  }

  .ant-tabs-tab-btn:hover {
    color: ${(props) => props.theme.darker};
    background: #e6f7ff;
  }
`;
