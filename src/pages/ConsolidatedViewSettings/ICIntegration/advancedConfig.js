import { useState, useCallback, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";

import {
  loadFieldsConfig,
  getToken,
  createConfigIC,
  updateConfigIC,
  resetAllIC,
} from "redux/slices/consolidatedViewSettings";
import ModalConfirm from "components/Modal/ModalConfirm";
import ImgConfirm from "assets/icons/common/confirm.png";
import { setShowModalConfirm } from "redux/slices/global";
import DeleteIcon from "assets/icons/common/delete.svg";
// import PlusGreen from "assets/icons/common/plus-green.svg";

function AdvancedConfig({ listObjects, selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const { Option } = Select;
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [typeAuthen, setTypeAuthen] = useState("");
  const [fieldsTicket, setFieldsTicket] = useState([]);

  const [fields, setFields] = useState([]);
  const [objectFields, setObjectFields] = useState([]);

  const { fieldsObjectIC, tokenIC, objectFieldsConfig, configIC } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const { userDetail } = useSelector((state) => state.userReducer);

  const AuthenticationType = [
    // {
    //   label: 'Basic',
    //   value: 'basic'
    // },
    {
      label: "Token",
      value: "token",
    },
  ];

  const _onSubmit = (values) => {
    let data = {
      tenant_id: userDetail.tenant_id,
      authen_type: values.authenticationType,
      object_id: selectedObject,
      field_ids: fields,
      token: values.token,
      object_create_ticket: values.object_create_ticket,
      interaction_id: values.interaction_id,
      link_api_ic: values.link_api_ic,
      user_admin_ic: values.user_admin_ic,
      password_admin_ic: _.get(values, "password_admin_ic") || undefined,
    };
    if (configIC !== null && Object.keys(configIC).length > 0) {
      dispatch(
        updateConfigIC({
          ...data,
          id: configIC._id,
        })
      );
    } else {
      dispatch(
        createConfigIC({
          ...data,
        })
      );
    }
  };

  const onSelectObject = useCallback(
    (e, edit) => {
      if (e) {
        dispatch(
          loadFieldsConfig({
            object_id: e,
          })
        );
      }
      if (!edit) {
        form.setFieldsValue({
          interaction_id: undefined,
        });
      }
    },
    [dispatch, form]
  );

  const onSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const _onGenerateToken = () => {
    dispatch(getToken({}));
  };

  const onDeleteField = (idx) => {
    let temp = [...fields];
    temp.splice(idx, 1);
    setFields(temp);
  };

  const _onAddFields = () => {
    const item = [...fields, ""];
    setFields(item);
  };

  const handleChangeField = (e, idx) => {
    let arr = [...fields];
    fields.forEach((item, index) => {
      if (index === idx) {
        arr[index] = e;
      }
    });
    setFields(arr);
  };

  useEffect(() => {
    fields.forEach((item, idx) => {
      if (item !== "") {
        form.setFieldsValue({
          [item + idx]: item,
        });
      } else {
        form.setFieldsValue({
          ["" + idx]: undefined,
        });
      }
    });
  }, [form, fields]);

  useEffect(() => {
    if (tokenIC !== null) {
      form.setFieldsValue({
        token: tokenIC,
      });
    }
  }, [tokenIC, form]);

  useEffect(() => {
    if (fieldsObjectIC !== null) {
      let listFields = [];
      fieldsObjectIC.forEach((item) => {
        listFields.push({
          label: item.name,
          value: item.ID,
          type: item.type,
          option: item.option || [],
          required: item.required,
        });
      });
      setFieldsTicket(listFields);
    }
  }, [fieldsObjectIC]);

  useEffect(() => {
    if (objectFieldsConfig !== null) {
      let listFields = [];
      objectFieldsConfig.forEach((item) => {
        listFields.push({
          label: item.name,
          value: item.ID,
          type: item.type,
          option: item.option || [],
          required: item.required,
        });
      });
      listFields.push({
        label: "Assign to",
        value: "owner",
        type: "select",
        option: [],
        required: false,
      });
      setObjectFields(listFields);
    }
  }, [objectFieldsConfig]);

  useEffect(() => {
    if (_.get(configIC, "object_id")) {
      form.setFieldsValue({
        object_create_ticket: configIC.object_create_ticket,
        interaction_id: configIC.interaction_id,
        authenticationType: configIC.authen_type,
        token: configIC.token,
        link_api_ic: _.get(configIC, "link_api_ic", ""),
        user_admin_ic: _.get(configIC, "user_admin_ic", ""),
      });

      onSelectObject(configIC.object_create_ticket, true);
      setTypeAuthen(configIC.authen_type);
      setFields(configIC.field_ids);
    } else {
      form.resetFields();
      setTypeAuthen("");
      setFields([]);
    }
  }, [configIC, form, onSelectObject]);

  return (
    <Form
      form={form}
      onFinish={_onSubmit}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      colon={false}
      labelAlign="left"
    >
      <Wrapper>
        <BasicInfo>
          <Form.Item
            label="Object create ticket"
            name="object_create_ticket"
            rules={[
              {
                required: true,
                message: "Please select object create ticket!",
              },
            ]}
          >
            <Select
              placeholder="Select object create ticket"
              options={listObjects}
              onChange={(e) => onSelectObject(e)}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <Form.Item
            label="Interaction ID"
            name="interaction_id"
            rules={[
              { required: true, message: "Please select Interaction ID!" },
            ]}
          >
            <Select
              placeholder="Select Interaction ID"
              options={fieldsTicket}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>

          <Form.Item
            label="Authentication type"
            name="authenticationType"
            rules={[
              { required: true, message: "Please select authentication type!" },
            ]}
          >
            <Select
              placeholder="Select authentication type"
              allowClear
              options={AuthenticationType}
              onChange={(e) => onSelectAuthenType(e)}
            ></Select>
          </Form.Item>
          {typeAuthen === "token" && (
            <>
              <Form.Item
                label={<Button onClick={_onGenerateToken}>Get token</Button>}
                name="token"
                colon={false}
              >
                <Input disabled />
              </Form.Item>
            </>
          )}

          <Form.Item label="Link API" name="link_api_ic">
            <Input placeholder="Please input Link API" />
          </Form.Item>

          <Form.Item label="User name admin IC" name="user_admin_ic">
            <Input placeholder="Please input User name admin IC" />
          </Form.Item>

          <Form.Item label="Password admin IC" name="password_admin_ic">
            <Input.Password
              placeholder="Please input Password admin IC"
              autocomplete="new-password"
            />
          </Form.Item>
        </BasicInfo>
        <Fields>
          {fields.length > 0 && (
            <>
              {fields.map((item, idx) => {
                return (
                  <FormFields>
                    <FormValue
                      wrapperCol={{ span: 24 }}
                      label=""
                      name={item + idx}
                      rules={[
                        { required: true, message: "Please select field" },
                      ]}
                    >
                      <Select
                        placeholder="Select field"
                        onChange={(e) => {
                          handleChangeField(e, idx);
                        }}
                      >
                        {objectFields.map((item) => {
                          return (
                            <Option
                              disabled={
                                fields.find((field) => field === item.value)
                                  ? true
                                  : false
                              }
                              key={item.value}
                            >
                              {item.label}
                            </Option>
                          );
                        })}
                      </Select>
                    </FormValue>
                    <Delete>
                      <Tooltip title="Delete">
                        <img
                          src={DeleteIcon}
                          onClick={() => onDeleteField(idx)}
                          alt="delete"
                        />
                      </Tooltip>
                    </Delete>
                  </FormFields>
                );
              })}
            </>
          )}

          <AddNew onClick={() => _onAddFields()}>
            {/* <img src={PlusGreen} alt="plus" /> */}
            <span>+ Add Fields</span>
          </AddNew>
        </Fields>
      </Wrapper>

      <WrapButton>
        <Button
          disabled={
            (configIC !== null &&
              Object.keys(configIC).length > 0 &&
              !checkRule("edit")) ||
            ((configIC === null || Object.keys(configIC).length === 0) &&
              !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {configIC !== null && Object.keys(configIC).length > 0
            ? "Update"
            : "Save"}
        </Button>

        <Button
          style={{ marginLeft: "16px" }}
          disabled={!checkRule("delete")}
          onClick={() => dispatch(setShowModalConfirm(true))}
        >
          {t("common.reset")}
        </Button>
      </WrapButton>

      <ModalConfirm
        title={t("common.confirmReset")}
        decs={t("common.afterReset")}
        method={resetAllIC}
        data={{}}
        img={ImgConfirm}
      />
    </Form>
  );
}

export default AdvancedConfig;

const Wrapper = styled.div`
  display: flex;

  .ant-form-item {
    :last-child {
      margin-bottom: 0;
    }
  }
`;

const BasicInfo = styled.div`
  flex: 1;
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;
  height: fit-content;

  .ant-btn {
    font-size: 16px;
    width: 140px;
    color: ${(props) => props.theme.main};

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;
    }
  }

  .ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.main};
  }

  .ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.main};
  }
`;

const Fields = styled(BasicInfo)`
  margin-left: 16px;
`;

const WrapButton = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 83px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn {
    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const FormFields = styled(Form.Item)`
  width: 100%;
  padding: 0 16px;

  .ant-form-item-control-input-content {
    display: flex;
    align-items: center;
  }

  .ant-col {
    max-width: 100% !important;
  }
`;

const FormValue = styled(Form.Item)`
  margin-bottom: 0;
  margin-right: 16px;
  width: 95%;
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  width: 32px;
  height: 32px;
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    &:hover {
      cursor: pointer;
      background: #eeeeee;
    }
  }
`;

const AddNew = styled.div`
  padding: 0 16px;
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;
