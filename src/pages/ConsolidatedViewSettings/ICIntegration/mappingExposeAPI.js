import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";

import {
  createMappingExpose,
  updateMappingExpose,
} from "redux/slices/consolidatedViewSettings";

function MappingExposeAPI({ listObjects, selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { detailsMappingExposeAPI } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [keyUpdate, setKeyUpdate] = useState([]);

  const [keyFields, setKeyFields] = useState([]);

  const [notRequiredFields, setNotRequiredFields] = useState([]);

  const [optionalFields, setOptionalFields] = useState([]);
  const [requiredFields, setRequiredFields] = useState([]);

  const [mappingFields, setMappingFields] = useState({});

  const { objectFieldsConfig } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const _onSubmit = (values) => {
    let tempFields = requiredFields.concat(notRequiredFields);
    let tempKeyFields = [];

    tempFields.forEach((item) => {
      tempKeyFields.push(item.value);
    });

    let tempMapping = Object.keys(mappingFields);
    let resultMapping = { ...mappingFields };

    tempMapping.forEach((item) => {
      if (tempKeyFields.includes(item) === false) {
        delete resultMapping[item];
      }
    });

    let data = {
      object_id: selectedObject,
      type: "IC",
      key_field_update: values.key_field_update,
      key_mapping_update: values.key_mapping_update,
      mapping_keys: resultMapping,
    };

    if (
      detailsMappingExposeAPI &&
      Object.keys(detailsMappingExposeAPI)?.length > 0
    ) {
      dispatch(
        updateMappingExpose({
          ...data,
          _id: detailsMappingExposeAPI._id,
        })
      );
    } else {
      dispatch(
        createMappingExpose({
          ...data,
        })
      );
    }
  };

  const _onSelectKey = (e, option) => {
    let tempKey = [];
    if (option !== undefined) {
      tempKey.push(option);
    }
    setKeyUpdate(tempKey);
  };

  const onChangeValue = (label, value) => {
    let arr = { ...mappingFields };
    arr[label] = value;
    setMappingFields(arr);
  };

  const onSelectField = (e, option) => {
    setNotRequiredFields(option);
  };

  useEffect(() => {
    let tempKeyFields = [];
    let tempOptionalFields = [];
    let tempRequiredFields = [];
    objectFieldsConfig.forEach((item) => {
      if (item.type !== "id" && item.type !== "formula") {
        if (item.required) {
          tempRequiredFields.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
            required: item.required,
          });
        } else {
          tempOptionalFields.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
            required: item.required,
          });
        }
      }
    });

    tempOptionalFields.push({
      label: "Assign to",
      value: "owner",
      type: "user",
      option: [],
    });

    tempKeyFields = tempKeyFields
      .concat(tempRequiredFields)
      .concat(tempOptionalFields);

    setRequiredFields(tempRequiredFields);
    setOptionalFields(tempOptionalFields);
    setKeyFields(tempKeyFields);
  }, [objectFieldsConfig]);

  useEffect(() => {
    if (
      detailsMappingExposeAPI &&
      Object.prototype.toString.call(detailsMappingExposeAPI) !==
        "[object Array]"
    ) {
      let tempMapping = {};
      let tempNotRequiredFields = [];
      let tempSelectFields = [];
      let tempKeyUpdate = [];

      if (detailsMappingExposeAPI?.key_field_update.length !== 0) {
        tempKeyUpdate.push(detailsMappingExposeAPI?.key_field_update);
      }

      detailsMappingExposeAPI.mapping_keys.forEach((item) => {
        tempMapping[item.value] = item.selected_value;
        form.setFieldsValue({
          [item.value]: item.selected_value,
        });

        if (item.required === false) {
          tempSelectFields.push(item);
          tempNotRequiredFields.push(item.value);
        }
      });

      setMappingFields(tempMapping);
      setNotRequiredFields(tempSelectFields);
      setKeyUpdate(tempKeyUpdate);

      form.setFieldsValue({
        notRequiredFields: tempNotRequiredFields,
        key_mapping_update: detailsMappingExposeAPI.key_mapping_update,
        key_field_update: detailsMappingExposeAPI?.key_field_update?.value,
      });
    } else {
      setNotRequiredFields([]);
      setMappingFields([]);
      setKeyUpdate([]);
      form.resetFields();
    }
  }, [detailsMappingExposeAPI, form]);

  return (
    <Form
      form={form}
      onFinish={_onSubmit}
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      colon={false}
      labelAlign="left"
    >
      <Wrapper>
        <BasicInfo>
          <Form.Item label="Key field update" name="key_field_update">
            <Select
              allowClear
              placeholder="Select key field update"
              options={keyFields}
              onChange={(e, option) => _onSelectKey(e, option)}
              optionFilterProp="label"
              showSearch
            />
          </Form.Item>
          {keyUpdate.length > 0 && (
            <>
              {keyUpdate.map((item) => {
                return (
                  <Form.Item
                    label="Key_mapping_update"
                    name="key_mapping_update"
                    rules={[
                      {
                        required: true,
                        message: "Please input key mapping update!",
                      },
                    ]}
                  >
                    <Input placeholder={"Input key mapping update"} />
                  </Form.Item>
                );
              })}
            </>
          )}
          {requiredFields.length > 0 && (
            <>
              {requiredFields.map((item) => {
                return (
                  <Form.Item
                    label={item.label}
                    name={item.value}
                    rules={[
                      {
                        required: true,
                        message: `Please input ${item.label}!`,
                      },
                    ]}
                  >
                    <Input
                      placeholder={`Input ${item.label}`}
                      onChange={(e) =>
                        onChangeValue(item.value, e.target.value)
                      }
                    />
                  </Form.Item>
                );
              })}
            </>
          )}
        </BasicInfo>
        <Fields>
          <Form.Item label="Add Fields" name="notRequiredFields">
            <Select
              mode="multiple"
              placeholder="Select field"
              allowClear
              optionFilterProp="label"
              options={optionalFields}
              onChange={(e, option) => onSelectField(e, option)}
            />
          </Form.Item>

          {notRequiredFields.length > 0 && (
            <WrapField>
              {notRequiredFields.map((item) => {
                return (
                  <Form.Item
                    label={item.label}
                    name={item.value}
                    rules={[
                      {
                        required: true,
                        message: `Please input ${item.label}!`,
                      },
                    ]}
                  >
                    <Input
                      placeholder={`Input ${item.label}`}
                      onChange={(e) =>
                        onChangeValue(item.value, e.target.value)
                      }
                    />
                  </Form.Item>
                );
              })}
            </WrapField>
          )}
        </Fields>
      </Wrapper>

      <WrapButton>
        <Button
          disabled={
            (detailsMappingExposeAPI &&
              Object.keys(detailsMappingExposeAPI)?.length > 0 &&
              !checkRule("edit")) ||
            (!detailsMappingExposeAPI && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {detailsMappingExposeAPI &&
          Object.keys(detailsMappingExposeAPI)?.length > 0
            ? "Update"
            : "Save"}
        </Button>
      </WrapButton>
    </Form>
  );
}

export default MappingExposeAPI;

const Wrapper = styled.div`
  display: flex;

  .ant-form-item {
    :last-child {
      margin-bottom: 0;
    }
  }
`;

const BasicInfo = styled.div`
  flex: 1;
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;
  height: fit-content;

  .ant-btn {
    font-size: 16px;
    width: 140px;
    color: ${(props) => props.theme.main};

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;
    }
  }

  .ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.main};
  }

  .ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: ${(props) => props.theme.main};
  }
`;

const Fields = styled(BasicInfo)`
  margin-left: 16px;
`;

const WrapButton = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 83px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const WrapField = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 8px;
  overflow-y: auto;

  .ant-form-item {
    margin-bottom: 8px;
  }
`;
