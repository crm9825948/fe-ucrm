import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Empty from "antd/lib/empty";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Row from "antd/lib/row";
import Col from "antd/lib/col";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import EmptyMapping from "assets/images/consolidatedViewSetting/empty_user_mapping.png";

import {
  createUserMappingIC,
  resetStatus,
  deleteUserMappingIC,
  loadDetailsUserMappingIC,
  updateUserMappingIC,
  loadUsersMappingIC,
} from "redux/slices/consolidatedViewSettings";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

const AuthenticationType = [
  {
    label: "Basic",
    value: "basic",
  },
  {
    label: "SSO",
    value: "sso",
  },
];

function MappingUser({ selectedObject, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { Column } = Table;

  const { usersMapping, statusUserMapping, loading, detailsUserMappingIC } =
    useSelector((state) => state.consolidatedViewSettingsReducer);

  const [dataTable, setDataTable] = useState([]);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const [listUser, setListUser] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [isEdit, setIsEdit] = useState(false);
  const [typeAuthen, $typeAuthen] = useState(undefined);

  const onSelectAuthenType = (e) => {
    $typeAuthen(e);
  };

  const _onSubmit = (values) => {
    if (isEdit) {
      dispatch(
        updateUserMappingIC({
          id: detailsUserMappingIC._id,
          agent_name: values.agent_name,
          object_id: selectedObject,
          user_id: values.user_name,
          authen_type: typeAuthen,
          password: _.get(values, "password") || undefined,
        })
      );
    } else {
      dispatch(
        createUserMappingIC({
          agent_name: values.agent_name,
          object_id: selectedObject,
          user_id: values.user_name,
          authen_type: typeAuthen,
          password: _.get(values, "password") || undefined,
        })
      );
    }
  };

  const _onDelete = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: id });
  };

  const _onEdit = (id) => {
    setIsEdit(true);
    dispatch(
      loadDetailsUserMappingIC({
        id: id,
      })
    );
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    if (usersMapping !== null) {
      let tempList = [];
      usersMapping.map((item) => {
        return tempList.push({
          key: item._id,
          user_name: item.user_name,
          agent_name: item.agent_name,
          created_by: item.created_by,
          created_date: item.created_date,
          action: "",
        });
      });
      setDataTable(tempList);
    }
  }, [usersMapping]);

  useEffect(() => {
    if (statusUserMapping === "success") {
      form.resetFields();
      setIsEdit(false);
      dispatch(resetStatus());
      dispatch(
        loadUsersMappingIC({
          object_id: selectedObject,
        })
      );
    }
  }, [statusUserMapping, form, dispatch, selectedObject]);

  useEffect(() => {
    if (isEdit && detailsUserMappingIC !== null) {
      form.setFieldsValue({
        user_name: detailsUserMappingIC.user_id,
        agent_name: detailsUserMappingIC.agent_name,
        authentication_type: _.get(
          detailsUserMappingIC,
          "authen_type",
          undefined
        ),
      });

      $typeAuthen(_.get(detailsUserMappingIC, "authen_type", ""));
    }
  }, [isEdit, detailsUserMappingIC, form]);

  useEffect(() => {
    form.resetFields();
    setIsEdit(false);
  }, [selectedObject, form]);

  return (
    <>
      <FormCustom
        form={form}
        onFinish={_onSubmit}
        colon={false}
        labelAlign="left"
        layout="vertical"
      >
        <Row gutter={16}>
          <Col span={12}>
            <Form.Item
              label="User name"
              name="user_name"
              rules={[
                {
                  required: true,
                  message: "Please select User name!",
                },
              ]}
            >
              <Select
                placeholder="Select User name"
                options={listUser}
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            <Form.Item
              label="Agent name"
              name="agent_name"
              rules={[{ required: true, message: "Please input Agent name!" }]}
            >
              <Input placeholder="Input agent name" />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={16}>
          <Col span={12}>
            <Form.Item label="Authentication type" name="authentication_type">
              <Select
                placeholder="Select authentication type"
                allowClear
                options={AuthenticationType}
                onChange={(e) => onSelectAuthenType(e)}
              />
            </Form.Item>
          </Col>

          <Col span={12}>
            {typeAuthen === "basic" && (
              <Form.Item label="Password" name="password">
                <Input.Password
                  autocomplete="new-password"
                  placeholder="Please input password"
                />
              </Form.Item>
            )}
          </Col>
        </Row>

        <Button
          disabled={
            (isEdit && !checkRule("edit")) || (!isEdit && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {isEdit ? "Update" : "Save"}
        </Button>
      </FormCustom>

      {usersMapping?.length > 0 ? (
        <UserMapping>
          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title="User name"
              dataIndex="user_name"
              key="user_name"
              width="24%"
            />
            <Column
              title="Agent name"
              dataIndex="agent_name"
              key="agent_name"
              width="16%"
            />
            <Column
              title="Create by"
              dataIndex="created_by"
              key="created_by"
              width="24%"
            />
            <Column
              title="Create time"
              dataIndex="created_date"
              key="created_date"
              width="24%"
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title="Action"
                dataIndex="action"
                key="action"
                width="12%"
                fixed="right"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title="Edit">
                        <img
                          onClick={() => _onEdit(record.key)}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title="Delete">
                        <img
                          onClick={() => _onDelete(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
        </UserMapping>
      ) : (
        <WrapEmpty>
          <Empty
            image={EmptyMapping}
            description={
              <p>
                There's no any <span>Mapping user</span>
              </p>
            }
          ></Empty>
        </WrapEmpty>
      )}

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteUserMappingIC}
        dataDelete={dataDelete}
        isLoading={loading.userMappingIC}
      />
    </>
  );
}

export default MappingUser;

const FormCustom = styled(Form)`
  background: #fff;
  border-radius: 0 0 10px 10px;
  padding: 16px;

  .ant-form-item {
    flex: 1;
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;
    width: 13.625rem;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const UserMapping = styled.div`
  background: #fff;
  border-radius: 10px;
  margin-top: 16px;

  .ant-table-wrapper {
    flex: 1;
    padding: 16px;
  }
`;

const WrapEmpty = styled.div`
  background: #fff;
  border-radius: 10px;
  min-height: 218px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;
    width: 18px;

    :hover {
      background: #eeeeee;
    }
  }
`;
