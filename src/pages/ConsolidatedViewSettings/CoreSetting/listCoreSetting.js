import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import {
  loadCoreSetting,
  deleteCoreSetting,
  loadDetailsCoreSetting,
} from "redux/slices/consolidatedViewSettings";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { useTranslation } from "react-i18next";

function ListCoreSetting({ selectedObject, setIsEdit, checkRule }) {
  const dispatch = useDispatch();
  const { Column } = Table;
  const { Text } = Typography;

  const { listCoreSetting, loading } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { t } = useTranslation();
  const [dataTable, setDataTable] = useState([]);
  const [dataDelete, setDataDelete] = useState({});

  const _onDeleteCoreSetting = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ api_id: id });
  };

  const _onEditCoreSetting = (id) => {
    setIsEdit(true);
    dispatch(
      loadDetailsCoreSetting({
        api_id: id,
      })
    );
  };

  useEffect(() => {
    dispatch(
      loadCoreSetting({
        object_id: selectedObject,
      })
    );
  }, [selectedObject, dispatch]);

  useEffect(() => {
    if (listCoreSetting !== null) {
      let tempList = [];
      listCoreSetting.map((item, idx) => {
        return tempList.push({
          key: item._id,
          name: item.name,
          api_url: item.api_url,
          method: item.method,
          created_by: item.created_by,
          created_date: item.created_date,
          action: "",
        });
      });
      setDataTable(tempList);
    }
  }, [listCoreSetting]);

  return (
    <Wrapper>
      <Table pagination={false} dataSource={dataTable}>
        <Column
          title="Name of component"
          dataIndex="name"
          key="name"
          width="229px"
        />
        <Column
          title="API URL"
          dataIndex="api_url"
          key="api_url"
          width="320px"
          render={(text) => <Text ellipsis={{ tooltip: text }}>{text}</Text>}
        />
        <Column title="Method" dataIndex="method" key="method" width="198px" />
        <Column
          title={t("coreSetting.createBy")}
          dataIndex="created_by"
          key="created_by"
          width="213px"
        />
        <Column
          title={t("coreSetting.createTime")}
          dataIndex="created_date"
          key="created_date"
          width="229px"
        />
        {(checkRule("edit") || checkRule("delete")) && (
          <Column
            title={t("coreSetting.action")}
            dataIndex="action"
            key="action"
            width="107px"
            fixed="right"
            render={(text, record) => (
              <WrapAction>
                {checkRule("edit") && (
                  <Tooltip title="Edit">
                    <img
                      onClick={() => _onEditCoreSetting(record.key)}
                      src={Edit}
                      alt="edit"
                    />
                  </Tooltip>
                )}
                {checkRule("delete") && (
                  <Tooltip title="Delete">
                    <img
                      onClick={() => _onDeleteCoreSetting(record.key)}
                      src={Delete}
                      alt="delete"
                    />
                  </Tooltip>
                )}
              </WrapAction>
            )}
          />
        )}
      </Table>

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteCoreSetting}
        dataDelete={dataDelete}
        isLoading={loading.coreSetting}
      />
    </Wrapper>
  );
}

export default ListCoreSetting;

const Wrapper = styled.div`
  width: calc(96% + 32px);
  padding: 16px;
  background: #fff;
  border-radius: 16px;
  margin-bottom: 16px;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;
    width: 18px;

    :hover {
      background: #eeeeee;
    }
  }
`;
