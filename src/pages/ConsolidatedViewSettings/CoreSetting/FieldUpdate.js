import React from "react";
import styled from "styled-components";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import { useTranslation } from "react-i18next";
// import InputNumber from 'antd/lib/input-number'
// import DatePicker from 'antd/lib/date-picker'

function FieldUpdate({ item, relatedFields, listUser, onChangeValue }) {
  const { t } = useTranslation();
  return (
    <Wrapper>
      {item.type === "select" ||
      item.type === "dynamic-field" ||
      item.type === "user" ||
      item.type === "linkingobject" ? (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            {
              required: item.required,
              message: `${t("coreSetting.pleaseSelect")} ${item.label}!`,
            },
          ]}
        >
          <Select
            placeholder={`${t("coreSetting.select")} ${item.label}`}
            allowClear
            options={
              item.type === "user"
                ? listUser
                : item.type === "linkingobject"
                ? relatedFields
                : item.option
            }
            onChange={(e) => onChangeValue(item.value, e, "update")}
          ></Select>
        </Form.Item>
      ) : (
        // ) : item.type === 'number' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={`${item.value}Update`}
        //     rules={[
        //       { required: item.required, message: `Please input ${item.label}!` }
        //     ]}
        //   >
        //     <InputNumber
        //       placeholder={`Input ${item.label}`}
        //       formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
        //       parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
        //       onChange={(e) => onChangeValue(item.value, e, 'update')}
        //     />
        //   </Form.Item>
        // ) : item.type === 'date' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={`${item.label}Update`}
        //     rules={[
        //       { required: item.required, message: `Please select ${item.label}!` }
        //     ]}
        //   >
        //     <DatePicker
        //       placeholder={`Select ${item.label}`}
        //       onChange={(date, dateString) =>
        //         onChangeValue(item.value, dateString, 'update')
        //       }
        //     />
        //   </Form.Item>
        // ) : item.type === 'datetime-local' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={`${item.label}Update`}
        //     rules={[
        //       { required: item.required, message: `Please select ${item.label}!` }
        //     ]}
        //   >
        //     <DatePicker
        //       placeholder={`Select ${item.label}`}
        //       showTime
        //       onChange={(date, dateString) =>
        //         onChangeValue(item.value, dateString, 'update')
        //       }
        //     />
        //   </Form.Item>
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            {
              required: item.required,
              message: `${t("coreSetting.pleaseInput")} ${item.label}!`,
            },
          ]}
        >
          <Input
            placeholder={`${t("coreSetting.input")} ${item.label}`}
            onChange={(e) =>
              onChangeValue(item.value, e.target.value, "update")
            }
          />
        </Form.Item>
      )}
    </Wrapper>
  );
}

export default FieldUpdate;

const Wrapper = styled.div``;
