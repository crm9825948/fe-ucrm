import styled from "styled-components";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";

function FieldCreate({ item, relatedFields, listUser, onChangeValue }) {
  return (
    <Wrapper>
      {item.type === "select" ||
      item.type === "dynamic-field" ||
      item.type === "user" ||
      item.type === "linkingobject" ? (
        <Form.Item
          label={item.label}
          name={item.value}
          rules={[
            {
              required: item.required,
              message: `Please select ${item.label}!`,
            },
          ]}
        >
          <Select
            placeholder={`Select ${item.label}`}
            allowClear
            options={
              item.type === "user"
                ? listUser
                : item.type === "linkingobject"
                ? relatedFields
                : item.option
            }
            onChange={(e) => onChangeValue(item.value, e)}
          ></Select>
        </Form.Item>
      ) : (
        // ) : item.type === 'number' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={item.value}
        //     rules={[
        //       { required: item.required, message: `Please input ${item.label}!` }
        //     ]}
        //   >
        //     <InputNumber
        //       placeholder={`Input ${item.label}`}
        //       formatter={(value) => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
        //       parser={(value) => value.replace(/\$\s?|(,*)/g, '')}
        //       onChange={(e) => onChangeValue(item.value, e)}
        //     />
        //   </Form.Item>
        // ) : item.type === 'date' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={item.label}
        //     rules={[
        //       { required: item.required, message: `Please select ${item.label}!` }
        //     ]}
        //   >
        //     <DatePicker
        //       placeholder={`Select ${item.label}`}
        //       onChange={(date, dateString) => onChangeValue(item.value, dateString)}
        //     />
        //   </Form.Item>
        // ) : item.type === 'datetime-local' ? (
        //   <Form.Item
        //     label={item.label}
        //     name={item.label}
        //     rules={[
        //       { required: item.required, message: `Please select ${item.label}!` }
        //     ]}
        //   >
        //     <DatePicker
        //       placeholder={`Select ${item.label}`}
        //       showTime
        //       onChange={(date, dateString) => onChangeValue(item.value, dateString)}
        //     />
        //   </Form.Item>
        <Form.Item
          label={item.label}
          name={item.value}
          rules={[
            { required: item.required, message: `Please input ${item.label}!` },
          ]}
        >
          <Input
            placeholder={`Input ${item.label}`}
            onChange={(e) => onChangeValue(item.value, e.target.value)}
          />
        </Form.Item>
      )}
    </Wrapper>
  );
}

export default FieldCreate;

const Wrapper = styled.div``;
