import { useState, useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import _ from "lodash";

import Form from "antd/lib/form";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Button from "antd/lib/button";
import Checkbox from "antd/lib/checkbox";

import {
  loadDataCoreSetting,
  loadFieldsCreate,
  createCoreSetting,
  updateCoreSetting,
  loadCoreSetting,
  resetStatus,
  loadDetailsCoreSettingFail,
  loadComponents,
  loadComponentsSuccess,
} from "redux/slices/consolidatedViewSettings";

import FieldCreate from "./FieldCreate";
import FieldUpdate from "./FieldUpdate";
import ListViewCoreSetting from "./listCoreSetting";
import { useTranslation } from "react-i18next";
import SelectObject from "components/ExternalService/selectObject";
import { Switch } from "antd";
const { Option } = Select;
function CoreSetting() {
  const dispatch = useDispatch();
  const [form] = Form.useForm();

  const { t } = useTranslation();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { fieldsObject, statusCoreSetting, detailsCoreSetting, components } =
    useSelector((state) => state.consolidatedViewSettingsReducer);

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { listFieldsObject } = useSelector((state) => state.reportReducer);
  const { listAllUser } = useSelector((state) => state.userReducer);

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const AuthenticationType = [
    {
      label: "Basic auth",
      value: "BASIC_AUTH",
    },
    {
      label: "Token",
      value: "token",
    },
  ];

  const Method = [
    {
      label: "GET",
      value: "GET",
    },
    {
      label: "POST",
      value: "POST",
    },
  ];

  const componentType = [
    {
      label: "TABLE",
      value: "TABLE",
    },
    {
      label: "DETAILS",
      value: "DETAILS",
    },
  ];

  const OptionColumns = [
    { label: 1, value: 1 },
    { label: 2, value: 2 },
    { label: 3, value: 3 },
    { label: 4, value: 4 },
    { label: 5, value: 5 },
  ];

  const [objectFields, setObjectFields] = useState([]);
  const [relatedFields, setRelatedFields] = useState([]);
  const [fieldsUpdate, setFieldsUpdate] = useState([]);
  const [fieldsCreate, setFieldsCreate] = useState([]);
  const [listUser, setListUser] = useState([]);
  const [isEdit, setIsEdit] = useState(false);
  const [IDEdit, setIDEdit] = useState("");

  const [typeAuthen, setTypeAuthen] = useState("");
  const [typeView, setTypeView] = useState("");
  const [section, setSection] = useState(0);
  const [enable, setEnable] = useState(false);
  const [getNewToken, $getNewToken] = useState(false);
  const [requestBodyFields, setRequestBodyFields] = useState([]);

  const [mappingFieldsToken, $mappingFieldsToken] = useState([]);

  const [requiredFieldsUpdate, setRequiredFieldsUpdate] = useState([]);
  const [notRequiredFieldsUpdate, setNotRequiredFieldsUpdate] = useState([]);

  const [requiredFieldsCreate, setRequiredFieldsCreate] = useState([]);
  const [notRequiredFields, setNotRequiredFields] = useState([]);

  const [mappingFieldsUpdate, setMappingFieldsUpdate] = useState({});
  const [mappingFields, setMappingFields] = useState({});
  const [objectComponent, setObjectComponent] = useState("");

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "external_app_integration" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSubmit = (values) => {
    let tempRequestBody = {};

    values.requestBody.map((item) => {
      return (tempRequestBody[item] = values[`${item}Body`]);
    });

    let tempFields = requiredFieldsCreate.concat(notRequiredFields);
    let tempKeyFields = [];

    tempFields.forEach((item) => {
      tempKeyFields.push(item.value);
    });

    let tempMapping = Object.keys(mappingFields);
    let resultMappingCreate = { ...mappingFields };

    tempMapping.forEach((item) => {
      if (tempKeyFields.includes(item) === false) {
        delete resultMappingCreate[item];
      }
    });

    let tempFieldsUpdate = requiredFieldsUpdate.concat(notRequiredFieldsUpdate);
    let tempKeyFieldsUpdate = [];

    tempFieldsUpdate.forEach((item) => {
      tempKeyFieldsUpdate.push(item.value);
    });

    let tempMappingUpdate = Object.keys(mappingFieldsUpdate);
    let resultMappingUpdate = { ...mappingFieldsUpdate };

    tempMappingUpdate.forEach((item) => {
      if (tempKeyFieldsUpdate.includes(item) === false) {
        delete resultMappingUpdate[item];
      }
    });
    let temp = [];
    if (values.componentType === "DETAILS") {
      for (let i = 0; i < section; i++) {
        temp.push({
          display_fields: values[`show_field${i}`],
          section_name: values[`section${i}`],
        });
      }
    }
    let data = {
      name: values.name,
      key_object: values.key_object,
      object_id: selectedObject,
      api_url: values.apiUrl,
      authentication_type: values.authenticationType,
      user_name: values.userName,
      password: _.get(values, "password", undefined),
      token: values.token,
      method: values.method,
      component_type: values.componentType,
      object_create_record: values.object,
      request_body: tempRequestBody,
      mapping_update: resultMappingUpdate,
      mapping_create: resultMappingCreate,
      query_key: values.query_key,
      get_new_token_when_call: _.get(values, "get_new_token_when_call", false),
      api_get_token: {
        api_url: _.get(values, "api_url", ""),
        method: _.get(values, "methodToken", "POST"),
        request_body: mappingFieldsToken,
        content_type: _.get(values, "content_type", ""),
      },
      columns: values.columns,
      details_column_setting: temp,
      enable_listview_with_details:
        values.componentType === "TABLE" ? enable : undefined,
      list_of_components: values.list_of_components,
    };
    if (isEdit) {
      dispatch(
        updateCoreSetting({
          ...data,
          api_id: IDEdit,
        })
      );
    } else {
      dispatch(
        createCoreSetting({
          ...data,
        })
      );
    }
  };

  const _onAddFieldsToken = () => {
    $mappingFieldsToken([...mappingFieldsToken, {}]);
  };

  const handleChangeFieldToken = (value, type, idx) => {
    let tempMapping = [...mappingFieldsToken];

    if (type === "key") {
      tempMapping[idx] = {
        [value]: Object.values(tempMapping[idx])[0],
      };
    } else {
      tempMapping[idx] = {
        [Object.keys(tempMapping[idx])[0]]: value,
      };
    }

    $mappingFieldsToken(tempMapping);
  };

  const onSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const onSelectBody = (e, option) => {
    let temp = [...option];
    temp.forEach((item, idx) => {
      if (item.label === undefined) {
        temp[idx] = { label: e[idx], value: e[idx] };
      }
    });
    setRequestBodyFields(temp);
  };

  const onSelectField = (e, option, type) => {
    if (type === "create") {
      setNotRequiredFields(option);
    } else {
      setNotRequiredFieldsUpdate(option);
    }
  };

  const onChangeValue = (label, value, type) => {
    if (type === "update") {
      let arr = { ...mappingFieldsUpdate };
      arr[label] = value;
      setMappingFieldsUpdate(arr);
    } else {
      let arr = { ...mappingFields };
      arr[label] = value;
      setMappingFields(arr);
    }
  };

  const onSelectObject = useCallback(
    (e, edit) => {
      dispatch(
        loadFieldsCreate({
          object_id: e,
        })
      );

      if (!edit) {
        setNotRequiredFields([]);
        form.setFieldsValue({
          notRequiredFieldCreate: [],
        });
      }
    },
    [dispatch, form]
  );

  const _onSelectObject = (value) => {
    $selectedObject(value);

    dispatch(
      loadDataCoreSetting({
        api_version: "2",
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (fieldsObject !== null) {
      let tempFieldsCreate = [];
      let listRequiredFieldsCreate = [];
      fieldsObject.forEach((item) => {
        if (item.type !== "id") {
          if (item.required) {
            listRequiredFieldsCreate.push({
              label: item.name,
              value: item.ID,
              type: item.type,
              option: item.option || [],
              required: item.required,
            });
          } else {
            tempFieldsCreate.push({
              label: item.name,
              value: item.ID,
              type: item.type,
              option: item.option || [],
            });
          }
        }
      });
      setFieldsCreate(tempFieldsCreate);
      setRequiredFieldsCreate(listRequiredFieldsCreate);
    }
  }, [fieldsObject]);

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    let tempList = [];
    let tempListUpdate = [];
    let listRequiredFieldsUpdate = [];
    fields_related.forEach((item) => {
      tempList.push({
        label: item.name,
        value: item.ID,
        type: item.type,
        option: item.option || [],
      });
      if (item.type !== "id") {
        if (item.required) {
          listRequiredFieldsUpdate.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
            required: item.required,
          });
        } else {
          tempListUpdate.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
          });
        }
      }
    });

    setRequiredFieldsUpdate(listRequiredFieldsUpdate);
    setFieldsUpdate(tempListUpdate);
    setObjectFields(tempList);
  }, [fields_related]);

  useEffect(() => {
    let tempFields = [];
    listFieldsObject.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempFields.push({
                label: field.related_name,
                value: field.full_field_id,
                type: field.type,
              });
            }
          });
        });
      }
    });
    setRelatedFields(tempFields);
  }, [listFieldsObject]);

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    if (isEdit) {
      if (detailsCoreSetting !== null) {
        let tempRequestBody = [];
        detailsCoreSetting.request_body.forEach((item) => {
          tempRequestBody.push(item.value);
          form.setFieldsValue({
            [item.value + "Body"]: item.selected_value,
          });
        });

        let tempMappingUpdate = {};
        let tempNotRequiredFieldsUpdate = [];
        let tempSelectFieldsUpdate = [];
        detailsCoreSetting.mapping_update.forEach((item) => {
          tempMappingUpdate[item.value] = item.selected_value;
          form.setFieldsValue({
            [item.value + "Update"]: item.selected_value,
          });

          if (item.required === false) {
            tempSelectFieldsUpdate.push(item);
            tempNotRequiredFieldsUpdate.push(item.value);
          }
        });

        let tempMappingCreate = {};
        let tempNotRequiredFields = [];
        let tempSelectFieldsCreate = [];
        detailsCoreSetting.mapping_create.forEach((item) => {
          tempMappingCreate[item.value] = item.selected_value;
          form.setFieldsValue({
            [item.value]: item.selected_value,
          });

          if (item.required === false) {
            tempSelectFieldsCreate.push(item);
            tempNotRequiredFields.push(item.value);
          }
        });
        setNotRequiredFields(tempSelectFieldsCreate);
        setNotRequiredFieldsUpdate(tempSelectFieldsUpdate);
        $getNewToken(
          _.get(detailsCoreSetting, "get_new_token_when_call", false)
        );
        $mappingFieldsToken(
          _.get(detailsCoreSetting, "api_get_token.request_body", [])
        );

        setMappingFields(tempMappingCreate);
        setMappingFieldsUpdate(tempMappingUpdate);
        setRequestBodyFields(detailsCoreSetting.request_body);
        onSelectObject(detailsCoreSetting.object_create_record, true);
        setTypeAuthen(detailsCoreSetting.authentication_type);
        setTypeView(detailsCoreSetting.component_type);
        setSection(detailsCoreSetting.columns ? detailsCoreSetting.columns : 0);
        setEnable(detailsCoreSetting.enable_listview_with_details);

        if (detailsCoreSetting.details_column_setting) {
          detailsCoreSetting.details_column_setting.forEach((item, index) => {
            form.setFieldsValue({
              [`section${index}`]: item.section_name,
              [`show_field${index}`]: item.display_fields,
            });
          });
        }
        form.setFieldsValue({
          name: detailsCoreSetting.name,
          key_object: detailsCoreSetting.key_object,
          apiUrl: detailsCoreSetting.api_url,
          authenticationType: detailsCoreSetting.authentication_type,
          userName: detailsCoreSetting.user_name,
          // password: detailsCoreSetting.password,
          method: detailsCoreSetting.method,
          componentType: detailsCoreSetting.component_type,
          object: detailsCoreSetting.object_create_record,
          requestBody: tempRequestBody,
          notRequiredFieldCreate: tempNotRequiredFields,
          notRequiredFieldUpdate: tempNotRequiredFieldsUpdate,
          query_key: detailsCoreSetting.query_key,
          token: detailsCoreSetting?.token,
          api_url: _.get(detailsCoreSetting, "api_get_token.api_url", ""),
          content_type: _.get(
            detailsCoreSetting,
            "api_get_token.content_type",
            ""
          ),
          methodToken: _.get(detailsCoreSetting, "api_get_token.method", ""),
          get_new_token_when_call: _.get(
            detailsCoreSetting,
            "get_new_token_when_call",
            false
          ),
          columns: detailsCoreSetting.columns,
          enable_listview_with_details:
            detailsCoreSetting.enable_listview_with_details,
          list_of_components: detailsCoreSetting.list_of_components,
        });
        setObjectComponent(detailsCoreSetting.object_create_record);
        setIDEdit(detailsCoreSetting._id);
      }
    }
  }, [detailsCoreSetting, form, isEdit, onSelectObject]);

  useEffect(() => {
    if (statusCoreSetting === "success") {
      form.resetFields();
      setRequiredFieldsCreate([]);
      setNotRequiredFields([]);
      setRequestBodyFields([]);
      setNotRequiredFieldsUpdate([]);
      setMappingFields([]);
      setMappingFieldsUpdate([]);
      $mappingFieldsToken([]);
      $getNewToken(false);
      setTypeAuthen("");
      setTypeView("");
      setSection(0);
      setEnable(false);
      setIsEdit(false);
      dispatch(
        loadCoreSetting({
          object_id: selectedObject,
        })
      );
      dispatch(resetStatus());
    }
  }, [form, statusCoreSetting, dispatch, selectedObject]);

  useEffect(() => {
    if (objectComponent) {
      dispatch(
        loadComponents({
          object_id: objectComponent,
        })
      );
    }
  }, [dispatch, objectComponent]);

  useEffect(() => {
    form.resetFields();
    setRequiredFieldsCreate([]);
    setNotRequiredFields([]);
    setRequestBodyFields([]);
    setNotRequiredFieldsUpdate([]);
    setMappingFields([]);
    setMappingFieldsUpdate([]);
    $mappingFieldsToken([]);
    $getNewToken(false);
    setTypeAuthen("");
    setTypeView("");
    setEnable(false);
    setSection(0);
    setIsEdit(false);
    dispatch(loadDetailsCoreSettingFail());
    setObjectComponent("");
    dispatch(loadComponentsSuccess([]));
  }, [selectedObject, form, dispatch]);

  const renderSession = (number) => {
    let temp = [];
    for (let i = 0; i < number; i++) {
      temp.push({ section: `section${i}`, field: `show_field${i}` });
    }

    return temp.map((item, index) => (
      <WrapSession key={index}>
        <Form.Item
          label="Section name"
          name={item.section}
          rules={[
            {
              required: true,
              message: "Please input!",
            },
          ]}
        >
          <Input placeholder="Please input" />
        </Form.Item>
        <Form.Item
          label="Show field"
          name={item.field}
          rules={[
            {
              required: true,
              message: "Please input!",
            },
          ]}
        >
          <Select placeholder="Please select" mode="tags" option={[]} />
        </Form.Item>
      </WrapSession>
    ));
  };

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="External app integration"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <>
          <Form
            form={form}
            onFinish={_onSubmit}
            colon={false}
            labelAlign="left"
            layout="vertical"
          >
            <Wrap>
              <WrapInfo col={getNewToken}>
                <BasicInfo>
                  <legend>{t("coreSetting.infomation")}</legend>

                  <Form.Item
                    label="Name of component"
                    name="name"
                    rules={[
                      {
                        required: true,
                        message: "Please input name component!",
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label={t("coreSetting.keyOfObject")}
                    name="key_object"
                    rules={[
                      { required: true, message: t("coreSetting.messageKey") },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label="API URL"
                    name="apiUrl"
                    rules={[
                      { required: true, message: t("coreSetting.messAPIURL") },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  <Form.Item
                    label={t("coreSetting.authenticationType")}
                    name="authenticationType"
                    rules={[
                      {
                        required: true,
                        message: t("coreSetting.messageAuthenticationType"),
                      },
                    ]}
                  >
                    <Select
                      placeholder={t(
                        "coreSetting.placeholderAuthenticationType"
                      )}
                      allowClear
                      options={AuthenticationType}
                      onChange={(e) => onSelectAuthenType(e)}
                    ></Select>
                  </Form.Item>

                  {typeAuthen === "BASIC_AUTH" && (
                    <>
                      <Form.Item
                        label={t("coreSetting.userName")}
                        name="userName"
                        rules={[
                          {
                            required: true,
                            message: t("coreSetting.messUserName"),
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item
                        label={t("coreSetting.password")}
                        name="password"
                      >
                        <Input.Password autocomplete="new-password" />
                      </Form.Item>
                    </>
                  )}

                  {typeAuthen === "token" && (
                    <>
                      <CustomFormItem
                        name="get_new_token_when_call"
                        valuePropName="checked"
                      >
                        <Checkbox
                          onChange={(e) => $getNewToken(e.target.checked)}
                        >
                          Get new token when call
                        </Checkbox>
                      </CustomFormItem>

                      {!getNewToken && (
                        <Form.Item
                          label="Token"
                          name="token"
                          rules={[
                            {
                              required: true,
                              message: "Please input token",
                            },
                          ]}
                        >
                          <Input />
                        </Form.Item>
                      )}
                    </>
                  )}

                  <CustomFormItem
                    label={t("coreSetting.method")}
                    name="method"
                    rules={[
                      { required: true, message: t("coreSetting.messMethod") },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={Method}
                    ></Radio.Group>
                  </CustomFormItem>

                  <Form.Item
                    label={t("coreSetting.requestBody")}
                    name="requestBody"
                    rules={[
                      {
                        required: true,
                        message: t("coreSetting.messageRequestBody"),
                      },
                    ]}
                  >
                    <Select
                      mode="multiple"
                      placeholder={t("coreSetting.placeholderRequestBody")}
                      options={objectFields}
                      onChange={(e, option) => onSelectBody(e, option)}
                      optionFilterProp="label"
                      showSearch
                    />
                  </Form.Item>

                  {requestBodyFields.length > 0 && (
                    <WrapField>
                      {requestBodyFields.map((item) => {
                        return (
                          <Form.Item
                            label={item.label}
                            name={`${item.value}Body`}
                            rules={[
                              {
                                required: true,
                                message: `Please input ${item.label}!`,
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        );
                      })}
                    </WrapField>
                  )}

                  <CustomFormItem
                    label={t("coreSetting.type")}
                    name="componentType"
                    rules={[
                      { required: true, message: t("coreSetting.messType") },
                    ]}
                  >
                    <Radio.Group
                      optionType="button"
                      options={componentType}
                      onChange={(e) => {
                        setTypeView(e.target.value);
                      }}
                    ></Radio.Group>
                  </CustomFormItem>
                  {typeView === "DETAILS" && (
                    <>
                      <Form.Item
                        label="Columns"
                        name="columns"
                        rules={[
                          {
                            required: true,
                            message: "Please choose columns",
                          },
                        ]}
                      >
                        <Select
                          placeholder="Select columns "
                          allowClear
                          options={OptionColumns}
                          onChange={(e) => {
                            setSection(e);
                          }}
                        ></Select>
                      </Form.Item>
                      {renderSession(section)}
                    </>
                  )}
                  {typeView === "TABLE" && (
                    <>
                      <CustomFormItem
                        name="enable_listview_with_details"
                        label="Enable listview with detail"
                        valuePropName="checked"
                        // rules={[
                        //   {
                        //     required: true,
                        //     message: "Please choose ",
                        //   },
                        // ]}
                      >
                        <Switch
                          checkedChildren={t("knowledgeBase.on")}
                          unCheckedChildren={t("knowledgeBase.off")}
                          onChange={(values) => {
                            setEnable(values);
                          }}
                        />
                      </CustomFormItem>
                      {enable && (
                        <>
                          {/* <Form.Item
                            label="Columns"
                            name="columns"
                            rules={[
                              {
                                required: true,
                                message: "Please choose columns",
                              },
                            ]}
                          >
                            <Select
                              placeholder="Select columns "
                              allowClear
                              options={OptionColumns}
                              onChange={(e) => {
                                setSection(e);
                              }}
                            ></Select>
                          </Form.Item>
                          {renderSession(section)} */}
                          <Form.Item
                            label="Components"
                            name="list_of_components"
                          >
                            <Select
                              placeholder="Select components"
                              mode="multiple"
                            >
                              {components.map(
                                (item, index) =>
                                  (item.type === "tab" ||
                                    (item.component_type &&
                                      !item.enable_listview_with_details)) &&
                                  item._id !== detailsCoreSetting?._id && (
                                    <Option
                                      key={index}
                                      value={JSON.stringify(item)}
                                    >
                                      {item.name}
                                    </Option>
                                  )
                              )}
                            </Select>
                          </Form.Item>
                        </>
                      )}
                    </>
                  )}
                </BasicInfo>
              </WrapInfo>

              {getNewToken && (
                <WrapInfo col={getNewToken}>
                  <BasicInfo>
                    <legend>{t("coreSetting.mappingFieldsToken")}</legend>

                    <Form.Item
                      label="API url"
                      name="api_url"
                      rules={[
                        {
                          required: true,
                          message: "Please input API url!",
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>

                    <Form.Item
                      label="Content type"
                      name="content_type"
                      rules={[
                        {
                          required: true,
                          message: "Please input Content type!",
                        },
                      ]}
                    >
                      <Select
                        options={[
                          {
                            label: "application/json",
                            value: "application/json",
                          },
                          {
                            label: "application/x-www-form-urlencoded",
                            value: "application/x-www-form-urlencoded",
                          },
                        ]}
                      />
                    </Form.Item>

                    <CustomFormItem
                      label={t("coreSetting.method")}
                      name="methodToken"
                      rules={[
                        {
                          required: true,
                          message: t("coreSetting.messMethod"),
                        },
                      ]}
                    >
                      <Radio.Group
                        optionType="button"
                        options={Method}
                      ></Radio.Group>
                    </CustomFormItem>
                    {mappingFieldsToken.length > 0 && (
                      <>
                        {mappingFieldsToken.map((item, idx) => (
                          <div
                            key={idx}
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              marginBottom: "8px",
                            }}
                          >
                            <Input
                              placeholder="key"
                              value={Object.keys(item)[0]}
                              style={{ width: "48%" }}
                              onChange={(e) =>
                                handleChangeFieldToken(
                                  e.target.value,
                                  "key",
                                  idx
                                )
                              }
                            />
                            <Input
                              placeholder="value"
                              value={Object.values(item)[0]}
                              style={{ width: "48%" }}
                              onChange={(e) =>
                                handleChangeFieldToken(
                                  e.target.value,
                                  "value",
                                  idx
                                )
                              }
                            />
                          </div>
                        ))}
                      </>
                    )}

                    <AddNew onClick={() => _onAddFieldsToken()}>
                      <span>+ Add new</span>
                    </AddNew>
                  </BasicInfo>
                </WrapInfo>
              )}

              <WrapInfo col={getNewToken}>
                <MappingUpdate>
                  <legend>{t("coreSetting.mappingFieldsUpdate")}</legend>
                  {requiredFieldsUpdate.length > 0 && (
                    <>
                      {requiredFieldsUpdate.map((item, idx) => {
                        return (
                          <FieldUpdate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </>
                  )}
                  <Form.Item
                    label={t("coreSetting.addFields")}
                    name="notRequiredFieldUpdate"
                  >
                    <Select
                      mode="multiple"
                      placeholder={t("coreSetting.placeholderAddfields")}
                      optionFilterProp="label"
                      options={fieldsUpdate}
                      onChange={(e, option) =>
                        onSelectField(e, option, "update")
                      }
                    />
                  </Form.Item>

                  {notRequiredFieldsUpdate.length > 0 && (
                    <WrapFieldUpdate>
                      {notRequiredFieldsUpdate.map((item, idx) => {
                        return (
                          <FieldUpdate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </WrapFieldUpdate>
                  )}
                </MappingUpdate>
              </WrapInfo>

              <WrapInfo col={getNewToken}>
                <MappingCreate>
                  <legend>{t("coreSetting.mappingFieldsCreate")}</legend>
                  <Form.Item
                    label={t("coreSetting.selectObject")}
                    name="object"
                    rules={[
                      {
                        required: true,
                        message: t("coreSetting.messSelectObject"),
                      },
                    ]}
                  >
                    <Select
                      placeholder={t("coreSetting.selectObject")}
                      optionFilterProp="label"
                      showSearch
                      options={listObjects}
                      onChange={(e) => {
                        onSelectObject(e);
                        setObjectComponent(e);
                      }}
                    ></Select>
                  </Form.Item>

                  <Form.Item
                    label={t("coreSetting.queryKey")}
                    name="query_key"
                    rules={[
                      {
                        required: true,
                        message: t("coreSetting.messQueryKey"),
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>

                  {requiredFieldsCreate.length > 0 && (
                    <>
                      {requiredFieldsCreate.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </>
                  )}
                  <Form.Item
                    label={t("coreSetting.addFields")}
                    name="notRequiredFieldCreate"
                  >
                    <Select
                      mode="multiple"
                      placeholder={t("coreSetting.placeholderAddfields")}
                      optionFilterProp="label"
                      options={fieldsCreate}
                      onChange={(e, option) =>
                        onSelectField(e, option, "create")
                      }
                    />
                  </Form.Item>

                  {notRequiredFields.length > 0 && (
                    <WrapFieldUpdate>
                      {notRequiredFields.map((item, idx) => {
                        return (
                          <FieldCreate
                            item={item}
                            idx={idx}
                            relatedFields={relatedFields}
                            listUser={listUser}
                            onChangeValue={onChangeValue}
                          />
                        );
                      })}
                    </WrapFieldUpdate>
                  )}
                </MappingCreate>
              </WrapInfo>
            </Wrap>
            <WrapButton>
              <Button
                disabled={
                  (isEdit && !checkRule("edit")) ||
                  (!isEdit && !checkRule("create"))
                }
                type="primary"
                htmlType="submit"
              >
                {t("common.save")}
              </Button>
            </WrapButton>
          </Form>

          <ListViewCoreSetting
            selectedObject={selectedObject}
            setIsEdit={setIsEdit}
            checkRule={checkRule}
          />
        </>
      )}
    </Wrapper>
  );
}

export default CoreSetting;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-form {
    margin-right: 16px;
  }

  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.darker};
  }

  .ant-form-item {
    margin-bottom: 8px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const Wrap = styled.div`
  width: 100%;
  display: flex;
  background: #ffffff;
  justify-content: space-between;
  padding: 10px 16px 24px 16px;
  border-radius: 2px;
`;

const WrapInfo = styled.div`
  /* width: 32%; */
  /* margin-right: 16px; */
  /* border: 1px solid #ebebeb; */
  width: ${({ col }) => (col ? "24%" : "33%")};
  border-radius: 2px;
`;

const BasicInfo = styled.fieldset`
  /* height: 518px; */
  border: 1px solid #ebebeb;
  max-height: 497px;
  overflow-y: auto;
  background: #fff;
  padding: 0 16px 8px 16px;
  border-radius: 10px;

  legend {
    background: #fff;
    padding: 5px 10px;
    width: fit-content;
    color: ${(props) => props.theme.main};
    border-radius: 5px;
    font-size: 16px;
    font-family: var(--roboto-500);
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const MappingUpdate = styled(BasicInfo)``;
const MappingCreate = styled(BasicInfo)`
  margin-right: 0;
`;

const WrapField = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 8px;
  max-height: 90px;
  overflow-y: auto;
  margin-bottom: 8px;

  .ant-form-item {
    :last-child {
      margin-bottom: 0;
    }
  }
`;

const WrapFieldUpdate = styled(WrapField)`
  max-height: unset;
  .ant-form-item {
    :last-child {
      margin-bottom: 8px;
    }
  }
`;

const WrapButton = styled.div`
  margin: 16px 0;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 160px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const CustomFormItem = styled(Form.Item)`
  .ant-form-item-row {
    flex-direction: row;
    align-items: center;
    justify-content: space-between;

    .ant-form-item-label {
      padding-bottom: 0;
    }

    .ant-form-item-control {
      flex: unset;
      width: unset;
    }
  }
  .ant-switch {
    width: 54px;
  }
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const AddNew = styled.div`
  width: fit-content;
  display: flex;
  align-items: center;
  cursor: pointer;

  span {
    margin-left: 8px;
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }
`;
const WrapSession = styled.div`
  border: 1px solid #ebebeb;
  border-radius: 10px;
  padding: 8px;
  width: 100%;
  margin-bottom: 8px;
`;
