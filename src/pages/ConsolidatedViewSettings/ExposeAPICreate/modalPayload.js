import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import { CopyOutlined } from "@ant-design/icons";

import { BASE_URL_API } from "constants/constants";
import { Notification } from "components/Notification/Noti";

import { changeStateExposeCreate } from "redux/slices/consolidatedViewSettings";

function ModalPayload({
  type,
  showPayload,
  setShowPayload,
  detailsExposeAPICreate,
  setIsPayload,
  selectedObject,
}) {
  const { TextArea } = Input;
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { userDetail } = useSelector((state) => state.userReducer);

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    {
      label: "Token",
      value: "token",
    },
  ];

  const [typeAuthen, setTypeAuthen] = useState("");
  const [valuePass, $valuePass] = useState("");

  const _onClose = () => {
    setShowPayload(false);
    setIsPayload(false);

    dispatch(
      changeStateExposeCreate({
        _id: detailsExposeAPICreate._id,
        type: "payload",
      })
    );
  };

  const _onCopyPayload = () => {
    navigator.clipboard.writeText(form.getFieldValue("payload"));
    Notification("success", "Copy payload successfully!");
  };

  useEffect(() => {
    if (detailsExposeAPICreate !== null) {
      form.setFieldsValue({
        payload: JSON.stringify(detailsExposeAPICreate.payload, null, 2),
        field_return_result: detailsExposeAPICreate.field_return,
        token: detailsExposeAPICreate.token,
        userName: detailsExposeAPICreate.username,
        authenticationType: detailsExposeAPICreate.authentication_type,
        end_point: `${BASE_URL_API}expose-api/execute-${
          type === "upload_file"
            ? "upload-file"
            : type === "delete_file"
            ? "delete-file"
            : type === "view_file"
            ? "view-file"
            : type === "batch_create"
            ? "batch-create"
            : type === "batch_update"
            ? "batch-update"
            : type
        }?tenant_id=${
          userDetail.tenant_id
        }&object_id=${selectedObject}&setting_id=${detailsExposeAPICreate._id}`,
      });

      if (type !== "create" && type !== "batch_create") {
        form.setFieldsValue({
          fieldUpdateResult: detailsExposeAPICreate.key_field_update,
        });
      }
      setTypeAuthen(detailsExposeAPICreate.authentication_type);
      $valuePass(detailsExposeAPICreate.password);
    }
  }, [
    detailsExposeAPICreate,
    form,
    type,
    selectedObject,
    userDetail.tenant_id,
  ]);

  return (
    <ModalCustom
      title={
        detailsExposeAPICreate?.description
          ? detailsExposeAPICreate.description
          : ""
      }
      visible={showPayload}
      footer={null}
      width={600}
      onCancel={_onClose}
    >
      <Form
        form={form}
        onFinish={_onClose}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        colon={false}
        labelAlign="left"
      >
        {type !== "create" && type !== "batch_create" && (
          <Form.Item label="Key field update" name="fieldUpdateResult">
            <Select disabled />
          </Form.Item>
        )}
        <Form.Item label="Field return" name="field_return_result">
          <Select disabled />
        </Form.Item>
        <Form.Item label="End point" disabled name="end_point">
          <Input disabled />
        </Form.Item>
        <Form.Item label="Method" disabled>
          <Input disabled defaultValue="POST" />
        </Form.Item>
        <Form.Item label="Authentication" name="authenticationType" disabled>
          <Select disabled options={AuthenticationType}></Select>
        </Form.Item>
        {typeAuthen === "basic" ? (
          <>
            <Form.Item label="User name" name="userName">
              <Input disabled />
            </Form.Item>

            <Form.Item label="Password" name="password">
              <div>
                <Input.Password autocomplete="new-password" value={valuePass} />
              </div>
            </Form.Item>
          </>
        ) : (
          <Form.Item label="Token" name="token">
            <Input disabled />
          </Form.Item>
        )}
        <Form.Item label="Body type" disabled>
          <Input
            disabled
            defaultValue={type === "upload_file" ? "Form data" : "Raw Body"}
          />
        </Form.Item>
        <Form.Item label="Payload" disabled>
          <Form.Item style={{ marginBottom: 0 }} name="payload" disabled>
            <TextArea rows={5} disabled />
          </Form.Item>

          <CopyButton onClick={_onCopyPayload}>
            <CopyOutlined /> Copy
          </CopyButton>
        </Form.Item>
        <WrapButton>
          <Button type="primary" htmlType="submit">
            Close
          </Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default ModalPayload;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: flex-end;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;

const CopyButton = styled(Button)`
  margin-right: 16px;
  position: absolute;
  bottom: 10px;
  right: -5px;

  &.ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  &.ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;

    span {
      color: #000;
    }
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;

    span {
      color: #000;
    }
  }
`;
