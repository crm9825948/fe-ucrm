import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Config from "./config";
import ListViewExposeCreate from "./listExposeAPICreate";

import { loadDataExposeCreate } from "redux/slices/consolidatedViewSettings";
import SelectObject from "components/ExternalService/selectObject";
import { Radio } from "antd";
const options = [
  { label: "Create", value: "create" },
  { label: "Batch Create", value: "batch_create" },
];
function ExposeAPICreate() {
  const dispatch = useDispatch();
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);
  const [option, $option] = useState("create");

  const [isEdit, setIsEdit] = useState(false);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "expose_api_create" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    if (option && selectedObject) {
      dispatch(
        loadDataExposeCreate({
          object_id: selectedObject,
          page: 0,
          limit: 100000,
          expose_api_type: option,
        })
      );
    }
  }, [option, selectedObject, dispatch]);

  const _onSelectObject = (value) => {
    $selectedObject(value);
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="Expose API Create"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <>
          <Radio.Group
            options={options}
            onChange={(value) => $option(value.target.value)}
            value={option}
            optionType="button"
          />
          <Config
            selectedObject={selectedObject}
            isEdit={isEdit}
            setIsEdit={setIsEdit}
            type={option}
            checkRule={checkRule}
          />
          <ListViewExposeCreate
            selectedObject={selectedObject}
            setIsEdit={setIsEdit}
            type={option}
            checkRule={checkRule}
          />
        </>
      )}
    </Wrapper>
  );
}

export default ExposeAPICreate;

const Wrapper = styled.div`
  padding: 16px 24px;
  .ant-radio-group {
    margin-bottom: 16px;
  }
  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):hover,
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: ${(props) => props.theme.main} !important;
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):focus-within {
    box-shadow: none;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }
`;
