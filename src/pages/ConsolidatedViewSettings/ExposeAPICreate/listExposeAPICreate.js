import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";

import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";

import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";
import Details from "assets/icons/users/details.svg";
import EmptyMapping from "assets/images/consolidatedViewSetting/empty_user_mapping.png";

import {
  deleteExposeCreate,
  loadDetailsExposeCreate,
} from "redux/slices/consolidatedViewSettings";
import { setShowModalConfirmDelete } from "redux/slices/global";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalPayload from "./modalPayload";

function ListCoreSetting({ selectedObject, setIsEdit, type, checkRule }) {
  const dispatch = useDispatch();
  const { Column } = Table;

  const { listExposeAPICreate, loading, detailsExposeAPICreate } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [dataTable, setDataTable] = useState([]);
  const [dataDelete, setDataDelete] = useState({});
  const [isPayload, setIsPayload] = useState(false);
  const [showPayload, setShowPayload] = useState(false);

  const _onDeleteExposeCreate = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ _id: id });
  };

  const _onEditExposeCreate = (id) => {
    setIsEdit(true);
    dispatch(
      loadDetailsExposeCreate({
        _id: id,
      })
    );
  };

  const _onShowPayload = (id) => {
    setIsPayload(true);
    dispatch(
      loadDetailsExposeCreate({
        _id: id,
      })
    );
  };

  useEffect(() => {
    if (detailsExposeAPICreate !== null && isPayload) {
      setShowPayload(true);
    }
  }, [detailsExposeAPICreate, isPayload]);

  useEffect(() => {
    if (listExposeAPICreate !== null) {
      let tempList = [];
      listExposeAPICreate.map((item, idx) => {
        return tempList.push({
          key: item._id,
          description: item.description,
          authen_type: item.authentication_type,
          created_by: item.created_by_name,
          created_date: item.created_date,
          action: "",
        });
      });
      setDataTable(tempList);
    }
  }, [listExposeAPICreate]);

  return (
    <Wrapper>
      {dataTable.length > 0 ? (
        <Table
          pagination={false}
          dataSource={dataTable}
          scroll={{ x: "max-content" }}
        >
          <Column
            title="Description"
            dataIndex="description"
            key="description"
            width="24%"
          />
          <Column
            title="Authentication type"
            dataIndex="authen_type"
            key="authen_type"
            width="16%"
          />
          <Column
            title="Create by"
            dataIndex="created_by"
            key="created_by"
            width="24%"
          />
          <Column
            title="Create time"
            dataIndex="created_date"
            key="created_date"
            width="24%"
          />
          <Column
            title="Action"
            dataIndex="action"
            key="action"
            width="12%"
            fixed="right"
            render={(text, record) => (
              <WrapAction>
                <Tooltip title="Show payload">
                  <img
                    onClick={() => _onShowPayload(record.key)}
                    src={Details}
                    alt="details"
                  />
                </Tooltip>
                {checkRule("edit") && (
                  <Tooltip title="Edit">
                    <img
                      onClick={() => _onEditExposeCreate(record.key)}
                      src={Edit}
                      alt="edit"
                    />
                  </Tooltip>
                )}
                {checkRule("delete") && (
                  <Tooltip title="Delete">
                    <img
                      onClick={() => _onDeleteExposeCreate(record.key)}
                      src={Delete}
                      alt="delete"
                    />
                  </Tooltip>
                )}
              </WrapAction>
            )}
          />
        </Table>
      ) : (
        <Empty>
          <img src={EmptyMapping} alt="empty" />
          <p>
            There's no any <span>Expose API</span>
          </p>
        </Empty>
      )}

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteExposeCreate}
        dataDelete={dataDelete}
        isLoading={loading.exposeCreate}
      />

      <ModalPayload
        type={type}
        showPayload={showPayload}
        setShowPayload={setShowPayload}
        setIsPayload={setIsPayload}
        detailsExposeAPICreate={detailsExposeAPICreate}
        selectedObject={selectedObject}
      />
    </Wrapper>
  );
}

export default ListCoreSetting;

const Wrapper = styled.div`
  width: calc(96% + 32px);
  padding: 16px;
  background: #fff;
  border-radius: 16px;
  margin-bottom: 16px;

  .ant-table-tbody > tr > td {
    :nth-child(2) {
      text-transform: capitalize;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;
    width: 18px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
