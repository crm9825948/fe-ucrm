import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import _ from "lodash";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Checkbox from "antd/lib/checkbox";

import Field from "./field";

import {
  createExposeCreate,
  updateExposeCreate,
  loadExposeCreate,
  resetStatus,
} from "redux/slices/consolidatedViewSettings";

import { metaData } from "util/staticData";

function Config({ type, selectedObject, isEdit, setIsEdit, checkRule }) {
  const [form] = Form.useForm();
  const dispatch = useDispatch();

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { listFieldsObject } = useSelector((state) => state.reportReducer);

  const { listAllUser } = useSelector((state) => state.userReducer);

  const { statusExposeAPICreate, detailsExposeAPICreate } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [listUser, setListUser] = useState([]);
  const [relatedFields, setRelatedFields] = useState([]);
  const [optionalFields, setOptionalFields] = useState([]);
  const [keyFields, setKeyFields] = useState([]);

  const [typeAuthen, setTypeAuthen] = useState("");
  const [requiredFields, setRequiredFields] = useState([]);
  const [notRequiredFields, setNotRequiredFields] = useState([]);
  const [fileFields, $fileFields] = useState([]);

  const [mappingFieldsUpdate, setMappingFieldsUpdate] = useState({});

  const [IDEdit, setIDEdit] = useState("");

  const AuthenticationType = [
    {
      label: "Basic",
      value: "basic",
    },
    {
      label: "Token",
      value: "token",
    },
  ];

  const _onSubmit = (values) => {
    let tempFields = requiredFields.concat(notRequiredFields);
    let tempKeyFields = [];

    tempFields.forEach((item) => {
      tempKeyFields.push(item.value);
    });

    let tempMapping = Object.keys(mappingFieldsUpdate);
    let resultMapping = { ...mappingFieldsUpdate };

    tempMapping.forEach((item) => {
      if (tempKeyFields.includes(item) === false) {
        delete resultMapping[item];
      }
    });

    let data = {
      expose_api_type: type,
      object_id: selectedObject,
      authentication_type: values.authenticationType,
      owner: values.assignTo,
      description: values.description,
    };
    if (values.authenticationType === "basic") {
      data = {
        ...data,
        username: values.userName,
        password: values.password,
      };
    }

    switch (type) {
      case "create":
      case "batch_create":
        data = {
          ...data,
          fields_value: resultMapping,
          field_return: values.field_return,
          is_ic_used: values.is_ic_used ? values.is_ic_used : false,
        };
        break;

      case "update":
      case "batch_update":
        data = {
          ...data,
          key_field_update: values.fieldUpdate,
          fields_value: resultMapping,
          field_return: values.field_return,
          is_ic_used: values.is_ic_used ? values.is_ic_used : false,
        };
        break;

      case "upload_file":
        data = {
          ...data,
          key_field_update: values.fieldUpdate,
          field_file: values.field_file,
          fields_value: {},
          type_upload: "form_data",
        };
        break;

      case "view_file":
        data = {
          ...data,
          key_field_update: values.fieldUpdate,
          field_file: values.field_file,
          fields_value: {},
        };
        break;

      case "delete_file":
        data = {
          ...data,
          key_field_update: values.fieldUpdate,
          field_file: values.field_file,
          fields_value: {},
          url_file_delete: ["ex"],
        };
        break;

      default:
        break;
    }

    if (isEdit) {
      dispatch(
        updateExposeCreate({
          ...data,
          _id: IDEdit,
        })
      );
    } else {
      dispatch(
        createExposeCreate({
          ...data,
        })
      );
    }
  };

  const onSelectAuthenType = (e) => {
    setTypeAuthen(e);
  };

  const onChangeValue = (label, value) => {
    let arr = { ...mappingFieldsUpdate };
    arr[label] = value;
    setMappingFieldsUpdate(arr);
  };

  const onSelectField = (e, option) => {
    setNotRequiredFields(option);
  };

  useEffect(() => {
    let tempUser = [];
    listAllUser.forEach((item) => {
      if (item.Active) {
        tempUser.push({
          label:
            item.Middle_Name === ""
              ? item.Last_Name + " " + item.First_Name
              : item.Last_Name + " " + item.Middle_Name + " " + item.First_Name,
          value: item._id,
        });
      }
    });
    setListUser(tempUser);
  }, [listAllUser]);

  useEffect(() => {
    let tempFields = [];
    listFieldsObject.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempFields.push({
                label: field.related_name,
                value: field.field_id,
                type: field.type,
              });
            }
          });
        });
      }
    });
    setRelatedFields(tempFields);
  }, [listFieldsObject]);

  useEffect(() => {
    let tempKeyFields = [];
    let tempOptionalFields = [];
    let tempRequiredFields = [];
    let tempFileFields = [];
    fields_related.forEach((item) => {
      // tempList.push({
      //   label: item.name,
      //   value: item.ID,
      //   type: item.type,
      //   option: item.option || [],
      // });
      if (item.type !== "id" && item.type !== "formula") {
        if (item.required) {
          tempRequiredFields.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
            required: item.required,
          });
        } else {
          tempOptionalFields.push({
            label: item.name,
            value: item.ID,
            type: item.type,
            option: item.option || [],
          });
        }
      }

      if (item.type === "id") {
        tempKeyFields.push({
          label: item.name,
          value: item.ID,
          type: item.type,
          option: item.option || [],
          required: item.required,
        });
      }

      if (item.type === "file") {
        tempFileFields.push({
          label: item.name,
          value: item.ID,
          type: item.type,
          option: item.option || [],
          required: item.required,
        });
      }
    });

    tempKeyFields = tempKeyFields
      .concat(tempRequiredFields)
      .concat(tempOptionalFields)
      .concat(metaData);

    setRequiredFields(tempRequiredFields);
    setOptionalFields(tempOptionalFields);
    setKeyFields(tempKeyFields);
    $fileFields(tempFileFields);
  }, [fields_related]);

  useEffect(() => {
    if (isEdit) {
      if (detailsExposeAPICreate !== null) {
        let tempMappingUpdate = {};
        let tempNotRequiredFieldsUpdate = [];
        let tempSelectFieldsUpdate = [];

        if (
          type === "create" ||
          type === "update" ||
          type === "batch_create" ||
          type === "batch_update"
        ) {
          detailsExposeAPICreate.fields_value.forEach((item) => {
            tempMappingUpdate[item.value] = item.selected_value;
            form.setFieldsValue({
              [item.value + "Update"]:
                item.type === "date" || item.type === "datetime-local"
                  ? moment(item.selected_value)
                  : item.selected_value,
            });

            if (item.required === false) {
              tempSelectFieldsUpdate.push(item);
              tempNotRequiredFieldsUpdate.push(item.value);
            }
          });
        }

        form.setFieldsValue({
          token: detailsExposeAPICreate.token,
          authenticationType: detailsExposeAPICreate.authentication_type,
          password: detailsExposeAPICreate.password,
          userName: detailsExposeAPICreate.username,
          assignTo: detailsExposeAPICreate.owner,
          notRequiredFieldUpdate: tempNotRequiredFieldsUpdate,
          field_return: detailsExposeAPICreate.field_return,
          description: detailsExposeAPICreate.description,
          is_ic_used: detailsExposeAPICreate.is_ic_used,
          field_file: _.get(detailsExposeAPICreate, "field_file", undefined),
        });

        if (type !== "create" && type !== "batch_create") {
          form.setFieldsValue({
            fieldUpdate: detailsExposeAPICreate.key_field_update,
          });
        }

        setIDEdit(detailsExposeAPICreate._id);
        setMappingFieldsUpdate(tempMappingUpdate);
        setTypeAuthen(detailsExposeAPICreate.authentication_type);
        setNotRequiredFields(tempSelectFieldsUpdate);
      }
    }
  }, [detailsExposeAPICreate, form, isEdit, type]);

  useEffect(() => {
    if (statusExposeAPICreate === "success") {
      form.resetFields();
      setNotRequiredFields([]);
      setMappingFieldsUpdate([]);
      setTypeAuthen("");
      setIsEdit(false);
      dispatch(
        loadExposeCreate({
          object_id: selectedObject,
          page: 0,
          limit: 100000,
          expose_api_type: type,
        })
      );
      dispatch(resetStatus());
      if (type === "batch_update") {
        form.setFieldsValue({ fieldUpdate: "_id" });
      }
    }

    if (statusExposeAPICreate === "delete success") {
      form.resetFields();
      setNotRequiredFields([]);
      setMappingFieldsUpdate([]);
      setTypeAuthen("");
      setIsEdit(false);
      dispatch(resetStatus());
      if (type === "batch_update") {
        form.setFieldsValue({ fieldUpdate: "_id" });
      }
    }
  }, [dispatch, form, selectedObject, type, setIsEdit, statusExposeAPICreate]);

  useEffect(() => {
    form.resetFields();
    setNotRequiredFields([]);
    setMappingFieldsUpdate([]);
    setTypeAuthen("");
    setIsEdit(false);
    if (type === "batch_update") {
      form.setFieldsValue({ fieldUpdate: "_id" });
    }
  }, [selectedObject, form, setIsEdit, type]);

  return (
    <Form
      form={form}
      onFinish={_onSubmit}
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      colon={false}
      labelAlign="left"
    >
      <Wrap>
        <BasicInfo>
          {type !== "create" && type !== "batch_create" && (
            <Form.Item
              label="Key field update"
              name="fieldUpdate"
              rules={[
                {
                  required: true,
                  message: "Please select key field update!",
                },
              ]}
            >
              <Select
                disabled={type === "batch_update"}
                placeholder="Select key field update"
                options={
                  type === "update" || type === "batch_update"
                    ? keyFields
                    : keyFields?.filter((item) => item?.type === "id")
                }
                optionFilterProp="label"
                showSearch
              />
            </Form.Item>
          )}
          <Form.Item
            label="Description"
            name="description"
            rules={[{ required: true, message: "Please input description!" }]}
          >
            <Input />
          </Form.Item>

          {type !== "upload_file" &&
            type !== "delete_file" &&
            type !== "view_file" && (
              <>
                <Form.Item label="Field return" name="field_return">
                  <Select
                    placeholder="Select field return"
                    options={keyFields}
                    optionFilterProp="label"
                    showSearch
                  />
                </Form.Item>
              </>
            )}

          <Form.Item
            label="Authentication type"
            name="authenticationType"
            rules={[
              {
                required: true,
                message: "Please select authentication type!",
              },
            ]}
          >
            <Select
              placeholder="Select authentication type"
              allowClear
              options={AuthenticationType}
              onChange={(e) => onSelectAuthenType(e)}
            ></Select>
          </Form.Item>

          {typeAuthen === "basic" && (
            <>
              <Form.Item
                label="User name"
                name="userName"
                rules={[{ required: true, message: "Please input user name!" }]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[{ required: true, message: "Please input password!" }]}
              >
                <Input.Password />
              </Form.Item>
            </>
          )}

          <Form.Item label="Assign to" name="assignTo">
            <Input />
          </Form.Item>

          {(type === "upload_file" ||
            type === "delete_file" ||
            type === "view_file") && (
            <Form.Item
              label="Field file"
              name="field_file"
              rules={[
                {
                  required: true,
                  message: "Please select field file!",
                },
              ]}
            >
              <Select
                placeholder="Select field file"
                options={fileFields}
                // onChange={(e) => onSelectAuthenType(e)}
              />
            </Form.Item>
          )}

          {type !== "upload_file" &&
            type !== "delete_file" &&
            type !== "view_file" && (
              <Form.Item
                label="Is IC use"
                name="is_ic_used"
                valuePropName="checked"
              >
                <Checkbox />
              </Form.Item>
            )}

          {type !== "upload_file" &&
            type !== "delete_file" &&
            type !== "view_file" && (
              <>
                {requiredFields.length > 0 && (
                  <>
                    {requiredFields.map((item) => {
                      return (
                        <Field
                          item={item}
                          relatedFields={relatedFields}
                          listUser={listUser}
                          onChangeValue={onChangeValue}
                        />
                      );
                    })}
                  </>
                )}
              </>
            )}
        </BasicInfo>

        {type !== "upload_file" &&
          type !== "delete_file" &&
          type !== "view_file" && (
            <WrapAddFields>
              <Form.Item label="Add fields" name="notRequiredFieldUpdate">
                <Select
                  mode="multiple"
                  placeholder="Select field"
                  allowClear
                  options={optionalFields}
                  onChange={(e, option) => onSelectField(e, option)}
                  optionFilterProp="label"
                />
              </Form.Item>

              {notRequiredFields.length > 0 && (
                <WrapField>
                  {notRequiredFields.map((item, idx) => {
                    return (
                      <Field
                        item={item}
                        idx={idx}
                        relatedFields={relatedFields}
                        listUser={listUser}
                        onChangeValue={onChangeValue}
                      />
                    );
                  })}
                </WrapField>
              )}
            </WrapAddFields>
          )}
      </Wrap>
      <WrapButton>
        <Button
          disabled={
            (isEdit && !checkRule("edit")) || (!isEdit && !checkRule("create"))
          }
          type="primary"
          htmlType="submit"
        >
          {isEdit ? <span>Update</span> : <span>Save</span>}
        </Button>
      </WrapButton>
    </Form>
  );
}

export default Config;

const Wrap = styled.div`
  display: flex;
  background: #fff;
  padding: 16px;
  border-radius: 10px;
  margin-right: 24px;

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const BasicInfo = styled.div`
  flex: 1;
  padding-right: 4px;
  max-height: 518px;
  overflow-y: auto;
  margin-left: 8px;
  .ant-form-item {
    margin-bottom: 8px;
  }
`;

const WrapAddFields = styled.div`
  flex: 1;
  max-height: 518px;
  overflow-y: auto;
  margin-left: 40px;
  .ant-form-item {
    margin-bottom: 8px;
  }
`;

const WrapField = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 8px;
  overflow-y: auto;
  margin-bottom: 8px;
`;

const WrapButton = styled.div`
  margin: 16px 0;
  display: flex;
  justify-content: center;

  .ant-btn-primary {
    width: 160px;
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;
