import React from "react";
import styled from "styled-components/macro";

import Form from "antd/lib/form";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import DatePicker from "antd/lib/date-picker";

function Field({ item, relatedFields, listUser, onChangeValue }) {
  return (
    <Wrapper>
      {item.type === "select" ||
      item.type === "dynamic-field" ||
      item.type === "linkingobject" ||
      item.type === "bridging-field" ? (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            {
              required: item.required,
              message: `Please select ${item.label}!`,
            },
          ]}
        >
          <Select
            placeholder={`Select ${item.label}`}
            allowClear
            options={
              item.type === "linkingobject" || item.type === "bridging-field"
                ? relatedFields
                : item.option
            }
            onChange={(e) => onChangeValue(item.value, e)}
          ></Select>
        </Form.Item>
      ) : item.type === "number" ? (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            { required: item.required, message: `Please input ${item.label}!` },
          ]}
        >
          <InputNumber
            placeholder={`Input ${item.label}`}
            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
            onChange={(e) => onChangeValue(item.value, e)}
          />
        </Form.Item>
      ) : item.type === "date" ? (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            {
              required: item.required,
              message: `Please select ${item.label}!`,
            },
          ]}
        >
          <DatePicker
            placeholder={`Select ${item.label}`}
            onChange={(date, dateString) =>
              onChangeValue(item.value, dateString)
            }
          />
        </Form.Item>
      ) : item.type === "datetime-local" ? (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            {
              required: item.required,
              message: `Please select ${item.label}!`,
            },
          ]}
        >
          <DatePicker
            placeholder={`Select ${item.label}`}
            showTime
            onChange={(date, dateString) =>
              onChangeValue(item.value, dateString)
            }
          />
        </Form.Item>
      ) : (
        <Form.Item
          label={item.label}
          name={`${item.value}Update`}
          rules={[
            { required: item.required, message: `Please input ${item.label}!` },
          ]}
        >
          <Input
            placeholder={`Input ${item.label}`}
            onChange={(e) => onChangeValue(item.value, e.target.value)}
          />
        </Form.Item>
      )}
    </Wrapper>
  );
}

export default Field;

const Wrapper = styled.div`
  .ant-form-item {
    margin-bottom: 8px;
  }

  .ant-input-number {
    width: 100%;
  }

  .ant-picker {
    width: 100%;
  }
`;
