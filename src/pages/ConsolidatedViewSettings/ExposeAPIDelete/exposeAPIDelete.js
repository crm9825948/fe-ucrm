import { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Config from "pages/ConsolidatedViewSettings/ExposeAPICreate/config";
import ListViewExposeCreate from "pages/ConsolidatedViewSettings/ExposeAPICreate/listExposeAPICreate";

import { loadDataExposeCreate } from "redux/slices/consolidatedViewSettings";
import SelectObject from "components/ExternalService/selectObject";

function ExposeAPICreate() {
  const dispatch = useDispatch();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const [isEdit, setIsEdit] = useState(false);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "expose_api_delete_file" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);
    dispatch(
      loadDataExposeCreate({
        object_id: value,
        page: 0,
        limit: 100000,
        expose_api_type: "delete_file",
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  return (
    <Wrapper>
      <SelectObject
        nameBreadcrumb="Expose API Delete File"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />
      {selectedObject && (
        <>
          <Config
            type="delete_file"
            selectedObject={selectedObject}
            isEdit={isEdit}
            setIsEdit={setIsEdit}
            checkRule={checkRule}
          />
          <ListViewExposeCreate
            type="delete_file"
            selectedObject={selectedObject}
            setIsEdit={setIsEdit}
            checkRule={checkRule}
          />
        </>
      )}
    </Wrapper>
  );
}

export default ExposeAPICreate;

const Wrapper = styled.div`
  padding: 16px 24px;
`;
