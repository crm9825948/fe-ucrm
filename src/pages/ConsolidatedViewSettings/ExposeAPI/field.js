import { Input, InputNumber, Select } from "antd";
import React, { useEffect } from "react";
import deleteImg from "assets/icons/common/delete-icon.png";
import styled from "styled-components";
import { useState } from "react";

const { Option } = Select;

const Field = ({
  index,
  listFields,
  setListFields,
  optionFields,
  listValue,
  setListValue,
}) => {
  const [type, setType] = useState("text");
  const [fullValue, setFullValue] = useState({});

  useEffect(() => {
    optionFields.map((item, idx) => {
      if (listFields[index].id_field === item.value.ID) {
        setFullValue(item.value);
        setType(item.value.type);
      }
      return null;
    });
  }, [listFields, index, optionFields]);

  return (
    <Wrapper>
      <div className="col-1 col">
        <div className="title">Field</div>
        <div>
          <Select
            showSearch
            placeholder="Please select"
            style={{ width: "200px" }}
            value={listFields[index]?.id_field}
            onChange={(e, value) => {
              setType(value.fullValue.type);
              setFullValue(value.fullValue);

              let temp = [...listFields];
              temp[index] = {
                id_field: e,
                value: "",
              };
              setListFields(temp);
            }}
          >
            {optionFields.map((item, idx) => {
              return (
                <Option value={item.value.ID} fullValue={item.value}>
                  {item.label}
                </Option>
              );
            })}
          </Select>
        </div>
      </div>
      <div className="col-1 col-2">
        <div className="title">Default value</div>
        <div>
          {type === "text" || type === "textarea" ? (
            <Input
              onChange={(e) => {
                let temp = [...listFields];
                temp[index].value = e.target.value;
                setListFields(temp);
              }}
              value={listFields[index]?.value}
              style={{ width: "200px" }}
            />
          ) : type === "email" ? (
            <Input
              onChange={(e) => {
                let temp = [...listFields];
                temp[index].value = e.target.value;
                setListFields(temp);
              }}
              value={listFields[index]?.value}
              style={{ width: "200px" }}
            />
          ) : type === "number" ? (
            <InputNumber
              onChange={(e) => {
                let temp = [...listFields];
                temp[index].value = e;
                setListFields(temp);
              }}
              value={listFields[index]?.value}
              style={{ width: "200px" }}
            />
          ) : type === "select" ? (
            <Select
              options={fullValue.option}
              style={{ width: "200px" }}
              onChange={(e) => {
                let temp = [...listFields];
                temp[index].value = e;
                setListFields(temp);
              }}
              value={listFields[index]?.value}
            />
          ) : (
            ""
          )}
        </div>
      </div>
      <div>
        <img
          alt=""
          src={deleteImg}
          onClick={() => {
            let temp = [...listFields];
            temp.splice(index, 1);
            setListFields(temp);
          }}
        />
      </div>
    </Wrapper>
  );
};

export default Field;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  margin-top: 16px;
  .title {
    font-style: normal;
    font-weight: bold;
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */
    color: #181818;
    margin-right: 16px;
  }
  img {
    width: 20px;
  }
  .col-1 {
    display: flex;
    align-items: center;
  }
  .col-2 {
    margin-left: 16px;
  }
`;
