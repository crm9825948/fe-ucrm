import { useState, useEffect, useRef } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import _ from "lodash";

import Table from "antd/lib/table";
import Typography from "antd/lib/typography";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import Tooltip from "antd/lib/tooltip";
import Button from "antd/lib/button";
import { Popover, Form, Input } from "antd";
import RouterPrompt from "components/Modal/ModalRouterPrompt";

import { Notification } from "components/Notification/Noti";

import Copy from "assets/icons/common/copy.svg";
import SettingImg from "assets/icons/users/Permission.svg";

import {
  resetStatus,
  loadAllExpose,
  updateExpose,
} from "redux/slices/consolidatedViewSettings";
import { BASE_URL_API } from "constants/constants";
import SelectObject from "components/ExternalService/selectObject";
import Field from "./field";

const { Option } = Select;

function ExposeAPI() {
  const dispatch = useDispatch();
  const { Column } = Table;
  const { Text } = Typography;

  const { fields_related } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );

  const { allExpose, statusUpdateExpose } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  // const { userInfo } = useSelector((state) => state.authenticatedReducer);
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const [selectedObject, $selectedObject] = useState(undefined);
  const [listObjects, $listObjects] = useState([]);

  const [dataTable, setDataTable] = useState([]);
  const [objectFields, setObjectFields] = useState([]);
  const [dataExpose, setDataExpose] = useState([]);
  const [change, setChange] = useState(false);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "expose_api_check" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const onSelectValue = (value, key, type) => {
    setChange(true);

    let tempExpose = [];
    if (dataExpose !== null) {
      tempExpose = [...dataExpose];
    }

    let indexExist = tempExpose.findIndex((item) => item.filterField === key);

    if (indexExist !== -1) {
      switch (type) {
        case "value":
          if (value) {
            tempExpose[indexExist] = {
              ...tempExpose[indexExist],
              showField: value,
            };
          } else {
            tempExpose.splice(indexExist, 1);
          }
          break;
        case "json":
          tempExpose[indexExist] = {
            ...tempExpose[indexExist],
            is_json: value,
          };
          break;
        case "details":
          tempExpose[indexExist] = {
            ...tempExpose[indexExist],
            is_get_details: value,
          };
          break;
        default:
          break;
      }
    } else {
      tempExpose.push({
        filterField: key,
        is_get_details: false,
        is_json: false,
        showField: value,
      });
    }

    setDataExpose(tempExpose);
  };

  const _onSave = () => {
    dispatch(
      updateExpose({
        fields: dataExpose,
        object_id: selectedObject,
      })
    );
  };

  const _onCopy = (URL) => {
    navigator.clipboard.writeText(URL);
    Notification("success", "Copy successfully!");
  };

  const _onSelectObject = (value) => {
    $selectedObject(value);
    dispatch(
      loadAllExpose({
        object_id: value,
      })
    );
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      $listObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    setDataExpose(allExpose);
  }, [allExpose]);

  const [optionFields, setOptionFields] = useState([]);
  const [listValue, setListValue] = useState({});

  useEffect(() => {
    console.log(listValue);
  }, [listValue]);

  useEffect(() => {
    let tempList = [];
    let tmp = [];
    fields_related.map((item) => {
      if (
        item.type === "text" ||
        item.type === "textarea" ||
        item.type === "select" ||
        item.type === "number" ||
        item.type === "email" ||
        item.type === "select"
      ) {
        tmp.push({
          label: item.name,
          value: item,
        });
      }

      return tempList.push({
        label: item.name,
        value: item.ID,
      });
    });
    setOptionFields(tmp);
    setObjectFields(tempList);
  }, [fields_related]);

  useEffect(() => {
    let tempList = [];
    console.log(fields_related, dataExpose);
    fields_related.map((item) => {
      return tempList.push({
        key: item.ID,
        name: item.name,
        URL: `${BASE_URL_API}expose-api/load-data?tenant_id=${userDetail.tenant_id}&object_id=${selectedObject}&field_id=${item.ID}&value=`,
        valueReturn: undefined,
        returnJSON: false,
        getDetails: false,
        copy: "",
      });
    });

    if (dataExpose !== null) {
      dataExpose.forEach((item) => {
        const fieldIndex = tempList.findIndex(
          (field) => field.key === item.filterField
        );

        if (fieldIndex !== -1) {
          let valueField = "";

          if (
            item &&
            item.add_field &&
            item.add_field !== null &&
            Object.keys(item.add_field).length > 0
          ) {
            Object.entries(item.add_field).map(([key, value], index) => {
              return (valueField = valueField + `&${key}=${value}`);
            });
          }
          console.log(valueField);
          tempList[fieldIndex] = {
            ...tempList[fieldIndex],
            valueReturn: item.showField,
            returnJSON: item.is_json,
            getDetails: item.is_get_details,
            URL:
              item &&
              item.add_field &&
              item.add_field !== null &&
              item.is_get_details === false
                ? `${BASE_URL_API}expose-api/load-data?tenant_id=${userDetail.tenant_id}&object_id=${selectedObject}&field_id=${tempList[fieldIndex].key}&value=${valueField}`
                : item.is_get_details
                ? `${BASE_URL_API}expose-api/load-data?tenant_id=${userDetail.tenant_id}&object_id=${selectedObject}&field_id=${tempList[fieldIndex].key}&is_get_details=true&value=${valueField}`
                : tempList[fieldIndex].URL,
            ...item,
          };
        }
      });
    }
    setDataTable(tempList);
  }, [dataExpose, fields_related, selectedObject, userDetail.tenant_id]);

  useEffect(() => {
    if (statusUpdateExpose === "success") {
      setChange(false);
      dispatch(resetStatus());
    }
  }, [dispatch, statusUpdateExpose]);

  const [form] = Form.useForm();
  const [type, setType] = useState("");
  const [filterField, setFilterField] = useState("");
  const [openPopover, setOpenPopover] = useState(false);
  const [listFields, setListFields] = useState([]);

  const onFinish = (values) => {
    let tmp = [...dataExpose];
    let flag = false;
    let flagNoti = false;
    let addField = {};

    if (
      listFields.length > 0 &&
      listFields.findIndex((item) => item.id_field === "") === -1 &&
      listFields.findIndex((item) => item.value === "") === -1
    ) {
      listFields.map((item, idx) => {
        addField[item.id_field] = item.value;
        return null;
      });
      flagNoti = true;
    }

    if (listFields.length > 0) {
      listFields.map((item, idx) => {
        if (Object.keys(item).length === 0) {
          flagNoti = false;
        }
        return null;
      });
    }

    if (flagNoti === false && listFields.length > 0) {
      setOpenPopover(true);
      Notification("error", "Please fullfill required field!");
    } else {
      tmp.forEach((item, idx) => {
        if (item.filterField === filterField) {
          tmp[idx] = {
            ...item,
            ...values,
            expose_api_type: "check",
            add_field: { ...addField },
          };
          flag = true;
        }
        if (flag === false) {
          tmp.push();
        }
        return null;
      });

      dispatch(
        updateExpose({
          fields: tmp,
          object_id: selectedObject,
        })
      );
      form.resetFields();
      setOpenPopover(false);
    }
  };

  const popoverRef = useRef(null);
  console.log("listField", listFields);
  const content = (
    <Form
      name="basic"
      style={{
        maxWidth: 600,
      }}
      layout="vertical"
      form={form}
      onFinish={onFinish}
      // onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item
        label="Authentication type"
        name="authentication_type"
        rules={[
          {
            required: true,
            message: "Please select your authentication type!",
          },
        ]}
      >
        <Select
          onChange={(e) => {
            setType(e);
          }}
          style={{ width: "100%" }}
        >
          <Option value="none">None</Option>
          <Option value="token">Token</Option>
          <Option value="basic">Basic</Option>
        </Select>
      </Form.Item>

      {type === "basic" ? (
        <>
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: "Please input username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
        </>
      ) : (
        ""
      )}

      {listFields.length >= 5 ? (
        ""
      ) : (
        <button
          type="button"
          onClick={() => {
            let tmp = [...listFields];
            tmp.push({});
            setListFields(tmp);
          }}
          className="add-btn"
          style={{
            border: "none",
            backgroundColor: "transparent",
            fontWeight: "400",
            fontSize: "16px",
            lineHeight: "22px",
            /* identical to box height, or 138% */
            textAlign: "center",
            /* Primary/6 - Primary Main */
            color: "#20A2A2",
          }}
        >
          + Add input field
        </button>
      )}
      {listFields.map((item, idx) => {
        return (
          <Field
            item={item}
            key={idx}
            index={idx}
            listFields={listFields}
            setListFields={setListFields}
            optionFields={optionFields}
            listValue={listValue}
            setListValue={setListValue}
          />
        );
      })}

      <Form.Item>
        <Button
          type="primary"
          htmlType="submit"
          onClick={() => {
            // popoverRef.current.setVisible(false);
            setOpenPopover(false);
          }}
          style={{ marginTop: "16px" }}
        >
          Submit
        </Button>
        {form.getFieldValue("token") &&
        form.getFieldValue("authentication_type") === "token" ? (
          <Button
            type="primary"
            style={{ marginLeft: "8px" }}
            onClick={() => {
              navigator.clipboard
                .writeText(form.getFieldValue("token"))
                .then(() => {
                  Notification("success", "Copy successfully!");
                })
                .catch((err) => {
                  Notification("error", "Copy error!");
                });
              setOpenPopover(false);
            }}
          >
            Copy token
          </Button>
        ) : (
          ""
        )}
      </Form.Item>
    </Form>
  );

  console.log(form.getFieldsValue());

  return (
    <Wrapper>
      <RouterPrompt isBlocking={change} />

      <SelectObject
        nameBreadcrumb="Expose API check"
        onChange={_onSelectObject}
        listObjects={listObjects}
      />

      {selectedObject && (
        <Wrap>
          {change && <SaveButton onClick={_onSave}>Save</SaveButton>}

          <Table
            pagination={false}
            dataSource={dataTable}
            scroll={{ x: "max-content" }}
          >
            <Column
              title="Name"
              dataIndex="name"
              key="name"
              width="205px"
              render={(name) => (
                <Text style={{ width: "205px" }} ellipsis={{ tooltip: name }}>
                  {name}
                </Text>
              )}
            />
            <Column
              title="URL"
              dataIndex="URL"
              key="URL"
              width="552px"
              render={(text) => (
                <Text style={{ width: "552px" }} ellipsis={{ tooltip: text }}>
                  {text}
                </Text>
              )}
            />
            <Column
              title="Value return"
              dataIndex="valueReturn"
              key="valueReturn"
              width="332px"
              render={(value, record) => (
                <Select
                  allowClear
                  optionFilterProp="label"
                  showSearch
                  placeholder="Please select"
                  options={objectFields}
                  value={value}
                  onChange={(e) => onSelectValue(e, record.key, "value")}
                  disabled={!checkRule("edit")}
                />
              )}
            />
            <Column
              title="Return JSON"
              dataIndex="returnJSON"
              key="returnJSON"
              width="130px"
              render={(value, record) => (
                <>
                  {record.valueReturn && checkRule("edit") ? (
                    <Checkbox
                      checked={value}
                      onChange={(e) =>
                        onSelectValue(e.target.checked, record.key, "json")
                      }
                    />
                  ) : (
                    <Tooltip title="Please select value return first!">
                      <Checkbox disabled />
                    </Tooltip>
                  )}
                </>
              )}
            />
            <Column
              title="Get details"
              dataIndex="getDetails"
              key="getDetails"
              width="112px"
              render={(value, record) => (
                <>
                  {record.valueReturn && checkRule("edit") ? (
                    <Checkbox
                      checked={value}
                      onChange={(e) =>
                        onSelectValue(e.target.checked, record.key, "details")
                      }
                    />
                  ) : (
                    <Tooltip title="Please select value return first!">
                      <Checkbox disabled />
                    </Tooltip>
                  )}
                </>
              )}
            />
            <Column
              title="Copy URL"
              dataIndex="copy"
              key="copy"
              width="107px"
              fixed="right"
              render={(text, record) => (
                <WrapAction>
                  <Tooltip title="Copy URL">
                    <img
                      onClick={() => _onCopy(record.URL)}
                      src={Copy}
                      alt="copy"
                    />
                  </Tooltip>
                </WrapAction>
              )}
            />
            <Column
              title="Authen"
              dataIndex="authen"
              key="authen"
              width="107px"
              fixed="right"
              render={(text, record) => (
                <WrapAction>
                  {/* <Tooltip title="Copy URL"> */}
                  {record.valueReturn && checkRule("edit") ? (
                    <Popover
                      content={content}
                      title="Authen"
                      placement="left"
                      trigger="click"
                      ref={popoverRef}
                      open={record.key === filterField ? openPopover : false}
                      onOpenChange={(e) => {
                        if (e === false) {
                          setType("");
                          form.resetFields();
                          setFilterField("");
                        }
                        if (e) {
                          console.log(record);
                          setFilterField(record.key);
                          setOpenPopover(true);
                          setType(record.authentication_type);
                          form.setFieldsValue({ ...record });

                          let temp = [];
                          if (
                            record &&
                            record.add_field &&
                            record.add_field !== null
                          ) {
                            Object.entries(record.add_field).map(
                              ([key, value], index) => {
                                temp.push({
                                  id_field: key,
                                  value: value,
                                });
                                return null;
                              }
                            );
                          }
                          setListFields(temp);
                        }
                      }}
                    >
                      <img src={SettingImg} alt="Authen" />
                    </Popover>
                  ) : (
                    ""
                  )}

                  {/* </Tooltip> */}
                </WrapAction>
              )}
            />
          </Table>
        </Wrap>
      )}
    </Wrapper>
  );
}

export default ExposeAPI;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    .ant-select {
      width: 100%;
    }

    .ant-checkbox-wrapper {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .ant-tooltip-disabled-compatible-wrapper {
      display: flex !important;
      justify-content: center;
      align-items: center;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const Wrap = styled.div`
  background: #fff;
  border-radius: 10px;
  height: fit-content;
`;

const SaveButton = styled(Button)`
  border: 1px solid ${(props) => props.theme.main};
  margin: 16px 0 16px 32px;

  span {
    color: ${(props) => props.theme.main};
    font-size: 16px;
  }

  &.ant-btn:active {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    span {
      color: ${(props) => props.theme.main};
    }
  }

  &.ant-btn:focus {
    background: #fff;
    border: 1px solid ${(props) => props.theme.main};
    span {
      color: ${(props) => props.theme.main};
    }
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border: 1px solid ${(props) => props.theme.darker} !important;

    span {
      color: #fff;
    }
  }
`;

const WrapAction = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
