import { Button, Col, Form, Input, Row } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import backgroundImage from "../../assets/images/login/login-bg.webp";
import loginImage from "../../assets/images/login/logo.jpg";
import welcomeBack from "../../assets/images/login/welcome-back.jpg";
import "../../index.scss";
import { login, resetLoginStatus } from "../../redux/slices/authenticated";
import AuthService from "../../services/AuthService";
import { USE_SSO } from "constants/constants";
import { useNavigate } from "react-router";
import LogoCRM from "../../assets/images/login/LogoCRM.png";
import SecurityImg from "../../assets/images/login/security-safe.svg";

const Login = (props) => {
  const dispatch = useDispatch();
  const { isLoading, loginStatus } = useSelector(
    (state) => state.authenticatedReducer
  );
  const [isLogin, setIsLogin] = useState(false);
  const navigate = useNavigate();
  const onFinish = (e) => {
    dispatch(
      login({
        email: e.email.trim(),
        pass: e.pass,
      })
    );
  };

  useEffect(() => {
    if (isLoading === false) {
      if (loginStatus === "success") {
        // Notification("success", "Login successfully!");
        dispatch(resetLoginStatus());
      } else if (loginStatus !== null) {
        // Notification("error", loginStatus);
        dispatch(resetLoginStatus());
      }
    }
  }, [dispatch, isLoading, loginStatus]);

  return (
    <PageWrapper>
      <ContentWrapper>
        <Row>
          <Col span={10}>
            <img
              src={welcomeBack}
              alt="welcome-back"
              className="welcome_back"
            />
          </Col>
          <Col span={14}>
            {!isLogin ? (
              <PolicyWrapper>
                <img src={LogoCRM} alt="logo" />
                <Welcome>
                  Welcome to <span>Unify CRM</span>
                </Welcome>
                <CustomText>Login with your account to continue</CustomText>
                <Button
                  type="primary"
                  style={{
                    backgroundColor: "#20a2a2",
                    borderColor: "#20a2a2",
                    margin: "16px 0px",
                  }}
                  onClick={() => {
                    // if (
                    //   USE_SSO === "true" &&
                    //   !window.location.pathname.includes("sign-in")
                    // ) {
                    //   if (AuthService) {
                    //     AuthService.login();
                    //   }
                    // } else {
                    setIsLogin(true);
                    // }
                  }}
                >
                  Login
                </Button>
                <CustomText
                  style={{ cursor: "pointer" }}
                  onClick={() => {
                    navigate("/policies/privacy-policy");
                  }}
                >
                  <img src={SecurityImg} alt="Seculity" />
                  Privacy Policy
                </CustomText>
              </PolicyWrapper>
            ) : (
              <FormWrapper>
                <img src={loginImage} alt="" className="logo" />
                <div className="login_title">LOGIN</div>
                <FormCustom
                  name="basic"
                  // labelCol={{ span: 8 }}
                  // wrapperCol={{ span: 8 }}
                  initialValues={{ remember: true }}
                  onFinish={(e) => onFinish(e)}
                  //   onFinishFailed={onFinishFailed}
                  layout="vertical"
                  autoComplete="off"
                >
                  <Form.Item
                    // label="Email"
                    name="email"
                    rules={[
                      {
                        required: true,
                        message: "Please input your username!",
                      },
                      {
                        type: "email",
                        message: "Please input valid username!",
                      },
                    ]}
                  >
                    <Input
                      placeholder="Username"
                      size="large"
                      className="input__username"
                      type={"email"}
                    />
                  </Form.Item>

                  <Form.Item
                    // label="Password"
                    name="pass"
                    rules={[
                      {
                        required: true,
                        message: "Please input your password!",
                      },
                    ]}
                  >
                    <Input.Password
                      placeholder="Password"
                      size="large"
                      autoComplete="on"
                    />
                  </Form.Item>
                  {/* <div
                  className="remember_me__container"
                  style={{ display: "block" }}
                >
                  <Form.Item name="remember" valuePropName="checked">
                    <Checkbox>Remember me</Checkbox>
                  </Form.Item>
                </div> */}
                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      // color="#20A2A2"
                      style={{
                        backgroundColor: "#20a2a2",
                        borderColor: "#20a2a2",
                      }}
                      loading={isLoading}
                    >
                      Login
                    </Button>
                  </Form.Item>
                  {USE_SSO === "true" ? (
                    <Form.Item>
                      <Button
                        // type="primary"
                        // color="#20A2A2"
                        // style={{
                        //   backgroundColor: "white",
                        //   color: "#20a2a2",
                        //   borderColor: "#20a2a2",
                        // }}
                        className="login-sso"
                        loading={isLoading}
                        onClick={() => {
                          if (AuthService) {
                            AuthService.login();
                          }
                        }}
                      >
                        Login with SSO
                      </Button>
                    </Form.Item>
                  ) : (
                    ""
                  )}
                  <div
                    className="forgot-password"
                    onClick={() => {
                      navigate("/forgot-password");
                    }}
                  >
                    Forgot password?
                  </div>
                </FormCustom>
              </FormWrapper>
            )}
          </Col>
        </Row>
      </ContentWrapper>
    </PageWrapper>
  );
};

const PageWrapper = styled.div`
  height: 100vh;
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  .forgot-password {
    color: #20a2a2;
    cursor: pointer;
    :hover {
      text-decoration: underline;
    }
  }
`;

const ContentWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 926px;
  height: 500px;
  background-color: #fff;
  /* display: flex;
  justify-content: flex-start;
  align-items: center; */
  .welcome_back {
    width: 386px;
  }

  @media screen and (max-width: 1199px) {
    width: 694.5px;
    height: fit-content;
    .welcome_back {
      width: 289.5px;
    }
  }
`;

const FormWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  .logo {
    width: 185px;
    height: 72px;
    margin-top: 38px;
    margin-bottom: 24px;
  }
  .login_title {
    font-family: var(--roboto-700);
    font-size: 28px;
    line-height: 33px;
    /* identical to box height */

    text-align: center;
    letter-spacing: 0.04em;
    font-variant: small-caps;
    margin-bottom: 16px;
    /* text - main */

    color: #545454;
  }
  .ant-form {
    width: 466px;
  }
  .input__username {
    margin-bottom: 6px;
  }
  .remember_me__container {
    display: flex;
    justify-content: flex-start;
    width: 100%;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* Character / Title .85 */

    color: rgba(0, 0, 0, 0.85);
    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
  .ant-btn-primary {
    width: 100%;
    border-color: ${(props) => props.theme.main};
    background-color: ${(props) => props.theme.main};
    color: #ffffff;
  }
  .ant-btn {
    height: 42px;
    width: 100%;
    font-family: var(--roboto-700);
    font-size: 16px;
  }
  .login-sso {
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
    :hover {
      color: #ffffff;
      background-color: #1c9292;
      border-color: #1c9292;
    }
  }

  @media screen and (max-width: 1199px) {
    .logo {
      width: 138.75px;
      height: 54px;
      margin-top: 0px;
      margin-bottom: 10px;
    }
    .login_title {
      font-family: var(--roboto-700);
      font-size: 21px;
      line-height: 24.5px;
      /* identical to box height */

      text-align: center;
      letter-spacing: 0.04em;
      font-variant: small-caps;
      margin-bottom: 12px;
      /* text - main */

      color: #545454;
    }
    .ant-form {
      width: 349.5px;
    }
    .input__username {
      margin-bottom: 0px;
    }
    .remember_me__container {
      display: flex;
      justify-content: flex-start;
      width: 100%;
      font-family: var(--roboto-400);
      font-size: 12px;
      line-height: 16.5px;
      margin-bottom: 0;
      /* identical to box height, or 137% */

      /* Character / Title .85 */

      color: rgba(0, 0, 0, 0.85);
      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.main};
      }
    }
    .ant-btn-primary {
      width: 100%;
      border-color: ${(props) => props.theme.main};
      background-color: ${(props) => props.theme.main};
    }
    .ant-btn {
      height: 36px;
    }
  }
`;
const PolicyWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding: 111px 37px 0px 37px;
  @media screen and (max-width: 1199px) {
    padding: 62px 37px 0px 37px;
  }

  .ant-btn-primary {
    width: 100%;
    border-color: ${(props) => props.theme.main};
    background-color: ${(props) => props.theme.main};
    color: #ffffff;
  }
  .ant-btn {
    height: 42px;
    width: 100%;
    font-family: var(--roboto-700);
    font-size: 16px;
  }
`;

const Welcome = styled.div`
  font-family: var(--roboto-500);
  font-size: 28px;
  line-height: 33px;
  color: #2c2c2c;
  margin: 24px 0px 16px 0px;
  span {
    color: #1c9292;
  }
`;
const CustomText = styled.span`
  font-size: 16px;
  line-height: 19px;
  color: #6b6b6b;
  display: flex;
  align-items: center;
  svg {
    margin-right: 8px;
  }
`;

const FormCustom = styled(Form)``;

export default Login;
