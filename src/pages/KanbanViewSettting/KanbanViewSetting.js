import {
  Row,
  Col,
  Button,
  Table,
  Input,
  Space,
  Modal,
  Steps,
  Form,
  Select,
  Dropdown,
  Menu,
  Checkbox,
  Breadcrumb,
} from "antd";
import { SearchOutlined } from "@ant-design/icons";
import React, { useEffect, useState } from "react";
import {
  getListKanbanView,
  setShowModal,
  updateColorColumnConfig,
  updateFieldKanbanView,
  updateDisplayFields,
  addDisplayField,
  updateColorStatus,
  addDisplayFieldUncategorized,
  updateColorUncategorized,
  getKanViewSettingByIdSuccess,
  saveKanbanViewSetting,
  updatePositionField,
  updatePositionFieldUncategorized,
  unmountKanbanViewSetting,
  getKanbanViewSettingById,
  deleteKanbanViewSetting,
  deleteDisplayField,
  deleteDisplayFieldUncategorized,
  updateDisplayFieldUncategorized,
  setStep,
  updateKanbanViewSetting,
  changePositionColumnsConfig,
} from "redux/slices/kanbanViewSetting";
import _ from "lodash";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
import { getListObject } from "redux/slices/objectsManagement";
import {
  loadListObjectField,
  loadListObjectFieldSuccess,
} from "redux/slices/objects";
import emptyEmail from "../../assets/icons/email/empty-email.svg";
import deleteIcon from "../../assets/icons/email/deleteIcon.svg";
import editIcon from "../../assets/icons/email/editIcon.svg";
import OptionField from "./OptionField";
import NewOptionField from "./NewOptionField";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import ThemeButton from "components/CustomThemes/ThemeButton";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import { changeTitlePage } from "redux/slices/authenticated";
const { Step } = Steps;
const { Option } = Select;
const KanbanViewSetting = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() => handleSearch(confirm)}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(confirm)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Tìm kiếm
          </Button>
          <Button
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Đặt lại
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
            }}
          >
            Lọc dữ liệu
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    render: (text) =>
      //   searchedColumn === dataIndex ? (
      //     <Highlighter
      //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //       searchWords={[searchText]}
      //       autoEscape
      //       textToHighlight={text ? text.toString() : ''}
      //     />
      //   ) : (
      text,
    //   ),
  });
  const [form_step_1] = Form.useForm();
  const [listOptionObjectField, setListOptionObjectField] = useState([]);
  const [listFullObjectField, setListFullObjectField] = useState([]);

  const [dataDelete, setDataDelete] = useState({});

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  useEffect(() => {
    dispatch(changeTitlePage(t("kanbanView.kanbanViewSetting")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "kanban_view" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleSearch = (confirm) => {
    confirm();
  };
  const handleReset = (clearFilters) => {
    clearFilters();
  };
  const deleteSetting = (record) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: record._id,
    });
  };
  const editSetting = (record) => {
    dispatch(loadListObjectFieldSuccess([]));
    dispatch(
      getKanbanViewSettingById({
        _id: record._id,
      })
    );
  };
  const columns = [
    {
      title: t("object.object"),
      dataIndex: "object_name",
      key: "object_name",
      ...getColumnSearchProps("object_name"),
    },
    {
      title: t("kanbanView.fieldPriority"),
      dataIndex: "column_field_name",
      ...getColumnSearchProps("column_field_name"),
    },
    {
      title: t("kanbanView.fieldForeign"),
      dataIndex: "status_field_name",
      ...getColumnSearchProps("status_field_name"),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "right",
      width: 100,
      render: (record) => (
        <>
          {checkRule("edit") && (
            <img
              alt="Edit"
              src={editIcon}
              onClick={() => editSetting(record)}
            />
          )}
          {checkRule("delete") && (
            <img
              alt="Delete"
              src={deleteIcon}
              onClick={() => deleteSetting(record)}
            />
          )}
        </>
      ),
    },
  ];
  const color = [
    "#E53935",
    "#D81B60",
    "#8E24AA",
    "#5E35B1",
    "#3949AB",
    "#1E88E5",
    "#039BE5",
    "#00ACC1",
    "#00897B",
    "#43A047",
    "#7CB342",
    "#C0CA33",
    "#FDD835",
    "#FFB300",
    "#FB8C00",
    "#F4511E",
    "#6D4C41",
    "#757575",
    "#546E7A",
    "#54587A",
  ];
  const renderColor = (index) => {
    return (
      <CustomMenu>
        <Row style={{ width: "15rem" }}>
          {color.map((item) => {
            return (
              <Col>
                <CustomColor
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    dispatch(
                      updateColorStatus({
                        index: index,
                        value: item,
                      })
                    );
                  }}
                  colorBorder={"white"}
                  color={item}
                />
              </Col>
            );
          })}
        </Row>
      </CustomMenu>
    );
  };
  const { listKanbanView, showModal, editKanbanView, step } = useSelector(
    (state) => state.kanbanViewSettingReducer
  );
  const { listObject } = useSelector((state) => state.objectsManagementReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const {
    object_id,
    column_field,
    column_configs,
    status_field,
    status_style,
    confirm_before_edit,
    uncategorized_column_config,
    show_uncategorized,
  } = editKanbanView;

  const addNew = () => {
    dispatch(setShowModal(true));
    dispatch(
      getKanViewSettingByIdSuccess({
        column_configs: [],
        column_field: "",
        confirm_before_edit: false,
        object_id: "",
        show_uncategorized: false,
        status_field: "",
        status_style: [],
        title: "",
        uncategorized_column_config: {},
      })
    );
  };
  const hideModal = () => {
    dispatch(setShowModal(false));
    dispatch(setStep(0));
    form_step_1.resetFields();
  };
  const nextStep = () => {
    form_step_1.submit();
  };
  const prevStep = () => {
    if (step >= 1) {
      let temp = step - 1;
      dispatch(setStep(temp));
    }
  };
  const changeColor = (valColor, index) => {
    dispatch(
      updateColorColumnConfig({
        value: {
          color: valColor,
        },
        index: index,
      })
    );
  };
  const addMoreDisplayField = (value, index) => {
    dispatch(
      addDisplayField({
        index: index,
        value: value,
      })
    );
  };
  const removeDisplayField = (value, index) => {
    dispatch(
      deleteDisplayField({
        index: index,
        value: value,
      })
    );
  };
  const updateDisplayField = (value, key, i, index) => {
    dispatch(
      updateDisplayFields({
        index: index,
        key: key,
        i: i,
        value: value,
      })
    );
  };
  const addMoreDisplayFieldUncategorized = (e) => {
    dispatch(addDisplayFieldUncategorized(e));
  };
  const removeDisplayFieldUncategorized = (e) => {
    dispatch(deleteDisplayFieldUncategorized(e));
  };
  const editDisplayFieldUncategorized = (value, key, i) => {
    dispatch(
      updateDisplayFieldUncategorized({
        key: key,
        i: i,
        value: value,
      })
    );
  };
  const changeColorUncategorized = (item) => {
    dispatch(updateColorUncategorized(item));
  };
  const onDragEndField = (result, fields_order, index) => {
    let arr = [...fields_order];
    if (result.destination) {
      if (result.destination.index >= arr.length) {
        var k = result.destination.index - arr.length + 1;
        while (k--) {
          arr.push(undefined);
        }
      }
      arr.splice(
        result.destination.index,
        0,
        arr.splice(result.source.index, 1)[0]
      );
    }
    dispatch(
      updatePositionField({
        index: index,
        value: arr,
      })
    );
  };
  const onDragEndFieldUncategorized = (result, fields_order) => {
    let arr = [...fields_order];
    if (result.destination) {
      if (result.destination.index >= arr.length) {
        var k = result.destination.index - arr.length + 1;
        while (k--) {
          arr.push(undefined);
        }
      }
      arr.splice(
        result.destination.index,
        0,
        arr.splice(result.source.index, 1)[0]
      );
    }
    dispatch(updatePositionFieldUncategorized(arr));
  };
  const changeObjectId = (value) => {
    dispatch(
      updateFieldKanbanView({
        key: "column_field",
        value: "",
      })
    );
    dispatch(
      updateFieldKanbanView({
        key: "status_field",
        value: "",
      })
    );
    dispatch(
      updateFieldKanbanView({
        key: "object_id",
        value: value,
      })
    );
    if (value) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: value,
        })
      );
    }
  };
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };
  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    if (result.type === "column_configs") {
      const arr = reorder(
        column_configs,
        result.source.index,
        result.destination.index
      );
      dispatch(changePositionColumnsConfig(arr));
    } else {
      const tem = reorder(
        column_configs[parseInt(result.type, 10)].fields_order,
        result.source.index,
        result.destination.index
      );
      const temp = JSON.parse(JSON.stringify(column_configs));
      temp[result.type].fields_order = tem;
      dispatch(changePositionColumnsConfig(temp));
    }
  };
  const submit = () => {
    if (editKanbanView._id) {
      dispatch(updateKanbanViewSetting(editKanbanView));
    } else {
      dispatch(saveKanbanViewSetting(editKanbanView));
    }
  };

  const renderDescription = (step, position) => {
    let description = "";
    if (step === position) {
      description = "In progress";
    } else if (step > position) {
      description = "Finished";
    } else {
      description = "Waiting";
    }
    return description;
  };

  renderDescription(step);

  useEffect(() => {
    let arr = [];
    let arrFull = [];
    if (listObjectField.length > 0) {
      if (
        listObjectField.length > 0 &&
        listObjectField.filter((x) => x.main_object).length > 0
      ) {
        // eslint-disable-next-line
        listObjectField
          .filter((x) => x.main_object)[0]
          ["main_object"]["sections"].forEach((data) => {
            // eslint-disable-next-line
            data.fields.map((item) => {
              arrFull.push(item);
              if (item.type === "select" && item.option.length <= 10) {
                arr.push(item);
              }
            });
          });
      }
    }
    setListFullObjectField(arrFull);
    setListOptionObjectField(arr);
  }, [listObjectField]);
  //   useEffect(() => {
  //     if(object_id) {
  //         dispatch(loadListObjectField({
  //           api_version: "2",
  //           object_id: object_id
  //         }))
  //     }
  //     // eslint-disable-next-line
  // }, [object_id, _id])
  useEffect(() => {
    if (!column_field) {
      dispatch(
        updateFieldKanbanView({
          key: "column_configs",
          value: [],
        })
      );
    }
    // eslint-disable-next-line
  }, [column_field, listOptionObjectField]);
  useEffect(() => {
    if (!status_field) {
      dispatch(
        updateFieldKanbanView({
          key: "status_style",
          value: [],
        })
      );
    }
    // eslint-disable-next-line
  }, [status_field, listOptionObjectField]);
  useEffect(() => {
    dispatch(getListKanbanView());
    dispatch(getListObject());
    // eslint-disable-next-line
  }, []);
  // unmount
  useEffect(
    () => () => {
      dispatch(setStep(0));
      dispatch(unmountKanbanViewSetting());
    },
    // eslint-disable-next-line
    []
  );
  return (
    <CustomContent>
      <ModalConfimDelete
        title={t("kanbanView.deleteKanban")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteKanbanViewSetting}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
      <Row justify={"space-between"} style={{ paddingBottom: "1rem" }}>
        <Col span={6}>
          <Breadcrumb>
            <Breadcrumb.Item onClick={() => navigate("/settings")}>
              {t("common.settings")}
            </Breadcrumb.Item>
            <BreadcrumbItem>
              {" "}
              {t("kanbanView.kanbanViewSetting")}{" "}
            </BreadcrumbItem>
          </Breadcrumb>
        </Col>
        {listKanbanView.length > 0 && checkRule("create") && (
          // <Col span={2}>
          <ThemeButton
            label={`+ ${t("kanbanView.addNew")}`}
            clickButton={addNew}
          />
          // </Col>
        )}
      </Row>
      <Row>
        <Col span={24}>
          {listKanbanView.length === 0 ? (
            <>
              <Row style={{ width: "100%" }} justify={"center"}>
                <img alt="Empty" src={emptyEmail} />
              </Row>
              <Row style={{ width: "100%" }} justify={"center"}>
                <p> {t("common.noData")} </p>
              </Row>
              {checkRule("create") && (
                <Row justify={"center"}>
                  {/* <Col span={2}> */}
                  <ThemeButton label={"+ Thêm mới"} clickButton={addNew} />
                  {/* </Col> */}
                </Row>
              )}
            </>
          ) : (
            <Table
              columns={columns}
              dataSource={listKanbanView}
              pagination={{
                position: ["bottomRight"],
                showSizeChanger: true,
                defaultPageSize: 10,
                showQuickJumper: true,
              }}
            />
          )}
        </Col>
      </Row>
      <CustomModal
        visible={showModal}
        // width={1000}
        width={800}
        onCancel={() => hideModal()}
        title={"Test"}
        footer={[
          <Row justify={"end"}>
            <Col
              style={{
                display: step === 0 ? "none" : "unset",
                marginRight: "1rem",
              }}
              className="btn-back"
            >
              <Button onClick={() => prevStep()}> {t("common.back")} </Button>
            </Col>
            <Col
              style={{
                display: step === 3 ? "none" : "unset",
                marginRight: "1rem",
              }}
              className="btn-next"
            >
              <Button onClick={() => nextStep()}> {t("common.next")} </Button>
            </Col>
            <Col
              style={{
                display: step === 3 ? "unset" : "none",
                marginRight: "1rem",
              }}
              className="btn-next"
            >
              <Button onClick={() => form_step_1.submit()}>
                {" "}
                {t("common.save")}{" "}
              </Button>
            </Col>
            <Col>
              <Button onClick={() => hideModal()}>
                {" "}
                {t("common.cancel")}{" "}
              </Button>
            </Col>
          </Row>,
        ]}
      >
        <Steps current={step}>
          <Step
            title={t("object.object")}
            description={renderDescription(step, 0)}
          />
          <Step
            title={t("kanbanView.columnField")}
            description={renderDescription(step, 1)}
          />
          <Step
            title={t("kanbanView.statusField")}
            description={renderDescription(step, 2)}
          />
          <Step
            title={t("kanbanView.uncategorized")}
            description={renderDescription(step, 3)}
          />
        </Steps>
        <Form
          name={"step-1"}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          validateMessages={"Trường này là bắt buộc"}
          onFinish={() => {
            if (step <= 2) {
              dispatch(setStep(step + 1));
            } else if (step === 3) {
              submit();
            }
          }}
          form={form_step_1}
        >
          <CustomBody>
            {(() => {
              switch (step) {
                case 1:
                  return (
                    <div>
                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name={"column_field"}
                            label={t("kanbanView.columnField")}
                            valuePropName={column_field}
                            rules={[
                              {
                                validator: (rule, value, cb) => {
                                  column_field.length === 0
                                    ? cb("Trường này là bắt buộc")
                                    : cb();
                                },
                                required: true,
                              },
                            ]}
                          >
                            <Select
                              showSearch
                              onChange={(e) => {
                                dispatch(
                                  updateFieldKanbanView({
                                    key: "column_field",
                                    value: e,
                                  })
                                );
                                if (e && listOptionObjectField.length > 0) {
                                  let arr = [];
                                  // eslint-disable-next-line
                                  listOptionObjectField
                                    .filter((x) => x.field_id === e)[0]
                                    .option.forEach((item) => {
                                      arr.push({
                                        value: item.value,
                                        style: {
                                          color: "#00897B",
                                        },
                                        fields_order: [],
                                        display_fields: {},
                                      });
                                    });
                                  dispatch(
                                    updateFieldKanbanView({
                                      key: "column_configs",
                                      value: arr,
                                    })
                                  );
                                }
                                form_step_1.resetFields();
                              }}
                              value={column_field}
                              optionFilterProp="children"
                              filterOption={(inputValue, option) => {
                                if (option.children) {
                                  return option.children
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                } else if (option.label) {
                                  return option.label
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                }
                              }}
                            >
                              {listOptionObjectField &&
                                listOptionObjectField.map((item) => {
                                  return (
                                    !item.multiple && (
                                      <Option value={item.field_id}>
                                        {item.name}
                                      </Option>
                                    )
                                  );
                                })}
                            </Select>
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={24}>
                          <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable
                              droppableId="column_configs"
                              type="column_configs"
                            >
                              {(provided) => (
                                <div ref={provided.innerRef}>
                                  {column_configs.map((column, index) => (
                                    <Draggable
                                      key={column.value}
                                      draggableId={column.value}
                                      index={index}
                                    >
                                      {(provided) => (
                                        <div
                                          ref={provided.innerRef}
                                          {...provided.draggableProps}
                                          {...provided.dragHandleProps}
                                        >
                                          <NewOptionField
                                            removeField={removeDisplayField}
                                            onDragEndField={onDragEndField}
                                            indexx={index} //questionNum
                                            listFullObjectField={
                                              listFullObjectField
                                            }
                                            addMoreDisplayField={
                                              addMoreDisplayField
                                            }
                                            updateDisplayField={
                                              updateDisplayField
                                            }
                                            changeColor={changeColor}
                                            item={column} //question
                                            key={column.value}
                                          />
                                        </div>
                                      )}
                                    </Draggable>
                                  ))}
                                </div>
                              )}
                            </Droppable>
                          </DragDropContext>
                        </Col>
                      </Row>
                    </div>
                  );
                case 2:
                  return (
                    <div>
                      <Row>
                        <Col span={24}>
                          <Form.Item
                            name={"status_field"}
                            label={t("kanbanView.statusField")}
                            valuePropName={status_field}
                            rules={[
                              {
                                validator: (rule, value, cb) => {
                                  status_field.length === 0
                                    ? cb("Trường này là bắt buộc")
                                    : cb();
                                },
                                required: true,
                              },
                            ]}
                          >
                            <Select
                              value={status_field}
                              onChange={(e) => {
                                dispatch(
                                  updateFieldKanbanView({
                                    key: "status_field",
                                    value: e,
                                  })
                                );
                                if (e && listOptionObjectField.length > 0) {
                                  let arr = [];
                                  // eslint-disable-next-line
                                  listOptionObjectField
                                    .filter((x) => x.field_id === e)[0]
                                    .option.forEach((item) => {
                                      arr.push({
                                        value: item.value,
                                        style: {
                                          color: "#00897B",
                                        },
                                      });
                                    });
                                  dispatch(
                                    updateFieldKanbanView({
                                      key: "status_style",
                                      value: arr,
                                    })
                                  );
                                }
                              }}
                              showSearch
                              optionFilterProp="children"
                              filterOption={(inputValue, option) => {
                                if (option.children) {
                                  return option.children
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                } else if (option.label) {
                                  return option.label
                                    .toLowerCase()
                                    .indexOf(inputValue.toLowerCase()) >= 0
                                    ? true
                                    : false;
                                }
                              }}
                            >
                              {listOptionObjectField &&
                                listOptionObjectField.map((item) => {
                                  return (
                                    !item.multiple && (
                                      <Option value={item.field_id}>
                                        {item.name}
                                      </Option>
                                    )
                                  );
                                })}
                            </Select>
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row justify={"space-between"}>
                        {status_style &&
                          status_style.map((item, index) => {
                            return (
                              <CustomStatus span={11}>
                                <Row>
                                  <Col span={22}>
                                    <span>{item.value}</span>
                                  </Col>
                                  <Col span={2}>
                                    <Dropdown
                                      overlay={renderColor(index)}
                                      trigger={["click"]}
                                    >
                                      <CustomColor
                                        colorBorder={"#D9D9D9"}
                                        color={item.style.color}
                                      />
                                    </Dropdown>
                                  </Col>
                                </Row>
                              </CustomStatus>
                            );
                          })}
                      </Row>
                      <Row>
                        <Col span={24}>
                          <Checkbox
                            checked={confirm_before_edit}
                            onChange={(e) =>
                              dispatch(
                                updateFieldKanbanView({
                                  key: "confirm_before_edit",
                                  value: e.target.checked,
                                })
                              )
                            }
                          >
                            {t("kanbanView.confirmAction")}
                          </Checkbox>
                        </Col>
                      </Row>
                    </div>
                  );
                case 3:
                  return (
                    <div>
                      <Row>
                        <Col span={24}>
                          <Checkbox
                            style={{ marginBottom: "16px" }}
                            value={show_uncategorized}
                            defaultChecked={show_uncategorized}
                            onChange={(e) => {
                              dispatch(
                                updateFieldKanbanView({
                                  key: "uncategorized_column_config",
                                  value: {
                                    display_fields: {},
                                    fields_order: [],
                                    style: { color: "#fff" },
                                    value: t("kanbanView.uncategorized"),
                                  },
                                })
                              );
                              dispatch(
                                updateFieldKanbanView({
                                  key: "show_uncategorized",
                                  value: e.target.checked,
                                })
                              );
                            }}
                          >
                            {t("kanbanView.uncategorized")}
                          </Checkbox>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={24}>
                          {show_uncategorized && (
                            <OptionField
                              removeField={removeDisplayFieldUncategorized}
                              onDragEndField={onDragEndFieldUncategorized}
                              listFullObjectField={listFullObjectField}
                              addMoreDisplayField={
                                addMoreDisplayFieldUncategorized
                              }
                              updateDisplayField={editDisplayFieldUncategorized}
                              changeColor={changeColorUncategorized}
                              item={uncategorized_column_config}
                            />
                          )}
                        </Col>
                      </Row>
                    </div>
                  );
                default:
                  return (
                    <Form.Item
                      name={"object_id"}
                      label={t("object.object")}
                      rules={[
                        {
                          validator: (rule, value, cb) => {
                            object_id.length === 0
                              ? cb("Trường này là bắt buộc")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                      valuePropName={object_id}
                    >
                      <Select
                        onChange={(e) => changeObjectId(e)}
                        value={object_id}
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                      >
                        {listObject &&
                          // eslint-disable-next-line
                          listObject.map((item) => {
                            if (item.Status) {
                              return (
                                item.Status && (
                                  <Option value={item._id}>{item.Name}</Option>
                                )
                              );
                            }
                          })}
                      </Select>
                    </Form.Item>
                  );
              }
            })()}
          </CustomBody>
        </Form>
      </CustomModal>
    </CustomContent>
  );
};

export default KanbanViewSetting;

const CustomContent = styled.div`
  padding: 1rem;
  .add-new {
    width: 100%;
    border: 1px solid #1fa2a2;
    color: #1fa2a2;
    &:hover {
      background-color: #1fa2a2;
      color: white;
    }
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
    button {
      padding: 0.5rem;
    }
    .ant-modal-title {
      display: none;
    }
    .ant-modal-header {
      background-color: #f2f4f5;
      border-radius: 10px 10px 0px 0px;
    }
    .ant-modal-close-x {
      width: unset;
      height: unset;
      line-height: unset;
    }
  }
  .ant-modal-body {
    height: 70vh;
    overflow-y: scroll;
    padding: 0 24px;
  }
  .ant-steps-item-active {
    .ant-steps-item-icon {
      background: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }

    .ant-steps-item-title {
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-item-wait
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-title {
    font-family: var(--roboto-500);
    color: #2c2c2c;
  }
  .ant-steps-item-finish {
    .ant-steps-item-icon {
      border-color: ${(props) => props.theme.main};
    }
    svg {
      color: ${(props) => props.theme.main};
    }
    .ant-steps-item-title {
      font-family: var(--roboto-500);
      color: ${(props) => props.theme.main} !important;
    }
    .ant-steps-item-title::after {
      background-color: ${(props) => props.theme.main} !important;
    }
  }
  .ant-steps-icon {
    top: -1.5px;
  }
  .ant-steps {
    margin-top: 40px;
    margin-bottom: 16px;
  }

  .ant-steps-item-description {
    font-size: 16px;
  }
  .ant-steps-item-process
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description,
  .ant-steps-item-finish
    > .ant-steps-item-container
    > .ant-steps-item-content
    > .ant-steps-item-description {
    color: #2c2c2c;
  }
  .ant-modal-footer {
    border-top: none;
    padding: 33px 24px;

    button {
      width: 130px;
      height: 40px;
      padding: 1px;
      border: 1px solid #d9d9d9;
      &:hover {
        border: 1px solid ${(props) => props.theme.main};
        color: #ffffff;
        background-color: ${(props) => props.theme.main};
      }
    }

    .btn-back {
      button {
        border-color: ${(props) => props.theme.main};
        color: ${(props) => props.theme.main};
        &:hover {
          color: #ffffff;
        }
      }
    }

    .btn-next {
      button {
        background-color: ${(props) => props.theme.main};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;
const CustomBody = styled.div`
  padding-top: 1rem;
  label {
    font-family: var(--roboto-700);
    font-size: 16px;
  }
  .ant-form-item-label {
    text-align: left !important;
  }

  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;
const CustomStatus = styled(Col)`
  padding: 0.3rem;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  margin-bottom: 1rem;
`;
const CustomColor = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  background-color: ${(props) => props.color};
  border: 2px solid ${(props) => props.colorBorder};
`;
const CustomMenu = styled(Menu)`
  padding: 0.4rem;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
