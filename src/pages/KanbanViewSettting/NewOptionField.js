import { Row, Collapse, Col, Menu, Dropdown, Select, Form } from "antd";
import styled from "styled-components";
import { Droppable, Draggable } from "react-beautiful-dnd";
import move from "../../assets/icons/kanban/Move.png";
import { useEffect, useState } from "react";
import { Notification } from "components/Notification/Noti";
import { useTranslation } from "react-i18next";
const { Panel } = Collapse;
const { Option } = Select;

const OptionField = (props) => {
  const { t } = useTranslation();
  const {
    changeColor,
    indexx,
    item,
    listFullObjectField,
    updateDisplayField,
    addMoreDisplayField,
    removeField,
  } = props;
  const { value, style, fields_order, display_fields } = item;
  const [valueColumnField, setValueColumnField] = useState([]);
  const color = [
    "#E53935",
    "#D81B60",
    "#8E24AA",
    "#5E35B1",
    "#3949AB",
    "#1E88E5",
    "#039BE5",
    "#00ACC1",
    "#00897B",
    "#43A047",
    "#7CB342",
    "#C0CA33",
    "#FDD835",
    "#FFB300",
    "#FB8C00",
    "#F4511E",
    "#6D4C41",
    "#757575",
    "#546E7A",
    "#54587A",
  ];
  const meta_data = [
    {
      label: "Create Date",
      value: "created_date",
    },
    {
      label: "Created By",
      value: "created_by",
    },
    {
      label: "Modify Time",
      value: "modify_time",
    },
    {
      label: "Modified by",
      value: "modify_by",
    },
    {
      label: "Assign To",
      value: "owner",
    },
  ];
  const renderColor = (i) => {
    return (
      <CustomMenu>
        <Row style={{ width: "15rem" }}>
          {color.map((el) => {
            return (
              <Col>
                <CustomColor
                  onClick={(e) => {
                    e.stopPropagation();
                    e.preventDefault();
                    changeColor(el, i);
                  }}
                  color={el}
                />
              </Col>
            );
          })}
        </Row>
      </CustomMenu>
    );
  };

  useEffect(() => {
    if (display_fields) {
      let arr = [];
      Object.keys(display_fields).forEach((key) => {
        arr.push(key);
      });
      setValueColumnField(arr);
    }
    // eslint-disable-next-line
  }, [display_fields]);

  return (
    <Row>
      <Col span={24}>
        <CustomCollapse defaultActiveKey={["1"]}>
          <Panel
            header={
              <div
                style={{
                  width: "100%",
                  display: "flex",
                  justifyContent: "space-between",
                }}
              >
                <div>{value}</div>
                <div>
                  <Dropdown overlay={renderColor(indexx)} trigger={["click"]}>
                    <CustomColor
                      color={style.color}
                      onClick={(e) => {
                        e.stopPropagation();
                        e.preventDefault();
                      }}
                    />
                  </Dropdown>
                </div>
              </div>
            }
            key="1"
          >
            <CustomContent>
              <Row>
                <Col
                  span={6}
                  style={{
                    fontSize: "16px",
                    color: "#2c2c2c",
                  }}
                >
                  {t("kanbanView.columnField")}
                </Col>
                <Col span={18}>
                  <CustomRow>
                    {" "}
                    <Form.Item
                      name={"valueColumnField_" + indexx}
                      valuePropName={valueColumnField}
                      rules={[
                        {
                          validator: (rule, val, cb) => {
                            Object.entries(display_fields).length === 0
                              ? cb("Chọn tối thiểu 1 giá trị")
                              : cb();
                          },
                          required: true,
                        },
                      ]}
                    >
                      <Select
                        showSearch
                        optionFilterProp="children"
                        filterOption={(inputValue, option) => {
                          if (option.children) {
                            return option.children
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          } else if (option.label) {
                            return option.label
                              .toLowerCase()
                              .indexOf(inputValue.toLowerCase()) >= 0
                              ? true
                              : false;
                          }
                        }}
                        mode="multiple"
                        onDeselect={(e) => {
                          removeField(e, indexx);
                        }}
                        onSelect={(e) => {
                          if (valueColumnField.length < 5) {
                            addMoreDisplayField(e, indexx);
                          } else {
                            Notification(
                              "warning",
                              "Chỉ được chọn tối đa 5 trường"
                            );
                          }
                        }}
                        style={{ width: "100%" }}
                        value={valueColumnField}
                      >
                        {listFullObjectField &&
                          listFullObjectField.map((option) => {
                            return (
                              <Option value={option.field_id}>
                                {option.name}
                              </Option>
                            );
                          })}
                        {meta_data.map((el) => {
                          return <Option value={el.value}>{el.label}</Option>;
                        })}
                      </Select>
                    </Form.Item>
                  </CustomRow>
                </Col>
              </Row>
              <Droppable
                droppableId={`droppable${item.value}`}
                type={`${indexx}`}
              >
                {(provided) => (
                  <div ref={provided.innerRef}>
                    {fields_order.length > 0 &&
                      fields_order.map((item, index) => {
                        return (
                          <Draggable
                            key={`${indexx}${index}`}
                            draggableId={`${indexx}${index}`}
                            index={index}
                          >
                            {(provided) => (
                              <div
                                ref={provided.innerRef}
                                {...provided.draggableProps}
                                {...provided.dragHandleProps}
                              >
                                <Row justify={"space-around"}>
                                  <Col
                                    span={8}
                                    style={{
                                      fontSize: "14px",
                                      color: "#252424",
                                      overflow: "hidden",
                                      textOverflow: "ellipsis",
                                      whiteSpace: "nowrap",
                                    }}
                                  >
                                    <img
                                      src={move}
                                      alt=""
                                      style={{ marginRight: "10px" }}
                                    />
                                    {listFullObjectField.filter(
                                      (x) => x.field_id === item
                                    )[0] &&
                                      listFullObjectField.filter(
                                        (x) => x.field_id === item
                                      )[0].name}
                                    {meta_data.filter(
                                      (x) => x.value === item
                                    )[0] &&
                                      meta_data.filter(
                                        (x) => x.value === item
                                      )[0].label}
                                  </Col>
                                  <Col
                                    span={15}
                                    style={{ marginBottom: "8px" }}
                                  >
                                    <Select
                                      showSearch
                                      optionFilterProp="children"
                                      filterOption={(inputValue, option) => {
                                        if (option.children) {
                                          return option.children
                                            .toLowerCase()
                                            .indexOf(
                                              inputValue.toLowerCase()
                                            ) >= 0
                                            ? true
                                            : false;
                                        } else if (option.label) {
                                          return option.label
                                            .toLowerCase()
                                            .indexOf(
                                              inputValue.toLowerCase()
                                            ) >= 0
                                            ? true
                                            : false;
                                        }
                                      }}
                                      style={{ width: "100%" }}
                                      value={display_fields[item].font_weight}
                                      onChange={(e) =>
                                        updateDisplayField(
                                          e,
                                          item,
                                          "font_weight",
                                          indexx
                                        )
                                      }
                                    >
                                      <Option value={"600"}>Bold</Option>
                                      <Option value={"300"}>Italic</Option>
                                      <Option value={"400"}>Regular</Option>
                                    </Select>
                                  </Col>
                                </Row>
                              </div>
                            )}
                          </Draggable>
                        );
                      })}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </CustomContent>
          </Panel>
        </CustomCollapse>
      </Col>
    </Row>
  );
};

export default OptionField;

const CustomCollapse = styled(Collapse)`
  border: 0px;
  /* .ant-collapse-item-active {
    .ant-collapse-header {
      border: 1px solid ${(props) => props.theme.main};
    }
  } */
  .ant-collapse-item {
    border-bottom: none;
  }
  .ant-collapse-header {
    margin-bottom: 1rem;
    /* padding: 0.3rem !important; */
    padding: 4px 4px 4px 8px !important;

    background-color: #f0f0f0;
    font-family: var(--roboto-500);
    font-size: 16px;
    border-radius: 2px !important;
    border: 1px solid #ececec;
    &:hover {
      border: 1px solid ${(props) => props.theme.main};
    }
  }
  .ant-collapse-content-box {
    border: 1px solid #ececec;
    padding: 8px 8px 0 8px;
    margin-bottom: 1rem;
    border-radius: 2px;
    background: #fafafa;
  }
  .ant-collapse-content {
    border-top: none;
  }

  background-color: white !important;
`;
const CustomColor = styled.div`
  height: 1.5rem;
  width: 1.5rem;
  background-color: ${(props) => props.color};
  &:hover {
    border: 2px solid white;
  }
`;
const CustomMenu = styled(Menu)`
  padding: 0.4rem;
`;
const CustomContent = styled.div``;

const CustomRow = styled.div`
  .ant-col-18 {
    max-width: 100%;
    width: 100%;
  }
`;
