import styled from "styled-components/macro";
import { useState, useEffect } from "react";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import _ from "lodash";

import {
  popupCheckWorkflow,
  updateRecordManyParam,
  updateRecordManyParamResult,
} from "redux/slices/consolidatedViewSettings";
import Table from "antd/lib/table";

function ListRecord({ isMany, pathname }) {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { objectID, fieldID } = useParams();
  const { detailsPopup, statusManyParam } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [dateSource, setDataSource] = useState([]);
  const [column, setColumn] = useState([]);

  const _onSelectRecord = async (record) => {
    if (isMany) {
      dispatch(
        updateRecordManyParam({
          payload: {
            object_id: objectID,
            id: record._id,
          },
          pathname: pathname,
        })
      );
    } else {
      navigate(`/consolidated-view/${objectID}/${record._id}`);
      dispatch(
        popupCheckWorkflow({
          object_id: objectID,
          record_id: _.get(record, "_id"),
          field_id: fieldID,
        })
      );
    }
  };

  useEffect(() => {
    if (Object.keys(detailsPopup.data).length > 0) {
      let tempColumn = [];
      detailsPopup.data.field.forEach((item) => {
        tempColumn.push({
          key: item.ID,
          title: item.name === "Owner" ? "Assign to" : item.name,
          dataIndex: item.ID,
        });
      });
      setColumn(tempColumn);

      let tempData = [];
      detailsPopup.data.data.forEach((item) => {
        let newItem = {};
        newItem["key"] = item._id;
        Object.entries(item).forEach(([key, value]) => {
          if (typeof value === "object" && value !== null) {
            newItem[key] = value.value;
          } else {
            newItem[key] = value;
          }
        });
        tempData.push(newItem);
      });
      setDataSource(tempData);
    }
  }, [detailsPopup]);

  useEffect(() => {
    if (statusManyParam) {
      dispatch(updateRecordManyParamResult(null));
      navigate(`/consolidated-view/${objectID}/${statusManyParam}`);
      dispatch(
        popupCheckWorkflow({
          object_id: objectID,
          record_id: statusManyParam,
          field_id: fieldID,
        })
      );
    }
  }, [dispatch, fieldID, navigate, objectID, statusManyParam]);

  return (
    <Wrapper>
      <Table
        dataSource={dateSource}
        columns={column}
        pagination={false}
        scroll={{
          x: "max-content",
        }}
        onRow={(record) => {
          return {
            onDoubleClick: () => {
              _onSelectRecord(record);
            },
          };
        }}
      />
    </Wrapper>
  );
}

export default ListRecord;

const Wrapper = styled.div`
  padding: 24px;

  .ant-table-wrapper {
    padding: 16px 24px;
    background: #fff;
  }
`;
