import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import moment from "moment";
import _ from "lodash";
import { useLocation } from "react-router-dom";

import Pagination from "antd/lib/pagination";
import Breadcrumb from "antd/lib/breadcrumb";
import Empty from "antd/lib/empty";
import Button from "antd/lib/button";
import Form from "antd/lib/form";

import EmptyMapping from "assets/images/consolidatedViewSetting/empty_user_mapping.png";

import {
  loadPopup,
  createRecordPopup,
  createRecordPopupSuccess,
  updateRecordPopup,
  updateRecordPopupResult,
  popupCheckWorkflow,
} from "redux/slices/consolidatedViewSettings";
import {
  setShowModal,
  loadListObjectField,
  loadDataWithOutPagi,
} from "redux/slices/objects";
import ModalRecord from "pages/Objects/modal/modalRecord";
import ListRecord from "./listRecord";
import { BASE_URL_API, BE_URL } from "constants/constants";
import ModalDuplicate from "components/Modal/ModalDuplicated";

function UrlPopup(props) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const [form] = Form.useForm();
  const { objectID, fieldID } = useParams();
  const [isMany, $isMany] = useState(false);
  const [valueField, $valueField] = useState("");

  const { userDetail } = useSelector((state) => state.userReducer);
  const { listDuplicate, searchList, fields } = useSelector(
    (state) => state.objectsReducer
  );
  const { detailsPopup, statusCreateRecordPopup } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [recordId, setRecordId] = useState("");

  const [, setEditingKey] = useState("");
  const [formValues, $formValues] = useState({});

  const _onSubmit = (values, isDuplicated, isOverWrite, recordOverWrite) => {
    $formValues(values);

    let fieldsObject = {};
    fields.forEach((section, idx) => {
      section.fields.forEach((item, index) => {
        fieldsObject[item.ID] = { ...item };
      });
    });
    let listValue = [];
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          if (value.fileList) {
            value.fileList.forEach((item, idx) => {
              if (item.url === undefined) {
                item.url =
                  BASE_URL_API + item &&
                  item.response &&
                  item.response.data &&
                  item.response.data[0];
                item.url = BE_URL + item.url;
              }
              result.push(item.url);
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(item);
                return null;
              });
            } else {
              value.forEach((item, idx) => {
                if (item.url) {
                  result.push(item.url);
                } else {
                  result.push(item);
                }
              });
            }
          }
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    let searchData = [];
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    let meta = [];
    let data = [];

    if (isOverWrite) {
      dispatch(
        updateRecordPopup({
          data: {
            data: listDuplicate[listDuplicate.length - 1]?.record_data,
            owner_id: listDuplicate[listDuplicate.length - 1]?.owner,
            id: recordOverWrite,
            object_id: objectID,
            ignore_duplication: true,
          },
          load: {
            object_id: objectID,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    } else {
      dispatch(
        createRecordPopup({
          data: {
            object_id: objectID,
            data: isDuplicated
              ? listDuplicate[listDuplicate.length - 1]?.record_data
              : listValue,
            owner: isDuplicated
              ? listDuplicate[listDuplicate.length - 1]?.owner
              : values["assignTo"],
            ignore_duplication: isDuplicated ? true : undefined,
          },
          load: {
            object_id: objectID,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: meta,
              data: data,
            },
          },
        })
      );
    }

    setRecordId("");
    setEditingKey("");
    form.resetFields();
  };

  const showTotal = () => {
    return `Total ${detailsPopup.pagination.total_record} items`;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  useEffect(() => {
    if (statusCreateRecordPopup !== null) {
      navigate(
        `/consolidated-view/${statusCreateRecordPopup.object_id}/${statusCreateRecordPopup.record_id}`
      );
    }
  }, [navigate, statusCreateRecordPopup]);

  useEffect(() => {
    return () => {
      dispatch(createRecordPopupSuccess(null));
      dispatch(updateRecordPopupResult(null));
    };
  }, [dispatch]);

  useEffect(() => {
    if (valueField) {
      dispatch(
        loadPopup({
          pagination: {
            current_page: currentPage,
            object_id: objectID,
            field_id: fieldID,
            pagination: true,
            record_per_page: recordPerPage,
            value: valueField,
          },
          data: {
            current_page: currentPage,
            object_id: objectID,
            field_id: fieldID,
            record_per_page: recordPerPage,
            value: valueField,
          },
        })
      );
    }
  }, [currentPage, dispatch, objectID, fieldID, recordPerPage, valueField]);

  useEffect(() => {
    if (
      pathname?.split("&&&")?.length === 1 &&
      detailsPopup.pagination.total_record === 1 &&
      Object.keys(detailsPopup.data).length > 0
    ) {
      navigate(
        `/consolidated-view/${objectID}/${detailsPopup.data.data[0]._id}`
      );
      dispatch(
        popupCheckWorkflow({
          object_id: objectID,
          record_id: _.get(detailsPopup, "data.data[0]._id"),
          field_id: fieldID,
        })
      );
    }
  }, [detailsPopup, dispatch, fieldID, navigate, objectID, pathname]);

  useEffect(() => {
    if (detailsPopup.pagination.total_record === 0) {
      dispatch(setShowModal(true));

      let fieldType = "";
      _.map(fields, (field) => {
        _.map(_.get(field, "fields", []), (item) => {
          if (_.get(item, "ID") === fieldID && !_.get(item, "readonly")) {
            fieldType = _.get(item, "type", "");
          }
        });
      });

      if (
        fieldType === "text" ||
        fieldType === "textarea" ||
        fieldType === "email"
      ) {
        form.setFieldValue(fieldID, valueField);
      }
    }
  }, [detailsPopup, dispatch, fieldID, fields, form, valueField]);

  useEffect(() => {
    if (objectID) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: objectID,
          show_meta_fields: true,
        })
      );

      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectID,
          },
          data: {
            object_id: objectID,
            first_record_id: "",
            last_record_id: "",
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }
  }, [dispatch, objectID]);

  useEffect(() => {
    if (pathname?.split("&&&").length > 1) {
      $valueField(pathname?.split("&&&")[0]?.split(fieldID)[1]?.split("/")[1]);
      $isMany(true);
      form.setFieldValue(
        fieldID,
        pathname?.split("&&&")[0]?.split(fieldID)[1]?.split("/")[1]
      );

      _.map(pathname?.split("&&&"), (field, idx) => {
        if (idx > 0) {
          form.setFieldsValue({
            [field.split("===")[0]]: decodeURI(field.split("===")[1]),
          });
        }
      });
    } else {
      $valueField(pathname.split(fieldID)[1]?.split("/")[1]);
      $isMany(false);
      form.setFieldValue(fieldID, pathname.split(fieldID)[1]?.split("/")[1]);
    }
  }, [pathname, form, fieldID]);

  return (
    <Wrapper>
      <Breadcrumb>
        <BreadcrumbItem>URL Popup</BreadcrumbItem>
      </Breadcrumb>

      {detailsPopup.pagination.total_record === 0 ? (
        <WrapEmpty>
          <Empty
            image={EmptyMapping}
            description={<p>There's no any record found</p>}
          ></Empty>

          <ButtonCreate onClick={() => dispatch(setShowModal(true))}>
            Create record
          </ButtonCreate>
        </WrapEmpty>
      ) : detailsPopup.pagination.total_record === 1 && !isMany ? (
        <span></span>
      ) : (
        <WrapList>
          <ListRecord isMany={isMany} pathname={pathname} />
          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={detailsPopup.pagination.total_record}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />

          {isMany && (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                paddingBottom: "16px",
              }}
            >
              <ButtonCreate onClick={() => dispatch(setShowModal(true))}>
                Create record
              </ButtonCreate>
            </div>
          )}
        </WrapList>
      )}

      <ModalRecord
        form={form}
        objectId={objectID}
        recordID={recordId}
        setRecordID={setRecordId}
        onFinish={_onSubmit}
        userDetail={userDetail}
        setEditingKey={setEditingKey}
      />

      <ModalDuplicate onFinishCall={_onSubmit} formValues={formValues} />
    </Wrapper>
  );
}

export default UrlPopup;

const Wrapper = styled.div`
  padding: 24px;

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 0 48px 16px 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapEmpty = styled.div`
  background: #fff;
  border-radius: 10px;
  min-height: 218px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  margin-top: 16px;

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }
`;

const WrapList = styled.div`
  background: #fff;
  margin-top: 16px;
`;

const ButtonCreate = styled(Button)`
  &.ant-btn {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    height: 40px;

    span {
      color: #fff;
      font-size: 16px;
    }

    :hover {
      background: ${(props) => props.theme.darker}!important;
      color: #fff !important;
    }

    :active {
      background: ${(props) => props.theme.main};
      color: #fff;
    }

    :focus {
      background: ${(props) => props.theme.main};
      color: #fff;
    }
  }
`;
