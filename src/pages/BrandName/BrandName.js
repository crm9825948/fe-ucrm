import {
  Row,
  Upload,
  Col,
  Button,
  Modal,
  Slider,
  InputNumber,
  Breadcrumb,
} from "antd";
import React, { useEffect, useState } from "react";
import _ from "lodash";

import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
// import { EditOutlined, PlusOutlined } from "@ant-design/icons";
import { BASE_URL_API, BE_URL } from "../../constants/constants";
import { THEME_COLOR } from "configs/color";
import {
  setDetailLogo,
  deleteLogoById,
  uploadLogo,
  getListLogo,
  getDefaultBrandNameSuccess,
  changeThemeColor,
} from "redux/slices/brandName";
import AvatarEditor from "react-avatar-editor";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import {
  setShowLoadingScreen,
  setShowModalConfirmDelete,
} from "redux/slices/global";
import { Notification } from "components/Notification/Noti";
import { useNavigate } from "react-router";
import { useTranslation } from "react-i18next";
import ThemeButton from "components/CustomThemes/ThemeButton";
import uploadIcon from "../../assets/icons/common/upload.png";
import editIcon from "../../assets/icons/common/edit-small.png";
import { changeTitlePage } from "redux/slices/authenticated";

const BrandName = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { listLogo, defaultBrandName, detailLogo } = useSelector(
    (state) => state.brandNameReducer
  );
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const [showModalListLogo, setShowModalListLogo] = useState(false);
  const [showModalEditLogo, setShowModalEditLogo] = useState(false);
  const [dataDelete, setDataDelete] = useState({});
  const [positionLogo, setPositionLogo] = useState({
    position: {
      x: 0.5,
      y: 0.5,
    },
    border: 0,
    borderRadius: "0%",
    scale: 1,
  });

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.brandName")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "brand_and_color" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const uploadAttachFile = ({ data }) => {
    dispatch(
      uploadLogo({
        media_url: BE_URL + data[0],
        media_config: {},
        theme_color: "#1fa2a2",
        status: false,
      })
    );
  };
  const deleteLogo = () => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: detailLogo._id,
    });
  };
  const hideModalListLogo = () => {
    setShowModalListLogo(false);
    dispatch(setDetailLogo({}));
  };
  const hideModalEditLogo = () => {
    setShowModalEditLogo(false);
  };
  const editLogo = () => {
    setShowModalEditLogo(true);
  };
  const changePositionLogo = (key, value) => {
    let temp = { ...positionLogo };
    temp[key] = value;
    setPositionLogo(temp);
  };
  const saveEditLogo = () => {
    setShowModalListLogo(false);
    setShowModalEditLogo(false);
    dispatch(
      getDefaultBrandNameSuccess({
        media_config: {
          ...positionLogo,
          width: positionLogo.width ? positionLogo.width : 150,
          height: positionLogo.height ? positionLogo.height : 150,
        },
        media_url: detailLogo.media_url,
        theme_color: defaultBrandName.theme_color,
      })
    );
  };
  const submit = () => {
    dispatch(uploadLogo(defaultBrandName));
  };
  useEffect(() => {
    dispatch(getListLogo());
    // eslint-disable-next-line
  }, []);

  return (
    <>
      <Row style={{ padding: "1rem" }}>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("common.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem> {t("brandName.brandname")} </BreadcrumbItem>
        </Breadcrumb>
      </Row>
      <CustomContent justify="center" color={defaultBrandName.theme_color}>
        <ModalConfimDelete
          title={t("brandName.thisPic")}
          decs={t("brandName.des")}
          methodDelete={deleteLogoById}
          dataDelete={dataDelete}
          isLoading={showLoadingScreen}
        />
        <Col span={12}>
          <Row>
            <Col className="tag" span={1}>
              <p> Logo </p>
            </Col>
            <CustomSection span={23}>
              <Row justify="center" style={{ marginBottom: "1rem" }}>
                <Col span={6}>
                  <AvatarEditor
                    style={{
                      border: "1px solid lightgrey",
                      borderRadius: defaultBrandName.media_config.borderRadius,
                    }}
                    position={defaultBrandName.media_config.position}
                    scale={defaultBrandName.media_config.scale}
                    border={defaultBrandName.media_config.border}
                    width={150}
                    height={150}
                    image={defaultBrandName.media_url}
                  />
                </Col>
              </Row>
              <Row justify="center">
                <Upload
                  action={BASE_URL_API + "upload-file"}
                  method={"post"}
                  showUploadList={false}
                  headers={{
                    Authorization: localStorage.getItem("setting_accessToken"),
                  }}
                  data={{
                    obj: "brand_name",
                  }}
                  beforeUpload={(file) => {
                    let regex = new RegExp('[!@#$%^&*(),?":{}|<>]');
                    const isLt5M = file.size / 1024 / 1024 < 5;
                    if (!isLt5M) {
                      Notification("error", "File must smaller than 5MB!");
                      return isLt5M || Upload.LIST_IGNORE;
                    }
                    if (file.name.match(regex)) {
                      Notification(
                        "error",
                        "Tên file không được chứa kí tự đặc biệt"
                      );
                      return Upload.LIST_IGNORE;
                    } else if (
                      file.type.indexOf("image") === -1 ||
                      file.type.indexOf("svg") > -1
                    ) {
                      Notification(
                        "error",
                        "Vui lòng chọn file định dạng hình ảnh"
                      );
                      return Upload.LIST_IGNORE;
                    } else {
                      return true;
                    }
                  }}
                  onChange={(info) => {
                    if (info.file.status === "uploading") {
                      dispatch(setShowLoadingScreen(true));
                    }
                    if (info.file.status === "done") {
                      uploadAttachFile(info.file.response);
                    } else if (info.file.status === "error") {
                      dispatch(setShowLoadingScreen(false));
                      Notification("error");
                    }
                  }}
                  progress={{
                    strokeColor: {
                      "0%": "#108ee9",
                      "100%": "#87d068",
                    },
                    strokeWidth: 3,
                    format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
                  }}
                  accept="image/png, image/jpeg, image/jpg"
                >
                  <Button
                    className="add-logo"
                    disabled={!checkRule("create")}
                    // icon={<PlusOutlined />}
                  >
                    <img src={uploadIcon} alt="Upload icom" />
                    <span>{t("brandName.changeLogo")}</span>
                  </Button>
                </Upload>
                <Button
                  className="add-logo"
                  // icon={<EditOutlined />}
                  onClick={() => setShowModalListLogo(true)}
                  disabled={!checkRule("edit")}
                >
                  <img src={editIcon} alt="Edit icon" />
                  <span>{t("brandName.edit")}</span>
                </Button>
              </Row>
            </CustomSection>
          </Row>
          <Row style={{ marginTop: "1rem" }} justify="space-around">
            <Col className="tag" span={1}>
              <p> {t("brandName.theme")} </p>
            </Col>
            <CustomSection
              span={23}
              //  style={{ padding: "8.5vh" }}
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Row style={{ width: "500px" }}>
                {THEME_COLOR.map((item, index) => {
                  return (
                    <Col
                      span={4}
                      style={{ padding: "0.75rem 0.5rem" }}
                      key={index}
                    >
                      <CustomItemColor
                        color={item.color}
                        className={"border-theme"}
                        selected={
                          item.color === defaultBrandName.theme_color
                            ? true
                            : false
                        }
                        onClick={() => dispatch(changeThemeColor(item.color))}
                      >
                        <ThemeColor
                          selected={
                            item.color === defaultBrandName.theme_color
                              ? true
                              : false
                          }
                          color={item.color}
                          colorHover={item.darker}
                        />
                      </CustomItemColor>
                    </Col>
                  );
                })}
              </Row>
            </CustomSection>
          </Row>
          {(checkRule("create") || checkRule("edit")) && (
            <Row justify={"end"} className={"button-save"}>
              <Col span={4}>
                <ThemeButton
                  label={t("common.save")}
                  clickButton={() => submit()}
                />
              </Col>
            </Row>
          )}
        </Col>
        <CustomModal
          title={t("brandName.choosePicture")}
          detailLogo={detailLogo}
          footer={[
            <Button
              className={"delete-logo"}
              key="back"
              onClick={() => deleteLogo()}
              disabled={!checkRule("delete")}
            >
              {t("brandName.delete")}
            </Button>,
            <Button
              className={"delete-logo"}
              key="submit"
              onClick={() => editLogo()}
            >
              {t("brandName.choose")}
            </Button>,
          ]}
          onCancel={() => hideModalListLogo()}
          visible={showModalListLogo}
        >
          <Row justify={"start"}>
            {listLogo &&
              listLogo.map((item, index) => {
                return (
                  <CustomLogo
                    className={"list-logo"}
                    key={index}
                    onClick={() => dispatch(setDetailLogo(item))}
                    selected={detailLogo._id === item._id ? true : false}
                  >
                    <img alt="" src={item.media_url} height={70} />
                  </CustomLogo>
                );
              })}
          </Row>
        </CustomModal>
        <CustomModal
          visible={showModalEditLogo}
          onOk={() => saveEditLogo()}
          onCancel={() => hideModalEditLogo()}
        >
          <Row justify="space-between" style={{ marginTop: "2rem" }}>
            <Col span={10}>
              <AvatarEditor
                style={{
                  border: "1px solid lightgrey",
                  borderRadius: positionLogo.borderRadius,
                }}
                position={positionLogo.position}
                onPositionChange={(e) => changePositionLogo("position", e)}
                scale={positionLogo.scale}
                border={positionLogo.border}
                width={positionLogo.width ? positionLogo.width : 150}
                height={positionLogo.height ? positionLogo.height : 150}
                borderRadius={
                  positionLogo.borderRadius &&
                  parseInt(
                    positionLogo.borderRadius.substring(
                      0,
                      positionLogo.borderRadius.length - 1
                    )
                  )
                }
                image={detailLogo.media_url}
              />
            </Col>
            <Col span={12}>
              <Row>
                <Col span={5}>
                  <p> Zoom: </p>
                </Col>
                <Col span={17}>
                  <Slider
                    value={positionLogo.scale}
                    onChange={(e) => changePositionLogo("scale", e)}
                    min={1}
                    max={2}
                    step={0.1}
                  />
                </Col>
              </Row>
              <Row>
                <Col span={5}>
                  <p> Border radius</p>
                </Col>
                <Col span={17}>
                  <Slider
                    onChange={(e) =>
                      changePositionLogo("borderRadius", e + "%")
                    }
                    min={0}
                    max={50}
                    step={1}
                  />
                </Col>
              </Row>
              <Row>
                <Col span={5}>
                  <p> Width: </p>
                </Col>
                <Col span={17}>
                  <InputNumber
                    max={70}
                    min={1}
                    value={positionLogo.width}
                    onChange={(e) => changePositionLogo("width", e)}
                  />
                </Col>
              </Row>
              <Row>
                <Col span={5}>
                  <p> Height: </p>
                </Col>
                <Col span={17}>
                  <InputNumber
                    max={70}
                    min={1}
                    value={positionLogo.height}
                    onChange={(e) => changePositionLogo("height", e)}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </CustomModal>
      </CustomContent>
    </>
  );
};

export default BrandName;

export const CustomSection = styled(Col)`
  background: #ffffff;
  padding: 2rem;
  border-radius: 10px;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
  text-align: center;
  .logo {
    height: 10rem;
    border-radius: 50%;
    border: 2px solid #f5f1f1;
  }

  .add-logo {
    /* border-radius: 20px; */
    /* margin-right: 1rem; */
    display: flex;
    align-items: center;
    justify-content: center;
    width: 136px;
    margin: 0 8px;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    text-align: center;
    color: #2c2c2c;
    border: 1px solid #d9d9d9;

    border-radius: 2px;
    transition: all 0.5s;

    img {
      margin-right: 8px;
    }
    &:hover {
      img {
        filter: brightness(200);
      }
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};
      color: #ffffff;
    }
  }
`;

export const CustomContent = styled(Row)`
  padding: 0rem 2rem 2rem 2rem;
  .tag {
    box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
    background-color: ${(props) => props.color};
    text-align: center;
    height: 100%;
    padding: 6rem 0rem 5rem 0rem;
    border-radius: 10px 0px 0px 10px;
    margin: 2rem 0rem 1rem 0rem;
    p {
      transform: rotate(270deg);
      color: white;
      white-space: nowrap;
    }
  }
  .button-save {
    margin-top: 1rem;
    button {
      width: 100%;
      border: 1px solid ${(props) => props.color};
    }
  }
`;

export const ThemeColor = styled.div`
  background-color: ${(props) => props.color};
  height: 1.5rem;
  width: 1.5rem;
  margin: auto;
  border-radius: 2rem 1rem 2rem 1rem;
  transform: ${(props) => (props.selected ? "rotate(45deg)" : "rotate(0deg)")};
  transition: all 0.5s;
  /* &:hover {
    transform: rotate(45deg);
    background-color: ${(props) => props.colorHover};
  } */
`;

export const CustomModal = styled(Modal)`
  .anticon-close {
    padding: 0.5rem;
    background-color: #1fa2a2;
    border-radius: 50%;
    color: white;
  }
  .delete-logo {
    display: ${(props) =>
      props.detailLogo && props.detailLogo._id ? "initial" : "none"};
  }
  .remove-logo {
    text-align: right;
    margin: 0;
  }
`;

export const CustomLogo = styled.div`
  border: 1px solid #e1dede;
  margin: 0rem 0.9rem 0rem 0.9rem;
  padding: 0.3rem;
  border-radius: 10px;
  background-color: ${(props) => (props.selected ? "#edebeb" : "white")};
  // img {
  //     height: 50px;
  //     width: 50px;
  // }
`;

export const CustomItemColor = styled(Col)`
  /* padding: 0.5rem 1.2rem 0.5rem 1.2rem; */
  padding: 10px 0px;
  /* border: 1px solid lightgrey; */
  border-radius: 10px;
  text-align: center;
  /* box-shadow: ${(props) =>
    props.selected ? "inset 0px 0px 19px rgb(32 162 162 / 49%)" : "white"};
  &:hover {
    box-shadow: inset 0px 0px 19px rgb(32 162 162 / 49%);
  } */
  border: ${(props) =>
    props.selected ? `1px solid ${props.color}` : "1px solid lightgrey"};
  box-shadow: ${(props) =>
    props.selected ? `inset 0px 0px 19px ${props.color}` : "white"};
  transition: all 0.5s;
  &:hover {
    box-shadow: inset 0px 0px 19px ${(props) => props.color};
    border-color: ${(props) => props.color};
    div {
      transform: rotate(45deg);
    }
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
