import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import Collapse from "antd/lib/collapse";
import Menu from "antd/lib/menu";
import { CaretRightOutlined } from "@ant-design/icons";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Dropdown from "antd/lib/dropdown";

import {
  showHighlightSetting,
  createSetting,
  updateSetting,
} from "redux/slices/highlightSetting";
import { loadListObjectField } from "redux/slices/objects";

function ModalSetting({ dataEdit, setDataEdit }) {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const typeHighlight = [
    {
      label: t("highlightSetting.row"),
      value: "row",
    },
    {
      label: t("highlightSetting.cell"),
      value: "cell",
    },
  ];

  const fontWeight = [
    {
      label: t("highlightSetting.light"),
      value: "300",
    },
    {
      label: t("highlightSetting.normal"),
      value: "400",
    },
    {
      label: t("highlightSetting.bold"),
      value: "700",
    },
  ];

  const colors = [
    "#E53935",
    "#D81B60",
    "#8E24AA",
    "#5E35B1",
    "#3949AB",
    "#1E88E5",
    "#039BE5",
    "#00ACC1",
    "#00897B",
    "#43A047",
    "#7CB342",
    "#C0CA33",
    "#FDD835",
    "#FFB300",
    "#FB8C00",
    "#F4511E",
    "#6D4C41",
    "#757575",
    "#fff",
    "#000",
  ];

  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showSetting } = useSelector((state) => state.highlightSettingReducer);

  const [showColor, setShowColor] = useState([]);
  const [showBackground, setShowBackground] = useState([]);
  const [listObjects, setListObjects] = useState([]);
  const [listFields, setListFields] = useState([]);
  const [selectedField, setSelectedField] = useState([]);

  const [style, setStyle] = useState([]);

  const _onSubmit = (values) => {
    let resultStyle = [];
    style.forEach((item) => {
      resultStyle.push({
        ...item,
        font_size: item.font_size.toString(),
      });
    });

    if (Object.keys(dataEdit).length > 0) {
      dispatch(
        updateSetting({
          name: values.name,
          field_id: values.field_id,
          object_id: values.object,
          type: values.type,
          style: [...resultStyle],
          id: dataEdit._id,
        })
      );
    } else {
      dispatch(
        createSetting({
          name: values.name,
          field_id: values.field_id,
          object_id: values.object,
          type: values.type,
          style: [...resultStyle],
        })
      );
    }
  };

  const _onCancel = () => {
    dispatch(showHighlightSetting(false));
    form.resetFields();
    setStyle([]);
    setSelectedField([]);
    setShowColor([]);
    setShowBackground([]);
    setListFields([]);
    setDataEdit({});
  };

  const renderColor = (type, idx) => {
    return (
      <CustomMenu>
        <Row>
          {colors.map((color) => {
            return (
              <Col key={color}>
                <CustomColor
                  onClick={() => {
                    handleChangeStyle(color, idx, type);

                    if (type === "background") {
                      handleShowBackgroundColor(idx, false);
                    }

                    if (type === "color") {
                      handleShowColor(idx, false);
                    }
                  }}
                  color={color}
                />
              </Col>
            );
          })}
        </Row>
      </CustomMenu>
    );
  };

  const handleShowColor = (idx, value) => {
    let tempShowColor = [...showColor];
    tempShowColor[idx] = value;
    setShowColor(tempShowColor);
  };

  const handleShowBackgroundColor = (idx, value) => {
    let tempShowColor = [...showBackground];
    tempShowColor[idx] = value;
    setShowBackground(tempShowColor);
  };

  const handleSelectObject = (value) => {
    setStyle([]);
    setSelectedField([]);
    setShowColor([]);
    setShowBackground([]);

    form.setFieldsValue({
      field_id: undefined,
    });

    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: value,
        show_meta_fields: false,
      })
    );
  };

  const handleSelectField = (value) => {
    setSelectedField(value.option);

    let tempStyle = [];
    let tempShowColor = [];
    value.option.forEach((item) => {
      tempStyle.push({
        value: item.value,
        font_weight: "400",
        color: "#000",
        background: "#fff",
        font_size: 14,
      });
      tempShowColor.push(false);
    });
    setStyle(tempStyle);
    setShowColor(tempShowColor);
    setShowBackground(tempShowColor);
  };

  const handleChangeStyle = (value, idx, type) => {
    let tempStyle = [...style];
    tempStyle[idx] = {
      ...tempStyle[idx],
      [type]: value,
    };
    setStyle(tempStyle);
  };

  useEffect(() => {
    if (Object.keys(category).length > 0) {
      let tempObjects = [];
      Object.entries(category).forEach(([key, val]) => {
        val.forEach((object) => {
          if (object.Status) {
            tempObjects.push({
              label: object.Name,
              value: object._id,
            });
          }
        });
      });
      setListObjects(tempObjects);
    }
  }, [category]);

  useEffect(() => {
    let tempOptionsFields = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                field.type === "select"
              ) {
                tempOptionsFields.push({
                  label: field.related_name,
                  value: field.full_field_id,
                  option: field.option,
                });
              }
            });
          });
        }
      }
    });

    setListFields(tempOptionsFields);
  }, [listObjectField, dataEdit]);

  useEffect(() => {
    if (!showSetting) {
      form.resetFields();
      setStyle([]);
      setSelectedField([]);
      setShowColor([]);
      setShowBackground([]);
      setListFields([]);
      setDataEdit({});
    }
  }, [form, setDataEdit, showSetting]);

  useEffect(() => {
    if (Object.keys(dataEdit).length > 0) {
      form.setFieldsValue({
        name: dataEdit.name,
        object: dataEdit.object_details._id,
        type: dataEdit.type,
        field_id: dataEdit.field_id,
      });

      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: dataEdit.object_details._id,
          show_meta_fields: false,
        })
      );

      let tempStyle = [];
      let tempShowColor = [];
      dataEdit.field_details.option.forEach((item) => {
        tempShowColor.push(false);

        const temp = dataEdit.style.find((ele) => ele.value === item.value);
        if (temp) {
          tempStyle.push({
            ...temp,
            font_size: parseInt(temp.font_size),
          });
        } else {
          tempStyle.push({
            value: item.value,
            font_weight: "400",
            color: "#000",
            background: "#fff",
            font_size: 14,
          });
        }
      });

      setSelectedField(dataEdit.field_details.option);
      setShowColor(tempShowColor);
      setShowBackground(tempShowColor);
      setStyle(tempStyle);
    }
  }, [dataEdit, dispatch, form]);

  return (
    <ModalCustom
      title={t("highlightSetting.highlightSetting")}
      visible={showSetting}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("highlightSetting.name")}
          name="name"
          rules={[{ required: true, message: t("common.placeholderInput") }]}
        >
          <Input placeholder={t("common.placeholderInput")} />
        </Form.Item>
        <Form.Item
          label={t("common.selectObject")}
          name="object"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            optionFilterProp="label"
            showSearch
            options={listObjects}
            placeholder={t("common.placeholderSelect")}
            onChange={handleSelectObject}
            disabled={Object.keys(dataEdit).length > 0}
          />
        </Form.Item>

        <Form.Item
          label={t("highlightSetting.selectType")}
          name="type"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={typeHighlight}
            placeholder={t("common.placeholderSelect")}
          />
        </Form.Item>

        <Form.Item
          label={t("highlightSetting.selectField")}
          name="field_id"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            optionFilterProp="label"
            showSearch
            options={listFields}
            placeholder={t("common.placeholderSelect")}
            onChange={(value, option) => handleSelectField(option)}
          />
        </Form.Item>

        {selectedField.length > 0 && (
          <Collapse
            expandIcon={({ isActive }) => (
              <CaretRightOutlined rotate={isActive ? -90 : 90} />
            )}
          >
            {selectedField.map((item, idx) => {
              return (
                <PanelCustom header={item.label} key={item.value}>
                  <Wrap>
                    <LabelName>{t("highlightSetting.textColor")}</LabelName>
                    <Value>
                      <Dropdown
                        overlay={renderColor("color", idx)}
                        trigger="click"
                        visible={showColor[idx]}
                        onVisibleChange={() => {
                          handleShowColor(idx, !showColor[idx]);
                        }}
                      >
                        <WrapColor>
                          <Color color={style[idx].color} />
                        </WrapColor>
                      </Dropdown>
                    </Value>
                  </Wrap>

                  <Wrap>
                    <LabelName>{t("highlightSetting.background")}</LabelName>
                    <Value>
                      <Dropdown
                        overlay={renderColor("background", idx)}
                        trigger="click"
                        visible={showBackground[idx]}
                        onVisibleChange={() =>
                          handleShowBackgroundColor(idx, !showBackground[idx])
                        }
                      >
                        <WrapColor>
                          <Color color={style[idx].background} />
                        </WrapColor>
                      </Dropdown>
                    </Value>
                  </Wrap>

                  <Wrap>
                    <LabelName>{t("highlightSetting.fontSize")}</LabelName>
                    <Value>
                      <InputNumber
                        placeholder={t("common.placeholderInput")}
                        onChange={(value) =>
                          handleChangeStyle(value, idx, "font_size")
                        }
                        max={20}
                        min={12}
                        value={style[idx].font_size}
                      />
                    </Value>
                  </Wrap>

                  <Wrap>
                    <LabelName>{t("highlightSetting.fontWeight")}</LabelName>
                    <Value>
                      <Select
                        options={fontWeight}
                        placeholder={t("common.placeholderSelect")}
                        onChange={(value) =>
                          handleChangeStyle(value, idx, "font_weight")
                        }
                        value={style[idx].font_weight}
                      />
                    </Value>
                  </Wrap>
                </PanelCustom>
              );
            })}
          </Collapse>
        )}

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const { Panel } = Collapse;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-collapse {
    border: none;
    background: #fff;
  }

  .ant-collapse-item {
    border-bottom: none;
    margin-bottom: 16px;
  }

  .ant-collapse-content {
    border-top: none;
  }

  .ant-collapse-content-box {
    background: #fafafa;
    margin-top: 16px;
    display: flex;
    flex-wrap: wrap;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;

const PanelCustom = styled(Panel)`
  .ant-collapse-header {
    background: #f0f0f0;
    border: 1px solid #ececec;
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #252424 !important;
    padding: 2.5px 16px !important;
  }
`;

const WrapColor = styled.div`
  padding: 4px;
  background: #f2f2f2;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  width: 90%;
`;

const Color = styled.div`
  height: 30px;
  width: 100%;
  background-color: ${(props) => props.color};
  cursor: pointer;

  &:hover {
    border: 2px solid white;
  }
`;

const CustomColor = styled.div`
  height: 24px;
  width: 24px;
  background-color: ${(props) => props.color};
  cursor: pointer;

  &:hover {
    border: 2px solid white;
  }
`;
const CustomMenu = styled(Menu)`
  padding: 8px;
  width: 256px;
`;

const Wrap = styled.div`
  width: 50%;
  display: flex;
  align-items: center;

  :nth-child(3) {
    margin-top: 10px;
  }

  :nth-child(4) {
    margin-top: 10px;
  }
`;

const LabelName = styled.div`
  font-size: 16px;
  width: 40%;
`;

const Value = styled.div`
  width: 60%;

  .ant-input-number {
    width: 90%;
  }

  .ant-select {
    width: 90%;
  }
`;
