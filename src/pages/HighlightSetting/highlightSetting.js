import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import Switch from "antd/lib/switch";
import Pagination from "antd/lib/pagination";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import ModalSetting from "./ModalSetting";

import {
  WrapBreadcrumb,
  BreadcrumbItem,
  AddButtonBreadcrumb,
} from "components/Style/common";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadDataNecessary,
  showHighlightSetting,
  loadAllSetting,
  changeStatus,
  deleteSetting,
} from "redux/slices/highlightSetting";
import { changeTitlePage } from "redux/slices/authenticated";

function HighlightSetting(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;
  const { Text } = Typography;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { allSetting, totalSetting } = useSelector(
    (state) => state.highlightSettingReducer
  );

  const [listSetting, setListSetting] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [dataDelete, setDataDelete] = useState({});
  const [dataEdit, setDataEdit] = useState({});

  useEffect(() => {
    dispatch(changeTitlePage(t("highlightSetting.highlightSetting")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "highlight_setting" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleSelectPage = (e) => {
    setCurrentPage(e);
  };

  const _onSizeChange = (current, pageSize) => {
    setRecordPerPage(pageSize);
  };

  const showTotal = () => {
    return `${t("common.total")} ${totalSetting} ${t("common.items")}`;
  };

  const handleActive = (checked, id) => {
    dispatch(
      changeStatus({
        status: checked,
        id: id,
      })
    );
  };

  const _onDeleteSetting = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: id });
  };

  const getListSetting = useCallback(() => {
    dispatch(
      loadAllSetting({
        current_page: currentPage,
        record_per_page: recordPerPage,
      })
    );
  }, [dispatch, currentPage, recordPerPage]);

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    getListSetting();
  }, [getListSetting]);

  useEffect(() => {
    let tempList = [];
    allSetting.map((item) => {
      return tempList.push({
        key: item._id,
        active: item.status,
        name: item.name,
        object: item.object_details.Name,
        field: item.field_details.name,
        type: item.type === "row" ? "Row" : "Cell",
        create_date: item.created_date,
        action: "",
        fullData: item,
      });
    });

    setTimeout(() => {
      setListSetting(tempList);
    }, 50);
  }, [dispatch, allSetting]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {t("highlightSetting.highlightSetting")}
          </BreadcrumbItem>
        </Breadcrumb>
        {listSetting.length > 0 && checkRule("create") && (
          <AddButtonBreadcrumb
            onClick={() => dispatch(showHighlightSetting(true))}
          >
            + {t("highlightSetting.add")}
          </AddButtonBreadcrumb>
        )}
      </WrapBreadcrumb>

      {listSetting.length === 0 ? (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")}{" "}
            <span> {t("highlightSetting.highlightSetting")}</span>
          </p>
          {checkRule("create") && (
            <AddButtonBreadcrumb
              onClick={() => dispatch(showHighlightSetting(true))}
            >
              + {t("highlightSetting.add")}
            </AddButtonBreadcrumb>
          )}
        </Empty>
      ) : (
        <WrapTable>
          <Table
            pagination={false}
            dataSource={listSetting}
            scroll={{ x: "max-content" }}
          >
            <Column
              title={t("highlightSetting.active")}
              dataIndex="active"
              key="active"
              width="120px"
              render={(text, record) => (
                <Switch
                  disabled={!checkRule("edit")}
                  checkedChildren={t("workflow.on")}
                  unCheckedChildren={t("workflow.off")}
                  checked={text}
                  onChange={(checked) => handleActive(checked, record.key)}
                />
              )}
            />
            <Column
              title={t("highlightSetting.name")}
              dataIndex="name"
              key="name"
              width="300px"
              sorter={(a, b) => a.name.localeCompare(b.name)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("object.object")}
              dataIndex="object"
              key="object"
              sorter={(a, b) => a.object.localeCompare(b.object)}
              width="200px"
            />
            <Column
              title={t("highlightSetting.field")}
              dataIndex="field"
              key="field"
              width="300px"
              sorter={(a, b) => a.field.localeCompare(b.field)}
              render={(text) => (
                <Text ellipsis={{ tooltip: text }}>{text}</Text>
              )}
            />
            <Column
              title={t("highlightSetting.type")}
              dataIndex="type"
              key="type"
              sorter={(a, b) => a.type.localeCompare(b.type)}
              width="200px"
            />
            <Column
              title={t("common.createdDate")}
              dataIndex="create_date"
              key="create_date"
              width="300px"
            />
            {(checkRule("edit") || checkRule("delete")) && (
              <Column
                title={t("common.action")}
                dataIndex="action"
                key="action"
                fixed="right"
                width="150px"
                render={(text, record) => (
                  <WrapAction>
                    {checkRule("edit") && (
                      <Tooltip title={t("common.edit")}>
                        <img
                          onClick={() => {
                            setDataEdit(record.fullData);
                            dispatch(showHighlightSetting(true));
                          }}
                          src={Edit}
                          alt="edit"
                        />
                      </Tooltip>
                    )}
                    {checkRule("delete") && (
                      <Tooltip title={t("common.delete")}>
                        <img
                          onClick={() => _onDeleteSetting(record.key)}
                          src={Delete}
                          alt="delete"
                        />
                      </Tooltip>
                    )}
                  </WrapAction>
                )}
              />
            )}
          </Table>
          <Pagination
            showTotal={showTotal}
            current={currentPage}
            pageSize={recordPerPage}
            total={totalSetting}
            onChange={handleSelectPage}
            showSizeChanger
            onShowSizeChange={_onSizeChange}
            showQuickJumper
          />
        </WrapTable>
      )}

      <ModalSetting dataEdit={dataEdit} setDataEdit={setDataEdit} />

      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteSetting}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default withTranslation()(HighlightSetting);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #fafafa;
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    padding: 16px 24px 0 16px;
    display: flex;
    justify-content: flex-end;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;
