import { Button, Form, Modal, Select } from "antd";
import { default as React, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { createRelatedObject } from "../../../redux/slices/relatedObject";
import { useTranslation, withTranslation } from "react-i18next";
const { Option, OptGroup } = Select;

const ModalRelatedObject = (props) => {
  const { open, setOpen } = props;
  const [form] = Form.useForm();
  const { category } = useSelector((state) => state.objectsManagementReducer);
  const { t } = useTranslation();
  const { isLoading } = useSelector((state) => state.relatedObjectReducer);

  const dispatch = useDispatch();

  const onFinish = (values) => {
    dispatch(
      createRelatedObject({
        Source_Object: values.target,
        Target_Object: values.source,
      })
    );
  };

  useEffect(() => {
    if (isLoading === false) {
      form.resetFields();
      setOpen(false);
    }
    // eslint-disable-next-line
  }, [isLoading]);

  return (
    <CustomModal
      title={t("relatedObject.addNew")}
      visible={open}
      onOk={() => onFinish()}
      onCancel={() => {
        form.resetFields();
        setOpen(false);
      }}
      width={600}
      footer={false}
    >
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={() => {}}
        autoComplete="off"
      >
        <Form.Item
          label={t("relatedObject.sourceObject")}
          name="target"
          rules={[{ required: true, message: "Please select source object!" }]}
        >
          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {Object.entries(category).map(([key, value], idx) => {
              return (
                <OptGroup label={key}>
                  {/* eslint-disable-next-line */}
                  {value.map((object, index) => {
                    if (object.Status)
                      return <Option value={object._id}>{object.Name}</Option>;
                  })}
                </OptGroup>
              );
            })}
          </Select>
        </Form.Item>
        <Form.Item
          label={t("relatedObject.targetObject")}
          name="source"
          rules={[{ required: true, message: "Please select target object!" }]}
        >
          <Select
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {Object.entries(category).map(([key, value], idx) => {
              return (
                <OptGroup label={key}>
                  {/* eslint-disable-next-line */}
                  {value.map((object, index) => {
                    if (object.Status)
                      return <Option value={object._id}>{object.Name}</Option>;
                  })}
                </OptGroup>
              );
            })}
          </Select>
        </Form.Item>
        <CustomFooter>
          <CustomButtonSave size="large" htmlType="submit">
            {t("common.save")}
          </CustomButtonSave>
          <CustomButtonCancel
            size="large"
            onClick={() => {
              form.resetFields();
              setOpen(false);
            }}
          >
            {t("common.cancel")}
          </CustomButtonCancel>
        </CustomFooter>
      </Form>
    </CustomModal>
  );
};

export default withTranslation()(ModalRelatedObject);

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
