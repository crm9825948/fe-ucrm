// import React, { useState } from "react";
// import {
//   getBezierPath,
//   getEdgeCenter,
//   // getMarkerEnd,
// } from "react-flow-renderer";
// import { useDispatch, useSelector } from "react-redux";
// import { deleteRelated } from "redux/slices/relatedObject";
// import styled from "styled-components";
// import "./index.scss";
// import img from "assets/icons/common/confirm.png";
// import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
// const foreignObjectSize = 40;

// export default function CustomEdge({
//   id,
//   sourceX,
//   sourceY,
//   targetX,
//   targetY,
//   sourcePosition,
//   targetPosition,
//   style = {},
//   markerEnd,
// }) {
//   const dispatch = useDispatch();
//   const [open, setOpen] = useState(false);
//   const [data, setData] = useState({});
//   const onEdgeClick = (evt, id) => {
//     evt.stopPropagation();
//     // alert(`remove ${id}`);
//     let listId = id.split("-");
//     setData({
//       source_id: listId[2],
//       target_id: listId[1],
//     });

//     setOpen(true);
//     // dispatch(
//     //   deleteRelated({
//     //     source_id: listId[2],
//     //     target_id: listId[1],
//     //   })
//     // );
//   };

//   const edgePath = getBezierPath({
//     sourceX,
//     sourceY,
//     sourcePosition,
//     targetX,
//     targetY,
//     targetPosition,
//   });
//   const [edgeCenterX, edgeCenterY] = getEdgeCenter({
//     sourceX,
//     sourceY,
//     targetX,
//     targetY,
//   });

//   return (
//     <>
//       <path
//         id={id}
//         style={style}
//         className="react-flow__edge-path"
//         d={edgePath}
//         markerEnd={markerEnd}
//       />
//       <foreignObject
//         width={foreignObjectSize}
//         height={foreignObjectSize}
//         x={edgeCenterX - foreignObjectSize / 2}
//         y={edgeCenterY - foreignObjectSize / 2}
//         className="edgebutton-foreignobject"
//         requiredExtensions="http://www.w3.org/1999/xhtml"
//       >
//         <body>
//           <CustomButton
//             className="edgebutton"
//             onClick={(event) => onEdgeClick(event, id)}
//           >
//             ×
//           </CustomButton>
//         </body>
//       </foreignObject>
//       <ModalConfirm
//         title={"Bạn có chắc chắn thay đổi mối quan hệ này?"}
//         decs={"Sau khi thay đổi hệ thống không thể hoàn tác được!"}
//         open={open}
//         setOpen={setOpen}
//         method={deleteRelated}
//         data={data}
//         setOpenModal={() => {}}
//         img={img}
//       />
//     </>
//   );
// }

// const CustomButton = styled.button`
//   opacity: 0;
//   :hover {
//     opacity: 1;
//   }
// `;
