// import React, { useEffect, useState, useCallback } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { loadCategory } from "../../redux/slices/objectsManagement";
// import { loadRelatedObject } from "../../redux/slices/relatedObject";
// import { createRelatedObject } from "redux/slices/relatedObject";
// import { Breadcrumb, Button, Col, Row } from "antd";
// import ReactFlow, {
//   applyEdgeChanges,
//   applyNodeChanges,
//   MiniMap,
//   Controls,
//   addEdge,
//   MarkerType,
// } from "react-flow-renderer";
// import ButtonEdge from "./ButtonEdge.js";
// import { SmartBezierEdge } from "@tisoap/react-flow-smart-edge";
// import styled from "styled-components";
// import { useTranslation, withTranslation } from "react-i18next";
// import { useNavigate } from "react-router-dom";

// const NewRelatedObject = () => {
//   const dispatch = useDispatch();
//   const { relatedObject } = useSelector((state) => state.relatedObjectReducer);
//   const { t } = useTranslation();
//   const [nodes, setNodes] = useState([]);
//   const [edges, setEdges] = useState([]);
//   let navigate = useNavigate();
//   useEffect(() => {
//     dispatch(loadCategory());
//     dispatch(loadRelatedObject());
//   }, [dispatch]);

//   const edgeTypes = {
//     buttonedge: ButtonEdge,
//     // smart: SmartBezierEdge,
//   };

//   useEffect(() => {
//     let nodeList = [];
//     let edgeList = [];
//     relatedObject.forEach((item, idx) => {
//       item.Source_Object.forEach((edge, idx) => {
//         edgeList.push({
//           id: `reactflow__edge-${item.Target_Object}-${edge.id}`,
//           source: item.Target_Object,
//           sourceHandle: null,
//           target: edge.id,
//           targetHandle: null,
//           type: "buttonedge",
//           markerEnd: {
//             type: MarkerType.Arrow,
//           },
//           animated: true,
//         });
//       });
//       nodeList.push({
//         id: item.Target_Object,
//         type: "default",
//         data: {
//           label: item.target_object_name,
//         },
//         position: { x: 100 * (idx + 1), y: 100 * (idx + 1) },
//       });
//     });

//     setEdges(edgeList);
//     setNodes(nodeList);
//   }, [relatedObject]);

//   const onNodesChange = useCallback(
//     (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
//     [setNodes]
//   );
//   const onEdgesChange = useCallback(
//     (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
//     [setEdges]
//   );

//   const onConnect = useCallback((params) => {
//     setEdges((eds) => addEdge({ ...params, type: "buttonedge" }, eds));
//     dispatch(
//       createRelatedObject({
//         Source_Object: params.target,
//         Target_Object: params.source,
//         type: "diagram",
//       })
//     );
//   }, []);

//   return (
//     <>
//       {" "}
//       <CustomHeader>
//         <Breadcrumb>
//           <Breadcrumb.Item>
//             {/* eslint-disable-next-line  */}
//             <a onClick={() => navigate("/settings")}>Settings</a>
//           </Breadcrumb.Item>
//           <BreadcrumbItem>
//             {/* eslint-disable-next-line */}
//             <a onClick={() => navigate("/related-objects")}>
//               {t("relatedObject.relatedObject")}
//             </a>
//           </BreadcrumbItem>
//         </Breadcrumb>
//       </CustomHeader>
//       <ReactFlow
//         nodes={nodes}
//         edges={edges}
//         onNodesChange={onNodesChange}
//         onEdgesChange={onEdgesChange}
//         onConnect={onConnect}
//         edgeTypes={edgeTypes}
//         snapToGrid={true}
//         //   fitView
//       >
//         <MiniMap />
//         <Controls />
//       </ReactFlow>
//     </>
//   );
// };

// export default withTranslation()(NewRelatedObject);

// const BreadcrumbItem = styled(Breadcrumb.Item)`
//   font-family: var(--roboto-500);
//   font-size: 18px !important;
//   color: #2c2c2c;
//   cursor: default;
// `;

// const CustomHeader = styled.div`
//   position: absolute;
//   display: flex;
//   justify-content: space-between;
//   /* align-items: center; */
//   margin-bottom: 24px;
//   padding-top: 24px;
//   padding-left: 24px;
// `;

// const Wrapper = styled.div`
//   padding: 24px;
// `;
