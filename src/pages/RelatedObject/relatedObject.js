import { Breadcrumb, Button, Typography, Row, Col, Radio } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import plusIcon from "../../assets/icons/objectsManagement/plus.svg";
// import { loadCategory } from "../../redux/slices/objectsManagement";
import {
  deleteRelated,
  loadRelatedObject,
} from "../../redux/slices/relatedObject";
import ModalRelatedObject from "./Modal/modalRelatedObject";
import { useNavigate } from "react-router-dom";
import { useTranslation, withTranslation } from "react-i18next";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import picklistImg from "assets/images/picklist/Picklist dependency.png";
import _ from "lodash";
import { changeTitlePage } from "redux/slices/authenticated";
import DeleteImg from "assets/icons/common/deleteRelated.png";
const { Text: TextComponent } = Typography;

const RelatedObject = () => {
  const dispatch = useDispatch();
  const { relatedObject } = useSelector((state) => state.relatedObjectReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const [listRelated, setListRelated] = useState([]);
  const [target, setTarget] = useState("");
  const [source, setSource] = useState("");

  const [view, setView] = useState("table");

  useEffect(() => {
    let tempRelated = [];
    if (relatedObject.length > 0) {
      relatedObject.forEach((item) => {
        let tempSource = [];
        item.Source_Object.forEach((source) => {
          tempSource.push({ name: source.name, idSource: source.id });
        });
        tempRelated.push({
          name: item.target_object_name,
          children: tempSource,
          idTarget: item.Target_Object,
        });
      });
    }

    setListRelated(tempRelated);
  }, [relatedObject]);

  const { t } = useTranslation();
  const [open, setOpen] = useState(false);
  let navigate = useNavigate();

  useEffect(() => {
    dispatch(changeTitlePage(t("relatedObject.relatedObject")));
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    // dispatch(loadCategory());/
    dispatch(loadRelatedObject());
  }, [dispatch]);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "related_object" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const renderListRelated = (list) => {
    return list.map((item, index) => {
      return (
        <Content key={index}>
          <div class="tf-tree">
            <ul>
              <li>
                <span class="tf-nc">
                  <Parent>
                    <TextComponent
                      ellipsis={{
                        tooltip: item.name,
                      }}
                    >
                      {item.name}
                    </TextComponent>
                  </Parent>
                </span>
                <ul>
                  {item.children.map((chil, idx) => {
                    return (
                      <li key={idx}>
                        <span class="tf-nc">
                          <Children>
                            <TextComponent
                              ellipsis={{
                                tooltip: chil.name,
                              }}
                            >
                              {chil.name}
                            </TextComponent>
                            <div
                              className="btn-delete"
                              onClick={() => {
                                setSource(chil.idSource);
                                setTarget(item.idTarget);
                                dispatch(setShowModalConfirmDelete(true));
                              }}
                            >
                              <img src={DeleteImg} alt="delete" />
                            </div>
                          </Children>
                        </span>
                      </li>
                    );
                  })}
                </ul>
              </li>
            </ul>
          </div>
        </Content>
      );
    });
  };
  return (
    <Wrapper>
      <CustomHeader>
        <Breadcrumb>
          <Breadcrumb.Item>
            {/* eslint-disable-next-line  */}
            <a onClick={() => navigate("/settings")}>Settings</a>
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {/* eslint-disable-next-line */}
            <a onClick={() => navigate("/related-objects")}>
              {t("relatedObject.relatedObject")}
            </a>
          </BreadcrumbItem>
        </Breadcrumb>
      </CustomHeader>
      {checkRule("create") ? (
        <>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              width: "100%",
              alignItems: "center",
            }}
          >
            {listRelated.length > 0 && (
              <CustomOption
                value={view}
                onChange={(e) => {
                  setView(e.target.value);
                }}
              >
                <Radio value="diagram">Diagram view</Radio>
                <Radio value="table">Table view</Radio>
              </CustomOption>
            )}
            <CustomButton size="large" onClick={() => setOpen(true)}>
              <img alt="" src={plusIcon} />
              {t("relatedObject.addNew")}
            </CustomButton>
          </div>
        </>
      ) : (
        ""
      )}

      <ModalRelatedObject setOpen={setOpen} open={open} />

      {view === "diagram" ? (
        <ContentWrap>{renderListRelated(listRelated)}</ContentWrap>
      ) : (
        <CustomItem>
          <Row gutter={[16, 16]}>
            {relatedObject.length > 0 ? (
              relatedObject &&
              relatedObject.map((item, idx) => {
                return (
                  <Col span={4} xs={32} sm={12} md={8} lg={4}>
                    <CustomCol>
                      <div className="title">{item.target_object_name}</div>
                      <div className="content">
                        <span className="source">
                          {t("relatedObject.source")}:{" "}
                        </span>
                        {item.Source_Object.map((object, idx) => {
                          return (
                            <span className="decs">
                              {object.name}{" "}
                              {idx === item.Source_Object.length - 1
                                ? ""
                                : ", "}
                            </span>
                          );
                        })}
                      </div>
                    </CustomCol>
                  </Col>
                );
              })
            ) : (
              <div className="empty">
                <img alt="" src={picklistImg} />
                <div className="text">
                  There's no any <span>related objects</span>
                </div>
              </div>
            )}
          </Row>
        </CustomItem>
      )}
      <ModalConfimDelete
        title={t("relatedObject.thisRelated")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteRelated}
        dataDelete={{
          source_id: source,
          target_id: target,
        }}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};
export default withTranslation()(RelatedObject);

const Wrapper = styled.div`
  padding: 24px;
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .empty {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    width: 100%;
    > img {
      width: 100px;
      margin-top: 60px;
    }
    .text {
      margin-top: 8px;
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      span {
        color: ${(props) => props.theme.main};
      }
    }
  }
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  /* align-items: center; */
  margin-bottom: -35px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const ContentWrap = styled.div`
  margin-top: 16px;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  gap: 16px;
`;

const Content = styled.div`
  background: #ffffff;
  border: 1px solid #ececec;
  border-radius: 2px;
  flex: 0 0 calc(50% - 8px);
  overflow: auto;
  display: flex;
  justify-content: center;
  /* padding: 24px 0; */
  border-radius: 5px;

  .tf-tree {
    width: fit-content;
    padding: 24px 0;
  }

  .tf-tree li ul {
    margin-top: 32px;
    margin-bottom: 0;
  }

  .tf-tree .tf-nc {
    width: 220px;
    border: none;
    padding: 0px;
  }

  .tf-tree .tf-nc:after,
  .tf-tree .tf-nc:before {
    border-left: 1px solid #d8d8d8;
  }

  .tf-tree li li:before {
    border-top: 1px solid #d8d8d8;
  }
`;

const Parent = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
  text-align: center;
  background: ${(props) => props.theme.main};
  padding: 8px 16px;
  border-radius: 5px;
  border-color: 1px solid ${(props) => props.theme.main};
  font-size: 20px;
  line-height: 130%;
  font-family: var(--roboto-500);
  letter-spacing: 1px;

  .ant-typography {
    color: #ffffff;
  }
`;

const Children = styled.div`
  width: 100%;
  height: 100%;
  /* overflow: hidden; */
  text-overflow: ellipsis;
  white-space: nowrap;
  text-align: center;
  background: #ebeef0;
  padding: 8px 16px;
  border-radius: 5px;
  border-color: 1px solid #ebeef0;
  font-size: 16px;
  position: relative;
  cursor: pointer;
  :hover {
    .btn-delete {
      visibility: visible;
      opacity: 1;
    }
  }
  /* opacity: 0.6; */
  .ant-typography {
    color: #6b6b6b;
  }
  .btn-delete {
    visibility: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    opacity: 0;
    width: 20px;
    height: 20px;
    background: #d8e1e7;
    box-shadow: 0px 8px 16px #adb6bc;
    border-radius: 3px;
    position: absolute;
    right: 10px;
    top: -10px;
    cursor: pointer;
    transition: all 0.5s;
  }
`;

const CustomItem = styled.div`
  /* padding-right: 24px; */
  margin-top: 16px;
  margin-bottom: 32px;
`;

const CustomCol = styled.div`
  height: 100px;
  background: #ffffff;
  /* drop-shadow / 0.15 */

  box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
  border-radius: 5px;
  overflow: auto;
  img {
    width: 50px;
  }
  .title {
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 21px */

    letter-spacing: 0.01em;
    color: #ffffff;
    background: ${(props) => props.theme.main};
    padding: 9px;
  }
  .decs {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    /* or 16px */

    letter-spacing: 0.01em;

    /* text xám */

    color: #6b6b6b;
  }

  .source {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 16px */

    letter-spacing: 0.01em;

    /* text */

    color: #252424;
    margin-right: 13px;
  }

  .content {
    margin-top: 11px;
    margin-left: 10px;
  }
  position: relative;
  top: 0;
  transition: top ease 0.5s;
  &:hover {
    cursor: pointer;
    box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s;
    top: -8px;
  }
`;

const CustomOption = styled(Radio.Group)`
  span {
    font-size: 16px;
  }
`;
