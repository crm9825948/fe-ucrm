import {
  DeleteOutlined,
  DownOutlined,
  EditOutlined,
  EllipsisOutlined,
  MergeCellsOutlined,
  CaretDownOutlined,
} from "@ant-design/icons";
import {
  Button,
  Checkbox,
  Dropdown,
  Form,
  Input,
  Menu,
  Select,
  Tooltip,
  Typography,
  Breadcrumb,
  InputNumber,
  DatePicker,
} from "antd";
import ImgConfirm from "assets/icons/common/confirm.png";
import deleteImg from "assets/icons/common/delete-icon.png";
import editImg from "assets/icons/common/icon-edit.png";
import KanbanViewImg from "assets/icons/common/kanbanView.png";
import ListViewImg from "assets/icons/common/listView.png";
import ListViewDetailImg from "assets/icons/common/listViewDetail.png";
import KnowledgeImg from "assets/icons/common/knowledge.png";
import { checkTokenExpiration } from "contexts/TokenCheck";
import trashImg from "assets/icons/common/trash.png";
import pinImg from "assets/icons/objects/png.svg";
import unpinImg from "assets/icons/objects/unpin.svg";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import Call from "assets/icons/listview/Call_ListView.svg";
import CRMLogo from "assets/icons/common/crmlogo.png";
import IconTop from "assets/icons/common/iconTop.png";
import IconBottom from "assets/icons/common/iconBottom.png";
import Details from "assets/icons/users/details.svg";
import ImportExcel from "assets/icons/common/importExcel.png";
import ExportExcel from "assets/icons/common/downExcel.png";
import {
  default as ModalConfimDelete,
  default as ModalDelete,
} from "components/Modal/ModalConfirmDelete";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import { BASENAME, BASE_URL_API, BE_URL, FE_URL } from "constants/constants";
import moment from "moment";
import React, { useEffect, useMemo, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { runDynamicButton } from "redux/slices/dynamicButton";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { loadAllSetting } from "redux/slices/highlightSetting";
import {
  createRecord,
  deleteRecord,
  exportTemplate,
  loadDataWithOutPagi,
  loadFavouriteObjects,
  // loadFormCreate,
  loadListObjectField,
  loadRecordDataSuccess,
  massDelete,
  setLinkingFieldValue,
  setShowModal,
  toggleFavouriteObject,
  updateRecord,
  loadPagination,
  loadPaginationSuccess,
  loadCustomView,
  setLoadingModal,
  viewObjectSearch,
  globalSearch,
  updateIsSearchGlobal,
} from "redux/slices/objects";
import {
  getFieldsMappingCallCenter,
  getObjectsToCall,
  makeCall,
} from "redux/slices/callCenter";
import { deleteCampaign } from "redux/slices/campaign";

import styled from "styled-components";
import CustomView from "./customView/customView";
import ModalImport from "./modal/modalImport";
import ModalMassEdit from "./modal/modalMassEdit";
import ModalMerge from "./modal/modalMerge";
import ModalRecord from "./modal/modalRecord";
import { loadConfig } from "redux/slices/datetimeSetting";
import {
  getKnowledgeBaseSetting,
  updateIsFirst,
} from "redux/slices/knowledgeBaseSetting";
import { Notification } from "components/Notification/Noti";
import Carousel from "react-multi-carousel";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import axios from "axios";
import Highlighter from "react-highlight-words";
import ModalAddCampaign from "pages/Campaign/ModalAddCampaign";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import { Spin } from "antd";
import { useTranslation } from "react-i18next";
import { changeTitlePage } from "redux/slices/authenticated";
// import MenuPopup from "./MenuPopup";
import _ from "lodash";
import ModalRunDynamicButton from "components/Modal/ModalRunDynamicButton";

const { SubMenu } = Menu;
const { Search } = Input;

const NewObject = () => {
  const { t } = useTranslation();
  const { Text: TextComponent } = Typography;
  const { visibleList, objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { allObject } = useSelector((state) => state.tenantsReducer);
  const [flagDelete, setFlagDelete] = useState("");
  const { Option } = Select;
  const [dataConfirm, setDataConfirm] = useState({});
  const [form] = Form.useForm();
  const [showConfirm, setShowConfirm] = useState(false);
  const [visibleModalRDB, $visibleModalRDB] = useState(false);
  const { objectId, customViewId } = useParams();
  const [openMassEdit, setOpenMassEdit] = useState(false);
  const dispatch = useDispatch();

  const navigate = useNavigate();
  const [openMerge, setOpenMerge] = useState(false);
  const [listColumn, setListColumn] = useState([]);
  const [openCustomView, setOpenCustomView] = useState(false);
  const [dataSource, setDataSource] = useState([]);

  const [sortBy, setSortBy] = useState({});
  const [openImport, setOpenImport] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(50);

  const [listId, setListId] = useState([]);

  const [listCheck, setListCheck] = useState([]);

  const [idDelete, setIdDelete] = useState("");

  const [recordID, setRecordID] = useState("");

  const [allFields, setAllFields] = useState({});

  const [, setFormatDatetime] = useState("");

  const [, setFormatDate] = useState("");

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);

  const { allSetting } = useSelector((state) => state.highlightSettingReducer);
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { isKnowledgeView } = useSelector(
    (state) => state.enhancementViewReducer
  );
  const { showModalConfirmDelete } = useSelector(
    (state) => state.globalReducer
  );
  const [open, setOpen] = useState(false);

  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);

  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);

  const [, setMeta] = useState([]);
  const [, setDataSearch] = useState([]);
  const [notiGlobal, setNotiGlobal] = useState("");

  const {
    data,
    listObjectField,
    recordData,
    // fields,
    header,
    customView,
    objectsFavourite,
    isLoadingDataWithOutPagi,
    totalRecord,
    isLoadingPagi,
    isLoadingModal,
    objectGlobalSearch,
    isSeacrhGlobal,
    // linkingFieldValue,
  } = useSelector((state) => state.objectsReducer);
  const [editingKey, setEditingKey] = useState("");
  const { userDetail } = useSelector((state) => state.userReducer);

  const [indeterminate, setIndeterminate] = useState(false);

  const [checkAll, setCheckAll] = useState(false);

  const [listSearch, setListSearch] = useState([]);

  const [write, setWrite] = useState(false);
  const [read, setRead] = useState(false);
  const [update, setUpdate] = useState(false);
  const [deleteRole, setDeleteRole] = useState(false);
  const [showModalCampaign, $showModalCampaign] = useState(false);
  const [linkIframe, $linkIframe] = useState("");
  const [isKnowledge, setIsKnowledge] = useState(false);
  const [formSearch] = Form.useForm();

  useEffect(() => {
    if (Object.entries(objectGlobalSearch).length > 0) {
      if (objectGlobalSearch.searchable_fields.length > 0) {
        let noti = "Only ";
        objectGlobalSearch.searchable_fields.forEach((field, index) => {
          noti += `${
            index === objectGlobalSearch.searchable_fields.length - 1
              ? field.name
              : `${field.name},`
          } `;
        });
        noti += `${
          objectGlobalSearch.searchable_fields.length > 2 ? "are" : "is"
        } searchable.`;
        setNotiGlobal(noti);
      } else {
        setNotiGlobal(
          "Global search is not available. Please contact the administrator to index data and enable searching."
        );
      }
    }
  }, [objectGlobalSearch]);

  useEffect(() => {
    let meta = [];
    let data = [];
    // eslint-disable-next-line
    listSearch.map((item, idx) => {
      if (
        item.id_field === "created_date" ||
        item.id_field === "created_by" ||
        item.id_field === "modify_time" ||
        item.id_field === "owner" ||
        item.id_field === "modify_by"
      ) {
        meta.push(item);
      } else {
        data.push(item);
      }
    });
    setMeta(meta);
    setDataSearch(data);
  }, [listSearch]);

  useEffect(() => {
    dispatch(changeTitlePage("List view"));
    //eslint-disable-next-line
  }, []);

  useEffect(() => {
    let temp = false;
    listKnowledgeSetting.forEach((setting) => {
      if (
        !isKnowledgeView &&
        setting.article_setting.obj_article === objectId
      ) {
        navigate(`/knowledge-base-enhancement/${objectId}`);
      }
      if (setting.article_setting.obj_article === objectId) {
        temp = true;
      }
    });
    setIsKnowledge(temp);
  }, [listKnowledgeSetting, objectId, navigate, isKnowledgeView]);

  const { editKnowledgeBaseSetting, isFirst } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );

  const { fieldsMappingCallCenter, objectsToCall } = useSelector(
    (state) => state.callCenterReducer
  );

  const { reportPermission } = useSelector((state) => state.userReducer);

  const checkReportPermission = (type, object) => {
    return _.get(reportPermission, `${object}.${type}`, false);
  };

  useEffect(() => {
    dispatch(loadConfig());
    dispatch(getKnowledgeBaseSetting());
  }, [dispatch]);

  useEffect(() => {
    if (!isFirst && editKnowledgeBaseSetting.object_id === objectId) {
      navigate("/knowledge-base-view?object_id=" + objectId);
    }
    // eslint-disable-next-line
  }, [isFirst, editKnowledgeBaseSetting]);

  useEffect(() => {
    dispatch(
      loadPaginationSuccess({
        total_page: null,
        total_record: null,
      })
    );
    dispatch(viewObjectSearch({ object_id: objectId }));
  }, [objectId, dispatch]);
  useEffect(() => {
    formSearch.resetFields();
    //eslint-disable-next-line
  }, [objectId]);
  useEffect(
    () => () => {
      dispatch(updateIsFirst(false));
    },
    // eslint-disable-next-line
    []
  );

  useEffect(() => {
    if (userDetail.use_cti) {
      dispatch(
        getFieldsMappingCallCenter({
          object_id: objectId,
        })
      );
      dispatch(
        getObjectsToCall({
          object_id: objectId,
        })
      );
    }
  }, [userDetail, objectId, dispatch]);

  useEffect(() => {
    dispatch(
      loadCustomView({
        object_id: objectId,
      })
    );
  }, [dispatch, objectId]);

  useEffect(() => {
    if (flagDelete === "many") {
      dispatch(setShowModalConfirmDelete(true));
    }
    // setFlagDelete("");
  }, [flagDelete, dispatch]);

  const menuAction = (
    <Menu
      mode="inline"
      triggerSubMenuAction="click"
      style={{
        width: 256,
      }}
    >
      <Menu.Item
        key="0"
        icon={<EditOutlined />}
        onClick={() => {
          if (listCheck.length > 0) {
            setOpenMassEdit(true);
          }
        }}
        disabled={
          editingKey === "" &&
          listColumn.length > 2 &&
          update &&
          listCheck.length > 0
            ? false
            : true
        }
      >
        Sửa đồng loạt
      </Menu.Item>
      <Menu.Item
        key="1"
        icon={<DeleteOutlined />}
        onClick={() => {
          // setOpenMassDelete(true);
          setFlagDelete("many");
          setIdDelete("");
          // dispatch(setShowModalConfirmDelete(true));
        }}
        disabled={
          editingKey === "" &&
          listColumn.length > 2 &&
          deleteRole &&
          listCheck.length > 0
            ? false
            : true
        }
      >
        Xóa đồng loạt
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<MergeCellsOutlined />}
        onClick={() => {
          if (listCheck.length <= 1) {
            Notification("error", "Chọn ít nhất 2 bản ghi để gộp!");
          } else if (listCheck.length > 3) {
            Notification("error", "Chọn nhiều nhất 3 bản ghi để gộp!");
          } else {
            setOpenMerge(true);
          }
        }}
        disabled={listCheck.length > 1 && update && write ? false : true}
      >
        Gộp bản ghi
      </Menu.Item>
      <Menu.Item
        icon={<img alt="" src={trashImg} style={{ width: "16px" }} />}
        key="4"
        onClick={() => {
          navigate(`/recycle-bin/${objectId}`);
        }}
      >
        Trash
      </Menu.Item>
      {checkReportPermission("Import_Data", objectId) ? (
        <Menu.Item
          icon={<img alt="" src={ImportExcel} />}
          key="5"
          onClick={() => setOpenImport(true)}
          disabled={
            editingKey === "" &&
            listColumn.length > 2 &&
            write &&
            read &&
            update &&
            deleteRole
              ? false
              : true
          }
        >
          Import file Excel
        </Menu.Item>
      ) : (
        <Menu.Item
          icon={<img alt="" src={ImportExcel} />}
          key="5"
          disabled={true}
        >
          Import file Excel
        </Menu.Item>
      )}
      <Menu.Item
        icon={<img alt="" src={ExportExcel} />}
        key="6"
        onClick={() =>
          dispatch(
            exportTemplate({
              data: {
                object_id: objectId,
                api_version: "2",
              },
              name: listObjectField[listObjectField.length - 1]["main_object"]
                .object_name,
            })
          )
        }
        disabled={
          editingKey === "" && listColumn.length > 2 && read ? false : true
        }
      >
        Download template
      </Menu.Item>
    </Menu>
  );

  const menuView = (
    <Menu>
      {/* <CustomItem>
        <ItemText>List view</ItemText>
        <img src={ListViewImg} alt="List view" />
      </CustomItem> */}
      <CustomItem
        onClick={() => {
          // navigate(
          //   "/kanban-view?object_id=" +
          //     objectId +
          //     "&custom_view_id=" +
          //     customViewId
          // );
          navigate(`/kanban-view/${objectId}/${customViewId}`);
        }}
      >
        <ItemText> Kanban view </ItemText>
        <img src={KanbanViewImg} alt="Kanban view" />
      </CustomItem>
      <CustomItem
        onClick={() => {
          navigate(
            `/list-view-with-details/objects/${objectId}/${customViewId}`
          );
        }}
      >
        <ItemText> List view with detail</ItemText>
        <img src={ListViewDetailImg} alt=" List view with detail" />
      </CustomItem>
      {isKnowledge && (
        <CustomItem
          onClick={() => {
            navigate(`/knowledge-base-enhancement/${objectId}`);
          }}
        >
          <ItemText> Knowledge base view</ItemText>
          <img src={KnowledgeImg} alt="Knowledge base view" />
        </CustomItem>
      )}
    </Menu>
  );

  // const menuExcel = (
  //   <Menu mode="inline">
  //     <Menu.Item
  //       key="0"
  //       onClick={() => setOpenImport(true)}
  //       disabled={
  //         editingKey === "" &&
  //         listColumn.length > 2 &&
  //         write &&
  //         read &&
  //         update &&
  //         deleteRole
  //           ? false
  //           : true
  //       }
  //     >
  //       Import file Excel
  //     </Menu.Item>
  //     <Menu.Item
  //       key="1"
  //       onClick={() =>
  //         dispatch(
  //           exportTemplate({
  //             data: {
  //               object_id: objectId,
  //               api_version: "2",
  //             },
  //             name: listObjectField[listObjectField.length - 1]["main_object"]
  //               .object_name,
  //           })
  //         )
  //       }
  //       disabled={
  //         editingKey === "" && listColumn.length > 2 && read ? false : true
  //       }
  //     >
  //       Download template
  //     </Menu.Item>
  //   </Menu>
  // );
  useEffect(() => {
    /* eslint-disable-next-line */
    listObjectField.map((item, idx) => {
      if (Object.keys(item)[0] === "main_object") {
        setWrite(item[Object.keys(item)[0]]?.create_permission);
        setRead(item[Object.keys(item)[0]]?.read_permission);
        setUpdate(item[Object.keys(item)[0]]?.edit_permission);
        setDeleteRole(item[Object.keys(item)[0]]?.delete_permission);
      }
    });
  }, [listObjectField]);

  const handleCustomViewName = (id) => {
    if (id === "default-view") {
      if (customView && customView.custom_views) {
        let tem = "Default";
        customView.custom_views.forEach((item) => {
          if (item.is_default) {
            tem = item.view_name;
          }
        });
        return tem;
      } else {
        return "Default";
      }
    }
    let name =
      customView &&
      customView.custom_views &&
      customView.custom_views.find((item) => item._id === id);
    return name && name.view_name;
  };

  const menu = (
    <CustimMenuWrapper triggerSubMenuAction="click" style={{ width: "200px" }}>
      {/*eslint-disable-next-line*/}
      {Object.entries(objectCategory).map(([key, value], idx) => {
        if (visibleList[key])
          return (
            <CustomSubMenuWrapper key={key} title={key}>
              {/*eslint-disable-next-line*/}
              {value.map((item, index) => {
                if (item.visible === true && item.Status === true)
                  return (
                    <CustomMenuItem
                      key={item._id}
                      style={{
                        whiteSpace: "normal",
                        wordBreak: "break-all",
                        width: "200px",
                      }}
                    >
                      <CustomOption>
                        <div> {item.Name}</div>
                        {objectsFavourite.findIndex(
                          (object) => object.object_id === item._id
                        ) >= 0 ? (
                          <img
                            className="unpin-img"
                            alt=""
                            src={unpinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        ) : (
                          <img
                            className="pin-img"
                            alt=""
                            src={pinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        )}
                      </CustomOption>
                    </CustomMenuItem>
                  );
              })}
            </CustomSubMenuWrapper>
          );
      })}
    </CustimMenuWrapper>
  );

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "YYYY MM DD",
      value: "%Y %m %d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "MM DD YYYY",
      value: "%m %d %Y",
    },

    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
    {
      label: "DD MM YYYY",
      value: "%d %m %Y",
    },
  ];

  const optionsTime = [
    {
      label: "HH:mm:ss",
      value: "%H:%M:%S",
    },
    {
      label: "HH:mm",
      value: "%H:%M",
    },
  ];
  const [styleCustom, setStyleCustom] = useState({});

  // useEffect(() => {
  //   let newObj = {};
  //   /* eslint-disable-next-line */
  //   Object.entries(allFields).forEach(([key, value], index) => {
  //     if (value.type === "linkingobject") {
  //       newObj[key] = { ...recordData[key] };
  //     }
  //   });
  //   dispatch(setLinkingFieldValue(newObj));
  //   /* eslint-disable-next-line */
  // }, [recordData]);

  useEffect(() => {
    if (objectId && allSetting.length > 0) {
      const tempStyle = allSetting.filter(
        (setting) => setting.status && setting.object_id === objectId
      );
      if (tempStyle) {
        setStyleCustom(tempStyle);
      }
    }
  }, [allSetting, objectId]);

  useEffect(() => {
    setListSearch([]);
    setSortBy({});
    setCurrentPage(1);
    setRecordPerPage(50);
  }, [objectId, customViewId]);

  useEffect(() => {
    if (Object.keys(allConfig).length > 0) {
      let date = allConfig.tenant_datetime_format.split(" ")[0];
      let time = allConfig.tenant_datetime_format.split(" ")[1];
      let finalDate = optionsDate.find((ele) => ele.value === date).label;
      let finalTime = optionsTime.find((ele) => ele.value === time).label;
      setFormatDatetime(finalDate + " " + finalTime);

      let formatTemp = optionsDate.find(
        (ele) => ele.value === allConfig.tenant_date_format
      );
      setFormatDate(formatTemp);
    }

    // eslint-disable-next-line
  }, [allConfig]);

  // let searchData = [];

  useEffect(() => {
    if (listCheck.length > 0 && listCheck.length < listId.length) {
      setIndeterminate(true);
    } else {
      setIndeterminate(false);
    }

    if (listCheck.length === listId.length && dataSource.length > 0) {
      setCheckAll(true);
    } else {
      setCheckAll(false);
    }
  }, [listCheck, listId, dataSource]);

  useEffect(() => {
    dispatch(loadFavouriteObjects());
    if (customViewId === "default-view") {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
          },
          data: {
            object_id: objectId,
            // current_page: 1,
            // record_per_page: 10,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    } else {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }

    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );
    // eslint-disable-next-line
  }, [dispatch, objectId, customViewId]);

  useEffect(() => {
    dispatch(
      loadAllSetting({
        current_page: 1,
        record_per_page: 1000,
      })
    );
  }, [dispatch]);

  useEffect(() => {
    let newData = [];

    let arrCheck = [];

    data.forEach((record) => {
      let newRecord = {};

      arrCheck.push(record._id);

      listColumn.forEach((col) => {
        let flag = 0;
        Object.entries(record).forEach(([key, value]) => {
          if (col.ID === key) {
            flag = 1;
            if (
              key !== "_id" &&
              key !== "created_by" &&
              key !== "created_date" &&
              key !== "modify_by" &&
              key !== "modify_time" &&
              key !== "owner"
            ) {
              newRecord[key] = value.value;
            } else {
              newRecord[key] = value;
            }
          }
        });
        if (flag === 0) {
          newRecord[col.ID] = null;
        }
      });

      newData.push(newRecord);
    });

    setDataSource(newData);

    setListId(arrCheck);
  }, [data, listColumn]);

  useEffect(() => {
    let arrColumn = [...header, { name: "Action", ID: "_id" }];

    setListColumn(arrColumn);
    let newObj = {};

    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        /*eslint-disable-next-line*/
        return section.fields.map((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });
      });

    setAllFields(newObj);
  }, [listObjectField, header]);

  const numberWithCommas = (x, col) => {
    if (x === 0) {
      return (
        <Highlighter
          highlightClassName="YourHighlightClass"
          searchWords={["0"] || []}
          autoEscape={true}
          textToHighlight={x
            .toFixed(
              col?.decimal_separator === 0 ? 0 : col?.decimal_separator || 3
            )
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        />
      );
    } else
      return (
        <Highlighter
          highlightClassName="YourHighlightClass"
          searchWords={
            [
              typeof searchKey[col.ID] === "number"
                ? searchKey[col.ID] && searchKey[col.ID].toString()
                : searchKey[col.ID],
            ] || []
          }
          autoEscape={true}
          textToHighlight={x
            .toFixed(
              col?.decimal_separator === 0 ? 0 : col?.decimal_separator || 3
            )
            .toString()
            .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
        />
      );
  };

  const reload = () => {
    let searchData = [];
    /* eslint-disable-next-line */
    // Object.entries(listSearch).forEach(([key, value], index) => {
    //   if (value) {
    //     let newItem = {
    //       id_field: key,
    //       value: value,
    //     };
    //     searchData.push(newItem);
    //   }
    // });
    listSearch.map((item, idx) => {
      searchData.push(item);
      return null;
    });
    reloadData(searchData, firstID, lastID, sortBy);
  };

  // const [popup, setPopup] = useState({
  //   visible: false,
  //   x: 0,
  //   y: 0,
  // });

  const reloadData = (searchData, firstID, lastID, sortBy) => {
    let meta = [];
    let data = [];
    // eslint-disable-next-line
    searchData.map((item, idx) => {
      if (
        item.id_field === "created_date" ||
        item.id_field === "created_by" ||
        item.id_field === "modify_time" ||
        item.id_field === "owner" ||
        item.id_field === "modify_by"
      ) {
        meta.push(item);
      } else {
        data.push(item);
      }
    });
    if (customViewId === "default-view") {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
          },
          data: {
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: meta,
              data: data,
            },
            // sort_by: sortBy,
          },
        })
      );
    } else {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: meta,
              data: data,
            },
            // sort_by: sortBy,
          },
        })
      );
    }
  };

  // const handleSort = (sortTmp) => {
  //   reloadData(listSearch, firstID, lastID, sortTmp);
  // };

  const handleCheckAll = (e) => {
    if (e.target.checked) {
      setListCheck(listId);
    } else {
      setListCheck([]);
    }
    setIndeterminate(false);
    setCheckAll(e.target.checked);
  };

  const handleCheckbox = (id) => {
    let tem = [...listCheck];

    let flag = 1;

    listCheck.forEach((item, index) => {
      if (item === id) {
        flag = 0;
        tem.splice(index, 1);
      }
    });

    if (flag) {
      tem.push(id);
    }

    setListCheck(tem);
  };

  const resetSelectedRowKeys = () => {
    setListCheck([]);
  };

  const isChecked = (value) => {
    let check = false;

    listCheck.forEach((id) => {
      if (id === value) {
        check = true;
      }
    });

    return check;
  };

  const renderFieldFile = (value, col) => {
    /* eslint-disable-next-line */
    const expression =
      /*eslint-disable-next-line*/
      /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
    const regex = new RegExp(expression);
    if (typeof value === "object" && value && col.type !== "select") {
      /*eslint-disable-next-line*/
      return value.map((item, index) => {
        if (item !== null && item.match(regex)) {
          let fileNew = item.split("/");
          return (
            <a
              key={index}
              style={{ display: "block" }}
              href={
                allObject?.find(
                  (object) => object?.object_id === objectId && object?.status
                )
                  ? `${FE_URL + item?.split(BE_URL)[1]}`
                  : item
              }
              target={"_blank"}
              rel="noreferrer"
            >
              <TextComponent
                ellipsis={{
                  tooltip: fileNew.includes("docs.google.com")
                    ? `${fileNew[3]}/${fileNew[5]}`
                    : fileNew[fileNew.length - 1],
                }}
              >
                <span style={{ color: "#1890ff" }}>
                  {fileNew.includes("docs.google.com")
                    ? `${fileNew[3]}/${fileNew[5]}`
                    : fileNew[fileNew.length - 1]}
                </span>
              </TextComponent>
            </a>
          );
        }
      });
    } else if (value !== null && typeof value === "object")
      return (
        <div>
          {value.map((item, idx) => {
            return (
              <div
                style={{
                  background: "rgb(243 239 239)",
                  marginRight: "5px",
                  padding: "2px",
                  borderRadius: "5px",
                  float: "left",
                  marginBottom: "5px",
                }}
              >
                {item}
              </div>
            );
          })}
        </div>
      );
    else if (typeof value === "string") {
      let listFile = value.split(",");
      return listFile.map((item, idx) => {
        let splitFile = item.split("/");
        return (
          <a
            key={idx}
            style={{ display: "block" }}
            href={
              allObject?.find(
                (object) => object?.object_id === objectId && object?.status
              )
                ? `${FE_URL + item?.split(BE_URL)[1]}`
                : item
            }
            target={"_blank"}
            rel="noreferrer"
          >
            <TextComponent
              ellipsis={{
                tooltip: splitFile.includes("docs.google.com")
                  ? `${splitFile[3]}/${splitFile[5]}`
                  : splitFile[splitFile.length - 1],
              }}
            >
              <span style={{ color: "#1890ff" }}>
                {splitFile.includes("docs.google.com")
                  ? `${splitFile[3]}/${splitFile[5]}`
                  : splitFile[splitFile.length - 1]}
              </span>
            </TextComponent>
          </a>
        );
      });
    }
  };

  const handldeChangeListSearch = (id, value) => {
    // let temp = [...listSearch];
    if (value || value === 0) {
      let flag = 0;
      listSearch.forEach((item, index) => {
        if (item.id_field === id) {
          flag = 1;
          listSearch[index].value = value;
        }
      });

      if (flag === 0) {
        listSearch.push({ id_field: id, value: value });
      }
    } else {
      listSearch.forEach((item, index) => {
        if (item.id_field === id) {
          listSearch.splice(index, 1);
        }
      });
    }

    let tempMeta = [];
    let tempData = [];
    listSearch.forEach((item) => {
      if (
        item.id_field === "created_date" ||
        item.id_field === "created_by" ||
        item.id_field === "modify_time" ||
        item.id_field === "owner" ||
        item.id_field === "modify_by"
      ) {
        tempMeta.push(item);
      } else {
        tempData.push(item);
      }
    });

    if (!value) {
      let temp = { ...searchKey };
      delete temp[id];
      setSearchKey(temp);
    }
  };
  const [searchKey, setSearchKey] = useState({});
  const handleSearch = (id, value) => {
    // let temp = [...listSearch];
    let searchKeyTmp = { ...searchKey };

    if (value || value === 0) {
      let flag = 0;
      listSearch.forEach((item, index) => {
        if (item.id_field === id) {
          flag = 1;
          listSearch[index].value =
            typeof value === "string" ? value?.trim() : value;
        }
      });
      if (flag === 0) {
        listSearch.push({
          id_field: id,
          value: typeof value === "string" ? value?.trim() : value,
        });
      }
    } else {
      listSearch.forEach((item, index) => {
        if (item.id_field === id) {
          listSearch.splice(index, 1);
        }
      });
    }

    let tempMeta = [];
    let tempData = [];
    listSearch.forEach((item) => {
      if (
        item.id_field === "created_date" ||
        item.id_field === "created_by" ||
        item.id_field === "modify_time" ||
        item.id_field === "owner" ||
        item.id_field === "modify_by"
      ) {
        tempMeta.push(item);
        searchKeyTmp[item.id_field] = item.value;
      } else {
        tempData.push(item);
        searchKeyTmp[item.id_field] = item.value;
      }
    });

    // setListSearch(temp);

    if (!value) {
      delete searchKeyTmp[id];
    }

    setSearchKey(searchKeyTmp);
    setCurrentPage(1);
    setRecordPerPage(50);

    if (customViewId === "default-view") {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
          },
          data: {
            object_id: objectId,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: tempMeta,
              data: tempData,
            },
            // sort_by: sortBy,
          },
        })
      );
      dispatch(
        loadPaginationSuccess({
          total_page: null,
          total_record: null,
        })
      );
    } else {
      dispatch(
        loadDataWithOutPagi({
          object_id: {
            object_id: objectId,
            id: customViewId,
          },
          data: {
            id: customViewId,
            object_id: objectId,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: tempMeta,
              data: tempData,
            },
            // sort_by: sortBy,
          },
        })
      );
      dispatch(
        loadPaginationSuccess({
          total_page: null,
          total_record: null,
        })
      );
    }
  };

  useEffect(() => {
    setSearchKey({});
    formSearch.resetFields();
    //eslint-disable-next-line
  }, [customViewId]);
  useEffect(() => {
    if (!isSeacrhGlobal) {
      formSearch.resetFields();
    }
    //eslint-disable-next-line
  }, [isSeacrhGlobal]);

  const _onMakeCall = (value, hotline) => {
    if (localStorage.getItem("inCall") === "true") {
      Notification("error", "On call!");
    } else {
      let data = {};
      if (hotline) {
        data = {
          phone: value,
          hotline: hotline,
        };
      } else {
        data = {
          phone: value,
        };
      }
      dispatch(
        makeCall({
          ...data,
        })
      );
    }
  };

  const normalizeLink = (value) => {
    if (
      allObject?.find(
        (object) => object?.object_id === objectId && object?.status
      )
    ) {
      if (value.split(BE_URL)?.length > 1) {
        return value;
      } else {
        return BE_URL + value?.split(FE_URL)[1];
      }
    } else {
      return value;
    }
  };

  const onFinish = (values) => {
    let fieldsObject = {};
    /* eslint-disable-next-line */
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        /* eslint-disable-next-line */
        section.fields.map((item, index) => {
          fieldsObject[item.ID] = { ...item };
        });
      });
    let listValue = [];
    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], idx) => {
      if (
        key !== "assignTo" &&
        key !== "share_to" &&
        key !== "action_type" &&
        key !== "subject" &&
        key !== "permission"
      ) {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              if (item.url === undefined) {
                item.url =
                  BASE_URL_API + item &&
                  item.response &&
                  item.response.data &&
                  item.response.data[0];
                item.url = BE_URL + item.url;
              }
              result.push(normalizeLink(item.url));
            });
          } else {
            if (typeof value === "string") {
              let listFile = value.split(",");
              listFile.forEach((item, idx) => {
                result.push(normalizeLink(item));
                return null;
              });
            } else {
              // eslint-disable-next-line
              value.map((item, idx) => {
                if (item.url) {
                  result.push(normalizeLink(item.url));
                } else {
                  result.push(normalizeLink(item));
                }
              });
            }
          }
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };

          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value?.toString() ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });

    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(listSearch).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    if (Object.entries(recordData).length > 0 && recordID) {
      if (customViewId === "default-view") {
        let meta = [];
        let data = [];
        // eslint-disable-next-line
        searchData.forEach((item, idx) => {
          if (
            item.id_field === "created_date" ||
            item.id_field === "created_by" ||
            item.id_field === "modify_time" ||
            item.id_field === "owner" ||
            item.id_field === "modify_by"
          ) {
            meta.push(item);
          } else {
            data.push(item.value);
          }
        });
        dispatch(
          updateRecord({
            data: {
              data: listValue,
              owner_id: values.assignTo,
              id: recordData._id,
              object_id: objectId,
              share_to: values.share_to,
              action_type: values.action_type,
              subject: values.subject,
              permission: values.permission,
            },
            load: {
              object_id: objectId,
              first_record_id: firstID,
              last_record_id: lastID,
              search_with: {
                meta: meta,
                data: data,
              },
            },
          })
        );
        setRecordID("");
      } else {
        let meta = [];
        let data = [];
        // eslint-disable-next-line

        searchData.forEach((item, idx) => {
          if (
            item.id_field === "created_date" ||
            item.id_field === "created_by" ||
            item.id_field === "modify_time" ||
            item.id_field === "owner" ||
            item.id_field === "modify_by"
          ) {
            meta.push(item);
          } else {
            data.push(item.value);
          }
        });

        dispatch(
          updateRecord({
            data: {
              data: listValue,
              owner_id: values.assignTo,
              id: recordData._id,
              object_id: objectId,
              share_to: values.share_to,
              action_type: values.action_type,
              subject: values.subject,
              permission: values.permission,
            },
            load: {
              id: customViewId,
              object_id: objectId,
              first_record_id: firstID,
              last_record_id: lastID,
              search_with: {
                meta: meta,
                data: data,
              },
            },
          })
        );
        setRecordID("");
      }
    } else {
      let meta = [];
      let data = [];
      // eslint-disable-next-line
      listSearch.forEach((item, idx) => {
        if (
          item.id_field === "created_date" ||
          item.id_field === "created_by" ||
          item.id_field === "modify_time" ||
          item.id_field === "owner" ||
          item.id_field === "modify_by"
        ) {
          meta.push(item);
        } else {
          data.push(item);
        }
      });
      dispatch(
        createRecord({
          data: {
            object_id: objectId,
            data: listValue,
            owner: values["assignTo"],
          },
          load: {
            object_id: objectId,
            first_record_id: firstID,
            last_record_id: lastID,
            search_with: {
              meta: meta,
              data: data,
            },
          },
        })
      );
      // dispatch(
      //   loadFormCreate({
      //     object_id: objectId,
      //   })
      // );
      setRecordID("");
    }

    // setEditingKey("");
    dispatch(setShowModal(false));
    setRecordID("");
    setEditingKey("");
    form.resetFields();
    dispatch(loadRecordDataSuccess({}));
  };

  const edit = (record, newObj) => {
    if (objectId === "obj_crm_campaign_00001") {
      navigate(`/edit-campaign/${record._id}`);
    } else {
      form.resetFields();
      let newRecord = { ...record };
      /* eslint-disable-next-line */
      Object.entries(record).forEach(([key, value], index) => {
        if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "datetime-local"
        ) {
          newRecord[key] = record[key] ? moment(record[key]) : null;
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "date"
        ) {
          newRecord[key] = record[key] ? moment(record[key]) : null;
        }
      });
      form.setFieldsValue({ ...newRecord, ...newObj });
      // setEditingKey(record.key);
      dispatch(setShowModal(true));
    }
  };
  const handleNameObject = () => {
    let name = "";
    // eslint-disable-next-line
    Object.entries(objectCategory).forEach(([key, value]) => {
      // eslint-disable-next-line
      value.map((object) => {
        if (object._id === objectId) {
          name = object.Name;
        }
      });
    });
    return name;
  };

  const handleNameGroupObject = () => {
    let name = "";
    // eslint-disable-next-line
    Object.entries(objectCategory).forEach(([key, value]) => {
      // eslint-disable-next-line
      value.map((object) => {
        if (object._id === objectId) {
          name = key;
        }
      });
    });
    return name;
  };

  useEffect(() => {
    setNext(
      dataSource.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + dataSource.length
    );
    /* eslint-disable-next-line */
  }, [dataSource]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
    /* eslint-disable-next-line */
  }, [currentPage]);

  useEffect(() => {
    if (showModalConfirmDelete === false) {
      setFlagDelete("");
    }
  }, [showModalConfirmDelete]);

  useEffect(() => {
    let tempLink = "";
    Object.entries(objectCategory).forEach(([key, value]) => {
      value.forEach((object) => {
        if (object._id === objectId && object?.is_embed_iframe) {
          tempLink = object.link_iframe;
        }
      });
    });

    $linkIframe(tempLink);
  }, [objectCategory, objectId]);

  const handleGlobalSearch = (value) => {
    if (value.trim().length > 100) {
      Notification("warning", "Chỉ tìm kiếm tối đa 100 ký tự!");
    } else {
      setCurrentPage(1);
      if (value.trim()) {
        dispatch(updateIsSearchGlobal(true));
        dispatch(
          globalSearch({
            object_id: objectId,
            search_text: value.trim(),
          })
        );
      } else {
        if (customViewId === "default-view") {
          dispatch(
            loadDataWithOutPagi({
              object_id: {
                object_id: objectId,
              },
              data: {
                object_id: objectId,
                // current_page: 1,
                // record_per_page: 10,
                first_record_id: null,
                last_record_id: null,
                search_with: {
                  meta: [],
                  data: [],
                },
              },
            })
          );
        } else {
          dispatch(
            loadDataWithOutPagi({
              object_id: {
                object_id: objectId,
                id: customViewId,
              },
              data: {
                id: customViewId,
                object_id: objectId,
                first_record_id: firstID,
                last_record_id: lastID,
                search_with: {
                  meta: [],
                  data: [],
                },
              },
            })
          );
        }
      }
    }
  };

  const outerRef = useRef(null);

  const renderTable = () => {
    return (
      <>
        {/* <MenuPopup outerRef={outerRef} /> */}
        <table>
          <thead className="table-header">
            <tr>
              <th>
                <CheckboxWrap isGlobal={objectGlobalSearch.allow_global_search}>
                  <Checkbox
                    indeterminate={indeterminate}
                    checked={checkAll}
                    onChange={handleCheckAll}
                  />
                  <span> {listCheck.length > 0 ? listCheck.length : ""}</span>
                </CheckboxWrap>
              </th>
              {listColumn.map((item, index) => {
                return (
                  <th key={index}>
                    <ContentHeader>
                      <TitleWrap
                        isGlobal={objectGlobalSearch.allow_global_search}
                        align={listColumn.length - 1 === index}
                      >
                        <span>
                          {item.ID === "owner" ? "Assign to" : item.name}
                        </span>
                        <IconHeader className="hidden__component">
                          {/* <CaretUpOutlined
                            style={{
                              color: `${
                                sortBy[item.ID] === 1
                                  ? "blue"
                                  : "rgba(0, 0, 0, 0.25)"
                              }`,
                            }}
                            onClick={() => {}}
                          />
                          <CaretDownOutlined
                            style={{
                              color: `${
                                sortBy[item.ID] === -1
                                  ? "blue"
                                  : "rgba(0, 0, 0, 0.25)"
                              }`,
                            }}
                            onClick={() => {}}
                          /> */}
                        </IconHeader>
                      </TitleWrap>
                      <SearchWrap
                        isGlobal={objectGlobalSearch.allow_global_search}
                        className="hidden__component"
                      >
                        {item.type === "date" ||
                        item.type === "datetime-local" ||
                        item.ID === "created_date" ||
                        item.ID === "modify_time" ? (
                          <DatePicker
                            disabled={objectGlobalSearch.allow_global_search}
                            onChange={(date, dateSting) => {
                              handleSearch(item.ID, dateSting);
                            }}
                          />
                        ) : item.type !== "select" &&
                          item.type !== "dynamic-field" &&
                          item.type !== "number" &&
                          item.type !== "formula" ? (
                          <CustomSearch
                            // value={
                            //   listSearch.find((val) => val.id_field === item.ID)
                            //     ?.value || ""
                            // }
                            onChange={(value) => {
                              let newValue = value.target.value.replace(
                                /[^-a-zA-Z0-9aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ-~!@#$%^&*-_\\+\\`\\-\\=\\<\\>\\,\\.\\/\\?\\:\\;\\"\\'\\|\\]/g,
                                ""
                              );

                              handldeChangeListSearch(item.ID, newValue);
                            }}
                            disabled={
                              // item.ID === "owner" ||

                              objectGlobalSearch.allow_global_search
                              // item.ID === "created_by" ||
                              // item.ID === "modify_by"
                            }
                            allowClear={true}
                            onPressEnter={(value) => {
                              let newValue = value.target.value.replace(
                                /[^-a-zA-Z0-9aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ-~!@#$%^&*-_\\+\\`\\-\\=\\<\\>\\,\\.\\/\\?\\:\\;\\"\\'\\|\\]/g,
                                ""
                              );
                              handleSearch(item.ID, newValue);
                            }}
                            placeholder={`${
                              item.ID === "owner" ? "Assign to" : item.name
                            }`}
                          />
                        ) : item.type === "number" ||
                          (item.type === "formula" &&
                            item.formula_type === "dateToParts") ||
                          (item.type === "formula" &&
                            item.formula_type === "dateDiff") ||
                          (item.type === "formula" &&
                            item.formula_type === "add") ||
                          (item.type === "formula" &&
                            item.formula_type === "subtract") ||
                          (item.type === "formula" &&
                            item.formula_type === "multiply") ||
                          (item.type === "formula" &&
                            item.formula_type === "devide") ||
                          (item.type === "formula" &&
                            item.formula_type === "mod") ||
                          (item.type === "formula" &&
                            item.formula_type === "max") ||
                          (item.type === "formula" &&
                            item.formula_type === "min") ||
                          (item.type === "formula" &&
                            item.formula_type === "advanceExpression") ? (
                          <CustomSearchNumber
                            // value={
                            //   listSearch.find((val) => val.id_field === item.ID)
                            //     ?.value || ""
                            // }
                            onChange={(value) => {
                              handldeChangeListSearch(item.ID, value);
                            }}
                            disabled={
                              // item.ID === "owner" ||
                              item.ID === "created_date" ||
                              item.ID === "modify_time" ||
                              objectGlobalSearch.allow_global_search
                              // item.ID === "created_by" ||
                              // item.ID === "modify_by"
                            }
                            allowClear={true}
                            onPressEnter={(value) => {
                              handleSearch(
                                item.ID,
                                parseInt(value.target.value)
                              );
                            }}
                            placeholder={`${
                              item.ID === "owner" ? "Assign to" : item.name
                            }`}
                          />
                        ) : item.type === "formula" ? (
                          <CustomSearch
                            // value={
                            //   listSearch.find((val) => val.id_field === item.ID)
                            //     ?.value || ""
                            // }
                            onChange={(value) => {
                              let newValue = value.target.value.replace(
                                /[^-a-zA-Z0-9aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ-~!@#$%^&*-_\\+\\`\\-\\=\\<\\>\\,\\.\\/\\?\\:\\;\\"\\'\\|\\]/g,
                                ""
                              );

                              handldeChangeListSearch(item.ID, newValue);
                            }}
                            disabled={
                              // item.ID === "owner" ||

                              objectGlobalSearch.allow_global_search
                              // item.ID === "created_by" ||
                              // item.ID === "modify_by"
                            }
                            allowClear={true}
                            onPressEnter={(value) => {
                              let newValue = value.target.value.replace(
                                /[^-a-zA-Z0-9aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ-~!@#$%^&*-_\\+\\`\\-\\=\\<\\>\\,\\.\\/\\?\\:\\;\\"\\'\\|\\]/g,
                                ""
                              );
                              handleSearch(item.ID, newValue);
                            }}
                            placeholder={`${
                              item.ID === "owner" ? "Assign to" : item.name
                            }`}
                          />
                        ) : (
                          <CustomSelect
                            // value={
                            //   listSearch.find((val) => val.id_field === item.ID)
                            //     ?.value || undefined
                            // }
                            disabled={objectGlobalSearch.allow_global_search}
                            showSearch
                            mode="multiple"
                            allowClear={true}
                            onClear={() => {
                              setSearchKey({});
                            }}
                            placeholder={`${item.name}`}
                            onChange={(value) => {
                              handleSearch(
                                item.ID,
                                value.length === 0 ? undefined : value
                              );
                            }}
                            style={{ marginBottom: 8, display: "block" }}
                          >
                            {item &&
                              item.option &&
                              item.option.map((op, index) => {
                                return (
                                  <Option key={index} value={op.label}>
                                    {op.label}
                                  </Option>
                                );
                              })}
                          </CustomSelect>
                        )}
                      </SearchWrap>
                    </ContentHeader>
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody ref={outerRef} className="table-body">
            {dataSource.map((record, index) => {
              return (
                <tr
                  key={index}
                  id={record._id}
                  onDoubleClick={() => {
                    if (read) {
                      window.open(
                        BASENAME + `consolidated-view/${objectId}/${record._id}`
                      );
                    }
                  }}
                >
                  {/* {Object.values(record).map((value, index) => {
             return <td key={index}>{value}</td>;
           })} */}
                  <td id={`ucrm_${record._id}_0`}>
                    <Checkbox
                      // value={record._id}
                      checked={isChecked(record._id)}
                      onChange={() => {
                        handleCheckbox(record._id);
                      }}
                    />
                  </td>
                  {listColumn.map((col, idx) => {
                    // eslint-disable-next-line
                    return Object.entries(record).map(([key, value]) => {
                      if (col.ID === key) {
                        if (col.ID === "_id") {
                          return (
                            <CustomTd
                              id={`ucrm_${record._id}_${idx + 1}`}
                              key={index}
                            >
                              <ActionWrap>
                                {Object.keys(fieldsMappingCallCenter).length >
                                  0 &&
                                  fieldsMappingCallCenter.phone_fields.length >
                                    0 && (
                                    <Dropdown
                                      overlay={
                                        <Menu>
                                          {fieldsMappingCallCenter.phone_fields.map(
                                            (item) => (
                                              <>
                                                {Object.entries(record).map(
                                                  ([keyRecord, valRecord]) => {
                                                    return (
                                                      <>
                                                        {item === keyRecord &&
                                                          valRecord && (
                                                            <>
                                                              {fieldsMappingCallCenter
                                                                .custom_hotlines
                                                                .length > 0 ? (
                                                                <>
                                                                  {fieldsMappingCallCenter.custom_hotlines.map(
                                                                    (
                                                                      hotline
                                                                    ) => {
                                                                      return (
                                                                        <Menu.Item
                                                                          onClick={() =>
                                                                            _onMakeCall(
                                                                              valRecord,
                                                                              hotline.hotline
                                                                            )
                                                                          }
                                                                          key={
                                                                            valRecord
                                                                          }
                                                                        >
                                                                          {
                                                                            hotline.prefix
                                                                          }
                                                                          {
                                                                            valRecord
                                                                          }
                                                                        </Menu.Item>
                                                                      );
                                                                    }
                                                                  )}
                                                                </>
                                                              ) : (
                                                                <Menu.Item
                                                                  onClick={() =>
                                                                    _onMakeCall(
                                                                      valRecord
                                                                    )
                                                                  }
                                                                  key={
                                                                    valRecord
                                                                  }
                                                                >
                                                                  {valRecord}
                                                                </Menu.Item>
                                                              )}
                                                            </>
                                                          )}
                                                      </>
                                                    );
                                                  }
                                                )}
                                              </>
                                            )
                                          )}
                                        </Menu>
                                      }
                                      placement="bottom"
                                    >
                                      <img src={Call} alt="call" />
                                    </Dropdown>
                                  )}

                                {Object.keys(objectsToCall).length > 0 &&
                                  _.get(objectsToCall, "phone_fields", [])
                                    ?.length > 0 && (
                                    <Dropdown
                                      overlay={
                                        <Menu>
                                          {_.get(
                                            objectsToCall,
                                            "phone_fields",
                                            []
                                          )?.map((item) => (
                                            <>
                                              {Object.entries(record).map(
                                                ([keyRecord, valRecord]) => {
                                                  return (
                                                    <>
                                                      {item === keyRecord &&
                                                        valRecord && (
                                                          <>
                                                            {_.get(
                                                              objectsToCall,
                                                              "custom_hotlines",
                                                              []
                                                            ).length > 0 ? (
                                                              <>
                                                                {_.get(
                                                                  objectsToCall,
                                                                  "custom_hotlines",
                                                                  []
                                                                ).map(
                                                                  (hotline) => {
                                                                    return (
                                                                      <Menu.Item
                                                                        onClick={() =>
                                                                          _onMakeCall(
                                                                            valRecord,
                                                                            hotline.hotline
                                                                          )
                                                                        }
                                                                        key={
                                                                          valRecord
                                                                        }
                                                                      >
                                                                        {
                                                                          hotline.prefix
                                                                        }
                                                                        {
                                                                          valRecord
                                                                        }
                                                                      </Menu.Item>
                                                                    );
                                                                  }
                                                                )}
                                                              </>
                                                            ) : (
                                                              <Menu.Item
                                                                onClick={() =>
                                                                  _onMakeCall(
                                                                    valRecord
                                                                  )
                                                                }
                                                                key={valRecord}
                                                              >
                                                                {valRecord}
                                                              </Menu.Item>
                                                            )}
                                                          </>
                                                        )}
                                                    </>
                                                  );
                                                }
                                              )}
                                            </>
                                          ))}
                                        </Menu>
                                      }
                                      placement="bottom"
                                    >
                                      <img src={Call} alt="call" />
                                    </Dropdown>
                                  )}

                                <Tooltip title="Consolidate view">
                                  <img
                                    onClick={() => {
                                      if (read) {
                                        window.open(
                                          BASENAME +
                                            `consolidated-view/${objectId}/${record._id}`
                                        );
                                      }
                                    }}
                                    src={Details}
                                    alt="Consolidate view"
                                  />
                                </Tooltip>
                                <Tooltip title="Edit">
                                  <img
                                    onClick={async () => {
                                      const isTokenValid =
                                        await checkTokenExpiration();
                                      if (update) {
                                        if (isKnowledge) {
                                          navigate(
                                            `/knowledge-base-enhancement/edit-article/${objectId}/${record._id}`
                                          );
                                        }
                                        setRecordID(record._id);

                                        dispatch(setLoadingModal(true));
                                        new Promise((resolve, reject) => {
                                          axios
                                            .get(
                                              BASE_URL_API +
                                                `load-record-data?id=${record._id}&object_id=${objectId}`,
                                              {
                                                headers: {
                                                  Authorization: isTokenValid,
                                                },
                                              }
                                            )
                                            .then((res) => {
                                              resolve(res.data.data);
                                              dispatch(setLoadingModal(false));
                                            })
                                            .catch((err) => {
                                              reject(err);
                                              dispatch(setLoadingModal(false));
                                            });
                                        })
                                          .then((res) => {
                                            dispatch(
                                              loadRecordDataSuccess(res)
                                            );
                                            let record = {};
                                            record["assignTo"] = res.owner;
                                            Object.entries(res).forEach(
                                              ([key, value]) => {
                                                if (
                                                  value !== null &&
                                                  typeof value === "object"
                                                ) {
                                                  record[key] = value.value;
                                                } else {
                                                  record[key] = value;
                                                }
                                              }
                                            );

                                            let newObj = {};

                                            /* eslint-disable-next-line */
                                            Object.entries(allFields).forEach(
                                              ([key, value], index) => {
                                                if (
                                                  value.type === "linkingobject"
                                                ) {
                                                  newObj[key] = {
                                                    ...res[key],
                                                  };
                                                }
                                                if (value.type === "lookup") {
                                                  record[key] = res[key];
                                                }
                                              }
                                            );

                                            dispatch(
                                              setLinkingFieldValue(newObj)
                                            );

                                            edit(record, newObj);
                                            // dispatch(setShowModal(true));
                                          })
                                          .catch((err) => {});
                                      } else {
                                        Notification(
                                          "warning",
                                          "Bạn không có quyền để chỉnh sửa!"
                                        );
                                      }
                                    }}
                                    src={editImg}
                                    alt="Edit"
                                  />
                                </Tooltip>
                                <Tooltip title="Delete">
                                  <img
                                    onClick={() => {
                                      if (deleteRole) {
                                        setIdDelete(record._id);
                                        setFlagDelete("one");

                                        dispatch(
                                          setShowModalConfirmDelete(true)
                                        );
                                      } else {
                                        Notification(
                                          "warning",
                                          "Bạn không có quyền để xóa!"
                                        );
                                      }
                                    }}
                                    src={deleteImg}
                                    alt="Delete"
                                  />
                                </Tooltip>
                              </ActionWrap>
                            </CustomTd>
                          );
                        } else {
                          return (
                            <CustomTd
                              id={`ucrm_${record._id}_${idx + 1}`}
                              style={{
                                textAlign: `${
                                  col.type === "number" ||
                                  (col.type === "formula" &&
                                    col.formula_type === "advanceExpression")
                                    ? "right"
                                    : "left"
                                }`,
                              }}
                              background={
                                record && styleCustom.length > 0
                                  ? styleCustom[0].type === "cell"
                                    ? styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                            record[styleCustom[0]?.field_id] &&
                                          allFields[col.ID]?.ID ===
                                            styleCustom[0].field_id
                                      )[0]?.background
                                    : styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                          record[styleCustom[0]?.field_id]
                                      )[0]?.background
                                  : "transparent"
                              }
                              color={
                                record && styleCustom.length > 0
                                  ? styleCustom[0].type === "cell"
                                    ? styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                            record[styleCustom[0]?.field_id] &&
                                          allFields[col.ID]?.ID ===
                                            styleCustom[0].field_id
                                      )[0]?.color
                                    : styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                          record[styleCustom[0]?.field_id]
                                      )[0]?.color
                                  : "#000"
                              }
                              fontWeight={
                                record && styleCustom.length > 0
                                  ? styleCustom[0].type === "cell"
                                    ? styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                            record[styleCustom[0]?.field_id] &&
                                          allFields[col.ID]?.ID ===
                                            styleCustom[0].field_id
                                      )[0]?.font_weight
                                    : styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                          record[styleCustom[0]?.field_id]
                                      )[0]?.font_weight
                                  : "400"
                              }
                              fontSize={
                                record && styleCustom.length > 0
                                  ? styleCustom[0].type === "cell"
                                    ? styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                            record[styleCustom[0]?.field_id] &&
                                          allFields[col.ID]?.ID ===
                                            styleCustom[0].field_id
                                      )[0]?.font_size
                                    : styleCustom[0].style.filter(
                                        (ele) =>
                                          ele.value ===
                                          record[styleCustom[0]?.field_id]
                                      )[0]?.font_size
                                  : "14"
                              }
                              key={index}
                            >
                              {col.type === "file" ||
                              (col.type === "select" &&
                                col.multiple === true) ? (
                                <TextComponent ellipsis={{ tooltip: value }}>
                                  {renderFieldFile(value, col)}
                                </TextComponent>
                              ) : (
                                <TextComponent ellipsis={{ tooltip: value }}>
                                  {/* {value} */}
                                  {(col.type === "number" && value !== null) ||
                                  (col.type === "formula" &&
                                    col.formula_type === "advanceExpression" &&
                                    value !== null) ? (
                                    numberWithCommas(value, col)
                                  ) : (
                                    <Highlighter
                                      highlightClassName="YourHighlightClass"
                                      searchWords={
                                        typeof searchKey[col.ID] === "object"
                                          ? searchKey[col.ID]
                                          : [
                                              typeof searchKey[col.ID] ===
                                              "number"
                                                ? searchKey[col.ID] &&
                                                  searchKey[col.ID].toString()
                                                : searchKey[col.ID],
                                            ] || []
                                      }
                                      autoEscape={true}
                                      textToHighlight={
                                        value || value === 0
                                          ? value.toString()
                                          : ""
                                      }
                                    />
                                  )}
                                </TextComponent>
                              )}
                            </CustomTd>
                          );
                        }
                      }
                    });
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </>
    );
  };

  const applyMemo = useMemo(
    () => renderTable(),
    //eslint-disable-next-line
    [listColumn, dataSource, listSearch, listCheck, indeterminate, checkAll]
  );

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1880 },
      items: 12,
      slidesToSlide: 12,
    },
    tablet: {
      breakpoint: { max: 1880, min: 1400 },
      items: 9,
      slidesToSlide: 9,
    },
    mobile: {
      breakpoint: { max: 1400, min: 0 },
      items: 7,
      slidesToSlide: 7,
    },
  };

  return (
    <>
      {linkIframe ? (
        <WrapIframe>
          <iframe
            src={linkIframe}
            title="iframe"
            width="100%"
            height="100%"
            style={{ border: "none" }}
          />
        </WrapIframe>
      ) : (
        <>
          <CustomHeader>
            {open === false ? (
              ""
            ) : (
              <CustomMenuHeader>
                <CarouselWrap>
                  <Carousel responsive={responsive}>
                    {objectsFavourite.map((item, idx) => {
                      if (item && item.object_name)
                        return (
                          <MenuItem
                            className={`btn btn-top ${
                              objectId === item.object_id ? "active" : ""
                            }`}
                            key={item.object_id}
                            onClick={() =>
                              navigate(
                                `/objects/${item.object_id}/default-view`
                              )
                            }
                          >
                            <TextComponent
                              style={{
                                color: `${
                                  objectId === item.object_id
                                    ? "#fff"
                                    : "#2c2c2c"
                                }`,
                              }}
                              ellipsis={{ tooltip: item.object_name }}
                            >
                              {item.object_name}
                            </TextComponent>
                          </MenuItem>
                        );
                      return null;
                    })}
                  </Carousel>
                </CarouselWrap>
                <CustomDropdown
                  overlay={menu}
                  placement="bottomLeft"
                  trigger="click"
                >
                  <CustomNewButton>
                    Tất cả <DownOutlined />
                  </CustomNewButton>
                </CustomDropdown>
                <CustomDisplay>
                  Hiển thị:
                  <span
                    onClick={() => {
                      setOpenCustomView(true);
                    }}
                  >
                    {handleCustomViewName(customViewId)}
                  </span>
                </CustomDisplay>
              </CustomMenuHeader>
            )}

            <div style={{ display: "none" }}>
              <CustomClose
                onClick={() => {
                  setOpen(!open);
                }}
              >
                {open === false ? (
                  <>
                    <img src={IconBottom} alt="" />
                  </>
                ) : (
                  <>
                    <img src={IconTop} alt="" />
                  </>
                )}
              </CustomClose>
            </div>
          </CustomHeader>
          <CustomWrapperAction>
            <Breadcrumb>
              <CustomBreadCrumb>{t("common.object")}</CustomBreadCrumb>
              <CustomBreadCrumb>{handleNameGroupObject()}</CustomBreadCrumb>
              <BreadcrumbItem> {handleNameObject()} </BreadcrumbItem>
            </Breadcrumb>
            <CustomDisplayView>
              Hiển thị:
              <span
                onClick={() => {
                  setOpenCustomView(true);
                }}
              >
                {handleCustomViewName(customViewId)}
              </span>
            </CustomDisplayView>
            <RightWrap>
              <Form form={formSearch}>
                {objectGlobalSearch.allow_global_search && (
                  <Tooltip
                    overlayInnerStyle={{
                      backgroundColor: notiGlobal.includes("not available")
                        ? "#d4b106"
                        : "rgba(0, 0, 0, 0.75)",
                    }}
                    trigger="hover"
                    placement="top"
                    title={notiGlobal}
                  >
                    <Form.Item name="test_name">
                      <CustomInputSearch
                        allowClear
                        onSearch={(value) => {
                          handleGlobalSearch(value);
                        }}
                        placeholder="Search this list..."
                      />
                    </Form.Item>
                  </Tooltip>
                )}
              </Form>
              <CustomButtonAddRecord
                size="large"
                onClick={() => {
                  if (objectId === "obj_crm_campaign_00001") {
                    $showModalCampaign(true);
                  } else if (isKnowledge) {
                    navigate(
                      `/knowledge-base-enhancement/create-article/${objectId}`
                    );
                  } else {
                    dispatch(setShowModal(true));
                    setRecordID("");
                    setEditingKey("");
                    dispatch(loadRecordDataSuccess({}));
                  }
                }}
                disabled={
                  editingKey === "" && listColumn.length > 2 && write
                    ? false
                    : true
                }
              >
                <img alt="" src={plusIcon} />
                {t("object.addRecord")}
              </CustomButtonAddRecord>
              {/* <Dropdown overlay={menuExcel} trigger="click">
            <Button
              size="large"
              style={{ marginLeft: "16px" }}
              disabled={
                editingKey === "" && listColumn.length > 1 ? false : true
              }
            >
              {t("object.workingExcel")} <DownOutlined />
            </Button>
          </Dropdown> */}
              {editKnowledgeBaseSetting.object_id === objectId && (
                <Button
                  size="large"
                  style={{ marginLeft: "16px" }}
                  disabled={
                    editingKey === "" && listColumn.length > 1 ? false : true
                  }
                  onClick={() => {
                    navigate("/knowledge-base-view?object_id=" + objectId);
                  }}
                >
                  Back to knowledge base
                </Button>
              )}

              <ViewWrap>
                <Dropdown overlay={menuView} arrow trigger={["click"]}>
                  <OptionView>
                    <img src={ListViewImg} alt="" /> <Text>List view</Text>
                    <CaretDownOutlined />
                  </OptionView>
                </Dropdown>
              </ViewWrap>
              <Dropdown overlay={menuAction} placement="bottomLeft">
                <Button
                  size="large"
                  style={{ marginLeft: "16px", marginRight: "16px" }}
                >
                  <EllipsisOutlined />
                </Button>
              </Dropdown>
            </RightWrap>
          </CustomWrapperAction>

          <ContentWrap>
            <div
              style={{
                background: "white",
                padding: "10px",
                height: "calc(100% - 30px)",
              }}
            >
              <TableWrap>
                {listColumn.length > 2 ? (
                  applyMemo
                ) : (
                  <WrapperLoading>
                    <img alt="" src={CRMLogo} />
                    <div className="lds-ellipsis">
                      <div></div>
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </WrapperLoading>
                )}
              </TableWrap>
            </div>
            {isLoadingModal && (
              <WrapperLoading>
                <img alt="" src={CRMLogo} />
                <div className="lds-ellipsis">
                  <div></div>
                  <div></div>
                  <div></div>
                  <div></div>
                </div>
              </WrapperLoading>
            )}
            {!isSeacrhGlobal && (
              <NewCustomPagination>
                {isLoadingDataWithOutPagi ? (
                  <Spin />
                ) : (
                  <>
                    <div className="total-record">
                      {next === 0 ? 0 : prev} - {next}{" "}
                      {isLoadingPagi ? (
                        <Spin style={{ marginLeft: "10px" }} />
                      ) : totalRecord === null ? (
                        <div
                          className="reload-pagi"
                          onClick={() => {
                            let temp = [...listSearch];
                            let searchKeyTmp = { ...searchKey };
                            let tempMeta = [];
                            let tempData = [];
                            temp.forEach((item) => {
                              if (
                                item.id_field === "created_date" ||
                                item.id_field === "created_by" ||
                                item.id_field === "modify_time" ||
                                item.id_field === "owner" ||
                                item.id_field === "modify_by"
                              ) {
                                tempMeta.push(item);
                                searchKeyTmp[item.id_field] = item.value;
                              } else {
                                tempData.push(item);
                                searchKeyTmp[item.id_field] = item.value;
                              }
                            });

                            dispatch(
                              loadPagination({
                                current_page: 1,
                                record_per_page: 50,
                                object_id: objectId,
                                search_with: {
                                  meta: tempMeta,
                                  data: tempData,
                                },
                              })
                            );
                          }}
                        >
                          <img alt="" src={Reload} />
                        </div>
                      ) : (
                        `of ${
                          totalRecord
                            ? totalRecord === -1
                              ? "1M+"
                              : totalRecord
                            : 0
                        } records`
                      )}
                    </div>
                  </>
                )}
                <div
                  className="left-pagi"
                  style={{
                    pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                    cursor: `${currentPage === 1 ? "not-allowed" : "pointer"}`,
                    opacity: `${currentPage === 1 ? 0.5 : 1}`,
                  }}
                  onClick={() => {
                    setListCheck([]);
                    setLastID(
                      dataSource.length > 0
                        ? dataSource[dataSource.length - 1]._id
                        : null
                    );
                    let currentPageTemp = currentPage;
                    currentPageTemp = currentPageTemp - 1;
                    setCurrentPage(currentPageTemp);
                    setFirstID(
                      dataSource.length > 0 ? dataSource[0]._id : null
                    );
                    reloadData(
                      listSearch,
                      dataSource.length > 0 ? dataSource[0]._id : null,
                      null,
                      sortBy
                    );
                  }}
                >
                  <img alt="" src={LeftPagi} />
                </div>
                <div
                  className="right-pagi"
                  style={{
                    pointerEvents: `${
                      dataSource.length < recordPerPage ? "none" : ""
                    }`,
                    cursor: `${
                      dataSource.length < recordPerPage
                        ? "not-allowed"
                        : "pointer"
                    }`,
                    opacity: `${dataSource.length < recordPerPage ? 0.5 : 1}`,
                  }}
                  onClick={() => {
                    setListCheck([]);
                    setLastID(
                      dataSource.length > 0
                        ? dataSource[dataSource.length - 1]._id
                        : null
                    );
                    let currentPageTemp = currentPage;
                    currentPageTemp = currentPageTemp + 1;
                    setCurrentPage(currentPageTemp);
                    setFirstID(
                      dataSource.length > 0 ? dataSource[0]._id : null
                    );
                    reloadData(
                      listSearch,
                      null,
                      dataSource.length > 0
                        ? dataSource[dataSource.length - 1]._id
                        : null,
                      sortBy
                    );
                  }}
                >
                  <img alt="" src={RightPagi} />
                </div>
              </NewCustomPagination>
            )}

            <ModalConfimDelete
              title={t("kanbanView.deleteRecord")}
              decs={t("common.descriptionDelete")}
              methodDelete={
                objectId === "obj_crm_campaign_00001"
                  ? deleteCampaign
                  : deleteRecord
              }
              dataDelete={{
                data: {
                  id: idDelete,
                  object_id: objectId,
                },
                load: {
                  object_id: objectId,
                  first_record_id: firstID,
                  last_record_id: lastID,
                  search_with: {
                    meta: listSearch.filter(
                      (item) =>
                        item.id_field === "created_date" ||
                        item.id_field === "created_by" ||
                        item.id_field === "modify_time" ||
                        item.id_field === "owner" ||
                        item.id_field === "modify_by"
                    ),
                    data: listSearch.filter(
                      (item) =>
                        item.id_field !== "created_date" &&
                        item.id_field !== "created_by" &&
                        item.id_field !== "modify_time" &&
                        item.id_field !== "owner" &&
                        item.id_field !== "modify_by"
                    ),
                  },
                },
              }}
              setShowModalDelete={() => {}}
            />

            <ModalRecord
              // recordPerPage={recordPerPage}
              // currentPage={currentPage}
              form={form}
              objectId={objectId}
              recordID={recordID}
              setRecordID={setRecordID}
              onFinish={onFinish}
              userDetail={userDetail}
              setEditingKey={setEditingKey}
              setDataConfirm={setDataConfirm}
              setShowConfirm={setShowConfirm}
              reload={reload}
              $visibleModalRDB={$visibleModalRDB}
            />
          </ContentWrap>

          {flagDelete === "one" ? (
            <ModalDelete
              // openConfirm={openDelete}
              // setOpenConfirm={setOpenDelete}
              title={t("kanbanView.deleteRecord")}
              decs={t("common.descriptionDelete")}
              methodDelete={
                objectId === "obj_crm_campaign_00001"
                  ? deleteCampaign
                  : deleteRecord
              }
              dataDelete={{
                data: {
                  id: idDelete,
                  object_id: objectId,
                },
                load: {
                  object_id: objectId,
                  first_record_id: firstID,
                  last_record_id: lastID,
                  search_with: {
                    meta: listSearch.filter(
                      (item) =>
                        item.id_field === "created_date" ||
                        item.id_field === "created_by" ||
                        item.id_field === "modify_time" ||
                        item.id_field === "owner" ||
                        item.id_field === "modify_by"
                    ),
                    data: listSearch.filter(
                      (item) =>
                        item.id_field !== "created_date" &&
                        item.id_field !== "created_by" &&
                        item.id_field !== "modify_time" &&
                        item.id_field !== "owner" &&
                        item.id_field !== "modify_by"
                    ),
                  },
                },
              }}
              setShowModalDelete={() => {}}
            />
          ) : flagDelete === "many" && idDelete === "" ? (
            <ModalDelete
              // openConfirm={openMassDelete}
              // setOpenConfirm={setOpenMassDelete}
              title={t("common.theseRecord")}
              decs={t("common.descriptionDelete")}
              methodDelete={massDelete}
              dataDelete={{
                data: {
                  ids: listCheck,
                  object_id: objectId,
                },
                type: objectId === "obj_crm_campaign_00001" ? "campaign" : "",
                load: {
                  object_id: objectId,
                  first_record_id: firstID,
                  last_record_id: lastID,
                  search_with: {
                    meta: listSearch.filter(
                      (item) =>
                        item.id_field === "created_date" ||
                        item.id_field === "created_by" ||
                        item.id_field === "modify_time" ||
                        item.id_field === "owner" ||
                        item.id_field === "modify_by"
                    ),
                    data: listSearch.filter(
                      (item) =>
                        item.id_field !== "created_date" &&
                        item.id_field !== "created_by" &&
                        item.id_field !== "modify_time" &&
                        item.id_field !== "owner" &&
                        item.id_field !== "modify_by"
                    ),
                  },
                },
              }}
              setShowModalDelete={() => {}}
            />
          ) : (
            ""
          )}

          <ModalMassEdit
            resetSelectedRowKeys={resetSelectedRowKeys}
            selectedRowKeys={listCheck}
            openMassEdit={openMassEdit}
            openMerge={openMerge}
            setOpenMassEdit={setOpenMassEdit}
            form={form}
            methodData={{
              load: {
                object_id: objectId,
                first_record_id: firstID,
                last_record_id: lastID,
                search_with: {
                  meta: listSearch.filter(
                    (item) =>
                      item.id_field === "created_date" ||
                      item.id_field === "created_by" ||
                      item.id_field === "modify_time" ||
                      item.id_field === "owner" ||
                      item.id_field === "modify_by"
                  ),
                  data: listSearch.filter(
                    (item) =>
                      item.id_field !== "created_date" &&
                      item.id_field !== "created_by" &&
                      item.id_field !== "modify_time" &&
                      item.id_field !== "owner" &&
                      item.id_field !== "modify_by"
                  ),
                },
              },
            }}
            objectId={objectId}
          />

          <ModalMerge
            openMerge={openMerge}
            openMassEdit={openMassEdit}
            setOpenMerge={setOpenMerge}
            selectedRowKeys={listCheck}
            resetSelectedRowKeys={resetSelectedRowKeys}
            objectId={objectId}
            methodData={{
              load: {
                object_id: objectId,
                first_record_id: firstID,
                last_record_id: lastID,
                search_with: {
                  meta: listSearch.filter(
                    (item) =>
                      item.id_field === "created_date" ||
                      item.id_field === "created_by" ||
                      item.id_field === "modify_time" ||
                      item.id_field === "owner" ||
                      item.id_field === "modify_by"
                  ),
                  data: listSearch.filter(
                    (item) =>
                      item.id_field !== "created_date" &&
                      item.id_field !== "created_by" &&
                      item.id_field !== "modify_time" &&
                      item.id_field !== "owner" &&
                      item.id_field !== "modify_by"
                  ),
                },
              },
            }}
          />

          <ModalConfirm
            title="Confirm"
            decs="Are you sure you want to proceed?"
            method={runDynamicButton}
            data={dataConfirm}
            img={ImgConfirm}
            showConfirm={showConfirm}
            setShowConfirm={setShowConfirm}
          />

          <ModalRunDynamicButton
            data={dataConfirm}
            method={runDynamicButton}
            visibleModalRDB={visibleModalRDB}
            $visibleModalRDB={$visibleModalRDB}
            dataRecord={recordData}
          />

          <ModalImport
            isModalVisible={openImport}
            setIsModalVisible={setOpenImport}
            objectId={objectId}
          />

          <CustomView
            visible={openCustomView}
            setVisible={setOpenCustomView}
            objectId={objectId}
            reload={reload}
          />

          <ModalDuplicate />

          <ModalAddCampaign
            showModalCampaign={showModalCampaign}
            $showModalCampaign={$showModalCampaign}
          />
          {/* <Popup popup={popup} objectId={objectId} /> */}
        </>
      )}
    </>
  );
};

export default NewObject;

const WrapIframe = styled.div`
  height: calc(100vh - 100px);
  padding: 24px;
`;

const ContentWrap = styled.div`
  width: 100%;

  overflow: hidden;
  /* background: #fff; */
  padding: 16px;
  height: calc(100% - 55px);
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  padding: 0px 10px 10px 10px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }

  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;

const TableWrap = styled.div`
  width: 100%;
  /* height: calc(100% - 187px); */
  height: 100%;

  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  background: #fff;
  overflow-x: auto;
  overflow-y: auto;
  &::-webkit-scrollbar {
    height: 12px !important;
    width: 12px !important;
  }
  &::-webkit-scrollbar-thumb {
    background: #d9d9d9;
  }

  /* padding: 10px; */
  /* padding-right: 0; */
  table {
    /* width: 100%; */
    min-width: 100%;
    width: max-content;
    table-layout: auto;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      text-align: left;

      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;

      overflow: hidden;
      resize: horizontal;
      background: #f0f0f0;
      min-width: 54px;
      /* width: 60px; */
      white-space: nowrap;
      /* min-width: fit-content; */

      &:first-child {
        border-left: none;
        text-align: center;
        width: 54px;
        max-width: 54px;
        resize: none;
        position: sticky;
        z-index: 4;
        left: 0;
        box-shadow: inset -1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:nth-child(2) {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:last-child {
        position: sticky;
        z-index: 4;
        right: 0;
        border-right: none;
        resize: none;
        width: 122px;
        max-width: 122px;

        .hidden__component {
          display: none;
        }
      }
    }

    .table-body td {
      background: #fff;
      border-top: none !important;
      border-left: none !important;
      border-right: none !important;
      border-bottom: 1px solid #ddd;
      padding: 6px 16px;

      max-width: 200px;

      &:first-child {
        /* border-left: 1px solid #ddd; */
        text-align: center;
        position: sticky;
        z-index: 2;
        left: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }

      &:last-child {
        /* border-right: 1px solid #ddd; */
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }
    }

    tr {
      :hover {
        background: #f5f5f5;
      }
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-checkbox-indeterminate .ant-checkbox-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const ContentHeader = styled.div`
  /* border-left: 1px solid #ddd;
  border-right: 1px solid #ddd; */
`;

const CustomSearch = styled(Input)`
  border-radius: 5px;
  &:hover,
  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
`;

const CustomSearchNumber = styled(InputNumber)`
  border-radius: 5px;
  width: 100%;
  &:hover,
  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
`;

const CustomSelect = styled(Select)`
  margin-bottom: 0px !important;

  .ant-select-selection-placeholder {
    font-weight: normal;
  }

  .ant-select-selector {
    border-radius: 5px !important;
  }
`;

const TitleWrap = styled.div`
  padding: ${(props) => (props.isGlobal ? "12px 8px" : "8px")};
  display: flex;
  justify-content: ${(props) => (props.align ? "center" : "unset")};
  span {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #252424;
    font-weight: normal;
  }
`;

const SearchWrap = styled.div`
  border-top: 1px solid #ddd;
  display: ${(props) => (props.isGlobal ? "none" : "")};
  padding: 8px;
`;

const IconHeader = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 6px;

  .anticon svg {
    font-size: 12px;
    /* color: rgba(0, 0, 0, 0.25); */
    cursor: pointer;
    transition: all 0.5s;
  }
`;

const ActionWrap = styled.div`
  display: flex;
  justify-content: space-between;
  img {
    width: 18px;
    &:hover {
      cursor: pointer;
    }
  }
`;

const CustomTd = styled.td`
  background: ${({ background }) => background}!important;

  .ant-typography {
    color: ${({ color }) => color}!important;
    font-family: ${({ fontWeight }) =>
      fontWeight === "300"
        ? "var(--roboto-300)"
        : fontWeight === "700"
        ? "var(--roboto-700)"
        : "var(--roboto-400)"}!important;
    font-size: ${({ fontSize }) => fontSize}px!important;
  }
`;

const CustomHeader = styled.div`
  /* max-width: max-content; */
  /* width: max-content; */
  min-width: 100%;
  /* overflow-x: scroll; */
  .ant-btn:active {
    border: none;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    /* line-height: 22px; */
    /* identical to box height, or 137% */

    text-align: center;

    /* Character/Color text main */

    color: #2c2c2c;
    background-color: #fff;
  }

  .btn {
    position: relative;
    /* transition: all 200ms cubic-bezier(0.77, 0, 0.175, 1); */
    transition: all 0.5s;
    cursor: pointer;
    border-radius: 2px;
  }

  .btn:before,
  .btn:after {
    content: "";
    position: absolute;
    /* transition: inherit; */
    z-index: -1;
  }

  /* .btn:hover {
    color: #ffffff;
    transition-delay: 0.5s;
  }

  .btn:hover:before {
    transition-delay: 0s;
  }

  .btn:hover:after {
    background: ${(props) => props.theme.main};
    transition-delay: 0.35s;
  } */

  /* From Top */

  .btn-top:before,
  .btn-top:after {
    left: 0;
    height: 0;
    width: 100%;
  }

  .btn-top:before {
    bottom: 0;
    /* border: 1px solid ${(props) => props.theme.main}; */
    border-top: 0;
    border-bottom: 0;
  }

  .btn-top:after {
    top: 0;
    height: 0;
  }

  .btn-top:hover:before,
  .btn-top:hover:after {
    height: 100%;
  }

  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

// const CustomButton = styled(Button)`
//   border: none;
//   font-style: normal;
//   font-family: var(--roboto-400);
//   font-size: 16px;
//   line-height: 22px;
//   /* identical to box height, or 137% */

//   text-align: center;

//   color: #2c2c2c;
//   background-color: #fff;
//   :hover {
//     background-color: ${(props) => props.theme.main};
//     color: #fff;
//   }
// `;

const CustomNewButton = styled(Button)`
  border: 1px solid #d9d9d9 !important;
  height: 34px;
  :hover {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const CustomMenuHeader = styled.div`
  background-color: #fff;
  padding: 11px;
  width: 100%;
  display: flex;
  padding-left: 24px;
  .active {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    color: #fff;
  }
  -webkit-animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  @-webkit-keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
`;

const CarouselWrap = styled.div`
  width: calc(100% - 260px);
  .react-multiple-carousel__arrow--right {
    right: 0;
    background: linear-gradient(
      268deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 90.08%
    );
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow--left {
    background: linear-gradient(
      90deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 87.93%
    );
    left: 0;
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow::before {
    color: ${(props) => props.theme.main};
    font-size: 20px;
  }
  .react-multiple-carousel__arrow--left::before {
    margin-left: -50px;
  }
  .react-multiple-carousel__arrow--right::before {
    margin-right: -50px;
  }
`;

const MenuItem = styled.div`
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #2c2c2c;
  padding: 5px 16px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  margin-right: 8px;
  height: calc(100% - 1px);

  :hover {
    /* background-color: ${(props) => props.theme.darker}; */
    border: 1px solid ${(props) => props.theme.darker};

    border-radius: 2px;
    cursor: pointer;
  }
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  text-align: center;

  color: #2c2c2c;
  margin-left: 8px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px 16px;
  /* :hover {
    color: ${(props) => props.theme.main};
  } */
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomMenuItem = styled.div`
  cursor: pointer;
  :hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
    border-right: 2px solid ${(props) => props.theme.darker};

    .pin-img {
      display: block;
      width: 16px;
      margin-right: 8px;
    }
  }
  padding: 8px 8px 8px 12px;
`;

const CustimMenuWrapper = styled(Menu)`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
    border-right: 2px solid ${(props) => props.theme.darker};
  }
  li {
    padding-top: 8px;
    padding-bottom: 8px;
  }
`;

const CustomSubMenuWrapper = styled(SubMenu)`
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomOption = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .pin-img {
    display: none;
  }
  .unpin-img {
    width: 18px;
    margin-right: 8px;
  }
`;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomWrapperAction = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-left: 16px;
  margin-top: 14px;
  .ant-btn:hover {
    border-color: ${(props) => props.theme.main};
    /* color: ${(props) => props.theme.main}; */
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const ViewWrap = styled.div`
  width: 140px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  cursor: pointer;
  margin-left: 16px;
`;
const CustomItem = styled(Menu.Item)`
  .ant-dropdown-menu-title-content {
    display: flex;
    justify-content: space-between;
  }

  img {
    width: 19px;
  }
`;

const ItemText = styled.span`
  font-size: 16px;
  line-height: 22px;
  margin-right: 30px;
  margin-left: 8px;
`;

const CustomDisplay = styled.div`
  margin-left: 10px;
  padding-top: 6px;
  font-size: 16px;
  line-height: 22px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 150px;
  span {
    margin-left: 6px;
    text-decoration: underline;
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;

const CustomDisplayView = styled.div`
  font-size: 16px;
  line-height: 22px;

  span {
    margin-left: 6px;
    text-decoration: underline;
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;

const CustomClose = styled.div`
  display: flex;
  justify-content: center;
  img {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: #2c2c2c;
  cursor: default;
`;

const RightWrap = styled.div`
  display: flex;
  .ant-form-item {
    margin-bottom: 0px;
  }
`;

const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: default;
`;

const WrapperLoading = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  background-color: rgba(255, 255, 255, 0.5);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10000 !important;
  transition: all 5s ease-in;
  img {
    margin-top: 300px;
    width: 250px;
  }
  .lds-ellipsis {
    width: 70px;
    /* display: inline-block; */
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: grey;
  }
  .lds-ellipsis div {
    position: absolute;
    top: 33px;
    width: 13px;
    height: 13px;
    left: 50%;
    border-radius: 50%;
    background: ${(props) => props.theme.main};
    animation-timing-function: cubic-bezier(0, 1, 1, 0);
  }
  .lds-ellipsis div:nth-child(1) {
    left: 8px;
    animation: lds-ellipsis1 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(2) {
    left: 8px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(3) {
    left: 32px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(4) {
    left: 56px;
    animation: lds-ellipsis3 0.6s infinite;
  }
  @keyframes lds-ellipsis1 {
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
    }
  }
  @keyframes lds-ellipsis3 {
    0% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
  @keyframes lds-ellipsis2 {
    0% {
      transform: translate(0, 0);
    }
    100% {
      transform: translate(24px, 0);
    }
  }
`;

const CheckboxWrap = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: ${(props) => (props.isGlobal ? "0px" : "16px")};
`;

const OptionView = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Text = styled.div`
  margin: 0 6px;
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const CustomInputSearch = styled(Search)`
  margin-right: 16px;
  width: 270px;
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 30px;
  }
  button {
    height: 40px;
  }
`;
