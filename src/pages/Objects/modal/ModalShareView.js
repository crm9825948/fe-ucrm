import { Button, Form, Modal, Select } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { shareView } from "redux/slices/objects";
import styled from "styled-components";

const { Option } = Select;

const ModalShareView = ({
  isModalVisible,
  setIsModalVisible,
  idView,
  listShare,
  objectId,
}) => {
  const { listAllGroups } = useSelector((state) => state.groupReducer);
  const [change, setChange] = useState(false);
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
    setChange(false);
  };
  const [form] = Form.useForm();
  const onFinish = (value) => {
    let data = {
      dataShare: { share_to: value.group, view_id: idView },
      dataLoad: {
        object_id: objectId,
      },
    };
    dispatch(shareView(data));
    setIsModalVisible(false);
    setChange(false);
    form.resetFields();
  };

  useEffect(() => {
    form.setFieldsValue({
      group: listShare ? listShare : [],
    });
  }, [listShare, form, isModalVisible]);

  return (
    <>
      <CustomModal
        width={600}
        title="Share to group"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={false}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          onFinish={onFinish}
          onFinishFailed={() => {}}
          autoComplete="off"
        >
          <Form.Item label="Share to group" name="group">
            <Select
              mode="multiple"
              onChange={() => {
                setChange(true);
              }}
            >
              {listAllGroups &&
                listAllGroups.map((item, idx) => {
                  return (
                    <Option key={item._id} value={item._id}>
                      {item.name}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>

          <CustomFooter>
            <CustomButtonSave size="large" htmlType="submit" disabled={!change}>
              {t("common.save")}
            </CustomButtonSave>
            <CustomButtonCancel size="large" onClick={() => handleCancel()}>
              {t("common.cancel")}
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

export default ModalShareView;

const CustomModal = styled(Modal)`
  .ant-modal-footer {
    display: none;
  }
  .ant-modal-body {
    padding: 24px 24px 18px 24px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    padding: 6px 24px;
    background: #f2f4f5;
  }
  .ant-modal-title {
    font-family: var(--roboto-700);
    font-size: 16px;
  }
  .ant-modal-close-x {
    line-height: 36px;
    font-size: 16px;
    color: #000000;
  }
  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
  .ant-form-item-label {
    display: flex;
  }
`;
const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-top: 16px;
`;

const CustomButtonSave = styled(Button)`
  width: 100px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 100px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
