import { Button, Form, Modal, Radio, Space } from "antd";
import Typography from "antd/lib/typography";
import img from "assets/icons/common/merge.png";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { getRecords, mergeRecord } from "redux/slices/objects";
import styled from "styled-components";
import ModalConfirm from "./ModalConfirm";
const { Text } = Typography;

const ModalMerge = (props) => {
  const {
    openMerge,
    setOpenMerge,
    selectedRowKeys,
    methodData,
    resetSelectedRowKeys,
  } = props;
  const [openConfirm, setOpenConfirm] = useState(false);
  const dispatch = useDispatch();
  const { objectId } = useParams();
  const [form] = Form.useForm();

  const { recordToMerge, isLoading, userAssignTo } = useSelector(
    (state) => state.objectsReducer
  );

  const handleOk = () => {
    setOpenMerge(false);
  };

  const handleCancel = () => {
    setOpenMerge(false);
  };

  useEffect(() => {
    if (isLoading === false) {
      // dispatch(setShowModalConfirm(false));
      resetSelectedRowKeys();
      setOpenConfirm(false);
      setOpenMerge(false);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    if (openMerge === true) {
      dispatch(
        getRecords({
          ids: selectedRowKeys,
          object_id: objectId,
        })
      );
    }
    // eslint-disable-next-line
  }, [openMerge]);

  const [dataMethod, setDataMethod] = useState({});

  useEffect(() => {
    let newObj = {};
    Object.entries(
      (recordToMerge &&
        recordToMerge.data &&
        recordToMerge.data.length > 0 &&
        recordToMerge.data[0]) ||
        {}
      // eslint-disable-next-line
    ).forEach(([key, value], idx) => {
      if (typeof value === "object") {
        if (
          recordToMerge &&
          recordToMerge.data &&
          recordToMerge.data.length === 2
        ) {
          if (
            recordToMerge &&
            recordToMerge.data &&
            recordToMerge.data.length > 0 &&
            recordToMerge.data[0][key]?.value
          ) {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[0][key]),
            });
          } else if (
            recordToMerge &&
            recordToMerge.data &&
            recordToMerge.data.length > 0 &&
            recordToMerge.data[1][key]?.value
          ) {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[1][key]),
            });
          } else {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[0][key]),
            });
          }
        }
        if (
          recordToMerge &&
          recordToMerge.data &&
          recordToMerge.data.length === 3
        ) {
          if (
            recordToMerge &&
            recordToMerge.data &&
            recordToMerge.data.length > 0 &&
            recordToMerge.data[0][key]?.value
          ) {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[0][key]),
            });
          } else if (
            recordToMerge &&
            recordToMerge.data &&
            recordToMerge.data.length > 0 &&
            recordToMerge.data[1][key]?.value
          ) {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[1][key]),
            });
          } else if (
            recordToMerge &&
            recordToMerge.data &&
            recordToMerge.data.length > 0 &&
            recordToMerge.data[2][key]?.value
          ) {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[2][key]),
            });
          } else {
            newObj[key] = JSON.stringify({
              id_field: key,
              ...(recordToMerge &&
                recordToMerge.data &&
                recordToMerge.data.length > 0 &&
                recordToMerge.data[0][key]),
            });
          }
        }
      }
    });
    newObj["assignTo"] =
      recordToMerge &&
      recordToMerge.data &&
      recordToMerge.data.length > 0 &&
      recordToMerge.data[0]["owner"];

    newObj["id"] =
      recordToMerge &&
      recordToMerge.data &&
      recordToMerge.data.length > 0 &&
      recordToMerge.data[0]["_id"];

    form.setFieldsValue(newObj);
    // eslint-disable-next-line
  }, [recordToMerge]);

  const onFinish = (values) => {
    let arr = [];
    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], index) => {
      if (key !== "assignTo" && key !== "id") {
        arr.push(JSON.parse(value));
      }
    });
    let slaves = selectedRowKeys.filter((item) => item !== values["id"]);
    setDataMethod({
      fields: arr,
      masterID: values["id"],
      object_id: objectId,
      owner: values["assignTo"],
      slaves: slaves,
    });
    // dispatch(setShowModalConfirm(true));
    setOpenConfirm(true);
  };

  return (
    <>
      <CustomModal
        title="Gộp bản ghi"
        visible={openMerge}
        onOk={handleOk}
        onCancel={handleCancel}
        width={960}
        footer={false}
      >
        <div className="content">
          Khi hợp nhất các bản ghi, bản ghi chính được cập nhật với các trường
          được chọn. Tất cả các danh sách liên quan, các mục nguồn cấp dữ liệu
          và các bản ghi con được hợp nhất vào các bản ghi chính.
        </div>
        <div style={{ marginTop: "20px", overflowX: "scroll", width: "100%" }}>
          <Form
            form={form}
            onFinish={onFinish}
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            colon={false}
            labelAlign="left"
            onValuesChange={(value, values) => {}}
          >
            <Header label=" " style={{ width: "100%" }}>
              {selectedRowKeys.map((item, idx) => {
                return <Title>Record {idx}</Title>;
              })}
            </Header>
            {recordToMerge &&
              recordToMerge.fields &&
              recordToMerge.fields.map((item, idx) => {
                return (
                  <FormCustom label={`${item.name}`} name={item.ID}>
                    <Radio.Group
                      onChange={(e) => {
                        form.setFieldsValue({
                          [item.ID]: e.target.value,
                        });
                      }}
                    >
                      <Space direction="horizontal">
                        {recordToMerge &&
                          recordToMerge.data &&
                          recordToMerge.data.map((record, index) => {
                            if (
                              item.type === "user" &&
                              record[item.ID] &&
                              record[item.ID].value
                            ) {
                              return (
                                <Radio
                                  value={JSON.stringify({
                                    id_field: item.ID,
                                    ...record[item.ID],
                                  })}
                                >
                                  <Text
                                    ellipsis={{
                                      tooltip: record[item.ID].fullname,
                                    }}
                                    style={{ width: "150px" }}
                                  >
                                    {record[item.ID].fullname}
                                  </Text>
                                </Radio>
                              );
                            } else
                              return (
                                <Radio
                                  value={JSON.stringify({
                                    id_field: item.ID,
                                    ...record[item.ID],
                                  })}
                                >
                                  <Text
                                    ellipsis={{
                                      tooltip:
                                        record[item.ID] &&
                                        record[item.ID].value,
                                    }}
                                    style={{ width: "150px" }}
                                  >
                                    {record[item.ID] && record[item.ID].value}
                                  </Text>
                                </Radio>
                              );
                          })}
                      </Space>
                    </Radio.Group>
                  </FormCustom>
                );
              })}
            <FormCustom label="Assign to" name="assignTo">
              <Radio.Group
                onChange={(e) => {
                  form.setFieldsValue({
                    owner: e.target.value,
                  });
                }}
              >
                <Space direction="horizontal">
                  {recordToMerge &&
                    recordToMerge.data &&
                    recordToMerge.data.map((record, index) => {
                      let name = userAssignTo.filter(
                        (item) => item._id === record["owner"]
                      );
                      let fullName =
                        name.length > 0
                          ? `${name[0].First_Name} ${name[0].Middle_Name} ${name[0].Last_Name}`
                          : "";
                      return (
                        <Radio value={record["owner"]} key={record["owner"]}>
                          <Text
                            ellipsis={{ tooltip: fullName }}
                            style={{ width: "150px" }}
                          >
                            {fullName}
                          </Text>
                        </Radio>
                      );
                    })}
                </Space>
              </Radio.Group>
            </FormCustom>

            <FormCustom label="Bản ghi" name="id">
              <Radio.Group
                onChange={(e) => {
                  form.setFieldsValue({
                    owner: e.target.value,
                  });
                }}
              >
                <Space direction="horizontal">
                  {recordToMerge &&
                    recordToMerge.data &&
                    recordToMerge.data.map((record, index) => {
                      return (
                        <Radio value={record["_id"]} key={record["_id"]}>
                          <Text
                            ellipsis={{ tooltip: `Record ${index}` }}
                            style={{ width: "150px" }}
                          >
                            Record {index}
                          </Text>
                        </Radio>
                      );
                    })}
                </Space>
              </Radio.Group>
            </FormCustom>
            <CustomFooter>
              <CustomButtonSave
                size="large"
                htmlType="submit"
                isLoading={isLoading}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  setOpenMerge(false);
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>

          {openMerge ? (
            <ModalConfirm
              openConfirm={openConfirm}
              setOpenConfirm={setOpenConfirm}
              title={"Bạn có chắc chắn thay đổi các bản ghi này?"}
              decs={"Sau khi lưu bản ghi, dữ liệu sẽ không thể hoàn tác."}
              method={mergeRecord}
              data={{
                data: dataMethod,
                methodData,
              }}
              // setOpenModal={setOpenConfirm}
              img={img}
            />
          ) : (
            ""
          )}
        </div>
      </CustomModal>
    </>
  );
};

export default ModalMerge;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }

  .content {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    /* or 21px */

    display: flex;
    align-items: center;
    text-align: center;

    /* Neutral / 8 */

    color: #595959;
    padding: 0 130px 0 130px;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: #1c9292;
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: #1c9292;
    color: #fff;
  }
`;

const Header = styled(Form.Item)`
  position: sticky;
  top: 0;
  z-index: 2;
  display: flex;
  background: #f0f0f0;
  height: 40px;
  border: 1px solid #d9d9d9;
  margin-bottom: 0;
  .ant-row {
    width: 100%;
  }
  .ant-form-item-control-input {
    height: 100%;
  }

  .ant-form-item-control-input-content {
    display: flex;
    height: 100%;
  }
`;

const Title = styled.div`
  display: flex;
  align-items: center;
  justify-content: left;
  flex: 1;
  border-right: 1px solid #d9d9d9;
  height: 100%;
  color: #000;
  font-family: var(--roboto-700);
  padding-left: 16px;

  :first-child {
    border-left: 1px solid #d9d9d9;
  }

  :last-child {
    border-right: none;
  }
`;

const FormCustom = styled(Form.Item)`
  border: 1px solid #ededed;
  border-top: none;
  margin-bottom: 0;

  .ant-form-item-label > label {
    padding-left: 16px;
    height: 100%;
    display: flex;
    align-items: center;
  }

  .ant-radio-group {
    width: 100%;
  }

  .ant-space {
    width: 100%;
    justify-content: center;
  }

  .ant-space-item {
    flex: 1;
    display: flex;
    justify-content: flex-start;
    align-items: center;
  }
  /* .ant-radio-disabled  */
  .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;
