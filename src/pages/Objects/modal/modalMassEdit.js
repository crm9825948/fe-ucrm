import { Button, Form, Modal, Select } from "antd";
import ModalConfirm from "components/Modal/ModalConfirm";
import { BE_URL } from "constants/constants";
import moment from "moment";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { massEdit, setLinkingFieldValue } from "redux/slices/objects";
import styled from "styled-components";
import Date from "./fieldsType/date";
import Datetime from "./fieldsType/dateTime";
import DynamicField from "./fieldsType/dynamicField";
import Email from "./fieldsType/email";
import File from "./fieldsType/file";
import FormulaField from "./fieldsType/formulaField";
import LinkingObject from "./fieldsType/linkingObject";
import Number from "./fieldsType/number";
import SelectType from "./fieldsType/select";
import Text from "./fieldsType/text";
import Textarea from "./fieldsType/textarea";
import User from "./fieldsType/user";
import Lookup from "./fieldsType/lookup";
import img from "assets/icons/common/confirm.png";
import { setShowModalConfirm } from "redux/slices/global";

const { Option } = Select;

const ModalMassEdit = (props) => {
  const {
    openMassEdit,
    setOpenMassEdit,
    methodData,
    objectId,
    resetSelectedRowKeys,
    selectedRowKeys,
  } = props;
  const [formValue, setFormValue] = useState({});
  // const [openConfirm, setOpenConfirm] = useState(false);
  const {
    isLoading,
    searchList,
    // selectedRowKeys,
    // fields,
    userAssignTo,
    linkingFieldValue,
    listObjectField,
  } = useSelector((state) => state.objectsReducer);

  const [dataFilterLinking, setDataFilterLinking] = useState({});
  const [allFields, setAllFields] = useState([]);
  const [form] = Form.useForm();
  const [fieldForm, setFieldForm] = useState([]);
  const [openModal] = useState(null);
  const dispatch = useDispatch();
  const [dataMethod, setDataMethod] = useState({});
  useEffect(() => {
    if (isLoading === false) {
      // setOpenConfirm(false);
      resetSelectedRowKeys();
      dispatch(setShowModalConfirm(false));
      setOpenMassEdit(false);
      setFieldForm([]);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  const handleOk = () => {
    setOpenMassEdit(false);
  };

  useEffect(() => {
    form.setFieldsValue({
      ...form.getFieldsValue(),
      ...linkingFieldValue,
    });
  }, [linkingFieldValue, form]);

  const handleCancel = () => {
    setOpenMassEdit(false);
    setFieldForm([]);
    form.resetFields();
  };

  const onFinish = (values) => {
    console.log("values", values);
    let fieldsObject = {};
    /* eslint-disable-next-line */

    /* eslint-disable-next-line */
    fieldForm.forEach((item, index) => {
      fieldsObject[item.ID] = { ...item };
    });

    let listValue = [];
    /* eslint-disable-next-line */
    Object.entries(values).forEach(([key, value], idx) => {
      if (key !== "fields")
        if (key !== "assignTo" && key !== "owner") {
          if (fieldsObject[key].type === "file" && value) {
            const result = [];
            /* eslint-disable-next-line */
            value.fileList.map((item, idx) => {
              result.push(BE_URL + item.response.data[0]);
            });
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: result.length === 0 ? null : result,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "date" && value) {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value ? moment(value).format("YYYY-MM-DD") : null,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "datetime-local" && value) {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "linkingobject") {
            let newItem = {
              id_field: key,
              id_field_related_record:
                (value && value.id_field_related_record) || null,
              id_related_record: (value && value.id_related_record) || null,
              object_related: (value && value.object_related) || null,
              value: (value && value.value) || null,
            };
            listValue.push(newItem);
          } else if (fieldsObject[key].type === "lookup") {
            let newItem = {
              id_field: key,
              id_field_related_record:
                (value && value.id_field_related_record) || null,
              id_related_record: (value && value.id_related_record) || null,
              object_related: (value && value.object_related) || null,
              value: (value && value.value) || null,
            };
            listValue.push(newItem);
          } else {
            let newItem = {
              id_field: key,
              id_field_related_record: null,
              id_related_record: null,
              object_related: null,
              value: value ? value : null,
            };
            listValue.push(newItem);
          }
        }
    });

    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });

    let newItem = {};
    // eslint-disable-next-line
    listValue.forEach((item, idx) => {
      newItem[item.id_field] = { ...item };
    });
    if (values["owner"]) {
      setDataMethod({
        data: newItem,
        ids: selectedRowKeys,
        object_id: objectId,
        owner: values["owner"],
      });
    } else {
      setDataMethod({
        data: newItem,
        ids: selectedRowKeys,
        object_id: objectId,
      });
    }

    // setOpenConfirm(true);
    dispatch(setShowModalConfirm(true));
  };

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        // eslint-disable-next-line
        section.fields.map((item, idx) => {
          if (item.mass_edit) {
            arr.push(item);
          }
        });
      });
    arr.push({
      ID: "owner",
      active: true,
      auto_fill: true,
      hidden: false,
      mass_edit: true,
      name: "Assign to",
      type: "owner",
      permission_hidden: false,
    });
    setAllFields(arr);
  }, [listObjectField]);

  useEffect(() => {
    let newObject = {};
    /* eslint-disable-next-line */
    Object.entries(linkingFieldValue).forEach(([key, value], index) => {
      newObject[key] = {
        id_field_related_record: null,
        id_related_record: null,
        object_related: null,
        value: null,
      };
    });

    let temp = {};
    setDataFilterLinking({
      ...temp,
      ...newObject,
    });

    dispatch(setLinkingFieldValue(newObject));
    form.setFieldsValue(newObject);
    /* eslint-disable-next-line */
  }, [open]);

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            listObjectField={listObjectField}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} />;
      case "date":
        return <Date field={field} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={""}
            setRecordID={() => {}}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={openModal}
            recordID={""}
            setRecordID={() => {}}
          />
        );
      case "file":
        return <File field={field} />;
      case "dynamic-field":
        return <DynamicField field={field} />;
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            selectedRowKeys={selectedRowKeys}
          />
        );
      case "formula":
        return <FormulaField field={field} />;
      case "lookup":
        return <Lookup field={field} form={form} recordData={{}} />;
      case "owner":
        return (
          <Form.Item
            label="Assign to"
            name="owner"
            rules={[{ required: true, message: "Please select assign to!" }]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                return option.children
                  .join("")
                  .toLowerCase()
                  .includes(inputValue.toLowerCase());
              }}
            >
              {userAssignTo &&
                userAssignTo.map((user, idx) => {
                  return (
                    <Option
                      value={user._id}
                      key={user._id}
                      disabled={user.disable}
                    >
                      {user.Last_Name} {user.Middle_Name} {user.First_Name}
                    </Option>
                  );
                })}
            </Select>
          </Form.Item>
        );

      default:
        break;
    }
  };

  return (
    <>
      <CustomModal
        title="Sửa đồng loạt"
        visible={openMassEdit}
        onOk={handleOk}
        onCancel={handleCancel}
        width={600}
        footer={false}
      >
        <Form
          name="basic"
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          form={form}
          autoComplete="off"
          onValuesChange={(value, values) => {
            setFormValue(values);
          }}
        >
          <Form.Item
            label="Chọn trường thông tin"
            name="fields"
            rules={[{ required: true, message: "Please input fields!" }]}
          >
            <Select
              onChange={(e) => {
                let newArr = [];
                // eslint-disable-next-line
                e.map((item, idx) => {
                  newArr.push(JSON.parse(item));
                });

                setFieldForm(newArr);
              }}
              mode="multiple"
              optionFilterProp="children"
              // optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {allFields.map((item, idx) => {
                return (
                  <Option value={JSON.stringify(item)} key={idx}>
                    {item.name}
                  </Option>
                );
              })}
            </Select>
          </Form.Item>
          <CustomContent
            style={{ display: `${fieldForm.length > 0 ? "" : "none"}` }}
          >
            <span className="title">Chỉnh sửa các trường</span>
            {/* eslint-disable-next-line*/}
            {fieldForm.map((item, idx) => {
              if (item.hidden === false && item.permission_hidden === false) {
                return handleFieldType(item);
              }
            })}
          </CustomContent>
          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              loading={isLoading}
            >
              Save
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        </Form>

        {openMassEdit ? (
          <ModalConfirm
            // openConfirm={openConfirm}
            // setOpenConfirm={setOpenConfirm}
            title={"Bạn có chắc chắn thay đổi các bản ghi này?"}
            decs={"Sau khi lưu bản ghi, dữ liệu sẽ không thể hoàn tác."}
            method={massEdit}
            data={{
              data: dataMethod,
              methodData,
            }}
            setOpenModal={setOpenMassEdit}
            img={img}
          />
        ) : (
          ""
        )}
      </CustomModal>
    </>
  );
};

export default ModalMassEdit;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;

const CustomContent = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  padding: 0px 24px 0px 24px;
  .title {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    display: flex;
    align-items: center;

    /* Primary/6 */

    color: ${(props) => props.theme.main};
    margin-top: -12px;
    margin-left: 12px;
    background-color: #ffffff;
    width: max-content;
    padding-left: 16px;
    padding-right: 16px;
    margin-bottom: 16px;
  }
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 47px;
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
  }
`;
