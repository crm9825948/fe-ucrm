import React, { useState, useEffect } from "react";
import { Button, Modal, Select, Input, Radio } from "antd";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { BE_URL } from "constants/constants";
import DefaultAvatarGroup from "assets/icons/common/avtGroupDefault.png";
import DefaultAvatarAgent from "assets/icons/common/avtAgentDefault.png";
import DeleteImg from "assets/icons/common/deleteImg.png";
import { loadAllGroups } from "redux/slices/group";

const { Option, OptGroup } = Select;
const { TextArea } = Input;

const ModalManualShare = ({
  open,
  setOpen,
  header,
  recordData,
  data,
  onFinish,
}) => {
  const dispatch = useDispatch();
  const { listAllUser } = useSelector((state) => state.userReducer);
  const { listAllGroups } = useSelector((state) => state.groupReducer);
  const { manualSharing } = useSelector((state) => state.objectsReducer);

  const [listUserActive, setListUserActive] = useState([]);
  const [listShareTo, setListShareTo] = useState([]);
  const [valueSearchShare, setValueSearchShare] = useState("");
  const [valueShareTo, setValueShareTo] = useState([]);
  const [lstShareTo, setLstShareTo] = useState([]);
  const [agent, setAgent] = useState([]); //share agent
  const [group, setGroup] = useState([]);
  const [noti, setNoti] = useState(1);
  const [content, setContent] = useState("");
  const [disabled, setDisabled] = useState(true);
  const [permission, setPermission] = useState("read");

  useEffect(() => {
    if (!open) {
      setValueShareTo([]);
      setNoti(1);
      setPermission("read");
    }
  }, [open]);

  useEffect(() => {
    // dispatch(
    //   loadAllUser({
    //     current_page: 1,
    //     record_per_page: 10000,
    //   })
    // );
    dispatch(
      loadAllGroups({
        CurrentPage: 1,
        RecordPerPage: 1000,
      })
    );
  }, [dispatch]);
  useEffect(() => {
    if (noti !== 1 && !content) {
      setDisabled(true);
    } else {
      setDisabled(false);
    }
  }, [noti, content]);

  useEffect(() => {
    const temp = listAllUser.filter((user) => user.Active);
    setListUserActive(temp);
  }, [listAllUser]);

  useEffect(() => {
    const listShare = [];

    if (listUserActive.length > 0) {
      listShare.push({
        label: "Agent",
        options: listUserActive,
      });
    }
    if (listAllGroups.length > 0) {
      listShare.push({
        label: "Group",
        options: listAllGroups,
      });
    }
    setListShareTo(listShare);

    const filterAgent = listUserActive.filter((user) =>
      valueShareTo.includes(user._id)
    );

    const filterGroup = listAllGroups.filter((group) =>
      valueShareTo.includes(group._id)
    );

    setAgent(filterAgent);
    setGroup(filterGroup);
  }, [valueShareTo, listAllGroups, listUserActive]);

  useEffect(() => {
    const filterShare = listShareTo.map((item) => {
      return {
        label: item.label,
        options: item.options.filter(
          (value) => !valueShareTo.includes(value._id)
        ),
      };
    });
    setLstShareTo(filterShare);
  }, [listShareTo, valueShareTo]);

  // useEffect(() => {
  //   if (recordData.share_to) {
  //     setValueShareTo(recordData.share_to);
  //   }
  // }, [recordData]);

  useEffect(() => {
    if (Object.entries(manualSharing).length > 0) {
      setPermission(manualSharing.permission);
      setValueShareTo(manualSharing.share_to);
    }
  }, [manualSharing]);

  const handleDeleteShareTo = (id) => {
    const deleteShare = valueShareTo.filter((item) => item !== id);

    setValueShareTo(deleteShare);
  };

  const renderListGuest = (value, option) => {
    return (
      <ContentGuest>
        {value.map((guest) => {
          return (
            <div className="guestItem" key={guest._id}>
              <div className="guestInfo">
                {guest.avatar_config?.url !== "" && guest.avatar_config?.url ? (
                  <img src={`${BE_URL}${guest.avatar_config?.url}`} alt="Img" />
                ) : (
                  <img
                    src={
                      option === "agent"
                        ? DefaultAvatarAgent
                        : DefaultAvatarGroup
                    }
                    alt="img"
                  />
                )}
                <div className="infoDetail">
                  <span>
                    {option === "agent" ? guest.Full_Name : guest.name}
                  </span>
                </div>
              </div>

              <div className="closeImg">
                <img
                  onClick={() => {
                    handleDeleteShareTo(guest._id);
                  }}
                  src={DeleteImg}
                  alt="close"
                />
              </div>
            </div>
          );
        })}
      </ContentGuest>
    );
  };

  const handleIDrecord = (list, record) => {
    let id = "";
    if (list.length > 0) {
      list.forEach((item) => {
        if (item.type === "id") {
          id = record[item.ID]?.value;
        }
      });
    }
    return id;
  };

  return (
    <CustomModal
      title={`Share record id: ${handleIDrecord(header, recordData)}`}
      visible={open}
      // visible={true}
      onCancel={() => setOpen(false)}
      footer={[
        <ButtonGroup key="button">
          <div className="btnGroup">
            <Button
              disabled={disabled}
              onClick={() => {
                let temp = {
                  ...data,
                  share_to: valueShareTo,
                  action_type: noti,
                  subject: content,
                  permission: permission,
                };
                onFinish(temp);
                setOpen(false);
                setValueShareTo([]);
                setNoti(1);
                setContent("");
              }}
              key="save"
            >
              Save
            </Button>
            <Button key="cancel" onClick={() => setOpen(false)}>
              Cancel
            </Button>
          </div>
        </ButtonGroup>,
      ]}
    >
      <ContentWrap>
        <Share>
          <p>
            <span>Add people and groups</span>
          </p>

          <div className="guestContent">
            <SelectGuest>
              {!valueSearchShare ? (
                <span>Add people and group</span>
              ) : (
                <span className="addGuest">Add people and group</span>
              )}
              <CustomSelectUser
                onSelect={() => setValueSearchShare("")}
                mode="multiple"
                optionFilterProp="children"
                value={valueShareTo}
                onSearch={(value) => {
                  setValueSearchShare(value);
                }}
                onChange={(value) => {
                  setValueShareTo(value);
                }}
              >
                {lstShareTo.map((item, index) => {
                  return (
                    <OptGroup label={item.label} key={index}>
                      {item.options.map((option) => {
                        return (
                          // option._id !== userDetail._id && (
                          <Option key={option._id} value={option._id}>
                            {item.label === "Agent"
                              ? option.Full_Name
                              : option.name}
                          </Option>
                          // )
                        );
                      })}
                    </OptGroup>
                  );
                })}
              </CustomSelectUser>
            </SelectGuest>
          </div>

          <ListShareTo>
            {renderListGuest(agent, "agent")}
            {renderListGuest(group, "group")}
          </ListShareTo>
        </Share>
        <Noti>
          <p>
            <span>Notify people</span>
          </p>
          <CustomSelect
            showSearch
            optionFilterProp="children"
            value={noti}
            onChange={(value) => setNoti(value)}
          >
            <Option value={1}>Notification</Option>
            <Option value={2}>Email</Option>
            <Option value={3}>Both</Option>
          </CustomSelect>
        </Noti>

        {noti !== 1 && (
          <Message>
            <p>
              <span>Message</span>
            </p>
            <TextArea
              placeholder="Enter message..."
              value={content}
              onChange={(e) => {
                setContent(e.target.value);
              }}
            />
          </Message>
        )}

        <Permission>
          <p>
            <span>Permission</span>
          </p>
          <CustomOption
            value={permission}
            onChange={(e) => {
              setPermission(e.target.value);
            }}
          >
            <Radio value="read">Read</Radio>
            <Radio value="write">Read & Write</Radio>
          </CustomOption>
        </Permission>
      </ContentWrap>
    </CustomModal>
  );
};

export default ModalManualShare;
const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0px 0px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-footer {
    padding: 10px 24px;
  }
`;

const ButtonGroup = styled.div`
  button {
    border-radius: 2px;
    width: 114px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:disabled {
        color: rgba(0, 0, 0, 0.25) !important;
        border-color: #d9d9d9 !important;
        background: #f5f5f5 !important;
      }
      &:first-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;

const ContentWrap = styled.div`
  width: 100%;
`;
const Share = styled.div`
  .guestContent {
    /* padding-right: 16px; */
  }
  width: 100%;
  p {
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: #2c2c2c;
    }
  }
`;

const SelectGuest = styled.div`
  border-radius: 2px;
  padding: 5px 16px;
  cursor: pointer;
  position: relative;
  margin-bottom: 12px;
  border: 1px solid #d9d9d9;
  span {
    color: #6b6b6b;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
  }
  .addGuest {
    color: transparent;
  }
`;

const CustomSelectUser = styled(Select)`
  width: 100%;
  border-radius: 2px;
  position: absolute;
  top: 0;
  left: 0;

  .ant-select-selector {
    cursor: pointer !important;
    border: none !important;
    border-radius: 5px !important;
    background: transparent !important;
    box-shadow: none !important;
  }

  .ant-select-selection-item {
    display: none;
  }

  .ant-select-selection-overflow {
    padding: 0 5px !important;
  }
`;

const ListShareTo = styled.div`
  margin-right: 16px;
  max-height: 192px;
  overflow-y: auto;
`;

const ContentGuest = styled.div`
  /* max-height: 240px;
  overflow: auto; */
  .guestItem {
    width: 100%;
    align-items: center;
    display: flex;
    justify-content: space-between;
    padding: 8px 16px;

    transition: all 1s;
    cursor: context-menu;

    &:hover {
      background: #f5f5f5;
      border-radius: 2px;

      .closeImg {
        opacity: 1;
      }
    }

    .guestInfo {
      display: flex;
      align-items: center;
      img {
        width: 32px;
        height: 32px;
        border-radius: 50%;
        margin-right: 10px;
        object-fit: cover;
      }
      .infoDetail {
        display: flex;
        flex-direction: column;
        span {
          font-family: var(--roboto-400);
          font-size: 16px;
          line-height: 20px;
          color: #000000;
          margin-bottom: 3px;

          &:nth-child(2) {
            font-size: 12px;
            color: #6b6b6b;
            line-height: 12px;

            &::first-letter {
              text-transform: capitalize;
            }
          }
        }
      }
    }

    .closeImg {
      opacity: 0;
      cursor: pointer;

      transition: all 0.5s;
    }
  }
`;

const Noti = styled.div`
  p {
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: #2c2c2c;
    }
  }
`;

const CustomSelect = styled(Select)`
  width: 100%;
`;

const Message = styled.div`
  margin-top: 8px;
  p {
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: #2c2c2c;
    }
  }
`;

const Permission = styled.div`
  margin-top: 8px;
  p {
    margin-bottom: 8px;
    span {
      font-family: var(--roboto-500);
      font-size: 16px;

      color: #2c2c2c;
    }
  }
`;

const CustomOption = styled(Radio.Group)`
  span {
    font-size: 16px;
  }
`;
