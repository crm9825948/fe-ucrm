import { Button, Col, Dropdown, Form, Menu, Modal, Row, Select } from "antd";
import { useLocation } from "react-router-dom";
import _ from "lodash";
import { loadValue } from "redux/slices/picklist";
import Call from "assets/icons/common/call.svg";
import IconDown from "assets/icons/common/icon-down.svg";
import { Notification } from "components/Notification/Noti";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getFieldsMappingCallCenter,
  getObjectsToCall,
  makeCall,
} from "redux/slices/callCenter";
import {
  loadUserDynamicButton,
  loadUserDynamicButtonSuccess,
  runDynamicButtonResult,
} from "redux/slices/dynamicButton";
import {
  getManualSharing,
  loadFormCreate,
  loadRecordDataSuccess,
  loadUserAssignTo,
  setHiddenArray,
  setLinkingFieldValue,
  setShowModal,
  loadTemplate,
} from "redux/slices/objects";
import styled from "styled-components";
import DateComp from "./fieldsType/date";
import Datetime from "./fieldsType/dateTime";
import DynamicField from "./fieldsType/dynamicField";
import Email from "./fieldsType/email";
import File from "./fieldsType/file";
import FormulaField from "./fieldsType/formulaField";
import IDComp from "./fieldsType/ID";
import LinkingObject from "./fieldsType/linkingObject";
import Number from "./fieldsType/number";
import SelectType from "./fieldsType/select";
import Text from "./fieldsType/text";
import Textarea from "./fieldsType/textarea";
import User from "./fieldsType/user";
import Lookup from "./fieldsType/lookup";
import moment from "moment";
import ShareImg from "assets/icons/common/share.png";
import ModalManualShare from "./ModalManualShare";
import { useCallback } from "react";
import { useMemo } from "react";

const { Option } = Select;

const ModalRecord = (props) => {
  // const [form] = Form.useForm();
  const {
    form,
    recordID,
    setRecordID,
    onFinish,
    objectId,
    userDetail,
    setEditingKey,
    setDataConfirm,
    setShowConfirm,
    reload,
    $visibleModalRDB,
  } = props;
  // const { objectId } = useParams();
  const dispatch = useDispatch();
  const { pathname } = useLocation();

  // const [form] = Form.useForm();

  const { showModal } = useSelector((state) => state.objectsReducer);
  const { userDynamicButton, isRunSuccess, isReload } = useSelector(
    (state) => state.dynamicButtonReducer
  );

  const { fieldsMappingCallCenter, objectsToCall } = useSelector(
    (state) => state.callCenterReducer
  );
  const { listValue } = useSelector((state) => state.picklistReducer);
  const [dataFilterLinking, setDataFilterLinking] = useState({});
  const [allFields, setAllFields] = useState({});

  const [numberOfFields, setNumberOfFields] = useState(0);
  const [formValue, setFormValue] = useState({});
  const [valueModal, setValueModal] = useState({});
  const {
    fields,
    isLoading,
    userAssignTo,
    // searchList,
    linkingFieldValue,
    recordData,
    originalFields,
    hiddenArray,
    hiddenDynamic,
    listObjectField,
    header,
    template,
    // isLoadingCreateRecord,
  } = useSelector((state) => state.objectsReducer);

  const [modalManual, setModalManual] = useState(false);
  const [show, setShow] = useState(false);

  const [templateId, setTemplateId] = useState("");
  const [templateData, setTemplateData] = useState({});

  const handleFormatToDay = (type) => {
    var currentdate = new Date();
    if (type === "date") {
      let result = "%Y-%m-%d";
      result = result.replace("%Y", currentdate.getFullYear().toString());
      result = result.replace("%m", (currentdate.getMonth() + 1).toString());
      result = result.replace("%d", currentdate.getDate());
      return result;
    } else if (type === "datetime-local") {
      let result = "%Y-%m-%d %H:%M:%S";
      result = result.replace("%Y", currentdate.getFullYear().toString());
      result = result.replace("%m", (currentdate.getMonth() + 1).toString());
      result = result.replace("%d", currentdate.getDate().toString());

      result = result.replace("%H", currentdate.getHours().toString());
      result = result.replace("%M", currentdate.getMinutes().toString());
      result = result.replace("%S", currentdate.getSeconds().toString());
      return result;
    }
  };

  useEffect(() => {
    if (Object.entries(recordData).length > 0) {
      if (recordData.owner === userDetail._id) {
        setShow(true);
      } else {
        setShow(false);
      }
    }
  }, [recordData, userDetail]);

  useEffect(() => {
    let newObj = {};
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"] &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        /*eslint-disable-next-line*/
        return section.fields.map((item, idx) => {
          newObj[item.ID] = { ...item };
          newObj[item.ID].name = "";
        });
      });

    setAllFields(newObj);
  }, [listObjectField]);

  useEffect(() => {
    if (
      isLoading === false &&
      !pathname.split("/").includes("call-center") &&
      !pathname.split("/").includes("popup")
    ) {
      dispatch(setShowModal(false));
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    if (objectId) {
      dispatch(
        loadFormCreate({
          object_id: objectId,
        })
      );
    }
    dispatch(loadUserAssignTo());

    dispatch(
      loadTemplate({
        object_id: objectId,
      })
    );
  }, [objectId, dispatch]);

  useEffect(() => {
    form.setFieldsValue({
      ...form.getFieldsValue(),
      ...linkingFieldValue,
    });
  }, [linkingFieldValue, form]);

  useEffect(() => {
    let newObject = {};
    /* eslint-disable-next-line */
    Object.entries(linkingFieldValue).forEach(([key, value], index) => {
      newObject[key] = {
        id_field: key,
        id_field_related_record: null,
        id_related_record: null,
        object_related: null,
        value: null,
      };
    });

    let temp = {};

    if (!recordID) {
      setDataFilterLinking({
        ...temp,
        ...newObject,
      });
      // Quân thêm fix bug BUG10953
      dispatch(setLinkingFieldValue(newObject));
    }

    if (!showModal) {
      form.resetFields();
    }

    /* eslint-disable-next-line */
  }, [showModal]);

  useEffect(() => {
    let numberOfFields = 0;
    /* eslint-disable-next-line */
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((field, idx) => {
        if (field.section_name !== "Meta fields") {
          numberOfFields = numberOfFields + field.fields.length;
        }
      });
    setNumberOfFields(numberOfFields);
  }, [listObjectField]);

  useEffect(() => {
    if (showModal) {
      if (recordID) {
      } else {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });
      }
    }
    /*eslint-disable-next-line*/
  }, [userDetail, showModal]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      setDataFilterLinking(linkingFieldValue);
    }
  }, [linkingFieldValue, recordData, recordID]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      form.setFieldsValue({
        assignTo: recordData.owner || form.getFieldValue("assignTo"),
      });
      setFormValue(form.getFieldsValue());
      setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (recordData[field.ID].value) {
              /*eslint-disable-next-line*/
              field.list_items[recordData[field.ID].value].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      dispatch(setHiddenArray(tmpHidden));
    }
    /* eslint-disable-next-line */
  }, [recordData, recordID, fields]);

  useEffect(() => {
    //find default template
    let defaultTemplate = false;
    let templateDataDefault = {};
    if (recordID?.length === 0) {
      template.forEach((item, idx) => {
        if (item.is_default) {
          setTemplateId(item._id);
          setTemplateData({ ...item });
          templateDataDefault = { ...item.record_values };
          defaultTemplate = true;
        } else return null;
      });
    } else if (recordID) {
      setTemplateId("");
    }

    if (recordID === "" && defaultTemplate === false) {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((sections, idx) => {
          /*eslint-disable-next-line*/
          sections.fields.map((field, index) => {
            if (field.type === "dynamic-field" && field.default_value) {
              form.setFieldsValue({
                [field.ID]: field.default_value,
              });
              /*eslint-disable-next-line*/
              field.list_items[field.default_value].map((item, idx) => {
                hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
              });
              dispatch(setHiddenArray(hiddenTmp));
            } else if (field.type === "select" && field.default_value) {
              if (field.target && field.target.length > 0 && !field.source) {
                /* eslint-disable-next-line */
                field.target.map((item, idx) => {
                  dispatch(
                    loadValue({
                      data: {
                        Object_ID: objectId,
                        Source_ID: field.ID,
                        Target_ID: item,
                        Value: field.default_value,
                      },
                      ID: item,
                      listValue: { ...listValue },
                    })
                  );
                });
              }

              form.setFieldsValue({
                [field.ID]: field.default_value,
              });
              console.log("run");
              setFormValue({
                ...form.getFieldsValue(),
                [field.ID]: field.default_value,
              });
            }
          });
        });
      // setFormValue("");
    } else if (defaultTemplate && recordID === "") {
      let hiddenTmp = [...hiddenArray];
      /*eslint-disable-next-line*/
      listObjectField.length > 0 &&
        listObjectField[listObjectField.length - 1]["main_object"][
          "sections"
          /*eslint-disable-next-line*/
        ].map((sections, idx) => {
          /*eslint-disable-next-line*/
          sections.fields.map((field, index) => {
            if (
              field.type === "dynamic-field" &&
              templateDataDefault[field.ID] &&
              templateDataDefault[field.ID].value
            ) {
              form.setFieldsValue({
                [field.ID]: templateDataDefault[field.ID].value,
              });

              field.list_items[templateDataDefault[field.ID].value]?.map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  hiddenTmp = hiddenTmp.filter((ele) => ele !== item);
                }
              );
              dispatch(setHiddenArray(hiddenTmp));
            } else if (
              field.type === "select" &&
              templateDataDefault[field.ID] &&
              templateDataDefault[field.ID].value
            ) {
              form.setFieldsValue({
                [field.ID]: templateDataDefault[field.ID].value,
              });
            }
          });
        });
    }
    if (showModal) {
      setValueModal(form.getFieldsValue());
    }
    /*eslint-disable-next-line*/
  }, [recordID, showModal, listObjectField, template]);

  useEffect(() => {
    let editorField = {};
    if (templateId && Object.keys(templateData).length > 0 && showModal) {
      dispatch(
        loadRecordDataSuccess({
          ...recordData,
          ...templateData.record_values,
          ...form.getFieldsValue(),
        })
      );
      //handle dynamic field
      setFormValue(form.getFieldsValue());
      // setValueModal(form.getFieldsValue());
      let tmpHidden = [...hiddenDynamic];
      /*eslint-disable-next-line*/
      fields.map((sections, idx) => {
        /*eslint-disable-next-line*/
        sections.fields.map((field, idx) => {
          if (field.type === "dynamic-field") {
            if (templateData.record_values[field.ID]?.value) {
              /*eslint-disable-next-line*/
              field.list_items[templateData.record_values[field.ID].value].map(
                /*eslint-disable-next-line*/
                (item, idx) => {
                  tmpHidden = tmpHidden.filter((ele) => ele !== item);
                }
              );
            }
          }
        });
      });
      dispatch(setHiddenArray(tmpHidden));

      let record = {};

      if (recordID) {
        Object.entries({
          ...recordData,
          ...templateData.record_values,
          ...form.getFieldsValue(),
        }).forEach(([key, value]) => {
          if (value !== null && typeof value === "object") {
            record[key] = value.value;
          } else {
            record[key] = value;
          }
        });
      }
      Object.entries(templateData.record_values).forEach(([key, value]) => {
        if (value !== null && typeof value === "object") {
          if (value.value !== null && value.value) record[key] = value.value;
        } else {
          record[key] = value;
        }
      });

      let newObj = {};
      /* eslint-disable-next-line */
      Object.entries(allFields).forEach(([key, value], index) => {
        if (
          value.type === "linkingobject" &&
          templateData.record_values[key]?.id_field_related_record !== null
        ) {
          newObj[key] = {
            ...templateData.record_values[key],
          };
        } else if (
          value.type === "lookup" &&
          templateData.record_values[key]?.id_field_related_record !== null
        ) {
          record[key] = {
            ...templateData.record_values[key],
          };
        }
      });
      let temp = { ...linkingFieldValue, ...newObj };
      dispatch(setLinkingFieldValue(temp));

      form.resetFields();
      let newRecord = { ...record };
      /* eslint-disable-next-line */
      Object.entries(record).forEach(([key, value], index) => {
        if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "datetime-local"
        ) {
          if (record[key]) {
            if (record[key] === "today") {
              // newRecord[key] = "today";
              handleFormatToDay("datetime-local");
              newRecord[key] = record[key]
                ? moment(handleFormatToDay("datetime-local"))
                : null;
            } else {
              newRecord[key] = record[key] ? moment(record[key]) : null;
            }
          } else {
            newRecord[key] = null;
          }
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "date"
        ) {
          if (record[key]) {
            if (record[key] === "today") {
              // newRecord[key] = "today";
              newRecord[key] = record[key]
                ? moment(handleFormatToDay("date"))
                : null;
            } else {
              newRecord[key] = record[key] ? moment(record[key]) : null;
            }
          } else {
            newRecord[key] = null;
          }
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "textarea" &&
          allFields &&
          allFields[key] &&
          allFields[key].is_editor === true
        ) {
          editorField = { ...allFields[key] };
        }
      });
      if (recordID) {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });
        let finalTempObject = { ...newRecord, ...newObj };

        Object.keys(finalTempObject).forEach((key) => {
          if (
            typeof finalTempObject[key] === "object" &&
            finalTempObject[key]?.value === null
          ) {
            delete finalTempObject[key];
          } else if (
            finalTempObject[key] === null ||
            finalTempObject[key] === "<p><br></p>"
          ) {
            delete finalTempObject[key];
          }
        });

        form.setFieldsValue({
          assignTo: userDetail._id,
        });
        let oldValue;
        if (Object.keys(editorField).length > 0) {
          let newObject = {
            ...formValue,
            assignTo: userDetail._id,
            ...finalTempObject,
          };
          oldValue = newObject[editorField.ID];
          Object.entries(newObject).map(([key, value], index) => {
            if (
              (allFields &&
                allFields[key] &&
                allFields[key].type === "datetime") ||
              (allFields &&
                allFields[key] &&
                allFields[key].type === "datetime-local")
            ) {
              oldValue = oldValue?.replace(
                `$${key}`,
                typeof value !== "object" && value
                  ? ""
                  : moment(value).format(
                      allFields &&
                        allFields[key] &&
                        allFields[key].type === "datetime"
                        ? "YYYY-MM-DD"
                        : "YYYY-MM-DD HH:mm:ss"
                    )
              );
            } else {
              oldValue = oldValue?.replace(
                `$${key}`,
                typeof value !== "object" && value ? value : value?.value
              );
            }
            oldValue = oldValue?.replace("undefined", "");
            oldValue = oldValue?.replace("null", "");
            return null;
          });
        }

        if (Object.keys(editorField).length > 0) {
          setFormValue({
            ...formValue,
            assignTo: userDetail._id,
            ...finalTempObject,
            [editorField["ID"]]: oldValue,
          });
          form.setFieldsValue({
            assignTo: userDetail._id,
            ...formValue,
            ...finalTempObject,
            [editorField["ID"]]: oldValue,
          });
        } else {
          setFormValue({
            ...formValue,
            assignTo: userDetail._id,
            ...finalTempObject,
          });
          form.setFieldsValue({
            assignTo: userDetail._id,
            ...formValue,
            ...finalTempObject,
          });
        }
      } else {
        form.setFieldsValue({
          assignTo: userDetail._id,
        });

        setFormValue({ assignTo: userDetail._id, ...newRecord, ...newObj });
        form.setFieldsValue({ ...newRecord, ...newObj });
      }

      // setEditingKey(record.key);
    }
    /*eslint-disable-next-line*/
  }, [
    templateId,
    templateData,
    userDetail,
    recordID,
    dispatch,
    showModal,
    allFields,
    form,
  ]);

  useEffect(() => {
    originalFields &&
      /*eslint-disable-next-line*/
      originalFields.map((section, idx) => {
        /*eslint-disable-next-line*/
        section.fields.map((field, idx) => {
          if (field.hidden === false && field.type === "dynamic-field") {
            form.setFieldsValue({
              [field.ID]: undefined,
            });
          }
        });
      });
    /*eslint-disable-next-line*/
  }, [originalFields]);

  useEffect(() => {
    if (showModal && recordID) {
      dispatch(
        loadUserDynamicButton({
          object_id: objectId,
          record_id: recordID,
        })
      );
    } else {
      dispatch(loadUserDynamicButtonSuccess([]));
    }
  }, [dispatch, objectId, recordID, showModal]);

  useEffect(() => {
    if (isRunSuccess) {
      dispatch(setShowModal(false));
      dispatch(
        runDynamicButtonResult({
          isRunSuccess: false,
          isReload: false,
        })
      );
      setEditingKey("");
      setRecordID("");
      form.resetFields();
      if (isReload && reload) {
        reload();
      }
    }
  }, [
    dispatch,
    form,
    isRunSuccess,
    isReload,
    setEditingKey,
    setRecordID,
    reload,
  ]);

  const _onRunButton = (button) => {
    if (_.get(button, "dynamic_field", []).length > 0) {
      $visibleModalRDB(true);
      setDataConfirm({
        button: button,
        data: {
          data: {
            object_id: objectId,
            record_id: recordID,
            button_id: button._id,
          },
          reloadable: button?.reloadable || false,
        },
      });
    } else {
      setDataConfirm({
        data: {
          object_id: objectId,
          record_id: recordID,
          button_id: button._id,
        },
        reloadable: button?.reloadable || false,
      });
      setShowConfirm(true);
    }
  };

  const _onMakeCall = (value, hotline) => {
    if (localStorage.getItem("inCall") === "true") {
      Notification("error", "On call!");
    } else {
      let data = {};
      if (hotline) {
        data = {
          phone: value,
          hotline: hotline,
        };
      } else {
        data = {
          phone: value,
        };
      }
      dispatch(
        makeCall({
          ...data,
        })
      );
      dispatch(setShowModal(false));
    }
    setEditingKey("");
    setRecordID("");
  };

  useEffect(() => {
    if (userDetail.use_cti) {
      dispatch(
        getFieldsMappingCallCenter({
          object_id: objectId,
        })
      );
      dispatch(
        getObjectsToCall({
          object_id: objectId,
        })
      );
    }
  }, [userDetail, objectId, dispatch]);

  const menuActions = (
    <Menu>
      {userDynamicButton.map((button, idx) => {
        return (
          <Menu.Item key={idx} onClick={() => _onRunButton(button)}>
            <span>{button.name}</span>
          </Menu.Item>
        );
      })}
    </Menu>
  );

  const CustomHeader = (
    <Header>
      <span>Chỉnh sửa bản ghi</span>
      {show && (
        <span
          style={{ display: "none" }}
          onClick={() => {
            dispatch(getManualSharing({ record_id: recordID }));
            dispatch(setShowModal(false));
            setModalManual(true);
          }}
        >
          <img src={ShareImg} alt="Share img" />
        </span>
      )}
    </Header>
  );

  const handleFieldType = (field, open) => {
    switch (field.type) {
      case "id":
        return (
          <IDComp field={field} recordData={recordData} recordID={recordID} />
        );
      case "text":
        return <Text field={field} />;
      case "textarea":
        return (
          <Textarea
            field={field}
            content={
              form.getFieldValue(field.ID)
                ? form.getFieldValue(field.ID)
                : undefined
            }
            form={form}
            listObjectField={listObjectField}
          />
        );
      case "number":
        return <Number field={field} />;
      case "datetime-local":
        return <Datetime field={field} form={form} />;
      case "date":
        return <DateComp field={field} form={form} />;
      case "email":
        return <Email field={field} />;
      case "select":
        return (
          <SelectType
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            objectId={objectId}
          />
        );
      case "user":
        return (
          <User
            field={field}
            form={form}
            open={showModal}
            recordID={recordID}
            setRecordID={setRecordID}
          />
        );
      case "file":
        return (
          <File
            field={field}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            recordID={recordID}
            setRecordID={setRecordID}
            open={showModal}
            objectID={objectId}
          />
        );
      case "dynamic-field":
        return <DynamicField field={field} form={form} recordID={recordID} />;
      case "linkingobject":
        return (
          <LinkingObject
            field={field}
            form={form}
            dataFilterLinking={dataFilterLinking}
            setDataFilterLinking={setDataFilterLinking}
            recordData={recordData}
          />
        );
      case "formula":
        return <FormulaField field={field} form={form} />;
      case "lookup":
        return <Lookup field={field} form={form} recordData={recordData} />;

      default:
        break;
    }
  };

  const handleCancel = () => {
    dispatch(setShowModal(false));
    setRecordID("");
    setEditingKey("");
    form.resetFields();
    dispatch(loadRecordDataSuccess({}));
    setTemplateData({});
    setTemplateId("");
  };

  const onChangeHandle = useCallback((values) => {
    setFormValue(values);
    setValueModal(values);
  }, []);

  const debouncedChangeHandler = useMemo(
    () => _.debounce(onChangeHandle, 500),
    [onChangeHandle]
  );
  if (numberOfFields > 10) {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID
              ? CustomHeader
              : "Thêm bản ghi"
          }
          visible={showModal}
          onCancel={() => {
            handleCancel();
          }}
          width={1200}
          footer={false}
          maskClosable={false}
          mask={true}
        >
          {template.length > 0 ? (
            <div>
              <div style={{ fontSize: "16px" }}>Choose template:</div>
              <div>
                <Select
                  style={{ width: "100%", marginBottom: "10px" }}
                  value={templateId}
                  onChange={(e) => {
                    setTemplateId(e);
                    template.forEach((item, idx) => {
                      if (item._id === e) {
                        setTemplateData({ ...item });
                      }
                    });
                  }}
                >
                  {template.map((item, idx) => {
                    if (item.status)
                      return (
                        <Option value={item._id}>{item.name_template}</Option>
                      );
                    return null;
                  })}
                </Select>
              </div>
            </div>
          ) : (
            ""
          )}
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            onValuesChange={(value, values) => {
              // setFormValue(values);
              // setValueModal(values);
              debouncedChangeHandler(values);
            }}
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.Last_Name} {user.Middle_Name} {user.First_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection key={idx}>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        <Row gutter={[70, 24]}>
                          {/* eslint-disable-next-line */}
                          {section.fields.map((field, index) => {
                            if (
                              field.hidden === false &&
                              field.permission_hidden === false &&
                              hiddenArray.findIndex(
                                (ele) => ele === field._id
                              ) < 0
                            ) {
                              if (
                                (Object.entries(recordData).length === 0 ||
                                  recordID === "") &&
                                field.type === "id"
                              ) {
                              } else {
                                return (
                                  <CustomCol span={12} key={field._id}>
                                    <div style={{ width: "100%" }}>
                                      {handleFieldType(field, showModal)}
                                    </div>
                                    {Object.entries(recordData).length > 0 &&
                                      recordID &&
                                      Object.keys(fieldsMappingCallCenter)
                                        .length > 0 &&
                                      fieldsMappingCallCenter.phone_fields
                                        .length > 0 &&
                                      fieldsMappingCallCenter.phone_fields.includes(
                                        field.field_id
                                      ) && (
                                        <Dropdown
                                          overlay={
                                            <Menu>
                                              {fieldsMappingCallCenter.phone_fields.map(
                                                (item) => {
                                                  return Object.entries(
                                                    recordData
                                                  ).map(([key, val]) => {
                                                    return (
                                                      <>
                                                        {item === key &&
                                                          item ===
                                                            field.field_id &&
                                                          val.value !==
                                                            null && (
                                                            <>
                                                              {fieldsMappingCallCenter
                                                                .custom_hotlines
                                                                .length > 0 ? (
                                                                <>
                                                                  {fieldsMappingCallCenter.custom_hotlines.map(
                                                                    (
                                                                      hotline
                                                                    ) => {
                                                                      return (
                                                                        <Menu.Item
                                                                          onClick={() =>
                                                                            _onMakeCall(
                                                                              val.value,
                                                                              hotline.hotline
                                                                            )
                                                                          }
                                                                          key={
                                                                            item
                                                                          }
                                                                        >
                                                                          {
                                                                            hotline.prefix
                                                                          }
                                                                          {
                                                                            val.value
                                                                          }
                                                                        </Menu.Item>
                                                                      );
                                                                    }
                                                                  )}
                                                                </>
                                                              ) : (
                                                                <Menu.Item
                                                                  onClick={() =>
                                                                    _onMakeCall(
                                                                      val.value
                                                                    )
                                                                  }
                                                                  key={item}
                                                                >
                                                                  {val.value}
                                                                </Menu.Item>
                                                              )}
                                                            </>
                                                          )}
                                                      </>
                                                    );
                                                  });
                                                }
                                              )}
                                            </Menu>
                                          }
                                          placement="bottom"
                                        >
                                          <ButtonCall size="large">
                                            <img src={Call} alt="makeCall" />
                                          </ButtonCall>
                                        </Dropdown>
                                      )}
                                    {Object.entries(recordData).length > 0 &&
                                      recordID &&
                                      Object.keys(objectsToCall)?.length > 0 &&
                                      _.get(objectsToCall, "phone_fields", [])
                                        ?.length > 0 &&
                                      _.get(
                                        objectsToCall,
                                        "phone_fields",
                                        []
                                      )?.includes(field.field_id) && (
                                        <Dropdown
                                          overlay={
                                            <Menu>
                                              {_.get(
                                                objectsToCall,
                                                "phone_fields",
                                                []
                                              )?.map((item) => {
                                                return Object.entries(
                                                  recordData
                                                ).map(([key, val]) => {
                                                  return (
                                                    <>
                                                      {item === key &&
                                                        item ===
                                                          field.field_id &&
                                                        val.value !== null && (
                                                          <>
                                                            {_.get(
                                                              objectsToCall,
                                                              "custom_hotlines",
                                                              []
                                                            ).length > 0 ? (
                                                              <>
                                                                {_.get(
                                                                  objectsToCall,
                                                                  "custom_hotlines",
                                                                  []
                                                                ).map(
                                                                  (hotline) => {
                                                                    return (
                                                                      <Menu.Item
                                                                        onClick={() =>
                                                                          _onMakeCall(
                                                                            val.value,
                                                                            hotline.hotline
                                                                          )
                                                                        }
                                                                        key={
                                                                          item
                                                                        }
                                                                      >
                                                                        {
                                                                          hotline.prefix
                                                                        }
                                                                        {
                                                                          val.value
                                                                        }
                                                                      </Menu.Item>
                                                                    );
                                                                  }
                                                                )}
                                                              </>
                                                            ) : (
                                                              <Menu.Item
                                                                onClick={() =>
                                                                  _onMakeCall(
                                                                    val.value
                                                                  )
                                                                }
                                                                key={item}
                                                              >
                                                                {val.value}
                                                              </Menu.Item>
                                                            )}
                                                          </>
                                                        )}
                                                    </>
                                                  );
                                                });
                                              })}
                                            </Menu>
                                          }
                                          placement="bottom"
                                        >
                                          <ButtonCall size="large">
                                            <img src={Call} alt="makeCall" />
                                          </ButtonCall>
                                        </Dropdown>
                                      )}
                                  </CustomCol>
                                );
                              }
                            }
                          })}
                        </Row>
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                // loading={loadings[2]}
                // onClick={() => enterLoading(2)}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  // dispatch(setShowModal(false));
                  // setRecordID("");
                  // setEditingKey("");
                  // form.resetFields();
                  // dispatch(loadRecordDataSuccess({}));
                  handleCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>
        <ModalManualShare
          recordID={recordID}
          open={modalManual}
          setOpen={setModalManual}
          header={header}
          recordData={recordData}
          data={valueModal}
          onFinish={onFinish}
        />
      </>
    );
  } else {
    return (
      <>
        <CustomModal
          title={
            Object.entries(recordData).length > 0 && recordID
              ? CustomHeader
              : "Thêm bản ghi"
          }
          visible={showModal}
          onCancel={() => {
            handleCancel();
          }}
          width={600}
          footer={false}
          maskClosable={false}
          mask={true}
        >
          {template.length > 0 ? (
            <div>
              <div style={{ fontSize: "16px" }}>Choose template:</div>
              <div>
                <Select
                  style={{ width: "100%", marginBottom: "10px" }}
                  value={templateId}
                  onChange={(e) => {
                    setTemplateId(e);
                    template.forEach((item, idx) => {
                      if (item._id === e) {
                        setTemplateData({ ...item });
                      }
                    });
                  }}
                >
                  {template.map((item, idx) => {
                    if (item.status)
                      return (
                        <Option value={item._id}>{item.name_template}</Option>
                      );
                    return null;
                  })}
                </Select>
              </div>
            </div>
          ) : (
            ""
          )}
          <Form
            name="basic"
            form={form}
            labelCol={{ span: 24 }}
            wrapperCol={{ span: 24 }}
            initialValues={{ remember: true }}
            onFinish={onFinish}
            autoComplete="off"
            onValuesChange={(value, values) => {
              // setFormValue(values);
              // setValueModal(values);
              debouncedChangeHandler(values);
            }}
            labelAlign="left"
          >
            <Form.Item
              label="Assign to"
              name="assignTo"
              rules={[{ required: true, message: "Please select assign to!" }]}
            >
              <Select
                showSearch
                optionFilterProp="children"
                filterOption={(inputValue, option) => {
                  return option.children
                    .join("")
                    .toLowerCase()
                    .includes(inputValue.toLowerCase());
                }}
              >
                {userAssignTo &&
                  userAssignTo.map((user, idx) => {
                    return (
                      <Option
                        value={user._id}
                        key={user._id}
                        disabled={user.disable}
                      >
                        {user.Last_Name} {user.Middle_Name} {user.First_Name}
                      </Option>
                    );
                  })}
              </Select>
            </Form.Item>
            {listObjectField.length > 0 &&
              listObjectField[listObjectField.length - 1]["main_object"] &&
              listObjectField[listObjectField.length - 1]["main_object"][
                "sections"
                /*eslint-disable-next-line*/
              ].map((section, idx) => {
                if (section.section_name !== "Meta fields")
                  return (
                    <WrapperSection key={idx}>
                      <CustomTitleSection>
                        {section.section_name}
                      </CustomTitleSection>
                      <div>
                        {/* eslint-disable-next-line */}
                        {section.fields.map((field, index) => {
                          if (
                            field.hidden === false &&
                            field.permission_hidden === false &&
                            hiddenArray.findIndex((ele) => ele === field._id) <
                              0
                          ) {
                            if (
                              (Object.entries(recordData).length === 0 ||
                                recordID === "") &&
                              field.type === "id"
                            ) {
                            } else
                              return (
                                <div style={{ display: "flex" }}>
                                  <div style={{ width: "100%" }}>
                                    {handleFieldType(field, showModal)}
                                  </div>
                                  {Object.entries(recordData).length > 0 &&
                                    recordID &&
                                    Object.keys(fieldsMappingCallCenter)
                                      .length > 0 &&
                                    fieldsMappingCallCenter.phone_fields
                                      .length > 0 &&
                                    fieldsMappingCallCenter.phone_fields.includes(
                                      field.field_id
                                    ) && (
                                      <Dropdown
                                        overlay={
                                          <Menu>
                                            {fieldsMappingCallCenter.phone_fields.map(
                                              (item) => {
                                                return Object.entries(
                                                  recordData
                                                ).map(([key, val]) => {
                                                  return (
                                                    <>
                                                      {item === key &&
                                                        item ===
                                                          field.field_id &&
                                                        val.value !== null && (
                                                          <>
                                                            {fieldsMappingCallCenter
                                                              .custom_hotlines
                                                              .length > 0 ? (
                                                              <>
                                                                {fieldsMappingCallCenter.custom_hotlines.map(
                                                                  (hotline) => {
                                                                    return (
                                                                      <Menu.Item
                                                                        onClick={() =>
                                                                          _onMakeCall(
                                                                            val.value,
                                                                            hotline.hotline
                                                                          )
                                                                        }
                                                                        key={
                                                                          item
                                                                        }
                                                                      >
                                                                        {
                                                                          hotline.prefix
                                                                        }
                                                                        {
                                                                          val.value
                                                                        }
                                                                      </Menu.Item>
                                                                    );
                                                                  }
                                                                )}
                                                              </>
                                                            ) : (
                                                              <Menu.Item
                                                                onClick={() =>
                                                                  _onMakeCall(
                                                                    val.value
                                                                  )
                                                                }
                                                                key={item}
                                                              >
                                                                {val.value}
                                                              </Menu.Item>
                                                            )}
                                                          </>
                                                        )}
                                                    </>
                                                  );
                                                });
                                              }
                                            )}
                                          </Menu>
                                        }
                                        placement="bottom"
                                      >
                                        <ButtonCall size="large">
                                          <img src={Call} alt="makeCall" />
                                        </ButtonCall>
                                      </Dropdown>
                                    )}
                                  {Object.entries(recordData).length > 0 &&
                                    recordID &&
                                    Object.keys(objectsToCall).length > 0 &&
                                    _.get(objectsToCall, "phone_fields", [])
                                      ?.length > 0 &&
                                    _.get(
                                      objectsToCall,
                                      "phone_fields",
                                      []
                                    )?.includes(field.field_id) && (
                                      <Dropdown
                                        overlay={
                                          <Menu>
                                            {_.get(
                                              objectsToCall,
                                              "phone_fields",
                                              []
                                            )?.map((item) => {
                                              return Object.entries(
                                                recordData
                                              ).map(([key, val]) => {
                                                return (
                                                  <>
                                                    {item === key &&
                                                      item === field.field_id &&
                                                      val.value !== null && (
                                                        <>
                                                          {_.get(
                                                            objectsToCall,
                                                            "custom_hotlines",
                                                            []
                                                          )?.length > 0 ? (
                                                            <>
                                                              {_.get(
                                                                objectsToCall,
                                                                "custom_hotlines",
                                                                []
                                                              )?.map(
                                                                (hotline) => {
                                                                  return (
                                                                    <Menu.Item
                                                                      onClick={() =>
                                                                        _onMakeCall(
                                                                          val.value,
                                                                          hotline.hotline
                                                                        )
                                                                      }
                                                                      key={item}
                                                                    >
                                                                      {
                                                                        hotline.prefix
                                                                      }
                                                                      {
                                                                        val.value
                                                                      }
                                                                    </Menu.Item>
                                                                  );
                                                                }
                                                              )}
                                                            </>
                                                          ) : (
                                                            <Menu.Item
                                                              onClick={() =>
                                                                _onMakeCall(
                                                                  val.value
                                                                )
                                                              }
                                                              key={item}
                                                            >
                                                              {val.value}
                                                            </Menu.Item>
                                                          )}
                                                        </>
                                                      )}
                                                  </>
                                                );
                                              });
                                            })}
                                          </Menu>
                                        }
                                        placement="bottom"
                                      >
                                        <ButtonCall size="large">
                                          <img src={Call} alt="makeCall" />
                                        </ButtonCall>
                                      </Dropdown>
                                    )}
                                </div>
                              );
                          }
                        })}
                      </div>
                    </WrapperSection>
                  );
              })}

            <CustomFooter>
              {userDynamicButton.length > 0 && (
                <Dropdown overlay={menuActions} trigger={["click"]}>
                  <CustomButtonAction size="large">
                    <img src={IconDown} alt="icondown" /> Action
                  </CustomButtonAction>
                </Dropdown>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                // loading={loadings[2]}
                // onClick={() => enterLoading(2)}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                size="large"
                onClick={() => {
                  handleCancel();
                }}
              >
                Cancel
              </CustomButtonCancel>
            </CustomFooter>
          </Form>
        </CustomModal>
        <ModalManualShare
          recordID={recordID}
          open={modalManual}
          setOpen={setModalManual}
          header={header}
          recordData={recordData}
          data={valueModal}
          onFinish={onFinish}
        />
      </>
    );
  }
};

export default ModalRecord;

const WrapperSection = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 5px;
  margin-bottom: 15px;
  padding: 24px;
  position: relative;
  .custom-label {
    font-size: 16px;
    line-height: 33px;
  }
`;

const CustomTitleSection = styled.div`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 20px;
  /* identical to box height, or 143% */

  display: flex;
  align-items: center;
  color: ${(props) => props.theme.main};

  margin-bottom: 24px;
  position: absolute;
  top: -11px;
  left: 24px;
  background-color: #fff;
  padding: 0 10px;
`;

const CustomModal = styled(Modal)`
  .ant-row {
    row-gap: 0 !important;
  }
  .ant-form-item-label {
    text-align: left;
  }
  label {
    font-size: 16px;
    white-space: normal;
    word-break: normal;
    height: fit-content;
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-modal-body {
    max-height: calc(100vh - 200px);
    padding-bottom: 0;
    overflow-y: scroll;
    /* width */
    ::-webkit-scrollbar {
      width: 5px !important;
    }

    /* Track */
    ::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px transparent;
      border-radius: 100px;
      background: transparent !important;
    }

    /* Handle */
    ::-webkit-scrollbar-thumb {
      background: #d8d6d6;
      border-radius: 100px;
    }

    /* Handle on hover */
    ::-webkit-scrollbar-thumb:hover {
      background: #d8d6d6;
    }
    ::-webkit-scrollbar-track-piece {
      margin-bottom: 10px;
      background: transparent !important;
    }
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-bottom: 24px;
  padding-top: 24px;
`;

const CustomButtonAction = styled(Button)`
  width: 102px;
  margin-right: 16px;

  img {
    margin-right: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;

    img {
      filter: brightness(200);
    }
  }
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const ButtonCall = styled(Button)`
  border-radius: 2px;
  padding: 0 24px;
  border-color: ${(props) => props.theme.main};
  height: 32px;
  margin-left: 8px;

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker} !important;

    img {
      filter: brightness(200);
    }
  }

  :active {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }

  :focus {
    border-color: ${(props) => props.theme.main};
    background: #fff;
  }
`;

const CustomCol = styled(Col)`
  display: flex;
`;

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding-right: 30px;
  img {
    cursor: pointer;
  }
`;
