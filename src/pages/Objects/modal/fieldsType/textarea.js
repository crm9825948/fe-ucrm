import React from "react";
import { Form, Input } from "antd";
import Editor from "components/Editor/Editor2";
import styled from "styled-components";
import { useEffect } from "react";
import { useState } from "react";

const { TextArea } = Input;

const Textarea = (props) => {
  const { field, content, form, isLabel, setIsDisabled, listObjectField } =
    props;

  const [appendFields, setAppendFields] = useState([]);

  const onBlur = (value) => {
    setIsDisabled(false);
    if (value === "<p><br></p>") {
      form.setFieldsValue({ [field.ID]: null });
    } else {
      form.setFieldsValue({ [field.ID]: value });
    }
  };

  useEffect(() => {
    let listAppend = [];
    listObjectField.length > 0 &&
      listObjectField[listObjectField.length - 1]["main_object"][
        "sections"
        /*eslint-disable-next-line*/
      ].map((section, idx) => {
        section.fields.map((item, idx) => {
          let newItem = {
            label: item.name,
            value: item.ID,
          };
          listAppend.push(newItem);
          return null;
        });
      });
    setAppendFields(listAppend);
  }, [listObjectField]);

  return (
    <Wrap>
      {field.is_editor ? (
        <>
          <Form.Item style={{ display: "none" }} name={field.ID}></Form.Item>
          {!isLabel && <span className="custom-label">{field.name}</span>}
          <Editor
            content={content}
            readonly={
              field.readonly ||
              (field.permission_read && field.permission_write === false)
            }
            objectID={field.object_id}
            showAppend={window.location.pathname.includes(
              "create-template-record"
            )}
            onBlur={onBlur}
            minHeight={"240px"}
            optionsAppend={
              window.location.pathname.includes("create-template-record")
                ? appendFields
                : []
            }
          />
        </>
      ) : (
        <Form.Item
          label={field.name}
          name={field.ID}
          rules={[
            {
              required: window.location.pathname.includes(
                "create-template-record"
              )
                ? false
                : field.required || field.soft_required,
              message: `Please input ${field.name}!`,
            },
          ]}
        >
          <TextArea
            rows={field.rows}
            showCount
            maxLength={
              field.object_id !== "obj_crm_campaign_task_00001" &&
              field.max_length
            }
            placeholder={field.placeholder}
            disabled={
              field.readonly ||
              (field.permission_read && field.permission_write === false)
            }
            autoComplete={field.auto_fill ? "on" : "off"}
          />
        </Form.Item>
      )}
    </Wrap>
  );
};

Textarea.defaultProps = {
  setIsDisabled: () => {},
};

export default Textarea;
const Wrap = styled.div`
  .jodit-workplace {
    max-height: 250px !important;
  }
`;
