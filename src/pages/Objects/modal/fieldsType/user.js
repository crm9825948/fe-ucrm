import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Form, Input } from "antd";
import Axios from "axios";
import { BASE_URL_API } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadUser } from "redux/slices/objects";
// import { loadUserDetail } from "redux/slices/user";
import UserList from "./userList";
import { checkTokenExpiration } from "contexts/TokenCheck";

const User = (props) => {
  const { field, form, recordID, open } = props;
  const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRows, setSelectedRows] = useState([]);
  const [name, setName] = useState("");
  const [searchData, setSearchData] = useState({});
  const dispatch = useDispatch();
  const { showModal } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadUser({
          current_page: 1,
          record_per_page: 10,
        })
      );
    }
    /* eslint-disable-next-line */
  }, [visible]);

  const reloadData = (current_page, record_per_page, searchData) => {
    dispatch(
      loadUser({
        current_page: current_page,
        record_per_page: record_per_page,
        search_data: searchData,
      })
    );
  };

  const { users, recordData } = useSelector((state) => state.objectsReducer);

  const { userDetail } = useSelector((state) => state.userReducer);

  // useEffect(() => {
  //   if (Object.entries(recordData).length > 0 && recordID) {
  //     dispatch(
  //       loadUserDetail({
  //         _id: userDetail.crm_claims
  //           ? JSON.parse(userDetail.crm_claims).ID
  //           : userDetail._id,
  //       })
  //     );
  //   } else {
  //     setSelectedRowKeys([]);
  //     setName(null);
  //     form.setFieldsValue({
  //       [field.ID]: null,
  //     });
  //   }
  //   /* eslint-disable-next-line */
  // }, [recordData, recordID]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (recordData && recordData[field.ID] && recordData[field.ID].value)
        if (
          Object.entries(recordData).length > 0 &&
          // recordID &&
          recordData &&
          recordData[field.ID] &&
          recordData[field.ID].value !== null &&
          (showModal === true || open === true)
        ) {
          Axios({
            url: BASE_URL_API + "user/get-user-details",
            method: "post",
            headers: {
              Authorization: isTokenValid,
            },
            data: {
              _id:
                recordData &&
                recordData[field.ID] &&
                recordData[field.ID].value,
            },
          })
            .then((res) => {
              let newName = `${res.data.data.Last_Name} ${res.data.data.Middle_Name} ${res.data.data.First_Name} `;
              form.setFieldsValue({
                [field.ID]: res.data.data._id,
              });
              setName(newName);
              let arr = [];
              arr.push(res.data.data._id);
              setSelectedRowKeys(arr);
            })
            .catch((err) => console.log(err));
        } else {
          setName(null);
        }
    };
    checkToken();
    /* eslint-disable-next-line */
  }, [recordData, showModal, recordID, open]);

  const handleSelect = (selectedRowKeys, selectedRows) => {
    form.setFieldsValue({
      [field.ID]: selectedRowKeys[0],
    });
    let newName = ` ${selectedRows[0].Last_Name} ${selectedRows[0].Middle_Name}  ${selectedRows[0].First_Name}`;
    setName(newName);
    setVisible(false);
  };

  useEffect(() => {
    setSelectedRowKeys([]);
    setName(null);
    if (showModal) {
      form.setFieldsValue({
        [field.ID]: null,
      });
    }
    /* eslint-disable-next-line */
  }, [showModal]);

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Input
        maxLength={field.max_length}
        placeholder={field.placeholder}
        value={name}
        onChange={(e) => {}}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        addonBefore={
          name ? (
            <ClearOutlined
              onClick={() => {
                if (
                  field.readonly ||
                  (field.permission_read && field.permission_write === false)
                ) {
                } else {
                  setSelectedRowKeys([]);
                  form.setFieldsValue({
                    [field.ID]: null,
                  });
                  let newName = null;
                  setName(newName);
                }
              }}
            />
          ) : (
            ""
          )
        }
        addonAfter={
          <>
            {!field.permission_read || field.permission_write === true ? (
              <SearchOutlined
                onClick={() => {
                  if (
                    field.readonly ||
                    (field.permission_read && field.permission_write === false)
                  ) {
                  } else setVisible(true);
                }}
                disabled={field.readonly}
              />
            ) : (
              ""
            )}
          </>
        }
      />
      <UserList
        visible={visible}
        setVisible={setVisible}
        users={users}
        selectedRowKeys={selectedRowKeys}
        setSelectedRowKeys={setSelectedRowKeys}
        reloadData={reloadData}
        handleSelect={handleSelect}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        searchData={searchData}
        setSearchData={setSearchData}
        userDetail={userDetail}
      />
    </Form.Item>
  );
};

export default User;
