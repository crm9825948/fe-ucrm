import { Form, InputNumber, Select } from "antd";
import React from "react";

const { Option } = Select;
const NumberType = (props) => {
  const { field } = props;

  return (
    <>
      {field?.options_number?.length > 0 ? (
        <Form.Item
          label={field.name}
          name={field.ID}
          rules={[
            {
              required: window.location.pathname.includes(
                "create-template-record"
              )
                ? false
                : field.required || field.soft_required,
              message: `Please input ${field.name}!`,
            },
            () => ({
              validator(_, value) {
                if (value && isNaN(value)) {
                  return Promise.reject(`${field.name} has to be a number.`);
                }

                return Promise.resolve();
              },
            }),
          ]}
        >
          <Select>
            {field?.options_number?.map((item, idx) => {
              return <Option value={item}>{item}</Option>;
            })}
          </Select>
        </Form.Item>
      ) : (
        <Form.Item
          label={field.name}
          name={field.ID}
          rules={[
            {
              required: window.location.pathname.includes(
                "create-template-record"
              )
                ? false
                : field.required || field.soft_required,
              message: `Please input ${field.name}!`,
            },
            () => ({
              validator(_, value) {
                if (value && isNaN(value)) {
                  return Promise.reject(`${field.name} has to be a number.`);
                }

                return Promise.resolve();
              },
            }),
          ]}
        >
          <InputNumber
            step={field.step}
            placeholder={field.placeholder}
            style={{ width: "100%" }}
            disabled={
              field.readonly ||
              (field.permission_read && field.permission_write === false)
            }
            autoComplete={field.auto_fill ? "on" : "off"}
            // formatter={(value) => {
            //   if (value) {
            //     return `${parseFloat(value)
            //       .toFixed(
            //         field?.decimal_separator === 0
            //           ? 0
            //           : field?.decimal_separator || 3
            //       )
            //       .toString()}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            //   }
            // }}
            // parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
            formatter={(value) =>
              `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
            }
            parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
          />
        </Form.Item>
      )}
    </>
  );
};

export default NumberType;
