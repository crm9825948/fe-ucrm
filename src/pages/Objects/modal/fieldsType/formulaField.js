import React from "react";
import { Form, Input, InputNumber } from "antd";

const FormulaField = (props) => {
  const { field, form } = props;
  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      {typeof form?.getFieldValue(field.ID) === "number" ? (
        <InputNumber
          disabled={true}
          placeholder={field.placeholder}
          formatter={(value) =>
            `${parseFloat(value)
              .toFixed(field.decimal_separator || 3)
              .toString()}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          }
          parser={(value) => value.replace(/\$\s?|(,*)/g, "")}
          style={{ width: "100%" }}
        />
      ) : (
        <Input
          maxLength={field.max_length}
          placeholder={field.placeholder}
          disabled={true}
          autoComplete={field.auto_fill ? "on" : "off"}
        />
      )}
    </Form.Item>
  );
};

export default FormulaField;
