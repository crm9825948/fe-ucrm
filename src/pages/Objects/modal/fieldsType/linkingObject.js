import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Form, Input } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  loadDataLinking,
  loadHeaderLinking,
  loadLinkingFieldValue,
  // loadPaginationLinking,
  setLinkingFieldValue,
  viewObjectLinkingSearch,
} from "redux/slices/objects";
import LinkingList from "./linking/linkingList";
import Axios from "axios";
import { BASE_URL_API } from "constants/constants";
import { checkTokenExpiration } from "contexts/TokenCheck";

const Linking = (props) => {
  const {
    field,
    form,
    setDataFilterLinking,
    dataFilterLinking,
    recordData,
    setIsDisabled,
    selectedRowKeys: selectedRowKeysOutside,
  } = props;

  const [visible, setVisible] = useState(false);
  const isMongoId = (str) => str.length === 24 && /^[A-F0-9]+$/i.test(str);

  const dispatch = useDispatch();
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [name, setName] = useState(undefined);
  const { linkingFieldValue, isLoadLinking } = useSelector(
    (state) => state.objectsReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  // const { hiddenArray } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    setSelectedRowKeys([]);
  }, []);

  useEffect(() => {
    if (recordData) {
      let arr = [];
      let value =
        recordData[field.ID] && recordData[field.ID].id_related_record;
      arr.push(value);
      setSelectedRowKeys(arr);
      setRowData(recordData[field.ID]);
    }
    /* eslint-disable-next-line */
  }, [recordData]);

  useEffect(() => {
    if (visible === true) {
      let dataFilter = [];
      /* eslint-disable-next-line */
      Object.entries(dataFilterLinking).forEach(([key, value], index) => {
        if (value && value.id_field_related_record !== null) {
          if (Object.keys(form.getFieldsValue()).includes(key)) {
            dataFilter.push(value);
          }
        }
      });
      dispatch(
        loadHeaderLinking({
          object_id: field.objectname,
        })
      );
      console.log(dataFilter);
      dispatch(
        loadDataLinking({
          object_id: field.objectname,
          first_record_id: null,
          last_record_id: null,
          search_with: {
            meta: [],
            data: [],
          },
          filter: field.independent_linking ? [] : dataFilter,
        })
      );
      dispatch(viewObjectLinkingSearch({ object_id: field.objectname }));
      // dispatch(
      //   loadPaginationLinking({
      //     object_id: field.objectname,
      //     current_page: 1,
      //     record_per_page: 10,
      //     search_with: {
      //       meta: [],
      //       data: [],
      //     },
      //     filter: field.independent_linking ? [] : dataFilter,
      //   })
      // );
    }
    /* eslint-disable-next-line */
  }, [dispatch, visible, dataFilterLinking]);

  const [rowData, setRowData] = useState({});

  const handleSelect = (selectedRowKeys) => {
    let dataFilter = [];
    /* eslint-disable-next-line */
    Object.entries(dataFilterLinking).forEach(([key, value], index) => {
      if (value && value.id_field_related_record !== null && field.ID !== key) {
        dataFilter.push(value);
      }
    });
    dispatch(
      loadLinkingFieldValue({
        object_related: field.objectname,
        record_id: selectedRowKeys[0],
        object_create_record: field.object_id,
        selected_data: dataFilter,
      })
    );
    // setVisible(false);
    setIsDisabled(false);
  };

  useEffect(() => {
    if (isLoadLinking === false) {
      setVisible(false);
    }
  }, [isLoadLinking]);

  useEffect(() => {
    // let temp = { ...dataFilterLinking };
    let newObject = {};
    Object.entries(linkingFieldValue).map(([key, value], idx) => {
      if (Object.keys(value).length > 0) {
        newObject[key] = {
          ...value,
          id_field: key,
        };
      }

      return null;
    });
    setDataFilterLinking({
      // ...temp,
      ...newObject,
    });
    // form.setFieldsValue(linkingFieldValue);
    /* eslint-disable-next-line */
  }, [linkingFieldValue]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (
        typeof (
          linkingFieldValue &&
          linkingFieldValue[field.ID] &&
          linkingFieldValue[field.ID].value
        ) === "undefined"
      ) {
        // form.setFieldsValue({
        //   [field.ID]: null,
        // });
      }
      if (
        linkingFieldValue &&
        linkingFieldValue[field.ID] &&
        linkingFieldValue[field.ID].value === null
      ) {
        setSelectedRowKeys([]);
      }
      if (
        typeof (
          linkingFieldValue &&
          linkingFieldValue[field.ID] &&
          linkingFieldValue[field.ID].value
        ) === "string" &&
        isMongoId(
          linkingFieldValue &&
            linkingFieldValue[field.ID] &&
            linkingFieldValue[field.ID].value
        )
      ) {
        if (
          linkingFieldValue &&
          linkingFieldValue[field.ID] &&
          linkingFieldValue[field.ID].value
        )
          Axios({
            url: BASE_URL_API + "/user/get-user-details",
            method: "post",
            headers: {
              Authorization: isTokenValid,
            },
            data: {
              _id:
                linkingFieldValue &&
                linkingFieldValue[field.ID] &&
                linkingFieldValue[field.ID].value,
            },
          })
            .then((res) => {
              let newName = `${res.data.data.Last_Name} ${res.data.data.Middle_Name} ${res.data.data.First_Name} `;
              setName(newName);
            })
            .catch((err) => console.log(err));
      }
    };
    checkToken();
    /* eslint-disable-next-line */
  }, [linkingFieldValue]);

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      {field.key === true ? (
        <Input
          value={
            name ||
            (linkingFieldValue &&
              linkingFieldValue[field.ID] &&
              linkingFieldValue[field.ID].value) ||
            (form.getFieldValue(field.ID) &&
              form.getFieldValue(field.ID).value) ||
            null
          }
          maxLength={field.max_length}
          placeholder={field.placeholder}
          disabled={
            !field.key ||
            (field.permission_read && field.permission_write === false)
          }
          addonBefore={
            // (linkingFieldValue &&
            //   linkingFieldValue[field.ID] &&
            //   linkingFieldValue[field.ID].value) ||
            // (form.getFieldValue(field.ID) &&
            //   form.getFieldValue(field.ID).value) ? (
            //   <ClearOutlined
            //     onClick={() => {
            //       let newObject = {};
            //       /* eslint-disable-next-line */
            //       Object.entries(linkingFieldValue).forEach(
            //         // eslint-disable-next-line
            //         ([key, value], index) => {
            //           newObject[key] = {
            //             id_field_related_record: null,
            //             id_related_record: null,
            //             object_related: null,
            //             value: null,
            //           };
            //         }
            //       );

            //       let temp = { ...dataFilterLinking };
            //       setDataFilterLinking({
            //         ...temp,
            //         ...newObject,
            //       });

            //       dispatch(setLinkingFieldValue(newObject));
            //       form.setFieldsValue(newObject);
            //     }}
            //   />
            // ) : (
            //   ""
            // )
            (linkingFieldValue &&
              linkingFieldValue[field.ID] &&
              linkingFieldValue[field.ID].value) ||
            (form.getFieldValue(field.ID) &&
              form.getFieldValue(field.ID).value) ? (
              <ClearOutlined
                onClick={async () => {
                  let newObject = {};
                  newObject[field.ID] = {
                    id_field_related_record: null,
                    id_related_record: null,
                    object_related: null,
                    value: null,
                  };
                  const isTokenValid = await checkTokenExpiration();
                  Axios.post(
                    BASE_URL_API + "object-related/all-related-targets",
                    {
                      source_object_id: field.objectname,
                    },
                    {
                      headers: {
                        Authorization: isTokenValid,
                      },
                    }
                  ).then((res) => {
                    let targetObject = [...res.data.data, field.objectname];
                    listObjectField[
                      listObjectField.length - 1
                    ]?.main_object?.sections?.map((fieldItem, idx) => {
                      return fieldItem.fields.map((item, idx) => {
                        if (item.type === "linkingobject") {
                          if (
                            targetObject.findIndex(
                              (ele) =>
                                ele === item.objectname &&
                                item.independent_linking === false
                            ) > -1
                          ) {
                            newObject[item.ID] = {
                              id_field_related_record: null,
                              id_related_record: null,
                              object_related: null,
                              value: null,
                            };
                          }
                        }
                        return null;
                      });
                    });
                    let temp = { ...dataFilterLinking };
                    setDataFilterLinking({
                      ...temp,
                      ...newObject,
                    });
                    dispatch(
                      setLinkingFieldValue({
                        ...linkingFieldValue,
                        ...newObject,
                      })
                    );
                    form.setFieldsValue({
                      ...form.getFieldsValue(),
                      ...newObject,
                    });
                  });
                  setIsDisabled(false);
                }}
              />
            ) : (
              ""
            )
          }
          addonAfter={
            <>
              {!field.permission_read || field.permission_write === true ? (
                <SearchOutlined
                  onClick={() => {
                    setVisible(true);
                  }}
                  disabled={field.readonly}
                />
              ) : (
                ""
              )}
            </>
          }
        />
      ) : (
        <Input
          value={
            (linkingFieldValue &&
              linkingFieldValue[field.ID] &&
              linkingFieldValue[field.ID].value) ||
            null
          }
          maxLength={field.max_length}
          placeholder={field.placeholder}
          disabled={!field.key}
        />
      )}
      {visible === true ? (
        <LinkingList
          setVisible={setVisible}
          visible={visible}
          field={field}
          selectedRowKeys={selectedRowKeys}
          setSelectedRowKeys={setSelectedRowKeys}
          handleSelect={handleSelect}
          dataFilterLinking={dataFilterLinking}
          recordData={recordData}
          selectedRowKeysOutside={selectedRowKeysOutside}
          form={form}
          rowData={rowData}
          setRowData={setRowData}
        />
      ) : (
        ""
      )}
    </Form.Item>
  );
};

Linking.defaultProps = {
  setIsDisabled: () => {},
};

export default Linking;
