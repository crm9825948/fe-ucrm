import { Form, Select } from "antd";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { setHiddenArray } from "redux/slices/objects";
import { Notification } from "components/Notification/Noti";

const { Option } = Select;

const DynamicField = (props) => {
  const { field } = props;
  const { hiddenArray } = useSelector((state) => state.objectsReducer);

  const dispatch = useDispatch();

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Select
        allowClear
        placeholder={field.placeholder}
        onClear={() => {
          // dispatch(setFields(rootFields));
          let hiddenArrayTmp = [...hiddenArray];
          /*eslint-disable-next-line*/
          Object.entries(field.list_items).forEach(([key, value], index) => {
            hiddenArrayTmp = [...hiddenArrayTmp, ...value];
          });
          hiddenArrayTmp = [...new Set(hiddenArrayTmp)];
          dispatch(setHiddenArray(hiddenArrayTmp));
        }}
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
        onChange={(e) => {
          //get hidden array
          if (e) {
            let hiddenArrayTmp = [...hiddenArray];
            /*eslint-disable-next-line*/
            Object.entries(field.list_items).forEach(([key, value], index) => {
              hiddenArrayTmp = [...hiddenArrayTmp, ...value];
            });
            hiddenArrayTmp = [...new Set(hiddenArrayTmp)];
            /*eslint-disable-next-line*/
            field.list_items[e].map((item, idx) => {
              hiddenArrayTmp = hiddenArrayTmp.filter((ele) => ele !== item);
            });
            dispatch(setHiddenArray(hiddenArrayTmp));
          }
        }}
        optionFilterProp="children"
        filterOption={(inputValue, option) => {
          if (option.children) {
            return option.children[option.children.length - 1]
              .toLowerCase()
              .indexOf(inputValue.toLowerCase()) >= 0
              ? true
              : false;
          } else if (option.label) {
            return option.label
              .toLowerCase()
              .indexOf(inputValue.toLowerCase()) >= 0
              ? true
              : false;
          }
        }}
      >
        {field.option.map((item, idx) => {
          return (
            <Option value={item.value} key={idx}>
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
                onClick={() => {
                  navigator.clipboard.writeText(item.label);
                  Notification("success", "Copied!");
                }}
                style={{
                  zIndex: 999999999999,
                  width: "16px",
                  marginRight: "10px",
                  marginBottom: "-10px",
                }}
              >
                <path
                  d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                  stroke="#2C2C2C"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                  stroke="#2C2C2C"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>{" "}
              {item.label}
            </Option>
          );
        })}
      </Select>
    </Form.Item>
  );
};

export default DynamicField;
