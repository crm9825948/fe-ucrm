import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Form, Input } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import {
  loadDataLinking,
  loadHeaderLinking,
  loadPaginationLinking,
  viewObjectLinkingSearch,
} from "redux/slices/objects";
import DrawerLookup from "./drawerLookup";
import { useSelector } from "react-redux";
const Lookup = (props) => {
  const {
    field,
    // recordData,
    setIsDisabled,
    selectedRowKeys: selectedRowKeysOutside,
    form,
  } = props;
  const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRows, setSelectedRows] = useState({});
  const [value, setValue] = useState("");
  const dispatch = useDispatch();
  const {
    recordData,
    // isLoadingCreateRecord,
  } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    if (Object.keys(recordData).length > 0) {
      form.setFieldsValue({
        [field.ID]: recordData[field.ID],
      });
    }
    /* eslint-disable-next-line */
  }, [recordData]);

  useEffect(() => {
    if (visible === true) {
      let dataFilter = [];
      /* eslint-disable-next-line */
      dispatch(
        loadHeaderLinking({
          object_id: field.objectname,
        })
      );
      dispatch(
        loadDataLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: dataFilter,
        })
      );
      dispatch(
        loadPaginationLinking({
          object_id: field.objectname,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: [],
          },
          filter: dataFilter,
        })
      );
      dispatch(viewObjectLinkingSearch({ object_id: field.objectname }));
    }
    /* eslint-disable-next-line */
  }, [dispatch, visible]);

  useEffect(() => {
    if (recordData) {
      let arr = [];
      let value =
        recordData[field.ID] && recordData[field.ID].id_related_record;
      arr.push(value);
      setValue((recordData[field.ID] && recordData[field.ID].value) || "");
      setSelectedRowKeys(arr);
      setRowData(recordData[field.ID]);
      form.setFieldsValue({
        [field.ID]: recordData[field.ID],
      });
    }
    /* eslint-disable-next-line */
  }, [recordData]);

  const [rowData, setRowData] = useState({});

  const handleSelect = () => {
    if (Object.keys(selectedRows).length > 0) {
      setValue(selectedRows[0][field.field]);
      form.setFieldsValue({
        [field.ID]: {
          id_field_related_record: field.field,
          id_related_record: selectedRowKeys[0],
          object_related: field.objectname,
          value: selectedRows[0][field.field],
        },
      });
    }
    setVisible(false);
    setIsDisabled(false);
  };

  const handleReset = () => {
    setValue(null);
    form.setFieldsValue({
      [field.ID]: null,
    });
    setSelectedRowKeys([]);
    setSelectedRows([]);
  };

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: window.location.pathname.includes("create-template-record")
            ? false
            : field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <Input
        placeholder={field.placeholder}
        addonBefore={
          value || form.getFieldValue(field.ID)?.value ? (
            <ClearOutlined
              onClick={() => {
                setIsDisabled(false);
                if (!field.readonly) handleReset();
              }}
            />
          ) : (
            ""
          )
        }
        value={value || form.getFieldValue(field.ID)?.value}
        addonAfter={
          <SearchOutlined
            onClick={() => {
              if (!field.readonly) setVisible(true);
            }}
          />
        }
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
      />
      <DrawerLookup
        setVisible={setVisible}
        visible={visible}
        field={field}
        selectedRowKeys={selectedRowKeys}
        setSelectedRowKeys={setSelectedRowKeys}
        handleSelect={handleSelect}
        dataFilterLinking={[]}
        recordData={recordData}
        selectedRowKeysOutside={selectedRowKeysOutside}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        rowData={rowData}
        setRowData={setRowData}
      />
    </Form.Item>
  );
};
Lookup.defaultProps = {
  setIsDisabled: () => {},
};
export default Lookup;
