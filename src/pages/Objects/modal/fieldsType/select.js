import { Form, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
// import { useParams } from "react-router";
import { loadValue } from "redux/slices/picklist";
import { loadTarget } from "redux/slices/objects";
import { Notification } from "components/Notification/Noti";

const { Option } = Select;

const SelectType = (props) => {
  const { field, form, formValue, setFormValue, recordID, objectId } = props;
  const dispatch = useDispatch();
  // const { objectId } = useParams();
  const { listValue } = useSelector((state) => state.picklistReducer);
  const { recordData, target } = useSelector((state) => state.objectsReducer);
  const [test, setTest] = useState({});
  // console.log(
  //   field.ID === "fld_trangthai_71299753" ? form.getFieldValue(field.ID) : ""
  // );
  useEffect(() => {
    if (field.source && formValue[field.source] === null) {
      let newObj = { ...formValue };
      newObj[field.ID] = null;
      console.log(formValue);
      form.setFieldsValue(newObj);
    }
    /* eslint-disable-next-line */
  }, [formValue]);

  useEffect(() => {
    if (field.multiple && formValue[field.ID] === null) {
      let newObj = { ...formValue };
      newObj[field.ID] = [];
      form.setFieldsValue(newObj);
    }
    /* eslint-disable-next-line */
  }, [formValue, field]);

  useEffect(() => {
    let value = form.getFieldsValue();
    /* eslint-disable-next-line */
    target.map((item, idx) => {
      form.setFieldsValue({ value, [item]: null });
    });
    // setFormValue(form.getFieldsValue());
    /* eslint-disable-next-line */
  }, [target]);

  useEffect(() => {
    if (Object.entries(recordData).length > 0 && recordID) {
      setFormValue(form.getFieldsValue());
      if (field.target && field.target.length > 0) {
        /* eslint-disable-next-line */
        field.target.map((item, idx) => {
          dispatch(
            loadValue({
              data: {
                Object_ID: objectId,
                Source_ID: field.ID,
                Target_ID: item,
                Value: recordData[field.ID]?.value,
              },
              ID: item,
              listValue: { ...listValue },
            })
          );
        });
      }
      //  else if (field.source && field.target && field.target.length > 0) {
      //   /* eslint-disable-next-line */
      //   field.target.map((item, idx) => {
      //     dispatch(
      //       loadValue({
      //         data: {
      //           Object_ID: objectId,
      //           Source_ID: field.ID,
      //           Target_ID: item,
      //           Value: form.getFieldValue(field.ID),
      //         },
      //         ID: item,
      //         listValue: { ...listValue },
      //       })
      //     );
      //   });
      // }
      else if (field.source && field.target && field.target.length === 0) {
      }
    }
    /* eslint-disable-next-line */
  }, [recordData, recordID]);

  useEffect(() => {
    let newObj = { ...test, ...listValue };
    setTest(newObj);
    /* eslint-disable-next-line */
  }, [listValue]);

  if (!field.source && !field.target) {
    //Select thường
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: window.location.pathname.includes(
              "create-template-record"
            )
              ? false
              : field.required || field.soft_required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          placeholder={field.placeholder}
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
          mode={`${field.multiple === true ? "multiple" : ""}`}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children[option.children.length - 1]
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          style={{ width: "100%" }}
        >
          {field.option.map((item, idx) => {
            return (
              <Option value={item.value} key={idx} title={item.label}>
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  onClick={() => {
                    navigator.clipboard.writeText(item.label);
                    Notification("success", "Copied!");
                  }}
                  style={{
                    zIndex: 999999999999,
                    width: "16px",
                    marginRight: "10px",
                    marginBottom: "-5px",
                  }}
                >
                  <path
                    d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>

                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    );
  } else if (
    (field.source === null && field.target && field.target.length > 0) ||
    (field.source === undefined && field.target && field.target.length > 0) ||
    (field.source === "" && field.target && field.target.length > 0)
  ) {
    //Level 1
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: window.location.pathname.includes(
              "create-template-record"
            )
              ? false
              : field.required || field.soft_required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          showSearch
          placeholder={field.placeholder}
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children[option.children.length - 1]
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          onChange={(e) => {
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              dispatch(
                loadValue({
                  data: {
                    Object_ID: objectId,
                    Source_ID: field.ID,
                    Target_ID: item,
                    Value: e,
                  },
                  ID: item,
                  listValue: listValue,
                })
              );
            });

            let tempFormValue = { ...form.getFieldsValue() };
            tempFormValue[field.ID] = e;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
          onClear={() => {
            let tempFormValue = { ...formValue };
            tempFormValue[field.ID] = null;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });

            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
        >
          {field.option.map((item, idx) => {
            return (
              <Option value={item.value} key={idx} title={item.label}>
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  onClick={() => {
                    navigator.clipboard.writeText(item.label);
                    Notification("success", "Copied!");
                  }}
                  style={{
                    zIndex: 999999999999,
                    width: "16px",
                    marginRight: "10px",
                    marginBottom: "-5px",
                  }}
                >
                  <path
                    d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>{" "}
                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    );
  } else if (field.source && field.target && field.target.length > 0) {
    //Level 2
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: window.location.pathname.includes(
              "create-template-record"
            )
              ? false
              : field.required || field.soft_required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          showSearch
          placeholder={field.placeholder}
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children[option.children.length - 1]
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          onClear={() => {
            let tempFormValue = { ...formValue };
            tempFormValue[field.ID] = null;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);
            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
          disabled={
            formValue[field.source] ||
            (field.permission_read && field.permission_write === false)
              ? false
              : true
          }
          onChange={(e) => {
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              dispatch(
                loadValue({
                  data: {
                    Object_ID: objectId,
                    Source_ID: field.ID,
                    Target_ID: item,
                    Value: e,
                  },
                  ID: item,
                  listValue: listValue,
                })
              );
            });

            let tempFormValue = { ...form.getFieldsValue() };
            tempFormValue[field.ID] = e;
            /* eslint-disable-next-line */
            field.target.map((item, idx) => {
              tempFormValue[item] = null;
            });
            setFormValue(tempFormValue);

            form.setFieldsValue(tempFormValue);

            dispatch(
              loadTarget({
                field_id: field.ID,
              })
            );
          }}
        >
          {test && test[field.ID]
            ? Object.entries(test && test[field.ID]).map(
                ([key, value], idx) => {
                  return (
                    <Option value={key} key={idx} title={key}>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        onClick={() => {
                          navigator.clipboard.writeText(key);
                          Notification("success", "Copied!");
                        }}
                        style={{
                          zIndex: 999999999999,
                          width: "16px",
                          marginRight: "10px",
                          marginBottom: "-5px",
                        }}
                      >
                        <path
                          d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                          stroke="#2C2C2C"
                          stroke-width="1.5"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                          stroke="#2C2C2C"
                          stroke-width="1.5"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>{" "}
                      {key}
                    </Option>
                  );
                }
              )
            : field.option.map((item, idx) => {
                return (
                  <Option value={item.value} key={idx} title={item.label}>
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                      onClick={() => {
                        navigator.clipboard.writeText(item.label);
                        Notification("success", "Copied!");
                      }}
                      style={{
                        zIndex: 999999999999,
                        width: "16px",
                        marginRight: "10px",
                        marginBottom: "-5px",
                      }}
                    >
                      <path
                        d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                        stroke="#2C2C2C"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                        stroke="#2C2C2C"
                        stroke-width="1.5"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>{" "}
                    {item.label}
                  </Option>
                );
              })}
        </Select>
      </Form.Item>
    );
  } else if (
    (field.source && field?.target?.length === 0) ||
    (field.source && field?.target === "")
  ) {
    //Level 3
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: window.location.pathname.includes(
              "create-template-record"
            )
              ? false
              : field.required || field.soft_required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          disabled={
            (form.getFieldValue(field.source) || formValue[field.source]) &&
            field.permission_write
              ? false
              : true
          }
          showSearch
          placeholder={field.placeholder}
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children[option.children.length - 1]
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          allowClear
        >
          {test && test[field.ID] && Object.keys(test[field.ID]).length > 0
            ? Object.entries(test && test[field.ID]).map(
                ([key, value], idx) => {
                  return (
                    <Option value={key} key={idx} title={key}>
                      <svg
                        width="24"
                        height="24"
                        viewBox="0 0 24 24"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                        onClick={() => {
                          navigator.clipboard.writeText(key);
                          Notification("success", "Copied!");
                        }}
                        style={{
                          zIndex: 999999999999,
                          width: "16px",
                          marginRight: "10px",
                          marginBottom: "-5px",
                        }}
                      >
                        <path
                          d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                          stroke="#2C2C2C"
                          stroke-width="1.5"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                        <path
                          d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                          stroke="#2C2C2C"
                          stroke-width="1.5"
                          stroke-linecap="round"
                          stroke-linejoin="round"
                        />
                      </svg>{" "}
                      {key}
                    </Option>
                  );
                }
              )
            : ""}
        </Select>
      </Form.Item>
    );
  } else {
    return (
      <Form.Item
        label={field.name}
        name={field.ID}
        rules={[
          {
            required: window.location.pathname.includes(
              "create-template-record"
            )
              ? false
              : field.required || field.soft_required,
            message: `Please input ${field.name}!`,
          },
        ]}
      >
        <Select
          allowClear
          disabled={
            field.readonly ||
            (field.permission_read && field.permission_write === false)
          }
          showSearch
          optionFilterProp="children"
          placeholder={field.placeholder}
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children[option.children.length - 1]
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          mode={`${field.multiple === true ? "multiple" : ""}`}
        >
          {field.option.map((item, idx) => {
            return (
              <Option value={item.value} key={idx} title={item.label}>
                <svg
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  onClick={() => {
                    navigator.clipboard.writeText(item.label);
                    Notification("success", "Copied!");
                  }}
                  style={{
                    zIndex: 999999999999,
                    width: "16px",
                    marginRight: "10px",
                    marginBottom: "-5px",
                  }}
                >
                  <path
                    d="M21.2875 21H9.65C9.46103 21 9.27981 20.9249 9.14619 20.7913C9.01257 20.6577 8.9375 20.4765 8.9375 20.2875V8.65C8.9375 8.46103 9.01257 8.27981 9.14619 8.14619C9.27981 8.01257 9.46103 7.9375 9.65 7.9375H21.2875C21.4765 7.9375 21.6577 8.01257 21.7913 8.14619C21.9249 8.27981 22 8.46103 22 8.65V20.2875C22 20.4765 21.9249 20.6577 21.7913 20.7913C21.6577 20.9249 21.4765 21 21.2875 21Z"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                  <path
                    d="M16.0625 7.9375V2.7125C16.0625 2.52353 15.9874 2.34231 15.8538 2.20869C15.7202 2.07507 15.539 2 15.35 2H3.7125C3.52353 2 3.34231 2.07507 3.20869 2.20869C3.07507 2.34231 3 2.52353 3 2.7125V14.35C3 14.539 3.07507 14.7202 3.20869 14.8538C3.34231 14.9874 3.52353 15.0625 3.7125 15.0625H8.9375"
                    stroke="#2C2C2C"
                    stroke-width="1.5"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                  />
                </svg>{" "}
                {item.label}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    );
  }
};

export default SelectType;
