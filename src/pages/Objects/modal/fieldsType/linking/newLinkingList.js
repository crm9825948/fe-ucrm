import {
  CaretDownOutlined,
  CaretUpOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import {
  Button,
  Drawer,
  Input,
  Pagination,
  Select,
  Space,
  Table,
  Typography,
} from "antd";
import React, { useEffect, useState } from "react";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import { loadDataLinking, loadPaginationLinking } from "redux/slices/objects";
import styled from "styled-components";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import ReloadPagi from "assets/icons/objects/reload.png";
import { Spin } from "antd";

const { Text: TextComponent } = Typography;
const { Option } = Select;

const LinkingList = (props) => {
  const {
    visible,
    setVisible,
    field,
    selectedRowKeys,
    setSelectedRowKeys,
    handleSelect,
    dataFilterLinking,
    onCancelModal,
    selectedRowKeysOutside,
    // recordData,
  } = props;
  /* eslint-disable-next-line */
  const [recordPerPage, setRecordPerPage] = useState(50);
  const [currentPage, setCurrentPage] = useState(1);
  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);

  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);

  const [searchList, setSearchList] = useState({});
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearcherColumn] = useState("");
  const dispatch = useDispatch();
  const [dateSource, setDataSource] = useState([]);

  const [column, setColumn] = useState([]);
  const {
    headerLinking,
    dataLinking,
    totalRecordLinking,
    recordData,
    isLoadingPagi,
  } = useSelector((state) => state.objectsReducer);

  function numberWithCommas(x) {
    if (x === 0) {
      return "0";
    } else return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  useEffect(() => {
    let newData = [];
    /* eslint-disable-next-line */
    dataLinking.map((item, index) => {
      let newItem = {};
      newItem["key"] = item._id;
      /* eslint-disable-next-line */
      Object.entries(item).forEach(([key, value], idx) => {
        if (typeof value === "object" && value !== null) {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      // if (
      //   recordData &&
      //   recordData._id !== newItem["key"]
      //   // selectedRowKeysOutside.indexOf(newItem["key"]) < 0
      // ) {
      newData.push(newItem);
      // }
    });
    setDataSource(newData);
  }, [dataLinking, recordData]);

  const [sortBy, setSortBy] = useState({});
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const handleSort = (sortTmp) => {
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    reloadData(searchData, firstID, lastID, sortTmp);
  };

  useEffect(() => {
    let newColumn = [];
    /* eslint-disable-next-line */
    headerLinking.map((item, idx) => {
      let newItem = {
        title: (
          <div style={{ display: "flex", alignItems: "center" }}>
            <span style={{ marginRight: "8px" }}> {item.name}</span>

            <span style={{ display: "flex", flexDirection: "column" }}>
              {/* <CaretUpOutlined
                style={{
                  color: `${
                    sortBy[item.ID] === 1
                      ? defaultBrandName.theme_color
                      : "#D9D9D9"
                  }`,
                  cursor: "pointer",
                }}
                onClick={() => {
                  let sortTmp = { ...sortBy };
                  if (sortTmp[item.ID] === 1) {
                    delete sortTmp[item.ID];
                  } else {
                    sortTmp[item.ID] = 1;
                  }
                  handleSort(sortTmp);
                  setSortBy(sortTmp);
                }}
              />
              <CaretDownOutlined
                style={{
                  color: `${
                    sortBy[item.ID] === -1
                      ? defaultBrandName.theme_color
                      : "#D9D9D9"
                  }`,
                  cursor: "pointer",
                }}
                onClick={() => {
                  let sortTmp = { ...sortBy };
                  if (sortTmp[item.ID] === -1) {
                    delete sortTmp[item.ID];
                  } else {
                    sortTmp[item.ID] = -1;
                  }
                  handleSort(sortTmp);
                  setSortBy(sortTmp);
                }}
              /> */}
            </span>
          </div>
        ),
        dataIndex: item.ID,
        key: item.ID,
        width: "maxContent",
        editable: true,
        type: item.type,
        ...getColumnSearchProps(item.ID, idx, item.name, searchList, item),
      };
      newColumn.push(newItem);
    });
    // newColumn.push({
    //   title: "Action",
    //   key: "action",
    //   fixed: "right",
    //   render: () => (
    //     <Space size="middle">
    //       <a href>
    //         <img alt="" src={editImg} style={{ width: "20px" }} />
    //       </a>
    //       <a className="ant-dropdown-link" href>
    //         <img alt="" src={deleteImg} style={{ width: "20px" }} />
    //       </a>
    //     </Space>
    //   ),
    // });
    setColumn(newColumn);
    /* eslint-disable-next-line */
  }, [headerLinking, searchList, sortBy]);

  const onClose = () => {
    setVisible(false);
    setSelectedRowKeys([]);
  };

  const getColumnSearchProps = (dataIndex, idx, name, searchList, item) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        {item.type === "select" || item.type === "dynamic-field" ? (
          <Select
            placeholder={`Search ${name}`}
            value={selectedKeys[0]}
            onChange={(e) => {
              setSelectedKeys(e ? [e] : []);
            }}
            onPressEnter={() => {
              handleSearch(selectedKeys, confirm, dataIndex);
            }}
            style={{ marginBottom: 8, display: "block" }}
          >
            {item &&
              item.option &&
              item.option.map((ele, idx) => {
                return <Option value={ele.label}>{ele.label}</Option>;
              })}
          </Select>
        ) : (
          <Input
            placeholder={`Search ${name}`}
            value={selectedKeys[0]}
            onChange={(e) => {
              setSelectedKeys(e.target.value ? [e.target.value] : []);
            }}
            onPressEnter={() => {
              handleSearch(selectedKeys, confirm, dataIndex);
            }}
            style={{ marginBottom: 8, display: "block" }}
          />
        )}

        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => {
              handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </CustomButtonCancel>
          <CustomButtonCancel
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });

              setSearchText(selectedKeys[0]);
              setSearcherColumn(dataIndex);
            }}
          >
            Filter
          </CustomButtonCancel>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text, ...props) => {
      /* eslint-disable-next-line */
      const expression =
        /* eslint-disable-next-line */
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
      const regex = new RegExp(expression);
      if (text && typeof text === "object") {
        return text.map((ele, idx) => {
          if (ele !== null && ele.match(regex)) {
            let fileNew = ele.split("/");
            return (
              <a style={{ display: "block" }} href>
                {fileNew[fileNew.length - 1]}
              </a>
            );
          } else {
            return searchedColumn === dataIndex ? (
              <Highlighter
                highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text ? text.toString() : ""}
              />
            ) : (
              <TextComponent
                ellipsis={{ tooltip: text }}
                style={{
                  width: "100%",
                  textAlign: `${typeof text === "number" ? "right" : ""}`,
                }}
              >
                {typeof text === "number" ? numberWithCommas(text) : text}
                {/* </div> */}
              </TextComponent>
            );
          }
        });
      } else {
        return searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ""}
          />
        ) : (
          <TextComponent
            ellipsis={{ tooltip: text }}
            style={{
              width: "100%",
              textAlign: `${typeof text === "number" ? "right" : ""}`,
            }}
          >
            {typeof text === "number" ? numberWithCommas(text) : text}
            {/* </div> */}
          </TextComponent>
        );
      }
    },
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    let searchTemp = { ...searchList };
    searchTemp[dataIndex] = selectedKeys[0];
    setSearchList(searchTemp);
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchTemp).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    reloadData(searchData, firstID, lastID, sortBy);
    setSearchText(selectedKeys[0]);
    setSearcherColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const reloadData = (searchData, firstID, lastID, sortBy) => {
    let dataFilter = [];
    /* eslint-disable-next-line */
    Object.entries(dataFilterLinking).forEach(([key, value], index) => {
      if (value && value.id_field_related_record !== null) {
        dataFilter.push(value);
      }
    });
    let meta = [];
    let data = [];
    // eslint-disable-next-line
    searchData.map((item, idx) => {
      if (
        item.id_field === "created_date" ||
        item.id_field === "created_by" ||
        item.id_field === "modify_time" ||
        item.id_field === "owner" ||
        item.id_field === "modify_by"
      ) {
        meta.push(item);
      } else {
        data.push(item);
      }
    });
    dispatch(
      loadDataLinking({
        object_id: field.objectname,
        first_record_id: firstID,
        last_record_id: lastID,
        search_with: {
          meta: meta,
          data: data,
        },
        // sort_by: sortBy,
        filter: field.independent_linking ? [] : dataFilter,
      })
    );
    dispatch(
      loadPaginationLinking({
        object_id: field.objectname,
        first_record_id: firstID,
        last_record_id: lastID,
        search_with: {
          meta: meta,
          data: data,
        },
        filter: field.independent_linking ? [] : dataFilter,
      })
    );
  };

  const onSelectChange = (selectedRowKeys, selectedRows) => {
    setSelectedRowKeys(selectedRowKeys);
  };

  useEffect(() => {
    setNext(
      dateSource.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + dateSource.length
    );
  }, [dateSource]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
  }, [currentPage]);

  return (
    <>
      <Drawer
        title="Linking list"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={1000}
        maskClosable={false}
      >
        <Wrapper>
          <Table
            // bordered
            dataSource={dateSource}
            columns={column}
            rowClassName="editable-row"
            pagination={false}
            rowSelection={{
              selectedRowKeys,
              onChange: onSelectChange,
              getCheckboxProps: (record) => ({
                disabled: selectedRowKeysOutside
                  ? selectedRowKeysOutside.indexOf(record["key"]) < 0
                    ? false
                    : true
                  : recordData && recordData._id === record["key"]
                  ? true
                  : false,
              }),
              type: "radio",
            }}
            scroll={{
              x: "max-content",
            }}
          />
          <div>
            {/* <CustomPagination
              showQuickJumper
              current={currentPage}
              total={totalRecordLinking}
              showSizeChanger
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              
              onChange={(e, pageSize) => {
                setCurrentPage(e);
                
                let searchData = [];
              
                Object.entries(searchList).forEach(([key, value], index) => {
                  if (value) {
                    let newItem = {
                      id_field: key,
                      value: value,
                    };
                    searchData.push(newItem);
                  }
                });
                reloadData(searchData, e, pageSize);
              }}
            /> */}
            <NewCustomPagination>
              <>
                <div className="total-record">
                  {prev} - {next}
                </div>
              </>

              <div
                className="left-pagi"
                style={{
                  pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                  cursor: `${currentPage === 1 ? "not-allowed" : "pointer"}`,
                  opacity: `${currentPage === 1 ? 0.5 : 1}`,
                }}
                onClick={() => {
                  let searchData = [];

                  Object.entries(searchList).forEach(([key, value], index) => {
                    if (value) {
                      let newItem = {
                        id_field: key,
                        value: value,
                      };
                      searchData.push(newItem);
                    }
                  });

                  setLastID(
                    dateSource.length > 0
                      ? dateSource[dateSource.length - 1]._id
                      : null
                  );
                  let currentPageTemp = currentPage;
                  currentPageTemp = currentPageTemp - 1;
                  setCurrentPage(currentPageTemp);
                  // setPrev(currentPageTemp * recordPerPage - recordPerPage + 1);
                  // setNext(
                  //   dataSource.length === recordPerPage
                  //     ? currentPageTemp * recordPerPage
                  //     : (currentPageTemp - 1) * recordPerPage + dataSource.length
                  // );
                  setFirstID(dateSource.length > 0 ? dateSource[0]._id : null);
                  reloadData(
                    searchData,
                    dateSource.length > 0 ? dateSource[0]._id : null,
                    null,
                    sortBy
                  );
                }}
              >
                <img alt="" src={LeftPagi} />
              </div>
              <div
                className="right-pagi"
                style={{
                  pointerEvents: `${
                    dateSource.length < recordPerPage ? "none" : ""
                  }`,
                  cursor: `${
                    dateSource.length < recordPerPage
                      ? "not-allowed"
                      : "pointer"
                  }`,
                  opacity: `${dateSource.length < recordPerPage ? 0.5 : 1}`,
                }}
                onClick={() => {
                  let searchData = [];

                  Object.entries(searchList).forEach(([key, value], index) => {
                    if (value) {
                      let newItem = {
                        id_field: key,
                        value: value,
                      };
                      searchData.push(newItem);
                    }
                  });

                  setLastID(
                    dateSource.length > 0
                      ? dateSource[dateSource.length - 1]._id
                      : null
                  );
                  let currentPageTemp = currentPage;
                  currentPageTemp = currentPageTemp + 1;
                  setCurrentPage(currentPageTemp);
                  // setPrev(currentPageTemp * recordPerPage - recordPerPage + 1);
                  // setNext(
                  //   dataSource.length === recordPerPage
                  //     ? currentPageTemp * recordPerPage
                  //     : (currentPageTemp - 1) * recordPerPage + dataSource.length
                  // );
                  setFirstID(dateSource.length > 0 ? dateSource[0]._id : null);
                  reloadData(
                    searchData,
                    null,
                    dateSource.length > 0
                      ? dateSource[dateSource.length - 1]._id
                      : null,
                    sortBy
                  );
                }}
              >
                <img alt="" src={RightPagi} />
              </div>
            </NewCustomPagination>
            <WrapperSave>
              <CustomButtonSave
                onClick={() => {
                  handleSelect(selectedRowKeys);
                }}
                disabled={selectedRowKeys[0] ? false : true}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel
                onClick={() => {
                  setVisible(false);

                  if (onCancelModal) {
                    onCancelModal();
                  }
                }}
              >
                Cancel
              </CustomButtonCancel>
            </WrapperSave>
          </div>
        </Wrapper>
      </Drawer>
    </>
  );
};

export default LinkingList;

const WrapperSave = styled.div`
  position: fixed;
  bottom: -8px;
  right: 826px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
`;

const CustomPagination = styled(Pagination)`
  position: fixed;
  bottom: -8px;
  right: 16px;
  width: 976px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
  .ant-pagination-item-active {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
  .ant-pagination-item:hover {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
`;

const Wrapper = styled.div`
  table {
    width: max-content;
    th {
      width: max-content;
      .ant-table-column-title {
        margin-right: 30px;
      }
    }
    td {
      max-width: 200px;
    }
  }
  padding-bottom: 100px;
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  border-color: ${(props) => props.theme.main};
  margin-right: 16px;
  /* img {
      width: 15px;
      margin-right: 8px;
    } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
      transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;

  /* img {
      width: 15px;
      margin-right: 8px;
    } */
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
      transition: 0.5s; */
  }
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  padding: 10px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;
