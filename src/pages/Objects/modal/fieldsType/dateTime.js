import { DatePicker, Form } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
// import { loadConfig } from "redux/slices/datetimeSetting";

const Datetime = (props) => {
  const { field } = props;

  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);
  const [format, setFormat] = useState("");
  // const dispatch = useDispatch();

  const optionsDate = [
    {
      label: "YYYY-MM-DD",
      value: "%Y-%m-%d",
    },
    {
      label: "YYYY/MM/DD",
      value: "%Y/%m/%d",
    },
    {
      label: "YYYY MM DD",
      value: "%Y %m %d",
    },
    {
      label: "MM-DD-YYYY",
      value: "%m-%d-%Y",
    },
    {
      label: "MM/DD/YYYY",
      value: "%m/%d/%Y",
    },
    {
      label: "MM DD YYYY",
      value: "%m %d %Y",
    },

    {
      label: "DD-MM-YYYY",
      value: "%d-%m-%Y",
    },
    {
      label: "DD/MM/YYYY",
      value: "%d/%m/%Y",
    },
    {
      label: "DD MM YYYY",
      value: "%d %m %Y",
    },
  ];

  const optionsTime = [
    {
      label: "HH:mm:ss",
      value: "%H:%M:%S",
    },
    {
      label: "HH:mm",
      value: "%H:%M",
    },
  ];

  // useEffect(() => {
  //   dispatch(loadConfig());
  // }, [dispatch]);

  useEffect(() => {
    if (Object.keys(allConfig).length > 0) {
      let date = allConfig.tenant_datetime_format.split(" ")[0];
      let time = allConfig.tenant_datetime_format.split(" ")[1];
      let finalDate = optionsDate.find((ele) => ele.value === date).label;
      let finalTime = optionsTime.find((ele) => ele.value === time).label;
      setFormat(finalDate + " " + finalTime);
    }

    // eslint-disable-next-line
  }, [allConfig]);

  return (
    <Form.Item
      label={field.name}
      name={field.ID}
      rules={[
        {
          required: field.required || field.soft_required,
          message: `Please input ${field.name}!`,
        },
      ]}
    >
      <DatePicker
        format={format}
        placeholder={field.placeholder}
        style={{ width: "100%" }}
        showTime
        disabled={
          field.readonly ||
          (field.permission_read && field.permission_write === false)
        }
      />
    </Form.Item>
  );
};

export default Datetime;
