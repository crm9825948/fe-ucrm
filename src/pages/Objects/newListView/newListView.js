// import {
//   DeleteOutlined,
//   DownOutlined,
//   EditOutlined,
//   EllipsisOutlined,
//   MergeCellsOutlined,
//   CaretDownOutlined,
// } from "@ant-design/icons";
// import {
//   Button,
//   Checkbox,
//   Dropdown,
//   Form,
//   Input,
//   Menu,
//   Select,
//   Tooltip,
//   Typography,
//   Breadcrumb,
//   InputNumber,
// } from "antd";
// import ImgConfirm from "assets/icons/common/confirm.png";
// import deleteImg from "assets/icons/common/delete-icon.png";
// import editImg from "assets/icons/common/icon-edit.png";
// import KanbanViewImg from "assets/icons/common/kanbanView.png";
// import ListViewImg from "assets/icons/common/listView.png";
// import ListViewDetailImg from "assets/icons/common/listViewDetail.png";
// import KnowledgeImg from "assets/icons/common/knowledge.png";

// import trashImg from "assets/icons/common/trash.png";
// import pinImg from "assets/icons/objects/png.svg";
// import unpinImg from "assets/icons/objects/unpin.svg";
// import plusIcon from "assets/icons/objectsManagement/plus.svg";
// import Call from "assets/icons/listview/Call_ListView.svg";
// import CRMLogo from "assets/icons/common/crmlogo.png";
// import IconTop from "assets/icons/common/iconTop.png";
// import IconBottom from "assets/icons/common/iconBottom.png";
// import Details from "assets/icons/users/details.svg";
// import ImportExcel from "assets/icons/common/importExcel.png";
// import ExportExcel from "assets/icons/common/downExcel.png";
// import {
//   default as ModalConfimDelete,
//   default as ModalDelete,
// } from "components/Modal/ModalConfirmDelete";
// import ModalConfirm from "components/Modal/ModalConfirmStateIn";
// import { BASENAME, BASE_URL_API, BE_URL } from "constants/constants";
// import moment from "moment";
// import React, {
//   useCallback,
//   useEffect,
//   useMemo,
//   useRef,
//   useState,
// } from "react";
// import { useDispatch, useSelector } from "react-redux";
// import { useNavigate, useParams } from "react-router";
// import { runDynamicButton } from "redux/slices/dynamicButton";
// import { setShowModalConfirmDelete } from "redux/slices/global";
// import { loadAllSetting } from "redux/slices/highlightSetting";
// import {
//   createRecord,
//   deleteRecord,
//   exportTemplate,
//   loadDataWithOutPagi,
//   loadFavouriteObjects,
//   // loadFormCreate,
//   loadListObjectField,
//   loadRecordDataSuccess,
//   massDelete,
//   setLinkingFieldValue,
//   setShowModal,
//   toggleFavouriteObject,
//   updateRecord,
//   loadPagination,
//   loadPaginationSuccess,
//   loadCustomView,
//   setLoadingModal,
// } from "redux/slices/objects";
// import { getFieldsMappingCallCenter, makeCall } from "redux/slices/callCenter";
// import { deleteCampaign } from "redux/slices/campaign";

// import styled from "styled-components";
// import CustomView from "../customView/customView";
// import ModalImport from "../modal/modalImport";
// import ModalMassEdit from "../modal/modalMassEdit";
// import ModalMerge from "../modal/modalMerge";
// import ModalRecord from "../modal/modalRecord";
// import { loadConfig } from "redux/slices/datetimeSetting";
// import {
//   getKnowledgeBaseSetting,
//   updateIsFirst,
// } from "redux/slices/knowledgeBaseSetting";
// import { Notification } from "components/Notification/Noti";
// import Carousel from "react-multi-carousel";
// import ModalDuplicate from "components/Modal/ModalDuplicated";
// import axios from "axios";
// import Highlighter from "react-highlight-words";
// import ModalAddCampaign from "pages/Campaign/ModalAddCampaign";
// import LeftPagi from "assets/icons/objects/pagi-left.png";
// import RightPagi from "assets/icons/objects/pagi-right.png";
// import Reload from "assets/icons/objects/reload.png";
// import { Spin } from "antd";
// import { useTranslation } from "react-i18next";
// import { changeTitlePage } from "redux/slices/authenticated";
// import NewTable from "./newTable";
// // import MenuPopup from "./MenuPopup";
// import _ from "lodash";

// const NewListView = () => {
//   const { objectId, customViewId } = useParams();
//   const dispatch = useDispatch();
//   const [firstID, setFirstID] = useState(null);
//   const [lastID, setLastID] = useState(null);

//   const [next, setNext] = useState(1);
//   const [prev, setPrev] = useState(1);

//   const {
//     data,
//     listObjectField,
//     recordData,
//     // fields,
//     header,
//     customView,
//     objectsFavourite,
//     isLoadingDataWithOutPagi,
//     totalRecord,
//     isLoadingPagi,
//     isLoadingModal,
//     // linkingFieldValue,
//   } = useSelector((state) => state.objectsReducer);

//   const loadData = useCallback(() => {
//     dispatch(loadFavouriteObjects());
//     if (customViewId === "default-view") {
//       dispatch(
//         loadDataWithOutPagi({
//           object_id: {
//             object_id: objectId,
//           },
//           data: {
//             object_id: objectId,
//             // current_page: 1,
//             // record_per_page: 10,
//             first_record_id: firstID,
//             last_record_id: lastID,
//             search_with: {
//               meta: [],
//               data: [],
//             },
//           },
//         })
//       );
//     } else {
//       dispatch(
//         loadDataWithOutPagi({
//           object_id: {
//             object_id: objectId,
//             id: customViewId,
//           },
//           data: {
//             id: customViewId,
//             object_id: objectId,
//             first_record_id: firstID,
//             last_record_id: lastID,
//             search_with: {
//               meta: [],
//               data: [],
//             },
//           },
//         })
//       );
//     }

//     dispatch(
//       loadListObjectField({
//         api_version: "2",
//         object_id: objectId,
//         show_meta_fields: true,
//       })
//     );
//   }, [objectId, customViewId, firstID, lastID, dispatch]);

//   useEffect(() => {
//     loadData();
//   }, [loadData]);

//   useEffect(() => {
//     console.log(data);
//   }, [data]);

//   return (
//     <Wrapper>
//       <NewTable header={header} data={data} />
//     </Wrapper>
//   );
// };

// export default NewListView;

// const Wrapper = styled.div``;
