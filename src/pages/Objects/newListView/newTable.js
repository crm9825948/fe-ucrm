import { Input, Typography } from "antd";
import { default as React, useState } from "react";
import styled from "styled-components";
const NewTable = ({ header, data }) => {
  const { Text: TextComponent } = Typography;
  const [listSearch, setListSearch] = useState({});
  //   const listSearch = useRef({});

  return (
    <TableWrap>
      <table className="table-header">
        <thead>
          <tr>
            {header.map((item, idx) => {
              return <th key={idx}>{item.name}</th>;
            })}
          </tr>
          <tr>
            {header.map((item, idx) => {
              return (
                <th key={idx}>
                  <Input
                    placeholder={item.name}
                    // value={listSearch[item.ID] || ""}
                    onChange={(e) => {
                      //   let tmpSearch = { ...listSearch };
                      // tmpSearch[item.ID] = e.target.value;
                      listSearch[item.ID] = e.target.value;
                      //   setListSearch({ ...tmpSearch });
                    }}
                    // onPressEnter={() => {
                    //   console.log(listSearch);
                    // }}
                  />
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody className="table-body">
          {data.map((item, idx) => {
            return (
              <tr key={idx}>
                {header.map((itemHeader, idx) => {
                  return (
                    <td>
                      <TextComponent
                        ellipsis={{ tooltip: item[itemHeader.ID]?.value }}
                      >
                        {item[itemHeader.ID]?.value}
                      </TextComponent>
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </TableWrap>
  );
};

export default NewTable;

const TableWrap = styled.div`
  width: 100%;
  /* height: calc(100% - 187px); */
  height: 100%;

  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  background: #fff;
  overflow-x: auto;
  overflow-y: auto;
  &::-webkit-scrollbar {
    height: 12px !important;
    width: 12px !important;
  }
  &::-webkit-scrollbar-thumb {
    background: #d9d9d9;
  }

  /* padding: 10px; */
  /* padding-right: 0; */
  table {
    /* width: 100%; */
    min-width: 100%;
    width: max-content;
    table-layout: auto;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      text-align: left;

      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;

      overflow: hidden;
      resize: horizontal;
      background: #f0f0f0;
      min-width: 54px;
      /* width: 60px; */
      white-space: nowrap;
      /* min-width: fit-content; */

      &:first-child {
        border-left: none;
        text-align: center;
        width: 54px;
        max-width: 54px;
        resize: none;
        position: sticky;
        z-index: 4;
        left: 0;
        box-shadow: inset -1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:nth-child(2) {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:last-child {
        position: sticky;
        z-index: 4;
        right: 0;
        border-right: none;
        resize: none;
        width: 122px;
        max-width: 122px;

        .hidden__component {
          display: none;
        }
      }
    }

    .table-body td {
      background: #fff;
      border-bottom: 1px solid #ddd;
      padding: 6px 16px;

      max-width: 200px;

      &:first-child {
        /* border-left: 1px solid #ddd; */
        text-align: center;
        position: sticky;
        z-index: 2;
        left: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }

      &:last-child {
        /* border-right: 1px solid #ddd; */
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }
    }

    tr {
      :hover {
        background: #f5f5f5;
      }
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-checkbox-indeterminate .ant-checkbox-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;
