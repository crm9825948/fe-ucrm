import { BASENAME } from "constants/constants";
import React from "react";
import styled from "styled-components";

const Popup = (props) => {
  const { popup, objectId } = props;
  const { visible, x, y, record } = popup;
  if (visible)
    return (
      <CustomOl
        className="popup"
        style={{
          position: "absolute",
          left: `${x - 100}px`,
          top: `${y - 80}px`,
        }}
      >
        <li>
          <a
            rel="noreferrer"
            target="_blank"
            // href={window.location.origin + `/ucrm-sso/consolidated-view/${objectId}/${record.key}`}
            href={BASENAME + `consolidated-view/${objectId}/${record.key}`}
          >
            Consolidated view
          </a>
        </li>
      </CustomOl>
    );
  else {
    return <></>;
  }
};

export default Popup;

const CustomOl = styled.ol`
  animation-name: fadeIn;
  animation-duration: 0.4s;
  background-clip: padding-box;
  background-color: #fff;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
  left: 0px;
  list-style-type: none;
  margin: 0;
  outline: none;
  padding: 0;
  position: absolute;
  text-align: left;
  top: 0px;
  overflow: hidden;
  -webkit-box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);

  li {
    clear: both;
    color: rgba(0, 0, 0, 0.65);
    cursor: pointer;
    font-size: 14px;
    font-family: var(--roboto-400);
    line-height: 22px;
    margin: 0;
    padding: 5px 12px;
    transition: all 0.3s;
    white-space: nowrap;
    -webkit-transition: all 0.3s;
  }

  li:hover {
    background-color: #e6f7ff;
  }

  li > i {
    margin-right: 8px;
  }
`;
