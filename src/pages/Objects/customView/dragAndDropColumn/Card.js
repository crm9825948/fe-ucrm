import { Tag } from "antd";
import { useRef } from "react";
import { useDrag, useDrop } from "react-dnd";
import { ItemTypes } from "./ItemTypes";

const style = {
  // padding: "0.5rem 1rem",
  marginBottom: ".5rem",
  backgroundColor: "white",
  //   lineHeight: 3,
  cursor: "move",
  marginRight: "10px",
  display: "inline-block",
  background: "#F5F5F5",
  border: "1px solid #F0F0F0",
  fontSize: "14px",
  padding: "10px",
  wordBreak: "break-all",
  whiteSpace: "normal",
};
export const Card = ({ id, text, index, moveCard, cards, setCards, form }) => {
  const ref = useRef(null);
  const [{ handlerId }, drop] = useDrop({
    accept: ItemTypes.CARD,
    collect(monitor) {
      return {
        handlerId: monitor.getHandlerId(),
      };
    },
    hover(item, monitor) {
      if (!ref.current) {
        return;
      }
      const dragIndex = item.index;
      const hoverIndex = index;
      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }
      // Determine rectangle on screen
      const hoverBoundingRect = ref.current?.getBoundingClientRect();
      // Get vertical middle
      const hoverMiddleY =
        (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
      // Determine mouse position
      const clientOffset = monitor.getClientOffset();
      // Get pixels to the top
      const hoverClientY = clientOffset.y - hoverBoundingRect.top;
      // Only perform the move when the mouse has crossed half of the items height
      // When dragging downwards, only move when the cursor is below 50%
      // When dragging upwards, only move when the cursor is above 50%
      // Dragging downwards
      if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
        return;
      }
      // Dragging upwards
      if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
        return;
      }
      // Time to actually perform the action
      moveCard(dragIndex, hoverIndex);
      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      item.index = hoverIndex;
    },
  });
  const [{ isDragging }, drag] = useDrag({
    type: ItemTypes.CARD,
    item: () => {
      return { id, index };
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  });
  const opacity = isDragging ? 0 : 1;
  drag(drop(ref));
  return (
    <Tag
      ref={ref}
      style={{ ...style, opacity }}
      data-handler-id={handlerId}
      closable
      onClose={(e) => {
        e.preventDefault();
        let cardsTmp = [...cards];

        cardsTmp.splice(index, 1);
        setCards(cardsTmp);

        let arrID = [];
        /*eslint-disable-next-line*/
        cardsTmp.forEach((item, idx) => {
          arrID.push(item.ID);
        });
        form.setFieldsValue({ column: arrID });
      }}
    >
      <div
        style={{
          background: "#FFD8BF",
          float: "left",
          width: "25px",
          height: "25px",
          display: "flex",
          justifyContent: "center",
          alignContent: "center",
          borderRadius: "50%",
          marginRight: "10px",
          lineHeight: 1.8,
        }}
      >
        {index}
      </div>

      {text}
    </Tag>
  );
};
