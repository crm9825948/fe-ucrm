import { Button, Col, Drawer, Row, Typography } from "antd";
import deleteIcon from "assets/icons/common/delete-icon.png";
import editIcon from "assets/icons/common/icon-edit.png";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import ModalDelete from "components/Modal/ModalConfirmDelete";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  deleteCustomView,
  loadCustomView,
  loadDetailsCustomView,
  loadHeaderSuccess,
  loadPermission,
  updateDefaultView,
} from "redux/slices/objects";
import styled from "styled-components";
import ModalView from "./modalView";
import ShareImg from "assets/icons/common/share.png";
import ModalShareView from "../modal/ModalShareView";
import { StarOutlined, CopyOutlined } from "@ant-design/icons";
import _ from "lodash";

const { Text } = Typography;

const CustomView = (props) => {
  const { visible, setVisible, objectId, mode, setFilterID, reload } = props;
  const [customViewID, setCustomViewID] = useState("");
  const {
    customView,
    customViewDetail,
    isLoadingDelete,
    customViewPermission,
  } = useSelector((state) => state.objectsReducer);
  const { userDetail, listAllUser } = useSelector((state) => state.userReducer);
  const [objectPermisson, setObjectPermission] = useState({});
  const [isClone, setIsClone] = useState(false);

  useEffect(() => {
    Object.entries(customViewPermission).forEach(([key, value]) => {
      if (objectId === key) {
        setObjectPermission(value);
      }
    });
  }, [objectId, customViewPermission]);

  let navigate = useNavigate();
  const { customViewId } = useParams();
  const handleChangeView = (view) => {
    let tem =
      (customViewId === "default-view" && view.is_default) ||
      customViewId === view._id;
    if (!tem) {
      dispatch(loadHeaderSuccess([]));
    }
  };
  const onClose = () => {
    setVisible(false);
  };

  const dispatch = useDispatch();

  useEffect(() => {
    if (
      window.location.pathname.includes("/consolidated-view") ||
      window.location.pathname.includes("/quality-management")
    ) {
      dispatch(
        loadCustomView({
          object_id: objectId,
        })
      );
    }
  }, [dispatch, objectId]);

  useEffect(() => {
    dispatch(loadPermission());
  }, [dispatch]);

  useEffect(() => {
    if (isLoadingDelete === false) {
      dispatch(setShowModalConfirmDelete(false));
      if (mode === "kanban") {
        // navigate(
        //   `/kanban-view?object_id=${objectId}&custom_view_id=${
        //     customViewID.length > 0 ? customViewID : "default_view"
        //   }`
        // );
        navigate(
          `/kanban-view/${objectId}/${
            customViewID.length > 0 ? customViewID : "default-view"
          }`
        );
      } else if (mode === "consolidated-view") {
      } else if (mode === "list-view-with-details") {
      } else if (mode === "QM View") {
      } else {
        navigate(`/objects/${objectId}/default-view`);
      }
    }
    /*eslint-disable-next-line*/
  }, [isLoadingDelete]);

  const [openView, setOpenView] = useState(false);
  const [deleteCustomViewModal, setDeleteCustomViewModal] = useState(false);
  const [openShareView, setOpenShareView] = useState(false);
  const [idView, setIdView] = useState("");
  const [share, setShare] = useState([]);
  const [myView, setMyView] = useState([]);
  const [shareView, setShareView] = useState([]);
  useEffect(() => {
    if (customView && customView.custom_views) {
      let tem = [...customView.custom_views];
      tem = tem.sort(
        (a, b) => new Date(b.modified_time) - new Date(a.modified_time)
      );
      let temMyView = [];
      let temShareView = [];
      tem.forEach((view) => {
        if (view.user_id === userDetail._id) {
          temMyView.push(view);
        } else {
          temShareView.push(view);
        }
      });
      setMyView(temMyView);
      setShareView(temShareView);
    } else {
      setMyView([]);
      setShareView([]);
    }
  }, [userDetail, customView]);
  const renderNameUser = (id) => {
    let name = "";
    listAllUser.forEach((user) => {
      if (user._id === id) {
        name = user.Full_Name;
      }
    });
    return name;
  };
  const renderView = (list) => {
    return list.map((view, idx) => (
      <CustomRow key={idx} gutter={16}>
        <Col
          span={14}
          onClick={() => {
            if (mode === "kanban") {
              // navigate(
              //   `/kanban-view?object_id=${objectId}&custom_view_id=${view._id}`
              // );
              navigate(`/kanban-view/${objectId}/${view._id}`);
            } else if (mode === "consolidated-view") {
              setFilterID(view._id);
            } else if (mode === "list-view-with-details") {
              navigate(
                `/list-view-with-details/objects/${objectId}/${view._id}`
              );
            } else if (mode === "QM View") {
              navigate(`/quality-management/${view._id}`);
            } else {
              handleChangeView(view);
              navigate(`/objects/${objectId}/${view._id}`);
            }
            setVisible(false);
          }}
        >
          <Title className="">
            <Text
              ellipsis={{
                tooltip: `${view.view_name} ${
                  userDetail._id !== view.user_id
                    ? ` (${renderNameUser(view.user_id)})`
                    : ""
                }`,
              }}
              style={{ width: "100%" }}
            >
              {view.view_name}
              {userDetail._id !== view.user_id
                ? ` (${renderNameUser(view.user_id)})`
                : ""}
            </Text>
          </Title>
        </Col>
        <Col
          span={1}
          style={{
            paddingTop: "10px",
            paddingLeft: "0px",
            paddingRight: "18px",
          }}
        >
          <StarOutlined
            onClick={() => {
              let data = {
                dataUpdate: {
                  view_id: view._id,
                  object_id: objectId,
                  is_default: !view.is_default,
                  default_share: userDetail._id !== view.user_id,
                },
                dataLoad: {
                  object_id: objectId,
                },
              };
              dispatch(updateDefaultView(data));

              setTimeout(() => {
                reload();

                if (mode === "QM View") {
                  navigate(`/quality-management/${_.get(view, "_id")}`);
                }
              }, 500);
            }}
            style={{
              color: `${view.is_default ? "#FADB14" : ""}`,
              fontSize: "20px",
            }}
          />
        </Col>
        <Col span={2}>
          {userDetail._id === view.user_id && objectPermisson.Edit_ListView && (
            <img
              onClick={() => {
                setOpenShareView(true);
                setIdView(view._id);
                setShare(view.share_to);
              }}
              style={{ width: "20px", marginTop: "2px" }}
              src={ShareImg}
              alt="Share view"
            />
          )}
        </Col>

        <Col span={2}>
          {userDetail._id === view.user_id && objectPermisson.Edit_ListView && (
            <img
              alt=""
              src={editIcon}
              style={{ width: "20px" }}
              onClick={() => {
                setCustomViewID(view._id);
                dispatch(
                  loadDetailsCustomView({
                    custom_view_id: view._id,
                    object_id: objectId,
                  })
                );
                setOpenView(true);
              }}
            />
          )}
        </Col>
        <Col
          span={2}
          style={{
            paddingTop: "10px",
            paddingLeft: "0px",
            paddingRight: "18px",
          }}
        >
          {userDetail._id === view.user_id &&
            objectPermisson.Create_ListView && (
              <CopyOutlined
                style={{ fontSize: "18px", marginLeft: "10px" }}
                onClick={() => {
                  setIsClone(true);
                  setCustomViewID(view._id);
                  dispatch(
                    loadDetailsCustomView({
                      custom_view_id: view._id,
                      object_id: objectId,
                    })
                  );
                  setOpenView(true);
                }}
              />
            )}
        </Col>
        <Col span={2}>
          {userDetail._id === view.user_id &&
            objectPermisson.Delete_ListView && (
              <img
                alt=""
                src={deleteIcon}
                style={{ width: "20px" }}
                onClick={() => {
                  setCustomViewID(view._id);
                  // dispatch(setShowModalConfirmDelete(true));
                  setDeleteCustomViewModal(true);
                }}
              />
            )}
        </Col>
      </CustomRow>
    ));
  };

  return (
    <>
      <Drawer
        title="Tùy chọn view"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={367}
      >
        <div style={{ display: "flex", justifyContent: "right" }}>
          {objectPermisson.Create_ListView && (
            <CustomButtonAddRecord
              size="large"
              onClick={() => {
                setOpenView(true);
              }}
            >
              <img alt="" src={plusIcon} />
              New
            </CustomButtonAddRecord>
          )}
        </div>
        <div>
          <CustomTitle>My custom View</CustomTitle>
          {renderView(myView)}
          <CustomRow gutter={16}>
            <Col
              span={24}
              onClick={() => {
                if (mode === "kanban") {
                  // navigate(
                  //   `/kanban-view?object_id=${objectId}&custom_view_id=default-view`
                  // );
                  navigate(`/kanban-view/${objectId}/default-view`);
                } else if (mode === "consolidated-view") {
                  setFilterID("default-view");
                } else if (mode === "list-view-with-details") {
                  navigate(
                    `/list-view-with-details/objects/${objectId}/default-view`
                  );
                } else if (mode === "QM View") {
                  navigate(`/quality-management/default-view`);
                } else {
                  // dispatch(loadHeaderSuccess([]));
                  navigate(`/objects/${objectId}/default-view`);
                }
                setVisible(false);
              }}
            >
              <Title className="">
                <Text
                  ellipsis={{
                    tooltip: "Default view",
                  }}
                  style={{ width: "100%" }}
                >
                  Default
                </Text>
              </Title>
            </Col>
          </CustomRow>
          <CustomTitle>Share to me</CustomTitle>

          {renderView(shareView)}
        </div>
        <ModalView
          isModalVisible={openView}
          setIsModalVisible={setOpenView}
          customViewDetail={customViewDetail}
          customViewID={customViewID}
          objectId={objectId}
          setCustomViewID={setCustomViewID}
          reload={reload}
          isClone={isClone}
          setIsClone={setIsClone}
        />
        <ModalShareView
          objectId={objectId}
          isModalVisible={openShareView}
          setIsModalVisible={setOpenShareView}
          idView={idView}
          listShare={share}
        />
        {customViewID ? (
          <ModalDelete
            openConfirm={deleteCustomViewModal}
            setOpenConfirm={setDeleteCustomViewModal}
            dataDelete={{
              data: {
                custom_view_id: customViewID,
                object_id: objectId,
              },
              load: {
                object_id: objectId,
              },
            }}
            title={"những bản ghi này"}
            decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
            methodDelete={deleteCustomView}
            setShowModalDelete={() => {}}
            type="custom-view"
          />
        ) : (
          ""
        )}
      </Drawer>
    </>
  );
};

export default CustomView;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  &:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  &:focus {
    color: #2c2c2c !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
  margin-bottom: 16px;
`;

const Title = styled.span`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  display: flex;
  align-items: center;

  /* Character/Color text main */

  color: #2c2c2c;
`;

const CustomRow = styled(Row)`
  /* margin-bottom: 20px; */
  padding: 4px 8px;
  :hover {
    background-color: #f5f5f5;
    cursor: pointer;
  }
  display: flex;
  justify-content: center;
  align-items: center;
`;

const CustomTitle = styled.div`
  font-family: var(--roboto-500);
  font-size: 16px;
`;
