import { colors } from "@atlaskit/theme";
import { Switch, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import eyeIconInvisible from "../../../../assets/icons/objectsManagement/eye-invisible.svg";
import eyeIcon from "../../../../assets/icons/objectsManagement/eye.svg";
import x from "../../../../assets/icons/objectsManagement/x.svg";
import { updateObject } from "../../../../redux/slices/objectsManagement";
import ModalAddObject from "../../Modal/modalAddObject";
import ModalDeleteObject from "../../Modal/modalDeleteObject";
import { borderRadius, grid } from "../constants";
import { useTranslation, withTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import _ from "lodash";

const { Text } = Typography;

const Container = styled.a`
  border-radius: ${borderRadius}px;
  border: 2px solid transparent;
  border-color: white;
  background-color: white;
  /* box-shadow: ${({ isDragging }) =>
    isDragging ? `2px 2px 1px ${colors.N70}` : "none"}; */
  padding: ${grid}px;
  min-height: 40px;
  margin-bottom: ${grid}px;
  user-select: none;

  /* anchor overrides */
  color: ${colors.N900};

  &:hover,
  &:active {
    color: ${colors.N900};
    text-decoration: none;
  }

  &:focus {
    outline: none;
    border-color: ${(props) => props.colors.hard};
    box-shadow: none;
  }

  /* flexbox */
  display: flex;
`;

// Previously this extended React.Component
// That was a good thing, because using React.PureComponent can hide
// issues with the selectors. However, moving it over does can considerable
// performance improvements when reordering big lists (400ms => 200ms)
// Need to be super sure we are not relying on PureComponent here for
// things we should be doing in the selector as we do not know if consumers
// will be using PureComponent

// export default class QuoteItem extends React.PureComponent {

const ObjectItem = (props) => {
  const { quote, isDragging, isGroupedOver, provided } = props;
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);
  const [openConfirm, setOpenConfirm] = useState(false);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "object_management" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const toggleVisible = () => {
    if (checkRule("edit")) {
      dispatch(
        updateObject({
          object_id: quote._id,
          data: {
            Name: quote.Name,
            record_number: quote.record_number,
            name_id: quote.name_id,
            label_name_id: quote.label_name_id,
            prefix: quote.prefix,
            group_id: quote.group_id,
            visible: !quote.visible,
            Status: quote.Status,
          },
        })
      );
    }
  };
  const onHide = () => {
    setOpen(false);
  };

  const [dataObject, setDataObject] = useState({});

  useEffect(() => {
    setDataObject(quote);
  }, [quote]);

  return (
    <>
      <Container
        // href={quote.author.url}
        isDragging={isDragging}
        isGroupedOver={isGroupedOver}
        colors={"#fff"}
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        onDoubleClick={() => {
          if (checkRule("edit")) {
            setOpen(true);
          }
        }}
      >
        <Content>
          <div className="title" style={{ width: "220px" }}>
            <Text ellipsis={{ tooltip: quote.Name }}>{quote.Name}</Text>
            <img
              alt=""
              src={quote.visible ? eyeIcon : eyeIconInvisible}
              onClick={toggleVisible}
            />
          </div>
          <div>
            <div className="decs">
              <div className="label">{t("object.labelIdName")}:</div>
              <div className="value">{quote.label_name_id || "Label name"}</div>
            </div>
            <div className="decs">
              <div className="label">{t("object.prefix")}:</div>
              <div className="value">{quote.prefix || "PRE"}</div>
            </div>
            <div className="decs">
              <div className="label">{t("object.startSequence")}:</div>
              <div className="value">{quote.record_number}</div>
            </div>
          </div>

          {checkRule("delete") &&
          quote._id.slice(quote._id.length - 6, quote._id.length) !==
            "_00001" ? (
            <div className="x">
              <img
                src={x}
                alt=""
                onClick={() => {
                  setOpenConfirm(true);
                }}
              />
            </div>
          ) : (
            ""
          )}

          <CustomToggle
            checkedChildren="On"
            unCheckedChildren="Off"
            checked={quote.Status}
            disabled={!checkRule("edit")}
            onClick={() => {
              dispatch(
                updateObject({
                  object_id: quote._id,
                  data: {
                    Name: quote.Name,
                    record_number: quote.record_number,
                    label_name_id: quote.label_name_id,
                    prefix: quote.prefix,
                    group_id: quote.group_id,
                    visible: quote.visible,
                    Status: !quote.Status,
                  },
                })
              );
            }}
          />
        </Content>
      </Container>
      <ModalAddObject
        setOpen={setOpen}
        open={open}
        onHide={onHide}
        object={dataObject}
      />
      <ModalDeleteObject
        setOpen={setOpenConfirm}
        open={openConfirm}
        title={quote.Name}
        object_id={quote._id}
      />
    </>
  );
};

export default withTranslation()(ObjectItem);

const Content = styled.div`
  /* flex child */
  flex-grow: 1;
  /*
    Needed to wrap text in ie11
    https://stackoverflow.com/questions/35111090/why-ie11-doesnt-wrap-the-text-in-flexbox
  */
  flex-basis: 100%;
  /* flex parent */
  display: flex;
  flex-direction: column;
  position: relative;
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
  .x {
    width: 23px;
    height: 23px;
    position: absolute;
    top: 0px;
    right: 0px;
    border: 1px solid #d9d9d9;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    border-radius: 2px;
    cursor: pointer;
    img {
      width: 11px;
    }
    :hover {
      box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
      transition: 0.5s;
    }
  }
  .title {
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* identical to box height, or 144% */

    /* Character/Color text main */
    margin-bottom: 4px;
    color: #2c2c2c;
    img {
      :hover {
        cursor: pointer;
      }
      margin-left: 5px;
    }
  }
  .decs {
    display: flex;
    .label {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      /* Character/Color text main */
      margin-right: 8px;
      color: #2c2c2c;
    }
    .value {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      /* Neutral / 7 */

      color: #8c8c8c;
    }
  }
`;

const CustomToggle = styled(Switch)`
  position: absolute;
  bottom: 0px;
  right: 0px;
`;
