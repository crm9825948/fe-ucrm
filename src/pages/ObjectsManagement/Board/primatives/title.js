import styled from "styled-components";
import { colors } from "@atlaskit/theme";
import { grid } from "../constants";

export default styled.div`
  width: 50px;
  padding: ${grid}px;
  transition: background-color ease 0.2s;
  flex-grow: 1;
  user-select: none;
  position: relative;
  display: flex;
  align-items: center;
  border-top-left-radius: 10px;
  border-top-right-radius: 10px;
  &:focus {
    outline: 2px solid ${colors.P100};
    outline-offset: 2px;
  }
  span {
    :hover {
      cursor: text;
    }
  }
`;
