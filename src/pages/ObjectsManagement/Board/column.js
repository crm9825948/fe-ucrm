import { colors } from "@atlaskit/theme";
import { Input, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import btnDelete from "../../../assets/icons/objectsManagement/button-delete.png";
import invisibleImg from "../../../assets/icons/objectsManagement/invisible.png";
import visibleImg from "../../../assets/icons/objectsManagement/visible.png";
import { updateGroup } from "../../../redux/slices/objectsManagement";
import ModalDeleteObject from "../Modal/modalDeleteObject";
import { borderRadius } from "./constants";
import QuoteList from "./primatives/objectList";
import Title from "./primatives/title";
import { useTranslation, withTranslation } from "react-i18next";
const { Text } = Typography;
const Container = styled.div`
  margin: 24px;
  margin-left: 0;
  display: flex;
  flex-direction: column;
  width: 285px;
  position: relative;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
  border-radius: 10px;
  min-height: 300px;
  height: fit-content;
  max-height: 600px;
  overflow-x: scroll;
  position: relative;
  padding-bottom: 0;
  background-color: #f5f5f5;
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-top-left-radius: ${borderRadius}px;
  border-top-right-radius: ${borderRadius}px;
  width: 100%;
  font-style: normal;
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  /* identical to box height, or 144% */
  position: sticky;
  top: 0;
  display: flex;
  align-items: center;
  z-index: 10000000000;
  /* Neutral/1 */

  color: #ffffff;
  background: ${(props) => props.theme.main};
  transition: background-color 0.2s ease;
  &:hover {
    background-color: ${(props) => props.theme.darker};
  }
  img {
    width: 24px;
    margin-right: 5px;
    cursor: pointer;
  }
`;

const Column = (props) => {
  const { title, object, index, groupID, columns, ordered, checkRule } = props;
  const { listID, visibleList } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [name, setName] = useState("");
  const [toggleEdit, setToggleEdit] = useState(false);
  const [open, setOpen] = useState(false);

  useEffect(() => {}, [ordered, columns]);

  useEffect(() => {
    setName(title);
    // eslint-disable-next-line
  }, [title]);

  return (
    <>
      <Draggable
        draggableId={title}
        index={index}
        isDragDisabled={!checkRule("edit")}
      >
        {(provided, snapshot) => (
          <Container
            ref={provided.innerRef}
            {...provided.draggableProps}
            className="columns"
          >
            <Header isDragging={snapshot.isDragging}>
              <Title
                isDragging={snapshot.isDragging}
                {...provided.dragHandleProps}
                onDoubleClick={() => {
                  if (checkRule("edit")) {
                    setName(title);
                    setToggleEdit(!toggleEdit);
                  }
                }}
              >
                {toggleEdit === true ? (
                  <Input
                    defaultValue={name}
                    autoFocus
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                    onBlur={() => {
                      if (name !== title && name.trim().length > 0) {
                        let newOrder = [];
                        // eslint-disable-next-line
                        ordered.map((item, idx) => {
                          let group = {};
                          group.name =
                            item === title ? name.trim() : item.trim();
                          group.objects = columns[item];
                          group.visible = visibleList[item];
                          group._id = listID[item];
                          group.position = idx;
                          newOrder.push(group);
                        });

                        dispatch(
                          updateGroup({
                            groups: newOrder,
                          })
                        );
                      }
                      setToggleEdit(false);
                    }}
                    onPressEnter={() => {
                      let newOrder = [];
                      // eslint-disable-next-line
                      if (name !== title && name.trim().length > 0) {
                        // eslint-disable-next-line
                        ordered.map((item, idx) => {
                          let group = {};
                          group.name =
                            item === title ? name.trim() : item.trim();
                          group.objects = columns[item];
                          group.visible = visibleList[item];
                          group._id = listID[item];
                          group.position = idx;
                          newOrder.push(group);
                        });

                        dispatch(
                          updateGroup({
                            groups: newOrder,
                          })
                        );
                      }

                      setToggleEdit(false);
                    }}
                  />
                ) : (
                  <>
                    <Text
                      ellipsis={{ tooltip: title }}
                      style={{ color: "white" }}
                    >
                      {title}
                    </Text>

                    <span
                      onClick={() => {
                        if (checkRule("edit")) {
                          let newOrder = [];
                          // eslint-disable-next-line
                          ordered.map((item, idx) => {
                            let group = {};
                            group.name = item;
                            group.objects = columns[item];
                            group.visible =
                              item === title
                                ? !visibleList[item]
                                : visibleList[item];
                            group._id = listID[item];
                            group.position = idx;
                            newOrder.push(group);
                          });

                          dispatch(
                            updateGroup({
                              groups: newOrder,
                            })
                          );
                        }
                      }}
                    >
                      {visibleList[title] === true ? (
                        <img
                          alt=""
                          src={visibleImg}
                          style={{ width: "15px", marginLeft: "12px" }}
                        />
                      ) : (
                        <img
                          alt=""
                          src={invisibleImg}
                          style={{ width: "15px", marginLeft: "12px" }}
                        />
                      )}
                    </span>
                  </>
                )}
              </Title>
              {object.length > 0 ? (
                ""
              ) : toggleEdit === true ? (
                ""
              ) : (
                <img
                  alt=""
                  src={btnDelete}
                  onClick={() => {
                    // dispatch(
                    //   deleteGroup({
                    //     group_id: groupID,
                    //   })
                    // );
                    setOpen(true);
                  }}
                />
              )}
            </Header>

            <QuoteList
              listId={title}
              listType="QUOTE"
              style={{
                backgroundColor: snapshot.isDragging ? colors.G50 : null,
              }}
              object={object}
              internalScroll={props.isScrollable}
              isCombineEnabled={Boolean(props.isCombineEnabled)}
              t={t}
              checkRule={checkRule}
            />
          </Container>
        )}
      </Draggable>
      <ModalDeleteObject
        open={open}
        setOpen={setOpen}
        ID={groupID}
        title={title}
      />
    </>
  );
};

export default withTranslation()(Column);
