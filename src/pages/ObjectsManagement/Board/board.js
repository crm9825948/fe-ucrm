import { colors } from "@atlaskit/theme";
import { css, Global } from "@emotion/react";
import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  reorderGroup,
  updateGroup,
} from "../../../redux/slices/objectsManagement";
import Column from "./column";
import reorder, { reorderQuoteMap } from "./reorder";

const ParentContainer = styled.div`
  /* height: ${({ height }) => height}; */
  /* overflow-x: hidden;
  overflow-y: auto; */
`;

const Container = styled.div`
  background-color: transparent;
  /* min-height: 100vh; */
  /* like display:flex but will allow bleeding over the window width */
  /* min-width: 100vw; */
  display: inline-flex;
`;

const Board = ({ isCombineEnabled = false, ...props }) => {
  const { initial, order, containerHeight, checkRule } = props;
  const { listID, visibleList } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const [columns, setColumns] = useState([]);
  const [ordered, setOrdered] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    setColumns(initial);
    setOrdered(order);
  }, [initial, order]);

  const onDragEnd = (result) => {
    if (result.combine) {
      if (result.type === "COLUMN") {
        const shallow = [...ordered];
        shallow.splice(result.source.index, 1);
        setOrdered(shallow);
        // this.setState({ ordered: shallow });
        return;
      }
      // eslint-disable-next-line
      const column = columns[result.source.droppableId];
      const withQuoteRemoved = [...column];
      withQuoteRemoved.splice(result.source.index, 1);
      // eslint-disable-next-line
      const columns = {
        // eslint-disable-next-line
        ...columns,
        [result.source.droppableId]: withQuoteRemoved,
      };
      // this.setState({ columns });
      setColumns(columns);
      return;
    }

    // dropped nowhere
    if (!result.destination) {
      return;
    }

    const source = result.source;
    const destination = result.destination;

    // did not move anywhere - can bail early
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    // reordering column
    if (result.type === "COLUMN") {
      const orderedTemp = reorder(ordered, source.index, destination.index);
      let newOrder = [];
      // eslint-disable-next-line
      /*eslint-disable-next-line*/
      orderedTemp.forEach((item, idx) => {
        let group = {};
        group.name = item;
        group.objects = columns[item];
        group.visible = visibleList[item];
        group._id = listID[item];
        group.position = idx;
        newOrder.push(group);
      });

      setOrdered(orderedTemp);
      dispatch(
        updateGroup({
          groups: newOrder,
        })
      );

      dispatch(reorderGroup(orderedTemp));

      return;
    }

    const data = reorderQuoteMap({
      quoteMap: columns,
      source,
      destination,
    });

    let newOrder = [];
    // eslint-disable-next-line
    ordered.forEach((item, idx) => {
      let group = {};
      group.name = item;
      group.objects = data.quoteMap[item];
      group.visible = visibleList[item];
      group._id = listID[item];
      group.position = idx;
      newOrder.push(group);
    });
    dispatch(
      updateGroup({
        groups: newOrder,
      })
    );

    setColumns(data.quoteMap);
  };

  const board = (
    <Droppable
      droppableId="board"
      type="COLUMN"
      direction="horizontal"
      ignoreContainerClipping={Boolean(containerHeight)}
      isCombineEnabled={props.isCombineEnabled}
    >
      {(provided) => (
        <Container ref={provided.innerRef} {...provided.droppableProps}>
          {ordered &&
            ordered.map((key, index) => (
              <Column
                key={key}
                index={index}
                title={key}
                object={columns[key]}
                groupID={listID[key]}
                isScrollable={props.withScrollableColumns}
                isCombineEnabled={props.isCombineEnabled}
                columns={columns}
                ordered={ordered}
                checkRule={checkRule}
              />
            ))}
          {provided.placeholder}
        </Container>
      )}
    </Droppable>
  );
  return (
    <React.Fragment>
      <DragDropContext onDragEnd={onDragEnd}>
        {containerHeight ? (
          <ParentContainer height={containerHeight}>{board}</ParentContainer>
        ) : (
          board
        )}
      </DragDropContext>
      <Global
        styles={css`
          body {
            background: ${colors.B200};
          }
        `}
      />
    </React.Fragment>
  );
};

export default Board;
