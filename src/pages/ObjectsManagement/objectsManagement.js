import { Breadcrumb, Button } from "antd";
import picklistImg from "assets/images/picklist/Picklist dependency.png";
import LoadingScreen from "components/LoadingScreen";
import { Notification } from "components/Notification/Noti";
import _ from "lodash";
import React, { useEffect, useState, useRef } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { changeTitlePage } from "redux/slices/authenticated";
import { setShowLoadingScreen } from "redux/slices/global";
import styled from "styled-components";
import plusIcon from "../../assets/icons/objectsManagement/plus.svg";
import {
  // loadCategory,
  // loadObject,
  setStatusCode,
} from "../../redux/slices/objectsManagement";
import Board from "./Board/board";
import ModalAddGroup from "./Modal/modalAddGroup";

const ObjectsManagement = () => {
  const { t } = useTranslation();
  const { category, order, isLoading, statusCode, objectStatus } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const [open, setOpen] = useState(false);

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const ref = useRef(null);
  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "object_management" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const dispatch = useDispatch();
  let navigate = useNavigate();

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.objectManagement")));
    //eslint-disable-next-line
  }, [t]);

  useEffect(() => {
    dispatch(setShowLoadingScreen(isLoading));
    if (isLoading === false) {
      if (objectStatus === "success") {
        switch (statusCode) {
          case "create-object":
            Notification("success", "Created object successfully!");
            dispatch(setStatusCode());
            break;
          case "update-object":
            Notification("success", "Updated object successfully!");
            dispatch(setStatusCode());
            break;
          case "delete-object":
            Notification("success", "Deleted object successfully!");
            dispatch(setStatusCode());
            break;
          case "create-group":
            Notification("success", "Created group successfully!");
            dispatch(setStatusCode());
            break;
          case "update-group":
            Notification("success", "Updated group successfully!");
            dispatch(setStatusCode());
            break;
          case "delete-group":
            Notification("success", "Deleted group successfully!");
            dispatch(setStatusCode());
            break;

          default:
            break;
        }
      } else if (objectStatus !== null) {
        switch (statusCode) {
          case "fail":
            Notification("error", objectStatus);
            dispatch(setStatusCode());
            break;
          default:
            break;
        }
      }
    }
    // eslint-disable-next-line
  }, [isLoading]);

  // useEffect(() => {
  //   dispatch(loadObject());
  //   // dispatch(loadCategory());
  // }, [dispatch]);

  const scroll = (scrollOffset) => {
    ref.current.scrollLeft += scrollOffset;
  };

  return (
    <Wrapper>
      <CustomHeader>
        <Breadcrumb>
          <Breadcrumb.Item>
            {/* eslint-disable-next-line  */}
            <a onClick={() => navigate("/settings")}> {t("common.settings")}</a>
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {/* eslint-disable-next-line */}
            <a onClick={() => navigate("/objects-management")}>
              {t("settings.objectManagement")}
            </a>
          </BreadcrumbItem>
        </Breadcrumb>
      </CustomHeader>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
      >
        {checkRule("create") ? (
          <CustomButton size="large" onClick={() => setOpen(true)}>
            <img alt="" src={plusIcon} />
            {t("object.addGRoup")}
          </CustomButton>
        ) : (
          ""
        )}
        <div style={{ display: "flex" }}>
          <button className="btn-scroll btn-right" onClick={() => scroll(-200)}>
            <svg
              width="6"
              height="12"
              viewBox="0 0 6 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.75 11.25L5.25 6L0.75 0.75"
                stroke="#637381"
                stroke-width="1.2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </button>
          <button className="btn-scroll btn-left" onClick={() => scroll(200)}>
            <svg
              width="6"
              height="12"
              viewBox="0 0 6 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M5.25 11.25L0.75 6L5.25 0.75"
                stroke="#637381"
                stroke-width="1.2"
                stroke-linecap="round"
                stroke-linejoin="round"
              />
            </svg>
          </button>
        </div>
      </div>

      {/* <Row gutter={[16, 32]}>
        {objects &&
          objects.map((object, idx) => {
            return <ObjectItem object={object} key={idx} />;
          })}
      </Row> */}
      <div style={{ overflowX: "scroll", minHeight: "80vh" }} ref={ref}>
        {Object.keys(category).length > 0 ? (
          <Board initial={category} order={order} checkRule={checkRule} />
        ) : (
          <div className="empty">
            <img alt="" src={picklistImg} />
            <div className="text">
              There's no any <span>groups</span>
            </div>
            {/* <CustomButton size="large">
              <img alt="" src={plusIcon} />
              {t("picklist.addNew")}
            </CustomButton> */}
          </div>
        )}
      </div>
      {/* <ModalAddObject open={open} setOpen={setOpen} /> */}
      <ModalAddGroup open={open} setOpen={setOpen} t={t} />
      <LoadingScreen />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  padding: 24px;
  transition: 0.5s;
  scroll-behavior: smooth;
  a {
    font-size: 16px;
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .empty {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    > img {
      width: 100px;
      margin-top: 60px;
    }
    .text {
      margin-top: 8px;
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 16px;
      line-height: 22px;
      /* identical to box height, or 137% */

      /* Character/Color text main */

      color: #2c2c2c;
      span {
        color: ${(props) => props.theme.main};
      }
    }
  }
  .btn-scroll {
    width: 24px;
    height: 24px;

    background: #ffffff;
    /* sh v3 */

    box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
    border-radius: 5px;
    transform: matrix(-1, 0, 0, 1, 0, 0);
    border: none;
    display: flex;
    justify-content: center;
    align-items: center;
    img {
      width: 7px;
    }
    &:hover {
      cursor: pointer;
      background-color: ${(props) => props.theme.main};
      transition: 0.5s;
      svg {
        path {
          stroke: white;
        }
      }
    }
  }
  .btn-right {
    margin-right: 9px;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  /* align-items: center; */
  margin-bottom: 24px;
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

export default withTranslation()(ObjectsManagement);
