import { Button, Checkbox, Form, Input, Modal, Select, Switch } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  createObject,
  updateObject,
} from "../../../redux/slices/objectsManagement";
import { useTranslation, withTranslation } from "react-i18next";
const { Option } = Select;

const ModalAddObject = (props) => {
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { open, setOpen, groupID, object } = props;
  const { t } = useTranslation();
  const { category, listID, isLoading } = useSelector(
    (state) => state.objectsManagementReducer
  );

  const [isLoggable, setIsLoggable] = useState(false);
  const [dayClean, setDayClean] = useState(30);
  const [isClean, setIsClean] = useState(true);
  const [dataDay, setDataDay] = useState(0);
  const [isEmbedIframe, $isEmbedIframe] = useState(false);

  useEffect(() => {
    if (!isLoggable) {
      setDataDay(0);
      return;
    }
    if (isClean) {
      setDataDay(dayClean);
    } else {
      setDataDay(0);
    }
  }, [isLoggable, isClean, dayClean]);

  useEffect(() => {
    if (isLoading === false) {
      form.resetFields();
      setOpen(false);
    }
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    if (object) {
      form.setFieldsValue({
        objectName: object.Name,
        labelIDName: object.label_name_id,
        prefix: object.prefix,
        startSequence: object.record_number,
        group: object.group_id,
        source_sharing: object.source_sharing || false,
        isEmbedIframe: object?.is_embed_iframe || false,
        linkIframe: object?.link_iframe || "",
      });

      $isEmbedIframe(object?.is_embed_iframe || false);

      if (Object.keys(object).includes("is_loggable")) {
        setIsLoggable(object.is_loggable);
      } else {
        setIsLoggable(true);
      }

      if (Object.keys(object).includes("clean_days_config")) {
        if (object.clean_days_config === 0) {
          setIsClean(false);
          setDayClean(30);
        } else {
          setIsClean(true);
          setDayClean(object.clean_days_config);
        }
      } else {
        setDayClean(30);
        setIsClean(true);
      }
    }

    // eslint-disable-next-line
  }, [object, open]);

  useEffect(() => {
    if (groupID)
      form.setFieldsValue({
        group: listID[groupID],
      });
    // eslint-disable-next-line
  }, [groupID, open]);

  const onFinish = (values) => {
    if (!object) {
      dispatch(
        createObject({
          Name: values.objectName.trim(),
          record_number: values.startSequence,
          label_name_id: values.labelIDName,
          prefix: values.prefix,
          group_id: values.group,
          is_loggable: isLoggable,
          clean_days_config: dataDay,
          source_sharing: false,
          is_embed_iframe: isEmbedIframe,
          link_iframe: values.linkIframe,
        })
      );
    } else {
      dispatch(
        updateObject({
          object_id: object._id,
          data: {
            Name: values.objectName.trim(),
            record_number: values.startSequence,
            label_name_id: values.labelIDName,
            prefix: values.prefix,
            group_id: values.group,
            is_loggable: isLoggable,
            clean_days_config: dataDay,
            visible: object.visible,
            source_sharing: values.source_sharing,
            is_embed_iframe: isEmbedIframe,
            link_iframe: values.linkIframe,
          },
        })
      );
    }
  };

  const onHandleCancel = (object) => {
    if (!object) {
      form.resetFields();
    }

    setOpen(false);
    $isEmbedIframe(false);
  };

  const onChangeIframe = (value) => {
    $isEmbedIframe(value);
  };

  return (
    <>
      <CustomModal
        title={!object ? "Thêm đối tượng" : "Chỉnh sửa đối tượng"}
        visible={open}
        onOk={() => onFinish()}
        onCancel={() => onHandleCancel(object)}
        width={600}
        footer={false}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={() => {}}
          autoComplete="off"
        >
          <Form.Item
            label={t("object.objectName")}
            name="objectName"
            rules={[{ required: true, message: "Please input object name!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("object.labelIdName")}
            name="labelIDName"
            rules={[{ required: true, message: "Please input label ID name!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("object.prefix")}
            name="prefix"
            rules={[
              {
                required: true,
                message: "Please input prefix without special characters!",
              },
              { type: "string", max: 3 },
            ]}
          >
            <Input pattern="^[a-zA-Z0-9]+$" disabled={object ? true : false} />
          </Form.Item>
          <Form.Item
            label={t("object.startSequence")}
            name="startSequence"
            rules={[
              { required: true, message: "Please input start sequence!" },
            ]}
          >
            <Input type="number" min={0} max={1000000000000} />
          </Form.Item>
          <Form.Item
            label={t("object.group")}
            name="group"
            rules={[{ required: true, message: "Please select group!" }]}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(category).map(([key, value], idx) => {
                return <Option value={listID[key]}>{key}</Option>;
              })}
            </Select>
          </Form.Item>
          {object ? (
            <Form.Item
              label={t("object.sourceSharing")}
              valuePropName="checked"
              name="source_sharing"
            >
              <Switch
                checkedChildren={t("workflow.on")}
                unCheckedChildren={t("workflow.off")}
              />
            </Form.Item>
          ) : (
            ""
          )}

          <Form.Item
            label="Embed iframe"
            valuePropName="checked"
            name="isEmbedIframe"
          >
            <Switch
              checkedChildren={t("workflow.on")}
              unCheckedChildren={t("workflow.off")}
              onChange={onChangeIframe}
              disabled={object ? true : false}
            />
          </Form.Item>

          {isEmbedIframe && (
            <Form.Item
              label="Link iframe"
              name="linkIframe"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          )}

          <LoggableWrap>
            <CustomCheckbox
              checked={isLoggable}
              onChange={(e) => setIsLoggable(e.target.checked)}
            >
              {t("object.saveLog")}
            </CustomCheckbox>

            {isLoggable && (
              <CleanWrap>
                <CustomCheckbox
                  checked={isClean}
                  onChange={(e) => setIsClean(e.target.checked)}
                >
                  {t("object.deleteAfter")}
                </CustomCheckbox>
                <Input
                  type="number"
                  disabled={!isClean}
                  min={1}
                  value={dayClean}
                  onChange={(e) => {
                    setDayClean(e.target.value);
                  }}
                />
                <span>{t("object.day")}</span>
              </CleanWrap>
            )}
          </LoggableWrap>

          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              loading={isLoading}
            >
              {t("object.save")}
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                onHandleCancel(object);
              }}
            >
              {t("object.cancel")}
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomCheckbox = styled(Checkbox)`
  .ant-checkbox-wrapper {
    display: flex;
    margin-bottom: 4px;
  }
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner {
    border-color: ${(props) => props.theme.main};
  }
`;

const LoggableWrap = styled.div`
  padding-left: 36px;
  span {
    font-family: var(--roboto-400);
    font-size: 14px;
    color: #2c2c2c;
  }
`;

const CleanWrap = styled.div`
  margin-top: 16px;

  input {
    margin-right: 8px;
    width: 65px;
  }

  .ant-input:hover,
  .ant-input:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }

  .ant-input {
    padding: 0 0 0 6px;
  }
`;
export default withTranslation()(ModalAddObject);
