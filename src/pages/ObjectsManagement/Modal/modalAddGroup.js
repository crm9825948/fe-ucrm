import { Button, Form, Input, Modal } from "antd";
import { default as React, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import { createGroup } from "../../../redux/slices/objectsManagement";
import { Notification } from "components/Notification/Noti";
import { useTranslation, withTranslation } from "react-i18next";

const ModalAddGroup = (props) => {
  const { open, setOpen } = props;
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isLoading } = useSelector((state) => state.objectsManagementReducer);

  useEffect(() => {
    if (isLoading === false) {
      setOpen(false);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  const onFinish = (e) => {
    if (e.groupName.trim().length === 0) {
      Notification("error", "Please input valid group name!");
    } else {
      dispatch(
        createGroup({
          name: e.groupName.trim(),
        })
      );
    }
  };

  const handleCancel = () => {
    setOpen(false);
    form.resetFields();
  };

  return (
    <CustomModal
      title={t("object.addGRoup")}
      visible={open}
      onOk={() => onFinish()}
      onCancel={() => handleCancel()}
      width={600}
      footer={false}
    >
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 6 }}
        wrapperCol={{ span: 18 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={() => {}}
        autoComplete="off"
      >
        <Form.Item
          label={t("object.groupName")}
          name="groupName"
          rules={[{ required: true, message: "Please input group name!" }]}
        >
          <Input />
        </Form.Item>
        <CustomFooter>
          <CustomButtonSave size="large" htmlType="submit" loading={isLoading}>
            {t("object.save")}
          </CustomButtonSave>
          <CustomButtonCancel size="large" onClick={() => handleCancel()}>
            {t("object.cancel")}
          </CustomButtonCancel>
        </CustomFooter>
      </Form>
    </CustomModal>
  );
};

export default withTranslation()(ModalAddGroup);

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
