import { Button, Modal } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import deleteObjectImg from "../../../assets/images/objectsManagement/deleteObject.png";
import {
  deleteGroup,
  deleteObject,
} from "../../../redux/slices/objectsManagement";
import { useTranslation, withTranslation } from "react-i18next";

const ModalDeleteObject = (props) => {
  const { open, setOpen, ID, title, object_id } = props;
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.objectsManagementReducer);
  const { t } = useTranslation();
  useEffect(() => {
    if (isLoading === false) {
      setOpen(false);
    }
    // eslint-disable-next-line
  }, [isLoading]);

  return (
    <>
      <CustomModal
        title={t("object.confirmAction")}
        visible={open}
        onCancel={() => setOpen(false)}
        width={400}
        footer={null}
      >
        <CustomContent>
          <img alt="" src={deleteObjectImg} className="delete-img" />
          <div className="title">
            {t("object.confirmDelete")} "{title}"?
          </div>
          <div className="decs">{t("object.afterDelete")}</div>
        </CustomContent>
        <CustomFooter>
          <CustomButtonSave
            size="large"
            htmlType="submit"
            loading={isLoading}
            onClick={() => {
              if (object_id) {
                dispatch(
                  deleteObject({
                    object_id: object_id,
                  })
                );
              } else if (ID) {
                dispatch(
                  deleteGroup({
                    group_id: ID,
                  })
                );
              }
            }}
          >
            Delete
          </CustomButtonSave>
          <CustomButtonCancel size="large" onClick={() => setOpen(false)}>
            {t("object.cancel")}
          </CustomButtonCancel>
        </CustomFooter>
      </CustomModal>
    </>
  );
};

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  width: 80px;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  .delete-img {
    width: 70px;
    margin-bottom: 16px;
  }
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* identical to box height, or 144% */

    /* Character/Color text main */

    color: #2c2c2c;

    /* Inside Auto Layout */

    flex: none;
    order: 0;
    flex-grow: 0;
    margin: 0px 0px;
  }
  .decs {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
    /* or 157% */

    text-align: center;

    /* Neutral / 8 */

    color: #595959;

    /* Inside Auto Layout */

    flex: none;
    order: 1;
    flex-grow: 0;
    margin: 0px 0px;
  }
`;

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
`;
export default withTranslation()(ModalDeleteObject);
