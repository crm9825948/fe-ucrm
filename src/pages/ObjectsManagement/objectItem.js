import { Col, Switch } from "antd";
import React, { useState } from "react";
import styled from "styled-components";
import accountIcon from "../../assets/icons/objectsManagement/object.png";
import contact from "../../assets/icons/objectsManagement/contact.png";
import ticket from "../../assets/icons/objectsManagement/ticket.png";
import defaultIcon from "../../assets/icons/objectsManagement/default.png";
import x from "../../assets/icons/objectsManagement/x.svg";
import ModalDeleteObject from "./Modal/modalDeleteObject";

const ObjectItem = (props) => {
  const { object } = props;
  const { Name } = object;
  const [openDelete, setOpenDelete] = useState(false);

  function getRandomArbitrary(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    switch (Math.floor(Math.random() * (max - min + 1)) + min) {
      case 1:
        return accountIcon;
      case 2:
        return contact;
      case 3:
        return ticket;
      case 4:
        return defaultIcon;
      default:
        return defaultIcon;
    }
  }

  return (
    <Col span={8} xs={24} sm={24} md={12} lg={8}>
      <CustomCol>
        <img alt="" src={getRandomArbitrary(1, 4)} className="icon-main" />
        <CustomContent>
          <div className="title">{Name}</div>
          <div className="decs">
            <div className="label">Label ID name:</div>
            <div className="value">Tickket ID</div>
          </div>
          <div className="decs">
            <div className="label">Prefix:</div>
            <div className="value">TIC01</div>
          </div>
          <div className="decs">
            <div className="label">Start sequence:</div>
            <div className="value">001</div>
          </div>
        </CustomContent>
        <div className="x" onClick={() => setOpenDelete(true)}>
          <img src={x} alt="" />
        </div>
        <CustomToggle checkedChildren="On" unCheckedChildren="Off" />
        <ModalDeleteObject open={openDelete} setOpen={setOpenDelete} />
      </CustomCol>
    </Col>
  );
};

const CustomCol = styled.div`
  height: 136px;
  background-color: #fff;
  position: relative;
  padding-top: 23px;
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
  .x {
    width: 23px;
    height: 23px;
    position: absolute;
    top: 23px;
    right: 23px;
    border: 1px solid #d9d9d9;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
    border-radius: 2px;
    cursor: pointer;
    img {
      width: 11px;
    }
    :hover {
      box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
      transition: 0.5s;
    }
  }
  .icon-main {
    width: 124px;
    position: absolute;
    left: 24px;
    top: -17px;
  }
  .title {
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* identical to box height, or 144% */

    /* Character/Color text main */
    margin-bottom: 4px;
    color: #2c2c2c;
  }
  .decs {
    display: flex;
    .label {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      /* Character/Color text main */
      margin-right: 8px;
      color: #2c2c2c;
    }
    .value {
      font-style: normal;
      font-family: var(--roboto-400);
      font-size: 14px;
      line-height: 20px;
      /* identical to box height, or 143% */

      /* Neutral / 7 */

      color: #8c8c8c;
    }
  }
  @media screen and (min-width: 768px) and (max-width: 1024px) {
    .icon-main {
      width: 49.6px;
      position: absolute;
      left: 9.6px;
      top: -6.8px;
    }
  }
  @media screen and (min-width: 1025px) and (max-width: 1366px) {
    .icon-main {
      width: 66px;
      position: absolute;
      left: 12.8px;
      top: -9px;
    }
  }
  position: relative;
  top: 0;
  transition: top ease 0.5s;
  :hover {
    box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 1s;
    cursor: pointer;
    top: -8px;
  }
`;

const CustomToggle = styled(Switch)`
  position: absolute;
  bottom: 28px;
  right: 23px;
`;

const CustomContent = styled.div`
  margin-left: 200px;
  @media screen and (min-width: 768px) and (max-width: 1024px) {
    margin-left: 80px;
  }
  @media screen and (min-width: 1025px) and (max-width: 1366px) {
    margin-left: 90px;
  }
`;

export default ObjectItem;
