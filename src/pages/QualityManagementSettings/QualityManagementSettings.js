import _ from "lodash";
import { memo, useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";

import { Breadcrumb, Button, Form, Select, Switch } from "antd";
import EmptyObject from "assets/icons/qualityManagement/IconEmpty.png";
import ModalConfim from "components/Modal/ModalConfirm";
import { setShowModalConfirm } from "redux/slices/global";
import {
  changeActiveSettings,
  getAllFormQM,
  getDetailSettings,
  saveDetailSettings,
} from "redux/slices/QualityManagement";
import { useForm } from "antd/lib/form/Form";
import { listOptionQMSettings } from "util/staticData";

const QualityManagementSettings = () => {
  const [form] = useForm();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [isReset, setIsReset] = useState(false);
  const [active, setActive] = useState(false);
  const { userDetail, userRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const { interactionConfig: interactionConfigState } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );
  const { listFormQMSettings, detailSettings } = useSelector(
    (state) => state.qualityManagementReducer
  );

  const onChange = (checked) => {
    setActive(checked);

    dispatch(
      changeActiveSettings({
        tenant_id: userDetail.tenant_id,
        active: checked,
        setting_id: detailSettings?._id || "",
      })
    );
  };

  const onFinish = (values) => {
    const config_setting = {};

    for (const key in values) {
      if (values.hasOwnProperty(key)) {
        if (isReset) {
          config_setting[key] = [];
        } else {
          config_setting[key] = Array.isArray(values[key])
            ? [...values[key]]
            : [];
        }
      }
    }

    dispatch(
      saveDetailSettings({
        tenant_id: userDetail.tenant_id,
        config_setting: { ...config_setting },
        setting_id: detailSettings?._id || "",
      })
    );

    setIsReset(false);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  const checkRule = useCallback(
    (rule, key) => {
      if (
        userRuleGlobal.find(
          (item) =>
            _.get(item, "domain", "") === key && item.actions.includes(rule)
        )
      )
        return true;
      else return false;
    },
    [userRuleGlobal]
  );

  useEffect(() => {
    dispatch(
      getAllFormQM({
        tenant_id: userDetail.tenant_id,
      })
    );

    dispatch(
      getDetailSettings({
        tenant_id: userDetail.tenant_id,
      })
    );
  }, [dispatch, userDetail.tenant_id]);

  useEffect(() => {
    if (!_.isArray(detailSettings)) {
      setActive(detailSettings.active);
    }
    form.setFieldsValue({
      ...detailSettings,
    });
  }, [detailSettings, form]);

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <ModalConfim
          img={EmptyObject}
          title={"Are you sure you want to go to Interaction Settings?"}
          decs={"You will be redirected to the Interaction Settings page."}
          type="Redirect"
          url="/interaction"
        />
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {t("QualityManagement.QualityManagementSettings")}
          </BreadcrumbItem>
        </Breadcrumb>
      </WrapBreadcrumb>
      {Object.keys(interactionConfigState).length > 0 ? (
        <WrapperBody>
          <WrapperHeader>
            <WrapperSwitch>
              <CustomSwitch
                checked={active}
                onChange={onChange}
                disabled={!checkRule("edit", "quality_management")}
              />
              <Title>{t("QualityManagement.QualityManagement")}</Title>
            </WrapperSwitch>
            <Des>{t("QualityManagement.textDes")}</Des>
          </WrapperHeader>
          <WrapperForm>
            <Form
              form={form}
              name="quality-management"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              style={{ width: "100%" }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              {_.map(listOptionQMSettings, (item) => (
                <Row
                  className={
                    active && checkRule("edit", "quality_management")
                      ? ""
                      : "disable"
                  }
                >
                  <WrapperLabel>
                    <CustomIcon bgColor={item.bgColor}>
                      <IMGIcon src={item.icon} alt="" />
                    </CustomIcon>
                    <Label>{item.text}</Label>
                  </WrapperLabel>
                  <WrapperInput>
                    <Text>{t("QualityManagement.chooseForm")}</Text>
                    <CustomFormItem name={item.value}>
                      <CustomSelect
                        disabled={
                          !active && !checkRule("edit", "quality_management")
                        }
                        style={{ width: 350 }}
                        mode="multiple"
                        showSearch
                        filterOption={(input, option) =>
                          (option?.label ?? "")
                            .toLowerCase()
                            .includes(input.toLowerCase())
                        }
                        options={listFormQMSettings.map((option) => {
                          return {
                            value: option._id,
                            label: option.form_name,
                            key: option._id,
                          };
                        })}
                      />
                    </CustomFormItem>
                  </WrapperInput>
                </Row>
              ))}
            </Form>
          </WrapperForm>
          {active && checkRule("edit", "quality_management") && (
            <WrapperButton>
              <ButtonCancel
                onClick={() => {
                  setIsReset(true);
                  form.submit();
                }}
              >
                Clear
              </ButtonCancel>
              <ButtonSave htmlType="submit" form="quality-management">
                {t("common.save")}
              </ButtonSave>
            </WrapperButton>
          )}
        </WrapperBody>
      ) : (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            Please configure the <span>Interaction Settings</span> before using
            Quality Management
          </p>
          <ButtonCustom onClick={() => dispatch(setShowModalConfirm(true))}>
            Go to Interation Settings
          </ButtonCustom>
        </Empty>
      )}
    </Wrapper>
  );
};

export default withTranslation()(memo(QualityManagementSettings));

const WrapperForm = styled.div`
  height: calc(100vh - 345px);
  overflow-y: auto;
  padding-right: 1rem;
  margin: 0.5rem 0;
`;

const Wrapper = styled.div`
  padding: 24px;
`;
const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapperBody = styled.div`
  width: 1000px;
  height: calc(100vh - 180px);
  margin: auto;
  margin-top: 1.5rem;
  background: #ffffff;
  border-radius: 8px;
  padding: 1.5rem 5rem;
  overflow-y: auto;
`;

const WrapperHeader = styled.div`
  display: flex;
  flex-direction: column;
  gap: 10px;
`;

const WrapperSwitch = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;
  gap: 1rem;
`;

const CustomSwitch = styled(Switch)`
  &.ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const Title = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #2c2c2c;
`;

const Des = styled.p`
  margin: 0;
  font-weight: 400;
  font-size: 14px;
  line-height: 20px;

  color: #fa8c16;
`;

const Row = styled.div`
  padding: 0.75rem 1rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #ffffff;

  border: 1px solid #ececec;
  border-radius: 8px;
  margin-bottom: 0.5rem;

  &.disable {
    filter: grayscale(100%);
    pointer-events: none;
    opacity: 0.7;
    user-select: none;
  }
`;

const WrapperLabel = styled.div`
  display: flex;
  align-items: center;
  gap: 0.75rem;
`;

const CustomIcon = styled.div`
  padding: 13px;
  border-radius: 8px;
  background-color: ${(props) => props.bgColor} !important;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const IMGIcon = styled.img`
  width: 20px;
  object-fit: contain;
`;

const Label = styled.text`
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;

  color: #2c2c2c;
`;

const WrapperInput = styled.div`
  display: flex;
  align-items: center;
  gap: 0.5rem;
`;

const Text = styled.p`
  margin: 0;
  font-weight: 500;
  font-size: 14px;
  line-height: 22px;
  color: #2c2c2c;
`;

const CustomFormItem = styled(Form.Item)`
  margin: 0;
`;

const WrapperButton = styled.div`
  text-align: right;
  margin-top: 1rem;
`;

const ButtonCancel = styled(Button)`
  padding: 5px 16px;
  width: 115px;
  height: 32px;

  background: #ffffff;

  border: 1px solid #d9d9d9;

  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;

  &:hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const ButtonSave = styled(Button)`
  margin-left: 1rem;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  width: 115px;
  height: 32px;
  color: #fff !important;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }
`;

const ButtonCustom = styled(Button)`
  margin-left: 1.5rem;
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 32px;
  color: #fff !important;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }

  span {
    background: ${(props) => props.theme.main};
    color: #fff !important;
  }
`;

const CustomSelect = styled(Select)`
  .ant-select-selector {
    .ant-select-selection-overflow {
      .ant-select-selection-overflow-item {
        .ant-select-selection-item {
          .ant-select-selection-item-content {
            .ant-checkbox-wrapper {
              display: none !important;
            }
          }
        }
      }
    }
  }
`;
const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
