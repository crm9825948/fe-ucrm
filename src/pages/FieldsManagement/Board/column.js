import { Input } from "antd";
import React, { useEffect, useState } from "react";
import { Draggable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import X from "../../../assets/images/fieldsManagement/X.png";
import { updatePosition } from "../../../redux/slices/fieldsManagement";
import ModalConfirm from "../Modal/modalConfirm";
import { borderRadius, grid } from "./constants";
import QuoteList from "./primatives/fieldList";
import Title from "./primatives/title";
import { useTranslation, withTranslation } from "react-i18next";
import { Collapse } from "antd";
import editIcon from "../../../assets/icons/common/icon-edit.png";

const { Panel } = Collapse;

const Container = styled.div`
  margin: ${grid}px;
  display: flex;
  flex-direction: column;
  margin-left: 0;
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
  .ant-collapse
    > .ant-collapse-item.ant-collapse-no-arrow
    > .ant-collapse-header,
  .ant-collapse
    > .ant-collapse-item.ant-collapse-no-arrow
    > .ant-collapse-header,
  .ant-collapse-content > .ant-collapse-content-box {
    padding: 0;
  }
`;

const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-top-left-radius: ${borderRadius}px;
  border-top-right-radius: ${borderRadius}px;
  background-color: #ffffff;
  transition: background-color 0.2s ease;
  position: relative;
  img {
    width: 15px;
    position: absolute;
    top: 20px;
    right: 24px;
    :hover {
      cursor: pointer;
    }
  }
`;

const Column = (props) => {
  const { title, quotes, index, order: orderProps, checkRule } = props;
  const dispatch = useDispatch();
  const [toggleEdit, setToggleEdit] = useState(false);
  const [name, setName] = useState("");
  const [openDelete, setOpenDelete] = useState(false);
  const { t } = useTranslation();
  const { listIDofSection, object_id, order } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  useEffect(() => {
    setName(title);
  }, [title]);

  return (
    <Draggable
      draggableId={title}
      index={index}
      isDragDisabled={!checkRule("edit")}
    >
      {(provided, snapshot) => (
        <Container ref={provided.innerRef} {...provided.draggableProps}>
          <Collapse
            // collapsible="icon"
            onChange={() => {}}
            defaultActiveKey={["1"]}
          >
            <Panel
              showArrow={false}
              header={
                <Header
                  isDragging={snapshot.isDragging}
                  // onDoubleClick={() => setToggleEdit(!toggleEdit)}
                >
                  <Title
                    isDragging={snapshot.isDragging}
                    {...provided.dragHandleProps}
                  >
                    {toggleEdit === true && checkRule("edit") ? (
                      <Input
                        onClick={(e) => {
                          e.preventDefault();
                          e.stopPropagation();
                        }}
                        defaultValue={name}
                        autoFocus
                        onChange={(e) => {
                          setName(e.target.value);
                        }}
                        onBlur={() => {
                          setToggleEdit(false);
                          if (
                            name.trim() === "" ||
                            name.trim() === title.trim()
                          ) {
                            setName(title);
                          } else if (
                            name.trim() !== title.trim() &&
                            orderProps.includes(name.trim()) === false
                          ) {
                            let newSections = [];
                            // eslint-disable-next-line
                            // eslint-disable-next-line
                            order.map((item, idx) => {
                              let section = {
                                _id: listIDofSection[item],
                                name:
                                  item === title.trim() ? name.trim() : item,
                                object_id: object_id,
                              };
                              newSections.push(section);
                            });
                            dispatch(
                              updatePosition({
                                sections: newSections,
                                object_id: object_id,
                              })
                            );
                          } else {
                            // Notification("error", "Section name must be unique!");
                            setName(title);
                          }
                        }}
                        onPressEnter={() => {
                          setToggleEdit(false);
                          if (
                            name.trim() === "" ||
                            name.trim() === title.trim()
                          ) {
                            setName(title);
                          } else if (
                            name.trim() !== title.trim() &&
                            orderProps.includes(name.trim()) === false
                          ) {
                            let newSections = [];
                            // eslint-disable-next-line
                            // eslint-disable-next-line
                            order.map((item, idx) => {
                              let section = {
                                _id: listIDofSection[item],
                                name:
                                  item === title.trim() ? name.trim() : item,
                                object_id: object_id,
                              };
                              newSections.push(section);
                            });
                            dispatch(
                              updatePosition({
                                sections: newSections,
                                object_id: object_id,
                              })
                            );
                          } else {
                            // Notification("error", "Section name must be unique!");
                            setName(title);
                          }
                        }}
                      />
                    ) : (
                      <>
                        <span>{name}</span>
                        <img
                          onClick={(e) => {
                            setToggleEdit(!toggleEdit);
                            e.preventDefault();
                            e.stopPropagation();
                          }}
                          src={editIcon}
                          alt="Edit name"
                        />
                      </>
                    )}
                  </Title>
                  {toggleEdit === true || quotes.length > 0 ? (
                    ""
                  ) : checkRule("delete") ? (
                    <img
                      alt=""
                      src={X}
                      onClick={(e) => {
                        setOpenDelete(true);
                        e.stopPropagation();
                        e.preventDefault();
                      }}
                    />
                  ) : (
                    ""
                  )}
                </Header>
              }
              key="1"
            >
              <QuoteList
                listId={title}
                listType="QUOTE"
                // style={{
                //   backgroundColor: snapshot.isDragging ? colors.G50 : null,
                // }}
                quotes={quotes}
                internalScroll={props.isScrollable}
                isCombineEnabled={Boolean(props.isCombineEnabled)}
                t={t}
                checkRule={checkRule}
              />
            </Panel>
          </Collapse>
          <ModalConfirm
            open={openDelete}
            setOpen={setOpenDelete}
            title={title}
            ID={listIDofSection[title]}
            object_id={object_id}
          />
        </Container>
      )}
    </Draggable>
  );
};

export default withTranslation()(Column);
