import { colors } from "@atlaskit/theme";
import { css, Global } from "@emotion/react";
import React, { useEffect, useState } from "react";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import {
  updatePosition,
  updatePositionField,
} from "../../../redux/slices/fieldsManagement";
import Column from "./column";
import reorder, { reorderQuoteMap } from "./reorder";

const ParentContainer = styled.div`
  /* height: ${({ height }) => height}; */
  /* overflow-x: hidden;
  overflow-y: auto; */
  display: flex;
  flex-direction: column;
`;

const Container = styled.div`
  /* background-color: ${colors.B100}; */
  display: flex;
  flex-direction: column;
  margin-top: 24px;
`;

const Board = ({ isCombineEnabled = false, ...props }) => {
  const { initial, order, containerHeight, checkRule } = props;
  const [columns, setColumns] = useState([]);
  const [ordered, setOrdered] = useState([]);
  const dispatch = useDispatch();
  const { listIDofSection, object_id } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  useEffect(() => {
    setColumns(initial);
    setOrdered(order);
  }, [initial, order]);

  const onDragEnd = (result) => {
    if (result.combine) {
      if (result.type === "COLUMN") {
        const shallow = [...ordered];
        shallow.splice(result.source.index, 1);
        // this.setState({ ordered: shallow });
        setOrdered(shallow);
        return;
      }

      const column = columns[result.source.droppableId];
      const withQuoteRemoved = [...column];
      withQuoteRemoved.splice(result.source.index, 1);
      const columnsTemp = {
        ...columns,
        [result.source.droppableId]: withQuoteRemoved,
      };
      // this.setState({ columns });
      setColumns(columnsTemp);
      return;
    }

    // dropped nowhere
    if (!result.destination) {
      return;
    }

    const source = result.source;
    const destination = result.destination;

    // did not move anywhere - can bail early
    if (
      source.droppableId === destination.droppableId &&
      source.index === destination.index
    ) {
      return;
    }

    // reordering column
    if (result.type === "COLUMN") {
      const orderedTemp = reorder(ordered, source.index, destination.index);

      let newSections = [];
      // eslint-disable-next-line
      orderedTemp.forEach((item, idx) => {
        let section = {
          _id: listIDofSection[item],
          name: item,
          object_id: object_id,
        };
        newSections.push(section);
      });

      setOrdered(orderedTemp);
      dispatch(
        updatePosition({
          sections: newSections,
          object_id: object_id,
        })
      );
      return;
    }

    const data = reorderQuoteMap({
      quoteMap: columns,
      source,
      destination,
    });

    //new data: data.quoteMap
    //old data: columns
    let fields = [];
    // eslint-disable-next-line
    ordered.forEach((section, idx) => {
      //In case the length of fields in section not equal with each other
      if (data.quoteMap[section].length !== columns[section].length) {
        let newPostion = data.quoteMap[section].map((item, index) => {
          return {
            ...item,
            position: index,
            section_id: listIDofSection[section],
          };
        });
        fields = [...fields, ...newPostion];
      }

      //In case the length of fields in section equal with each other

      if (data.quoteMap[section].length === columns[section].length) {
        // eslint-disable-next-line
        data.quoteMap[section].map((item, index) => {
          if (item._id !== columns[section][index]._id) {
            let newItem = {
              ...item,
              position: index,
            };
            fields.push(newItem);
          }
        });
      }
    });

    dispatch(
      updatePositionField({
        data: {
          fields: fields,
        },
        object_id: object_id,
      })
    );

    setColumns(data.quoteMap);
  };

  const board = (
    <Droppable
      droppableId="board"
      type="COLUMN"
      direction="vertical"
      ignoreContainerClipping={Boolean(containerHeight)}
      isCombineEnabled={props.isCombineEnabled}
    >
      {(provided) => (
        <Container ref={provided.innerRef} {...provided.droppableProps}>
          {ordered.map((key, index) => (
            <Column
              key={key}
              index={index}
              title={key}
              quotes={columns[key]}
              isScrollable={props.withScrollableColumns}
              isCombineEnabled={props.isCombineEnabled}
              order={order}
              checkRule={checkRule}
            />
          ))}
          {provided.placeholder}
        </Container>
      )}
    </Droppable>
  );

  return (
    <React.Fragment>
      <DragDropContext onDragEnd={onDragEnd}>
        {containerHeight ? (
          <ParentContainer height={containerHeight}>{board}</ParentContainer>
        ) : (
          board
        )}
      </DragDropContext>
      <Global
        styles={css`
          body {
            display: flex;
            flex-direction: column;
          }
        `}
      />
    </React.Fragment>
  );
};

export default Board;
