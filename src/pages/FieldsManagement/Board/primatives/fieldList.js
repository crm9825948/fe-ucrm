import { Button, Input, Switch, Tooltip } from "antd";
import React from "react";
import { Draggable, Droppable } from "react-beautiful-dnd";
import styled from "styled-components";
import deleteIcon from "../../../../assets/icons/common/delete-icon.png";
import editIcon from "../../../../assets/icons/common/icon-edit.png";
import noDataPic from "../../../../assets/images/objectsManagement/pic-empty.png";
import ModalAddField from "../../Modal/modalAddField";
import { grid } from "../constants";
import QuoteItem from "./fieldItem";
import Title from "./title";
import InfoCircle from "assets/icons/common/note.svg";

// const getBackgroundColor = (isDraggingOver, isDraggingFrom) => {
//   if (isDraggingOver) {
//     return colors.R50;
//   }
//   if (isDraggingFrom) {
//     return colors.T50;
//   }
//   return colors.N30;
// };

const Wrapper = styled.div`
  background-color: #fff;
  display: flex;
  flex-direction: column;
  opacity: ${({ isDropDisabled }) => (isDropDisabled ? 0.5 : "inherit")};
  padding: ${grid}px;
  border: ${grid}px;
  padding-bottom: 0;
  transition: background-color 0.2s ease, opacity 0.1s ease;
  user-select: none;
  width: 100%;
`;

const scrollContainerHeight = 250;

const DropZone = styled.div`
  /* stop the list collapsing when empty */
  min-height: fit-content;
  /*
    not relying on the items for a margin-bottom
    as it will collapse when the list is empty
  */
  padding-bottom: ${grid}px;
`;

const ScrollContainer = styled.div`
  overflow-x: hidden;
  overflow-y: auto;
  max-height: ${scrollContainerHeight}px;
`;

/* stylelint-disable block-no-empty */
const Container = styled.div`
  tr {
    transition: all 0.5s;
    :hover {
      background: #f4f4f4;
    }
  }
`;
/* stylelint-enable */

class InnerQuoteList extends React.Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.quotes !== this.props.quotes) {
      return true;
    }

    return false;
  }

  render() {
    return this.props.quotes.map((quote, index) => (
      <Draggable
        key={quote._id}
        draggableId={quote._id}
        index={index}
        shouldRespectForceTouch={false}
        isDragDisabled={!this.props.checkRule("edit")}
      >
        {(dragProvided, dragSnapshot) => (
          <QuoteItem
            key={quote._id}
            quote={quote}
            isDragging={dragSnapshot.isDragging}
            isGroupedOver={Boolean(dragSnapshot.combineTargetFor)}
            provided={dragProvided}
            title={this.props.title}
            width={this.props.width}
            checkRule={this.props.checkRule}
          />
        )}
      </Draggable>
    ));
  }
}

class InnerList extends React.Component {
  render() {
    const { quotes, dropProvided, width, checkRule } = this.props;
    const title = this.props.title ? <Title>{this.props.title}</Title> : null;

    return (
      <Container>
        {title}
        <DropZone ref={dropProvided.innerRef}>
          <InnerQuoteList
            quotes={quotes}
            title={this.props.listId}
            width={width}
            checkRule={checkRule}
          />
          {dropProvided.placeholder}
        </DropZone>
      </Container>
    );
  }
}

export default class QuoteList extends React.Component {
  static defaultProps = {
    listId: "LIST",
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      width: 0,
    };
    this.onHide = this.onHide.bind(this);
  }

  onHide() {
    this.setState({
      open: false,
    });
  }

  onSetWidth() {
    this.setState({
      width:
        document.getElementById("label-name") &&
        document.getElementById("label-name").clientWidth,
    });
  }

  render() {
    const {
      ignoreContainerClipping,
      internalScroll,
      scrollContainerStyle,
      isDropDisabled,
      isCombineEnabled,
      listId,
      listType,
      // style,
      quotes,
      title,
    } = this.props;

    return (
      <>
        <Droppable
          droppableId={listId}
          type={listType}
          ignoreContainerClipping={ignoreContainerClipping}
          isDropDisabled={isDropDisabled}
          isCombineEnabled={isCombineEnabled}
        >
          {(dropProvided, dropSnapshot) => (
            <CustomWrapper
              // style={style}
              isDraggingOver={dropSnapshot.isDraggingOver}
              isDropDisabled={isDropDisabled}
              isDraggingFrom={Boolean(dropSnapshot.draggingFromThisWith)}
              {...dropProvided.droppableProps}
              // eslint-disable-next-line
              style={{
                height: `${quotes.length === 0 ? "240px" : "fit-content"}`,
              }}
            >
              {quotes.length > 0 ? (
                <TableContainer>
                  <table>
                    <thead>
                      <tr>
                        <th id="label-name">
                          {this.props.t("objectLayoutField.dataFieldName")}
                        </th>
                        <th> {this.props.t("objectLayoutField.format")}</th>
                        <th>
                          {this.props.t("objectLayoutField.softRequired")}
                          <CustomTooltip
                            title={this.props.t("objectLayoutField.noteSoft")}
                          >
                            <img
                              src={InfoCircle}
                              alt="info"
                              style={{ marginLeft: "8px", marginBottom: "4px" }}
                            />
                          </CustomTooltip>
                        </th>
                        <th>
                          {" "}
                          {this.props.t(
                            "objectLayoutField.requiredInformation"
                          )}
                        </th>
                        <th> {this.props.t("objectLayoutField.massEdit")}</th>
                        <th> {this.props.t("objectLayoutField.readOnly")}</th>
                        <th>Page title</th>
                        <th> {this.props.t("objectLayoutField.hidden")}</th>
                        <th> {this.props.t("objectLayoutField.action")}</th>
                      </tr>
                      <tr>
                        <th>
                          <Input disabled />
                        </th>
                        <th>
                          <Input disabled />
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>
                          <Switch
                            checkedChildren="Yes"
                            unCheckedChildren="No"
                            defaultChecked
                            style={{ width: "50px", visibility: "hidden" }}
                          />
                        </th>
                        <th>
                          <div
                            className="action"
                            style={{ visibility: "hidden" }}
                          >
                            <img
                              alt=""
                              src={editIcon}
                              style={{ width: "18px", marginRight: "10px" }}
                            />
                            <img
                              alt=""
                              src={deleteIcon}
                              style={{ width: "26px" }}
                            />
                          </div>
                        </th>
                      </tr>
                    </thead>
                  </table>
                </TableContainer>
              ) : (
                ""
              )}
              {quotes.length === 0 ? (
                <>
                  <CustomEmpty>
                    <img alt="" src={noDataPic} />
                    <div className="title">
                      {this.props.t("objectLayoutField.noData")}{" "}
                      <span> {this.props.t("objectLayoutField.field")}</span>
                    </div>
                    {this.props.checkRule("create") ? (
                      <CustomButton
                        size="large"
                        onClick={() => {
                          this.setState({
                            open: true,
                          });
                        }}
                      >
                        {this.props.t("objectLayoutField.addNew")}
                      </CustomButton>
                    ) : (
                      ""
                    )}
                  </CustomEmpty>
                </>
              ) : (
                ""
              )}
              {internalScroll ? (
                <ScrollContainer style={scrollContainerStyle}>
                  <InnerList
                    quotes={quotes}
                    title={title}
                    dropProvided={dropProvided}
                    listId={listId}
                    width={this.state.width}
                    checkRule={this.props.checkRule}
                  />
                </ScrollContainer>
              ) : (
                <InnerList
                  quotes={quotes}
                  title={title}
                  dropProvided={dropProvided}
                  listId={listId}
                  width={this.state.width}
                  checkRule={this.props.checkRule}
                />
              )}
              {quotes.length > 0 && this.props.checkRule("create") ? (
                <CustomButtonInside
                  size="large"
                  onClick={() => {
                    this.setState({
                      open: true,
                    });
                  }}
                >
                  + {this.props.t("objectLayoutField.addNew")}
                </CustomButtonInside>
              ) : (
                ""
              )}
            </CustomWrapper>
          )}
        </Droppable>
        <ModalAddField
          open={this.state.open}
          setOpen={this.onHide}
          title={this.props.listId}
        />
      </>
    );
  }
}

const CustomButtonInside = styled(Button)`
  background-color: #fff;
  color: #2c2c2c;
  width: fit-content;
  margin-bottom: 30px;
  margin-top: 20px;
`;

const CustomWrapper = styled(Wrapper)`
  position: relative;
  height: fit-content;
`;

const CustomEmpty = styled.div`
  -webkit-animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #fff;
  flex-direction: column;
  position: absolute;
  top: 0px;
  left: 0;
  width: 100%;
  img {
    margin-top: 40px;
    width: 100px;
  }
  .title {
    margin-top: 8px;
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* Character/Color text main */

    color: rgba(0, 0, 0, 0.85);
    span {
      color: ${(props) => props.theme.main};
    }
    .button-add-object {
    }
  }
  button {
    margin-top: 16px;
    /* width: 105px; */
    margin-bottom: 24px;
  }
`;

const CustomButton = styled(Button)`
  width: max-content;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const TableContainer = styled.div`
  /* td {
    text-align: left;
  } */
  .action {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  table {
    width: 100%;
    thead {
      th {
        text-align: left;
        font-style: normal;
        font-family: var(--roboto-500);
        font-size: 16px;
        line-height: 22px;
        padding: 8px;
        border: 1px solid #d9d9d9;
        background-color: #f0f0f0;
      }
    }
  }
`;
const CustomTooltip = styled(Tooltip)``;
