import { colors } from "@atlaskit/theme";
import { Input, Switch, Tooltip } from "antd";
import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { borderRadius, grid } from "../constants";
import Checkbox from "antd/lib/checkbox/Checkbox";
import editIcon from "../../../../assets/icons/common/icon-edit.png";
import deleteIcon from "../../../../assets/icons/common/delete-icon.png";
import { useDispatch, useSelector } from "react-redux";
import {
  setField,
  updateField,
  loadObjectFields,
  loadListOfFieldKey,
} from "../../../../redux/slices/fieldsManagement";
import ModalAddField from "../../Modal/modalAddField";
import ModalConfirm from "../../Modal/modalConfirm";
import { Notification } from "components/Notification/Noti";
import { useTranslation, withTranslation } from "react-i18next";
import InfoCircle from "assets/icons/common/note.svg";

const Container = styled.div`
  border-radius: ${borderRadius}px;
  border: 1px solid #d9d9d9;
  background-color: #fff;
  width: 100%;
  /* padding: ${grid}px; */
  min-height: 40px;
  user-select: none;
  border-top: ${({ isDragging }) =>
    isDragging ? `1px solid #d9d9d9` : "none"};

  /* anchor overrides */
  color: ${colors.N900};

  &:hover,
  &:active {
    color: ${colors.N900};
    text-decoration: none;
  }

  &:focus {
    outline: none;
    border-color: ${(props) => props.colors.hard};
    box-shadow: none;
  }

  /* flexbox */
  display: flex;
  tr {
    padding: 0;
  }

  table {
    width: 100%;
    thead {
      th {
        text-align: left;
        font-style: normal;
        font-family: var(--roboto-500);
        font-size: 16px;
        line-height: 22px;
        padding: 8px;
        background-color: #f0f0f0;
        max-width: max-content;
      }
    }
    tbody {
      tr {
        td {
          padding: 8px;
          border-right: 1px solid #d9d9d9;
          white-space: normal;
          word-break: break-all;
        }
      }
    }
  }

  .action {
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      cursor: pointer;
    }
  }
  .label-name {
    :hover {
      cursor: text;
    }
  }
`;

const QuoteItem = (props) => {
  const {
    quote,
    isDragging,
    isGroupedOver,
    provided,
    title,
    width,
    checkRule,
  } = props;
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);
  const [editLabelName, setEditLabelName] = useState(false);
  const [labelName, setLabelName] = useState("");
  const [openDelete, setOpenDelete] = useState(false);
  const { listIDofSection, object_id, isLoading, sections } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const [fieldsDynamic, setFieldsDynamic] = useState([]);

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    Object.entries(sections).forEach(([key, value], index) => {
      // eslint-disable-next-line
      value.forEach((item, idx) => {
        if (item.type === "dynamic-field") {
          // eslint-disable-next-line
          Object.entries(item.list_items).forEach(([ele, valueEle], index) => {
            arr = [...arr, ...valueEle];
          });
        }
      });
    });
    setFieldsDynamic(arr);
  }, [sections]);

  const dispatch = useDispatch();

  useEffect(() => {
    if (isLoading === false) {
      dispatch(setField({}));
    }
    // eslint-disable-next-line
  }, [isLoading]);

  useEffect(() => {
    setLabelName(quote.name);
    // eslint-disable-next-line
  }, [quote]);

  return (
    <Wrapper>
      <Container
        // href={quote.author.url}
        isDragging={isDragging}
        isGroupedOver={isGroupedOver}
        colors={"#fff"}
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
      >
        <table style={{ width: "100%" }}>
          <thead style={{ visibility: "collapse", border: "none" }}>
            <tr>
              <th
                id="label-name"
                style={{ minWidth: `${width}px`, maxWidth: `${width}px` }}
              >
                {t("objectLayoutField.dataFieldName")}
              </th>
              <th> {t("objectLayoutField.format")}</th>
              <th>
                {t("objectLayoutField.softRequired")}
                <Tooltip title={t("objectLayoutField.noteSoft")}>
                  <img
                    src={InfoCircle}
                    alt="info"
                    style={{ marginLeft: "8px", marginBottom: "4px" }}
                  />
                </Tooltip>
              </th>
              <th>{t("objectLayoutField.requiredInformation")}</th>
              <th>{t("objectLayoutField.massEdit")}</th>
              <th>{t("objectLayoutField.readOnly")}</th>
              <th>Page title</th>
              <th>{t("objectLayoutField.hidden")}</th>
              <th>{t("objectLayoutField.action")}</th>
            </tr>
            <tr>
              <th>
                <Input />
              </th>
              <th>
                <Input />
              </th>
              <th></th>
              <th></th>
              <th></th>
              <th>
                {" "}
                <Switch
                  checkedChildren="Yes"
                  unCheckedChildren="No"
                  defaultChecked
                  style={{ width: "50px" }}
                />
              </th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td
                width={width}
                className="label-name"
                style={{ minWidth: `${width}px`, maxWidth: `${width}px` }}
                onDoubleClick={() => setEditLabelName(true)}
              >
                {editLabelName === true && checkRule("edit") ? (
                  <Input
                    defaultValue={quote.name}
                    autoFocus
                    onChange={(e) => {
                      setLabelName(e.target.value);
                    }}
                    onPressEnter={() => {
                      if (labelName !== quote.name) {
                        let newQuote = {
                          ...quote,
                          name: labelName.trim(),
                          object_id: object_id,
                          section_id: listIDofSection[title],
                        };
                        dispatch(
                          updateField({
                            data: newQuote,
                            object_id: object_id,
                          })
                        );
                        setEditLabelName(false);
                      }
                      setEditLabelName(false);
                    }}
                    onBlur={() => {
                      if (labelName !== quote.name) {
                        let newQuote = {
                          ...quote,
                          name: labelName.trim(),
                          object_id: object_id,
                          section_id: listIDofSection[title],
                        };
                        dispatch(
                          updateField({
                            data: newQuote,
                            object_id: object_id,
                          })
                        );
                        setEditLabelName(false);
                      }
                      setEditLabelName(false);
                    }}
                  />
                ) : (
                  quote.name
                )}
              </td>
              <td>{quote.type}</td>
              <td>
                <Checkbox
                  checked={quote.soft_required || quote.required}
                  disabled={
                    (quote.type === "linkingobject" && quote.key === false) ||
                    quote.required ||
                    quote.is_editor ||
                    quote.type === "id"
                      ? true
                      : quote.type === "formula"
                      ? true
                      : checkRule("edit")
                      ? false
                      : true
                  }
                  onClick={() => {
                    if (
                      fieldsDynamic.findIndex((ele) => ele === quote._id) < 0
                    ) {
                      let newQuote = {
                        ...quote,
                        soft_required: !quote.soft_required,
                        object_id: object_id,
                        section_id: listIDofSection[title],
                      };
                      if (
                        newQuote.soft_required === true &&
                        newQuote.readonly === true
                      ) {
                        Notification(
                          "warning",
                          "Soft required field can not be read only!"
                        );
                      } else if (
                        newQuote.soft_required === true &&
                        newQuote.hidden === true
                      ) {
                        Notification(
                          "warning",
                          "Hidden field cannot be soft required field!"
                        );
                      } else if (
                        newQuote.hidden === true &&
                        newQuote.mass_edit === true
                      ) {
                        Notification(
                          "warning",
                          "Mass edit and hidden field cannot true!"
                        );
                      } else {
                        dispatch(
                          updateField({
                            data: newQuote,
                            object_id: object_id,
                          })
                        );
                      }
                    } else {
                      Notification("warning", "Can not change soft required!");
                    }
                  }}
                />
              </td>
              <td>
                <Checkbox
                  checked={quote.required}
                  disabled={
                    (quote.type === "linkingobject" && quote.key === false) ||
                    quote.is_editor ||
                    quote.type === "id"
                      ? true
                      : quote.type === "formula"
                      ? true
                      : checkRule("edit")
                      ? false
                      : true
                  }
                  onClick={() => {
                    if (
                      fieldsDynamic.findIndex((ele) => ele === quote._id) < 0
                    ) {
                      let newQuote = {
                        ...quote,
                        required: !quote.required,
                        object_id: object_id,
                        section_id: listIDofSection[title],
                        soft_required: !quote.required
                          ? !quote.required
                          : quote.soft_required,
                      };
                      if (
                        newQuote.required === true &&
                        newQuote.readonly === true
                      ) {
                        Notification(
                          "warning",
                          "Required field can not be read only!"
                        );
                      } else if (
                        newQuote.required === true &&
                        newQuote.hidden === true
                      ) {
                        Notification(
                          "warning",
                          "Hidden field cannot be required field!"
                        );
                      } else if (
                        newQuote.hidden === true &&
                        newQuote.mass_edit === true
                      ) {
                        Notification(
                          "warning",
                          "Mass edit and hidden field cannot true!"
                        );
                      } else {
                        dispatch(
                          updateField({
                            data: newQuote,
                            object_id: object_id,
                          })
                        );
                      }
                    } else {
                      Notification("warning", "Can not change required!");
                    }
                  }}
                />
              </td>
              <td>
                {" "}
                <Checkbox
                  checked={quote.mass_edit}
                  disabled={
                    (quote.type === "linkingobject" && quote.key === false) ||
                    quote.is_editor ||
                    quote.type === "id"
                      ? true
                      : quote.type === "formula"
                      ? true
                      : checkRule("edit")
                      ? false
                      : true
                  }
                  onClick={() => {
                    let newQuote = {
                      ...quote,
                      mass_edit: !quote.mass_edit,
                      object_id: object_id,
                      section_id: listIDofSection[title],
                    };
                    if (
                      newQuote.mass_edit === true &&
                      newQuote.readonly === true
                    ) {
                      Notification(
                        "warning",
                        "Mass edit and read-only field cannot true!"
                      );
                    } else {
                      dispatch(
                        updateField({
                          data: newQuote,
                          object_id: object_id,
                        })
                      );
                    }
                  }}
                />
              </td>
              <td>
                <Checkbox
                  checked={quote.readonly}
                  disabled={
                    (quote.type === "linkingobject" && quote.key === false) ||
                    quote.type === "id"
                      ? true
                      : quote.type === "formula"
                      ? true
                      : checkRule("edit")
                      ? false
                      : true
                  }
                  onClick={() => {
                    let newQuote = {
                      ...quote,
                      readonly: !quote.readonly,
                      object_id: object_id,
                      section_id: listIDofSection[title],
                    };

                    if (
                      newQuote.required === true &&
                      newQuote.readonly === true
                    ) {
                      Notification(
                        "warning",
                        "Required field can not be read only!"
                      );
                    } else if (
                      newQuote.soft_required === true &&
                      newQuote.readonly === true
                    ) {
                      Notification(
                        "warning",
                        "Sort required field can not be read only!"
                      );
                    } else if (
                      newQuote.mass_edit === true &&
                      newQuote.readonly === true
                    ) {
                      Notification(
                        "warning",
                        "Mass edit and read-only field cannot true!"
                      );
                    } else {
                      dispatch(
                        updateField({
                          data: newQuote,
                          object_id: object_id,
                        })
                      );
                    }
                  }}
                />
              </td>
              <td>
                <Checkbox
                  disabled={
                    quote.type === "file" ||
                    quote.type === "user" ||
                    quote.type === "id"
                  }
                  checked={quote.page_title === true ? true : false}
                  onClick={() => {
                    let newQuote = {
                      ...quote,
                      page_title: quote.page_title === true ? false : true,
                    };

                    dispatch(
                      updateField({
                        data: newQuote,
                        object_id: object_id,
                      })
                    );
                  }}
                />
              </td>
              <td>
                <Switch
                  checked={quote.hidden}
                  checkedChildren="Yes"
                  unCheckedChildren="No"
                  disabled={quote.type === "id" || checkRule("edit") === false}
                  style={{ width: "50px" }}
                  onClick={() => {
                    let newQuote = {
                      ...quote,
                      hidden: !quote.hidden,
                      object_id: object_id,
                      section_id: listIDofSection[title],
                    };

                    if (
                      newQuote.required === true &&
                      newQuote.hidden === true
                    ) {
                      Notification(
                        "warning",
                        "Hidden field cannot be required field!"
                      );
                    } else if (
                      newQuote.soft_required === true &&
                      newQuote.hidden === true
                    ) {
                      Notification(
                        "warning",
                        "Hidden field cannot be soft required field!"
                      );
                    } else if (
                      newQuote.hidden === true &&
                      newQuote.mass_edit === true
                    ) {
                      Notification(
                        "warning",
                        "Mass edit and hidden field cannot true!"
                      );
                    } else {
                      dispatch(
                        updateField({
                          data: newQuote,
                          object_id: object_id,
                        })
                      );
                    }
                  }}
                />
              </td>
              <td>
                <div
                  className="action"
                  style={{
                    visibility: `${quote.type === "id" ? "hidden" : ""}`,
                  }}
                >
                  <img
                    alt=""
                    src={editIcon}
                    style={{
                      width: "18px",
                      marginRight: "10px",
                      visibility: `${checkRule("edit") ? "" : "hidden"}`,
                    }}
                    onClick={() => {
                      if (quote.type !== "id" && checkRule("edit")) {
                        dispatch(setField(quote));
                        if (quote.type === "linkingobject") {
                          dispatch(
                            loadObjectFields({
                              object_id: quote.objectname,
                            })
                          );
                          dispatch(
                            loadListOfFieldKey({
                              object_id: object_id,
                            })
                          );
                        }
                        setOpen(true);
                      }
                    }}
                  />
                  <img
                    alt=""
                    src={deleteIcon}
                    style={{
                      width: "26px",
                      visibility: `${
                        checkRule("delete") &&
                        quote.ID.slice(quote.ID.length - 3, quote.ID.length) !==
                          "_01"
                          ? ""
                          : "hidden"
                      }`,
                    }}
                    onClick={() => {
                      if (
                        quote.type !== "id" &&
                        checkRule("delete") &&
                        quote.ID.slice(quote.ID.length - 3, quote.ID.length) !==
                          "_01"
                      ) {
                        setOpenDelete(true);
                      }
                    }}
                  />
                </div>
              </td>
            </tr>
          </tbody>
        </table>

        <ModalConfirm
          open={openDelete}
          setOpen={setOpenDelete}
          fieldID={quote._id}
          title={quote.name}
          object_id={object_id}
        />
      </Container>

      <ModalAddField open={open} setOpen={setOpen} title={title} />
    </Wrapper>
  );
};
export default withTranslation()(QuoteItem);

const Wrapper = styled.div`
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
  .ant-checkbox-checked {
    background-color: ${(props) => props.theme.main};
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
`;
