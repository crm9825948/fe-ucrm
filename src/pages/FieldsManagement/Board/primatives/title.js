import styled from "styled-components";
import { colors } from "@atlaskit/theme";
import { grid } from "../constants";

export default styled.h4`
  padding: ${grid}px;
  transition: background-color ease 0.2s;
  flex-grow: 1;
  user-select: none;
  position: relative;
  &:focus {
    outline: 2px solid ${colors.P100};
    outline-offset: 2px;
  }
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 24px;
  /* identical to box height, or 150% */

  /* Primary/6 */
  margin: 0;
  color: ${(props) => props.theme.main};
  background-color: #f2f4f5;
  padding: 16px 24px;
  span {
    :hover {
      cursor: text;
    }
  }

  img {
    position: unset !important;
    margin-left: 10px;
    margin-bottom: 6px;
    width: 16px !important;
  }
`;
