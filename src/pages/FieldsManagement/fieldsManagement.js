import { Breadcrumb, Button, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import plusIcon from "../../assets/icons/objectsManagement/plus.svg";
import emptyImg from "../../assets/images/fieldsManagement/empty.png";
import { useNavigate } from "react-router-dom";
import {
  loadFields,
  setObjectID,
  setStatusCode,
  setOrder,
  loadFieldsSuccess,
} from "../../redux/slices/fieldsManagement";
// import { loadCategory, loadObject } from "../../redux/slices/objectsManagement";
import Board from "./Board/board";
import ModalSection from "./Modal/modalSection";
import LoadingScreen from "../../components/LoadingScreen";
import {
  loadRelatedObject,
  loadListOfFieldKey,
} from "redux/slices/fieldsManagement";
import { Notification } from "components/Notification/Noti";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";
import { changeTitlePage } from "redux/slices/authenticated";

const { Option, OptGroup } = Select;

const FieldsManagement = () => {
  const { t } = useTranslation();
  const {
    category,
    isLoading: objectLoading,
    // listFieldObject,
  } = useSelector((state) => state.objectsManagementReducer);
  const { object_id, sections, order, isLoading, statusCode, fieldsStatus } =
    useSelector((state) => state.fieldsManagementReducer);
  let navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const dispatch = useDispatch();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "object_layout" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    dispatch(changeTitlePage(t("objectLayoutField.objectLayoutField")));
    //eslint-disable-next-line
  }, [t]);

  useEffect(() => {
    dispatch(setObjectID(""));
    // dispatch(loadObject());
    // dispatch(loadCategory());
  }, [dispatch]);

  useEffect(() => {
    if (object_id) {
      dispatch(
        loadRelatedObject({
          object_id: object_id,
        })
      );
      dispatch(
        loadListOfFieldKey({
          object_id: object_id,
        })
      );
    } else {
      dispatch(setOrder([]));
    }
    // eslint-disable-next-line
  }, [object_id]);

  useEffect(() => {
    if (isLoading === false) {
      if (fieldsStatus === "success") {
        switch (statusCode) {
          case "create-section":
            Notification("success", "Created section successfully!");
            dispatch(setStatusCode());
            break;
          case "update-section":
            Notification("success", "Updated section successfully!");
            dispatch(setStatusCode());
            break;
          case "delete-section":
            Notification("success", "Deleted section successfully!");
            dispatch(setStatusCode());
            break;
          case "create-field":
            Notification("success", "Created field successfully!");
            dispatch(setStatusCode());
            break;
          case "update-position-field":
            Notification("success", "Updated field successfully!");
            dispatch(setStatusCode());
            break;
          case "update-field":
            Notification("success", "Updated field successfully!");
            dispatch(setStatusCode());
            break;
          case "delete-position-field":
            Notification("success", "Deleted field successfully!");
            dispatch(setStatusCode());
            break;

          default:
            break;
        }
      } else if (fieldsStatus !== null) {
        switch (statusCode) {
          case "fail":
            Notification("error", fieldsStatus);
            dispatch(setStatusCode());
            break;
          default:
            break;
        }
      }
    }
    // eslint-disable-next-line
  }, [isLoading]);

  return (
    <Wrapper>
      <CustomHeader>
        <Breadcrumb>
          <Breadcrumb.Item>
            {/* eslint-disable-next-line  */}
            <a onClick={() => navigate("/settings")}> {t("common.settings")}</a>
          </Breadcrumb.Item>
          <BreadcrumbItem>
            {/* eslint-disable-next-line */}
            <a onClick={() => navigate("/fields-management")}>
              {t("objectLayoutField.objectLayoutField")}
            </a>
          </BreadcrumbItem>
        </Breadcrumb>
      </CustomHeader>
      <CustomSelectObject>
        <div className="select-object">
          {" "}
          {t("objectLayoutField.chooseObject")}
        </div>
        <Select
          style={{ width: "305px" }}
          size="large"
          placeholder={t("objectLayoutField.selectObject")}
          onChange={(e) => {
            dispatch(setObjectID(e));
            dispatch(
              loadFieldsSuccess({
                order: [],
                sections: [],
                listIDofSection: [],
              })
            );
            dispatch(
              loadFields({
                object_id: e,
              })
            );
          }}
          value={object_id === "" ? null : object_id}
          defaultValue={object_id === "" ? null : object_id}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {Object.entries(category).map(([key, value], idx) => {
            return (
              <OptGroup label={key}>
                {/* eslint-disable-next-line*/}
                {value.map((object, index) => {
                  if (object.Status && !object?.is_embed_iframe)
                    return <Option value={object._id}>{object.Name}</Option>;
                })}
              </OptGroup>
            );
          })}
        </Select>
        {order.length > 0 && checkRule("create") ? (
          <CustomButtonAdd
            size="large"
            onClick={() => {
              setOpen(true);
            }}
          >
            <img alt="" src={plusIcon} />
            {t("objectLayoutField.addSection")}
          </CustomButtonAdd>
        ) : (
          ""
        )}
      </CustomSelectObject>
      {object_id && order && order.length === 0 ? (
        <CustomContent>
          <div className="empty-div">
            <img alt="" src={emptyImg} />
            <div>
              {t("objectLayoutField.noData")}{" "}
              <span>{t("objectLayoutField.section")}</span>
            </div>
            {checkRule("create") ? (
              <CustomButton
                onClick={() => {
                  setOpen(true);
                }}
              >
                {t("objectLayoutField.createNow")}
              </CustomButton>
            ) : (
              ""
            )}
          </div>
        </CustomContent>
      ) : object_id && order.length > 0 ? (
        <Board initial={sections} order={order} checkRule={checkRule} />
      ) : (
        ""
      )}

      <ModalSection open={open} setOpen={setOpen} />
      {isLoading || objectLoading ? <LoadingScreen /> : ""}
    </Wrapper>
  );
};

export default withTranslation()(FieldsManagement);

const Wrapper = styled.div`
  padding: 24px;
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const CustomHeader = styled.div`
  display: flex;
  justify-content: space-between;
  /* align-items: center; */
  margin-bottom: 24px;
`;

const CustomSelectObject = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  .select-object {
    margin-right: 39px;
  }
`;

const CustomContent = styled.div`
  .empty-div {
    margin-top: 48px;
    img {
      width: 100px;
      margin-bottom: 8px;
    }
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    div {
      margin-bottom: 16px;
      span {
        color: ${(props) => props.theme.main};
      }
    }
  }
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonAdd = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-left: 48px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
