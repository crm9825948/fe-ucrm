import React, { useEffect } from "react";
import { Button, Modal, Form, Input } from "antd";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { createSection } from "../../../redux/slices/fieldsManagement";
import { Notification } from "components/Notification/Noti";

const ModalSection = (props) => {
  const { open, setOpen } = props;
  const [form] = Form.useForm();
  const { isLoading, object_id } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  const dispatch = useDispatch();

  const onFinish = (e) => {
    if (e.sectionName.trim().length === 0) {
      Notification("error", "Please input valid section name!");
    } else {
      dispatch(
        createSection({
          data: {
            name: e.sectionName.trim(),
            object_id: object_id,
          },
          object_id: object_id,
        })
      );
    }
  };

  useEffect(() => {
    if (isLoading === false) {
      setOpen(false);
      form.resetFields();
    }
    // eslint-disable-next-line
  }, [isLoading]);

  const handleCancel = () => {
    setOpen(false);
    form.resetFields();
  };

  return (
    <CustomModal
      title="Thêm hạng mục"
      visible={open}
      onOk={() => onFinish()}
      onCancel={() => handleCancel()}
      width={400}
      footer={null}
    >
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        autoComplete="off"
        onFinish={onFinish}
        onFinishFailed={() => {}}
      >
        <Form.Item
          label="Tên hạng mục"
          name="sectionName"
          rules={[{ required: true, message: "Vui lòng thêm tên hạng mục!" }]}
        >
          <Input />
        </Form.Item>

        <CustomFooter>
          <CustomButtonSave size="large" htmlType="submit" loading={isLoading}>
            Save
          </CustomButtonSave>
          <CustomButtonCancel size="large" onClick={() => handleCancel()}>
            Cancel
          </CustomButtonCancel>
        </CustomFooter>
      </Form>
    </CustomModal>
  );
};

export default ModalSection;

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
`;
const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  width: 80px;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
