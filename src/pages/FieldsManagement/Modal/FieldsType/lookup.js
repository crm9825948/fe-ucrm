import { Checkbox, Form, Input, Select, Switch, Tooltip } from "antd";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import InfoCircle from "assets/icons/common/note.svg";
import { loadObjectFields } from "redux/slices/fieldsManagement";
const { Option, OptGroup } = Select;

const LinkingObjectType = (props) => {
  const { form, t } = props;
  const {
    field,
    // objects_related,
    fields_related,
    // list_of_field_key,
    // object_id,
  } = useSelector((state) => state.fieldsManagementReducer);
  const {
    category,
    // listFieldObject,
  } = useSelector((state) => state.objectsManagementReducer);

  const [, setObjectRelatedID] = useState("");
  const dispatch = useDispatch();
  // const [listLinking, setListLinking] = useState({});

  useEffect(() => {
    if (Object.keys(field).length > 0) {
      setObjectRelatedID(field.objectname);
      dispatch(
        loadObjectFields({
          object_id: field.objectname,
        })
      );
    }
    /* eslint-disable-next-line*/
  }, [field]);

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>
      <Form.Item
        label="Object"
        name="objectname"
        rules={[{ required: true, message: "Please select object!" }]}
      >
        <Select
          disabled={Object.keys(field).length > 0 ? true : false}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
          onChange={(e) => {
            setObjectRelatedID(e);
            form.setFieldsValue({ field: "" });
            dispatch(
              loadObjectFields({
                object_id: e,
              })
            );
          }}
        >
          {Object.entries(category).map(([key, value], idx) => {
            return (
              <OptGroup label={key}>
                {/* eslint-disable-next-line*/}
                {value.map((object, index) => {
                  if (object.Status)
                    return <Option value={object._id}>{object.Name}</Option>;
                })}
              </OptGroup>
            );
          })}
        </Select>
      </Form.Item>
      <Form.Item
        label="Field"
        name="field"
        rules={[{ required: true, message: "Please select field!" }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {fields_related &&
            fields_related.map((item, idx) => {
              if (item.type !== "file")
                return (
                  <Option
                    value={item.ID}
                    key={idx}
                    //   disabled={
                    //     list_of_field_key &&
                    //     list_of_field_key.findIndex(
                    //       (field, idx) => field.field === item.ID
                    //     ) >= 0
                    //       ? true
                    //       : false
                    //   }
                  >
                    {item.name}
                  </Option>
                );
              else return null;
            })}
        </Select>
      </Form.Item>
      <Form.Item name="required" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.requiredInformation")}</Checkbox>
      </Form.Item>{" "}
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox disabled={form.getFieldValue("required")}>
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.massEdit")}</Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
    </>
  );
};

export default LinkingObjectType;
