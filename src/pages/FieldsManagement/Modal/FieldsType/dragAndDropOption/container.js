import { useState, useCallback, useEffect } from "react";
import { Card } from "./Card";
import update from "immutability-helper";
import { PlusOutlined } from "@ant-design/icons";
import { Tag } from "antd";
import { useSelector } from "react-redux";
import { setOption as setOptionAction } from "../../../../../redux/slices/fieldsManagement";
import { useDispatch } from "react-redux";

const style = {
  width: 400,
};
const Container = (props) => {
  const { setOption } = props;
  const dispatch = useDispatch();

  const [cards, setCards] = useState([]);

  const { field } = useSelector((state) => state.fieldsManagementReducer);

  const [inputVisible, setInputVisible] = useState(false);
  const [inputValue, setInputValue] = useState("");

  useEffect(() => {
    if (field.type === "select" || field.type === "dynamic-field") {
      let newOption = [];
      // eslint-disable-next-line
      field.option.map((item, idx) => {
        let newItem = {
          id: idx + 1,
          text: item.label,
        };
        newOption.push(newItem);
      });
      setCards(newOption);
    }
  }, [field]);

  useEffect(() => {
    setOption(cards);
    dispatch(setOptionAction(cards));
    // eslint-disable-next-line
  }, [cards]);

  const moveCard = useCallback(
    (dragIndex, hoverIndex) => {
      const dragCard = cards[dragIndex];
      setCards(
        update(cards, {
          $splice: [
            [dragIndex, 1],
            [hoverIndex, 0, dragCard],
          ],
        })
      );
    },
    [cards]
  );

  const renderCard = (card, index) => {
    return (
      <Card
        key={card.id}
        index={index}
        id={card.id}
        text={card.text}
        moveCard={moveCard}
        cards={cards}
        setCards={setCards}
      />
    );
  };

  // const saveInputRef = (input) => {
  //   // eslint-disable-next-line
  //   input = input;
  // };

  const showInput = () => {
    setInputVisible(true);
  };

  const handleInputChange = (e) => {
    // if (e.target.value.trim().length > 0) {
    setInputValue(
      e.target.value.replace(
        /[^/-a-zA-Z0-9aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆ fFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTu UùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ()-]/g,
        ""
      )
    );
    // }
  };

  const handleInputConfirm = () => {
    if (inputValue.trim()) {
      let newTags = [...cards];

      let newKey = 0;
      // eslint-disable-next-line
      cards.forEach((card, idx) => {
        if (newKey <= card.id) {
          newKey = card.id;
        }
      });
      if (
        cards.length === 0 ||
        (inputValue &&
          cards.findIndex((item) => item.text === inputValue.trim()) < 0)
      ) {
        let newTag = {
          id: parseInt(newKey) + 1,
          text: inputValue.trim(),
        };
        newTags.push(newTag);
      }

      setCards(newTags);
      setInputVisible(false);
      setInputValue("");
    } else {
      setInputVisible(false);
      setInputValue("");
    }
  };

  return (
    <>
      <div style={style}>{cards.map((card, i) => renderCard(card, i))}</div>
      {inputVisible && (
        <input
          // ref={saveInputRef}
          type="text"
          size="small"
          style={{
            width: 150,
            padding: "0.5rem 1rem",
            marginBottom: ".5rem",
            backgroundColor: "white",
            cursor: "move",
          }}
          value={inputValue}
          autoFocus
          onChange={handleInputChange}
          onBlur={handleInputConfirm}
          onPressEnter={handleInputConfirm}
        />
      )}
      {!inputVisible && (
        <Tag
          onClick={showInput}
          className="site-tag-plus"
          style={{
            padding: "0.5rem 1rem",
            marginBottom: ".5rem",
            backgroundColor: "white",
            cursor: "move",
          }}
        >
          <PlusOutlined /> New Tag
        </Tag>
      )}
    </>
  );
};

export default Container;
