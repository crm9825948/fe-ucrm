import React from "react";
// import { DndProvider } from "react-dnd";
// import { HTML5Backend } from "react-dnd-html5-backend";
import Container from "./container";

const DragAndDropOption = (props) => {
  const { setOption, option } = props;
  return (
    // <DndProvider backend={HTML5Backend}>
    <Container setOption={setOption} option={option} />
    // </DndProvider>
  );
};

export default DragAndDropOption;
