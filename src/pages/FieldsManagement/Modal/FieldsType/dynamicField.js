import { Checkbox, Form, Input, Select, Switch, Tooltip } from "antd";
import React from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
// import DragAndDrogOption from "./dragAndDropOption";
import { useSelector } from "react-redux";
import Container from "./dragAndDropOption/container";
import InfoCircle from "assets/icons/common/note.svg";

const { Option, OptGroup } = Select;

const DynamicField = (props) => {
  const { field: fieldProps, option: optionNew } = useSelector(
    (state) => state.fieldsManagementReducer
  );
  const { option, setOption, sections, form, t } = props;

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.autoFill")}
        name="auto_fill"
        rules={[{ required: true, message: "Please input auto fill!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={fieldProps.auto_fill}
        />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={fieldProps.hidden}
        />
      </Form.Item>
      <Form.Item
        label="Option"
        rules={[{ required: true, message: "Please input options!" }]}
      >
        <DndProvider backend={HTML5Backend}>
          <Container setOption={setOption} option={option} />
        </DndProvider>
      </Form.Item>
      {optionNew.map((item, idx) => {
        form.setFieldsValue({
          [item.text]: form.getFieldValue(item.text)
            ? form.getFieldValue(item.text)
            : [],
        });

        return (
          <Form.Item label={item.text} name={item.text}>
            <Select
              mode="multiple"
              defaultValue={
                form.getFieldValue(item.text)
                  ? form.getFieldValue(item.text)
                  : []
              }
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {Object.entries(sections).map(([key, value], idx) => {
                return (
                  <OptGroup label={key}>
                    {/*eslint-disable-next-line*/}
                    {value.map((field, index) => {
                      if (
                        !field.required &&
                        form.getFieldValue("name") !== field.name
                      )
                        return (
                          <Option value={field._id} key={field._id}>
                            {field.name}
                          </Option>
                        );
                    })}
                  </OptGroup>
                );
              })}
            </Select>
          </Form.Item>
        );
      })}
      <Form.Item label="Default value" name="default_value">
        <Select
          allowClear
          onClear={() => {
            form.setFieldsValue({
              [fieldProps.ID]: null,
            });
          }}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {optionNew.map((item, idx) => {
            return (
              <Option value={item.text} key={idx}>
                {item.text}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
      <Form.Item name="required" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.requiredInformation")}</Checkbox>
      </Form.Item>{" "}
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox disabled={form.getFieldValue("required")}>
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.massEdit")}</Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
    </>
  );
};

export default DynamicField;
