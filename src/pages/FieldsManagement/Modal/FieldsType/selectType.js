import { Checkbox, Form, Input, Switch, Select, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
// import DragAndDrogOption from "./dragAndDropOption";
import { useSelector } from "react-redux";
import Container from "./dragAndDropOption/container";
import InfoCircle from "assets/icons/common/note.svg";

const { Option } = Select;

const SelectType = (props) => {
  const { field, option: optionNew } = useSelector(
    (state) => state.fieldsManagementReducer
  );
  const { option, setOption, form, t } = props;
  const [multipleMode, setMultipleMode] = useState(false);

  useEffect(() => {
    if (field) {
      if (field.type === "select") {
        setMultipleMode(field.multiple);
      }
    }
  }, [field]);

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>
      <Form.Item label="Option">
        <DndProvider backend={HTML5Backend}>
          <Container setOption={setOption} option={option} />
        </DndProvider>
      </Form.Item>
      <Form.Item label="Multiple" name="multiple">
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.multiple}
          // disabled={true}
          disabled={Object.keys(field).length > 0 ? true : false}
          onChange={(e) => {
            setMultipleMode(e);
            if (e) {
              form.setFieldsValue({ default_value: [] });
            } else {
              form.setFieldsValue({ default_value: "" });
            }
          }}
        />
      </Form.Item>
      <Form.Item label="Default value" name="default_value">
        <Select
          mode={`${multipleMode === true ? "multiple" : ""}`}
          allowClear
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {optionNew.map((item, idx) => {
            return (
              <Option value={item.text} key={idx}>
                {item.text}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
      <Form.Item name="required" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.requiredInformation")}</Checkbox>
      </Form.Item>{" "}
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox disabled={form.getFieldValue("required")}>
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.massEdit")}</Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
    </>
  );
};

export default SelectType;
