import { Checkbox, Form, Input, Select, Switch, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  loadObjectFields,
  loadListOfFieldKey,
} from "redux/slices/fieldsManagement";
import InfoCircle from "assets/icons/common/note.svg";
const { Option } = Select;

const LinkingObjectType = (props) => {
  const { form, t } = props;
  const {
    field,
    objects_related,
    fields_related,
    list_of_field_key,
    object_id,
    sections,
  } = useSelector((state) => state.fieldsManagementReducer);
  const [objectRelatedID, setObjectRelatedID] = useState("");
  const dispatch = useDispatch();
  const [listLinking, setListLinking] = useState({});

  const [fieldsDynamic, setFieldsDynamic] = useState([]);

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    Object.entries(sections).forEach(([key, value], index) => {
      // eslint-disable-next-line
      value.forEach((item, idx) => {
        if (item.type === "dynamic-field") {
          // eslint-disable-next-line
          Object.entries(item.list_items).forEach(([ele, valueEle], index) => {
            arr = [...arr, ...valueEle];
          });
        }
      });
    });
    setFieldsDynamic(arr);
  }, [sections]);

  useEffect(() => {
    dispatch(
      loadListOfFieldKey({
        object_id: object_id,
      })
    );
    /*eslint-disable-next-line*/
  }, [object_id]);

  useEffect(() => {
    if (Object.keys(field).length > 0) {
      if (field.type === "linkingobject") {
        setObjectRelatedID(field.objectname);
      }
    }
  }, [field]);

  useEffect(() => {
    if (form.getFieldValue("type") === "linkingobject") {
      let newObject = {};
      // eslint-disable-next-line
      objects_related.map((item, idx) => {
        newObject[item._id] = [];
        // eslint-disable-next-line
        list_of_field_key.map((value, index) => {
          if (value.objectname === item._id) {
            newObject[item._id].push(value);
          }
        });
      });
      form.setFieldsValue({
        readonly:
          form.getFieldValue("type") === "linkingobject"
            ? newObject &&
              newObject[objectRelatedID] &&
              newObject[objectRelatedID].length === 0
              ? false
              : newObject &&
                newObject[objectRelatedID] &&
                newObject[objectRelatedID].length > 0
              ? field.key === true
                ? false
                : true
              : true
            : false,
      });
      setListLinking(newObject);
    }
    // eslint-disable-next-line
  }, [objects_related, field, form, objectRelatedID]);

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>

      <Form.Item
        label="Object"
        name="objectname"
        rules={[{ required: true, message: "Please select object!" }]}
      >
        <Select
          disabled={Object.keys(field).length > 0 ? true : false}
          onChange={(e) => {
            setObjectRelatedID(e);
            form.setFieldsValue({ field: "" });
            dispatch(
              loadObjectFields({
                object_id: e,
              })
            );
          }}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {objects_related &&
            objects_related.map((item, idx) => {
              return (
                <Option value={item._id} key={idx}>
                  {item.Name}
                </Option>
              );
            })}
        </Select>
      </Form.Item>

      <Form.Item
        label="Field"
        name="field"
        rules={[{ required: true, message: "Please select field!" }]}
      >
        <Select
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {fields_related &&
            fields_related.map((item, idx) => {
              return (
                <Option
                  value={item.ID}
                  key={idx}
                  disabled={
                    list_of_field_key &&
                    list_of_field_key.findIndex(
                      (field, idx) => field.field === item.ID
                    ) >= 0
                      ? true
                      : false
                  }
                >
                  {item.name}
                </Option>
              );
            })}
        </Select>
      </Form.Item>

      {!(form.getFieldValue("type") === "linkingobject"
        ? listLinking &&
          listLinking[objectRelatedID] &&
          listLinking[objectRelatedID].length === 0
          ? false
          : listLinking &&
            listLinking[objectRelatedID] &&
            listLinking[objectRelatedID].length > 0
          ? field.key === true
            ? false
            : true
          : true
        : false) ? (
        <Form.Item
          label="Independent linking"
          name="independent_linking"
          valuePropName="checked"
        >
          <Switch
            checkedChildren="Yes"
            unCheckedChildren="No"
            defaultChecked={field.independent_linking}
          />
        </Form.Item>
      ) : (
        ""
      )}

      <Form.Item name="required" valuePropName="checked">
        <Checkbox
          disabled={
            fieldsDynamic.findIndex((ele) => ele === field._id) >= 0 ||
            (form.getFieldValue("type") === "linkingobject"
              ? listLinking &&
                listLinking[objectRelatedID] &&
                listLinking[objectRelatedID].length === 0
                ? false
                : listLinking &&
                  listLinking[objectRelatedID] &&
                  listLinking[objectRelatedID].length > 0
                ? field.key === true
                  ? false
                  : true
                : true
              : false)
          }
        >
          {t("objectLayoutField.requiredInformation")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox
          disabled={
            form.getFieldValue("required") ||
            fieldsDynamic.findIndex((ele) => ele === field._id) >= 0 ||
            (form.getFieldValue("type") === "linkingobject"
              ? listLinking &&
                listLinking[objectRelatedID] &&
                listLinking[objectRelatedID].length === 0
                ? false
                : listLinking &&
                  listLinking[objectRelatedID] &&
                  listLinking[objectRelatedID].length > 0
                ? field.key === true
                  ? false
                  : true
                : true
              : false)
          }
        >
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox
          disabled={
            form.getFieldValue("type") === "linkingobject"
              ? listLinking &&
                listLinking[objectRelatedID] &&
                listLinking[objectRelatedID].length === 0
                ? false
                : listLinking &&
                  listLinking[objectRelatedID] &&
                  listLinking[objectRelatedID].length > 0
                ? field.key === true
                  ? false
                  : true
                : true
              : false
          }
        >
          {t("objectLayoutField.massEdit")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox
          disabled={
            form.getFieldValue("type") === "linkingobject"
              ? listLinking &&
                listLinking[objectRelatedID] &&
                listLinking[objectRelatedID].length === 0
                ? false
                : listLinking &&
                  listLinking[objectRelatedID] &&
                  listLinking[objectRelatedID].length > 0
                ? field.key === true
                  ? false
                  : true
                : true
              : false
          }
        >
          {t("objectLayoutField.readOnly")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="encrypted" valuePropName="checked">
        <Checkbox disabled={Object.keys(field).length > 0 ? true : false}>
          {t("objectLayoutField.encrypted")}
        </Checkbox>
      </Form.Item>
    </>
  );
};

export default LinkingObjectType;
