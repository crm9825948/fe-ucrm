import { Checkbox, Form, Input, Switch, Tooltip } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import InfoCircle from "assets/icons/common/note.svg";

const DateType = ({ t, form }) => {
  const { field } = useSelector((state) => state.fieldsManagementReducer);

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.autoFill")}
        name="auto_fill"
        rules={[{ required: true, message: "Please input auto fill!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.auto_fill}
        />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>
      <Form.Item name="required" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.requiredInformation")}</Checkbox>
      </Form.Item>{" "}
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox disabled={form.getFieldValue("required")}>
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.massEdit")}</Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
      <Form.Item name="encrypted" valuePropName="checked">
        <Checkbox disabled={Object.keys(field).length > 0 ? true : false}>
          {t("objectLayoutField.encrypted")}
        </Checkbox>
      </Form.Item>
    </>
  );
};

export default DateType;
