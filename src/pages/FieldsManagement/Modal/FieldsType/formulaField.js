import {
  Checkbox,
  Form,
  Input,
  Select,
  Switch,
  Typography,
  InputNumber,
  Tooltip,
} from "antd";
import _ from "lodash";
import React, { useEffect, useState, useRef } from "react";
import { useSelector } from "react-redux";
import InfoCircle from "assets/icons/common/note.svg";

const { Text } = Typography;
const { Option, OptGroup } = Select;

const FormulaField = (props) => {
  const { field } = useSelector((state) => state.fieldsManagementReducer);
  const { sections, t, form, formula, setFormula } = props;
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const [fields, setFields] = useState({});
  const [valueUnit, setValueUnit] = useState("");

  useEffect(() => {
    if (listObjectField.length > 0) {
      let objectFields = {};
      /*eslint-disable-next-line*/
      listObjectField.map((object, idx) => {
        if (listObjectField.length - 1 === idx) {
          objectFields["main_object"] = [];
          /*eslint-disable-next-line*/
          object["main_object"].sections.map((section, idx) => {
            /*eslint-disable-next-line*/
            section.fields.map((field, index) => {
              objectFields["main_object"].push(field);
            });
          });
        } else {
          /*eslint-disable-next-line*/
          Object.entries(object).forEach(([key, value], idx) => {
            objectFields[object[key].object_name] = [];
            /*eslint-disable-next-line*/
            return object[key].sections.map((section, idx) => {
              /*eslint-disable-next-line*/
              return section.fields.map((field, index) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false
                ) {
                  objectFields[object[key].object_name].push(field);
                } else if (field.permission_hidden === undefined) {
                  objectFields[object[key].object_name].push(field);
                }
              });
            });
          });
        }
      });
      setFields(objectFields);
    }
  }, [listObjectField]);

  const [newFields, setNewFields] = useState({});
  const [formulaTypeValue, setFormulaTypeValue] = useState(null);
  const [formulaValue, setFormulaValue] = useState([]);

  const [insertField, setInsertField] = useState([]);

  useEffect(() => {
    if (Object.keys(field).length > 0) {
      setFormulaTypeValue(field.formula_type);
      setFormulaValue(field.formula);
      setValueUnit(_.get(field, "unit", ""));
    }
  }, [field]);

  const metaData = [
    {
      name: "Created Date",
      _id: "created_date",
      type: "date",
    },
    {
      name: "Created By",
      _id: "created_by",
      type: "text",
    },
    {
      name: "Modify Time",
      _id: "modify_time",
      type: "date",
    },
    {
      name: "Modified by",
      _id: "modify_by",
      type: "text",
    },
    {
      name: "Assign To",
      _id: "owner",
      type: "text",
    },
  ];

  const [formulaType] = useState({
    String: [
      {
        label: "Concat",
        value: "strConcat",
      },
    ],
    Date: [
      {
        label: "Date add",
        value: "dateAdd",
      },
      {
        label: "Date subtract",
        value: "dateSub",
      },
      {
        label: "Date diff",
        value: "dateDiff",
      },
      {
        label: "Date to parts",
        value: "dateToParts",
      },
    ],
    Number: [
      {
        label: "Advance expression",
        value: "advanceExpression",
      },
    ],
  });

  const unitDateType = [
    {
      label: "Date",
      value: "date",
    },
    {
      label: "Hour",
      value: "hour",
    },
    {
      label: "Minute",
      value: "minute",
    },
    {
      label: "Second",
      value: "second",
    },
    {
      label: "Millisecond",
      value: "millisecond",
    },
  ];

  const unitDatePartType = [
    {
      label: "Year",
      value: "year",
    },
    {
      label: "Month",
      value: "month",
    },
    {
      label: "Date",
      value: "date",
    },
    {
      label: "Weekday",
      value: "weekday",
    },
    {
      label: "Hour",
      value: "hour",
    },
    {
      label: "Minute",
      value: "minute",
    },
    {
      label: "Second",
      value: "second",
    },
    {
      label: "Millisecond",
      value: "millisecond",
    },
  ];

  useEffect(() => {
    if (formulaTypeValue) {
      let tmpSections = { ...sections };
      tmpSections["Meta data"] = [...metaData];

      let newFieldsTmp = { ...tmpSections };
      switch (formulaTypeValue) {
        case "strConcat":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter(
              (item) => item.type === "text" || item.type === "textarea"
            );
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;

        case "dateAdd":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter(
              (item) =>
                item.type === "date" ||
                item.type === "datetime-local" ||
                item.type === "number"
            );
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;
        case "dateSub":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter(
              (item) =>
                item.type === "date" ||
                item.type === "datetime-local" ||
                item.type === "number"
            );
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;
        case "dateDiff":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter(
              (item) => item.type === "date" || item.type === "datetime-local"
            );
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;
        case "dateToParts":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter(
              (item) => item.type === "date" || item.type === "datetime-local"
            );
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;
        // case "add":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;
        // case "subtract":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;
        // case "multiply":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;

        // case "min":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;

        // case "max":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;

        // case "divide":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;

        // case "mod":
        //   // eslint-disable-next-line
        //   Object.entries(newFieldsTmp).map(([key, value], idx) => {
        //     let newValueArray = value.filter((item) => item.type === "number");
        //     newFieldsTmp[key] = [...newValueArray];
        //   });
        //   setNewFields(newFieldsTmp);
        //   break;
        case "advanceExpression":
          // eslint-disable-next-line
          Object.entries(newFieldsTmp).forEach(([key, value], idx) => {
            let newValueArray = value.filter((item) => item.type === "number");
            newFieldsTmp[key] = [...newValueArray];
          });
          setNewFields(newFieldsTmp);
          break;

        default:
          break;
      }
    }
    // eslint-disable-next-line
  }, [formulaTypeValue]);

  const ref = useRef(null);

  const handleClick = () => {
    ref.current.focus();
  };

  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label="Placeholder" name="placeholder">
        <Input />
      </Form.Item>

      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>

      <Form.Item
        label="Formula type"
        name="formula_type"
        rules={[{ required: true, message: "Please select fomular type!" }]}
      >
        <Select
          disabled={Object.keys(field).length > 0 ? true : false}
          onChange={(e) => {
            setFormulaTypeValue(e);
            setFormulaValue([]);
            form.setFieldsValue({
              ...form.getFieldsValue(),
              formula: [],
              unit: "",
            });
            setValueUnit("");
          }}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {Object.entries(formulaType).map(([key, value], idx) => {
            return (
              <OptGroup label={key}>
                {value.map((item, index) => {
                  return <Option value={item.value}>{item.label}</Option>;
                })}
              </OptGroup>
            );
          })}
        </Select>
      </Form.Item>
      {formulaTypeValue === "dateDiff" ? (
        <Form.Item name="in_words" valuePropName="checked">
          <Checkbox disabled={Object.keys(field).length > 0 ? true : false}>
            In words
          </Checkbox>
        </Form.Item>
      ) : (
        ""
      )}
      {formulaTypeValue !== "advanceExpression" ? (
        <Form.Item
          label="Formula"
          name="formula"
          rules={[{ required: true, message: "Please select fomular!" }]}
        >
          <Select
            mode="multiple"
            onChange={(e) => setFormulaValue(e)}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {Object.entries(newFields).map(([key, value], idx) => {
              return (
                <OptGroup label={key}>
                  {/* eslint-disable-next-line */}
                  {value.map((field, index) => {
                    if (
                      field.type === "text" ||
                      field.type === "textarea" ||
                      field.type === "date" ||
                      field.type === "datetime-local" ||
                      field.type === "number"
                    ) {
                      if (
                        formulaTypeValue === "dateAdd" ||
                        formulaTypeValue === "dateSub"
                      ) {
                        if (formulaValue.length === 0) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.includes(field.ID)
                                  ? false
                                  : field.type === "number"
                                  ? true
                                  : false
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        } else if (formulaValue.length === 1) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.includes(field.ID)
                                  ? false
                                  : formulaValue.findIndex(
                                      (item) =>
                                        item === field._id &&
                                        (field.type === "date" ||
                                          field.type === "datetime-local")
                                    ) >= 0
                                  ? false
                                  : field.type === "date" ||
                                    field.type === "datetime-local"
                                  ? true
                                  : false
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        } else if (formulaValue.length > 1) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.includes(field.ID)
                                  ? false
                                  : formulaValue.findIndex(
                                      (item) =>
                                        item === field._id &&
                                        field.type === "number"
                                    ) >= 0
                                  ? false
                                  : true
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        }
                      } else if (formulaTypeValue === "dateDiff") {
                        if (formulaValue.length <= 1) {
                          return (
                            <Option value={field.ID} key={field._id}>
                              {field.name}
                            </Option>
                          );
                        } else if (formulaValue.length < 3) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.findIndex(
                                  (item) => item === field.ID
                                ) >= 0
                                  ? false
                                  : true
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        }
                      } else if (formulaTypeValue === "dateToParts") {
                        if (formulaValue.length < 1) {
                          return (
                            <Option value={field.ID} key={field._id}>
                              {field.name}
                            </Option>
                          );
                        } else if (formulaValue.length < 2) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.findIndex(
                                  (item) => item === field._id
                                ) >= 0
                                  ? false
                                  : true
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        }
                      } else if (
                        formulaTypeValue === "divide" ||
                        formulaTypeValue === "mod"
                      ) {
                        if (formulaValue.length === 0) {
                          return (
                            <Option value={field.ID} key={field._id}>
                              {field.name}
                            </Option>
                          );
                        } else if (formulaValue.length === 1) {
                          return (
                            <Option
                              value={field.ID}
                              key={field._id}
                              disabled={
                                formulaValue.findIndex(
                                  (item) => item === field._id
                                ) >= 0
                                  ? false
                                  : true
                              }
                            >
                              {field.name}
                            </Option>
                          );
                        } else {
                          return (
                            <Option value={field.ID} key={field._id}>
                              {field.name}
                            </Option>
                          );
                        }
                      } else {
                        return (
                          <Option value={field.ID} key={field._id}>
                            {field.name}
                          </Option>
                        );
                      }
                    }
                  })}
                </OptGroup>
              );
            })}
          </Select>
        </Form.Item>
      ) : (
        <>
          <Form.Item
            label="Advance expression"
            name="expression_string"
            rules={[
              { required: true, message: "Please select advance expression!" },
            ]}
          >
            <Input.TextArea ref={ref} />
          </Form.Item>
          <Form.Item
            label="Decimal Places"
            name="decimal_separator"
            rules={[
              {
                type: "number",
                min: 0,
                max: 10,
              },
            ]}
          >
            <InputNumber type="number" min={0} max={10} />
          </Form.Item>
          <Select
            style={{ width: "100%", marginBottom: "16px" }}
            placeholder="Insert field"
            mode="multiple"
            onChange={(e) => {
              let tempString = form.getFieldValue("expression_string") || "";
              // let temp = [...formula];
              // temp.push(e[0]);
              setFormula([...formula, ...e]);
              form.setFieldsValue({
                expression_string: tempString + ` $${e}`,
              });
              setInsertField([]);
              handleClick();
            }}
            value={insertField}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              // if (option.children) {
              //   return option?.children
              //     .toLowerCase()
              //     .indexOf(inputValue.toLowerCase()) >= 0
              //     ? true
              //     : false;
              // } else if (option.label) {
              //   return option?.label
              //     .toLowerCase()
              //     .indexOf(inputValue.toLowerCase()) >= 0
              //     ? true
              //     : false;
              // }
              if (option.field) {
                if (option.field.related_name) {
                  return option?.field?.related_name
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.field.name) {
                  return option?.field?.name
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }
            }}
          >
            {Object.entries(fields).map(([key, value], idx) => {
              if (key !== "main_object") {
                return (
                  <OptGroup label={key}>
                    {value.map((field, idx) => {
                      if (
                        field.type === "number" ||
                        (field.type === "formula" &&
                          field.formula_type === "advanceExpression")
                      ) {
                        return (
                          <Option
                            value={field.full_field_id}
                            field={field}
                            type={field.type}
                            key={field.full_field_id + Math.random() + ""}
                          >
                            <Text
                              ellipsis={{
                                tooltip: field.name,
                              }}
                              style={{ width: "100%" }}
                            >
                              {key}.{field.name}
                            </Text>
                          </Option>
                        );
                      } else return null;
                    })}
                  </OptGroup>
                );
              } else {
                return (
                  <OptGroup
                    label={` ${
                      listObjectField[listObjectField.length - 1]["main_object"]
                        .object_name
                    }(Main)`}
                  >
                    {value.map((field, idx) => {
                      if (
                        field.type === "number" ||
                        (field.type === "formula" &&
                          field.formula_type === "advanceExpression")
                      )
                        return (
                          <Option
                            value={field.ID}
                            field={field}
                            type={field.type}
                            key={field.full_field_id + Math.random() + key + ""}
                          >
                            {" "}
                            <Text
                              ellipsis={{
                                tooltip: field.name,
                              }}
                              style={{ width: "100%" }}
                            >
                              {field.name}
                            </Text>
                          </Option>
                        );
                      else return null;
                    })}
                  </OptGroup>
                );
              }
            })}
          </Select>
        </>
      )}

      {formulaTypeValue === "dateAdd" ||
      formulaTypeValue === "dateDiff" ||
      formulaTypeValue === "dateSub" ||
      formulaTypeValue === "dateToParts" ? (
        <Form.Item
          label="Unit"
          name="unit"
          rules={[{ required: true, message: "Please select unit!" }]}
        >
          <Select
            disabled={
              Object.keys(field).length > 0 &&
              formulaTypeValue === "dateToParts"
            }
            onChange={(value) => setValueUnit(value)}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {formulaTypeValue === "dateToParts"
              ? unitDatePartType.map((item, idx) => {
                  return <Option value={item.value}>{item.label}</Option>;
                })
              : unitDateType.map((item, idx) => {
                  return <Option value={item.value}>{item.label}</Option>;
                })}
          </Select>
        </Form.Item>
      ) : (
        ""
      )}

      {formulaTypeValue === "dateToParts" && valueUnit === "weekday" && (
        <Form.Item
          name="format_weekday"
          label="Format"
          initialValue={1}
          required
        >
          <Select
            disabled={Object.keys(field).length > 0}
            placeholder="Select format"
            options={[
              {
                value: 1,
                label: "Monday - Sunday",
              },
              {
                value: 2,
                label: "Thứ hai - Chủ nhật",
              },
              {
                value: 3,
                label: "Mon - Sun",
              },
              {
                value: 4,
                label: "Number (0-6)",
              },
            ]}
          />
        </Form.Item>
      )}

      <Form.Item name="required" valuePropName="checked">
        <Checkbox disabled={true}>
          {t("objectLayoutField.requiredInformation")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox disabled>
          {t("objectLayoutField.softRequired")}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox disabled={true}>{t("objectLayoutField.massEdit")}</Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox disabled={true}>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
    </>
  );
};

export default FormulaField;
