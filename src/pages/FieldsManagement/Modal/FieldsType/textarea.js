import { Checkbox, Form, Input, Switch, InputNumber, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import InfoCircle from "assets/icons/common/note.svg";

const TextAreaType = ({ t, form }) => {
  const { field } = useSelector((state) => state.fieldsManagementReducer);
  const [editor, setEditor] = useState(false);
  const [massEdit, setMassEdit] = useState(false);
  const [reruired, setRequired] = useState(false);
  const [soft, setSoft] = useState(false);
  useEffect(() => {
    if (Object.entries(field).length > 0) {
      setEditor(field.is_editor);
      setMassEdit(field.mass_edit);
      setRequired(field.required);
      setSoft(field.soft_required);
    } else {
      setEditor(false);
      setMassEdit(false);
      setRequired(false);
      setSoft(false);
    }
  }, [field]);
  return (
    <>
      <Form.Item
        label={t("objectLayoutField.dataFieldName")}
        name="name"
        rules={[{ required: true, message: "Please input label name!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item label={t("objectLayoutField.placeholder")} name="placeholder">
        <Input />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.autoFill")}
        name="auto_fill"
        rules={[{ required: true, message: "Please input auto fill!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.auto_fill}
        />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.hidden")}
        name="hidden"
        rules={[{ required: true, message: "Please input hidden!" }]}
        valuePropName="checked"
      >
        <Switch
          checkedChildren="Yes"
          unCheckedChildren="No"
          defaultChecked={field.hidden}
        />
      </Form.Item>
      <Form.Item
        label={t("objectLayoutField.maxLength")}
        name="max_length"
        rules={[
          {
            required: true,
            message:
              "Please input max length! Max length must be greater than 0!",
          },
          {
            type: "number",
            min: 1,
            max: 500000,
          },
        ]}
      >
        <InputNumber
          type="number"
          min={1}
          max={500000}
          style={{ width: "100%" }}
        />
      </Form.Item>
      <Form.Item
        label="Rows"
        name="rows"
        rules={[
          { required: true, message: "Please input rows!" },
          {
            type: "number",
            min: 1,
            max: 10,
          },
        ]}
      >
        <InputNumber type="number" min={1} max={10} style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item name="is_editor" valuePropName="checked">
        <Checkbox
          disabled={
            reruired || massEdit || soft || form.getFieldValue("soft_required")
          }
          onChange={(e) => setEditor(e.target.checked)}
        >
          Is editor
        </Checkbox>
      </Form.Item>
      <Form.Item name="required" valuePropName="checked">
        <Checkbox
          disabled={editor}
          onChange={(e) => setRequired(e.target.checked)}
        >
          {t("objectLayoutField.requiredInformation")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="soft_required" valuePropName="checked">
        <Checkbox
          disabled={editor || form.getFieldValue("required")}
          onChange={(e) => setSoft(e.target.checked)}
        >
          {t("objectLayoutField.softRequired")}{" "}
          <Tooltip title={t("objectLayoutField.noteSoft")}>
            <img
              src={InfoCircle}
              alt="info"
              style={{ marginLeft: "4px", marginBottom: "2px" }}
            />
          </Tooltip>
        </Checkbox>
      </Form.Item>
      <Form.Item name="mass_edit" valuePropName="checked">
        <Checkbox
          disabled={editor}
          onChange={(e) => setMassEdit(e.target.checked)}
        >
          {t("objectLayoutField.massEdit")}
        </Checkbox>
      </Form.Item>
      <Form.Item name="readonly" valuePropName="checked">
        <Checkbox>{t("objectLayoutField.readOnly")}</Checkbox>
      </Form.Item>
      <Form.Item name="encrypted" valuePropName="checked">
        <Checkbox disabled={Object.keys(field).length > 0 ? true : false}>
          {t("objectLayoutField.encrypted")}
        </Checkbox>
      </Form.Item>
    </>
  );
};

export default TextAreaType;
