import { Button, Modal } from "antd";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components";
import deleteObjectImg from "../../../assets/images/objectsManagement/deleteObject.png";
import {
  deleteSection,
  deleteField,
} from "../../../redux/slices/fieldsManagement";
const ModalDeleteObject = (props) => {
  const { open, setOpen, ID, title, object_id, fieldID } = props;
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.objectsManagementReducer);
  const { isLoading: isLoadingField } = useSelector(
    (state) => state.fieldsManagementReducer
  );

  useEffect(() => {
    if (isLoading === false && !fieldID) {
      setOpen(false);
    }
    if (isLoadingField === false && fieldID) {
      setOpen(false);
    }
    // eslint-disable-next-line
  }, [isLoading, isLoadingField]);

  return (
    <>
      <CustomModal
        title="Confirm action"
        visible={open}
        onCancel={() => setOpen(false)}
        width={400}
        footer={null}
      >
        <CustomContent>
          <img alt="" src={deleteObjectImg} className="delete-img" />
          <div className="title">Bạn chắc chắn muốn xóa "{title}"</div>
          <div className="decs">
            Sau khi xóa dữ liệu sẽ không hoàn tác. Dữ liệu con cũng sẽ bị xóa
            theo
          </div>
        </CustomContent>
        <CustomFooter>
          <CustomButtonSave
            size="large"
            htmlType="submit"
            loading={fieldID ? isLoadingField : isLoading}
            onClick={() => {
              if (fieldID) {
                dispatch(
                  deleteField({
                    data: {
                      _id: fieldID,
                    },
                    object_id: object_id,
                  })
                );
              } else {
                dispatch(
                  deleteSection({
                    data: {
                      section_id: ID,
                    },
                    object_id: object_id,
                  })
                );
              }
            }}
          >
            Delete
          </CustomButtonSave>
          <CustomButtonCancel size="large" onClick={() => setOpen(false)}>
            Cancel
          </CustomButtonCancel>
        </CustomFooter>
      </CustomModal>
    </>
  );
};

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  width: 80px;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  .delete-img {
    width: 70px;
    margin-bottom: 16px;
  }
  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 18px;
    line-height: 26px;
    /* identical to box height, or 144% */

    /* Character/Color text main */

    color: #2c2c2c;

    /* Inside Auto Layout */

    flex: none;
    order: 0;
    flex-grow: 0;
    margin: 0px 0px;
  }
  .decs {
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 22px;
    /* or 157% */

    text-align: center;

    /* Neutral / 8 */

    color: #595959;

    /* Inside Auto Layout */

    flex: none;
    order: 1;
    flex-grow: 0;
    margin: 0px 0px;
  }
`;

const CustomModal = styled(Modal)`
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
`;
export default ModalDeleteObject;
