import React, { useState, useEffect } from "react";
import { Button, Modal, Form, Select, Checkbox } from "antd";
import styled from "styled-components";
import {
  createField,
  setField,
  updateField,
  updateIsChange,
} from "../../../redux/slices/fieldsManagement";
import { useDispatch, useSelector } from "react-redux";
import TextType from "./FieldsType/text";
import TextAreaType from "./FieldsType/textarea";
import NumberType from "./FieldsType/number";
import DatetimeType from "./FieldsType/datetimeLocal";
import Date from "./FieldsType/date";
import EmailType from "./FieldsType/email";
import SelectType from "./FieldsType/selectType";
import UserType from "./FieldsType/user";
import FileType from "./FieldsType/file";
import DynamicField from "./FieldsType/dynamicField";
import LinkingObject from "./FieldsType/linkingObject";
import FormulaField from "./FieldsType/formulaField";
import LookupField from "./FieldsType/lookup";
import { Notification } from "components/Notification/Noti";
import { useTranslation, withTranslation } from "react-i18next";
import { loadListObjectField } from "redux/slices/objects";
const { Option } = Select;

const ModalAddField = (props) => {
  const { open, setOpen, title } = props;
  const [form] = Form.useForm();
  const dispatch = useDispatch();
  const [, setFormValue] = useState({});
  const [option, setOption] = useState([]);
  const [formula, setFormula] = useState([]);
  const [formula1, setFormula1] = useState([]);
  const { t } = useTranslation();
  const {
    listIDofSection,
    object_id,
    isLoading,
    field,
    sections,
    list_of_field_key,
    objects_related,
    isChange,
  } = useSelector((state) => state.fieldsManagementReducer);

  const { category } = useSelector((state) => state.objectsManagementReducer);

  const [mainObject, setMainObject] = useState({});

  const [isLoggable, setIsLoggable] = useState(true);

  const [isFomula, setIsFomula] = useState(false);

  useEffect(() => {
    if (isFomula) {
      setIsLoggable(false);
    } else {
      setIsLoggable(true);
    }
  }, [isFomula]);

  useEffect(() => {
    if (Object.keys(field).length > 0) {
      setIsLoggable(field.is_loggable);
      if (field.type === "formula") {
        setIsFomula(true);
      } else {
        setIsFomula(false);
      }
    } else {
      setIsLoggable(true);
      setIsFomula(false);
    }
  }, [field]);

  useEffect(() => {
    Object.values(category).forEach((value) => {
      value.forEach((object) => {
        if (object._id === object_id) {
          setMainObject(object);
          return;
        }
      });
    });
  }, [category, object_id]);

  useEffect(() => {
    if (open) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object_id,
          // show_meta_fields: true,
        })
      );
    }
  }, [dispatch, object_id, open]);

  useEffect(() => {
    setFormula1((prev) => {
      return [...prev, ...formula];
    });
  }, [formula]);

  const onFinish = (values) => {
    if (values.name.trim().length === 0) {
      Notification("error", "Please input valid name!");
    } else {
      if (Object.keys(field).length === 0) {
        values.object_id = object_id;
        values.section_id = listIDofSection[title];

        if (values.type === "number" && values?.options_number?.length > 0) {
          values.options_number = values.options_number.map((item, idx) => {
            return parseFloat(item);
          });
        }
        if (
          (values.type === "number" && values.decimal_separator === "") ||
          (values.type === "formula" && values.decimal_separator === "")
        ) {
          values.decimal_separator = 3;
        }
        if (values.type === "select" || values.type === "dynamic-field") {
          let reoderOption = [];
          // eslint-disable-next-line
          option.forEach((item, idx) => {
            let newItem = {
              label: item.text,
              value: item.text,
              active: true,
            };
            reoderOption.push(newItem);
          });
          values.option = [...reoderOption];
        }

        if (values.type === "dynamic-field") {
          // eslint-disable-next-line
          option.forEach((item, idx) => {
            if (!values["list_items"]) {
              values["list_items"] = {};
            }
            values["list_items"][`${item.text}`] = [...values[`${item.text}`]];
            delete values[`${item.text}`];
          });
        }

        if (values.type === "formula") {
          if (values["formula_type"] === "advanceExpression") {
            let formulaUnique = formula1.filter(function (value, index, array) {
              return array.indexOf(value) === index;
            });
            /*eslint-disable-next-line*/
            let finalArr = [];
            formulaUnique.forEach((item, idx) => {
              if (values["expression_string"].includes(item)) {
                finalArr.push(item);
              }
            });
            values["formula"] = [...finalArr];
          }
        }

        if (values.type === "linkingobject") {
          let newObject = {};
          // eslint-disable-next-line
          objects_related.map((item, idx) => {
            newObject[item._id] = [];
            // eslint-disable-next-line
            list_of_field_key.map((field, index) => {
              if (field.objectname === item._id) {
                newObject[item._id].push(field);
              }
            });
          });
          if (newObject[values.objectname].length === 0) {
            values.key = true;
          }
          if (newObject[values.objectname].length > 0) {
            values.key = false;
          }
        }

        if (values.required === true && values.hidden === true) {
          Notification("warning", "Hidden field cannot be required field!");
        } else if (values.required === true && values.readonly === true) {
          Notification("warning", "Required field can not be read only!");
        } else if (values.readonly === true && values.mass_edit === true) {
          Notification("warning", "Mass edit and read-only field cannot true!");
        } else if (values.hidden === true && values.mass_edit === true) {
          Notification("warning", "Mass edit and hidden field cannot true!");
        } else {
          if (
            (values["type"] === "select" && values["option"].length === 0) ||
            (values["type"] === "dynamic-field" &&
              values["option"].length === 0)
          ) {
            Notification("warning", "Please input options!");
          } else {
            dispatch(
              createField({
                data: {
                  ...values,
                  name: values.name.trim(),
                  is_loggable: isLoggable,
                },
                object_id: object_id,
              })
            );
            setFormula([]);
            setFormula1([]);
          }
        }
      }
      if (Object.keys(field).length > 0) {
        values.object_id = object_id;
        values.section_id = listIDofSection[title];
        values._id = field._id;
        if (values.type === "number" && values?.options_number?.length > 0) {
          values.options_number = values.options_number.map((item, idx) => {
            return parseFloat(item);
          });
        }
        if (
          (values.type === "number" && values.decimal_separator === "") ||
          (values.type === "formula" && values.decimal_separator === "")
        ) {
          values.decimal_separator = 3;
        }
        if (values.type === "select" || values.type === "dynamic-field") {
          let reoderOption = [];
          // eslint-disable-next-line
          option.forEach((item, idx) => {
            let newItem = {
              label: item.text,
              value: item.text,
              active: true,
            };
            reoderOption.push(newItem);
          });
          values.option = [...reoderOption];

          values.default_value =
            values.default_value === undefined ? null : values.default_value;
        }

        if (values.type === "formula") {
          if (values["formula_type"] === "advanceExpression") {
            let formulaUnique = formula1.filter(function (value, index, array) {
              return array.indexOf(value) === index;
            });
            /*eslint-disable-next-line*/
            let finalArr = [];
            formulaUnique.forEach((item, idx) => {
              if (values["expression_string"].includes(item)) {
                finalArr.push(item);
              }
            });
            values["formula"] = [...finalArr];
          }
        }

        if (values.type === "dynamic-field") {
          // eslint-disable-next-line
          option.forEach((item, idx) => {
            if (!values["list_items"]) {
              values["list_items"] = {};
            }
            values["list_items"][`${item.text}`] = [...values[`${item.text}`]];
            delete values[`${item.text}`];
          });
        }

        if (values.type === "linkingobject") {
          values.key = field.key;
        }
        if (values.required === true && values.hidden === true) {
          Notification("warning", "Hidden field cannot be required field!");
        } else if (values.required === true && values.readonly === true) {
          Notification("warning", "Required field can not be read only!");
        } else if (values.readonly === true && values.mass_edit === true) {
          Notification("warning", "Mass edit and read-only field cannot true!");
        } else if (values.hidden === true && values.mass_edit === true) {
          Notification("warning", "Mass edit and hidden field cannot true!");
        } else {
          if (
            (values["type"] === "select" && values["option"].length === 0) ||
            (values["type"] === "dynamic-field" &&
              values["option"].length === 0)
          ) {
            Notification("warning", "Please input options!");
          } else
            dispatch(
              updateField({
                data: { ...values, is_loggable: isLoggable },
                object_id: object_id,
              })
            );
          setFormula([]);
          setFormula1([]);
        }
      }
    }
  };

  useEffect(() => {
    if (Object.keys(field).length > 0) {
      if (
        field.type === "formula" &&
        field.formula_type === "advanceExpression"
      ) {
        setFormula1(field.formula);
      }
    }
  }, [field]);

  useEffect(() => {
    if (isLoading === false) {
      setOpen(false);
      dispatch(setField({}));
      form.resetFields();
      form.setFieldsValue({
        type: "text",
        mass_edit: false,
        required: false,
        readonly: false,
        auto_fill: true,
        hidden: false,
      });
    }
    // eslint-disable-next-line
  }, [isLoading]);

  const [component, setComponent] = useState(null);
  const fieldsType = [
    {
      label: "Text",
      value: "text",
    },
    {
      label: "Textarea",
      value: "textarea",
    },
    {
      label: "Number",
      value: "number",
    },
    {
      label: "Datetime-local",
      value: "datetime-local",
    },
    {
      label: "Date",
      value: "date",
    },
    {
      label: "Email",
      value: "email",
    },
    {
      label: "Select",
      value: "select",
    },
    {
      label: "Linking object",
      value: "linkingobject",
    },
    {
      label: "User",
      value: "user",
    },
    {
      label: "File",
      value: "file",
    },
    {
      label: "Dynamic field",
      value: "dynamic-field",
    },
    {
      label: "Formula field",
      value: "formula",
    },
    {
      label: "Look up",
      value: "lookup",
    },
  ];

  useEffect(() => {
    // eslint-disable-next-line
    if (Object.keys(field).length === 0) {
      form.resetFields();
      handleSwitchType("text");
      form.setFieldsValue({
        type: "text",
        mass_edit: false,
        required: false,
        readonly: false,
        auto_fill: true,
        multiple: false,
        hidden: false,
      });
    } else {
      if (field.type === "select" || field.type === "dynamic-field") {
        let newOption = [];
        // eslint-disable-next-line
        field.option.forEach((item, idx) => {
          let newItem = {
            id: idx + 1,
            text: item.label,
          };
          if (field.type === "dynamic-field") {
            form.setFieldsValue({
              [item.label]: field.list_items[`${item.label}`],
            });
          }

          newOption.push(newItem);
        });
        setOption(newOption);
      }
      setFormValue({
        ...field,
        soft_required: field.required || field.soft_required,
      });
      handleSwitchType(field.type);
      form.setFieldsValue({
        ...field,
        soft_required: field.required || field.soft_required,
      });
    }
    // eslint-disable-next-line
  }, [field]);

  useEffect(() => {
    handleSwitchType("text");
    form.setFieldsValue({
      type: "text",
      mass_edit: false,
      required: false,
      readonly: false,
      auto_fill: true,
      multiple: false,
      hidden: false,
    });

    // eslint-disable-next-line
  }, []);

  const handleSwitchType = (type) => {
    form.setFieldsValue({
      mass_edit: false,
      required: false,
      readonly: false,
    });
    switch (type) {
      case "text":
        setComponent(<TextType t={t} form={form} />);
        break;
      case "textarea":
        setComponent(<TextAreaType t={t} form={form} />);
        break;
      case "number":
        setComponent(<NumberType t={t} form={form} />);
        break;
      case "datetime-local":
        setComponent(<DatetimeType t={t} form={form} />);
        break;
      case "date":
        setComponent(<Date t={t} form={form} />);
        break;
      case "email":
        setComponent(<EmailType t={t} form={form} />);
        break;
      case "select":
        setComponent(
          <SelectType option={option} setOption={setOption} form={form} t={t} />
        );
        break;
      case "user":
        setComponent(<UserType t={t} form={form} />);
        break;
      case "file":
        setComponent(<FileType t={t} form={form} />);
        break;
      case "dynamic-field":
        setComponent(
          <DynamicField
            option={option}
            setOption={setOption}
            sections={sections}
            form={form}
            t={t}
          />
        );
        break;
      case "linkingobject":
        setComponent(<LinkingObject form={form} t={t} />);
        break;
      case "formula":
        setComponent(
          <FormulaField
            sections={sections}
            form={form}
            t={t}
            formula={formula}
            setFormula={setFormula}
            object_id={object_id}
          />
        );
        break;
      case "lookup":
        setComponent(<LookupField form={form} t={t} />);
        break;
      default:
        break;
    }
  };

  return (
    <>
      <CustomModal
        title={
          Object.keys(field).length === 0
            ? "Thêm trường thông tin"
            : "Chỉnh sửa trường thông tin"
        }
        open={open}
        onCancel={() => {
          setOpen(false);
          dispatch(setField({}));
          form.resetFields();
          form.setFieldsValue({
            type: "text",
            mass_edit: false,
            required: false,
            readonly: false,
            auto_fill: true,
            hidden: false,
          });
        }}
        width={700}
        footer={false}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 18 }}
          onFinish={onFinish}
          onFinishFailed={() => {}}
          autoComplete="off"
          initialValues={{
            max_length: 10000,
            rows: 3,
            step: 1,
            decimal_separator: 3,
          }}
          onValuesChange={(value, values) => {
            if (Object.keys(value)[0] === "required") {
              dispatch(updateIsChange(!isChange));
              if (Object.keys(values)[0]) {
                form.setFieldsValue({ soft_required: true });
              }
            }
            setFormValue(values);
            if (values.type === "formula") {
              setIsFomula(true);
            } else {
              setIsFomula(false);
            }
          }}
        >
          <Form.Item label="Type" name="type">
            <Select
              onChange={(e) => {
                handleSwitchType(e);
              }}
              disabled={Object.keys(field).length > 0 ? true : false}
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.children) {
                  return option.children
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                } else if (option.label) {
                  return option.label
                    .toLowerCase()
                    .indexOf(inputValue.toLowerCase()) >= 0
                    ? true
                    : false;
                }
              }}
            >
              {fieldsType.map((item, idx) => {
                return <Option value={item.value}>{item.label}</Option>;
              })}
            </Select>
          </Form.Item>
          {component}

          {mainObject.is_loggable && (
            <CustomCheckbox
              checked={isLoggable}
              disabled={isFomula}
              onChange={(e) => setIsLoggable(e.target.checked)}
            >
              {t("object.saveLog")}
            </CustomCheckbox>
          )}
          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              loading={isLoading}
            >
              {t("common.save")}
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                setOpen(false);
                dispatch(setField({}));
                form.resetFields();
                form.setFieldsValue({
                  type: "text",
                  mass_edit: false,
                  required: false,
                  readonly: false,
                  auto_fill: true,
                  hidden: false,
                });
              }}
            >
              {t("common.cancel")}
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

const CustomModal = styled(Modal)`
  .ant-checkbox-wrapper {
    display: flex;
    margin-bottom: 4px;
  }
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-modal-footer {
    border-top: none;
  }
  .ant-modal-content {
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    background-color: #f2f4f5;

    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    color: #000000;
  }
  .ant-form-item-label {
    text-align: left;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
  }
  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomCheckbox = styled(Checkbox)`
  .ant-checkbox-wrapper {
    display: flex;
    margin-bottom: 4px;
  }
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner {
    border-color: ${(props) => props.theme.main};
  }
`;

export default withTranslation()(ModalAddField);
