import {
  Breadcrumb,
  Col,
  Form,
  Input,
  Row,
  DatePicker,
  Button,
  Select,
} from "antd";
import { useForm } from "antd/lib/form/Form";
import Editor from "components/Editor/Editor2";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { META_DATA } from "constants/constants";
import moment from "moment";
import React, { useEffect, useState, useRef } from "react";
import { useLocation, useNavigate } from "react-router";
import {
  deleteArticle,
  getArticleById,
  getOptionCategory,
  unmountArticle,
  updateFieldArticle,
} from "redux/slices/articles";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { getKnowledgeBaseSetting } from "redux/slices/knowledgeBaseSetting";
import {
  createRecord,
  loadListObjectField,
  updateRecord,
} from "redux/slices/objects";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
const { RangePicker } = DatePicker;
const { Option } = Select;
const Articles = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { search } = useLocation();
  const query = new URLSearchParams(search);
  const object_id = query.get("object_id");
  const id = query.get("id");
  const [form] = useForm();

  const editorJodit = useRef(null);

  const { editArticle, optionCategory } = useSelector(
    (state) => state.articlesReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );
  const { objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const [dataDelete, setDataDelete] = useState({});
  const { userDetail } = useSelector((state) => state.userReducer);
  const [sectionOptions, setSectionOptions] = useState([]);
  const dateFormat = "YYYY/MM/DD";
  const { title, body, start_date, end_date, section_name, category_name } =
    editArticle;

  const handleNameRoute = () => {
    let name = "";
    // eslint-disable-next-line
    Object.entries(objectCategory).forEach(([key, value], idx) => {
      // eslint-disable-next-line
      value.map((object, index) => {
        if (object._id === editKnowledgeBaseSetting.object_id) {
          name = object.Name;
        }
      });
    });
    return name;
  };

  const submit = () => {
    let temp = [];

    dispatch(
      updateFieldArticle({
        key: "body",
        value: editorJodit.current.value,
      })
    );

    // eslint-disable-next-line
    Object.keys(editKnowledgeBaseSetting).forEach((item) => {
      if (item === "body") {
        if (
          editorJodit.current.value &&
          item !== "_id" &&
          META_DATA.filter((x) => x.ID === item).length === 0
        ) {
          temp.push({
            id_field: editKnowledgeBaseSetting[item],
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: editorJodit.current.value,
          });
        }
      } else {
        if (
          editArticle[item] &&
          item !== "_id" &&
          META_DATA.filter((x) => x.ID === item).length === 0
        ) {
          temp.push({
            id_field: editKnowledgeBaseSetting[item],
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: editArticle[item],
          });
        }
      }
    });

    if (editArticle._id) {
      let data = {
        id: editArticle._id,
        object_id: editKnowledgeBaseSetting.object_id,
        owner: userDetail._id,
        data: [...temp],
      };
      dispatch(
        updateRecord({
          data: {
            ...data,
          },
          load: {
            object_id: object_id,
            search_with: {
              meta: [],
              data: "",
            },
          },
          navigate,
        })
      );
    } else {
      let data = {
        object_id: editKnowledgeBaseSetting.object_id,
        owner: userDetail._id,
        data: [...temp],
      };
      dispatch(
        createRecord({
          data: data,
          navigate,
        })
      );
    }
  };

  useEffect(() => {
    if (editKnowledgeBaseSetting.object_id) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: editKnowledgeBaseSetting.object_id,
        })
      );
    }
    // eslint-disable-next-line
  }, [editKnowledgeBaseSetting.object_id]);
  useEffect(() => {
    if (
      listObjectField.length > 0 &&
      Object.keys(editKnowledgeBaseSetting).length > 0
    ) {
      listObjectField[0].main_object &&
        // eslint-disable-next-line
        listObjectField[0].main_object.sections.map((section) => {
          let sectionOptions =
            section.fields.filter(
              (x) => x.field_id === editKnowledgeBaseSetting.section_name
            ).length > 0
              ? section.fields.filter(
                  (x) => x.field_id === editKnowledgeBaseSetting.section_name
                )[0].option
              : [];
          setSectionOptions(sectionOptions);
        });
    }
  }, [listObjectField, editKnowledgeBaseSetting]);
  useEffect(() => {
    dispatch(getKnowledgeBaseSetting());
    if (id) {
      dispatch(
        getArticleById({
          data: {
            id: id,
            object_id: editKnowledgeBaseSetting.object_id,
          },
          navigate,
        })
      );
    }
    // eslint-disable-next-line
  }, [id]);
  useEffect(() => {
    if (section_name.length > 0) {
      dispatch(
        getOptionCategory({
          Object_ID: editKnowledgeBaseSetting.object_id,
          Source_ID: editKnowledgeBaseSetting.section_name,
          Target_ID: editKnowledgeBaseSetting.category_name,
          Value: section_name,
        })
      );
    }
    // eslint-disable-next-line
  }, [section_name]);
  useEffect(
    () => () => {
      dispatch(unmountArticle());
    },
    // eslint-disable-next-line
    []
  );
  return (
    <CustomContent>
      <Breadcrumb>
        <Breadcrumb.Item
          onClick={() =>
            navigate(
              "/knowledge-base-view?object_id=" +
                editKnowledgeBaseSetting.object_id
            )
          }
        >
          {handleNameRoute()}
        </Breadcrumb.Item>
        <BreadcrumbItem>{id ? "Cập nhật bài viết" : "Thêm mới"}</BreadcrumbItem>
      </Breadcrumb>
      <Form
        name={"create-article"}
        style={{ height: "100%" }}
        onFinish={() => {
          submit();
        }}
        form={form}
      >
        <Row style={{ height: "100%" }}>
          <Col span={14}>
            <Row>
              <Col span={24}>
                <Form.Item
                  name={"title"}
                  valuePropName={title}
                  rules={[
                    {
                      validator: (rule, value = title, cb) => {
                        title.length === 0
                          ? cb("Trường này là bắt buộc")
                          : cb();
                      },
                      required: true,
                    },
                  ]}
                >
                  <Input
                    placeholder={"Enter title here"}
                    onChange={(e) => {
                      dispatch(
                        updateFieldArticle({
                          key: "title",
                          value: e.target.value,
                        })
                      );
                    }}
                    value={title}
                  />
                </Form.Item>
              </Col>
            </Row>
            <Row style={{ height: "100%" }}>
              <Col span={24} style={{ height: "100%" }}>
                <Form.Item
                  style={{ height: "100%" }}
                  name={"body"}
                  rules={[
                    {
                      validator: (
                        rule,
                        value = editorJodit.current.value,
                        cb
                      ) => {
                        editorJodit.current.value.length === 0
                          ? cb("Trường này là bắt buộc")
                          : cb();
                      },
                      required: true,
                    },
                  ]}
                >
                  <Editor
                    editorJodit={editorJodit}
                    content={body}
                    showAppend={false}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={10} style={{ paddingLeft: "1rem" }}>
            <Row
              style={{
                backgroundColor: "white",
                padding: "1rem",
                border: "1px solid #D9D9D9",
              }}
            >
              <Col span={24}>
                <Row>
                  <Col span={24}>
                    <span> General </span>
                  </Col>
                </Row>
                <hr />
                <Row
                  style={{
                    padding: "0.5rem 0rem",
                  }}
                >
                  <Col span={4}>
                    <span> Time apply</span>
                  </Col>
                  <Col span={20}>
                    <Form.Item
                      style={{ margin: "0px" }}
                      name={"start_date"}
                      valuePropName={start_date}
                    >
                      <RangePicker
                        placement={"topRight"}
                        value={[
                          start_date && start_date.length > 0
                            ? moment(start_date, dateFormat)
                            : null,
                          end_date && end_date.length > 0
                            ? moment(end_date, dateFormat)
                            : null,
                        ]}
                        format={dateFormat}
                        onChange={(e) => {
                          if (e[0]) {
                            dispatch(
                              updateFieldArticle({
                                key: "start_date",
                                value: moment(e[0]).format("YYYY-MM-DD"),
                              })
                            );
                          }
                          if (e[1]) {
                            dispatch(
                              updateFieldArticle({
                                key: "end_date",
                                value: moment(e[1]).format("YYYY-MM-DD"),
                              })
                            );
                          }
                        }}
                      />
                    </Form.Item>
                  </Col>
                </Row>
                <hr />
                <Row justify={"space-between"}>
                  <Col span={4}>
                    <Button
                      disabled={id ? false : true}
                      onClick={() => {
                        setDataDelete({
                          data: {
                            id: id,
                            object_id: object_id,
                          },
                          navigate,
                        });
                        dispatch(setShowModalConfirmDelete(true));
                      }}
                    >
                      Move to trash
                    </Button>
                  </Col>
                  <Col span={4}>
                    <Button
                      onClick={() => {
                        form.submit();
                      }}
                    >
                      Publish
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
            <Row
              style={{
                backgroundColor: "white",
                padding: "1rem",
                border: "1px solid #D9D9D9",
                marginTop: "1rem",
              }}
            >
              <Col span={24}>
                <Row>
                  <Col span={4}>
                    <span>
                      {" "}
                      Section <span style={{ color: "red" }}>*</span>{" "}
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item
                      name={"section_name"}
                      valuePropName={section_name}
                      // rules={[
                      //   {
                      //     validator: (rule, value = section_name, cb) => {
                      //       section_name.length === 0
                      //         ? cb("Trường này là bắt buộc")
                      //         : cb();
                      //     },
                      //     required: true,
                      //   },
                      // ]}
                    >
                      <Select
                        value={section_name}
                        onChange={(e) => {
                          dispatch(
                            updateFieldArticle({
                              key: "section_name",
                              value: e,
                            })
                          );
                          dispatch(
                            updateFieldArticle({
                              key: "category_name",
                              value: "",
                            })
                          );
                        }}
                      >
                        {sectionOptions &&
                          sectionOptions.map((option) => {
                            return (
                              <Option value={option.value}>
                                {option.label}
                              </Option>
                            );
                          })}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col span={4}>
                    <span>
                      {" "}
                      Category <span style={{ color: "red" }}>*</span>{" "}
                    </span>
                  </Col>
                  <Col span={20}>
                    <Form.Item
                      style={{ margin: "0px" }}
                      name={"category_name"}
                      valuePropName={category_name}
                      // rules={[
                      //   {
                      //     validator: (rule, value = category_name, cb) => {
                      //       category_name.length === 0
                      //         ? cb("Trường này là bắt buộc")
                      //         : cb();
                      //     },
                      //     required: true,
                      //   },
                      // ]}
                    >
                      <Select
                        value={category_name}
                        onChange={(e) => {
                          dispatch(
                            updateFieldArticle({
                              key: "category_name",
                              value: e,
                            })
                          );
                        }}
                      >
                        {optionCategory.map((item) => {
                          return (
                            <Option value={item.value}>{item.label}</Option>
                          );
                        })}
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
      <ModalConfimDelete
        title={"bài viết này"}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteArticle}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </CustomContent>
  );
};

export default Articles;

const CustomContent = styled.div`
  padding: 1rem;
  height: 100%;
  .ant-form-item-control-input {
    height: 80%;
    align-items: inherit;
  }
  .sun-editor-editable {
    height: auto;
  }
  .ant-picker-range {
    width: 100%;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
