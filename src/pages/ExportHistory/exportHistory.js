import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Empty from "antd/lib/empty";
import Menu from "antd/lib/menu";
import Breadcrumb from "antd/lib/breadcrumb";
import Spin from "antd/lib/spin";
import Tooltip from "antd/lib/tooltip";

import Empty_folder from "assets/images/reports/empty-folder.webp";
import Refresh from "assets/icons/exportHistory/Refresh.svg";
import Delete from "assets/icons/common/delete.svg";
import CSV from "assets/icons/exportHistory/CSV.svg";
import PDF from "assets/icons/exportHistory/PDF.svg";
import XLSX from "assets/icons/exportHistory/XLSX.svg";

import {
  loadAllExportHistory,
  loadExportHistory,
  deleteExportHistory,
  setActive,
} from "redux/slices/exportHistory";
import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { BE_URL } from "constants/constants";

function ExportHistory(props) {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { allExportHistory, loading, defaultActive } = useSelector(
    (state) => state.exportHistoryReducer
  );

  const [detailsExport, setDetailsExport] = useState({});
  const [timePresent, setTimePresent] = useState("");

  const [dataDelete, setDataDelete] = useState("");

  const _onSelectReport = (value) => {
    dispatch(setActive(value.key));
  };

  useEffect(() => {
    if (allExportHistory.length > 0) {
      setDetailsExport(allExportHistory[parseInt(defaultActive)]);
    }
  }, [allExportHistory, defaultActive]);

  const onRefreshTime = () => {
    const today = new Date();
    const time = today.getHours() + ":" + today.getMinutes();
    setTimePresent(time);
  };

  const _onRefresh = () => {
    onRefreshTime();
    dispatch(
      loadExportHistory({
        export_history_id: detailsExport._id,
      })
    );
  };

  const _onDelete = () => {
    setDataDelete({
      export_history_id: detailsExport._id,
    });
    dispatch(setShowModalConfirmDelete(true));
  };

  useEffect(() => {
    onRefreshTime();
  }, []);

  useEffect(() => {
    dispatch(loadAllExportHistory());
  }, [dispatch]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/report#my_folders")}>
          Report
        </Breadcrumb.Item>
        <BreadcrumbItem>Export history</BreadcrumbItem>
      </Breadcrumb>

      {allExportHistory.length === 0 ? (
        <Empty
          image={Empty_folder}
          description={
            <p>
              Hiện chưa có <span>Export history</span>
            </p>
          }
        ></Empty>
      ) : (
        <Wrap>
          <MenuCustom onClick={_onSelectReport} selectedKeys={defaultActive}>
            {allExportHistory.map((item, idx) => {
              return (
                <Menu.Item key={idx}>
                  <span>{item.name}</span>
                </Menu.Item>
              );
            })}
          </MenuCustom>
          {Object.keys(detailsExport).length > 0 && (
            <DetailsExport>
              <WrapTitle>
                {detailsExport.file_type === "xlsx" ? (
                  <img src={XLSX} alt="xlsx" />
                ) : detailsExport.file_type === "csv" ? (
                  <img src={CSV} alt="csv" />
                ) : (
                  <img src={PDF} alt="pdf" />
                )}
                <Title>
                  <span>{detailsExport.name}</span>
                  <SubTitle>
                    <span>Refreshed at {timePresent}</span>
                    {detailsExport.is_running && (
                      <>
                        {loading ? (
                          <Spin />
                        ) : (
                          <img
                            onClick={_onRefresh}
                            src={Refresh}
                            alt="refresh"
                          />
                        )}
                      </>
                    )}
                    <p>•</p>

                    {detailsExport.is_running ? (
                      <p>
                        Exporting {detailsExport.filenames.length}/
                        {detailsExport.total_files}
                      </p>
                    ) : (
                      <p>Completed</p>
                    )}
                  </SubTitle>
                </Title>
                <DeleteHistory>
                  <img onClick={_onDelete} src={Delete} alt="delete" />
                </DeleteHistory>
              </WrapTitle>
              {detailsExport.filenames.length > 0 && (
                <ListFile>
                  {detailsExport.filenames.map((file, idx) => {
                    return (
                      <a href={`${BE_URL}${file}`}>
                        <Tooltip title="Download now">
                          {detailsExport.name}.{detailsExport.file_type}_
                          {idx + 1}
                        </Tooltip>
                      </a>
                    );
                  })}
                </ListFile>
              )}
            </DetailsExport>
          )}
        </Wrap>
      )}

      <ModalConfirmDelete
        title={""}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={deleteExportHistory}
        dataDelete={dataDelete}
        isLoading={false}
      />
    </Wrapper>
  );
}

export default ExportHistory;

const Wrapper = styled.div`
  padding: 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-empty-description {
    span {
      color: ${(props) => props.theme.main};
      font-size: 16px;
    }
  }

  .ant-empty-footer {
    display: flex;
    justify-content: center;
  }

  .ant-empty {
    margin-top: 16px;
  }

  .ant-spin {
    margin: 0 16px;
  }

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }

  img {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const Wrap = styled.div`
  display: flex;
  margin-top: 8px;
`;

const MenuCustom = styled(Menu)`
  width: 42.5rem;
  box-shadow: inset 0px -1px 0px #f0f0f0;
  border-radius: 0px 0px 0px 10px;
  height: fit-content;

  .ant-menu-item {
    height: 46px !important;
    line-height: 46px !important;
    border-bottom: 1px solid #f0f0f0;
    margin: 0 !important;
    transition: border-color 0.5s ease;
  }

  .ant-menu-item-selected {
    background: #f0f0f0 !important;
    color: #2c2c2c;
  }

  .ant-menu-item {
    :hover {
      color: #2c2c2c;
      box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
      border: 1px solid #d9d9d9;
    }
  }

  .ant-menu-title-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

const DetailsExport = styled.div`
  padding: 24px;
  background: #f5f5f5;
  border: 1px solid #ececec;
  flex: 1;
`;

const WrapTitle = styled.div`
  border-bottom: 1px solid #ececec;
  padding-bottom: 16px;
  display: flex;
`;

const Title = styled.div`
  padding-left: 24px;

  span {
    font-family: var(--roboto-500);
    font-size: 22px;
    color: #2c2c2c;
  }
`;

const SubTitle = styled.div`
  display: flex;
  align-items: center;

  img {
    margin-left: 16px;
  }

  span {
    font-family: var(--roboto-400);
    font-size: 16px;
  }

  p {
    font-size: 14px;
    color: #6b6b6b;
    margin-bottom: 0;
    margin-left: 16px;
  }
`;

const ListFile = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 24px;
  padding-left: 66px;

  a {
    color: #1267fb;

    :hover {
      text-decoration-line: underline;
    }
  }
`;

const DeleteHistory = styled.div`
  margin-left: auto;
`;
