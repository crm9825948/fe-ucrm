import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useLocation } from "react-router-dom";
import styled from "styled-components/macro";
import { useTranslation, withTranslation } from "react-i18next";

import { Button, Col, Input, Row, Form } from "antd";

import ForgotPassword from "assets/images/login/forgotPassword.jpg";
import ChangePassword from "assets/images/login/changePassword.png";
import backgroundImage from "assets/images/login/login-bg.webp";
import loginImage from "assets/images/login/logo.jpg";
import { RANDOM_VARIABLE } from "constants/constants";
import "index.scss";

import { forgotPassword, resetPassword } from "redux/slices/authenticated";

const ChangePass = (props) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const jwt = require("jsonwebtoken");
  const url = window.location.search;
  const newURL = new URLSearchParams(url);
  const token = newURL.get("token");
  const { isLoading } = useSelector((state) => state.authenticatedReducer);

  const [email, setEmail] = useState("");

  const onFinish = (values) => {
    if (pathname.split("/").includes("forgot-password")) {
      dispatch(
        forgotPassword({
          email: values.trim(),
        })
      );
    } else {
      dispatch(
        resetPassword({
          reset_token: token,
          password: jwt.sign({ password: values.new_pass }, RANDOM_VARIABLE, {
            algorithm: "HS256",
          }),
          confirm_password: jwt.sign(
            { password: values.verify_pass },
            RANDOM_VARIABLE,
            {
              algorithm: "HS256",
            }
          ),
        })
      );
    }
  };

  return (
    <PageWrapper>
      <ContentWrapper>
        <Row>
          <Col span={10}>
            <img
              src={
                pathname.split("/").includes("forgot-password")
                  ? ForgotPassword
                  : ChangePassword
              }
              alt="welcome-back"
              className="welcome_back"
            />
          </Col>
          <Col span={14}>
            {pathname.split("/").includes("forgot-password") ? (
              <FormWrapper>
                <img src={loginImage} alt="" className="logo" />
                <div
                  style={{
                    display: "flex",
                    justifyContent: "left",
                    width: "90%",
                    fontStyle: "normal",
                    fontWeight: "bold",
                    fontSize: "16px",
                    lineHeight: "22px",
                    /* identical to box height, or 138% */
                    textAlign: "center",
                    color: "#000",
                    marginBottom: "8px",
                  }}
                >
                  <span>Email của bạn</span>
                </div>
                <Input
                  type="email"
                  style={{ width: "90%" }}
                  size="large"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                />
                <CustomButton
                  loading={isLoading}
                  onClick={() => {
                    onFinish(email);
                  }}
                  disabled={email.length === 0 ? true : false}
                >
                  Khôi phục lại mật khẩu
                </CustomButton>
                <div
                  className="backToLogin"
                  onClick={() => {
                    navigate("/login");
                  }}
                >
                  Về trang đăng nhập
                </div>
              </FormWrapper>
            ) : (
              <FormWrapper>
                <img src={loginImage} alt="" className="logo" />
                <Form
                  onFinish={(e) => onFinish(e)}
                  layout="vertical"
                  autoComplete="off"
                  requiredMark={false}
                >
                  <Form.Item
                    label="Mật khẩu mới"
                    name="new_pass"
                    rules={[
                      {
                        required: true,
                        message: "Please input your new password!",
                      },
                      () => ({
                        validator(_, value) {
                          if (value) {
                            if (value.length < 8) {
                              return Promise.reject(
                                new Error(t("user.errorPass"))
                              );
                            } else if (!value.match(/(?=.*?[a-z])/)) {
                              return Promise.reject(
                                new Error(t("user.errorPass2"))
                              );
                            } else if (!value.match(/(?=.*?[0-9])/)) {
                              return Promise.reject(
                                new Error(t("user.errorPass5"))
                              );
                            } else if (
                              !value.match(
                                /(?=.*?[,./=+<({})>[|!@#$%^&*?_-])/
                              ) &&
                              !value.includes("]") &&
                              !value.includes("\\")
                            ) {
                              return Promise.reject(
                                new Error(t("user.errorPass4"))
                              );
                            } else if (!value.match(/(?=.*?[A-Z])/)) {
                              return Promise.reject(
                                new Error(t("user.errorPass3"))
                              );
                            } else return Promise.resolve();
                          }
                          return Promise.resolve();
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      placeholder="Nhập mật khẩu mới"
                      style={{ height: "54px" }}
                    />
                  </Form.Item>

                  <Form.Item
                    label="Nhập lại mật khẩu mới"
                    name="verify_pass"
                    rules={[
                      {
                        required: true,
                        message: "Please input your verify password!",
                      },
                      ({ getFieldValue }) => ({
                        validator(_, value) {
                          if (!value || getFieldValue("new_pass") === value) {
                            return Promise.resolve();
                          }
                          return Promise.reject(
                            new Error(t("user.errorPass6"))
                          );
                        },
                      }),
                    ]}
                  >
                    <Input.Password
                      placeholder="Nhập lại mật khẩu mới"
                      style={{ height: "54px" }}
                    />
                  </Form.Item>
                  <Form.Item>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{
                        backgroundColor: "#20a2a2",
                        borderColor: "#20a2a2",
                        height: "54px",
                      }}
                      loading={isLoading}
                    >
                      Save
                    </Button>
                  </Form.Item>
                </Form>
              </FormWrapper>
            )}
          </Col>
        </Row>
      </ContentWrapper>
    </PageWrapper>
  );
};

export default withTranslation()(ChangePass);

const PageWrapper = styled.div`
  height: 100vh;
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  .forgot-password {
    color: #20a2a2;
    cursor: pointer;
    :hover {
      text-decoration: underline;
    }
  }
`;

const CustomButton = styled(Button)`
  display: flex;
  margin-top: 48px;
  justify-content: center;
  align-items: center;
  padding: 26px;
  width: 90%;
  background: #20a2a2;
  font-family: var(--roboto-700);
  font-size: 16px;
  color: #fff;
`;

const ContentWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 926px;
  height: 580px;
  background-color: #fff;
  /* display: flex;
  justify-content: flex-start;
  align-items: center; */
  .ant-btn:hover {
    border-color: #20a2a2;
    color: #20a2a2;
  }
  .backToLogin {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */

    text-align: center;

    /* Blue 2 */

    color: #016eff;
    margin-top: 16px;
    :hover {
      cursor: pointer;
      text-decoration: underline;
    }
  }
  .ant-input-lg {
    height: 66px;
  }
  .ant-btn:active {
    color: #fff;
    background-color: #1c8f8f !important;
    border-color: #1c8f8f !important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: #1c8f8f !important;
    border-color: #1c8f8f !important;
  }
  .welcome_back {
    width: 386px;
  }

  @media screen and (max-width: 1199px) {
    width: 694.5px;
    height: 435px;
    .welcome_back {
      width: 289.5px;
    }
  }
`;

const FormWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 100%;
  .logo {
    width: 185px;
    height: 72px;
    margin-top: 127px;
    margin-bottom: 24px;
  }
  .login_title {
    font-family: var(--roboto-700);
    font-size: 28px;
    line-height: 33px;
    /* identical to box height */

    text-align: center;
    letter-spacing: 0.04em;
    font-variant: small-caps;
    margin-bottom: 16px;
    /* text - main */

    color: #545454;
  }
  .ant-form {
    width: 466px;
  }
  .input__username {
    margin-bottom: 6px;
  }
  .remember_me__container {
    display: flex;
    justify-content: flex-start;
    width: 100%;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    /* Character / Title .85 */

    color: rgba(0, 0, 0, 0.85);
    .ant-checkbox-checked .ant-checkbox-inner {
      background-color: #20a2a2;
      border-color: #20a2a2;
    }
  }
  .ant-btn-primary {
    width: 100%;
    border-color: #20a2a2;
    background-color: #20a2a2;
  }
  .ant-btn {
    height: 42px;
  }

  @media screen and (max-width: 1199px) {
    .logo {
      width: 138.75px;
      height: 54px;
      margin-top: 28.8px;
      margin-bottom: 18px;
    }
    .login_title {
      font-family: var(--roboto-700);
      font-size: 21px;
      line-height: 24.5px;
      /* identical to box height */

      text-align: center;
      letter-spacing: 0.04em;
      font-variant: small-caps;
      margin-bottom: 12px;
      /* text - main */

      color: #545454;
    }
    .ant-form {
      width: 349.5px;
    }
    .input__username {
      margin-bottom: 4.5px;
    }
    .remember_me__container {
      display: flex;
      justify-content: flex-start;
      width: 100%;
      font-family: var(--roboto-400);
      font-size: 12px;
      line-height: 16.5px;
      margin-bottom: 0;
      /* identical to box height, or 137% */

      /* Character / Title .85 */

      color: rgba(0, 0, 0, 0.85);
      .ant-checkbox-checked .ant-checkbox-inner {
        background-color: #20a2a2;
        border-color: #20a2a2;
      }
    }
    .ant-btn-primary {
      width: 100%;
      border-color: #20a2a2;
      background-color: #20a2a2;
    }
    .ant-btn {
      height: 31.5px;
    }
  }
`;
