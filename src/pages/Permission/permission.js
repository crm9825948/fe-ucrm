import { Button } from "antd";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import { FE_URL } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadRootPermission } from "redux/slices/tenants";
import styled from "styled-components";
import ModalAdd from "./modalAdd";
import PermissionItem from "./permissionItem";

const Permission = () => {
  const dispatch = useDispatch();
  const { rootPermission } = useSelector((state) => state.tenantsReducer);
  const [open, setOpen] = useState(false);
  const { userDetail } = useSelector((state) => state.userReducer);
  useEffect(() => {
    dispatch(loadRootPermission());
  }, [dispatch]);

  useEffect(() => {
    if (
      userDetail &&
      userDetail.permission &&
      userDetail.permission.role === "normal"
    ) {
      window.open(FE_URL + "/tenants", "_self");
    }
  }, [userDetail]);

  return (
    <Wrapper>
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "right",
          marginBottom: "16px",
        }}
      >
        {userDetail &&
        userDetail.permission &&
        userDetail.permission.create_tenant ? (
          <CustomButtonAddRecord
            size="large"
            onClick={() => {
              setOpen(true);
            }}
          >
            <img alt="" src={plusIcon} />
            Add user
          </CustomButtonAddRecord>
        ) : (
          ""
        )}
      </div>

      <div className="container">
        <table>
          <thead>
            <tr>
              <th>User</th>
              <th>View</th>
              <th>Create</th>
              <th>Update</th>
              <th>Delete</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {rootPermission.map((item, idx) => {
              return <PermissionItem item={item} />;
            })}
          </tbody>
        </table>
      </div>
      <ModalAdd isModalVisible={open} setIsModalVisible={setOpen} />
    </Wrapper>
  );
};

export default Permission;

const Wrapper = styled.div`
  padding: 24px;
  .container {
    padding: 24px;
    background-color: white;
  }
  table {
    border-collapse: collapse;
    width: 100%;
  }
  td {
    border-bottom: 1px solid #ededed;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    text-align: left;
    padding: 8px;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 130%;
    /* identical to box height, or 21px */

    /* Character/Body text */

    color: #2c2c2c;
  }
  th {
    border-bottom: 1px solid #ededed;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    text-align: left;
    padding: 8px;
    /* background-color: #fff; */
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 138% */

    color: #2c2c2c;
    background: #fafafa;
    box-shadow: -18px 0px 0px -17px rgba(0, 0, 0, 0.06);
    /* Character/Body text */
  }
  .ant-checkbox-checked {
    background-color: ${(props) => props.theme.main};
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #2c2c2c !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
