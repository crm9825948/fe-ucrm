import { Checkbox, Input } from "antd";
import changePass from "assets/icons/common/key.png";
import img from "assets/icons/common/confirm.png";
import deleteIcon from "assets/icons/common/delete.png";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { deletePermission, updateRoot } from "redux/slices/tenants";
import ModalChangePass from "pages/Users/ModalChangePass";

export const jwt = require("jsonwebtoken");

// const { Option } = Select;

const PermissionItem = (props) => {
  const { item } = props;
  const [showModalChangePass, setShowModalChangePass] = useState(false);

  let userDetail = jwt.decode(localStorage.getItem("setting_accessToken"));
  const { userDetail: userDetailData } = useSelector(
    (state) => state.userReducer
  );
  const dispatch = useDispatch();
  const [edit, setEdit] = useState(false);
  const [value, setValue] = useState({});
  const handleUpdateEmail = (item) => {
    dispatch(
      updateRoot({
        _id: item._id,
        role: item.role,
        update_tenant: item.update_tenant,
        create_tenant: item.create_tenant,
        delete_tenant: item.delete_tenant,
        email: item.email,
        view_tenant: item.view_tenant,
      })
    );
    setEdit(false);
  };
  const [open, setOpen] = useState(false);

  return (
    <tr>
      <td onDoubleClick={() => setEdit(true)}>
        {edit &&
        userDetail &&
        userDetail.permission &&
        userDetail.permission.update_tenant ? (
          <Input
            type={"email"}
            defaultValue={item.email}
            onPressEnter={() => {
              handleUpdateEmail(value);
            }}
            onBlur={() => {
              setValue({
                _id: item._id,
                role: item.role,
                update_tenant: item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: item.delete_tenant,
                email: item.email,
                view_tenant: item.view_tenant,
              });
              setEdit(false);
            }}
            onChange={(e) => {
              setValue({
                _id: item._id,
                role: item.role,
                update_tenant: item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: item.delete_tenant,
                email: e.target.value,
                view_tenant: item.view_tenant,
              });
            }}
          />
        ) : (
          item.email
        )}
      </td>
      {/* <td>
        <Select
          value={item.role}
          disabled={userDetail.Email === item.email}
          style={{ width: "100%" }}
          onChange={(e) => {
            dispatch(
              updateRoot({
                _id: item._id,
                role: e,
                update_tenant: item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: !item.delete_tenant,
                email: item.email,
                view_tenant: item.view_tenant,
              })
            );
          }}
        >
          <Option value="normal">Operation user</Option>
          <Option value="super">System admin</Option>
        </Select>
      </td> */}
      <td>
        <Checkbox
          disabled={true}
          checked={item.view_tenant}
          onClick={() => {
            dispatch(
              updateRoot({
                _id: item._id,
                role: item.role,
                update_tenant: item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: item.delete_tenant,
                email: item.email,
                view_tenant: !item.view_tenant,
              })
            );
          }}
        />
      </td>
      <td>
        <Checkbox
          disabled={
            userDetail.Email === item.email ||
            (userDetailData &&
              userDetailData.permission &&
              userDetailData.permission.update_tenant === false)
          }
          checked={item.create_tenant}
          onClick={() => {
            dispatch(
              updateRoot({
                _id: item._id,
                role: item.role,
                update_tenant: item.update_tenant,
                create_tenant: !item.create_tenant,
                delete_tenant: item.delete_tenant,
                email: item.email,
                view_tenant: item.view_tenant,
              })
            );
          }}
        />
      </td>
      <td>
        <Checkbox
          disabled={
            userDetail.Email === item.email ||
            (userDetailData &&
              userDetailData.permission &&
              userDetailData.permission.update_tenant === false)
          }
          checked={item.update_tenant}
          onClick={() => {
            dispatch(
              updateRoot({
                _id: item._id,
                role: item.role,
                update_tenant: !item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: item.delete_tenant,
                email: item.email,
                view_tenant: item.view_tenant,
              })
            );
          }}
        />
      </td>
      <td>
        <Checkbox
          disabled={
            userDetail.Email === item.email ||
            (userDetailData &&
              userDetailData.permission &&
              userDetailData.permission.update_tenant === false)
          }
          checked={item.delete_tenant}
          onClick={() => {
            dispatch(
              updateRoot({
                _id: item._id,
                role: item.role,
                update_tenant: item.update_tenant,
                create_tenant: item.create_tenant,
                delete_tenant: !item.delete_tenant,
                email: item.email,
                view_tenant: item.view_tenant,
              })
            );
          }}
        />
      </td>

      <td>
        {userDetailData &&
        userDetailData.permission &&
        userDetailData.permission.update_tenant === false &&
        userDetail.Email !== item.email ? (
          ""
        ) : (
          <img
            alt=""
            src={changePass}
            style={{ width: "16px", cursor: "pointer" }}
            onClick={() => setShowModalChangePass(true)}
          />
        )}

        {userDetail.Email === item.email ? (
          ""
        ) : userDetailData &&
          userDetailData.permission &&
          userDetailData.permission.delete_tenant ? (
          <img
            alt=""
            src={deleteIcon}
            style={{ width: "20px", marginLeft: "8px", cursor: "pointer" }}
            onClick={() => setOpen(true)}
          />
        ) : (
          ""
        )}
      </td>

      <ModalConfirm
        title={"Bạn có chắc chắn thay đổi bản ghi này?"}
        decs={"Do you want to update this record with the exist setting?"}
        open={open}
        setOpen={setOpen}
        method={deletePermission}
        data={{
          email: item.email,
        }}
        setOpenModal={() => {}}
        img={img}
      />
      <ModalChangePass
        showModalChangePass={showModalChangePass}
        onHideModalChangePass={() => setShowModalChangePass(false)}
        getListUser={() => {}}
        user={userDetail}
        isAdmin={false}
        root={true}
        email={item.email}
      />
    </tr>
  );
};

export default PermissionItem;
