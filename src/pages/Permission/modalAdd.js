import { Button, Form, Input, Modal } from "antd";
import React, { useEffect } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { createPermission } from "redux/slices/tenants";
import styled from "styled-components";

// const { Option } = Select;

const ModalAdd = (props) => {
  const { isModalVisible, setIsModalVisible } = props;
  const { isCreatePermission } = useSelector((state) => state.tenantsReducer);
  // const [role, setRole] = useState("");
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
    form.resetFields();
  };

  useEffect(() => {
    if (isCreatePermission === false) {
      setIsModalVisible(false);
    }
  }, [isCreatePermission, setIsModalVisible]);

  const onFinish = (values) => {
    dispatch(
      createPermission({
        role: "super",
        update_tenant: true,
        create_tenant: true,
        delete_tenant: true,
        email: values.email,
        view_tenant: true,
        password: values.password,
      })
    );
    form.resetFields();
  };

  return (
    <>
      <CustomModal
        title="Add permission"
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          labelAlign="left"
          name="basic"
          form={form}
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          initialValues={{
            view: false,
            create: false,
            update: false,
            delete: false,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="Email"
            name="email"
            rules={[
              {
                required: true,
                message: "Please input email!",
              },
              {
                type: "email",
                message: "Please input valid username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          {/* <Form.Item
            label="Role"
            name="role"
            rules={[
              {
                required: true,
                message: "Please input role!",
              },
            ]}
          >
            <Select
              onChange={(e) => {
                setRole(e);
              }}
            >
              <Option value="normal">Operation user</Option>
              <Option value="super">System admin</Option>
            </Select>
          </Form.Item> */}

          {/* {role === "super" ? (
            ""
          ) : (
            <>
              <Form.Item
                name="view"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox>View</Checkbox>
              </Form.Item>

              <Form.Item
                name="update"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox>Update</Checkbox>
              </Form.Item>

              <Form.Item
                name="create"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox>Create</Checkbox>
              </Form.Item>

              <Form.Item
                name="delete"
                valuePropName="checked"
                wrapperCol={{
                  offset: 8,
                  span: 16,
                }}
              >
                <Checkbox>Delete</Checkbox>
              </Form.Item>
            </>
          )} */}
          <Form.Item
            label={t("user.newPass")}
            name="password"
            rules={[
              { required: true, message: t("common.placeholderInput") },
              () => ({
                validator(_, value) {
                  if (value) {
                    if (value.length < 8) {
                      return Promise.reject(new Error(t("user.errorPass")));
                    } else if (!value.match(/(?=.*?[a-z])/)) {
                      return Promise.reject(new Error(t("user.errorPass2")));
                    } else if (!value.match(/(?=.*?[0-9])/)) {
                      return Promise.reject(new Error(t("user.errorPass5")));
                    } else if (
                      !value.match(/(?=.*?[,./=+<({})>[|!@#$%^&*?_-])/) &&
                      !value.includes("]") &&
                      !value.includes("\\")
                    ) {
                      return Promise.reject(new Error(t("user.errorPass4")));
                    } else if (!value.match(/(?=.*?[A-Z])/)) {
                      return Promise.reject(new Error(t("user.errorPass3")));
                    } else return Promise.resolve();
                  }
                  return Promise.resolve();
                },
              }),
            ]}
          >
            <Input.Password placeholder={t("common.placeholderInput")} />
          </Form.Item>

          <Form.Item
            label={t("user.verifyPass")}
            name="verify_password"
            dependencies={["password"]}
            rules={[
              { required: true, message: t("common.placeholderInput") },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(new Error(t("user.errorPass6")));
                },
              }),
            ]}
          >
            <Input.Password placeholder={t("common.placeholderInput")} />
          </Form.Item>
          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              loading={isCreatePermission}
            >
              Save
            </CustomButtonSave>
            <CustomButtonCancel
              size="large"
              onClick={() => {
                handleCancel();
              }}
            >
              Cancel
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

export default withTranslation()(ModalAdd);

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-bottom: 24px;
  padding-top: 24px;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;

  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomModal = styled(Modal)`
  .ant-checkbox-checked {
    background-color: ${(props) => props.theme.main};
    .ant-checkbox-inner {
      background-color: ${(props) => props.theme.main};
      border-color: ${(props) => props.theme.main};
    }
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #2c2c2c !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;
