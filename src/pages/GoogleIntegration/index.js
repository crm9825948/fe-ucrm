import { Breadcrumb, Button } from "antd";
import _ from "lodash";

import styled from "styled-components";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "redux/store";
import {
  addAccountMapping,
  getListAccountMapping,
  getListGoogleIntegrationSetting,
  deleteGoogleIntegrationSetting,
  deleteAccountMapping,
} from "redux/slices/googleIntegration";
import deleteIcon from "../../assets/icons/common/delete-icon.png";
import refreshIcon from "../../assets/icons/common/refresh-icon.png";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { changeTitlePage } from "redux/slices/authenticated";

const GoogleIntegration = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { editGoogleIntegrationSetting, listAccountMapping } = useSelector(
    (state) => state.googleIntegrationReducer
  );
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const [dataDelete, setDataDelete] = useState({});
  const [messageDelete, setMessageDelete] = useState();

  useEffect(() => {
    dispatch(changeTitlePage("Google Integration"));
    //eslint-disable-next-line
  }, []);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "oauth2" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  useEffect(() => {
    dispatch(getListGoogleIntegrationSetting());
    // eslint-disable-next-line
  }, []);
  useEffect(() => {
    dispatch(
      getListAccountMapping({
        credential_id: "",
      })
    );
  }, [dispatch]);
  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          Settings
        </Breadcrumb.Item>
        <BreadcrumbItem>Google Integration</BreadcrumbItem>
      </Breadcrumb>

      <Wrap>
        <Content>
          <WrapAction>
            <Title>Mapping account</Title>
            <WrapButton>
              {checkRule("create") && (
                <Button
                  type="primary"
                  onClick={() => {
                    dispatch(
                      addAccountMapping({
                        window: window,
                        payload: {
                          credential_id: editGoogleIntegrationSetting._id,
                        },
                      })
                    );
                  }}
                >
                  + Add account
                </Button>
              )}
              <RefreshButton
                onClick={() => {
                  dispatch(
                    getListAccountMapping({
                      credential_id: "",
                    })
                  );
                }}
              >
                <img src={refreshIcon} alt="refresh" />
                <span>Refresh</span>
              </RefreshButton>
            </WrapButton>
          </WrapAction>

          {listAccountMapping.map((item) => {
            return (
              <AccountMapping key={item._id}>
                <Info>
                  <Avatar src={item.avatar_url} alt="avatar" />
                  <Email>{item.email}</Email>
                </Info>

                {checkRule("delete") && (
                  <Delete
                    className="delete"
                    src={deleteIcon}
                    alt="delete"
                    onClick={() => {
                      setDataDelete({
                        payload: {
                          _id: item._id,
                        },
                        credential_id: editGoogleIntegrationSetting._id,
                      });
                      setMessageDelete("liên kết tới tài khoản này");
                      dispatch(setShowModalConfirmDelete(true));
                    }}
                  />
                )}
              </AccountMapping>
            );
          })}
        </Content>
      </Wrap>

      <ModalConfimDelete
        title={messageDelete}
        decs="Sau khi xóa dữ liệu sẽ không thể hoàn tác."
        methodDelete={
          messageDelete && messageDelete.indexOf("liên kết") > -1
            ? deleteAccountMapping
            : deleteGoogleIntegrationSetting
        }
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};

export default GoogleIntegration;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px;
  color: #2c2c2c;
  cursor: default;
`;

const Wrap = styled.div`
  background-color: #fff;
  margin-top: 32px;
  padding: 16px 100px;

  @media screen and (min-width: 1600px) {
    padding: 32px 200px;
  }
`;

const Content = styled.div`
  background: #f2f4f5;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 16px;
`;

const WrapAction = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  margin-bottom: 16px;

  .ant-btn {
    font-size: 16px;
    height: unset;
    width: 138px;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Title = styled.div`
  color: #252424;
  font-size: 17px;
  font-family: var(--roboto-500);
`;

const WrapButton = styled.div`
  display: flex;
  align-items: center;
`;

const AccountMapping = styled.div`
  background-color: #fff;
  width: 100%;
  padding: 12px 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 16px;
  border-radius: 5px;
  border: 1px solid #fff;
  cursor: pointer;

  :last-child {
    margin-bottom: 0;
  }

  .delete {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  :hover {
    border: 1px solid #20a2a2;

    .delete {
      visibility: visible;
      opacity: 1;
    }
  }
`;

const Info = styled.div`
  display: flex;
  align-items: center;
`;

const Avatar = styled.img`
  width: 46px;
  height: 46px;
  border-radius: 50%;
  margin-right: 24px;
`;

const Email = styled.div``;

const Delete = styled.img`
  width: 35px;
`;

const RefreshButton = styled(Button)`
  display: flex;
  align-items: center;
  justify-content: center;

  img {
    margin-right: 10px;
  }

  &:hover {
    img {
      filter: brightness(200);
    }
  }

  &:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  &:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;
