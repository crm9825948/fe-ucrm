import axios from "axios";
import ChartComponent from "components/Charts/charts";
import { BASE_URL_API } from "constants/constants";
import ModalChart from "pages/ReportDetails/ModalChart";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadListObjectField } from "redux/slices/objects";
import { loadAllCharts, loadDetailsChart } from "redux/slices/reportDetails";
import { checkTokenExpiration } from "contexts/TokenCheck";

const Chart = ({ data, onRemove, readonly }) => {
  const [showModalChart, setShowModalChart] = useState(false);
  const [isEdit, setIsEdit] = useState(false);
  const dispatch = useDispatch();
  const removeItem = () => {
    onRemove(data.i);
  };
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [infoReport, setInfoReport] = useState({});
  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();

      if (showModalChart) {
        axios
          .post(
            BASE_URL_API + "report/get-report-info",
            {
              report_id: data.report_id,
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            setInfoReport(res.data.data);
          })
          .catch((err) => {});
      }
    };
    checkToken();
  }, [data, showModalChart]);

  const _onShowModalChart = () => {
    setShowModalChart(true);
  };

  useEffect(() => {
    // if (showModalChart)
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: data.object_id,
      })
    );
  }, [dispatch, data]);

  const _onHideModalChart = () => {
    setShowModalChart(false);
  };

  const loadCharts = useCallback(() => {
    dispatch(
      loadAllCharts({
        report_id: data.report_id,
      })
    );
  }, [dispatch, data]);

  const _onEditChart = (id) => {
    _onShowModalChart();
    setIsEdit(true);

    dispatch(
      loadDetailsChart({
        _id: id,
      })
    );
  };

  return (
    <div>
      <ChartComponent
        chart={{
          _id: data.chart_id,
        }}
        allCondition={[]}
        anyCondition={[]}
        _onDeleteChart={() => {}}
        _onEditChart={_onEditChart}
        width="100%"
        height=""
        onRemove={removeItem}
        readonly={readonly}
      />
      <ModalChart
        showModalChart={showModalChart}
        onHideModalChart={_onHideModalChart}
        listFieldsObject={listObjectField}
        loadCharts={loadCharts}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        infoReport={infoReport}
        report_id={data.report_id}
      />
    </div>
  );
};

export default Chart;
