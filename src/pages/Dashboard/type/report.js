import axios from "axios";
import { BASE_URL_API } from "constants/constants";
// import ReportSetting from "pages/Report/ModalReportSetting";
// import ShareReport from "pages/Report/ModalShareReport";
import DataReport from "pages/ReportDetails/DataReport";
import DescriptionReport from "pages/ReportDetails/DescriptionReport";
// import ModalExport from "pages/ReportDetails/ModalExport";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { loadDetailsReport, setCurrentPage } from "redux/slices/report";
import {
  filterReport,
  // loadInfoReport,
  loadReportArithmetic,
  loadSpecificReport,
  loadTotalRecordsSuccess,
  // loadTotalRecords,
  pinToDashBoard,
  unpinReport,
} from "redux/slices/reportDetails";
import { useSelector } from "react-redux";
import { loadListObjectField } from "redux/slices/objects";
import { checkTokenExpiration } from "contexts/TokenCheck";

const ReportComponent = ({ data, onRemove, readonly }) => {
  const dispatch = useDispatch();
  const [infoReport, setInfoReport] = useState({});
  const [showFilter, setShowFilter] = useState(false);
  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);
  const [, setShowModalChart] = useState(false);
  const [, setShowModalWidget] = useState(false);
  // const [isEdit, setIsEdit] = useState(false);
  // const [showReportSetting, setShowReportSetting] = useState(false);
  // const [showModalExport, setShowModalExport] = useState(false);
  // const [showModalShare, setShowModalShare] = useState(false);
  // const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [tempAllCondition, setTempAllCondition] = useState([]);
  const [tempAnyCondition, setTempAnyCondition] = useState([]);
  const [columnReport, setColumnReport] = useState([]);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  // const { listAllGroups } = useSelector((state) => state.groupReducer);
  const { currentPage } = useSelector((state) => state.reportReducer);
  // const { category } = useSelector((state) => state.objectsManagementReducer);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "report/get-report-info",
          {
            report_id: data.report_id,
            api_version: "2",
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          if (res.data.data) {
            setInfoReport(res.data.data);
            dispatch(
              loadListObjectField({
                api_version: "2",
                object_id: res.data.data.report_info.main_object,
                show_meta_fields: true,
              })
            );
          }
        })
        .catch((err) => {});
    };
    checkToken();
    //eslint-disable-next-line
  }, [data]);

  const _onShowModalChart = () => {
    setShowModalChart(true);
  };

  const _onShowModalWidget = () => {
    setShowModalWidget(true);
  };

  const _onShowReportSetting = () => {
    // setIsEdit(true);
    // setShowReportSetting(true);
  };

  const _onPinReport = () => {
    let pathname = window.location.pathname.split("/");
    if (onRemove && pathname[pathname.length - 1] === "dashboard") {
      onRemove(data.i);
    }
    if (pathname[pathname.length - 1] !== "dashboard") {
      if (infoReport.report_info.is_pinned_to_dashboard) {
        dispatch(
          unpinReport({
            report_id: infoReport.report_info._id,
          })
        );
      } else {
        dispatch(
          pinToDashBoard({
            report_id: infoReport.report_info._id,
            type: "list",
          })
        );
      }
    }
  };

  const _onShowModalExport = () => {
    // setShowModalExport(true);
  };

  const _onShowModalShare = () => {
    // setShowModalShare(true);
  };

  const _onFilter = () => {
    dispatch(setCurrentPage(1));
    setRecordPerPage(10);
    setTempAllCondition(allCondition);
    setTempAnyCondition(anyCondition);
    dispatch(
      filterReport({
        current_page: 1,
        record_per_page: 10,
        object_id: data.object_id,
        report_id: data.report_id,
        quick_filter: {
          and_filter: allCondition,
          or_filter: anyCondition,
        },
        api_version: "2",
      })
    );

    if (infoReport.report_info.report_type === "detail") {
      dispatch(
        loadReportArithmetic({
          quick_filter: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
          object_id: data.object_id,
          report_id: data.report_id,
          api_version: "2",
        })
      );
    }
  };

  const _onShowFilter = () => {
    setShowFilter(true);
  };

  const _onCloseFilter = useCallback(() => {
    setShowFilter(false);
    setAllCondition(tempAllCondition);
    setAnyCondition(tempAnyCondition);
  }, [tempAllCondition, tempAnyCondition]);

  const loadData = () => {
    let tempPage = currentPage + 1;
    dispatch(setCurrentPage(tempPage));
  };
  /*eslint-disable-next-line*/
  const loadReport = useCallback(() => {
    let dataTemp = {
      current_page: currentPage,
      record_per_page: recordPerPage,
      object_id: data.object_id,
      report_id: data.report_id,
      api_version: "2",
    };

    if (tempAllCondition.length > 0 || tempAnyCondition.length > 0) {
      dispatch(
        loadSpecificReport({
          ...dataTemp,
          quick_filter: {
            and_filter: tempAllCondition,
            or_filter: tempAnyCondition,
          },
        })
      );
    } else {
      dispatch(
        loadSpecificReport({
          ...dataTemp,
        })
      );
    }
  }, [
    tempAllCondition,
    tempAnyCondition,
    currentPage,
    dispatch,
    recordPerPage,
    data,
  ]);

  // useEffect(() => {
  //   loadReport();
  // }, []);

  // const loadPagi = useCallback(() => {
  //   dispatch(
  //     loadTotalRecords({
  //       current_page: 1,
  //       record_per_page: 10,
  //       object_id: data.object_id,
  //       report_id: data.report_id,
  //     })
  //   );
  // }, [dispatch, data]);

  // const _onHideReportSetting = () => {
  //   setShowReportSetting(false);
  // };

  // useEffect(() => {
  //   loadPagi();
  //   /*eslint-disable-next-line*/
  // }, []);

  // useEffect(() => {
  //   loadDetailReport();
  // }, []);
  /*eslint-disable-next-line*/
  const loadDetailReport = useCallback(() => {
    dispatch(
      loadDetailsReport({
        report_id: data.report_id,
        api_version: "2",
      })
    );
  }, [dispatch, data]);

  const [dataReportDashboard, setDataReportDashboard] = useState({});
  const [arithmeticReportData, setArimeticReportData] = useState([]);

  const loadDataReport = useCallback(
    async (first, last) => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "report/get-specific-report",
          {
            api_version: "2",
            // current_page: currentPage,
            // record_per_page: recordPerPage,
            first_record_id: first,
            last_record_id: last,
            report_id: data.report_id,
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => setDataReportDashboard(res.data.data))
        .catch((err) => console.log(err));
    },
    [data]
  );

  useEffect(() => {
    // dispatch(loadDataNecessary());

    loadDataReport(null, null);
    dispatch(loadTotalRecordsSuccess(null));

    /*eslint-disable-next-line*/
  }, []);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (infoReport?.report_info?.report_type === "detail") {
        if (
          infoReport &&
          infoReport.report_info &&
          infoReport.report_info.main_object
        )
          axios
            .post(
              BASE_URL_API + "report/get-report-arithmetic",
              {
                quick_filter: {
                  and_filter: [],
                  or_filter: [],
                },
                object_id: infoReport?.report_info?.main_object,
                report_id: data.report_id,
                api_version: "2",
              },
              {
                headers: {
                  Authorization: isTokenValid,
                },
              }
            )
            .then((res) => setArimeticReportData(res.data.data))
            .catch((err) => console.log(err));
      }
    };
    checkToken();
    /*eslint-disable-next-line*/
  }, [infoReport, data]);

  // const _onHideModalShare = () => {
  //   setShowModalShare(false);
  // };

  // const _onHideModalExport = () => {
  //   setShowModalExport(false);
  // };

  // const folder = {
  //   id: infoReport?.report_info?.folder_id,
  //   name: infoReport?.report_info?.folder_name,
  // };

  return (
    <>
      <DescriptionReport
        infoReport={infoReport}
        showFilter={showFilter}
        allCondition={allCondition}
        setAllCondition={setAllCondition}
        anyCondition={anyCondition}
        setAnyCondition={setAnyCondition}
        _onShowModalChart={_onShowModalChart}
        _onShowModalWidget={_onShowModalWidget}
        _onShowReportSetting={_onShowReportSetting}
        _onPinReport={_onPinReport}
        _onShowModalExport={_onShowModalExport}
        _onShowModalShare={_onShowModalShare}
        _onFilter={_onFilter}
        _onShowFilter={_onShowFilter}
        _onCloseFilter={_onCloseFilter}
        readonly={readonly}
        listObjectField={listObjectField}
      />
      <DataReport
        infoReport={infoReport}
        loadData={loadData}
        setCurrentPage={setCurrentPage}
        setRecordPerPage={setRecordPerPage}
        currentPage={currentPage}
        recordPerPage={recordPerPage}
        columnReport={columnReport}
        setColumnReport={setColumnReport}
        dataReportDashboard={dataReportDashboard}
        arithmeticReportData={arithmeticReportData}
        objectID={data.object_id}
        recordID={data.report_id}
        loadReport={loadDataReport}
        tempAllCondition={tempAllCondition}
        tempAnyCondition={tempAnyCondition}
      />

      {/* <ReportSetting
        showReportSetting={showReportSetting}
        onHideReportSetting={_onHideReportSetting}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        listAllGroups={listAllGroups}
        reportDetails={reportDetails}
        getListReport={loadDetailReport}
        loadReport={loadReport}
        loadPagi={loadPagi}
        loadInfoReport={() => {
          dispatch(
            loadInfoReport({
              report_id: data.report_id,
            })
          );
        }}
        folder={folder}
        objects={category}
        loadWidgets={() => {}}
        loadCharts={() => {}}
        setColumnReport={() => {}}
      />

      <ShareReport
        showModalShare={showModalShare}
        onHideModalShare={_onHideModalShare}
        listAllGroups={listAllGroups}
        reportDetails={reportDetails}
        isEdit={false}
      />

      <ModalExport
        showModalExport={showModalExport}
        onHideModalExport={_onHideModalExport}
        infoReport={infoReport}
      /> */}
    </>
  );
};

export default ReportComponent;
