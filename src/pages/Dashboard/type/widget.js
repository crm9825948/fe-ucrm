import Widget from "components/Widget/widget";
import ModalWidget from "pages/ReportDetails/ModalWidget";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadListObjectField } from "redux/slices/objects";
import { loadAllWidgets, loadDetailsWidget } from "redux/slices/reportDetails";

const WidgetComponent = ({ data, onRemove, readonly, init }) => {
  const [showModalWidget, setShowModalWidget] = useState(false);
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const [isEdit, setIsEdit] = useState(false);
  const dispatch = useDispatch();

  const removeItem = () => {
    onRemove(data.i);
  };

  useEffect(() => {
    // if (showModalWidget)
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: data.object_id,
      })
    );
  }, [dispatch, data]);

  const loadWidgets = useCallback(() => {
    dispatch(
      loadAllWidgets({
        report_id: data.report_id,
      })
    );
  }, [dispatch, data]);

  const _onShowModalWidget = () => {
    setShowModalWidget(true);
  };

  const _onHideModalWidget = () => {
    setShowModalWidget(false);
  };

  const _onEditWidget = (id) => {
    _onShowModalWidget();
    setIsEdit(true);

    dispatch(
      loadDetailsWidget({
        _id: id,
      })
    );
  };

  return (
    <div>
      {init ? (
        ""
      ) : (
        <>
          <Widget
            widget={{
              _id: data.widget_id,
            }}
            allCondition={[]}
            anyCondition={[]}
            _onDeleteWidget={() => {}}
            onRemove={removeItem}
            _onEditWidget={_onEditWidget}
            readonly={readonly}
          />
          <ModalWidget
            showModalWidget={showModalWidget}
            onHideModalWidget={_onHideModalWidget}
            loadWidgets={loadWidgets}
            isEdit={isEdit}
            setIsEdit={setIsEdit}
            listFieldsObject={listObjectField}
            report_id={data.report_id}
          />
        </>
      )}
    </div>
  );
};

export default WidgetComponent;
