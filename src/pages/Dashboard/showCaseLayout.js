import _ from "lodash";
import React, { useEffect, useRef, useState } from "react";
import RGL, { WidthProvider } from "react-grid-layout";
import { useDispatch } from "react-redux";
import { updateDashboard } from "redux/slices/dashboard";
import Chart from "./type/chart";
import ReportComponent from "./type/report";
import WidgetComponent from "./type/widget";
import SeoIllustration from "./illustration_dashboard";
import styled from "styled-components";
import { useNavigate } from "react-router-dom";

const ReactGridLayout = WidthProvider(RGL);

const ShowCaseLayout = (props) => {
  const { dashboardID, layoutList, setLayoutList, userDetail } = props;
  const navigate = useNavigate();
  // const [init, setInit] = useState(false);

  const dispatch = useDispatch();

  // useEffect(() => {
  //   setInit(false);
  // }, [dashboardID]);

  const onRemove = (i) => {
    let newLayoutList = [...layoutList];
    /*eslint-disable-next-line*/
    newLayoutList.forEach((layout) => {
      if (layout._id === dashboardID) {
        layout.layout = _.reject(layout.layout, { i: i });
        handleUpdate(layout.layout);
      }
    });
    setLayoutList(newLayoutList);
  };

  const createElement = (el, readonly) => {
    const index = el.i;
    return (
      <div
        key={index}
        id={`dashboard-item${index}`}
        data-grid={el}
        style={{
          border: "none",
          background: "#fff",
          // paddingTop: "2rem",
          // paddingLeft: "2rem",
          // paddingRight: "2rem",
          width: "100%",
          overflow: "auto",
          padding: el.type !== "chart" ? "1rem 1rem 0 1rem" : "0",
          boxShadow: "0px 0px 12px rgba(0, 0, 0, 0.1)",
          borderRadius: "10px",
        }}
      >
        {el.type === "chart" ? (
          <Chart data={el} onRemove={onRemove} readonly={readonly} />
        ) : el.type === "widget" ? (
          <WidgetComponent data={el} onRemove={onRemove} readonly={readonly} />
        ) : (
          <ReportComponent data={el} onRemove={onRemove} readonly={readonly} />
        )}
      </div>
    );
  };

  const handleUpdate = (layoutsChanged, readonly) => {
    // if (init) {
    if (!readonly) {
      let arr = [];
      /*eslint-disable-next-line*/
      layoutList.map((layout) => {
        if (layout._id === dashboardID) {
          /*eslint-disable-next-line*/
          layout.layout.map((item, index) => {
            let newItem = {
              report_id: item.report_id,
              type: item.type,
              is_hidden: item.is_hidden,
              chart_id: item.chart_id,
              widget_id: item.type === "widget" ? item.widget_id : "",
              object_id: item.object_id,
              no: index + 1,
              chart_type: item.type === "chart" ? item.chart_type : null,
              bounding_box: [
                layoutsChanged &&
                  layoutsChanged[index] &&
                  layoutsChanged[index].x,
                layoutsChanged &&
                  layoutsChanged[index] &&
                  layoutsChanged[index].y,
                layoutsChanged &&
                  layoutsChanged[index] &&
                  layoutsChanged[index].w,
                layoutsChanged &&
                  layoutsChanged[index] &&
                  layoutsChanged[index].h,
              ],
            };
            arr.push(newItem);
          });
        }
      });

      if (userDetail._id !== "") {
        dispatch(
          updateDashboard({
            _id: dashboardID,
            user_id: userDetail._id,
            active: true,
            components: arr,
          })
        );
      }
    }

    // } else {
    //   setInit(true);
    // }
  };
  useEffect(() => {
    setInit(true);
  }, []);
  const [init, setInit] = useState(true);
  const onLayoutChange = (layout, readonly) => {
    if (init === false) {
      handleUpdate(layout, readonly);
    } else {
      setInit(false);
    }
  };

  const componentRef = useRef();

  return (
    <>
      <div
        ref={componentRef}
        style={{ background: "white", borderRadius: "20px", marginTop: "16px" }}
        id="dashboard"
      >
        <style type="text/css" media="print">
          {"\
   @page { size: landscape; }\
"}
        </style>
        {layoutList && layoutList.length > 0
          ? /*eslint-disable-next-line*/
            layoutList.map((layout, index) => {
              if (layout._id === dashboardID) {
                let readonly = layout.readonly;
                if (layout.layout.length > 0) {
                  return (
                    <ReactGridLayout
                      className="layout"
                      onLayoutChange={(layout) => {
                        onLayoutChange(layout, readonly);
                      }}
                      cols={12}
                      rowHeight={10}
                      key={index}
                      isDraggable={!layout.readonly}
                      isResizable={!layout.readonly}
                    >
                      {_.map(layout.layout, (el) =>
                        createElement(el, readonly)
                      )}
                    </ReactGridLayout>
                  );
                } else {
                  return (
                    <Wrapper className="welcome-back">
                      {/* <div className="fake-bg"></div> */}
                      <div
                        // style={{ width: "600px" }}
                        className="right-comp animate__animated animate__slideInDown"
                      >
                        <SeoIllustration
                          sx={{
                            p: 3,
                            // width: 360,
                            margin: { xs: "auto", md: "inherit" },
                          }}
                        />
                      </div>
                      <div className="left-comp animate__animated animate__slideInUp">
                        <div className="title">
                          Dashboard has no component, please go to Report to add
                          new widgets, charts, reports!
                        </div>
                        <button
                          class="MuiButton-root MuiButton-contained MuiButton-containedPrimary MuiButton-sizeMedium MuiButton-containedSizeMedium MuiButtonBase-root  css-a3sctw"
                          tabindex="0"
                          type="button"
                          onClick={() => navigate("/report#my_folders")}
                        >
                          Go to Report
                          <span class="MuiTouchRipple-root css-w0pj6f"></span>
                        </button>
                      </div>
                    </Wrapper>
                  );
                }
              }
            })
          : ""}
      </div>
    </>
  );
};
export default ShowCaseLayout;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
  align-items: center;
  padding: 40px;

  border-radius: 20px;
  position: relative;
  margin-top: 40px;
  .fake-bg {
    width: 100%;
    height: 100%;
    position: absolute;
    z-index: 0;
    top: 0;
    left: 0;
    opacity: 0.2;
    background-color: ${(props) => props.theme.main};
    border-radius: 20px;
  }
  .right-comp {
    z-index: 1000000;
  }
  .left-comp {
    z-index: 1000000;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    .title {
      font-size: 20px;
      font-weight: bold;
      margin-top: 24px;
      margin-bottom: 24px;
    }

    .decs {
      margin-top: 24px;
      font-weight: lighter;
      font-size: 16px;
      margin-bottom: 24px;
    }

    /*! CSS Used from: Embedded */
    *,
    ::before,
    ::after {
      box-sizing: inherit;
    }
    * {
      margin: 0px;
      padding: 0px;
      box-sizing: border-box;
    }
    /*! CSS Used from: Embedded */
    *,
    ::before,
    ::after {
      box-sizing: inherit;
    }
    * {
      margin: 0px;
      padding: 0px;
      box-sizing: border-box;
    }
    /*! CSS Used from: Embedded */
    .css-w0pj6f {
      overflow: hidden;
      pointer-events: none;
      position: absolute;
      z-index: 0;
      inset: 0px;
      border-radius: inherit;
    }
    .css-a3sctw {
      display: inline-flex;
      align-items: center;
      justify-content: center;
      position: relative;
      box-sizing: border-box;
      -webkit-tap-highlight-color: transparent;
      outline: 0px;
      border: 0px;
      margin: 0px;
      cursor: pointer;
      user-select: none;
      vertical-align: middle;
      appearance: none;
      text-decoration: none;
      font-weight: 700;
      line-height: 1.71429;
      font-size: 0.875rem;
      text-transform: capitalize;
      font-family: "Public Sans", sans-serif;
      min-width: 64px;
      padding: 6px 16px;
      border-radius: 8px;
      transition: background-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
        box-shadow 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
        border-color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms,
        color 250ms cubic-bezier(0.4, 0, 0.2, 1) 0ms;
      color: rgb(255, 255, 255);
      background-color: ${(props) => props.theme.main};
      /* box-shadow: rgba(0, 171, 85, 0.24) 0px 8px 16px 0px; */
    }
    .css-a3sctw:hover {
      text-decoration: none;
      background-color: ${(props) => props.theme.darker};
      box-shadow: ${(props) => props.theme.darker} 0px 2px 4px -1px,
        ${(props) => props.theme.darker} 0px 4px 5px 0px,
        ${(props) => props.theme.darker} 0px 1px 10px 0px;
    }
    @media (hover: none) {
      .css-a3sctw:hover {
        background-color: rgb(0, 171, 85);
      }
    }
    .css-a3sctw:active {
      box-shadow: rgba(145, 158, 171, 0.2) 0px 5px 5px -3px,
        rgba(145, 158, 171, 0.14) 0px 8px 10px 1px,
        rgba(145, 158, 171, 0.12) 0px 3px 14px 2px;
    }
    .css-a3sctw:hover {
      box-shadow: none;
    }
  }
`;
