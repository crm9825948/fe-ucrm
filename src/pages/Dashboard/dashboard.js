import { FE_URL } from "constants/constants";
import html2canvas from "html2canvas";
import { jsPDF } from "jspdf";
import { default as React, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { changeTitlePage } from "redux/slices/authenticated";
import {
  loadAllDashboard,
  loadDefaultDashboard,
  unMountDashboard,
} from "redux/slices/dashboard";
import { loadDataNecessary } from "redux/slices/reportDetails";
import HeaderDashboard from "./headerDashboard";
import ShowCaseLayout from "./showCaseLayout";

const Dashboard = () => {
  const { t } = useTranslation();
  const [layoutList, setLayoutList] = useState([]);

  const { userDetail } = useSelector((state) => state.userReducer);
  const dispatch = useDispatch();
  const [dashboardID, setDashboardID] = useState("");
  const { dashboards, defaultDashboard } = useSelector(
    (state) => state.dashboardReducer
  );

  useEffect(() => {
    if (userDetail && userDetail.user_root) {
      window.open(FE_URL + "/tenants", "_self");
    }
  }, [userDetail]);

  useEffect(() => {
    setDashboardID(defaultDashboard && defaultDashboard._id);
  }, [defaultDashboard]);

  useEffect(() => {
    return () => {
      dispatch(unMountDashboard());
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch(loadAllDashboard());
    dispatch(loadDefaultDashboard());
    // eslint-disable-next-line
  }, [dispatch]);

  useEffect(() => {
    dispatch(changeTitlePage(t("common.dashboard")));
    // eslint-disable-next-line
  }, [t]);

  const reloadDashboard = () => {
    dispatch(loadAllDashboard());
  };

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    if (dashboards.length > 0) {
      parseLayoutList(dashboards);
    }
  }, [dashboards]);

  const parseLayoutList = (data) => {
    let layoutList = [];
    if (data && data.length > 0) {
      /*eslint-disable-next-line*/
      data.map((layout) => {
        let arr = [];
        if (layout.components && layout.components.length > 0) {
          /*eslint-disable-next-line*/
          layout.components.map((component) => {
            let comp = {
              i: (component.no - 1).toString(),
              x: component.bounding_box[0],
              y: component.bounding_box[1],
              w: component.bounding_box[2],
              h: component.bounding_box[3],
              chart_id: component.chart_id,
              object_id: component.object_id,
              widget_id: component.type === "widget" ? component.widget_id : "",
              chart_type:
                component.type === "chart" ? component.chart_type : null,
              static: layout.readonly,
              is_hidden: component.is_hidden,
              report_id: component.report_id,
              type: component.type,
            };
            arr.push(comp);
          });
        }
        let item = {
          readonly: layout.readonly,
          _id: layout._id,
          layout: arr,
        };

        layoutList.push(item);
      });
    }
    setLayoutList(layoutList);
  };

  const printDocument = () => {
    const input = document.getElementById("dashboard");
    html2canvas(input).then((canvas) => {
      const imgData = canvas.toDataURL("image/png");
      const pdf = new jsPDF();
      pdf.addImage(imgData, "JPEG", 0, 0);
      // pdf.output('dataurlnewwindow');
      pdf.save("download.pdf");
    });
  };

  return (
    <>
      <div style={{ padding: "8px" }}>
        <HeaderDashboard
          setDashboardID={setDashboardID}
          dashboardID={dashboardID}
          printDocument={printDocument}
          reloadDashboard={reloadDashboard}
        />
        {Object.keys(defaultDashboard).length > 0 && dashboards.length > 0 ? (
          <ShowCaseLayout
            dashboardID={dashboardID}
            layoutList={layoutList}
            setLayoutList={setLayoutList}
            userDetail={userDetail}
            reloadDashboard={reloadDashboard}
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default Dashboard;
