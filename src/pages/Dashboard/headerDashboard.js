import {
  DeleteOutlined,
  DownOutlined,
  EditOutlined,
  ShareAltOutlined,
  StarOutlined,
} from "@ant-design/icons";
import { Button, Col, Dropdown, Menu, Row } from "antd";
import ModalDelete from "components/Modal/ModalConfirmDelete";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteDashboard,
  loadListDashboard,
  makeDefault,
} from "redux/slices/dashboard";
import { setShowModalConfirmDelete } from "redux/slices/global";
import styled from "styled-components";
import ModalDashboard from "./modalDashboard";
import ModalShare from "./modalShare";

const { SubMenu } = Menu;

const HeaderDashboard = (props) => {
  const { setDashboardID, dashboardID, reloadDashboard } = props;
  const {
    defaultDashboard,
    // listDashboard,
    // isLoadListDashboard,
    isLoadingDelete,
    listDashboard,
  } = useSelector((state) => state.dashboardReducer);
  const dispatch = useDispatch();
  const [dashboardName, setDashboardName] = useState("");
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [openShare, setOpenShare] = useState(false);
  const [readOnly, setReadOnly] = useState(false);
  const [flagTop, setFlagTop] = useState(false);
  const [flagBottom, setFlagBottom] = useState(false);
  // useEffect(() => {
  //   dispatch(
  //     loadListDashboard({
  //       view_components: false,
  //     })
  //   );
  // }, [dispatch]);

  useEffect(() => {
    setDashboardItem(defaultDashboard);
    if (Object.entries(defaultDashboard).length > 0) {
      setDashboardName(defaultDashboard.name);
    }
  }, [defaultDashboard]);

  // useEffect(() => {
  //   let item = listDashboard.find((ele) => ele._id === defaultDashboard._id);
  //   setDashboardName(item && item.name);
  // }, [defaultDashboard, listDashboard]);

  const [dashboardItem, setDashboardItem] = useState({});

  const reloadAllDashboard = () => {
    dispatch(
      loadListDashboard({
        view_components: false,
      })
    );
  };

  useEffect(() => {
    dispatch(setShowModalConfirmDelete(isLoadingDelete));
  }, [isLoadingDelete, dispatch]);

  const SubMenuCustomComponent = ({
    flagTop,
    style,
    title,
    key,
    onTitleClick,
  }) => {
    return (
      <SubMenuCustom
        flagTop={flagTop}
        style={style}
        title={title}
        key={key}
        onTitleClick={onTitleClick}
      />
    );
  };

  const SubMenuCustomComponentBottom = ({
    flagBottom,
    style,
    title,
    key,
    onTitleClick,
  }) => {
    return (
      <SubMenuCustomBottom
        flagBottom={flagBottom}
        style={style}
        title={title}
        key={key}
        onTitleClick={onTitleClick}
      />
    );
  };

  const menu = (
    <CustomMenu>
      <SubMenuCustomComponent
        flagTop={flagTop}
        onTitleClick={() => {
          setFlagTop(!flagTop);
        }}
        style={{ width: "475px" }}
        title={
          <b
            style={{
              fontWeight: "bold",
              fontSize: "14px",
              color: "#2c2c2c",
            }}
          >
            My Dashboard
          </b>
        }
        key="1"
      ></SubMenuCustomComponent>

      {flagTop === false
        ? /*eslint-disable-next-line*/
          listDashboard.map((dashboard, idx) => {
            if (!dashboard.readonly)
              return (
                <Menu.Item value={dashboard._id} key={Math.random()}>
                  <Row style={{ width: "450px" }}>
                    <Col
                      span={18}
                      style={{ fontSize: "14px" }}
                      onClick={() => {
                        reloadDashboard();
                        setDashboardID(dashboard._id);
                        setDashboardName(dashboard.name);
                        setDashboardItem(dashboard);
                        setReadOnly(false);
                      }}
                    >
                      {dashboard.name}
                    </Col>
                    <Col span={2} style={{ textAlign: "right" }}>
                      <StarOutlined
                        onClick={() => {
                          dispatch(
                            makeDefault({
                              dashboard_id: dashboard._id,
                            })
                          );
                        }}
                        style={{
                          color: `${
                            dashboard._id === defaultDashboard._id
                              ? "#FADB14"
                              : ""
                          }`,
                        }}
                      />
                    </Col>
                    <Col span={2} style={{ textAlign: "right" }}>
                      <EditOutlined
                        onClick={() => {
                          setIsModalVisible(true);
                          setDashboardItem(dashboard);
                        }}
                      />
                    </Col>
                    <Col span={2} style={{ textAlign: "right" }}>
                      <DeleteOutlined
                        onClick={() => {
                          setDashboardItem(dashboard);
                          dispatch(setShowModalConfirmDelete(true));
                        }}
                      />
                    </Col>
                  </Row>
                </Menu.Item>
              );
          })
        : ""}

      <SubMenuCustomComponentBottom
        title={
          <b
            style={{
              fontWeight: "bold",
              fontSize: "16px",
              color: "#2c2c2c",
            }}
          >
            Share to me
          </b>
        }
        key="2"
        flagBottom={flagBottom}
        onTitleClick={() => {
          setFlagBottom(!flagBottom);
        }}
      ></SubMenuCustomComponentBottom>
      {/*eslint-disable-next-line*/}
      {flagBottom === false
        ? /*eslint-disable-next-line*/
          listDashboard.map((dashboard, idx) => {
            if (dashboard.readonly === true)
              return (
                <Menu.Item value={dashboard._id} key={Math.random()}>
                  <Row style={{ width: "450px" }}>
                    <Col
                      span={18}
                      style={{ fontSize: "14px" }}
                      onClick={() => {
                        setDashboardID(dashboard._id);
                        setDashboardName(dashboard.name);
                        setDashboardItem(dashboard);
                        setReadOnly(true);
                      }}
                    >
                      {dashboard.name}
                    </Col>
                    <Col span={2} style={{ textAlign: "right" }}>
                      <StarOutlined
                        onClick={() => {
                          dispatch(
                            makeDefault({
                              dashboard_id: dashboard._id,
                            })
                          );
                        }}
                        style={{
                          color: `${
                            dashboard._id === defaultDashboard._id
                              ? "#FADB14"
                              : ""
                          }`,
                        }}
                      />
                    </Col>
                  </Row>
                </Menu.Item>
              );
          })
        : ""}
    </CustomMenu>
  );

  return (
    <>
      <Wrapper>
        <div>
          <Dropdown overlay={menu} placement="bottomRight" trigger={"click"}>
            <CustomButtonWhite>
              {dashboardName} <DownOutlined />
            </CustomButtonWhite>
          </Dropdown>
          <CustomButton
            style={{ marginLeft: "8px" }}
            onClick={() => {
              setIsModalVisible(true);
              setDashboardItem({});
            }}
          >
            + Add Dashboard
          </CustomButton>
        </div>
        <CustomButtonShare
          disabled={readOnly}
          onClick={() => {
            setOpenShare(true);
          }}
        >
          <ShareAltOutlined /> Share
        </CustomButtonShare>
        {/* <button
          onClick={() => {
            printDocument();
          }}
        >
          download ne
        </button> */}
      </Wrapper>
      <ModalDashboard
        isModalVisible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        dashboardItem={dashboardItem}
        reloadAllDashboard={reloadAllDashboard}
        dashboardName={dashboardName}
        setDashboardName={setDashboardName}
        setDashboardItem={setDashboardItem}
      />
      <ModalDelete
        title={dashboardItem.name}
        decs={"Sau khi xóa dữ liệu sẽ không thể hoàn tác."}
        methodDelete={deleteDashboard}
        dataDelete={{
          dashboard_id: dashboardItem._id,
        }}
        setShowModalDelete={() => {}}
      />
      <ModalShare
        isModalVisible={openShare}
        setIsModalVisible={setOpenShare}
        dashboardID={dashboardID}
        dashboardItem={dashboardItem}
        setDashboardItem={setDashboardItem}
      />
    </>
  );
};

export default HeaderDashboard;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  padding-top: 8px;
`;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: ${(props) => props.theme.white};
  :hover {
    background-color: ${(props) => props.theme.darker};
    color: ${(props) => props.theme.white};
    border: 1px solid ${(props) => props.theme.main};
  }
  :active {
    color: #fff;
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
  :focus {
    background-color: ${(props) => props.theme.darker}!important;
    color: ${(props) => props.theme.white}!important;
    border: 1px solid ${(props) => props.theme.main}!important;
  }
`;

const CustomButtonWhite = styled(Button)`
  margin-left: 8px;
  background-color: ${(props) => props.theme.white};
  color: #000;
  :hover {
    background-color: ${(props) => props.theme.white};
    color: #000;
    border: 1px solid ${(props) => props.theme.main};
  }
  :active {
    background-color: ${(props) => props.theme.white};
    color: #000;
    border: 1px solid ${(props) => props.theme.main};
  }
  :focus {
    background-color: ${(props) => props.theme.white}!important;
    color: #000 !important;
    border: 1px solid ${(props) => props.theme.main}!important;
  }
`;

const CustomButtonShare = styled(Button)`
  background-color: white;
  margin-right: 8px;
  color: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  :hover {
    background-color: white;
    color: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
  }
  :active {
    background-color: white;
    color: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
  }
  :focus {
    background-color: white !important;
    color: ${(props) => props.theme.main}!important;
    border: 1px solid ${(props) => props.theme.main}!important;
  }
`;

const CustomMenu = styled(Menu)`
  .ant-dropdown-menu-submenu-expand-icon {
    transform: rotate(90deg);
  }
`;

const SubMenuCustom = styled(SubMenu)`
  .ant-dropdown-menu-submenu-expand-icon {
    transform: rotate(
      ${(props) => (props.flagTop === true ? "90deg" : "270deg")}
    );
  }
`;
const SubMenuCustomBottom = styled(SubMenu)`
  .ant-dropdown-menu-submenu-expand-icon {
    transform: rotate(
      ${(props) => (props.flagBottom === true ? "90deg" : "270deg")}
    );
  }
`;
