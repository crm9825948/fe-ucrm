import { Button } from "antd";
import { useCallback, useState } from "react";
import ReactFlow, {
  addEdge,
  applyEdgeChanges,
  applyNodeChanges,
  MiniMap,
  Controls,
  Background,
} from "react-flow-renderer";

function Flow() {
  const initialNodes = [
    {
      id: "1",
      //   type: "input",
      data: { label: "Input Node" },
      position: { x: 250, y: 25 },
    },

    {
      id: "2",
      // you can also pass a React component as a label
      data: { label: <div>Default Node</div> },
      position: { x: 100, y: 125 },
    },
    {
      id: "3",
      //   type: "output",
      data: { label: "Output Node" },
      position: { x: 250, y: 250 },
    },
    {
      id: "4",
      data: { label: "Output Node x2" },
      position: { x: 400, y: 350 },
    },
  ];

  const initialEdges = [
    { id: "e1-2", source: "1", target: "2" },
    { id: "e2-3", source: "2", target: "3", animated: true },
  ];

  const edgeOptions = {
    animated: true,
    style: {
      stroke: "black",
    },
  };

  const [nodes, setNodes] = useState(initialNodes);
  const [edges, setEdges] = useState(initialEdges);

  const onNodesChange = useCallback(
    (cqq) => {
      setNodes((nds) => applyNodeChanges(cqq, nds));
    },
    [setNodes]
  );
  const onEdgesChange = useCallback(
    (changes) => {
      setEdges((eds) => applyEdgeChanges(changes, eds));
    },
    [setEdges]
  );
  const onConnect = useCallback(
    (connection) => {
      setEdges((eds) => addEdge(connection, eds));
    },
    [setEdges]
  );

  return (
    <>
      <Button
        onClick={() => {
          let temp = [...nodes];
          temp.push({
            id: `${Math.random()}`,
            data: { label: `Number ${Math.random()}` },
            position: {
              x: Math.random() * window.innerWidth - 100,
              y: Math.random() * window.innerHeight,
            },
            width: 150,
            height: 40,
          });

          setNodes(temp);
        }}
        danger
      >
        Add node
      </Button>
      <ReactFlow
        nodes={nodes}
        edges={edges}
        onNodesChange={onNodesChange}
        onEdgesChange={onEdgesChange}
        onConnect={onConnect}
        defaultEdgeOptions={edgeOptions}
        onNodeDragStop={(value, node, nodes) => {}}
        fitView
      >
        <Background />
        <Controls />
        <MiniMap />
      </ReactFlow>
    </>
  );
}

export default Flow;
