import { Button, Form, Input, Modal } from "antd";
import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { createDashboard, updateNameDashboard } from "redux/slices/dashboard";
import styled from "styled-components";

const ModalDashboard = (props) => {
  const {
    isModalVisible,
    setIsModalVisible,
    dashboardItem,
    // reloadAllDashboard,
    // dashboardName,
    // setDashboardName,
    setDashboardItem,
  } = props;
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const handleOk = () => {
    setIsModalVisible(false);
    setDashboardItem({ ...dashboardItem });
  };

  const { isLoadingCreate } = useSelector((state) => state.dashboardReducer);

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const dispatch = useDispatch();
  useEffect(() => {
    if (isLoadingCreate === false) {
      setIsModalVisible(false);
    }
  }, [isLoadingCreate, setIsModalVisible]);

  const onFinish = (value) => {
    if (Object.keys(dashboardItem).length > 0) {
      dispatch(
        updateNameDashboard({
          name: value["name"],
          user_id: dashboardItem.user_id,
          _id: dashboardItem._id,
        })
      );
      // reloadAllDashboard();
      setIsModalVisible(false);
    } else {
      dispatch(
        createDashboard({
          name: value["name"],
        })
      );
    }
  };

  useEffect(() => {
    form.setFieldsValue({
      name: dashboardItem.name,
    });
  }, [dashboardItem, form]);

  return (
    <>
      <CustomModal
        width={400}
        title={`${
          Object.keys(dashboardItem).length > 0
            ? "Edit dashboard"
            : "Add dashboard"
        } `}
        visible={isModalVisible}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={<></>}
      >
        <Form
          name="basic"
          form={form}
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          onFinish={onFinish}
          onFinishFailed={() => {}}
          autoComplete="off"
        >
          <Form.Item
            label="Tên Dashboard"
            name="name"
            rules={[
              { required: true, message: "Please input dashboard name!" },
            ]}
          >
            <Input maxLength={100} />
          </Form.Item>

          <CustomFooter>
            <CustomButtonSave
              size="large"
              htmlType="submit"
              loading={isLoadingCreate}
            >
              {t("common.save")}
            </CustomButtonSave>
            <CustomButtonCancel size="large" onClick={() => handleCancel()}>
              {t("common.cancel")}
            </CustomButtonCancel>
          </CustomFooter>
        </Form>
      </CustomModal>
    </>
  );
};

export default ModalDashboard;

const CustomModal = styled(Modal)`
  .ant-modal-footer {
    display: none;
  }
  .ant-modal-body {
    padding: 24px 24px 18px 24px;
  }
  .ant-modal-content {
    border-radius: 10px;
  }
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    padding: 6px 24px;
    background: #f2f4f5;
  }
  .ant-modal-title {
    font-family: var(--roboto-700);
    font-size: 16px;
  }
  .ant-modal-close-x {
    line-height: 36px;
    font-size: 16px;
    color: #000000;
  }
  .ant-form-item-label > label {
    display: flex;
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: white;
  position: sticky;
  bottom: 0;
  padding-top: 16px;
`;

const CustomButtonSave = styled(Button)`
  width: 80px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  width: 80px;
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover {
    color: #000;
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
