import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";

import RGL, { WidthProvider } from "react-grid-layout";
import { Button } from "antd";
import { useDispatch } from "react-redux";
import { loadLayouts } from "redux/slices/consolidatedViewSettings";

const ReactGridLayout = WidthProvider(RGL);

const ReactGrid = () => {
  const [list, setList] = useState([]);

  useEffect(() => {
    if (localStorage.getItem("local-grid")) {
      const a = JSON.parse(localStorage.getItem("local-grid"));
      setList(a);
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("local-grid", JSON.stringify(list));
  }, [list]);

  const renderContent = () => {
    return list.map((item) => {
      return (
        <CustomLayout
          data-grid={item}
          key={item.i}
          onDoubleClick={() => {
            let temp = [...list];
            temp = temp.filter((i) => {
              return i.i !== item.i;
            });
            setList(temp);
          }}
        >
          Number: <CallApi i={item.i}></CallApi>
        </CustomLayout>
      );
    });
  };

  return (
    <Wrap>
      <Button
        danger
        onClick={() => {
          let temp = [...list];
          temp.push({ i: Math.random(), x: 0, y: 0, w: 2, h: 1 });
          setList(temp);
        }}
      >
        Add
      </Button>
      <ReactGridLayout
        onLayoutChange={(layout) => {
          setList(layout);
        }}
      >
        {renderContent()}
      </ReactGridLayout>
    </Wrap>
  );
};

const CallApi = React.memo(({ i }) => {
  let dispatch = useDispatch();
  useEffect(() => {
    // if (i === "1") {
    dispatch(
      loadLayouts({
        object_id: "obj_hopdong_38293910",
        placement: "middle",
      })
    );
    // }
  }, [i, dispatch]);
  return <span>{i}</span>;
});

const Wrap = styled.div`
  width: 100%;
`;

const CustomLayout = styled.div`
  background: #0bb865 !important;
  font-size: 18px;
  color: #fff;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default ReactGrid;
