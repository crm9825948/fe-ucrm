import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";
import { Button } from "antd/lib";
import { useEffect, useState } from "react";
import ModalApply from "./ModalApply";
import ModalReview from "./ModalReview";
import { CALL_VIEW_TEMPLATES, handlerIconComp } from "util/staticData";
import { useDispatch, useSelector } from "react-redux";
import { getTemplateDefaultCallView } from "redux/slices/callView";
import _ from "lodash";
import { useLocation, useNavigate } from "react-router-dom";
import LayoutCallView2 from "components/CallView/LayoutCallView2";
import LayoutCallView1 from "components/CallView/LayoutCallView1";
import HttpCodePage from "components/httpCodePage";
import { loadComponents } from "redux/slices/consolidatedViewSettings";

const LIST_TEMPLATE = {
  1: <LayoutCallView1 />,
  2: <LayoutCallView2 />,
};

const CallViewSettings = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { search } = useLocation();
  const navigate = useNavigate();
  const queryParams = new URLSearchParams(search);
  const templateId = queryParams.get("template");
  const [openModal, setOpenModal] = useState(false);
  const [openModalReview, setOpenModalReview] = useState(false);
  const [templateSelected, setTemplateSelected] = useState(null);
  const [componentsCreated, $componentsCreated] = useState(null);
  const [listTemplates, setListTemplates] = useState(CALL_VIEW_TEMPLATES);
  const { templateDefault } = useSelector((state) => state.callViewReducer);
  const { components } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const handleReview = (value) => {
    setTemplateSelected(value);
    setOpenModalReview(true);
  };

  const normalizeComponents = (items) => {
    return _.map(
      items,
      (item) =>
        (item = {
          ...item,
          title: item.name,
          icon: handlerIconComp(
            _.get(item, "type") || _.get(item, "component_type")
          ),
        })
    );
  };

  useEffect(() => {
    dispatch(
      getTemplateDefaultCallView({
        object_id: "obj_contact_00000001",
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (_.get(templateDefault, "template") && !templateId) {
      navigate(
        `/call-view-settings?template=${_.get(templateDefault, "template")}`,
        { replace: true }
      );
    }
    //eslint-disable-next-line
  }, [templateDefault, navigate]);

  useEffect(() => {
    dispatch(
      loadComponents({
        object_id: "obj_contact_00000001",
      })
    );
  }, [dispatch]);

  useEffect(() => {
    $componentsCreated(normalizeComponents(components));
    //eslint-disable-next-line
  }, [components]);

  useEffect(() => {
    if (componentsCreated) {
      //Template 1
      let template1 = _.get(listTemplates, "[0]");
      if (_.get(template1, "components")) {
        _.get(template1, "components", []).splice(
          1,
          1,
          componentsCreated.find(
            (item) =>
              _.get(item, "type") === "details" && _.get(item, "auto_create")
          )
        );

        _.get(template1, "components", []).splice(
          2,
          1,
          componentsCreated.find(
            (item) =>
              _.get(item, "type") === "form" &&
              _.get(item, "title") === "Interaction call" &&
              _.get(item, "auto_create")
          )
        );
      }

      //Template 2
      let template2 = _.get(listTemplates, "[1]");
      _.get(template2, "components") &&
        _.get(template2, "components", []).splice(
          0,
          1,
          componentsCreated.find(
            (item) =>
              _.get(item, "type") === "multi_detail" &&
              _.get(item, "auto_create")
          )
        );

      setListTemplates([template1, template2]);
    }
    //eslint-disable-next-line
  }, [componentsCreated]);

  return (
    <Wrapper>
      {templateId ? (
        _.get(templateDefault, "template") === templateId.toString() ? (
          LIST_TEMPLATE[templateId]
        ) : (
          <HttpCodePage
            code={404}
            content={`The page doesn't exist!`}
            decs={`Sorry, the page you were looking for could not be found.`}
          />
        )
      ) : (
        <>
          <HeaderCallView>
            <h1>{t("CallView.title")}</h1>
            <div>{t("CallView.decs")}</div>
          </HeaderCallView>
          <WrapperTemplate>
            {listTemplates.map((item) => (
              <TemplateCustom key={_.get(item, "value")}>
                <ImgCustom src={_.get(item, "BG")} alt="Img" />
                <LayoutHover>
                  <div className="title">{t(_.get(item, "title"))}</div>
                  <p>{t(_.get(item, "desc"))}</p>
                </LayoutHover>
                <LayoutHover className="hover">
                  <div className="title">{t(_.get(item, "title"))}</div>
                  <p>{t(_.get(item, "desc"))}</p>
                  <WrapperButton>
                    <CustomButton onClick={() => handleReview(item)}>
                      {t("CallView.preview")}
                    </CustomButton>
                    <CustomButton
                      type="primary"
                      onClick={() => {
                        if (
                          _.get(templateDefault, "template") !==
                          _.get(item, "value")
                        ) {
                          setTemplateSelected(item);
                          setOpenModal(true);
                        }

                        if (
                          _.get(templateDefault, "template") ===
                          _.get(item, "value")
                        ) {
                          navigate(
                            `/call-view-settings?template=${_.get(
                              templateDefault,
                              "template"
                            )}`,
                            { replace: true }
                          );
                        }
                      }}
                    >
                      {templateDefault &&
                      _.get(templateDefault, "template") ===
                        _.get(item, "value")
                        ? t("CallView.applying")
                        : t("CallView.apply")}
                    </CustomButton>
                  </WrapperButton>
                </LayoutHover>
              </TemplateCustom>
            ))}
          </WrapperTemplate>
          {/* <History /> */}
          <ModalApply
            open={openModal}
            setOpen={setOpenModal}
            templateSelected={templateSelected}
          />
          <ModalReview
            open={openModalReview}
            setOpen={setOpenModalReview}
            templateSelected={templateSelected}
          />
        </>
      )}
    </Wrapper>
  );
};

export default withTranslation()(CallViewSettings);

const Wrapper = styled.div``;

const HeaderCallView = styled.div`
  padding: 57px 0 38px 0;
  border-bottom: 1px solid #ede9e9;

  h1 {
    font-size: 21px;
    font-family: var(--roboto-500);
    line-height: 25px;
    letter-spacing: 0em;
    text-align: center;
    color: #2c2c2c;
  }

  div {
    font-size: 14px;
    line-height: 22px;
    letter-spacing: 0em;
    text-align: center;
    color: #2c2c2c;
  }
`;

const WrapperTemplate = styled.div`
  padding: 40px 35px 60px 35px;
  display: flex;
  gap: 45px;
  border-bottom: 1px solid #ede9e9;
  flex-wrap: wrap;
`;

const ImgCustom = styled.img`
  width: 100%;
  height: auto;
`;

const LayoutHover = styled.div`
  &.hover {
    opacity: 0;
    height: 100%;
    background: rgba(255, 255, 255, 0.9);
    transition: 0.2s all ease-in-out;
  }
  opacity: 1;
  transition: 0.1s all ease-in-out;
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 30%;
  background: linear-gradient(180deg, rgba(255, 255, 255, 0.15) 0%, #fff 72.4%);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  gap: 10px;

  .title {
    font-size: 23px;
    font-family: var(--roboto-700);
    line-height: 22px;
    letter-spacing: 0em;
    text-align: center;
  }

  p {
    color: #4a4a4a;
    text-align: center;
    font-size: 14px;
    font-style: normal;
    line-height: normal;
    font-family: var(--roboto-400);
  }
`;

const WrapperButton = styled.div`
  gap: 12px;
  display: flex;
`;

const TemplateCustom = styled.div`
  width: calc(50% - 22.5px);
  position: relative;

  &:hover ${LayoutHover} {
    &.hover {
      opacity: 1;
      transition: 0.2s all ease-in-out;
    }
    opacity: 0;
    transition: 0.1s all ease-in-out;
  }
`;

const CustomButton = styled(Button)`
  width: 136px;

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-primary {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;
