import React, { memo, useEffect, useRef, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";
import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import ModalApply from "./ModalApply";
import _ from "lodash";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router";

const ModalReview = ({ open, setOpen, templateSelected }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const elementRef = useRef(null);
  const [openModal, setOpenModal] = useState(false);
  const { templateDefault } = useSelector((state) => state.callViewReducer);

  const _onSubmit = () => {
    setOpenModal(true);
  };

  const handleScrollToElement = () => {
    if (elementRef?.current) {
      setTimeout(() => {
        elementRef.current.scrollTop = 0;
      }, 100);
    }
  };

  useEffect(() => {
    if (open) {
      handleScrollToElement();
    }
  }, [open]);

  return (
    <CustomModal
      title={`${t("CallView.preview")} ${t(
        _.get(templateSelected, "title", "")
      ).toLowerCase()}`}
      centered
      open={open}
      onCancel={() => {
        setOpen(false);
      }}
      width={1200}
      footer={null}
    >
      <CustomContent ref={elementRef}>
        <ImgCustom src={_.get(templateSelected, "BG_Review")} alt="Img" />
      </CustomContent>
      <CustomFooter>
        <CustomButtonCancel onClick={() => setOpen(false)}>
          {t("CallView.close")}
        </CustomButtonCancel>
        <CustomButtonSave
          htmlType="submit"
          onClick={() => {
            if (
              _.get(templateDefault, "template") !==
              _.get(templateSelected, "value")
            )
              _onSubmit();

            if (
              _.get(templateDefault, "template") ===
              _.get(templateSelected, "value")
            ) {
              navigate(
                `/call-view-settings?template=${_.get(
                  templateDefault,
                  "template"
                )}`,
                { replace: true }
              );

              setOpen(false);
            }
          }}
        >
          {templateDefault &&
          _.get(templateDefault, "template") ===
            _.get(templateSelected, "value")
            ? t("CallView.applying")
            : t("CallView.apply")}
        </CustomButtonSave>
      </CustomFooter>
      <ModalApply
        open={openModal}
        setOpen={setOpenModal}
        setOpenReview={setOpen}
        templateSelected={templateSelected}
      />
    </CustomModal>
  );
};

export default withTranslation()(memo(ModalReview));

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 18px 24px;

  border-radius: 0px 0px 10px 10px;
  background: #fff;
  box-shadow: 0px 0px 16px 0px rgba(0, 0, 0, 0.16);
  z-index: 2;
`;

const CustomButtonSave = styled(Button)`
  min-width: 90px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-left: 16px;
  border-radius: 4px;

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;

const CustomButtonCancel = styled(Button)`
  min-width: 90px;
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  height: calc(100vh - 175px);
  overflow-y: auto;
  padding: 12px;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const CustomModal = styled(Modal)`
  margin: 16px 0;

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;

const ImgCustom = styled.img`
  width: 100%;
  height: auto;
`;
