import { memo } from "react";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import BaseTemplate from "assets/images/callView/BaseTemplateImg.png";
import { Button } from "antd";
import IconUser from "assets/icons/callView/IconUser.png";
import IconClock from "assets/icons/callView/IconClock.png";

const History = () => {
  const { t } = useTranslation();

  return (
    <Wrapper>
      <Title>{t("CallView.history")}</Title>
      <WrapperItems>
        <HistoryItem>
          <div className="left">
            <ImgCustom src={BaseTemplate} alt="Img" />
            <div className="wrapperContent">
              <TitleItem>Mẫu cở bản</TitleItem>
              <div className="Info">
                <DetailItem>
                  <img src={IconUser} alt="icon" />
                  <p>Admin</p>
                </DetailItem>
                <DetailItem>
                  <img src={IconClock} alt="icon" />
                  <p>19/12/2023 12:38</p>
                </DetailItem>
              </div>
            </div>
          </div>
          <div className="right">
            <CustomButton>Hoàn tác</CustomButton>
          </div>
        </HistoryItem>
        <HistoryItem>
          <div className="left">
            <ImgCustom src={BaseTemplate} alt="Img" />
            <div className="wrapperContent">
              <TitleItem>Mẫu cở bản</TitleItem>
              <div className="Info">
                <DetailItem>
                  <img src={IconUser} alt="icon" />
                  <p>Admin</p>
                </DetailItem>
                <DetailItem>
                  <img src={IconClock} alt="icon" />
                  <p>19/12/2023 12:38</p>
                </DetailItem>
              </div>
            </div>
          </div>
          <div className="right">
            <CustomButton>Hoàn tác</CustomButton>
          </div>
        </HistoryItem>
        <HistoryItem>
          <div className="left">
            <ImgCustom src={BaseTemplate} alt="Img" />
            <div className="wrapperContent">
              <TitleItem>Mẫu cở bản</TitleItem>
              <div className="Info">
                <DetailItem>
                  <img src={IconUser} alt="icon" />
                  <p>Admin</p>
                </DetailItem>
                <DetailItem>
                  <img src={IconClock} alt="icon" />
                  <p>19/12/2023 12:38</p>
                </DetailItem>
              </div>
            </div>
          </div>
          <div className="right">
            <CustomButton>Hoàn tác</CustomButton>
          </div>
        </HistoryItem>
        <HistoryItem>
          <div className="left">
            <ImgCustom src={BaseTemplate} alt="Img" />
            <div className="wrapperContent">
              <TitleItem>Mẫu cở bản</TitleItem>
              <div className="Info">
                <DetailItem>
                  <img src={IconUser} alt="icon" />
                  <p>Admin</p>
                </DetailItem>
                <DetailItem>
                  <img src={IconClock} alt="icon" />
                  <p>19/12/2023 12:38</p>
                </DetailItem>
              </div>
            </div>
          </div>
          <div className="right">
            <CustomButton>Hoàn tác</CustomButton>
          </div>
        </HistoryItem>
      </WrapperItems>
    </Wrapper>
  );
};

export default withTranslation()(memo(History));

const Wrapper = styled.div`
  padding: 24px 35px;
`;

const Title = styled.div`
  color: #2c2c2c;
  text-align: left;
  font-size: 16px;
  font-style: normal;
  font-family: var(--roboto-500);
  line-height: normal;
  margin-bottom: 20px;
`;

const WrapperItems = styled.div`
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  gap: 20px;
  flex-wrap: wrap;
`;

const HistoryItem = styled.div`
  width: 100%;
  border: 1px solid #ececec;
  background-color: #fff;
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-right: 16px;

  .left {
    display: flex;
    gap: 16px;

    .wrapperContent {
      padding: 12px 0;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      justify-content: space-between;

      .Info {
        display: flex;
        flex-direction: row;
        gap: 16px;
      }
    }
  }
`;

const ImgCustom = styled.img`
  width: 150px;
  height: auto;
`;

const TitleItem = styled.div`
  color: #2c2c2c;
  text-align: left;
  font-size: 16px;
  font-style: normal;
  font-family: var(--roboto-700);
  line-height: normal;
`;

const DetailItem = styled.div`
  display: flex;
  gap: 4px;

  img {
    object-fit: contain;
  }

  p {
    margin: 0;
    color: #637381;
    text-align: center;
    font-size: 14px;
    font-style: normal;
    font-family: var(--roboto-400);
    line-height: normal;
  }
`;

const CustomButton = styled(Button)`
  padding: 5px 16px;
  width: 114px;

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-primary {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;
