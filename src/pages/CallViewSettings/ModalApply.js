import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import React, { memo } from "react";
import styled from "styled-components";
import Icon from "assets/icons/callView/IconModal.png";
import { useTranslation, withTranslation } from "react-i18next";
import { useDispatch } from "react-redux";
import { updateTemplateCallView } from "redux/slices/callView";
import _ from "lodash";
import { useNavigate } from "react-router";

const ModalApply = ({ open, setOpen, setOpenReview, templateSelected }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const navigate = useNavigate();

  const _onSubmit = () => {
    dispatch(
      updateTemplateCallView({
        object_id: "obj_contact_00000001",
        template: _.get(templateSelected, "value"),
        components: _.get(templateSelected, "components"),
      })
    );

    setOpen(false);
    setOpenReview &&
      setTimeout(() => {
        setOpenReview(false);
      }, 300);

    setTimeout(() => {
      navigate(
        `/call-view-settings?template=${_.get(templateSelected, "value")}`,
        { replace: true }
      );
    }, 500);
  };

  return (
    <CustomModal
      title={t("CallView.confirm")}
      centered
      open={open}
      onCancel={() => {
        setOpen(false);
      }}
      width={400}
      footer={null}
    >
      <CustomContent>
        <img alt="" src={Icon} />
        <Title>{t("CallView.titleConfirm")}</Title>
        <Decs>{t("CallView.descConfirm")}</Decs>
      </CustomContent>
      <CustomFooter>
        <CustomButtonCancel size="large" onClick={() => setOpen(false)}>
          {t("CallView.cancel")}
        </CustomButtonCancel>
        <CustomButtonSave
          size="large"
          htmlType="submit"
          onClick={() => {
            _onSubmit();
          }}
        >
          {t("CallView.apply")}
        </CustomButtonSave>
      </CustomFooter>
    </CustomModal>
  );
};

export default withTranslation()(memo(ModalApply));

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-left: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  img {
    width: 70px;
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #2c2c2c;
  text-align: center;
`;

const Decs = styled.span`
  font-size: 14px;
  line-height: 22px;
  text-align: center;
  color: #595959;
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
