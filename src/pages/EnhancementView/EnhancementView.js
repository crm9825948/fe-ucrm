import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import {
  getListCategory,
  getListArticleDashboard,
  getTopview,
  getNewest,
  updateDataSearch,
  updateLoadMore,
} from "redux/slices/enhancementView";

import styled from "styled-components";
import { Input } from "antd";
import ArticleGroup from "./HomeView/ArticleGroup";
import TopView from "./HomeView/TopView";
import HeaderView from "./HeaderView";
import BackgrounImg from "assets/icons/knowledgeEnhancement/background.png";
import { useTranslation } from "react-i18next";
const { Search } = Input;

const EnhancementView = () => {
  const { objectId } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { listArticleDashboard, listArticleTopview, listArticleNewest } =
    useSelector((state) => state.enhancementViewReducer);

  const [valueSearch, setValueSearch] = useState("");
  const [config, setConfig] = useState({});

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  useEffect(() => {
    dispatch(getListCategory({ object_id: objectId }));
    dispatch(
      getListArticleDashboard({
        object_id: objectId,
        data: {
          search_data: {},
          limit: 0,
          page: 0,
        },
      })
    );
    dispatch(getTopview({ object_id: objectId }));
    dispatch(getNewest({ object_id: objectId }));
  }, [objectId, dispatch]);

  const renderListArticle = (list) => {
    return list.map((group, index) => {
      return <ArticleGroup key={index} group={group} config={config} />;
    });
  };

  return (
    <ContentWrap>
      <HeaderView objectId={objectId} />

      <Wrap style={{ display: "none" }}>
        <img src={BackgrounImg} alt="background" />
        <ContentSearch>
          <p>{t("knowledgeBase.help")}</p>
          <span>{t("knowledgeBase.desHelp")}</span>

          <SearchWrap>
            <CustomInputSearch
              allowClear
              placeholder={`${t("knowledgeBase.search")}...`}
              onPressEnter={(e) => {
                dispatch(updateLoadMore(false));
                dispatch(
                  updateDataSearch({
                    text_search: e.target.value,
                    page: 0,
                  })
                );

                navigate(`/knowledge-base-enhancement/${objectId}/search`);
              }}
              onChange={(e) => {
                setValueSearch(e.target.value);
              }}
            />
            <ButtonSearch
              onClick={() => {
                dispatch(updateLoadMore(false));
                dispatch(
                  updateDataSearch({
                    text_search: valueSearch,
                    page: 0,
                  })
                );

                navigate(`/knowledge-base-enhancement/${objectId}/search`);
              }}
            >
              {t("knowledgeBase.search")}
            </ButtonSearch>
          </SearchWrap>
        </ContentSearch>
      </Wrap>
      <WrapContent>
        <ArticleWrap>{renderListArticle(listArticleDashboard)}</ArticleWrap>
        <RightWrap>
          <SearchWrap>
            <CustomInputSearch
              allowClear
              placeholder={`${t("knowledgeBase.search")}...`}
              onSearch={(e) => {
                dispatch(updateLoadMore(false));
                dispatch(
                  updateDataSearch({
                    text_search: e,
                    page: 0,
                  })
                );

                navigate(`/knowledge-base-enhancement/${objectId}/search`);
              }}
              onChange={(e) => {
                setValueSearch(e);
              }}
            />
            <ButtonSearch
              style={{ display: "none" }}
              onClick={() => {
                dispatch(updateLoadMore(false));
                dispatch(
                  updateDataSearch({
                    text_search: valueSearch,
                    page: 0,
                  })
                );

                navigate(`/knowledge-base-enhancement/${objectId}/search`);
              }}
            >
              {t("knowledgeBase.search")}
            </ButtonSearch>
          </SearchWrap>
          <TopView
            setting={config.article_setting}
            listArticleTopview={listArticleTopview}
            isTop={true}
            isDetail={true}
          />
          <TopView
            setting={config.article_setting}
            listArticleTopview={listArticleNewest}
            isTop={false}
            isDetail={true}
          />
        </RightWrap>
      </WrapContent>
    </ContentWrap>
  );
};

export default EnhancementView;

const ContentWrap = styled.div`
  width: 100%;
  padding: 24px 24px 0 24px;
  color: #2c2c2c;
  font-size: 16px;
`;

const Wrap = styled.div`
  position: relative;
  width: 100%;
  img {
    width: 100%;
    object-fit: cover;
    height: 240px;
  }
  margin-bottom: 16px;
`;
const WrapContent = styled.div`
  width: 100%;
  display: flex;
  border-radius: 6px;
  border: 1px solid #ececec;
  background: #fff;
  overflow: hidden;
  padding: 24px;
`;

const ContentSearch = styled.div`
  padding: 24px;
  position: absolute;
  top: 0;
  left: 0;
  p {
    margin-bottom: 8px;
    font-size: 32px;
    font-family: var(--roboto-700);
  }
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none !important;
  }
`;

const CustomInputSearch = styled(Search)`
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }
  button {
    height: 40px;
  }
`;

const SearchWrap = styled.div`
  display: flex;
  padding-left: 24px;
  border-left: 1px solid #ececec;
  padding-bottom: 10px;
  .ant-input-affix-wrapper {
    height: 40px;
  }
`;

const ButtonSearch = styled.div`
  cursor: pointer;
  margin-left: 16px;
  padding: 6px 20px;

  color: #fff;
  border-radius: 2px;
  background: ${(props) => props.theme.main};
  transition: all 0.5s;
  :hover {
    background: ${(props) => props.theme.darker};
  }
`;

const ArticleWrap = styled.div`
  overflow: auto;
  flex: 1;
`;

const RightWrap = styled.div`
  display: block;
  overflow: auto;
  width: 536px;
  @media screen and (max-width: 1366px) {
    display: none;
  }
`;
