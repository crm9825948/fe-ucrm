import React from "react";
import styled from "styled-components";
import { Modal } from "antd";
import ItemCompare from "./ItemCompare";
import { useTranslation } from "react-i18next";

const ModalCompare = ({ open, setOpen, data }) => {
  const { t } = useTranslation();
  return (
    <CustomModal
      title="History"
      visible={open}
      onCancel={() => setOpen(false)}
      footer={null}
      width={1200}
    >
      <ContenWrap>
        <Content>
          <ItemCompare data={data} />
        </Content>
        <Content>
          <ItemCompare data={data} isTo />
        </Content>
      </ContenWrap>
      <ButtonWrap>
        <CustomButton onClick={() => setOpen(false)}>
          {t("common.close")}
        </CustomButton>
      </ButtonWrap>
    </CustomModal>
  );
};

export default ModalCompare;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const ContenWrap = styled.div`
  display: flex;
  justify-content: space-between;
  max-height: 600px;
  overflow-y: auto;
  overflow-x: hidden;
`;
const Content = styled.div`
  width: 50%;
`;

const ButtonWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  cursor: pointer;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }
`;
