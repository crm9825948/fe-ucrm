import React from "react";
import styled from "styled-components";
import parse from "html-react-parser";

const ItemCompare = ({ data, isTo }) => {
  return (
    <Wrap isTo={isTo}>
      <Title>
        <span>{isTo ? "To" : "From"}</span>
        {isTo && <span>{data.created_date}</span>}
      </Title>

      <Content>
        {isTo
          ? parse(data.new_value ? data.new_value : "")
          : parse(data.old_value ? data.old_value : "")}
      </Content>
    </Wrap>
  );
};

export default ItemCompare;

const Wrap = styled.div`
  width: 100%;
  padding-left: ${(props) => (props.isTo ? "16px" : "0px")};
  padding-right: ${(props) => (props.isTo ? "0px" : "16px")};
  box-shadow: ${(props) =>
    props.isTo
      ? "inset 1px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9"
      : "inset 0px 0px 0px #d9d9d9, inset -1px 0px 0px #d9d9d9, inset 0px 0px 0px #d9d9d9"};
  margin-left: ${(props) => (props.isTo ? "-1px" : "0px")};
`;
const Title = styled.div`
  display: flex;
  justify-content: space-between;
  span {
    font-size: 16px;
    color: #2c2c2c;
    :first-child {
      font-family: var(--roboto-500);
    }
  }
`;

const Content = styled.div`
  font-size: 16px;
  width: 100%;
  overflow-x: auto;
  overflow-y: hidden;
  p {
    margin-bottom: 0;
  }
`;
