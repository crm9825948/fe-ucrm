import React, { useEffect } from "react";
import styled from "styled-components";
import OptionSearch from "./SearchView/OptionSearch";
import ResultSearch from "./SearchView/ResultSearch";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { useParams } from "react-router";
import { getListArticleSearch } from "redux/slices/enhancementView";
import HeaderView from "./HeaderView";

const EnhancementSearch = () => {
  const dateFormatSecond = "YYYY-MM-DD HH:mm:ss";
  const dispatch = useDispatch();
  const { objectId } = useParams();

  const { dataSearch, objectSearch, isLoadMore } = useSelector(
    (state) => state.enhancementViewReducer
  );

  useEffect(() => {
    let tem = { ...dataSearch };
    if (!tem.text_search) {
      delete tem.text_search;
    }
    if (tem.category === "all") {
      delete tem.category;
    }

    if (tem.created_date === "all") {
      delete tem.created_date;
    } else {
      let objectTime = {};
      switch (tem.created_date) {
        case "day":
          objectTime = {
            gte: moment()
              .subtract(1, "day")
              .startOf("day")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "day")
              .endOf("day")
              .format(dateFormatSecond),
          };
          break;
        case "week":
          objectTime = {
            gte: moment()
              .subtract(1, "week")
              .startOf("isoWeek")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "week")
              .endOf("isoWeek")
              .format(dateFormatSecond),
          };
          break;
        case "month":
          objectTime = {
            gte: moment()
              .subtract(1, "month")
              .startOf("month")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "month")
              .endOf("month")
              .format(dateFormatSecond),
          };
          break;
        case "year":
          objectTime = {
            gte: moment()
              .subtract(1, "year")
              .startOf("year")
              .format(dateFormatSecond),
            lte: moment()
              .subtract(1, "year")
              .endOf("year")
              .format(dateFormatSecond),
          };
          break;
        default:
          break;
      }
      tem.created_date = objectTime;
    }
    if (tem.sort === "related") {
      delete tem.sort;
    } else {
      let objectSort = {};
      switch (tem.sort) {
        case "newest":
          objectSort = { field: "created_date", order: "desc" };
          break;
        case "oldest":
          objectSort = { field: "created_date", order: "asc" };
          break;
        case "viewascending":
          objectSort = { field: "total_views", order: "asc" };
          break;
        case "viewdecrease":
          objectSort = { field: "total_views", order: "desc" };
          break;
        case "upvoteascending":
          objectSort = { field: "up_votes", order: "asc" };
          break;
        case "upvotedecrease":
          objectSort = { field: "up_votes", order: "desc" };
          break;
        default:
          break;
      }
      tem.sort = objectSort;
    }

    if (dataSearch)
      dispatch(
        getListArticleSearch({
          object_id: objectId,
          data: tem,
        })
      );
  }, [dispatch, dataSearch, objectId]);

  return (
    <Wrap>
      <HeaderView objectId={objectId} />
      <Content>
        <OptionSearch />
        <ResultSearch
          dataSearch={dataSearch}
          objectSearch={objectSearch}
          isLoadMore={isLoadMore}
        />
      </Content>
    </Wrap>
  );
};

export default EnhancementSearch;

const Wrap = styled.div`
  padding: 24px 24px 0 24px;
  justify-content: center;
  flex-wrap: wrap;
  display: flex;
`;

const Content = styled.div`
  display: flex;
  width: 1140px;
`;
