import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  getArticleDetail,
  getListArticleFilter,
  getListCategory,
  getNewest,
  getTopview,
} from "redux/slices/enhancementView";
import { checkTokenExpiration } from "contexts/TokenCheck";
import styled from "styled-components";
import TreeCategory from "./DetailView/TreeCategory";
import Details from "./DetailView/Details";
// import TopView from "./HomeView/TopView";
import ResultFilter from "./SearchView/ResultFilter";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import HeaderView from "./HeaderView";

const EnhancementDetail = () => {
  const { objectId, recordID, categoryId } = useParams();

  const dispatch = useDispatch();
  const {
    listCategory,
    // listArticleTopview,
    // listArticleNewest,
    articleDetail,
    objectFilter,
    isLoadMore,
  } = useSelector((state) => state.enhancementViewReducer);
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const [config, setConfig] = useState({});

  const [list, setList] = useState([]);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  useEffect(() => {
    dispatch(getListCategory({ object_id: objectId }));
    dispatch(getTopview({ object_id: objectId }));
    dispatch(getNewest({ object_id: objectId }));
  }, [objectId, dispatch]);

  useEffect(() => {
    if (recordID && objectId) {
      dispatch(
        getArticleDetail({
          object_id: objectId,
          record_id: recordID,
        })
      );
    }
  }, [recordID, objectId, dispatch]);
  useEffect(() => {
    if (categoryId && objectId) {
      dispatch(
        getListArticleFilter({
          object_id: objectId,
          data: {
            category: categoryId,
            page: 0,
            size: 10,
          },
        })
      );
    }
  }, [dispatch, categoryId, objectId]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (listCategory.length > 0 && Object.entries(config).length > 0) {
        axios
          .post(
            BASE_URL_API + "list-view/load-data",
            {
              object_id: config.category_setting.obj_category,
              current_page: 1,
              record_per_page: 10,
              search_with: {
                meta: [],
                data: "",
              },
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            if (res.data.data) {
              let tem = [];
              res.data.data.forEach((category) => {
                tem.push({
                  label: category[config.category_setting.fld_category]?.value,
                  value: category._id,
                });
              });
              setList(tem);
            }
          })
          .catch((error) => Notification("error", error.response.data.error));
      }
    };
    checkToken();
  }, [config, listCategory]);

  return (
    <Wrap>
      <HeaderView objectId={objectId} />
      <Content>
        <TreeCategory listCategory={listCategory} list={list} />

        {Object.entries(objectFilter).length > 0 ? (
          <ResultFilter
            objectFilter={objectFilter}
            isLoadMore={isLoadMore}
            list={list}
          />
        ) : (
          <Details
            article={articleDetail.article ? articleDetail.article : {}}
            stats={articleDetail.stats ? articleDetail.stats : {}}
            config={config}
            objectId={objectId}
            recordID={recordID}
          />
        )}
        {/* <Col xl={7} xxl={7}>
            <RightWrap>
              <TopView
                setting={config.article_setting}
                listArticleTopview={listArticleTopview}
                isTop={true}
              />
              <TopView
                setting={config.article_setting}
                listArticleTopview={listArticleNewest}
                isTop={false}
              />
            </RightWrap>
          </Col> */}
      </Content>
    </Wrap>
  );
};

export default EnhancementDetail;

const Wrap = styled.div`
  padding: 24px 24px 0 24px;
  justify-content: center;
  flex-wrap: wrap;
  display: flex;
`;

const Content = styled.div`
  display: flex;
  width: 1140px;
`;

// const RightWrap = styled.div`
//   max-height: calc(100vh - 186px);
//   overflow: auto;
//   margin-top: 24px;
// `;
