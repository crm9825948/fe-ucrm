import React, { useState } from "react";
import styled from "styled-components";
import {
  CaretDownOutlined,
  PlusOutlined,
  MinusOutlined,
} from "@ant-design/icons";
import {
  Tooltip,
  Tree,
  Typography,
  Popover,
  Input,
  Dropdown,
  Checkbox,
  Collapse,
} from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import DeleteRoles from "assets/icons/roles/DeleteRoles.svg";
import EditRoles from "assets/icons/roles/EditRoles.svg";
import PlusRoles from "assets/icons/roles/PlusRoles.svg";
import FilterIcon from "assets/icons/knowledgeEnhancement/fillter.svg";
import { Radio } from "antd";
import { useNavigate, useParams } from "react-router";
import { updateDataSearch } from "redux/slices/enhancementView";
import ModalCategory from "../ModalCategory";

const { Text } = Typography;
const { Search } = Input;
const { Panel } = Collapse;
const TreeCategory = ({ listCategory, list }) => {
  const { t } = useTranslation();
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const { objectId } = useParams();
  const [objectSearch, setObjectSearch] = useState({
    text_fields: ["title", "body", "tag", "article_id"],
    created_date: "all",
  });

  const [objectModal, setObjectModal] = useState({});

  const [openModal, setOpenmodal] = useState(false);
  const navigate = useNavigate();

  const onSelect = (selectedKeys, info) => {
    navigate(
      `/knowledge-base-enhancement/${objectId}/category/${info.node.key}`
    );
  };

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  const action = (name, id) => (
    <WrapAction>
      <Tooltip title={t("common.add")}>
        <img
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setOpenmodal(true);

            let temp = { key: "add", name: name, id: id };
            setObjectModal(temp);
          }}
          src={PlusRoles}
          alt="add"
        />
      </Tooltip>

      <Tooltip title={t("common.edit")}>
        <img
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setOpenmodal(true);

            let temp = { key: "edit", name: name, id: id };
            setObjectModal(temp);
          }}
          src={EditRoles}
          alt="edit"
        />
      </Tooltip>

      <Tooltip title={t("common.delete")}>
        <img
          onClick={(e) => {
            e.stopPropagation();
            e.preventDefault();
            setOpenmodal(true);
            let temp = { key: "delete", name: name, id: id };
            setObjectModal(temp);
          }}
          src={DeleteRoles}
          alt="delete"
        />
      </Tooltip>
    </WrapAction>
  );

  const menu = (
    <PopupWrap>
      <Title>{t("knowledgeBase.sortBy")}</Title>
      <Checkbox.Group
        defaultValue={["title", "body", "tag", "article_id"]}
        value={objectSearch.text_fields}
        onChange={(value) => {
          setObjectSearch((prev) => ({
            ...prev,
            text_fields: value,
          }));
        }}
      >
        <Checkbox value="title">{t("knowledgeBase.title")}</Checkbox>
        <Checkbox value="body">{t("knowledgeBase.content")}</Checkbox>
        <Checkbox value="tag">{t("knowledgeBase.tag")}</Checkbox>
        <Checkbox value="article_id">ID</Checkbox>
      </Checkbox.Group>
      <Title>{t("knowledgeBase.time")}</Title>
      <CustomRadio
        defaultValue="all"
        value={objectSearch.created_date}
        onChange={(value) => {
          setObjectSearch((prev) => ({
            ...prev,
            created_date: value.target.value,
          }));
        }}
        color={hexToRGB(defaultBrandName.theme_color)}
      >
        <Radio.Button value="all">{t("knowledgeBase.all")}</Radio.Button>
        <Radio.Button value="day">{t("knowledgeBase.day")}</Radio.Button>
        <Radio.Button value="week">{t("knowledgeBase.week")}</Radio.Button>
        <Break />
        <Radio.Button value="month">{t("knowledgeBase.month")}</Radio.Button>
        <Radio.Button value="year">{t("knowledgeBase.year")}</Radio.Button>
      </CustomRadio>
      <ButtonWrap>
        <Button
          onClick={() => {
            setObjectSearch({
              text_fields: ["title", "body", "tag", "article_id"],
              created_date: "all",
            });
            setOpen(false);
          }}
        >
          {t("common.cancel")}
        </Button>
        <Button onClick={() => setOpen(false)}>
          {t("knowledgeBase.done")}
        </Button>
      </ButtonWrap>
    </PopupWrap>
  );

  const renderListPanel = (list) =>
    list.map((parent) => (
      <Panel
        key={parent.key}
        header={
          <Popover
            content={action(parent.title, parent.key)}
            placement="right"
            trigger="hover"
            overlayClassName="popoverRoles"
          >
            <Text
              ellipsis={{ tooltip: parent.title }}
              onClick={(e) =>
                navigate(
                  `/knowledge-base-enhancement/${objectId}/category/${parent.key}`
                )
              }
            >
              {parent.title}
            </Text>
          </Popover>
        }
        style={{
          marginBottom: "24px",
          borderRadius: "10px",
          background: "#f9fafc",
          boxShadow: "0px 2px 8px 0px rgba(0, 0, 0, 0.15)",
        }}
      >
        <Tree
          showLine={{ showLeafIcon: false }}
          switcherIcon={<CaretDownOutlined style={{ color: "#006964" }} />}
          onSelect={onSelect}
          treeData={parent.children}
          titleRender={(nodeData) => (
            <Popover
              content={action(nodeData.title, nodeData.key)}
              placement="right"
              trigger="hover"
              overlayClassName="popoverRoles"
            >
              <Text ellipsis={{ tooltip: nodeData.title }}>
                {nodeData.title}
              </Text>
            </Popover>
          )}
        />
      </Panel>
    ));

  return (
    <Wrap color={hexToRGB(defaultBrandName.theme_color)}>
      <Header>{t("knowledgeBase.category")}</Header>
      <FilterWrap>
        <CustomInputSearch
          placeholder={`${t("knowledgeBase.search")}...`}
          onSearch={(e) => {
            dispatch(
              updateDataSearch({ ...objectSearch, text_search: e, page: 0 })
            );
            navigate(`/knowledge-base-enhancement/${objectId}/search`);
          }}
        />
        <CustomDropdown
          visible={open}
          overlay={menu}
          placement="bottomCenter"
          trigger="click"
          overlayClassName="object-view"
          onVisibleChange={() => setOpen(!open)}
        >
          <ButtonFilter>
            <img src={FilterIcon} alt="filter" />
          </ButtonFilter>
        </CustomDropdown>
      </FilterWrap>
      {/* <Tree
        showLine={{ showLeafIcon: false }}
        switcherIcon={<CaretDownOutlined />}
        onSelect={onSelect}
        treeData={listCategory}
        titleRender={(nodeData) => (
          <Popover
            content={action(nodeData.title, nodeData.key)}
            placement="right"
            trigger="hover"
            overlayClassName="popoverRoles"
          >
            <Text ellipsis={{ tooltip: nodeData.title }}>{nodeData.title}</Text>
          </Popover>
        )}
      /> */}
      <CustomCollapse
        expandIconPosition={"end"}
        bordered={false}
        expandIcon={({ isActive }) =>
          isActive ? <MinusOutlined /> : <PlusOutlined />
        }
      >
        {renderListPanel(listCategory)}
      </CustomCollapse>

      <ModalCategory
        openModal={openModal}
        setOpenmodal={setOpenmodal}
        list={list}
        objectModal={objectModal}
      />
    </Wrap>
  );
};

export default TreeCategory;

const Wrap = styled.div`
  padding: 16px;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ececec;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  height: fit-content;
  width: 350px;
  min-width: 350px;
  margin-right: 16px;
  .ant-tree-title {
    font-size: 16px;
    color: #2c2c2c;
  }
  .ant-tree-show-line .ant-tree-switcher {
    background: transparent;
    color: #252424;
  }
  .ant-tree-switcher {
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .ant-tree-switcher .ant-tree-switcher-icon {
    font-size: 14px;
    vertical-align: unset;
  }
  .ant-tree .ant-tree-node-content-wrapper.ant-tree-node-selected {
    /* background-color: ${(props) => props.color};
     */
    background-color: transparent;
    .ant-tree-title {
      /* color: ${(props) => props.theme.main}; */
      color: #2c2c2c;
      .ant-typography {
        /* color: ${(props) => props.theme.main}; */
        color: #2c2c2c;
      }
    }
  }
  .ant-tree .ant-tree-node-content-wrapper {
    padding: 0px 8px;
  }
  .ant-tree-treenode,
  .ant-tree-node-content-wrapper,
  .ant-collapse-header .ant-collapse-header-text {
    max-width: 100%;
  }

  .ant-tree {
    /* overflow: auto; */
    /* max-height: calc(100vh - 310px); */
    background: transparent;
  }
`;

const WrapAction = styled.div`
  img {
    margin-right: 10px;

    :hover {
      background: #ececec;
      cursor: pointer;
    }
  }
`;

const FilterWrap = styled.div`
  width: 100%;
  display: flex;
  margin-bottom: 12px;
`;

const CustomInputSearch = styled(Search)`
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 40px;
  }
  button {
    height: 40px;
  }
`;

const ButtonFilter = styled.div`
  border: 1px solid #d9d9d9 !important;
  width: 48px;
  cursor: pointer;
  margin-left: 12px;
  border-radius: 2px;
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  text-align: center;

  color: #2c2c2c;

  display: flex;
  justify-content: center;
  align-items: center;

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const PopupWrap = styled.div`
  padding: 16px;
  font-size: 16px;
  line-height: 22px;
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main}!important;
  }
  .ant-checkbox + span {
    font-size: 16px;
  }
  .ant-checkbox + span {
    margin-bottom: 8px;
  }
`;

const Title = styled.div`
  font-family: var(--roboto-500);
  color: #2c2c2c;
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 8px;
`;

const CustomRadio = styled(Radio.Group)`
  .ant-radio-button-wrapper {
    border: 1px solid #d9d9d9 !important;
    margin-right: 8px;
    border-radius: 5px;
    font-size: 16px;
    :hover {
      border-color: ${(props) => props.theme.main} !important ;
    }
  }
  .ant-radio-button-wrapper-checked {
    color: ${(props) => props.theme.main};
    background: ${(props) => props.color};
    border-color: ${(props) => props.theme.main} !important ;
  }

  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }

  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-input:focus + .ant-radio-inner {
    box-shadow: none;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper:not(:first-child)::before {
    width: 0;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):focus-within {
    box-shadow: none;
  }
`;

const Break = styled.div`
  margin-bottom: 8px;
`;

const ButtonWrap = styled.div`
  display: flex;
  justify-content: end;
  margin-top: 16px;
`;

const Button = styled.div`
  width: 70px;
  height: 32px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 16px;
  cursor: pointer;
  transition: all 0.5s;

  :last-child {
    border-color: ${(props) => props.theme.darker};
    background: ${(props) => props.theme.darker};
    color: #fff;
  }

  :hover {
    border-color: ${(props) => props.theme.darker};
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;
const Header = styled.div`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  margin-bottom: 8px;
`;

const CustomCollapse = styled(Collapse)`
  background: #fff;
  .ant-collapse-item {
    border: none;
  }
  .ant-collapse {
    background: #fff;
  }
  .ant-collapse-header {
    font-size: 18px;
    font-family: var(--roboto-500);
    padding: 16px 16px 21px 16px !important;
    svg {
      font-size: 16px;
    }
  }
  .ant-collapse-content-active {
    background: #fbfbfb !important;
  }
  .ant-tree-treenode {
    .ant-tree-indent {
      .ant-tree-indent-unit {
        &::before {
          border-inline-end: 1px dashed #006964;
          top: 2px !important;
        }
      }
    }

    .ant-tree-switcher {
      .ant-tree-switcher-leaf-line {
        &::before {
          border-inline-end: 1px dashed #006964;
          top: 1px !important;
        }

        &::after {
          border-bottom: 1px dashed #006964;
          top: 1px;
          right: 0px;
        }
      }
    }
  }
  .ant-collapse-item .ant-collapse-header::before {
    position: absolute;
    left: 50%;
    bottom: 16px;
    transform: translateX(-50%);
    content: "";
    width: 94%;
    height: 1px;
    background-color: rgb(0, 177, 161);
  }
  .ant-collapse-header .ant-collapse-arrow {
    width: 24px;
    height: 24px;
    display: flex !important;
    justify-content: center;
    align-items: center;
    background: #00b1a1;
    border-radius: 5px;
    top: 26px !important;
    svg {
      color: #fff;
    }
  }
`;
