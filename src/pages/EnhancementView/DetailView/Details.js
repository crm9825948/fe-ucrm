import React, { useState, useEffect } from "react";
import styled from "styled-components";
import {
  EyeFilled,
  LikeFilled,
  // DislikeFilled,
  CalendarFilled,
} from "@ant-design/icons";
import _ from "lodash";
import parse from "html-react-parser";
import pdfIcon from "assets/icons/knowledgeEnhancement/pdfIcon.png";
import excelIcon from "assets/icons/knowledgeEnhancement/excelIcon.png";
import wordIcon from "assets/icons/knowledgeEnhancement/wordIcon.png";
import fileIcon from "assets/icons/knowledgeEnhancement/defaultIcon.png";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteArticle,
  getArticleDetail,
  getListFieldObject,
} from "redux/slices/enhancementView";
import CopyImg from "assets/icons/knowledgeEnhancement/LinkIcon.svg";

import EditImg from "assets/icons/knowledgeEnhancement/edit.svg";
import DeleteImg from "assets/icons/knowledgeEnhancement/delete.svg";

import { Tooltip } from "antd";
import { Notification } from "components/Notification/Noti";
import { FE_URL, BE_URL } from "constants/constants";
import { useNavigate } from "react-router";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import { useTranslation } from "react-i18next";
import Comment from "pages/ConsolidatedView/componentType/comment";
import { optionsParse } from "util/staticData";
import AuthenImage from "components/Image/AuthenImage";

const Details = ({ article, stats, config, objectId, recordID }) => {
  const { article_setting } = config;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [showConfirmDelete, setShowConfirmDelete] = useState(false);
  const { allObject } = useSelector((state) => state.tenantsReducer);
  const { listFieldObject } = useSelector(
    (state) => state.enhancementViewReducer
  );
  const [edit, setEdit] = useState(false);
  const [isDelete, setIsDelete] = useState(false);
  useEffect(() => {
    dispatch(
      getListFieldObject({
        data: {
          api_version: "2",
          object_id: objectId,
          show_meta_fields: true,
        },
      })
    );
  }, [dispatch, objectId]);

  useEffect(() => {
    listFieldObject.forEach((item, idx) => {
      if (Object.keys(item)[0] === "main_object") {
        setEdit(item[Object.keys(item)[0]]?.edit_permission);
        setIsDelete(item[Object.keys(item)[0]]?.delete_permission);
      }
    });
  }, [listFieldObject]);

  const renderTag = (setting, article) => {
    if (_.get(article, `${setting?.fld_tag}.value`, "")) {
      return _.get(article, `${setting?.fld_tag}.value`, "")
        .split("#")
        .map((tag, index) => tag && <Tag key={index}>#{tag.trim()}</Tag>);
    } else {
      return "";
    }
  };

  const renderSrc = (setting, article) => {
    if (_.get(article, `${setting?.fld_thumbnail}.value`, [])) {
      return _.get(article, `${setting?.fld_thumbnail}.value`, [])[0];
    } else {
      return "";
    }
  };

  const renderListFile = (list) => {
    if (list) {
      return list.map((item, index) => {
        const arr = item.split("/");
        const type = arr[arr.length - 1].split(".")[1];
        let icon = "";
        switch (type) {
          case "pdf":
            icon = pdfIcon;
            break;
          case "xlsx":
            icon = excelIcon;
            break;
          case "doc":
            icon = wordIcon;
            break;

          default:
            icon = fileIcon;
            break;
        }
        if (list) {
          return (
            <File
              key={index}
              href={
                allObject?.find(
                  (object) => object?.object_id === objectId && object?.status
                )
                  ? `${FE_URL + item?.split(BE_URL)[1]}`
                  : item
              }
              rel="noreferrer"
              target="_blank"
            >
              <img src={icon} alt="" />
              <span>{arr[arr.length - 1]}</span>
            </File>
          );
        } else {
          return "";
        }
      });
    }
  };

  const handleRightClick = (event) => {
    event.preventDefault();
  };

  return (
    <div style={{ position: "relative", flex: "1", height: "fit-content" }}>
      {Object.entries(article).length > 0 && (
        <Wrap>
          <Header>{article[article_setting?.fld_category]?.value}</Header>
          {renderSrc(article_setting, article) && (
            <ImgWrap>
              <AuthenImage src={renderSrc(article_setting, article)} />
              {/* <img src={renderSrc(article_setting, article)} alt="Thumbnail" /> */}
            </ImgWrap>
          )}
          <Title>
            <span>
              {_.get(
                article,
                `${article_setting?.fld_title}.value`,
                "No title"
              )}
              <Tooltip title={t("knowledgeBase.copy")}>
                <img
                  src={CopyImg}
                  alt="Copy"
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    navigator.clipboard.writeText(
                      `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
                    );
                    Notification("success", "Copy link article success");
                  }}
                />
              </Tooltip>
              <Tooltip title={t("knowledgeBase.edit")}>
                <IconAction
                  onClick={() => {
                    if (edit) {
                      navigate(
                        `/knowledge-base-enhancement/edit-article/${objectId}/${recordID}`
                      );
                    } else {
                      Notification(
                        "warning",
                        "Bạn không có quyền để chỉnh sửa!"
                      );
                    }
                  }}
                >
                  <img src={EditImg} alt="edit" />
                </IconAction>
              </Tooltip>
              <Tooltip title={t("knowledgeBase.delete")}>
                <IconAction
                  onClick={() => {
                    if (isDelete) {
                      setShowConfirmDelete(true);
                    } else {
                      Notification("warning", "Bạn không có quyền để xóa!");
                    }
                  }}
                >
                  <img src={DeleteImg} alt="delete" />
                </IconAction>
              </Tooltip>
            </span>
          </Title>

          <TagWrap>{renderTag(article_setting, article)}</TagWrap>

          <Content onContextMenu={handleRightClick}>
            {_.get(article, `${article_setting?.fld_body}.value`, "No body") &&
              parse(
                _.get(article, `${article_setting?.fld_body}.value`, "No body"),
                optionsParse
              )}
          </Content>

          <FileWrap>
            {renderListFile(article[article_setting?.fld_attachments]?.value)}
          </FileWrap>

          <ActionWrap>
            {config.show_total_views && (
              <Action>
                <EyeFilled />
                {_.get(
                  article,
                  `${article_setting?.fld_total_views}.value`,
                  "0"
                )
                  ? _.get(
                      article,
                      `${article_setting?.fld_total_views}.value`,
                      "0"
                    )
                  : "0"}
              </Action>
            )}
            {config.show_votes && (
              <Action
                isAction={stats.is_upvoted}
                onClick={() => {
                  if (!stats.is_upvoted) {
                    dispatch(
                      getArticleDetail({
                        object_id: objectId,
                        record_id: `${recordID}?action=upvote`,
                      })
                    );
                  } else {
                    dispatch(
                      getArticleDetail({
                        object_id: objectId,
                        record_id: `${recordID}?action=downvote`,
                      })
                    );
                  }
                }}
              >
                <LikeFilled />
                {_.get(article, `${article_setting?.fld_upvotes}.value`, "0")
                  ? _.get(article, `${article_setting?.fld_upvotes}.value`, "0")
                  : "0"}
              </Action>
            )}
            {/* {config.show_votes && (
              <Action
                isAction={stats.is_downvoted}
                onClick={() => {
                  if (!stats.is_downvoted) {
                    dispatch(
                      getArticleDetail({
                        object_id: objectId,
                        record_id: `${recordID}?action=downvote`,
                      })
                    );
                  }
                }}
              >
                <DislikeFilled />
                {_.get(article, `${article_setting?.fld_downvotes}.value`, "0")
                  ? _.get(
                      article,
                      `${article_setting?.fld_downvotes}.value`,
                      "0"
                    )
                  : "0"}
              </Action>
            )} */}
            <Action>
              <CalendarFilled /> {article.created_date}
            </Action>
          </ActionWrap>

          {recordID && (
            <Comment objectId={objectId} recordID={recordID} isArticle />
          )}
          <ModalConfirmDelete
            title={t("knowledgeBase.thisArt")}
            decs={t("knowledgeBase.desDelete")}
            methodDelete={deleteArticle}
            dataDelete={{
              service: "article",
              object_id: objectId,
              data: {
                record_id: recordID,
              },
            }}
            isLoading={false}
            openConfirm={showConfirmDelete}
            setOpenConfirm={setShowConfirmDelete}
          />
        </Wrap>
      )}
    </div>
  );
};

export default Details;

const Wrap = styled.div`
  font-size: 16px;
  line-height: 22px;
  padding: 24px;
  overflow: auto;
  /* max-height: calc(100vh - 176px); */
  border-radius: 6px;
  border: 1px solid #ececec;
  background: #fff;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
`;

const ImgWrap = styled.div`
  width: 100%;
  margin-bottom: 24px;
  img {
    width: 100%;
    object-fit: cover;
    height: 300px;
  }
`;

const Title = styled.div`
  font-size: 18px;
  font-family: var(--roboto-500);
  margin: 10px 0;
  display: flex;
  align-items: flex-end;
  /* span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  } */

  img {
    margin-bottom: 3px;
    margin-left: 6px;
    width: 16px;
    cursor: pointer;
  }
`;

const Content = styled.div`
  * {
    max-width: 100%;
  }
  font-size: 16px;
  overflow: hidden;
  p {
    margin-bottom: 0px;
  }
`;

const TagWrap = styled.div`
  display: flex;
  margin: 8px 0 16px 0;
`;

const Tag = styled.div`
  background: rgba(32, 162, 162, 0.15);
  border-radius: 5px;
  color: #0a7979;
  line-height: 24px;
  padding: 0 6px;
  margin-right: 8px;
`;

const ActionWrap = styled.div`
  display: flex;
`;
const Action = styled.div`
  margin-right: 16px;
  font-size: 12px;
  line-height: 16px;
  color: #6b6b6b;
  font-family: var(--roboto-500);
  display: flex;
  margin-top: 16px;
  margin-bottom: 8px;
  svg {
    font-size: 16px;
    margin-right: 6px;
    cursor: pointer;
    color: ${(props) => (props.isAction ? "#096DD9" : "#7E7E7E")};
  }
`;

const FileWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 8px;
  margin-bottom: 16px;
`;

const File = styled.a`
  border: 1px solid #efeff4;
  border-radius: 5px;
  display: flex;
  align-items: center;
  width: 200px;
  margin-right: 16px;
  height: 48px;
  padding: 0 8px;
  font-family: var(--roboto-500);
  font-size: 14px;
  line-height: 22px;
  margin-top: 16px;
  color: #2c2c2c;
  :hover {
    color: #2c2c2c;
  }
  span {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;
const Header = styled.div`
  font-size: 24px;
  font-family: var(--roboto-500);
  font-size: 22px;
  line-height: 24px;
  margin-bottom: 20px;
`;

const IconAction = styled.div`
  cursor: pointer;
  display: inline-block;
  margin-left: 8px;
  img {
    width: 15px;
    margin: 0;
    margin-bottom: 5px;
  }
`;
