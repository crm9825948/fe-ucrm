import React from "react";
import {
  EyeFilled,
  LikeFilled,
  // DislikeFilled,
  CalendarFilled,
} from "@ant-design/icons";
import parse from "html-react-parser";
import { FE_URL } from "constants/constants";
import CopyImg from "assets/icons/knowledgeEnhancement/LinkIcon.svg";
import { Tooltip } from "antd";
import { Notification } from "components/Notification/Noti";
import { Col } from "antd";
import _ from "lodash";
import styled from "styled-components";
import { useTranslation } from "react-i18next";
import { optionsParse } from "util/staticData";
import AuthenImage from "components/Image/AuthenImage";

const ItemArticle = ({ setting, article, config, rf }) => {
  const checkContain = (field, article) => {
    let tem = "";
    Object.keys(article.highlight).forEach((item) => {
      if (item.includes(field)) {
        tem = item;
      }
    });
    return tem;
  };
  const { t } = useTranslation();
  const renderSrc = (setting, article) => {
    if (_.get(article, `${setting?.fld_thumbnail}.value`, [])) {
      return _.get(article, `${setting?.fld_thumbnail}.value`, [])[0];
    } else {
      return "";
    }
  };

  const renderTag = (setting, article) => {
    if (checkContain(`${setting?.fld_tag}.value`, article)) {
      return article.highlight[
        `${checkContain(`${setting?.fld_tag}.value`, article)}`
      ][0]
        .split("#")
        .map(
          (tag, index) =>
            tag && (
              <Tag highlight={tag.trim().includes("</em>")} key={index}>
                #{tag.trim() && parse(tag.trim(), optionsParse)}
              </Tag>
            )
        );
    } else {
      if (_.get(article, `${setting?.fld_tag}.value`, "")) {
        return _.get(article, `${setting?.fld_tag}.value`, "")
          .split("#")
          .map((tag, index) => tag && <Tag key={index}>#{tag.trim()}</Tag>);
      } else {
        return "";
      }
    }
  };
  return (
    <ArticleWrap
      span={rf?.current?.offsetWidth > 760 ? 12 : 24}
      onClick={() => {
        window.open(
          `${FE_URL}/knowledge-base-enhancement/details/${setting.obj_article}/${article._id}`
        );
      }}
    >
      {renderSrc(setting, article) && (
        <CustomImg>
          <AuthenImage src={renderSrc(setting, article)} />
        </CustomImg>
        // <img src={renderSrc(setting, article)} alt="Thumbnail" />
      )}

      <ArticleContent>
        {Object.entries(article.highlight).length === 0 ? (
          <>
            <Title>
              <span>
                {_.get(article, `${setting?.fld_title}.value`, "No title")}
              </span>
              <Tooltip title={t("knowledgeBase.copy")}>
                <img
                  src={CopyImg}
                  alt="Copy"
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    navigator.clipboard.writeText(
                      `${FE_URL}/knowledge-base-enhancement/details/${setting.obj_article}/${article._id}`
                    );
                    Notification("success", "Copy link article success");
                  }}
                />
              </Tooltip>
            </Title>
            {_.get(article, `${setting?.fld_body}.value`, "No body") && (
              <Content>
                {parse(
                  _.get(
                    article,
                    `${setting?.fld_body}.value`,
                    "No body"
                  ).replace(/<[^>]*>/g, ""),
                  optionsParse
                )}
              </Content>
            )}
          </>
        ) : (
          <>
            {article.highlight[
              `${checkContain(`${setting?.fld_title}.value`, article)}`
            ] ? (
              <Title>
                <span>
                  {article.highlight[
                    `${checkContain(`${setting?.fld_title}.value`, article)}`
                  ].map((item, index) => {
                    return <span key={index}>{parse(item, optionsParse)}</span>;
                  })}
                </span>
                <Tooltip title={t("knowledgeBase.copy")}>
                  <img
                    src={CopyImg}
                    alt="Copy"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      navigator.clipboard.writeText(
                        `${FE_URL}/knowledge-base-enhancement/details/${setting.obj_article}/${article._id}`
                      );
                      Notification("success", "Copy link article success");
                    }}
                  />
                </Tooltip>
              </Title>
            ) : (
              <Title>
                <span>
                  {_.get(article, `${setting?.fld_title}.value`, "No title")}
                </span>
                <Tooltip title={t("knowledgeBase.copy")}>
                  <img
                    src={CopyImg}
                    alt="Copy"
                    onClick={(e) => {
                      e.preventDefault();
                      e.stopPropagation();
                      navigator.clipboard.writeText(
                        `${FE_URL}/knowledge-base-enhancement/details/${setting.obj_article}/${article._id}`
                      );
                      Notification("success", "Copy link article success");
                    }}
                  />
                </Tooltip>
              </Title>
            )}
            {article.highlight[
              `${checkContain(`${setting?.fld_body}.plain_text`, article)}`
            ] ? (
              <Content>
                {article.highlight[
                  `${checkContain(`${setting?.fld_body}.plain_text`, article)}`
                ].map((item, index) => {
                  return (
                    <span key={index}>
                      {parse(item, optionsParse)}... <br />
                    </span>
                  );
                })}
              </Content>
            ) : article.highlight[
                `${checkContain(`${setting?.fld_body}.value`, article)}`
              ] ? (
              <Content>
                {article.highlight[
                  `${checkContain(`${setting?.fld_body}.value`, article)}`
                ].map((item, index) => {
                  return (
                    <span key={index}>
                      {parse(item, optionsParse)}... <br />
                    </span>
                  );
                })}
              </Content>
            ) : (
              <>
                {_.get(article, `${setting?.fld_body}.value`, "No body") && (
                  <Content>
                    {parse(
                      _.get(
                        article,
                        `${setting?.fld_body}.value`,
                        "No body"
                      ).replace(/<[^>]*>/g, ""),
                      optionsParse
                    )}
                  </Content>
                )}
              </>
            )}
          </>
        )}
        <TagWrap>{renderTag(setting, article)}</TagWrap>
        <Line />

        <ActionWrap>
          {config.show_total_views && (
            <Action>
              <EyeFilled />
              {_.get(article, `${setting?.fld_total_views}.value`, "0")
                ? _.get(article, `${setting?.fld_total_views}.value`, "0")
                : "0"}
            </Action>
          )}
          {config.show_votes && (
            <Action>
              <LikeFilled />
              {_.get(article, `${setting?.fld_upvotes}.value`, "0")
                ? _.get(article, `${setting?.fld_upvotes}.value`, "0")
                : "0"}
            </Action>
          )}
          {/* {config.show_votes && (
            <Action>
              <DislikeFilled />{" "}
              {_.get(article, `${setting?.fld_downvotes}.value`, "0")
                ? _.get(article, `${setting?.fld_downvotes}.value`, "0")
                : "0"}
            </Action>
          )} */}
          <Action>
            <CalendarFilled /> {article.created_date}
          </Action>
        </ActionWrap>
      </ArticleContent>
    </ArticleWrap>
  );
};

export default ItemArticle;

const Content = styled.div`
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  word-break: break-word;
  min-height: 50px;
  line-height: 25px;
  color: #6b6b6b;
  p {
    margin-bottom: 0;
  }
`;

const ActionWrap = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const TagWrap = styled.div`
  display: flex;
  margin: 8px 0 12px 0;
  flex-wrap: wrap;
  min-height: 32px;
`;

const Action = styled.div`
  font-size: 12px;
  line-height: 16px;
  color: #6b6b6b;
  font-family: var(--roboto-500);
  display: flex;
  color: #919eab;
  margin: 8px 8px 8px 0;
  svg {
    font-size: 16px;
    margin-right: 6px;
    cursor: pointer;
    color: #919eab;
  }
`;

const Line = styled.div`
  height: 1px;
  background: #ececec;
  flex: 1;
`;
const ArticleWrap = styled(Col)`
  display: flex;
  cursor: pointer;
  margin-bottom: 24px;
`;
const ArticleContent = styled.div`
  flex: 1;
  em {
    font-style: normal;
    background: rgb(255 255 0);
  }
`;

const Tag = styled.div`
  background: ${(props) =>
    props.highlight ? "rgb(255 255 0)" : `rgba(32, 162, 162, 0.15)`};
  border-radius: 5px;
  color: #0a7979;
  line-height: 24px;
  padding: 0 6px;
  margin-right: 8px;
  margin-bottom: 8px;
  white-space: nowrap;
`;

const Title = styled.div`
  font-size: 18px;
  font-family: var(--roboto-500);
  display: flex;
  align-items: start;
  justify-content: space-between;
  span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    line-height: 25px;
  }

  img {
    margin-top: 3px;
    margin-left: 6px;
    width: 16px !important;
  }
`;
const CustomImg = styled.div`
  img {
    width: 258px;
    height: 155px;
    border-radius: 8px;
    object-fit: cover;
    margin-right: 16px;
  }
`;
