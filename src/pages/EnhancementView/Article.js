import {
  Form,
  Input,
  Row,
  Col,
  Select,
  DatePicker,
  Menu,
  Dropdown,
  TreeSelect,
} from "antd";
import Editor from "components/Editor/Editor2";
import React, {
  useRef,
  useState,
  useEffect,
  useCallback,
  useMemo,
} from "react";
import { useNavigate, useParams } from "react-router";
import styled from "styled-components";
import Upload from "antd/lib/upload";
import { BASE_URL_API, BE_URL, FE_URL } from "constants/constants";
import { Notification } from "components/Notification/Noti";
import pdfIcon from "assets/icons/knowledgeEnhancement/pdfIcon.png";
import excelIcon from "assets/icons/knowledgeEnhancement/excelIcon.png";
import wordIcon from "assets/icons/knowledgeEnhancement/wordIcon.png";
import fileIcon from "assets/icons/knowledgeEnhancement/defaultIcon.png";
import CloseIcon from "assets/icons/knowledgeEnhancement/close-circle.png";
import AddImg from "assets/icons/knowledgeEnhancement/Union.png";
import ImageIcon from "assets/icons/knowledgeEnhancement/ImageIcon.png";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import {
  createService,
  getArticleDetail,
  getArticleDetailSuccess,
  getListCategory,
  updateArticle,
} from "redux/slices/enhancementView";
import axios from "axios";
import moment from "moment";
import _ from "lodash";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import { runDynamicButton } from "redux/slices/dynamicButton";
import ImgConfirm from "assets/icons/common/confirm.png";
import {
  loadUserDynamicButton,
  loadUserDynamicButtonSuccess,
} from "redux/slices/dynamicButton";
import IconDown from "assets/icons/common/icon-down.svg";
import { checkTokenExpiration } from "contexts/TokenCheck";
import ModalRunDynamicButton from "components/Modal/ModalRunDynamicButton";
import AuthenImage from "components/Image/AuthenImage";

const { Option } = Select;

const Article = () => {
  const dateFormatSecond = "YYYY-MM-DD 00:00:00";
  const [form] = Form.useForm();
  const { objectId, recordID } = useParams();
  const editorJodit = useRef(null);
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { articleDetail, listCategory } = useSelector(
    (state) => state.enhancementViewReducer
  );
  const { userDynamicButton } = useSelector(
    (state) => state.dynamicButtonReducer
  );
  const { allObject } = useSelector((state) => state.tenantsReducer);

  const navigate = useNavigate();
  const [config, setConfig] = useState({});
  const [listFile, setListFile] = useState([]);
  const [thumb, setThumb] = useState([]);
  const [treeCategory, setTreeCategory] = useState([]);
  const [tag, setTag] = useState("");
  const [optionTag, setOptionTag] = useState([]);
  const [listStatus, setListStatus] = useState([]);
  const [dataConfirm, setDataConfirm] = useState({});
  const [showConfirm, setShowConfirm] = useState(false);
  const [listField, setListField] = useState([]);
  const [visibleModalRDB, $visibleModalRDB] = useState(false);

  useEffect(() => {
    const getNewTree = (tree) =>
      tree.map(({ key, title, children }) => ({
        value: title,
        title,
        children: getNewTree(children),
      }));
    setTreeCategory(getNewTree(listCategory));
  }, [listCategory]);

  const onSearchHandle = useCallback((searchText) => {
    setTag(searchText);
  }, []);

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 50),
    [onSearchHandle]
  );
  useEffect(() => {
    if (
      Object.entries(articleDetail).length > 0 &&
      Object.entries(config).length > 0
    ) {
      if (
        articleDetail.article[config.article_setting?.fld_attachments]?.value
      ) {
        setListFile(
          articleDetail.article[config.article_setting?.fld_attachments]?.value
        );
      }

      if (articleDetail.article[config.article_setting?.fld_category].value) {
        form.setFieldsValue({
          fld_category:
            articleDetail.article[config.article_setting?.fld_category].value,
        });
      }

      if (articleDetail.article[config.article_setting?.fld_title].value) {
        form.setFieldsValue({
          fld_title:
            articleDetail.article[config.article_setting?.fld_title].value,
        });
      }

      if (articleDetail.article[config.article_setting?.fld_version]?.value) {
        form.setFieldsValue({
          fld_version:
            articleDetail.article[config.article_setting?.fld_version]?.value,
        });
      }

      if (
        articleDetail.article[config.article_setting?.fld_expired_date]?.value
      ) {
        form.setFieldsValue({
          fld_expired_date: moment(
            articleDetail.article[config.article_setting?.fld_expired_date]
              ?.value
          ),
        });
      }

      if (articleDetail.article[config.article_setting?.fld_tag]?.value) {
        let tem = articleDetail.article[config.article_setting?.fld_tag]?.value
          .split("#")
          .filter((item) => item);
        form.setFieldsValue({
          fld_tag: tem,
        });
      }

      if (articleDetail.article[config.article_setting?.fld_status].value) {
        form.setFieldsValue({
          fld_status:
            articleDetail.article[config.article_setting?.fld_status].value,
        });
      }

      if (articleDetail.article[config.article_setting?.fld_thumbnail]?.value) {
        setThumb(
          articleDetail.article[config.article_setting?.fld_thumbnail]?.value
        );
      }
    }
    //eslint-disable-next-line
  }, [articleDetail, config]);

  useEffect(() => {
    if (recordID) {
      dispatch(
        getArticleDetail({
          object_id: objectId,
          record_id: recordID,
        })
      );
    } else {
      dispatch(getArticleDetailSuccess({}));
    }

    if (recordID && objectId) {
      dispatch(
        loadUserDynamicButton({
          object_id: objectId,
          record_id: recordID,
        })
      );
    } else {
      dispatch(loadUserDynamicButtonSuccess([]));
    }
  }, [recordID, objectId, dispatch]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();

      if (Object.entries(config).length > 0 && objectId) {
        axios
          .get(
            BASE_URL_API +
              "knowledge-base-enhancement/" +
              objectId +
              `/tag?s=${tag}`,
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            if (res.data.data) {
              let tem = [];
              if (Object.entries(config).length > 0) {
                res.data.data.records.forEach((record) => {
                  tem.push({
                    label: record[config.tag_setting.fld_tag]?.value.replace(
                      "#",
                      ""
                    ),
                    value: record[config.tag_setting.fld_tag]?.value.replace(
                      "#",
                      ""
                    ),
                  });
                });
              }
              setOptionTag(tem);
            }
          })
          .catch((error) => Notification("error", error.response.data.error));
      }
    };
    checkToken();
  }, [tag, config, objectId]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      if (Object.entries(config).length > 0) {
        axios
          .post(
            BASE_URL_API + "object/objects-fields-permission",
            {
              api_version: "2",
              object_id: config.article_setting.obj_article,
              show_meta_fields: false,
            },
            {
              headers: {
                Authorization: isTokenValid,
              },
            }
          )
          .then((res) => {
            let temp = [];
            let tempField = [];
            if (res.data.data.length > 0) {
              res.data.data[
                res.data.data.length - 1
              ].main_object.sections.forEach((item) => {
                item.fields.forEach((field) => {
                  tempField.push(field);
                  if (field.ID === config.article_setting.fld_status) {
                    temp = field.option;
                  }
                });
              });
            }
            setListField(tempField);
            setListStatus(temp);
          })
          .catch((error) => Notification("error", error.response.data.error));
      }
    };
    checkToken();
  }, [config]);

  useEffect(() => {
    dispatch(getListCategory({ object_id: objectId }));
  }, [objectId, dispatch]);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  const handleCheckReadOnly = (list, key) => {
    let tem = false;
    list.forEach((item) => {
      if (item.ID === key) {
        tem = item.readonly;
      }
    });
    return tem;
  };

  useEffect(() => {
    if (!recordID) {
      let tem = undefined;
      listField.forEach((item) => {
        if (item.ID === config?.article_setting?.fld_status) {
          tem = item.default_value ? item.default_value : undefined;
        }
      });
      form.setFieldsValue({
        fld_status: tem,
      });
    }
  }, [listField, config, recordID, form]);

  const _onRunButton = (button) => {
    if (_.get(button, "dynamic_field", []).length > 0) {
      $visibleModalRDB(true);
      setDataConfirm({
        button: button,
        data: {
          data: {
            object_id: objectId,
            record_id: recordID,
            button_id: button._id,
          },
          reloadable: button?.reloadable || false,
        },
      });
    } else {
      setDataConfirm({
        data: {
          object_id: objectId,
          record_id: recordID,
          button_id: button._id,
        },
        reloadable: button?.reloadable || false,
      });
      setShowConfirm(true);
    }
  };

  const menuActions = (
    <Menu>
      {userDynamicButton.map((button, idx) => {
        return (
          <Menu.Item key={idx} onClick={() => _onRunButton(button)}>
            <span>{button.name}</span>
          </Menu.Item>
        );
      })}
    </Menu>
  );

  const renderListFile = (list) => {
    if (list) {
      return list.map((item, index) => {
        const arr = item.split("/");
        const type = arr[arr.length - 1].split(".")[1];
        let icon = "";
        switch (type) {
          case "pdf":
            icon = pdfIcon;
            break;
          case "xlsx":
            icon = excelIcon;
            break;
          case "doc":
            icon = wordIcon;
            break;

          default:
            icon = fileIcon;
            break;
        }
        if (list) {
          return (
            <File
              key={index}
              href={
                allObject?.find(
                  (object) => object?.object_id === objectId && object?.status
                )
                  ? `${FE_URL + item?.split(BE_URL)[1]}`
                  : item
              }
              rel="noreferrer"
              target="_blank"
            >
              <img src={icon} alt="" />
              <span>{arr[arr.length - 1]}</span>
              <div
                className="icon-delete"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  let temp = listFile.filter((url) => url !== item);
                  setListFile(temp);
                }}
              >
                <img src={CloseIcon} alt="" />
              </div>
            </File>
          );
        } else {
          return "";
        }
      });
    }
  };

  const handleFinish = (value) => {
    let temp = { ...value, fld_title: value.fld_title.trim() };

    if (value.fld_expired_date) {
      temp = {
        ...value,
        fld_expired_date: moment(value.fld_expired_date).format(
          dateFormatSecond
        ),
      };
    }

    if (value.fld_tag) {
      let tem = "";
      value.fld_tag.forEach((tag) => {
        tem = tem + `#${tag}`;
      });
      temp = { ...temp, fld_tag: tem };
    }

    if (listFile.length > 0) {
      temp = { ...temp, fld_attachments: listFile };
    }
    if (thumb.length > 0) {
      temp = { ...temp, fld_thumbnail: thumb[thumb.length - 1] };
    }

    temp = {
      ...temp,
      fld_body: editorJodit.current.value
        ? editorJodit.current.value.replaceAll(
            '<table style="border-collapse:',
            '<table border="1" style="border-collapse:'
          )
        : "",
    };
    if (recordID) {
      temp = { ...temp, record_id: recordID };
      dispatch(
        updateArticle({ object_id: objectId, service: "article", data: temp })
      );
    } else {
      dispatch(
        createService({ object_id: objectId, service: "article", data: temp })
      );
    }
  };
  return (
    <Wrap>
      <ButtonGroup>
        <CustomButton
          onClick={() => {
            dispatch(getArticleDetailSuccess({}));
            navigate(`/knowledge-base-enhancement/${objectId}`);
          }}
        >
          {t("common.cancel")}
        </CustomButton>
        <CustomButton onClick={() => form.submit()}>
          {t("common.save")}
        </CustomButton>
      </ButtonGroup>
      <ContentWrap>
        <Form
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
          name="article"
          form={form}
          onFinish={handleFinish}
        >
          <Row gutter={[16, 16]}>
            <Col xl={16}>
              <Left>
                <Field
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                    () => ({
                      validator: (_, value) => {
                        let temp = false;
                        if (value && value.trim().length === 0) {
                          temp = true;
                        }
                        if (temp) {
                          return Promise.reject(
                            new Error(t("knowledgeBase.required"))
                          );
                        } else {
                          return Promise.resolve();
                        }
                      },
                    }),
                  ]}
                  name="fld_title"
                  label={t("knowledgeBase.title")}
                >
                  <Input
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_title
                    )}
                    placeholder="Please input"
                  />
                </Field>

                <Text>{t("knowledgeBase.content")}</Text>
                <Editor
                  readonly={handleCheckReadOnly(
                    listField,
                    config?.article_setting?.fld_body
                  )}
                  content={
                    Object.entries(articleDetail).length > 0
                      ? articleDetail.article[config.article_setting?.fld_body]
                          ?.value
                      : ""
                  }
                  editorJodit={editorJodit}
                  objectID={objectId}
                  showAppend={false}
                  isPublicImage={true}
                />

                <WrapContent>
                  <Text>{t("knowledgeBase.attach")}</Text>
                  <GroupFile>
                    <Upload
                      disabled={handleCheckReadOnly(
                        listField,
                        config?.article_setting?.fld_attachments
                      )}
                      action={BASE_URL_API + "upload-file"}
                      method={"post"}
                      headers={{
                        Authorization: localStorage.getItem(
                          "setting_accessToken"
                        ),
                      }}
                      data={{
                        obj: objectId,
                      }}
                      showUploadList={false}
                      multiple={true}
                      onChange={(info) => {
                        const status = info.file.status;
                        if (status === "done") {
                          const temp = [...listFile];

                          if (temp.length === config.number_of_attachments) {
                            Notification(
                              "warning",
                              `You can only attachement up to ${config.number_of_attachments} files`
                            );
                          } else {
                            temp.push(`${BE_URL}${info.file.response.data[0]}`);
                            setListFile(temp);
                          }
                        } else if (status === "error") {
                          Notification("error", "Upload file errors");
                        }
                      }}
                      onRemove={(file) => {
                        console.log("file remove", file);
                      }}
                      progress={{
                        strokeColor: {
                          "0%": "#108ee9",
                          "100%": "#87d068",
                        },
                        strokeWidth: 3,
                        format: (percent) =>
                          `${parseFloat(percent.toFixed(2))}%`,
                      }}
                    >
                      <ButtonUpload>
                        <svg
                          width="22"
                          height="28"
                          viewBox="0 0 22 28"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M21.7062 7.01875L14.9813 0.29375C14.7938 0.10625 14.5406 0 14.275 0H1C0.446875 0 0 0.446875 0 1V27C0 27.5531 0.446875 28 1 28H21C21.5531 28 22 27.5531 22 27V7.72813C22 7.4625 21.8937 7.20625 21.7062 7.01875ZM13.8125 2.30625L19.6938 8.1875H13.8125V2.30625ZM19.75 25.75H2.25V2.25H11.6875V9C11.6875 9.3481 11.8258 9.68194 12.0719 9.92808C12.3181 10.1742 12.6519 10.3125 13 10.3125H19.75V25.75Z"
                            fill="#d9d9d9"
                          />
                          <path
                            d="M12 12.75C12 12.6125 11.8875 12.5 11.75 12.5H10.25C10.1125 12.5 10 12.6125 10 12.75V16.125H6.625C6.4875 16.125 6.375 16.2375 6.375 16.375V17.875C6.375 18.0125 6.4875 18.125 6.625 18.125H10V21.5C10 21.6375 10.1125 21.75 10.25 21.75H11.75C11.8875 21.75 12 21.6375 12 21.5V18.125H15.375C15.5125 18.125 15.625 18.0125 15.625 17.875V16.375C15.625 16.2375 15.5125 16.125 15.375 16.125H12V12.75Z"
                            fill="#d9d9d9"
                          />
                        </svg>

                        <span>{t("knowledgeBase.addAttachment")}</span>
                      </ButtonUpload>
                    </Upload>
                    {renderListFile(listFile)}
                  </GroupFile>
                </WrapContent>
              </Left>
            </Col>
            <Col xl={8}>
              <Right>
                <UploadWrap>
                  <Upload
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_thumbnail
                    )}
                    accept=".jpg, .png, .gif, .jfif, .jpeg"
                    action={BASE_URL_API + "upload-file"}
                    method={"post"}
                    headers={{
                      Authorization: localStorage.getItem(
                        "setting_accessToken"
                      ),
                    }}
                    data={{
                      obj: objectId,
                    }}
                    onChange={(info) => {
                      const status = info.file.status;
                      if (status === "done") {
                        const temp = [...thumb];
                        temp.push(`${BE_URL}${info.file.response.data[0]}`);
                        setThumb(temp);
                      } else if (status === "error") {
                        Notification("error", "Error!");
                      }
                    }}
                    showUploadList={false}
                    progress={{
                      strokeColor: {
                        "0%": "#108ee9",
                        "100%": "#87d068",
                      },
                      strokeWidth: 3,
                      format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
                    }}
                  >
                    {thumb.length > 0 ? (
                      <Thumb>
                        <AuthenImage src={thumb[thumb.length - 1]} />

                        {/* <img src={thumb[thumb.length - 1]} alt="thumb" /> */}
                        <div className="shadow">
                          <span>
                            <img src={ImageIcon} alt="" />{" "}
                            {t("knowledgeBase.choosePicture")}
                          </span>
                        </div>
                      </Thumb>
                    ) : (
                      <UploadThumb>
                        <img src={AddImg} alt="" />
                        <span> {t("knowledgeBase.uploadThumbnail")}</span>
                      </UploadThumb>
                    )}
                  </Upload>
                </UploadWrap>
                <Field name="fld_tag" label={t("knowledgeBase.tag")}>
                  <Select
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_tag
                    )}
                    onChange={() => {
                      setTimeout(() => {
                        setTag("");
                      }, 100);
                    }}
                    onSearch={debouncedSearchHandler}
                    placeholder={t("common.placeholderSelect")}
                    allowClear
                    mode="tags"
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {optionTag.map((option, index) => (
                      <Option key={index} value={option.value}>
                        {option.value}
                      </Option>
                    ))}
                  </Select>
                </Field>

                <Field
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                  name="fld_status"
                  label={t("knowledgeBase.status")}
                >
                  <Select
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_status
                    )}
                    placeholder={t("common.placeholderSelect")}
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {listStatus.map((status, index) => {
                      return (
                        <Option key={index} value={status.value}>
                          {status.label}
                        </Option>
                      );
                    })}
                  </Select>
                </Field>

                <Field
                  name="fld_expired_date"
                  label={t("knowledgeBase.expiredDate")}
                >
                  <DatePicker
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_expired_date
                    )}
                    onChange={(date, dateString) => {}}
                  />
                </Field>
                <Field
                  rules={[
                    { required: true, message: t("knowledgeBase.required") },
                  ]}
                  name="fld_category"
                  label={t("knowledgeBase.category")}
                >
                  {/* <Select
                    placeholder={t("common.placeholderSelect")}
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    filterOption={(inputValue, option) => {
                      if (option.children) {
                        return option.children
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      } else if (option.label) {
                        return option.label
                          .toLowerCase()
                          .indexOf(inputValue.toLowerCase()) >= 0
                          ? true
                          : false;
                      }
                    }}
                  >
                    {listCategory.map((option, index) => (
                      <Option key={index} value={option}>
                        {option}
                      </Option>
                    ))}
                  </Select> */}
                  <TreeSelect
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_category
                    )}
                    placement="topLeft"
                    placeholder={t("common.placeholderSelect")}
                    showSearch
                    treeData={treeCategory}
                  ></TreeSelect>
                </Field>

                <Field name="fld_version" label={t("knowledgeBase.version")}>
                  <Input
                    disabled={handleCheckReadOnly(
                      listField,
                      config?.article_setting?.fld_version
                    )}
                    placeholder={t("common.placeholderInput")}
                  />
                </Field>
              </Right>
              {userDynamicButton.length > 0 && (
                <DynamicWrap>
                  <p>{t("settings.dynamicButton")}</p>
                  <Dropdown overlay={menuActions} trigger={["click"]}>
                    <CustomButtonAction>
                      <span>{t("knowledgeBase.action")}</span>
                      <img src={IconDown} alt="icondown" />
                    </CustomButtonAction>
                  </Dropdown>
                </DynamicWrap>
              )}
            </Col>
          </Row>
        </Form>
      </ContentWrap>

      <ModalConfirm
        title={t("knowledgeBase.confirm")}
        decs={t("knowledgeBase.confirmActionDynamic")}
        method={runDynamicButton}
        data={dataConfirm}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={setShowConfirm}
      />

      <ModalRunDynamicButton
        data={dataConfirm}
        method={runDynamicButton}
        visibleModalRDB={visibleModalRDB}
        $visibleModalRDB={$visibleModalRDB}
        dataRecord={_.get(articleDetail, "article")}
      />
    </Wrap>
  );
};

export default Article;

const Wrap = styled.div`
  padding: 24px;
  color: #2c2c2c;
  line-height: 22px;
  font-size: 16px;
  .ant-form-item-label {
    padding: 0;
  }
  .ant-form-item {
    margin-bottom: 16px;
    label {
      color: #2c2c2c;
      line-height: 22px;
      font-size: 16px;
      font-family: var(--roboto-500);
    }
  }
  .ant-input:hover,
  .ant-input:focus,
  .ant-input-focused {
    border-color: ${(props) => props.theme.main} !important;
    box-shadow: none;
  }
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector,
  .ant-picker-focused,
  .ant-picker:hover {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  .ant-picker {
    width: 100%;
  }
  .jodit-workplace {
    height: 400px !important;
  }
`;

const ContentWrap = styled.div``;

const Left = styled.div`
  padding: 16px;
  background: #fff;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
`;

const Field = styled(Form.Item)``;

const ButtonUpload = styled.div`
  cursor: pointer;
  border: solid 1px #efeff4;
  border-radius: 5px;
  display: flex;
  align-items: center;
  padding: 9px;
  transition: all 0.5s;
  span {
    margin-left: 8px;
    font-family: var(--roboto-700);
    font-size: 14px;
    line-height: 22px;

    color: #2c2c2c;
  }
  :hover {
    border-color: ${(props) => props.theme.main};
    svg {
      color: ${(props) => props.theme.main};
    }
  }
`;

const WrapContent = styled.div`
  margin-top: 16px;
`;

const Text = styled.p`
  color: #2c2c2c;
  line-height: 22px;
  font-size: 16px;
  font-family: var(--roboto-700);
  margin-bottom: 8px;
`;

const GroupFile = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 8px;
  margin-bottom: 16px;
  align-items: center;
  .ant-upload {
    margin-bottom: 16px;
  }
`;

const File = styled.a`
  margin-bottom: 16px;
  border: 1px solid #efeff4;
  border-radius: 5px;
  display: flex;
  align-items: center;
  width: 200px;
  margin-left: 16px;
  height: 48px;
  padding: 0 8px;
  font-family: var(--roboto-500);
  font-size: 14px;
  line-height: 22px;
  color: #2c2c2c;
  transition: all 0.5s;
  :hover {
    color: #2c2c2c;
    background: #f2f4f5;
    .icon-delete {
      opacity: 1;
      visibility: visible;
    }
  }
  span {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  img {
    margin-right: 6px;
  }
  position: relative;

  .icon-delete {
    position: absolute;
    top: -12px;
    right: 10px;
    box-shadow: 0px 8px 16px #adb6bc;
    border-radius: 50%;
    display: flex;
    visibility: hidden;
    opacity: 0;
    transition: all 0.5s;

    img {
      margin: 0;
    }
  }
`;

const UploadWrap = styled.div`
  .ant-upload {
    width: 100%;
  }
`;

const UploadThumb = styled.div`
  width: 100%;
  height: 280px;
  display: flex;
  align-items: center;
  justify-content: center;
  background: #fff;
  cursor: pointer;
  flex-direction: column;
  span {
    font-size: 16px;
    margin-top: 16px;
  }
  border: 1px dashed ${(props) => props.theme.main};
`;

const Right = styled.div`
  background: #fff;
  padding: 16px;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
`;

const Thumb = styled.div`
  cursor: pointer;
  img {
    width: 100%;
    height: 280px;
    object-fit: cover;
    border-radius: 5px;
  }

  position: relative;
  :hover {
    .shadow {
      visibility: visible;
      opacity: 1;
    }
  }
  .shadow {
    transition: all 0.5s;
    visibility: hidden;
    opacity: 0;
    background: rgba(39, 36, 36, 0.5);
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    span {
      background: #fff;
      padding: 2px 8px;
      border-radius: 2px;
      img {
        width: unset;
        height: unset;
      }
    }
  }
`;

const ButtonGroup = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  margin-bottom: 16px;
`;

const CustomButton = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  margin-left: 16px;
  background: #fff;
  cursor: pointer;
  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }
  :last-child {
    background: ${(props) => props.theme.main};
    color: #fff;
    border: ${(props) => props.theme.main};
  }
`;
const CustomButtonAction = styled.div`
  width: 100%;
  margin-right: 16px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  cursor: pointer;
  transition: all 0.5s;
  display: flex;
  justify-content: space-between;
  padding: 5px 8px;
  img {
    margin-right: 8px;
  }

  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;

    img {
      filter: brightness(200);
    }
  }
`;

const DynamicWrap = styled.div`
  width: 100%;
  background: #fff;
  padding: 16px;
  border: 1px solid #f4f4f4;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
  margin-top: 16px;
  p {
    margin-bottom: 8px;
    font-family: var(--roboto-700);
    font-size: 16px;
    line-height: 20px;
  }
`;
