import { Modal, Button, Radio, Input, Select } from "antd";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import styled from "styled-components";
import Tranfer from "assets/icons/roles/Transfer.jpg";
import { useDispatch } from "react-redux";
import {
  createCategory,
  deleteCategory,
  updateCategory,
} from "redux/slices/enhancementView";
import { useParams } from "react-router";

const options = [
  { label: "Category", value: "category" },
  {
    label: "Sub-category",
    value: "sub",
  },
];
const { Option } = Select;

const ModalCategory = ({ list, openModal, setOpenmodal, objectModal }) => {
  const [type, setType] = useState("category");
  const [name, setName] = useState("");
  const [parent, setParent] = useState(undefined);
  const dispatch = useDispatch();
  const { objectId } = useParams();

  useEffect(() => {
    if (objectModal.key === "edit") {
      setName(objectModal.name);
    }
  }, [objectModal]);

  useEffect(() => {
    if (!openModal) {
      setType("category");
      setName("");
      setParent(undefined);
    }
  }, [openModal]);

  const handleDisable = () => {
    let tem = true;
    if (objectModal.key === "edit" || objectModal.key === "add") {
      if (!name) {
        tem = true;
      } else {
        tem = false;
      }
    } else {
      if (!parent) {
        tem = true;
      } else {
        tem = false;
      }
    }
    return tem;
  };

  const handleModal = () => {
    if (objectModal.key === "add") {
      if (type === "category") {
        dispatch(
          createCategory({
            object_id: objectId,
            service: "category",
            data: { category_name: name },
          })
        );
      } else {
        dispatch(
          createCategory({
            object_id: objectId,
            service: "category",
            data: { category_name: name, parent_id: objectModal.id },
          })
        );
      }
    } else if (objectModal.key === "edit") {
      dispatch(
        updateCategory({
          object_id: objectId,
          service: "category",
          data: { category_name: name, record_id: objectModal.id },
        })
      );
    } else if (objectModal.key === "delete") {
      dispatch(
        deleteCategory({
          object_id: objectId,
          service: "category",
          data: { inherit_id: parent, record_id: objectModal.id },
        })
      );
    }
  };

  return (
    <CustomModal
      onCancel={() => setOpenmodal(false)}
      title={
        objectModal.key === "add"
          ? "Add category"
          : objectModal.key === "edit"
          ? "Edit category"
          : "Delete category"
      }
      width={500}
      visible={openModal}
      footer={[
        <ButtonGroup key="button">
          <div className="btnGroup">
            {objectModal.key === "delete" ? (
              <Button
                key="send"
                disabled={handleDisable()}
                onClick={() => {
                  handleModal();
                  setOpenmodal(false);
                }}
              >
                Chuyển bài viết và xóa danh mục
              </Button>
            ) : (
              <Button
                key="send"
                disabled={handleDisable()}
                onClick={() => {
                  handleModal();
                  setOpenmodal(false);
                }}
              >
                Save
              </Button>
            )}
            <Button key="cancel" onClick={() => setOpenmodal(false)}>
              Cancel
            </Button>
          </div>
        </ButtonGroup>,
      ]}
    >
      {objectModal.key === "delete" && (
        <WrapNote>
          <img src={Tranfer} alt="transfer" />
          <Title>
            Bạn có chắn chắn muốn xóa danh mục " {objectModal.name} "
          </Title>
          <SubTitle>
            Sau khi xóa dữ liệu sẽ không thểm hoàn tác. Dữ liệu con cũng sẽ bị
            xóa theo đối tượng
          </SubTitle>
        </WrapNote>
      )}
      {objectModal.key !== "delete" && (
        <Item>
          <CustomText>Name category</CustomText>
          <Input
            value={name}
            placeholder="Please input"
            onChange={(e) => setName(e.target.value)}
          />
        </Item>
      )}
      {objectModal.key === "add" && (
        <Item>
          <CustomText>Type category</CustomText>
          <Radio.Group
            options={options}
            value={type}
            onChange={(e) => {
              setType(e.target.value);
            }}
            optionType="button"
          />
        </Item>
      )}
      {objectModal.key === "delete" && (
        <Item>
          <CustomText>Chuyển bài viết đến danh mục</CustomText>
          <Select
            value={parent}
            placeholder="Please select"
            showSearch
            allowClear
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
            onChange={(value) => {
              setParent(value);
            }}
          >
            {list.map(
              (option, index) =>
                option.value !== objectModal.id && (
                  <Option key={index} value={option.value}>
                    {option.label}
                  </Option>
                )
            )}
          </Select>
        </Item>
      )}
    </CustomModal>
  );
};

export default ModalCategory;

const CustomModal = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-input:focus,
  .ant-input-focused {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }

  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  .ant-select {
    width: 100%;
  }
`;
const ButtonGroup = styled.div`
  button {
    border-radius: 2px;
    min-width: 114px;
    transition: all 0.5s;
    &:hover {
      background-color: ${(props) => props.theme.darker};
      border-color: ${(props) => props.theme.darker};

      color: #ffffff;
    }
  }
  .btnGroup {
    button {
      &:disabled {
        color: rgba(0, 0, 0, 0.25) !important;
        border-color: #d9d9d9 !important;
        background: #f5f5f5 !important;
      }
      &:first-child {
        background-color: ${(props) => props.theme.main};
        border-color: ${(props) => props.theme.darker};
        color: #ffffff;

        &:hover {
          background-color: ${(props) => props.theme.darker};
        }
      }
    }
  }
`;
const Item = styled.div`
  width: 100%;
  margin-bottom: 16px;
  .ant-radio-button-wrapper:hover {
    color: ${(props) => props.theme.main};
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):hover,
  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: ${(props) => props.theme.main} !important;
    border-color: ${(props) => props.theme.main} !important;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    ):focus-within {
    box-shadow: none;
  }
  .ant-radio-button-wrapper-checked:not(
      .ant-radio-button-wrapper-disabled
    )::before {
    background-color: ${(props) => props.theme.main};
  }
`;
const CustomText = styled.div`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  margin-bottom: 8px;
`;

const WrapNote = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;

  img {
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-size: 16px;
  font-family: var(--roboto-700);
  color: #2c2c2c;
  margin-bottom: 8px;
`;

const SubTitle = styled.span`
  font-size: 14px;
  color: #595959;
  text-align: center;
  margin-bottom: 8px;
`;
