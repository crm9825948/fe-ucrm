import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { updateDataSearch, updateLoadMore } from "redux/slices/enhancementView";
import styled from "styled-components";
import { ArrowLeftOutlined } from "@ant-design/icons";

import {
  OptionSortArticleDefault,
  OptionSortArticleView,
  OptionSortArticleVote,
} from "util/staticData";
import { Select, Spin } from "antd";

import InfiniteScroll from "react-infinite-scroll-component";
import EmptyImg from "assets/icons/knowledgeEnhancement/empty.png";
import ItemArticle from "../ItemArticle";
import { useTranslation } from "react-i18next";
import _ from "lodash";
const { Option } = Select;

const ResultSearch = ({ dataSearch, objectSearch, isLoadMore }) => {
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { objectId } = useParams();
  const [listArticle, setListArticle] = useState([]);
  const [optionFilter, setOptionFilter] = useState(OptionSortArticleDefault);
  const [config, setConfig] = useState({});

  const { article_setting } = config;

  useEffect(() => {
    let temp = OptionSortArticleDefault;
    if (config.show_votes) {
      temp = temp.concat(OptionSortArticleVote);
    }
    if (config.show_total_views) {
      temp = temp.concat(OptionSortArticleView);
    }
    setOptionFilter(temp);
  }, [config]);

  useEffect(() => {
    if (objectSearch.records) {
      if (isLoadMore) {
        const temp = listArticle.concat(objectSearch.records);
        setListArticle(temp);
      } else {
        setListArticle(objectSearch.records);
      }
    }
    //eslint-disable-next-line
  }, [objectSearch]);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  const renderListArticle = (list, setting) => {
    return list.map((article, index) => {
      return (
        <ItemArticle
          key={index}
          config={config}
          setting={setting}
          article={article}
        />
      );
    });
  };

  const loadMoreArticle = () => {
    dispatch(updateLoadMore(true));
    let next = dataSearch.page + 1;
    dispatch(
      updateDataSearch({
        page: next,
      })
    );
  };

  return (
    <Wrap>
      <HeaderWrap>
        <Right>
          <div
            style={{
              display: "flex",
              fontSize: "18px",
              marginBottom: "8px",
              alignItems: "center",
            }}
          >
            <CustomIcon
              onClick={() =>
                navigate(`/knowledge-base-enhancement/${objectId}`)
              }
            >
              <ArrowLeftOutlined style={{ color: "#000" }} />
            </CustomIcon>
            {t("knowledgeBase.article")}
            <Customspan>
              {listArticle.length} of {_.get(objectSearch, "total", 0)} articles
            </Customspan>
          </div>
          {dataSearch.text_search && (
            <span>
              {t("knowledgeBase.result")} "{dataSearch.text_search}"
            </span>
          )}
        </Right>
        <Left>
          <span> {t("knowledgeBase.sortBy")} </span>
          <CustomSelect
            defaultValue="related"
            onChange={(value) => {
              dispatch(updateLoadMore(false));
              dispatch(updateDataSearch({ sort: value, page: 0 }));
            }}
          >
            {optionFilter.map((option, index) => {
              return (
                <Option key={index} value={option.value}>
                  {t(`knowledgeBase.${option.label}`)}
                </Option>
              );
            })}
          </CustomSelect>
        </Left>
      </HeaderWrap>
      {listArticle.length === 0 ? (
        <NoData>
          <DataWrap>
            <span>
              <img src={EmptyImg} alt="no data " />
            </span>
            <p>{t("knowledgeBase.noArticle")}</p>
          </DataWrap>
        </NoData>
      ) : (
        <ContentWrap id="scrollableDiv">
          <InfiniteScroll
            hasMore={listArticle.length < objectSearch.total}
            dataLength={listArticle.length}
            loader={<Spin />}
            next={loadMoreArticle}
            scrollableTarget="scrollableDiv"
          >
            {renderListArticle(listArticle, article_setting)}
          </InfiniteScroll>
        </ContentWrap>
      )}
    </Wrap>
  );
};

export default ResultSearch;

const Wrap = styled.div`
  color: #2c2c2c;
  line-height: 22px;
  flex: 1;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ececec;
  background: #fff;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  padding: 24px;
  height: fit-content;
`;

const HeaderWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
`;

const Right = styled.div`
  font-family: var(--roboto-500);

  span {
    color: #6b6b6b;
    font-family: var(--roboto-500);
  }
`;

const Left = styled.div`
  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(
      .ant-select-customize-input
    )
    .ant-select-selector {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
  span {
    :first-child {
      font-size: 16px;
      margin-right: 16px;
    }
  }

  .ant-select-arrow {
    right: -3px;
  }
`;

const CustomSelect = styled(Select)`
  width: 160px;
`;

const ContentWrap = styled.div`
  width: 100%;
  max-height: calc(100vh - 286px);
  overflow: auto;
`;
const NoData = styled.div`
  display: flex;
  justify-content: center;

  img {
    width: 100px;
  }
`;
const DataWrap = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  flex-direction: column;
  p {
    font-size: 16px;
    line-height: 22px;
    margin-top: 8px;
    span {
      color: ${(props) => props.theme.main};
    }
  }
`;

const Customspan = styled.span`
  font-size: 14px;
  margin-left: 8px;
  font-family: var(--roboto-400) !important;
  margin-top: 2px;
  color: #919eab !important;
`;
const CustomIcon = styled.div`
  margin-right: 16px;
  border-radius: 6px;
  border: 1px solid #d9d9d9;
  width: 24px;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  svg {
    font-size: 14px;
  }
`;
