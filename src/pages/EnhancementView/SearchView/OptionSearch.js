import { Input, Checkbox } from "antd";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import {
  getListCategory,
  updateDataSearch,
  updateLoadMore,
} from "redux/slices/enhancementView";
import styled from "styled-components";
const { Search } = Input;

const OptionSearch = () => {
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { listCategory, dataSearch } = useSelector(
    (state) => state.enhancementViewReducer
  );
  const { t } = useTranslation();
  const listTime = [
    {
      label: t("knowledgeBase.all"),
      value: "all",
    },
    {
      label: t("knowledgeBase.day"),
      value: "day",
    },
    {
      label: t("knowledgeBase.week"),
      value: "week",
    },
    {
      label: t("knowledgeBase.month"),
      value: "month",
    },
    {
      label: t("knowledgeBase.year"),
      value: "year",
    },
  ];
  const { objectId } = useParams();
  const dispatch = useDispatch();
  const [categorys, setCategorys] = useState([]);

  useEffect(() => {
    dispatch(getListCategory({ object_id: objectId }));
  }, [objectId, dispatch]);

  useEffect(() => {
    let tem = [{ label: t("knowledgeBase.all"), value: "all" }];
    listCategory.forEach((item) => {
      tem.push({ label: item.title, value: item.key });
    });
    setCategorys(tem);
  }, [listCategory, t]);

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  const renderOption = (list, option) => {
    return list.map((item, index) => {
      if (option === "time") {
        return (
          <Item
            key={index}
            active={item.value === dataSearch.created_date}
            color={hexToRGB(defaultBrandName.theme_color)}
            onClick={() => {
              dispatch(updateLoadMore(false));
              dispatch(
                updateDataSearch({
                  created_date: item.value,
                  page: 0,
                })
              );
            }}
          >
            {item.label}
          </Item>
        );
      } else {
        return (
          <Item
            key={index}
            active={item.value === dataSearch.category}
            color={hexToRGB(defaultBrandName.theme_color)}
            onClick={() => {
              dispatch(updateLoadMore(false));
              dispatch(
                updateDataSearch({
                  category: item.value,
                  page: 0,
                })
              );
            }}
          >
            {item.label}
          </Item>
        );
      }
    });
  };

  return (
    <Wrap>
      <p>{t("knowledgeBase.search")}</p>
      <CustomInputSearch
        defaultValue={dataSearch.text_search}
        allowClear
        placeholder={`${t("knowledgeBase.search")}...`}
        onSearch={(e) => {
          dispatch(updateLoadMore(false));
          dispatch(updateDataSearch({ text_search: e, page: 0 }));
        }}
      />
      <Option>
        <p>{t("knowledgeBase.searchBy")}</p>

        <Checkbox.Group
          defaultValue={dataSearch.text_fields}
          onChange={(value) => {
            dispatch(updateLoadMore(false));
            dispatch(updateDataSearch({ text_fields: value, page: 0 }));
          }}
        >
          <Checkbox value="title">{t("knowledgeBase.title")}</Checkbox>
          <Checkbox value="body">{t("knowledgeBase.content")}</Checkbox>
          <Checkbox value="tag">{t("knowledgeBase.tag")}</Checkbox>
          <Checkbox value="article_id">ID</Checkbox>
        </Checkbox.Group>
      </Option>

      <Option>
        <p>{t("knowledgeBase.time")}</p>
        {renderOption(listTime, "time")}
      </Option>

      <Option>
        <p>{t("knowledgeBase.category")}</p>

        <CategoryWrap> {renderOption(categorys, "category")}</CategoryWrap>
      </Option>
    </Wrap>
  );
};

export default OptionSearch;

const Wrap = styled.div`
  padding: 24px;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ececec;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  height: fit-content;
  width: 350px;
  margin-right: 16px;
  font-size: 16px;
  p {
    font-size: 18px;
    font-family: var(--roboto-500);
    margin-bottom: 16px;
  }
`;

const CustomInputSearch = styled(Search)`
  .ant-input:focus,
  .ant-input-affix-wrapper:focus,
  .ant-input-affix-wrapper-focused {
    box-shadow: none;
    border-color: ${(props) => props.theme.main};
  }

  .ant-btn:hover,
  .ant-btn:focus,
  .ant-input:focus,
  .ant-input:hover,
  .ant-input-affix-wrapper:not(.ant-input-affix-wrapper-disabled):hover {
    border-color: ${(props) => props.theme.main};
  }

  input {
    height: 30px;
  }
  button {
    height: 40px;
  }
`;

const Option = styled.div`
  margin-top: 16px;
  p {
    margin-bottom: 8px;
    font-size: 16px;
  }
  .ant-checkbox + span {
    font-size: 16px;
  }
  .ant-checkbox-group {
    display: flex;
    justify-content: space-between;
  }
  /* .ant-checkbox-wrapper {
    margin-right: 16px;
  } */
  .ant-checkbox-wrapper + .ant-checkbox-wrapper {
    margin-left: 0px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main}!important;
  }
`;

const Item = styled.div`
  width: 100%;
  font-family: ${(props) =>
    props.active ? `var(--roboto-500)` : `var(--roboto-400)`};
  color: ${(props) => (props.active ? props.theme.main : `#2c2c2c`)};
  cursor: pointer;
  border-radius: 6px;
  background: ${(props) => (props.active ? props.color : `unset`)};
  padding: 8px 16px;
  transition: all 0.5s;
  :hover {
    color: ${(props) => props.theme.main};
    background-color: ${(props) => props.color};
  }
`;

const CategoryWrap = styled.div`
  width: 100%;
`;
