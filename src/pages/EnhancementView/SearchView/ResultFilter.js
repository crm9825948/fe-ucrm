import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import {
  getListArticleFilter,
  updateLoadMore,
} from "redux/slices/enhancementView";
import styled from "styled-components";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { Spin } from "antd";
import InfiniteScroll from "react-infinite-scroll-component";
import EmptyImg from "assets/icons/knowledgeEnhancement/empty.png";
import ItemArticle from "../ItemArticle";
import { useTranslation } from "react-i18next";
import _ from "lodash";

const ResultFilter = ({ objectFilter, isLoadMore, list }) => {
  const { listKnowledgeSetting } = useSelector(
    (state) => state.knowledgeEnhancementReducer
  );
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { objectId, categoryId } = useParams();
  const [listArticle, setListArticle] = useState([]);
  const [page, setPage] = useState(0);

  const [config, setConfig] = useState({});

  const { article_setting } = config;
  useEffect(() => {
    dispatch(updateLoadMore(false));
    setPage(0);
  }, [dispatch, categoryId]);

  useEffect(() => {
    if (objectFilter.records) {
      if (isLoadMore) {
        const temp = listArticle.concat(objectFilter.records);
        setListArticle(temp);
      } else {
        setListArticle(objectFilter.records);
      }
    }
    //eslint-disable-next-line
  }, [objectFilter]);

  useEffect(() => {
    listKnowledgeSetting.forEach((setting) => {
      if (setting.article_setting.obj_article === objectId) {
        setConfig(setting);
      }
    });
  }, [listKnowledgeSetting, objectId]);

  const handleNameCategory = () => {
    let name = "";
    list.forEach((cate) => {
      if (cate.value === categoryId) {
        name = cate.label;
      }
    });
    return name;
  };

  const renderListArticle = (list, setting) => {
    return list.map((article, index) => {
      return (
        <ItemArticle
          key={index}
          config={config}
          setting={setting}
          article={article}
        />
      );
    });
  };

  const loadMoreArticle = () => {
    dispatch(updateLoadMore(true));
    let next = page + 1;
    dispatch(
      getListArticleFilter({
        object_id: objectId,
        data: {
          category: categoryId,
          page: next,
          size: 10,
        },
      })
    );
    setPage(next);
  };

  return (
    <Wrap>
      <HeaderWrap>
        <Right>
          <CustomIcon
            onClick={() => navigate(`/knowledge-base-enhancement/${objectId}`)}
          >
            <ArrowLeftOutlined />
          </CustomIcon>
          {handleNameCategory()}
        </Right>
        <Customspan>
          {listArticle.length} of {_.get(objectFilter, "total", 0)} articles
        </Customspan>
      </HeaderWrap>
      {listArticle.length === 0 ? (
        <NoData>
          <DataWrap>
            <span>
              <img src={EmptyImg} alt="no data " />
            </span>
            <p>
              {t("knowledgeBase.noResultCate")}{" "}
              <span>"{handleNameCategory()}"</span>
            </p>
          </DataWrap>
        </NoData>
      ) : (
        <ContentWrap id="scrollableDiv">
          <InfiniteScroll
            hasMore={listArticle.length < objectFilter.total}
            dataLength={listArticle.length}
            loader={<Spin />}
            next={loadMoreArticle}
            scrollableTarget="scrollableDiv"
          >
            {renderListArticle(listArticle, article_setting)}
          </InfiniteScroll>
        </ContentWrap>
      )}
    </Wrap>
  );
};

export default ResultFilter;

const Wrap = styled.div`
  color: #2c2c2c;
  line-height: 22px;
  flex: 1;
  background: #fff;
  border-radius: 6px;
  border: 1px solid #ececec;
  background: #fff;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  padding: 24px;
  height: fit-content;
`;

const HeaderWrap = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
  align-items: center;
`;

const Right = styled.div`
  font-size: 18px;
  font-family: var(--roboto-500);
  line-height: 26px;
  display: flex;
`;

const ContentWrap = styled.div`
  width: 100%;
  max-height: calc(100vh - 274px);
  overflow: auto;
`;
const NoData = styled.div`
  display: flex;
  justify-content: center;

  img {
    width: 100px;
  }
`;
const DataWrap = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  flex-direction: column;
  p {
    font-size: 16px;
    line-height: 22px;
    margin-top: 8px;
    span {
      color: ${(props) => props.theme.main};
    }
  }
`;
const Customspan = styled.span`
  color: #637381;
  font-size: 14px;
  margin-left: 8px;
`;

const CustomIcon = styled.div`
  margin-right: 16px;
  border-radius: 6px;
  border: 1px solid #d9d9d9;
  width: 24px;
  height: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  svg {
    font-size: 14px;
  }
`;
