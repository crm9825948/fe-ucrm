import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { Dropdown, Breadcrumb, Menu } from "antd";
import { useTranslation } from "react-i18next";
import { useDispatch, useSelector } from "react-redux";
import { CaretDownOutlined, SyncOutlined } from "@ant-design/icons";
import KanbanViewImg from "assets/icons/common/kanbanView.png";
import ListViewImg from "assets/icons/common/listView.png";
import ListViewDetailImg from "assets/icons/common/listViewDetail.png";
import { useNavigate } from "react-router";
import { syncData, updateView } from "redux/slices/enhancementView";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import { loadListObjectField } from "redux/slices/objects";

const HeaderView = ({ objectId }) => {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { userDetail } = useSelector((state) => state.userReducer);
  const { objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const [write, setWrite] = useState(false);

  useEffect(() => {
    /* eslint-disable-next-line */
    listObjectField.map((item, idx) => {
      if (Object.keys(item)[0] === "main_object") {
        setWrite(item[Object.keys(item)[0]]?.create_permission);
      }
    });
  }, [listObjectField]);
  useEffect(() => {
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectId,
        show_meta_fields: true,
      })
    );
  }, [dispatch, objectId]);
  const handleNameObject = () => {
    let name = "";
    // eslint-disable-next-line
    Object.entries(objectCategory).forEach(([key, value]) => {
      // eslint-disable-next-line
      value.map((object) => {
        if (object._id === objectId) {
          name = object.Name;
        }
      });
    });
    return name;
  };

  const handleNameGroupObject = () => {
    let name = "";
    // eslint-disable-next-line
    Object.entries(objectCategory).forEach(([key, value]) => {
      // eslint-disable-next-line
      value.map((object) => {
        if (object._id === objectId) {
          name = key;
        }
      });
    });
    return name;
  };
  const menuView = (
    <Menu>
      <CustomItem
        onClick={() => {
          dispatch(updateView(true));
          navigate("/objects/" + objectId + "/default-view");
        }}
      >
        <ItemText> List view </ItemText>
        <img src={ListViewImg} alt="Kanban view" />
      </CustomItem>
      <CustomItem
        onClick={() => {
          navigate(`/kanban-view/${objectId}/default-view`);
        }}
      >
        <ItemText> Kanban view </ItemText>
        <img src={KanbanViewImg} alt="Kanban view" />
      </CustomItem>
      <CustomItem
        onClick={() => {
          navigate(`/list-view-with-details/objects/${objectId}/default-view`);
        }}
      >
        <ItemText> List view with detail</ItemText>
        <img src={ListViewDetailImg} alt=" List view with detail" />
      </CustomItem>
    </Menu>
  );

  return (
    <CustomWrapperAction>
      <Breadcrumb>
        <CustomBreadCrumb>{t("common.object")}</CustomBreadCrumb>
        <CustomBreadCrumb>{handleNameGroupObject()}</CustomBreadCrumb>
        <BreadcrumbItem
          onClick={() => navigate(`/knowledge-base-enhancement/${objectId}`)}
        >
          {handleNameObject()}
        </BreadcrumbItem>
      </Breadcrumb>

      <RightWrap>
        {write && (
          <Button
            style={{ marginRight: "16px" }}
            size="large"
            onClick={() => {
              navigate(
                `/knowledge-base-enhancement/create-article/${objectId}`
              );
            }}
          >
            <img alt="" src={plusIcon} />
            {t("object.addRecord")}
          </Button>
        )}
        {userDetail.Is_Admin && (
          <WrapIcon onClick={() => dispatch(syncData({ object_id: objectId }))}>
            <span>
              <SyncOutlined />
            </span>
          </WrapIcon>
        )}

        <ViewWrap>
          <Dropdown overlay={menuView} arrow trigger={["click"]}>
            <OptionView>
              <img src={ListViewImg} alt="" /> <Text>Knowledge</Text>
              <CaretDownOutlined />
            </OptionView>
          </Dropdown>
        </ViewWrap>
      </RightWrap>
    </CustomWrapperAction>
  );
};

export default HeaderView;

const CustomWrapperAction = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
  width: 100%;

  .ant-btn:hover {
    border-color: ${(props) => props.theme.main};
  }
  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const CustomBreadCrumb = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: rgba(0, 0, 0, 0.45);
  cursor: default;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-size: 16px !important;
  color: #2c2c2c;
  cursor: pointer;
`;

const RightWrap = styled.div`
  display: flex;
`;

const ViewWrap = styled.div`
  width: 140px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  cursor: pointer;
  margin-left: 16px;
`;

const OptionView = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Text = styled.div`
  margin: 0 6px;
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;

const CustomItem = styled(Menu.Item)`
  .ant-dropdown-menu-title-content {
    display: flex;
    justify-content: space-between;
  }

  img {
    width: 19px;
  }
`;

const ItemText = styled.span`
  font-size: 16px;
  line-height: 22px;
  margin-right: 30px;
  margin-left: 8px;
`;

const Button = styled.div`
  width: 130px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${(props) => props.theme.darker};
  cursor: pointer;
  transition: all 0.5s;
  border-radius: 2px;
  font-size: 16px;
  background: ${(props) => props.theme.main};
  color: #fff;

  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff;
    border: ${(props) => props.theme.darker};
  }
`;

const WrapIcon = styled.div`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 2px;
  border: 1px solid #d9d9d9;
  cursor: pointer;
  background: #fff;
`;
