import React, { useRef } from "react";
import { useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import styled from "styled-components";

const CategoryGroup = ({ category, children, setting }) => {
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { objectId } = useParams();
  const navigate = useNavigate();
  const ref = useRef(null);
  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };
  const handleNameCategory = (category, setting) => {
    if (category && setting) {
      return (
        <Item
          color={hexToRGB(defaultBrandName.theme_color)}
          onClick={() =>
            navigate(
              `/knowledge-base-enhancement/${objectId}/category/${category._id}`
            )
          }
        >
          {category[setting?.fld_category]?.value}
        </Item>
      );
    } else {
      return "";
    }
  };
  const scroll = (scrollOffset) => {
    ref.current.scrollLeft += scrollOffset;
  };
  const render = () => {
    return (
      <CategoryWrap ref={ref}>
        <ScrollButton
          style={{ bottom: "18px", left: "0px" }}
          onClick={() => scroll(-150)}
        >
          <LeftOutlined />
        </ScrollButton>
        {children.map((child, index) => {
          return (
            <ChildCategory key={index}>
              {handleNameCategory(child, setting)}
            </ChildCategory>
          );
        })}
        <ScrollButton
          style={{ bottom: "18px", right: "0px" }}
          onClick={() => scroll(150)}
        >
          <RightOutlined />
        </ScrollButton>
      </CategoryWrap>
    );
  };
  return (
    <Wrap>
      <ParentCategory>{handleNameCategory(category, setting)}</ParentCategory>
      {render()}
    </Wrap>
  );
};

export default CategoryGroup;

const Wrap = styled.div`
  width: 100%;
  color: #2c2c2c;
  font-size: 16px;
  position: relative;

  span {
    width: 100%;
  }
`;
const CategoryWrap = styled.div`
  width: 100%;
  display: flex;
  overflow: auto;
  border-bottom: 1px solid #006964;
  padding-bottom: 16px;
  margin-bottom: 24px;
  font-size: 18px;
  ::-webkit-scrollbar {
    width: 0px !important;
    height: 0px !important;
  }
`;
const ParentCategory = styled.div`
  font-size: 20px;
  font-family: var(--roboto-500);
  line-height: 30px;
`;

const ChildCategory = styled.div`
  cursor: pointer;
  line-height: 26px;
`;

const Item = styled.div`
  color: ${(props) => (props.active ? props.theme.main : `#2c2c2c`)};
  cursor: pointer;
  border-radius: 6px;
  background: ${(props) => (props.active ? props.color : `unset`)};
  padding: 6px 12px;
  transition: all 0.5s;
  white-space: nowrap;
  :hover {
    color: ${(props) => props.theme.main};
    background-color: ${(props) => props.color};
  }
`;

const ScrollButton = styled.div`
  width: 36px;
  height: 36px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.05);
  border-radius: 50%;
  border: 1px solid #d9d9d9;
  background: #fff;
  cursor: pointer;
  span {
    width: fit-content;
  }
`;
