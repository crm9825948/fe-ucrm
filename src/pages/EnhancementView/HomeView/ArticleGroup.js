import React from "react";
import styled from "styled-components";
import CategoryGroup from "./CategoryGroup";
import ListArticle from "./ListArticle";
import MainArticle from "./MainArticle";
import { useEffect } from "react";
import { useState } from "react";

const ArticleGroup = ({ group, config }) => {
  const [mainArticle, setMainArticle] = useState({});
  const [listArticle, setListArticle] = useState([]);

  useEffect(() => {
    if (group && group.article.length > 0) {
      setMainArticle(group.article[0]);
      setListArticle(group.article.slice(1));
    }
  }, [group]);

  return (
    <Wrap>
      <CategoryGroup
        setting={config.category_setting}
        category={group.category}
        children={group.children}
      />
      <WrapContent>
        <MainArticle
          config={config}
          setting={config.article_setting}
          article={mainArticle}
        />
        <ListArticle
          setting={config.article_setting}
          listArticle={listArticle}
        />
      </WrapContent>
    </Wrap>
  );
};

export default ArticleGroup;

const Wrap = styled.div`
  background: #fff;
  margin-bottom: 24px;
  margin-right: 24px;
  border-bottom: 1px solid #ececec;
  padding-bottom: 24px;
`;

const WrapContent = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`;
