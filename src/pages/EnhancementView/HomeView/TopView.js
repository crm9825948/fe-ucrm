import React from "react";
import styled from "styled-components";

import _ from "lodash";
import { FE_URL } from "constants/constants";
import { useParams } from "react-router";
import CopyImg from "assets/icons/knowledgeEnhancement/LinkIcon.svg";
import { Tooltip } from "antd";
import { Notification } from "components/Notification/Noti";
import { useTranslation } from "react-i18next";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import AuthenImage from "components/Image/AuthenImage";

const TopView = ({ setting, listArticleTopview, isTop, isDetail }) => {
  const { objectId } = useParams();
  const { t } = useTranslation();
  const renderSrc = (setting, article) => {
    if (_.get(article, `${setting?.fld_thumbnail}.value`, [])) {
      return _.get(article, `${setting?.fld_thumbnail}.value`, [])[0];
    } else {
      return "";
    }
  };
  const renderContent = (list, setting) => {
    return list.map((article, index) => {
      return (
        <TopArticle
          onClick={() => {
            window.open(
              `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
            );
          }}
        >
          {renderSrc(setting, article) && (
            <AuthenImage src={renderSrc(setting, article)} />
          )}

          <ArticleWrap>
            <TitleTop>
              <span>
                {_.get(article, `${setting?.fld_title}.value`, "No title")}
              </span>
              <Tooltip title={t("knowledgeBase.copy")}>
                <img
                  src={CopyImg}
                  alt="Copy"
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    navigator.clipboard.writeText(
                      `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
                    );
                    Notification("success", "Copy link article success");
                  }}
                />
              </Tooltip>
            </TitleTop>
            {_.get(article, `${setting?.fld_body}.value`, "No body") && (
              <Content>
                {parse(
                  _.get(
                    article,
                    `${setting?.fld_body}.value`,
                    "No body"
                  ).replace(/<[^>]*>/g, ""),
                  optionsParse
                )}
              </Content>
            )}
          </ArticleWrap>
        </TopArticle>
      );
    });
  };
  return (
    <Wrap isDetail={isDetail}>
      <Header>
        {isTop ? t("knowledgeBase.topView") : t("knowledgeBase.newestArticle")}
      </Header>
      {renderContent(listArticleTopview, setting)}
    </Wrap>
  );
};

export default TopView;

const Wrap = styled.div`
  background: #fff;
  width: 100%;
  line-height: 22px;
  color: #282828;
  border-left: 1px solid #ececec;
  padding: 0px 0px 24px 24px;
`;

const TopArticle = styled.div`
  margin-top: 24px;
  display: flex;
  cursor: pointer;
  img {
    width: 100px;
    height: 100px;
    border-radius: 8px;
    object-fit: cover;
    margin-right: 20px;
  }
`;

const Header = styled.div`
  font-size: 20px;
  font-family: var(--roboto-500);
  line-height: 30px;
  width: fit-content;
  border-bottom: 1px solid #006964;
  padding-bottom: 8px;
`;

const ArticleWrap = styled.div`
  flex: 1;
  cursor: pointer;
`;

const Content = styled.div`
  color: #6b6b6b;
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  word-break: break-word;
  p {
    margin-bottom: 0px;
  }
`;
const TitleTop = styled.div`
  font-size: 16px;
  font-family: var(--roboto-500);
  display: flex;
  justify-content: space-between;

  span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    line-height: 26px;
  }

  img {
    margin-top: 4px;
    margin-left: 6px;
    width: 16px;
    height: 16px;
  }
`;
