import React from "react";
import styled from "styled-components";
import _ from "lodash";
import { useParams } from "react-router";
import { FE_URL } from "constants/constants";
import CopyImg from "assets/icons/knowledgeEnhancement/LinkIcon.svg";
import { Tooltip } from "antd";
import { Notification } from "components/Notification/Noti";
import { useTranslation } from "react-i18next";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import AuthenImage from "components/Image/AuthenImage";
const ListArticle = ({ setting, listArticle }) => {
  const { t } = useTranslation();
  const { objectId } = useParams();
  const renderTag = (setting, article) => {
    if (_.get(article, `${setting?.fld_tag}.value`, "")) {
      return _.get(article, `${setting?.fld_tag}.value`, "")
        .split("#")
        .map((tag, index) => tag && <Tag key={index}>#{tag.trim()}</Tag>);
    } else {
      return "";
    }
  };
  const renderSrc = (setting, article) => {
    if (_.get(article, `${setting?.fld_thumbnail}.value`, [])) {
      return _.get(article, `${setting?.fld_thumbnail}.value`, [])[0];
    } else {
      return "";
    }
  };
  const renderContent = (list) => {
    return list.map((article, index) => {
      return (
        <ArticleWrap
          key={index}
          onClick={() => {
            window.open(
              `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
            );
          }}
        >
          <ImgWrap>
            {renderSrc(setting, article) && (
              <AuthenImage src={renderSrc(setting, article)} />
            )}
          </ImgWrap>

          <ContentWrap>
            <Title>
              <span>
                {_.get(article, `${setting?.fld_title}.value`, "No title")}
              </span>
              <Tooltip title={t("knowledgeBase.copy")}>
                <img
                  src={CopyImg}
                  alt="Copy"
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    navigator.clipboard.writeText(
                      `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
                    );
                    Notification("success", "Copy link article success");
                  }}
                />
              </Tooltip>
            </Title>
            {_.get(article, `${setting?.fld_body}.value`, "No body") && (
              <Content>
                {parse(
                  _.get(
                    article,
                    `${setting?.fld_body}.value`,
                    "No body"
                  ).replace(/<[^>]*>/g, ""),
                  optionsParse
                )}
              </Content>
            )}
            <TagWrap style={{ display: "none" }}>
              {renderTag(setting, article)}
            </TagWrap>
          </ContentWrap>
        </ArticleWrap>
      );
    });
  };

  return <Wrap>{renderContent(listArticle)}</Wrap>;
};

export default ListArticle;

const Wrap = styled.div`
  flex: 1;
  margin-left: 24px;
`;

const ArticleWrap = styled.div`
  cursor: pointer;
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  margin-bottom: 16px;
`;

const Title = styled.div`
  font-size: 16px;
  font-family: var(--roboto-500);
  margin-bottom: 4px;
  display: flex;
  justify-content: space-between;
  align-items: start;
  span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    line-height: 25px;
  }

  img {
    margin-top: 3px;
    margin-left: 6px;
    width: 16px !important;
  }
`;
const Content = styled.div`
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
  word-break: break-word;
  color: #6b6b6b;
  line-height: 24px;
  p {
    margin-bottom: 0px;
  }
`;

const TagWrap = styled.div`
  display: flex;
  margin: 8px 0 16px 0;
  flex-wrap: wrap;
`;

const Tag = styled.div`
  background: rgba(32, 162, 162, 0.15);
  border-radius: 5px;
  color: #0a7979;
  line-height: 24px;
  padding: 0 6px;
  margin-right: 8px;
  margin-bottom: 8px;
`;

const ImgWrap = styled.div`
  img {
    width: 100px;
    height: 100px;
    border-radius: 8px;
    margin-right: 20px;
    object-fit: cover;
  }
`;

const ContentWrap = styled.div`
  flex: 1;
`;
