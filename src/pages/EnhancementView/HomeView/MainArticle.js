import React from "react";
import styled from "styled-components";
import {
  EyeFilled,
  LikeFilled,
  // DislikeFilled,
  CalendarFilled,
} from "@ant-design/icons";
import _ from "lodash";
import { FE_URL } from "constants/constants";
import { useParams } from "react-router";
import CopyImg from "assets/icons/knowledgeEnhancement/LinkIcon.svg";
import { Tooltip } from "antd";
import { Notification } from "components/Notification/Noti";
import { useTranslation } from "react-i18next";
// import Truncate from "react-truncate-html";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
import AuthenImage from "components/Image/AuthenImage";

const MainArticle = ({ setting, article, config }) => {
  const { t } = useTranslation();
  const { objectId } = useParams();
  const renderTag = (setting, article) => {
    if (_.get(article, `${setting?.fld_tag}.value`, "")) {
      return _.get(article, `${setting?.fld_tag}.value`, "")
        .split("#")
        .map((tag, index) => tag && <Tag key={index}>#{tag.trim()}</Tag>);
    } else {
      return "";
    }
  };

  const renderSrc = (setting, article) => {
    if (_.get(article, `${setting?.fld_thumbnail}.value`, [])) {
      return _.get(article, `${setting?.fld_thumbnail}.value`, [])[0];
    } else {
      return "";
    }
  };

  return (
    <>
      {Object.entries(article).length > 0 && (
        <Wrap
          onClick={() => {
            window.open(
              `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
            );
          }}
        >
          {renderSrc(setting, article) && (
            <ImgWrap>
              <AuthenImage src={renderSrc(setting, article)} />

              {/* <img src={renderSrc(setting, article)} alt="Thumbnail" /> */}
            </ImgWrap>
          )}
          <Title>
            <span>
              {_.get(article, `${setting?.fld_title}.value`, "No title")}
            </span>
            <Tooltip title={t("knowledgeBase.copy")}>
              <img
                src={CopyImg}
                alt="Copy"
                onClick={(e) => {
                  e.preventDefault();
                  e.stopPropagation();
                  navigator.clipboard.writeText(
                    `${FE_URL}/knowledge-base-enhancement/details/${objectId}/${article._id}`
                  );
                  Notification("success", "Copy link article success");
                }}
              />
            </Tooltip>
          </Title>
          {_.get(article, `${setting?.fld_body}.value`, "No body") && (
            <Content>
              {parse(
                _.get(article, `${setting?.fld_body}.value`, "No body").replace(
                  /<[^>]*>/g,
                  ""
                ),
                optionsParse
              )}
            </Content>
          )}
          <TagWrap style={{ display: "none" }}>
            {renderTag(setting, article)}
          </TagWrap>
          <Line style={{ display: "none" }} />

          <ActionWrap style={{ display: "none" }}>
            {config.show_total_views && (
              <Action>
                <EyeFilled />
                {_.get(article, `${setting?.fld_total_views}.value`, "0")
                  ? _.get(article, `${setting?.fld_total_views}.value`, "0")
                  : "0"}
              </Action>
            )}
            {config.show_votes && (
              <Action>
                <LikeFilled />
                {_.get(article, `${setting?.fld_upvotes}.value`, "0")
                  ? _.get(article, `${setting?.fld_upvotes}.value`, "0")
                  : "0"}
              </Action>
            )}

            <Action>
              <CalendarFilled /> {article.created_date}
            </Action>
          </ActionWrap>
        </Wrap>
      )}
    </>
  );
};

export default MainArticle;

const Wrap = styled.div`
  color: #2c2c2c;
  cursor: pointer;
  width: 668px;
`;

const ImgWrap = styled.div`
  width: 100%;
  img {
    width: 100%;
    object-fit: cover;
    height: 360px;
    border-radius: 8px;
  }
`;

const Title = styled.div`
  font-size: 20px;
  font-family: var(--roboto-500);
  margin: 20px 0px 8px 0px;
  display: flex;
  justify-content: space-between;
  line-height: 31px;
  align-items: start;
  span {
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
  }

  img {
    margin-top: 8px;
    margin-left: 6px;
    width: 16px !important;
  }
`;

const Content = styled.div`
  font-size: 16px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
  word-break: break-word;
  line-height: 26px;
  color: #6b6b6b;
  p {
    margin-bottom: 0px;
  }
`;

const TagWrap = styled.div`
  display: flex;
  margin: 8px 0 16px 0;
  flex-wrap: wrap;
`;

const Tag = styled.div`
  background: rgba(32, 162, 162, 0.15);
  border-radius: 5px;
  color: #0a7979;
  line-height: 24px;
  padding: 0 6px;
  margin-right: 8px;
  margin-bottom: 8px;
`;

const Line = styled.div`
  height: 1px;
  background: #ececec;
  width: 100%;
`;

const ActionWrap = styled.div`
  display: flex;
`;
const Action = styled.div`
  margin-right: 16px;
  font-size: 12px;
  line-height: 16px;
  color: #6b6b6b;
  font-family: var(--roboto-500);
  display: flex;
  margin-top: 16px;
  margin-bottom: 8px;
  svg {
    font-size: 16px;
    margin-right: 6px;
    cursor: pointer;
  }
`;
