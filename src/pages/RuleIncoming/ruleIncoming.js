import _ from "lodash";
import styled from "styled-components/macro";
import { useEffect, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import { useParams } from "react-router";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";

import { CheckOutlined } from "@ant-design/icons";
import Breadcrumb from "antd/lib/breadcrumb";
import Button from "antd/lib/button";
import Switch from "antd/lib/switch";
import Form from "antd/lib/form";
import Typography from "antd/lib/typography";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Tooltip from "antd/lib/tooltip";

import {
  getListRule,
  getRuleById,
  updateStatusRule,
  getListRuleSuccess,
  updatePositionRule,
  unmountRuleIncoming,
  updateEditRuleIncoming,
  deleteRuleById,
  deleteRuleByIdSuccess,
  updateRule,
  saveRule,
  setIsCreate,
} from "redux/slices/ruleIncoming";
import { getUserName } from "redux/slices/user";
import { loadListObjectField } from "redux/slices/objects";
import { changeTitlePage } from "redux/slices/authenticated";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { Notification } from "components/Notification/Noti";

import Number from "./FieldType/number";
import Date from "./FieldType/date";
import SelectType from "./FieldType/select";
import SelectTypeReply from "./FieldType/selectReply";
import File from "./FieldType/file";
import User from "./FieldType/user";
import Linking from "./FieldType/linkingobject";
import Other from "./FieldType/other";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";

import DeleteIcon from "assets/icons/common/delete.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

const { Text } = Typography;

const optionsOnFail = [
  {
    label: "Send Email",
    value: "send_email",
  },
  {
    label: "Send Notification",
    value: "send_noti",
  },
  {
    label: "Pass",
    value: "pass",
  },
  {
    label: "Off this rule",
    value: "off_this_rule",
  },
];

function RuleIncoming(props) {
  const { t } = useTranslation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { recordID, objectID } = useParams();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { listUserName } = useSelector((state) => state.userReducer);
  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { listRule, editRuleIncoming, isDelete, isCreate } = useSelector(
    (state) => state.ruleIncomingReducer
  );

  const [selectedRule, $selectedRule] = useState(0);
  const [requiredFields, $requiredFields] = useState([]);
  const [optionalFields, $optionalFields] = useState([]);
  const [fullFields, $fullFields] = useState([]);

  const [initRequiredFieldsValue, $initRequiredFieldsValue] = useState([]);
  const [requiredFieldsValue, $requiredFieldsValue] = useState([]);
  const [optionalFieldsSelected, $optionalFieldsSelected] = useState([]);
  const [replyFieldsSelected, $replyFieldsSelected] = useState([]);

  const [dataDelete, $dataDelete] = useState({});
  const [formValue, setFormValue] = useState({});

  const _onSubmit = () => {
    const fieldsMapping = requiredFieldsValue
      .concat(optionalFieldsSelected)
      .concat(replyFieldsSelected);

    let checkEmptyMapping = false;

    fieldsMapping.forEach((item) => {
      if (!item.value) {
        checkEmptyMapping = true;
      }
    });

    requiredFieldsValue.forEach((item) => {
      if (item.type === "linkingobject" || item.type === "lookup") {
        if (!item.value || !item.default_value) {
          checkEmptyMapping = true;
        }
      }
    });

    if (!form.getFieldValue("ruleName") || !form.getFieldValue("assignTo")) {
      Notification("warning", "Please fullfill required!");
    } else if (checkEmptyMapping) {
      Notification("warning", "Please fullfill mapping!");
    } else {
      const payload = {
        id_mailbox: recordID,
        name_rule: form.getFieldsValue().ruleName,
        from: form.getFieldsValue().from || "",
        to: form.getFieldsValue().to || "",
        cc: form.getFieldsValue().cc || "",
        bcc: form.getFieldsValue().bcc || "",
        has_id: false,
        subject: form.getFieldsValue().subject || "",
        match: form.getFieldsValue().match || "ALL",
        action: "Create",
        assign_to: form.getFieldsValue().assignTo,
        mapping: requiredFieldsValue.concat(optionalFieldsSelected),
        mapping_reply: replyFieldsSelected,
        status: false,
        on_fail: form.getFieldsValue().onFail || "send_noti",
      };

      if (editRuleIncoming._id) {
        dispatch(
          updateRule({
            ...payload,
            _id: editRuleIncoming._id,
            position: editRuleIncoming.position,
            status: editRuleIncoming.status,
          })
        );
      } else {
        dispatch(
          saveRule({
            ...payload,
          })
        );
      }
    }
  };

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "inbox" && item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const onDragEnd = (result) => {
    let tempRules = [...listRule];
    let payload = [];

    if (result.destination) {
      if (result.destination.index >= tempRules.length) {
        var k = result.destination.index - tempRules.length + 1;
        while (k--) {
          tempRules.push(undefined);
        }
      }
      tempRules.splice(
        result.destination.index,
        0,
        tempRules.splice(result.source.index, 1)[0]
      );

      tempRules.forEach((item, index) => {
        payload.push({
          _id: item._id,
          position: index,
        });
      });

      dispatch(getListRuleSuccess(tempRules));
      dispatch(
        updatePositionRule({
          id_mailbox: recordID,
          list_rules: payload,
        })
      );
    }
  };

  const _onAddRule = () => {
    let tempRules = [...listRule];
    tempRules.push({
      name_rule: "New rule",
      _id: "1",
    });
    $selectedRule(tempRules.length - 1);
    dispatch(getListRuleSuccess(tempRules));
    dispatch(setIsCreate(true));
    dispatch(updateEditRuleIncoming({}));
  };

  const _onCancelCreate = () => {
    let tempRules = [...listRule];
    tempRules.splice(tempRules.length - 1, 1);
    $selectedRule(0);
    dispatch(getListRuleSuccess(tempRules));
    dispatch(setIsCreate(false));
  };

  const _onAddOptionalField = () => {
    const item = [
      ...optionalFieldsSelected,
      { key_object: undefined, value: undefined, type: "" },
    ];
    $optionalFieldsSelected(item);
  };

  const _onDeleteFieldOptional = (index, key_object) => {
    return () => {
      let temp = [...optionalFieldsSelected];
      temp.splice(index, 1);
      $optionalFieldsSelected(temp);

      if (key_object) {
        form.setFieldsValue({
          [key_object]: undefined,
        });
      }
    };
  };

  const _onAddReplyField = () => {
    const item = [
      ...replyFieldsSelected,
      {
        key_object: undefined,
        key_object_reply: undefined,
        value: undefined,
        type: "",
      },
    ];
    $replyFieldsSelected(item);
  };

  const _onDeleteFieldReply = (index, key_object) => {
    return () => {
      let temp = [...replyFieldsSelected];
      temp.splice(index, 1);
      $replyFieldsSelected(temp);

      if (key_object) {
        form.setFieldsValue({
          [key_object]: undefined,
        });
      }
    };
  };

  const _onDeleteRule = (ruleId) => {
    return () => {
      if (isCreate) return;
      dispatch(setShowModalConfirmDelete(true));
      $dataDelete({
        _id: ruleId,
        id_mailbox: recordID,
      });
    };
  };

  const handleChangeField = (index, type) => {
    return (e) => {
      if (type === "optional") {
        let tempOptionalFields = [...optionalFieldsSelected];
        tempOptionalFields[index] = {
          ...tempOptionalFields[index],
          key_object: e,
          type: optionalFields.find((item) => item.full_field_id === e)?.type,
          value:
            optionalFields.find((item) => item.full_field_id === e)?.type ===
            "file"
              ? "attach_file"
              : undefined,
        };

        delete tempOptionalFields[index].default_value;

        if (e) {
          form.setFieldsValue({
            [e]: undefined,
          });
        }

        $optionalFieldsSelected(tempOptionalFields);
      } else if (type === "reply") {
        let tempReplyFields = [...replyFieldsSelected];
        tempReplyFields[index] = {
          ...tempReplyFields[index],
          key_object: e,
          key_object_reply: `${e}_reply`,
          type: fullFields.find((item) => item.full_field_id === e)?.type,
          value:
            fullFields.find((item) => item.full_field_id === e)?.type === "file"
              ? "attach_file"
              : undefined,
        };

        delete tempReplyFields[index].default_value;

        if (e) {
          form.setFieldsValue({
            [e]: undefined,
          });
        }

        $replyFieldsSelected(tempReplyFields);
      }
    };
  };

  const handleChangeValue = (type, typeField, value, index, defaultValue) => {
    let tempRequired = [...requiredFieldsValue];
    let tempOptional = [...optionalFieldsSelected];
    let tempReply = [...replyFieldsSelected];

    switch (typeField) {
      case "linkingobject":
      case "lookup":
        switch (type) {
          case "required":
            tempRequired[index] = {
              ...tempRequired[index],
              type: typeField,
              value: value,
              default_value: defaultValue,
            };
            $requiredFieldsValue(tempRequired);
            break;
          case "nonRequired":
            tempOptional[index] = {
              ...tempOptional[index],
              value: value,
              type: typeField,
              default_value: defaultValue,
            };
            $optionalFieldsSelected(tempOptional);

            break;
          case "reply":
            tempReply[index] = {
              ...tempReply[index],
              value: value,
              type: typeField,
              default_value: defaultValue,
            };
            $replyFieldsSelected(tempReply);
            break;
          default:
            break;
        }

        break;
      case "file":
        break;

      //text, textarea, email, select, dynamic-field, user, number,date, datetime-local
      default:
        switch (type) {
          case "required":
            tempRequired[index] = {
              ...tempRequired[index],
              value: value,
              type: typeField,
            };
            $requiredFieldsValue(tempRequired);
            break;
          case "nonRequired":
            tempOptional[index] = {
              ...tempOptional[index],
              value: value,
              type: typeField,
            };
            $optionalFieldsSelected(tempOptional);

            break;
          case "reply":
            tempReply[index] = {
              ...tempReply[index],
              value: value,
              type: typeField,
            };
            $replyFieldsSelected(tempReply);
            break;
          default:
            break;
        }
        break;
    }
  };

  const handleFieldType = (field, value, index, type, defaultValue) => {
    switch (field.type) {
      case "number":
        return (
          <Number
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            handleChangeValue={handleChangeValue}
          />
        );

      case "date":
      case "datetime-local":
        return (
          <Date
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            handleChangeValue={handleChangeValue}
          />
        );

      case "linkingobject":
      case "lookup":
        return (
          <Linking
            field={field}
            value={value}
            defaultValue={defaultValue}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            handleChangeValue={handleChangeValue}
          />
        );

      case "file":
        return (
          <File
            field={field}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
          />
        );

      case "user":
        return (
          <User
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            handleChangeValue={handleChangeValue}
          />
        );

      case "email":
      case "select":
      case "dynamic-field":
        return type === "reply" ? (
          <SelectTypeReply
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            objectID={objectID}
            editRuleIncoming={editRuleIncoming}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            handleChangeValue={handleChangeValue}
          />
        ) : (
          <SelectType
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            objectID={objectID}
            editRuleIncoming={editRuleIncoming}
            form={form}
            formValue={formValue}
            setFormValue={setFormValue}
            handleChangeValue={handleChangeValue}
          />
        );

      //text, textarea
      default:
        return (
          <Other
            field={field}
            value={value}
            type={type}
            index={index}
            required={
              type === "reply" ? false : _.get(field, "required", false)
            }
            handleChangeValue={handleChangeValue}
          />
        );
    }
  };

  useEffect(() => {
    let tempFullFields = [];

    let tempRequiredFields = [];
    let tempRequiredFieldsValue = [];

    let tempOptionalFields = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        if (Object.keys(item)[0] === "main_object") {
          Object.values(item)[0].sections.forEach((ele) => {
            ele.fields.forEach((field) => {
              if (
                field.hidden === false &&
                field.permission_hidden === false &&
                field.type !== "id"
              ) {
                if (field.required) {
                  if (
                    field.type !== "linkingobject" ||
                    (field.type === "linkingobject" && field.key)
                  ) {
                    //handle value
                    if (field.type === "file") {
                      tempRequiredFieldsValue.push({
                        key_object: field.full_field_id,
                        type: field.type,
                        value: "attach_file",
                      });
                    } else if (
                      field.type === "linkingobject" ||
                      field.type === "lookup"
                    ) {
                      tempRequiredFieldsValue.push({
                        key_object: field.full_field_id,
                        type: field.type,
                        value: undefined,
                        default_value: undefined,
                        field_linking: field.field,
                        related_object_id: field.objectname,
                      });
                    } else {
                      tempRequiredFieldsValue.push({
                        key_object: field.full_field_id,
                        type: field.type,
                        value: undefined,
                      });
                    }

                    tempRequiredFields.push(field);
                    tempFullFields.push(field);
                  }
                } else {
                  if (
                    field.type !== "formula" &&
                    field.type !== "bridging-field"
                  ) {
                    if (
                      field.type !== "linkingobject" ||
                      (field.type === "linkingobject" && field.key)
                    ) {
                      tempOptionalFields.push(field);
                      tempFullFields.push(field);
                    }
                  }
                }
              }
            });
          });
        }
      }
    });

    $requiredFields(tempRequiredFields);
    $requiredFieldsValue(tempRequiredFieldsValue);
    $initRequiredFieldsValue(tempRequiredFieldsValue);
    $optionalFields(tempOptionalFields);
    $fullFields(tempFullFields);
  }, [listObjectField]);

  useEffect(() => {
    if (
      Object.keys(editRuleIncoming).length > 0 &&
      (requiredFields.length > 0 || optionalFields.length > 0)
    ) {
      //handle edit
      form.setFieldsValue({
        ruleName: _.get(editRuleIncoming, "name_rule", undefined),
        subject: _.get(editRuleIncoming, "subject", undefined),
        from: _.get(editRuleIncoming, "from", undefined),
        to: _.get(editRuleIncoming, "to", undefined),
        cc: _.get(editRuleIncoming, "cc", undefined),
        bcc: _.get(editRuleIncoming, "bcc", undefined),
        assignTo: _.get(editRuleIncoming, "assign_to", undefined),
        match: _.get(editRuleIncoming, "match", "ALL"),
        onFail: _.get(editRuleIncoming, "on_fail", undefined),
      });

      let tempRequiredFieldsValue = [];
      let tempOptionalFields = [];

      editRuleIncoming?.mapping?.forEach((field) => {
        if (
          requiredFields.find((item) => item.full_field_id === field.key_object)
        ) {
          if (field.type === "file") {
            tempRequiredFieldsValue.push({
              key_object: _.get(field, "key_object", ""),
              type: _.get(field, "type", ""),
              value: _.get(field, "value", undefined),
            });
          } else if (
            field.type === "linkingobject" ||
            field.type === "lookup"
          ) {
            tempRequiredFieldsValue.push({
              key_object: _.get(field, "key_object", ""),
              type: _.get(field, "type", ""),
              value: _.get(field, "value", undefined),
              default_value: _.get(field, "default_value", undefined),
              field_linking: _.get(field, "field_linking", ""),
              related_object_id: _.get(field, "related_object_id", ""),
            });
          } else {
            tempRequiredFieldsValue.push({
              key_object: _.get(field, "key_object", ""),
              type: _.get(field, "type", ""),
              value: _.get(field, "value", undefined),
            });
          }
        } else {
          tempOptionalFields.push(field);
        }

        if (
          _.get(field, "type", "") === "select" ||
          _.get(field, "type", "") === "email" ||
          _.get(field, "type", "") === "dynamic-field"
        ) {
          form.setFieldsValue({
            [_.get(field, "key_object", "")]: _.get(field, "value", undefined),
          });
        }
      });

      _.get(editRuleIncoming, "mapping_reply", []).forEach((field) => {
        if (
          _.get(field, "type", "") === "select" ||
          _.get(field, "type", "") === "email" ||
          _.get(field, "type", "") === "dynamic-field"
        ) {
          form.setFieldsValue({
            [_.get(field, "key_object_reply", "")]: _.get(
              field,
              "value",
              undefined
            ),
          });
        }
      });

      $requiredFieldsValue(tempRequiredFieldsValue);
      $optionalFieldsSelected(tempOptionalFields);
      $replyFieldsSelected(_.get(editRuleIncoming, "mapping_reply", []));
    } else {
      form.resetFields();
      $requiredFieldsValue(initRequiredFieldsValue);
      $optionalFieldsSelected([]);
      $replyFieldsSelected([]);
    }
  }, [
    editRuleIncoming,
    form,
    requiredFields,
    optionalFields,
    initRequiredFieldsValue,
  ]);

  useEffect(() => {
    dispatch(
      getListRule({
        id_mailbox: recordID,
      })
    );
    dispatch(getUserName());
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: objectID,
      })
    );
  }, [dispatch, objectID, recordID]);

  useEffect(() => {
    if (selectedRule === 0 && listRule.length > 0) {
      dispatch(
        getRuleById({
          _id: _.get(listRule[0], "_id", ""),
        })
      );
    }
  }, [listRule, selectedRule, dispatch]);

  useEffect(() => {
    dispatch(changeTitlePage(t("emailIncoming.viewRule")));
  }, [dispatch, t]);

  useEffect(() => {
    if (isDelete) {
      $selectedRule(0);
      dispatch(deleteRuleByIdSuccess(false));
    }
  }, [isDelete, dispatch]);

  useEffect(
    () => () => {
      dispatch(unmountRuleIncoming());
    },
    [dispatch]
  );

  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <Breadcrumb.Item onClick={() => navigate("/email-incoming")}>
            {t("emailIncoming.emailIncoming")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("emailIncoming.viewRule")}</BreadcrumbItem>
        </Breadcrumb>

        <div>
          {listRule.length > 0 && checkRule("edit") && (
            <SaveButton onClick={_onSubmit}>
              <CheckOutlined style={{ fontSize: 14 }} />
              {t("common.save")}
            </SaveButton>
          )}
          {isCreate && (
            <CancelButton onClick={_onCancelCreate}>
              {t("common.cancel")}
            </CancelButton>
          )}
        </div>
      </WrapBreadcrumb>

      {listRule.length > 0 ? (
        <Wrap>
          <Left>
            {checkRule("create") && (
              <AddNew onClick={!isCreate && _onAddRule}>
                + {t("emailIncoming.addNew")}
              </AddNew>
            )}

            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId="listRule">
                {(provided) => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    {_.map(listRule, (item, index) => (
                      <Draggable
                        key={_.get(item, "_id", "")}
                        draggableId={_.get(item, "_id", "")}
                        index={index}
                      >
                        {(provided) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Rule
                              active={selectedRule === index}
                              onClick={() => {
                                if (isCreate) return;
                                $selectedRule(index);

                                if (selectedRule !== index) {
                                  dispatch(
                                    getRuleById({
                                      _id: _.get(item, "_id", ""),
                                    })
                                  );
                                }
                              }}
                            >
                              <NameRule active={selectedRule === index}>
                                {_.get(item, "name_rule", "")}

                                {isCreate && index === listRule.length - 1 ? (
                                  <></>
                                ) : (
                                  <Switch
                                    size="small"
                                    checked={_.get(item, "status", "")}
                                    onClick={(value, e) => {
                                      if (isCreate) return;
                                      e.stopPropagation();
                                      dispatch(
                                        updateStatusRule({
                                          status: {
                                            _id: _.get(item, "_id", ""),
                                            status: value,
                                          },
                                          id_mailbox: _.get(
                                            item,
                                            "id_mailbox",
                                            ""
                                          ),
                                        })
                                      );
                                    }}
                                  />
                                )}
                              </NameRule>
                              <AssignTo>
                                <div>
                                  <span style={{ width: 75 }}>
                                    {t("emailIncoming.assignTo")}: &nbsp;
                                  </span>
                                  <Text
                                    ellipsis={{
                                      tooltip:
                                        Object.entries(listUserName).find(
                                          ([key]) =>
                                            key === _.get(item, "assign_to", "")
                                        ) &&
                                        Object.entries(listUserName).find(
                                          ([key]) =>
                                            key === _.get(item, "assign_to", "")
                                        )[1],
                                    }}
                                  >
                                    {Object.entries(listUserName).find(
                                      ([key]) =>
                                        key === _.get(item, "assign_to", "")
                                    ) &&
                                      Object.entries(listUserName).find(
                                        ([key]) =>
                                          key === _.get(item, "assign_to", "")
                                      )[1]}
                                  </Text>
                                </div>

                                {isCreate && index === listRule.length - 1 ? (
                                  <></>
                                ) : (
                                  <>
                                    {checkRule("delete") && (
                                      <DeleteRule
                                        className="action"
                                        onClick={_onDeleteRule(
                                          _.get(item, "_id", "")
                                        )}
                                      >
                                        {t("common.delete")}
                                      </DeleteRule>
                                    )}
                                  </>
                                )}
                              </AssignTo>
                            </Rule>
                          </div>
                        )}
                      </Draggable>
                    ))}

                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </Left>

          <Form
            form={form}
            layout="vertical"
            colon={false}
            onValuesChange={(value, values) => {
              setFormValue(values);
            }}
          >
            <Center>
              <RuleInfo>{t("emailIncoming.ruleInfo")}</RuleInfo>

              <WrapRuleInfo>
                <Form.Item
                  label={t("emailIncoming.ruleName")}
                  name="ruleName"
                  rules={[
                    {
                      required: true,
                      message: t("common.requiredField"),
                    },
                  ]}
                >
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>
                <Form.Item label={t("workflow.subject")} name="subject">
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>

                <Form.Item label={t("emailIncoming.from")} name="from">
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>

                <Form.Item label={t("emailIncoming.to")} name="to">
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>

                <Form.Item label="CC" name="cc">
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>

                <Form.Item label="BCC" name="bcc">
                  <Input placeholder={t("common.placeholderInput")} />
                </Form.Item>

                <Form.Item
                  label={t("emailIncoming.assignTo")}
                  name="assignTo"
                  rules={[
                    {
                      required: true,
                      message: t("common.requiredField"),
                    },
                  ]}
                >
                  <Select placeholder={t("common.placeholderSelect")}>
                    {Object.keys(listUserName).map((key) => (
                      <Select.Option value={key}>
                        {listUserName[key]}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>

                <Form.Item label={t("emailIncoming.match")} name="match">
                  <Radio.Group optionType="button" defaultValue="ALL">
                    <Radio value={"ALL"}>{t("common.allCondition")}</Radio>
                    <Radio value={"ANY"}>{t("common.anyCondition")}</Radio>
                  </Radio.Group>
                </Form.Item>

                <Form.Item label={t("emailIncoming.onFail")} name="onFail">
                  <Select
                    options={optionsOnFail}
                    placeholder={t("common.placeholderSelect")}
                    defaultValue="send_noti"
                  />
                </Form.Item>
              </WrapRuleInfo>
            </Center>

            <Right>
              <RuleInfo>{t("emailIncoming.mappingNewEmail")}</RuleInfo>

              <WrapMapping>
                {requiredFields.length > 0 && (
                  <RequiredFields>
                    <legend>{t("workflow.requiredFields")}</legend>

                    {_.map(requiredFields, (field, index) => (
                      <Row
                        gutter={8}
                        align="middle"
                        style={{
                          marginBottom:
                            index !== requiredFields.length - 1 ? 16 : 0,
                        }}
                      >
                        {handleFieldType(
                          field,
                          requiredFieldsValue[index]?.value,
                          index,
                          "required",
                          requiredFieldsValue[index]?.default_value
                        )}
                      </Row>
                    ))}
                  </RequiredFields>
                )}

                <OptionalFields>
                  <legend>{t("workflow.optionalFields")}</legend>

                  {_.map(optionalFieldsSelected, (field, index) => (
                    <Row
                      gutter={8}
                      align="middle"
                      style={{
                        marginBottom: 16,
                      }}
                    >
                      <Col flex="50%">
                        <Select
                          placeholder={t("common.placeholderSelect")}
                          style={{ width: "100%" }}
                          onChange={handleChangeField(index, "optional")}
                          value={_.get(field, "key_object", undefined)}
                        >
                          {_.map(optionalFields, (optionalField) => (
                            <Select.Option
                              key={_.get(optionalField, "full_field_id", "")}
                              value={_.get(optionalField, "full_field_id", "")}
                              disabled={
                                optionalFieldsSelected.find(
                                  (ele) =>
                                    _.get(ele, "key_object", "") ===
                                    _.get(optionalField, "full_field_id", "")
                                )
                                  ? true
                                  : false
                              }
                            >
                              {_.get(optionalField, "name", "")}
                            </Select.Option>
                          ))}
                        </Select>
                      </Col>

                      {_.get(field, "key_object", "") ? (
                        <>
                          {handleFieldType(
                            optionalFields.find(
                              (item) => item.full_field_id === field.key_object
                            ),
                            field.value,
                            index,
                            "nonRequired",
                            field?.default_value
                          )}
                        </>
                      ) : (
                        <ColCustom>
                          <Input disabled />
                        </ColCustom>
                      )}

                      <ColDelCustom>
                        <Tooltip title="Delete">
                          <Delete
                            onClick={_onDeleteFieldOptional(
                              index,
                              _.get(field, "key_object", "")
                            )}
                          >
                            <img src={DeleteIcon} alt="delete" />
                          </Delete>
                        </Tooltip>
                      </ColDelCustom>
                    </Row>
                  ))}

                  <AddNew
                    style={{ marginBottom: 0 }}
                    onClick={_onAddOptionalField}
                  >
                    + {t("emailIncoming.addFieldMapping")}
                  </AddNew>
                </OptionalFields>
              </WrapMapping>

              <RuleInfo style={{ marginTop: 24 }}>
                {t("emailIncoming.mappingReplyEmail")}
              </RuleInfo>

              <WrapMapping>
                <ReplyFields>
                  {_.map(replyFieldsSelected, (field, index) => (
                    <Row
                      gutter={8}
                      align="middle"
                      style={{
                        marginBottom: 16,
                      }}
                    >
                      <Col flex="50%">
                        <Select
                          placeholder={t("common.placeholderSelect")}
                          style={{ width: "100%" }}
                          onChange={handleChangeField(index, "reply")}
                          value={_.get(field, "key_object", undefined)}
                        >
                          {_.map(fullFields, (optionalField) => (
                            <Select.Option
                              key={_.get(optionalField, "full_field_id", "")}
                              value={_.get(optionalField, "full_field_id", "")}
                              disabled={
                                replyFieldsSelected.find(
                                  (ele) =>
                                    _.get(ele, "key_object", "") ===
                                    _.get(optionalField, "full_field_id", "")
                                )
                                  ? true
                                  : false
                              }
                            >
                              {_.get(optionalField, "name", "")}
                            </Select.Option>
                          ))}
                        </Select>
                      </Col>

                      {_.get(field, "key_object", "") ? (
                        <>
                          {handleFieldType(
                            fullFields.find(
                              (item) => item.full_field_id === field.key_object
                            ),
                            field.value,
                            index,
                            "reply",
                            field?.default_value
                          )}
                        </>
                      ) : (
                        <ColCustom>
                          <Input disabled />
                        </ColCustom>
                      )}

                      <ColDelCustom>
                        <Tooltip title="Delete">
                          <Delete
                            onClick={_onDeleteFieldReply(
                              index,
                              _.get(field, "key_object_reply", "")
                            )}
                          >
                            <img src={DeleteIcon} alt="delete" />
                          </Delete>
                        </Tooltip>
                      </ColDelCustom>
                    </Row>
                  ))}

                  <AddNew
                    style={{ marginBottom: 0 }}
                    onClick={_onAddReplyField}
                  >
                    + {t("emailIncoming.addFieldMapping")}
                  </AddNew>
                </ReplyFields>
              </WrapMapping>
            </Right>
          </Form>
        </Wrap>
      ) : (
        <Empty>
          <img src={EmptyObject} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("emailIncoming.rule")}</span>
          </p>

          {checkRule("create") && (
            <AddButton onClick={_onAddRule}>
              + {t("emailIncoming.addNew")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalConfimDelete
        title={t("emailIncoming.thisRule")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteRuleById}
        dataDelete={dataDelete}
      />
    </Wrapper>
  );
}

export default withTranslation()(RuleIncoming);

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-breadcrumb {
    font-size: 16px !important;
  }

  .ant-switch-checked {
    background-color: ${(props) => props.theme.main};
  }

  .ant-form-item-label > label {
    color: #2c2c2c;
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 22px;
  }

  .ant-form-item {
    margin-bottom: 16px;
  }

  .ant-form {
    width: 100%;
    display: flex;
  }

  .ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main}!important;
  }

  .ant-radio-button-wrapper {
    @media screen and (max-width: 1440px) {
      font-size: 12px;
      padding: 0 8px;
    }
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 24px;
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  color: #2c2c2c;
  cursor: default;
`;

const CancelButton = styled(Button)`
  width: 136px;
  margin-left: 16px;

  span {
    font-size: 16px;
    line-height: 22px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const SaveButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  width: 136px;

  span {
    color: #fff;
    font-size: 16px;
    line-height: 22px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Wrap = styled.div`
  display: flex;
`;

const Left = styled.div`
  margin-right: 16px;
`;

const Center = styled.div`
  width: 29%;
  margin-right: 16px;
`;

const RuleInfo = styled.div`
  font-family: var(--roboto-500);
  line-height: 22px;
  font-size: 16px;
  margin-bottom: 8px;
`;

const WrapRuleInfo = styled.div`
  background: #fff;
  padding: 24px 16px;
  border: 1px solid #ececec;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
  border-radius: 5px;
`;

const Right = styled.div`
  width: 71%;
  /* overflow: auto;
  height: calc(100vh - 170px); */
`;

const WrapMapping = styled.div`
  padding: 24px 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  background: #fff;
`;

const AddNew = styled.div`
  line-height: 22px;
  color: ${(props) => props.theme.main};
  margin-bottom: 8px;
  cursor: pointer;
  width: fit-content;
`;

const Rule = styled.div`
  background: #fff;
  width: 338px;
  height: 85px;
  padding: 16px;
  border: ${(props) =>
    props.active ? `1px solid ${props.theme.main}` : "1px solid #ececec"};
  border-radius: 5px;
  margin-bottom: 8px;
  cursor: pointer;

  :hover {
    border-color: ${(props) => props.theme.main};

    .action {
      visibility: visible;
      opacity: 1;
    }
  }

  @media screen and (max-width: 1599px) {
    width: 300px;
  }

  @media screen and (max-width: 1199px) {
    width: 250px;
  }
`;

const NameRule = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: ${(props) => (props.active ? props.theme.main : "#2c2c2c")};
  margin-bottom: 3px;
`;

const AssignTo = styled.div`
  font-size: 16px;
  line-height: 24px;
  color: #637381;
  display: flex;
  justify-content: space-between;

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;

    :hover {
      text-decoration-line: underline;
      color: ${(props) => props.theme.main};
    }
  }

  @media screen and (max-width: 1199px) {
    font-size: 12px;
  }

  .ant-typography {
    color: #637381;
    font-size: 16px;
    line-height: 24px;
    max-width: 65%;

    @media screen and (max-width: 1599px) {
      max-width: 60%;
    }

    @media screen and (max-width: 1199px) {
      max-width: 50%;
      font-size: 12px;
    }
  }
`;

const DeleteRule = styled.div`
  line-height: 16px;
  color: #2c2c2c;
  margin-left: 8px;
  display: flex;
  align-items: center;
`;

const RequiredFields = styled.fieldset`
  padding: 16px 16px 24px 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-bottom: 24px;

  legend {
    width: fit-content;
    color: #6b6b6b;
    font-size: 14px;
    line-height: 20px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
  }
`;

const OptionalFields = styled(RequiredFields)`
  margin-bottom: 0;
`;

const ReplyFields = styled.div`
  border-radius: 5px;
`;

const Delete = styled.div`
  background: #ffffff;
  border: 1px solid #d9d9d9;
  display: flex;
  align-items: center;
  justify-content: center;
  width: 32px;
  height: 32px;

  :hover {
    cursor: pointer;
    background: #eeeeee;
  }

  img {
    width: 20px;
  }
`;

const ColCustom = styled(Col)`
  flex: 0 0 44%;

  @media screen and (max-width: 1499px) {
    flex: 0 0 41%;
  }
`;

const ColDelCustom = styled(Col)`
  display: flex;
  justify-content: flex-end;
  flex: 0 0 6%;

  @media screen and (max-width: 1499px) {
    flex: 0 0 9%;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;
