import React from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";

import InputNumber from "antd/lib/input-number";
import Input from "antd/lib/input";
import Col from "antd/lib/col";

function Number({ field, value, type, index, required, handleChangeValue }) {
  const { t } = useTranslation();

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom required={required}>
        <InputNumber
          placeholder={t("common.placeholderInput")}
          style={{ width: "100%" }}
          value={value}
          onChange={(e) =>
            handleChangeValue(type, _.get(field, "type", ""), e, index)
          }
        />
      </ColCustom>
    </>
  );
}

export default withTranslation()(Number);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }
`;
