import { useEffect, useState } from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components/macro";
import moment from "moment";

import Input from "antd/lib/input";
import Col from "antd/lib/col";
import DatePicker from "antd/lib/date-picker";
import Select from "antd/lib/select";

const optionsDate = [
  {
    label: "Select Date",
    value: "selectDate",
  },
  {
    label: "Date",
    value: "date",
  },
];

function Date({ field, value, type, index, required, handleChangeValue }) {
  const { t } = useTranslation();

  const [typeDate, $typeDate] = useState("");

  const handleChangeType = (e) => {
    $typeDate(e);

    if (e === "date") {
      handleChangeValue(type, "email", e, index);
    } else {
      handleChangeValue(type, _.get(field, "type", ""), undefined, index);
    }
  };

  useEffect(() => {
    if (value && value !== "date") {
      $typeDate("selectDate");
    } else {
      $typeDate("");
    }
  }, [value]);

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom type={typeDate} required={required}>
        {typeDate === "selectDate" ? (
          <DatePicker
            style={{ width: "100%" }}
            showTime={field.type === "datetime-local"}
            onChange={(date, dateString) =>
              handleChangeValue(
                type,
                _.get(field, "type", ""),
                dateString,
                index
              )
            }
            value={value && moment(value)}
          />
        ) : (
          <Select
            style={{ width: "100%" }}
            placeholder={t("common.placeholderSelect")}
            options={optionsDate}
            onChange={handleChangeType}
            value={value}
          />
        )}
      </ColCustom>
      {typeDate === "selectDate" && (
        <Col flex="10%">
          <Clear
            onClick={() => {
              $typeDate("");
              handleChangeValue(
                type,
                _.get(field, "type", ""),
                undefined,
                index
              );
            }}
          >
            Clear
          </Clear>
        </Col>
      )}
    </>
  );
}

export default withTranslation()(Date);

const Clear = styled.span`
  cursor: pointer;
`;

const ColCustom = styled(Col)`
  flex: ${({ type, required }) =>
    type === "selectDate"
      ? required
        ? "0 0 40%"
        : "0 0 34%"
      : required
      ? "0 0 50%"
      : "0 0 44%"};

  @media screen and (max-width: 1499px) {
    flex: ${({ type, required }) =>
      type === "selectDate"
        ? required
          ? "0 0 40%"
          : "0 0 31%"
        : required
        ? "0 0 50%"
        : "0 0 41%"};
  }
`;
