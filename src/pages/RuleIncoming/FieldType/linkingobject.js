import React, { useEffect, useState } from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";
import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Col from "antd/lib/col";
import Row from "antd/lib/row";

import { BASE_URL_API } from "constants/constants";

const options = [
  {
    label: "From",
    value: "from",
  },
  {
    label: "To",
    value: "to",
  },
  {
    label: "Cc",
    value: "cc",
  },
  {
    label: "Bcc",
    value: "bcc",
  },
];

function SelectType({
  field,
  value,
  defaultValue,
  type,
  index,
  required,
  handleChangeValue,
}) {
  const { t } = useTranslation();

  const [optionsLinking, $optionsLinking] = useState([]);

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .post(
          BASE_URL_API + "list-view/load-data",
          {
            object_id: field.objectname,
            current_page: 1,
            record_per_page: 10,
            search_with: {
              meta: [],
              data: "",
            },
          },
          {
            headers: {
              Authorization: isTokenValid,
            },
          }
        )
        .then((res) => {
          let tempResult = [];
          res.data.data.forEach((item) => {
            if (item[field.field]) {
              let temp = {
                label: item[field.field]?.value,
                value: item._id,
              };
              tempResult.push(temp);
            }
          });

          $optionsLinking(tempResult);
        })
        .catch((err) => console.log(err));
    };
    checkToken();
  }, [field]);

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom required={required}>
        <Row gutter={4}>
          <Col flex={required ? "50%" : "100%"}>
            <Select
              placeholder={t("common.placeholderSelect")}
              style={{ width: "100%" }}
              options={options}
              value={value}
              onChange={(e) =>
                handleChangeValue(
                  type,
                  _.get(field, "type", ""),
                  e,
                  index,
                  undefined
                )
              }
            />
          </Col>

          {required && (
            <Col flex="50%">
              <Select
                placeholder={t("common.placeholderSelect")}
                style={{ width: "100%" }}
                options={optionsLinking}
                value={defaultValue}
                onChange={(e) =>
                  handleChangeValue(
                    type,
                    _.get(field, "type", ""),
                    value,
                    index,
                    e
                  )
                }
              />
            </Col>
          )}
        </Row>
      </ColCustom>
    </>
  );
}

export default withTranslation()(SelectType);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }

  .ant-select-selection-item {
    display: -webkit-box;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    white-space: unset;
    word-break: break-all;
  }

  .ant-select-selection-placeholder {
    display: -webkit-box;
    text-overflow: ellipsis;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    white-space: unset;
    word-break: break-all;
  }
`;
