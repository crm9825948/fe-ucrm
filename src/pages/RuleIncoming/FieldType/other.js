import React from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";

import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Col from "antd/lib/col";

const options = [
  {
    label: "Subject",
    value: "subject",
  },
  {
    label: "Body",
    value: "body",
  },
  {
    label: "From",
    value: "from",
  },
  {
    label: "From name",
    value: "from_name",
  },
  {
    label: "Date",
    value: "date",
  },
  {
    label: "Attach file",
    value: "attach_file",
  },
  {
    label: "To",
    value: "to",
  },
  {
    label: "Cc",
    value: "cc",
  },
  {
    label: "Bcc",
    value: "bcc",
  },
];

function Number({ field, value, type, index, required, handleChangeValue }) {
  const { t } = useTranslation();

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom required={required}>
        <Select
          style={{ width: "100%" }}
          mode="tags"
          placeholder={t("common.placeholderInputSelect")}
          options={options}
          value={value}
          onChange={(e) => {
            if (e?.length > 1) {
              e.shift();
            }
            handleChangeValue(
              type,
              options.find((item) => item.value === e[0])
                ? "email"
                : _.get(field, "type", ""),
              e[0],
              index
            );
          }}
        />
      </ColCustom>
    </>
  );
}

export default withTranslation()(Number);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }
`;
