import React from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import { useSelector } from "react-redux";
import styled from "styled-components";

import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Col from "antd/lib/col";

function SelectType({
  field,
  value,
  type,
  index,
  required,
  handleChangeValue,
}) {
  const { t } = useTranslation();
  const { listUserName } = useSelector((state) => state.userReducer);

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom required={required}>
        <Select
          placeholder={t("common.placeholderSelect")}
          style={{ width: "100%" }}
          value={value}
          onChange={(e) =>
            handleChangeValue(type, _.get(field, "type", ""), e, index)
          }
        >
          {Object.keys(listUserName).map((key) => {
            return (
              <Select.Option value={key}>{listUserName[key]}</Select.Option>
            );
          })}
        </Select>
      </ColCustom>
    </>
  );
}

export default withTranslation()(SelectType);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }
`;
