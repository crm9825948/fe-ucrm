import React from "react";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import styled from "styled-components";

import Select from "antd/lib/select";
import Input from "antd/lib/input";
import Col from "antd/lib/col";

const options = [
  {
    label: "Attach file",
    value: "attach_file",
  },
];

function File({ field, required }) {
  const { t } = useTranslation();

  return (
    <>
      {required && (
        <Col flex="50%">
          <Input
            style={{ width: "100%" }}
            defaultValue={_.get(field, "name", "")}
            disabled
          />
        </Col>
      )}

      <ColCustom required={required}>
        <Select
          value="attach_file"
          placeholder={t("common.placeholderSelect")}
          style={{ width: "100%" }}
          options={options}
          disabled
        />
      </ColCustom>
    </>
  );
}

export default withTranslation()(File);

const ColCustom = styled(Col)`
  flex: ${({ required }) => (required ? "0 0 50%" : "0 0 44%")};

  @media screen and (max-width: 1499px) {
    flex: ${({ required }) => (required ? "0 0 50%" : "0 0 41%")};
  }
`;
