import styled from "styled-components/macro";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";
import _ from "lodash";

import Breadcrumb from "antd/lib/breadcrumb";
import Collapse from "antd/lib/collapse";
import Button from "antd/lib/button";
import Table from "antd/lib/table";
import Tooltip from "antd/lib/tooltip";
import Typography from "antd/lib/typography";
import { CaretRightOutlined } from "@ant-design/icons";

import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Edit from "assets/icons/common/edit.svg";
import Delete from "assets/icons/common/delete.svg";

import { setShowModalConfirmDelete } from "redux/slices/global";

import {
  loadDataNecessary,
  loadRuleObject,
  deleteRule,
  resetStatus,
} from "redux/slices/criteriaSharing";

import ModalConfirmDelete from "components/Modal/ModalConfirmDelete";
import ModalRule from "./ModalRule";
import { Notification } from "components/Notification/Noti";
import { changeTitlePage } from "redux/slices/authenticated";

function CriteriaSharing(props) {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { Column } = Table;
  const { Text } = Typography;

  const { userRuleGlobal } = useSelector((state) => state.userReducer);
  const { loading, status, allRules } = useSelector(
    (state) => state.criteriaSharingReducer
  );

  const { category } = useSelector((state) => state.objectsManagementReducer);

  const { listObjectField } = useSelector((state) => state.objectsReducer);

  const { listAllGroups } = useSelector((state) => state.groupReducer);

  const [showModalRule, setShowModalRule] = useState(false);
  const [activeKey, setActiveKey] = useState([]);
  const [objectID, setObjectID] = useState("");
  const [isEdit, setIsEdit] = useState(false);
  const [IDRule, setIDRule] = useState({});

  const [listRules, setListRules] = useState([]);
  const [dataDelete, setDataDelete] = useState({});

  useEffect(() => {
    dispatch(changeTitlePage(t("settings.criteriaSharing")));
    //eslint-disable-next-line
  }, [t]);

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "criteria_sharing" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const handleChangePanel = (key) => {
    setActiveKey(key);

    if (key) {
      dispatch(
        loadRuleObject({
          object_id: key,
        })
      );
    }
  };

  const _onAddRule = (e, id) => {
    e.stopPropagation();
    setObjectID(id);
    setShowModalRule(true);
  };

  const _onDeleteRule = (data, id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({ id: data });
    setObjectID(id);
  };

  const _onEditRule = (data, id) => {
    setIDRule(data.key);
    setObjectID(id);
    setIsEdit(true);
    setShowModalRule(true);
  };

  const showCondition = (conditions, fields) => {
    let listCondition = [];

    conditions.forEach((item, idx) => {
      Object.entries(item).forEach(([key, val]) => {
        if (key === "id_field") {
          let found = fields.find((ele) => ele.value === val);
          if (found) {
            listCondition.push({
              name: found.label,
              decimal_separator: _.get(found, "decimal_separator", 3),
            });
          } else {
            listCondition.push({
              name: val,
            });
          }
        }
        if (key === "type") {
          let tempType = {
            type: val,
          };
          listCondition[idx] = {
            ...listCondition[idx],
            ...tempType,
          };
        }
        if (key === "value") {
          let tempObj = {};

          if (Object.keys(val)[0] === "$not") {
            tempObj = {
              equal: "$not",
              condition: val.$not.$regex,
            };
          } else if (Object.keys(val).length > 2) {
            tempObj = {
              equal: Object.keys(val),
              condition: Object.values(val),
            };
          } else {
            tempObj = {
              equal: Object.keys(val)[0],
              condition: Object.values(val)[0],
            };
          }

          listCondition[idx] = {
            ...listCondition[idx],
            ...tempObj,
          };
        }
      });
    });

    listCondition.forEach((item, idx) => {
      if (item.status === "owner") {
        listCondition[idx] = {
          ...listCondition[idx],
          status: "Assign to",
        };
      }

      if (item.condition === null) {
        if (item.equal === "$eq") {
          listCondition[idx] = {
            ...listCondition[idx],
            equal: "Is Empty",
          };
        } else {
          listCondition[idx] = {
            ...listCondition[idx],
            equal: "Is not empty",
          };
        }
      } else if (Array.isArray(item.equal)) {
        listCondition[idx] = {
          ...listCondition[idx],
          equal: "Between",
          condition: item.condition[0] + " - " + item.condition[1],
        };
      } else if (item.condition === "mine") {
        listCondition[idx] = {
          ...listCondition[idx],
          equal: item.equal === "$eq" ? "Mine" : "Not mine",
          condition: null,
        };
      } else {
        switch (item.equal) {
          case "$eq":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Equals",
            };
            break;
          case "$ne":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Not Equals",
            };
            break;
          case "$lt":
            if (item.type === "date" || item.type === "datetime-local") {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Before",
              };
            } else {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Less than",
              };
            }
            break;
          case "$lte":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Less than or equal",
            };
            break;
          case "$gt":
            if (item.type === "date" || item.type === "datetime-local") {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "After",
              };
            } else {
              listCondition[idx] = {
                ...listCondition[idx],
                equal: "Greater than",
              };
            }
            break;
          case "$gte":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Greater than or equal",
            };
            break;
          case "$regex":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Contains",
            };
            break;
          case "$not":
            listCondition[idx] = {
              ...listCondition[idx],
              equal: "Not Contains",
            };
            break;

          default:
            break;
        }
      }
    });

    return listCondition;
  };

  function numberWithCommas(number, decimal_separator) {
    if (number === 0) {
      return "0";
    } else {
      return number
        .toFixed(decimal_separator)
        .toString()
        .replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  useEffect(() => {
    dispatch(loadDataNecessary());
  }, [dispatch]);

  useEffect(() => {
    let tempList = [];
    let tempOptionsFields = [];

    listObjectField.forEach((item) => {
      if (
        Object.values(item)[0] !== null &&
        (Object.values(item)[0].readable || Object.values(item)[0].writeable)
      ) {
        let tempFields = [];
        Object.values(item)[0].sections.forEach((ele) => {
          ele.fields.forEach((field) => {
            if (field.hidden === false && field.permission_hidden === false) {
              tempFields.push({
                label: field.related_name,
                value: field.full_field_id,
                type: field.type,
                decimal_separator: _.get(field, "decimal_separator", 3),
              });
            }
          });
        });
        if (tempFields.length > 0) {
          tempOptionsFields = [...tempOptionsFields, ...tempFields];
        }
      }
    });

    allRules.map((item, idx) => {
      let tempGroupShare = [];
      let listConditions = [];

      item.shared_to.map((group) => {
        return tempGroupShare.push(group.name);
      });

      if (item.filter_condition.and_filter.length > 0) {
        listConditions.push({
          title: t("common.allCondition"),
          subTitle: t("common.descAllCondition"),
          list: showCondition(
            item.filter_condition.and_filter,
            tempOptionsFields
          ),
        });
      }

      if (item.filter_condition.or_filter.length > 0) {
        listConditions.push({
          title: t("common.anyCondition"),
          subTitle: t("common.descAnyCondition"),
          list: showCondition(
            item.filter_condition.or_filter,
            tempOptionsFields
          ),
        });
      }

      return tempList.push({
        key: item._id,
        rule_number: idx + 1,
        conditions: listConditions,
        share_to: tempGroupShare,
        privileges:
          item.access_level === "read"
            ? t("common.read")
            : `${t("common.read")} & ${t("common.write")}`,
      });
    });
    setListRules(tempList);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allRules, listObjectField]);

  useEffect(() => {
    if (!loading.modalDelete) {
      if (status.deleteRule === "success") {
        dispatch(setShowModalConfirmDelete(false));

        Notification("success", "Delete successfully!");
        dispatch(resetStatus());
        dispatch(
          loadRuleObject({
            object_id: objectID,
          })
        );
      }

      if (status.deleteRule !== null && status.deleteRule !== "success") {
        Notification("error", status.deleteRule);
        dispatch(resetStatus());
      }
    }
  }, [dispatch, loading.modalDelete, objectID, status.deleteRule]);

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("settings.settings")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{t("settings.criteriaSharing")}</BreadcrumbItem>
      </Breadcrumb>

      <WrapCollapse>
        {Object.keys(category).length > 0 ? (
          <>
            <Title>
              <span>{t("object.objectName")}</span>
            </Title>
            <Collapse
              accordion
              expandIcon={({ isActive }) => (
                <CaretRightOutlined rotate={isActive ? -90 : 90} />
              )}
              activeKey={activeKey}
              onChange={handleChangePanel}
            >
              {Object.entries(category).map(([key, val]) => {
                return val.map((item) => {
                  return (
                    item.Status &&
                    item.active && (
                      <PanelCustom
                        header={item.Name}
                        key={item._id}
                        $active={activeKey === item._id}
                        extra={
                          activeKey === item._id &&
                          checkRule("create") && (
                            <AddRule
                              $active={activeKey === item._id}
                              onClick={(e) => _onAddRule(e, item._id)}
                            >
                              + {t("sharing.addRule")}
                            </AddRule>
                          )
                        }
                      >
                        <Table
                          pagination={false}
                          dataSource={listRules}
                          scroll={{ y: 350 }}
                        >
                          <Column
                            title={t("sharing.ruleNumber")}
                            dataIndex="rule_number"
                            key="rule_number"
                            width="10%"
                          />
                          <Column
                            title={`${item.Name} ${t("criteriaSharing.of")}`}
                            dataIndex="conditions"
                            key="conditions"
                            width="30%"
                            render={(list) => (
                              <>
                                {list.map((item, indexCondition) => {
                                  return (
                                    <WrapCondition key={indexCondition}>
                                      <TitleCondition>
                                        {item.title}
                                      </TitleCondition>
                                      <SubTitleCondition>
                                        {item.subTitle}
                                      </SubTitleCondition>
                                      {item.list.map(
                                        (condition, idxCondition) => {
                                          return (
                                            <Condition key={idxCondition}>
                                              <Name>
                                                <Text
                                                  ellipsis={{
                                                    tooltip: condition.name,
                                                  }}
                                                >
                                                  {condition.name}
                                                </Text>
                                              </Name>
                                              <Equal>
                                                <Text
                                                  ellipsis={{
                                                    tooltip: condition.equal,
                                                  }}
                                                >
                                                  {condition.equal}
                                                </Text>
                                              </Equal>
                                              {condition.condition != null && (
                                                <>
                                                  {typeof condition.condition ===
                                                  "string" ? (
                                                    <Value>
                                                      <Text
                                                        ellipsis={{
                                                          tooltip:
                                                            condition.condition,
                                                        }}
                                                      >
                                                        {condition.condition}
                                                      </Text>
                                                    </Value>
                                                  ) : typeof condition.condition ===
                                                    "number" ? (
                                                    <Value>
                                                      <Text
                                                        ellipsis={{
                                                          tooltip:
                                                            numberWithCommas(
                                                              condition.condition,
                                                              condition.decimal_separator
                                                            ),
                                                        }}
                                                      >
                                                        {numberWithCommas(
                                                          condition.condition,
                                                          condition.decimal_separator
                                                        )}
                                                      </Text>
                                                    </Value>
                                                  ) : (
                                                    <Value>
                                                      <Text
                                                        ellipsis={{
                                                          tooltip:
                                                            Object.values(
                                                              condition.condition
                                                            ),
                                                        }}
                                                      >
                                                        {Object.values(
                                                          condition.condition
                                                        )}
                                                      </Text>
                                                    </Value>
                                                  )}
                                                </>
                                              )}
                                            </Condition>
                                          );
                                        }
                                      )}
                                    </WrapCondition>
                                  );
                                })}
                              </>
                            )}
                          />
                          <Column
                            title={t("sharing.shareTo")}
                            dataIndex="share_to"
                            key="share_to"
                            width="40%"
                            render={(text) => (
                              <Wrap>
                                {text.map((share, indexShare) => {
                                  return (
                                    <Shared key={indexShare}>
                                      <span>{share}</span>
                                    </Shared>
                                  );
                                })}
                              </Wrap>
                            )}
                          />
                          <Column
                            title={t("role.privileges")}
                            dataIndex="privileges"
                            key="privileges"
                            width="20%"
                            render={(text, record) => (
                              <WrapPrivileges>
                                <span>{text}</span>

                                {(checkRule("edit") || checkRule("delete")) && (
                                  <WrapAction>
                                    {checkRule("edit") && (
                                      <Tooltip title={t("common.edit")}>
                                        <img
                                          onClick={() =>
                                            _onEditRule(record, item._id)
                                          }
                                          src={Edit}
                                          alt="edit"
                                        />
                                      </Tooltip>
                                    )}

                                    {checkRule("delete") && (
                                      <Tooltip title={t("common.delete")}>
                                        <img
                                          onClick={() =>
                                            _onDeleteRule(record.key, item._id)
                                          }
                                          src={Delete}
                                          alt="delete"
                                        />
                                      </Tooltip>
                                    )}
                                  </WrapAction>
                                )}
                              </WrapPrivileges>
                            )}
                          />
                        </Table>
                      </PanelCustom>
                    )
                  );
                });
              })}
            </Collapse>
          </>
        ) : (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>
              {t("object.noObject")} {t("object.object")}
            </span>
          </Empty>
        )}
      </WrapCollapse>

      <ModalRule
        showModalRule={showModalRule}
        onHideModalRule={() => setShowModalRule(false)}
        listAllGroups={listAllGroups}
        objectID={objectID}
        isEdit={isEdit}
        setIsEdit={setIsEdit}
        IDRule={IDRule}
        setActiveKey={setActiveKey}
      />
      <ModalConfirmDelete
        title={""}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteRule}
        dataDelete={dataDelete}
        isLoading={loading.modalDelete}
      />
    </Wrapper>
  );
}

export default withTranslation()(CriteriaSharing);

const { Panel } = Collapse;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }

  .ant-space {
    width: 100%;
  }

  .ant-collapse {
    border: none;
    background: #f2f8ff;
  }

  .ant-collapse-item {
    border-bottom: none;
    margin-bottom: 10px;
  }

  .ant-collapse-content {
    border-top: none;
  }

  .ant-collapse-extra {
    display: none;
  }

  .ant-spin {
    display: flex;
    justify-content: center;
  }

  .ant-collapse-content > .ant-collapse-content-box {
    padding: 0;
  }

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;
    border-bottom: 1px solid #ededed;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-body {
    ::-webkit-scrollbar {
      width: 8px;
    }
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapCollapse = styled.div`
  margin-top: 16px;
`;

const Title = styled.div`
  background: #fff;
  border-radius: 10px 10px 0px 0px;
  height: 46px;
  padding: 10px 0 10px 43px;
  margin-bottom: 10px;

  span {
    font-family: var(--roboto-500);
    font-size: 18px;
    color: #2c2c2c;
  }
`;

const PanelCustom = styled(Panel)`
  .ant-collapse-header {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    font-size: 16px;
    font-family: var(--roboto-500);
    color: ${(props) => (props.$active ? "#fff" : "#2c2c2c")}!important;
    border: 1px solid #ececec;
    padding: 9px 16px !important;

    .ant-collapse-extra {
      display: ${(props) => (props.$active ? "block" : "none")};
    }

    :hover {
      .ant-collapse-extra {
        display: block;
      }
      box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.16);
    }
  }
`;

const AddRule = styled(Button)`
  border: ${(props) =>
    props.$active ? "1px solid #fff" : "1px solid #20a2a2"};
  padding: 0 15px;
  background: ${(props) => (props.$active ? props.theme.main : "#fff")};

  span {
    color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    font-size: 16px;
  }

  :hover {
    background: ${(props) =>
      props.$active ? "#fff" : props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker} !important;

    span {
      color: ${(props) =>
        props.$active ? props.theme.darker : "#fff"}!important;
    }
  }

  &.ant-btn:active {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    border-color: ${(props) => props.theme.main} span {
      color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    }
  }

  &.ant-btn:focus {
    background: ${(props) => (props.$active ? props.theme.main : "#fff")};
    border-color: ${(props) => (props.$active ? "#fff" : props.theme.main)};

    span {
      color: ${(props) => (props.$active ? "#fff" : props.theme.main)};
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;

  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapPrivileges = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const Wrap = styled.div`
  display: flex;
  align-items: center;
`;

const Shared = styled.div`
  background: #f2f4f5;
  border-radius: 5px;
  padding: 2px 8px;
  width: fit-content;
  margin-right: 8px;
`;

const WrapCondition = styled.div`
  display: flex;
  flex-direction: column;
`;

const TitleCondition = styled.div`
  color: #2c2c2c;
  font-family: var(--roboto-500);
  font-size: 16px;
`;

const SubTitleCondition = styled.div`
  color: #ffa940;
  font-size: 14px;
  margin-bottom: 8px;
`;

const Condition = styled.div`
  display: flex;
  margin-bottom: 8px;
`;

const Name = styled.div`
  width: 32%;
  text-align: center;
  border: 1px solid #000;
  padding: 10px;
  border-radius: 5px;
  margin-right: 10px;

  :last-child {
    margin-right: 0;
  }
`;
const Equal = styled(Name)``;

const Value = styled(Name)``;
