import { useEffect, useState } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";
import Space from "antd/lib/space";

import { Notification } from "components/Notification/Noti";
import Conditions from "components/Conditions/conditions";

import {
  createRule,
  updateRule,
  loadRuleObject,
  resetStatus,
} from "redux/slices/criteriaSharing";

function ModalRule({
  showModalRule,
  onHideModalRule,
  listAllGroups,
  objectID,
  isEdit,
  setIsEdit,
  IDRule,
  setActiveKey,
}) {
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { loading, status, allRules } = useSelector(
    (state) => state.criteriaSharingReducer
  );

  const [listGroups, setListGroups] = useState([]);

  const [allCondition, setAllCondition] = useState([]);
  const [anyCondition, setAnyCondition] = useState([]);

  const [detailsRule, setDetailsRule] = useState({});

  const [operatorValueAnd, setOperatorValueAnd] = useState([]);
  const [operatorValueOr, setOperatorValueOr] = useState([]);
  const [valueAnd, setValueAnd] = useState([]);
  const [valueOr, setValueOr] = useState([]);

  const _onSubmit = (values) => {
    //operator
    let flag = false;
    if (operatorValueAnd.length === allCondition.length) {
      operatorValueAnd.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    if (operatorValueOr.length === anyCondition.length) {
      operatorValueOr.forEach((item) => {
        if (item === undefined) {
          flag = true;
        }
      });
    } else {
      flag = true;
    }
    //value
    valueAnd.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueAnd[idx] !== "empty" &&
        operatorValueAnd[idx] !== "not-empty" &&
        operatorValueAnd[idx] !== "mine" &&
        operatorValueAnd[idx] !== "not-mine" &&
        operatorValueAnd[idx] !== "today" &&
        operatorValueAnd[idx] !== "not-today" &&
        operatorValueAnd[idx] !== "yesterday" &&
        operatorValueAnd[idx] !== "this-week" &&
        operatorValueAnd[idx] !== "last-week" &&
        operatorValueAnd[idx] !== "this-month" &&
        operatorValueAnd[idx] !== "last-month" &&
        operatorValueAnd[idx] !== "this-year" &&
        operatorValueAnd[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueAnd[idx] !== "$inWorkingDayUCRM" &&
        operatorValueAnd[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    valueOr.forEach((item, idx) => {
      if (
        item === "" &&
        operatorValueOr[idx] !== "empty" &&
        operatorValueOr[idx] !== "not-empty" &&
        operatorValueOr[idx] !== "mine" &&
        operatorValueOr[idx] !== "not-mine" &&
        operatorValueOr[idx] !== "today" &&
        operatorValueOr[idx] !== "not-today" &&
        operatorValueOr[idx] !== "yesterday" &&
        operatorValueOr[idx] !== "this-week" &&
        operatorValueOr[idx] !== "last-week" &&
        operatorValueOr[idx] !== "this-month" &&
        operatorValueOr[idx] !== "last-month" &&
        operatorValueOr[idx] !== "this-year" &&
        operatorValueOr[idx] !== "$inWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingTimeUCRM" &&
        operatorValueOr[idx] !== "$inWorkingDayUCRM" &&
        operatorValueOr[idx] !== "$ninWorkingDayUCRM"
      ) {
        flag = true;
      }
      if (item === undefined) {
        flag = true;
      }
    });
    if (flag) {
      Notification("warning", "Please fullfill conditions!");
    } else {
      let data = {
        access_level: values.status,
        group_id: values.object_of,
        main_object: objectID,
        shared_to: values.share_to,
        filter_condition: {
          and_filter: allCondition,
          or_filter: anyCondition,
        },
      };
      if (isEdit) {
        dispatch(
          updateRule({
            ...data,
            id: IDRule,
          })
        );
      } else {
        dispatch(
          createRule({
            ...data,
          })
        );
      }
    }
  };

  const _onCancel = () => {
    onHideModalRule();
    form.resetFields();
    setIsEdit(false);
    setAllCondition([]);
    setAnyCondition([]);
  };

  useEffect(() => {
    let tempGroup = [];
    if (listAllGroups.length > 0) {
      listAllGroups.map((item) => {
        return tempGroup.push({
          label: item.name,
          value: item._id,
        });
      });
    }
    setListGroups(tempGroup);
  }, [listAllGroups]);

  useEffect(() => {
    if (isEdit) {
      let detailsRule = allRules.filter((rule) => rule._id === IDRule);
      setDetailsRule(detailsRule[0]);
      let tempShared = [];
      detailsRule[0].shared_to.map((item) => {
        return tempShared.push(item._id);
      });
      form.setFieldsValue({
        share_to: tempShared,
        status: detailsRule[0].access_level,
      });

      setAllCondition(detailsRule[0].filter_condition.and_filter);
      setAnyCondition(detailsRule[0].filter_condition.or_filter);
    }
  }, [isEdit, IDRule, allRules, form]);

  useEffect(() => {
    if (!loading.modalRule) {
      if (status.createRule === "success" || status.updateRule === "success") {
        onHideModalRule();
        form.resetFields();
        setIsEdit(false);
        setAllCondition([]);
        setAnyCondition([]);

        Notification(
          "success",
          isEdit ? "Update successfully!" : "Create successfully!"
        );
        dispatch(resetStatus());
        setActiveKey(objectID);
        dispatch(
          loadRuleObject({
            object_id: objectID,
          })
        );
      }

      if (status.createRule !== null && status.createRule !== "success") {
        Notification("error", status.createRule);
        dispatch(resetStatus());
      }

      if (status.updateRule !== null && status.updateRule !== "success") {
        Notification("error", status.updateRule);
        dispatch(resetStatus());
      }
    }
  }, [
    dispatch,
    form,
    isEdit,
    loading,
    objectID,
    onHideModalRule,
    setIsEdit,
    status,
    setActiveKey,
  ]);

  return (
    <ModalCustom
      title={isEdit ? t("sharing.editRule") : t("sharing.addRule")}
      visible={showModalRule}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Conditions
          title={t("common.allCondition")}
          decs={`(${t("common.descAllCondition")})`}
          conditions={allCondition}
          setConditions={setAllCondition}
          ID={isEdit ? detailsRule._id : ""}
          dataDetails={detailsRule || {}}
          operatorValue={operatorValueAnd}
          setOperatorValue={setOperatorValueAnd}
          value={valueAnd}
          setValue={setValueAnd}
        />
        <Conditions
          title={t("common.anyCondition")}
          decs={`(${t("common.descAnyCondition")})`}
          conditions={anyCondition}
          setConditions={setAnyCondition}
          ID={isEdit ? detailsRule._id : ""}
          dataDetails={detailsRule || {}}
          operatorValue={operatorValueOr}
          setOperatorValue={setOperatorValueOr}
          value={valueOr}
          setValue={setValueOr}
        />

        <Form.Item
          label={t("sharing.shareTo")}
          name="share_to"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Select
            options={listGroups}
            placeholder={t("common.placeholderSelect")}
            mode="multiple"
            optionFilterProp="label"
          />
        </Form.Item>

        <Form.Item
          label={t("common.status")}
          name="status"
          rules={[{ required: true, message: t("common.placeholderSelect") }]}
        >
          <Radio.Group>
            <Space direction="vertical">
              <Radio value={"read"}>{t("common.read")}</Radio>
              <Radio value={"write"}>
                {t("common.read")} & {t("common.write")}
              </Radio>
            </Space>
          </Radio.Group>
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit" loading={loading.modalRule}>
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalRule);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-radio-checked .ant-radio-inner {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
