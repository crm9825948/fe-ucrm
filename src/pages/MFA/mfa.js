import React from "react";
import styled from "styled-components";
import { Input } from "antd";

const MFA = () => {
  return (
    <Wrapper>
      {/* <div>Please input your token:</div> */}
      <Input size="large" style={{ width: "80%" }} />
    </Wrapper>
  );
};

export default MFA;

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;
