import { Button, Col, Row, Select, Tree, Breadcrumb } from "antd";
import Search from "antd/lib/input/Search";
import { useLocation, useNavigate } from "react-router";
import React, { useEffect, useRef, useState } from "react";
import {
  deleteArticleById,
  getListArticle,
  getTreeViewKnowledgeBase,
  unmountKnowledgeBaseView,
  updateSearchArticle,
  loadMoreArticle,
  updateFilterArticle,
  updatePagination,
} from "redux/slices/knowledgeBaseView";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
import editIcon from "../../assets/icons/consolidatedViewSettings/editKb.svg";
import deleteIcon from "../../assets/icons/consolidatedViewSettings/deleteKb.svg";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import {
  getKnowledgeBaseSetting,
  updateIsFirst,
} from "redux/slices/knowledgeBaseSetting";
import { unmountObjects } from "redux/slices/objects";
import { useTranslation } from "react-i18next";
import { changeTitlePage } from "redux/slices/authenticated";
import parse from "html-react-parser";
import { optionsParse } from "util/staticData";
const { Option } = Select;
const KnowledgeBaseView = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { search } = useLocation();
  const navigate = useNavigate();
  const ref = useRef();
  const query = new URLSearchParams(search);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const object_id = query.get("object_id");
  const {
    listArticle,
    treeView,
    searchArticle,
    pagination,
    filterArticle,
    totalPage,
  } = useSelector((state) => state.knowledgeBaseViewReducer);
  const { editKnowledgeBaseSetting } = useSelector(
    (state) => state.knowledgeBaseSettingReducer
  );
  const { page, limit } = pagination;
  const [dataDelete, setDataDelete] = useState({});

  useEffect(() => {
    dispatch(changeTitlePage(t("knowledgeBase.knowledgeBase")));
    //eslint-disable-next-line
  }, [t]);

  const deleteArticle = (id) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      ids: [id],
      object_id: object_id,
    });
  };
  useEffect(() => {
    dispatch(getTreeViewKnowledgeBase());
    dispatch(getKnowledgeBaseSetting());
    dispatch(updateSearchArticle(""));
    dispatch(
      getListArticle({
        data: {
          object_id: object_id,
          page: 0,
          limit: 16,
          search_with_title: "",
        },
        pagination: {
          object_id: object_id,
          current_page: 1,
          record_per_page: 16,
          search_with: {
            meta: [],
            data: [],
          },
        },
      })
    );
    // eslint-disable-next-line
  }, []);
  useEffect(
    () => () => {
      dispatch(unmountKnowledgeBaseView());
      dispatch(unmountObjects());
    },
    // eslint-disable-next-line
    []
  );
  return (
    <>
      <Breadcrumb style={{ paddingLeft: "2rem" }}>
        <BreadcrumbItem>
          {" "}
          {t("knowledgeBase.knowledgeBaseView")}{" "}
        </BreadcrumbItem>
      </Breadcrumb>
      <CustomContent justify={"space-around"}>
        <TreeView span={4}>
          <Tree
            ref={ref}
            className="draggable-tree"
            treeData={treeView}
            defaultExpandAll={false}
            onSelect={(e, event) => {
              if (event.selected) {
                dispatch(updateSearchArticle(""));
                if (event.node.children.length > 0) {
                  dispatch(
                    getListArticle({
                      data: {
                        object_id: object_id,
                        page: 0,
                        limit: 16,
                        search_with_title: "",
                        section_name: event.node.title,
                      },
                      pagination: {
                        object_id: object_id,
                        current_page: 0,
                        record_per_page: 16,
                        search_with: {
                          meta: [],
                          data: [
                            {
                              id_field: editKnowledgeBaseSetting.section_name,
                              value: event.node.title,
                            },
                          ],
                        },
                        sort_by: {},
                      },
                    })
                  );
                  dispatch(
                    updateFilterArticle({
                      key: "section_name",
                      value: "",
                    })
                  );
                  dispatch(
                    updateFilterArticle({
                      key: "category_name",
                      value: "",
                    })
                  );
                } else {
                  dispatch(
                    getListArticle({
                      data: {
                        object_id: object_id,
                        page: 0,
                        limit: 16,
                        search_with_title: "",
                        section_name: event.node.parent_title,
                        category_name: event.node.title,
                      },
                      pagination: {
                        object_id: object_id,
                        current_page: page,
                        record_per_page: 16,
                        search_with: {
                          meta: [],
                          data: [
                            {
                              id_field: editKnowledgeBaseSetting.category_name,
                              value: event.node.title,
                            },
                            {
                              id_field: editKnowledgeBaseSetting.section_name,
                              value: event.node.parent_title,
                            },
                          ],
                        },
                        sort_by: {},
                      },
                    })
                  );
                }
              } else {
                dispatch(
                  getListArticle({
                    data: {
                      object_id: object_id,
                      page: 0,
                      limit: 16,
                      search_with_title: "",
                    },
                    pagination: {
                      object_id: object_id,
                      current_page: 1,
                      record_per_page: 16,
                      search_with: {
                        meta: [],
                        data: [],
                      },
                    },
                  })
                );
                dispatch(
                  updatePagination({
                    page: 0,
                    limit: 16,
                  })
                );
                dispatch(
                  updateFilterArticle({
                    key: "section_name",
                    value: "",
                  })
                );
                dispatch(
                  updateFilterArticle({
                    key: "category_name",
                    value: "",
                  })
                );
              }
            }}
          />
        </TreeView>
        <Articles span={19}>
          <Row justify={"space-between"}>
            <Col span={16}>
              <Row justify={"center"}>
                <Col span={18}>
                  <Search
                    placeholder={t("knowledgeBase.searchPlaceholder")}
                    allowClear
                    enterButton={t("common.search")}
                    size="large"
                    value={searchArticle}
                    onChange={(e) => {
                      dispatch(updateSearchArticle(e.target.value));
                    }}
                    onSearch={(e) => {
                      // ref.current.state.selectedKeys = []
                      // let dataSearch = []
                      dispatch(updateSearchArticle(e));
                      if (e.length > 0) {
                        navigate("/article-detail?object_id=" + object_id);
                      }
                      // if(e.length > 0){
                      //     dataSearch.push({
                      //         "id_field": editKnowledgeBaseSetting.title,
                      //         "value": e
                      //     })
                      // }
                      // dispatch(getListArticle({
                      //     data:
                      //     {
                      //         "object_id": object_id,
                      //         "page": 0,
                      //         "limit": 16,
                      //         "search_with_title": e
                      //     },
                      //     pagination: {
                      //         "object_id": object_id,
                      //         "current_page": page,
                      //         "record_per_page": 16,
                      //         "search_with": {
                      //             "meta": [],
                      //             "data": dataSearch
                      //         }
                      //     }
                      // }))
                    }}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={8}>
              <Row justify={"end"}>
                <Button
                  span={12}
                  style={{ marginRight: "0.5rem" }}
                  onClick={() => {
                    navigate("/articles");
                  }}
                >
                  {t("knowledgeBase.addArticle")}
                </Button>
                <Select
                  span={12}
                  defaultValue={"knowledgebase"}
                  onChange={(e) => {
                    if (e === "list_view") {
                      navigate("/objects/" + object_id + "/default-view");
                      dispatch(updateIsFirst(true));
                    }
                  }}
                >
                  <Option value={"list_view"}>
                    {t("knowledgeBase.goToListView")}
                  </Option>
                  <Option value={"knowledgebase"}>
                    {t("knowledgeBase.knowledgeBase")}
                  </Option>
                </Select>
              </Row>
            </Col>
          </Row>
          <Row
            style={{ height: "90%", overflowX: "hidden", overflowY: "scroll" }}
            onScroll={(e) => {
              if (
                e.target.scrollTop + e.target.clientHeight >=
                  e.target.scrollHeight &&
                totalPage - 1 > page
              ) {
                let temp = [];
                if (searchArticle.length > 0) {
                  temp.push({
                    id_field: editKnowledgeBaseSetting.title,
                    value: searchArticle,
                  });
                }
                if (filterArticle.section_name) {
                  temp.push({
                    id_field: editKnowledgeBaseSetting.section_name,
                    value: filterArticle.section_name,
                  });
                }
                if (filterArticle.category_name) {
                  temp.push({
                    id_field: editKnowledgeBaseSetting.category_name,
                    value: filterArticle.category_name,
                  });
                }
                dispatch(
                  loadMoreArticle({
                    data: {
                      object_id: object_id,
                      page: page + 1,
                      limit: limit,
                      search_with_title: searchArticle,
                      ...filterArticle,
                    },
                    pagination: {
                      object_id: object_id,
                      current_page: page,
                      record_per_page: 16,
                      search_with: {
                        meta: [],
                        data: temp,
                      },
                      sort_by: {},
                    },
                  })
                );
              }
            }}
          >
            <Col span={24}>
              <Row
                style={{ overflowX: "hidden", overflowY: "scroll" }}
                align="top"
              >
                {listArticle &&
                  listArticle.map((article) => {
                    return (
                      <Col span={6} style={{ padding: "1rem 1rem 0rem 0rem" }}>
                        <Article
                          onClick={() => {
                            navigate(
                              "/article-detail?id=" +
                                article._id +
                                "&object_id=" +
                                object_id
                            );
                          }}
                        >
                          <Row justify={"space-between"}>
                            <Col span={18}>
                              <span className={"title-article"}>
                                {" "}
                                {article.title}{" "}
                              </span>
                            </Col>
                            <Col span={5} className={"icon-edit-delete"}>
                              <img
                                src={editIcon}
                                alt=""
                                onClick={(e) => {
                                  e.stopPropagation();
                                  e.preventDefault();
                                  navigate(
                                    "/articles?object_id=" +
                                      object_id +
                                      "&id=" +
                                      article._id
                                  );
                                }}
                              />
                              <img
                                src={deleteIcon}
                                alt=""
                                onClick={(e) => {
                                  e.stopPropagation();
                                  e.preventDefault();
                                  deleteArticle(article._id);
                                }}
                              />
                            </Col>
                          </Row>
                          <span className={"body-article"}>
                            {" "}
                            <p>{parse(article.body, optionsParse)}</p>
                          </span>
                        </Article>
                      </Col>
                    );
                  })}
              </Row>
            </Col>
          </Row>
          {/* {
                   totalPage-1 > page && 
                   <Row justify='end'>
                        <Col span={2}>
                                <span style={{fontSize: '16px'}} onClick={() => {
                                    let temp = []
                                    if(searchArticle.length > 0) {
                                        temp.push({
                                            "id_field": editKnowledgeBaseSetting.title,
                                            "value": searchArticle
                                        })
                                    }
                                    if(filterArticle.section_name) {
                                        temp.push({
                                            "id_field": editKnowledgeBaseSetting.section_name,
                                            "value": filterArticle.section_name
                                        })
                                    }
                                    if(filterArticle.category_name) {
                                        temp.push({
                                            "id_field": editKnowledgeBaseSetting.category_name,
                                            "value": filterArticle.category_name
                                        })
                                    }
                                dispatch(loadMoreArticle({
                                    data:
                                    {
                                        "object_id": object_id,
                                        "page": page+1,
                                        "limit": limit,
                                        "search_with_title": searchArticle,
                                        ...filterArticle
                                    },
                                    pagination: {
                                        "object_id": object_id,
                                        "current_page": page,
                                        "record_per_page": 16,
                                        "search_with": {
                                            "meta": [],
                                            "data": temp
                                        },
                                        "sort_by": {}
                                    }
                                }))
                                }}> Load more </span>
                        </Col>
                    </Row>
               } */}
        </Articles>
        <ModalConfimDelete
          title={t("knowledgeBase.deleteArticle")}
          decs={t("common.descriptionDelete")}
          methodDelete={deleteArticleById}
          dataDelete={dataDelete}
          isLoading={showLoadingScreen}
        />
      </CustomContent>
    </>
  );
};

export default KnowledgeBaseView;

const CustomContent = styled(Row)`
  padding: 1rem;
  width: 100%;
  height: 95%;
  overflow: hidden scroll;
`;

const TreeView = styled(Col)`
  height: 100%;
  background-color: #ffffff;
`;

const Articles = styled(Col)`
  height: 100%;
  overflow: hidden;
  padding: 1rem;
  background-color: #ffffff;
`;
const Article = styled.div`
  padding: 1rem;
  height: 20vh;
  .title-article {
    font-size: 16px;
    font-family: var(--roboto-700);
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
  }
  .body-article {
    font-family: var(--roboto-400);
    font-size: 16px;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
  border: 1px solid #ececec;
  box-sizing: border-box;
  border-radius: 4px;
  .icon-edit-delete {
    display: none;
  }
  &:hover {
    border-bottom: 3px solid ${(props) => props.theme.main};
    .icon-edit-delete {
      display: block;
    }
  }
`;
const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;
