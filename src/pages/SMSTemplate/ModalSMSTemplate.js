import { useEffect, useRef } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useTranslation, withTranslation } from "react-i18next";

import Modal from "antd/lib/modal";
import Form from "antd/lib/form";
import Button from "antd/lib/button";
import Input from "antd/lib/input";
import Select from "antd/lib/select";
import Radio from "antd/lib/radio";

import {
  createSMSTemplate,
  getSMSTemplateByIdSuccess,
  setShowDrawer,
  updateFieldSMSTemplate,
  updateSMSTemplate,
} from "redux/slices/smsTemplate";

import { loadListObjectFieldSuccess } from "redux/slices/objects";

import { generalInfo } from "util/staticData";
import Editor from "components/Editor/Editor2";

function ModalSetting({
  listOptionObjectField,
  optionsAppend,
  setOptionsAppend,
}) {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const editorJodit = useRef(null);

  const { showDrawer, editSMSTemplate } = useSelector(
    (state) => state.smsTemplateReducer
  );

  const {
    content,
    description,
    object_id,
    selected_fields,
    selected_general_fields,
    sms_template_name,
    unicode,
  } = editSMSTemplate;
  const { listObject } = useSelector((state) => state.objectsManagementReducer);

  // const [deselect, setDeselect] = useState("");

  const _onSubmit = () => {
    if (editSMSTemplate._id) {
      dispatch(
        updateSMSTemplate({
          ...editSMSTemplate,
          content: editorJodit.current.value.replaceAll(/<[^>]*>/g, ""),
        })
      );
    } else {
      dispatch(
        createSMSTemplate({
          ...editSMSTemplate,
          content: editorJodit.current.value.replaceAll(/<[^>]*>/g, ""),
        })
      );
    }
  };

  const _onCancel = () => {
    dispatch(setShowDrawer(false));
    dispatch(loadListObjectFieldSuccess([]));
    dispatch(getSMSTemplateByIdSuccess({}));
    setOptionsAppend([]);
    form.resetFields();
  };

  // const handleChangeEditor = (value) => {
  //   dispatch(
  //     updateFieldSMSTemplate({
  //       key: "content",
  //       value: value,
  //     })
  //   );
  // };

  useEffect(() => {
    let arr = [];
    // eslint-disable-next-line
    listOptionObjectField.length > 0 &&
      selected_fields &&
      selected_fields.forEach((item) => {
        listOptionObjectField.forEach((object) => {
          object.sections.forEach((section) => {
            let temp = section.fields.filter(
              (x) =>
                (x.full_field_id && x.full_field_id === item) ||
                (!x.full_field_id && x.field_id === item)
            )[0];
            if (temp) {
              arr.push({
                label: temp.related_name ? temp.related_name : temp.name,
                value: item,
              });
            }
          });
        });
      });
    // eslint-disable-next-line
    selected_general_fields &&
      selected_general_fields.forEach((item) => {
        arr.push(generalInfo.filter((x) => x.value === item)[0]);
      });
    setOptionsAppend(arr);
    // eslint-disable-next-line
  }, [selected_general_fields, selected_fields, listOptionObjectField]);

  return (
    <ModalCustom
      title={
        editSMSTemplate._id
          ? t("SMSTemplate.editSMSTemplate")
          : t("SMSTemplate.addNewTemplate")
      }
      visible={showDrawer}
      footer={null}
      width={800}
      onCancel={_onCancel}
    >
      <Form
        form={form}
        onFinish={_onSubmit}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        colon={false}
        labelAlign="left"
      >
        <Form.Item
          label={t("SMSTemplate.smsTemplateName")}
          name={"sms_template_name"}
          rules={[
            {
              validator: (rule, value = sms_template_name, cb) => {
                sms_template_name.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
          valuePropName={sms_template_name}
        >
          <Input
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "sms_template_name",
                  value: e.target.value,
                })
              );
            }}
            value={sms_template_name}
          ></Input>
        </Form.Item>

        <Form.Item
          label={t("common.description")}
          name={"description"}
          valuePropName={description}
        >
          <Input
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "description",
                  value: e.target.value,
                })
              );
            }}
            value={description}
          />
        </Form.Item>

        <Form.Item
          label={t("object.object")}
          name={"object"}
          rules={[
            {
              validator: (rule, value = object_id, cb) => {
                object_id.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
          valuePropName={object_id}
        >
          <Select
            value={object_id}
            disabled={editSMSTemplate._id ? true : false}
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "object_id",
                  value: e,
                })
              );
              dispatch(
                updateFieldSMSTemplate({
                  key: "selected_fields",
                  value: [],
                })
              );
              dispatch(
                updateFieldSMSTemplate({
                  key: "selected_general_fields",
                  value: [],
                })
              );
            }}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listObject &&
              // eslint-disable-next-line
              listObject.map((item) => {
                if (item.Status) {
                  return (
                    <Select.Option value={item._id}>{item.Name}</Select.Option>
                  );
                }
              })}
          </Select>
        </Form.Item>

        <Form.Item
          label={t("SMSTemplate.fields")}
          valuePropName={selected_fields}
          name={"selected_fields"}
          rules={[
            {
              validator: (rule, value = selected_fields, cb) => {
                selected_fields.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
        >
          <Select
            style={{ width: "100%" }}
            mode="multiple"
            value={selected_fields}
            onDeselect={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "content",
                  value: content.replaceAll("$" + e, ""),
                })
              );
              // setDeselect("$" + e);
            }}
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "selected_fields",
                  value: e,
                })
              );
            }}
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {listOptionObjectField &&
              listOptionObjectField.map((object) => {
                return (
                  object.sections && (
                    <Select.OptGroup label={object.object_name}>
                      {object.sections.map((section) => {
                        return section.fields.map((field) => {
                          return (
                            <>
                              {field.type !== "file" &&
                                field.type !== "formula" &&
                                field.type !== "dynamic" &&
                                !field.hidden && (
                                  <Select.Option
                                    value={
                                      field.full_field_id
                                        ? field.full_field_id
                                        : field.field_id
                                    }
                                    key={
                                      field.full_field_id
                                        ? field.full_field_id
                                        : field.field_id
                                    }
                                  >
                                    {field.type && !object.main_object
                                      ? object.object_name + "." + field.name
                                      : field.name}
                                  </Select.Option>
                                )}
                            </>
                          );
                        });
                      })}
                    </Select.OptGroup>
                  )
                );
              })}
          </Select>
        </Form.Item>
        <Form.Item
          label={t("SMSTemplate.generalFields")}
          valuePropName={selected_general_fields}
          name={"selected_general_fields"}
          rules={[
            {
              validator: (rule, value = selected_general_fields, cb) => {
                selected_general_fields.length === 0
                  ? cb(t("common.requiredField"))
                  : cb();
              },
              required: true,
            },
          ]}
        >
          <Select
            style={{ width: "100%" }}
            value={selected_general_fields}
            mode="multiple"
            onDeselect={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "content",
                  value: content.replaceAll("$" + e, ""),
                })
              );
              // setDeselect("$" + e);
            }}
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "selected_general_fields",
                  value: e,
                })
              );
            }}
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {generalInfo &&
              generalInfo.map((item) => {
                return (
                  <Select.Option value={item.value}>{item.label}</Select.Option>
                );
              })}
          </Select>
        </Form.Item>

        <Form.Item
          valuePropName={unicode}
          label={"Unicode"}
          name={"unicode"}
          rules={[
            {
              validator: (rule, value = unicode, cb) => {
                unicode.length === 0 ? cb(t("common.requiredField")) : cb();
              },
              required: true,
            },
          ]}
        >
          <Radio.Group
            options={[
              {
                label: "0",
                value: 0,
              },
              {
                label: "1",
                value: 1,
              },
            ]}
            optionType="button"
            value={unicode}
            onChange={(e) => {
              dispatch(
                updateFieldSMSTemplate({
                  key: "unicode",
                  value: e.target.value,
                })
              );
            }}
          />
        </Form.Item>
        <Form.Item
          wrapperCol={{ span: 24 }}
          valuePropName={content}
          name="content"
        >
          <Editor
            editorJodit={editorJodit}
            showToolbar={false}
            optionsAppend={optionsAppend}
            content={content}
            minHeightInput={"300px"}
          />
        </Form.Item>

        <WrapButton label=" ">
          <Button type="primary" htmlType="submit">
            {t("common.save")}
          </Button>
          <Button onClick={_onCancel}>{t("common.cancel")}</Button>
        </WrapButton>
      </Form>
    </ModalCustom>
  );
}

export default withTranslation()(ModalSetting);

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const WrapButton = styled(Form.Item)`
  margin-bottom: 0;

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: flex-end;
  }
`;
