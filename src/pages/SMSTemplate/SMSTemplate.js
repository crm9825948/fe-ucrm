import _ from "lodash";
import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";

import { Breadcrumb, Button, Table, Tooltip } from "antd";

import { setShowModalConfirmDelete } from "redux/slices/global";
import { loadListObjectField } from "redux/slices/objects";
import {
  deleteSMSTemplate,
  getListSMSTemplate,
  getSMSTemplateById,
  getSMSTemplateByIdSuccess,
  setShowDrawer,
  unmountSMSTemplate,
  updateFieldSMSTemplate,
} from "redux/slices/smsTemplate";

import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import ModalSMSTemplate from "./ModalSMSTemplate";

import emptyEmail from "assets/icons/email/empty-email.svg";
import deleteIcon from "assets/icons/email/deleteIcon.svg";
import editIcon from "assets/icons/email/editIcon.svg";
import { changeTitlePage } from "redux/slices/authenticated";

const SMSTemplate = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { userRuleGlobal } = useSelector((state) => state.userReducer);

  const { listSMSTemplate, editSMSTemplate } = useSelector(
    (state) => state.smsTemplateReducer
  );
  const { listObjectField } = useSelector((state) => state.objectsReducer);
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  // eslint-disable-next-line
  const { content, object_id, selected_fields } = editSMSTemplate;
  // eslint-disable-next-line
  const [searchText, setSearchText] = useState("");
  const [optionsAppend, setOptionsAppend] = useState([]);
  // eslint-disable-next-line
  const [searchedColumn, setSearchedColumn] = useState("");
  const [dataDelete, setDataDelete] = useState({});
  const [listOptionObjectField, setListOptionObjectField] = useState([]);

  useEffect(() => {
    dispatch(changeTitlePage(t("SMSTemplate.smsTemplate")));
    //eslint-disable-next-line
  }, [t]);

  const metaData = [
    {
      name: "Created Date",
      full_field_id: "created_date",
      type: "date",
    },
    {
      name: "Created By",
      full_field_id: "created_by",
      type: "text",
    },
    {
      name: "Modify Time",
      full_field_id: "modify_time",
      type: "date",
    },
    {
      name: "Modified by",
      full_field_id: "modify_by",
      type: "text",
    },
    {
      name: "Assign To",
      full_field_id: "owner",
      type: "text",
    },
  ];

  const checkRule = (rule) => {
    if (
      userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === "sms_template" &&
          item.actions.includes(rule)
      )
    )
      return true;
    else return false;
  };

  const getColumnSearchProps = (dataIndex) => ({
    // filterDropdown: ({
    //   setSelectedKeys,
    //   selectedKeys,
    //   confirm,
    //   clearFilters,
    // }) => (
    //   <div style={{ padding: 8 }}>
    //     <Input
    //       placeholder={`Search`}
    //       value={selectedKeys[0]}
    //       onChange={(e) =>
    //         setSelectedKeys(e.target.value ? [e.target.value] : [])
    //       }
    //       onPressEnter={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //       style={{ marginBottom: 8, display: "block" }}
    //     />
    //     <Space>
    //       <Button
    //         type="primary"
    //         onClick={() => handleSearch(selectedKeys, confirm, dataIndex)}
    //         icon={<SearchOutlined />}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.search")}
    //       </Button>
    //       <Button
    //         onClick={() => handleReset(clearFilters)}
    //         size="small"
    //         style={{ width: 90 }}
    //       >
    //         {t("common.reset")}
    //       </Button>
    //       <Button
    //         type="link"
    //         size="small"
    //         onClick={() => {
    //           confirm({ closeDropdown: false });
    //           setSearchText(selectedKeys[0]);
    //           setSearchedColumn(dataIndex);
    //         }}
    //       >
    //         {t("SMSTemplate.filterData")}
    //       </Button>
    //     </Space>
    //   </div>
    // ),
    // filterIcon: (filtered) => (
    //   <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    // ),
    // onFilter: (value, record) =>
    //   record[dataIndex]
    //     ? record[dataIndex]
    //         .toString()
    //         .toLowerCase()
    //         .includes(value.toLowerCase())
    //     : "",
    render: (text) =>
      //   searchedColumn === dataIndex ? (
      //     <Highlighter
      //       highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
      //       searchWords={[searchText]}
      //       autoEscape
      //       textToHighlight={text ? text.toString() : ''}
      //     />
      //   ) : (
      text,
    //   ),
  });
  // const handleSearch = (selectedKeys, confirm, dataIndex) => {
  //   confirm();
  //   setSearchText(selectedKeys[0]);
  //   setSearchedColumn(dataIndex);
  // };
  const columns = [
    {
      title: t("SMSTemplate.smsTemplateName"),
      dataIndex: "sms_template_name",
      key: "sms_template_name",
      ...getColumnSearchProps("sms_template_name"),
    },
    {
      title: t("object.object"),
      dataIndex: "object_name",
      key: "object_name",
      ...getColumnSearchProps("object_name"),
    },
    {
      title: t("common.description"),
      dataIndex: "description",
      key: "description",
      ...getColumnSearchProps("description"),
    },
    {
      title: t("common.action"),
      key: "operation",
      fixed: "right",
      width: 100,
      render: (record) => (
        <WrapAction>
          {checkRule("edit") && (
            <Tooltip title={t("common.edit")}>
              <img
                alt="Edit"
                src={editIcon}
                onClick={() => updateSMSTemplateEdit(record)}
              />
            </Tooltip>
          )}
          {checkRule("delete") && (
            <Tooltip title={t("common.delete")}>
              <img
                alt="Delete"
                src={deleteIcon}
                onClick={() => removeSMSTemplate(record)}
              />
            </Tooltip>
          )}
        </WrapAction>
      ),
    },
  ];

  const addSmsTemplate = () => {
    dispatch(setShowDrawer(true));
    dispatch(
      getSMSTemplateByIdSuccess({
        object_id: "",
        selected_fields: [],
        selected_general_fields: [],
        sms_template_name: "",
        unicode: "",
        content: "",
      })
    );
  };
  const updateSMSTemplateEdit = (record) => {
    dispatch(
      getSMSTemplateById({
        _id: record._id,
      })
    );
  };
  const removeSMSTemplate = (record) => {
    dispatch(setShowModalConfirmDelete(true));
    setDataDelete({
      _id: record._id,
    });
  };

  useEffect(() => {
    dispatch(getListSMSTemplate());
  }, [dispatch]);
  useEffect(() => {
    if (object_id) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object_id,
        })
      );
    }
    // eslint-disable-next-line
  }, [object_id]);

  useEffect(() => {
    let arr = [];
    if (listObjectField.length > 0) {
      // eslint-disable-next-line
      listObjectField.map((item) => {
        Object.keys(item).forEach((key) => {
          let isFirst = true;
          let temp = { ...item[key] };
          let object_name = temp.object_name;
          temp.sections =
            temp.sections &&
            temp.sections.map((section) => {
              let t = { ...section };
              let temp = t.fields.filter((x) => x.hidden);
              temp.forEach((x) => {
                let t1 = [...selected_fields];
                _.remove(t1, (y) => {
                  return y === x.full_field_id;
                });

                dispatch(
                  updateFieldSMSTemplate({
                    key: "selected_fields",
                    value: t1,
                  })
                );
                dispatch(
                  updateFieldSMSTemplate({
                    key: "content",
                    value: content.replaceAll("$" + x.full_field_id, ""),
                  })
                );
              });
              if (key !== "main_object" && isFirst) {
                isFirst = false;
                t.fields = [
                  ...t.fields,
                  {
                    full_field_id:
                      section.fields &&
                      section.fields.length > 0 &&
                      section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "created_date"
                          )
                        : "created_date",
                    name: object_name + ".Created date",
                  },
                  {
                    full_field_id:
                      section.fields &&
                      section.fields.length > 0 &&
                      section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "created_by"
                          )
                        : "created_by",
                    name: object_name + ".Created by",
                  },
                  {
                    full_field_id:
                      section.fields &&
                      section.fields.length > 0 &&
                      section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "modify_by"
                          )
                        : "modify_by",
                    name: object_name + ".Modified by",
                  },
                  {
                    full_field_id:
                      section.fields &&
                      section.fields.length > 0 &&
                      section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "modify_date"
                          )
                        : "modify_date",
                    name: object_name + ".Modify date",
                  },
                  {
                    full_field_id:
                      section.fields &&
                      section.fields.length > 0 &&
                      section.fields[0].full_field_id
                        ? section.fields[0].full_field_id.replace(
                            section.fields[0].field_id,
                            "owner"
                          )
                        : "owner",
                    name: object_name + ".Assign to",
                  },
                ];
              }
              return t;
            });
          temp.main_object = key !== "main_object" ? false : true;

          if (temp.main_object) {
            temp.sections[0].fields = [...temp.sections[0].fields, ...metaData];
          }

          arr.push(temp);
        });
      });
    }
    setListOptionObjectField(arr);
    // eslint-disable-next-line
  }, [listObjectField]);

  // unmount
  useEffect(
    () => () => {
      dispatch(unmountSMSTemplate());
      setSearchText("");
      setOptionsAppend([]);
      setSearchedColumn("");
      setListOptionObjectField([]);
    },
    // eslint-disable-next-line
    []
  );
  return (
    <Wrapper>
      <WrapBreadcrumb>
        <Breadcrumb>
          <Breadcrumb.Item onClick={() => navigate("/settings")}>
            {t("settings.settings")}
          </Breadcrumb.Item>
          <BreadcrumbItem>{t("SMSTemplate.smsTemplate")}</BreadcrumbItem>
        </Breadcrumb>
        {listSMSTemplate.length > 0 && checkRule("create") && (
          <AddButton onClick={addSmsTemplate}>
            + {t("SMSTemplate.addNewTemplate")}
          </AddButton>
        )}
      </WrapBreadcrumb>

      {listSMSTemplate.length > 0 ? (
        <WrapTable>
          <Table
            dataSource={listSMSTemplate}
            columns={columns}
            pagination={{
              position: ["bottomRight"],
              showSizeChanger: true,
              defaultPageSize: 10,
              showQuickJumper: true,
            }}
          />
        </WrapTable>
      ) : (
        <Empty>
          <img src={emptyEmail} alt="empty" />
          <p>
            {t("object.noObject")} <span>{t("SMSTemplate.smsTemplate")}</span>
          </p>
          {checkRule("create") && (
            <AddButton onClick={addSmsTemplate}>
              + {t("SMSTemplate.addNewTemplate")}
            </AddButton>
          )}
        </Empty>
      )}

      <ModalSMSTemplate
        listOptionObjectField={listOptionObjectField}
        optionsAppend={optionsAppend}
        setOptionsAppend={setOptionsAppend}
      />

      <ModalConfimDelete
        title={t("SMSTemplate.deleteSMS")}
        decs={t("common.descriptionDelete")}
        methodDelete={deleteSMSTemplate}
        dataDelete={dataDelete}
        isLoading={showLoadingScreen}
      />
    </Wrapper>
  );
};

export default SMSTemplate;

const Wrapper = styled.div`
  padding: 16px 24px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapAction = styled.div`
  img {
    cursor: pointer;
    margin-left: 8px;

    :hover {
      background: #eeeeee;
    }
  }
`;

const WrapBreadcrumb = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const AddButton = styled(Button)`
  background: ${(props) => props.theme.main};
  border: 1px solid ${(props) => props.theme.main};
  height: 40px;

  span {
    color: #fff !important;
    font-size: 16px;
  }

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;

const WrapTable = styled.div`
  background: #fff;
  padding: 24px;
  margin-top: 16px;

  .ant-table-thead > tr > th {
    font-size: 16px;
    color: #2c2c2c;
    background: #f0f0f0;
    border-right: 1px solid #d9d9d9;
    border-bottom: 1px solid #d9d9d9;
    padding: 8.5px 16px;

    :before {
      display: none;
    }

    :first-child {
      border-left: 1px solid #d9d9d9;
    }
  }

  .ant-table-tbody > tr > td {
    font-size: 16px;
    color: #2c2c2c;
    padding: 8.5px 16px;

    :first-child {
      border-left: 1px solid #d9d9d9;
    }

    :last-child {
      border-right: 1px solid #d9d9d9;
    }
  }

  .ant-table-content {
    ::-webkit-scrollbar {
      height: 8px;
    }
  }

  .ant-pagination {
    width: 100%;
    background: #fff;
    display: flex;
    justify-content: flex-end;
    margin-bottom: 0;
  }

  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};

    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.darker};

    a {
      color: ${(props) => props.theme.darker};
    }
  }

  .ant-pagination-next:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }

  .ant-pagination-prev:hover .ant-pagination-item-link {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
  }
`;
