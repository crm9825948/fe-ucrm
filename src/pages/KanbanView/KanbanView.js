import {
  Col,
  // Pagination,
  Row,
  Menu,
  Dropdown,
  Button,
  Form,
  Modal,
  Typography,
  Tooltip,
} from "antd";
import CustomView from "pages/Objects/customView/customView";
import React, { useEffect, useState } from "react";
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import ModalRecord from "pages/Objects/modal/modalRecord";
import { useNavigate, useParams } from "react-router";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";
import ModalDuplicate from "components/Modal/ModalDuplicated";
import {
  getDataKanbanView,
  getSettingKanban,
  updateColumn,
  updateRecordKanbanOnDrag,
  updateRecordKanban,
  unmountKanbanView,
  setTotalRecord,
  getPaginationKanban,
  updateArgument,
  setCurrentPage,
} from "redux/slices/kanbanView";
import {
  loadListObjectField,
  setShowModal,
  toggleFavouriteObject,
  createRecord,
  loadRecordDataSuccess,
  setLinkingFieldValue,
  loadFavouriteObjects,
} from "redux/slices/objects";
import emptyEmail from "../../assets/icons/email/empty-email.svg";

import { useDispatch, useSelector } from "redux/store";
import styled from "styled-components";
import Record from "./Record";
import unpinImg from "assets/icons/objects/unpin.svg";
import pinImg from "assets/icons/objects/png.svg";
import plusIcon from "assets/icons/objectsManagement/plus.svg";
import IconTop from "assets/icons/common/iconTop.png";
import IconBottom from "assets/icons/common/iconBottom.png";
import NodataImg from "assets/icons/common/emptyData.png";
import moment from "moment";
import {
  DeleteOutlined,
  DownOutlined,
  EditOutlined,
  MergeCellsOutlined,
  CaretDownOutlined,
  EllipsisOutlined,
} from "@ant-design/icons";
import { setShowModalConfirmDelete } from "redux/slices/global";
import ThemeButton from "components/CustomThemes/ThemeButton";
import { runDynamicButton } from "redux/slices/dynamicButton";
import ImgConfirm from "assets/icons/common/confirm.png";
import { loadConfig } from "redux/slices/datetimeSetting";
import { useTranslation, withTranslation } from "react-i18next";
import Carousel from "react-multi-carousel";
import KanbanViewImg from "assets/icons/common/kanbanView.png";
import ListViewImg from "assets/icons/common/listView.png";
import ListViewDetailImg from "assets/icons/common/listViewDetail.png";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import { Spin } from "antd";
import { changeTitlePage } from "redux/slices/authenticated";

const { SubMenu } = Menu;
const KanbanView = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { object_id, customViewId } = useParams();
  const [recordID, setRecordID] = useState("");
  const [form] = Form.useForm();
  const {
    settingKanban,
    dataKanbanView,
    lastRecordId,
    firstRecordId,
    totalRecord,
    isLoadingData,
    page_records,
    argument,
    currentPage,
  } = useSelector((state) => state.kanbanViewReducer);

  const { userDetail } = useSelector((state) => state.userReducer);
  const { columns } = dataKanbanView;

  const recordPerPage = 50;
  // const [currentPage, setCurrentPage] = useState(1);
  const [prev, setPrev] = useState(0);
  const [next, setNext] = useState(0);

  const {
    column_configs,
    status_field,
    column_field,
    status_style,
    uncategorized_column_config,
    show_uncategorized,
    confirm_before_edit,
  } = settingKanban;

  useEffect(() => {
    // let response = 0;
    // if (columns) {
    //   columns.forEach((array) => {
    //     response = response + array.data.length;
    //   });
    // }
    if (columns) {
      let temPrev = currentPage * recordPerPage - 49;
      setPrev(temPrev);
      let temNext = temPrev + page_records - 1;
      setNext(temNext);
    } else {
      setPrev(0);
      setNext(0);
    }
    // eslint-disable-next-line
  }, [columns]);

  const { Text: TextComponent } = Typography;

  const [showCustomView, setShowCustomView] = useState(false);
  const [showModalConfirmEdit, setShowModalConfirmEdit] = useState(false);
  const [recordEdit, setRecordEdit] = useState({});
  // eslint-disable-next-line
  const [openMerge, setOpenMerge] = useState(false);
  // eslint-disable-next-line
  const [openMassEdit, setOpenMassEdit] = useState(false);
  // eslint-disable-next-line
  const [flagDelete, setFlagDelete] = useState("");
  // eslint-disable-next-line
  const [editingKey, setEditingKey] = useState("");
  const [create, setCreate] = useState(false);
  const [edit, setEdit] = useState(false);
  // eslint-disable-next-line
  const [read, setRead] = useState(false);
  const [dataConfirm, setDataConfirm] = useState({});
  const [showConfirm, setShowConfirm] = useState(false);

  const { category, visibleList } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const { recordData, fields, searchList } = useSelector(
    (state) => state.objectsReducer
  );
  let navigate = useNavigate();

  useEffect(() => {
    dispatch(changeTitlePage("Kanban view"));
    //eslint-disable-next-line
  }, [t]);

  const [allFields, setAllFields] = useState({});
  const { objectsFavourite, selectedRowKeys, customView, listObjectField } =
    useSelector((state) => state.objectsReducer);
  const menu = (
    <CustimMenuWrapper triggerSubMenuAction="click" style={{ width: "200px" }}>
      {/*eslint-disable-next-line*/}
      {Object.entries(category).map(([key, value], idx) => {
        if (visibleList[key])
          return (
            <CustomSubMenuWrapper key={key} title={key}>
              {/*eslint-disable-next-line*/}
              {value.map((item, index) => {
                if (item.visible === true && item.Status === true)
                  return (
                    <CustomMenuItem
                      key={item._id}
                      style={{
                        whiteSpace: "normal",
                        wordBreak: "break-all",
                        width: "200px",
                      }}
                    >
                      <CustomOption>
                        <div> {item.Name}</div>
                        {objectsFavourite.findIndex(
                          (object) => object.object_id === item._id
                        ) >= 0 ? (
                          <img
                            className="unpin-img"
                            alt=""
                            src={unpinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        ) : (
                          <img
                            className="pin-img"
                            alt=""
                            src={pinImg}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          />
                        )}
                      </CustomOption>
                    </CustomMenuItem>
                  );
              })}
            </CustomSubMenuWrapper>
          );
      })}
    </CustimMenuWrapper>
  );

  const menuView = (
    <Menu>
      <CustomItem
        onClick={() => {
          navigate("/objects/" + object_id + "/" + customViewId);
        }}
      >
        <ItemText> List view </ItemText>
        <img src={ListViewImg} alt="Kanban view" />
      </CustomItem>
      <CustomItem
        onClick={() => {
          navigate(
            `/list-view-with-details/objects/${object_id}/${customViewId}`
          );
        }}
      >
        <ItemText> List view with detail</ItemText>
        <img src={ListViewDetailImg} alt=" List view with detail" />
      </CustomItem>
    </Menu>
  );

  const [open, setOpen] = useState(false);
  // eslint-disable-next-line
  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };
  const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;

    return result;
  };
  const onDragEnd = (result) => {
    const { source, destination, draggableId } = result;
    if (!destination) {
      return;
    }
    const sInd = source.droppableId - 1;
    const dInd = destination.droppableId - 1;
    if (confirm_before_edit && sInd !== dInd && dInd !== columns.length - 1) {
      setShowModalConfirmEdit(true);
      setRecordEdit({
        columns: columns,
        sInd: sInd,
        dInd: dInd,
        source: source,
        destination: destination,
        draggableId: draggableId,
      });
    } else {
      if (sInd !== dInd && dInd !== columns.length - 1) {
        const result = move(
          columns[sInd].data,
          columns[dInd].data,
          source,
          destination
        );
        // eslint-disable-next-line
        Object.keys(result).forEach((key) => {
          dispatch(
            updateColumn({
              index: key - 1,
              data: result[key],
            })
          );
        });
        dispatch(
          updateRecordKanbanOnDrag({
            record: {
              object_id: object_id,
              id: draggableId,
            },
            column_field: column_field,
            value_field: columns[dInd].value,
            // current_page: current_page,
            first_record_id: argument.first,
            last_record_id: argument.last,
          })
        );
      }
    }
  };
  const handleCustomViewName = (id) => {
    if (id === "default-view") {
      return t("emailIncoming.default");
    }
    let name =
      customView &&
      customView.custom_views &&
      customView.custom_views.find((item) => item._id === id);
    return name && name.view_name;
  };
  // eslint-disable-next-line
  const menuExcel = (
    <Menu mode="inline">
      <Menu.Item key="0">{t("kanbanView.importExcel")}</Menu.Item>
      <Menu.Item key="1">{t("kanbanView.downloadTemplate")}</Menu.Item>
    </Menu>
  );
  // eslint-disable-next-line
  const menuAction = (
    <Menu mode="inline">
      <Menu.Item
        key="0"
        icon={<EditOutlined />}
        onClick={() => {
          if (selectedRowKeys.length > 0) {
            setOpenMassEdit(true);
          }
        }}
      >
        {t("objectLayoutField.massEdit")}
      </Menu.Item>
      <Menu.Item
        key="1"
        icon={<DeleteOutlined />}
        onClick={() => {
          // setOpenMassDelete(true);
          setFlagDelete("many");
          dispatch(setShowModalConfirmDelete(true));
        }}
      >
        {t("objectLayoutField.massDelete")}
      </Menu.Item>
      <Menu.Item
        key="2"
        icon={<MergeCellsOutlined />}
        onClick={() => {
          if (selectedRowKeys.length <= 1) {
            Notification("error", t("objectLayoutField.errorMergeRecord1"));
          } else if (selectedRowKeys.length > 3) {
            Notification("error", t("objectLayoutField.errorMergeRecord2"));
          } else {
            setOpenMerge(true);
          }
        }}
      >
        {t("objectLayoutField.mergeRecord")}
      </Menu.Item>
    </Menu>
  );
  const submit = (values) => {
    let fieldsObject = {};
    /* eslint-disable-next-line */
    fields.map((section, idx) => {
      /* eslint-disable-next-line */
      section.fields.map((item, index) => {
        fieldsObject[item.ID] = { ...item };
      });
    });
    let listValue = [];
    // eslint-disable-next-line
    Object.entries(values).forEach(([key, value], idx) => {
      if (key !== "assignTo") {
        if (fieldsObject[key].type === "file" && value) {
          const result = [];
          /* eslint-disable-next-line */
          if (value.fileList) {
            // eslint-disable-next-line
            value.fileList.map((item, idx) => {
              result.push(item.url);
            });
          } else {
            // eslint-disable-next-line
            value.map((item, idx) => {
              result.push(item.url);
            });
          }

          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: result.length === 0 ? null : result,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "date" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "datetime-local" && value) {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? moment(value).format("YYYY-MM-DD HH:mm:ss") : null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "linkingobject") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else if (fieldsObject[key].type === "lookup") {
          let newItem = {
            id_field: key,
            id_field_related_record:
              (value && value.id_field_related_record) || null,
            id_related_record: (value && value.id_related_record) || null,
            object_related: (value && value.object_related) || null,
            value: (value && value.value) || null,
          };
          listValue.push(newItem);
        } else {
          let newItem = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: value ? value : null,
          };
          listValue.push(newItem);
        }
      }
    });
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchList).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    if (recordData._id) {
      let payload = [];
      // eslint-disable-next-line
      Object.keys(values).forEach((key) => {
        if (key !== "assignTo") {
          let temp = {
            id_field: key,
            id_field_related_record: null,
            id_related_record: null,
            object_related: null,
            value: values[key],
          };
          payload.push(temp);
        }
      });
      dispatch(
        updateRecordKanban({
          id: recordData._id,
          object_id: object_id,
          owner_id: recordData.owner,
          data: listValue,
          // current_page: current_page,
          first_record_id: argument.first,
          last_record_id: argument.last,
        })
      );
    } else {
      dispatch(setCurrentPage(1));
      dispatch(
        createRecord({
          data: {
            object_id: object_id,
            data: listValue,
            owner: values["assignTo"],
          },
          kanban: {
            object_id: object_id,
            search_with: { meta: [], data: [] },
            // record_per_page: 10,
            // current_page: current_page,
            first_record_id: null,
            last_record_id: null,
          },
        })
      );
    }
    dispatch(loadRecordDataSuccess({}));
  };

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.1)`;
    }
  };

  useEffect(() => {
    if (object_id) {
      dispatch(
        getSettingKanban({
          object_id: object_id,
        })
      );
      dispatch(
        getDataKanbanView({
          object_id: object_id,
          search_with: { meta: [], data: [] },
          custom_view_id: customViewId,
          // record_per_page: 50,
          // current_page: 1,
          first_record_id: null,
          last_record_id: null,
        })
      );
    }
    dispatch(loadConfig());
    dispatch(
      loadListObjectField({
        api_version: "2",
        object_id: object_id,
      })
    );
    dispatch(loadFavouriteObjects());
    dispatch(setCurrentPage(1));
    dispatch(setTotalRecord(null));
    const param = {
      first: null,
      last: null,
    };
    dispatch(updateArgument(param));
    // eslint-disable-next-line
  }, [object_id, customViewId]);
  useEffect(() => {
    /* eslint-disable-next-line */
    listObjectField.map((item, idx) => {
      if (Object.keys(item)[0] === "main_object") {
        if (item[Object.keys(item)[0]]) {
          setCreate(item[Object.keys(item)[0]].create_permission);
          setEdit(item[Object.keys(item)[0]].edit_permission);
          setRead(item[Object.keys(item)[0]].readable);
        } else {
          setCreate(false);
          setRead(false);
        }
      }
    });
  }, [listObjectField]);
  // unmount
  useEffect(
    () => () => {
      dispatch(unmountKanbanView());
    },
    // eslint-disable-next-line
    []
  );
  useEffect(() => {
    let newObj = {};
    /* eslint-disable-next-line */
    Object.entries(allFields).forEach(([key, value], index) => {
      if (value.type === "linkingobject") {
        newObj[key] = { ...recordData[key] };
      }
    });
    dispatch(setLinkingFieldValue(newObj));
    /* eslint-disable-next-line */
  }, [recordData]);
  useEffect(() => {
    let newObj = {};
    /* eslint-disable-next-line */
    fields.map((section, idx) => {
      /* eslint-disable-next-line */
      section.fields.map((item, idx) => {
        newObj[item.ID] = { ...item };
        newObj[item.ID].name = "";
      });
    });
    setAllFields(newObj);
  }, [fields]);

  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1880 },
      items: 12,
      slidesToSlide: 12,
    },
    tablet: {
      breakpoint: { max: 1880, min: 1400 },
      items: 9,
      slidesToSlide: 9,
    },
    mobile: {
      breakpoint: { max: 1400, min: 0 },
      items: 7,
      slidesToSlide: 7,
    },
  };

  return (
    <KanbanWrap>
      <CustomHeader>
        {open === false ? (
          ""
        ) : (
          <CustomMenuHeader>
            <CarouselWrap>
              <Carousel responsive={responsive}>
                {objectsFavourite.map((item, idx) => {
                  if (item && item.object_name)
                    return (
                      <MenuItem
                        className={`btn btn-top ${
                          object_id === item.object_id ? "active" : ""
                        }`}
                        key={item.object_id}
                        onClick={() =>
                          navigate(
                            `/kanban-view/${item.object_id}/default-view`
                          )
                        }
                      >
                        <TextComponent
                          style={{
                            color: `${
                              object_id === item.object_id ? "#fff" : "#2c2c2c"
                            }`,
                          }}
                          ellipsis={{ tooltip: item.object_name }}
                        >
                          {item.object_name}
                        </TextComponent>
                      </MenuItem>
                    );
                  return null;
                })}
              </Carousel>
            </CarouselWrap>
            <CustomDropdown
              overlay={menu}
              placement="bottomLeft"
              trigger="click"
            >
              <CustomNewButton>
                Tất cả <DownOutlined />
              </CustomNewButton>
            </CustomDropdown>
            <CustomDisplay>
              {t("common.view")}:
              <span
                onClick={() => {
                  setShowCustomView(true);
                }}
              >
                {handleCustomViewName(customViewId)}
              </span>
            </CustomDisplay>
          </CustomMenuHeader>
        )}

        <div style={{ display: "none" }}>
          <CustomClose
            onClick={() => {
              setOpen(!open);
            }}
          >
            {open === false ? (
              <>
                <img src={IconBottom} alt="" />
              </>
            ) : (
              <>
                <img src={IconTop} alt="" />
              </>
            )}
          </CustomClose>
        </div>
      </CustomHeader>
      <HeaderContent>
        <PaginationWrap>
          {isLoadingData ? (
            <Spin />
          ) : (
            <InfoPagi>
              <span>
                {prev} - {next}
              </span>
              {totalRecord === null ? (
                <ReloadWrap
                  onClick={() => {
                    dispatch(
                      getPaginationKanban({
                        current_page: 1,
                        record_per_page: 50,
                        object_id: object_id,
                      })
                    );
                  }}
                >
                  <img src={Reload} alt="reload" />
                </ReloadWrap>
              ) : (
                <span style={{ marginLeft: "8px" }}>
                  of {totalRecord} records
                </span>
              )}
            </InfoPagi>
          )}
          <CustomButton>
            <ImgPrev
              current={currentPage}
              onClick={() => {
                const param = {
                  first: firstRecordId,
                  last: null,
                };
                if (page_records === 0) {
                  dispatch(setCurrentPage(1));
                } else {
                  const tem = currentPage - 1;
                  dispatch(setCurrentPage(tem));
                }
                dispatch(updateArgument(param));
                dispatch(
                  getDataKanbanView({
                    object_id: object_id,
                    search_with: { meta: [], data: [] },
                    custom_view_id: customViewId,
                    first_record_id: firstRecordId,
                    last_record_id: null,
                  })
                );
              }}
            >
              <img src={LeftPagi} alt="left" />
            </ImgPrev>
            <ImgNext
              totalRecord={page_records}
              onClick={() => {
                const tem = currentPage + 1;
                dispatch(setCurrentPage(tem));
                const param = {
                  first: null,
                  last: lastRecordId,
                };
                dispatch(updateArgument(param));
                dispatch(
                  getDataKanbanView({
                    object_id: object_id,
                    search_with: { meta: [], data: [] },
                    custom_view_id: customViewId,
                    first_record_id: null,
                    last_record_id: lastRecordId,
                  })
                );
              }}
            >
              <img src={RightPagi} alt="right" />
            </ImgNext>
          </CustomButton>
        </PaginationWrap>

        <CustomWrapperAction>
          <CustomButtonAddRecord
            style={{ marginRight: "16px" }}
            size="large"
            onClick={() => {
              dispatch(setShowModal(true));
              dispatch(loadRecordDataSuccess({}));
            }}
            disabled={editingKey === "" && create ? false : true}
          >
            <img alt="" src={plusIcon} />
            {t("kanbanView.addRecord")}
          </CustomButtonAddRecord>
          <ViewWrap>
            <Dropdown overlay={menuView} arrow trigger={["click"]}>
              <OptionView>
                <img src={KanbanViewImg} alt="" />
                <Tooltip title="Kanban view">
                  <Text>Kanban view</Text>
                </Tooltip>
                <CaretDownOutlined />
              </OptionView>
            </Dropdown>
          </ViewWrap>
          <Button disabled size="large" style={{ marginLeft: "16px" }}>
            <EllipsisOutlined />
          </Button>
        </CustomWrapperAction>
      </HeaderContent>
      {columns && columns.length > 0 ? (
        <ContentWrap>
          <DragDropContext onDragEnd={onDragEnd}>
            {columns &&
              columns.map(
                (el, ind) =>
                  (ind !== columns.length - 1 ||
                    (ind === columns.length - 1 && show_uncategorized)) && (
                    <Droppable key={ind + 1} droppableId={ind + 1}>
                      {(provided) => (
                        <ColumnWrap>
                          <ColumnTitle
                            color={
                              column_configs &&
                              column_configs[ind] &&
                              column_configs[ind].style.color
                            }
                          >
                            <span>{el.value}</span>
                          </ColumnTitle>

                          <ColumnContent
                            color={() =>
                              hexToRGB(
                                column_configs &&
                                  column_configs[ind] &&
                                  column_configs[ind].style.color
                              )
                            }
                          >
                            <CustomWrap
                              ref={provided.innerRef}
                              {...provided.droppableProps}
                            >
                              {el.data.length === 0 && (
                                <Nodata>
                                  <img src={NodataImg} alt="no data" />
                                  <span>{t("common.noData")}</span>
                                </Nodata>
                              )}
                              {el.data.map((item, index) => (
                                <Draggable
                                  key={item._id}
                                  draggableId={item._id}
                                  index={index}
                                >
                                  {(provided) => (
                                    <div
                                      ref={provided.innerRef}
                                      {...provided.draggableProps}
                                      {...provided.dragHandleProps}
                                    >
                                      <div>
                                        {column_configs && (
                                          <Record
                                            listObjectField={listObjectField}
                                            colorShadow={
                                              column_configs &&
                                              column_configs[ind] &&
                                              column_configs[ind].style.color
                                            }
                                            edit={edit}
                                            form={form}
                                            display_fields={
                                              el.value !== "Uncategorized"
                                                ? column_configs[ind] &&
                                                  column_configs[ind]
                                                    .display_fields
                                                : uncategorized_column_config &&
                                                  uncategorized_column_config.display_fields
                                            }
                                            status_field={status_field}
                                            status_style={status_style}
                                            setRecordID={setRecordID}
                                            style={{
                                              backgroundColor: "white",
                                            }}
                                            fields_order={
                                              el.value !== "Uncategorized"
                                                ? el.fields_order
                                                : uncategorized_column_config &&
                                                  uncategorized_column_config.fields_order
                                            }
                                            object_id={object_id}
                                            type={
                                              el.value !== "Uncategorized"
                                                ? ""
                                                : t("kanbanView.uncategorized")
                                            }
                                            data={item}
                                            allFields={allFields}
                                            argument={argument}
                                          />
                                        )}
                                      </div>
                                    </div>
                                  )}
                                </Draggable>
                              ))}
                              {provided.placeholder}
                            </CustomWrap>
                          </ColumnContent>
                        </ColumnWrap>
                      )}
                    </Droppable>
                  )
              )}
          </DragDropContext>
        </ContentWrap>
      ) : settingKanban.column_configs ? (
        <>
          <Row style={{ width: "100%" }} justify={"center"}>
            <img alt="Empty" src={emptyEmail} />
          </Row>
          <Row style={{ width: "100%" }} justify={"center"}>
            <p> {t("common.noData")} </p>
          </Row>
          <Row justify="center">
            <Col>
              <ThemeButton
                label={"Thêm bản mẫu"}
                disabled={editingKey === "" && create ? false : true}
                clickButton={() => dispatch(setShowModal(true))}
              />
            </Col>
          </Row>
        </>
      ) : (
        <>
          <Row style={{ width: "100%" }} justify={"center"}>
            <img alt="Empty" src={emptyEmail} />
          </Row>
          <Row style={{ width: "100%" }} justify={"center"}>
            <p> {t("kanbanView.noConfig")} </p>
          </Row>
        </>
      )}
      <ModalRecord
        form={form}
        objectId={object_id}
        recordID={recordID}
        userDetail={userDetail}
        setRecordID={setRecordID}
        onFinish={submit}
        setEditingKey={setEditingKey}
        setDataConfirm={setDataConfirm}
        setShowConfirm={setShowConfirm}
      />

      <ModalConfirm
        title="Confirm"
        decs="Are you sure you want to proceed?"
        method={runDynamicButton}
        data={dataConfirm}
        img={ImgConfirm}
        showConfirm={showConfirm}
        setShowConfirm={setShowConfirm}
      />

      <CustomView
        visible={showCustomView}
        setVisible={setShowCustomView}
        objectId={object_id}
        mode={"kanban"}
      />

      <CustomModal
        visible={showModalConfirmEdit}
        onCancel={() => {
          setShowModalConfirmEdit(false);
        }}
        title="Edit record"
        footer={[
          <Button
            onClick={() => {
              const { sInd, dInd, columns, draggableId, source, destination } =
                recordEdit;
              if (sInd !== dInd && dInd !== columns.length - 1) {
                const result = move(
                  columns[sInd].data,
                  columns[dInd].data,
                  source,
                  destination
                );
                // eslint-disable-next-line
                Object.keys(result).forEach((key) => {
                  dispatch(
                    updateColumn({
                      index: key - 1,
                      data: result[key],
                    })
                  );
                });
                dispatch(
                  updateRecordKanbanOnDrag({
                    record: {
                      object_id: object_id,
                      id: draggableId,
                    },
                    column_field: column_field,
                    value_field: columns[dInd].value,
                    // current_page: current_page,
                    first_record_id: argument.first,
                    last_record_id: argument.last,
                  })
                );
              }
              setShowModalConfirmEdit(false);
            }}
          >
            Yes
          </Button>,
          <Button
            onClick={() => {
              setShowModalConfirmEdit(false);
            }}
          >
            No
          </Button>,
        ]}
      >
        <p> {t("kanbanView.confirmUpdate")} ? </p>
      </CustomModal>

      <ModalDuplicate />
    </KanbanWrap>
  );
};

export default withTranslation()(KanbanView);

const CustomHeader = styled.div`
  /* max-width: max-content; */
  /* width: max-content; */
  min-width: 100%;
  /* overflow-x: scroll; */
  .ant-btn:active {
    border: none;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    text-align: center;

    /* Character/Color text main */

    color: #2c2c2c;
    background-color: #fff;
  }

  .btn {
    position: relative;
    /* transition: all 200ms cubic-bezier(0.77, 0, 0.175, 1); */
    transition: all 0.5s;
    cursor: pointer;
    border-radius: 2px;
  }

  .btn:before,
  .btn:after {
    content: "";
    position: absolute;
    /* transition: inherit; */
    z-index: -1;
  }

  /* .btn:hover {
    color: #ffffff;
    transition-delay: 0.5s;
  }

  .btn:hover:before {
    transition-delay: 0s;
  }

  .btn:hover:after {
    background: ${(props) => props.theme.main};
    transition-delay: 0.35s;
  } */

  /* From Top */

  .btn-top:before,
  .btn-top:after {
    left: 0;
    height: 0;
    width: 100%;
  }

  .btn-top:before {
    bottom: 0;
    /* border: 1px solid ${(props) => props.theme.main}; */
    border-top: 0;
    border-bottom: 0;
  }

  .btn-top:after {
    top: 0;
    height: 0;
  }

  .btn-top:hover:before,
  .btn-top:hover:after {
    height: 100%;
  }

  .ant-btn:active {
    color: #fff;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
  .ant-btn:focus {
    color: #fff !important;
    background-color: ${(props) => props.theme.darker}!important;
    border-color: ${(props) => props.theme.darker}!important;
  }
`;

const CustimMenuWrapper = styled(Menu)`
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
    border-right: 2px solid ${(props) => props.theme.main};
  }
  li {
    padding-top: 8px;
    padding-bottom: 8px;
  }
`;

const CustomSubMenuWrapper = styled(SubMenu)`
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
  }

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
  }
`;

const CustomMenuItem = styled.div`
  cursor: pointer;
  :hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
    border-right: 2px solid ${(props) => props.theme.main};

    .pin-img {
      display: block;
      width: 16px;
      margin-right: 8px;
    }
  }
  padding: 8px 8px 8px 12px;
`;

const CustomOption = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  .pin-img {
    display: none;
  }
  .unpin-img {
    width: 18px;
    margin-right: 8px;
  }
`;

const CustomMenuHeader = styled.div`
  background-color: #fff;
  min-height: 64px;
  height: max-content;
  -webkit-animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  animation: slide-in-top 0.5s cubic-bezier(0.25, 0.46, 0.45, 0.94) both;
  width: 100%;
  display: flex;
  align-items: center;
  padding-left: 24px;
  .active {
    background-color: ${(props) => props.theme.main};
    color: #fff;
  }
  @-webkit-keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
  @keyframes slide-in-top {
    0% {
      -webkit-transform: translateY(-1000px);
      transform: translateY(-50px);
      opacity: 0;
    }
    100% {
      -webkit-transform: translateY(0);
      transform: translateY(0);
      opacity: 1;
    }
  }
`;

const MenuItem = styled.div`
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #2c2c2c;
  padding: 5px 16px;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  margin-right: 8px;
  /* width: 120px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden; */

  :hover {
    /* background-color: ${(props) => props.theme.darker}; */
    border: 1px solid ${(props) => props.theme.darker};

    border-radius: 2px;
    cursor: pointer;
  }
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  /* identical to box height, or 137% */

  text-align: center;

  /* Character/Color text main */
  color: #2c2c2c;
  margin-right: 15px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 5px 16px;
  /* :hover {
    color: ${(props) => props.theme.main};
  } */
  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.main};
  }
`;

const HeaderContent = styled.div`
  width: 100%;
  display: flex;
  /* justify-content: space-between; */
  justify-content: end;
  margin-top: 16px;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }

  .ant-pagination-prev:hover .ant-pagination-item-link,
  .ant-pagination-next:hover .ant-pagination-item-link {
    border-color: ${(props) => props.theme.main};
    color: ${(props) => props.theme.main};
  }

  .ant-select-focused,
  .ant-select:not(.ant-select-disabled):hover .ant-select-selector,
  .ant-input:hover,
  .ant-pagination-options-quick-jumper input:hover {
    border-color: ${(props) => props.theme.main} !important;
  }

  .ant-select-focused:not(.ant-select-disabled).ant-select:not(.ant-select-customize-input)
    .ant-select-selector,
  .ant-pagination-options-quick-jumper input:focus,
  .ant-pagination-options-quick-jumper input-focuse {
    box-shadow: none !important;
    border-color: ${(props) => props.theme.main};
  }
`;
const CustomWrapperAction = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-right: 16px;
`;

const CustomButtonAddRecord = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  font-size: 16px;
  .ant-btn:hover {
    border-color: ${(props) => props.theme.main};
    color: ${(props) => props.theme.main};
  }
  img {
    width: 15px;
    margin-right: 8px;
  }
  &:hover,
  &:active,
  &:focus {
    background-color: ${(props) => props.theme.darker} !important;
    border-color: ${(props) => props.theme.darker};
    color: #fff !important;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;

const ContentWrap = styled.div`
  width: 100%;
  overflow: hidden;
  height: calc(100% - 30px);
  overflow-x: auto;
  padding: 24px 32px 0 32px;
  margin-bottom: 10px;
  display: -webkit-box;

  .ant-pagination {
    position: fixed;
  }

  &::-webkit-scrollbar {
    height: 10px !important;
  }

  &::-webkit-scrollbar-thumb {
    background: #d9d9d9;
  }
`;

const ColumnWrap = styled.div`
  height: fit-content;
  box-shadow: 8px 5px 8px rgba(0, 0, 0, 0.14);
  border-radius: 10px;
  width: 370px;
  margin-right: 24px;
  /* 
  @media screen and (max-width: 1630px) {
    width: 25%;
  }
  @media screen and (max-width: 1340px) {
    width: 30%;
  } */
`;

const ColumnTitle = styled.div`
  background-color: ${(props) => props.color};
  font-family: var(--roboto-700);
  font-size: 16px;
  color: #ffffff;
  line-height: 22px;
  padding: 12px 8px;
  border-radius: 10px 10px 0px 0px;
`;

const ColumnContent = styled.div`
  position: relative;
  background-color: ${(props) => props.color};
  border-radius: 0 0 10px 10px;
  max-height: calc(100vh - 270px);
  overflow-y: auto;
  overflow-x: hidden;
  padding: 16px 16px 0px 16px;
`;
const CustomWrap = styled(Col)``;

const KanbanWrap = styled.div`
  height: calc(100vh - 130px);
`;

const CarouselWrap = styled.div`
  width: calc(100% - 260px);
  .react-multiple-carousel__arrow--right {
    right: 0;
    background: linear-gradient(
      268deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 90.08%
    );
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow--left {
    background: linear-gradient(
      90deg,
      #ffffff 0%,
      rgba(255, 255, 255, 0) 87.93%
    );
    left: 0;
    width: 90px;
    height: 32px;
    border-radius: unset;
  }
  .react-multiple-carousel__arrow::before {
    color: ${(props) => props.theme.main};
    font-size: 20px;
  }
  .react-multiple-carousel__arrow--left::before {
    margin-left: -50px;
  }
  .react-multiple-carousel__arrow--right::before {
    margin-right: -50px;
  }
`;
const CustomNewButton = styled(Button)`
  border: 1px solid #d9d9d9 !important;
  height: 34px;
  :hover {
    color: #fff;
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main} !important;
  }
`;
const CustomDisplay = styled.div`
  margin-left: 10px;
  padding-top: 6px;
  font-size: 16px;
  line-height: 22px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
  max-width: 150px;
  span {
    margin-left: 6px;
    text-decoration: underline;
    color: ${(props) => props.theme.main};
    cursor: pointer;
  }
`;
const CustomClose = styled.div`
  display: flex;
  justify-content: center;
  img {
    cursor: pointer;
  }
`;

const CustomItem = styled(Menu.Item)`
  .ant-dropdown-menu-title-content {
    display: flex;
    justify-content: space-between;
  }

  img {
    width: 19px;
  }
`;

const ItemText = styled.span`
  font-size: 16px;
  line-height: 22px;
  margin-right: 30px;
  margin-left: 8px;
`;

const ViewWrap = styled.div`
  width: 140px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 8px;
  background: #ffffff;
  border: 1px solid #d9d9d9;
  box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.016);
  border-radius: 2px;
  cursor: pointer;
`;

const OptionView = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Text = styled.div`
  margin-left: 6px;
  font-size: 16px;
  width: 85px;
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
`;
const PaginationWrap = styled.div`
  display: flex;
  color: #637381;
  font-size: 16px;
  align-items: center;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const InfoPagi = styled.div`
  padding-top: 3px;
  display: flex;
  align-items: center;
`;

const ReloadWrap = styled.div`
  margin-right: 8px;
  display: flex;
  width: 30px;
  height: 30px;

  border-radius: 50%;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  transition: all 0.5s;
  :hover {
    background: #e9e9e9;
  }
  margin-bottom: 3px;
  img {
    width: 15px;
  }
`;

const CustomButton = styled.div`
  display: flex;
`;

const ImgPrev = styled.div`
  display: flex;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  pointer-events: ${(props) => (props.current === 1 ? "none" : "")};
  cursor: ${(props) => (props.current === 1 ? "not-allowed" : "pointer")};
  opacity: ${(props) => (props.current === 1 ? 0.5 : 1)};
  transition: all 0.5s;
  :hover {
    background: #e9e9e9;
  }
  img {
    height: 15px;
  }
`;

const ImgNext = styled.div`
  display: flex;
  width: 30px;
  height: 30px;
  border-radius: 50%;
  align-items: center;
  justify-content: center;
  pointer-events: ${(props) => (props.totalRecord < 50 ? "none" : "")};
  cursor: ${(props) => (props.totalRecord < 50 ? "not-allowed" : "pointer")};
  opacity: ${(props) => (props.totalRecord < 50 ? 0.5 : 1)};
  transition: all 0.5s;
  :hover {
    background: #e9e9e9;
  }
  img {
    height: 15px;
  }
`;

const Nodata = styled.div`
  padding: 16px;
  margin-bottom: 16px;
  height: 162px;
  background: #ffffff;
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;
