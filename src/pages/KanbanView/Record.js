import { Button, Col, Popover, Row, Typography } from "antd";
import styled from "styled-components";
import React, { useEffect, useState } from "react";
// import moreIcon from "../../assets/icons/kanban/more.svg";
import deleteIcon from "../../assets/icons/email/deleteIcon.svg";
import moment from "moment";
import editIcon from "../../assets/icons/email/editIcon.svg";
import { useDispatch, useSelector } from "redux/store";
import { getDataKanbanViewById, deleteRecord } from "redux/slices/kanbanView";
import ModalConfimDelete from "components/Modal/ModalConfirmDelete";
import { setShowModalConfirmDelete } from "redux/slices/global";
import { useTranslation } from "react-i18next";
import viewIcon from "../../assets/icons/email/viewRuleIcon.svg";
// import { useNavigate } from "react-router";
import { FE_URL } from "constants/constants";

const Record = (props) => {
  const { t } = useTranslation();
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  const {
    fields_order,
    data,
    status_field,
    status_style,
    display_fields,
    object_id,
    form,
    setRecordID,
    allFields,
    edit,
    colorShadow,
    listObjectField,
  } = props;
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  const [dataDelete, setDataDelete] = useState({});

  const [visible, setVisible] = useState(false);

  const { Text: TextComponent } = Typography;

  const { recordData } = useSelector((state) => state.objectsReducer);
  const ConvertFieldName = (listField, fieldName) => {
    let newField = "";
    let arrFull = [];
    if (listField.length > 0) {
      if (
        listField.length > 0 &&
        listField.filter((x) => x.main_object).length > 0
      ) {
        listField
          .filter((x) => x.main_object)[0]
          ["main_object"]["sections"].forEach((data) => {
            // eslint-disable-next-line
            data.fields.map((item) => {
              arrFull.push(item);
            });
          });
      }
    }
    if (fieldName === "created_date") {
      newField = "Create date";
    } else if (fieldName === "created_by") {
      newField = "Create by";
    } else if (fieldName === "modify_time") {
      newField = "Modify time";
    } else if (fieldName === "modify_by") {
      newField = "Modified by";
    } else if (fieldName === "owner") {
      newField = "Assign to";
    } else {
      newField = fieldName;
      arrFull.forEach((item) => {
        if (item.field_id === fieldName) {
          newField = item.name;
        }
      });
    }
    return newField;
  };
  const formatNumber = (field, data, listField) => {
    let decimal = 3;
    if (
      listField.length > 0 &&
      listField.filter((x) => x.main_object).length > 0
    ) {
      listField
        .filter((x) => x.main_object)[0]
        ["main_object"]["sections"].forEach((data) => {
          data.fields.forEach((item) => {
            if (item.ID === field) {
              if (item.decimal_separator || item.decimal_separator === 0) {
                decimal = item.decimal_separator;
              }
            }
          });
        });
    }
    if (data === 0) {
      return "0";
    } else {
      return data
        .toFixed(decimal)
        .toString()
        .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  };

  const renderFieldFile = (value) => {
    /* eslint-disable-next-line */
    const expression =
      /*eslint-disable-next-line*/
      /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
    const regex = new RegExp(expression);
    if (value) {
      /*eslint-disable-next-line*/
      return value.map((item, index) => {
        if (item !== null && item.match(regex)) {
          let fileNew = item.split("/");
          return (
            <a
              key={index}
              style={{ display: "block" }}
              href={item}
              target={"_blank"}
              rel="noreferrer"
            >
              {fileNew[fileNew.length - 1]}
            </a>
          );
        }
      });
    }
  };

  const content = () => {
    return (
      <>
        {edit && (
          <PopoverItem
            onClick={() => {
              setRecordID(typeof data == "string" ? data : data._id);
              dispatch(
                getDataKanbanViewById({
                  id: data._id,
                  object_id: object_id,
                })
              );
              setVisible(false);
            }}
          >
            <img alt="" src={editIcon} /> <span>{t("common.edit")}</span>
          </PopoverItem>
        )}

        <PopoverItem
          onClick={() => {
            deleteRecordSaga(data);
            setVisible(false);
          }}
        >
          <img alt="" src={deleteIcon} /> <span>{t("common.delete")}</span>
        </PopoverItem>
        <PopoverItem
          onClick={() => {
            window.open(FE_URL + `/consolidated-view/${object_id}/${data._id}`);
          }}
        >
          <img alt="" src={viewIcon} />
          <span> Consolidated view</span>
        </PopoverItem>
        <ModalConfimDelete
          title={t("kanbanView.deleteRecord")}
          decs={t("common.descriptionDelete")}
          methodDelete={deleteRecord}
          dataDelete={dataDelete}
          mask={false}
          isLoading={showLoadingScreen}
        />
      </>
    );
  };
  const deleteRecordSaga = (record) => {
    setDataDelete({
      id: record._id,
      object_id: object_id,
    });
    dispatch(setShowModalConfirmDelete(true));
  };

  useEffect(() => {
    if (Object.keys(recordData).length > 0) {
      let temp = {};
      // eslint-disable-next-line
      Object.keys(recordData).forEach((key) => {
        if (typeof recordData[key] == "object") {
          temp[key] = recordData[key].value;
        }
        if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "datetime-local" &&
          recordData[key].value !== null
        ) {
          temp[key] = recordData[key]
            ? moment(recordData[key].value, "YYYY-MM-DD HH:mm:ss", true)
            : null;
        } else if (
          allFields &&
          allFields[key] &&
          allFields[key].type === "date" &&
          recordData[key].value !== null
        ) {
          temp[key] = recordData[key].value
            ? moment(recordData[key].value, "YYYY-MM-DD", true)
            : null;
        }
      });
      form.setFieldsValue(temp);
      form.setFieldsValue({
        assignTo: recordData.owner,
      });
    }
    // eslint-disable-next-line
  }, [recordData]);

  return (
    <CustomRecord
      color={colorShadow}
      onDoubleClick={() => {
        setRecordID(typeof data == "string" ? data : data._id);
        dispatch(
          getDataKanbanViewById({
            id: data._id,
            object_id: object_id,
          })
        );
        setVisible(false);
      }}
    >
      <HeaderWrap>
        <CustomStatus
          color={
            data[status_field] &&
            status_style.filter((x) => x.value === data[status_field]).length >
              0 &&
            status_style.filter((x) => x.value === data[status_field])[0].style
              .color
          }
        >
          {data[status_field]}
        </CustomStatus>

        <Popover
          content={content}
          placement={"bottomRight"}
          trigger="click"
          visible={visible}
          onVisibleChange={(e) => setVisible(e)}
        >
          <CustomButton>
            {/* <img alt="" src={moreIcon} />
             */}
            <span>...</span>
          </CustomButton>
        </Popover>
      </HeaderWrap>

      {fields_order &&
        fields_order.map((item) => {
          return (
            <Row>
              <CustomField span={24}>
                <CusTomSpan
                  // style={{
                  //   fontWeight: display_fields
                  //     ? display_fields[item]?.font_weight
                  //     : 400,
                  // }}
                  styleConfig={
                    display_fields ? display_fields[item]?.font_weight : "400"
                  }
                >
                  {ConvertFieldName(listObjectField, item)}:{" "}
                  {typeof data[item] === "number" ? (
                    formatNumber(item, data[item], listObjectField)
                  ) : typeof data[item] === "object" ? (
                    <TextComponent ellipsis={{ tooltip: data[item] }}>
                      {renderFieldFile(data[item])}
                    </TextComponent>
                  ) : (
                    data[item]
                  )}
                </CusTomSpan>
              </CustomField>
            </Row>
          );
        })}
    </CustomRecord>
  );
};

export default Record;

const CustomRecord = styled.div`
  /* border: 1px solid #ececec;
  border-radius: 5px;
  margin-top: 0.5rem;
  padding: 0.5rem;
  background-color: white; */

  border-radius: 5px;
  background-color: #ffffff;
  margin-bottom: 16px;
  padding: 16px;
  transition: all 0.5s;

  &:hover {
    box-shadow: inset 3px 0px 0px ${(props) => props.color};
  }
`;

const CustomStatus = styled.p`
  /* width: fit-content;
  padding: 0.2rem 0.4rem;
  color: white;
  font-size: 10px;
  border-radius: 5px;
  background-color: ${(props) => props.color}; */
  width: fit-content;
  padding: 2px 8px;
  color: white;
  font-size: 12px;
  border-radius: 2px;
  font-family: var(--roboto-500);
  background-color: ${(props) => props.color};
`;

const CustomField = styled(Col)`
  span {
    font-size: 16px;
    color: #595959;
  }
`;
const CusTomSpan = styled.span`
  font-family: ${(props) =>
    props.styleConfig === "600"
      ? `var(--roboto-500)`
      : props.styleConfig === "300"
      ? `var(--roboto-400-i)`
      : `var(--roboto-400)`};
`;
const HeaderWrap = styled.div`
  display: flex;
  justify-content: space-between;
  .ant-popover-inner-content {
  }
`;

const CustomButton = styled(Button)`
  border-color: #d9d9d9;
  width: 24px;
  height: 24px;
  &:hover,
  &:focus {
    border-color: #d9d9d9;
  }
  display: flex;
  align-items: center;
  justify-content: center;
  padding-bottom: 13px;
  border-radius: 2px;
  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;

const PopoverItem = styled.div`
  margin-bottom: 2px;
  cursor: pointer;

  img {
    margin-bottom: 4px;
  }

  span {
    font-size: 16px;
    color: #2c2c2c;
  }
`;
