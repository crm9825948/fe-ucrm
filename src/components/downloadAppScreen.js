import React from "react";
import styled from "styled-components";
import { Row, Col } from "antd";
import crmLogo from "assets/icons/common/crmlogo.png";
import appStore from "assets/icons/common/appstore.png";
import chPlay from "assets/icons/common/googleplay.png";
import mobile from "assets/icons/common/App download 3.png";

const DownloadAppScreen = () => {
  return (
    <Wrapper>
      <Row style={{ height: "100vh" }}>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <ColLeft>
            <img
              alt=""
              src={crmLogo}
              style={{
                width: "187px",
                marginLeft: "-10px",
                marginBottom: "32px",
              }}
            />
            <div className="title">
              Brings your team together wherever you are
            </div>
            <div className="decs">
              You can download on Google Play or App Store
            </div>
            <div>
              <img
                alt=""
                src={chPlay}
                style={{
                  width: "180px",
                  marginRight: "16px",
                  marginBottom: "16px",
                }}
              />
              <img
                alt=""
                src={appStore}
                style={{ width: "180px", marginBottom: "16px" }}
              />
            </div>
          </ColLeft>
        </Col>
        <Col xs={24} sm={24} md={12} lg={12} xl={12}>
          <ColRight>
            <img alt="" src={mobile} />
          </ColRight>
        </Col>
      </Row>
    </Wrapper>
  );
};

export default DownloadAppScreen;

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  background-color: white;

  .title {
    font-style: normal;
    font-family: var(--roboto-500);
    font-size: 40px;
    line-height: 48px;
    /* or 120% */

    /* text */

    color: #252424;
    margin-bottom: 16px;
  }
  .decs {
    font-family: var(--roboto-400);
    font-size: 14px;
    line-height: 20px;
    /* identical to box height, or 143% */

    /* Neutral / 7 */

    color: #8c8c8c;
    margin-bottom: 64px;
  }
`;

const ColLeft = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding-left: 10%;
  img {
    :hover {
      cursor: pointer;
    }
  }
`;

const ColRight = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  img {
    width: 100%;
  }
`;
