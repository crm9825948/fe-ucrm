import { Input, Select } from "antd";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { loadData } from "redux/slices/objects";
import { useDispatch } from "redux/store";
const { Option } = Select;
const DynamicInput = (props) => {
  const dispatch = useDispatch();
  const {
    type,
    value,
    changeEvent,
    name,
    index,
    dataSource,
    labelKey,
    valueKey,
    item,
    data,
    mode,
    disabled,
  } = props;
  const { listUserName } = useSelector((state) => state.userReducer);
  const [listOption, setListOption] = useState([]);
  const [optionLinkingField, setOptionLinkingField] = useState([]);

  useEffect(() => {
    if (type === "user") {
      let arr = [];
      Object.keys(listUserName).forEach((el) => {
        arr.push({
          label: listUserName[el],
          value: el,
        });
      });
      setListOption(arr);
    } else if (
      item &&
      mode !== "normal" &&
      (item.type === "linkingobject" || item.type === "lookup")
    ) {
      dispatch(
        loadData({
          object_id: item.related_object_id,
          current_page: 1,
          record_per_page: 10,
          search_with: {
            meta: [],
            data: "",
          },
        })
      );
    } else {
      setListOption(dataSource);
    }
  }, [dataSource, dispatch, item, listUserName, mode, type]);

  useEffect(() => {
    if (data) {
      let arr = [];
      data.forEach((el) => {
        if (el[item.field_linking]) {
          let temp = {
            label: el[item.field_linking].value,
            value: el._id,
          };
          arr.push(temp);
        }
      });
      setOptionLinkingField(arr);
    }
    // eslint-disable-next-line
  }, [data]);
  switch (type) {
    case "select":
    case "user":
      return (
        <Select
          style={{ width: "100%" }}
          value={value}
          disabled={disabled}
          onChange={(e) => changeEvent && changeEvent(e, name, index)}
          {...props}
        >
          {listOption &&
            listOption.map((item, index) => {
              return <Option value={item[valueKey]}>{item[labelKey]}</Option>;
            })}
        </Select>
      );
    case "file":
      return <></>;
    case "linkingobject":
    case "lookup":
      return (
        <Select
          style={{ width: "100%" }}
          value={value}
          disabled={disabled}
          onChange={(e) =>
            changeEvent && changeEvent(e, "default_value", index)
          }
          {...props}
        >
          {optionLinkingField &&
            optionLinkingField.map((item, index) => {
              return <Option value={item.value}>{item.label}</Option>;
            })}
        </Select>
      );
    case "textarea":
    case "text":
    case "email":
    default:
      return (
        <Input
          value={value}
          onChange={(e) => changeEvent(e.target.value, name, index)}
          {...props}
        />
      );
  }
};

export default DynamicInput;
