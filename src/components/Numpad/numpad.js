import styled from "styled-components/macro";
import { useState } from "react";
import { useStopwatch } from "react-timer-hook";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Draggable from "react-draggable";
import _ from "lodash";
import { useTranslation, withTranslation } from "react-i18next";
import ModalConfirm from "components/Modal/ModalConfirmStateIn";

import "./numpad.scss";
import Spin from "antd/lib/spin";
import Popover from "antd/lib/popover";
import Typography from "antd/lib/typography";
import Menu from "antd/lib/menu";
import Dropdown from "antd/lib/dropdown";
import Input from "antd/lib/input";
import Button from "antd/lib/button";
import Tooltip from "antd/lib/tooltip";
import Table from "antd/lib/table";

import ImgConfirm from "assets/icons/common/confirm.png";
import PhoneReady from "assets/icons/numpad/phone_ready.svg";
import PhoneNotReady from "assets/icons/numpad/phone_not_ready.svg";
import IconDown from "assets/icons/common/icon-down.svg";
import Exit from "assets/icons/numpad/exit.svg";
import FullScreenExit from "assets/icons/numpad/full_screen_exit.svg";
import DeleteNumber from "assets/icons/numpad/delete_input.svg";
import IconMakeCall from "assets/icons/numpad/make_call.svg";
import IconAvatar from "assets/icons/numpad/avatar_CTI.svg";
import DeclineCall from "assets/icons/numpad/decline_call.svg";
import AnswerCall from "assets/icons/numpad/answer_call.svg";
import Hold from "assets/icons/numpad/hold.svg";
import Transfer from "assets/icons/numpad/transfer.svg";
import Conference from "assets/icons/numpad/conference.svg";
import Consult from "assets/icons/numpad/consult.svg";
import Numpad from "assets/icons/numpad/numpad.svg";
import TransferAction from "assets/icons/numpad/transfer_action.svg";
import FullScreen from "assets/icons/numpad/full_screen.svg";
import ConferenceAction from "assets/icons/numpad/conference_action.svg";
import DisconnectExt from "assets/icons/numpad/disconnect.svg";
import { Notification } from "components/Notification/Noti";
import { FE_URL } from "constants/constants";
import VoiceBiometric from "components/VoiceBiometric/voiceBiometric";

import {
  updateStatusCall,
  updateScreen,
  loadlistStatus,
  loadListReasonCode,
  changeAgentStatus,
  makeCall,
  endCall,
  holdCall,
  unHoldCall,
  loadAgentsOnline,
  transferCall,
  conferenceCall,
  consultCall,
  answerCall,
  updateInteraction,
  updateHold,
  connectExt,
  setAlreadyUseExt,
  disconnectExt,
} from "redux/slices/callCenter";

import { callEvent } from "redux/slices/voiceBiometric";

function PopupNumpad({ dataWebSocket, userDetail }) {
  let dialogID = localStorage.getItem("dialogID");
  let hotLine = localStorage.getItem("hotline");
  let typeCall = localStorage.getItem("typeCall");
  let inCall = localStorage.getItem("inCall");
  let phoneNumber = localStorage.getItem("phoneNumber");
  let statusInCall = localStorage.getItem("statusInCall");
  let screenIncomingCall = localStorage.getItem("incomingCall");
  let screenCall = localStorage.getItem("screenCall");
  let callTime = localStorage.getItem("call_time");

  let inPopupVoice = localStorage.getItem("popup-voice");

  const dispatch = useDispatch();
  const {
    isLoading,
    listStatus,
    initialStatus,
    statusCall,
    screen,
    isHold,
    listAgentsOnline,
    alreadyUseExt,
  } = useSelector((state) => state.callCenterReducer);

  const { Text } = Typography;
  const { Column } = Table;
  const { t } = useTranslation();
  const offsetTimestamp = new Date();
  const { seconds, minutes, hours, pause, reset } = useStopwatch({
    autoStart: true,
    offsetTimestamp: localStorage.getItem("call_time")
      ? offsetTimestamp.setSeconds(
          offsetTimestamp.getSeconds() +
            parseInt(
              (offsetTimestamp.getTime() -
                new Date(localStorage.getItem("call_time")).getTime()) /
                1000
            )
        )
      : offsetTimestamp.setSeconds(offsetTimestamp.getSeconds() + 0),
  });

  const [popupVoice, setPopupVoice] = useState(false);
  const [showVoice, setShowVoice] = useState(false);

  const [isShowConnect, $isShowConnect] = useState(false);
  const [isShowNumpad, setIsShowNumpad] = useState(false);
  const [isShowMenuStatus, setIsShowStatus] = useState(false);
  const [isShowNumpadIncall, setIsShowNumpadInCall] = useState(false);
  const [isMini, setIsMini] = useState(false);
  const [isShowMenuTransfer, setIsShowMenuTransfer] = useState(false);
  const [isShowMenuConference, setIsShowMenuConference] = useState(false);
  const [isShowMenuConsult, setIsShowMenuConsult] = useState(false);
  const [showConfirmDisconnect, $showConfirmDisconnect] = useState(false);

  const [status, setStatus] = useState("");
  const [valueInput, setValueInput] = useState("");
  const [consultList, setConsultList] = useState("");
  const [activeDrags, setActiveDrags] = useState(0);
  const [valueExt, $valueExt] = useState("");

  const handleShowNumpad = () => {
    if (screenCall === "inCall" || screenIncomingCall === "true") {
      setIsShowNumpad(true);
    } else {
      setIsShowNumpad(!isShowNumpad);
    }
  };

  const handleShowConnect = () => {
    $isShowConnect(!isShowConnect);
  };

  const handleShowMenuStatus = () => {
    setIsShowStatus(!isShowMenuStatus);
  };

  const handleShowMenuTransfer = () => {
    setIsShowMenuTransfer(!isShowMenuTransfer);
  };

  const handleShowMenuConference = () => {
    setIsShowMenuConference(!isShowMenuConference);
  };

  const handleShowMenuConsult = () => {
    setIsShowMenuConsult(!isShowMenuConsult);
  };

  const handleChangeInput = (e) => {
    setValueInput(e.target.value);
  };

  const hanldeChangeStatus = (reasonCode) => {
    dispatch(
      changeAgentStatus({
        agentStatus: reasonCode,
      })
    );
    handleShowMenuStatus();
  };

  const onDeleteInput = () => {
    let temp = valueInput;
    temp = temp.slice(0, temp.length - 1);
    setValueInput(temp);
  };

  const onClickNumber = (num) => {
    let temp = valueInput + num;
    setValueInput(temp);
  };

  const _onMakeCall = () => {
    if (valueInput !== "") {
      dispatch(
        makeCall({
          phone: valueInput,
        })
      );
    } else {
      Notification("error", "Please input phone number!");
    }
  };

  const _onDeclineCall = () => {
    if (dialogID) {
      dispatch(
        endCall({
          dialogID: dialogID,
        })
      );
    } else {
      dispatch(updateScreen(""));
      setIsShowNumpad(false);
      setPopupVoice(false);
      localStorage.removeItem("dialogID");
      localStorage.removeItem("hotline");
      localStorage.removeItem("typeCall");
      localStorage.removeItem("inCall");
      localStorage.removeItem("statusInCall");
      localStorage.removeItem("phoneNumber");
      localStorage.removeItem("incomingCall");
      localStorage.removeItem("call_time");
      localStorage.removeItem("agent-callName");
      localStorage.removeItem("popup-voice");
      localStorage.removeItem("inforAgentCIF");
      localStorage.removeItem("inforAgentName");
      localStorage.removeItem("inforAgentChoice");
      localStorage.removeItem("statusRegister");
      localStorage.removeItem("statusAuthenManually");
      localStorage.removeItem("screenVoice");
      localStorage.removeItem("statusAuthenAuto");
    }
  };

  useEffect(() => {
    if (window) {
      window.onbeforeunload = () => {
        dispatch(updateScreen(""));
        setIsShowNumpad(false);
        setPopupVoice(false);
        localStorage.removeItem("dialogID");
        localStorage.removeItem("hotline");
        localStorage.removeItem("typeCall");
        localStorage.removeItem("inCall");
        localStorage.removeItem("statusInCall");
        localStorage.removeItem("phoneNumber");
        localStorage.removeItem("incomingCall");
        localStorage.removeItem("call_time");
        localStorage.removeItem("agent-callName");
        localStorage.removeItem("popup-voice");
        localStorage.removeItem("inforAgentCIF");
        localStorage.removeItem("inforAgentName");
        localStorage.removeItem("inforAgentChoice");
        localStorage.removeItem("statusRegister");
        localStorage.removeItem("statusAuthenManually");
        localStorage.removeItem("screenVoice");
        localStorage.removeItem("statusAuthenAuto");
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch]);

  const _onHoldCall = () => {
    if (isHold) {
      dispatch(
        unHoldCall({
          dialogID: dialogID,
        })
      );
    } else {
      dispatch(
        holdCall({
          dialogID: dialogID,
        })
      );
    }
  };

  const _onLoadAgentsOnline = () => {
    dispatch(
      loadAgentsOnline({
        dialogID: dialogID,
      })
    );
  };

  const _onTransferCall = (extension) => {
    dispatch(
      transferCall({
        transferAgent: extension,
        dialogID: dialogID,
      })
    );
    setIsShowMenuTransfer(false);
  };

  const _onConferenceCall = (extension) => {
    dispatch(
      conferenceCall({
        conferenceAgent: extension,
        dialogID: dialogID,
      })
    );
    setIsShowMenuConference(false);
  };

  const _onConsultCall = (record) => {
    let tempConsult = [...consultList];
    tempConsult.push(record);
    setConsultList(tempConsult);

    dispatch(
      consultCall({
        extensionDest: record.extension,
        dialogID: dialogID,
      })
    );
    setIsShowMenuConsult(false);
  };

  const _onAnswerCall = () => {
    dispatch(
      answerCall({
        dialogID: dialogID,
      })
    );
  };

  const _onDisconnectExt = () => {
    $valueExt("");
    $isShowConnect(false);
    $showConfirmDisconnect(true);
  };

  const menuStatus = (
    <MenuStatus>
      {_.map(listStatus, (item) => (
        <MenuStatusItem
          ready={item.reasonName === "Ready"}
          key={item.reasonCode}
          onClick={() => hanldeChangeStatus(item.reasonCode)}
        >
          <Text
            ellipsis={{ tooltip: item.reasonName }}
            style={{ width: "177px" }}
          >
            {item.reasonName}
          </Text>
        </MenuStatusItem>
      ))}

      {_.get(userDetail, "allow_dynamic_extension", false) && (
        <Menu.Item
          onClick={_onDisconnectExt}
          style={{
            background: "#ED2D2D",
            height: "36px",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            cursor: "pointer",
            marginTop: "16px",
          }}
        >
          <img src={DisconnectExt} alt="" style={{ marginRight: "8px" }} />
          <span style={{ color: "#fff", fontSize: "16px" }}>
            {t("common.disconnect")}
          </span>
        </Menu.Item>
      )}
    </MenuStatus>
  );

  const menuListAgents = (type) => {
    return (
      <MenuTransfer>
        <TitleTransfer>
          {type === "transfer"
            ? "Transfer a call to another number"
            : type === "conference"
            ? "Conference a call"
            : "Consult a call"}
        </TitleTransfer>
        <Table
          pagination={false}
          dataSource={
            typeCall === "1" && type === "conference"
              ? consultList
              : listAgentsOnline
          }
          scroll={{ x: "max-content" }}
        >
          <Column
            title="Agent"
            dataIndex="name"
            key="name"
            width="371px"
            render={(text) => (
              <Text ellipsis={{ tooltip: text }} style={{ width: "339px" }}>
                {text}
              </Text>
            )}
          />
          <Column
            title="Extension"
            dataIndex="extension"
            key="extension"
            width="163px"
            render={(text) => (
              <Text ellipsis={{ tooltip: text }} style={{ width: "131px" }}>
                {text}
              </Text>
            )}
          />
          <Column
            title=""
            dataIndex="action"
            key="action"
            width="72px"
            render={(text, record) => (
              <Tooltip title={type === "transfer" ? "Transfer" : "Add"}>
                <ActionTransfer
                  // onClick={() => _onTransferCall(record.extension)}
                  onClick={() => {
                    type === "transfer"
                      ? _onTransferCall(record.extension)
                      : type === "conference"
                      ? _onConferenceCall(record.extension)
                      : _onConsultCall(record);
                  }}
                >
                  <img
                    src={
                      type === "transfer" ? TransferAction : ConferenceAction
                    }
                    alt="transfer"
                  />
                </ActionTransfer>
              </Tooltip>
            )}
          />
        </Table>
      </MenuTransfer>
    );
  };

  const formatTime = (time) => {
    return String(time).padStart(2, "0");
  };

  const onStart = () => {
    let temp = activeDrags;
    temp = ++temp;
    setActiveDrags(temp);
  };

  const onStop = () => {
    let temp = activeDrags;
    temp = --temp;
    setActiveDrags(--temp);
  };

  const onChangeExt = (e) => {
    $valueExt(e.target.value);
  };

  const _onConnectExt = () => {
    if (valueExt) {
      dispatch(
        connectExt({
          ext: valueExt,
          force_connect: 0,
          userID: _.get(userDetail, "_id"),
        })
      );
      $isShowConnect(false);
    }
  };

  useEffect(() => {
    if (userDetail.use_cti) {
      dispatch(loadlistStatus());
      dispatch(loadListReasonCode({}));
    }
  }, [dispatch, userDetail]);

  useEffect(() => {
    setStatus(initialStatus);
  }, [initialStatus]);

  useEffect(() => {
    if (statusCall !== null) {
      localStorage.setItem("statusInCall", statusCall);
    }
  }, [statusCall]);

  useEffect(() => {
    if (screen !== null) {
      localStorage.setItem("screenCall", screen);
    }
  }, [screen]);

  useEffect(() => {
    if (Object.keys(dataWebSocket).length > 0) {
      switch (dataWebSocket.event) {
        case "check_customer_registered_offcall":
          if (dataWebSocket.data.full_name !== null) {
            localStorage.setItem(
              "inforAgentName",
              dataWebSocket.data.full_name
            );
          }
          if (dataWebSocket.data.cif !== null) {
            localStorage.setItem("inforAgentCIF", dataWebSocket.data.cif);
          }
          localStorage.setItem(
            "inforAgentChoice",
            dataWebSocket.data.last_choice
              ? dataWebSocket.data.last_choice
              : "Chưa có"
          );

          if (
            dataWebSocket.data.code === "M_Customer_Not_Found" ||
            dataWebSocket.data.code === "M_Customer_Not_Registered_Voice" ||
            dataWebSocket.data.code === "N_Customer_Not_Registered_Voice_01" ||
            dataWebSocket.data.code === "M_Customer_Decline_Registered_Voice"
          ) {
            localStorage.setItem("statusRegister", "Chưa đăng ký");
            localStorage.setItem("screenVoice", "notRegister");
          } else if (
            dataWebSocket.data.code === "N_Customer_Synthesizing_Voice"
          ) {
            localStorage.setItem("statusRegister", "Chờ kết quả");
            localStorage.setItem("screenVoice", "waiting");
            localStorage.setItem("statusAuthenAuto", "waiting");
          } else if (
            dataWebSocket.data.code === "N_Customer_Registered_Voice"
          ) {
            localStorage.setItem("statusRegister", "Đã đăng ký");
            localStorage.setItem("screenVoice", "alreadyRegister");
            localStorage.setItem("statusAuthenAuto", "waiting");
          } else {
            Notification("error", dataWebSocket.data.message);
          }
          break;

        case "Change status":
          setStatus(dataWebSocket.data.reasonName);

          if (dataWebSocket.data.reasonName === "Wrap-Up") {
            dispatch(updateStatusCall("Wrap-up"));
            reset();
          }

          break;

        case "Ended":
          pause();
          dispatch(updateStatusCall("Ended"));
          dispatch(updateHold(false));

          setTimeout(function () {
            reset();
            pause();
            setIsShowNumpad(false);
            setPopupVoice(false);
            dispatch(updateScreen(""));
            localStorage.removeItem("dialogID");
            localStorage.removeItem("hotline");
            localStorage.removeItem("typeCall");
            localStorage.removeItem("inCall");
            localStorage.removeItem("statusInCall");
            localStorage.removeItem("phoneNumber");
            localStorage.removeItem("incomingCall");
            localStorage.removeItem("call_time");
            localStorage.removeItem("agent-callName");
            localStorage.removeItem("popup-voice");
            localStorage.removeItem("inforAgentCIF");
            localStorage.removeItem("inforAgentName");
            localStorage.removeItem("inforAgentChoice");
            localStorage.removeItem("statusRegister");
            localStorage.removeItem("statusAuthenManually");
            localStorage.removeItem("screenVoice");
            localStorage.removeItem("statusAuthenAuto");
          }, 2000);

          break;

        case "Outgoing call":
        case "Incoming call":
          if (dataWebSocket.event === "Incoming call") {
            dispatch(updateScreen("incomingCall"));
            dispatch(updateStatusCall("Incoming call"));
            localStorage.setItem("incomingCall", "true");
          }

          if (dataWebSocket.event === "Outgoing call") {
            dispatch(updateStatusCall("Outgoing call"));
            localStorage.setItem("inCall", "true");
            dispatch(updateScreen("inCall"));
          }

          localStorage.setItem("agent-callName", dataWebSocket.data.username);
          localStorage.setItem("phoneNumber", dataWebSocket.data.phone);
          localStorage.setItem("dialogID", dataWebSocket.data.third_party_id);
          localStorage.setItem("hotline", dataWebSocket.data.hotline);
          localStorage.setItem("typeCall", dataWebSocket.data.type_call);

          if (dataWebSocket.data.data.length === 1) {
            localStorage.setItem(
              "dataCall",
              JSON.stringify(dataWebSocket.data)
            );
            localStorage.setItem("popup-voice", "true");

            if (userDetail?.use_voice_biometric) {
              dispatch(
                callEvent({
                  voice_biometric_event: "check_customer_registered_offcall",
                  phone_incoming: dataWebSocket.data.phone,
                  third_party_id: dataWebSocket.data.third_party_id,
                  record_id: dataWebSocket.data.data[0]._id,
                  object_id: dataWebSocket.data.object_id,
                  agent_name: dataWebSocket.data.username,
                })
              );
            }

            dispatch(
              updateInteraction({
                call_id: dataWebSocket.data.third_party_id,
                record_id: dataWebSocket.data.data[0]._id,
                object_id: dataWebSocket.data.object_id,
                extension: dataWebSocket.data.extension,
                run_workflow_interaction: false,
              })
            );

            if (dataWebSocket.current_session === dataWebSocket.last_session) {
              window.open(
                `${FE_URL}/consolidated-view/${dataWebSocket.data.object_id}/${dataWebSocket.data.data[0]._id}`
                // `/consolidated-view/${dataWebSocket.data.object_id}/${dataWebSocket.data.data[0]._id}`
              );
            }
          } else if (dataWebSocket.data.data.length > 1) {
            localStorage.setItem("popup-voice", "true");
            localStorage.setItem(
              "dataCall",
              JSON.stringify(dataWebSocket.data)
            );
            if (dataWebSocket.current_session === dataWebSocket.last_session) {
              window.open(`${FE_URL}/call-center/${dataWebSocket.data.phone}`);
            }
          } else {
            localStorage.setItem(
              "dataCall",
              JSON.stringify(dataWebSocket.data)
            );
            if (dataWebSocket.current_session === dataWebSocket.last_session) {
              window.open(`${FE_URL}/call-center/${dataWebSocket.data.phone}`);
            }
          }
          break;

        case "Answered":
          reset();
          dispatch(updateScreen("inCall"));
          dispatch(updateStatusCall("Talking"));
          localStorage.setItem("inCall", "true");

          if (
            _.get(dataWebSocket, "data.call_start_time", "") &&
            _.get(dataWebSocket, "data.call_start_time") !== null
          ) {
            localStorage.setItem(
              "call_time",
              _.get(dataWebSocket, "data.call_start_time")
            );
          } else {
            localStorage.setItem(
              "call_time",
              _.get(dataWebSocket, "call_start_time")
            );
          }

          localStorage.removeItem("incomingCall");
          break;

        case "Hold":
          dispatch(updateStatusCall("Hold"));
          dispatch(updateHold(true));
          break;

        case "Retrieve":
          dispatch(updateStatusCall("Talking"));
          dispatch(updateHold(false));
          break;

        default:
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataWebSocket, dispatch]);

  useEffect(() => {
    if (inCall === "true" || screenIncomingCall === "true") {
      setIsShowNumpad(true);
    }
  }, [dataWebSocket, dispatch, inCall, screenIncomingCall]);

  useEffect(() => {
    if (inPopupVoice === "true") {
      setPopupVoice(true);
    }
  }, [inPopupVoice]);

  return (
    <>
      {_.get(userDetail, "cti_extension", "") !== null ? (
        <Popover
          content={
            <Draggable onStart={onStart} onStop={onStop}>
              {isMini ? (
                <WrapperMini>
                  <AvatarMini>
                    <img src={IconAvatar} alt="avatar" />
                  </AvatarMini>
                  <DurationMini>
                    {Object.keys(dataWebSocket).length > 0 &&
                    callTime &&
                    callTime !== null ? (
                      <>
                        {hours > 0 && <>{formatTime(hours)}:</>}
                        {formatTime(minutes)}:{formatTime(seconds)}
                      </>
                    ) : (
                      <span>00:00</span>
                    )}
                  </DurationMini>

                  <ActionsMini>
                    <Tooltip title={isHold ? "Unhold" : "Hold"}>
                      <Action onClick={_onHoldCall} active={isHold}>
                        <img src={Hold} alt="hold" />
                      </Action>
                    </Tooltip>
                    <Dropdown
                      visible={isShowMenuTransfer}
                      onVisibleChange={handleShowMenuTransfer}
                      overlay={menuListAgents("transfer")}
                      placement="bottomCenter"
                      trigger={["click"]}
                    >
                      <Tooltip title="Transfer">
                        <Action
                          onClick={!isShowMenuTransfer && _onLoadAgentsOnline}
                          active={isShowMenuTransfer}
                        >
                          <img src={Transfer} alt="transfer" />
                        </Action>
                      </Tooltip>
                    </Dropdown>
                    <Dropdown
                      visible={isShowMenuConference}
                      onVisibleChange={handleShowMenuConference}
                      overlay={menuListAgents("conference")}
                      placement="bottomCenter"
                      trigger={["click"]}
                    >
                      <Tooltip title="Conference">
                        <Action
                          onClick={!isShowMenuConference && _onLoadAgentsOnline}
                          active={isShowMenuConference}
                        >
                          <img src={Conference} alt="conference" />
                        </Action>
                      </Tooltip>
                    </Dropdown>
                    <Dropdown
                      visible={isShowMenuConsult}
                      onVisibleChange={handleShowMenuConsult}
                      overlay={menuListAgents("consult")}
                      placement="bottomCenter"
                      trigger={["click"]}
                    >
                      <Tooltip title="Consult">
                        <Action
                          onClick={!isShowMenuConsult && _onLoadAgentsOnline}
                          active={isShowMenuConsult}
                        >
                          <img src={Consult} alt="consult" />
                        </Action>
                      </Tooltip>
                    </Dropdown>
                    <Tooltip title="Numpad">
                      <Action
                        onClick={() => {
                          if (isMini) {
                            setIsMini(false);
                          }

                          setIsShowNumpadInCall(!isShowNumpadIncall);
                        }}
                        active={isShowNumpadIncall}
                      >
                        <img src={Numpad} alt="numpad" />
                      </Action>
                    </Tooltip>
                  </ActionsMini>
                  <ButtonDeclineCallMini onClick={_onDeclineCall}>
                    {isLoading.loadingEndCall ? (
                      <Spin />
                    ) : (
                      <img src={DeclineCall} alt="declineCall" />
                    )}
                  </ButtonDeclineCallMini>
                  <WrapExit>
                    <img
                      onClick={() => setIsMini(false)}
                      src={FullScreen}
                      alt="mini"
                    />
                  </WrapExit>
                </WrapperMini>
              ) : (
                <Wrapper screen={screenCall}>
                  <Header>
                    <Dropdown
                      visible={isShowMenuStatus}
                      onVisibleChange={handleShowMenuStatus}
                      overlay={menuStatus}
                      trigger={"click"}
                      disabled={
                        screenCall === "inCall" || screenIncomingCall === "true"
                      }
                    >
                      <WrapStatusInPopup>
                        {status === "Ready" ? (
                          <img src={PhoneReady} alt="ready" />
                        ) : (
                          <img src={PhoneNotReady} alt="notReady" />
                        )}
                        <Infor>
                          <Extension status={status}>
                            <Text
                              ellipsis={{ tooltip: userDetail?.cti_extension }}
                              style={{ width: "65px" }}
                            >
                              {userDetail?.cti_extension}
                            </Text>
                          </Extension>
                          <StatusInPopup ready={status === "Ready"}>
                            <Text
                              ellipsis={{ tooltip: status }}
                              style={{ width: "60px" }}
                            >
                              {status}
                            </Text>
                          </StatusInPopup>
                        </Infor>
                        <img src={IconDown} alt="down" />
                      </WrapStatusInPopup>
                    </Dropdown>
                    <WrapExit>
                      {screenCall === "inCall" && (
                        <img
                          onClick={() => setIsMini(true)}
                          src={FullScreenExit}
                          alt="mini"
                        />
                      )}

                      {screenCall === "inCall" ||
                      screenIncomingCall === "true" ? (
                        <Tooltip title="Bạn ko thể đóng khi đang trong cuộc gọi.">
                          <img src={Exit} alt="exit" />
                        </Tooltip>
                      ) : (
                        <img
                          onClick={() => setIsShowNumpad(false)}
                          src={Exit}
                          alt="exit"
                        />
                      )}
                    </WrapExit>
                  </Header>
                  <Screen isShowMenuStatus={isShowMenuStatus}>
                    {screenCall === "inCall" ? (
                      <CallOut>
                        {isShowNumpadIncall ? (
                          <>
                            <WrapInput isShowNumpadIncall={isShowNumpadIncall}>
                              <Input
                                value={valueInput}
                                onChange={handleChangeInput}
                              />
                              <ButtonDeleteInput
                                onClick={() => onDeleteInput()}
                              >
                                <img src={DeleteNumber} alt="delete" />
                              </ButtonDeleteInput>
                            </WrapInput>
                            <WrapNumber>
                              <Row>
                                <span onClick={() => onClickNumber("1")}>
                                  1
                                </span>
                                <span onClick={() => onClickNumber("2")}>
                                  2
                                </span>
                                <span onClick={() => onClickNumber("3")}>
                                  3
                                </span>
                              </Row>
                              <Row>
                                <span onClick={() => onClickNumber("4")}>
                                  4
                                </span>
                                <span onClick={() => onClickNumber("5")}>
                                  5
                                </span>
                                <span onClick={() => onClickNumber("6")}>
                                  6
                                </span>
                              </Row>
                              <Row>
                                <span onClick={() => onClickNumber("7")}>
                                  7
                                </span>
                                <span onClick={() => onClickNumber("8")}>
                                  8
                                </span>
                                <span onClick={() => onClickNumber("9")}>
                                  9
                                </span>
                              </Row>
                              <Row>
                                <span onClick={() => onClickNumber("*")}>
                                  *
                                </span>
                                <span onClick={() => onClickNumber("0")}>
                                  0
                                </span>
                                <span onClick={() => onClickNumber("#")}>
                                  #
                                </span>
                              </Row>
                            </WrapNumber>
                          </>
                        ) : (
                          <>
                            <Avatar>
                              <img src={IconAvatar} alt="avatar" />
                            </Avatar>

                            <PhoneNumber>{phoneNumber}</PhoneNumber>

                            <StatusCall>{statusInCall}</StatusCall>

                            <Duration>
                              {Object.keys(dataWebSocket).length > 0 &&
                              callTime &&
                              callTime !== null ? (
                                <>
                                  {hours > 0 && <>{formatTime(hours)}:</>}
                                  {formatTime(minutes)}:{formatTime(seconds)}
                                </>
                              ) : (
                                <span>00:00</span>
                              )}
                            </Duration>

                            <ButtonDeclineCall onClick={_onDeclineCall}>
                              {isLoading.loadingEndCall ? (
                                <Spin />
                              ) : (
                                <img src={DeclineCall} alt="declineCall" />
                              )}
                            </ButtonDeclineCall>
                          </>
                        )}
                        <Hotline>
                          <span>Hotline: &nbsp;</span>

                          {hotLine !== null && <span>{hotLine}</span>}
                        </Hotline>

                        <Actions>
                          <Tooltip title={isHold ? "Unhold" : "Hold"}>
                            <Action onClick={_onHoldCall} active={isHold}>
                              <img src={Hold} alt="hold" />
                            </Action>
                          </Tooltip>
                          <Dropdown
                            visible={isShowMenuTransfer}
                            onVisibleChange={handleShowMenuTransfer}
                            overlay={menuListAgents("transfer")}
                            placement="bottomCenter"
                            trigger={["click"]}
                          >
                            <Tooltip title="Transfer">
                              <Action
                                onClick={
                                  !isShowMenuTransfer && _onLoadAgentsOnline
                                }
                                active={isShowMenuTransfer}
                              >
                                <img src={Transfer} alt="transfer" />
                              </Action>
                            </Tooltip>
                          </Dropdown>
                          <Dropdown
                            visible={isShowMenuConference}
                            onVisibleChange={handleShowMenuConference}
                            overlay={menuListAgents("conference")}
                            placement="bottomCenter"
                            trigger={["click"]}
                          >
                            <Tooltip title="Conference">
                              <Action
                                onClick={
                                  !isShowMenuConference && _onLoadAgentsOnline
                                }
                                active={isShowMenuConference}
                              >
                                <img src={Conference} alt="conference" />
                              </Action>
                            </Tooltip>
                          </Dropdown>
                          <Dropdown
                            visible={isShowMenuConsult}
                            onVisibleChange={handleShowMenuConsult}
                            overlay={menuListAgents("consult")}
                            placement="bottomCenter"
                            trigger={["click"]}
                          >
                            <Tooltip title="Consult">
                              <Action
                                onClick={
                                  !isShowMenuConsult && _onLoadAgentsOnline
                                }
                                active={isShowMenuConsult}
                              >
                                <img src={Consult} alt="consult" />
                              </Action>
                            </Tooltip>
                          </Dropdown>
                          <Tooltip title="Numpad">
                            <Action
                              onClick={() =>
                                setIsShowNumpadInCall(!isShowNumpadIncall)
                              }
                              active={isShowNumpadIncall}
                            >
                              <img src={Numpad} alt="numpad" />
                            </Action>
                          </Tooltip>
                        </Actions>
                      </CallOut>
                    ) : screenCall === "incomingCall" ? (
                      <IncomingCall>
                        <Avatar>
                          <img src={IconAvatar} alt="avatar" />
                        </Avatar>

                        <PhoneNumber>{phoneNumber}</PhoneNumber>

                        <StatusCall>{statusInCall}</StatusCall>

                        <Actions screen={screenCall}>
                          {/* <ButtonDeclineCall onClick={_onDeclineCall}>
                        {isLoading.loadingEndCall ? (
                          <Spin />
                        ) : (
                          <img src={DeclineCall} alt="declineCall" />
                        )}
                      </ButtonDeclineCall> */}
                          <ButtonAnswerCall onClick={_onAnswerCall}>
                            {isLoading.loadingAnswerCall ? (
                              <Spin />
                            ) : (
                              <img src={AnswerCall} alt="answerCall" />
                            )}
                          </ButtonAnswerCall>
                        </Actions>
                      </IncomingCall>
                    ) : (
                      <KeyBoard>
                        <WrapInput>
                          <Input
                            value={valueInput}
                            onChange={handleChangeInput}
                          />
                          <ButtonDeleteInput onClick={() => onDeleteInput()}>
                            <img src={DeleteNumber} alt="delete" />
                          </ButtonDeleteInput>
                        </WrapInput>
                        <WrapNumber>
                          <Row>
                            <span onClick={() => onClickNumber("1")}>1</span>
                            <span onClick={() => onClickNumber("2")}>2</span>
                            <span onClick={() => onClickNumber("3")}>3</span>
                          </Row>
                          <Row>
                            <span onClick={() => onClickNumber("4")}>4</span>
                            <span onClick={() => onClickNumber("5")}>5</span>
                            <span onClick={() => onClickNumber("6")}>6</span>
                          </Row>
                          <Row>
                            <span onClick={() => onClickNumber("7")}>7</span>
                            <span onClick={() => onClickNumber("8")}>8</span>
                            <span onClick={() => onClickNumber("9")}>9</span>
                          </Row>
                          <Row>
                            <span onClick={() => onClickNumber("*")}>*</span>
                            <span onClick={() => onClickNumber("0")}>0</span>
                            <span onClick={() => onClickNumber("#")}>#</span>
                          </Row>
                        </WrapNumber>

                        <ButtonMakeCall
                          isShowMenuStatus={isShowMenuStatus}
                          onClick={() => _onMakeCall()}
                          disabled={
                            userDetail.onlyCallOutWhenReady === false &&
                            status === "Ready"
                          }
                        >
                          {isLoading.loadingMakeCall ? (
                            <Spin />
                          ) : (
                            <img src={IconMakeCall} alt="makeCall" />
                          )}
                        </ButtonMakeCall>
                      </KeyBoard>
                    )}
                  </Screen>
                </Wrapper>
              )}
            </Draggable>
          }
          trigger="click"
          visible={isShowNumpad}
          onVisibleChange={handleShowNumpad}
          overlayClassName="customNumpad"
        >
          <WrapStatus>
            {userDetail.use_voice_biometric && inCall && popupVoice && (
              <VoiceBiometric
                showVoice={showVoice}
                setShowVoice={setShowVoice}
                inCall={inCall}
                dataWebSocket={dataWebSocket}
              />
            )}

            {status === "Ready" ? (
              <img src={PhoneReady} alt="ready" />
            ) : (
              <img src={PhoneNotReady} alt="notReady" />
            )}
            <Infor>
              <Extension status={status}>
                <Text
                  ellipsis={{ tooltip: userDetail?.cti_extension }}
                  style={{ width: "65px" }}
                >
                  {userDetail?.cti_extension}
                </Text>
              </Extension>
              {status && (
                <Status ready={status === "Ready"}>
                  <Text>{status}</Text>
                </Status>
              )}
            </Infor>
            <img src={IconDown} alt="down" />
          </WrapStatus>
        </Popover>
      ) : (
        <>
          <Popover
            title={
              <span
                style={{
                  color: "#6B6B6B",
                  fontSize: "16px",
                  fontFamily: "var(--roboto-700)",
                }}
              >
                {t("common.connectCall")}
              </span>
            }
            content={
              <div style={{ display: "flex", flexDirection: "column" }}>
                <span
                  style={{ fontSize: "16px", fontFamily: "var(--roboto-500)" }}
                >
                  {t("common.inputExtension")}
                </span>
                <Input
                  style={{
                    width: "216px",
                    height: "32px",
                    marginTop: "8px",
                    marginBottom: "16px",
                  }}
                  value={valueExt}
                  onChange={onChangeExt}
                  onPressEnter={_onConnectExt}
                />
                <ConnectButton
                  style={{
                    width: "216px",
                    height: "32px",
                    padding: "0 15px",
                    fontSize: "16px",
                  }}
                  onClick={_onConnectExt}
                >
                  {t("common.connect")}
                </ConnectButton>
              </div>
            }
            trigger="click"
            visible={isShowConnect}
            onVisibleChange={handleShowConnect}
          >
            <ConnectButton>
              <img src={PhoneReady} alt="" style={{ marginRight: "8px" }} />
              {t("common.connect")}
            </ConnectButton>
          </Popover>

          <ModalConfirm
            title="Confirm"
            decs={t("common.extensionIsUsed")}
            method={connectExt}
            data={{
              ext: valueExt,
              force_connect: 1,
              userID: _.get(userDetail, "_id"),
            }}
            img={ImgConfirm}
            showConfirm={alreadyUseExt}
            setShowConfirm={() => dispatch(setAlreadyUseExt(false))}
          />
        </>
      )}

      <ModalConfirm
        title="Confirm"
        decs={t("common.areYouSure")}
        method={disconnectExt}
        data={{
          userID: _.get(userDetail, "_id"),
        }}
        img={ImgConfirm}
        showConfirm={showConfirmDisconnect}
        setShowConfirm={$showConfirmDisconnect}
      />
    </>
  );
}

export default withTranslation()(PopupNumpad);

const WrapStatus = styled.div`
  display: flex;
  align-items: center;
  height: 44px;
  width: fit-content;
  cursor: pointer;
  position: relative;

  img {
    height: fit-content;
  }
`;

const WrapStatusInPopup = styled(WrapStatus)`
  height: 100%;
`;

const Infor = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 8px;
`;

const Extension = styled.div`
  height: ${({ status }) => (status ? "20px" : "unset")};
  color: #6b6b6b;
  font-family: var(--roboto-700);
  font-size: 14px;
`;

const Status = styled.div`
  color: #0bb865;
  font-family: var(--roboto-500);
  font-size: 12px;
  position: relative;
  padding-left: 12px;

  ::before {
    content: "";
    background: ${({ ready }) => (ready ? "#0bb865" : "#CF1322")};
    width: 8px;
    height: 8px;
    position: absolute;
    border-radius: 50%;
    top: 27.5px;
    left: 0;
  }
`;

const StatusInPopup = styled(Status)`
  ::before {
    top: 5px;
  }
`;

const Wrapper = styled.div`
  width: 216px;
  height: ${({ screen }) =>
    screen === "inCall"
      ? "370px"
      : screen === "incomingCall"
      ? "242px"
      : "332px"};
  background: #fff;
  box-shadow: 0px 0px 38px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
`;

const Header = styled.div`
  height: 44px;
  box-shadow: 0px 0px 17px rgba(0, 0, 0, 0.1);
  padding: 0px 8px;
  border-radius: 10px 10px 0 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WrapExit = styled.div`
  img {
    cursor: pointer;
    width: fit-content;
    margin-left: 8px;
  }
`;

const Screen = styled.div`
  border-radius: 0 0 10px 10px;
  /* height: 288px; */
  background-color: ${({ isShowMenuStatus }) =>
    isShowMenuStatus ? "rgba(0, 0, 0, 0.7);" : ""};
  transition: background-color 0.5s linear;
`;

const KeyBoard = styled.div`
  padding: 16px 18px;
`;

const MenuStatus = styled(Menu)`
  width: 201px;
`;

const MenuStatusItem = styled(Menu.Item)`
  position: relative;
  .ant-typography {
    padding-left: 20px;

    ::before {
      content: "";
      background: ${({ ready }) => (ready ? "#0bb865" : "#CF1322")};
      width: 8px;
      height: 8px;
      position: absolute;
      border-radius: 50%;
      top: 13px;
      left: 16px;
    }
  }
`;

const WrapInput = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${({ isShowNumpadIncall }) =>
    isShowNumpadIncall ? "8px" : "16px"};
  margin-top: ${({ isShowNumpadIncall }) =>
    isShowNumpadIncall ? "12.5px" : "0"};

  .ant-input {
    width: 141px;
    background: #f5f5f5;
    font-size: 16px;
    padding: 2px 11px;
  }
`;

const ButtonDeleteInput = styled(Button)`
  width: 31px;
  display: flex;
  align-items: center;
  justify-content: center;

  :hover {
    border-color: ${(props) => props.theme.darker}!important;
  }

  :active {
    border-color: #d9d9d9;
  }

  :focus {
    border-color: #d9d9d9;
  }
`;

const WrapNumber = styled.div``;

const Row = styled.div`
  display: flex;
  justify-content: space-around;

  span {
    width: 60px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 16px;

    :hover {
      cursor: pointer;
      background: #e8eaeb;
    }
  }
`;

const ButtonMakeCall = styled(Button)`
  margin-top: 16px;
  background: #0bb865;
  border-color: #0bb865;
  width: 100%;
  padding: 8px 16px;
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  opacity: ${({ isShowMenuStatus }) => (isShowMenuStatus ? "0.3" : "1")};

  .ant-spin {
    display: flex;
  }

  .ant-spin-dot-item {
    background-color: #fff;
  }

  :hover {
    background: #0c9654 !important;
    border-color: #0c9654 !important;
  }

  :active {
    background: #0bb865;
    border-color: #0bb865;
  }

  :focus {
    background: #0bb865;
    border-color: #0bb865;
  }
`;

const CallOut = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const IncomingCall = styled(CallOut)``;

const Avatar = styled.div`
  margin-bottom: 8px;
  margin-top: 18px;
`;

const PhoneNumber = styled.div`
  font-size: 16px;
  font-family: var(--roboto-500);
  min-height: 25px;
`;

const StatusCall = styled.div`
  color: #6b6b6b;
  font-family: var(--roboto-500);
  margin-bottom: 8px;
`;

const Duration = styled.div`
  font-family: var(--roboto-500);
  font-size: 20px;
  color: #252424;
  margin-bottom: 8px;
`;

const ButtonDeclineCall = styled(Button)`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 84px;
  background: #c62828;
  border-radius: 5px;
  padding: 8px 16px;

  .ant-spin {
    display: flex;
  }

  .ant-spin-dot-item {
    background-color: #fff;
  }

  :hover {
    background: #9d1212 !important;
    border-color: #9d1212 !important;
  }

  :active {
    background: #c62828;
    border-color: #c62828;
  }

  :focus {
    background: #c62828;
    border-color: #c62828;
  }
`;

const ButtonAnswerCall = styled(ButtonDeclineCall)`
  background: #0bb865;
  margin-left: 16px;

  :hover {
    background: #0c9654 !important;
    border-color: #0c9654 !important;
  }

  :active {
    background: #0bb865;
    border-color: #0bb865;
  }

  :focus {
    background: #0bb865;
    border-color: #0bb865;
  }
`;

const Hotline = styled.div`
  color: #6b6b6b;
  font-family: var(--roboto-500);
  border-top: 1px solid #ececec;
  border-bottom: 1px solid #ececec;
  padding: 4px 0;
  width: 100%;
  text-align: center;
  margin-top: 16px;
`;

const Actions = styled.div`
  padding: ${({ screen }) =>
    screen === "incomingCall" ? "8px 16px 16px 16px" : "16px 16px 0 16px"};
  display: flex;
`;

const Action = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  background: ${({ active }) => (active ? "#40a9ff" : "#ececec")};
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  margin-right: 6px;

  img {
    filter: ${({ active }) => (active ? "brightness(200)" : "unset")};
  }

  :hover {
    background: #40a9ff;

    img {
      filter: brightness(200);
    }
  }

  :last-child {
    margin-right: 0;
  }
`;

const MenuTransfer = styled.div`
  background-color: #fff;
  width: 638px;
  padding: 16px;
  border-radius: 10px;

  .ant-table-thead > tr > th {
    padding: 8.5px 16px;
  }

  .ant-table-tbody > tr > td {
    padding: 5px 16px;
  }
`;

const TitleTransfer = styled.div`
  color: #2c2c2c;
  font-family: var(--roboto-700);
  font-size: 18px;
  margin-bottom: 10px;
`;

const ActionTransfer = styled.div`
  width: 31px;
  height: 17px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;

  :hover {
    background: #eeeeee;
    border-radius: 2px;
  }
`;

const WrapperMini = styled.div`
  display: flex;
  align-items: center;
  width: 555px;
  height: 92px;
  background: #ffffff;
  box-shadow: 0px 0px 38px rgba(0, 0, 0, 0.1);
  border-radius: 10px;
  padding: 16px 24px;
`;

const AvatarMini = styled(Avatar)`
  margin-top: 0;
  margin-bottom: 0;
  margin-right: 16px;
`;

const DurationMini = styled(Duration)`
  margin-bottom: 0;
  margin-right: 32px;
`;

const ActionsMini = styled(Actions)`
  padding: 0;
  margin-right: 32px;
`;

const ButtonDeclineCallMini = styled(ButtonDeclineCall)`
  margin-right: 16px;
`;

// const TextStatus = styled.div`
//   width: 60px;
//   display: -webkit-box;
//   text-overflow: ellipsis;
//   -webkit-line-clamp: 1;
//   -webkit-box-orient: vertical;
//   overflow: hidden;
//   color: #000;
// `;

const ConnectButton = styled(Button)`
  width: 121px;
  height: 44px;
  font-family: var(--roboto-700);
  background: ${(props) => props.theme.main};
  color: #fff;
  border-radius: 5px;

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) => props.theme.main};
    color: #fff;
  }

  :focus {
    background: ${(props) => props.theme.main};
    color: #fff;
  }
`;
