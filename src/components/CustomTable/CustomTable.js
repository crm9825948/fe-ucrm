import React, { useState } from "react";
import styled from "styled-components";
import { Input, Select, Pagination } from "antd";

import { CaretDownOutlined, CaretUpOutlined } from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import { useSelector } from "react-redux";

const CustomTable = (props) => {
  const {
    listColumns,
    dataSource,
    totalRecords,
    getRecord,
    sortBy,
    setSortBy,
    handleSort,
    handleSearch,
    valueSearch,
    setValueSearch,
  } = props;

  const [currentPage, setCurrentPage] = useState(1);
  const [recordPerPage, setRecordPerPage] = useState(10);

  const { t } = useTranslation();

  const { Option } = Select;

  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  const renderTable = () => {
    return (
      <table>
        <thead className="table-header">
          <tr>
            {listColumns.map((item, index) => {
              return (
                <th key={index}>
                  <ContentHeader>
                    <TitleWrap align={!item.isSort || !item.isSearch}>
                      <span>{item.name}</span>
                      {item.isSort && (
                        <IconHeader>
                          <CaretUpOutlined
                            style={{
                              color: `${
                                sortBy[item.id] === 1
                                  ? defaultBrandName.theme_color
                                  : "rgba(0, 0, 0, 0.25)"
                              }`,
                            }}
                            onClick={() => {
                              let sortTmp = { ...sortBy };
                              if (sortTmp[item.id] === 1) {
                                delete sortTmp[item.id];
                              } else {
                                sortTmp = {};
                                sortTmp[item.id] = 1;
                              }
                              handleSort(sortTmp, item.id);
                              setSortBy(sortTmp);
                            }}
                          />
                          <CaretDownOutlined
                            style={{
                              color: `${
                                sortBy[item.id] === -1
                                  ? defaultBrandName.theme_color
                                  : "rgba(0, 0, 0, 0.25)"
                              }`,
                            }}
                            onClick={() => {
                              let sortTmp = { ...sortBy };
                              if (sortTmp[item.id] === -1) {
                                delete sortTmp[item.id];
                              } else {
                                sortTmp = {};
                                sortTmp[item.id] = -1;
                              }
                              handleSort(sortTmp, item.id);
                              setSortBy(sortTmp);
                            }}
                          />
                        </IconHeader>
                      )}
                    </TitleWrap>

                    {item.isSearch && (
                      <SearchWrap>
                        {item.type !== "select" ? (
                          <CustomSearch
                            onChange={(e) => {
                              let tmp = { ...valueSearch };
                              tmp[item.id] = e.target.value;
                              setValueSearch(tmp);
                            }}
                            onPressEnter={() => {
                              handleSearch(currentPage, recordPerPage);
                            }}
                            allowClear={true}
                            placeholder={`${t("common.search")} ${item.name}`}
                          />
                        ) : (
                          <CustomSelect
                            showSearch
                            allowClear={true}
                            placeholder={`Search ${item.name}`}
                            style={{ marginBottom: 8, display: "block" }}
                          >
                            {item &&
                              item.option &&
                              item.option.map((op, index) => {
                                return (
                                  <Option key={index} value={op.value}>
                                    {op.label}
                                  </Option>
                                );
                              })}
                          </CustomSelect>
                        )}
                      </SearchWrap>
                    )}
                  </ContentHeader>
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody className="table-body">
          {dataSource.map((record, index) => {
            return (
              <tr key={index}>
                {listColumns.map((col) => {
                  // eslint-disable-next-line
                  return Object.entries(record).map(([key, value]) => {
                    if (col.id === key) {
                      return <CustomTd key={key}>{value}</CustomTd>;
                    }
                  });
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  return (
    <ContentWrap>
      <div
        style={{
          background: "white",
          padding: "10px",
          // height: "calc(100vh - 300px)",
        }}
      >
        <TableWrap>{renderTable()}</TableWrap>
      </div>

      <CustomPagination
        showQuickJumper
        current={currentPage}
        total={totalRecords}
        showSizeChanger
        showTotal={(total, range) =>
          `${range[0]}-${range[1]} of ${total} records`
        }
        onChange={(e, pageSize) => {
          // setSortBy({});
          setCurrentPage(e);
          setRecordPerPage(pageSize);
          getRecord(e, pageSize);
        }}
      />
    </ContentWrap>
  );
};

export default CustomTable;

const ContentHeader = styled.div``;

const CustomSearch = styled(Input)`
  border-radius: 5px;
  &:hover,
  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
`;

const CustomSelect = styled(Select)`
  margin-bottom: 0px !important;

  .ant-select-selection-placeholder {
    font-weight: normal;
  }

  .ant-select-selector {
    border-radius: 5px !important;
  }
`;

const TitleWrap = styled.div`
  padding: 8px;
  display: flex;

  /* justify-content: ${(props) => (props.align ? "center" : "unset")}; */
  span {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #252424;
    font-weight: normal;
  }
`;

const SearchWrap = styled.div`
  border-top: 1px solid #ddd;
  padding: 8px;
`;

const IconHeader = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 6px;

  .anticon svg {
    font-size: 12px;
    /* color: rgba(0, 0, 0, 0.25); */
    cursor: pointer;
    transition: all 0.5s;
  }
`;

const CustomTd = styled.td``;

const ContentWrap = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
  background: #fff;
  padding: 16px;
`;

const TableWrap = styled.div`
  width: 100%;
  /* height: calc(100% - 187px); */
  height: 100%;
  &::-webkit-scrollbar {
    height: 8px !important;
  }

  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  background: #fff;
  overflow-x: auto;
  overflow-y: auto;
  /* padding: 10px; */
  /* padding-right: 0; */
  table {
    /* width: 100%; */
    min-width: 100%;
    width: max-content;
    table-layout: auto;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      text-align: left;
      /* border: 1px solid #ddd;
       */
      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;

      overflow: hidden;
      resize: horizontal;
      background: #f0f0f0;
      min-width: 100px;
      white-space: nowrap;
      /* min-width: fit-content; */

      /* &:first-child {
        border-left: none;
        text-align: center;
        width: 100px;
        max-width: 100px;
        resize: none;
      } */

      &:first-child {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:last-child {
        position: sticky;
        z-index: 4;
        right: 0;
        border-right: none;
        resize: none;
        width: 150px;
        max-width: 150px;
      }
    }

    .table-body td {
      background: #fff;
      border-bottom: 1px solid #ddd;
      padding: 8.5px 16px;

      max-width: 200px;

      &:first-child {
        /* border-left: 1px solid #ddd; */
        /* text-align: center; */
      }

      &:last-child {
        /* border-right: 1px solid #ddd; */
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      }
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner,
  .ant-checkbox-indeterminate .ant-checkbox-inner::after {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox:focus .ant-checkbox-inner,
  .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner,
  .ant-checkbox-checked::after {
    border-color: ${(props) => props.theme.main} !important;
  }
`;

const CustomPagination = styled(Pagination)`
  width: 100%;
  text-align: right;
  background-color: #fff;
  padding: 16px;
  border: 1px solid #ececec;
  margin: auto;
  .ant-pagination-item-active {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
  .ant-pagination-item:hover {
    border-color: ${(props) => props.theme.main};
    a {
      color: ${(props) => props.theme.main};
    }
  }
`;
