import { Button } from "antd";
import { THEME_COLOR } from "configs/color";
import React from "react";
import { useSelector } from "redux/store";
import styled from "styled-components";

const ThemeButton = (props) => {
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { label, clickButton, className, icon, htmlType, style } = props;
  return (
    <>
      {THEME_COLOR.filter(
        (x) =>
          x.name.toLocaleLowerCase() ===
          defaultBrandName.theme_color.toLocaleLowerCase()
      )[0] && (
        <CustomButton
          htmlType={htmlType}
          style={style}
          color={
            THEME_COLOR.filter(
              (x) =>
                x.name.toLocaleLowerCase() ===
                defaultBrandName.theme_color.toLocaleLowerCase()
            )[0].main
          }
          hover={
            THEME_COLOR.filter(
              (x) =>
                x.name.toLocaleLowerCase() ===
                defaultBrandName.theme_color.toLocaleLowerCase()
            )[0].darker
          }
          className={className}
          onClick={clickButton}
        >
          {icon}
          {label}
        </CustomButton>
      )}
    </>
  );
};

export default ThemeButton;

const CustomButton = styled(Button)`
  background-color: ${(props) => props.color};
  border: 0px;
  &:hover {
    background-color: ${(props) => props.hover};
    color: #fff;
    border: 0px;
  }
  color: #fff;
  width: max-content;
`;
