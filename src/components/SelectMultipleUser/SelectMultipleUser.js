import { memo, useCallback, useEffect, useMemo, useRef, useState } from "react";
import { useTranslation, withTranslation } from "react-i18next";
import AvatarImg from "assets/images/header/avatar.png";
import styled from "styled-components";
import _ from "lodash";
import { BE_URL } from "constants/constants";
import { Input, Tooltip, Typography } from "antd";
import { CheckCircleTwoTone, CloseOutlined } from "@ant-design/icons";

const { Text } = Typography;

const SelectMultipleUser = ({
  listUser,
  showUserSelect = 5,
  listId,
  setListId,
}) => {
  const { t } = useTranslation();
  const [listIdOther, setListIdOther] = useState([]);
  const [showUser, setShowUser] = useState([]);
  const [showUserOther, setShowUserOther] = useState([]);
  const [showUserSearch, setShowUserSearch] = useState([]);
  const [show, setShow] = useState(false);
  const [textInput, setTextInput] = useState("");
  const ref = useRef();
  const refOther = useRef();

  useEffect(() => {
    setShowUser(listUser.filter((item, index) => index < showUserSelect));
    setShowUserOther(listUser.filter((item, index) => index >= showUserSelect));
  }, [listUser, showUserSelect]);

  const handleAddId = (id) => {
    if (listId.includes(id)) {
      setListId((pre) => pre.filter((item) => item !== id));
    } else {
      setListId((pre) => [...pre, id]);
    }
  };

  const onSearchHandle = useCallback((searchText) => {
    setTextInput(searchText);
  }, []);

  const debouncedSearchHandler = useMemo(
    () => _.debounce(onSearchHandle, 300),
    [onSearchHandle]
  );

  const checkOverlap = (a, b) => {
    for (let i = 0; i < a.length; i++) {
      if (b.includes(a[i])) {
        return true;
      }
    }
    return false;
  };

  useEffect(() => {
    setListIdOther(showUserOther.map((item) => item?._id));
  }, [showUserOther]);

  useEffect(() => {
    if (textInput?.length > 0) {
      let listSearch = showUserOther?.filter((user) =>
        user?.Full_name?.toLowerCase().includes(textInput?.toLowerCase())
      );

      setShowUserSearch(listSearch);
    }
  }, [textInput, showUserOther]);

  useEffect(() => {
    const handleClickOutside = (event) => {
      if (!refOther?.current?.contains(event.target)) {
        if (!ref?.current?.contains(event.target)) {
          setShow(false);
        }
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
  }, [ref, refOther]);

  return (
    <Wrapper
      style={{
        width: `${40 * (showUserSelect + 2) - (showUserSelect + 1) * 11.5}px`,
      }}
    >
      {showUser.map((item, index) => {
        return (
          <Tooltip
            placement="bottom"
            title={_.get(item, "Full_name", "")}
            key={index}
          >
            <CustomAvatar
              src={
                _.get(item, "avatar_config.url", "") !== ""
                  ? BE_URL + _.get(item, "avatar_config.url", "")
                  : AvatarImg
              }
              alt=""
              style={{ left: `${Number(index) * 30}px`, zIndex: `${index}` }}
              onClick={() => handleAddId(item?._id)}
              className={listId.includes(item?._id) ? "selected" : ""}
            />
          </Tooltip>
        );
      })}
      {showUserOther.length > 0 && (
        <CustomOther
          style={{
            left: `${showUserSelect * 30}px`,
            zIndex: `${showUserSelect}`,
          }}
          onClick={() => {
            setTextInput("");
            setShow((pre) => !pre);
          }}
          className={checkOverlap(listId, listIdOther) ? "selected" : ""}
          ref={refOther}
        >
          <Text
            ellipsis={{
              tooltip: `+${showUserOther.length}`,
            }}
            style={{ maxWidth: 50 }}
          >
            +{showUserOther.length}
          </Text>
        </CustomOther>
      )}
      <Tooltip placement="bottom" title={"Clear"}>
        <CustomClear
          onClick={() => {
            if (listId?.length > 0) setListId([]);
          }}
          style={{
            left: `${(showUserSelect + 1) * 30}px`,
            zIndex: `${showUserSelect + 1}`,
          }}
        >
          <CloseOutlined />
        </CustomClear>
      </Tooltip>
      {show && (
        <WrapperOther ref={ref}>
          <CustomInput
            autoFocus
            placeholder={`${t("QualityManagement.search")}...`}
            allowClear
            className={show ? "show" : ""}
            onChange={(e) => debouncedSearchHandler(e?.target?.value)}
          />
          <WrapperSearchUser>
            {textInput?.length > 0
              ? showUserSearch?.map((user) => {
                  return (
                    <Option
                      onClick={() => handleAddId(user?._id)}
                      className={listId.includes(user?._id) ? "selected" : ""}
                    >
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          gap: "5px",
                        }}
                      >
                        <CustomAvatarSelect
                          src={
                            _.get(user, "avatar_config.url", "") !== ""
                              ? BE_URL + _.get(user, "avatar_config.url", "")
                              : AvatarImg
                          }
                          alt=""
                        />
                        <Text
                          ellipsis={{
                            tooltip: user?.Full_name,
                          }}
                          style={{ maxWidth: 130 }}
                        >
                          {user?.Full_name}
                        </Text>
                      </div>
                      {listId.includes(user?._id) && <CheckCircleTwoTone />}
                    </Option>
                  );
                })
              : showUserOther?.map((user) => {
                  return (
                    <Option
                      onClick={() => handleAddId(user?._id)}
                      className={listId.includes(user?._id) ? "selected" : ""}
                    >
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          gap: "5px",
                        }}
                      >
                        <CustomAvatarSelect
                          src={
                            _.get(user, "avatar_config.url", "") !== ""
                              ? BE_URL + _.get(user, "avatar_config.url", "")
                              : AvatarImg
                          }
                          alt=""
                        />
                        <Text
                          ellipsis={{
                            tooltip: user?.Full_name,
                          }}
                          style={{ maxWidth: 130 }}
                        >
                          {user?.Full_name}
                        </Text>
                      </div>
                      {listId.includes(user?._id) && <CheckCircleTwoTone />}
                    </Option>
                  );
                })}
          </WrapperSearchUser>
        </WrapperOther>
      )}
    </Wrapper>
  );
};

export default withTranslation()(memo(SelectMultipleUser));

const Wrapper = styled.div`
  position: relative;
  height: 40px;
`;

const CustomAvatar = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 2px solid #fff;
  position: absolute;
  top: 0;
  transition: all ease-in-out 0.3s;
  cursor: pointer;
  object-fit: cover;

  &.selected {
    border: 3px solid ${(props) => props.theme.main};
  }

  &:hover {
    top: -6px;
    z-index: 100 !important;
    transition: all ease-in-out 0.3s;
  }
`;

const CustomAvatarSelect = styled.img`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 2px solid #fff;
  transition: all ease-in-out 0.3s;
  cursor: pointer;
  object-fit: cover;
`;

const CustomOther = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 2px solid #fff;

  position: absolute;
  top: 0;
  transition: all ease-in-out 0.3s;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #ccc;

  &.selected {
    border: 3px solid ${(props) => props.theme.main};
  }

  &:hover {
    top: -6px;
    z-index: 100 !important;
    transition: all ease-in-out 0.3s;
  }
`;

const CustomClear = styled.div`
  width: 40px;
  height: 40px;
  border-radius: 50%;
  border: 2px solid #fff;

  position: absolute;
  top: 0;
  transition: all ease-in-out 0.3s;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #ccc;

  &:hover {
    z-index: 100 !important;
    transition: all ease-in-out 0.3s;
    border: 2px solid #cf1322;
    background-color: #fff;

    span {
      color: #cf1322;
    }
  }
`;

const WrapperOther = styled.div`
  border-radius: 8px;
  width: 250px;
  transition: all ease-in-out 0.3s;
  max-height: 70vh;
  background-color: red;
  position: absolute;
  top: 50px;
  right: -10px;
  background-color: #fff;
  z-index: 100;
  padding: 12px 0;
  box-shadow: 1px 4px 17px -6px rgba(0, 0, 0, 0.75);
  -webkit-box-shadow: 1px 4px 17px -6px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 1px 4px 17px -6px rgba(0, 0, 0, 0.75);
`;

const WrapperSearchUser = styled.div`
  max-height: calc(70vh - 64px);
  overflow-y: auto;
`;

const CustomInput = styled(Input)`
  border-radius: 5px;
  width: calc(100% - 32px);
  padding: 8px 16px;
  margin-bottom: 8px;
  margin: 0 16px;
  margin-bottom: 8px;
`;

const Option = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 8px 16px;
  background-color: #fff;
  transition: all ease-in-out 0.3s;
  cursor: pointer;

  p {
    margin: 0;
  }

  &.selected {
    background-color: #f4f5f7;
    transition: all ease-in-out 0.3s;
  }

  &:hover {
    background-color: #f4f5f7;
    transition: all ease-in-out 0.3s;
  }
`;
