import { useState, useEffect, useRef, useLayoutEffect } from "react";
import _ from "lodash";

import Form from "antd/lib/form";
import styled from "styled-components/macro";
import Select from "antd/lib/select";
import Checkbox from "antd/lib/checkbox";
import { CloseOutlined } from "@ant-design/icons";
import Input from "antd/lib/input";
import { Notification } from "components/Notification/Noti";

import {
  getEmailTemplateById,
  updateFieldEmail,
  updateShowEdit,
} from "redux/slices/email";
import { getSender } from "redux/slices/emailOutgoing";
import { useDispatch, useSelector } from "react-redux";

function FormEmail({
  object_id,
  emailEdit,
  recordData,
  emailTemplate,
  typeEmail,
  form,
  showModal,
}) {
  const dispatch = useDispatch();
  const { subject, to, reply_to, cc, bcc, from_config, is_alias } = emailEdit;

  const refTo = useRef(null);
  const refCc = useRef(null);
  const refBcc = useRef(null);

  const { listSender } = useSelector((state) => state.emailOutgoingReducer);
  const { userDetail } = useSelector((state) => state.userReducer);

  const [toInput, setToInput] = useState("");
  const [ccInput, setCcInput] = useState("");
  const [bccInput, setBccInput] = useState("");
  const [subjectInput, $subjectInput] = useState("");
  const [widthTo, $widthTo] = useState(50);

  const removeTag = (key, index) => {
    let arr = [...emailEdit[key]];
    arr.splice(index, 1);
    dispatch(
      updateFieldEmail({
        key: key,
        value: arr,
      })
    );
  };

  const handleChangeField = (key, e) => {
    if (key === "subject" || key === "message") {
      dispatch(
        updateFieldEmail({
          key: key,
          value: e,
        })
      );
    } else {
      if (e && key.indexOf("from") > -1) {
        dispatch(
          updateFieldEmail({
            key: key,
            value: e,
          })
        );
      }

      if (e && key.indexOf("from") === -1) {
        let arr = [...emailEdit[key]];

        if (
          key === "to" ||
          key === "reply_to" ||
          key === "cc" ||
          key === "bcc"
        ) {
          _.map(e, (value) => {
            if (value) {
              arr.push(value.trim());
            }
          });
        } else {
          arr.push(e);
        }

        setToInput("");
        setCcInput("");
        setBccInput("");
        dispatch(
          updateFieldEmail({
            key: key,
            value: arr,
          })
        );
      }
    }
  };

  const handleSelectTemplate = (value) => {
    dispatch(updateShowEdit(false));
    if (value) {
      dispatch(
        getEmailTemplateById({
          record_id: recordData._id,
          template_id: value,
          message: "",
          subject: subject,
        })
      );
    } else {
      dispatch(
        updateFieldEmail({
          key: "message",
          value: "",
        })
      );
      dispatch(
        updateFieldEmail({
          key: "subject",
          value: "",
        })
      );
    }
  };

  useEffect(() => {
    if (
      showModal &&
      from_config &&
      listSender.find(
        (item) => item.object_id === object_id && from_config === item._id
      )
    ) {
      form.setFieldsValue({
        from_config: from_config,
      });
    }
  }, [form, from_config, listSender, object_id, showModal]);

  useEffect(() => {
    if (object_id)
      dispatch(
        getSender({
          object_id: object_id,
        })
      );
  }, [dispatch, object_id]);

  useEffect(() => {
    form.setFieldsValue({
      template: undefined,
    });
  }, [form]);

  useLayoutEffect(() => {
    if (toInput.length > 5) {
      $widthTo(toInput.length * 6 + 50);
    } else {
      $widthTo(50);
    }
  }, [toInput]);

  useEffect(() => {
    form.setFieldsValue({
      subject: subject,
    });
    $subjectInput(subject);
  }, [subject, form]);

  return (
    <Form labelAlign="left" colon={false} form={form}>
      <WrapFrom>
        <Form.Item
          label="From"
          name="from_config"
          style={{ marginBottom: 0, width: "100%", marginRight: "8px" }}
        >
          <Select
            onChange={(e) => {
              handleChangeField("from_config", e);
            }}
            bordered={false}
            placeholder="Please select"
          >
            {listSender.map((item) => {
              return (
                item.object_id === object_id && (
                  <Select.Option value={item._id} key={item._id}>
                    {item.from_email}
                  </Select.Option>
                )
              );
            })}
          </Select>
        </Form.Item>

        <WrapAlias>
          <Checkbox
            checked={is_alias}
            onChange={(e) => {
              dispatch(
                updateFieldEmail({
                  key: "is_alias",
                  value: e.target.checked,
                })
              );
              handleChangeField("_from", userDetail.Full_Name);
            }}
          >
            Alias
          </Checkbox>
        </WrapAlias>
      </WrapFrom>

      <WrapEmailDetails>
        <To
          label="To"
          name="to"
          onClick={() => {
            refTo.current.focus({
              cursor: "end",
            });
          }}
        >
          <WrapInputTo>
            {to ? (
              <InputTo>
                {to.map((item, index) => {
                  return (
                    <LabelTo key={index}>
                      {item}
                      <CloseOutlined onClick={(e) => removeTag("to", index)} />
                    </LabelTo>
                  );
                })}

                <CustomInput
                  onPressEnter={(e) => {
                    if (typeEmail === "send_email") {
                      if (to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    } else {
                      if (reply_to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "reply_to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    }
                  }}
                  onBlur={(e) => {
                    if (typeEmail === "send_email") {
                      if (to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    } else {
                      if (reply_to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "reply_to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    }
                  }}
                  ref={refTo}
                  onChange={(e) => setToInput(e.target.value)}
                  value={toInput}
                  bordered={false}
                  style={{
                    width: toInput.length > 5 ? toInput.length * 6 + 50 : 50,
                  }}
                />
              </InputTo>
            ) : (
              <InputTo>
                {reply_to.map((item, index) => {
                  return (
                    <LabelTo key={index}>
                      {item}
                      <CloseOutlined
                        onClick={(e) => removeTag("reply_to", index)}
                      />
                    </LabelTo>
                  );
                })}

                <CustomInput
                  onPressEnter={(e) => {
                    if (typeEmail === "send_email") {
                      if (to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    } else {
                      if (reply_to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "reply_to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    }
                  }}
                  onBlur={(e) => {
                    if (typeEmail === "send_email") {
                      if (to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    } else {
                      if (reply_to.includes(e.target.value)) {
                        Notification("error", "Email exist!");
                      } else {
                        handleChangeField(
                          "reply_to",
                          e.target.value.trim().split(" ")
                        );
                      }
                    }
                  }}
                  ref={refTo}
                  onChange={(e) => setToInput(e.target.value)}
                  value={toInput}
                  bordered={false}
                  style={{
                    width: widthTo,
                  }}
                />
              </InputTo>
            )}
          </WrapInputTo>
        </To>

        <To
          label="Cc"
          name="cc"
          onClick={(e) => {
            refCc.current.focus({
              cursor: "end",
            });
          }}
        >
          <WrapInputCC>
            <InputTo>
              {cc.map((item, index) => {
                return (
                  <LabelTo key={index}>
                    {item}
                    <CloseOutlined onClick={(e) => removeTag("cc", index)} />
                  </LabelTo>
                );
              })}
              <CustomInput
                onPressEnter={(e) =>
                  handleChangeField("cc", e.target.value.trim().split(" "))
                }
                onBlur={(e) =>
                  handleChangeField("cc", e.target.value.trim().split(" "))
                }
                onChange={(e) => setCcInput(e.target.value)}
                value={ccInput}
                bordered={false}
                ref={refCc}
                style={{
                  width: ccInput.length > 19 ? ccInput.length * 5 + 180 : 180,
                }}
              />
            </InputTo>
          </WrapInputCC>
        </To>

        <To
          label="Bcc"
          name="bcc"
          onClick={(e) => {
            refBcc.current.focus({
              cursor: "end",
            });
          }}
        >
          <WrapInputBCC>
            <InputTo>
              {bcc.map((item, index) => {
                return (
                  <LabelTo key={index}>
                    {item}
                    <CloseOutlined onClick={(e) => removeTag("bcc", index)} />
                  </LabelTo>
                );
              })}
              <CustomInput
                onPressEnter={(e) =>
                  handleChangeField("bcc", e.target.value.trim().split(" "))
                }
                onBlur={(e) =>
                  handleChangeField("bcc", e.target.value.trim().split(" "))
                }
                onChange={(e) => setBccInput(e.target.value)}
                value={bccInput}
                bordered={false}
                ref={refBcc}
                style={{
                  width: bccInput.length > 19 ? bccInput.length * 5 + 180 : 180,
                }}
              />
            </InputTo>
          </WrapInputBCC>
        </To>

        <Form.Item label="Template" name="template">
          <SelectTemplate
            onChange={handleSelectTemplate}
            bordered={false}
            placeholder="Please select"
          >
            {
              // eslint-disable-next-line
              emailTemplate.map((item) => {
                if (item.object_id === object_id) {
                  return (
                    <Select.Option value={item._id} key={item._id}>
                      {item.email_template_name}
                    </Select.Option>
                  );
                }
              })
            }
          </SelectTemplate>
        </Form.Item>
      </WrapEmailDetails>

      {/* {typeEmail === "send_email" && ( */}
      <Form.Item
        label="Subject"
        name="subject"
        style={{
          borderTop: "1px solid #e4e8eb",
          paddingTop: "4px",
        }}
      >
        <Input
          onChange={(e) => $subjectInput(e.target.value)}
          onBlur={(e) => handleChangeField("subject", e.target.value)}
          value={subjectInput}
          bordered={false}
        />
      </Form.Item>
      {/* )} */}
    </Form>
  );
}

export default FormEmail;

const WrapAlias = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const LabelTo = styled.div`
  display: flex;
  align-items: center;
  padding: 2px 4px 2px 8px;
  width: fit-content;
  background: #f5f5f5;
  border: 1px solid #f0f0f0;
  border-radius: 2px;
  color: #2c2c2c;
  line-height: 20px;
  margin-right: 16px;
  margin-bottom: 5px;

  :last-child {
    margin-right: 0;
  }

  .anticon-close {
    margin-left: 4px;
    width: 24px;
    height: 24px;
    background: #ffffff;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const To = styled(Form.Item)`
  border-bottom: 1px solid #e4e8eb;
  margin-top: 4px;

  .action {
    visibility: hidden;
    opacity: 0;
    transition: visibility 0s, opacity 0.5s linear;
  }

  :hover {
    .action {
      visibility: visible;
      opacity: 1;
    }
  }

  .ant-form-item-control-input-content {
    display: flex;
    justify-content: space-between;
  }
`;

const WrapInputTo = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

const WrapInputCC = styled(WrapInputTo)``;

const WrapInputBCC = styled(WrapInputTo)``;

const WrapFrom = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid #e4e8eb;
  padding-bottom: 2px;

  .ant-select-arrow {
    display: none;
  }
`;

const InputTo = styled.div`
  display: flex;
  flex-wrap: wrap;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
`;

const CustomInput = styled(Input)`
  display: flex;
  margin-left: 4px;
  -webkit-box-orient: vertical;
  -webkit-box-direction: normal;
  flex-direction: column;
  overflow: hidden;
  padding: 4px 0;
`;

const SelectTemplate = styled(Select)`
  .ant-select-arrow {
    display: none;
  }
`;

const WrapEmailDetails = styled.div``;
