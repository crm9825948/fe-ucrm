/* eslint-disable no-useless-escape */
import { useEffect, useState, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";
import parse from "html-react-parser";
import _ from "lodash";
import { useTranslation } from "react-i18next";

import Upload from "antd/lib/upload";
import Button from "antd/lib/button";
import Checkbox from "antd/lib/checkbox";
import Drawer from "antd/lib/drawer";
import Popover from "antd/lib/popover";
import Tooltip from "antd/lib/tooltip";
import Form from "antd/lib/form";
import {
  MinusOutlined,
  CloseOutlined,
  FullscreenOutlined,
  FullscreenExitOutlined,
  LoadingOutlined,
  UndoOutlined,
  ArrowsAltOutlined,
  MoreOutlined,
  CopyOutlined,
} from "@ant-design/icons";

import {
  loadEmails,
  selectEmail,
  updateEmail,
  loadEmailTemplate,
  sendNewEmail,
  replyEmail,
  forwardEmail,
  loadRecordData,
  setIsSending,
  setShowModal,
  setShowEditor,
  setStatusDrawer,
  setTypeEmail,
  markReadEmail,
  uploadFile,
  removeFile,
  updateShowEdit,
  saveDraft,
  editDraft,
  deleteDraft,
} from "redux/slices/email";

import { loadListObjectField } from "redux/slices/objects";
import { getSender } from "redux/slices/emailOutgoing";

import paperclipsIcon from "assets/icons/email/paperclips.svg";
import pdfIcon from "assets/icons/email/pdfIcon.svg";
import pptIcon from "assets/icons/email/pptIcon.svg";
import wordIcon from "assets/icons/email/wordIcon.svg";
import excelIcon from "assets/icons/email/excelIcon.svg";
import fileIcon from "assets/icons/email/fileIcon.svg";
import iconReply from "assets/icons/email/iconReply.svg";
import iconReplyAll from "assets/icons/email/iconReplyAll.svg";
import iconForward from "assets/icons/email/iconForward.svg";
import AttachFileIcon from "assets/icons/common/attach-file.svg";
import MailIn from "assets/icons/email/mail-in.svg";
import MailOut from "assets/icons/email/mail-out.svg";
import MailDraft from "assets/icons/email/mail-draft2.svg";
import MailApproval from "assets/icons/email/mail-approval.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";
import Delete from "assets/icons/common/delete.svg";

import EditorEmail2 from "./EditorEmail2";
import { Notification } from "components/Notification/Noti";
import ModalConfirmDelete from "components/Modal/ModalConfirmDeleteStateIn";
import { BASE_URL_API, BE_URL } from "constants/constants";
import { allowedTags } from "util/staticData";

const replaceStyle = (originalString) => {
  const styleRegex = /<style\s*[^>]*>.*?<\/style>/gs;
  const stringWithoutStyle = originalString?.replace(styleRegex, "");
  return stringWithoutStyle;
};

function getAttachFiles(attachFiles, fileLabels) {
  if (_.isEmpty(fileLabels)) {
    return _.isEmpty(attachFiles)
      ? []
      : _.map(attachFiles, (file) => ({
          label: file.split("/")[file.split("/").length - 1],
          name: file.split("/")[file.split("/").length - 1],
          url: file,
        }));
  } else {
    return _.map(fileLabels, (file) => ({
      ...file,
      name: _.get(file, "label", ""),
      url: _.get(file, "path", ""),
    }));
  }
}

function Email2({ record_id, object_id, loadListData }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const {
    listEmail,
    emailSelected,
    emailEdit,
    emailTemplate,
    isSending,
    recordData,
    showModal,
    // showEditor,
    statusDrawer,
    typeEmail,
    showEdit,
  } = useSelector((state) => state.componentEmail);

  const { listUserName } = useSelector((state) => state.userReducer);
  const { users } = useSelector((state) => state.objectsReducer);
  const { userDetail } = useSelector((state) => state.userReducer);

  const { listSender } = useSelector((state) => state.emailOutgoingReducer);

  let emailError = localStorage.getItem("emailError");
  // let selectedEmailError = localStorage.getItem("selectedEmailError");
  let typeEmailError = localStorage.getItem("typeEmailError");

  const editorJodit = useRef(null);

  const [timeout, setTimeOut] = useState();
  const [fileList, $fileList] = useState([]);
  const [isDraft, $isDraft] = useState(false);
  const [dataDelete, $dataDelete] = useState({});
  const [IDEmail, $IDEmail] = useState("");

  const [listEmailShow, setListEmailShow] = useState([]);
  const [filterEmailType, setFilterEmailType] = useState({
    inbox: true,
    sent: true,
    draft: true,
    pending: true,
  });
  const [showConfirmDelete, $showConfirmDelete] = useState(false);
  const [showFilter, $showFilter] = useState(false);

  const _onSelectEmail = (item) => {
    $IDEmail(_.get(item, "_id", ""));

    if (item.type_mail === 0) {
      if (item?.id_reply) {
        dispatch(setTypeEmail("reply"));
      } else if (item?.id_forward) {
        dispatch(setTypeEmail("forward"));
      } else {
        dispatch(setTypeEmail("send_email"));
      }

      let listAttachFile = [];
      item.attach_files.forEach((file) => {
        listAttachFile.push({
          name: file.split("/")[file.split("/").length - 1],
          url: file,
        });
      });
      $fileList(listAttachFile);

      dispatch(updateShowEdit(true));
      dispatch(setShowModal(true));
      $isDraft(true);
      dispatch(selectEmail({}));

      dispatch(
        updateEmail({
          ...item,
        })
      );
    } else {
      dispatch(setShowEditor(false));
      dispatch(selectEmail(item));
      if (item.read) {
        dispatch(
          markReadEmail({
            payload: {
              _id: item._id,
            },
            record_id: record_id,
          })
        );
        if (window.location.pathname.includes("list-view-with-details")) {
          loadListData();
        }
      }
    }
  };

  const _onSaveDraft = () => {
    const body = editorJodit.current.value.replaceAll(
      '<table style="border-collapse:collapse;width: 100%;">',
      '<table border="1" style="border-collapse:collapse;width: 100%;">'
    );

    const payload = {
      ...emailEdit,
      message: body,
    };

    if (isDraft) {
      dispatch(
        editDraft({
          _id: emailEdit._id,
          ...payload,
        })
      );
    } else {
      dispatch(saveDraft(payload));
    }

    closeDrawer();
  };

  const _onDeleteDraft = (item) => {
    return (e) => {
      e.stopPropagation();
      $isDraft(false);
      $dataDelete({
        _id: item._id,
        record_id: item?.record_id_main || item?.record_id,
        isSend: false,
      });
      $showConfirmDelete(true);
    };
  };

  const _onFilter = (key, e) => {
    let temp = {
      ...filterEmailType,
    };
    temp[key] = e;
    setFilterEmailType(temp);
    dispatch(selectEmail({}));
  };

  const _onReloadEmail = () => {
    dispatch(loadEmails(record_id));
  };

  const renderTypeEmail = (emailSelected) => {
    if (emailSelected.type_mail === 1) {
      return typeof emailSelected.to == "string"
        ? [emailSelected.to]
        : emailSelected.to;
    } else {
      return typeof emailSelected.from == "string"
        ? [emailSelected.from]
        : emailSelected.from;
    }
  };

  const _onNewEmail = () => {
    dispatch(updateShowEdit(true));
    dispatch(setShowModal(true));
    dispatch(setStatusDrawer("open"));
    dispatch(setShowEditor(false));
    dispatch(setTypeEmail("send_email"));
    dispatch(
      updateEmail({
        attach_files: [],
        bcc: [],
        cc: [],
        message: "",
        object_id: object_id,
        record_id: record_id,
        reply_to: "",
        subject: "",
        to: [],
        _from: [],
        from_config: "",
        is_alias: false,
        signature: _.get(userDetail, "email_signatures", [])?.find(
          (signature) => signature?.is_default
        )?.content
          ? "<br/><br/>--" +
            _.get(userDetail, "email_signatures", [])
              ?.find((signature) => signature?.is_default)
              ?.content?.replaceAll("iframe", "")
              ?.replaceAll("javascript", "")
          : "",
      })
    );
  };

  const _onReplyFwEmail = (key) => {
    dispatch(updateShowEdit(true));

    let fromConfig = "";

    //1 là email gửi ra
    if (emailSelected.type_mail === 1) {
      let temp = listSender.find(
        (email) => email.from_email === emailSelected.from
      );

      if (temp) {
        fromConfig = temp._id;
      }
    }

    //2 là email đến
    if (emailSelected.type_mail === 2) {
      if (typeof emailSelected.to === "string") {
        let temp = listSender.find(
          (email) =>
            email.object_id === object_id &&
            email.from_email === emailSelected.to
        );
        if (temp) {
          fromConfig = temp._id;
        } else {
          temp = listSender.find(
            (email) =>
              email.object_id === object_id &&
              emailSelected.cc.includes(email.from_email)
          );

          if (temp) {
            fromConfig = temp._id;
          }
        }
      } else {
        let temp = listSender.find(
          (email) =>
            email.object_id === object_id &&
            emailSelected.to.includes(email.from_email)
        );

        if (temp) {
          fromConfig = temp._id;
        } else {
          temp = listSender.find(
            (email) =>
              email.object_id === object_id &&
              emailSelected.cc.includes(email.from_email)
          );
          if (temp) {
            fromConfig = temp._id;
          }
        }
      }
    }
    // dispatch(setShowEditor(true));

    dispatch(setShowModal(true));
    dispatch(setTypeEmail(key));

    if (key.indexOf("reply") > -1) {
      dispatch(
        updateEmail({
          attach_files: [],
          bcc:
            key === "replyAll"
              ? typeof emailSelected.bcc === "string"
                ? [emailSelected.bcc]
                : emailSelected.bcc
              : [],
          cc:
            key === "replyAll"
              ? typeof emailSelected.cc === "string"
                ? [emailSelected.cc]
                : emailSelected.cc
              : [],
          id_reply: emailSelected.message_id,
          message: "",
          content_reply:
            "<br><br><p> Vào " +
            emailSelected.date +
            " < " +
            emailSelected.from +
            " > đã viết: </p><div style='border-left:1px solid lightgrey;padding-left:1rem'> " +
            replaceStyle(emailSelected.body) +
            " </div>",
          signature: _.get(userDetail, "email_signatures", [])?.find(
            (signature) => signature?.is_default
          )?.content
            ? "<br/><br/>--" +
              _.get(userDetail, "email_signatures", [])?.find(
                (signature) => signature?.is_default
              )?.content
            : "",
          show_signature_before_quote: _.get(
            userDetail,
            "show_signature_before_quote",
            false
          ),
          object_id: object_id,
          record_id: emailSelected.record_id,
          record_id_main: emailSelected.record_id_main,
          reply_to: renderTypeEmail(emailSelected),
          subject: emailSelected.subject,
          _from: [],
          from_config: fromConfig ? fromConfig : "",
          is_alias: false,
        })
      );
    }
    if (key === "forward") {
      $fileList(
        getAttachFiles(
          _.get(emailSelected, "attach_file", []),
          _.get(emailSelected, "file_label", [])
        )
      );

      dispatch(
        updateEmail({
          attach_files: getAttachFiles(
            _.get(emailSelected, "attach_file", []),
            _.get(emailSelected, "file_label", [])
          ),
          bcc: [],
          cc: [],
          id_forward: emailSelected.message_id,
          message:
            "<br><br><p>----------- Forward message -----------</p><p> Từ: < " +
            emailSelected.from +
            " ></p><p>Date: " +
            emailSelected.date +
            "</p><p>Subject: " +
            emailSelected.subject +
            "</p><p>To: < " +
            emailSelected.to +
            " ></p><div style='border-left:1px solid lightgrey;padding-left:1rem'> " +
            emailSelected.body +
            " </div>",
          signature: _.get(userDetail, "email_signatures", [])?.find(
            (signature) => signature?.is_default
          )?.content
            ? "<br/><br/>--" +
              _.get(userDetail, "email_signatures", [])?.find(
                (signature) => signature?.is_default
              )?.content
            : "",
          show_signature_before_quote: _.get(
            userDetail,
            "show_signature_before_quote",
            false
          ),
          object_id: object_id,
          record_id: emailSelected.record_id,
          record_id_main: emailSelected.record_id_main,
          reply_to: [],
          subject: emailSelected.subject,
          from_config: fromConfig ? fromConfig : "",
        })
      );
    }
  };

  const _onSendMail = () => {
    let body = editorJodit.current.value.replaceAll(
      '<table style="border-collapse:collapse;width: 100%;">',
      '<table border="1" style="border-collapse:collapse;width: 100%;">'
    );

    body = body.replaceAll(`border="0"`, `border="1"`);

    const payload = {
      ...emailEdit,
      message: body,
    };

    if (
      (emailEdit.is_alias && emailEdit._from && emailEdit._from.length === 0) ||
      (emailEdit.to && emailEdit.to.length === 0) ||
      (emailEdit.reply_to && emailEdit.reply_to.length === 0) ||
      !emailEdit.from_config
    ) {
      Notification("warning", "Please fill recipients email address");
    } else {
      dispatch(setIsSending(true));
      let t = setTimeout(() => {
        if (isDraft) {
          dispatch(
            deleteDraft({
              _id: emailEdit._id,
              record_id: emailEdit?.record_id_main || emailEdit?.record_id,
              isSend: true,
            })
          );
        }

        if (typeEmail === "send_email") {
          dispatch(sendNewEmail(payload));
        } else if (payload.id_reply) {
          dispatch(replyEmail(payload));
        } else if (payload.id_forward) {
          dispatch(forwardEmail(payload));
        }
        closeDrawer();
        dispatch(setIsSending(false));
      }, 5000);
      setTimeOut(t);
    }
  };

  const closeDrawer = () => {
    $isDraft(false);
    dispatch(setShowModal(false));
    dispatch(setShowEditor(false));
    dispatch(setStatusDrawer("close"));
    $fileList([]);
    form.setFieldsValue({
      template: undefined,
      from_config: undefined,
    });
    dispatch(updateShowEdit(false));
    dispatch(
      updateEmail({
        attach_files: [],
        bcc: [],
        cc: [],
        message: "",
        object_id: "",
        record_id: "",
        reply_to: "",
        subject: "",
        to: [],
        _from: [],
        from_config: "",
        is_alias: false,
      })
    );
  };

  const minimizeDrawer = () => {
    dispatch(setShowModal(false));
    dispatch(setStatusDrawer("minimize"));
  };
  const unMinimizeDrawer = () => {
    dispatch(setShowModal(true));
    dispatch(setStatusDrawer("open"));
  };
  const fullscreenDrawer = () => {
    dispatch(setShowModal(true));
    dispatch(setStatusDrawer("fullscreen"));
  };
  const ExitFullscreenDrawer = () => {
    dispatch(setStatusDrawer("open"));
  };

  const uploadAttachFile = (data) => {
    dispatch(uploadFile(data));
  };

  useEffect(() => {
    let result = [];
    listEmail.forEach((item) => {
      if (
        (filterEmailType.inbox && item.type_mail === 2) ||
        (filterEmailType.sent && item.type_mail === 1) ||
        (filterEmailType.draft && item.type_mail === 0) ||
        (filterEmailType.pending && item.type_mail === 3)
      ) {
        result.push(item);
      }
    });

    setListEmailShow(result);
  }, [filterEmailType, listEmail]);

  useEffect(() => {
    dispatch(selectEmail({}));
    dispatch(loadEmails(record_id));
    dispatch(loadEmailTemplate());
    dispatch(
      loadRecordData({
        object_id: object_id,
        id: record_id,
      })
    );
    if (object_id) {
      dispatch(
        loadListObjectField({
          api_version: "2",
          object_id: object_id,
        })
      );
    }
  }, [record_id, object_id, dispatch]);

  useEffect(() => {
    if (object_id) {
      dispatch(
        getSender({
          object_id: object_id,
        })
      );
    }
  }, [dispatch, object_id]);

  useEffect(() => {
    if (emailError) {
      const result = JSON.parse(emailError);
      $fileList(
        getAttachFiles(
          _.get(result, "attach_files", []),
          _.get(result, "file_label", [])
        )
      );

      dispatch(
        updateEmail({
          attach_files: getAttachFiles(
            _.get(result, "attach_files", []),
            _.get(result, "file_label", [])
          ),
          to:
            typeof _.get(result, "to") === "string"
              ? [_.get(result, "to")]
              : _.get(result, "to"),
          bcc:
            typeof _.get(result, "bcc") === "string"
              ? [_.get(result, "bcc")]
              : _.get(result, "bcc"),
          cc:
            typeof _.get(result, "cc") === "string"
              ? [_.get(result, "cc")]
              : _.get(result, "cc"),
          id_reply: _.get(result, "id_reply", ""),
          message: _.get(result, "message", ""),
          object_id: _.get(result, "object_id", ""),
          record_id: _.get(result, "record_id", ""),
          record_id_main: _.get(result, "record_id_main", ""),
          reply_to:
            typeof _.get(result, "reply_to") == "string"
              ? [_.get(result, "reply_to")]
              : _.get(result, "reply_to"),
          subject: _.get(result, "subject", ""),
          _from: [],
          from_config: _.get(result, "from_config", ""),
          is_alias: _.get(result, "is_alias", false),
        })
      );

      dispatch(setShowModal(true));
      dispatch(updateShowEdit(true));
      dispatch(setStatusDrawer("open"));

      localStorage.removeItem("emailError");
    }

    if (typeEmailError) {
      dispatch(setTypeEmail(typeEmailError));
      localStorage.removeItem("typeEmailError");
    }

    // if (selectedEmailError) {
    //   dispatch(selectEmail(selectedEmailError));
    //   localStorage.removeItem("selectedEmailError");
    // }
  }, [emailError, typeEmailError, dispatch]);

  const options = {
    replace: (domNode) => {
      if (_.get(domNode, "name", "") === "style") {
        return <style></style>;
      }

      if (
        _.get(domNode, "name", "") !== "" &&
        !allowedTags.includes(_.get(domNode, "name", ""))
      ) {
        return (
          <>
            {"< "}
            {_.get(domNode, "name", "")}
            {" >"}
            {_.map(_.get(domNode, "children", []), (item) =>
              _.get(item, "data", "")
            )}
          </>
        );
      }
    },
  };

  return (
    <Wrapper>
      <Left>
        <Filter>
          <Popover
            visible={showFilter}
            onVisibleChange={() => $showFilter(!showFilter)}
            content={
              <WrapCheckBox>
                <Checkbox
                  checked={filterEmailType.inbox}
                  onChange={(e) => _onFilter("inbox", e.target.checked)}
                >
                  {t("Email.inbox")}
                </Checkbox>
                <Checkbox
                  checked={filterEmailType.sent}
                  onChange={(e) => _onFilter("sent", e.target.checked)}
                >
                  {t("Email.sent")}
                </Checkbox>
                <Checkbox
                  checked={filterEmailType.draft}
                  onChange={(e) => _onFilter("draft", e.target.checked)}
                >
                  {t("Email.drafts")}
                </Checkbox>
                <Checkbox
                  checked={filterEmailType.pending}
                  onChange={(e) => _onFilter("pending", e.target.checked)}
                >
                  {t("Email.pendingApproval")}
                </Checkbox>
              </WrapCheckBox>
            }
            placement="bottom"
            trigger="click"
          >
            <Tooltip title={t("Email.filter")}>
              <MoreOutlined />
            </Tooltip>
          </Popover>
          <Tooltip title={t("Email.reloadEmail")}>
            <UndoOutlined
              style={{ marginLeft: "8px" }}
              onClick={_onReloadEmail}
            />
          </Tooltip>
        </Filter>

        <ListEmail>
          {listEmailShow.map((item) => {
            return (
              <WrapEmail
                active={IDEmail === item._id}
                onClick={(e) => _onSelectEmail(item)}
                key={item._id}
              >
                <SubjectEmail read={item.read} draft={item.type_mail === 0}>
                  <p>{item.subject}</p>

                  {item.type_mail === 0 && (
                    <Tooltip title={t("common.delete")}>
                      <img
                        src={Delete}
                        alt="delete"
                        onClick={_onDeleteDraft(item)}
                      />
                    </Tooltip>
                  )}
                </SubjectEmail>

                <ToEmail>
                  {item?.to
                    ? typeof item.to == "string"
                      ? item.to
                      : item.to[0]
                    : ""}
                </ToEmail>

                <TimeEmail>
                  {item.date}{" "}
                  <TypeEmail>
                    {item.type_mail === 2 ? (
                      <img src={MailIn} alt="MailIn" />
                    ) : item.type_mail === 1 ? (
                      <img src={MailOut} alt="MailOut" />
                    ) : item.type_mail === 3 ? (
                      <img src={MailApproval} alt="MailApproval" />
                    ) : (
                      <img src={MailDraft} alt="MailDraft" />
                    )}
                  </TypeEmail>
                </TimeEmail>
              </WrapEmail>
            );
          })}
        </ListEmail>
      </Left>

      <Right>
        <NewEmail type="primary" onClick={() => _onNewEmail()}>
          + {t("Email.newEmail")}
        </NewEmail>

        {listEmailShow.length === 0 && (
          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>No email data</span>
          </Empty>
        )}

        {emailSelected._id && (
          <>
            <EmailDetails>
              <Title>
                <span>{emailSelected.subject}</span>

                {emailSelected.type_mail && (
                  <Type>
                    {emailSelected.type_mail === 2
                      ? t("Email.inbox")
                      : emailSelected.type_mail === 3
                      ? t("Email.pendingApproval")
                      : t("Email.sent")}
                  </Type>
                )}
              </Title>
              <From>{emailSelected.from}</From>
              <To>
                To:{" "}
                {typeof emailSelected.to === "string"
                  ? emailSelected.to
                  : emailSelected.to.length > 0
                  ? emailSelected.to.reduce(
                      (accumulator, item) => accumulator + ", " + item
                    )
                  : ""}
                <div style={{ display: "flex", alignItems: "center" }}>
                  <Time>{emailSelected.date}</Time>

                  <WrapAction>
                    {emailSelected.type_mail !== 3 && (
                      <>
                        <Tooltip title={t("Email.reply")}>
                          <Action onClick={() => _onReplyFwEmail("reply")}>
                            <img src={iconReply} alt={""} />
                          </Action>
                        </Tooltip>
                        <Tooltip title={t("Email.replyAll")}>
                          <Action onClick={() => _onReplyFwEmail("replyAll")}>
                            <img src={iconReplyAll} alt={""} />
                          </Action>
                        </Tooltip>
                        <Tooltip title={t("Email.forward")}>
                          <Action onClick={() => _onReplyFwEmail("forward")}>
                            <img src={iconForward} alt={""} />
                          </Action>
                        </Tooltip>
                      </>
                    )}
                    <Tooltip title={t("common.copy")}>
                      <Action
                        onClick={() => {
                          navigator.clipboard.writeText(emailSelected.body);
                          Notification("success", "Copy body success");
                        }}
                      >
                        <CopyOutlined />
                      </Action>
                    </Tooltip>
                  </WrapAction>
                </div>
              </To>

              {emailSelected.cc.length > 0 && (
                <CC>
                  CC:{" "}
                  {typeof emailSelected.cc === "string"
                    ? emailSelected.cc
                    : emailSelected.cc.reduce(
                        (accumulator, item) => accumulator + ", " + item
                      )}
                </CC>
              )}

              {emailSelected.bcc.length > 0 && (
                <Bcc>
                  BCC:{" "}
                  {typeof emailSelected.bcc == "string"
                    ? emailSelected.bcc
                    : emailSelected.bcc.reduce(
                        (accumulator, item) => accumulator + ", " + item
                      )}
                </Bcc>
              )}

              <WrapBody>
                <Body>
                  {parse(
                    _.replace(
                      emailSelected.body,
                      `ref=\"noopener noreferrer\"`,
                      ""
                    ),
                    options
                  )}
                </Body>
              </WrapBody>

              {getAttachFiles(
                _.get(emailSelected, "attach_file", []),
                _.get(emailSelected, "file_label", [])
              ).map((item, idx) => {
                const arr = _.get(item, "url").split("/");
                const type = arr[arr.length - 1].split(".")[1];
                let icon = "";
                switch (type) {
                  case "pdf":
                    icon = pdfIcon;
                    break;
                  case "xlsx":
                    icon = excelIcon;
                    break;
                  case "doc":
                    icon = wordIcon;
                    break;
                  case "ppt":
                    icon = pptIcon;
                    break;
                  default:
                    icon = fileIcon;
                    break;
                }
                return (
                  <AttachFile key={idx}>
                    <img
                      style={{ marginRight: "12px" }}
                      src={paperclipsIcon}
                      alt={"paperclipsIcon"}
                    />
                    <img
                      style={{ marginRight: "4px" }}
                      src={icon}
                      alt={"icon"}
                    />
                    <a
                      href={_.get(item, "url")}
                      rel="noreferrer"
                      target="_blank"
                    >
                      {_.get(item, "name")}
                    </a>
                  </AttachFile>
                );
              })}

              {emailSelected.type_mail !== 3 && (
                <ActionRep>
                  <Reply onClick={() => _onReplyFwEmail("reply")}>
                    <img src={iconReply} alt={""} />
                    {t("Email.reply")}
                  </Reply>
                  <ReplyAll onClick={() => _onReplyFwEmail("replyAll")}>
                    <img src={iconReplyAll} alt={""} />
                    {t("Email.replyAll")}
                  </ReplyAll>
                  <Forward onClick={() => _onReplyFwEmail("forward")}>
                    <img src={iconForward} alt={""} />
                    {t("Email.forward")}
                  </Forward>
                </ActionRep>
              )}
            </EmailDetails>
          </>
        )}
      </Right>

      <CustomDrawer
        visible={showModal}
        mask={statusDrawer === "fullscreen" ? true : false}
        placement={"right"}
        width={statusDrawer === "fullscreen" ? "100%" : "calc(100vw / 2)"}
        title={"New Email"}
        onClose={() => closeDrawer()}
        closeIcon={false}
        fullScreen={statusDrawer === "fullscreen"}
        footer={
          <>
            <SendEmail
              disabled={isSending}
              onClick={() => _onSendMail()}
              type="primary"
            >
              {isSending ? <LoadingOutlined /> : "Send"}
            </SendEmail>

            {isSending && (
              <CancelSendEmail
                onClick={() => {
                  clearTimeout(timeout);
                  dispatch(setIsSending(false));
                }}
              >
                Cancel
              </CancelSendEmail>
            )}

            <SaveDraft onClick={_onSaveDraft}>Save draft</SaveDraft>

            <Upload
              action={BASE_URL_API + "upload-file"}
              method={"post"}
              headers={{
                Authorization: localStorage.getItem("setting_accessToken"),
              }}
              data={{
                obj: object_id,
              }}
              multiple={true}
              fileList={fileList}
              onChange={(info) => {
                let { fileList } = info;

                const status = info.file.status;
                if (status === "done") {
                  uploadAttachFile(BE_URL + info.file.response.data[0]);
                } else if (status === "error") {
                  Notification("error", "Error!");
                }
                $fileList([...fileList]);
              }}
              onRemove={(file) => {
                dispatch(
                  removeFile(
                    file?.response?.data[0]
                      ? BE_URL + file?.response?.data[0]
                      : file?.url
                  )
                );
              }}
              progress={{
                strokeColor: {
                  "0%": "#108ee9",
                  "100%": "#87d068",
                },
                strokeWidth: 3,
                format: (percent) => `${parseFloat(percent.toFixed(2))}%`,
              }}
            >
              <ButtonUpload>
                <img src={AttachFileIcon} alt="AttachFile" />
                <span>Attach file</span>
              </ButtonUpload>
            </Upload>
          </>
        }
        extra={
          <div style={{ display: "flex", alignItems: "center" }}>
            <MinusOutlined onClick={() => minimizeDrawer()} />
            {statusDrawer === "fullscreen" ? (
              <FullscreenExitOutlined onClick={() => ExitFullscreenDrawer()} />
            ) : (
              <FullscreenOutlined
                onClick={() => dispatch(setStatusDrawer("fullscreen"))}
              />
            )}
            <CloseOutlined onClick={() => closeDrawer()} />
          </div>
        }
      >
        <EditorEmail2
          showEdit={showEdit}
          showModal={showModal}
          emailEdit={emailEdit}
          object_id={object_id}
          typeEmail={typeEmail}
          emailTemplate={emailTemplate}
          listUserName={listUserName}
          users={users}
          recordData={recordData}
          form={form}
          fullScreen={statusDrawer === "fullscreen"}
          editorJodit={editorJodit}
        />
      </CustomDrawer>

      <DrawerMini
        visible={statusDrawer === "minimize" ? true : false}
        mask={false}
        placement={"bottom"}
        closeIcon={false}
        height={35}
        left={"70%"}
        width={"260px"}
        mini={statusDrawer === "minimize"}
        extra={
          <div style={{ display: "flex", alignItems: "center" }}>
            <ArrowsAltOutlined onClick={() => unMinimizeDrawer()} />
            <FullscreenOutlined onClick={() => fullscreenDrawer()} />
            <CloseOutlined onClick={() => closeDrawer()} />
          </div>
        }
      >
        <div></div>
      </DrawerMini>

      <ModalConfirmDelete
        title={""}
        decs="After deleting data will not be undone"
        methodDelete={deleteDraft}
        dataDelete={dataDelete}
        isLoading={false}
        openConfirm={showConfirmDelete}
        setOpenConfirm={$showConfirmDelete}
      />
    </Wrapper>
  );
}

export default Email2;

Email2.defaultProps = {
  loadListData: () => {},
};

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 10px;
  padding-top: 12px;
  display: flex;
  overflow: hidden;

  .ant-btn-primary {
    font-size: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;
      border: 1px solid ${(props) => props.theme.darker}!important;
    }
  }
`;

const Left = styled.div`
  flex: 1;
  padding: 0 12px 16px 16px;
  height: 100%;
  overflow: auto;
`;

const Right = styled.div`
  flex: 3;
  padding: 0px 48px 16px 16px;
  height: 100%;
  overflow: auto;
`;

const Filter = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  position: sticky;
  top: 0;
  background: #fff;
  padding: 0 16px 16px 0;

  .anticon {
    cursor: pointer;
  }
`;

const WrapCheckBox = styled.div`
  .ant-checkbox-wrapper {
    margin-left: 0;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  display: flex;
  flex-direction: column;
`;

const ListEmail = styled.div``;

const WrapEmail = styled.div`
  padding: 17px;
  border: 1px solid #eeeeee;
  border-bottom: none;
  cursor: pointer;
  background: ${({ active }) => (active ? "#E6F7FF" : "#fff")};

  :last-child {
    border-bottom: 1px solid #eeeeee;
  }
`;

const SubjectEmail = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  p {
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 16px;
    font-family: ${({ read }) => read && "var(--roboto-700)"};
    color: #252424;
    width: ${({ draft }) => (draft ? "80%" : "100%")};
    margin-bottom: 0;
  }

  img {
    width: 23px;
  }
`;

const ToEmail = styled.div`
  margin-top: 8px;
  color: #707070;
`;

const TimeEmail = styled.div`
  color: #707070;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const TypeEmail = styled.div``;

const NewEmail = styled(Button)`
  width: 136px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 24px;
`;

const EmailDetails = styled.div`
  display: flex;
  flex-direction: column;
`;

const Title = styled.div`
  font-family: var(--roboto-500);
  font-size: 20px;
  color: #252424;
  display: flex;
  align-items: center;
  flex-wrap: wrap;

  span {
    margin-right: 24px;
  }
`;

const Type = styled(TypeEmail)`
  font-family: var(--roboto-400);
  font-size: 14px;
  padding: 1px 8px;
  background: #fafafa;
  border: 1px solid #d9d9d9;
  border-radius: 2px;
  width: fit-content;
`;

const From = styled.div`
  font-family: var(--roboto-700);
  font-size: 16px;
  margin-top: 16px;
  margin-bottom: 4px;
`;

const Time = styled.div``;

const To = styled.div`
  font-size: 12px;
  color: #707070;
  display: flex;
  justify-content: space-between;
`;

const CC = styled(To)``;

const Bcc = styled(To)``;

const WrapBody = styled.div`
  display: flex;
`;

const Body = styled.div`
  margin-top: 16px;
  margin-bottom: 24px;
  /* white-space: pre-line; */

  p {
    margin-bottom: 0;
  }
`;

const AttachFile = styled.div``;

const ActionRep = styled.div`
  margin: 24px 0;
  display: flex;
`;

const Reply = styled(Button)`
  width: 136px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 12px;
  font-size: 16px;
  color: #2c2c2c;
  border: 1px solid #d9d9d9;

  img {
    margin-right: 10px;
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    border: 1px solid ${(props) => props.theme.darker} !important;
    color: #fff !important;

    img {
      filter: brightness(200);
    }
  }

  :active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }

  :focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #2c2c2c;
  }
`;

const ReplyAll = styled(Reply)``;

const Forward = styled(Reply)`
  margin-right: 0;
`;

const WrapAction = styled.div`
  display: flex;
  margin-left: 14px;
`;

const Action = styled.div`
  width: 24px;
  height: 24px;
  border-radius: 50%;
  padding: 2px;
  cursor: pointer;
  margin-left: 10px;
  display: flex;
  align-items: center;
  justify-content: center;

  :hover {
    background: #eeeeee;
  }
`;

const SendEmail = styled(Button)`
  margin-right: 16px;
`;

const CancelSendEmail = styled(Button)`
  font-size: 16px;
  margin-right: 16px;

  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff !important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }
`;

const SaveDraft = styled(Button)`
  margin-right: 16px;
  font-size: 16px;

  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff !important;
    border: 1px solid ${(props) => props.theme.darker}!important;
  }
`;

const CustomDrawer = styled(Drawer)`
  width: ${(props) => props.width};
  border-radius: 10px 10px 0px 0px;
  right: 0;
  left: unset;

  .ant-drawer-content-wrapper {
    height: ${({ fullScreen }) =>
      fullScreen ? "100%" : "calc(100vh - 187px)"};
    top: 50%;
    right: 0;
    transform: translate(0, -50%);
  }

  .ant-drawer-footer {
    position: relative;
  }

  .ant-upload-list {
    display: flex;
    flex-wrap: wrap;
    max-height: 84px;
    overflow: auto;
    overflow-x: hidden;
    margin-right: 16px;
    margin-top: 8px;

    ::-webkit-scrollbar {
      width: 10px;
    }
  }

  .ant-upload-list-text-container {
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 4px 0px 4px 8px;
    margin-right: 8px;
    margin-bottom: 4px;
    border: 1px solid #dddddd;
    border-radius: 5px;
    max-width: 199px;
    height: 38px;
    background: #eeeeee;
  }

  .ant-upload-list-item {
    margin-top: 0;
  }

  .ant-upload-span {
    max-width: 190px;
  }

  .ant-drawer-header {
    padding: 4.5px 24px;
    background: ${(props) => props.theme.main};
  }

  .ant-drawer-title {
    color: #fff;
  }

  .ant-drawer-close {
    display: none;
  }

  .ant-drawer-extra {
    span {
      margin-left: 8px;
      color: #fff;
    }
  }

  .ant-drawer-body {
    margin: 14px 14px 0 14px;
    padding: 0;
    overflow-x: hidden;

    ::-webkit-scrollbar {
      width: 0;
    }
  }

  .ant-drawer-content {
    border-radius: 10px 10px 0px 0px;
  }
  .ant-drawer-content-wrapper {
    border-radius: 10px 10px 0px 0px;
  }

  .button-header-drawer {
    color: white;
    font-size: 17px;
  }
  .ant-input:focus {
    box-shadow: none;
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
    font-size: 16px;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const DrawerMini = styled(Drawer)`
  width: ${(props) => props.width};
  right: 0;
  left: unset;

  .ant-drawer-header {
    padding: 4.5px 24px;
    background: ${(props) => props.theme.main};
    border-radius: 10px 10px 0px 0px;
  }

  .ant-drawer-title {
    color: #fff;
  }

  .ant-drawer-close {
    display: none;
  }

  .ant-drawer-extra {
    span {
      margin-left: 8px;
      color: #fff;
    }
  }
  .button-header-drawer {
    color: white;
    font-size: 17px;
  }
`;

const ButtonUpload = styled(Button)`
  margin-right: 16px;
  padding: 8px 16px;
  background: #dddddd;
  box-shadow: 0px 1px 2px rgba(31, 41, 55, 0.08);
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;

  span {
    font-size: 16px;
    line-height: 22px;
    color: #6b6b6b;
  }

  img {
    margin-right: 8px;
  }

  :hover {
    background: ${(props) => props.theme.darker} !important;
    color: #fff !important;
  }

  :active {
    background: #dddddd;

    span {
      color: #6b6b6b;
    }
  }

  :focus {
    background: #dddddd;
    span {
      color: #6b6b6b;
    }
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 24px;
  font-size: 16px;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
    width: fit-content;
  }
`;
