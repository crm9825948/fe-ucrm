import styled from "styled-components/macro";

import Editor from "components/Editor/Editor2";
import FormEmail from "./FormEmail";

function EditorEmail2({
  object_id,
  emailEdit,
  recordData,
  emailTemplate,
  typeEmail,
  minHeightInput,
  form,
  fullScreen,
  showModal,
  showEdit,
  editorJodit,
}) {
  const {
    message,
    attach_files,
    content_reply,
    signature,
    show_signature_before_quote,
  } = emailEdit;

  return (
    <Wrapper
      maxHeight={
        attach_files?.length > 3
          ? fullScreen
            ? "calc(100vh - 430px)"
            : "calc(100vh - 654px)"
          : attach_files?.length < 4 && attach_files?.length > 0
          ? fullScreen
            ? "calc(100vh - 430px)"
            : "calc(100vh - 610px)"
          : fullScreen
          ? "calc(100vh - 380px)"
          : "calc(100vh - 570px)"
      }
    >
      <FormEmail
        object_id={object_id}
        emailEdit={emailEdit}
        recordData={recordData}
        emailTemplate={emailTemplate}
        typeEmail={typeEmail}
        minHeightInput={minHeightInput}
        form={form}
        showModal={showModal}
      />

      {showEdit && (
        <Editor
          editorJodit={editorJodit}
          content={
            emailEdit.type_mail === 0
              ? message
              : typeEmail === "reply" || typeEmail === "replyAll"
              ? show_signature_before_quote
                ? message.concat(signature).concat(content_reply)
                : message.concat(content_reply).concat(signature)
              : show_signature_before_quote
              ? signature.concat(message)
              : message.concat(signature)
          }
          objectID={object_id}
          showAppend={false}
          minHeight={minHeightInput}
        />
      )}
    </Wrapper>
  );
}

export default EditorEmail2;

EditorEmail2.defaultProps = {
  minHeightInput: "300px",
};

const Wrapper = styled.div`
  .ant-form-item {
    margin-bottom: 0;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }

  .jodit-wysiwyg {
    height: ${({ maxHeight }) => `${maxHeight}`} !important;
  }
`;
