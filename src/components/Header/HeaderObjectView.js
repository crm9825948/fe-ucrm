import React from "react";
import styled from "styled-components";
import Carousel from "react-multi-carousel";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router";
import { Typography, Dropdown } from "antd";
import { DownOutlined } from "@ant-design/icons";
import { useEffect } from "react";
import {
  loadDataSuccess,
  loadFavouriteObjects,
  loadHeaderSuccess,
  toggleFavouriteObject,
} from "redux/slices/objects";
import { useTranslation } from "react-i18next";
import { useState } from "react";

const { Text: TextComponent } = Typography;
const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1600 },
    items: 5,
    slidesToSlide: 5,
  },
  tablet: {
    breakpoint: { max: 1599, min: 1000 },
    items: 4,
    slidesToSlide: 4,
  },
  mobile: {
    breakpoint: { max: 999, min: 0 },
    items: 2,
    slidesToSlide: 2,
  },
};

const HeaderObjectView = () => {
  const { objectsFavourite } = useSelector((state) => state.objectsReducer);
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);
  const { visibleList, objectCategory } = useSelector(
    (state) => state.objectsManagementReducer
  );
  const [category, setCategory] = useState({});

  const [open, setOpen] = useState(false);
  const [listObject, setListObject] = useState([]);

  const { objectId, object_id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { t } = useTranslation();

  useEffect(() => {
    dispatch(loadFavouriteObjects());
  }, [dispatch]);

  useEffect(() => {
    if (objectCategory) {
      setCategory(objectCategory);
    }
  }, [objectCategory]);

  const hexToRGB = (color) => {
    if (color) {
      const r = parseInt(color.slice(1, 3), 16);
      const g = parseInt(color.slice(3, 5), 16);
      const b = parseInt(color.slice(5, 7), 16);

      return `rgb(${r}, ${g}, ${b},0.2)`;
    }
  };

  useEffect(() => {
    if (objectsFavourite) {
      let temp = [...objectsFavourite];
      temp.unshift({
        objectName: "All",
        _id: "no_id",
      });
      setListObject(temp);
    }
  }, [objectsFavourite]);

  const menu = (
    <ContentWrap>
      {/*eslint-disable-next-line*/}
      {Object.entries(category).map(([key, value], idx) => {
        if (visibleList[key]) {
          return (
            <Group key={idx}>
              <CustomTitle>{key}</CustomTitle>
              <GroupContent>
                {/*eslint-disable-next-line*/}
                {value.map((item, index) => {
                  if (item.Status && item.visible) {
                    return (
                      <ItemWrap key={index}>
                        <CustomName
                          onClick={() => {
                            setOpen(false);
                            if (objectId !== item._id) {
                              dispatch(loadDataSuccess([]));
                              dispatch(loadHeaderSuccess([]));
                            }
                            navigate(`/objects/${item._id}/default-view`);
                          }}
                        >
                          <TextComponent>{item.Name}</TextComponent>
                        </CustomName>
                        {objectsFavourite.findIndex(
                          (object) => object.object_id === item._id
                        ) >= 0 ? (
                          <ImgWrap
                            color={() => hexToRGB(defaultBrandName.theme_color)}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          >
                            <svg
                              width="15"
                              height="15"
                              viewBox="0 0 15 15"
                              fill="none"
                              xmlns="http://www.w3.org/2000/svg"
                              className="unpin-img"
                            >
                              <path
                                d="M13.801 4.81716L10.1815 1.19766C9.9504 0.966614 9.6677 0.79379 9.3567 0.693427C9.04571 0.593065 8.71532 0.568037 8.39275 0.620406C8.07019 0.672776 7.76468 0.801043 7.50141 0.994639C7.23814 1.18823 7.02464 1.44162 6.8785 1.73391L5.05225 5.38641C4.98655 5.51811 4.87185 5.61881 4.73275 5.66691L1.60825 6.74691C1.45806 6.79881 1.32352 6.88802 1.21728 7.00619C1.11104 7.12435 1.03657 7.26758 1.00088 7.42243C0.965184 7.57727 0.969436 7.73864 1.01323 7.89139C1.05703 8.04414 1.13893 8.18325 1.25125 8.29566L3.57925 10.6237L0.25 13.9537V14.7487H1.045L4.375 11.4187L6.703 13.7474C6.81539 13.8599 6.95456 13.942 7.1074 13.9859C7.26024 14.0298 7.42173 14.0341 7.57669 13.9984C7.73166 13.9627 7.87499 13.8881 7.99321 13.7818C8.11143 13.6754 8.20066 13.5407 8.2525 13.3904L9.3325 10.2659C9.38042 10.1269 9.48084 10.0123 9.61225 9.94641L13.2648 8.12016C13.557 7.97402 13.8104 7.76052 14.004 7.49725C14.1976 7.23398 14.3259 6.92847 14.3783 6.60591C14.4306 6.28334 14.4056 5.95295 14.3052 5.64196C14.2049 5.33096 14.032 5.04826 13.801 4.81716Z"
                                fill={defaultBrandName.theme_color}
                              />
                            </svg>
                          </ImgWrap>
                        ) : (
                          <ImgWrap
                            color={() => hexToRGB(defaultBrandName.theme_color)}
                            onClick={() => {
                              dispatch(
                                toggleFavouriteObject({
                                  object_id: item._id,
                                })
                              );
                            }}
                          >
                            <svg
                              width="15"
                              height="14"
                              viewBox="0 0 15 14"
                              fill="none"
                              className="pin-img"
                              xmlns="http://www.w3.org/2000/svg"
                            >
                              <path
                                d="M7.05335 1.28418C7.65912 0.0563843 9.34925 -0.224722 10.3499 0.735875L13.8025 4.05051C14.8038 5.01171 14.5103 6.63366 13.2314 7.21522L9.83728 8.75858C9.58144 8.87503 9.3821 9.08155 9.28061 9.33531L8.28001 11.832C8.21405 11.9964 8.10637 12.1424 7.96673 12.2567C7.82709 12.3711 7.65992 12.4501 7.4804 12.4867C7.30087 12.5233 7.11468 12.5162 6.93871 12.4662C6.76274 12.4161 6.60258 12.3247 6.47276 12.2002L4.50052 10.3068L0.917488 13.7466H0.25V13.1058L3.83303 9.66598L1.86079 7.7726C1.73107 7.64796 1.63583 7.4942 1.58371 7.32527C1.5316 7.15634 1.52425 6.97759 1.56235 6.80524C1.60044 6.63289 1.68277 6.47241 1.80186 6.33835C1.92094 6.2043 2.07302 6.10092 2.24428 6.0376L4.84497 5.0764C5.10944 4.97967 5.32417 4.78804 5.44508 4.5426L7.05271 1.28418H7.05335ZM9.68174 1.37728C9.55688 1.25739 9.40384 1.16807 9.23551 1.11683C9.06717 1.06559 8.88848 1.05394 8.71448 1.08286C8.54048 1.11178 8.37626 1.18042 8.23566 1.283C8.09507 1.38557 7.98221 1.51909 7.9066 1.67229L6.29896 4.9307C6.07398 5.3867 5.67489 5.74213 5.18438 5.92334L2.58369 6.88333C2.55918 6.89231 2.53739 6.90703 2.52031 6.92616C2.50323 6.94528 2.4914 6.96819 2.48589 6.99282C2.48039 7.01745 2.48138 7.04301 2.48878 7.06717C2.49618 7.09134 2.50976 7.11335 2.52828 7.13119L7.14024 11.5588C7.15879 11.5766 7.18167 11.5896 7.20681 11.5968C7.23195 11.6039 7.25855 11.6049 7.28419 11.5997C7.30984 11.5945 7.33372 11.5832 7.35367 11.5668C7.37362 11.5505 7.389 11.5297 7.39842 11.5062L8.39903 9.00946C8.58778 8.53856 8.95801 8.15543 9.433 7.93945L12.8271 6.39608C12.9868 6.32352 13.126 6.21516 13.2329 6.08013C13.3399 5.94511 13.4114 5.78738 13.4415 5.62025C13.4717 5.45311 13.4595 5.28147 13.406 5.1198C13.3526 4.95813 13.2594 4.81117 13.1344 4.69131L9.68174 1.37789V1.37728Z"
                                fill={defaultBrandName.theme_color}
                              />
                            </svg>
                          </ImgWrap>
                        )}
                      </ItemWrap>
                    );
                  }
                })}
              </GroupContent>
            </Group>
          );
        }
      })}
    </ContentWrap>
  );
  // const ButtonGroup = ({ next, previous, goToSlide, ...rest }) => {
  //   const {
  //     carouselState: { currentSlide },
  //   } = rest;
  //   return (
  //     <div className="carousel-button-group">
  //       <button
  //         className={currentSlide === 0 ? "disable" : ""}
  //         onClick={() => previous()}
  //       >
  //         ello
  //       </button>
  //       <button onClick={() => next()}>QQ</button>
  //     </div>
  //   );
  // };

  return (
    <CustomMenuHeader>
      <CarouselWrap color={() => hexToRGB(defaultBrandName.theme_color)}>
        <Carousel
          // customButtonGroup={<ButtonGroup />}
          // renderButtonGroupOutside={true}
          responsive={responsive}
        >
          {listObject.map((item, idx) => {
            if (idx !== 0) {
              if (item && item.object_name) {
                return (
                  <MenuItem
                    color={() => hexToRGB(defaultBrandName.theme_color)}
                    isActive={
                      objectId === item.object_id ||
                      object_id === item.object_id
                    }
                    key={item.object_id}
                    onClick={() => {
                      if (window.location.pathname.includes("/kanban-view/")) {
                        navigate(`/kanban-view/${item.object_id}/default-view`);
                      } else if (
                        window.location.pathname.includes(
                          "/list-view-with-details/"
                        )
                      ) {
                        navigate(
                          `/list-view-with-details/objects/${item.object_id}/default-view`
                        );
                      } else {
                        if (objectId !== item.object_id) {
                          dispatch(loadDataSuccess([]));
                          dispatch(loadHeaderSuccess([]));
                        }
                        navigate(`/objects/${item.object_id}/default-view`);
                      }
                    }}
                  >
                    <TextComponent
                      style={{
                        color: `${
                          objectId === item.object_id ||
                          object_id === item.object_id
                            ? defaultBrandName.theme_color
                            : "#2c2c2c"
                        }`,
                      }}
                      ellipsis={{ tooltip: item.object_name }}
                    >
                      {item.object_name}
                    </TextComponent>
                  </MenuItem>
                );
              }
              return null;
            } else {
              return (
                <CustomDropdown
                  visible={open}
                  overlay={menu}
                  placement="bottomRight"
                  trigger="click"
                  overlayClassName="object-view"
                  onVisibleChange={() => setOpen(!open)}
                >
                  <CustomNewButton onClick={() => setOpen(!open)}>
                    <span>
                      {t("objectLayoutField.all")} <DownOutlined />
                    </span>
                  </CustomNewButton>
                </CustomDropdown>
              );
            }
          })}
        </Carousel>
      </CarouselWrap>

      {/* <CustomDropdown
        visible={open}
        overlay={menu}
        placement="bottomRight"
        trigger="click"
        overlayClassName="object-view"
        onVisibleChange={() => setOpen(!open)}
      >
        <CustomNewButton onClick={() => setOpen(!open)}>
          <span>
            {t("objectLayoutField.all")} <DownOutlined />
          </span>
        </CustomNewButton>
      </CustomDropdown> */}
    </CustomMenuHeader>
  );
};

export default HeaderObjectView;

const CustomMenuHeader = styled.div`
  padding-left: 10px;
  flex: 1;
  display: flex;
  justify-content: space-between;
  ul {
    li {
      display: flex;
      justify-content: center;
    }
  }
`;

const CarouselWrap = styled.div`
  /* display: flex;
  max-width: 560px;
  overflow: auto; */
  width: 600px;
  :hover {
    .react-multiple-carousel__arrow--right,
    .react-multiple-carousel__arrow--left {
      visibility: visible;
      opacity: 1;
    }
  }
  .react-multiple-carousel__arrow--right {
    visibility: hidden;
    opacity: 0;
    right: 0;
  }
  .react-multiple-carousel__arrow--left {
    visibility: hidden;
    opacity: 0;
    left: 0;
  }

  .react-multiple-carousel__arrow {
    min-width: unset;
    min-height: unset;
    width: 28px;
    height: 28px;
    align-items: center;
    display: flex;
    justify-content: center;
    border-radius: 50%;
    background: ${(props) => props.color};
    ::before {
      color: #2c2c2c;
      font-size: 12px;
      font-weight: 600;
    }
    :hover {
      background: ${(props) => props.theme.main};
      ::before {
        color: #fff;
      }
    }
  }
  @media screen and (max-width: 1500px) {
    max-width: 400px;
  }
`;

const MenuItem = styled.div`
  font-size: 16px;
  line-height: 22px;
  text-align: center;
  color: #2c2c2c;
  padding: 5px 16px;
  border: 1px solid ${(props) => (props.isActive ? props.color : "#d9d9d9")};
  border-radius: 2px;
  height: calc(100% - 1px);
  width: 110px;
  transition: all 0.5s;
  background: ${(props) => (props.isActive ? props.color : "#fff")};
  /* margin: 0 4px; */
  @media screen and (max-width: 1500px) {
    font-size: 14px;
    width: 80px;
    padding: 5px 8px;
  }

  :hover {
    background: ${(props) => props.color};
    border: 1px solid ${(props) => props.color};
    span {
      color: ${(props) => props.theme.main} !important;
    }
    cursor: pointer;
  }
`;

const CustomDropdown = styled(Dropdown)`
  border: none;
  font-style: normal;
  font-family: var(--roboto-400);
  font-size: 16px;
  line-height: 22px;
  text-align: center;

  color: #2c2c2c;

  display: flex;
  justify-content: center;
  align-items: center;

  .ant-dropdown-menu-submenu-title:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
  .ant-dropdown-menu-item:hover {
    background-color: #e6f7ff;
    color: ${(props) => props.theme.darker};
  }
`;

const CustomNewButton = styled.div`
  background: #eeeeee;
  height: 33px;
  border-radius: 2px;
  width: 110px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  span {
    display: flex;
    align-items: center;
    svg {
      font-size: 13px;
      margin-left: 5px;
    }
  }
  @media screen and (max-width: 1500px) {
    font-size: 14px;
    width: 80px;
  }
`;

const ContentWrap = styled.div`
  display: -webkit-box;
  max-width: 1200px;
  overflow: auto;
  padding: 22px;
  height: fit-content;
  &::-webkit-scrollbar {
    height: 12px !important;
    width: 12px !important;
  }
  ::-webkit-scrollbar-thumb {
    background: #d9d9d9;
  }
  ::-webkit-scrollbar-thumb:hover {
    background: #d9d9d9;
  }
  @media screen and (max-width: 1500px) {
    max-width: 900px;
  }
`;

const Group = styled.div`
  width: 260px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin: 6px;
  height: fit-content;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
`;
const CustomTitle = styled.div`
  font-family: var(--roboto-500);
  font-size: 16px;
  color: #000000;
  background: #efeff4;
  padding: 11px 10px;
`;
const GroupContent = styled.div`
  padding: 8px 8px 0 8px;

  max-height: 268px;
  overflow: auto;
`;
const ItemWrap = styled.div`
  border: 1px solid #ececec;
  border-radius: 5px;
  padding-left: 16px;
  margin-bottom: 8px;
  cursor: pointer;
  display: flex;
  height: 44px;
  justify-content: space-between;
  align-items: center;
  transition: all 0.5s;
  .pin-img {
    display: none;
  }
  :hover {
    .pin-img {
      display: block;
    }

    border: 1px solid ${(props) => props.theme.darker};
  }
`;

const CustomName = styled.span`
  flex: 1;
  height: 100%;
  display: flex;
  align-items: center;
`;

const ImgWrap = styled.div`
  width: 40px;
  height: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 5px;
  margin-right: 1px;
  transition: all 0.5s;

  :hover {
    background: ${(props) => props.color};
  }
`;
