import { Button, Modal } from "antd";
import warning from "assets/icons/common/warning.png";
import { BASENAME } from "constants/constants";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { setIsOpenDuplicate, restoreRecord } from "redux/slices/objects";
import { useSelector } from "redux/store";
import styled from "styled-components";
import ModalCreate from "./ModalCreate";
import ModalOverwrite from "./OverwriteRecord";
import { mergeRecord } from "redux/slices/objects";
import ModalConfirm from "components/Modal/ModalConfirmWithoutRedux";
import img from "assets/icons/common/confirm.png";
import { updateRecord } from "redux/slices/objects";

const ModalDuplicate = ({ objectId, onFinishCall, formValues }) => {
  const { isOpenDuplicate, listDuplicate } = useSelector(
    (state) => state.objectsReducer
  );
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const [openForce, setOpenForce] = useState(false);
  const [openListRecord, setOpenListRecord] = useState(false);
  const handleOk = () => {
    dispatch(setIsOpenDuplicate(false));
  };

  const handleCancel = () => {
    dispatch(setIsOpenDuplicate(false));
  };

  /*eslint-disable-next-line*/
  const handleRestoreRecord = (Id) => {
    dispatch(
      restoreRecord({
        load: {
          object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
          current_page: 1,
          record_per_page: 10,
        },
        data: {
          object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
          id: Id,
          ignore_duplication: true,
        },
      })
    );
    handleCancel();
  };

  return (
    <>
      <CustomModal
        title={`Trùng bản ghi ${
          listDuplicate[listDuplicate.length - 1]?.third_party
            ? "ở Third party"
            : ""
        }`}
        visible={isOpenDuplicate}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <CustomContent>
          <img alt="" src={warning} />
          <Title>Trùng dữ liệu</Title>
          <Decs>
            Những record:{" "}
            {listDuplicate.map((item, idx) => {
              if (idx !== listDuplicate.length - 1 && idx < 5) {
                return (
                  <a
                    href={
                      BASENAME +
                      `consolidated-view/${item.object_id}/${item._id}`
                    }
                    target={"_blank"}
                    rel="noreferrer"
                    style={{ marginRight: "4px" }}
                  >
                    {item.ID},
                  </a>
                );
              } else return null;
            })}
            đã tồn tại trên hệ thống, bạn không thể tạo thêm, vui lòng kiểm tra
            lại! (Trùng {listDuplicate[listDuplicate.length - 1]?.rule_name})
          </Decs>
        </CustomContent>
        <CustomFooter>
          {listDuplicate[listDuplicate.length - 1]?.action_duplicate ===
            "create_override" &&
          window.location.pathname.includes("recycle-bin") === false &&
          listDuplicate[listDuplicate.length - 1]?.info_id === undefined ? (
            <>
              {listDuplicate[listDuplicate.length - 1]?.record_id ===
              undefined ? (
                <CustomButtonSave
                  size="large"
                  htmlType="submit"
                  onClick={() => {
                    setOpenListRecord(true);
                  }}
                >
                  Overwrite
                </CustomButtonSave>
              ) : (
                <CustomButtonSave
                  size="large"
                  htmlType="submit"
                  onClick={() => {
                    setOpenForce(true);
                    handleCancel();
                  }}
                >
                  Force update
                </CustomButtonSave>
              )}
              <CustomButtonSave
                size="large"
                htmlType="submit"
                onClick={() => {
                  setOpen(true);
                }}
              >
                Create
              </CustomButtonSave>
            </>
          ) : (
            ""
          )}

          {listDuplicate[listDuplicate.length - 1]?.info_id ? (
            <>
              <CustomButtonSave
                size="large"
                htmlType="submit"
                onClick={() => {
                  dispatch(
                    mergeRecord({
                      data: {
                        fields:
                          listDuplicate[listDuplicate.length - 1]?.record_data,
                        masterID:
                          listDuplicate[listDuplicate.length - 1]?.info_id
                            .masterID,
                        slaves:
                          listDuplicate[listDuplicate.length - 1]?.info_id
                            ?.slaves,
                        owner: listDuplicate[listDuplicate.length - 1]?.owner,
                        object_id:
                          listDuplicate[listDuplicate.length - 1]?.object_id,
                        ignore_duplication: true,
                      },
                      methodData: {
                        load: {
                          object_id:
                            listDuplicate[listDuplicate.length - 1]?.object_id,
                          first_record_id: null,
                          last_record_id: null,
                          search_with: {
                            meta: [],
                            data: [],
                          },
                        },
                      },
                    })
                  );
                  dispatch(setIsOpenDuplicate(false));
                  setOpen(false);
                }}
              >
                Merge
              </CustomButtonSave>
            </>
          ) : (
            ""
          )}
          {window.location.pathname.includes("recycle-bin") ? (
            <>
              <CustomButtonSave
                size="large"
                htmlType="submit"
                onClick={() => {
                  handleRestoreRecord(
                    listDuplicate[listDuplicate.length - 1]?.record_data._id
                  );
                }}
              >
                Restore
              </CustomButtonSave>
            </>
          ) : (
            ""
          )}
          <CustomButtonCancel
            size="large"
            onClick={() => {
              handleCancel();
            }}
          >
            Close
          </CustomButtonCancel>
        </CustomFooter>
      </CustomModal>
      <ModalCreate
        open={open}
        setOpen={setOpen}
        listDuplicate={listDuplicate}
        objectId={objectId}
        handleCancel={handleCancel}
        onFinishCall={onFinishCall}
        formValues={formValues}
      />
      <ModalOverwrite
        open={openListRecord}
        setOpen={setOpenListRecord}
        listDuplicate={listDuplicate}
        handleCancel={handleCancel}
        onFinishCall={onFinishCall}
        formValues={formValues}
      />

      <ModalConfirm
        title={"Bạn có chắc chắn thay đổi bản ghi này?"}
        decs={"Do you want to update this record with the exist setting?"}
        open={openForce}
        setOpen={setOpenForce}
        method={updateRecord}
        data={{
          data: {
            data: listDuplicate[listDuplicate.length - 1]?.record_data,
            owner_id: listDuplicate[listDuplicate.length - 1]?.owner,
            id: listDuplicate[listDuplicate.length - 1]?.record_id,
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            ignore_duplication: true,
          },
          load: {
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
        }}
        setOpenModal={() => {}}
        img={img}
      />
    </>
  );
};

export default ModalDuplicate;

ModalDuplicate.defaultProps = {
  onFinishCall: () => {},
  formValues: {},
};

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  img {
    width: 70px;
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #2c2c2c;
  text-align: center;
`;

const Decs = styled.span`
  font-size: 14px;
  line-height: 22px;
  text-align: center;
  color: #595959;
`;

const CustomModal = styled(Modal)`
  .ant-btn:active {
    border: none;
    font-style: normal;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 22px;
    /* identical to box height, or 137% */

    text-align: center;

    /* Character/Color text main */

    color: #2c2c2c;
    background-color: ${(props) => props.theme.main};
  }
  .ant-btn:focus {
    background-color: ${(props) => props.theme.main};
  }
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
