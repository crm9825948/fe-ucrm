import { Button, Modal, Checkbox } from "antd";
import warning from "assets/icons/common/Create.png";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { createRecord } from "redux/slices/objects";
import { notify } from "redux/slices/duplicateRules";
import _ from "lodash";
const ModalCreate = (props) => {
  const {
    open,
    setOpen,
    listDuplicate,
    handleCancel: handleCancelProps,
    onFinishCall,
    formValues,
  } = props;

  const handleOk = () => {
    setOpen(false);
  };
  const dispatch = useDispatch();
  const handleCancel = () => {
    setOpen(false);
  };

  const [notification, setNotification] = useState(false);

  const handleCreate = () => {
    let owner = [];
    if (notification) {
      listDuplicate.map((item, idx) => {
        if (idx !== listDuplicate.length - 1) {
          if (!owner.includes(item.owner)) {
            owner.push({
              ID: item.ID,
              owner: item.owner,
            });
          }
        }
        return null;
      });

      let output = [];

      owner.forEach((item, idx) => {
        var existing = output.filter(function (v, i) {
          return v.name === item.name;
        });
        if (existing.length) {
          var existingIndex = output.indexOf(existing[0]);
          output[existingIndex].ID = output[existingIndex].ID.concat(item.ID);
        } else {
          if (typeof item.ID == "string") item.ID = [item.ID];
          output.push(item);
        }
      });

      dispatch(
        notify({
          object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
          send_notify: notification,
          list_owner: output,
          rules: listDuplicate[listDuplicate.length - 1]?.rule_name,
        })
      );
    }

    if (!_.isEmpty(formValues)) {
      onFinishCall(formValues, true);
    } else {
      dispatch(
        createRecord({
          data: {
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            data: listDuplicate[listDuplicate.length - 1]?.record_data,
            owner: listDuplicate[listDuplicate.length - 1]?.owner,
            ignore_duplication: true,
          },
          load: {
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }

    setOpen(false);
    handleCancelProps();
  };

  return (
    <>
      <CustomModal
        title="Tạo mới"
        visible={open}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
      >
        <CustomContent>
          <img alt="" src={warning} />
          <Title>Bạn có chắc chắn muốn tạo mới bản ghi?</Title>
          <Decs>Tạo mới bản ghi</Decs>
          <Decs>
            <Checkbox
              onChange={() => {
                setNotification(!notification);
              }}
              value={notification}
              checked={notification}
            />{" "}
            Thông báo đến những người dùng có bản ghi bị trùng
          </Decs>
        </CustomContent>

        <CustomFooter>
          <CustomButtonSave
            size="large"
            htmlType="submit"
            onClick={() => {
              handleCreate();
            }}
          >
            Create
          </CustomButtonSave>

          <CustomButtonCancel
            size="large"
            onClick={() => {
              handleCancel();
            }}
          >
            Close
          </CustomButtonCancel>
        </CustomFooter>
      </CustomModal>
    </>
  );
};

export default ModalCreate;

ModalCreate.defaultProps = {
  onFinishCall: () => {},
  formValues: {},
};

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;

  img {
    width: 70px;
    margin-bottom: 16px;
  }
`;

const Title = styled.span`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #2c2c2c;
  text-align: center;
`;

const Decs = styled.span`
  font-size: 14px;
  line-height: 22px;
  text-align: center;
  color: #595959;
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main}!important;
    color: ${(props) => props.theme.main}!important;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
