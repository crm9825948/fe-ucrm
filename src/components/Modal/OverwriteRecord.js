import { Button, Modal, Table, Checkbox } from "antd";
import Axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { updateRecord } from "redux/slices/objects";
import { useSelector } from "redux/store";
import styled from "styled-components";
import { notify } from "redux/slices/duplicateRules";
import { BASE_URL_API } from "constants/constants";
import _ from "lodash";
import { checkTokenExpiration } from "contexts/TokenCheck";

const ModalDuplicate = ({
  setOpen,
  open,
  handleCancel: handleCancelProps,
  onFinishCall,
  formValues,
}) => {
  const { listDuplicate, header } = useSelector(
    (state) => state.objectsReducer
  );
  const [data, setData] = useState([]);
  const [originalData, setOriginalData] = useState([]);
  const dispatch = useDispatch();
  const [listColumn, setListColumn] = useState([]);
  const [notification, setNotification] = useState(false);
  const handleOk = () => {
    setOpen(false);
  };

  useEffect(() => {
    const checkToken = async () => {
      const isTokenExpired = await checkTokenExpiration();

      if (open) {
        let listRecordID = [];
        listDuplicate.map((item, idx) => {
          if (idx !== listDuplicate.length - 1) listRecordID.push(item._id);
          return null;
        });
        Axios({
          method: "post",
          url: BASE_URL_API + "object-duplicate-rules/load-list-record",
          data: {
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            list_record_id: listRecordID,
          },
          headers: {
            Authorization: isTokenExpired,
          },
        })
          .then((res) => setOriginalData(res.data.data))
          .catch((err) => console.log(err));
      }
    };

    checkToken();
    /* eslint-disable-next-line */
  }, [listDuplicate, open]);

  const handleCancel = () => {
    setOpen(false);
    setRecordID("");
  };

  useEffect(() => {
    let arr = [];
    header.map((item, idx) => {
      arr.push({
        ...item,
        key: item.ID,
        title: item.name,
        dataIndex: item.ID,
        ellipsis: true,
        width: 150,
      });
      return null;
    });
    setListColumn(arr);

    let dataList = [];
    originalData.map((item, idx) => {
      let newOject = {};
      Object.entries(item).map(([key, value], index) => {
        newOject[key] =
          typeof value === "object" && value !== null ? value.value : value;
        return null;
      });
      dataList.push({
        ...newOject,
        key: item._id,
      });
      return null;
    });
    setData(dataList);
  }, [header, originalData]);

  const [recordID, setRecordID] = useState("");

  useEffect(() => {
    if (open) {
      setRecordID("");
    }
  }, [open]);

  const handleUpdate = () => {
    let owner = [];
    if (notification) {
      listDuplicate.map((item, idx) => {
        if (idx !== listDuplicate.length - 1) {
          if (!owner.includes(item.owner)) {
            owner.push({
              ID: item.ID,
              owner: item.owner,
            });
          }
        }
        return null;
      });

      let output = [];

      owner.forEach((item, idx) => {
        var existing = output.filter(function (v, i) {
          return v.name === item.name;
        });
        if (existing.length) {
          var existingIndex = output.indexOf(existing[0]);
          output[existingIndex].ID = output[existingIndex].ID.concat(item.ID);
        } else {
          if (typeof item.ID == "string") item.ID = [item.ID];
          output.push(item);
        }
      });

      dispatch(
        notify({
          object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
          send_notify: notification,
          list_owner: output,
          rules: listDuplicate[listDuplicate.length - 1]?.rule_name,
        })
      );
    }

    if (!_.isEmpty(formValues)) {
      onFinishCall(formValues, true, true, recordID[0]);
    } else {
      dispatch(
        updateRecord({
          data: {
            data: listDuplicate[listDuplicate.length - 1]?.record_data,
            owner_id: listDuplicate[listDuplicate.length - 1]?.owner,
            id: recordID[0],
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            ignore_duplication: true,
          },
          load: {
            object_id: listDuplicate[listDuplicate.length - 1]?.object_id,
            first_record_id: null,
            last_record_id: null,
            search_with: {
              meta: [],
              data: [],
            },
          },
        })
      );
    }

    handleCancelProps();
    setOpen(false);
    setRecordID("");
  };

  return (
    <>
      <CustomModal
        title="Trùng bản ghi"
        visible={open}
        onOk={handleOk}
        onCancel={handleCancel}
        footer={null}
        width={1200}
      >
        <CustomContent>
          <Table
            dataSource={data}
            columns={listColumn}
            pagination={false}
            scroll={{ y: 300 }}
            rowSelection={{
              type: "radio",
              selectedRowKeys: recordID,
              // ...rowSelection,
              onChange: (selectedRowKeys, selectedRows) => {
                setRecordID(selectedRowKeys);
              },
            }}
          />
          <div style={{ marginTop: "10px" }}>
            <Decs>
              <Checkbox
                onChange={() => {
                  setNotification(!notification);
                }}
                value={notification}
                checked={notification}
              />{" "}
              Thông báo đến những người dùng có bản ghi bị trùng
            </Decs>
          </div>
        </CustomContent>

        <CustomFooter>
          <CustomButtonSave
            size="large"
            htmlType="submit"
            disabled={recordID ? false : true}
            onClick={() => {
              handleUpdate();
            }}
          >
            Overwrite
          </CustomButtonSave>

          <CustomButtonCancel
            size="large"
            onClick={() => {
              handleCancel();
            }}
          >
            Close
          </CustomButtonCancel>
        </CustomFooter>
      </CustomModal>
    </>
  );
};

export default ModalDuplicate;

ModalDuplicate.defaultProps = {
  onFinishCall: () => {},
  formValues: {},
};

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const CustomButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const Decs = styled.span`
  font-size: 14px;
  line-height: 22px;
  text-align: center;
  color: #595959;
  margin-top: 10px;
`;

const CustomContent = styled.div`
  width: 100%;
  /* overflow-x: scroll; */
  margin-bottom: 10px;
  height: 500px;
  /* overflow-y: scroll; */
  table {
    /* width: max-content; */
  }
  img {
    width: 70px;
    margin-bottom: 16px;
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main}!important;
    color: ${(props) => props.theme.main}!important;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
