import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import { useCallback, useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import styled from "styled-components/macro";

function AlertDialog({ isBlocking }) {
  function useCallbackPrompt(when) {
    const navigate = useNavigate();
    const location = useLocation();

    const [showPrompt, setShowPrompt] = useState(false);
    const [lastLocation, setLastLocation] = useState(null);
    const [confirmedNavigation, setConfirmedNavigation] = useState(false);
    // eslint-disable-next-line
    const handleBlockedNavigation = useCallback(
      (nextLocation) => {
        if (
          !confirmedNavigation &&
          nextLocation.location.pathname !== location.pathname
        ) {
          setShowPrompt(true);
          setLastLocation(nextLocation);
          return false;
        }
        return true;
      },
      [confirmedNavigation, location.pathname]
    );

    const confirmNavigation = useCallback(() => {
      setShowPrompt(false);
      setConfirmedNavigation(true);
    }, []);

    const cancelNavigation = useCallback(() => {
      setShowPrompt(false);
    }, []);

    useEffect(() => {
      if (confirmedNavigation && lastLocation) {
        navigate(lastLocation.location.pathname);
      }
    }, [confirmedNavigation, lastLocation, navigate]);

    // useBlocker(handleBlockedNavigation, when);

    return [showPrompt, confirmNavigation, cancelNavigation];
  }

  const [showPrompt, confirmNavigation, cancelNavigation] =
    useCallbackPrompt(isBlocking);

  return (
    <ModalCustom
      title="Vui lòng chờ!"
      visible={showPrompt}
      onCancel={cancelNavigation}
      closable={true}
      footer={[
        <>
          <Button type="primary" onClick={confirmNavigation}>
            OK
          </Button>
          <Button onClick={cancelNavigation}>Cancel</Button>
        </>,
      ]}
    >
      It looks like you have been editing something. If you leave before saving,
      your changes will be lost.
    </ModalCustom>
  );
}

export default AlertDialog;

const ModalCustom = styled(Modal)`
  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-modal-content {
    border-radius: 8px;
  }

  .ant-modal-header {
    border-radius: 8px 8px 0 0;
  }
`;
