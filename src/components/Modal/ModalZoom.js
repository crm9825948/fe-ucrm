import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import Axios from "axios";
import fileDownload from "js-file-download";
import Modal from "antd/lib/modal";
import Button from "antd/lib/button";
import { checkTokenExpiration } from "contexts/TokenCheck";
import axios from "axios";

function ModalZoom({
  isZoom,
  $isZoom,
  selectedImage,
  $selectedImage,
  dimensions,
  $dimensions,
}) {
  const _onDownload = async () => {
    Axios.get(selectedImage, {
      responseType: "blob",
    }).then((res) => {
      fileDownload(
        res.data,
        `${selectedImage.split("/")[selectedImage.split("/").length - 1]}`
      );
    });
  };

  const [image, $image] = useState("");

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .get(selectedImage, {
          headers: {
            Authorization: isTokenValid,
          },
          responseType: "blob",
        })
        .then((res) => {
          $image(URL.createObjectURL(res.data));
        })
        .catch((err) => {});
    };

    checkToken();
  }, [selectedImage]);

  return (
    <ModalCustom
      title=""
      visible={isZoom}
      onCancel={() => {
        $isZoom(false);
        $selectedImage("");
      }}
      width={dimensions > 600 ? dimensions : 600}
      footer={null}
      closable={false}
    >
      <TransformWrapper>
        {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
          <React.Fragment>
            <TransformComponent>
              <img
                src={image}
                alt="zoom"
                onLoad={({ target: img }) => $dimensions(img?.offsetWidth)}
                style={{ minWidth: "552px", maxHeight: "calc(100vh - 250px)" }}
              />
            </TransformComponent>
            <WrapActionZoom>
              <Button onClick={() => zoomIn()}>ZOOM IN +</Button>
              <Button onClick={() => zoomOut()}>ZOOM OUT -</Button>
              <Button onClick={() => resetTransform()}>RESET</Button>
              <Button onClick={_onDownload}>DOWNLOAD</Button>
            </WrapActionZoom>
          </React.Fragment>
        )}
      </TransformWrapper>
    </ModalCustom>
  );
}

export default ModalZoom;

const ModalCustom = styled(Modal)`
  overflow: hidden;
`;

const WrapActionZoom = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;

  .ant-btn {
    font-size: 16px;
    height: unset;
    margin-left: 16px;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }

  .ant-btn:active {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }

  .ant-btn:focus {
    background: #fff;
    border: 1px solid #d9d9d9;
    color: #000;
  }
`;
