import React from "react";
import { Modal, Radio } from "antd";
import styled from "styled-components";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import ReactMarkdown from "react-markdown";

import { getDataVersionBE } from "redux/slices/authenticated";

const InfoVersion = [
  {
    version: "2.63.0",
    create_day: "23/11/2023",
    new_feature: [
      {
        content: "feat: Filter condition in/exclude (UCRM-2263)",
      },
      {
        content: "feat: Improvement custom report (UCRM-2186)",
      },
      {
        content: "feat: Improvement UI knowledge base type blog (UCRM-1667)",
      },
    ],
    fix_bug: [
      {
        content: "fix: [Report] duplicate field ID chart (BUCRM-2145)",
      },
      {
        content: "fix: default value (BUCRM-2157)",
      },
    ],
  },
  {
    version: "2.62.0",
    create_day: "16/11/2023",
    new_feature: [
      {
        content: "feat: [Report] add row summary report type (UCRM-2180)",
      },
      {
        content: "feat: [Email] change attach files to file label (UCRM-2249)",
      },
    ],
    fix_bug: [],
  },
  {
    version: "2.61.0",
    create_day: "13/11/2023",
    new_feature: [
      {
        content: "feat: Allow setting current time in min max value",
      },
    ],
    fix_bug: [],
  },
  {
    version: "2.60.0",
    create_day: "10/11/2023",
    new_feature: [
      {
        content:
          "feat: bổ sung min/max cho trường ngày tháng và trường number ở form Internet (UCRM-2237)",
      },
      {
        content:
          "feat: on/off cho phép search ở field select ở form internet (UCRM-2243)",
      },
    ],
    fix_bug: [],
  },
  {
    version: "2.59.0",
    create_day: "2/11/2023",
    new_feature: [
      {
        content: "feat: allow to setup soft required in field setting",
      },
      {
        content: "feat: export agent status",
      },
      {
        content: "feat: [Tenant] add API change pass root",
      },
      {
        content: "feat: [LDAP] allow to login with Alias",
      },
    ],
    fix_bug: [
      {
        content: "fix: datetime local in internet form",
      },
      {
        content: "fix: update refresh token",
      },
      {
        content: "fix: [Noti] change navigate view logs",
      },
    ],
  },
  {
    version: "2.58.0",
    create_day: "26/10/2023",
    new_feature: [
      {
        content: "feat: allow sort form in internet form (UCRM-1980)",
      },
      {
        content: "feat: mask value input in internet form (UCRM-2018)",
      },
      {
        content: "feat: format value number in internet form (UCRM-2024)",
      },
      {
        content:
          "feat: [DateTimeSetting] allow tenant to setup working hours and holidays (UCRM-2063)",
      },
      {
        content: "feat: [AssignmentRule] add working time (UCRM-2075)",
      },
      {
        content:
          "feat: [AssignmentRule] allow to set the maximum number (UCRM-2081)",
      },
      {
        content: "feat: [Report] allow user save quick filter (UCRM-2006)",
      },
      {
        content:
          "feat: [Workflow] allow to use selected value of triggered record (UCRM-2095)",
      },
      {
        content:
          "feat: [Workflow] allow to use field ID and Assign To in Create record and Update Related Source (UCRM-2138)",
      },
      {
        content:
          "feat: ucrm-2069 [Workflow] Condition to check if the created date or modified time",
      },
      {
        content:
          "feat: uCRM-2087 Tính năng merge Contact theo mô hình standard Object",
      },
    ],
    fix_bug: [
      {
        content: "fix: bucrm 2056 formula field in condition",
      },
      {
        content: "fix inword in date diff",
      },
    ],
  },
  {
    version: "2.57.3",
    create_day: "12/10/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "fix: [VoiceBiometric] call voice authen one time",
      },
      {
        content: " fix: [VoiceBiometric] add agent name",
      },
      {
        content: " fix: Template 500",
      },
      {
        content: " fix: Default picklist",
      },
    ],
  },
  {
    version: "2.57.2",
    create_day: "06/10/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "fix: security issues with payload API Bucrm-1970",
      },
    ],
  },
  {
    version: "2.57.1",
    create_day: "21/09/2023",
    new_feature: [
      {
        content:
          "feat: allow setting default value for internet form (UCRM-1938)",
      },
    ],
    fix_bug: [
      {
        content: "fix: can not submit form Bucrm-1909",
      },
      {
        content: "fix: pagination in form setting (UCRM-1925)",
      },
    ],
  },
  {
    version: "2.57.0",
    create_day: "14/09/2023",
    new_feature: [
      {
        content:
          "feat: allow setting default value for internet form (UCRM-1938)",
      },
      {
        content: "feat: save IP address of customer submit form (UCRM-1944)",
      },
      {
        content: "feat: allow delete tenants (UCRM-1962)",
      },
      {
        content: "feat: add or conditions in SLA (UCRM-2000)",
      },
    ],
    fix_bug: [
      {
        content: "hidden owner on chat IC QM view (BUCRM 1775)",
      },
      {
        content: "fix: fix 500 when mass edit field editor (UCRM-1897)",
      },
    ],
  },
  {
    version: "2.56.0",
    create_day: "31/8/2023",
    new_feature: [
      {
        content: "feat: [Internet form] Allow pagination in internet form",
      },
      {
        content: "feat: Add owa type outgoing email setting (UCRM 1851)",
      },
    ],
    fix_bug: [],
  },
  {
    version: "2.55.0",
    create_day: "24/8/2023",
    new_feature: [
      {
        content:
          "feat: [DynamicButton] Add select, date, datetime in modal udpate fields (UCRM-1739)",
      },
      {
        content:
          "feat: [URLPopup] allow add multiple param in URL popup (UCRM-1906)",
      },
      {
        content: "feat: [Comments] add sort comments (UCRM-1763)",
      },
      {
        content:
          "feat: [Dynamic button] add Triggered User in action Create/Update/Call API (UCRM-1745)",
      },
      {
        content: "feat: add unit weekday type dateToParts (UCRM-1900)",
      },
      {
        content: "feat: add owa type (UCRM-1851)",
      },
    ],
    fix_bug: [
      {
        content:
          "fix: [Email] remove style css when parse in Editor (BUCRM-1744)",
      },
      {
        content: "fix: multiDetail crash page",
      },
      {
        content: "fix: [SLA] hide pending condition when deadline is active",
      },
      {
        content: "fix: date to parts allow get weekday (UCRM-1900)",
      },
      {
        content: "fix: cai thien temaplte record (UCRM-1733)",
      },
      {
        content: "fix: 500 when edit record (BUCRM-1840)",
      },
      {
        content: "fix: layout Comment QM View",
      },
    ],
  },
  {
    version: "2.54.0",
    create_day: "17/8/2023",
    new_feature: [
      {
        content:
          "feat: allow to increase/decrease field number when Update Source Record (UCRM-390)",
      },
      {
        content:
          "feat: allow display default value in form description (UCRM-1869)",
      },
      {
        content: "feat: allow generate link QR code in internet form",
      },
      {
        content: "feat: search in formula field",
      },
    ],
    fix_bug: [
      {
        content: "fix: remove redundant APIs get user (UCRM-1839)",
      },
      {
        content: "fix: [KB] add token media",
      },
      {
        content: "fix: show form type ThirdParty (QM)",
      },
    ],
  },
  {
    version: "2.53.0",
    create_day: "10/8/2023",
    new_feature: [
      {
        content: "feat: add knowledge in left-right component",
      },
      {
        content: "feat: add not today in report SLA and Campaign",
      },
      {
        content: "feat: add option use fixed Deadline on Rules",
      },
    ],
    fix_bug: [
      {
        content: "fix: update search in knowledge base",
      },
      {
        content: "fix: add authen file",
      },
      {
        content: "fix: [IC Integration] reset value after change object",
      },
      {
        content:
          "fix: [Modal Record] remove empty tag editor field when update record",
      },
      {
        content: "fix: [Modal interaction] overflow UI body email",
      },
      {
        content: "fix: [Modal record] error when upload file in call and table",
      },
      {
        content: "fix: [IC Integration] call API advanced config",
      },
      {
        content: "fix: permission QM",
      },
      {
        content: "fix: permission quickfilter QM",
      },
      {
        content: "fix: default custom view QM",
      },
      {
        content: "fix: add button scroll quickFilter and style details info IC",
      },
    ],
  },
  {
    version: "2.52.0",
    create_day: "3/8/2023",
    new_feature: [
      {
        content: "[Email] allow config on off overwrite subject",
      },
      {
        content: "Allow config clone tenant",
      },
      {
        content: "Search knowledge base with ID",
      },
    ],
    fix_bug: [
      {
        content: "fix: [Report] set color chart",
      },
      {
        content: "fix: add check condition when call API load form create",
      },
      {
        content: "fix: Layout chat ic QM ",
      },
      {
        content: "fix: additional not today in condition",
      },
    ],
  },
  {
    version: "2.51.0",
    create_day: "27/7/2023",
    new_feature: [
      {
        content: "Quality management",
      },
      {
        content: "Custom report",
      },
      {
        content: "Add default alias config",
      },
    ],
    fix_bug: [
      {
        content: "Fix dư dấu / trong API get-users-detail",
      },
      {
        content: "Fix mất field linking khi sử dụng template record",
      },
      {
        content: "Fix allow to change report folder",
      },
    ],
  },
  {
    version: "2.50.0",
    create_day: "20/7/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Update Modal Assignment Rule",
      },
      {
        content: "Add user when select another record",
      },
      {
        content: "Add default search type call Finesse",
      },
    ],
  },
  {
    version: "2.49.1",
    create_day: "13/7/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "fix: [Email] white space ",
      },
      {
        content: "fix: [Numpad] show disconnect btn when dynamic ext; ",
      },
      {
        content: "fix: parse HTML in component detail, multi-detail, tags",
      },
    ],
  },
  {
    version: "2.49.0",
    create_day: "6/7/2023",
    new_feature: [
      { content: "[Finesse] Allow users to share extension" },
      { content: "Capcha in internet form" },
      { content: "Format only number in internet form" },
      { content: "Allow setting uppercase and lowercase in internet form" },
      { content: "Monitoring" },
    ],
    fix_bug: [
      {
        content:
          "fix: [Interaction Settings] allow user edit interaction settings ",
      },
      {
        content: "Fix Authentication type",
      },
      {
        content: "Fix template record when edit",
      },
    ],
  },
  {
    version: "2.48.0",
    create_day: "27/6/2023",
    new_feature: [{ content: "Improvement search in listview" }],
    fix_bug: [
      {
        content: "fix: add clone config email [Root]",
      },
      {
        content: "Allow update Interaction setting",
      },
    ],
  },
  {
    version: "2.47.0",
    create_day: "22/6/2023",
    new_feature: [
      { content: "Allow to attach file in Internet Form" },
      { content: " Add Email config in Root" },
      { content: "Object standard" },
    ],
    fix_bug: [
      {
        content: "fix: (CTI) add generate Token C247",
      },
    ],
  },
  {
    version: "2.46.1",
    create_day: "16/6/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Hot fix FE version 2.46.0",
      },
    ],
  },
  {
    version: "2.46.0",
    create_day: "13/6/2023",
    new_feature: [
      { content: "Allow to set required field in Form " },
      { content: "Apply grading in internet form" },
      {
        content:
          "[SLA] Allow to set Action for each Alert Milestone and Overdue SLA",
      },
      { content: "[Workflow] add trigger when popup" },
    ],
    fix_bug: [
      {
        content: "fix: issue Cross-Site Scripting",
      },
      {
        content: "fix: hide encrypted fields in some features",
      },
      {
        content: "fix: limit user's information in API",
      },
      { content: "fix: add API run index in root" },
      { content: "fix: [PopupURL] add API check workflow" },
      { content: "fix: add lineHeight editor" },
      {
        content: "fix: add ask before paste html Editor",
      },
    ],
  },
  {
    version: "2.45.3",
    create_day: "1/6/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Color in QR code",
      },
      {
        content:
          "fix: Allow update display_status_options in knowledge base setting",
      },
      {
        content: "fix: Delete showtime in article",
      },
    ],
  },
  {
    version: "2.45.2",
    create_day: "29/5/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "fix: Crash comment",
      },
      {
        content: "fix: [Email] Allow copy table from excel",
      },
    ],
  },
  {
    version: "2.45.1",
    create_day: "25/5/2023",
    new_feature: [
      {
        content: "Editor with field textarea",
      },
      {
        content:
          "[Dynamic Button - Update Field] Load old data; required; text editor",
      },
    ],
    fix_bug: [],
  },
  {
    version: "2.45.0",
    create_day: "23/5/2023",
    new_feature: [
      {
        content: "[VoiceBiometric] Add outgoing call",
      },
    ],
    fix_bug: [
      {
        content: "fix: [Report] Edit value fields chart",
      },
      {
        content: "fix: Protect from cross-site scripting attacks",
      },
      {
        content: "fix: IC chat widget SSO",
      },
      {
        content: "fix: Time range in modal component",
      },
      {
        content: "fix: [Email] Allow change subject when reply, forward email",
      },
    ],
  },
  {
    version: "2.44.1",
    create_day: "19/5/2023",
    new_feature: [
      {
        content: "Allow clone customview.",
      },
      {
        content: "Add time range in custom view, component tags, table ",
      },
      {
        content: "Template file",
      },
      {
        content: "[Report] Add horizontal bar, funnel chart; details pie chart",
      },
    ],
    fix_bug: [
      {
        content: "Fix get token IC SSO",
      },
      {
        content: "Fix security",
      },
    ],
  },
  {
    version: "2.44.0",
    create_day: "15/5/2023",
    new_feature: [
      {
        content: "Allow admin to setup dynamic field for a form.",
      },
      {
        content: "[URL Popup] Add param to modal create record",
      },
    ],
    fix_bug: [
      {
        content: "Authen in Expose API Check",
      },
      {
        content: "Validate field phone number in internet form",
      },
    ],
  },
  {
    version: "2.43.0",
    create_day: "9/5/2023",
    new_feature: [
      {
        content:
          "[Dynamic Button] Allow Admin to setup which fields User can update manually when clicking a Dynamic Button.",
      },
      {
        content:
          "[Workflow Action] Link Assign to của Interaction đến User trong Object User (standard Object)",
      },
      {
        content:
          "Lấy các metadata liên quan đến QM cho các Interaction từ Calabiro",
      },
      {
        content:
          "Allow Root Admin to config Custom Admin Setting or turn off Admin Permission of a Tenant",
      },
      {
        content: "Integrate with C247 Cloud",
      },
      {
        content: "Add external QR for internet form",
      },
    ],
    fix_bug: [
      {
        content: "Fix filter core 3rd third party",
      },
    ],
  },
  {
    version: "2.42.1",
    create_day: "18/4/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Fix length of file in table",
      },
      {
        content: "Add new field into interaction setting",
      },
      {
        content: "Add identify phone prefix ",
      },
    ],
  },
  {
    version: "2.42.0",
    create_day: "13/4/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Fix date format in consolidated view",
      },
    ],
  },
  {
    version: "2.41.0",
    create_day: "6/4/2023",
    new_feature: [
      { content: "Allow to copy html text from Email body" },
      { content: "Allow to set shared custom view as default" },
    ],
    fix_bug: [
      {
        content: "Update UI third party in consolidated view",
      },
      {
        content: "Add new timeline into listview with details",
      },
      {
        content: "Fix SSO auto reload page",
      },
    ],
  },
  {
    version: "2.40.2",
    create_day: "29/3/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "Add loading when search core",
      },
      {
        content: "Change button switch",
      },
      {
        content: "Add encrypted field",
      },
      {
        content: "[Call Center] Fix overwrite data call",
      },
    ],
  },
  {
    version: "2.40.0",
    create_day: "21/3/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "fix: [Interaction Setting] Add phone number",
      },
      {
        content: "fix: [CallCenter] Add search core",
      },
    ],
  },
  {
    version: "2.39.0",
    create_day: "16/3/2023",
    new_feature: [
      {
        content: "Feature share custom view",
      },
    ],
    fix_bug: [
      {
        content: "Add show file type when upload",
      },
      {
        content: "Fix bug duplicate rules",
      },
      {
        content: "Fix number in consolidated view",
      },
    ],
  },
  {
    version: "2.38.2",
    create_day: "9/3/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "[Interaction Email] Add body",
      },
      {
        content: "[TabComponent] Add drag",
      },
      {
        content: "Fix detail third party and with detail",
      },
      {
        content: "Update with detail and detail third party",
      },
    ],
  },
  {
    version: "2.38.1",
    create_day: "6/3/2023",
    new_feature: [],
    fix_bug: [
      {
        content:
          "[IC Chat Widget] Add loading when upload file; Send comment, file viber",
      },
      {
        content: "Fix multi details",
      },
      {
        content: "Fix decimals number",
      },
      {
        content: "Improvement consolidated view",
      },
      {
        content: "[IC Chat Widget] Add type email and comment, send voice",
      },
      {
        content: "[IC Chat Widget] Add type email and comment, send voice",
      },
    ],
  },
  {
    version: "2.38.0",
    create_day: "23/2/2023",
    new_feature: [
      {
        content: "Multi columns details",
      },
      {
        content: "Page title in consolidated view",
      },
      {
        content: "[VBEE] Add outgoing type",
      },
      {
        content: "[Callcenter] Add search global, add object click to call",
      },
    ],
    fix_bug: [
      {
        content: "Fix linking in table consolidated view",
      },
      {
        content: "Option number",
      },
      {
        content: "Fix decimals number",
      },
      {
        content: "[Agent Monitor] Get init status CRM",
      },
    ],
  },
  {
    version: "2.37.0",
    create_day: "17/2/2023",
    new_feature: [],
    fix_bug: [
      {
        content: "- Report details 500",
      },
      {
        content: "- Fix UI of component in Consolidated view",
      },
    ],
  },
  {
    version: "2.36.0",
    create_day: "15/2/2023",
    new_feature: [
      {
        content: "- Agent monitor",
      },
      {
        content: "- New interaction timeline",
      },
      {
        content: "- Login enhancment: Refresh token",
      },
      {
        content: "- Global search",
      },
      {
        content: "- New Knowledge Base",
      },
      {
        content: "- Refresh token External app intergration",
      },
      {
        content: "- Approval processes",
      },
      {
        content: "- IC Chat Widget",
      },
    ],
    fix_bug: [
      { content: "- DFI472: Lỗi linking field: Hiển thị sai records đã chọn" },
      {
        content: "- DFI469: SLA không có thông báo khi gần quá hạn và quá hạn",
      },
      { content: "- DFI422: Lỗi không serach được assign to khi tạo view" },
      { content: "- DFI420: Lỗi khi edit trong màn hình consolidated view" },
      {
        content:
          "- DFI389: Không Edit Picklist Dependency trong Consolidated view.",
      },
      { content: "- fix: [Finesse integration] Add default holine" },
      { content: "- fix: [Action workflow] Add today +- n" },
      { content: "- fix: Add decimal separator" },
      { content: "- fix: hot fix logout" },
    ],
  },
  {
    version: "2.35.0",
    create_day: "17/1/2023",
    new_feature: [
      {
        content: "- [Expose API] Add expose API view, upload, delete file",
      },
    ],
    fix_bug: [
      {
        content: "- OTP Finesse, CTI, Voicebiometric, Internal app integration",
      },
      {
        content:
          "- Allow to set Success Milestone to a datetime field of a record which is matched SLA rule",
      },
      { content: "- Add SECRET KEY" },
      { content: "- Fix comp detail profile for table and tag" },
      { content: "- Add language for template select in duplicate" },
    ],
  },
  {
    version: "2.34.0",
    create_day: "10/1/2023",
    new_feature: [],
    fix_bug: [
      { content: "- Fix type linkingobject" },
      { content: "- Optimize search in listview and hot fix edit detail comp" },
      { content: "- Fix role profile for consolidated view tag comp" },
      { content: "- Fix create template record" },
      { content: "- Double click edit detail, UI SLA" },
    ],
  },
  {
    version: "2.33.0",
    create_day: "30/12/2022",
    new_feature: [
      {
        content: "- Create record template",
      },
      {
        content: "- Duplicate rule",
      },
    ],
    fix_bug: [{ content: "- [Popup contact] Fix two new phone number popup" }],
  },
  {
    version: "2.32.0",
    create_day: "27/12/2022",
    new_feature: [],
    fix_bug: [
      { content: "- [Email Signature] Allow show signature before quote" },
      { content: "- [Finesse] Hide password" },
      { content: "- [CTI] Add C247 Standard " },
    ],
  },
  {
    version: "2.30.1",
    create_day: "22/12/2022",
    new_feature: [],
    fix_bug: [
      { content: "- Delete draft email when have component Comments" },
      { content: "- Field type formula datediff in words" },
    ],
  },
  {
    version: "2.30.0",
    create_day: "19/12/2022",
    new_feature: [
      {
        content: "- Allow user copy paste image",
      },
      {
        content: "- Draft email",
      },
      {
        content: "- Embed Iframe",
      },
      {
        content: "- Add trigger workflow with interaction",
      },
    ],
    fix_bug: [
      { content: "- Call center create record" },
      { content: "- Auto delete last signature" },
      { content: "- Upload one image when copy" },
      {
        content: "- Default view",
      },
      { content: "- Delete keep filter in listview" },
      { content: "- Fix bug option dashboard" },
      {
        content:
          "- Fix linking filter and double click to edit detail in consolidated view",
      },
      { content: "- Upsize text and textarea length from 10000 to 100000" },
    ],
  },
  {
    version: "2.29.2",
    create_day: "15/12/2022",
    new_feature: [
      {
        content: "- Add copy paste list email when write email",
      },
    ],
    fix_bug: [
      { content: "- Add note version BE in Modal Version" },
      { content: "- Fix display report details" },
      { content: "- Fix search number in list view" },
      {
        content:
          "- Fix linking filter and double click to edit detail in consolidated view",
      },
      { content: "- Fix third party" },
    ],
  },
  {
    version: "2.29.1",
    create_day: "13/12/2022",
    new_feature: [
      {
        content: "- Draft Email",
      },
      {
        content: "- Email Signature",
      },
      {
        content: "- Embed Iframe",
      },
      {
        content: "- Add authentication type token in External app integration",
      },
    ],
    fix_bug: [
      { content: "- Add quick filter when export report" },
      { content: "- Fix duplicated rule" },
      { content: "- Only call event to check data when use Voice Biometric" },
    ],
  },
  {
    version: "2.28.0",
    create_day: "08/12/2022",
    new_feature: [
      {
        content: "- New Duplicate rule",
      },
      {
        content: "- Add report permission",
      },
      {
        content: "- Add permission for Export, Import, Report",
      },
    ],
    fix_bug: [
      { content: "- Add download image" },
      { content: "- Fix edit detail loading" },
      { content: "- Fix third party and duplicate rule" },
      { content: "- Update display listview" },
      {
        content:
          "- Improvement modal list view, edit, UI modal record, consolidated view",
      },
    ],
  },
  {
    version: "2.27.1",
    create_day: "29/11/2022",
    new_feature: [
      {
        content: "Nothing to update",
      },
    ],
    fix_bug: [
      { content: "- Update permission in Brand and Color" },
      { content: "- Fix empty description in Calendar" },
      { content: "- Add click notification show error email" },
      { content: "- Keep content of reply email" },
      { content: "- Update UI; Update logic Action on read" },
      { content: "- Add responsive chart" },
      { content: "- Fix UI detail component" },
      {
        content:
          "- Number in kanban view, detail consolidated view, trim list view",
      },
      { content: "- Add modal zoom image in comments component" },
      { content: "- Fix edit file upload in consolidated view" },
      { content: "- Add new editor" },
      { content: "- Unlock action update related target record" },
    ],
  },
  {
    version: "2.27.0",
    create_day: "24/11/2022",
    new_feature: [
      {
        content: "- Add button scan mail",
      },
    ],
    fix_bug: [
      { content: "- Select template do not replace subject" },
      { content: "- Update UI; Update logic Action on read" },
      {
        content:
          "- Add on fail, Add select picklist, Change linking type, New UI new logic, Type select in Rule incomming",
      },
    ],
  },
];

const ModalVersion = ({ modalVersion, setModalVersion }) => {
  const dispatch = useDispatch();
  const { dataVersionBE } = useSelector((state) => state.authenticatedReducer);

  const [view, setView] = useState("fe");
  const _onCancel = () => {
    setModalVersion(false);
  };

  useEffect(() => {
    if (modalVersion) {
      dispatch(getDataVersionBE());
    }
  }, [modalVersion, dispatch]);

  return (
    <ModalCustom
      title="Version"
      visible={modalVersion}
      footer={null}
      width={1000}
      onCancel={_onCancel}
    >
      <CustomOption
        value={view}
        onChange={(e) => {
          setView(e.target.value);
        }}
      >
        <Radio value="fe">Version FE</Radio>
        <Radio value="be">Version BE</Radio>
      </CustomOption>
      {view === "fe" ? (
        <>
          {InfoVersion.map((version, index) => {
            return (
              <>
                <BasicInfo key={index}>
                  <legend>{version.version}</legend>
                  <Content>
                    <Group>
                      <CustomTitle>New feature</CustomTitle>
                      <GroupContent>
                        {version.new_feature.map((feature, idx) => {
                          return (
                            <ItemWrap key={idx}>{feature.content}</ItemWrap>
                          );
                        })}
                      </GroupContent>
                    </Group>
                    <Group>
                      <CustomTitle>Bug fix</CustomTitle>
                      <GroupContent>
                        {version.fix_bug.map((bug, idx) => {
                          return <ItemWrap key={idx}>{bug.content}</ItemWrap>;
                        })}
                      </GroupContent>
                    </Group>
                  </Content>
                </BasicInfo>
              </>
            );
          })}
        </>
      ) : (
        <ReactMarkdown source={dataVersionBE}>{dataVersionBE}</ReactMarkdown>
      )}
    </ModalCustom>
  );
};

export default ModalVersion;

const ModalCustom = styled(Modal)`
  .ant-modal-content {
    border-radius: 10px;
  }

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-form-item-label > label {
    font-size: 16px;
    color: #2c2c2c;
  }

  .ant-modal-body {
    height: 660px;
    overflow: auto;
  }
`;

const CustomOption = styled(Radio.Group)`
  .ant-radio-checked .ant-radio-inner,
  .ant-radio:hover .ant-radio-inner,
  .ant-radio-checked::after {
    border-color: ${(props) => props.theme.main};
  }

  .ant-radio-input:focus + .ant-radio-inner {
    box-shadow: none;
  }

  .ant-radio-inner::after {
    background-color: ${(props) => props.theme.main};
  }
  span {
    font-size: 16px;
  }
`;

const Content = styled.div`
  display: flex;
  justify-content: space-between;

  flex-wrap: wrap;
  width: 100%;

  iframe {
    width: 100%;
    border-width: inherit;
    height: 600px;
  }
`;

const Group = styled.div`
  width: 440px;

  border: 1px solid #ececec;
  border-radius: 5px;
  margin: 6px;
  box-shadow: 0px 8px 16px rgba(0, 0, 0, 0.05);
`;
const CustomTitle = styled.div`
  font-family: var(--roboto-500);
  font-size: 16px;
  color: #000000;
  background: #efeff4;
  padding: 11px 10px;
`;

const GroupContent = styled.div`
  padding: 8px;
  max-height: 400px;
  overflow: auto;
`;
const ItemWrap = styled.div`
  font-size: 16px;
`;

const BasicInfo = styled.fieldset`
  padding: 6px 16px 16px 16px;
  border: 1px solid #ececec;
  border-radius: 5px;
  margin-top: 10px;
  legend {
    width: fit-content;
    color: ${(props) => props.theme.main};
    font-size: 16px;
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;
    font-family: var(--roboto-500);
  }
`;
