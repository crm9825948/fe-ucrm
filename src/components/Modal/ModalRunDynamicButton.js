import Button from "antd/lib/button";
import Modal from "antd/lib/modal";
import { useDispatch } from "react-redux";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import Form from "antd/lib/form";
import Input from "antd/lib/input";
import InputNumber from "antd/lib/input-number";
import Select from "antd/lib/select";
import DatePicker from "antd/lib/date-picker";
import _ from "lodash";
import isEmail from "validator/lib/isEmail";
import Editor from "components/Editor/Editor3";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getFormatDate, getFormatDateTime } from "util/staticData";
import moment from "moment";

const normalizePayload = (data, values) => {
  _.map(_.get(data, "button.dynamic_field", []), (field) => {
    if (_.get(field, "type", "") === "date") {
      values = {
        ...values,
        [field.ID]: values[field.ID]
          ? moment(values[field.ID]).format("YYYY-MM-DD")
          : null,
      };
    } else if (_.get(field, "type", "") === "datetime-local") {
      values = {
        ...values,
        [field.ID]: values[field.ID]
          ? moment(values[field.ID]).format("YYYY-MM-DD HH:mm:ss")
          : null,
      };
    }
  });
  return values;
};

const ModalRunDynamicButton = ({
  method,
  data,
  visibleModalRDB,
  $visibleModalRDB,
  dataRecord,
  isFormatSystem,
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const { allConfig } = useSelector((state) => state.datetimeSettingReducer);

  const [content, $content] = useState("");

  const _onSubmit = (values) => {
    dispatch(
      method({
        ...data.data,
        dynamic_field: {
          ...normalizePayload(data, values),
        },
        reloadable: true,
      })
    );
    form.resetFields();
    $content("<p></p>");
    setTimeout(() => {
      $visibleModalRDB(false);
    }, 100);
  };

  const _onCancel = () => {
    form.resetFields();
    $content("<p></p>");
    setTimeout(() => {
      $visibleModalRDB(false);
    }, 100);
  };

  const onChangeEditor = (fieldID) => {
    return (value) => {
      form.setFieldValue([fieldID], value);
    };
  };

  const handlerType = (field) => ({
    textarea: () => (
      <>
        {_.get(field, "is_editor", false) ? (
          <>
            <Form.Item style={{ display: "none" }} name={field.ID}></Form.Item>
            <span>{field.name}</span>
            <Editor
              content={content}
              showAppend={false}
              minHeight={"240px"}
              onChange={onChangeEditor(field.ID)}
            />
          </>
        ) : (
          <Form.Item
            label={field.name}
            key={field.ID}
            name={field.ID}
            rules={[
              {
                required:
                  _.get(field, "required_dynamic_field", false) ||
                  _.get(field, "required", false),
                message: `${t("common.placeholderInput")} ${field.name}!`,
              },
            ]}
          >
            <TextArea />
          </Form.Item>
        )}
      </>
    ),
    number: () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
          () => ({
            validator(_, value) {
              if (value && isNaN(value)) {
                return Promise.reject(`${field.name} has to be a number.`);
              }

              return Promise.resolve();
            },
          }),
        ]}
      >
        <InputNumber
          style={{ width: "100%" }}
          formatter={(value) =>
            `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
          }
        />
      </Form.Item>
    ),
    email: () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
          () => ({
            validator(_, value) {
              if (value) {
                if (isEmail(value) === false) {
                  return Promise.reject(new Error(t("workflow.invalidEmail")));
                }
              }
              return Promise.resolve();
            },
          }),
        ]}
      >
        <Input />
      </Form.Item>
    ),
    text: () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
        ]}
      >
        <Input />
      </Form.Item>
    ),
    select: () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
        ]}
      >
        <Select options={_.get(field, "option", [])} allowClear />
      </Form.Item>
    ),
    date: () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
        ]}
      >
        <DatePicker
          style={{ width: "100%" }}
          format={getFormatDate(allConfig)}
        />
      </Form.Item>
    ),
    "datetime-local": () => (
      <Form.Item
        label={field.name}
        key={field.ID}
        name={field.ID}
        rules={[
          {
            required:
              _.get(field, "required_dynamic_field", false) ||
              _.get(field, "required", false),
            message: `${t("common.placeholderInput")} ${field.name}!`,
          },
        ]}
      >
        <DatePicker
          showTime
          style={{ width: "100%" }}
          format={getFormatDateTime(allConfig)}
        />
      </Form.Item>
    ),
  });

  function getValue(obj, path, defaultValue) {
    let value = _.get(obj, path, defaultValue);
    if (_.isObject(value)) {
      value = _.get(value, "value");
    }
    return value !== null ? value : defaultValue;
  }

  useEffect(() => {
    if (!_.isEmpty(dataRecord)) {
      const formatDate = isFormatSystem
        ? getFormatDate(allConfig)
        : "YYYY-MM-DD";
      const formatDateTime = isFormatSystem
        ? getFormatDateTime(allConfig)
        : "YYYY-MM-DD HH:mm:ss";

      _.map(_.get(data, "button.dynamic_field", []), (field) => {
        if (_.get(field, "is_editor", false)) {
          $content(
            getValue(dataRecord, field.ID, field?.type === "number" ? 0 : "")
          );
        } else if (_.get(field, "type") === "date") {
          if (
            _.get(
              moment(getValue(dataRecord, field.ID, ""), formatDate),
              "_isValid",
              false
            )
          ) {
            form.setFieldValue(
              field.ID,
              moment(getValue(dataRecord, field.ID, ""), formatDate)
            );
          }
        } else if (_.get(field, "type") === "datetime-local") {
          if (
            _.get(
              moment(getValue(dataRecord, field.ID, ""), formatDateTime),
              "_isValid",
              false
            )
          ) {
            form.setFieldValue(
              field.ID,
              moment(getValue(dataRecord, field.ID, ""), formatDateTime)
            );
          }
        } else {
          form.setFieldValue(
            field.ID,
            getValue(dataRecord, field.ID, field?.type === "number" ? 0 : "")
          );
        }
      });
    }
  }, [allConfig, data, dataRecord, form, isFormatSystem]);

  return (
    <CustomModal
      title={t("dynamicButton.updateFields")}
      visible={visibleModalRDB}
      onCancel={_onCancel}
      width={600}
      footer={null}
    >
      <Form form={form} onFinish={_onSubmit} layout="vertical">
        {_.map(_.get(data, "button.dynamic_field", []), (field) =>
          handlerType(field)[_.get(field, "type")]()
        )}
        <Footer>
          <ButtonSave size="large" htmlType="submit">
            {t("common.save")}
          </ButtonSave>
          <ButtonCancel size="large" onClick={_onCancel}>
            {t("common.cancel")}
          </ButtonCancel>
        </Footer>
      </Form>
    </CustomModal>
  );
};

export default withTranslation()(ModalRunDynamicButton);

ModalRunDynamicButton.defaultProps = {
  isFormatSystem: false,
};

const Footer = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 34px;
`;

const ButtonSave = styled(Button)`
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-right: 16px;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const ButtonCancel = styled(Button)`
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomModal = styled(Modal)`
  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;
