import { ArrowLeftOutlined } from "@ant-design/icons";
import backgroundImage from "assets/images/login/login-bg.webp";
import { FE_URL } from "constants/constants";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import _ from "lodash";

const RoleBaseGuard = (props) => {
  const { code, content, decs, children, setting, isEdit } = props;
  const { userDetail, userRuleGlobal, isLoadingUserRuleGlobal } = useSelector(
    (state) => state.userReducer
  );
  const navigate = useNavigate();

  if (userDetail && userDetail.user_root) {
    window.open(FE_URL + "/tenants", "_self");
  } else if (
    !isLoadingUserRuleGlobal &&
    ((!userRuleGlobal.find(
      (item) =>
        _.get(item, "domain", "") === setting && item.actions.includes("view")
    ) &&
      !isEdit) ||
      (!userRuleGlobal.find(
        (item) =>
          _.get(item, "domain", "") === setting && item.actions.includes("edit")
      ) &&
        isEdit))
  ) {
    return (
      <Wrapper>
        <div>
          <div className="code">{code}</div>
          <div className="content">{content}</div>
          <div className="decs">{decs}</div>

          <div style={{ display: "flex", justifyContent: "center" }}>
            <button
              className="backToDashBoard"
              onClick={() => {
                navigate("/dashboard");
              }}
            >
              <ArrowLeftOutlined /> Back to dashboard
            </button>
          </div>
        </div>
      </Wrapper>
    );
  } else return <>{children}</>;
};

export default RoleBaseGuard;

const Wrapper = styled.div`
  height: calc(100vh - 130px);
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
  .code {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 250px;
    line-height: 330px;
    /* identical to box height */

    letter-spacing: 0.01em;

    color: #ffffff;
  }
  .content {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 50px;
    line-height: 59px;
    /* identical to box height */

    letter-spacing: 0.01em;

    color: #ffffff;
    margin-top: 20px;
  }
  .decs {
    font-family: var(--roboto-400);
    font-size: 20px;
    line-height: 23px;
    letter-spacing: 0.01em;

    color: #ffffff;
    margin-top: 40px;
  }
  .backToDashBoard {
    background: ${(props) => props.theme.main};
    /* Primary/6 */

    border: 1px solid ${(props) => props.theme.main};
    box-sizing: border-box;
    /* drop-shadow / button-primary */

    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
    border-radius: 30px;
    padding: 13px;
    color: white;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    text-align: center;

    /* Neutral/1 */

    color: #ffffff;
    margin-top: 56px;
    :hover {
      cursor: pointer;
      background-color: ${(props) => props.theme.darker};
    }
  }
`;
