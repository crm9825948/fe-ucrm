import PropTypes from "prop-types";
import React from "react";
import { ThemeProvider } from "styled-components";
// hooks
import useSettings from "../hooks/useSettings";

ThemePrimaryColor.propTypes = {
  children: PropTypes.node,
};

export default function ThemePrimaryColor({ children }) {
  const { setColor } = useSettings();

  return <ThemeProvider theme={setColor}>{children}</ThemeProvider>;
}
