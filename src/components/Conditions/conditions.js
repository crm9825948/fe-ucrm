import React from "react";
import Condition from "./condition";
import styled from "styled-components";

const Conditions = ({
  title,
  decs,
  conditions,
  setConditions,
  ID,
  dataDetails,
  operatorValue,
  setOperatorValue,
  value,
  setValue,
  background,
}) => {
  return (
    <Wrapper className="required">
      <Condition
        dataDetails={dataDetails}
        title={title}
        decs={decs}
        conditions={conditions}
        setConditions={setConditions}
        ID={ID}
        operatorValue={operatorValue}
        setOperatorValue={setOperatorValue}
        value={value}
        setValue={setValue}
        background={background}
      />
    </Wrapper>
  );
};

export default Conditions;

Conditions.defaultProps = {
  background: "transparent",
};

const Wrapper = styled.div`
  margin-bottom: 16px;
  position: relative;
`;
