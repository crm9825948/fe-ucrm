import React, { useEffect, useState } from "react";
import { Button, Row, Col, Select, Input } from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import styled from "styled-components";
import Typography from "antd/lib/typography";

import TextComp from "./type/text";
import User from "./type/user";
import Linking from "./type/linking";
import Number from "./type/number";
import Date from "./type/date";
import Datetime from "./type/dateTime";
import Email from "./type/email";
import SelectComp from "./type/select";
import Dynamic from "./type/dynamicField";

const { Text } = Typography;

const { Option, OptGroup } = Select;

const operator = {
  Number: [
    {
      label: "Equals",
      value: "$eq",
    },
    {
      label: "Not equals",
      value: "$ne",
    },
    {
      label: "Less than",
      value: "$lt",
    },
    {
      label: "Less than or equal",
      value: "$lte",
    },
    {
      label: "Greater than",
      value: "$gt",
    },
    {
      label: "Greater than or equal",
      value: "$gte",
    },
  ],
  Text: [
    {
      label: "Is empty",
      value: "empty",
    },
    {
      label: "Is not empty",
      value: "not-empty",
    },
    {
      label: "Contains",
      value: "$regex",
    },
    {
      label: "Not contains",
      value: "$not",
    },
    {
      label: "Is one of",
      value: "$in",
    },
    {
      label: "Is not one of",
      value: "$nin",
    },
  ],
  Select: [
    {
      label: "Is empty",
      value: "empty",
    },
    {
      label: "Is not empty",
      value: "not-empty",
    },
    {
      label: "Equals",
      value: "$eq",
    },
    {
      label: "Not equals",
      value: "$ne",
    },
    {
      label: "Is one of",
      value: "$in",
    },
    {
      label: "Is not one of",
      value: "$nin",
    },
  ],
  Field: [
    {
      label: "Equals",
      value: "$eq",
    },
    {
      label: "Not equals",
      value: "$ne",
    },
    {
      label: "Is empty",
      value: "empty",
    },
    {
      label: "Is not empty",
      value: "not-empty",
    },
    {
      label: "Is one of",
      value: "$in",
    },
    {
      label: "Is not one of",
      value: "$nin",
    },
    // {
    //   label: "Less than",
    //   value: "$lt",
    // },
    // {
    //   label: "Less than or equal",
    //   value: "$lte",
    // },
    // {
    //   label: "Greater than",
    //   value: "$gt",
    // },
    // {
    //   label: "Greater than or equal",
    //   value: "$gte",
    // },
  ],
  Date: [
    {
      label: "Before",
      value: "$lt",
    },
    {
      label: "After",
      value: "$gt",
    },
    {
      label: "Equals",
      value: "$eq",
    },
    {
      label: "Not equals",
      value: "$ne",
    },
    {
      label: "Is empty",
      value: "empty",
    },
    {
      label: "Is not empty",
      value: "not-empty",
    },
    {
      label: "Between",
      value: "$between",
    },
    {
      label: "Today",
      value: "today",
    },
    {
      label: "Not today",
      value: "not-today",
    },
    {
      label: "Yesterday",
      value: "yesterday",
    },
    {
      label: "This week",
      value: "this-week",
    },
    {
      label: "Last week",
      value: "last-week",
    },
    {
      label: "This month",
      value: "this-month",
    },
    {
      label: "Last month",
      value: "last-month",
    },
    {
      label: "This year",
      value: "this-year",
    },
    //
    {
      label: "In Working Time",
      value: "$inWorkingTimeUCRM",
    },
    {
      label: "Not In Working Time",
      value: "$ninWorkingTimeUCRM",
    },
    {
      label: "In Working Day",
      value: "$inWorkingDayUCRM",
    },
    {
      label: "Not In Working Day",
      value: "$ninWorkingDayUCRM",
    },
    {
      label: "Is one of",
      value: "$in",
    },
    {
      label: "Is not one of",
      value: "$nin",
    },
  ],
  User: [
    {
      label: "Mine",
      value: "mine",
    },
    {
      label: "Not mine",
      value: "not-mine",
    },
    {
      label: "Is empty",
      value: "empty",
    },
    {
      label: "Is not empty",
      value: "not-empty",
    },
    {
      label: "Equals",
      value: "$eq",
    },
    {
      label: "Not equals",
      value: "$ne",
    },
    {
      label: "Is one of",
      value: "$in",
    },
    {
      label: "Is not one of",
      value: "$nin",
    },
  ],
};

const ConditionItem = ({
  condition,
  fields,
  object_name,
  conditions,
  setConditions,
  index,
  operatorValue,
  setOperatorValue,
  setValue,
  value,
  ID,
  dataDetails,
  init,
}) => {
  const style = { padding: "8px 0" };

  const [type, setType] = useState("");

  const [field, setField] = useState({});

  const handleOperator = (type) => {
    switch (type) {
      case "number":
        return operator["Number"];
      case "text":
      case "id":
        return operator["Text"];
      case "linkingobject":
        return operator["Field"];
      case "lookup":
        return operator["Field"];
      case "date":
        return operator["Date"];
      case "datetime-local":
        return operator["Date"];
      case "user":
        return operator["User"];
      case "select":
        return operator["Select"];
      case "dynamic-field":
        return operator["Select"];
      default:
        return operator["Text"];
    }
  };

  const handleType = (type, operatorValue) => {
    console.log(type);
    switch (type) {
      case "text":
        return (
          <TextComp
            operatorValue={operatorValue[index]}
            disabled={false}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      // case "lookup":
      //   return (
      //     <TextComp
      //       operatorValue={operatorValue[index]}
      //       disabled={false}
      //       conditions={conditions}
      //       setConditions={setConditions}
      //       setValue={setValue}
      //       value={value}
      //       index={index}
      //       dataDetails={dataDetails}
      //     />
      //   );
      case "id":
        return (
          <TextComp
            operatorValue={operatorValue[index]}
            disabled={false}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      case "textarea":
        return (
          <TextComp
            operatorValue={operatorValue[index]}
            disabled={false}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      case "number":
        if (field.type === "formula" && field?.format_weekday) {
          return (
            <SelectComp
              operatorValue={operatorValue[index]}
              field={{
                ...field,
                option: [
                  {
                    label: 0,
                    value: 0,
                  },
                  {
                    label: 1,
                    value: 1,
                  },
                  {
                    label: 2,
                    value: 2,
                  },
                  {
                    label: 3,
                    value: 3,
                  },
                  {
                    label: 4,
                    value: 4,
                  },
                  {
                    label: 5,
                    value: 5,
                  },
                  {
                    label: 6,
                    value: 6,
                  },
                ],
              }}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
        } else
          return (
            <Number
              operatorValue={operatorValue[index]}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
      case "lookup":
        return (
          <Linking
            operatorValue={operatorValue[index]}
            field={field}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      case "linkingobject":
        return (
          <Linking
            operatorValue={operatorValue[index]}
            field={field}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      case "date":
        switch (operatorValue[index]) {
          case "today":
          case "not-today":
          case "yesterday":
          case "this-week":
          case "last-week":
          case "this-month":
          case "last-month":
          case "this-year":
          case "$inWorkingTimeUCRM":
          case "$ninWorkingTimeUCRM":
          case "$inWorkingDayUCRM":
          case "$ninWorkingDayUCRM":
            return (
              <TextComp
                operatorValue={operatorValue[index]}
                disabled={true}
                conditions={conditions}
                setConditions={setConditions}
                setValue={setValue}
                value={value}
                index={index}
                dataDetails={dataDetails}
              />
            );

          case "$between":
            return (
              <Date
                operatorValue={operatorValue[index]}
                disabled={true}
                conditions={conditions}
                setConditions={setConditions}
                setValue={setValue}
                value={value}
                index={index}
                dataDetails={dataDetails}
                init={init}
              />
            );
          default:
            break;
        }
        return (
          <Date
            operatorValue={operatorValue[index]}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
            ID={ID}
            init={init}
            style={{ width: "100%" }}
          />
        );
      case "datetime-local":
        switch (operatorValue[index]) {
          case "today":
          case "not-today":
          case "yesterday":
          case "this-week":
          case "last-week":
          case "this-month":
          case "last-month":
          case "this-year":
          case "$inWorkingTimeUCRM":
          case "$ninWorkingTimeUCRM":
          case "$inWorkingDayUCRM":
          case "$ninWorkingDayUCRM":
            return (
              <TextComp
                operatorValue={operatorValue[index]}
                disabled={true}
                conditions={conditions}
                setConditions={setConditions}
                setValue={setValue}
                value={value}
                index={index}
                dataDetails={dataDetails}
              />
            );
          case "$between":
            return (
              <Datetime
                operatorValue={operatorValue[index]}
                disabled={true}
                conditions={conditions}
                setConditions={setConditions}
                setValue={setValue}
                value={value}
                index={index}
                dataDetails={dataDetails}
                ID={ID}
                init={init}
              />
            );
          default:
            break;
        }
        return (
          <Datetime
            operatorValue={operatorValue[index]}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
            ID={ID}
            init={init}
          />
        );
      case "user":
        return (
          <User
            operatorValue={operatorValue[index]}
            field={field}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
            ID={ID}
          />
        );
      case "email":
        return (
          <Email
            operatorValue={operatorValue[index]}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );
      case "select":
        if (field.type === "formula" && field.format_weekday === 1) {
          return (
            <SelectComp
              operatorValue={operatorValue[index]}
              field={{
                ...field,
                option: [
                  {
                    label: "Monday",
                    value: "Monday",
                  },
                  {
                    label: "Tuesday",
                    value: "Tuesday",
                  },
                  {
                    label: "Wednesday",
                    value: "Wednesday",
                  },
                  {
                    label: "Thursday",
                    value: "Thursday",
                  },
                  {
                    label: "Friday",
                    value: "Friday",
                  },
                  {
                    label: "Saturday",
                    value: "Saturday",
                  },
                  {
                    label: "Sunday",
                    value: "Sunday",
                  },
                ],
              }}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
        } else if (field.type === "formula" && field.format_weekday === 2) {
          return (
            <SelectComp
              operatorValue={operatorValue[index]}
              field={{
                ...field,
                option: [
                  {
                    label: "Thứ 2",
                    value: "Thứ 2",
                  },
                  {
                    label: "Thứ 3",
                    value: "Thứ 3",
                  },
                  {
                    label: "Thứ 4",
                    value: "Thứ 4",
                  },
                  {
                    label: "Thứ 5",
                    value: "Thứ 5",
                  },
                  {
                    label: "Thứ 6",
                    value: "Thứ 6",
                  },
                  {
                    label: "Thứ 7",
                    value: "Thứ 7",
                  },
                  {
                    label: "Chủ Nhật",
                    value: "Chủ Nhật",
                  },
                ],
              }}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
        } else if (field.type === "formula" && field.format_weekday === 3) {
          return (
            <SelectComp
              operatorValue={operatorValue[index]}
              field={{
                ...field,
                option: [
                  {
                    label: "Mon",
                    value: "Mon",
                  },
                  {
                    label: "Tue",
                    value: "Tue",
                  },
                  {
                    label: "Wed",
                    value: "Wed",
                  },
                  {
                    label: "Thu",
                    value: "Thu",
                  },
                  {
                    label: "Fri",
                    value: "Fri",
                  },
                  {
                    label: "Sat",
                    value: "Sat",
                  },
                  {
                    label: "Sun",
                    value: "Sun",
                  },
                ],
              }}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
        } else
          return (
            <SelectComp
              operatorValue={operatorValue[index]}
              field={field}
              conditions={conditions}
              setConditions={setConditions}
              setValue={setValue}
              value={value}
              index={index}
              dataDetails={dataDetails}
            />
          );
      case "dynamic-field":
        return (
          <Dynamic
            operatorValue={operatorValue[index]}
            field={field}
            conditions={conditions}
            setConditions={setConditions}
            setValue={setValue}
            value={value}
            index={index}
            dataDetails={dataDetails}
          />
        );

      default:
        return <Input disabled />;
    }
  };

  useEffect(() => {
    // if (ID) {
    setType(condition.type);
    /*eslint-disable-next-line*/
    Object.entries(fields).forEach(([key, values], index) => {
      /*eslint-disable-next-line*/
      values.forEach((field, idx) => {
        if (field.type === "linkingobject") {
          if (
            condition.id_field === field.ID ||
            condition.id_field === field.full_field_id
          ) {
            setField(field);
          }
        } else if (
          condition.id_field === field.ID ||
          condition.id_field === field.full_field_id
        ) {
          setField(field);
        }
      });
    });
    //
    // }
  }, [ID, condition, fields]);

  const unitDateToParts = [
    "year",
    "month",
    "date",
    "hour",
    "minute",
    "second",
    "millisecond",
  ];

  return (
    <CustomCondition>
      <Row gutter={8}>
        <Col className="gutter-row" span={7}>
          <div style={style}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(inputValue, option) => {
                if (option.field) {
                  if (option.field.related_name) {
                    return option?.field?.related_name
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  } else if (option.field.name) {
                    return option?.field?.name
                      .toLowerCase()
                      .indexOf(inputValue.toLowerCase()) >= 0
                      ? true
                      : false;
                  }
                }
              }}
              style={{ width: "100%" }}
              placeholder={"Please select"}
              onChange={(e, values) => {
                setType(values && values.field.type);
                if (values && values.field.type !== "formula") {
                  setType(values.field.type);
                  setField(values.field);
                  let conditionsTemp = [...conditions];
                  let newItem = {
                    id_field: e,
                    value: {},
                    type: values.field.type,
                  };
                  conditionsTemp[index] = { ...newItem };
                  let temp = [...operatorValue];
                  temp[index] = undefined;
                  setOperatorValue(temp);
                  setConditions(conditionsTemp);

                  let valueTmp = [...value];
                  valueTmp[index] = "";

                  setValue(valueTmp);
                } else if (values && values.field.type === "formula") {
                  let typeTemp = "";
                  console.log(values.field.formula_type);
                  switch (values.field.formula_type) {
                    case "dateAdd":
                    case "dateSub":
                      typeTemp = "datetime-local";
                      setType("datetime-local");
                      break;

                    case "add":
                    case "subtract":
                    case "multiply":
                    case "divide":
                    case "mod":
                    case "min":
                    case "max":
                      typeTemp = "number";
                      setType("number");
                      break;
                    case "dateDiff":
                      if (values.field?.in_words) {
                        typeTemp = "text";
                        setType("text");
                        break;
                      } else {
                        typeTemp = "number";
                        setType("number");
                        break;
                      }
                    case "dateToParts":
                      if (
                        values.field.format_weekday === 4 ||
                        unitDateToParts.includes(values.field?.unit)
                      ) {
                        typeTemp = "number";
                        setType("number");
                      } else {
                        typeTemp = "select";
                        setType("select");
                      }
                      break;
                    default:
                      typeTemp = "text";
                      setType("text");
                      break;
                  }
                  // setType(values.field.type);
                  setField(values.field);
                  let conditionsTemp = [...conditions];
                  let newItem = {
                    id_field: e,
                    value: {},
                    type: typeTemp,
                  };
                  conditionsTemp[index] = { ...newItem };
                  let temp = [...operatorValue];
                  temp[index] = undefined;
                  setOperatorValue(temp);
                  setConditions(conditionsTemp);

                  let valueTmp = [...value];
                  valueTmp[index] = "";

                  setValue(valueTmp);
                }
              }}
              value={conditions[index].id_field}
              allowClear
              onClear={() => {
                setType("");
                setField({});
                let conditionsTemp = [...conditions];
                let newItem = {
                  id_field: "",
                  value: {},
                  type: "",
                };
                conditionsTemp[index] = { ...newItem };
                let temp = [...operatorValue];
                temp[index] = undefined;
                setOperatorValue(temp);
                setConditions(conditionsTemp);

                let valueTmp = [...value];
                valueTmp[index] = "";
                setValue(valueTmp);
              }}
            >
              {Object.entries(fields).map(([key, value], idx) => {
                if (
                  key !== "main_object" &&
                  !window.location.pathname.includes("duplicate-rules")
                ) {
                  return (
                    <OptGroup label={key}>
                      {/*eslint-disable-next-line*/}
                      {value.map((field, idx) => {
                        if (
                          field.hidden === false &&
                          field.permission_hidden === false &&
                          field.type !== "file" &&
                          field?.encrypted !== true
                        )
                          return (
                            <Option
                              value={field.full_field_id}
                              field={field}
                              type={field.type}
                              key={field.full_field_id + Math.random() + ""}
                            >
                              <Text
                                ellipsis={{
                                  tooltip: field.name,
                                }}
                                style={{ width: "100%" }}
                              >
                                {key}.{field.name}
                              </Text>
                            </Option>
                          );
                      })}
                    </OptGroup>
                  );
                } else if (key === "main_object") {
                  return (
                    <OptGroup label={`${object_name} (Main)`}>
                      {/*eslint-disable-next-line*/}
                      {value.map((field, idx) => {
                        if (
                          window.location.pathname.includes("duplicate-rules")
                        ) {
                          if (
                            field.type === "text" ||
                            field.type === "number" ||
                            field.type === "email" ||
                            (field.type === "select" &&
                              field.multiple === false)
                          ) {
                            if (field?.encrypted !== true)
                              return (
                                <Option
                                  value={field.ID}
                                  field={field}
                                  type={field.type}
                                  key={
                                    field.full_field_id +
                                    Math.random() +
                                    key +
                                    ""
                                  }
                                >
                                  {" "}
                                  <Text
                                    ellipsis={{
                                      tooltip: field.name,
                                    }}
                                    style={{ width: "100%" }}
                                  >
                                    {field.name}
                                  </Text>
                                </Option>
                              );
                          }
                        } else if (
                          field.hidden === false &&
                          field.permission_hidden === false &&
                          field.type !== "file" &&
                          field?.encrypted !== true
                        )
                          return (
                            <Option
                              value={field.ID}
                              field={field}
                              type={field.type}
                              key={
                                field.full_field_id + Math.random() + key + ""
                              }
                            >
                              {" "}
                              <Text
                                ellipsis={{
                                  tooltip: field.name,
                                }}
                                style={{ width: "100%" }}
                              >
                                {field.name}
                              </Text>
                            </Option>
                          );
                      })}
                    </OptGroup>
                  );
                }
                return null;
              })}
            </Select>
          </div>
        </Col>

        <Col className="gutter-row" span={7}>
          <div style={style}>
            <Select
              showSearch
              style={{ width: "100%" }}
              placeholder={"Please select"}
              onChange={(e) => {
                let temp = [...operatorValue];
                temp[index] = e;
                setOperatorValue(temp);

                let valueTmp = [...value];
                valueTmp[index] = "";

                setValue(valueTmp);
              }}
              // allowClear
              onClear={() => {
                setOperatorValue("");
              }}
              disabled={conditions[index].id_field ? false : true}
              value={operatorValue[index]}
            >
              {handleOperator(type).map((item, idx) => {
                return (
                  <Option
                    value={item.value}
                    key={idx}
                    disabled={
                      (type === "date" && item.label === "In Working Time") ||
                      (type === "date" &&
                        item.label === "Not In Working Time") ||
                      (type === "text" && item.label === "Is one of") ||
                      (type === "text" && item.label === "Is not one of")
                    }
                  >
                    <Text
                      ellipsis={{
                        tooltip: item.label,
                      }}
                      style={{ width: "100%" }}
                    >
                      {item.label}
                    </Text>
                  </Option>
                );
              })}
            </Select>
          </div>
        </Col>
        <Col className="gutter-row" span={7}>
          <CustomCol style={style}>
            {operatorValue[index] ? (
              handleType(type, operatorValue)
            ) : (
              <Input disabled />
            )}
          </CustomCol>
        </Col>
        <Col className="gutter-row" span={3}>
          <div style={style}>
            <Button
              className="delete-btn"
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
              onClick={() => {
                let tmp = [...conditions];
                tmp.splice(index, 1);

                let temp = [...operatorValue];
                temp.splice(index, 1);

                let valueTmp = [...value];
                valueTmp.splice(index, 1);
                setOperatorValue(temp);
                setConditions(tmp);
                setValue(valueTmp);
              }}
            >
              <img alt="" src={deleteImg} style={{ width: "20px" }} />
            </Button>
          </div>
        </Col>
      </Row>
    </CustomCondition>
  );
};

export default ConditionItem;

const CustomCondition = styled.div`
  padding-left: 19px;
  padding-right: 19px;
  .delete-btn {
    :hover {
      border-color: ${(props) => props.theme.main};
    }
  }
`;

const CustomCol = styled.div`
  .ant-picker {
    width: 100%;
  }

  .ant-select {
    width: 100%;
  }

  .ant-input {
    width: 100%;
  }

  .ant-input-number {
    width: 100%;
  }
`;
