import { Input, Select } from "antd";
import React, { useEffect } from "react";
import styled from "styled-components";

const Email = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
}) => {
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return operatorValue === "$in" || operatorValue === "$nin" ? (
    <Wrapper>
      <Select
        mode="tags"
        style={{
          width: "100%",
        }}
        value={value[index] === "" ? [] : value[index]}
        onChange={(e) => {
          let tmpConditions = [...conditions];
          let newTmpConditions = [];
          // handle contain and not contain
          if (operatorValue === "$regex") {
            tmpConditions[index].value = {
              [operatorValue]: e.target.value,
              $options: "i",
            };
          } else if (operatorValue === "$not") {
            tmpConditions[index].value = {
              $not: {
                $regex: e.target.value,
                $options: "i",
              },
            };
          } else {
            tmpConditions.forEach((item, idx) => {
              newTmpConditions[idx] = { ...item };
            });
            newTmpConditions[index].value = {
              [operatorValue]: e,
            };

            // tmpConditions[index].value = {
            //   [operatorValue]: e,
            // };
          }

          let tmp = [...value];
          tmp[index] = e;
          setValue(tmp);

          setConditions(newTmpConditions);
        }}
        options={[]}
      />
    </Wrapper>
  ) : (
    <Wrapper>
      <Input
        disabled={
          disabled || operatorValue === "empty" || operatorValue === "not-empty"
        }
        type="email"
        value={value[index]}
        maxLength={1000}
        onChange={(e) => {
          let tmpConditions = [...conditions];
          // handle contain and not contain
          if (operatorValue === "$regex") {
            tmpConditions[index].value = {
              [operatorValue]: e.target.value,
              $options: "i",
            };
          } else if (operatorValue === "$not") {
            tmpConditions[index].value = {
              $not: {
                $regex: e.target.value,
                $options: "i",
              },
            };
          } else {
            tmpConditions[index].value = {
              [operatorValue]: e.target.value,
            };
          }

          let tmp = [...value];
          tmp[index] = e.target.value;
          setValue(tmp);

          setConditions(tmpConditions);
        }}
      />
      {/* </Form.Item> */}
      {/* </Form> */}
    </Wrapper>
  );
};

export default Email;

const Wrapper = styled.div`
  .ant-form-item {
    margin-bottom: 0px;
  }
`;
