import {
  SearchOutlined,
  CaretDownOutlined,
  CaretUpOutlined,
} from "@ant-design/icons";
import { useTranslation } from "react-i18next";
import {
  Button,
  Drawer,
  Input,
  // Pagination,
  Space,
  Table,
  Select,
  Radio,
  Spin,
  Typography,
} from "antd";
import deleteImg from "assets/icons/common/delete-icon.png";
import editImg from "assets/icons/common/icon-edit.png";
import React, { useEffect, useState } from "react";
import Highlighter from "react-highlight-words";
import { useDispatch, useSelector } from "react-redux";
import {
  loadDataLinking,
  // loadPaginationLinking,
  loadPaginationSuccess,
  loadPagination,
} from "redux/slices/objects";
import LeftPagi from "assets/icons/objects/pagi-left.png";
import RightPagi from "assets/icons/objects/pagi-right.png";
import Reload from "assets/icons/objects/reload.png";
import styled from "styled-components";

const { Text: TextComponent } = Typography;

const { Option } = Select;

const LinkingList = (props) => {
  const {
    visible,
    setVisible,
    field,
    selectedRowKeys,
    setSelectedRowKeys,
    handleSelect,
    setSelectedRows,
    selectedRows,
    conditions,
    index,
  } = props;

  const { t } = useTranslation();

  const [recordPerPage] = useState(50);
  const [currentPage, setCurrentPage] = useState(1);
  const [searchList, setSearchList] = useState({});
  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearcherColumn] = useState("");

  const [firstID, setFirstID] = useState(null);
  const [lastID, setLastID] = useState(null);

  const [next, setNext] = useState(1);
  const [prev, setPrev] = useState(1);

  const dispatch = useDispatch();

  const {
    headerLinking,
    dataLinking,
    // recordData,
    isLoadingDataLinking,
    isLoadingPagi,
    totalRecord,
  } = useSelector((state) => state.objectsReducer);

  const [dateSource, setDataSource] = useState([]);
  const [column, setColumn] = useState([]);

  const [listColumn, setListColumn] = useState([]);
  const [listSearch, setListSearch] = useState([]);
  const [sortBy, setSortBy] = useState({});
  const [source, setSource] = useState([]);
  const { defaultBrandName } = useSelector((state) => state.brandNameReducer);

  useEffect(() => {
    let arrColumn = [...headerLinking];
    setListColumn(arrColumn);
  }, [headerLinking]);

  useEffect(() => {
    let newData = [];

    dataLinking.forEach((record) => {
      let newRecord = {};
      newRecord["_id"] = record._id;

      listColumn.forEach((col) => {
        let flag = 0;
        Object.entries(record).forEach(([key, value]) => {
          if (col.ID === key) {
            flag = 1;
            if (typeof value === "object" && value !== null) {
              newRecord[key] = value.value;
            } else {
              newRecord[key] = value;
            }
          }
        });
        if (flag === 0) {
          newRecord[col.ID] = null;
        }
      });
      newData.push(newRecord);
    });

    setSource(newData);
  }, [dataLinking, listColumn]);

  useEffect(() => {
    let newData = [];
    /* eslint-disable-next-line */
    dataLinking.map((item, index) => {
      let newItem = {};
      newItem["key"] = item._id;
      /* eslint-disable-next-line */
      Object.entries(item).forEach(([key, value], idx) => {
        if (typeof value === "object" && value !== null) {
          newItem[key] = value.value;
        } else {
          newItem[key] = value;
        }
      });
      newData.push(newItem);
    });
    setDataSource(newData);
  }, [dataLinking]);

  useEffect(() => {
    let newColumn = [];
    /* eslint-disable-next-line */
    headerLinking.map((item, idx) => {
      let newItem = {
        title: item.name,
        dataIndex: item.ID,
        key: item.ID,
        width: "maxContent",
        editable: true,
        type: item.type,
        ...getColumnSearchProps(item.ID, idx, item.name, searchList, item.type),
      };
      newColumn.push(newItem);
    });
    newColumn.push({
      title: "Action",
      key: "action",
      fixed: "right",
      render: () => (
        <Space size="middle">
          <a href>
            <img alt="" src={editImg} style={{ width: "20px" }} />
          </a>
          <a className="ant-dropdown-link" href>
            <img alt="" src={deleteImg} style={{ width: "20px" }} />
          </a>
        </Space>
      ),
    });
    setColumn(newColumn);
    /* eslint-disable-next-line */
  }, [headerLinking, searchList]);

  const mapRecordbyId = (id) => {
    let record = {};
    dataLinking.forEach((item) => {
      if (item._id === id) {
        Object.entries(item).forEach(([key, value]) => {
          if (value !== null && typeof value === "object") {
            record[key] = value.value;
          } else {
            record[key] = value;
          }
        });
      }
    });
    return record;
  };

  const onCloseList = () => {
    setVisible(false);
    setSelectedRowKeys([]);
  };

  const getColumnSearchProps = (dataIndex, idx, name, searchList, type) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${name}`}
          value={selectedKeys[0]}
          onChange={(e) => {
            setSelectedKeys(e.target.value ? [e.target.value] : []);
          }}
          onPressEnter={() => {
            handleSearch(selectedKeys, confirm, dataIndex);
          }}
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() => {
              handleSearch(selectedKeys, confirm, dataIndex);
            }}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </CustomButtonCancel>
          <CustomButtonCancel
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });

              setSearchText(selectedKeys[0]);
              setSearcherColumn(dataIndex);
            }}
          >
            Filter
          </CustomButtonCancel>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text, ...props) => {
      /* eslint-disable-next-line */
      const expression =
        /*eslint-disable-next-line*/
        /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
      const regex = new RegExp(expression);
      if (text && typeof text === "object") {
        return text.map((ele, idx) => {
          if (ele !== null && ele.match(regex)) {
            let fileNew = ele.split("/");
            return (
              <a style={{ display: "block" }} href>
                {fileNew[fileNew.length - 1]}
              </a>
            );
          } else {
            return searchedColumn === dataIndex ? (
              <Highlighter
                highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text ? text.toString() : ""}
              />
            ) : (
              text
            );
          }
        });
      } else {
        return searchedColumn === dataIndex ? (
          <Highlighter
            highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
            searchWords={[searchText]}
            autoEscape
            textToHighlight={text ? text.toString() : ""}
          />
        ) : (
          text
        );
      }
    },
  });

  const handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    let searchTemp = { ...searchList };
    searchTemp[dataIndex] = selectedKeys[0];
    setSearchList(searchTemp);
    let searchData = [];
    /* eslint-disable-next-line */
    Object.entries(searchTemp).forEach(([key, value], index) => {
      if (value) {
        let newItem = {
          id_field: key,
          value: value,
        };
        searchData.push(newItem);
      }
    });
    reloadData(searchData, currentPage, recordPerPage);
    setSearchText(selectedKeys[0]);
    setSearcherColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  const reloadData = (searchData, currentPage, recordPerPage, sortBy) => {
    dispatch(
      loadDataLinking({
        object_id: field.objectname,
        first_record_id: firstID,
        last_record_id: lastID,
        search_with: {
          meta: [],
          data: searchData,
        },
        // sort_by: sortBy,
      })
    );
    // dispatch(
    //   loadPaginationLinking({
    //     object_id: field.objectname,
    //     current_page: currentPage,
    //     record_per_page: recordPerPage,
    //     search_with: {
    //       meta: [],
    //       data: searchData,
    //     },
    //     filter: [],
    //   })
    // );
    dispatch(
      loadPaginationSuccess({
        total_page: null,
        total_record: null,
      })
    );
  };

  const onSelectChange = (selectedRowKeys, selectedRows) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRows(selectedRows);
  };

  const handleSortNew = (sortTmp) => {
    reloadData(listSearch, currentPage, recordPerPage, sortTmp);
  };

  const handldeChangeListSearch = (id, value) => {
    let temp = [...listSearch];
    if (value) {
      let flag = 0;
      temp.forEach((item, index) => {
        if (item.id_field === id) {
          flag = 1;
          temp[index].value = value;
        }
      });
      if (flag === 0) {
        temp.push({ id_field: id, value: value });
      }
    } else {
      temp.forEach((item, index) => {
        if (item.id_field === id) {
          temp.splice(index, 1);
        }
      });
    }

    if (!value) {
      reloadData(temp, currentPage, recordPerPage, sortBy);
    }

    dispatch(
      loadPaginationSuccess({
        total_page: null,
        total_record: null,
      })
    );
    setListSearch(temp);
  };

  const handleSearchNew = (id, value) => {
    let temp = [...listSearch];
    if (value) {
      let flag = 0;
      temp.forEach((item, index) => {
        if (item.id_field === id) {
          flag = 1;
          temp[index].value = value;
        }
      });
      if (flag === 0) {
        temp.push({ id_field: id, value: value });
      }
    } else {
      temp.forEach((item, index) => {
        if (item.id_field === id) {
          temp.splice(index, 1);
        }
      });
    }

    setListSearch(temp);

    reloadData(temp, currentPage, recordPerPage, sortBy);
  };

  const renderFieldFile = (value) => {
    /* eslint-disable-next-line */
    const expression =
      /*eslint-disable-next-line*/
      /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]+)?/gi;
    const regex = new RegExp(expression);
    if (value) {
      /*eslint-disable-next-line*/
      return value.map((item, index) => {
        if (item !== null && item.match(regex)) {
          let fileNew = item.split("/");
          return (
            <a
              key={index}
              style={{ display: "block" }}
              href={item}
              target={"_blank"}
              rel="noreferrer"
            >
              {fileNew[fileNew.length - 1]}
            </a>
          );
        }
      });
    }
  };

  function numberWithCommas(x, col) {
    if (x === 0) {
      return "0";
    } else {
      x = x.toFixed(
        col?.decimal_separator === 0 ? 0 : col?.decimal_separator || 3
      );
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  const renderTable = () => {
    return (
      <table>
        <thead className="table-header">
          <tr>
            <th></th>
            {listColumn.map((item, index) => {
              return (
                <th key={index}>
                  <ContentHeader>
                    <TitleWrap>
                      <span>{item.name}</span>
                      <IconHeader className="hidden__component">
                        <CaretUpOutlined
                          style={{
                            color: `${
                              sortBy[item.ID] === 1
                                ? defaultBrandName.theme_color
                                : "rgba(0, 0, 0, 0.25)"
                            }`,
                          }}
                          onClick={() => {
                            let sortTmp = { ...sortBy };
                            if (sortTmp[item.ID] === 1) {
                              delete sortTmp[item.ID];
                            } else {
                              sortTmp[item.ID] = 1;
                            }
                            handleSortNew(sortTmp);
                            setSortBy(sortTmp);
                          }}
                        />
                        <CaretDownOutlined
                          style={{
                            color: `${
                              sortBy[item.ID] === -1
                                ? defaultBrandName.theme_color
                                : "rgba(0, 0, 0, 0.25)"
                            }`,
                          }}
                          onClick={() => {
                            let sortTmp = { ...sortBy };
                            if (sortTmp[item.ID] === -1) {
                              delete sortTmp[item.ID];
                            } else {
                              sortTmp[item.ID] = -1;
                            }
                            handleSortNew(sortTmp);
                            setSortBy(sortTmp);
                          }}
                        />
                      </IconHeader>
                    </TitleWrap>
                    <SearchWrap className="hidden__component">
                      {item.type !== "select" ? (
                        <CustomSearch
                          value={
                            listSearch.find((val) => val.id_field === item.ID)
                              ?.value || ""
                          }
                          onChange={(value) => {
                            handldeChangeListSearch(
                              item.ID,
                              value.target.value
                            );
                          }}
                          allowClear={true}
                          onPressEnter={(value) => {
                            handleSearchNew(item.ID, value.target.value);
                          }}
                          placeholder={`${t("common.search")} ${item.name}`}
                        />
                      ) : (
                        <CustomSelect
                          value={
                            listSearch.find((val) => val.id_field === item.ID)
                              ?.value || undefined
                          }
                          showSearch
                          allowClear={true}
                          placeholder={`Search ${item.name}`}
                          onChange={(value) => {
                            handleSearchNew(item.ID, value);
                          }}
                          style={{ marginBottom: 8, display: "block" }}
                        >
                          {item &&
                            item.option &&
                            item.option.map((op, index) => {
                              return (
                                <Option key={index} value={op.label}>
                                  {op.label}
                                </Option>
                              );
                            })}
                        </CustomSelect>
                      )}
                    </SearchWrap>
                  </ContentHeader>
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody className="table-body">
          {source.map((record, index) => {
            return (
              <tr key={index}>
                <CustomTd check={selectedRowKeys[0] === record._id}>
                  <Radio
                    // disabled={
                    //   selectedRowKeysOutside
                    //     ? selectedRowKeysOutside.indexOf(record._id) < 0
                    //       ? false
                    //       : true
                    //     : recordData && recordData._id === record._id
                    //     ? true
                    //     : false
                    // }
                    checked={selectedRowKeys[0] === record._id}
                    onChange={() => {
                      setSelectedRowKeys([record._id]);
                      setSelectedRows([mapRecordbyId(record._id)]);
                    }}
                  ></Radio>
                </CustomTd>
                {listColumn.map((col) => {
                  // eslint-disable-next-line
                  return Object.entries(record).map(([key, value]) => {
                    if (col.ID === key) {
                      return (
                        <CustomTd
                          style={{
                            textAlign: `${
                              col.type === "number" ||
                              (col.type === "formula" &&
                                col.formula_type === "advanceExpression")
                                ? "right"
                                : "left"
                            }`,
                          }}
                          key={index}
                          check={selectedRowKeys[0] === record._id}
                        >
                          {typeof value === "object" ? (
                            <TextComponent ellipsis={{ tooltip: value }}>
                              {renderFieldFile(value)}
                            </TextComponent>
                          ) : (
                            <TextComponent ellipsis={{ tooltip: value }}>
                              {/* {value} */}
                              {col.type === "number" ||
                              (col.type === "formula" &&
                                col.formula_type === "advanceExpression")
                                ? numberWithCommas(value, col)
                                : value}
                            </TextComponent>
                          )}
                        </CustomTd>
                      );
                    }
                  });
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  useEffect(() => {
    setNext(
      source.length === recordPerPage
        ? currentPage * recordPerPage
        : (currentPage - 1) * recordPerPage + source.length
    );
    /* eslint-disable-next-line */
  }, [source]);

  useEffect(() => {
    setPrev(currentPage * recordPerPage - recordPerPage + 1);
    /* eslint-disable-next-line */
  }, [currentPage]);

  return (
    <>
      <Drawer
        title="Linking list"
        placement="right"
        onClose={onCloseList}
        visible={visible}
        width={1000}
      >
        <Wrapper>
          <NewCustomPagination>
            <>
              {isLoadingDataLinking ? (
                <Spin />
              ) : (
                <div className="total-record">
                  {next === 0 ? 0 : prev} - {next}{" "}
                  {isLoadingPagi ? (
                    <Spin style={{ marginLeft: "10px" }} />
                  ) : totalRecord === null ? (
                    <div
                      className="reload-pagi"
                      onClick={() => {
                        let temp = [...listSearch];
                        let tempMeta = [];
                        let tempData = [];
                        temp.forEach((item) => {
                          if (
                            item.id_field === "created_date" ||
                            item.id_field === "created_by" ||
                            item.id_field === "modify_time" ||
                            item.id_field === "owner" ||
                            item.id_field === "modify_by"
                          ) {
                            tempMeta.push(item);
                          } else {
                            tempData.push(item);
                          }
                        });
                        // let dataFilter = [];
                        // /* eslint-disable-next-line */
                        // Object.entries(dataFilterLinking).forEach(
                        //   ([key, value], index) => {
                        //     if (
                        //       value &&
                        //       value.id_field_related_record !== null
                        //     ) {
                        //       dataFilter.push(value);
                        //     }
                        //   }
                        // );
                        dispatch(
                          loadPagination({
                            current_page: 1,
                            record_per_page: 50,
                            object_id: field.objectname,
                            search_with: {
                              meta: tempMeta,
                              data: tempData,
                            },
                            // filter: field.independent_linking ? [] : dataFilter,
                          })
                        );
                      }}
                    >
                      <img alt="" src={Reload} />
                    </div>
                  ) : (
                    `of ${totalRecord ? totalRecord : 0} records`
                  )}
                </div>
              )}
            </>

            <div
              className="left-pagi"
              style={{
                pointerEvents: `${currentPage === 1 ? "none" : ""}`,
                cursor: `${currentPage === 1 ? "not-allowed" : "pointer"}`,
                opacity: `${currentPage === 1 ? 0.5 : 1}`,
              }}
              onClick={() => {
                setLastID(
                  source.length > 0 ? source[source.length - 1]._id : null
                );
                let currentPageTemp = currentPage;
                currentPageTemp = currentPageTemp - 1;
                setCurrentPage(currentPageTemp);
                // setPrev(currentPageTemp * recordPerPage - recordPerPage + 1);
                // setNext(
                //   dataSource.length === recordPerPage
                //     ? currentPageTemp * recordPerPage
                //     : (currentPageTemp - 1) * recordPerPage + dataSource.length
                // );
                setFirstID(source.length > 0 ? source[0]._id : null);
                reloadData(
                  listSearch,
                  source.length > 0 ? source[0]._id : null,
                  null,
                  sortBy
                );
              }}
            >
              <img alt="" src={LeftPagi} />
            </div>
            <div
              className="right-pagi"
              style={{
                pointerEvents: `${source.length < recordPerPage ? "none" : ""}`,
                cursor: `${
                  source.length < recordPerPage ? "not-allowed" : "pointer"
                }`,
                opacity: `${source.length < recordPerPage ? 0.5 : 1}`,
              }}
              onClick={() => {
                setLastID(
                  source.length > 0 ? source[source.length - 1]._id : null
                );
                let currentPageTemp = currentPage;
                currentPageTemp = currentPageTemp + 1;
                setCurrentPage(currentPageTemp);
                setFirstID(source.length > 0 ? source[0]._id : null);
                reloadData(
                  listSearch,
                  null,
                  source.length > 0 ? source[source.length - 1]._id : null,
                  sortBy
                );
              }}
            >
              <img alt="" src={RightPagi} />
            </div>
          </NewCustomPagination>

          <Table
            style={{ display: "none" }}
            // bordered
            dataSource={dateSource}
            columns={column}
            rowClassName="editable-row"
            pagination={false}
            rowSelection={{
              selectedRowKeys: conditions[index].key
                ? [conditions[index].key]
                : selectedRowKeys,
              onChange: onSelectChange,
              type: "radio",
            }}
            scroll={{
              x: "max-content",
            }}
          />
          <TableWrap>{renderTable()}</TableWrap>
          <div>
            {/* <CustomPagination
              showQuickJumper
              current={currentPage}
              total={totalRecordLinking}
              showSizeChanger
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              // pageSize={recordPerPage}
              onChange={(e, pageSize) => {
                setCurrentPage(e);
                setRecordPerPage(pageSize);

                reloadData(listSearch, e, pageSize, sortBy);
              }}
            /> */}
            {selectedRowKeys.length > 0 ? (
              <WrapperSave>
                <CustomButtonSave
                  onClick={() => {
                    handleSelect(selectedRowKeys, selectedRows);
                  }}
                >
                  Save
                </CustomButtonSave>
                <CustomButtonCancel
                  onClick={() => {
                    setVisible(false);
                  }}
                >
                  Cancel
                </CustomButtonCancel>
              </WrapperSave>
            ) : (
              ""
            )}
          </div>
        </Wrapper>
      </Drawer>
    </>
  );
};

export default LinkingList;

const WrapperSave = styled.div`
  position: fixed;
  bottom: -8px;
  right: 826px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
  z-index: 3;
`;

// const CustomPagination = styled(Pagination)`
//   position: fixed;
//   bottom: -8px;
//   right: 16px;
//   width: 976px;
//   text-align: right;
//   background-color: #fff;
//   height: 100px;
//   padding-top: 16px;
//   .ant-pagination-item-active {
//     border-color: ${(props) => props.theme.main};
//     a {
//       color: ${(props) => props.theme.main};
//     }
//   }
//   .ant-pagination-item:hover {
//     border-color: ${(props) => props.theme.main};
//     a {
//       color: ${(props) => props.theme.main};
//     }
//   }
// `;

const Wrapper = styled.div`
  table {
    width: max-content;
    th {
      width: max-content;
      .ant-table-column-title {
        margin-right: 30px;
      }
    }
  }
  padding-bottom: 100px;
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  border-color: ${(props) => props.theme.main};
  margin-right: 16px;
  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;

  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const TableWrap = styled.div`
  width: 100%;
  /* height: calc(100% - 187px); */
  height: 100%;

  border-left: 1px solid #ddd;
  border-right: 1px solid #ddd;
  background: #fff;
  overflow-x: auto;
  overflow-y: auto;
  max-height: calc(100vh - 180px);
  &::-webkit-scrollbar {
    height: 8px !important;
  }

  table {
    /* width: 100%; */
    min-width: 100%;
    width: max-content;
    table-layout: auto;

    thead {
      position: sticky;
      top: 0;
      z-index: 3;
    }
    .table-header th {
      text-align: left;

      box-shadow: inset 1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
        inset 0px -1px 0px #d9d9d9;

      overflow: hidden;
      resize: horizontal;
      background: #f0f0f0;
      min-width: 32px;
      white-space: nowrap;

      &:first-child {
        border-left: none;
        /* text-align: center; */
        width: 32px;
        max-width: 32px;
        resize: none;
        position: sticky;
        z-index: 4;
        left: 0;
        box-shadow: inset -1px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }

      &:nth-child(2) {
        box-shadow: inset 0px 0px 0px #d9d9d9, inset 0px 1px 0px #d9d9d9,
          inset 0px -1px 0px #d9d9d9;
      }
      /* 
      &:last-child {
        position: sticky;
        z-index: 4;
        right: 0;
        border-right: none;
        resize: none;
        width: 122px;
        max-width: 122px;

        .hidden__component {
          display: none;
        }
      } */
    }

    .table-body td {
      border-bottom: 1px solid #ddd;
      padding: 16px;

      max-width: 200px;

      &:first-child {
        /* border-left: 1px solid #ddd; */
        width: 32px;
        max-width: 32px;
        padding: 0;
        text-align: center;
        position: sticky;
        z-index: 2;
        left: 0;

        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
        .ant-radio-wrapper {
          margin-right: 0px;
        }

        .ant-radio-checked::after,
        .ant-radio-checked .ant-radio-inner {
          border-color: ${(props) => props.theme.main};
        }
      }

      /* &:last-child {
        border-right: 1px solid #ddd;
        position: sticky;
        z-index: 2;
        right: 0;
        filter: drop-shadow(-3px 3px 8px rgba(0, 0, 0, 0.15));
      } */
    }
  }
`;

const ContentHeader = styled.div`
  /* border-left: 1px solid #ddd;
  border-right: 1px solid #ddd; */
`;

const CustomSearch = styled(Input)`
  border-radius: 5px;
  &:hover,
  &:focus {
    border-color: ${(props) => props.theme.main};
    box-shadow: none;
  }
`;

const CustomSelect = styled(Select)`
  margin-bottom: 0px !important;

  .ant-select-selection-placeholder {
    font-weight: normal;
  }

  .ant-select-selector {
    border-radius: 5px !important;
  }
`;

const TitleWrap = styled.div`
  padding: 8px;
  display: flex;

  span {
    font-family: var(--roboto-500);
    font-size: 16px;
    color: #252424;
    font-weight: normal;
  }
`;

const SearchWrap = styled.div`
  border-top: 1px solid #ddd;
  padding: 8px;
`;

const IconHeader = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 6px;

  .anticon svg {
    font-size: 12px;
    /* color: rgba(0, 0, 0, 0.25); */
    cursor: pointer;
    transition: all 0.5s;
  }
`;

const CustomTd = styled.td`
  background: ${(props) => (props.check ? "#e6f7ff" : "#fff")};
`;

const NewCustomPagination = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  background-color: white;
  padding: 10px;
  position: absolute;
  top: 0;
  right: 7px;
  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
  .total-record {
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 16px;
    letter-spacing: 0.01em;
    color: #637381;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .reload-pagi {
    img {
      width: 15px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
  .left-pagi {
    img {
      width: 7px;
    }
    margin-right: 8px;
    margin-left: 8px;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
  .right-pagi {
    img {
      width: 7px;
    }
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    :hover {
      background-color: #e9e9e9;
      border-radius: 50%;
      cursor: pointer;
    }
  }
`;
