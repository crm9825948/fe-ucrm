import { InputNumber } from "antd";
import React from "react";

const NumberComp = ({
  operatorValue,
  conditions,
  setConditions,
  setValue,
  value,
  index,
}) => {
  return (
    <>
      <InputNumber
        style={{ width: "100%" }}
        value={value[index]}
        onChange={(e) => {
          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });
          tmpConditions[index].value = {
            [operatorValue]: e,
          };

          let tmp = [...value];
          tmp[index] = e;
          setValue(tmp);

          setConditions(tmpConditions);
        }}
      />
    </>
  );
};

export default NumberComp;
