import { ClearOutlined, SearchOutlined } from "@ant-design/icons";
import { Input } from "antd";
import axios from "axios";
import { BASE_URL_API } from "constants/constants";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadUser } from "redux/slices/objects";
import UserList from "./userList";
import { checkTokenExpiration } from "contexts/TokenCheck";

const User = ({
  operatorValue,
  field,
  conditions,
  setConditions,
  setValue,
  value,
  disabled,
  index,
  ID,
}) => {
  const [visible, setVisible] = useState(false);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [selectedRows, setSelectedRows] = useState([]);
  /*eslint-disable-next-line*/
  const [name, setName] = useState("");
  const [searchData, setSearchData] = useState({});
  const dispatch = useDispatch();
  const { users } = useSelector((state) => state.objectsReducer);

  useEffect(() => {
    if (visible === true) {
      dispatch(
        loadUser({
          current_page: 1,
          record_per_page: 10,
        })
      );
    }
    /* eslint-disable-next-line */
  }, [visible]);

  useEffect(() => {
    setName("");
    setSelectedRowKeys([]);
  }, [operatorValue]);

  useEffect(() => {
    if (!checkTokenExpiration()) return;

    if (conditions[index].key) {
      axios
        .post(
          BASE_URL_API + "user/get-user-details",
          {
            _id: conditions[index].key,
          },
          {
            headers: {
              Authorization: localStorage.getItem("setting_accessToken"),
            },
          }
        )
        .then((res) => {
          let newName = res.data.data.Full_Name;
          setName(newName);
          setVisible(false);

          let tmp = [...value];
          tmp[index] = newName;
          setValue(tmp);
        })
        .catch((err) => console.log(err));
    }
    /* eslint-disable-next-line */
  }, [ID, conditions, checkTokenExpiration]);

  const reloadData = (current_page, record_per_page, searchData) => {
    dispatch(
      loadUser({
        current_page: current_page,
        record_per_page: record_per_page,
        search_data: searchData,
      })
    );
  };

  const handleSelect = (selectedRowKeys, selectedRows) => {
    let newName = `${selectedRows[0].First_Name} ${selectedRows[0].Middle_Name} ${selectedRows[0].Last_Name}`;
    setName(newName);
    setVisible(false);

    let tmpConditions = [...conditions];

    let newTmpConditions = [];
    /* eslint-disable-next-line */
    tmpConditions.forEach((item, idx) => {
      newTmpConditions[idx] = { ...item };
    });

    newTmpConditions[index].value = {
      [operatorValue]: selectedRowKeys[0],
    };
    newTmpConditions[index].key = selectedRowKeys[0];
    setConditions(newTmpConditions);

    let tmp = [...value];
    tmp[index] = newName;
    setValue(tmp);
  };

  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });

    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      case "mine":
        tmpConditions[index].value = {
          $eq: "mine",
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      case "not-mine":
        tmpConditions[index].value = {
          $ne: "mine",
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;

      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  return (
    <>
      <Input
        disabled={
          disabled ||
          operatorValue === "empty" ||
          operatorValue === "not-empty" ||
          operatorValue === "mine" ||
          operatorValue === "not-mine"
        }
        addonAfter={
          <>
            {disabled ||
            operatorValue === "empty" ||
            operatorValue === "not-empty" ||
            operatorValue === "mine" ||
            operatorValue === "not-mine" ? (
              <SearchOutlined style={{ pointerEvents: "none" }} />
            ) : (
              <SearchOutlined
                onClick={() => {
                  setVisible(true);
                }}
              />
            )}
          </>
        }
        addonBefore={
          disabled ||
          operatorValue === "empty" ||
          operatorValue === "not-empty" ||
          operatorValue === "mine" ||
          operatorValue === "not-mine" ? (
            ""
          ) : name ? (
            <ClearOutlined
              onClick={() => {
                // let tmpConditions = [...conditions];
                // tmpConditions[index].value = {
                //   [operatorValue]: null,
                // };

                // tmpConditions[index].key = null;
                // setConditions(tmpConditions);

                // let tmp = [...value];
                // tmp[index] = null;
                // setValue(tmp);
                setName("");
                let tmpConditions = [...conditions];

                let newTmpConditions = [];
                /* eslint-disable-next-line */
                tmpConditions.forEach((item, idx) => {
                  newTmpConditions[idx] = { ...item };
                });

                newTmpConditions[index].value = {
                  [operatorValue]: "",
                };
                newTmpConditions[index].key = "";
                setConditions(newTmpConditions);

                let tmp = [...value];
                tmp[index] = "";
                setValue(tmp);
              }}
            />
          ) : (
            ""
          )
        }
        value={name}
      />
      <UserList
        visible={visible}
        setVisible={setVisible}
        users={users}
        selectedRowKeys={selectedRowKeys}
        setSelectedRowKeys={setSelectedRowKeys}
        reloadData={reloadData}
        handleSelect={handleSelect}
        selectedRows={selectedRows}
        setSelectedRows={setSelectedRows}
        searchData={searchData}
        setSearchData={setSearchData}
        conditions={conditions}
        index={index}
        ID={ID}
      />
    </>
  );
};

export default User;
