import React, { useEffect } from "react";
import { DatePicker, Tag } from "antd";
import moment from "moment";
import styled from "styled-components";
const { RangePicker } = DatePicker;

const Date = ({
  operatorValue,
  disabled,
  conditions,
  setConditions,
  setValue,
  value,
  index,
  // init,
}) => {
  const listOperatorDisable = [
    "$inWorkingTimeUCRM",
    "$ninWorkingTimeUCRM",
    "$inWorkingDayUCRM",
    "$ninWorkingDayUCRM",
  ];
  useEffect(() => {
    let tmp = [];
    let tmpConditions = [];
    /*eslint-disable-next-line*/
    conditions.map((item, idx) => {
      tmpConditions.push({ ...item });
    });
    /*eslint-disable-next-line*/
    value.map((item, idx) => {
      tmp.push(item);
    });
    switch (operatorValue) {
      case "empty":
        tmpConditions[index].value = {
          $eq: null,
        };

        tmp[index] = "";
        setValue(tmp);

        setConditions(tmpConditions);
        break;
      case "not-empty":
        tmpConditions[index].value = {
          $ne: null,
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      case "not-today":
        tmpConditions[index].value = {
          $ne: "today",
        };
        tmp[index] = "";
        setValue(tmp);
        setConditions(tmpConditions);
        break;
      default:
        break;
    }

    /*eslint-disable-next-line*/
  }, [operatorValue]);

  if (operatorValue === "$between") {
    return (
      <RangePicker
        value={
          value[index]
            ? [moment(value[index][0]), moment(value[index][1])]
            : null
        }
        onChange={(e, dateString) => {
          let tmpConditions = [];
          /*eslint-disable-next-line*/
          conditions.map((item, idx) => {
            tmpConditions.push({ ...item });
          });

          tmpConditions[index].value = {
            $gte: dateString[0],
            $lte: dateString[1],
          };

          let tmp = [...value];
          tmp[index] = dateString;
          setValue(tmp);
          setConditions(tmpConditions);
        }}
      />
    );
  } else
    return operatorValue === "$in" || operatorValue === "$nin" ? (
      <>
        <div>
          <DatePicker
            onChange={(e, dateString) => {
              let tmpConditions = [];
              /*eslint-disable-next-line*/
              conditions.map((item, idx) => {
                tmpConditions.push({ ...item });
              });

              if (operatorValue !== "$between") {
                let tmp = [...value];
                tmp[index] = [...new Set([...tmp[index], dateString])];
                let tempValue = [...new Set([...tmp[index], dateString])];
                tmpConditions[index].value = {
                  [operatorValue]: tempValue,
                };

                setValue(tmp);
                setConditions(tmpConditions);
              }
            }}
          />
          <div>
            {conditions[index]?.value[operatorValue]?.map((item, idx) => {
              return (
                <CustomTag
                  // closeIcon={<CloseCircleOutlined />}
                  style={{ display: "block" }}
                  closable
                  onClose={() => {
                    let tmpConditions = [];
                    /*eslint-disable-next-line*/
                    conditions.map((item, idx) => {
                      tmpConditions.push({ ...item });
                    });
                    if (operatorValue !== "$between") {
                      let tmp = [...value];

                      console.log(tmp[index]);
                      let tempValue = [...new Set([...tmp[index]])].filter(
                        (el) => el !== item
                      );

                      console.log(tempValue);
                      tmp[index] = tempValue;

                      tmpConditions[index].value = {
                        [operatorValue]: tempValue,
                      };

                      setValue(tmp);
                      setConditions(tmpConditions);
                    }
                  }}
                >
                  {item}
                </CustomTag>
              );
            })}
          </div>
        </div>
      </>
    ) : (
      <>
        <DatePicker
          disabled={
            disabled ||
            operatorValue === "empty" ||
            operatorValue === "not-empty" ||
            listOperatorDisable.includes(operatorValue)
          }
          value={value[index] ? moment(value[index]) : null}
          onChange={(e, dateString) => {
            console.log(dateString);
            let tmpConditions = [];
            /*eslint-disable-next-line*/
            conditions.map((item, idx) => {
              tmpConditions.push({ ...item });
            });

            if (operatorValue !== "$between") {
              tmpConditions[index].value = {
                [operatorValue]: dateString,
              };
              let tmp = [...value];
              tmp[index] = dateString;
              setValue(tmp);
              setConditions(tmpConditions);
            }
          }}
        />
      </>
    );
};

export default Date;

const CustomTag = styled(Tag)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 5px;
  text-align: center;
  width: 100%;
  .ant-tag {
    padding: 20px;
  }
`;
