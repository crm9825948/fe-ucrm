import { SearchOutlined } from "@ant-design/icons";
import { Button, Drawer, Input, Pagination, Space, Table } from "antd";
import { default as React, useEffect, useState } from "react";
import Highlighter from "react-highlight-words";
import styled from "styled-components";

const UserList = (props) => {
  const {
    visible,
    setVisible,
    selectedRowKeys,
    setSelectedRowKeys,
    searchData,
    selectedRows,
    setSelectedRows,
    setSearchData,
    reloadData,
    handleSelect,
    users,
    conditions,
    index,
    ID,
  } = props;

  const onClose = () => {
    setVisible(false);
  };

  const [usersData, setUsersData] = useState([]);
  const [recordPerPage, setRecordPerPage] = useState(10);
  const [currentPage, setCurrentPage] = useState(1);

  useEffect(() => {
    let newUsers = [];
    /* eslint-disable-next-line */
    users &&
      users.records &&
      /* eslint-disable-next-line */
      users.records.map((item, idx) => {
        let newItem = { ...item };
        newItem["key"] = item._id;
        newUsers.push(newItem);
      });
    setUsersData(newUsers);
  }, [users]);

  const getColumnSearchProps = (dataIndex, searchData) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            handleSearch(selectedKeys, confirm, dataIndex, searchData)
          }
          style={{ marginBottom: 8, display: "block" }}
        />
        <Space>
          <CustomButtonSave
            type="primary"
            onClick={() =>
              handleSearch(selectedKeys, confirm, dataIndex, searchData)
            }
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </CustomButtonSave>
          <CustomButtonCancel
            onClick={() => handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </CustomButtonCancel>
          <CustomButtonCancel
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
            }}
          >
            Filter
          </CustomButtonCancel>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  const columns = [
    {
      title: "First name",
      dataIndex: "First_Name",
      key: "First_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("First_Name", searchData),
    },
    {
      title: "Middle name",
      dataIndex: "Middle_Name",
      key: "Middle_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Middle_Name", searchData),
    },
    {
      title: "Last name",
      dataIndex: "Last_Name",
      key: "Last_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Last_Name", searchData),
    },
    {
      title: "Email",
      dataIndex: "Email",
      key: "Email",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Email", searchData),
    },
    {
      title: "Report to",
      dataIndex: "Report_To_Name",
      key: "Report_To_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Report_To_Name", searchData),
    },
    {
      title: "User role",
      dataIndex: "User_Role_Name",
      key: "Report_To_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("User_Role_Name", searchData),
    },
    {
      title: "Is admin",
      dataIndex: "Is_Admin",
      key: "Is_Admin",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Is_Admin", searchData),
      render: (text) => {
        return <div>{text ? "Yes" : "No"}</div>;
      },
    },
    {
      title: "Created by",
      dataIndex: "Created_By_Name",
      key: "Created_By_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Created_By_Name", searchData),
    },
    {
      title: "Created date",
      dataIndex: "Created_Date",
      key: "Created_Date",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Created_Date", searchData),
    },
    {
      title: "Modified by",
      dataIndex: "Modify_By_Name",
      key: "Modify_By_Name",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Modify_By_Name", searchData),
    },
    {
      title: "Modify time",
      dataIndex: "Modify_Time",
      key: "Modify_Time",
      width: "maxContent",
      editable: true,
      ...getColumnSearchProps("Modify_Time", searchData),
    },
  ];

  const onSelectChange = (selectedRowKeys, selectedRows) => {
    setSelectedRowKeys(selectedRowKeys);
    setSelectedRows(selectedRows);
  };

  const [searchText, setSearchText] = useState("");
  const [searchedColumn, setSearchedColumn] = useState("");

  const handleSearch = (selectedKeys, confirm, dataIndex, searchData) => {
    confirm();
    let searchDataTemp = { ...searchData };
    searchDataTemp[dataIndex] = selectedKeys[0];
    setSearchData(searchDataTemp);
    reloadData(currentPage, recordPerPage, searchDataTemp);
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters) => {
    clearFilters();
    setSearchText("");
  };

  useEffect(() => {
    if (ID) {
      // setSelectedRowKeys(
      //    ?  :
      // );
      setSelectedRowKeys((prev) => {
        if (conditions[index].key) {
          return [conditions[index].key];
        } else {
          return prev;
        }
      });
    }
  }, [ID, conditions, index, setSelectedRowKeys]);

  return (
    <>
      <Drawer
        title="User list"
        placement="right"
        onClose={onClose}
        visible={visible}
        width={1000}
      >
        <Wrapper>
          <Table
            // bordered
            dataSource={usersData}
            columns={columns}
            rowClassName="editable-row"
            pagination={false}
            rowSelection={{
              selectedRowKeys: selectedRowKeys,
              onChange: onSelectChange,
              type: "radio",
            }}
            scroll={{
              x: "max-content",
            }}
          />
          <div>
            <CustomPagination
              showQuickJumper
              current={currentPage}
              total={users.total_record}
              showSizeChanger
              showTotal={(total, range) =>
                `${range[0]}-${range[1]} of ${total} records`
              }
              pageSize={recordPerPage}
              onChange={(e, pageSize) => {
                reloadData(e, pageSize, searchData);
                setCurrentPage(e);
                setRecordPerPage(pageSize);
              }}
            />

            <WrapperSave>
              <CustomButtonSave
                onClick={() => {
                  handleSelect(selectedRowKeys, selectedRows);
                }}
                disabled={selectedRowKeys[0] ? false : true}
              >
                Save
              </CustomButtonSave>
              <CustomButtonCancel onClick={() => setVisible(false)}>
                {" "}
                Cancel
              </CustomButtonCancel>
            </WrapperSave>
          </div>
        </Wrapper>
      </Drawer>
    </>
  );
};

export default UserList;

const WrapperSave = styled.div`
  position: fixed;
  bottom: -8px;
  right: 826px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
`;

const CustomPagination = styled(Pagination)`
  position: fixed;
  bottom: -8px;
  right: 16px;
  width: 976px;
  text-align: right;
  background-color: #fff;
  height: 100px;
  padding-top: 16px;
  .ant-pagination-item-active {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
  .ant-pagination-item:hover {
    border-color: #0ab2ab;
    a {
      color: #0ab2ab;
    }
  }
`;

const Wrapper = styled.div`
  table {
    width: max-content;
    th {
      width: max-content;
      .ant-table-column-title {
        margin-right: 30px;
      }
    }
  }
  padding-bottom: 100px;
`;

const CustomButtonSave = styled(Button)`
  /* width: 80px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  /* font-size: 16px; */
  background-color: ${(props) => props.theme.main};
  color: #fff;
  border-color: ${(props) => props.theme.main};
  margin-right: 16px;
  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    background-color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    color: #fff;
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;

const CustomButtonCancel = styled(Button)`
  /* font-size: 16px; */
  background-color: #fff;
  /* width: 80px; */
  color: #000;

  /* img {
    width: 15px;
    margin-right: 8px;
  } */
  &:hover {
    color: ${(props) => props.theme.darker};
    border-color: ${(props) => props.theme.darker};
    /* box-shadow: 0px 0px 16px rgba(62, 62, 62, 0.42);
    transition: 0.5s; */
  }
`;
