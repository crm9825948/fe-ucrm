import React, { useEffect, useState } from "react";
import styled from "styled-components";
import backgroundImage from "assets/images/login/login-bg.jpg";
import { ArrowLeftOutlined } from "@ant-design/icons";
import { useNavigate } from "react-router-dom";

const HttpCodePage = (props) => {
  const { code, content, decs } = props;
  const navigate = useNavigate();

  const [errorCrash, setErrorCrash] = useState("");

  useEffect(() => {
    if (localStorage.getItem("errorCrash")) {
      setErrorCrash(localStorage.getItem("errorCrash"));
      localStorage.removeItem("errorCrash");
    }
  }, []);

  return (
    <Wrapper>
      <div>
        <div className="code">{code}</div>
        <div className="content">{content}</div>
        <div className="decs"> {errorCrash ? errorCrash : decs}</div>

        <div style={{ display: "flex", justifyContent: "center" }}>
          <button
            className="backToDashBoard"
            onClick={() => {
              errorCrash
                ? window.open(`${document.referrer}`, "_self")
                : navigate("/dashboard");
            }}
          >
            <ArrowLeftOutlined />{" "}
            {errorCrash ? "Back to work" : "Back to dashboard"}
          </button>
        </div>
      </div>
    </Wrapper>
  );
};

export default HttpCodePage;

const Wrapper = styled.div`
  height: 100vh;
  background-image: url(${backgroundImage});
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
  .code {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 250px;
    line-height: 330px;
    /* identical to box height */

    letter-spacing: 0.01em;

    color: #ffffff;
  }
  .content {
    font-style: normal;
    font-family: var(--roboto-700);
    font-size: 50px;
    line-height: 59px;
    /* identical to box height */

    letter-spacing: 0.01em;

    color: #ffffff;
    margin-top: 20px;
  }
  .decs {
    font-family: var(--roboto-400);
    font-size: 20px;
    line-height: 23px;
    letter-spacing: 0.01em;

    color: #ffffff;
    margin-top: 40px;
  }
  .backToDashBoard {
    background: ${(props) => props.theme.main};
    /* Primary/6 */

    border: 1px solid ${(props) => props.theme.main};
    box-sizing: border-box;
    /* drop-shadow / button-primary */

    box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
    border-radius: 30px;
    padding: 13px;
    color: white;
    font-family: var(--roboto-400);
    font-size: 16px;
    line-height: 24px;
    /* identical to box height, or 150% */

    text-align: center;

    /* Neutral/1 */

    color: #ffffff;
    margin-top: 56px;
    :hover {
      cursor: pointer;
      background-color: ${(props) => props.theme.darker};
    }
  }
`;
