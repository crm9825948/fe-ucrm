import styled from "styled-components";
import Button from "antd/lib/button";
import _ from "lodash";

const View = ({ title, ...props }) => {
  return (
    <ButtonCustom type={_.get(props, "type", "default")} {...props}>
      {title}
    </ButtonCustom>
  );
};
export default View;

const ButtonCustom = styled(Button)`
  background: ${(props) =>
    props.type === "primary" ? props.theme.main : "#fff"};
  border: 1px solid
    ${(props) => (props.type === "primary" ? props.theme.main : "#d9d9d9")};

  :hover {
    background: ${(props) => props.theme.darker}!important;
    color: #fff !important;
  }

  :active {
    background: ${(props) =>
      props.type === "primary" ? props.theme.main : "#fff"};
    color: ${(props) => (props.type === "primary" ? "#fff" : "#2C2C2C")};
    border: 1px solid
      ${(props) => (props.type === "primary" ? props.theme.main : "#d9d9d9")};
  }

  :focus {
    background: ${(props) =>
      props.type === "primary" ? props.theme.main : "#fff"};
    color: ${(props) => (props.type === "primary" ? "#fff" : "#2C2C2C")};
    border: 1px solid
      ${(props) => (props.type === "primary" ? props.theme.main : "#d9d9d9")};
  }
`;
