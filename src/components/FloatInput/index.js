import { useState } from "react";
import { Input, Select } from "antd";

import "./index.scss";

const FloatInput = (props) => {
  const [focus, setFocus] = useState(false);

  let { label, value, placeholder, type, required, options } = props;

  if (!placeholder) placeholder = label;

  const isOccupied = focus || (value && value.length !== 0);

  const labelClass = isOccupied ? "label as-label" : "label as-placeholder";

  const requiredMark = required ? <span className="text-danger">*</span> : null;

  return (
    <div
      className="float-label height-select"
      onBlur={() => setFocus(false)}
      onFocus={() => setFocus(true)}
    >
      {type === "input" && (
        <Input
          style={props.style}
          onChange={props.onChange}
          value={value}
          onPressEnter={props.onPressEnter}
        />
      )}
      {type === "select" && (
        <Select
          style={props.style}
          onChange={props.onChange}
          value={value}
          options={options}
          onSelect={props.onSelect}
        />
      )}
      <label className={labelClass}>
        {isOccupied ? label : placeholder} {requiredMark}
      </label>
    </div>
  );
};

export default FloatInput;
