import { useState, useEffect, useCallback } from "react";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { useParams } from "react-router";

import Typography from "antd/lib/typography";
import Spin from "antd/lib/spin";
import Dropdown from "antd/lib/dropdown";
import { MoreOutlined } from "@ant-design/icons";

import PinWidget from "assets/icons/report/PinWidget1.svg";
import UnpinWidget from "assets/icons/report/UnpinWidget1.svg";
import EditWidget from "assets/icons/report/EditWidget1.svg";
import DeleteWidget from "assets/icons/report/DeleteWidget1.svg";
import Down from "assets/icons/report/Down.svg";
import Up from "assets/icons/report/Up.svg";
import EmptyDashboard from "assets/images/reports/empty-dashboard.jpg";
import { Tooltip } from "antd";
import {
  pinToDashBoard,
  unpinReport,
  getDataWidget,
  resetStatus,
} from "redux/slices/reportDetails";
const { Text } = Typography;

function Widget({
  widget,
  allCondition,
  anyCondition,
  _onDeleteWidget,
  _onEditWidget,
  onRemove,
  readonly,
}) {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const { objectID, recordID } = useParams();

  const { listWidgets, isLoading, statusPin, notFoundWidget } = useSelector(
    (state) => state.reportDetailsReducer
  );

  const [data, setData] = useState({});
  const [showAction, $showAction] = useState(false);

  const _onPinWidget = () => {
    $showAction(false);

    let pathname = window.location.pathname.split("/");
    if (onRemove && pathname[pathname.length - 1] === "dashboard") {
      onRemove();
    }
    if (pathname[pathname.length - 1] !== "dashboard") {
      if (data.widget_config?.is_pinned_to_dashboard) {
        dispatch(
          unpinReport({
            widget_id: widget._id,
          })
        );
      } else {
        dispatch(
          pinToDashBoard({
            widget_id: widget._id,
            type: "widget",
            object_id: objectID,
            report_id: recordID,
          })
        );
      }
    }
  };

  const loadDataWidget = useCallback(() => {
    setData({
      ...widget,
      isLoading: true,
    });

    if (allCondition.length > 0 || anyCondition.length > 0) {
      dispatch(
        getDataWidget({
          _id: widget._id,
          api_version: "2",
          quick_filter: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
        })
      );
    } else {
      dispatch(
        getDataWidget({
          _id: widget._id,
          api_version: "2",
        })
      );
    }
  }, [allCondition, anyCondition, dispatch, widget]);

  const menuAction = (isPin, isDashboard) => {
    return (
      <WrapAction>
        {isPin ? (
          <Action onClick={_onPinWidget}>
            <img
              src={
                pathname.split("/").includes("dashboard")
                  ? DeleteWidget
                  : UnpinWidget
              }
              alt="pin"
            />
            <span>Unpin</span>
          </Action>
        ) : (
          <Action onClick={_onPinWidget}>
            <img src={PinWidget} alt="pin" />
            <span>Pin</span>
          </Action>
        )}

        {!isDashboard && (
          <>
            <Action
              onClick={() => {
                _onEditWidget(widget._id);
                $showAction(false);
              }}
            >
              <img src={EditWidget} alt="edit" />
              <span>Edit</span>
            </Action>
            <Action
              onClick={() => {
                _onDeleteWidget(widget._id);
                $showAction(false);
              }}
            >
              <img src={DeleteWidget} alt="delete" />
              <span>Delete</span>
            </Action>
          </>
        )}
      </WrapAction>
    );
  };

  useEffect(() => {
    if (!window.location.pathname.includes("/dashboard")) {
      loadDataWidget();
    }
    /*eslint-disable-next-line*/
  }, [loadDataWidget]);

  useEffect(() => {
    if (window.location.pathname.includes("/dashboard")) {
      loadDataWidget();
    }
    /*eslint-disable-next-line*/
  }, []);

  useEffect(() => {
    listWidgets.forEach((item) => {
      if (item._id === widget._id) {
        setData(item);
      }
    });
  }, [listWidgets, widget]);

  useEffect(() => {
    if (isLoading === false) {
      if (
        (statusPin?.status === "unpin success" ||
          statusPin?.status === "pin success") &&
        statusPin?._id === widget._id
      ) {
        loadDataWidget();
        dispatch(resetStatus());
      }
    }
  }, [isLoading, loadDataWidget, statusPin, widget, dispatch]);

  useEffect(() => {
    if (notFoundWidget.status === true && notFoundWidget.id === widget._id) {
      setData({
        ...widget,
        isLoading: false,
      });
    }
  }, [widget, notFoundWidget]);

  const formattedNumber = (value) => {
    return value.toFixed(3).replace(/[.,]000$/, "");
  };

  function abbrNum(number, decPlaces) {
    /*eslint-disable-next-line*/
    var orig = number;
    /*eslint-disable-next-line*/
    var dec = decPlaces;
    // 2 decimal places => 100, 3 => 1000, etc
    decPlaces = Math.pow(10, decPlaces);

    // Enumerate number abbreviations
    var abbrev = [" K", " M", " B"];

    // Go through the array backwards, so we do the largest first
    for (var i = abbrev.length - 1; i >= 0; i--) {
      // Convert array index to "1000", "1000000", etc
      var size = Math.pow(10, (i + 1) * 3);

      // If the number is bigger or equal do the abbreviation
      if (size <= number) {
        // Here, we multiply by decPlaces, round, and then divide by decPlaces.
        // This gives us nice rounding to a particular decimal place.
        /*eslint-disable-next-line*/
        var number = Math.round((number * decPlaces) / size) / decPlaces;

        // Handle special case where we round up to the next abbreviation
        /*eslint-disable-next-line*/
        if (number == 1000 && i < abbrev.length - 1) {
          number = 1;
          i++;
        }

        // Add the letter for the abbreviation
        number += abbrev[i];

        // We are done... stop
        break;
      }
    }
    return number;
  }

  return (
    <Wrapper
      isNotFound={
        notFoundWidget.status === true && notFoundWidget.id === widget._id
      }
    >
      {data.isLoading ? (
        <WrapSpin>
          <Spin />
        </WrapSpin>
      ) : (
        <>
          {Object.keys(data).length > 0 &&
            !notFoundWidget.status &&
            notFoundWidget.id !== widget._id && (
              <>
                <WrapTitle>
                  <TitleWidget ellipsis={{ tooltip: data?.name }}>
                    {data?.name}
                  </TitleWidget>
                  <Dropdown
                    visible={showAction}
                    onVisibleChange={() => $showAction(!showAction)}
                    overlay={menuAction(
                      data.widget_config?.is_pinned_to_dashboard,
                      pathname.split("/").includes("dashboard")
                    )}
                    placement="bottomCenter"
                    trigger={["click"]}
                  >
                    <MoreOutlined
                      style={{
                        color: "#bbbbbb",
                        fontSize: 24,
                      }}
                    />
                  </Dropdown>
                </WrapTitle>

                <WrapValue>
                  <Value isCompared={data?.is_show_icon_compare}>
                    <Text ellipsis={{ tooltip: data?.value?.toLocaleString() }}>
                      {data?.value > 100000 ? (
                        <Tooltip
                          placement="topRight"
                          title={data?.value?.toLocaleString()}
                        >
                          {abbrNum(data?.value, 3)}
                        </Tooltip>
                      ) : (
                        data?.value?.toLocaleString()
                      )}
                    </Text>
                  </Value>

                  {data?.is_show_icon_compare && (
                    <>
                      {data?.show_diff_by === "percentage" ? (
                        <ValueCompared
                          up={data?.value - data.compare_widget?.value > 0}
                        >
                          {data?.value - data.compare_widget?.value > 0 ? (
                            <Text
                              ellipsis={{
                                tooltip:
                                  formattedNumber(
                                    (Math.abs(
                                      data?.value - data.compare_widget?.value
                                    ) /
                                      data?.value) *
                                      100
                                  ) + "%",
                              }}
                            >
                              <img src={Up} alt="up" />
                              {formattedNumber(
                                (Math.abs(
                                  data?.value - data.compare_widget?.value
                                ) /
                                  data?.value) *
                                  100
                              )}
                              %
                            </Text>
                          ) : (
                            <Text
                              ellipsis={{
                                tooltip:
                                  formattedNumber(
                                    (Math.abs(
                                      data?.value - data.compare_widget?.value
                                    ) /
                                      data.compare_widget?.value) *
                                      100
                                  ) + "%",
                              }}
                            >
                              <img src={Down} alt="down" />
                              {formattedNumber(
                                (Math.abs(
                                  data?.value - data.compare_widget?.value
                                ) /
                                  data.compare_widget?.value) *
                                  100
                              )}
                              %
                            </Text>
                          )}
                        </ValueCompared>
                      ) : (
                        <ValueCompared
                          up={data?.value - data.compare_widget?.value > 0}
                        >
                          <Text
                            ellipsis={{
                              tooltip: Math.abs(
                                data?.value - data.compare_widget?.value
                              ).toLocaleString(),
                            }}
                          >
                            {data?.value - data.compare_widget?.value > 0 ? (
                              <img src={Up} alt="up" />
                            ) : (
                              <img src={Down} alt="down" />
                            )}

                            {Math.abs(
                              data?.value - data.compare_widget?.value
                            ).toLocaleString()}
                          </Text>
                        </ValueCompared>
                      )}
                    </>
                  )}
                </WrapValue>

                {data.compare_widget !== undefined && (
                  <Compared>
                    Compared to{" "}
                    <Text
                      ellipsis={{
                        tooltip: data.compare_widget?.value.toLocaleString(),
                      }}
                    >
                      {abbrNum(data.compare_widget?.value, 3)}
                      {/* {data.compare_widget?.value.toLocaleString()} */}
                    </Text>{" "}
                    {data.compare_widget?.name}
                  </Compared>
                )}
              </>
            )}

          {notFoundWidget.status === true && notFoundWidget.id === widget._id && (
            <>
              {!readonly && (
                <ImageEmpty
                  onClick={_onPinWidget}
                  src={DeleteWidget}
                  alt="remove"
                />
              )}

              <NotFound>
                <img src={EmptyDashboard} alt="empty" />
                <span>Widget Not Found</span>
              </NotFound>
            </>
          )}
        </>
      )}
    </Wrapper>
  );
}

export default Widget;

Widget.defaultProps = {
  readonly: false,
};

const Wrapper = styled.div`
  min-width: 392px;
  max-width: 527px;
  height: 196px;
  padding: 18px 24px 24px;
  border: ${({ isNotFound }) =>
    isNotFound || window.location.pathname.includes("dashboard")
      ? "none"
      : "1px solid #ececec"};
  border-radius: 2px;
  background: #fff;
  margin-right: 16px;
  margin-bottom: 16px;
`;

const WrapSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  .ant-typography {
    max-width: 450px;
    margin-bottom: 0;
  }
`;

const TitleWidget = styled(Text)`
  font-family: var(--roboto-500);
  font-size: 18px;
  line-height: 26px;
  color: #252424;
`;

const WrapAction = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  padding: 5px 0;
`;

const Action = styled.div`
  cursor: pointer;
  width: 121px;
  height: 32px;
  display: flex;
  align-items: center;
  padding-left: 13px;

  img {
    margin-right: 9px;
  }

  span {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  :hover {
    background: #f5f5f5;
  }
`;

const WrapValue = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 32px;
  margin-bottom: 16px;
  width: 100%;
`;

const Value = styled.div`
  .ant-typography {
    max-width: 345px;
    font-size: 60px;
    line-height: 48px;
    color: #2c2c2c;
  }
`;

const ValueCompared = styled.div`
  margin-top: 14px;
  margin-left: 8px;
  width: fit-content;
  padding: 6px 12px;
  background: #efeff4;
  border-radius: 8px;
  height: 32.42px;

  .ant-typography {
    font-family: var(--roboto-500);
    font-size: 16px;
    line-height: 19px;
    color: #2c2c2c;
    max-width: 100px;

    span {
      display: flex;
    }
  }

  img {
    margin-right: 4px;
  }
`;

const Compared = styled.div`
  text-align: center;
  font-size: 16px;
  line-height: 22px;
  color: #6b6b6b;
  display: -webkit-box;
  -webkit-line-clamp: 1;
  -webkit-box-orient: vertical;
  overflow: hidden;
  text-overflow: ellipsis;

  .ant-typography {
    font-family: var(--roboto-500);
  }
`;

const ImageEmpty = styled.img`
  float: right;
  cursor: pointer;
`;

const NotFound = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;

  span {
    margin-top: 8px;
  }

  img {
    width: fit-content;
  }
`;
