import _ from "lodash";
import Modal from "antd/lib/modal";
import Button from "antd/lib/button";
import styled from "styled-components";
import Typography from "antd/lib/typography/Typography";
import { useDispatch, useSelector } from "react-redux";
import { componentsLayoutSettings, handlerIconComp } from "util/staticData";
import { useTranslation, withTranslation } from "react-i18next";
import React, { memo, useEffect, useRef, useState } from "react";

import IconActive from "assets/icons/callView/tick-circle.svg";
import { updateTemplateCallView } from "redux/slices/callView";
import { useLocation } from "react-router";

const { Text } = Typography;

const ModalChangeComponent = ({
  visible,
  setVisible,
  keySelectedProp,
  itemSelectedProp,
  componentList,
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const elementRef = useRef(null);
  const { search } = useLocation();
  const queryParams = new URLSearchParams(search);
  const templateId = queryParams.get("template");
  const [componentsCreated, $componentsCreated] = useState([]);
  const [data, setData] = useState(
    componentsLayoutSettings.filter((item) => _.get(item, "title") !== "Email")
  );
  const [itemSelected, setItemSelected] = useState(null);
  const [newItemSelected, setNewItemSelected] = useState(null);

  const { components } = useSelector(
    (state) => state.consolidatedViewSettingsReducer
  );

  const handleScrollToElement = () => {
    if (elementRef?.current) {
      setTimeout(() => {
        elementRef.current.scrollTop = 0;
      }, 100);
    }
  };

  const checkDataRender = (dataList) => {
    if (componentList) {
      return dataList.filter((item) =>
        componentList
          .filter(
            (it) => _.get(it, "title") !== _.get(itemSelectedProp, "title")
          )
          .every((i) => _.get(i, "title") !== _.get(item, "title"))
      );
    }
    return dataList;
  };

  const handleClose = () => {
    setVisible(false);
    setItemSelected(null);
  };

  const handleSubmit = () => {
    dispatch(
      updateTemplateCallView({
        object_id: "obj_contact_00000001",
        template: templateId ? templateId.toString() : "",
        components: newItemSelected,
      })
    );
    setVisible(false);
  };

  const normalizeComponents = (items) => {
    return _.map(
      items,
      (item) =>
        (item = {
          ...item,
          title: item.name,
          icon: handlerIconComp(
            _.get(item, "type") || _.get(item, "component_type")
          ),
        })
    );
  };

  const renderComponent = (item) => {
    const title = _.get(item, "title");
    const des = _.get(item, "des");
    const icon = _.get(item, "icon");
    const objectName = _.get(item, "object_name");

    return (
      <Component
        className={title === _.get(itemSelected, "title") ? "active" : ""}
        onClick={() => {
          const newList = [...componentList];
          newList.splice(Number(keySelectedProp), 1, item);
          setItemSelected(item);
          setNewItemSelected(newList);
        }}
      >
        <div
          style={{
            display: "flex",
            alignItems: "center",
          }}
        >
          <img src={icon} alt="" />

          <div>
            <p>{title}</p>
            <Text
              style={{ textTransform: "capitalize" }}
              ellipsis={{ tooltip: des }}
            >
              {des ? des.toLowerCase().replaceAll("_", " ") : objectName}
            </Text>
          </div>
        </div>
        {title === _.get(itemSelected, "title") && (
          <img src={IconActive} alt="icon" />
        )}
      </Component>
    );
  };

  useEffect(() => {
    $componentsCreated(normalizeComponents(components));
    //eslint-disable-next-line
  }, [components]);

  useEffect(() => {
    setData((pre) => [...componentsCreated, ...pre]);
  }, [componentsCreated]);

  useEffect(() => {
    if (visible) {
      handleScrollToElement();
    }
  }, [visible]);

  useEffect(() => {
    if (itemSelectedProp && visible) setItemSelected(itemSelectedProp);
  }, [itemSelectedProp, visible]);

  return (
    <CustomModal
      title={"Đổi component"}
      centered
      open={visible}
      onCancel={handleClose}
      width={651}
      footer={null}
    >
      <CustomContent ref={elementRef}>
        {_.map(checkDataRender(data), (item, idx) => (
          <div key={idx}>{renderComponent(item)}</div>
        ))}
      </CustomContent>
      <CustomFooter>
        <CustomButtonCancel onClick={handleClose}>
          {t("CallView.cancel")}
        </CustomButtonCancel>
        <CustomButtonSave onClick={handleSubmit}>
          {t("CallView.save")}
        </CustomButtonSave>
      </CustomFooter>
    </CustomModal>
  );
};

export default withTranslation()(memo(ModalChangeComponent));

const CustomFooter = styled.div`
  display: flex;
  justify-content: flex-end;
  padding: 18px 24px;

  border-radius: 0px 0px 10px 10px;
  background: #fff;
  box-shadow: 0px 0px 16px 0px rgba(0, 0, 0, 0.16);
  z-index: 2;
`;

const CustomButtonSave = styled(Button)`
  min-width: 90px;
  background-color: ${(props) => props.theme.main};
  color: #fff;
  margin-left: 16px;
  border-radius: 4px;

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;

const CustomButtonCancel = styled(Button)`
  min-width: 90px;
  background-color: #fff;
  border-radius: 4px;

  :hover {
    background: ${(props) => props.theme.darker};
    color: #fff;
  }
`;

const CustomContent = styled.div`
  max-height: calc(100vh - 175px);
  overflow-y: auto;

  &::-webkit-scrollbar {
    display: none;
  }
`;

const CustomModal = styled(Modal)`
  margin: 16px 0;

  .ant-modal-header {
    border-radius: 10px 10px 0 0;
    background: #f2f4f5;
    padding: 7px 24px;
  }

  .ant-modal-body {
    padding: 0;
  }

  .ant-modal-title {
    font-family: var(--roboto-700);
  }

  .ant-modal-close-x {
    height: 36.6px;
    line-height: 36.6px;

    .anticon {
      color: #141414;
    }
  }

  .ant-modal-content {
    border-radius: 10px;
  }
`;

const Component = styled.div`
  border-bottom: 1px solid rgb(236, 236, 236);
  padding: 12px;
  cursor: pointer;
  display: flex;
  align-items: center;
  justify-content: space-between;

  &.active {
    background: #dbfcfc;
  }

  &:hover {
    background: #f1f3f6;
  }

  div {
    img {
      width: 40px;
      margin-right: 8px;
    }

    p {
      margin-bottom: 0;
      color: #2c2c2c;
      font-family: var(--roboto-500);
    }

    span {
      color: #6b6b6b;
    }
  }
`;
