import _ from "lodash";
import { Button, Dropdown } from "antd";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import ModalChangeComponent from "./ModalChangeComponent";
import { getTemplateDefaultCallView } from "redux/slices/callView";
import IconSettings from "assets/icons/callView/IconSettings.svg";
import IconEditData from "assets/icons/callView/IconEditData.svg";
import IconChangeComponent from "assets/icons/callView/IconChangeComponent.svg";
import IconEditTemplate from "assets/icons/callView/IconEditTemplate.svg";
import { useNavigate } from "react-router";
// import { loadListObjectField } from "redux/slices/objects";
// import FormFieldObject from "components/FormFieldObject/FormFieldObject";

const LayoutCallView1 = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [visible, setVisible] = useState(false);
  const [componentList, setComponentlist] = useState([]);
  const { templateDefault } = useSelector((state) => state.callViewReducer);
  const [itemSelected, setItemSelected] = useState(null);
  const [keySelected, setKeySelected] = useState(null);

  //test show
  // const { listObjectField } = useSelector((state) => state.objectsReducer);
  // const [allFields, setAllFields] = useState(null);

  const handleOpenModalAdd = (addNew) => {
    setVisible(true);
    setItemSelected(addNew ? null : _.get(componentList, `[${keySelected}]`));
  };

  const renderComponent = (component) => {
    const icon = _.get(component, "icon");
    const title = _.get(component, "title");
    const des = _.get(component, "des");
    const objectName = _.get(component, "object_name");

    return (
      <ItemWrapper>
        <ImgCustom src={icon} alt="" />
        <div className="item-content">
          <p>{title}</p>
          <div
            style={{ textTransform: "capitalize" }}
            ellipsis={{ tooltip: des }}
          >
            {des ? des.toLowerCase().replaceAll("_", " ") : objectName}
          </div>
        </div>
      </ItemWrapper>
    );
  };

  useEffect(() => {
    dispatch(
      getTemplateDefaultCallView({
        object_id: "obj_contact_00000001",
      })
    );
  }, [dispatch]);

  useEffect(() => {
    if (templateDefault) {
      setComponentlist(_.get(templateDefault, "components"));
    }
  }, [templateDefault]);

  const items = [
    {
      key: "1",
      label: (
        <Option>
          <IconCustomPopup src={IconEditData} alt="" />
          <div>{t("CallView.editData")}</div>
        </Option>
      ),
    },
    {
      key: "2",
      label: (
        <Option
          onClick={() => {
            handleOpenModalAdd(false);
          }}
        >
          <IconCustomPopup src={IconChangeComponent} alt="" />
          <div>{t("CallView.changeComponent")}</div>
        </Option>
      ),
    },
  ];

  //test show
  // useEffect(() => {
  //   dispatch(
  //     loadListObjectField({
  //       api_version: "2",
  //       object_id: "obj_contact_00000001",
  //     })
  //   );
  // }, [dispatch]);

  // useEffect(() => {
  //   let newArr = [];
  //   listObjectField &&
  //     _.size(listObjectField) > 0 &&
  //     listObjectField[_.size(listObjectField) - 1]["main_object"] &&
  //     _.get(
  //       listObjectField[_.size(listObjectField) - 1],
  //       "main_object.sections"
  //     ).forEach((section) => {
  //       section.fields.forEach((item) => newArr.push(item));
  //     });
  //   setAllFields(newArr);
  // }, [listObjectField]);

  return (
    <Wrapper>
      {/* <FormFieldObject
        titleHeader={"CRM infomation"}
        listFileds={allFields}
        cols={2}
      />
      <FormFieldObject
        titleHeader={"Thông tin interaction"}
        listFileds={_.get(templateDefault, "components[2].show_fields")}
        cols={3}
      /> */}
      <div style={{ textAlign: "right" }}>
        <CustomButton
          onClick={() => navigate("/call-view-settings", { replace: true })}
        >
          <img src={IconEditTemplate} alt="" />
          <span>{t("CallView.changeTemplate")}</span>
        </CustomButton>
      </div>
      {componentList && _.size(componentList) > 0 && (
        <>
          <WrapperContent>
            <FirstLayout
              className={_.get(componentList, "[0].title") ? "active" : ""}
            >
              {_.get(componentList, "[0].title") ? (
                <>
                  <HeaderLayout>
                    <div className="title">
                      {_.get(componentList, "[0].title")}
                    </div>
                    <Dropdown
                      trigger={["click"]}
                      menu={{ items }}
                      placement="bottomRight"
                      arrow
                      onClick={() => setKeySelected(0)}
                    >
                      <IconCustom src={IconSettings} alt="" />
                    </Dropdown>
                  </HeaderLayout>
                  <ContentLayout>
                    <div className="content">
                      {renderComponent(_.get(componentList, "[0]"))}
                    </div>
                  </ContentLayout>
                </>
              ) : (
                <CustomButton
                  onClick={() => {
                    setKeySelected(0);
                    handleOpenModalAdd(true);
                  }}
                  type="primary"
                >
                  {t("CallView.addNew")}
                </CustomButton>
              )}
            </FirstLayout>
            <SecondLayout
              className={_.get(componentList, "[1].title") ? "active" : ""}
            >
              {_.get(componentList, "[1].title") ? (
                <>
                  <HeaderLayout>
                    <div className="title">
                      {_.get(componentList, "[1].title")}
                    </div>
                    <Dropdown
                      trigger={["click"]}
                      menu={{ items }}
                      placement="bottomRight"
                      arrow
                      onClick={() => setKeySelected(1)}
                    >
                      <IconCustom src={IconSettings} alt="" />
                    </Dropdown>
                  </HeaderLayout>
                  <ContentLayout>
                    <div className="content">
                      {renderComponent(_.get(componentList, "[1]"))}
                    </div>
                  </ContentLayout>
                </>
              ) : (
                <CustomButton
                  onClick={() => {
                    setKeySelected(1);
                    handleOpenModalAdd(true);
                  }}
                  type="primary"
                >
                  {t("CallView.addNew")}
                </CustomButton>
              )}
            </SecondLayout>
          </WrapperContent>
          <ThirdLayout
            className={_.get(componentList, "[2].title") ? "active" : ""}
          >
            {_.get(componentList, "[2].title") ? (
              <>
                <HeaderLayout>
                  <div className="title">
                    {_.get(componentList, "[2].title")}
                  </div>
                  <Dropdown
                    trigger={["click"]}
                    menu={{ items }}
                    placement="bottomRight"
                    arrow
                    onClick={() => setKeySelected(2)}
                  >
                    <IconCustom src={IconSettings} alt="" />
                  </Dropdown>
                </HeaderLayout>
                <ContentLayout>
                  <div className="content">
                    {renderComponent(_.get(componentList, "[2]"))}
                  </div>
                </ContentLayout>
              </>
            ) : (
              <CustomButton
                onClick={() => {
                  setKeySelected(2);
                  handleOpenModalAdd(true);
                }}
                type="primary"
              >
                {t("CallView.addNew")}
              </CustomButton>
            )}
          </ThirdLayout>
          <FourthLayout
            className={_.get(componentList, "[3].title") ? "active" : ""}
          >
            {_.get(componentList, "[3].title") ? (
              <>
                <HeaderLayout>
                  <div className="title">
                    {_.get(componentList, "[3].title")}
                  </div>
                  <Dropdown
                    trigger={["click"]}
                    menu={{ items }}
                    placement="bottomRight"
                    arrow
                    onClick={() => setKeySelected(3)}
                  >
                    <IconCustom src={IconSettings} alt="" />
                  </Dropdown>
                </HeaderLayout>
                <ContentLayout>
                  <div className="content">
                    {renderComponent(_.get(componentList, "[3]"))}
                  </div>
                </ContentLayout>
              </>
            ) : (
              <CustomButton
                onClick={() => {
                  setKeySelected(3);
                  handleOpenModalAdd(true);
                }}
                type="primary"
              >
                {t("CallView.addNew")}
              </CustomButton>
            )}
          </FourthLayout>
        </>
      )}
      <ModalChangeComponent
        visible={visible}
        setVisible={setVisible}
        keySelectedProp={keySelected}
        itemSelectedProp={itemSelected}
        componentList={componentList}
      />
    </Wrapper>
  );
};

export default withTranslation()(memo(LayoutCallView1));

const ItemWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 8px;

  .item-content {
    p {
      margin: 0;
      font-size: 14px;
      font-family: var(--roboto-500);
      line-height: 22px;
      letter-spacing: 0em;
      text-align: center;
    }

    div {
      font-size: 14px;
      line-height: 16px;
      letter-spacing: 0em;
      text-align: center;
      color: #6b6b6b;
    }
  }
`;

const HeaderLayout = styled.div`
  border-radius: 8px 8px 0 0;
  background-color: #cfe9f4;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 4px 12px;

  .title {
    font-size: 14px;
    line-height: 22px;
    letter-spacing: 0em;
    text-align: bottomRight;
    font-family: var(--roboto-500);
  }
`;

const ContentLayout = styled.div`
  padding: 8px;
  height: 100%;
  width: 100%;

  .content {
    background-color: #f5fcff;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;

const Option = styled.div`
  cursor: pointer;
  padding: 5px 12px;
  display: flex;
  align-items: center;
  gap: 8px;

  div {
    color: #2c2c2c;
    font-size: 16px;
    line-height: 22px;
  }
`;

const CustomButton = styled(Button)`
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  gap: 4px;
  padding: 4px 12px;

  span {
    font-size: 14px;
    line-height: 22px;
    letter-spacing: 0em;
  }

  img {
    width: 20px;
    height: 20px;
  }

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-primary {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;

const IconCustomPopup = styled.img`
  width: 16px;
  height: 16px;
  cursor: pointer;
`;

const IconCustom = styled.img`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const ImgCustom = styled.img`
  width: 40px;
  height: 40px;
`;

const Wrapper = styled.div`
  padding: 16px;
  display: flex;
  gap: 8px;
  flex-direction: column;
`;

const FirstLayout = styled.div`
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 204px;
  width: 100%;
  border: 1px dashed ${(props) => props.theme.main};
  flex: 1011;

  &.active {
    box-shadow: 0px 0px 6px 0px #201c1c14;
    flex-direction: column;
    border: none;
    justify-content: unset;
    align-items: unset;
  }
`;

const SecondLayout = styled.div`
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 204px;
  border: 1px dashed ${(props) => props.theme.main};
  position: relative;
  flex: 236;

  &.active {
    box-shadow: 0px 0px 6px 0px #201c1c14;
    flex-direction: column;
    border: none;
    justify-content: unset;
    align-items: unset;
  }
`;

const WrapperContent = styled.div`
  border-radius: 8px;
  display: flex;
  flex-direction: row;
  gap: 8px;
`;

const ThirdLayout = styled.div`
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 379px;
  border: 1px dashed ${(props) => props.theme.main};

  &.active {
    box-shadow: 0px 0px 6px 0px #201c1c14;
    flex-direction: column;
    border: none;
    justify-content: unset;
    align-items: unset;
  }
`;

const FourthLayout = styled.div`
  border-radius: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 494px;
  border: 1px dashed ${(props) => props.theme.main};

  &.active {
    box-shadow: 0px 0px 6px 0px #201c1c14;
    flex-direction: column;
    border: none;
    justify-content: unset;
    align-items: unset;
  }
`;
