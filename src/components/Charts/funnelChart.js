import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useLocation } from "react-router-dom";
import _ from "lodash";

import Typography from "antd/lib/typography";
import Dropdown from "antd/lib/dropdown";
import { MoreOutlined } from "@ant-design/icons";

import PinWidget from "assets/icons/report/PinWidget1.svg";
import EditWidget from "assets/icons/report/EditWidget1.svg";
import DeleteWidget from "assets/icons/report/DeleteWidget1.svg";
import UnpinWidget from "assets/icons/report/UnpinWidget1.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import { colorChart } from "util/staticData";

function FunnelChart({
  data,
  _onPinChart,
  _onDeleteChart,
  _onEditChart,
  isShowAction,
  readonly,
}) {
  const { Title } = Typography;
  const { pathname } = useLocation();
  const [showAction, $showAction] = useState(false);

  const [dataChart, setDataChart] = useState({});

  const [color, setColor] = useState([]);

  const menuAction = (readOnly, isPin, isDashboard) => {
    return (
      <WrapAction>
        {!readOnly && (
          <>
            {isPin ? (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img
                  src={
                    pathname.split("/").includes("dashboard")
                      ? DeleteWidget
                      : UnpinWidget
                  }
                  alt="pin"
                />
                <span>Unpin</span>
              </Action>
            ) : (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img src={PinWidget} alt="pin" />
                <span>Pin</span>
              </Action>
            )}
          </>
        )}

        {!isDashboard && (
          <>
            <Action
              onClick={() => {
                _onEditChart(data._id);
                $showAction(false);
              }}
            >
              <img src={EditWidget} alt="edit" />
              <span>Edit</span>
            </Action>
            <Action
              onClick={() => {
                _onDeleteChart(data._id);
                $showAction(false);
              }}
            >
              <img src={DeleteWidget} alt="delete" />
              <span>Delete</span>
            </Action>
          </>
        )}
      </WrapAction>
    );
  };

  useEffect(() => {
    let tempDatasets = [];
    let tempColor = [...colorChart];
    let listColor = [];
    let listLegend = [];

    data.datasets[0].data.map((item, idx) => {
      // const colorRandom =
      //   tempColor[Math.floor(Math.random() * tempColor.length)];
      //   tempColor = tempColor.filter((color) => color !== colorRandom);

      let colorRandom = tempColor.shift();
      return listColor.push(colorRandom);
    });

    data.labels.map((item, idx) => {
      return listLegend.push({
        label: item,
        color: listColor[idx],
      });
    });

    data.labels.map((item, idx) => {
      return tempDatasets.push({
        label: item,
        value: data.datasets[0].data[idx],
      });
    });

    const sortedByValue = _.sortBy(tempDatasets, "value");

    // setLegend(listLegend);
    setColor(listColor);
    setDataChart(sortedByValue);

    // tempDatasets = {
    //   data: data.datasets[0].data,
    //   backgroundColor: listColor,
    //   borderColor: listColor,
    // };

    // setDataChart({
    //   labels: data.labels,
    //   datasets: [tempDatasets],
    // });
  }, [data]);

  function numberWithCommas(x) {
    if (x === 0) {
      return "0";
    } else return x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  return (
    <>
      {Object.keys(dataChart).length > 0 ? (
        <Wrapper>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>
            {isShowAction && (
              <Dropdown
                visible={showAction}
                onVisibleChange={() => $showAction(!showAction)}
                overlay={menuAction(
                  readonly,
                  data.chart_config?.is_pinned_to_dashboard,
                  pathname.split("/").includes("dashboard")
                )}
                placement="bottomCenter"
                trigger={["click"]}
              >
                <MoreOutlined
                  style={{
                    color: "#bbbbbb",
                    fontSize: 24,
                  }}
                />
              </Dropdown>
            )}
          </WrapTitle>

          <Wrap>
            <WrapChart>
              {_.map(dataChart, (item, idx) => (
                <WrapItem
                  idx={idx + 1}
                  bgColor={_.get(color, idx, "#C80815")}
                  style={{
                    marginBottom: idx === 0 && 0,
                  }}
                >
                  <div>{_.get(item, "label", "")}</div>
                  <div>{numberWithCommas(_.get(item, "value", ""))}</div>
                </WrapItem>
              ))}
            </WrapChart>
          </Wrap>
        </Wrapper>
      ) : (
        <>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>
            {isShowAction && (
              <Dropdown
                visible={showAction}
                onVisibleChange={() => $showAction(!showAction)}
                overlay={menuAction(
                  readonly,
                  data.chart_config?.is_pinned_to_dashboard,
                  pathname.split("/").includes("dashboard")
                )}
                placement="bottomCenter"
                trigger={["click"]}
              >
                <MoreOutlined
                  style={{
                    color: "#bbbbbb",
                    fontSize: 24,
                  }}
                />
              </Dropdown>
            )}
          </WrapTitle>

          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>No data</span>
          </Empty>
        </>
      )}
    </>
  );
}

export default FunnelChart;

FunnelChart.defaultProps = {
  _onPinChart: () => {},
  _onDeleteChart: () => {},
  _onEditChart: () => {},
  isShowAction: true,
};

const Wrapper = styled.div`
  width: 100%;
  height: 100%;

  .ant-table-cell {
    padding: 5px 16px;
  }
`;

const Wrap = styled.div`
  display: flex;
  justify-content: center;
  height: calc(100% - 48px);
  overflow: auto;
`;

const WrapChart = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column-reverse;
`;

const WrapItem = styled.div`
  background: ${({ bgColor }) => bgColor};
  margin-bottom: 16px;
  color: #fff;
  font-size: 20px;
  height: 70px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 0 16px;
  clip-path: polygon(2% 0, 98% 0, 90% 100%, 10% 100%);
  width: ${({ idx }) => idx && 180 + idx * 70}px;
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;

  .ant-typography {
    margin-bottom: 0;
    width: 65%;
  }
`;

const WrapAction = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  padding: 5px 0;
`;

const Action = styled.div`
  cursor: pointer;
  width: 121px;
  height: 32px;
  display: flex;
  align-items: center;
  padding-left: 13px;

  img {
    margin-right: 9px;
  }

  span {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  :hover {
    background: #f5f5f5;
  }
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  height: 100%;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
