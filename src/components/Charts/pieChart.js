import { useEffect, useState } from "react";
import styled from "styled-components/macro";
import { useLocation } from "react-router-dom";
import _ from "lodash";

import Typography from "antd/lib/typography";
import Dropdown from "antd/lib/dropdown";
import Table from "antd/lib/table";
import { MoreOutlined } from "@ant-design/icons";

// import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
// import { Pie } from "react-chartjs-2";
// import ChartDataLabels from "chartjs-plugin-datalabels";

import PieCharts, {
  Legend,
  Series,
  Label,
  Font,
  Connector,
} from "devextreme-react/pie-chart";

import PinWidget from "assets/icons/report/PinWidget1.svg";
import EditWidget from "assets/icons/report/EditWidget1.svg";
import DeleteWidget from "assets/icons/report/DeleteWidget1.svg";
import UnpinWidget from "assets/icons/report/UnpinWidget1.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import { colorChart } from "util/staticData";

// ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);

function PieChart({
  data,
  _onPinChart,
  _onDeleteChart,
  _onEditChart,
  isShowAction,
  readonly,
}) {
  const { Title } = Typography;
  const { pathname } = useLocation();
  const [showAction, $showAction] = useState(false);

  const [dataChart, setDataChart] = useState({});

  const [legend, setLegend] = useState([]);

  const [color, setColor] = useState([]);

  // const options = {
  //   plugins: {
  //     legend: {
  //       display: false,
  //     },
  //     datalabels: {
  //       backgroundColor: function (context) {
  //         return context.dataset.backgroundColor;
  //       },
  //       borderRadius: 4,
  //       color: "white",
  //       font: {
  //         weight: "bold",
  //       },
  //       // formatter: Math.round,
  //       formatter: (value, ctx) => {
  //         const reducer = (previousValue, currentValue) =>
  //           previousValue + currentValue;
  //         let sum = ctx.dataset.data.reduce(reducer);
  //         let percentage = ((value * 100) / sum).toFixed(2) + "%";
  //         return percentage;
  //       },
  //       padding: 3,
  //       display: "auto",
  //     },
  //   },
  // };

  // const plugins = {
  //   plugins: [ChartDataLabels],
  // };

  const menuAction = (readOnly, isPin, isDashboard) => {
    return (
      <WrapAction>
        {!readOnly && (
          <>
            {isPin ? (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img
                  src={
                    pathname.split("/").includes("dashboard")
                      ? DeleteWidget
                      : UnpinWidget
                  }
                  alt="pin"
                />
                <span>Unpin</span>
              </Action>
            ) : (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img src={PinWidget} alt="pin" />
                <span>Pin</span>
              </Action>
            )}
          </>
        )}

        {!isDashboard && (
          <>
            <Action
              onClick={() => {
                _onEditChart(data._id);
                $showAction(false);
              }}
            >
              <img src={EditWidget} alt="edit" />
              <span>Edit</span>
            </Action>
            <Action
              onClick={() => {
                _onDeleteChart(data._id);
                $showAction(false);
              }}
            >
              <img src={DeleteWidget} alt="delete" />
              <span>Delete</span>
            </Action>
          </>
        )}
      </WrapAction>
    );
  };

  useEffect(() => {
    let tempDatasets = [];
    let tempColor = [...colorChart];
    let listColor = [];
    let listLegend = [];

    data.datasets[0].data.map((item, idx) => {
      // const colorRandom =
      //   tempColor[Math.floor(Math.random() * tempColor.length)];
      //   tempColor = tempColor.filter((color) => color !== colorRandom);

      let colorRandom = tempColor.shift();
      return listColor.push(colorRandom);
    });

    data.labels.map((item, idx) => {
      return listLegend.push({
        label: item,
        color: listColor[idx],
      });
    });

    data.labels.map((item, idx) => {
      return tempDatasets.push({
        label: item,
        value: data.datasets[0].data[idx],
      });
    });

    setLegend(listLegend);
    setColor(listColor);
    setDataChart(tempDatasets);

    // tempDatasets = {
    //   data: data.datasets[0].data,
    //   backgroundColor: listColor,
    //   borderColor: listColor,
    // };

    // setDataChart({
    //   labels: data.labels,
    //   datasets: [tempDatasets],
    // });
  }, [data]);

  function numberWithCommas(x) {
    if (x === 0) {
      return "0";
    } else return x?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const columns = [
    {
      title: _.get(data, "chart_config.label_fields[0].name", "Label"),
      dataIndex: "label",
      key: "label",
    },
    {
      title: "Value",
      dataIndex: "value",
      key: "value",
      render: (text) => numberWithCommas(text),
    },
  ];

  return (
    <>
      {Object.keys(dataChart).length > 0 ? (
        <Wrapper>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>
            {isShowAction && (
              <Dropdown
                visible={showAction}
                onVisibleChange={() => $showAction(!showAction)}
                overlay={menuAction(
                  readonly,
                  data.chart_config?.is_pinned_to_dashboard,
                  pathname.split("/").includes("dashboard")
                )}
                placement="bottomCenter"
                trigger={["click"]}
              >
                <MoreOutlined
                  style={{
                    color: "#bbbbbb",
                    fontSize: 24,
                  }}
                />
              </Dropdown>
            )}
          </WrapTitle>
          <Wrap isShowDetails={_.get(data, "chart_config.show_details", false)}>
            <WrapPie
              isShowDetails={_.get(data, "chart_config.show_details", false)}
            >
              {/* <Pie data={dataChart} options={options} plugins={plugins} /> */}
              <PieCharts
                id="pie"
                palette={color}
                dataSource={dataChart}
                resolveLabelOverlapping="shift"
              >
                <Legend visible={false} />
                <Series argumentField="label" valueField="value">
                  <Label
                    visible={true}
                    position="columns"
                    customizeText={customizeText}
                  >
                    <Font size={16} />
                    <Connector visible={true} width={0.5} />
                  </Label>
                </Series>
              </PieCharts>
            </WrapPie>

            {_.get(data, "chart_config.show_details", false) && (
              <div
                style={{
                  width: "49.5%",
                }}
              >
                <Table
                  columns={columns}
                  dataSource={dataChart}
                  pagination={false}
                  bordered
                  summary={(pageData) => {
                    let totalValue = 0;
                    pageData.forEach(({ value }) => {
                      totalValue += value;
                    });
                    return (
                      <>
                        <Table.Summary.Row>
                          <Table.Summary.Cell index={0}>
                            <span style={{ fontFamily: "var(--roboto-700)" }}>
                              Total
                            </span>
                          </Table.Summary.Cell>
                          <Table.Summary.Cell index={1}>
                            <span style={{ fontFamily: "var(--roboto-700)" }}>
                              {numberWithCommas(totalValue)}
                            </span>
                          </Table.Summary.Cell>
                        </Table.Summary.Row>
                      </>
                    );
                  }}
                />
              </div>
            )}
          </Wrap>
          <WrapLegend>
            <WrapNote>
              {legend.map((item) => {
                return (
                  <Note>
                    <ColorNote color={item.color} />
                    <span>{item.label}</span>
                  </Note>
                );
              })}
            </WrapNote>
          </WrapLegend>
        </Wrapper>
      ) : (
        <>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>
            {isShowAction && (
              <Dropdown
                visible={showAction}
                onVisibleChange={() => $showAction(!showAction)}
                overlay={menuAction(
                  readonly,
                  data.chart_config?.is_pinned_to_dashboard,
                  pathname.split("/").includes("dashboard")
                )}
                placement="bottomCenter"
                trigger={["click"]}
              >
                <MoreOutlined
                  style={{
                    color: "#bbbbbb",
                    fontSize: 24,
                  }}
                />
              </Dropdown>
            )}
          </WrapTitle>

          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>No data</span>
          </Empty>
        </>
      )}
    </>
  );
}

function customizeText(arg) {
  return `${arg.valueText} (${arg.percentText})`;
}

export default PieChart;

PieChart.defaultProps = {
  _onPinChart: () => {},
  _onDeleteChart: () => {},
  _onEditChart: () => {},
  isShowAction: true,
};

const Wrapper = styled.div`
  width: 100%;
  height: 100%;

  .ant-table-cell {
    padding: 5px 16px;
  }
`;

const Wrap = styled.div`
  display: flex;
  justify-content: ${({ isShowDetails }) =>
    isShowDetails ? "space-between" : "center"};
`;

const WrapPie = styled.div`
  width: ${({ isShowDetails }) => (isShowDetails ? "49.5%" : "500px")};
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;

  .ant-typography {
    margin-bottom: 0;
    width: 65%;
  }
`;

const WrapAction = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  padding: 5px 0;
`;

const Action = styled.div`
  cursor: pointer;
  width: 121px;
  height: 32px;
  display: flex;
  align-items: center;
  padding-left: 13px;

  img {
    margin-right: 9px;
  }

  span {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  :hover {
    background: #f5f5f5;
  }
`;

const WrapLegend = styled.div`
  margin-top: 8px;
  display: flex;
  justify-content: center;

  span {
    font-size: 12px;
    color: #2c2c2c;
  }
`;

const ColorNote = styled.div`
  background: ${(props) => (props.color ? props.color : "#000")};
  width: 14px;
  height: 14px;
  border-radius: 50%;
  margin-right: 8px;
`;

const WrapNote = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

const Note = styled.div`
  margin-right: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 8px;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  height: 100%;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
