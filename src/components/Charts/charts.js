import { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";

import EmptyDashboard from "assets/images/reports/empty-dashboard.jpg";

import Spin from "antd/lib/spin";

import {
  getDataChart,
  pinToDashBoard,
  unpinReport,
  resetStatus,
} from "redux/slices/reportDetails";

import LineChart from "./lineChart";
import BarComboChart from "./barComboChart";
import PieChart from "./pieChart";
import BarHorizontalChart from "./barHorizontalChart";
import FunnelChart from "./funnelChart";

import DeleteWidget from "assets/icons/report/DeleteWidget.svg";

function Charts({
  chart,
  allCondition,
  anyCondition,
  _onDeleteChart,
  _onEditChart,
  width,
  height,
  onRemove,
  readonly,
}) {
  const dispatch = useDispatch();
  const { objectID, recordID } = useParams();
  const { listCharts, isLoading, statusPin, notFoundChart } = useSelector(
    (state) => state.reportDetailsReducer
  );

  const [data, setData] = useState([]);

  const loadDataChart = useCallback(() => {
    setData({
      ...chart,
      isLoading: true,
    });

    if (allCondition.length > 0 || anyCondition.length > 0) {
      dispatch(
        getDataChart({
          _id: chart._id,
          api_version: "2",
          quick_filter: {
            and_filter: allCondition,
            or_filter: anyCondition,
          },
        })
      );
    } else {
      dispatch(
        getDataChart({
          _id: chart._id,
          api_version: "2",
        })
      );
    }
  }, [chart, allCondition, anyCondition, dispatch]);

  const _onPinChart = () => {
    let pathname = window.location.pathname.split("/");
    if (onRemove && pathname[pathname.length - 1] === "dashboard") {
      onRemove();
    }
    if (pathname[pathname.length - 1] !== "dashboard") {
      if (data.chart_config?.is_pinned_to_dashboard) {
        dispatch(
          unpinReport({
            chart_id: chart._id,
            api_version: "2",
          })
        );
      } else {
        dispatch(
          pinToDashBoard({
            chart_id: chart._id,
            type: "chart",
            api_version: "2",
            object_id: objectID,
            report_id: recordID,
            chart_type: data.type,
          })
        );
      }
    }
  };

  useEffect(() => {
    if (!window.location.pathname.includes("/dashboard")) {
      loadDataChart();
    }
    /*eslint-disable-next-line*/
  }, [loadDataChart]);

  useEffect(() => {
    if (window.location.pathname.includes("/dashboard")) {
      loadDataChart();
    }
    /*eslint-disable-next-line*/
  }, []);

  useEffect(() => {
    listCharts.forEach((item) => {
      if (item._id === chart._id) {
        setData(item);
      }
    });
  }, [listCharts, chart]);

  useEffect(() => {
    if (isLoading === false) {
      if (
        (statusPin?.status === "unpin success" ||
          statusPin?.status === "pin success") &&
        statusPin?._id === chart._id
      ) {
        loadDataChart();
        dispatch(resetStatus());
      }
    }
  }, [chart, dispatch, isLoading, loadDataChart, statusPin]);

  useEffect(() => {
    if (notFoundChart.status === true && notFoundChart.id === chart._id) {
      setData({
        ...chart,
        isLoading: false,
      });
    }
  }, [chart, notFoundChart]);

  return (
    <Wrapper width={width} height={height}>
      {data.isLoading ? (
        <WrapSpin>
          <Spin />
        </WrapSpin>
      ) : (
        <>
          {data.type === "line" && (
            <LineChart
              data={data}
              _onPinChart={_onPinChart}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
              readonly={readonly}
            />
          )}
          {(data.type === "bar" || data.type === "combo") && (
            <BarComboChart
              data={data}
              _onPinChart={_onPinChart}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
              readonly={readonly}
            />
          )}

          {data.type === "bar_horizontal" && (
            <BarHorizontalChart
              data={data}
              _onPinChart={_onPinChart}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
              readonly={readonly}
            />
          )}

          {data.type === "pie" && (
            <PieChart
              data={data}
              _onPinChart={_onPinChart}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
              readonly={readonly}
            />
          )}

          {data.type === "funnel" && (
            <FunnelChart
              data={data}
              _onPinChart={_onPinChart}
              _onDeleteChart={_onDeleteChart}
              _onEditChart={_onEditChart}
              readonly={readonly}
            />
          )}

          {notFoundChart.status === true && notFoundChart.id === chart._id && (
            <>
              {!readonly && (
                <ImageEmpty
                  onClick={_onPinChart}
                  src={DeleteWidget}
                  alt="remove"
                />
              )}

              <NotFound>
                <img src={EmptyDashboard} alt="empty" />
                <span>Chart Not Found</span>
              </NotFound>
            </>
          )}
        </>
      )}
    </Wrapper>
  );
}

export default Charts;

Charts.defaultProps = {
  readonly: false,
};

const Wrapper = styled.div`
  width: ${(props) => props?.width};
  height: ${(props) =>
    props.height
      ? props.height
      : window.location.pathname.includes("dashboard")
      ? "100%"
      : "510px"};
  padding: 20px 20px 28px 20px;
  background: #fff;
  overflow: auto;
  border: ${(props) =>
    window.location.pathname.includes("dashboard")
      ? "none"
      : "1px solid #d9d9d9"};
`;

const WrapSpin = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;

  .ant-spin-dot-item {
    background-color: ${(props) => props.theme.main};
  }
`;

const ImageEmpty = styled.img`
  float: right;
  cursor: pointer;
`;

const NotFound = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  height: 100%;

  span {
    margin-top: 8px;
  }

  img {
    width: fit-content;
  }
`;
