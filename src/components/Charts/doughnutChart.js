import { useEffect, useState } from "react";
import styled from "styled-components/macro";
// import { useLocation } from "react-router-dom";

import Typography from "antd/lib/typography";
// import TooltipAntd from "antd/lib/tooltip";

import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";

import { colorChart } from "util/staticData";

ChartJS.register(ArcElement, Tooltip, Legend);

function DoughnutChart({ data }) {
  const { Title } = Typography;
  // const { pathname } = useLocation();

  const [dataChart, setDataChart] = useState({});

  const [legend, setLegend] = useState([]);
  //   const [color, setColor] = useState([]);

  const options = {
    plugins: {
      legend: {
        display: false,
      },
    },
  };

  const plugins = [
    {
      beforeDraw: function (chart) {
        var width = chart.width,
          height = chart.height,
          ctx = chart.ctx;

        ctx.restore();
        var fontSize = (height / 114).toFixed(2);
        ctx.font = fontSize + "em sans-serif";
        ctx.textBaseline = "middle";

        var text = `${data.percentage}%`,
          textX = Math.round((width - ctx.measureText(text).width) / 2),
          textY = height / 2;

        ctx.fillText(text, textX, textY);
        ctx.save();
      },
    },
  ];

  useEffect(() => {
    let tempColor = [...colorChart];
    let listColor = [];
    let listLegend = [];
    let tempDatasets = [...data.datasets];

    data.datasets[0].data.map((item, idx) => {
      // const colorRandom =
      //   tempColor[Math.floor(Math.random() * tempColor.length)];
      //   tempColor = tempColor.filter((color) => color !== colorRandom);

      let colorRandom = tempColor.shift();
      return listColor.push(colorRandom);
    });

    data.labels.map((item, idx) => {
      return listLegend.push({
        label: item,
        color: listColor[idx],
      });
    });

    tempDatasets = {
      data: data.datasets[0].data,
      backgroundColor: listColor,
      borderColor: listColor,
    };

    setLegend(listLegend);
    // setColor(listColor);
    setDataChart({
      name: data.name,
      labels: data.labels,
      datasets: [tempDatasets],
    });
  }, [data]);

  return (
    <>
      {Object.keys(dataChart).length > 0 && (
        <Wrapper>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>
            {/* {isShowAction && (
              <Action>
                {data.chart_config?.is_pinned_to_dashboard ? (
                  <TooltipAntd title="Unpin">
                    <img onClick={_onPinChart} src={PinedWidget} alt="pin" />
                  </TooltipAntd>
                ) : (
                  <TooltipAntd title="Pin">
                    <img onClick={_onPinChart} src={PinWidget} alt="pin" />
                  </TooltipAntd>
                )}
                <TooltipAntd title="Edit">
                  <img
                    onClick={() => _onEditChart(data._id)}
                    src={EditWidget}
                    alt="edit"
                  />
                </TooltipAntd>

                {!pathname.split("/").includes("dashboard") && (
                  <TooltipAntd title="Delete">
                    <img
                      onClick={() => _onDeleteChart(data._id)}
                      src={DeleteWidget}
                      alt="delete"
                    />
                  </TooltipAntd>
                )}
              </Action>
            )} */}
          </WrapTitle>
          <Wrap>
            <WrapPie>
              <Doughnut data={dataChart} options={options} plugins={plugins} />
            </WrapPie>
          </Wrap>
          <WrapLegend>
            <WrapNote>
              {legend.map((item) => {
                return (
                  <Note>
                    <ColorNote color={item.color} />
                    <span>{item.label}</span>
                  </Note>
                );
              })}
            </WrapNote>
          </WrapLegend>
        </Wrapper>
      )}
    </>
  );
}

export default DoughnutChart;

const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Wrap = styled.div`
  display: flex;
  justify-content: center;
`;

const WrapPie = styled.div`
  width: 300px;
`;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;

  .ant-typography {
    margin-bottom: 0;
    width: 65%;
  }
`;

// const Action = styled.div`
//   img {
//     cursor: pointer;
//     margin-left: 8px;

//     :hover {
//       background: #eeeeee;
//     }
//   }
// `;

const WrapLegend = styled.div`
  margin-top: 8px;
  display: flex;
  justify-content: center;

  span {
    font-size: 12px;
    color: #2c2c2c;
  }
`;

const ColorNote = styled.div`
  background: ${(props) => (props.color ? props.color : "#000")};
  width: 14px;
  height: 14px;
  border-radius: 50%;
  margin-right: 8px;
`;

const WrapNote = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
`;

const Note = styled.div`
  margin-right: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 8px;
`;
