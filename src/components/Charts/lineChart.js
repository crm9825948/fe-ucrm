import { useEffect, useState } from "react";
import styled from "styled-components";
import { useLocation } from "react-router-dom";

import Typography from "antd/lib/typography";
import Dropdown from "antd/lib/dropdown";
import { MoreOutlined } from "@ant-design/icons";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";

import PinWidget from "assets/icons/report/PinWidget1.svg";
import EditWidget from "assets/icons/report/EditWidget1.svg";
import DeleteWidget from "assets/icons/report/DeleteWidget1.svg";
import UnpinWidget from "assets/icons/report/UnpinWidget1.svg";
import EmptyObject from "assets/images/sharing/EmptyObject.webp";

import { colorChart } from "util/staticData";

ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Tooltip,
  Legend,
  ChartDataLabels
);

function LineChart({
  data,
  _onPinChart,
  _onDeleteChart,
  _onEditChart,
  readonly,
}) {
  const { Title } = Typography;
  const { pathname } = useLocation();

  const [dataChart, setDataChart] = useState({});
  const [showAction, $showAction] = useState(false);

  const [legend, setLegend] = useState([]);

  const options = {
    plugins: {
      legend: {
        display: false,
      },
      datalabels: {
        backgroundColor: function (context) {
          return context.dataset.backgroundColor;
        },
        borderRadius: 4,
        color: "white",
        font: {
          weight: "bold",
        },
        formatter: Math.round,
        padding: 3,
        display: false,
      },
    },
  };

  const plugins = {
    plugins: [ChartDataLabels],
  };

  const menuAction = (readOnly, isPin, isDashboard) => {
    return (
      <WrapAction>
        {!readOnly && (
          <>
            {isPin ? (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img
                  src={
                    pathname.split("/").includes("dashboard")
                      ? DeleteWidget
                      : UnpinWidget
                  }
                  alt="pin"
                />
                <span>Unpin</span>
              </Action>
            ) : (
              <Action
                onClick={() => {
                  $showAction(false);
                  _onPinChart();
                }}
              >
                <img src={PinWidget} alt="pin" />
                <span>Pin</span>
              </Action>
            )}
          </>
        )}

        {!isDashboard && (
          <>
            <Action
              onClick={() => {
                _onEditChart(data._id);
                $showAction(false);
              }}
            >
              <img src={EditWidget} alt="edit" />
              <span>Edit</span>
            </Action>
            <Action
              onClick={() => {
                _onDeleteChart(data._id);
                $showAction(false);
              }}
            >
              <img src={DeleteWidget} alt="delete" />
              <span>Delete</span>
            </Action>
          </>
        )}
      </WrapAction>
    );
  };

  useEffect(() => {
    let tempDatasets = [...data.datasets];
    let tempColor = [...colorChart];
    let listLegend = [];

    data.datasets.map((item, idx) => {
      // const colorRandom =
      //   tempColor[Math.floor(Math.random() * tempColor.length)];
      //   tempColor = tempColor.filter((color) => color !== colorRandom);

      let colorRandom = tempColor.shift();
      return (tempDatasets[idx] = {
        ...tempDatasets[idx],
        borderColor: colorRandom,
        backgroundColor: colorRandom,
      });
    });

    tempDatasets.forEach((item) => {
      listLegend.push({
        label: item.label,
        color: item.backgroundColor,
      });
    });

    setLegend(listLegend);

    setDataChart({
      labels: data.labels,
      datasets: tempDatasets,
    });
  }, [data]);

  return (
    <>
      {Object.keys(dataChart).length > 0 ? (
        <Wrapper>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>

            <Dropdown
              visible={showAction}
              onVisibleChange={() => $showAction(!showAction)}
              overlay={menuAction(
                readonly,
                data.chart_config?.is_pinned_to_dashboard,
                pathname.split("/").includes("dashboard")
              )}
              placement="bottomCenter"
              trigger={["click"]}
            >
              <MoreOutlined
                style={{
                  color: "#bbbbbb",
                  fontSize: 24,
                }}
              />
            </Dropdown>
          </WrapTitle>

          <WrapLegend>
            {legend.map((item) => {
              return (
                <Note>
                  <ColorNote color={item.color} />
                  <span>{item.label}</span>
                </Note>
              );
            })}
          </WrapLegend>
          <Line
            data={dataChart}
            height={163}
            options={options}
            plugins={plugins}
          />
        </Wrapper>
      ) : (
        <>
          <WrapTitle>
            <Title level={3} ellipsis={{ tooltip: data.name }}>
              {data.name}
            </Title>

            <Dropdown
              visible={showAction}
              onVisibleChange={() => $showAction(!showAction)}
              overlay={menuAction(
                readonly,
                data.chart_config?.is_pinned_to_dashboard,
                pathname.split("/").includes("dashboard")
              )}
              placement="bottomCenter"
              trigger={["click"]}
            >
              <MoreOutlined
                style={{
                  color: "#bbbbbb",
                  fontSize: 24,
                }}
              />
            </Dropdown>
          </WrapTitle>

          <Empty>
            <img src={EmptyObject} alt="empty" />
            <span>No data</span>
          </Empty>
        </>
      )}
    </>
  );
}

export default LineChart;

const Wrapper = styled.div``;

const WrapTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;

  .ant-typography {
    margin-bottom: 0;
    width: 65%;
  }
`;

const WrapAction = styled.div`
  display: flex;
  flex-direction: column;
  background: #fff;
  box-shadow: 0px 3px 6px -4px rgba(0, 0, 0, 0.12),
    0px 6px 16px rgba(0, 0, 0, 0.08), 0px 9px 28px 8px rgba(0, 0, 0, 0.05);
  padding: 5px 0;
`;

const Action = styled.div`
  cursor: pointer;
  width: 121px;
  height: 32px;
  display: flex;
  align-items: center;
  padding-left: 13px;

  img {
    margin-right: 9px;
  }

  span {
    font-size: 16px;
    line-height: 22px;
    color: #2c2c2c;
  }

  :hover {
    background: #f5f5f5;
  }
`;

const WrapLegend = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;

  span {
    font-size: 12px;
    color: #2c2c2c;
  }
`;

const ColorNote = styled.div`
  background: ${(props) => (props.color ? props.color : "#000")};
  width: 14px;
  height: 14px;
  border-radius: 50%;
  margin-right: 8px;
`;

const Note = styled.div`
  margin-right: 24px;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-bottom: 8px;
`;

const Empty = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  height: 100%;

  p {
    color: #2c2c2c;
    margin-bottom: 16px;
  }

  span {
    color: ${(props) => props.theme.main};
  }

  img {
    margin-bottom: 8px;
  }
`;
