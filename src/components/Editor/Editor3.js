import React, { useState, useEffect, useMemo } from "react";
import styled from "styled-components/macro";
import JoditEditor from "jodit-react";
import _ from "lodash";

import { BASE_URL_API, BE_URL } from "constants/constants";
import CopyIcon from "assets/icons/common/copy.svg";
import AppendIcon from "assets/icons/email/appendFields.svg";

function Editor3({
  editorJodit,
  content,
  objectID,
  showToolbar,
  showAppend,
  optionsAppend,
  minHeight,
  readonly,
  onBlur,
  isIC,
  onChange,
}) {
  const [fieldsAppend, $fieldsAppend] = useState({
    label: ["Select..."],
    value: ["Select..."],
  });

  const copyStringToClipboard = function (str) {
    var el = document.createElement("textarea");
    el.value = str;
    el.setAttribute("readonly", "");
    el.style = { position: "absolute", left: "-9999px" };
    document.body.appendChild(el);
    el.select();
    document.execCommand("copy");
    document.body.removeChild(el);
  };

  const createOptionGroupElement = (mergeFields, optionGrouplabel) => {
    let optionGroupElement = document.createElement("optgroup");
    optionGroupElement.setAttribute("label", optionGrouplabel);
    for (let index = 0; index < mergeFields.length; index++) {
      let optionElement = document.createElement("option");

      if (index === 0) {
        optionElement.setAttribute("disabled", true);
        optionElement.setAttribute("selected", true);
      }

      optionElement.setAttribute(
        "value",
        _.get(fieldsAppend, "value")[
          _.get(fieldsAppend, "label").findIndex(
            (item) => item === mergeFields[index]
          )
        ]
      );
      optionElement.text = mergeFields[index];
      optionGroupElement.appendChild(optionElement);
    }

    return optionGroupElement;
  };

  const buttons = [
    "undo",
    "redo",
    "|",
    "bold",
    "strikethrough",
    "underline",
    "italic",
    "|",
    "superscript",
    "subscript",
    "|",
    "align",
    "|",
    "ul",
    "ol",
    "outdent",
    "indent",
    "|",
    "font",
    "fontsize",
    "brush",
    "paragraph",
    "|",
    "image",
    "link",
    "table",
    "|",
    "hr",
    "eraser",
    "copyformat",
    "|",
    "selectall",
    {
      name: "copyContent",
      tooltip: "Copy HTML to Clipboard",
      iconURL: CopyIcon,
      exec: function (editor) {
        let html = editor.value;
        copyStringToClipboard(html);
      },
    },
    "print",
    "|",
    // "source",
  ];

  const buttonAppend = [
    {
      name: "appendField",
      tooltip: "Append Field",
      iconURL: AppendIcon,
      popup: (editor, current, self, close) => {
        function onSelected(e) {
          let mergeField = e.target.value;
          if (mergeField) {
            editor.selection.insertNode(editor.create.fromHTML(mergeField));
            close();
          }
        }
        let divElement = editor.create.div("merge-field-popup");

        let selectElement = document.createElement("select");
        selectElement.appendChild(
          createOptionGroupElement(
            _.get(fieldsAppend, "label"),
            "Append fields"
          )
        );
        selectElement.onchange = onSelected;
        divElement.appendChild(selectElement);
        return divElement;
      },
    },
  ];

  useEffect(() => {
    let temp = {
      label: ["Select..."],
      value: ["Select..."],
    };
    optionsAppend.forEach((item) => {
      temp.label.push(item.label);
      temp.value.push(`$${item.value}`);
    });

    $fieldsAppend(temp);
  }, [optionsAppend]);

  const editorConfig = useMemo(
    () => ({
      readonly: readonly,
      toolbar: true,
      spellcheck: false,
      language: "en",
      toolbarButtonSize: "medium",
      toolbarAdaptive: false,
      showCharsCounter: false,
      showWordsCounter: false,
      showXPathInStatusbar: false,
      askBeforePasteHTML: false,
      askBeforePasteFromWord: false,
      minHeight: minHeight,
      buttons: showToolbar
        ? showAppend
          ? buttons.concat(buttonAppend)
          : buttons
        : buttonAppend,
      style: { font: "18px Arial" },
      uploader: {
        url: isIC
          ? localStorage.getItem("icIntegration_link_api") +
            "resource/api/custom-api/upload-file"
          : BASE_URL_API + "upload-file",
        insertImageAsBase64URI: false,
        headers: {
          Authorization: isIC
            ? "Bearer " + localStorage.getItem("icIntegration_accessToken")
            : localStorage.getItem("setting_accessToken"),
        },
        filesVariableName: function (t) {
          return "file";
        },
        withCredentials: false,
        format: "json",
        method: "POST",
        prepareData: function (formData) {
          if (isIC) {
            formData.append("channel_type", "email");
          } else {
            formData.append("obj", objectID);
          }
          return formData;
        },
        isSuccess: function (e) {
          return e;
        },
        process: function (resp) {
          return {
            files: isIC
              ? _.get(resp, "data.items", [])
              : [BE_URL + resp.data[0]],
          };
        },
        error: function (error) {
          console.log("error :>> ", error);
        },
        defaultHandlerSuccess: function (data) {
          const j = this;
          console.log("data.fields", data.files);
          if (data.files && data.files.length) {
            const tagName = "img";
            data.files.forEach((filename) => {
              const elm = j.createInside.element(tagName);
              elm.setAttribute(
                "src",
                isIC ? _.get(filename, "url", "") : filename
              );
              j.s.insertImage(elm, null, j.o.imageDefaultWidth);
            });
          }
        },
      },
    }),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  return (
    <Wrapper>
      <JoditEditor
        ref={editorJodit}
        value={content}
        config={editorConfig}
        onBlur={onBlur}
        onChange={onChange}
      />
    </Wrapper>
  );
}

export default Editor3;

Editor3.defaultProps = {
  editorJodit: null,
  content: "",
  objectID: undefined,
  showToolbar: true,
  showAppend: true,
  optionsAppend: [],
  minHeight: "250px",
  readonly: false,
  onBlur: () => {},
  onChange: () => {},
  isIC: false,
};

const Wrapper = styled.div`
  .jodit-wysiwyg {
    p {
      margin-bottom: 0;
    }
  }

  .jodit-toolbar-button_appendField {
    span {
      width: 100px;
    }
  }
`;
