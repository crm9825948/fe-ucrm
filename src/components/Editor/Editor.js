import { Modal, Checkbox, Row, Button } from "antd";
import React, { useEffect, useRef, useState } from "react";
import SunEditor from "suneditor-react";
import styled from "styled-components";
import * as allPlugin from "../../../node_modules/suneditor/src/plugins/index";
import { BASE_URL_API, BE_URL } from "../../constants/constants";
import axios from "axios";
import { checkTokenExpiration } from "contexts/TokenCheck";

const Editor = (props) => {
  const custom_container = {
    // @Required @Unique
    // plugin name
    name: "custom_container",

    // @Required
    // data display
    display: "container",

    // @Required
    // add function - It is called only once when the plugin is first run.
    // This function generates HTML to append and register the event.
    // arguments - (core : core object, targetElement : clicked button element)
    add: function (core, targetElement) {
      // @Required
      // Registering a namespace for caching as a plugin name in the context object
      const context = core.context;
      context.custom_container = {};

      // Generate submenu HTML
      // Always bind "core" when calling a plugin function
      let listDiv = this.setSubmenu(core);

      // You must bind "core" object when registering an event.
      /** add event listeners */
      listDiv
        .querySelector(".se-form-group")
        .addEventListener("click", this.onClick.bind(core));

      // @Required
      // You must add the "submenu" element using the "core.initMenuTarget" method.
      /** append target button menu */
      core.initMenuTarget(this.name, targetElement, listDiv);
    },

    setSubmenu: function (core) {
      const listDiv = core.util.createElement("DIV");
      // eslint-disable-next-line
      const icons = core.icons; // assets/defaultIcons.js
      listDiv.className = "se-menu-container se-submenu se-list-layer";
      listDiv.innerHTML =
        "" +
        '<div class="se-list-inner">' +
        '<div class="se-form-group">' +
        // @Required
        // The "position" style of each element surrounding the button must be "relative".
        // suneditor.css: .sun-editor .se-form-group > div {position:relative;}
        "<div>" +
        '<p style="font-weight:500"> Append Field </p>' +
        "</div>" +
        "</div>" +
        "</div>";

      return listDiv;
    },

    onClick: function (e) {
      setShowDrawer(true);
      e.preventDefault();
      e.stopPropagation();

      // let target = e.target;
      // let command = '';

      // while (!command && !/^UL$/i.test(target.tagName)) {
      //     command = target.getAttribute('data-command');
      //     if (command) break;
      //     target = target.parentNode;
      // }

      // if (!command) return;

      // const plugin = this.plugins[command];
      // this.actionCall(command, (plugin ? plugin.display : ''), target);
    },
  };
  const editor = useRef();
  const getSunEditorInstance = (sunEditor) => {
    editor.current = sunEditor;
  };
  const {
    showToolbar,
    optionsAppend,
    showAppendField,
    content,
    handleChange,
    folder_name,
    limited,
    deselect,
    minHeightInput,
  } = props;
  const [countText, countRealText] = useState(0);

  const optionsSunEditor = {
    mode: "classic",
    rtl: false,
    imageGalleryUrl:
      "https://etyswjpn79.execute-api.ap-northeast-1.amazonaws.com/suneditor-demo",
    katex: "window.katex",
    // "imageFileInput": false,
    tabDisable: false,
    resizingBar: false,
    showPathLabel: false,
    minHeight: minHeightInput,
    plugins: [
      allPlugin.font,
      allPlugin.fontSize,
      allPlugin.formatBlock,
      allPlugin.fontColor,
      allPlugin.link,
      allPlugin.align,
      allPlugin.image,
      allPlugin.list,
      custom_container,
    ],
    buttonList: [
      [
        "font",
        "fontSize",
        "formatBlock",
        "bold",
        "underline",
        "italic",
        "strike",
        "fontColor",
        "link",
        "align",
        "list",
        "image",
        {
          name: "custom_container",
          dataDisplay: "container",
          title: "Append field",
          buttonClass: "button-append-field",
          innerHTML:
            '<svg viewBox="0 0 24 24" style="width:24px;height:24px;"><path fill="currentColor" d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z" /></svg>',
        },
      ],
    ],
    maxCharCount: limited,
  };
  const optionsSunEditorWithoutToolbar = {
    mode: "classic",
    rtl: false,
    imageGalleryUrl:
      "https://etyswjpn79.execute-api.ap-northeast-1.amazonaws.com/suneditor-demo",
    katex: "window.katex",
    // "imageFileInput": false,
    tabDisable: false,
    minHeight: "20vh",
    resizingBar: false,
    showPathLabel: false,
    plugins: [custom_container],
    buttonList: [
      [
        {
          name: "custom_container",
          dataDisplay: "container",
          title: "Append field",
          buttonClass: "button-append-field",
          innerHTML:
            '<svg viewBox="0 0 24 24" style="width:24px;height:24px;"><path fill="currentColor" d="M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z" /></svg>',
        },
      ],
    ],
    maxCharCount: limited,
  };
  const [showDrawer, setShowDrawer] = useState(false);
  const [appendText, setAppendText] = useState();
  const [checkboxAppend, setCheckBoxAppend] = useState([]);
  const changeFieldAppend = (e) => {
    setCheckBoxAppend(e);
    if (e.length) {
      let text = "";
      // eslint-disable-next-line
      e.map((item) => {
        text = text + " $" + item;
      });
      setAppendText(text);
    }
  };
  const uploadImage = (file, info, uploadHandler) => {
    if (!checkTokenExpiration()) return;

    const formData = new FormData();
    formData.append("file", file[0]);
    formData.append("obj", folder_name);
    axios
      .post(BASE_URL_API + "upload-file", formData, {
        headers: {
          Authorization: localStorage.getItem("setting_accessToken"),
        },
      })
      .then((res) => {
        const response = {
          result: [
            {
              url: BE_URL + res.data.data[0],
              name: file[0].name,
              size: file[0].size,
            },
          ],
        };
        uploadHandler(response);
      });
  };
  useEffect(() => {
    if (deselect && deselect.length > 0) {
      editor.current.setContents(content);
    }
    // eslint-disable-next-line
  }, [deselect]);
  return (
    <CustomContent showAppendField={showAppendField}>
      <SunEditor
        setContents={content}
        getSunEditorInstance={getSunEditorInstance}
        onKeyDown={() => {
          setTimeout(() => {
            countRealText(editor.current.getText().length);
          }, 10);
        }}
        onChange={(e) => {
          handleChange(e);
          countRealText(editor.current.getText().length);
        }}
        onImageUploadBefore={(file, info, uploadHandler) =>
          uploadImage(file, info, uploadHandler)
        }
        // appendContents={text}
        setOptions={
          showToolbar ? optionsSunEditor : optionsSunEditorWithoutToolbar
        }
        setDefaultStyle="font-family: Arial; font-size: 18px;"
      />
      {limited && (
        <p>
          {countText}/{limited}
        </p>
      )}
      <Modal
        visible={showDrawer}
        mask={false}
        footer={null}
        onCancel={() => {
          setShowDrawer(false);
          setCheckBoxAppend([]);
        }}
      >
        <CheckboxGroup
          style={{ width: "100%" }}
          onChange={(e) => changeFieldAppend(e)}
          value={checkboxAppend}
        >
          {optionsAppend &&
            optionsAppend.map((item) => {
              return (
                <Row>
                  <Checkbox value={item.value}>{item.label}</Checkbox>
                </Row>
              );
            })}
        </CheckboxGroup>

        <WrapButton>
          <Button
            type="primary"
            onClick={() => {
              setShowDrawer(false);
              appendText &&
                appendText.length !== 0 &&
                editor.current.insertHTML(appendText);
              setCheckBoxAppend([]);
            }}
          >
            OK
          </Button>
          <Button
            onClick={() => {
              setShowDrawer(false);
              setCheckBoxAppend([]);
            }}
          >
            Cancel
          </Button>
        </WrapButton>
      </Modal>
    </CustomContent>
  );
};

export default Editor;

Editor.defaultProps = {
  minHeightInput: "250px",
};

const CustomContent = styled.div`
  .sun-editor .se-btn {
    font-size: 18px;
  }
  .sun-editor .se-wrapper .se-wrapper-inner {
    max-height: 480px;
    height: unset !important;
  }

  .button-append-field {
    display: ${(props) => (props.showAppendField ? "block" : "none")};
  }

  .ant-checkbox-group-item {
    display: block;
    margin-right: 0;
  }

  .sun-editor-editable {
    height: auto;
  }

  .se-dialog-image {
    ._se_tab_link {
      :last-child {
        display: none;
      }
    }
  }

  .se-dialog-form-footer {
    label {
      :last-child {
        display: none !important;
      }
    }
  }
`;

const CheckboxGroup = styled(Checkbox.Group)`
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
  }
`;

const WrapButton = styled.div`
  display: flex;
  justify-content: flex-end;

  .ant-btn-primary {
    margin-right: 16px;
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;
  }

  .ant-btn {
    font-size: 16px;
    height: unset;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;
