import React from "react";
import { useSelector } from "redux/store";
import styled from "styled-components";
import CRMLogo from "../assets/icons/common/crmlogo.png";

const LoadingScreen = () => {
  const { showLoadingScreen } = useSelector((state) => state.globalReducer);
  return (
    <>
      {showLoadingScreen && (
        <Wrapper>
          <img alt="" src={CRMLogo} />
          <div className="lds-ellipsis">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
          </div>
        </Wrapper>
      )}
    </>
  );
};

export default LoadingScreen;

const Wrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;
  background-color: rgba(255, 255, 255, 0.5);
  position: absolute;
  top: 0;
  left: 0;
  z-index: 10000 !important;
  transition: all 5s ease-in;
  img {
    margin-top: 300px;
    width: 250px;
  }
  .lds-ellipsis {
    width: 70px;
    /* display: inline-block; */
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: grey;
  }
  .lds-ellipsis div {
    position: absolute;
    top: 33px;
    width: 13px;
    height: 13px;
    left: 50%;
    border-radius: 50%;
    background: ${(props) => props.theme.main};
    animation-timing-function: cubic-bezier(0, 1, 1, 0);
  }
  .lds-ellipsis div:nth-child(1) {
    left: 8px;
    animation: lds-ellipsis1 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(2) {
    left: 8px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(3) {
    left: 32px;
    animation: lds-ellipsis2 0.6s infinite;
  }
  .lds-ellipsis div:nth-child(4) {
    left: 56px;
    animation: lds-ellipsis3 0.6s infinite;
  }
  @keyframes lds-ellipsis1 {
    0% {
      transform: scale(0);
    }
    100% {
      transform: scale(1);
    }
  }
  @keyframes lds-ellipsis3 {
    0% {
      transform: scale(1);
    }
    100% {
      transform: scale(0);
    }
  }
  @keyframes lds-ellipsis2 {
    0% {
      transform: translate(0, 0);
    }
    100% {
      transform: translate(24px, 0);
    }
  }
`;
