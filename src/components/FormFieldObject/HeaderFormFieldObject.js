import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import IconSettings from "assets/icons/callView/IconSettings.svg";
import IconEditData from "assets/icons/callView/IconEditData.svg";
import IconChangeComponent from "assets/icons/callView/IconChangeComponent.svg";
import { Dropdown } from "antd";
import { memo } from "react";

const HeaderFormFieldObject = ({ titleHeader }) => {
  const { t } = useTranslation();

  const items = [
    {
      key: "1",
      label: (
        <Option>
          <IconCustomPopup src={IconEditData} alt="" />
          <div>{t("CallView.editData")}</div>
        </Option>
      ),
    },
    {
      key: "2",
      label: (
        <Option>
          <IconCustomPopup src={IconChangeComponent} alt="" />
          <div>{t("CallView.changeComponent")}</div>
        </Option>
      ),
    },
  ];

  return (
    <HeaderLayout>
      <div className="title">{titleHeader}</div>
      <Dropdown menu={{ items }} placement="bottomRight" arrow>
        <IconCustom src={IconSettings} alt="" />
      </Dropdown>
    </HeaderLayout>
  );
};

export default withTranslation()(memo(HeaderFormFieldObject));

const HeaderLayout = styled.div`
  padding: 12px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background-color: #fff;

  .title {
    font-size: 16px;
    line-height: 22px;
    letter-spacing: 0em;
    font-family: var(--roboto-500);
  }
`;

const IconCustom = styled.img`
  width: 24px;
  height: 24px;
  cursor: pointer;
`;

const Option = styled.div`
  cursor: pointer;
  padding: 5px 12px;
  display: flex;
  align-items: center;
  gap: 8px;

  div {
    color: #2c2c2c;
    font-size: 16px;
    line-height: 22px;
  }
`;

const IconCustomPopup = styled.img`
  width: 16px;
  height: 16px;
  cursor: pointer;
`;
