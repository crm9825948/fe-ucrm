import _ from "lodash";
import styled from "styled-components";
import { useTranslation, withTranslation } from "react-i18next";
import { memo, useCallback, useMemo } from "react";
import {
  Form,
  Select,
  Input,
  DatePicker,
  InputNumber,
  Button,
  Empty,
} from "antd";
import HeaderFormFieldObject from "./HeaderFormFieldObject";

const inputComponents = {
  Input: Input,
  Number: InputNumber,
  TextArea: Input.TextArea,
  Select: Select,
  DatePicker: DatePicker,
};

const RenderType = (type) => {
  switch (type) {
    case "date":
    case "datetime-local":
      return "DatePicker";
    case "select":
      return "Select";
    case "number":
      return "Number";
    case "textArea":
      return "TextArea";
    default:
      return "Input";
  }
};

const FormFieldObject = ({ titleHeader, listFileds, cols = 2 }) => {
  const [form] = Form.useForm();
  const { t } = useTranslation();

  const RenderInput = (type, optionsSelect) => {
    const Component = inputComponents[type];
    if (Component) {
      const commonProps = {
        style: { width: "100%" },
        row: type === "TextArea" ? 3 : undefined,
        options: optionsSelect || undefined,
        format: type === "DatePicker" ? "YYYY-MM-DD HH:mm:ss" : undefined,
        showTime: type === "DatePicker",
      };
      return <Component {...commonProps} />;
    }
    return null;
  };

  const renderField = useCallback(
    (item, index) => {
      const fieldID = _.get(item, "ID");
      const fieldName = _.get(item, "name");
      const fieldType = RenderType(_.get(item, "type"));
      const isRequired = _.get(item, "required");

      return (
        <FormItem
          style={{
            width: `calc(${100 / cols}% - ${(32 * (cols - 1)) / cols}px)`,
          }}
          key={fieldID}
          name={fieldID}
          label={<LabelForm>{fieldName}</LabelForm>}
          className={(index + 1) % cols === 0 ? "" : "marginRight"}
          rules={[
            {
              required: isRequired,
              message: "Field is required!",
            },
          ]}
        >
          {RenderInput(fieldType, _.get(item, "option"))}
        </FormItem>
      );
    },
    [cols]
  );

  const formItems = useMemo(() => {
    if (!listFileds) return null;
    return listFileds.map((item, index) => renderField(item, index));
  }, [listFileds, renderField]);

  const _onSubmit = (values) => {
    console.log(values);
  };

  return (
    <Wrapper>
      <HeaderFormFieldObject titleHeader={titleHeader} />
      {listFileds && _.size(listFileds) > 0 ? (
        <>
          <ContentLayout>
            <FormCustom
              layout="vertical"
              name="contactForm"
              autoComplete="off"
              onFinish={_onSubmit}
              form={form}
            >
              {formItems}
            </FormCustom>
          </ContentLayout>
          <Footer>
            <WrapperBtn>
              <CustomButton
                onClick={() => {
                  form.resetFields();
                  console.log("reset");
                }}
              >
                {t("common.cancel")}
              </CustomButton>
              <CustomButton type="primary" form="contactForm" htmlType="submit">
                {t("common.save")}
              </CustomButton>
            </WrapperBtn>
          </Footer>
        </>
      ) : (
        <div
          style={{
            width: "100%",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
        </div>
      )}
    </Wrapper>
  );
};

export default withTranslation()(memo(FormFieldObject));

const WrapperBtn = styled.div`
  display: flex;
  gap: 12px;
  flex-direction: row;
`;

const CustomButton = styled(Button)`
  width: 114px;
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  padding: 4px 12px;

  &.ant-btn-default:hover {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-default:focus {
    color: ${(props) => props.theme.main};
    border-color: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }

  &.ant-btn-primary {
    color: #fff;
    border-color: ${(props) => props.theme.main};
    background: ${(props) => props.theme.main};
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
    box-shadow: 0 2px 0 rgba(0, 0, 0, 0.045);
  }
`;

const Footer = styled.div`
  padding: 12px 0;
  width: 100%;
  height: 56px;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

const LabelForm = styled.div`
  color: #2c2c2c;
  font-size: 14px;
  font-family: var(--roboto-500);
  line-height: 22px;
`;

const FormCustom = styled(Form)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
`;

const FormItem = styled(Form.Item)`
  /* width:; */

  &.marginRight {
    margin-right: 32px;
  }
`;

const Wrapper = styled.div`
  box-sizing: border-box;
  padding: 0 12px;
  border-radius: 8px;
  min-height: 100px;
  max-height: 500px;
  width: 100%;
  display: flex;
  flex-direction: column;
  border: 1px solid #d9d9d9;
  box-shadow: 0px 0px 6px 0px #201c1c14;
`;

const ContentLayout = styled.div`
  width: 100%;
  height: 100%;
  overflow-y: auto;

  .content {
    background-color: #f5fcff;
    height: 100%;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`;
