import styled from "styled-components/macro";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";

import Breadcrumb from "antd/lib/breadcrumb";
import Select from "antd/lib/select";

function SelectObject({ nameBreadcrumb, listObjects, onChange }) {
  const { t } = useTranslation();
  const navigate = useNavigate();

  return (
    <Wrapper>
      <Breadcrumb>
        <Breadcrumb.Item onClick={() => navigate("/settings")}>
          {t("consolidatedView.setting")}
        </Breadcrumb.Item>
        <BreadcrumbItem>{nameBreadcrumb}</BreadcrumbItem>
      </Breadcrumb>

      <WrapSelectObject>
        <span style={{ fontSize: "16px", marginRight: "16px" }}>
          {t("coreSetting.selectObject")}
        </span>
        <Select
          placeholder={t("common.placeholderSelect")}
          options={listObjects}
          onChange={onChange}
          optionFilterProp={false}
          showSearch
        />
      </WrapSelectObject>
    </Wrapper>
  );
}

export default SelectObject;

const Wrapper = styled.div`
  margin-bottom: 16px;

  .ant-breadcrumb-link {
    cursor: pointer;
  }
`;

const BreadcrumbItem = styled(Breadcrumb.Item)`
  font-family: var(--roboto-500);
  font-size: 18px !important;
  color: #2c2c2c;
  cursor: default;
`;

const WrapSelectObject = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  .ant-select {
    width: 400px;
  }
`;
