import React from "react";
import styled from "styled-components";
import { InfoCircleOutlined, WarningOutlined } from "@ant-design/icons";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import { optionsTimeRangeReport } from "util/staticData";
import moment from "moment";
import { Select, Tooltip, InputNumber, DatePicker } from "antd";
import { useState } from "react";
import { useEffect } from "react";
const { Option } = Select;

const TimeRangeFilter = ({
  timeRangeFilter,
  $timeRangeFilter,
  listObjectField,
}) => {
  const { t } = useTranslation();
  const [listFieldsMain, setListFieldsMain] = useState([]);
  useEffect(() => {
    if (listObjectField.length > 0) {
      let tempFieldsMain = [];
      listObjectField.forEach((item) => {
        if (
          Object.values(item)[0] !== null &&
          (Object.values(item)[0].readable || Object.values(item)[0].writeable)
        ) {
          if (Object.keys(item)[0] === "main_object") {
            Object.values(item)[0].sections.forEach((ele) => {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false
                ) {
                  tempFieldsMain.push({
                    label: field.related_name,
                    value: field.full_field_id,
                    type: field.type,
                    is_indexed: _.get(field, "is_indexed", false),
                    // formula_type: !field?.in_words && (field?.formula_type || ""),
                    formula_type:
                      field.type !== "formula"
                        ? ""
                        : field?.formula_type === "dateDiff"
                        ? !field?.in_words
                          ? field?.formula_type
                          : ""
                        : field?.formula_type,
                  });
                }
              });
            });
          }
        }
      });
      setListFieldsMain(tempFieldsMain);
    }
  }, [listObjectField]);
  const handlerTimeRange = {
    filterField: (value, option) => {
      $timeRangeFilter({
        filter_field_id: _.get(option, "value", ""),
        field_type: _.get(option, "type", ""),
        filter_type: "all-time",
        amount_of_time: null,
        start_time: null,
        end_time: null,
      });
    },
    filterType: (value) => {
      switch (value) {
        case "last-n-days":
        case "last-n-weeks":
        case "last-n-months":
        case "last-n-years":
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: 3,
            start_time: null,
            end_time: null,
          });
          break;

        default:
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: null,
            start_time: null,
            end_time: null,
          });
          break;
      }
    },
    amountOfTime: (value) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        amount_of_time: value ? value : 1,
      });
    },
    startTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        start_time: valueString ? valueString : null,
      });
    },
    endTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        end_time: valueString ? valueString : null,
      });
    },
  };
  const disabledStartDate = (current) => {
    return (
      timeRangeFilter.end_time &&
      timeRangeFilter.end_time <
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  const disabledEndDate = (current) => {
    return (
      timeRangeFilter.start_time &&
      timeRangeFilter.start_time >
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  return (
    <TimeRange>
      <legend>
        Time range
        <Tooltip title={t("report.desTimeRange")}>
          <InfoCircleOutlined style={{ marginLeft: "8px", fontSize: "18px" }} />
        </Tooltip>
      </legend>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          width: "100%",
        }}
      >
        <div style={{ display: "flex" }}>
          <Select
            value={_.get(timeRangeFilter, "filter_field_id", "modify_time")}
            onChange={handlerTimeRange.filterField}
            showSearch
            optionFilterProp="children"
            filterOption={(inputValue, option) => {
              if (option.children) {
                return option.children
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              } else if (option.label) {
                return option.label
                  .toLowerCase()
                  .indexOf(inputValue.toLowerCase()) >= 0
                  ? true
                  : false;
              }
            }}
          >
            {(
              listFieldsMain.filter(
                (item) =>
                  item?.type === "datetime-local" || item?.type === "date"
              ) || []
            ).map((item) => (
              <Option
                key={item.value}
                style={{ fontWeight: item.is_indexed ? "700" : "400" }}
                type={item.type}
              >
                {item.label}
              </Option>
            ))}
          </Select>
          <Select
            options={optionsTimeRangeReport}
            value={_.get(timeRangeFilter, "filter_type", "all-time")}
            onChange={handlerTimeRange.filterType}
          />

          {(_.get(timeRangeFilter, "filter_type", "") === "last-n-days" ||
            _.get(timeRangeFilter, "filter_type", "") === "last-n-weeks" ||
            _.get(timeRangeFilter, "filter_type", "") === "last-n-months" ||
            _.get(timeRangeFilter, "filter_type", "") === "last-n-years") && (
            <InputNumber
              value={_.get(timeRangeFilter, "amount_of_time", 3)}
              min={1}
              onChange={handlerTimeRange.amountOfTime}
            />
          )}

          {_.get(timeRangeFilter, "filter_type", "") === "custom" && (
            <>
              <DatePicker
                showTime={
                  _.get(timeRangeFilter, "field_type", "") === "datetime-local"
                }
                onChange={handlerTimeRange.startTime}
                placeholder={t("knowledgeBase.startDate")}
                disabledDate={disabledStartDate}
                value={
                  _.get(timeRangeFilter, "start_time")
                    ? moment(_.get(timeRangeFilter, "start_time"))
                    : undefined
                }
              />
              <DatePicker
                showTime={
                  _.get(timeRangeFilter, "field_type", "") === "datetime-local"
                }
                onChange={handlerTimeRange.endTime}
                placeholder={t("knowledgeBase.endDate")}
                disabledDate={disabledEndDate}
                value={
                  _.get(timeRangeFilter, "end_time")
                    ? moment(_.get(timeRangeFilter, "end_time"))
                    : undefined
                }
              />
            </>
          )}
        </div>

        {listFieldsMain.find(
          (item) =>
            _.get(item, "value") === _.get(timeRangeFilter, "filter_field_id")
        )?.is_indexed === false && (
          <p
            style={{
              marginBottom: 0,
              marginTop: "8px",
              color: "#708090",
            }}
          >
            <WarningOutlined style={{ color: "#eb7029", fontSize: "18px" }} />{" "}
            {t("report.warningIndex")}
          </p>
        )}
      </div>
    </TimeRange>
  );
};

export default TimeRangeFilter;
const SelectedColumn = styled.fieldset`
  border: 1px solid #d9d9d9;
  padding: 16px 16px 0 16px;
  border-radius: 5px;

  legend {
    width: fit-content;
    color: #2c2c2c;
    font-size: 16px;
    font-family: var(--roboto-700);
    padding: 0 8px;
    margin-bottom: 0;
    border-bottom: none;

    span {
      font-family: var(--roboto-400);
    }
  }
`;
const TimeRange = styled(SelectedColumn)`
  display: flex;
  margin-bottom: 16px;
  padding-bottom: 16px;

  legend {
    display: flex;
    align-items: center;
  }

  .ant-select {
    width: 28%;
    margin-right: 16px;
  }

  .ant-input-number {
    width: 28%;
  }
`;
