import React from "react";
import styled from "styled-components";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import { optionsTimeRangeReport } from "util/staticData";
import moment from "moment";
import { Select, InputNumber, DatePicker } from "antd";
import { useEffect } from "react";
const { Option } = Select;

const TimeRangeQuickFilter = ({
  timeRangeFilter,
  $timeRangeFilter,
  listObjectField,
  listFieldsMain,
  setListFieldsMain,
}) => {
  const { t } = useTranslation();
  useEffect(() => {
    if (listObjectField.length > 0) {
      let tempFieldsMain = [];
      listObjectField.forEach((item) => {
        if (
          Object.values(item)[0] !== null &&
          (Object.values(item)[0].readable || Object.values(item)[0].writeable)
        ) {
          if (Object.keys(item)[0] === "main_object") {
            Object.values(item)[0].sections.forEach((ele) => {
              ele.fields.forEach((field) => {
                if (
                  field.hidden === false &&
                  field.permission_hidden === false
                ) {
                  tempFieldsMain.push({
                    label: field.related_name,
                    value: field.full_field_id,
                    type: field.type,
                    is_indexed: _.get(field, "is_indexed", false),
                    // formula_type: !field?.in_words && (field?.formula_type || ""),
                    formula_type:
                      field.type !== "formula"
                        ? ""
                        : field?.formula_type === "dateDiff"
                        ? !field?.in_words
                          ? field?.formula_type
                          : ""
                        : field?.formula_type,
                  });
                }
              });
            });
          }
        }
      });
      setListFieldsMain(tempFieldsMain);
    }
    //eslint-disable-next-line
  }, [listObjectField]);
  const handlerTimeRange = {
    filterField: (value, option) => {
      $timeRangeFilter({
        filter_field_id: _.get(option, "value", ""),
        field_type: _.get(option, "type", ""),
        filter_type: "all-time",
        amount_of_time: null,
        start_time: null,
        end_time: null,
      });
    },
    filterType: (value) => {
      switch (value) {
        case "last-n-days":
        case "last-n-weeks":
        case "last-n-months":
        case "last-n-years":
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: 3,
            start_time: null,
            end_time: null,
          });
          break;

        default:
          $timeRangeFilter({
            ...timeRangeFilter,
            filter_type: value,
            amount_of_time: null,
            start_time: null,
            end_time: null,
          });
          break;
      }
    },
    amountOfTime: (value) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        amount_of_time: value ? value : 1,
      });
    },
    startTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        start_time: valueString ? valueString : null,
      });
    },
    endTime: (value, valueString) => {
      $timeRangeFilter({
        ...timeRangeFilter,
        end_time: valueString ? valueString : null,
      });
    },
  };
  const disabledStartDate = (current) => {
    return (
      timeRangeFilter.end_time &&
      timeRangeFilter.end_time <
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  const disabledEndDate = (current) => {
    return (
      timeRangeFilter.start_time &&
      timeRangeFilter.start_time >
        moment(current).endOf("day").format("YYYY-MM-DD")
    );
  };

  return (
    <TimeRange>
      <div style={{ display: "flex", gap: "0.5rem" }}>
        <Select
          style={{ height: "40px" }}
          value={_.get(timeRangeFilter, "filter_field_id", "modify_time")}
          onChange={handlerTimeRange.filterField}
          showSearch
          optionFilterProp="children"
          filterOption={(inputValue, option) => {
            if (option.children) {
              return option.children
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            } else if (option.label) {
              return option.label
                .toLowerCase()
                .indexOf(inputValue.toLowerCase()) >= 0
                ? true
                : false;
            }
          }}
        >
          {(
            listFieldsMain.filter(
              (item) =>
                item?.type === ("datetime-local" || item?.type === "date")
              // item?.label.toLowerCase() !== "start time" &&
              // item?.label.toLowerCase() !== "end time"
            ) || []
          ).map((item) => (
            <Option key={item.value} type={item.type}>
              {item.label}
            </Option>
          ))}
        </Select>
        <Select
          options={optionsTimeRangeReport}
          value={_.get(timeRangeFilter, "filter_type", "all-time")}
          onChange={handlerTimeRange.filterType}
        />

        {(_.get(timeRangeFilter, "filter_type", "") === "last-n-days" ||
          _.get(timeRangeFilter, "filter_type", "") === "last-n-weeks" ||
          _.get(timeRangeFilter, "filter_type", "") === "last-n-months" ||
          _.get(timeRangeFilter, "filter_type", "") === "last-n-years") && (
          <InputNumber
            value={_.get(timeRangeFilter, "amount_of_time", 3)}
            min={1}
            onChange={handlerTimeRange.amountOfTime}
          />
        )}

        {_.get(timeRangeFilter, "filter_type", "") === "custom" && (
          <>
            <DatePicker
              showTime={
                _.get(timeRangeFilter, "field_type", "") === "datetime-local"
              }
              onChange={handlerTimeRange.startTime}
              placeholder={t("knowledgeBase.startDate")}
              disabledDate={disabledStartDate}
              value={
                _.get(timeRangeFilter, "start_time")
                  ? moment(_.get(timeRangeFilter, "start_time"))
                  : undefined
              }
            />
            <DatePicker
              showTime={
                _.get(timeRangeFilter, "field_type", "") === "datetime-local"
              }
              onChange={handlerTimeRange.endTime}
              placeholder={t("knowledgeBase.endDate")}
              disabledDate={disabledEndDate}
              value={
                _.get(timeRangeFilter, "end_time")
                  ? moment(_.get(timeRangeFilter, "end_time"))
                  : undefined
              }
            />
          </>
        )}
      </div>
    </TimeRange>
  );
};

export default TimeRangeQuickFilter;
const SelectedColumn = styled.fieldset``;
const TimeRange = styled(SelectedColumn)`
  display: flex;
  gap: 0.5rem;
  max-width: 800px;

  .ant-select {
    height: 40px;
    width: 200px;
    display: flex;
    align-items: center;
  }

  .ant-input-number {
    display: flex;
    align-items: center;
  }

  .ant-picker {
    border-radius: 5px;
  }

  .ant-select {
    width: 150px;

    .ant-select-selector {
      width: 150px;
      height: 40px;
      display: flex;
      align-items: center;
      border-radius: 5px;
    }
  }
`;
