import React, { useState, useEffect } from "react";
import styled from "styled-components/macro";
import { useDispatch } from "react-redux";

import Popover from "antd/lib/popover";
import Button from "antd/lib/button";
import "./VoiceBiometric.scss";

import IconVoiceBiometric from "assets/icons/VoiceBiometric/iconVoice.svg";
import NotRegister from "assets/icons/VoiceBiometric/notRegister.svg";
import AlreadyRegister from "assets/icons/VoiceBiometric/alreadyRegister.svg";
import Waiting from "assets/icons/VoiceBiometric/warning.svg";
import AuthenPass from "assets/icons/VoiceBiometric/authenPass.svg";
import AuthenFailed from "assets/icons/VoiceBiometric/authenFailed.svg";
import WaveAudio from "assets/icons/VoiceBiometric/waveaudio.png";
import AuthenAutoSuccess from "assets/icons/VoiceBiometric/authenAutoSuccess.png";
import AuthenAutoFailed from "assets/icons/VoiceBiometric/authenAutoFailed.png";
import AuthenAutoWarning from "assets/icons/VoiceBiometric/authenAutoWarning.png";

import { callEvent } from "redux/slices/voiceBiometric";
import { Notification } from "components/Notification/Noti";

function VoiceBiometric({ inCall, showVoice, setShowVoice, dataWebSocket }) {
  const dispatch = useDispatch();

  let dataCall = localStorage.getItem("dataCall");
  let agentName = localStorage.getItem("agent-callName");
  let screenVoiceBiometric = localStorage.getItem("screenVoice");
  let statusRegisterVoiceBiometric = localStorage.getItem("statusRegister");
  let statusAuthenAutoVoiceBiometric = localStorage.getItem("statusAuthenAuto");

  let inforAgentCIF = localStorage.getItem("inforAgentCIF");
  let inforAgentName = localStorage.getItem("inforAgentName");
  let inforAgentChoice = localStorage.getItem("inforAgentChoice");
  let statusAuthenManuallyVoice = localStorage.getItem("statusAuthenManually");

  const [statusAuthenManually, setStatusAuthenManually] = useState("");
  const [statusAuthenAuto, setStatusAuthenAuto] = useState("");
  const [screenVoice, setScreenVoice] = useState("notRegister");

  const [statusRegister, setStatusRegister] = useState("");
  const [inforAgent, setInforAgent] = useState({
    name: "",
    cif: "",
    last_choice: "",
  });

  const [authen, setAuthen] = useState(false);

  useEffect(() => {
    if (
      dataWebSocket.current_session === dataWebSocket.last_session &&
      inCall &&
      !authen &&
      statusRegister === "Đã đăng ký"
    ) {
      setTimeout(() => {
        setAuthen(true);

        dispatch(
          callEvent({
            voice_biometric_event: "voice_authentication",
            phone_incoming: JSON.parse(dataCall).phone,
            third_party_id: JSON.parse(dataCall).third_party_id,
            record_id: JSON.parse(dataCall).data[0]._id,
            object_id: JSON.parse(dataCall).object_id,
            agent_name: agentName,
          })
        );
      }, 20000);
    }
  }, [
    agentName,
    authen,
    dataCall,
    dispatch,
    inCall,
    statusRegister,
    dataWebSocket,
  ]);

  const _onAuthen = (status) => {
    if (status === "failed") {
      setStatusAuthenManually(status);
    }

    if (status === "success") {
      dispatch(
        callEvent({
          voice_biometric_event: "voice_authentication_manual",
          phone_incoming: JSON.parse(dataCall).phone,
          third_party_id: JSON.parse(dataCall).third_party_id,
          record_id: JSON.parse(dataCall).data[0]._id,
          object_id: JSON.parse(dataCall).object_id,
          agent_name: agentName,
        })
      );
    }
  };

  const handleShowVoice = () => {
    setShowVoice(!showVoice);
  };

  const _onButton1 = (type) => {
    switch (type) {
      case "decline":
        dispatch(
          callEvent({
            voice_biometric_event: "decline_voice_registration",
            phone_incoming: JSON.parse(dataCall).phone,
            third_party_id: JSON.parse(dataCall).third_party_id,
            record_id: JSON.parse(dataCall).data[0]._id,
            object_id: JSON.parse(dataCall).object_id,
            agent_name: agentName,
          })
        );
        break;
      case "unregister":
        localStorage.setItem("statusAuthenAuto", "warning");
        break;
      case "notUnregister":
        localStorage.setItem("statusAuthenAuto", "success");
        break;
      case "manual":
        localStorage.setItem("statusAuthenAuto", "manual");
        break;
      default:
        break;
    }
  };

  const _onCancel = (type) => {
    switch (type) {
      case "skip":
        setShowVoice(false);
        break;
      case "unregister":
        dispatch(
          callEvent({
            voice_biometric_event: "unregister_voice",
            phone_incoming: JSON.parse(dataCall).phone,
            third_party_id: JSON.parse(dataCall).third_party_id,
            record_id: JSON.parse(dataCall).data[0]._id,
            object_id: JSON.parse(dataCall).object_id,
            agent_name: agentName,
          })
        );

        break;
      default:
        break;
    }
  };

  const _onRegister = () => {
    dispatch(
      callEvent({
        voice_biometric_event: "register_voice",
        phone_incoming: JSON.parse(dataCall).phone,
        third_party_id: JSON.parse(dataCall).third_party_id,
        record_id: JSON.parse(dataCall).data[0]._id,
        object_id: JSON.parse(dataCall).object_id,
        agent_name: agentName,
      })
    );
  };

  const _onButton2 = (type) => {
    switch (type) {
      case "unregister":
        localStorage.setItem("statusAuthenAuto", "warning2");
        break;

      case "notUnregister":
        localStorage.setItem("statusAuthenAuto", "manual");
        break;

      default:
        break;
    }
  };

  useEffect(() => {
    if (inCall) {
      setShowVoice(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [inCall]);

  useEffect(() => {
    if (Object.keys(dataWebSocket).length > 0) {
      switch (dataWebSocket.event) {
        case "voice_authentication_manual":
          if (dataWebSocket.data.status) {
            localStorage.setItem("statusAuthenManually", "true");
            Notification("success", dataWebSocket.data.message);
          } else {
            Notification("error", dataWebSocket.data.message);
          }
          break;

        case "decline_voice_registration":
          if (dataWebSocket.data.status) {
            Notification("success", dataWebSocket.data.message);
            setShowVoice(false);
          } else {
            Notification("error", dataWebSocket.data.message);
          }
          break;

        case "register_voice":
          if (dataWebSocket.data.status) {
            Notification("success", dataWebSocket.data.message);
            setShowVoice(false);
          } else {
            Notification("error", dataWebSocket.data.message);
          }
          break;

        case "voice_authentication":
          if (dataWebSocket.data.status) {
            localStorage.setItem("statusAuthenAuto", "success");
          } else {
            localStorage.setItem("statusAuthenAuto", "failed");
          }
          break;
        case "unregister_voice":
          if (dataWebSocket.data.status) {
            Notification("success", dataWebSocket.data.message);
            setShowVoice(false);
            localStorage.setItem("popup-voice", "false");
          } else {
            Notification("error", dataWebSocket.data.message);
          }
          break;

        default:
          break;
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataWebSocket]);

  useEffect(() => {
    setScreenVoice(screenVoiceBiometric);
  }, [screenVoiceBiometric]);

  useEffect(() => {
    setStatusRegister(statusRegisterVoiceBiometric);
  }, [statusRegisterVoiceBiometric]);

  useEffect(() => {
    setStatusAuthenAuto(statusAuthenAutoVoiceBiometric);
  }, [statusAuthenAutoVoiceBiometric]);

  useEffect(() => {
    setInforAgent({
      name: inforAgentName,
      cif: inforAgentCIF,
      last_choice: inforAgentChoice,
    });
  }, [inforAgentCIF, inforAgentName, inforAgentChoice]);

  useEffect(() => {
    if (statusAuthenManuallyVoice === "true") {
      setStatusAuthenManually("success");
    }
  }, [statusAuthenManuallyVoice]);

  return (
    <Popover
      content={
        <Wrapper>
          <div id="waveform"></div>
          <Title>
            <span>Voice Biometric</span>
          </Title>
          <Body>
            {screenVoice === "notRegister" || screenVoice === "waiting" ? (
              <>
                <WrapStatus>
                  {screenVoice === "notRegister" ? (
                    <Status status="notRegister">
                      <img src={NotRegister} alt="NotRegister" />
                      <span>{statusRegister}</span>
                    </Status>
                  ) : (
                    <Status status="waiting">
                      <img src={Waiting} alt="Waiting" />
                      <span>{statusRegister}</span>
                    </Status>
                  )}
                </WrapStatus>
                <Name>
                  <span>Họ tên: &nbsp;</span>
                  <p>{inforAgent.name}</p>
                </Name>
                <CIF>
                  <span>CIF: &nbsp;</span>
                  <p>{inforAgent.cif}</p>
                </CIF>
                <LastChoice>
                  <span>Lựa chọn trước: &nbsp;</span>
                  <p>{inforAgent.last_choice}</p>
                </LastChoice>
                <AuthenManually>
                  <span>Xác thực thủ công</span>
                  <ResultAuthenManually>
                    <Result
                      active={statusAuthenManually === "success"}
                      onClick={() => _onAuthen("success")}
                    >
                      <img src={AuthenPass} alt="authenPass" />
                      <span>Thành công</span>
                    </Result>
                    <Result
                      active={statusAuthenManually === "failed"}
                      onClick={() => _onAuthen("failed")}
                    >
                      <img src={AuthenFailed} alt="authenFailed" />
                      <span>Thất bại</span>
                    </Result>
                  </ResultAuthenManually>
                </AuthenManually>
              </>
            ) : (
              <>
                {statusAuthenAuto === "waiting" ? (
                  <>
                    <WrapStatus>
                      <Status status="alreadyRegister">
                        <img src={AlreadyRegister} alt="AlreadyRegister" />
                        <span>Đã đăng ký</span>
                      </Status>
                    </WrapStatus>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                    <AuthenAuto>
                      <span>Đang xác thực khách hàng</span>
                      <img src={WaveAudio} alt="waveAudio" />
                    </AuthenAuto>
                  </>
                ) : statusAuthenAuto === "success" ? (
                  <WrapAuthenAutoSuccess>
                    <img src={AuthenAutoSuccess} alt="authenAutoSuccess" />
                    <StatusAuthenAuto>
                      Chúc mừng! Giọng nói đã Xác thực thành công
                    </StatusAuthenAuto>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                  </WrapAuthenAutoSuccess>
                ) : statusAuthenAuto === "failed" ? (
                  <WrapAuthenAutoSuccess>
                    <img src={AuthenAutoFailed} alt="authenAutoFailed" />
                    <StatusAuthenAuto>
                      Thất bại! Giọng nói Xác thực chưa thành công
                    </StatusAuthenAuto>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                  </WrapAuthenAutoSuccess>
                ) : statusAuthenAuto === "warning" ? (
                  <WrapAuthenAutoSuccess>
                    <img src={AuthenAutoWarning} alt="authenAutoWarning" />
                    <StatusAuthenAuto>
                      Bạn có chắc chắn muốn huỷ đăng ký xác thực bằng giọng nói?
                    </StatusAuthenAuto>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                  </WrapAuthenAutoSuccess>
                ) : statusAuthenAuto === "warning2" ? (
                  <WrapAuthenAutoSuccess>
                    <img src={AuthenAutoWarning} alt="authenAutoWarning" />
                    <StatusAuthenAuto>
                      Bạn có chắc chắn muốn huỷ đăng ký xác thực bằng giọng nói?
                    </StatusAuthenAuto>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                  </WrapAuthenAutoSuccess>
                ) : statusAuthenAuto === "manual" ? (
                  <>
                    <Name>
                      <span>Họ tên: &nbsp;</span>
                      <p>{inforAgent.name}</p>
                    </Name>
                    <CIF>
                      <span>CIF: &nbsp;</span>
                      <p>{inforAgent.cif}</p>
                    </CIF>
                    <LastChoice>
                      <span>Lựa chọn trước: &nbsp;</span>
                      <p>{inforAgent.last_choice}</p>
                    </LastChoice>
                    <AuthenManually>
                      <span>Xác thực thủ công</span>
                      <ResultAuthenManually>
                        <Result
                          active={statusAuthenManually === "success"}
                          onClick={() => _onAuthen("success")}
                        >
                          <img src={AuthenPass} alt="authenPass" />
                          <span>Thành công</span>
                        </Result>
                        <Result
                          active={statusAuthenManually === "failed"}
                          onClick={() => _onAuthen("failed")}
                        >
                          <img src={AuthenFailed} alt="authenFailed" />
                          <span>Thất bại</span>
                        </Result>
                      </ResultAuthenManually>
                    </AuthenManually>
                  </>
                ) : (
                  ""
                )}
              </>
            )}
          </Body>
          {statusAuthenAuto !== "waiting" &&
            statusAuthenAuto !== "manual" &&
            statusAuthenAuto !== "warning2" && (
              <BtnAction>
                {screenVoice !== "waiting" && (
                  <Button
                    type={statusAuthenAuto === "failed" ? "primary" : "default"}
                    onClick={() =>
                      _onButton1(
                        statusAuthenAuto === "success"
                          ? "unregister"
                          : statusAuthenAuto === "failed"
                          ? "manual"
                          : statusAuthenAuto === "warning"
                          ? "notUnregister"
                          : "decline"
                      )
                    }
                  >
                    {statusAuthenAuto === "success"
                      ? "Huỷ đăng ký"
                      : statusAuthenAuto === "failed"
                      ? "Xác thực thủ công"
                      : statusAuthenAuto === "warning"
                      ? "Không huỷ"
                      : "Từ chối đăng ký"}
                  </Button>
                )}
                {statusAuthenManually === "success" ? (
                  <Button onClick={_onRegister} type="primary">
                    Đăng ký
                  </Button>
                ) : (
                  statusAuthenAuto !== "failed" && (
                    <Button
                      type="primary"
                      onClick={() =>
                        _onCancel(
                          statusAuthenAuto === "warning" ? "unregister" : "skip"
                        )
                      }
                    >
                      {statusAuthenAuto === "success"
                        ? "OK"
                        : statusAuthenAuto === "warning"
                        ? "Huỷ đăng ký"
                        : "Bỏ qua"}
                    </Button>
                  )
                )}
              </BtnAction>
            )}

          {statusAuthenAuto === "waiting" &&
            screenVoice !== "alreadyRegister" && (
              <BtnAction>
                <Button type="primary" onClick={() => _onCancel("skip")}>
                  Bỏ qua
                </Button>
              </BtnAction>
            )}

          {(statusAuthenAuto === "manual" ||
            statusAuthenAuto === "warning2") && (
            <BtnAction>
              <Button
                type="primary"
                onClick={() =>
                  _onButton2(
                    statusAuthenAuto === "warning2"
                      ? "notUnregister"
                      : "unregister"
                  )
                }
              >
                {statusAuthenAuto === "warning2" ? "Không huỷ" : "Huỷ đăng ký"}
              </Button>
              <Button
                type="primary"
                onClick={() =>
                  _onCancel(
                    statusAuthenAuto === "warning2" ? "unregister" : "skip"
                  )
                }
              >
                {statusAuthenAuto === "warning2" ? "Huỷ đăng ký" : "Bỏ qua"}
              </Button>
            </BtnAction>
          )}
        </Wrapper>
      }
      trigger="click"
      overlayClassName="customPopoverVoice"
      placement="bottom"
      visible={showVoice}
      onVisibleChange={handleShowVoice}
    >
      <IconVoice>
        <img src={IconVoiceBiometric} alt="voice" />
      </IconVoice>
    </Popover>
  );
}

export default VoiceBiometric;

const Wrapper = styled.div`
  width: 307px;
`;

const Title = styled.div`
  padding: 14px 8px;
  box-shadow: 0px 0px 17px rgba(0, 0, 0, 0.1);
  border-radius: 10px 10px 0 0;

  span {
    font-family: var(--roboto-700);
    font-size: 14px;
    color: #6b6b6b;
  }
`;

const Body = styled.div`
  padding: 0 16px;
`;

const WrapStatus = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 16px;
`;

const Status = styled.div`
  background: ${({ status }) =>
    status === "notRegister"
      ? "#7d7d7d"
      : status === "waiting"
      ? "#FFF4E9"
      : status === "alreadyRegister"
      ? "#E9FCE9"
      : "unset"};
  border: 1px solid
    ${({ status }) =>
      status === "waiting"
        ? "#FA8C16"
        : status === "alreadyRegister"
        ? "#0BC90D"
        : "unset"};
  border-radius: 19px;
  padding: 4px 8px;
  width: 112px;

  span {
    font-family: var(--roboto-500);
    font-size: 12px;
    color: ${({ status }) =>
      status === "notRegister"
        ? "#fff"
        : status === "waiting"
        ? "#FA8C16"
        : status === "alreadyRegister"
        ? "#0BC90D"
        : "#000   "};
    margin-left: 4px;
  }
`;

const Name = styled.div`
  margin-top: 16px;
  font-size: 16px;
  color: #2c2c2c;
  display: flex;
  align-items: center;
  align-self: flex-start;

  p {
    margin-bottom: 0;
    font-family: var(--roboto-700);
  }
`;

const CIF = styled(Name)`
  margin-top: 12px;
`;

const LastChoice = styled(Name)`
  margin-top: 12px;

  /* p {
        color: 

    } */
`;

const AuthenManually = styled.div`
  margin-top: 12px;
  > span {
    color: #6b6b6b;
    font-size: 14px;
  }
`;

const AuthenAuto = styled.div`
  margin-top: 12px;

  span {
    color: #6b6b6b;
    font-size: 14px;
  }

  img {
    width: 100%;
    margin-top: 4px;
    margin-bottom: 32px;
  }
`;

const ResultAuthenManually = styled.div`
  margin-top: 8px;
  padding: 17px;
  border: 1px solid #d9d9d9;
  border-radius: 10px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Result = styled.div`
  width: 86px;
  height: 82px;
  border-radius: 5px;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background: ${({ active }) => (active ? "#ececec" : "unset")};

  :hover {
    background: #ececec;
  }

  img {
    width: fit-content;
  }

  :first-child {
    margin-right: 24px;
  }
`;

const BtnAction = styled.div`
  margin-top: 16px;
  box-shadow: 0px -2px 17px rgba(0, 0, 0, 0.16);
  padding: 10px 16px;
  display: flex;
  align-items: center;
  justify-content: center;

  > .ant-btn {
    margin-right: 8px;
    flex: 1;
    color: ${(props) => props.theme.main};

    :hover {
      background: ${(props) => props.theme.darker} !important;
      color: #fff !important;
    }
  }

  .ant-btn-primary {
    background: ${(props) => props.theme.main};
    border: 1px solid ${(props) => props.theme.main};
    color: #fff;

    :hover {
      background: ${(props) => props.theme.darker};
      color: #fff;
    }
  }
`;

const IconVoice = styled.div`
  cursor: pointer;
  width: 26px;
  height: 24px;
  background: #fff;
  box-shadow: 0px 0px 17px rgba(0, 0, 0, 0.1);
  border-radius: 2px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  left: -200px;

  img {
    width: fit-content;
  }
`;

const StatusAuthenAuto = styled.div`
  text-align: center;
  font-family: var(--roboto-500);
  color: #6b6b6b;
  margin-top: 16px;
`;

const WrapAuthenAutoSuccess = styled.div`
  margin-top: 16px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: fit-content;
  }
`;
