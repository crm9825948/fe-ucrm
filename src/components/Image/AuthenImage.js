import { useState, useEffect } from "react";
import { checkTokenExpiration } from "contexts/TokenCheck";
import axios from "axios";

function AuthenImage({ src, onClickImage }) {
  const [image, $image] = useState("");

  useEffect(() => {
    const checkToken = async () => {
      const isTokenValid = await checkTokenExpiration();
      axios
        .get(src, {
          headers: {
            Authorization: isTokenValid,
          },
          responseType: "blob",
        })
        .then((res) => {
          $image(URL.createObjectURL(res.data));
        })
        .catch((err) => {});
    };

    checkToken();
  }, [src]);

  return <img src={image} alt="media" onClick={onClickImage} />;
}

export default AuthenImage;

AuthenImage.defaultProps = {
  onClickImage: () => {},
};
