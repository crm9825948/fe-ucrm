import React from "react";
import ReactDOM from "react-dom";
import "antd/dist/antd.css";
import "./index.scss";
import "./App.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider as ReduxProvider } from "react-redux";
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/lib/integration/react";
import { BrowserRouter } from "react-router-dom";
import { AuthProvider } from "./contexts/JWTContext";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import LoadingScreen from "./components/LoadingScreen";
import "./i18n";
import "animate.css";
import "treeflex/dist/css/treeflex.css";
import "jodit/build/jodit.min.css";
import { BASENAME } from "constants/constants";
import Linkify from "react-linkify";

try {
  if (typeof window.console != "undefined") {
    console.log("Release date: 23/11/2023");
    console.log("Version: 2.63.0");

    window.console = {};
    window.console.log = function () {};
    window.console.debug = function () {};
    window.console.info = function () {};
    window.console.warn = function () {};
    window.console.error = function () {};
  }

  //   if (typeof alert !== 'undefined') {
  //     // eslint-disable-next-line
  //     alert = function () {}
  //   }
} catch (ex) {}
ReactDOM.render(
  <React.StrictMode>
    <ReduxProvider store={store}>
      <PersistGate loading={<LoadingScreen />} persistor={persistor}>
        <BrowserRouter basename={BASENAME}>
          <AuthProvider>
            <Linkify>
              <App />
            </Linkify>
          </AuthProvider>
        </BrowserRouter>
      </PersistGate>
    </ReduxProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
